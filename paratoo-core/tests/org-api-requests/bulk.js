const request = require('supertest')
const { cloneDeep } = require('lodash')
const mock = require('./mock.json')
const { registerMockUris } = require('../helpers/mockAdapter')

test('Core handles when Org cannot find the orgMintedUUID during /status call', async () => {
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs1),
    mock.projectId1,
    mock.protocolId1,
    mock.orgMintedUUID1,
  )

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndPoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(mock.body1)
    .then((data) => {
      console.log('org api request bulk returned data 001 ', data.body)
      expect(data.status).toBe(400)

      const coreErrObj = {
        data: null,
        error: {
          status: 400,
          name: 'Error',
          message: `There was an issue looking up if the collection identifier exists. Got error: No data set found with mintedCollectionId=${mock.orgMintedUUID1}. Unable to check the collection submission status`,
          details: {
            errorCode: 'ERR_MISSING_MINTED_ORG_IDENTIFIER',
          },
        },
      }
      expect(data.body).toStrictEqual(coreErrObj)
    })
})

test('Core handles when Org cannot find the orgMintedUUID during /collection call', async () => {
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs2),
    mock.projectId1,
    mock.protocolId1,
    mock.orgMintedUUID1,
  )

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndPoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(mock.body1)
    .then((data) => {
      console.log('org api request bulk returned data 002 ', data.body)
      expect(data.status).toBe(400)

      const coreErrObj = {
        data: null,
        error: {
          status: 400,
          name: 'Error',
          message: `There was an issue notifying org of the submission. Got error: No data set found with mintedCollectionId=${mock.orgMintedUUID1}. Cannot update the success of a collection that does not exist`,
          details: {
            errorCode: 'ERR_MISSING_MINTED_ORG_IDENTIFIER',
          },
        },
      }
      expect(data.body).toStrictEqual(coreErrObj)
    })
})

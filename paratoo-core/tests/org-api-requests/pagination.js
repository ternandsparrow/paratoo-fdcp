const request = require('supertest')
const { cloneDeep } = require('lodash')
const { API, REQUEST_TYPE } = require('../helpers/constants')
const mock = require('./mock.json')
const { registerMockUris } = require('../helpers/mockAdapter')

// TODO test-to-fail
// based on when org has only 5 records per page and 14 total Plot Selections from init data
test('Core handles when org returns paginated response during Plot Selection submission', async () => {
  await reservePlot()
  let apiMockConfigs = cloneDeep(mock.apiConfigs4)

  // paginated requests for plot selections from org
  for (const orgPlotSelectionResp of mock.response1) {
    let url = API.PLOT_SELECTION
    let paginatedStart = orgPlotSelectionResp?.meta?.pagination?.start
    // url with pagination start
    if (paginatedStart) url += `?pagination[start]=${paginatedStart}`

    apiMockConfigs.push([
      REQUEST_TYPE.GET,
      url,
      200,
      cloneDeep(orgPlotSelectionResp),
    ])
  }

  // request for projects from org
  apiMockConfigs.push([
    REQUEST_TYPE.GET,
    API.USER_PROJECT,
    200,
    cloneDeep(mock.response2),
  ])

  // send plot selection to org
  // the helper creates the POST request, we just need to handle the response from org
  apiMockConfigs.push([
    REQUEST_TYPE.POST,
    API.PLOT_SELECTION,
    200,
    cloneDeep(mock.response3),
  ])

  // update org project with new plot assignment - handle response
  const projectId = mock.response4.id
  apiMockConfigs.push([
    REQUEST_TYPE.PUT,
    `${API.PROJECT}/${projectId}`,
    200,
    cloneDeep(mock.response4),
  ])

  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(apiMockConfigs),
    mock.projectId1,
    mock.protocolId2, // plot selection protocol uuid
    mock.orgMintedUUID2,
  )

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndPoint2)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(mock.body4)
    .then((data) => {
      console.log('paginated returned data 001 ', data.body)
      expect(data.status).toBe(200)
    })
})

async function reservePlot() {
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs3),
    mock.projectId1,
    mock.protocolId2, // plot selection protocol uuid
  )

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.postEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(mock.body3)
    .then((data) => {
      console.log(`reserve plot response:`, data.body)
      expect(data.status).toBe(200)
    })
}

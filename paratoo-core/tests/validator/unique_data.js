const request = require('supertest')
const { cloneDeep } = require('lodash')
const { registerMockUris } = require('../helpers/mockAdapter')
const mock = require('./mock.json')

test('Duplicate visit - setup data - submit visit', async () => {
  registerMockUris(
    cloneDeep(mock.apiConfigs5),
    mock.projectId2,
    mock.protocolId2,
    mock.orgMintedUuid2,
  )

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint2)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt2)
    .send(mock.body2)
    .then((data) => {
      console.log('Duplicate visit - setup data - submit visit returned data ', data.body)
      expect(data.status).toBe(200)
    })
})

test('[test-to-fail] Duplicate visit - submit visit that already exists', async () => {
  registerMockUris(
    cloneDeep(mock.apiConfigs6),
    mock.projectId2,
    mock.protocolId2,
    mock.orgMintedUuid3,
  )

  //send the same request as before; same body, except new orgMintedIdentifier
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint2)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt2)
    .send(mock.body3)
    .then((data) => {
      console.log('Duplicate visit - submit visit that already exists returned data ', data.body)
      expect(data.status).toBe(400)
      expect(data.body.error.message).toBe('Plot visit already exists')
    })
})


test('[test-to-pass] Non-duplicate visit - new visit for same plot', async () => {
  registerMockUris(
    cloneDeep(mock.apiConfigs5),
    mock.projectId2,
    mock.protocolId2,
    mock.orgMintedUuid4,
  )

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint2)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt2)
    .send(mock.body4)
    .then((data) => {
      console.log('Non-duplicate visit - new visit for same plot returned data ', data.body)
      expect(data.status).toBe(200)
    })
})
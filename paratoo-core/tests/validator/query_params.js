import * as cc from '../helpers/constants'

const request = require('supertest')
const mock = require('./mock.json')
const { cloneDeep } = require('lodash')
const { registerMockUris } = require('../helpers/mockAdapter')

/* 
  TODO complete rest of cases
  test-to-pass cases:
  - [done] singular
    - project_id=1, prot_uuid=1
  - [done] plural
    - comma-separated: project_ids=1,2, prot_uuids=1,2
    - indexed: project_ids[0]=1, project_ids[1]=2, prot_uuids[0]=1, prot_uuids[1]=2
    - separate params: project_ids=1, project_ids=2, prot_uuids=1, prot_uuids=2
  - plural with single prot:
    - comma-separated: project_ids=1,2, prot_uuids=1
    - indexed: project_ids[0]=1, project_ids[1]=2, prot_uuids[0]=1
    - separate params: project_ids=1, project_ids=2, prot_uuids=1
  - plural with single project:
    - comma-separated: project_ids=1, prot_uuids=1,2
    - indexed: project_ids[0]=1, prot_uuids[0]=1, prot_uuids[1]=2
    - separate params: project_ids=1, prot_uuids=1, prot_uuids=2
  - plural project, single prot:
    - comma-separated: project_ids=1,2, prot_uuid=1
    - indexed: project_ids[0]=1, project_ids[1]=2, prot_uuid=1
    - separate params: project_ids=1, project_ids=2, prot_uuid=1
  - single project, plural prot:
    - comma-separated: project_id=1, prot_uuids=1,2
    - indexed: project_id=1, prot_uuids[0]=1, prot_uuids[1]=2
    - separate params: project_id=1, prot_uuids=1, prot_uuids=2

*/

for (const [idType, value] of Object.entries(mock.projectIds1)) {
  const testName = `Query params test-to-pass 001 - singular (${idType} project ID)`
  test(testName, async () => {
    // TODO: as value is a list of project ids we should test all of them
    const projectId = value[0]
    const protocolId = mock.protocolUuids1[0]

    let apiMockConfigs = cloneDeep(mock.apiConfigs4)
    // user-projects
    const userProjects = cloneDeep(mock.response1[idType])
    for (const p of userProjects.projects) {
      p.protocols = cloneDeep(mock.protocols1)
    }
    apiMockConfigs.push([
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(userProjects),
    ])

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs), projectId, protocolId)

    const url = `/api/bird-surveys?project_id=${projectId}&prot_uuid=${protocolId}`
    console.log(`making request: ${url}`)
    await request(global.strapiServer)
      .get(url)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .then((data) => {
        console.log(testName, data.body)
        expect(data.status).toBe(200)
      })
  })
}

for (const [idType, value] of Object.entries(mock.projectIds1)) {
  const testName = `Query params test-to-pass 002 - plural, comma-separated params (${idType} project ID)`
  test(testName, async () => {
    let apiMockConfigs = cloneDeep(mock.apiConfigs2)

    // all PDP requests
    for (const p of value) {
      for (const prot of mock.protocolUuids1) {
        // PDP call for each project/protocol combination
        apiMockConfigs.push([
          cc.REQUEST_TYPE.GET,
          `<org>/pdp/${p}/${prot}/read`,
          200,
          { data: { isAuthorised: true } },
        ])
      }
    }

    // user-projects
    const userProjects = cloneDeep(mock.response1[idType])
    for (const p of userProjects.projects) {
      p.protocols = cloneDeep(mock.protocols1)
    }
    apiMockConfigs.push([
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(userProjects),
    ])
    // register apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs))

    const url = `/api/bird-surveys?project_ids=${value.join(
      ',',
    )}&prot_uuids=${mock.protocolUuids1.join(',')}`
    console.log(`making request: ${url}`)
    await request(global.strapiServer)
      .get(url)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .then((data) => {
        console.log(testName, data.body)
        expect(data.status).toBe(200)
      })
  })
}

for (const [idType, value] of Object.entries(mock.projectIds1)) {
  const testName = `Query params test-to-pass 002 - plural, indexed params (${idType} project ID)`
  test(testName, async () => {
    let apiMockConfigs = cloneDeep(mock.apiConfigs2)

    // all PDP requests
    for (const p of value) {
      for (const prot of mock.protocolUuids1) {
        // PDP call for each project/protocol combination
        apiMockConfigs.push([
          cc.REQUEST_TYPE.GET,
          `<org>/pdp/${p}/${prot}/read`,
          200,
          { data: { isAuthorised: true } },
        ])
      }
    }

    // user-projects
    const userProjects = cloneDeep(mock.response1[idType])
    for (const p of userProjects.projects) {
      p.protocols = cloneDeep(mock.protocols1)
    }
    apiMockConfigs.push([
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(userProjects),
    ])
    // register apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs))

    let url = `/api/bird-surveys?`
    for (const [index, proj] of value.entries()) {
      url += `project_ids[${index}]=${proj}&`
    }
    for (const [index, prot] of mock.protocolUuids1.entries()) {
      url += `prot_uuids[${index}]=${prot}`
      if (index < mock.protocolUuids1.length - 1) url += '&'
    }
    console.log(`making request: ${url}`)
    await request(global.strapiServer)
      .get(url)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .then((data) => {
        console.log(testName, data.body)
        expect(data.status).toBe(200)
      })
  })
}

for (const [idType, value] of Object.entries(mock.projectIds1)) {
  const testName = `Query params test-to-pass 002 - plural, separate params (${idType} project ID)`
  test(testName, async () => {
    let apiMockConfigs = cloneDeep(mock.apiConfigs2)

    // all PDP requests
    for (const p of value) {
      for (const prot of mock.protocolUuids1) {
        // PDP call for each project/protocol combination
        apiMockConfigs.push([
          cc.REQUEST_TYPE.GET,
          `<org>/pdp/${p}/${prot}/read`,
          200,
          { data: { isAuthorised: true } },
        ])
      }
    }

    // user-projects
    const userProjects = cloneDeep(mock.response1[idType])
    for (const p of userProjects.projects) {
      p.protocols = cloneDeep(mock.protocols1)
    }
    apiMockConfigs.push([
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(userProjects),
    ])
    // register apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs))

    let url = `/api/bird-surveys?`
    for (const [index, proj] of value.entries()) {
      url += `project_ids=${proj}&`
    }
    for (const [index, prot] of mock.protocolUuids1.entries()) {
      url += `prot_uuids=${prot}`
      if (index < mock.protocolUuids1.length - 1) url += '&'
    }
    console.log(`making request: ${url}`)
    await request(global.strapiServer)
      .get(url)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .then((data) => {
        console.log(testName, data.body)
        expect(data.status).toBe(200)
      })
  })
}

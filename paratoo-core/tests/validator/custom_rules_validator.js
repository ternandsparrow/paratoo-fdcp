const request = require('supertest')
import * as cc from '../helpers/constants'
const { cloneDeep } = require('lodash')
const mock = require('./mock.json')
const {
  createMockData,
  postMockData,
  generateInvalidMockDataUsingRule,
} = require('../helpers/mocker')
const { registerMockUris } = require('../helpers/mockAdapter')

// Temporary cache so that we don't need to generate every time
const PROTOCOLS = {}
const PROTOCOL_MOCKS = {}
const DOCUMENTATION = {}

it('Custom-Rules-Validator-Test-001: Should successfully create and store mock data with correct values', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  if (!PROTOCOLS.CACHED) {
    PROTOCOLS.CACHED = await helpers.getAllProtocols()
  }
  expect(PROTOCOLS.CACHED).toBeDefined()
  if (!DOCUMENTATION.CACHED) {
    DOCUMENTATION.CACHED = await helpers.readFullDocumentation()
  }
  expect(DOCUMENTATION.CACHED).toBeDefined()

  const tempMocks = await createMockData(
    PROTOCOLS.CACHED,
    DOCUMENTATION.CACHED,
    cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS,
  )
  expect(tempMocks).toBeDefined()
  for (const protocol of PROTOCOLS.CACHED) {
    const prot_uuid = protocol['identifier']
    if (!cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS.includes(prot_uuid)) continue
    PROTOCOL_MOCKS[prot_uuid] = tempMocks[prot_uuid]
    expect(PROTOCOL_MOCKS[prot_uuid]).toBeDefined()
  }
})

it('Custom-Rules-Validator-Test-002: Should successfully POST all collections with correct values', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    const prot_uuid = protocol['identifier']
    if (!cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS.includes(prot_uuid)) continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated/updated mocks
    expect(PROTOCOL_MOCKS[prot_uuid]).toBeDefined()
    const surveyMetadata = PROTOCOL_MOCKS[prot_uuid].surveyMetadata
    expect(surveyMetadata).toBeDefined()

    // register apis with axios mock adapter
    registerMockUris(
      cloneDeep(mock.apiConfigs1),
      surveyMetadata.survey_details.project_id,
      surveyMetadata.survey_details.protocol_id,
      surveyMetadata.orgMintedUUID,
    )

    await postMockData(
      `Custom-Rules-Validator-Test-002-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      PROTOCOL_MOCKS[prot_uuid].data,
      200,
    )
  }
})

it('Custom-Rules-Validator-Test-003: Test the ability of validator to reject requests to POST collections which fail to satisfy rule multipleValuesMinimumCheck', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    const prot_uuid = protocol['identifier']
    if (!cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS.includes(prot_uuid)) continue
    const validMock = await createMockData(
      [protocol],
      DOCUMENTATION.CACHED,
      cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS,
    )
    const inValidMock = generateInvalidMockDataUsingRule(
      protocol,
      validMock[prot_uuid],
      cc.CUSTOM_RULE_TYPES.multipleValuesMinimumCheck,
    )

    // if protocol doest have any custom rule assigned
    if (!inValidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    const endpoint = `${protocol['endpointPrefix']}/bulk`
    await postMockData(
      `Custom-Rules-Validator-Test-003-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      inValidMock.data,
      inValidMock.errorCode,
      inValidMock.errorMessage,
    )
  }
})

it('Custom-Rules-Validator-Test-004: Test the ability of validator to reject requests to POST collections which fail to satisfy rule OneDecimalPlaceCheck', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    const prot_uuid = protocol['identifier']
    if (!cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS.includes(prot_uuid)) continue
    const validMock = await createMockData(
      [protocol],
      DOCUMENTATION.CACHED,
      cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS,
    )

    const inValidMock = generateInvalidMockDataUsingRule(
      protocol,
      validMock[prot_uuid],
      cc.CUSTOM_RULE_TYPES.OneDecimalPlaceCheck,
    )
    // if protocol doest have any custom rule assigned
    if (!inValidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    const endpoint = `${protocol['endpointPrefix']}/bulk`
    await postMockData(
      `Custom-Rules-Validator-Test-004-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      inValidMock.data,
      inValidMock.errorCode,
      inValidMock.errorMessage,
    )
  }
})

it('Custom-Rules-Validator-Test-005: Test the ability of validator to reject requests to POST collections which fail to satisfy rule maximumDifferenceCheck', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    const prot_uuid = protocol['identifier']
    if (!cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS.includes(prot_uuid)) continue
    const validMock = await createMockData(
      [protocol],
      DOCUMENTATION.CACHED,
      cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS,
    )

    const inValidMock = generateInvalidMockDataUsingRule(
      protocol,
      validMock[prot_uuid],
      cc.CUSTOM_RULE_TYPES.maximumDifferenceCheck,
    )
    // if protocol doest have any custom rule assigned
    if (!inValidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    const endpoint = `${protocol['endpointPrefix']}/bulk`
    await postMockData(
      `Custom-Rules-Validator-Test-005-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      inValidMock.data,
      inValidMock.errorCode,
      inValidMock.errorMessage,
    )
  }
})

it('Custom-Rules-Validator-Test-006: Test the ability of validator to reject requests to POST collections which fail to satisfy rule ConditionalFieldsCheck', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    const prot_uuid = protocol['identifier']
    if (!cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS.includes(prot_uuid)) continue
    const validMock = await createMockData(
      [protocol],
      DOCUMENTATION.CACHED,
      cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS,
    )
    const inValidMock = generateInvalidMockDataUsingRule(
      protocol,
      validMock[prot_uuid],
      cc.CUSTOM_RULE_TYPES.ConditionalFieldsCheck,
    )
    // if protocol doest have any custom rule assigned
    if (!inValidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    const endpoint = `${protocol['endpointPrefix']}/bulk`
    await postMockData(
      `Custom-Rules-Validator-Test-006-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      inValidMock.data,
      inValidMock.errorCode,
      inValidMock.errorMessage,
    )
  }
})

it('Custom-Rules-Validator-Test-007: Test the ability of validator to reject requests to POST collections which fail to satisfy rule minNumOfReplicates', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    const prot_uuid = protocol['identifier']
    if (!cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS.includes(prot_uuid)) continue
    const validMock = await createMockData(
      [protocol],
      DOCUMENTATION.CACHED,
      cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS,
    )
    const inValidMock = generateInvalidMockDataUsingRule(
      protocol,
      validMock[prot_uuid],
      cc.CUSTOM_RULE_TYPES.minNumOfReplicates,
    )
    // if protocol doest have any custom rule assigned
    if (!inValidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    const endpoint = `${protocol['endpointPrefix']}/bulk`
    await postMockData(
      `Custom-Rules-Validator-Test-007-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      inValidMock.data,
      inValidMock.errorCode,
      inValidMock.errorMessage,
    )
  }
})

it('Custom-Rules-Validator-Test-008: Test the ability of validator to reject requests to POST collections which fail to satisfy rule distanceBetweenReplicates', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    const prot_uuid = protocol['identifier']
    if (!cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS.includes(prot_uuid)) continue
    const validMock = await createMockData(
      [protocol],
      DOCUMENTATION.CACHED,
      cc.CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS,
    )

    const inValidMock = generateInvalidMockDataUsingRule(
      protocol,
      validMock[prot_uuid],
      cc.CUSTOM_RULE_TYPES.distanceBetweenReplicates,
    )
    // if protocol doest have any custom rule assigned
    if (!inValidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    const endpoint = `${protocol['endpointPrefix']}/bulk`
    await postMockData(
      `Custom-Rules-Validator-Test-008-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      inValidMock.data,
      inValidMock.errorCode,
      inValidMock.errorMessage,
    )
  }
})

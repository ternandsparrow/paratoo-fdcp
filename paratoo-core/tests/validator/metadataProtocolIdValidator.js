const request = require('supertest')
const { registerMockUris } = require('../helpers/mockAdapter')
const mock = require('./mock.json')
const { cloneDeep } = require('lodash')

test('MetadataProtocolIdValidator: Failure to POST a collection of bird surveys to Core when protocol ID in survey metadata does not match the actual protocol id', async () => {
  const metadata =
    mock.body1.data.collections[0]['bird-survey'].data.survey_metadata

  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(cloneDeep(mock.apiConfigs3)),
    metadata.project_id,
    metadata.protocol_id,
  )

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(mock.body1)
    .then((data) => {
      console.log('returned data 011 ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Protocol ID in metadata does not match the actual protocol ID of "Vertebrate Fauna - Bird Survey"',
      )
    })
})

const request = require('supertest')
import * as cc from '../helpers/constants'
const { cloneDeep } = require('lodash')
const mock = require('./mock.json')
const {
  createMockData,
  generateInvalidMockData,
  postMockData,
  prettyFormatFieldName,
} = require('../helpers/mocker')
const { registerMockUris } = require('../helpers/mockAdapter')

// Temporary cache so that we don't need to generate every time
const PROTOCOLS = {}
const PROTOCOL_MOCKS = {}

it('Validator-Test-001: Should successfully create and store mock data with correct values', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  if (!PROTOCOLS.CACHED) {
    PROTOCOLS.CACHED = await helpers.getAllProtocols()
  }
  expect(PROTOCOLS.CACHED).toBeDefined()
  const documentation = await helpers.readFullDocumentation()

  const tempMocks = await createMockData(
    PROTOCOLS.CACHED,
    documentation,
    cc.AJV_VALIDATOR_TEST_PROTOCOLS,
  )
  expect(tempMocks).toBeDefined()
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    PROTOCOL_MOCKS[protocol['identifier']] = tempMocks[protocol['identifier']]
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()
  }
})

it('Validator-Test-002: Should successfully POST all collections with correct values', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated/updated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const surveyMetadata = PROTOCOL_MOCKS[protocol['identifier']].surveyMetadata
    expect(surveyMetadata).toBeDefined()

    // register apis with axios mock adapter
    registerMockUris(
      cloneDeep(mock.apiConfigs1),
      surveyMetadata.survey_details.project_id,
      surveyMetadata.survey_details.protocol_id,
      surveyMetadata.orgMintedUUID,
    )

    await postMockData(
      `Validator-Test-002-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      PROTOCOL_MOCKS[protocol['identifier']].data,
      200,
    )
  }
})

it('Ajv-Validator-Test-001: Test the ability of ajv validator to reject requests to POST collections with invalid string', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue

    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.STRING,
    )
    // if no string type field found
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-001-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be string`,
    )
  }
})

it('Ajv-Validator-Test-002: Test the ability of ajv validator to reject requests to POST collections with invalid integer', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.INTEGER,
    )
    // if no integer type field found
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-002-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be integer`,
    )
  }
})

it('Ajv-Validator-Test-003: Test the ability of ajv validator to reject requests to POST collections with invalid boolean', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.BOOLEAN,
    )
    // if no boolean type field found
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-003-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be boolean`,
    )
  }
})

it('Ajv-Validator-Test-004: Test the ability of ajv validator to reject requests to POST collections with invalid enum', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.STRING,
      null,
      cc.PROPERTY_TYPE.ENUM,
    )
    // if no enum field found
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-004-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be equal to one of the allowed values`,
    )
  }
})

it('Ajv-Validator-Test-005: Test the ability of ajv validator to reject requests to POST collections with invalid float', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
    )
    // if no float field found
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-005-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be number`,
    )
  }
})

it('Ajv-Validator-Test-006: Test the ability of ajv validator to reject requests to POST collections with invalid model reference', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.INTEGER,
      null,
      null,
      'x-model-ref',
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-006-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be integer`,
    )
  }
})

it('Ajv-Validator-Test-007: Test the ability of ajv validator to reject requests to POST collections with missing required integer field', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.INTEGER,
      null,
      null,
      null,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-007-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `must have required property '${invalidMock.fieldData.field}'`,
    )
  }
})

it('Ajv-Validator-Test-008: Test the ability of ajv validator to reject requests to POST collections with missing required float field', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
      null,
      null,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-008-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `must have required property '${invalidMock.fieldData.field}'`,
    )
  }
})

it('Ajv-Validator-Test-009: Test the ability of ajv validator to reject requests to POST collections with missing required string field', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.STRING,
      null,
      null,
      null,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-009-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `must have required property '${invalidMock.fieldData.field}'`,
    )
  }
})

it('Ajv-Validator-Test-010: Test the ability of ajv validator to reject requests to POST collections with missing required boolean field', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.BOOLEAN,
      null,
      null,
      null,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-010-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `must have required property '${invalidMock.fieldData.field}'`,
    )
  }
})

it('Ajv-Validator-Test-011: Test the ability of ajv validator to reject requests to POST collections with missing required string with date-time format', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.STRING,
      cc.FORMAT.DATE_TIME,
      null,
      null,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-011-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `must have required property '${invalidMock.fieldData.field}'`,
    )
  }
})

it('Ajv-Validator-Test-011: Test the ability of ajv validator to reject requests to POST collections when a float number is less then its minimum', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
      cc.PROPERTY_TYPE.MINIMUM,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-011-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be >= ${invalidMock.fieldData.propertyValue}`,
    )
  }
})
it('Ajv-Validator-Test-012: Test the ability of ajv validator to reject requests to POST collections when a float number is greater then its maximum', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
      cc.PROPERTY_TYPE.MAXIMUM,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-012-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be <= ${invalidMock.fieldData.propertyValue}`,
    )
  }
})

it('Ajv-Validator-Test-013: Test the ability of ajv validator to reject requests to POST collections when an integer is less then its minimum', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.INTEGER,
      null,
      cc.PROPERTY_TYPE.MINIMUM,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-013-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be >= ${invalidMock.fieldData.propertyValue}`,
    )
  }
})
it('Ajv-Validator-Test-014: Test the ability of ajv validator to reject requests to POST collections when an integer is greater then its maximum', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.INTEGER,
      null,
      cc.PROPERTY_TYPE.MAXIMUM,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-014-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be <= ${invalidMock.fieldData.propertyValue}`,
    )
  }
})

it('Ajv-Validator-Test-015: Test the ability of ajv validator to reject requests to POST collections with same unique value', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    // as most of the unique fields are required e.g. voucher_barcode
    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.STRING,
      null,
      cc.PROPERTY_TYPE.UNIQUE,
      null,
      true,
    )
    if (!invalidMock) continue

    const field = prettyFormatFieldName(invalidMock.fieldData.field)

    const surveyMetadata = PROTOCOL_MOCKS[protocol['identifier']].surveyMetadata
    expect(surveyMetadata).toBeDefined()

    // register apis with axios mock adapter
    registerMockUris(
      cloneDeep(mock.apiConfigs1),
      surveyMetadata.survey_details.project_id,
      surveyMetadata.survey_details.protocol_id,
      surveyMetadata.orgMintedUUID,
    )

    await postMockData(
      `Ajv-Validator-Test-015-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      //FIXME can't pass `invalidMock.fieldData.field` as we'll get a false positive from orgMintedUUID, not the field we're actually trying to test (related to todo in `createSurveyId`)
      // `Survey has failure: Fields are not unique: ${invalidMock.fieldData.field}`
      'Fields are not unique',
    )
  }
})

it('Ajv-Validator-Test-016: Test the ability of ajv validator to reject requests to POST collections when a string is greater then maxLength', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.STRING,
      null,
      cc.PROPERTY_TYPE.MAXLENGTH,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-016-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must NOT have more than ${invalidMock.fieldData.propertyValue} characters`,
    )
  }
})

it('Ajv-Validator-Test-017: Test the ability of ajv validator to reject requests to POST collections with an invalid component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.NON_REPEATABLE_COMPONENT,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-017-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be object`,
    )
  }
})

it('Ajv-Validator-Test-018: Test the ability of ajv validator to reject requests to POST collections with an invalid repeatable component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.REPEATABLE_COMPONENT,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-018-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be array`,
    )
  }
})

it('Ajv-Validator-Test-019: Test the ability of ajv validator to reject requests to POST collections when number of components is greater then maxItems', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.REPEATABLE_COMPONENT,
      null,
      cc.PROPERTY_TYPE.MAXITEMS,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-019-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must NOT have more than ${invalidMock.fieldData.propertyValue} items`,
    )
  }
})

it('Ajv-Validator-Test-020: Test the ability of ajv validator to reject requests to POST collections with invalid enum inside a component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.STRING,
      null,
      cc.PROPERTY_TYPE.ENUM,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-020-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be equal to one of the allowed values`,
    )
  }
})

it('Ajv-Validator-Test-021: Test the ability of ajv validator to reject requests to POST collections with missing required enum inside a component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.STRING,
      null,
      cc.PROPERTY_TYPE.ENUM,
      null,
      true,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-021-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `must have required property '${invalidMock.fieldData.field}`,
    )
  }
})
it('Ajv-Validator-Test-021: Test the ability of ajv validator to reject requests to POST collections with invalid string inside component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue

    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.STRING,
      null,
      null,
      null,
      false,
      true,
    )
    // if no string type field found
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-021-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be string`,
    )
  }
})

it('Ajv-Validator-Test-022: Test the ability of ajv validator to reject requests to POST collections with invalid integer inside component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.INTEGER,
      null,
      null,
      null,
      false,
      true,
    )
    // if no integer type field found
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-022-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be integer`,
    )
  }
})

it('Ajv-Validator-Test-023: Test the ability of ajv validator to reject requests to POST collections with invalid boolean inside component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.BOOLEAN,
      null,
      null,
      null,
      false,
      true,
    )
    // if no boolean type field found
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-023-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be boolean`,
    )
  }
})

it('Ajv-Validator-Test-024: Test the ability of ajv validator to reject requests to POST collections with invalid float inside component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
      null,
      null,
      false,
      true,
    )
    // if no float field found
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-024-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be number`,
    )
  }
})

it('Ajv-Validator-Test-025: Test the ability of ajv validator to reject requests to POST collections with missing required integer field inside component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.INTEGER,
      null,
      null,
      null,
      true,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-025-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `must have required property '${invalidMock.fieldData.field}'`,
    )
  }
})

it('Ajv-Validator-Test-026: Test the ability of ajv validator to reject requests to POST collections with missing required float field inside component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
      null,
      null,
      true,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-026-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `must have required property '${invalidMock.fieldData.field}'`,
    )
  }
})

it('Ajv-Validator-Test-027: Test the ability of ajv validator to reject requests to POST collections with missing required string field inside component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.STRING,
      null,
      null,
      null,
      true,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-027-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `must have required property '${invalidMock.fieldData.field}'`,
    )
  }
})

it('Ajv-Validator-Test-028: Test the ability of ajv validator to reject requests to POST collections with missing required boolean field inside component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.BOOLEAN,
      null,
      null,
      null,
      true,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-028-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `must have required property '${invalidMock.fieldData.field}'`,
    )
  }
})

it('Ajv-Validator-Test-029: Test the ability of ajv validator to reject requests to POST collections when a float number is less then its minimum inside component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
      cc.PROPERTY_TYPE.MINIMUM,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-029-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be >= ${invalidMock.fieldData.propertyValue}`,
    )
  }
})
it('Ajv-Validator-Test-030: Test the ability of ajv validator to reject requests to POST collections when a float number is greater then its maximum', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
      cc.PROPERTY_TYPE.MAXIMUM,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-030-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be <= ${invalidMock.fieldData.propertyValue}`,
    )
  }
})

it('Ajv-Validator-Test-031: Test the ability of ajv validator to reject requests to POST collections when an integer is less then its minimum inside component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.INTEGER,
      null,
      cc.PROPERTY_TYPE.MINIMUM,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-031-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be >= ${invalidMock.fieldData.propertyValue}`,
    )
  }
})
it('Ajv-Validator-Test-032: Test the ability of ajv validator to reject requests to POST collections when an integer is greater then its maximum inside component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.INTEGER,
      null,
      cc.PROPERTY_TYPE.MAXIMUM,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-032-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be <= ${invalidMock.fieldData.propertyValue}`,
    )
  }
})

it('Ajv-Validator-Test-033: Test the ability of ajv validator to reject requests to POST collections with same unique value inside component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    // as most of the unique fields are required e.g. voucher_barcode
    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.STRING,
      null,
      cc.PROPERTY_TYPE.UNIQUE,
      null,
      true,
      true,
    )
    if (!invalidMock) continue

    const field = prettyFormatFieldName(invalidMock.fieldData.field)
    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-033-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `This attribute must be unique. The following observation field must be unique: ${field}`,
    )
  }
})

it('Ajv-Validator-Test-034: Test the ability of ajv validator to reject requests to POST collections when a string in component is greater then maxLength', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.STRING,
      null,
      cc.PROPERTY_TYPE.MAXLENGTH,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-034-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must NOT have more than ${invalidMock.fieldData.propertyValue} characters`,
    )
  }
})

it('Ajv-Validator-Test-035: Test the ability of ajv validator to reject requests to POST collections with an invalid nested component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.NON_REPEATABLE_COMPONENT,
      null,
      null,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-035-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be object`,
    )
  }
})

it('Ajv-Validator-Test-036: Test the ability of ajv validator to reject requests to POST collections with an invalid repeatable nested component', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.REPEATABLE_COMPONENT,
      null,
      null,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-036-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must be array`,
    )
  }
})

it('Ajv-Validator-Test-037: Test the ability of ajv validator to reject requests to POST collections when number of components is greater then maxItems', async () => {
  for (const protocol of PROTOCOLS.CACHED) {
    if (!cc.AJV_VALIDATOR_TEST_PROTOCOLS.includes(protocol['identifier']))
      continue
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    // we have already generated mocks
    expect(PROTOCOL_MOCKS[protocol['identifier']]).toBeDefined()

    const invalidMock = await generateInvalidMockData(
      protocol,
      PROTOCOL_MOCKS[protocol['identifier']],
      cc.FIELD_TYPE.REPEATABLE_COMPONENT,
      null,
      cc.PROPERTY_TYPE.MAXITEMS,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(mock.apiConfigs2))

    await postMockData(
      `Ajv-Validator-Test-037-${protocol.name}`,
      global.strapiServer,
      `/api${endpoint}`,
      invalidMock.data,
      400,
      `${invalidMock.fieldData.field} must NOT have more than ${invalidMock.fieldData.propertyValue} items`,
    )
  }
})

import request from 'supertest'
import * as cc from '../helpers/constants'

const { cloneDeep } = require('lodash')
const mock = require('./mock.json')
const { createSurveyId, createOrgIdentifier } = require('../helpers/mocker')
const { registerMockUris } = require('../helpers/mockAdapter')

function makeBulkPayload() {
  const payload = cloneDeep(mock.body1)
  const surveyMetadata = createSurveyId(
    'bird-survey',
    mock.protocolId1,
    mock.projectId1,
  )
  payload.data.collections[0]['bird-survey'].data.survey_metadata =
    cloneDeep(surveyMetadata)

  payload.data.collections[0].orgMintedIdentifier = createOrgIdentifier(
    cloneDeep(surveyMetadata),
    new Date(Date.now()), // current time
  )

  return cloneDeep(payload)
}

test('Use-cache 1: should return the same response even there is a new submission if use-cache=true flag is used', async () => {
  process.env.ENABLE_CACHE = true

  let apiMockConfigs = cloneDeep(mock.apiConfigs1)
  apiMockConfigs.push([
    cc.REQUEST_TYPE.GET,
    cc.API.USER_PROJECT,
    200,
    cloneDeep(mock.userProjects1),
  ])
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(apiMockConfigs), mock.projectId1, mock.protocolId1)

  const getResponse1 = await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get(mock.getEndPoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)

  expect(getResponse1.status).toBe(200)
  const cachedResponseData = getResponse1.body

  const requestBody = makeBulkPayload()
  const surveyMetadata =
    requestBody.data.collections[0]['bird-survey'].data.survey_metadata

  // as cache is enabled so core will skip calling /validate-token API
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs2),
    surveyMetadata.survey_details.project_id,
    surveyMetadata.survey_details.protocol_id,
    surveyMetadata.orgMintedUUID,
  )

  const postResponse = await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndPoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)

  expect(postResponse.status).toBe(200)

  // only pdp(read) is required
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs3),
    mock.projectId1,
    mock.protocolId1,
  )

  const getResponse2 = await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get(`${mock.getEndPoint1}&use-cache=true`)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)

  expect(getResponse2.status).toBe(200)
  expect(getResponse2.body).toEqual(cachedResponseData)
})

test('Use-cache 2: should return the fresh data after a new submission was made if use-cache flag is not used', async () => {
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs3),
    mock.projectId1,
    mock.protocolId1,
  )

  const getResponse1 = await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get(mock.getEndPoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
  // console.log('use cache 2 GET resp body 1:', JSON.stringify(getResponse1.body, null, 2))
  expect(getResponse1.status).toBe(200)
  const cachedResponseData = getResponse1.body

  const requestBody = makeBulkPayload()
  const surveyMetadata =
    requestBody.data.collections[0]['bird-survey'].data.survey_metadata

  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs2),
    surveyMetadata.survey_details.project_id,
    surveyMetadata.survey_details.protocol_id,
    surveyMetadata.orgMintedUUID,
  )

  const response = await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndPoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
  // console.log('use cache 2 POST resp body:', JSON.stringify(response.body, null, 2))
  expect(response.status).toBe(200)

  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs3),
    mock.projectId1,
    mock.protocolId1,
  )
  const getResponse2 = await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get(mock.getEndPoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)

  expect(getResponse2.status).toBe(200)
  expect(getResponse2.body).not.toEqual(cachedResponseData)
})

test('Use-cache 3: should return the fresh data after a new submission is made if use-cache=false flag is used', async () => {
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs3),
    mock.projectId1,
    mock.protocolId1,
  )
  const getResponse1 = await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get(mock.getEndPoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
  // console.log('use cache 3 GET resp body 1:', JSON.stringify(getResponse1.body, null, 2))
  expect(getResponse1.status).toBe(200)
  const cachedResponseData = getResponse1.body

  const requestBody = makeBulkPayload()
  const surveyMetadata =
    requestBody.data.collections[0]['bird-survey'].data.survey_metadata

  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs2),
    surveyMetadata.survey_details.project_id,
    surveyMetadata.survey_details.protocol_id,
    surveyMetadata.orgMintedUUID,
  )
  const response = await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndPoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
  // console.log('use cache 3 POST resp body:', JSON.stringify(response.body, null, 2))
  expect(response.status).toBe(200)

  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs3),
    mock.projectId1,
    mock.protocolId1,
  )
  const getResponse2 = await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get(`${mock.getEndPoint1}&use-cache=false`)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
  // console.log('use cache 3 GET resp body 2:', JSON.stringify(getResponse2.body, null, 2))
  expect(getResponse2.status).toBe(200)
  expect(getResponse2.body).not.toEqual(cachedResponseData)
})

// TODO: add test for plot-selections as use-cache flag also apply to user-project for all plot models, but this require a lot of mocking

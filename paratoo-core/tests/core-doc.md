# Paratoo-Core API implementation

Please update this document every time you make changes to core interface
>NOTE: the expected data (especially for bulk) in the [test cases](./test-cases.md) doc and this doc differs from the actual test data. The tests have the most up-to-date data, but soon both the tests and test cases will be out of date.

>TODO: update test and test case data when `michael-webapp` branch is finalized.

#### Example of Expected input format:

```
{
  "headers": "Bearer " <jwt>
}
```
Note: the below `orgMintedIdentifier` is created for `projectId` = 2, `protocolId` = 1, and `protocol version` equals 1. The `survey_metadata` is `"124"`.
```
{
        "collections": [
            {
                "orgMintedIdentifier": "{\"iv\":\"MPLwAthmU4pNrKso90vEhg==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"LSiJN8fhBoQ=\",\"ct\":\"7Zx7AQ4t+DpAkTeepmg0/Kyll4MdgpwzQ9XHRvONbwTqhUNYrkoKN41wSDwdBPZyqip3dfJOwIuOP2nQsmDlTvaj67Pd3Z3DzE4JBDGcp+LvheepW4m4ioB8sCZkPMMxDIWgJ3l0D25Bm0+TMpI1bs4fTg18HTeOukDoLdk=\"}",
                "bird-survey": {
                    "start_date": "2021-08-26T00:26:54.317Z",
                    "end_date": "2021-08-26T00:26:54.317Z",
                    "playback_used": true,
                    "survey_type": "202",
                    "survey_metadata": "124",
                    "site_location_id": 2
                },
                "weather-survey": {
                    "temperature": 10,
                    "precipitation": "NO",
                    "precipitation_duration": "I",
                    "wind_description": "C",
                    "cloud_cover": "O"
                },
                "bird-survey-observation": [
                    {
                        "species": "BFHO",
                        "count": 464,
                        "observation_type": "S",
                        "activity_type": "FO",
                        "observation_location_type": "WS",
                        "breeding_type": "NE",
                        "fauna_maturity": "A"
                    },
                    {
                        "species": "BFHO",
                        "count": 98,
                        "observation_type": "S",
                        "activity_type": "FO",
                        "observation_location_type": "WS",
                        "breeding_type": "NE",
                        "fauna_maturity": "A"
                    }
                ]
            }
        ]
    }

```
#### Example of Success Response 

Status code: 200
Response: 
```
{
    "collections": [
        {
            "orgMintedIdentifier": "{\"iv\":\"xFYEjE/8tQbcJXistpp6iw==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"poTut/98+Vw=\",\"ct\":\"U3LQuX8DmUV4e3M1HwvVK8uPeKTISrmIDudJQwqwGfEpwW4NGap5Pnz5DDfJbYWUgH558+kFXA39bSeKSMdYqBPXL2VTxZDAhlOaI1cPI+19uiJUCQbkipNhznR4Ld7S0DSGG6gctupzd38uP8VoCMrsbm7TMYcYbDfNTIp8\"}",
            "bird-survey": {
                "start_date": "2021-08-25T00:26:54.317Z",
                "end_date": "2021-08-25T00:26:54.317Z",
                "playback_used": true,
                "survey_type": "202",
                "survey_metadata": "124",
                "site_location_id": 2
            },
            "weather-survey": {
                "temperature": 10,
                "precipitation": "NO",
                "precipitation_duration": "I",
                "wind_description": "C",
                "cloud_cover": "O"
            },
            "bird-survey-observation": [
                {
                    "species": "Bird1",
                    "count": 464,
                    "observation_type": "S",
                    "activity_type": "FO",
                    "observation_location_type": "WS",
                    "breeding_type": "NE",
                    "fauna_maturity": "P"
                },
                {
                    "species": "Bird3",
                    "count": 98,
                    "observation_type": "S",
                    "activity_type": "FO",
                    "observation_location_type": "WS",
                    "breeding_type": "NE",
                    "fauna_maturity": "P"
                }
            ]
        }
    ]
}

```

#### Expected error code

Error Code | Message | Reason | Implemented | Tested | TestID
-----------|---------|--------|-------------|--------|-------
403 | User is not authenticated (logged in) | User is not authenticated (no JWT attached or invalid JWT) | [x] | [x] | Bird-Srvy-POST-bulk-002
400 | Invalid: data/collections/0/orgMintedIdentifier must NOT have fewer than 1 characters | `orgMintedIdentifier` is not provided | [x] | [x] | Bird-Srvy-POST-bulk-003
400 | json decrypt: invalid parameters. Could not decrypt the orgMintedIdentifier | Unable to decrypt identifier | [x] | [x] | Bird-Srvy-POST-bulk-004
400 | Invalid: data/collections/0/bird_survey/start_time must match format \"date-time\" <br>or Invalid: data/collections/0/bird_survey/end_date must match format \"date-time\" <br>or Invalid: data/collections/0/bird_survey/playback_used must be boolean <br>or Invalid: data/collections/0/bird_survey/survey_type must be integer <br>or Invalid: data/collections/0/bird_survey/survey_metadata must NOT have fewer than 1 characters | Provided survey data is invalid | [x] | [x] | Bird-Srvy-POST-bulk-005
400 | Invalid: Invalid: data/collections/0/weather-survey/temperature must be number <br>or Invalid: Invalid: data/collections/0/weather-survey/precipitation must be equal to one of the allowed values <br>or Invalid: data/collections/0/weather-survey/precipitation_duration must be equal to one of the allowed values <br> or Invalid: data/collections/0/weather-survey/wind_description must be equal to one of the allowed values <br>or Invalid: data/collections/0/weather-survey/cloud_cover must be equal to one of the allowed values | Weather data is invalid | [x] | [x] | Bird-Srvy-POST-bulk-006
400 | Invalid: data/collections/0/bird_survey_observations must NOT have fewer than 1 items | `bird_survey_observations` array is empty | [x] | [x] | Bird-Srvy-POST-bulk-007
400 | Invalid: data/collections/0/bird_survey_observations/0/species must be string <br>or Invalid: data/collections/0/bird_survey_observations/0/count must be integer <br>or Invalid: data/collections/0/bird-survey-observation/0/observation_type must be equal to one of the allowed values <br>or Invalid: data/collections/0/bird-survey-observation/1/activity_type must be equal to one of the allowed values <br>or observation_location_type <br> or nvalid: data/collections/0/bird-survey-observation/1/breeding_type must be equal to one of the allowed values | `bird_survey_observations` object is invalid | [x] | [x] | Bird-Srvy-POST-bulk-008
400 | Invalid: data/collections must NOT have fewer than 1 items | `collections` array is empty | [x] | [x] | Bird-Srvy-POST-bulk-009
401 | User is NOT authorized to write project 1 | The `bird survey` protocol is not writable | [x] | [x] | Bird-Srvy-POST-bulk-010
400 | Collection has already been submitted; the collection identifier already exists | The `collectionIdentifier` already exists - CANT TEST | [x] | [] | Bird-Srvy-POST-bulk-011

### GET /bird-surveys

#### Example of Expected input format:

```
{
  "headers": "Bearer " <jwt>
}
```
#### Example of Success Response 

Status code: 200
Response: 
```
[
  {
    "id": 1,
    "start_time": "2020-12-09T11:54:38.163Z",
    "end_date": "2020-12-10T11:54:38.163Z",
    "survey_type": {
      "id": 1,
      "abbreviation": "202",
      "label": "20 minute, 2ha",
      "created_at": "2021-09-16T04:58:20.648Z",
      "updated_at": "2021-09-16T04:58:20.648Z"
    },
    "site_location_id": null,
    "playback_used": true,
    "created_at": "2021-09-16T04:58:54.370Z",
    "updated_at": "2021-09-16T04:58:54.375Z"
  }
]
```
#### Expected error code

Error Code | Message | Reason | Implemented | Tested | TestID
-----------|---------|--------|-------------|--------|--------
403 | User is not authenticated (logged in) | Failure to find bird-surveys when not authenticated | [x] | [x] | Bird-Srvy-GET-001
404 | Could not find protocol for endpoint: '/foo' | Provided endpoint prefix could not be matched to a protocol ID<br>NOTE: there might be issues testing this, see the test case doc -CANT TEST | [] | [] | Bird-Srvy-GET-003
404 | User does not have any projects | User does not have any projects assigned to them | [x] | [x] | Bird-Srvy-GET-004
401 | User is not authorized (does not have projects assigned for given protocol of ID 1) | User does not have any projects assigned to them for the given protocol | [x] | [x] | Bird-Srvy-GET-005
418 | Request failed with status code 418. Something happened to the PDP. The PDP encountered an error | PDP encounters a problem<br>NOTE: there might be issues testing this, see the test case doc - CANT TEST | [] | [] | Bird-Srvy-GET-006
401 | Auth token is not valid | JWT is not valid | [x] | [x] | Bird-Srvy-GET-007

### POST /bird-surveys
#### Example of Expected input format:

```
{
  "headers": "Bearer " <jwt>
}
```
```
{
    "start_time": "2020-12-10T11:54:38.163Z",
    "end_date": "2020-12-11T11:54:38.163Z",
    "playback_used": true,
    "survey_type": "202",
    "survey_metadata": "125"
}
```
#### Example of Success Response 

Status code: 200
Response: 
```
{
  "id": 55,
  "start_time": "2020-12-09T11:54:38.163Z",
  "end_date": "2020-12-10T11:54:38.163Z",
  "survey_type": {
    "id": 1,
    "abbreviation": "202",
    "label": "20 minute, 2ha",
    "created_at": "2021-09-12T23:30:51.094Z",
    "updated_at": "2021-09-12T23:30:51.094Z"
  },
  "site_location_id": null,
  "playback_used": true,
  "created_at": "2021-09-17T01:06:24.973Z",
  "updated_at": "2021-09-17T01:06:24.977Z"
}
```
#### Expected error code
Error Code | Message | Reason | Implemented | Tested | TestID
-----------|---------|--------|-------------|--------|--------
403 | User is not authenticated (logged in) | User is not authenticated | [x] | [x] | Bird-Srvy-POST-002
400 | Invalid: data must have required property 'playback_used' | Empty request body | [x] | [x] | Bird-Srvy-POST-003

### PUT /bird-surveys{id}
#### Example of Expected input format:

```
{
    "headers": "Bearer " <jwt>
}
```
```
{
  "start_time": "2020-12-09T11:54:38.163Z",
  "end_date": "2025-12-10T11:54:38.163Z",
  "playback_used": true,
  "survey_type": 2,
  "survey_metadata": "123"
}
```
#### Example of Success Response 

Status code: 200
Response: 
```
{
  "id": 55,
  "start_time": "2020-12-09T11:54:38.163Z",
  "end_date": "2025-12-10T11:54:38.163Z",
  "survey_type": {
    "id": 2,
    "abbreviation": "500",
    "label": "500m area search",
    "created_at": "2021-09-12T23:30:51.101Z",
    "updated_at": "2021-09-12T23:30:51.101Z"
  },
  "site_location_id": null,
  "playback_used": true,
  "created_at": "2021-09-17T01:06:24.973Z",
  "updated_at": "2021-09-17T01:07:40.075Z"
}
```
#### Expected error code
Error Code | Message | Reason | Implemented | Tested | TestID
-----------|---------|--------|-------------|--------|--------
403 | User is not authenticated (logged in) | User is not authenticated | [x] | [x] | Bird-Srvy-PUT-002
404 | entry.notFound | Supplied ID does not exist in the database | [x] | [x] | Bird-Srvy-PUT-003
400 | id must be a `number` type, but the final value was: `NaN` (cast from the value `\"foo\"`)."} | The supplied ID is not a valid parameter | [x] | [x] | Bird-Srvy-PUT-004
n/a | Method Not Allowed | No ID is supplied (no path parameters) | [x] | [x] | Bird-Srvy-PUT-005

### DELETE /bird-surveys{id}
#### Example of Expected input format:

```
{
  "headers": "Bearer " <jwt>
}
```
#### Example of Success Response 

Status code: 200
Response: 
```
{
  "id": 55,
  "start_time": "2020-12-09T11:54:38.163Z",
  "end_date": "2025-12-10T11:54:38.163Z",
  "survey_type": {
      "id": 2,
      "abbreviation": "500",
      "label": "500m area search",
      "created_at": "2021-09-12T23:30:51.101Z",
      "updated_at": "2021-09-12T23:30:51.101Z"
  },
  "site_location_id": null,
  "playback_used": true,
  "created_at": "2021-09-17T01:06:24.973Z",
  "updated_at": "2021-09-17T01:07:40.075Z"
}
```
#### Expected error code
Error Code | Message | Reason | Implemented | Tested | TestID
-----------|---------|--------|-------------|--------|--------
401 | User is not authenticated (logged in) | User is not authenticated | [x] | [x] | Bird-Srvy-DELETE-002
n/a | Not found | Supplied ID does not exist | [x] | [x] | Bird-Srvy-DELETE-003
400 | id must be a `number` type, but the final value was: `NaN` (cast from the value `\"foo\"`). | Supplied ID is not a valid parameter | [x] | [x] | Bird-Srvy-DELETE-004
n/a | Method not allowed | No ID is supplied | [x] | [x] | Bird-Srvy-DELETE-005
const request = require('supertest')
import * as cc from '../helpers/constants'
import { union, cloneDeep, join } from 'lodash'
const mock = require('./mock.json')
const { createMockData, postMockData } = require('../helpers/mocker')
const {
  registerMockUris,
  generatePDPApiConfigs,
} = require('../helpers/mockAdapter')

// Temporary cache so that we don't need to generate every time
const PROTOCOLS = {}
const DOCUMENTATION = {}

it(`Outgoing-Data-Filter-Test-001: Should successfully POST all collections using project ids: ${cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS}`, async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  if (!PROTOCOLS.CACHED) {
    PROTOCOLS.CACHED = await helpers.getAllProtocols()
  }
  expect(PROTOCOLS.CACHED).toBeDefined()

  if (!DOCUMENTATION.CACHED) {
    DOCUMENTATION.CACHED = await helpers.readFullDocumentation()
  }
  expect(DOCUMENTATION.CACHED).toBeDefined()
  const mockData = {}
  // create mock
  for (const project of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS) {
    mockData[project] = await createMockData(
      PROTOCOLS.CACHED,
      DOCUMENTATION.CACHED,
      cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS,
      project, // project id
    )
  }

  for (const protocol of PROTOCOLS.CACHED) {
    const prot_uuid = protocol['identifier']
    const endpoint = `${protocol['endpointPrefix']}/bulk`

    // post collections
    for (const project of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS) {
      if (!mockData[project][prot_uuid]) continue

      // survey metadata
      const surveyMetadata = mockData[project][prot_uuid].surveyMetadata
      expect(surveyMetadata).toBeDefined()

      // register apis with axios mock adapter
      registerMockUris(
        cloneDeep(mock.apiConfigs4),
        surveyMetadata.survey_details.project_id,
        surveyMetadata.survey_details.protocol_id,
        surveyMetadata.orgMintedUUID,
      )

      // publish mock data
      await postMockData(
        `Outgoing-Data-Filter-Test-001-protocol: ${protocol.name} project: ${project}`,
        global.strapiServer,
        `/api${endpoint}`,
        cloneDeep(mockData[project][prot_uuid].data),
        200,
      )
    }
  }
})

it('Outgoing-Data-Filter-Test-002: filer outgoing data of survey models(e.g. bird-survey of bird protocol) by adding project id and prot uuid in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-002 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
      prot_uuid,
    )
    // post collections
    for (const project of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS) {
      let apiMockConfigs = cloneDeep(mock.apiConfigs3)
      apiMockConfigs.push([
        cc.REQUEST_TYPE.GET,
        cc.API.USER_PROJECT,
        200,
        cloneDeep(mock.userProjects1),
      ])
      // register apis with axios mock adapter
      registerMockUris(cloneDeep(apiMockConfigs), project, prot_uuid)

      // survey models e.g. bird-survey of bird protocol
      await request(global.strapiServer) // app server is an instance of Class: http.Server
        .get(
          `/api/${
            workFlow.modelsPaths[workFlow.surveyModel]
          }?project_id=${project}&prot_uuid=${prot_uuid}`,
        )
        .set('accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('authorization', mock.validJwt1)
        .then((data) => {
          expect(data.status).toBe(200)
          if (data.body) {
            for (const survey of data.body) {
              // all the project ids or protocol uuids should be the one we passed in the url
              expect(survey.survey_metadata.survey_details.project_id).toBe(
                project,
              )
              expect(survey.survey_metadata.survey_details.protocol_id).toBe(
                prot_uuid,
              )
            }
          }
        })
    }
  }
})

it('Outgoing-Data-Filter-Test-003: filer outgoing data of observation models(e.g. bird-observation of bird protocol) by adding project id and prot uuid in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-003 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
      prot_uuid,
    )
    // usually second model in the workflow is an observation
    const observationModel = workFlow.modelNames[1]
    let apiMockConfigs = cloneDeep(mock.apiConfigs3)
    apiMockConfigs.push([
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(mock.userProjects1),
    ])
    // post collections
    for (const project of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS) {
      // register apis with axios mock adapter
      registerMockUris(cloneDeep(apiMockConfigs), project, prot_uuid)

      // observation e.g. bird-observation of bird protocol
      await request(global.strapiServer) // app server is an instance of Class: http.Server
        .get(
          `/api/${workFlow.modelsPaths[observationModel]}?project_id=${project}&prot_uuid=${prot_uuid}`,
        )
        .set('accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('authorization', mock.validJwt1)
        .then((data) => {
          expect(data.status).toBe(200)
          if (data.body) {
            // observation has field linked to survey
            const survey_field = helpers.findAssociationAttributeName(
              observationModel,
              helpers.modelToApiName(workFlow.surveyModel),
            )
            for (const observation of data.body) {
              // all the project ids or protocol uuids should be the one we passed in the url
              expect(
                observation[survey_field].survey_metadata.survey_details
                  .project_id,
              ).toBe(project)
              expect(
                observation[survey_field].survey_metadata.survey_details
                  .protocol_id,
              ).toBe(prot_uuid)
            }
          }
        })
    }
  }
})

it('Outgoing-Data-Filter-Test-004: filer outgoing data of child observations(e.g. cover-point-intercept-species-intercept of Cover + Fire - Standard) by adding project id and prot uuid in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-004 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
      prot_uuid,
    )
    if (workFlow.allModels.length == workFlow.modelNames.length) {
      console.warn(`No child observation found in ${protocol.name}`)
      continue
    }
    const childObsInfo = {}
    for (const parent of Object.keys(workFlow.raw)) {
      if (!workFlow.raw[parent]) continue
      if (childObsInfo.parent) continue
      childObsInfo['parent'] = parent
      childObsInfo['parentModelPath'] = workFlow.modelsPaths[parent]
      // first child
      const child = Object.keys(workFlow.raw[parent])[0]
      const childModel = workFlow.raw[parent][child]
      childObsInfo['childField'] = child
      childObsInfo['childModel'] = childModel
      childObsInfo['childModelPath'] = workFlow.modelsPaths[childModel]
      const parentSurveyField = helpers.findAssociationAttributeName(
        parent,
        helpers.modelToApiName(workFlow.surveyModel),
      )
      childObsInfo['parentSurveyField'] = parentSurveyField
    }
    if (!childObsInfo.parent) {
      console.warn(`No child observation found in ${protocol.name}`)
      continue
    }
    // post collections
    for (const project of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS) {
      // so that we can assert result
      let authorisedChildObs = []
      let apiMockConfigs = cloneDeep(mock.apiConfigs3)
      apiMockConfigs.push([
        cc.REQUEST_TYPE.GET,
        cc.API.USER_PROJECT,
        200,
        cloneDeep(mock.userProjects1),
      ])
      // register apis with axios mock adapter
      registerMockUris(cloneDeep(apiMockConfigs), project, prot_uuid)

      // first hit parent model to get the ids of authorised child observations
      await request(global.strapiServer) // app server is an instance of Class: http.Server
        .get(
          `/api/${childObsInfo.parentModelPath}?project_id=${project}&prot_uuid=${prot_uuid}`,
        )
        .set('accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('authorization', mock.validJwt1)
        .then((data) => {
          expect(data.status).toBe(200)
          if (data.body) {
            for (const observation of data.body) {
              // all the project ids or protocol uuids should be the one we passed in the url
              expect(
                observation[childObsInfo.parentSurveyField].survey_metadata
                  .survey_details.project_id,
              ).toBe(project)
              expect(
                observation[childObsInfo.parentSurveyField].survey_metadata
                  .survey_details.protocol_id,
              ).toBe(prot_uuid)
              // list of all the authorised child observations
              authorisedChildObs = authorisedChildObs.concat(
                observation[childObsInfo.childField],
              )
            }
          }
        })
      apiMockConfigs = cloneDeep(mock.apiConfigs3)
      apiMockConfigs.push([
        cc.REQUEST_TYPE.GET,
        cc.API.USER_PROJECT,
        200,
        cloneDeep(mock.userProjects1),
      ])
      // register apis with axios mock adapter
      registerMockUris(cloneDeep(apiMockConfigs), project, prot_uuid)

      // child observation e.g. ground-counts-vantage-point-observation of Fauna Ground Counts
      await request(global.strapiServer) // app server is an instance of Class: http.Server
        .get(
          `/api/${childObsInfo.childModelPath}?project_id=${project}&prot_uuid=${prot_uuid}`,
        )
        .set('accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('authorization', mock.validJwt1)
        .then((data) => {
          expect(data.status).toBe(200)
          if (data.body) {
            for (const [i, value] of data.body.entries()) {
              // all the authorised childObservation only
              expect(value.id).toBe(authorisedChildObs[i].id)
            }
          }
        })
    }
  }
})

it('Outgoing-Data-Filter-Test-005: pdp will reject request if invalid project id or prot uuid provided in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-005 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
      prot_uuid,
    )
    // usually second model in the workflow is an observation
    const observationModel = workFlow.modelNames[1]

    // post collections
    for (const project of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS) {
      const invalidProject = `${project}xxxxxxxxxxxxxxxx`
      // register apis with axios mock adapter
      registerMockUris(cloneDeep(mock.apiConfigs5), invalidProject, prot_uuid)

      await request(global.strapiServer) // app server is an instance of Class: http.Server
        .get(
          `/api/${workFlow.modelsPaths[observationModel]}?project_id=${invalidProject}&prot_uuid=${prot_uuid}`,
        )
        .set('accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('authorization', mock.validJwt1)
        .then((data) => {
          expect(data.status).toBe(400)
          expect(data.body.error.message).toContain(
            `Invalid: parameter 'project_id' with value '${invalidProject}' data must match format "project_id"`,
          )
        })
    }
  }
})

it('Outgoing-Data-Filter-Test-006: filer outgoing data of survey models using userProjects if no project id or prot uuid found in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const authorisedProjects = mock.userProjects1.projects.map((p) =>
    p.id.toString(),
  )
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-006 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
      prot_uuid,
    )
    let apiMockConfigs = cloneDeep(mock.apiConfigs1)
    const userProjectGet = [
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(mock.userProjects1),
    ]
    // GET /user-projects (2x, once for PME, again for pdp-data-filter)
    apiMockConfigs.push(userProjectGet)
    apiMockConfigs.push(userProjectGet)
    // register apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs))

    // survey models e.g. bird-survey of bird protocol
    await request(global.strapiServer) // app server is an instance of Class: http.Server
      .get(`/api/${workFlow.modelsPaths[workFlow.surveyModel]}`)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .then((data) => {
        expect(data.status).toBe(200)
        if (data.body) {
          for (const survey of data.body) {
            // all the project_id should be any of the authorised project ids
            expect(authorisedProjects).toContain(
              survey.survey_metadata.survey_details.project_id,
            )
          }
        }
      })
  }
})

it('Outgoing-Data-Filter-Test-007: filer outgoing data of observation models using userProjects if no project id or prot uuid found in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const authorisedProjects = mock.userProjects1.projects.map((p) =>
    p.id.toString(),
  )
  let apiMockConfigs = cloneDeep(mock.apiConfigs1)
  const userProjectGet = [
    cc.REQUEST_TYPE.GET,
    cc.API.USER_PROJECT,
    200,
    cloneDeep(mock.userProjects1),
  ]
  // GET /user-projects (2x, once for PME, again for pdp-data-filter)
  apiMockConfigs.push(userProjectGet)
  apiMockConfigs.push(userProjectGet)
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-007 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
      prot_uuid,
    )
    // usually second model in the workflow is an observation
    const observationModel = workFlow.modelNames[1]

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs))

    // observation e.g. bird-observation of bird protocol
    await request(global.strapiServer) // app server is an instance of Class: http.Server
      .get(`/api/${workFlow.modelsPaths[observationModel]}`)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .then((data) => {
        expect(data.status).toBe(200)
        if (data.body) {
          // observation has field linked to survey
          const survey_field = helpers.findAssociationAttributeName(
            observationModel,
            helpers.modelToApiName(workFlow.surveyModel),
          )
          for (const observation of data.body) {
            // all the project_id should be any of the authorised project ids
            expect(authorisedProjects).toContain(
              observation[survey_field].survey_metadata.survey_details
                .project_id,
            )
          }
        }
      })
  }
})

it('Outgoing-Data-Filter-Test-008: filer outgoing data of child observations using userProjects if no project id or prot uuid found in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const authorisedProjects = mock.userProjects1.projects.map((p) =>
    p.id.toString(),
  )
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-008 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
      prot_uuid,
    )
    if (workFlow.allModels.length == workFlow.modelNames.length) {
      console.warn(`No child observation found in ${protocol.name}`)
      continue
    }
    const childObsInfo = {}
    for (const parent of Object.keys(workFlow.raw)) {
      if (!workFlow.raw[parent]) continue
      if (childObsInfo.parent) continue
      childObsInfo['parent'] = parent
      childObsInfo['parentModelPath'] = workFlow.modelsPaths[parent]
      // first child
      const child = Object.keys(workFlow.raw[parent])[0]
      const childModel = workFlow.raw[parent][child]
      childObsInfo['childField'] = child
      childObsInfo['childModel'] = childModel
      childObsInfo['childModelPath'] = workFlow.modelsPaths[childModel]
      const parentSurveyField = helpers.findAssociationAttributeName(
        parent,
        helpers.modelToApiName(workFlow.surveyModel),
      )
      childObsInfo['parentSurveyField'] = parentSurveyField
    }
    if (!childObsInfo.parent) {
      console.warn(`No child observation found in ${protocol.name}`)
      continue
    }
    // so that we can assert result
    let authorisedChildObs = []
    let apiMockConfigs = cloneDeep(mock.apiConfigs1)
    const userProjectGet = [
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(mock.userProjects1),
    ]
    // GET /user-projects (2x, once for PME, again for pdp-data-filter)
    apiMockConfigs.push(userProjectGet)
    apiMockConfigs.push(userProjectGet)
    // register apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs))

    // first hit parent model to get the ids of authorised child observations
    await request(global.strapiServer) // app server is an instance of Class: http.Server
      .get(`/api/${childObsInfo.parentModelPath}`)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .then((data) => {
        expect(data.status).toBe(200)
        if (data.body) {
          for (const observation of data.body) {
            // all the project_id should be any of the authorised project ids
            expect(authorisedProjects).toContain(
              observation[childObsInfo.parentSurveyField].survey_metadata
                .survey_details.project_id,
            )
            // list of all the authorised child observations
            authorisedChildObs = authorisedChildObs.concat(
              observation[childObsInfo.childField],
            )
          }
        }
      })

    // register apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs))

    // child observation e.g. ground-counts-vantage-point-observation of Fauna Ground Counts
    await request(global.strapiServer) // app server is an instance of Class: http.Server
      .get(`/api/${childObsInfo.childModelPath}`)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .then((data) => {
        expect(data.status).toBe(200)
        if (data.body) {
          for (const [i, value] of data.body.entries()) {
            // all the authorised childObservation only
            expect(value.id).toBe(authorisedChildObs[i].id)
          }
        }
      })
  }
})

it('Outgoing-Data-Filter-Test-009: reject request to access unauthorised models using userProjects if no project id or prot uuid found in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-009 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
      prot_uuid,
    )
    let apiMockConfigs = cloneDeep(mock.apiConfigs1)
    const userProjectGet = [
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(mock.userProjects2),
    ]
    // GET /user-projects (2x, once for PME, again for pdp-data-filter)
    apiMockConfigs.push(userProjectGet)
    apiMockConfigs.push(userProjectGet)
    // register apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs))

    // survey models e.g. bird-survey of bird protocol
    await request(global.strapiServer) // app server is an instance of Class: http.Server
      .get(`/api/${workFlow.modelsPaths[workFlow.surveyModel]}`)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .then((data) => {
        expect(data.body.error.status).toBe(401)
        expect(data.body.error.message).toBe(
          `No user projects that authorize accessing '${workFlow.surveyModel}'`,
        )
      })
  }
})

it('Outgoing-Data-Filter-Test-010: filer outgoing data of observation models inside submodule(e.g. fire-char-observation of cover + fire) by adding project id and prot uuid in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-010 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
      prot_uuid,
    )
    if (!workFlow.modelExtension) {
      console.warn(`submodule not found in protocol ${protocol['name']}`)
      continue
    }
    // usually second model in the workflow is an observation
    const subModuleObservationModel = workFlow.modelExtension.modelNames[1]

    // post collections
    for (const project of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS) {
      let apiMockConfigs = cloneDeep(mock.apiConfigs3)
      apiMockConfigs.push([
        cc.REQUEST_TYPE.GET,
        cc.API.USER_PROJECT,
        200,
        cloneDeep(mock.userProjects1),
      ])
      // register apis with axios mock adapter
      registerMockUris(cloneDeep(apiMockConfigs), project, prot_uuid)

      // observation e.g. bird-observation of bird protocol
      await request(global.strapiServer) // app server is an instance of Class: http.Server
        .get(
          `/api/${workFlow.modelExtension.modelsPaths[subModuleObservationModel]}?project_id=${project}&prot_uuid=${prot_uuid}`,
        )
        .set('accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('authorization', mock.validJwt1)
        .then((data) => {
          expect(data.status).toBe(200)
          if (data.body) {
            // observation has field linked to survey
            const survey_field = helpers.findAssociationAttributeName(
              subModuleObservationModel,
              helpers.modelToApiName(workFlow.modelExtension.surveyModel),
            )
            for (const observation of data.body) {
              // all the project ids or protocol uuids should be the one we passed in the url
              expect(
                observation[survey_field].survey_metadata.survey_details
                  .project_id,
              ).toBe(project)
              expect(
                observation[survey_field].survey_metadata.survey_details
                  .protocol_id,
              ).toBe(prot_uuid)
              // submodule id should not be null
              expect(
                observation[survey_field].survey_metadata.survey_details
                  .submodule_protocol_id,
              ).toBe(cc.SUB_MODULES_RELATION[prot_uuid])
            }
          }
        })
    }
  }
})

it('Outgoing-Data-Filter-Test-011: filer outgoing data of observation models inside submodule(e.g. fire-char-observation of cover + fire) using userProjects if no project id or prot uuid found in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-011 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
      prot_uuid,
    )
    if (!workFlow.modelExtension) {
      console.warn(`submodule not found in protocol ${protocol['name']}`)
      continue
    }
    // usually second model in the workflow is an observation
    const subModuleObservationModel = workFlow.modelExtension.modelNames[1]
    let apiMockConfigs = cloneDeep(mock.apiConfigs1)
    const userProjectGet = [
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(mock.userProjects1),
    ]
    // GET /user-projects (2x, once for PME, again for pdp-data-filter)
    apiMockConfigs.push(userProjectGet)
    apiMockConfigs.push(userProjectGet)
    // register apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs))

    // observation e.g. bird-observation of bird protocol
    await request(global.strapiServer) // app server is an instance of Class: http.Server
      .get(
        `/api/${workFlow.modelExtension.modelsPaths[subModuleObservationModel]}`,
      )
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .then((data) => {
        expect(data.status).toBe(200)
        if (data.body) {
          // observation has field linked to survey
          const survey_field = helpers.findAssociationAttributeName(
            subModuleObservationModel,
            helpers.modelToApiName(workFlow.modelExtension.surveyModel),
          )
          for (const observation of data.body) {
            expect(cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS).toContain(
              observation[survey_field].survey_metadata.survey_details
                .project_id,
            )
            expect(
              observation[survey_field].survey_metadata.survey_details
                .protocol_id,
            ).toBe(prot_uuid)
            // submodule id should not be null
            expect(
              observation[survey_field].survey_metadata.survey_details
                .submodule_protocol_id,
            ).toBe(cc.SUB_MODULES_RELATION[prot_uuid])
          }
        }
      })
  }
})

it('Outgoing-Data-Filter-Test-012: filer outgoing data of child observations inside submodule (e.g. fire-char-observation of cover + fire) by adding project id and prot uuid in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-012 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    let workFlow = await helpers.findModelAndChildObsFromProtocolUUID(prot_uuid)
    if (workFlow.allModels.length == workFlow.modelNames.length) {
      console.warn(`No child observation found in ${protocol.name}`)
      continue
    }
    if (!workFlow.modelExtension) {
      console.warn(`submodule not found in protocol ${protocol['name']}`)
      continue
    }
    workFlow = workFlow.modelExtension

    const childObsInfo = {}
    for (const parent of Object.keys(workFlow.raw)) {
      if (!workFlow.raw[parent]) continue
      if (childObsInfo.parent) continue
      childObsInfo['parent'] = parent
      childObsInfo['parentModelPath'] = workFlow.modelsPaths[parent]
      // first child
      const child = Object.keys(workFlow.raw[parent])[0]
      const childModel = workFlow.raw[parent][child]
      childObsInfo['childField'] = child
      childObsInfo['childModel'] = childModel
      childObsInfo['childModelPath'] = workFlow.modelsPaths[childModel]
      const parentSurveyField = helpers.findAssociationAttributeName(
        parent,
        helpers.modelToApiName(workFlow.surveyModel),
      )
      childObsInfo['parentSurveyField'] = parentSurveyField
    }
    if (!childObsInfo.parent) {
      console.warn(`No child observation found in ${protocol.name}`)
      continue
    }
    // post collections
    for (const project of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS) {
      // so that we can assert result
      let authorisedChildObs = []
      let apiMockConfigs = cloneDeep(mock.apiConfigs3)
      apiMockConfigs.push([
        cc.REQUEST_TYPE.GET,
        cc.API.USER_PROJECT,
        200,
        cloneDeep(mock.userProjects1),
      ])
      // register apis with axios mock adapter
      registerMockUris(
        cloneDeep(apiMockConfigs),
        project,
        prot_uuid,
      )

      // first hit parent model to get the ids of authorised child observations
      await request(global.strapiServer) // app server is an instance of Class: http.Server
        .get(
          `/api/${childObsInfo.parentModelPath}?project_id=${project}&prot_uuid=${prot_uuid}`,
        )
        .set('accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('authorization', mock.validJwt1)
        .then((data) => {
          expect(data.status).toBe(200)
          if (data.body) {
            for (const observation of data.body) {
              // all the project ids or protocol uuids should be the one we passed in the url
              expect(
                observation[childObsInfo.parentSurveyField].survey_metadata
                  .survey_details.project_id,
              ).toBe(project)
              expect(
                observation[childObsInfo.parentSurveyField].survey_metadata
                  .survey_details.protocol_id,
              ).toBe(prot_uuid)
              // list of all the authorised child observations
              authorisedChildObs = authorisedChildObs.concat(
                observation[childObsInfo.childField],
              )
            }
          }
        })
      // register apis with axios mock adapter
      registerMockUris(
        cloneDeep(apiMockConfigs),
        project,
        prot_uuid,
      )

      // child observation e.g. ground-counts-vantage-point-observation of Fauna Ground Counts
      await request(global.strapiServer) // app server is an instance of Class: http.Server
        .get(
          `/api/${childObsInfo.childModelPath}?project_id=${project}&prot_uuid=${prot_uuid}`,
        )
        .set('accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('authorization', mock.validJwt1)
        .then((data) => {
          expect(data.status).toBe(200)
          if (data.body) {
            for (const [i, value] of data.body.entries()) {
              // all the authorised childObservation only
              expect(value.id).toBe(authorisedChildObs[i].id)
            }
          }
        })
    }
  }
})

it('Outgoing-Data-Filter-Test-013: filer outgoing data of child observations using userProjects if no project id or prot uuid found in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const authorisedProjects = mock.userProjects1.projects.map((p) => p.id.toString())
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-013 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    let workFlow = await helpers.findModelAndChildObsFromProtocolUUID(prot_uuid)
    if (workFlow.allModels.length == workFlow.modelNames.length) {
      console.warn(`No child observation found in ${protocol.name}`)
      continue
    }
    if (!workFlow.modelExtension) {
      console.warn(`submodule not found in protocol ${protocol['name']}`)
      continue
    }
    workFlow = workFlow.modelExtension
    const childObsInfo = {}
    for (const parent of Object.keys(workFlow.raw)) {
      if (!workFlow.raw[parent]) continue
      if (childObsInfo.parent) continue
      childObsInfo['parent'] = parent
      childObsInfo['parentModelPath'] = workFlow.modelsPaths[parent]
      // first child
      const child = Object.keys(workFlow.raw[parent])[0]
      const childModel = workFlow.raw[parent][child]
      childObsInfo['childField'] = child
      childObsInfo['childModel'] = childModel
      childObsInfo['childModelPath'] = workFlow.modelsPaths[childModel]
      const parentSurveyField = helpers.findAssociationAttributeName(
        parent,
        helpers.modelToApiName(workFlow.surveyModel),
      )
      childObsInfo['parentSurveyField'] = parentSurveyField
    }
    if (!childObsInfo.parent) {
      console.warn(`No child observation found in ${protocol.name}`)
      continue
    }
    // so that we can assert result
    let authorisedChildObs = []
    let apiMockConfigs = cloneDeep(mock.apiConfigs1)
    const userProjectGet = [
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(mock.userProjects1),
    ]
    // GET /user-projects (2x, once for PME, again for pdp-data-filter)
    apiMockConfigs.push(userProjectGet)
    apiMockConfigs.push(userProjectGet)
    // register apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs))

    // first hit parent model to get the ids of authorised child observations
    await request(global.strapiServer) // app server is an instance of Class: http.Server
      .get(`/api/${childObsInfo.parentModelPath}`)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .then((data) => {
        expect(data.status).toBe(200)
        if (data.body) {
          for (const observation of data.body) {
            // all the project_id should be any of the authorised project ids
            expect(authorisedProjects).toContain(
              observation[childObsInfo.parentSurveyField].survey_metadata
                .survey_details.project_id,
            )
            // list of all the authorised child observations
            authorisedChildObs = authorisedChildObs.concat(
              observation[childObsInfo.childField],
            )
          }
        }
      })
    // register apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs))

    // child observation e.g. ground-counts-vantage-point-observation of Fauna Ground Counts
    await request(global.strapiServer) // app server is an instance of Class: http.Server
      .get(`/api/${childObsInfo.childModelPath}`)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .then((data) => {
        expect(data.status).toBe(200)
        if (data.body) {
          for (const [i, value] of data.body.entries()) {
            // all the authorised childObservation only
            expect(value.id).toBe(authorisedChildObs[i].id)
          }
        }
      })
  }
})

it('Outgoing-Data-Filter-Test-014: filer outgoing data of survey models(e.g. bird-survey of bird protocol) by adding multiple project ids and prot uuid in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-014 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
      prot_uuid,
    )
    let apiMockConfigs = cloneDeep(mock.apiConfigs1)
    const modelPrefix = workFlow.modelsPaths[workFlow.surveyModel]
    let endpoint = `/api/${modelPrefix}?populate=deep&prot_uuid=${prot_uuid}`

    // all project id params and PDP mock configs
    for (const [i, p] of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS.entries()) {
      endpoint += `&project_ids[${i}]=${p}`
    }
    // all PDP combinations
    apiMockConfigs = apiMockConfigs.concat(
      generatePDPApiConfigs(
        cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS,
        [prot_uuid],
        cc.PDP_TYPE.READ,
      ),
    )
    // GET /user-projects (middleware does this when > 1 project)
    apiMockConfigs.push([
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(mock.userProjects1),
    ])
    // register apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs))

    // survey models e.g. bird-survey of bird protocol
    await request(global.strapiServer) // app server is an instance of Class: http.Server
      .get(endpoint)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .then((data) => {
        expect(data.status).toBe(200)
        if (data.body) {
          for (const survey of data.body) {
            // all the project ids or protocol uuids should be the one we passed in the url
            expect(cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS).toContain(
              survey.survey_metadata.survey_details.project_id,
            )
            expect(survey.survey_metadata.survey_details.protocol_id).toBe(
              prot_uuid,
            )
          }
        }
      })
  }
})

it('Outgoing-Data-Filter-Test-015: filer outgoing data of observation models(e.g. bird-observation of bird protocol) by adding multiple project ids and prot uuid in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-015 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
      prot_uuid,
    )
    // usually second model in the workflow is an observation
    const observationModel = workFlow.modelNames[1]

    let apiMockConfigs = cloneDeep(mock.apiConfigs1)
    const modelPrefix = workFlow.modelsPaths[observationModel]
    let endpoint = `/api/${modelPrefix}?populate=deep&prot_uuid=${prot_uuid}`

    // all project id params and PDPs
    for (const [i, p] of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS.entries()) {
      endpoint += `&project_ids[${i}]=${p}`
    }
    // all PDP combinations
    apiMockConfigs = apiMockConfigs.concat(
      generatePDPApiConfigs(
        cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS,
        [prot_uuid],
        cc.PDP_TYPE.READ,
      ),
    )
    // GET /user-projects (middleware does this when > 1 project)
    apiMockConfigs.push([
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(mock.userProjects1),
    ])
    // register apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs))

    // observation e.g. bird-observation of bird protocol
    await request(global.strapiServer) // app server is an instance of Class: http.Server
      .get(endpoint)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .then((data) => {
        expect(data.status).toBe(200)
        if (data.body) {
          // observation has field linked to survey
          const survey_field = helpers.findAssociationAttributeName(
            observationModel,
            helpers.modelToApiName(workFlow.surveyModel),
          )
          for (const observation of data.body) {
            // all the project ids or protocol uuids should be the one we passed in the url
            expect(cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS).toContain(
              observation[survey_field].survey_metadata.survey_details
                .project_id,
            )
            expect(
              observation[survey_field].survey_metadata.survey_details
                .protocol_id,
            ).toBe(prot_uuid)
          }
        }
      })
  }
})

it('Outgoing-Data-Filter-Test-016: filer outgoing data of plot visit by adding project id and prot uuid in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  for (const protocol of PROTOCOLS.CACHED) {
    // console.log('Outgoing-Data-Filter-Test-016 protocol:', JSON.stringify(protocol, null, 2))
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue

    let apiMockConfigs = cloneDeep(mock.apiConfigs3)
    apiMockConfigs.push([
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(mock.userProjects1),
    ])

    for (const project of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS) {
      // register apis with axios mock adapter
      registerMockUris(cloneDeep(apiMockConfigs), project, prot_uuid)

      // survey models e.g. bird-survey of bird protocol
      await request(global.strapiServer) // app server is an instance of Class: http.Server
        .get(
          `/api/plot-visits?populate=deep&project_id=${project}&prot_uuid=${prot_uuid}`,
        )
        .set('accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('authorization', mock.validJwt1)
        .then(async (data) => {
          expect(data.status).toBe(200)
          if (data.body) {
            const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
              prot_uuid,
            )
            const plotVisitField = helpers.findAssociationAttributeName(
              workFlow['surveyModel'],
              helpers.modelToApiName('plot-visit'),
            )
            if (!plotVisitField) {
              console.warn(`protocol: ${prot_uuid} is not plot based`)
              return
            }
            const plotLayoutField = helpers.findAssociationAttributeName(
              'plot-visit',
              helpers.modelToApiName('plot-layout'),
            )
            const plotSelectionField = helpers.findAssociationAttributeName(
              'plot-layout',
              helpers.modelToApiName('plot-selection'),
            )
            // all the plot names we got
            let plotVisitNames = data.body.map(
              (a) => a[plotLayoutField][plotSelectionField].plot_label,
            )

            let projectInfo = mock.userProjects1.projects.find(
              (p) => p.id.toString() === project,
            )
            let plotSelections = []
            for (const p of projectInfo.plot_selections) {
              if (plotSelections.includes(p.name)) continue
              plotSelections.push(p.name)
            }
            // query all plot names associated to the protocol
            const surveyData = await helpers.deepPopulateQuery(
              workFlow['surveyModel'],
              {
                survey_metadata: {
                  survey_details: {
                    protocol_id: prot_uuid,
                  },
                },
                [plotVisitField]: {
                  [plotLayoutField]: {
                    [plotSelectionField]: {
                      plot_label: { $in: plotSelections },
                    },
                  },
                },
              },
              {
                survey_metadata: {
                  populate: {
                    survey_details: true,
                  },
                },
                [plotVisitField]: {
                  populate: {
                    [plotLayoutField]: {
                      populate: {
                        [plotSelectionField]: true,
                      },
                    },
                  },
                },
              },
              true,
            )
            let surveyPlotVisitName = surveyData.map(
              (a) =>
                a[plotVisitField][plotLayoutField][plotSelectionField]
                  .plot_label,
            )
            plotVisitNames = union(plotVisitNames)
            surveyPlotVisitName = union(surveyPlotVisitName)

            // lists will be same
            expect(plotVisitNames.sort()).toEqual(surveyPlotVisitName.sort())
          }
        })
    }
  }
})

it('Outgoing-Data-Filter-Test-017: filer outgoing data of plot visit by adding multiple project ids and prot uuids in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  let endpoint = `/api/plot-visits?populate=deep`
  let apiMockConfigs = cloneDeep(mock.apiConfigs1)

  let allAuthPlotSelections = []
  for (const [i, proj] of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS.entries()) {
    // all project id params
    endpoint += `&project_ids[${i}]=${proj}`

    // list of all plot selections
    let projectInfo = mock.userProjects1.projects.find(
      (p) => p.id.toString() === proj,
    )
    for (const p of projectInfo.plot_selections) {
      if (allAuthPlotSelections.includes(p.name)) continue
      allAuthPlotSelections.push(p.name)
    }
  }
  for (const [i, p] of cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.entries()) {
    // all protocol id params
    endpoint += `&prot_uuids[${i}]=${p}`
  }
  // all PDP combinations
  apiMockConfigs = apiMockConfigs.concat(
    generatePDPApiConfigs(
      cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS,
      cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS,
      cc.PDP_TYPE.READ,
    ),
  )
  apiMockConfigs.push([
    cc.REQUEST_TYPE.GET,
    cc.API.USER_PROJECT,
    200,
    cloneDeep(mock.userProjects1),
  ])
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(apiMockConfigs))

  // survey models e.g. bird-survey of bird protocol
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get(endpoint)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .then(async (data) => {
      expect(data.status).toBe(200)
      if (data.body) {
        if (data.body.length == 0) {
          console.warn(
            `No plot visit is associated with projects: ${cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS} and protocols: ${cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS}`,
          )
          return
        }
        const plotLayoutField = helpers.findAssociationAttributeName(
          'plot-visit',
          helpers.modelToApiName('plot-layout'),
        )
        const plotSelectionField = helpers.findAssociationAttributeName(
          'plot-layout',
          helpers.modelToApiName('plot-selection'),
        )
        // all the plot names we got
        let plotVisitNames = data.body.map(
          (a) => a[plotLayoutField][plotSelectionField].plot_label,
        )
        plotVisitNames = union(plotVisitNames)
        plotVisitNames = plotVisitNames.sort()
        console.log('plotVisitNames ' + JSON.stringify(plotVisitNames))

        // query all plot names associated to the protocols
        let allProtPlotVisitNames = []
        for (const prot_uuid of cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS) {
          const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
            prot_uuid,
          )
          const plotVisitField = helpers.findAssociationAttributeName(
            workFlow['surveyModel'],
            helpers.modelToApiName('plot-visit'),
          )
          if (!plotVisitField) continue

          // query survey data
          const surveyData = await helpers.deepPopulateQuery(
            workFlow['surveyModel'],
            {
              survey_metadata: {
                survey_details: {
                  protocol_id: prot_uuid,
                },
              },
              [plotVisitField]: {
                [plotLayoutField]: {
                  [plotSelectionField]: {
                    plot_label: { $in: allAuthPlotSelections },
                  },
                },
              },
            },
            {
              survey_metadata: {
                populate: {
                  survey_details: true,
                },
              },
              [plotVisitField]: {
                populate: {
                  [plotLayoutField]: {
                    populate: {
                      [plotSelectionField]: true,
                    },
                  },
                },
              },
            },
            true,
          )
          const surveyPlotVisitName = surveyData.map(
            (a) =>
              a[plotVisitField][plotLayoutField][plotSelectionField].plot_label,
          )
          allProtPlotVisitNames =
            allProtPlotVisitNames.concat(surveyPlotVisitName)
        }
        allProtPlotVisitNames = union(allProtPlotVisitNames)
        allProtPlotVisitNames = allProtPlotVisitNames.sort()
        // lists should be same
        expect(plotVisitNames).toEqual(allProtPlotVisitNames)
      }
    })
})

it('Outgoing-Data-Filter-Test-018: filer outgoing data of plot visit using userProjects if no project id or prot uuid found in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  let apiMockConfigs = cloneDeep(mock.apiConfigs1)
  const userProjectGet = [
    cc.REQUEST_TYPE.GET,
    cc.API.USER_PROJECT,
    200,
    cloneDeep(mock.userProjects1),
  ]
  // GET /user-projects (2x, once for PME, again for pdp-data-filter)
  apiMockConfigs.push(userProjectGet)
  apiMockConfigs.push(userProjectGet)
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(apiMockConfigs))

  let allAuthPlotSelections = []
  let allAuthProtUuids = []
  for (const project of mock.userProjects1.projects) {
    allAuthPlotSelections = allAuthPlotSelections.concat(
      project.plot_selections.map((a) => a.name.toString()),
    )
    allAuthProtUuids = allAuthProtUuids.concat(
      project.protocols.map((a) => a.identifier.toString()),
    )
  }
  allAuthPlotSelections = union(allAuthPlotSelections)
  allAuthProtUuids = union(allAuthProtUuids)

  // survey models e.g. bird-survey of bird protocol
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/plot-visits?populate=deep')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .then(async (data) => {
      expect(data.status).toBe(200)
      if (data.body) {
        if (data.body.length == 0) {
          console.warn(
            `No plot visit is associated with user: ${mock.validJwt1}`,
          )
          return
        }

        const plotLayoutField = helpers.findAssociationAttributeName(
          'plot-visit',
          helpers.modelToApiName('plot-layout'),
        )
        const plotSelectionField = helpers.findAssociationAttributeName(
          'plot-layout',
          helpers.modelToApiName('plot-selection'),
        )
        // all authorised plot names
        let plotVisitNames = data.body.map(
          (a) => a[plotLayoutField][plotSelectionField].plot_label,
        )
        plotVisitNames = union(plotVisitNames)
        plotVisitNames = plotVisitNames.sort()
        console.log('plotVisitNames ' + JSON.stringify(plotVisitNames))
        // console.log('allAuthPlotSelections ' + JSON.stringify(allAuthPlotSelections))

        // query all authorised plot names
        let allProtPlotVisitNames = []
        for (const prot_uuid of allAuthProtUuids) {
          const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
            prot_uuid,
          )
          if (!workFlow['surveyModel']) continue
          if (!workFlow['allModels']) continue

          const plotVisitField = helpers.findAssociationAttributeName(
            workFlow['surveyModel'],
            helpers.modelToApiName('plot-visit'),
          )
          if (!plotVisitField) continue
          console.log('workFlow ' + JSON.stringify(workFlow))
          // populate survey data
          const surveyData = await helpers.deepPopulateQuery(
            workFlow['surveyModel'],
            {
              survey_metadata: {
                survey_details: {
                  protocol_id: prot_uuid,
                },
              },
              [plotVisitField]: {
                [plotLayoutField]: {
                  [plotSelectionField]: {
                    plot_label: { $in: allAuthPlotSelections },
                  },
                },
              },
            },
            {
              survey_metadata: {
                populate: {
                  survey_details: true,
                },
              },
              [plotVisitField]: {
                populate: {
                  [plotLayoutField]: {
                    populate: {
                      [plotSelectionField]: true,
                    },
                  },
                },
              },
            },
            true,
          )
          const surveyPlotVisitName = surveyData.map(
            (a) =>
              a[plotVisitField][plotLayoutField][plotSelectionField].plot_label,
          )
          allProtPlotVisitNames =
            allProtPlotVisitNames.concat(surveyPlotVisitName)
        }

        allProtPlotVisitNames = union(allProtPlotVisitNames)
        allProtPlotVisitNames = allProtPlotVisitNames.sort()
        // lists should be same
        expect(plotVisitNames).toEqual(allProtPlotVisitNames)
      }
    })
})

it('Outgoing-Data-Filter-Test-019: filer outgoing data of plot layout by adding project id and prot uuid in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  for (const protocol of PROTOCOLS.CACHED) {
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    let apiMockConfigs = cloneDeep(mock.apiConfigs3)
    apiMockConfigs.push([
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(mock.userProjects1),
    ])

    for (const project of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS) {
      // register apis with axios mock adapter
      registerMockUris(cloneDeep(apiMockConfigs), project, prot_uuid)

      // survey models e.g. bird-survey of bird protocol
      await request(global.strapiServer) // app server is an instance of Class: http.Server
        .get(
          `/api/plot-layouts?populate=deep&project_id=${project}&prot_uuid=${prot_uuid}`,
        )
        .set('accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('authorization', mock.validJwt1)
        .then(async (data) => {
          expect(data.status).toBe(200)
          if (data.body) {
            const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
              prot_uuid,
            )
            const plotVisitField = helpers.findAssociationAttributeName(
              workFlow['surveyModel'],
              helpers.modelToApiName('plot-visit'),
            )
            if (!plotVisitField) {
              console.warn(`protocol: ${prot_uuid} is not plot based`)
              return
            }
            const plotLayoutField = helpers.findAssociationAttributeName(
              'plot-visit',
              helpers.modelToApiName('plot-layout'),
            )
            const plotSelectionField = helpers.findAssociationAttributeName(
              'plot-layout',
              helpers.modelToApiName('plot-selection'),
            )
            let plotLayoutsNames = data.body.map(
              (a) => a[plotSelectionField].plot_label,
            )
            plotLayoutsNames = union(plotLayoutsNames)
            plotLayoutsNames = plotLayoutsNames.sort()

            let projectInfo = mock.userProjects1.projects.find(
              (p) => p.id.toString() === project,
            )
            let plotSelections = []
            for (const p of projectInfo.plot_selections) {
              if (plotSelections.includes(p.name)) continue
              plotSelections.push(p.name)
            }
            // query all plot names associated to the protocol
            const surveyData = await helpers.deepPopulateQuery(
              workFlow['surveyModel'],
              {
                survey_metadata: {
                  survey_details: {
                    protocol_id: prot_uuid,
                  },
                },
                [plotVisitField]: {
                  [plotLayoutField]: {
                    [plotSelectionField]: {
                      plot_label: { $in: plotSelections },
                    },
                  },
                },
              },
              {
                survey_metadata: {
                  populate: {
                    survey_details: true,
                  },
                },
                [plotVisitField]: {
                  populate: {
                    [plotLayoutField]: {
                      populate: {
                        [plotSelectionField]: true,
                      },
                    },
                  },
                },
              },
              true,
            )
            let surveyPlotLayoutName = surveyData.map(
              (a) =>
                a[plotVisitField][plotLayoutField][plotSelectionField]
                  .plot_label,
            )
            surveyPlotLayoutName = union(surveyPlotLayoutName)
            surveyPlotLayoutName = surveyPlotLayoutName.sort()

            // lists will be same
            expect(plotLayoutsNames).toEqual(surveyPlotLayoutName)
          }
        })
    }
  }
})

it('Outgoing-Data-Filter-Test-020: filer outgoing data of plot layout by adding multiple project ids and prot uuid in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  let endpoint = `/api/plot-layouts?populate=deep`
  let apiMockConfigs = cloneDeep(mock.apiConfigs1)

  let allAuthPlotSelections = []
  for (const [i, proj] of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS.entries()) {
    // all project id params
    endpoint += `&project_ids[${i}]=${proj}`

    // list of all plot selections
    let projectInfo = mock.userProjects1.projects.find(
      (p) => p.id.toString() === proj,
    )
    for (const p of projectInfo.plot_selections) {
      if (allAuthPlotSelections.includes(p.name)) continue
      allAuthPlotSelections.push(p.name)
    }
  }
  for (const [i, p] of cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.entries()) {
    // all protocol id params
    endpoint += `&prot_uuids[${i}]=${p}`
  }
  // all PDP combinations
  apiMockConfigs = apiMockConfigs.concat(
    generatePDPApiConfigs(
      cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS,
      cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS,
      cc.PDP_TYPE.READ,
    ),
  )
  apiMockConfigs.push([
    cc.REQUEST_TYPE.GET,
    cc.API.USER_PROJECT,
    200,
    cloneDeep(mock.userProjects1),
  ])
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(apiMockConfigs))

  // survey models e.g. bird-survey of bird protocol
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get(endpoint)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .then(async (data) => {
      expect(data.status).toBe(200)
      if (data.body) {
        if (data.body.length == 0) {
          console.warn(
            `No plot layout is associated with projects: ${cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS} and protocols: ${cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS}`,
          )
          return
        }
        const plotLayoutField = helpers.findAssociationAttributeName(
          'plot-visit',
          helpers.modelToApiName('plot-layout'),
        )
        const plotSelectionField = helpers.findAssociationAttributeName(
          'plot-layout',
          helpers.modelToApiName('plot-selection'),
        )
        // all the plot names we got
        let plotLayoutNames = data.body.map(
          (a) => a[plotSelectionField].plot_label,
        )
        plotLayoutNames = union(plotLayoutNames)
        plotLayoutNames = plotLayoutNames.sort()
        console.log('plotLayoutNames ' + JSON.stringify(plotLayoutNames))

        // query all plot names associated to the protocols
        let allProtPlotLayoutNames = []
        for (const prot_uuid of cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS) {
          const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
            prot_uuid,
          )
          const plotVisitField = helpers.findAssociationAttributeName(
            workFlow['surveyModel'],
            helpers.modelToApiName('plot-visit'),
          )
          if (!plotVisitField) continue

          // query survey data
          const surveyData = await helpers.deepPopulateQuery(
            workFlow['surveyModel'],
            {
              survey_metadata: {
                survey_details: {
                  protocol_id: prot_uuid,
                },
              },
              [plotVisitField]: {
                [plotLayoutField]: {
                  [plotSelectionField]: {
                    plot_label: { $in: allAuthPlotSelections },
                  },
                },
              },
            },
            {
              survey_metadata: {
                populate: {
                  survey_details: true,
                },
              },
              [plotVisitField]: {
                populate: {
                  [plotLayoutField]: {
                    populate: {
                      [plotSelectionField]: true,
                    },
                  },
                },
              },
            },
            true,
          )
          const surveyPlotVisitName = surveyData.map(
            (a) =>
              a[plotVisitField][plotLayoutField][plotSelectionField].plot_label,
          )
          allProtPlotLayoutNames =
            allProtPlotLayoutNames.concat(surveyPlotVisitName)
        }
        allProtPlotLayoutNames = union(allProtPlotLayoutNames)
        allProtPlotLayoutNames = allProtPlotLayoutNames.sort()

        // lists should be same
        expect(plotLayoutNames).toEqual(allProtPlotLayoutNames)
      }
    })
})

it('Outgoing-Data-Filter-Test-021: filer outgoing data of plot layout using userProjects if no project id or prot uuid found in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  let apiMockConfigs = cloneDeep(mock.apiConfigs1)
  const userProjectGet = [
    cc.REQUEST_TYPE.GET,
    cc.API.USER_PROJECT,
    200,
    cloneDeep(mock.userProjects1),
  ]
  // GET /user-projects (2x, once for PME, again for pdp-data-filter)
  apiMockConfigs.push(userProjectGet)
  apiMockConfigs.push(userProjectGet)
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(apiMockConfigs))

  let allAuthPlotSelections = []
  let allAuthProtUuids = []
  for (const project of mock.userProjects1.projects) {
    allAuthPlotSelections = allAuthPlotSelections.concat(
      project.plot_selections.map((a) => a.name.toString()),
    )
    allAuthProtUuids = allAuthProtUuids.concat(
      project.protocols.map((a) => a.identifier.toString()),
    )
  }
  allAuthPlotSelections = union(allAuthPlotSelections)
  allAuthProtUuids = union(allAuthProtUuids)

  // survey models e.g. bird-survey of bird protocol
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/plot-layouts?populate=deep')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .then(async (data) => {
      expect(data.status).toBe(200)
      if (data.body) {
        if (data.body.length == 0) {
          console.warn(
            `No plot visit is associated with user: ${mock.validJwt1}`,
          )
          return
        }

        const plotLayoutField = helpers.findAssociationAttributeName(
          'plot-visit',
          helpers.modelToApiName('plot-layout'),
        )
        const plotSelectionField = helpers.findAssociationAttributeName(
          'plot-layout',
          helpers.modelToApiName('plot-selection'),
        )
        // all authorised plot names
        let plotLayoutNames = data.body.map(
          (a) => a[plotSelectionField].plot_label,
        )
        plotLayoutNames = union(plotLayoutNames)
        plotLayoutNames = plotLayoutNames.sort()
        console.log('plotLayoutNames ' + JSON.stringify(plotLayoutNames))

        // query all authorised plot names
        let allProtPlotLayoutNames = []
        for (const prot_uuid of allAuthProtUuids) {
          const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
            prot_uuid,
          )
          if (!workFlow['surveyModel']) continue
          if (!workFlow['allModels']) continue

          const plotVisitField = helpers.findAssociationAttributeName(
            workFlow['surveyModel'],
            helpers.modelToApiName('plot-visit'),
          )
          if (!plotVisitField) continue

          // populate survey data
          const surveyData = await helpers.deepPopulateQuery(
            workFlow['surveyModel'],
            {
              survey_metadata: {
                survey_details: {
                  protocol_id: prot_uuid,
                },
              },
              [plotVisitField]: {
                [plotLayoutField]: {
                  [plotSelectionField]: {
                    plot_label: { $in: allAuthPlotSelections },
                  },
                },
              },
            },
            {
              survey_metadata: {
                populate: {
                  survey_details: true,
                },
              },
              [plotVisitField]: {
                populate: {
                  [plotLayoutField]: {
                    populate: {
                      [plotSelectionField]: true,
                    },
                  },
                },
              },
            },
            true,
          )
          const surveyPlotLayoutName = surveyData.map(
            (a) =>
              a[plotVisitField][plotLayoutField][plotSelectionField].plot_label,
          )
          allProtPlotLayoutNames =
            allProtPlotLayoutNames.concat(surveyPlotLayoutName)
        }

        allProtPlotLayoutNames = union(allProtPlotLayoutNames)
        allProtPlotLayoutNames = allProtPlotLayoutNames.sort()
        // lists should be same
        expect(plotLayoutNames).toEqual(allProtPlotLayoutNames)
      }
    })
})

it('Outgoing-Data-Filter-Test-022: filer outgoing data of plot selection by adding project id and prot uuid in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  for (const protocol of PROTOCOLS.CACHED) {
    const prot_uuid = protocol['identifier']
    if (!cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.includes(prot_uuid)) continue
    let apiMockConfigs = cloneDeep(mock.apiConfigs3)
    apiMockConfigs.push([
      cc.REQUEST_TYPE.GET,
      cc.API.USER_PROJECT,
      200,
      cloneDeep(mock.userProjects1),
    ])

    for (const project of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS) {
      // register apis with axios mock adapter
      registerMockUris(cloneDeep(apiMockConfigs), project, prot_uuid)

      // survey models e.g. bird-survey of bird protocol
      await request(global.strapiServer) // app server is an instance of Class: http.Server
        .get(
          `/api/plot-selections?populate=deep&project_id=${project}&prot_uuid=${prot_uuid}`,
        )
        .set('accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('authorization', mock.validJwt1)
        .then(async (data) => {
          expect(data.status).toBe(200)
          if (data.body) {
            const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
              prot_uuid,
            )
            const plotVisitField = helpers.findAssociationAttributeName(
              workFlow['surveyModel'],
              helpers.modelToApiName('plot-visit'),
            )
            if (!plotVisitField) {
              console.warn(`protocol: ${prot_uuid} is not plot based`)
              return
            }
            const plotLayoutField = helpers.findAssociationAttributeName(
              'plot-visit',
              helpers.modelToApiName('plot-layout'),
            )
            const plotSelectionField = helpers.findAssociationAttributeName(
              'plot-layout',
              helpers.modelToApiName('plot-selection'),
            )
            let plotSelectionsNames = data.body.map((a) => a.plot_label)
            plotSelectionsNames = union(plotSelectionsNames)
            plotSelectionsNames = plotSelectionsNames.sort()

            let projectInfo = mock.userProjects1.projects.find(
              (p) => p.id.toString() === project,
            )
            let plotSelections = []
            for (const p of projectInfo.plot_selections) {
              if (plotSelections.includes(p.name)) continue
              plotSelections.push(p.name)
            }
            // query all plot names associated to the protocol
            const surveyData = await helpers.deepPopulateQuery(
              workFlow['surveyModel'],
              {
                survey_metadata: {
                  survey_details: {
                    protocol_id: prot_uuid,
                  },
                },
                [plotVisitField]: {
                  [plotLayoutField]: {
                    [plotSelectionField]: {
                      plot_label: { $in: plotSelections },
                    },
                  },
                },
              },
              {
                survey_metadata: {
                  populate: {
                    survey_details: true,
                  },
                },
                [plotVisitField]: {
                  populate: {
                    [plotLayoutField]: {
                      populate: {
                        [plotSelectionField]: true,
                      },
                    },
                  },
                },
              },
              true,
            )
            let surveyPlotSelectionName = surveyData.map(
              (a) =>
                a[plotVisitField][plotLayoutField][plotSelectionField]
                  .plot_label,
            )
            surveyPlotSelectionName = union(surveyPlotSelectionName)
            surveyPlotSelectionName = surveyPlotSelectionName.sort()

            // lists will be same
            expect(plotSelectionsNames).toEqual(surveyPlotSelectionName)
          }
        })
    }
  }
})

it('Outgoing-Data-Filter-Test-023: filer outgoing data of plot selection by adding multiple project ids and prot uuid in the url', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  let endpoint = `/api/plot-selections?populate=deep`
  let apiMockConfigs = cloneDeep(mock.apiConfigs1)

  let allAuthPlotSelections = []
  for (const [i, proj] of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS.entries()) {
    // all project id params
    endpoint += `&project_ids[${i}]=${proj}`

    // list of all plot selections
    let projectInfo = mock.userProjects1.projects.find(
      (p) => p.id.toString() === proj,
    )
    for (const p of projectInfo.plot_selections) {
      if (allAuthPlotSelections.includes(p.name)) continue
      allAuthPlotSelections.push(p.name)
    }
  }
  for (const [i, p] of cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS.entries()) {
    // all protocol id params
    endpoint += `&prot_uuids[${i}]=${p}`
  }
  // all PDP combinations
  apiMockConfigs = apiMockConfigs.concat(
    generatePDPApiConfigs(
      cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS,
      cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS,
      cc.PDP_TYPE.READ,
    ),
  )
  apiMockConfigs.push([
    cc.REQUEST_TYPE.GET,
    cc.API.USER_PROJECT,
    200,
    cloneDeep(mock.userProjects1),
  ])
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(apiMockConfigs))

  // survey models e.g. bird-survey of bird protocol
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get(endpoint)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .then(async (data) => {
      expect(data.status).toBe(200)
      if (data.body) {
        if (data.body.length == 0) {
          console.warn(
            `No plot layout is associated with projects: ${cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS} and protocols: ${cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS}`,
          )
          return
        }
        const plotLayoutField = helpers.findAssociationAttributeName(
          'plot-visit',
          helpers.modelToApiName('plot-layout'),
        )
        const plotSelectionField = helpers.findAssociationAttributeName(
          'plot-layout',
          helpers.modelToApiName('plot-selection'),
        )
        // all the plot names we got
        let plotSelectionNames = data.body.map((a) => a.plot_label)
        plotSelectionNames = union(plotSelectionNames)
        plotSelectionNames = plotSelectionNames.sort()
        console.log('plotSelectionNames ' + JSON.stringify(plotSelectionNames))

        // query all plot names associated to the protocols
        let allProtPlotSelectionNames = []
        for (const prot_uuid of cc.PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS) {
          const workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
            prot_uuid,
          )
          const plotVisitField = helpers.findAssociationAttributeName(
            workFlow['surveyModel'],
            helpers.modelToApiName('plot-visit'),
          )
          if (!plotVisitField) continue

          // query survey data
          const surveyData = await helpers.deepPopulateQuery(
            workFlow['surveyModel'],
            {
              survey_metadata: {
                survey_details: {
                  protocol_id: prot_uuid,
                },
              },
              [plotVisitField]: {
                [plotLayoutField]: {
                  [plotSelectionField]: {
                    plot_label: { $in: allAuthPlotSelections },
                  },
                },
              },
            },
            {
              survey_metadata: {
                populate: {
                  survey_details: true,
                },
              },
              [plotVisitField]: {
                populate: {
                  [plotLayoutField]: {
                    populate: {
                      [plotSelectionField]: true,
                    },
                  },
                },
              },
            },
            true,
          )
          const surveyPlotSelectionNames = surveyData.map(
            (a) =>
              a[plotVisitField][plotLayoutField][plotSelectionField].plot_label,
          )
          allProtPlotSelectionNames = allProtPlotSelectionNames.concat(
            surveyPlotSelectionNames,
          )
        }
        allProtPlotSelectionNames = union(allProtPlotSelectionNames)
        allProtPlotSelectionNames = allProtPlotSelectionNames.sort()

        // lists should be same
        expect(plotSelectionNames).toEqual(allProtPlotSelectionNames)
      }
    })
})

it('Outgoing-Data-Filter-Test-024: filer outgoing data of plot selection using userProjects if no project id or prot uuid found in the url', async () => {
  let apiMockConfigs = cloneDeep(mock.apiConfigs1)
  const userProjectGet = [
    cc.REQUEST_TYPE.GET,
    cc.API.USER_PROJECT,
    200,
    cloneDeep(mock.userProjects1),
  ]
  // GET /user-projects (2x, once for PME, again for pdp-data-filter)
  apiMockConfigs.push(userProjectGet)
  apiMockConfigs.push(userProjectGet)
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(apiMockConfigs))

  let allAuthPlotSelections = []
  for (const project of mock.userProjects1.projects) {
    allAuthPlotSelections = allAuthPlotSelections.concat(
      project.plot_selections.map((a) => a.name.toString()),
    )
  }
  allAuthPlotSelections = union(allAuthPlotSelections)

  // survey models e.g. bird-survey of bird protocol
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/plot-selections?populate=deep')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .then(async (data) => {
      expect(data.status).toBe(200)
      if (data.body) {
        if (data.body.length == 0) {
          console.warn(
            `No plot visit is associated with user: ${mock.validJwt1}`,
          )
          return
        }

        // all authorised plot names
        let plotSelectionNames = data.body.map((a) => a.plot_label)
        plotSelectionNames = union(plotSelectionNames)
        plotSelectionNames = plotSelectionNames.sort()
        console.log('plotSelectionNames ' + JSON.stringify(plotSelectionNames))

        allAuthPlotSelections = allAuthPlotSelections.sort()
        console.log(
          'allAuthPlotSelections ' + JSON.stringify(allAuthPlotSelections),
        )
        // lists should be same
        expect(plotSelectionNames).toEqual(allAuthPlotSelections)
      }
    })
})

it('Outgoing-Data-Filter-Test-025: filer outgoing data of plot selection if protocol is `Plot Layout and Visit` or `Plot Selection and Layout`', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  let apiMockConfigs = cloneDeep(mock.apiConfigs3)
  apiMockConfigs.push([
    cc.REQUEST_TYPE.GET,
    cc.API.USER_PROJECT,
    200,
    cloneDeep(mock.userProjects1),
  ])

  for (const protocol of cc.PDP_DATA_FILTER_PLOT_SELECTION_PROTOCOLS) {
    for (const project of cc.PDP_DATA_FILTER_MIDDLEWARE_PROJECTS) {
      // register apis with axios mock adapter
      registerMockUris(cloneDeep(apiMockConfigs), project, protocol)

      await request(global.strapiServer) // app server is an instance of Class: http.Server
        .get(
          `/api/plot-selections?populate=deep&project_id=${project}&prot_uuid=${protocol}`,
        )
        .set('accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('authorization', mock.validJwt1)
        .then(async (data) => {
          expect(data.status).toBe(200)
          if (data.body) {
            let plotSelectionNames = data.body.map((a) => a.plot_label)
            plotSelectionNames = union(plotSelectionNames)
            plotSelectionNames = plotSelectionNames.sort()

            let projectInfo = mock.userProjects1.projects.find(
              (p) => p.id.toString() === project,
            )
            // all plot selections assigned to the project.
            let plotSelections = []
            for (const p of projectInfo.plot_selections) {
              if (plotSelections.includes(p.name)) continue
              plotSelections.push(p.name)
            }
            plotSelections = union(plotSelections)
            plotSelections = plotSelections.sort()

            // lists will be same
            expect(plotSelectionNames).toEqual(plotSelections)
          }
        })
    }
  }
})

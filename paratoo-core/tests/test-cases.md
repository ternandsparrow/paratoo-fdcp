# Core Unit Test Cases
>TODO Kira: write more guidelines on specificity of test cases

If you write a test case, make yourself as an author.<br>
If you edit/amend a test case, append your name to the list of authors
<br><br>

**Ensure correct usage of 'authorize' and 'authenticate'.<br>**
*Authorize* means to give access or permission to something. It is usually associated with access control or client privilege.
*Authenticate* means to validate the identity of a user. It is usually associated with login functionalities.

## 1. Bird-survey
### 1.1 GET /bird-surveys

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-GET-001 |
| Author            | Luke |
| Title             | Failure to find bird surveys when not authenticated |
| Description       | Test the ability of Core's project membership enforcer to reject GET requests when User is not authenticated |
| Initial Condition | -User is not authenticated (not logged in) |
| Testing Steps     | -submit GET request to http://localhost:1337/bird-surveys |
| Success Criteria  | -JSON: `{"statusCode":403,"error":"Forbidden","message":"User is not authenticated (logged in)"}` |
| Data              | -n/a |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-GET-002 |
| Author            | Luke |
| Title             | Successfully find bird surveys when authenticated |
| Description       | Test the ability of Core to perform bird-survey requests when the User is authenticated |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -submit GET request to http://localhost:1337/bird-surveys |
| Success Criteria  | -JSON: `[{ <bird-survey-1>, <bird-survey-2>, ... , <bird-survey-n> }]` |
| Data              | -a User account for authentication |

<br>

>`Bird-Srvy-GET-003` to `Bird-Srvy-GET-007` are testing the project membership enforcer policy.

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-GET-003 |
| Author            | Luke |
| Title             | Failure to find bird surveys when the protocol could not be found |
| Description       | Test the ability of Core's `getProtocolId()` helper function to gracefully fail finding a protocol ID when the provided endpoint prefix could not be matched to a protocol ID <br><br>**NOTE**: I'm not sure how we'll test this. If we provide an incorrect endpoint prefix to the request itself (e.g., `/foo` ), Strapi will simply return `not found` (not even a proper 404) and not even get to the policy.<br>To get the desired result we'd need to provide a valid endpoint prefix but modify it in the code so that `getProtocolId()`'s provided endpoint prefix is incorrect. It could be argued that because of this, we don't even need to test this. But it's not impossible that the endpoint prefix gets corrupted during runtime.<br>Another potential idea could be to add this dud route but not store it in Core, so that the lookup fails.<br>Or similarly, we remove/modify the endpoint prefix from the `bird survey` entry in `protocols` so that the `/bird-surveys` route exists but the endpoint prefix isn't stored with the protocol. Note that we lookup the protocols from the Core stored protocols. |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -submit a GET request to http://localhost:1337/foo |
| Success Criteria  | -JSON: `{"statusCode":404,"error":"Not Found","message":"Could not find protocol for endpoint: '/foo'"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-GET-004 |
| Author            | Luke |
| Title             | Failure to find bird surveys when the User does not have any projects assigned to them |
| Description       | Test the ability of Core's `getProjectId()` helper function to gracefully fail finding the User's project IDs when said User does not have any projects assigned to them. |
| Initial Condition | -User is authenticated (logged in) <br>-the User does not have any projects assigned to them |
| Testing Steps     | -submit GET request to http://localhost:1337/bird-surveys |
| Success Criteria  | -JSON: `{"statusCode":404,"error":"Not Found","message":"User does not have any projects"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-GET-005 |
| Author            | Luke |
| Title             | Failure to find bird surveys when the User does not have any projects assigned for the given protocol |
| Description       | Test the ability of Core's `getProjectId()` helper function to gracefully fail finding the User's project IDs when said User does not have any projects assigned to them for the given protocol. |
| Initial Condition | -User is authenticated (logged in) <br>-the User has a project assigned to them that is not associated with the given protocol |
| Testing Steps     | -submit GET request to http://localhost:1337/bird-surveys |
| Success Criteria  | -JSON: `{"statusCode":401,"error":"Unauthorized","message":"User is not authorized (does not have projects assigned for given protocol of ID 1)"}` |
| Data              | -a User account for authentication <br>-a project that is not associated with the `bird survey` protocol, assigned to the User |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-GET-006 |
| Author            | Luke |
| Title             | Failure to find bird surveys when the PDP encounters an error |
| Description       | Test the ability of the project membership enforcer to gracefully fail when the PDP encounters a problem<br><br>-**NOTE**: similar issue as Bird-Srvy-GET-003, except the PDP will never explicitly throw an error, and will only throw if an unexpected error occurs (like a 500).<br>We Could test this by modifying the code to throw some arbitrary error from the PDP so the the enforcer catches and handles appropriately. To simulate this scenario I usually throw a `418` at the top of `org-interface`'s `decide()`. It's a weird error that's included in the HTTP spec that I don't think anyone uses. The error to throw could look like:<br>`return paratooErrorHandler(ctx, 418, new Error('Something happened to the PDP'))` |
| Initial Condition | -User is authenticated (logged in) <br>-the User has a project assigned to them that is associated with the `bird survey` protocol |
| Testing Steps     | -submit GET request to http://localhost:1337/bird-surveys |
| Success Criteria  | -JSON: `{"statusCode":418,"error":"I'm a teapot","message":"Request failed with status code 418. Something happened to the PDP. The PDP encountered an error"}` |
| Data              | -a User account for authentication <br>-a project that is associated with the `bird survey` protocol, assigned to the User |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-GET-007 |
| Author            | Luke |
| Title             | Failure to find bird surveys when the supplied JWT is invalid |
| Description       | Test the ability of the project membership enforcer to gracefully fail when the supplied JWT is not valid.<br>-**NOTE**: we're only testing the ability of Core to handle the error correctly, but the actual error is not received from Org due to the limitations of Jest. Instead we mock Org's response and test it's ability to reject bad tokens in its own unit test. |
| Initial Condition | -n/a |
| Testing Steps     | -submit GET request to http://localhost:1337/bird-surveys with a header containing the auth token `foobar` |
| Success Criteria  | -JSON: `{"statusCode":401,"error":"Unauthorized","message":"Auth token is not valid"}` |
| Data              | -n/a |

### 1.2 POST /bird-surveys/bulk

>`Bird-Srvy-POST-bulk-001` to `Bird-Srvy-POST-bulk-011` are testing the submission of a single collection, i.e., the `collections` array has only one object

>Section 1.1 (GET /bird-surveys) tests the project membership enforcer, so those tests don't need to be repeated here. Though there is a few instances where we need to test it with a `bulk()` request.

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-bulk-001 |
| Author            | Luke |
| Title             | Successfully POST a collection of bird surveys to Core |
| Description       | Test the ability of Core to POST a collection using the bulk() method |
| Initial Condition | -User is authenticated (logged in) <br> -the User has a list of their Projects (required to mint identifier) <br> -the User has been minted an identifier |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-1 <br> -submit a POST request to http://localhost:1337/bird-surveys/bulk |
| Success Criteria  | -returns a JSON of the body provided for the POST request <br> -the submitted data is in the bird-survey-obs database |
| Data              | -a User account for authentication <br>-a User account that is assigned to a Project in which they have write permission |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-bulk-002 |
| Author            | Luke |
| Title             | Failure to POST a collection of bird surveys to Core when not authenticated |
| Description       | Test the ability of Core to reject requests to POST bird survey collections when User is not authenticated |
| Initial Condition | -input a body with the supplied JSON in Appendix 1-1 <br>-User is not authenticated (not logged in) |
| Testing Steps     | -submit a POST request to http://localhost:1337/bird-surveys/bulk |
| Success Criteria  | -JSON: `{"statusCode":403,"error":"Forbidden","message":"User is not authenticated (logged in)"}` |
| Data              | -n/a |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-bulk-003 |
| Author            | Luke |
| Title             | Failure to POST a collection of bird surveys to Core when an `orgMintedIdentifier` is not provided |
| Description       | Test the ability of Core to reject requests to POST bird survey collections when an `orgMintedIdentifier` is not provided (empty string)  |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-2 <br> -submit a POST request to http://localhost:1337/bird-surveys/bulk |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/orgMintedIdentifier must NOT have fewer than 1 characters"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-bulk-004 |
| Author            | Luke |
| Title             | Failure to POST a collection of bird surveys to Core when an `orgMintedIdentifier` could not be decrypted |
| Description       | Test the ability of the project membership enforcer to gracefully fail when the `orgMintedIdentifier` could not be decrypted |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-3 <br> -submit a POST request to http://localhost:1337/bird-surveys/bulk |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"json decrypt: invalid parameters. Could not decrypt the orgMintedIdentifier"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-bulk-005 |
| Author            | Luke |
| Title             | Failure to POST a collection of bird surveys to Core when the provided survey data is invalid |
| Description       | Test the ability of Core to reject requests to POST bird survey collections when the provided bird survey data is not valid (A: `start_time` is not of format `date-time`, B: `end_date` is not of format `date-time`, C: `playback-used` is not a `boolean`, D: the `survey_type` is not an `integer`, E: the `survey_metadata` is not an `object`) |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-4 <br> -submit a POST request to http://localhost:1337/bird-surveys/bulk |
| Success Criteria  | -A: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/bird-survey/start_time must match format \"date-time\""}` <br>-B: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/bird-survey/end_date must match format \"date-time\""}` <br>-C: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/bird-survey/playback_used must be boolean"}` <br>-D: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/bird-survey/survey_type must be equal to one of the allowed values"}` <br>-E: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/bird-survey/survey_metadata must be object"}` |
| Data              | -a User account for authentication <br>-see Appendix 1-4 |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-bulk-006 |
| Author            | Luke |
| Title             | Failure to POST a collection of bird surveys to Core when the provided weather data is invalid |
| Description       | Test the ability of Core to reject requests to POST bird survey collections when the provided weather survey data is not valid (A: `temperature` is not an `integer`, B: `precipitation` is not an `integer`, C: `precipitation_duration` is not an `integer`, D: `wind_description` is not an `integer`, E: `cloud_cover` is not an `integer`) |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-5 <br> -submit a POST request to http://localhost:1337/bird-surveys/bulk |
| Success Criteria  | -A: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/weather-survey/temperature must be number"}` <br>-B: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/weather-survey/precipitation must be equal to one of the allowed values"}` <br>-C: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/weather_survey/precipitation_duration must be integer"}` <br>-D: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/weather_survey/wind_description must be integer"}` <br>-E: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/weather_survey/cloud_cover must be integer"}` |
| Data              | -a User account for authentication <br>-see Appendix 1-5 |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-bulk-007 |
| Author            | Luke |
| Title             | Failure to POST a collection of bird surveys to Core when the provided `bird_survey_observations` array is empty |
| Description       | Test the ability of Core to reject requests to POST bird survey collections when the provided `bird_survey_observations` array is empty |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-6 <br> -submit a POST request to http://localhost:1337/bird-surveys/bulk |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/bird_survey_observations must NOT have fewer than 1 items"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-bulk-008 |
| Author            | Luke |
| Title             | Failure to POST a collection of bird surveys to Core when the provided `bird_survey_observations` object is invalid |
| Description       | Test the ability of Core to reject requests to POST bird survey collections when the provided `bird_survey_observations` object is invalid (A: `species` is not a `string`, B: `count` is not at `integer`, C: `observation_type` is not an `integer`, D: `activity_type` is not an `integer`, E: `observation_location_type` is not an `integer`, F: `breeding_type` is not an `integer`) |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-7 <br> -submit a POST request to http://localhost:1337/bird-surveys/bulk |
| Success Criteria  | -A: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/bird_survey_observations/0/species must be string"}` <br>-B: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/bird_survey_observations/0/count must be integer"}` <br>-C: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/bird_survey_observations/0/observation_type must be integer"}` <br>-D: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/bird_survey_observations/0/activity_type must be integer"}` <br>-E: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/bird_survey_observations/0/observation_location_type must be integer"}` <br>-F: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections/0/bird_survey_observations/0/breeding_type must be integer"}` |
| Data              | -a User account for authentication <br>-see Appendix 1-7 |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-bulk-009 |
| Author            | Luke |
| Title             | Failure to POST a collection of bird surveys to Core when the `collections` array is empty |
| Description       | Test the ability of Core to reject requests to POST bird survey collections when the `collections` array is empty |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-8 <br> -submit a POST request to http://localhost:1337/bird-surveys/bulk |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/collections must NOT have fewer than 1 items"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-bulk-010 |
| Author            | Luke |
| Title             | Failure to POST a collection of bird surveys to Core when the `bird survey` protocol is not writable |
| Description       | Test the ability of the project membership enforcer to gracefully fail when the PDP determines that the supplied protocol is not writable |
| Initial Condition | -User is authenticated (logged in) <br>-the  `bird survey` protocol is not writable |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-1 <br> -submit a POST request to http://localhost:1337/bird-surveys/bulk |
| Success Criteria  | -JSON: `{"statusCode":401,"error":"Unauthorized","message":"User is NOT authorized to write project 1"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-bulk-011 |
| Author            | Luke |
| Title             | Failure to POST a collection of bird surveys to Core when the `collectionIdentifier` already exists |
| Description       | Test the ability of Core to reject requests to POST bird survey collections when the `collectionIdentifier` already exists, i.e., the collection has already been POSTed |
| Initial Condition | -User is authenticated (logged in) <br>-the collection has already been POSTed |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-1 <br> -submit a POST request to http://localhost:1337/bird-surveys/bulk |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Collection has already been submitted; the collection identifier already exists"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-bulk-012 |
| Author            | Luke |
| Title             | Failure to POST a collection of bird surveys to Core when the survey IDs in the survey and orgMintedIdentifier do not match |
| Description       | Test the ability of Core to reject requests to POST bird survey collections when the survey IDs in the survey and orgMintedIdentifier do not match |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1- <br> -submit a POST request to http://localhost:1337/bird-surveys/bulk |
| Success Criteria  | -JSON: `` |
| Data              | -a User account for authentication |

### 1.3 POST /bird-surveys
>NOTE: we don't need to test the validity of the request body, as the tests for bulk() are doing this. As they reference the same model, there is no point testing these twice.<br> Additionally, we don't need to test the enforcer's ability to check projects and protocols as the tests for GET do this.

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-001 |
| Author            | Luke |
| Title             | Successful submission of an individual bird survey |
| Description       | Test the ability of Core to successfully create a bird survey entry |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-9 <br> -submit a POST request to http://localhost:1337/bird-surveys |
| Success Criteria  | -JSON of the submitted bird-survey is returned |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-002 |
| Author            | Luke |
| Title             | Failure to submit an individual bird survey when the User is not authenticated |
| Description       | Test the ability of Core to gracefully fail submitting an individual bird survey when the User is not authenticated (not logged in) |
| Initial Condition | -User is not authenticated (not logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-9 (required to get past validation) <br> -submit a POST request to http://localhost:1337/bird-surveys |
| Success Criteria  | -JSON: `{"statusCode":403,"error":"Forbidden","message":"User is not authenticated (logged in)"}` |
| Data              | -n/a |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-POST-003 |
| Author            | Luke |
| Title             | Failure to submit an individual bird survey when the supplied request body is empty JSON |
| Description       | Test the ability of Core to gracefully fail submitting an individual bird survey when the supplied request body is empty JSON |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input an empty request body: `{}` <br> -submit a POST request to http://localhost:1337/bird-surveys |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data must have required property 'playback_used'"}` |
| Data              | -a User account for authentication |

### 1.4 PUT /bird-surveys
>NOTE: we don't need to test the validity of the request body, as the tests for bulk() are doing this. As they reference the same model, there is no point testing these twice.<br> Additionally, we don't need to test the enforcer's ability to check projects and protocols as the tests for GET do this.

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-PUT-001 |
| Author            | Luke |
| Title             | Successful update of an individual bird survey |
| Description       | Test the ability of Core to successfully update a bird survey entry |
| Initial Condition | -User is authenticated (logged in) <br>-a bird survey exists in the database |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-9, with some modifications <br> -submit a PUT request to http://localhost:1337/bird-surveys{id}, with an ID of the survey to update |
| Success Criteria  | -JSON of the updated bird-survey is returned <br>-the database reflects the supplied changed data |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-PUT-002 |
| Author            | Luke |
| Title             | Failure to update an individual bird survey when the User is not authenticated |
| Description       | Test the ability of Core to gracefully fail updating an individual bird survey when the User is not authenticated |
| Initial Condition | -User is not authenticated (not logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-9 (required to get past validation) <br> -submit a PUT request to http://localhost:1337/bird-surveys/1 <br>NOTE: the ID of the bird survey does not have to exist, as the project enforcer will throw the error before checking the existence of said ID |
| Success Criteria  | -JSON: `{"statusCode":403,"error":"Forbidden","message":"User is not authenticated (logged in)"}` |
| Data              | -n/a |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-PUT-003 |
| Author            | Luke |
| Title             | Failure to update an individual bird survey when the supplied ID does not exist |
| Description       | Test the ability of Core to gracefully fail updating an individual bird survey when the supplied ID does not correspond to a stored bird survey |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-9 (required to get past validation) <br> -submit a PUT request to http://localhost:1337/bird-surveys/{id}, with an ID that does not exist in the database |
| Success Criteria  | -JSON: `{"statusCode":404,"error":"Not Found","message":"entry.notFound"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-PUT-004 |
| Author            | Luke |
| Title             | Failure to update an individual bird survey when the supplied ID is not a valid parameter |
| Description       | Test the ability of Core to gracefully fail updating an individual bird survey when the supplied ID is not a valid parameter |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-9 (required to get past validation) <br> -submit a PUT request to http://localhost:1337/bird-surveys/foo |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"id must be a `number` type, but the final value was: `NaN` (cast from the value `\"foo\"`)."}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-PUT-005 |
| Author            | Luke |
| Title             | Failure to update an individual bird survey when no ID is supplied |
| Description       | Test the ability of Core to gracefully fail updating an individual bird survey when no ID is supplied (no path parameters) |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-9 (required to get past validation) <br> -submit a PUT request to http://localhost:1337/bird-surveys |
| Success Criteria  | -returns `Method Not Allowed` |
| Data              | -a User account for authentication |

### 1.5 DELETE /bird-surveys
>NOTE: we don't need to test the validity of the request body, as the tests for bulk() are doing this. As they reference the same model, there is no point testing these twice.<br> Additionally, we don't need to test the enforcer's ability to check projects and protocols as the tests for GET do this.

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-DELETE-001 |
| Author            | Luke |
| Title             | Successful deletion of an individual bird survey |
| Description       | Test the ability of Core to successfully delete a bird survey entry |
| Initial Condition | -User is authenticated (logged in) <br>-a bird survey exists in the database |
| Testing Steps     | -submit a DELETE request to http://localhost:1337/bird-surveys{id}, with an ID of the survey to delete |
| Success Criteria  | -JSON of the updated bird-survey is returned <br>-entry is no longer present in the database |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-DELETE-002 |
| Author            | Luke |
| Title             | Failure to delete an individual bird survey when the User is not authenticated |
| Description       | Test the ability of Core to to gracefully fail deleting an individual bird survey when the User is not authenticated |
| Initial Condition | -User is not authenticated (not logged in) |
| Testing Steps     | -submit a DELETE request to http://localhost:1337/bird-surveys/1 <br>NOTE: the ID of the bird survey does not have to exist, as the project enforcer will throw the error before checking the existence of said ID |
| Success Criteria  | -JSON: `{"statusCode":403,"error":"Forbidden","message":"User is not authenticated (logged in)"}` |
| Data              | -n/a |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-DELETE-003 |
| Author            | Luke |
| Title             | Failure to delete an individual bird survey when the supplied ID does not exist |
| Description       | Test the ability of Core to gracefully fail deleting an individual bird survey when the supplied ID does not correspond to a stored bird-survey |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -submit a DELETE request to http://localhost:1337/bird-surveys/{id}, with an ID that does not exist in the database |
| Success Criteria  | -returns `Not Found` <br> NOTE: should this be consistent with PUT and return a proper error code? |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-DELETE-004 |
| Author            | Luke |
| Title             | Failure to delete an individual bird survey when the supplied ID is not a valid parameter |
| Description       | Test the ability of Core to gracefully fail deleting an individual bird survey when the supplied ID is not a valid parameter |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -submit a DELETE request to http://localhost:1337/bird-surveys/foo |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"id must be a `number` type, but the final value was: `NaN` (cast from the value `\"foo\"`)."}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Bird-Srvy-DELETE-005 |
| Author            | Luke |
| Title             | Failure to delete an individual bird survey when no ID is supplied |
| Description       | Test the ability of Core to gracefully fail deleting an individual bird survey when no ID is supplied (no path parameters) |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -submit a DELETE request to http://localhost:1337/bird-surveys |
| Success Criteria  | -returns `Method Not Allowed` |
| Data              | -a User account for authentication |

### 1.6 Bootstrap createAdminUser()
>TODO add more tests (especially test-to-fail)

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | boot-create-admin-001 |
| Author            | Luke |
| Title             | Successfully create an admin user during Strapi bootstrap |
| Description       | Test the ability of Core to successfully create an admin user during the bootstrap process |
| Initial Condition | -Strapi does not have a persistent volume,<br>i.e., the time Strapi was brought down was with the `--volumes` flag |
| Testing Steps     | -start Strapi <br>-submit a POST request to http://localhost:1337/admin/login with request body in data row |
| Success Criteria  | -a code 200 is returned that includes a JWT in the response payload |
| Data              | -request body JSON: `{"email": "tech.contact@environmentalmonitoringgroup.com.au","password": "admin"}` <br>-note: these credentials are what the bootstrap will default to. If different credentials are supplied in the environment variables, then those should be used to login. |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | boot-create-admin-002 |
| Author            | Luke |
| Title             | Failure to login with the bootstrapped admin account when the provided credentials are incorrect |
| Description       | \<description> |
| Initial Condition | \<init-cond.> |
| Testing Steps     | \<steps> |
| Success Criteria  | \<criteria> |
| Data              | \<data> |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | boot-create-admin-003 |
| Author            | Luke |
| Title             | Failure to create a bootstrapped admin account when the database query fails |
| Description       | Test the ability of Core to gracefully fail when an admin user could not be created during the bootstrap process when the database query fails due to bad variables being provided in the environment variables |
| Initial Condition | \<init-cond.> |
| Testing Steps     | \<steps> |
| Success Criteria  | \<criteria> |
| Data              | \<data> |

# Appendices
## Appendix 1: Code for Tests
>Add code for tests here that won't format correctly inside the tables

### Appendix 1-1: Bird-Srvy-POST-bulk-001, Bird-Srvy-POST-bulk-002, Bird-Srvy-POST-bulk-010, Bird-Srvy-POST-bulk-011 POST request JSON
```
{"headers":{"Authorization": "Bearer + eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywiaWF0IjoxNjI5MjQ1NDgxLCJleHAiOjE2MzE4Mzc0ODF9.GJAWx_tQUc0H7GfKMaNbbkDyCeRtm_Aa9WaOtMmu3J8 }}
```
Note: the below `orgMintedIdentifier` is created for `projectId`, `protocolId`, and `protocol version` equals 1. The `survey_metadata` is `"123"`.
```
{
    "collections": [
        {
            "orgMintedIdentifier": "{\"iv\":\"vF5ehIlUZASiXu8pNtkk0Q==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"ki/c++XiA8U=\",\"ct\":\"57gqA9Z12/C4PizMl3+okOeGpcAChxdXz2pYviuY7Wyx0a0zmlRlC/ECXyaI6EsZg4eu0gHt4B4bu6rIQpJVntlS+gE3IOcTgEZPSg4ag9xy2aHqGCls8JAKHqmdPc8kDzG3PdqUHEgNrDwJlz75x9tYl3nO9ko7OITR2Vs=\"}",
            "bird_survey": {
                "start_time": "2020-12-09T11:54:38.163Z",
                "end_date": "2020-12-10T11:54:38.163Z",
                "playback_used": true,
                "survey_type": 1,
                "survey_metadata": {
                    "survey_details": {
                        "survey_model": "bird-survey",
                        "time": "2022-09-14T01:39:21.635Z",
                        "uuid": "def92b55-b786-43ed-8e89-0e25b135454c",
                        "project_id": "1",
                        "protocol_id": "c1b38b0f-a888-4f28-871b-83da2ac1e533",
                        "protocol_version": "1"
                    },
                    "user_details": {
                        "role": "collector",
                        "email": "testuser@email.com",
                        "username": "testuser"
                    },
                    "provenance": {
                        "version_app": "0.0.1-xxxxx",
                        "version_core": "0.0.1-xxxxx",
                        "version_core_documentation": "0.0.1-xxxxx",
                        "version_org": "4.4-SNAPSHOT",
                        "system_app": "monitor",
                        "system_core": "Monitor FDCP API",
                        "system_org": "monitor"
                    }
                },
            },
            "weather_survey": {
                "temperature": 45,
                "precipitation": 1,
                "precipitation_duration": 2,
                "wind_description": 7,
                "cloud_cover": 2
            },
            "bird_survey_observations": [
                {
                "species": "bird1",
                "count": 412,
                "observation_type": 1,
                "activity_type": 8,
                "observation_location_type": 1,
                "breeding_type": 3
                },
                {
                "species": "bird3",
                "count": 543,
                "observation_type": 2,
                "activity_type": 6,
                "observation_location_type": 2,
                "breeding_type": 2
                }
            ]
        }
    ]
}
```

### Appendix 1-2: Bird-Srvy-POST-bulk-003 POST request JSON
```
{
    "bird_survey": {
        "start_time": "2021-07-30", 
        "end_time": "2021-08-01", 
        "playback_used": true, 
        "survey_type": "202",
        "site_location_id": 1
    },
    "bird_survey_observations": [
        {
            "species":"TestBird",
            "count":"10",
            "observation_type":"H",
            "activity":"FOS",
            "location_of_observation":"WS",
            "breeding_activity":"DBE"
        }
    ]
}
```

### Appendix 1-3: Bird-Srvy-POST-bulk-004 POST request JSON
```
{
    "collections": [
        {
            "orgMintedIdentifier": "{\"foo\": \"bar\" }",
            "bird_survey": {
                "start_time": "2020-12-09T11:54:38.163Z",
                "end_date": "2020-12-10T11:54:38.163Z",
                "playback_used": true,
                "survey_type": 1,
                "survey_metadata": {
                    "survey_details": {
                        "survey_model": "bird-survey",
                        "time": "2022-09-14T01:39:21.635Z",
                        "uuid": "def92b55-b786-43ed-8e89-0e25b135454c",
                        "project_id": "1",
                        "protocol_id": "c1b38b0f-a888-4f28-871b-83da2ac1e533",
                        "protocol_version": "1"
                    },
                    "user_details": {
                        "role": "collector",
                        "email": "testuser@email.com",
                        "username": "testuser"
                    },
                    "provenance": {
                        "version_app": "0.0.1-xxxxx",
                        "version_core": "0.0.1-xxxxx",
                        "version_core_documentation": "0.0.1-xxxxx",
                        "version_org": "4.4-SNAPSHOT",
                        "system_app": "monitor",
                        "system_core": "Monitor FDCP API",
                        "system_org": "monitor"
                    }
                },
            },
            "weather_survey": {
                "temperature": 45,
                "precipitation": 1,
                "precipitation_duration": 2,
                "wind_description": 7,
                "cloud_cover": 2
            },
            "bird_survey_observations": [
                {
                    "species": "bird1",
                    "count": 412,
                    "observation_type": 1,
                    "activity_type": 8,
                    "observation_location_type": 1,
                    "breeding_type": 3
                },
                {
                    "species": "bird3",
                    "count": 543,
                    "observation_type": 2,
                    "activity_type": 6,
                    "observation_location_type": 2,
                    "breeding_type": 2
                }
            ]
        }
    ]
}
```

### Appendix 1-4: Bird-Srvy-POST-bulk-005 POST request JSON
Note: the below `orgMintedIdentifier` is created for `projectId`, `protocolId`, and `protocol version` equals 1. The `survey_metadata` is `"123"`.
```
{
    "collections": [
        {
            "orgMintedIdentifier": "{\"iv\":\"vF5ehIlUZASiXu8pNtkk0Q==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"ki/c++XiA8U=\",\"ct\":\"57gqA9Z12/C4PizMl3+okOeGpcAChxdXz2pYviuY7Wyx0a0zmlRlC/ECXyaI6EsZg4eu0gHt4B4bu6rIQpJVntlS+gE3IOcTgEZPSg4ag9xy2aHqGCls8JAKHqmdPc8kDzG3PdqUHEgNrDwJlz75x9tYl3nO9ko7OITR2Vs=\"}",
            "bird_survey": {
                <data>
            },
            "weather_survey": {
                "temperature": 45,
                "precipitation": 1,
                "precipitation_duration": 2,
                "wind_description": 7,
                "cloud_cover": 2
            },
            "bird_survey_observations": [
                {
                "species": "bird1",
                "count": 412,
                "observation_type": 1,
                "activity_type": 8,
                "observation_location_type": 1,
                "breeding_type": 3
                },
                {
                "species": "bird3",
                "count": 543,
                "observation_type": 2,
                "activity_type": 6,
                "observation_location_type": 2,
                "breeding_type": 2
                }
            ]
        }
    ]
}
```

#### Bird-Srvy-POST-bulk-005 CASE A (invalid `start_time`):
```
"bird_survey": {
    "start_time": "",
    "end_date": "2020-12-10T11:54:38.163Z",
    "playback_used": true,
    "survey_type": 1,
    "survey_metadata": {
        "survey_details": {
            "survey_model": "bird-survey",
            "time": "2022-09-14T01:39:21.635Z",
            "uuid": "def92b55-b786-43ed-8e89-0e25b135454c",
            "project_id": "1",
            "protocol_id": "c1b38b0f-a888-4f28-871b-83da2ac1e533",
            "protocol_version": "1"
        },
        "user_details": {
            "role": "collector",
            "email": "testuser@email.com",
            "username": "testuser"
        },
        "provenance": {
            "version_app": "0.0.1-xxxxx",
            "version_core": "0.0.1-xxxxx",
            "version_core_documentation": "0.0.1-xxxxx",
            "version_org": "4.4-SNAPSHOT",
            "system_app": "monitor",
            "system_core": "Monitor FDCP API",
            "system_org": "monitor"
        }
    }
}
```

#### Bird-Srvy-POST-bulk-005 CASE B (invalid `end_date`):
```
"bird_survey": {
    "start_time": "2020-12-09T11:54:38.163Z",
    "end_date": "",
    "playback_used": true,
    "survey_type": 1,
    "survey_metadata": {
        "survey_details": {
            "survey_model": "bird-survey",
            "time": "2022-09-14T01:39:21.635Z",
            "uuid": "def92b55-b786-43ed-8e89-0e25b135454c",
            "project_id": "1",
            "protocol_id": "c1b38b0f-a888-4f28-871b-83da2ac1e533",
            "protocol_version": "1"
        },
        "user_details": {
            "role": "collector",
            "email": "testuser@email.com",
            "username": "testuser"
        },
        "provenance": {
            "version_app": "0.0.1-xxxxx",
            "version_core": "0.0.1-xxxxx",
            "version_core_documentation": "0.0.1-xxxxx",
            "version_org": "4.4-SNAPSHOT",
            "system_app": "monitor",
            "system_core": "Monitor FDCP API",
            "system_org": "monitor"
        }
    }
}
```

#### Bird-Srvy-POST-bulk-005 CASE C (invalid `playback_used`):
```
"bird_survey": {
    "start_time": "2020-12-09T11:54:38.163Z",
    "end_date": "2020-12-10T11:54:38.163Z",
    "playback_used": "true",
    "survey_type": 1,
    "survey_metadata": {
        "survey_details": {
            "survey_model": "bird-survey",
            "time": "2022-09-14T01:39:21.635Z",
            "uuid": "def92b55-b786-43ed-8e89-0e25b135454c",
            "project_id": "1",
            "protocol_id": "c1b38b0f-a888-4f28-871b-83da2ac1e533",
            "protocol_version": "1"
        },
        "user_details": {
            "role": "collector",
            "email": "testuser@email.com",
            "username": "testuser"
        },
        "provenance": {
            "version_app": "0.0.1-xxxxx",
            "version_core": "0.0.1-xxxxx",
            "version_core_documentation": "0.0.1-xxxxx",
            "version_org": "4.4-SNAPSHOT",
            "system_app": "monitor",
            "system_core": "Monitor FDCP API",
            "system_org": "monitor"
        }
    }
}
```

#### Bird-Srvy-POST-bulk-005 CASE D (invalid `survey_type`):
```
"bird_survey": {
    "start_time": "2020-12-09T11:54:38.163Z",
    "end_date": "2020-12-10T11:54:38.163Z",
    "playback_used": true,
    "survey_type": "1",
    "survey_metadata": {
        "survey_details": {
            "survey_model": "bird-survey",
            "time": "2022-09-14T01:39:21.635Z",
            "uuid": "def92b55-b786-43ed-8e89-0e25b135454c",
            "project_id": "1",
            "protocol_id": "c1b38b0f-a888-4f28-871b-83da2ac1e533",
            "protocol_version": "1"
        },
        "user_details": {
            "role": "collector",
            "email": "testuser@email.com",
            "username": "testuser"
        },
        "provenance": {
            "version_app": "0.0.1-xxxxx",
            "version_core": "0.0.1-xxxxx",
            "version_core_documentation": "0.0.1-xxxxx",
            "version_org": "4.4-SNAPSHOT",
            "system_app": "monitor",
            "system_core": "Monitor FDCP API",
            "system_org": "monitor"
        }
    }
}
```

#### Bird-Srvy-POST-bulk-005 CASE E (invalid `survey_metadata`):
```
"bird_survey": {
    "start_time": "2020-12-09T11:54:38.163Z",
    "end_date": "2020-12-10T11:54:38.163Z",
    "playback_used": true,
    "survey_type": 1,
    "survey_metadata": ""
}
```

### Appendix 1-5: Bird-Srvy-POST-bulk-006 POST request JSON
Note: the below `orgMintedIdentifier` is created for `projectId`, `protocolId`, and `protocol version` equals 1. The `survey_metadata` is `"123"`.
```
{
    "collections": [
        {
            "orgMintedIdentifier": "{\"iv\":\"vF5ehIlUZASiXu8pNtkk0Q==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"ki/c++XiA8U=\",\"ct\":\"57gqA9Z12/C4PizMl3+okOeGpcAChxdXz2pYviuY7Wyx0a0zmlRlC/ECXyaI6EsZg4eu0gHt4B4bu6rIQpJVntlS+gE3IOcTgEZPSg4ag9xy2aHqGCls8JAKHqmdPc8kDzG3PdqUHEgNrDwJlz75x9tYl3nO9ko7OITR2Vs=\"}",
            "bird_survey": {
                "start_time": "2020-12-09T11:54:38.163Z",
                "end_date": "2020-12-10T11:54:38.163Z",
                "playback_used": true,
                "survey_type": 1,
                "survey_metadata": {
                    "survey_details": {
                        "survey_model": "bird-survey",
                        "time": "2022-09-14T01:39:21.635Z",
                        "uuid": "def92b55-b786-43ed-8e89-0e25b135454c",
                        "project_id": "1",
                        "protocol_id": "c1b38b0f-a888-4f28-871b-83da2ac1e533",
                        "protocol_version": "1"
                    },
                    "user_details": {
                        "role": "collector",
                        "email": "testuser@email.com",
                        "username": "testuser"
                    },
                    "provenance": {
                        "version_app": "0.0.1-xxxxx",
                        "version_core": "0.0.1-xxxxx",
                        "version_core_documentation": "0.0.1-xxxxx",
                        "version_org": "4.4-SNAPSHOT",
                        "system_app": "monitor",
                        "system_core": "Monitor FDCP API",
                        "system_org": "monitor"
                    }
                }
            },
            "weather_survey": {
                <data>
            },
            "bird_survey_observations": [
                {
                "species": "bird1",
                "count": 412,
                "observation_type": 1,
                "activity_type": 8,
                "observation_location_type": 1,
                "breeding_type": 3
                },
                {
                "species": "bird3",
                "count": 543,
                "observation_type": 2,
                "activity_type": 6,
                "observation_location_type": 2,
                "breeding_type": 2
                }
            ]
        }
    ]
}
```
#### Bird-Srvy-POST-bulk-006 CASE A (invalid `temperature`):
```
"weather_survey": {
    "temperature": "45",
    "precipitation": 1,
    "precipitation_duration": 2,
    "wind_description": 7,
    "cloud_cover": 2
}
```

#### Bird-Srvy-POST-bulk-006 CASE B (invalid `precipitation`):
```
"weather_survey": {
    "temperature": 45,
    "precipitation": "1",
    "precipitation_duration": 2,
    "wind_description": 7,
    "cloud_cover": 2
}
```

#### Bird-Srvy-POST-bulk-006 CASE C (invalid `precipitation_duration`):
```
"weather_survey": {
    "temperature": 45,
    "precipitation": 1,
    "precipitation_duration": "2",
    "wind_description": 7,
    "cloud_cover": 2
}
```

#### Bird-Srvy-POST-bulk-006 CASE D (invalid `wind_description`):
```
"weather_survey": {
    "temperature": 45,
    "precipitation": 1,
    "precipitation_duration": 2,
    "wind_description": "7",
    "cloud_cover": 2
}
```

#### Bird-Srvy-POST-bulk-006 CASE E (invalid `cloud_cover`):
```
"weather_survey": {
    "temperature": 45,
    "precipitation": 1,
    "precipitation_duration": 2,
    "wind_description": 7,
    "cloud_cover": "2"
}
```

### Appendix 1-6: Bird-Srvy-POST-bulk-007 POST request JSON
Note: the below `orgMintedIdentifier` is created for `projectId`, `protocolId`, and `protocol version` equals 1. The `survey_metadata` is `"123"`.
```
{
    "collections": [
        {
            "orgMintedIdentifier": "{\"iv\":\"vF5ehIlUZASiXu8pNtkk0Q==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"ki/c++XiA8U=\",\"ct\":\"57gqA9Z12/C4PizMl3+okOeGpcAChxdXz2pYviuY7Wyx0a0zmlRlC/ECXyaI6EsZg4eu0gHt4B4bu6rIQpJVntlS+gE3IOcTgEZPSg4ag9xy2aHqGCls8JAKHqmdPc8kDzG3PdqUHEgNrDwJlz75x9tYl3nO9ko7OITR2Vs=\"}",
            "bird_survey": {
                <data>
            },
            "weather_survey": {
                "temperature": 45,
                "precipitation": 1,
                "precipitation_duration": 2,
                "wind_description": 7,
                "cloud_cover": 2
            },
            "bird_survey_observations": []
        }
    ]
}
```

### Appendix 1-7: Bird-Srvy-POST-bulk-008 POST request JSON
Note: the below `orgMintedIdentifier` is created for `projectId`, `protocolId`, and `protocol version` equals 1. The `survey_metadata` is `"123"`.

We only need to provide one `bird_survey_observations` entry for this test
```
{
    "collections": [
        {
            "orgMintedIdentifier": "{\"iv\":\"vF5ehIlUZASiXu8pNtkk0Q==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"ki/c++XiA8U=\",\"ct\":\"57gqA9Z12/C4PizMl3+okOeGpcAChxdXz2pYviuY7Wyx0a0zmlRlC/ECXyaI6EsZg4eu0gHt4B4bu6rIQpJVntlS+gE3IOcTgEZPSg4ag9xy2aHqGCls8JAKHqmdPc8kDzG3PdqUHEgNrDwJlz75x9tYl3nO9ko7OITR2Vs=\"}",
            "bird_survey": {
                "start_time": "2020-12-09T11:54:38.163Z",
                "end_date": "2020-12-10T11:54:38.163Z",
                "playback_used": true,
                "survey_type": 1,
                "survey_metadata": {
                    "survey_details": {
                        "survey_model": "bird-survey",
                        "time": "2022-09-14T01:39:21.635Z",
                        "uuid": "def92b55-b786-43ed-8e89-0e25b135454c",
                        "project_id": "1",
                        "protocol_id": "c1b38b0f-a888-4f28-871b-83da2ac1e533",
                        "protocol_version": "1"
                    },
                    "user_details": {
                        "role": "collector",
                        "email": "testuser@email.com",
                        "username": "testuser"
                    },
                    "provenance": {
                        "version_app": "0.0.1-xxxxx",
                        "version_core": "0.0.1-xxxxx",
                        "version_core_documentation": "0.0.1-xxxxx",
                        "version_org": "4.4-SNAPSHOT",
                        "system_app": "monitor",
                        "system_core": "Monitor FDCP API",
                        "system_org": "monitor"
                    }
                }
            },
            "weather_survey": {
                "temperature": 45,
                "precipitation": 1,
                "precipitation_duration": 2,
                "wind_description": 7,
                "cloud_cover": 2
            },
            "bird_survey_observations": [
                {
                    <data>
                }
            ]
        }
    ]
}
```

#### Bird-Srvy-POST-bulk-007 CASE A (invalid `species`):
```
"bird_survey_observations": [
    {
    "species": 123,
    "count": 412,
    "observation_type": 1,
    "activity_type": 8,
    "observation_location_type": 1,
    "breeding_type": 3
    }
]
```

#### Bird-Srvy-POST-bulk-007 CASE B (invalid `count`):
```
"bird_survey_observations": [
    {
    "species": "bird1",
    "count": "412",
    "observation_type": 1,
    "activity_type": 8,
    "observation_location_type": 1,
    "breeding_type": 3
    }
]
```

#### Bird-Srvy-POST-bulk-007 CASE C (invalid `observation_type`):
```
"bird_survey_observations": [
    {
    "species": "bird1",
    "count": 412,
    "observation_type": "1",
    "activity_type": 8,
    "observation_location_type": 1,
    "breeding_type": 3
    }
]
```

#### Bird-Srvy-POST-bulk-007 CASE D (invalid `activity_type`):
```
"bird_survey_observations": [
    {
    "species": "bird1",
    "count": 412,
    "observation_type": 1,
    "activity_type": "8",
    "observation_location_type": 1,
    "breeding_type": 3
    }
]
```

#### Bird-Srvy-POST-bulk-007 CASE E (invalid `observation_location_type`):
```
"bird_survey_observations": [
    {
    "species": "bird1",
    "count": 412,
    "observation_type": 1,
    "activity_type": 8,
    "observation_location_type": "1",
    "breeding_type": 3
    }
]
```

#### Bird-Srvy-POST-bulk-007 CASE F (invalid `breeding_type`):
```
"bird_survey_observations": [
    {
    "species": "bird1",
    "count": 412,
    "observation_type": 1,
    "activity_type": 8,
    "observation_location_type": 1,
    "breeding_type": "3"
    }
]
```

### Appendix 1-8: Bird-Srvy-POST-bulk-009 POST request JSON
```
{
    "collections": []
}
```

### Appendix 1-9: Bird-Srvy-POST-001 to 002, Bird-Srvy-PUT-001 to 005
```
{
  "start_time": "2020-12-09T11:54:38.163Z",
  "end_date": "2020-12-10T11:54:38.163Z",
  "playback_used": true,
  "survey_type": 1,
  "survey_metadata": {
        "survey_details": {
            "survey_model": "bird-survey",
            "time": "2022-09-14T01:39:21.635Z",
            "uuid": "def92b55-b786-43ed-8e89-0e25b135454c",
            "project_id": "1",
            "protocol_id": "c1b38b0f-a888-4f28-871b-83da2ac1e533",
            "protocol_version": "1"
        },
        "user_details": {
            "role": "collector",
            "email": "testuser@email.com",
            "username": "testuser"
        },
        "provenance": {
            "version_app": "0.0.1-xxxxx",
            "version_core": "0.0.1-xxxxx",
            "version_core_documentation": "0.0.1-xxxxx",
            "version_org": "4.4-SNAPSHOT",
            "system_app": "monitor",
            "system_core": "Monitor FDCP API",
            "system_org": "monitor"
        }
    }
}
```

## Appendix 2: Template:
|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | \<id> |
| Author            | \<author-1>, \<author-2>, etc. |
| Title             | \<title> |
| Description       | \<description> |
| Initial Condition | \<init-cond.> |
| Testing Steps     | \<steps> |
| Success Criteria  | \<criteria> |
| Data              | \<data> |

Test case ID's should be based on the content type and endpoint. For example, the Bird-survey (content type) GET /bird-surveys (endpoint) should look like: `Bird-Srvy-GET-XYZ`
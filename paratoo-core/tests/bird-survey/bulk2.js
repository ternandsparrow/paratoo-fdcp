const request = require('supertest')
const { cloneDeep } = require('lodash')
const { registerMockUris } = require('../helpers/mockAdapter')
const mock = require('./mock.json')

/*
  Test_ID: Bird-Srvy-POST-bulk-007
  Description: Test the ability of Core to allow requests to POST bird survey collections when the provided bird_survey_observations array is empty
*/
test('Bird-Srvy-POST-bulk-007: Success to POST a collection of bird surveys to Core when the provided bird_survey_observations array is empty', async () => {
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs1),
    mock.projectId2,
    mock.protocolId1,
    mock.orgMintedUuid2,
  )
  const requestBody = mock.body14
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier2

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      expect(data.status).toBe(200)
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-008
  Description:Failure to POST a collection of bird surveys to Core when the provided bird_survey_observations object is invalid
*/
test('Bird-Srvy-POST-bulk-008 - Case A (species) (Failure to POST a collection of bird surveys to Core when the provided bird_survey_observations object is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body15
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier2

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 008-A ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/bird-survey-observation/0/data/species must be string',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-008
  Description:Failure to POST a collection of bird surveys to Core when the provided bird_survey_observations object is invalid
*/
test('Bird-Srvy-POST-bulk-008 - Case B (count) (Failure to POST a collection of bird surveys to Core when the provided bird_survey_observations object is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body16
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier2

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 008-B ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/bird-survey-observation/0/data/count must be integer',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-008
  Description:Failure to POST a collection of bird surveys to Core when the provided bird_survey_observations object is invalid
*/
test('Bird-Srvy-POST-bulk-008 - Case C (observation_type) (Failure to POST a collection of bird surveys to Core when the provided bird_survey_observations object is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body17
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier2

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 008-C ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/bird-survey-observation/0/data/observation_type must be equal to one of the allowed values',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-008
  Description:Failure to POST a collection of bird surveys to Core when the provided bird_survey_observations object is invalid
*/
test('Bird-Srvy-POST-bulk-008 - Case D (activity_type) (Failure to POST a collection of bird surveys to Core when the provided bird_survey_observations object is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body18
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier2

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 008-D ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/bird-survey-observation/1/data/activity_type must be equal to one of the allowed values',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-008
  Description:Failure to POST a collection of bird surveys to Core when the provided bird_survey_observations object is invalid
*/
test('Bird-Srvy-POST-bulk-008 - Case E (observation_location_type) (Failure to POST a collection of bird surveys to Core when the provided bird_survey_observations object is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body19
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier2

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 008-E ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/bird-survey-observation/1/data/observation_location_type must be equal to one of the allowed values',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-008
  Description:Failure to POST a collection of bird surveys to Core when the provided bird_survey_observations object is invalid
*/
test('Bird-Srvy-POST-bulk-008 - Case F (breeding_type) (Failure to POST a collection of bird surveys to Core when the provided bird_survey_observations object is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body20
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier2

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 008-F ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/bird-survey-observation/1/data/breeding_type must be equal to one of the allowed values',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-009
  Description: Test the ability of Core to reject requests to POST bird survey collections when the collections array is empty
*/
test('Bird-Srvy-POST-bulk-009 Failure to POST a collection of bird surveys to Core when the collections array is empty', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send({
      data: {
        collections: [],
      },
    })
    .then((data) => {
      console.log('returned data 009 ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections must NOT have fewer than 1 items',
      )
    })
})

const request = require('supertest')
const { cloneDeep } = require('lodash')
const { registerMockUris } = require('../helpers/mockAdapter')
const mock = require('./mock.json')

/*
  Test_ID: Bird-Srvy-GET-001
  Description: Failure to find bird surveys when not authenticated
*/
test('Bird-Srvy-GET-001: Failure to find bird surveys when not authenticated', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get(mock.getEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .then((data) => {
      console.log('Bird-Srvy-GET-001 returned data: ', data.body)
      expect(data.body.error.status).toBe(401)
      expect(data.body.error.message).toBe(
        'User is not authenticated (logged in) - no auth token supplied',
      )
    })
})

/*
  Test_ID: Bird-Srvy-GET-002
  Description: Successfully find bird surveys when authenticated
*/
test('Bird-Srvy-GET-002: Test the ability of Core to perform bird-survey requests when the User is authenticated', async () => {
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs6),
    mock.projectId1,
    mock.protocolId1,
  )
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get(mock.getEndpoint2)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .then((data) => {
      console.log('Bird-Srvy-GET-002 returned data: ', data.body)
      expect(data.status).toBe(200)
      expect(data.text).toBeDefined()
    })
})

/*
  Test_ID: Bird-Srvy-GET-003
  Description: Test the ability of Core's getProtocolId() helper function to gracefully fail finding a protocol ID when the provided endpoint prefix could not be matched to a protocol ID
*/
test('Bird-Srvy-GET-003: Failure to find bird surveys when the protocol could not be found', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/foo')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .then((data) => {
      console.log('Bird-Srvy-GET-003 returned data: ', data.body)
      expect(data.body.error.status).toBe(404)
      expect(data.body.error.message).toBe('Not Found')
    })
})

/*
  Test_ID: Bird-Srvy-GET-007
  Description: Failure to find bird surveys when the supplied JWT is invalid
*/

test('Bird-Srvy-GET-007: Failure to find bird surveys when the supplied JWT is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs5))

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/bird-surveys')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', 'some-invalid-jwt')
    .then((data) => {
      console.log('Bird-Srvy-GET-007 returned data: ', data.body)
      expect(data.body.error.status).toBe(401)
      expect(data.body.error.message).toBe('Auth token is not valid')
    })
})

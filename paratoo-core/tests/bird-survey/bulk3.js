const request = require('supertest')
const { cloneDeep } = require('lodash')
const { registerMockUris } = require('../helpers/mockAdapter')
const mock = require('./mock.json')
/*
  Test_ID: Bird-Srvy-POST-bulk-010
  Description: Failure to POST a collection of bird surveys to Core when the bird survey protocol is not writable
*/
test('Bird-Srvy-POST-bulk-010 in BULK 3: Failure to POST a collection of bird surveys to Core when the bird survey protocol is not writable', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs3), mock.projectId1, mock.protocolId1)

  const requestBody = mock.body21
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier3
    
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 010 ', data.body)
      expect(data.body.error.status).toBe(401)
      expect(data.body.error.message).toBe(
        'User is NOT authorized to write project/protocol 1/c1b38b0f-a888-4f28-871b-83da2ac1e533',
      )
      //expect(axios.post).toHaveBeenCalledTimes(1) //GET called 1 time, POST call 0 times
      //expect(axios.post).toHaveBeenCalledTimes(0)
      //expect(axios.get).nthCalledWith(2,'abc', 'def')
    })
})

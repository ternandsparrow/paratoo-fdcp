//TODO write tests for generic bulk
const request = require('supertest')
const { cloneDeep } = require('lodash')
const { registerMockUris } = require('../helpers/mockAdapter')
const mock = require('./mock.json')

/*
  Test_ID: Bird-Srvy-POST-bulk-001
  Description: Test the ability of Core to POST a collection using the bulk() method
*/
test('Bird-Srvy-POST-bulk-001: should successfully POST a collection of bird-surveys to Core', async () => {
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs1),
    mock.projectId1,
    mock.protocolId1,
    mock.orgMintedUuid1,
  )

  const requestBody = mock.body1
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier1

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 001 ', data.body)
      expect(data.status).toBe(200)
      expect(data.status).toBe(200)
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-002
  Description: Test the ability of Core to reject requests to POST bird-survey collections when User is not authenticated
*/
test('Bird-Srvy-POST-bulk-002: Test the ability of Core to reject requests to POST bird-survey collections when User is not authenticated', async () => {
  const requestBody = mock.body2
  requestBody.data.collections[0].orgMintedIdentifier = 'foobar'

  //don't need to mock any requests, as it'll fail before then
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send(requestBody)
    .then((data) => {
      console.log('returned data 002 ', data.body)
      expect(data.body.error.status).toBe(401)
      expect(data.body.error.message).toBe(
        'User is not authenticated (logged in) - no auth token supplied',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-003
  Description: Test the ability of Core to reject requests to POST bird-survey collections when an identifier is not provided
*/
test('Bird-Srvy-POST-bulk-003: Test the ability of Core to reject requests to POST bird-survey collections when an identifier is not provided', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  //when
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(mock.body3)
    .then((data) => {
      console.log('returned data 003 ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/orgMintedIdentifier must NOT have fewer than 1 characters',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-004
  Description: Failure to POST a collection of bird surveys to Core when an orgMintedIdentifier could not be decoded
*/
test('Bird-Srvy-POST-bulk-004: Failure to POST a collection of bird surveys to Core when an orgMintedIdentifier could not be decoded', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(mock.body4)
    .then((data) => {
      console.log('returned data 004 ', data.body)
      expect(data.body.error.status).toBe(400)
      //want to test-to-fail when buffer is invalid but the output error contains a weird
      //character: `Unexpected token '~', \"~�\" is not valid JSON. Could not decode the
      //orgMintedIdentifier`.
      //so even if we `expect` this exact string it will cause the test to fail even
      //though they match. Workaround is to just check it includes part of the error
      //message
      expect(
        data.body.error.message.includes(
          'Could not decode the orgMintedIdentifier',
        ),
      ).toBe(true)
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-005 - case A
  Description: Failure to POST a collection of bird surveys to Core when the provided survey data is invalid
*/
test('Bird-Srvy-POST-bulk-005 - CASE A (Invalid start_date): Failure to POST a collection of bird surveys to Core when the provided survey data is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body5
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier1

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 005-A ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/bird-survey/data/start_date must match format "date-time"',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-005 - case B
  Description: Failure to POST a collection of bird surveys to Core when the provided survey data is invalid
*/
test('Bird-Srvy-POST-bulk-005 - CASE B (Invalid end_time): Failure to POST a collection of bird surveys to Core when the provided survey data is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body6
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier1

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 005-B ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/bird-survey/data/end_date must match format "date-time"',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-005 - case C
  Description: Failure to POST a collection of bird surveys to Core when the provided survey data is invalid
*/
test('Bird-Srvy-POST-bulk-005 - CASE C (Invalid playback_used): Failure to POST a collection of bird surveys to Core when the provided survey data is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body7
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier1

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 005-C ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/bird-survey/data/playback_used must be boolean',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-005 - case D
  Description: Failure to POST a collection of bird surveys to Core when the provided survey data is invalid
*/
test('Bird-Srvy-POST-bulk-005 - CASE D (Invalid survey_type): Failure to POST a collection of bird surveys to Core when the provided survey data is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body8
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier1

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 005-D ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/bird-survey/data/survey_type must be equal to one of the allowed values',
      )
    })
})

//TODO implement Bird-Srvy-POST-bulk-005 - case E

/*
  Test_ID: Bird-Srvy-POST-bulk-006
  Description: Failure to POST a collection of bird surveys to Core when the provided weather data is invalid
*/
test('Bird-Srvy-POST-bulk-006 - Case A (Temperature): Failure to POST a collection of bird surveys to Core when the provided weather data is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body9
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier1

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 006-A ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/weather-observation/data/temperature must be number',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-006
  Description: Failure to POST a collection of bird surveys to Core when the provided weather data is invalid
*/
test('Bird-Srvy-POST-bulk-006 - Case B (Precipitation): Failure to POST a collection of bird surveys to Core when the provided weather data is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body10
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier1

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 006-B ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/weather-observation/data/precipitation must be equal to one of the allowed values',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-006
  Description: Failure to POST a collection of bird surveys to Core when the provided weather data is invalid
*/
test('Bird-Srvy-POST-bulk-006 - Case C (precipitation_duration): Failure to POST a collection of bird surveys to Core when the provided weather data is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body11
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier1

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 006-C ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/weather-observation/data/precipitation_duration must be equal to one of the allowed values',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-006
  Description: Failure to POST a collection of bird surveys to Core when the provided weather data is invalid
*/
test('Bird-Srvy-POST-bulk-006 - Case D (wind_description): Failure to POST a collection of bird surveys to Core when the provided weather data is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body12
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier1

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 006-D ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/weather-observation/data/wind_description must be equal to one of the allowed values',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-bulk-006
  Description: Failure to POST a collection of bird surveys to Core when the provided weather data is invalid
*/
test('Bird-Srvy-POST-bulk-006 - Case E (cloud_cover): Failure to POST a collection of bird surveys to Core when the provided weather data is invalid', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  const requestBody = mock.body13
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier1

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 006-E ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        'Invalid: data/data/collections/0/weather-observation/data/cloud_cover must be equal to one of the allowed values',
      )
    })
})

// /////////////////////////////////////////////////// stale and redundant tests (#1947 & #1949) ///////////////////////////////


// const request = require('supertest')
// import axios from 'axios'

// jest.mock('axios')

// var test01_jwt =
//   'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjUxNDcwOTc5LCJleHAiOjE2NTQwNjI5Nzl9.LIDkASm6kbWfR9FbV5Bp48uPkEm3Dw4z0Q9oEoCggXc'

// var pdp_returned_val = {
//   data: {
//     isAuthorised: true,
//   },
// }

// var validation_true = { data: true }

// /*
//   Test_ID: Bird-Srvy-DELETE-001
//   Description: Test the ability of Core to successfully delete a bird survey entry
// */
// test('Bird-Srvy-DELETE-001: Test the ability of Core to successfully delete a bird survey entry', async () => {
//   axios.post.mockResolvedValueOnce(validation_true)
//   axios.get.mockResolvedValueOnce(pdp_returned_val)
//   await request(global.strapiServer) // app server is an instance of Class: http.Server
//     .delete('/api/bird-surveys/1')
//     .set('accept', 'application/json')
//     .set('Content-Type', 'application/json')
//     .set('authorization', test01_jwt)
//     .then((data) => {
//       console.log('Bird-Srvy-DELETE-001 returned data: ', data.body)
//       expect(data.status).toBe(200)
//       expect(data.text).toBeDefined()
//     })
// })

// /*
//   Test_ID: Bird-Srvy-DELETE-002
//   Description: Test the ability of Core to successfully delete a bird survey entry
// */
// test('Bird-Srvy-DELETE-002: Failure to delete an individual bird survey when the User is not authenticated', async () => {
//   axios.post.mockResolvedValueOnce(validation_true)
//   axios.get.mockResolvedValueOnce(pdp_returned_val)
//   await request(global.strapiServer) // app server is an instance of Class: http.Server
//     .delete(`/api/bird-surveys/2`)
//     .set('accept', 'application/json')
//     .set('Content-Type', 'application/json')
//     //.set('Authorization', 'Bearer ' + 'i_am_a_random_jwt')
//     .then((data) => {
//       console.log('Bird-Srvy-DELETE-002 returned data: ', data.body)
//       expect(data.body.error.status).toBe(401)
//       expect(data.body.error.message).toBe(
//         'User is not authenticated (logged in) - no auth token supplied',
//       )
//     })
// })

// /*
//   Test_ID: Bird-Srvy-DELETE-003
//   Description: Test the ability of Core to gracefully fail deleting an individual bird survey when the supplied ID does not correspond to a stored bird-survey
// */
// test('Bird-Srvy-DELETE-003: Failure to delete an individual bird survey when the supplied ID does not exist', async () => {
//   axios.post.mockResolvedValueOnce(validation_true)
//   axios.get.mockResolvedValueOnce(pdp_returned_val)
//   await request(global.strapiServer) // app server is an instance of Class: http.Server
//     .delete('/api/bird-surveys/100')
//     .set('accept', 'application/json')
//     .set('Content-Type', 'application/json')
//     .set('authorization', test01_jwt)
//     .then((data) => {
//       console.log('Bird-Srvy-DELETE-003 returned data: ', data.body)
//       expect(data.body.error.status).toBe(404)
//       expect(data.body.error.message).toBe('Not Found')
//     })
// })

// /*
//   Test_ID: Bird-Srvy-DELETE-004
//   Description: Test the ability of Core to gracefully fail deleting an individual bird survey when the supplied ID is not a valid parameter
// */
// test('Bird-Srvy-DELETE-004: Failure to delete an individual bird survey when the supplied ID is not a valid parameter', async () => {
//   axios.post.mockResolvedValueOnce(validation_true)
//   axios.get.mockResolvedValueOnce(pdp_returned_val)
//   await request(global.strapiServer) // app server is an instance of Class: http.Server
//     .delete('/api/bird-surveys/foo')
//     .set('accept', 'application/json')
//     .set('Content-Type', 'application/json')
//     .set('authorization', test01_jwt)
//     .then((data) => {
//       console.log('Bird-Srvy-DELETE-004 returned data: ', data.body)
//       expect(data.body.error.status).toBe(400)
//       expect(data.body.error.message).toBe(
//         `Invalid: query parameter 'id' with value 'foo' is not an integer`,
//       )
//     })
// })

// /*
//   Test_ID: Bird-Srvy-DELETE-005
//   Description: Test the ability of Core to gracefully fail deleting an individual bird survey when no ID is supplied (no path parameters)
// */
// test('Bird-Srvy-DELETE-005:Failure to delete an individual bird survey when no ID is supplied', async () => {
//   axios.post.mockResolvedValueOnce(validation_true)
//   axios.get.mockResolvedValueOnce(pdp_returned_val)
//   await request(global.strapiServer) // app server is an instance of Class: http.Server
//     .delete(`/api/bird-surveys?auth=${test01_jwt}`)
//     .set('accept', 'application/json')
//     .set('Content-Type', 'application/json')
//     .then((data) => {
//       console.log('Bird-Srvy-DELETE-005 returned data: ', data.body)
//       expect(data.status).toBe(405)
//       expect(data.text).toBe('Method Not Allowed')
//     })
// })

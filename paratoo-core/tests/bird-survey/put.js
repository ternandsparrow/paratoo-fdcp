/////////////////////////////////////////////////// stale and redundant tests (#1947 & #1949) ///////////////////////////////


const request = require('supertest')
import axios from 'axios'

jest.mock('axios')
const mock_pdp_true = { data: { isAuthorised: 'true' } }

var test01_jwt = 'Bearer Iamarandomjwt'
var validation_true = { data: true }
/*
  Test_ID: Bird-Srvy-PUT-001
  Description: Test the ability of Core to successfully update a bird survey entry
*/
test('Bird-Srvy-PUT-001: Successful update of an individual bird survey', async () => {
  //Mock POST /validate-token
  axios.post.mockResolvedValueOnce(validation_true) // to  POST /validate-token
  axios.get.mockResolvedValueOnce(mock_pdp_true) //to GET /pdp

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .put('/api/bird-surveys/1')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', test01_jwt)
    .send({
      data: {
        start_date: '2020-12-10T11:54:38.163Z',
        end_date: '2020-12-11T11:54:38.163Z',
        playback_used: true,
        survey_type: '500',
        plot_type: 'F',
        number_of_surveyors: 2,
        surveyor_names: [{ observer_name: 'John Doe' }, { observer_name: 'Jane Smith' }],
        "survey_metadata": {
          "survey_details": {
            "survey_model": "bird-survey",
            "time": "2022-09-14T01:39:21.635Z",
            "uuid": "def92b55-b786-43ed-8e89-0e25b135454c",
            "project_id": "1",
            "protocol_id": "068d17e8-e042-ae42-1e42-cff4006e64b0",
            "protocol_version": "1"
          },
          "provenance": {
            "version_app": "0.0.1-xxxxx",
            "version_core": "0.0.1-xxxxx",
            "version_core_documentation": "0.0.1-xxxxx",
            "version_org": "4.4-SNAPSHOT",
            "system_app": "monitor",
            "system_core": "Monitor FDCP API",
            "system_org": "monitor"
          }
        },
      },
    })
    .then((data) => {
      console.log('Bird-Srvy-PUT-001 returned data: ', data.body)
      expect(data.status).toBe(200)
      expect(data.text).toBeDefined()
    })
})

/*
  Test_ID: Bird-Srvy-PUT-002
  Description: Test the ability of Core to gracefully fail updating an individual bird survey when the User is not authenticated
*/
test('Bird-Srvy-PUT-002: Failure to update an individual bird survey when the User is not authenticated', async () => {
  //Mock POST /validate-token
  axios.post.mockResolvedValueOnce(validation_true) // to  POST /validate-token
  axios.get.mockResolvedValueOnce(mock_pdp_true) //to GET /pdp

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .put(`/api/bird-surveys/2`)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', "")
    .send({
      data: {
        start_date: '2020-12-10T11:54:38.163Z',
        end_date: '2020-12-11T11:54:38.163Z',
        playback_used: true,
        survey_type: '500',
        plot_type: 'F',
        number_of_surveyors: 2,
        surveyor_names: [{ observer_name: 'John Doe' }, { observer_name: 'Jane Smith' }],
        "survey_metadata": {
          "survey_details": {
            "survey_model": "bird-survey",
            "time": "2022-09-14T01:39:21.635Z",
            "uuid": "def92b55-b786-43ed-8e89-0e25b135454c",
            "project_id": "1",
            "protocol_id": "068d17e8-e042-ae42-1e42-cff4006e64b0",
            "protocol_version": "1"
          },
          "provenance": {
            "version_app": "0.0.1-xxxxx",
            "version_core": "0.0.1-xxxxx",
            "version_core_documentation": "0.0.1-xxxxx",
            "version_org": "4.4-SNAPSHOT",
            "system_app": "monitor",
            "system_core": "Monitor FDCP API",
            "system_org": "monitor"
          }
        },
      },
    })
    .then((data) => {
      console.log('Bird-Srvy-PUT-002 returned data: ', data.body)
      expect(data.body.error.status).toBe(401)
      expect(data.body.error.message).toBe(
        'User is not authenticated (logged in) - no auth token supplied',
      )
    })
})

/*
  Test_ID: Bird-Srvy-PUT-003
  Description: Test the ability of Core to gracefully fail updating an individual bird survey when the supplied ID does not correspond to a stored bird survey
*/
test('Bird-Srvy-PUT-003: Failure to update an individual bird survey when the supplied ID does not exist', async () => {
  //Mock POST /validate-token
  axios.post.mockResolvedValueOnce(validation_true) // to  POST /validate-token
  axios.get.mockResolvedValueOnce(mock_pdp_true) //to GET /pdp
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .put('/api/bird-surveys/100')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', test01_jwt)
    .send({
      data: {
        start_date: '2020-12-10T11:54:38.163Z',
        end_date: '2020-12-11T11:54:38.163Z',
        playback_used: true,
        plot_type: 'F',
        survey_type: '500',
        number_of_surveyors: 2,
        surveyor_names: [{ observer_name: 'John Doe' }, { observer_name: 'Jane Smith' }],
        "survey_metadata": {
          "survey_details": {
            "survey_model": "bird-survey",
            "time": "2022-09-14T01:39:21.635Z",
            "uuid": "def92b55-b786-43ed-8e89-0e25b135454c",
            "project_id": "1",
            "protocol_id": "068d17e8-e042-ae42-1e42-cff4006e64b0",
            "protocol_version": "1"
          },
          "provenance": {
            "version_app": "0.0.1-xxxxx",
            "version_core": "0.0.1-xxxxx",
            "version_core_documentation": "0.0.1-xxxxx",
            "version_org": "4.4-SNAPSHOT",
            "system_app": "monitor",
            "system_core": "Monitor FDCP API",
            "system_org": "monitor"
          }
        },
      },
    })
    .then((data) => {
      console.log('Bird-Srvy-PUT-003 returned data: ', data.body)
      expect(data.body.error.status).toBe(404)
      expect(data.body.error.message).toBe('Not Found')
    })
})

/*
  Test_ID: Bird-Srvy-PUT-004
  Description: Test the ability of Core to gracefully fail updating an individual bird survey when the supplied ID is not a valid parameter
*/
test('Bird-Srvy-PUT-004: Failure to update an individual bird survey when the supplied ID is not a valid parameter', async () => {
  //Mock POST /validate-token
  axios.post.mockResolvedValueOnce(validation_true) // to  POST /validate-token
  axios.get.mockResolvedValueOnce(mock_pdp_true) //to GET /pdp
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .put('/api/bird-surveys/foo')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', test01_jwt)
    .send({
      data: {
        start_date: '2020-12-10T11:54:38.163Z',
        end_date: '2020-12-11T11:54:38.163Z',
        playback_used: true,
        survey_type: '500',
        number_of_surveyors: 2,
        surveyor_names: [{ observer_name: 'John Doe' }, { observer_name: 'Jane Smith' }],
        survey_details: '125',
      },
    })
    .then((data) => {
      console.log('Bird-Srvy-PUT-004 returned data: ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        `Invalid: query parameter 'id' with value 'foo' is not an integer`,
      )
    })
})

/*
  Test_ID: Bird-Srvy-PUT-005
  Description: Test the ability of Core to gracefully fail updating an individual bird survey when no ID is supplied (no path parameters)
*/
test('Bird-Srvy-PUT-005: Failure to update an individual bird survey when no ID is supplied', async () => {
  //Mock POST /validate-token
  axios.post.mockResolvedValueOnce(validation_true) // to  POST /validate-token
  axios.get.mockResolvedValueOnce(mock_pdp_true) //to GET /pdp
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .put('/api/bird-surveys')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', test01_jwt)
    .send({
      data: {
        start_date: '2020-12-10T11:54:38.163Z',
        end_date: '2020-12-11T11:54:38.163Z',
        playback_used: true,
        survey_type: '500',
        number_of_surveyors: 2,
        surveyor_names: [{ observer_name: 'John Doe' }, { observer_name: 'Jane Smith' }],
        survey_details: '125',
      },
    })
    .then((data) => {
      console.log('Bird-Srvy-PUT-005 returned data: ', data.body)
      expect(data.status).toBe(405)
      expect(data.text).toBe('Method Not Allowed')
    })
})

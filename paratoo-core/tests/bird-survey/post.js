const request = require('supertest')
const { cloneDeep } = require('lodash')
const { registerMockUris } = require('../helpers/mockAdapter')
const mock = require('./mock.json')

/*
  Test_ID: Bird-Srvy-POST-001
  Description: Test the ability of Core to successfully create a bird survey entry
*/
test('Bird-Srvy-POST-001: Successful submission of an individual bird survey', async () => {
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs3),
    mock.projectId1,
    mock.protocolId1,
  )

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.postEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(mock.body23)
    .then((data) => {
      console.log('Bird-Srvy-POST-001 returned data: ', data.body)
      expect(data.status).toBe(200)
    })
})

/*
  Test_ID: Bird-Srvy-POST-002
  Description: Test the ability of Core to gracefully fail submitting an individual bird survey when the User is not authenticated (not logged in)
*/
test('Bird-Srvy-POST-002: Failure to submit an individual bird survey when the User is not authenticated', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.postEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send(mock.body24)
    .then((data) => {
      console.log('Bird-Srvy-POST-002 returned data: ', data.body)
      expect(data.body.error.status).toBe(401)
      expect(data.body.error.message).toBe(
        'User is not authenticated (logged in) - no auth token supplied',
      )
    })
})

/*
  Test_ID: Bird-Srvy-POST-003
  Description: Test the ability of Core to gracefully fail submitting an individual bird survey when the supplied request body is empty JSON
*/
test('Bird-Srvy-POST-003: Failure to submit an individual bird survey when the supplied request body is empty JSON', async () => {
  // register apis with axios mock adapter
  registerMockUris(cloneDeep(mock.apiConfigs2))

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.postEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .then((data) => {
      console.log('Bird-Srvy-POST-003 returned data: ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe(
        "Invalid: data must have required property 'data'",
      )
    })
})

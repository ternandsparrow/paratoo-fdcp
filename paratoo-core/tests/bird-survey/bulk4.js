const request = require('supertest')
const { cloneDeep } = require('lodash')
const { registerMockUris } = require('../helpers/mockAdapter')
const mock = require('./mock.json')

// This is a new collection
/*
  Test_ID: Bird-Srvy-POST-bulk-011
  Description: Test the ability of Core to reject requests to POST bird survey collections when the collectionIdentifier already exists, i.e., the collection has already been POSTed
*/
test('Bird-Srvy-POST-bulk-011: Failure to POST a collection of bird surveys to Core when the collectionIdentifier already exists', async () => {
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs4),
    mock.projectId1,
    mock.protocolId1,
    mock.orgMintedUuid3,
  )

  const requestBody = mock.body22
  requestBody.data.collections[0].orgMintedIdentifier =
    mock.orgEncodedIdentifier4

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 011 ', data.body)
      expect(data.body.error.status).toBe(400)
      expect(data.body.error.message).toBe('Collection has already been submitted; the collection identifier already exists')
    })
})

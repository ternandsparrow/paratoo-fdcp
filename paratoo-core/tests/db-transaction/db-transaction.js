const request = require('supertest')
const { cloneDeep } = require('lodash')
const mock = require('./mock.json')
const { v4: uuid } = require('uuid')

const { createSurveyId, createOrgIdentifier } = require('../helpers/mocker')
const { registerMockUris } = require('../helpers/mockAdapter')

async function checkSubmittedData(isSubmitted = true, data, model) {
  if (Array.isArray(data)) {
    for (const obs of data) {
      isSubmitted = await checkSubmittedData(isSubmitted, obs, model)
      if (!isSubmitted) break
    }
  } else {
    const count = await strapi.db.query(`api::${model}.${model}`).count(data.id)
    isSubmitted = count === 1
  }
  return isSubmitted
}

async function verifyFailedTransaction(
  payload,
  expectedStatusCode = 400,
  msgToExpect,
) {
  const resp = await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(payload)
  console.log('resp.body:', JSON.stringify(resp.body, null, 2))
  expect(resp.status).toBe(expectedStatusCode)
  expect(resp.body.error.message).toBe(msgToExpect)

  let submitted = false
  const models = [
    'dev-sandbox-bulk-survey',
    'dev-sandbox-general-entry',
    'dev-sandbox-general-multi-entry',
    'dev-sandbox-bulk-observation',
  ]
  for (const model of models) {
    const api = `api::${model}.${model}`
    const count = await strapi.db.query(api).count('*')
    submitted = count > 1
  }
  expect(submitted).toBe(false)
}

//we using dev-sand-box-bulk as it can cover most the cases we need
it('db-transaction-01: Test the ability strapi transaction be successful', async () => {
  const surveyMetadata =
    mock.body1.data.collections[0]['dev-sandbox-bulk-survey'].data
      .survey_metadata

  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs1),
    surveyMetadata.survey_details.project_id,
    surveyMetadata.survey_details.protocol_id,
    surveyMetadata.orgMintedUUID,
  )

  const resp = await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(mock.body1)

  let isSubmitted = true
  const respData = resp.body[0]
  for (const model in respData) {
    isSubmitted = await checkSubmittedData(isSubmitted, respData[model], model)
    if (!isSubmitted) break
  }
  expect(isSubmitted).toBe(true)
})

let childObsUniqueFieldVal
it('db-transaction-02: Test the ability strapi transaction to reroll when failed with duplicated unique field in observation', async () => {
  // we need new survey metadata with new orgMintedUuid to mock endpoint /status/<org_uuid>
  const surveyMetadata = createSurveyId(
    'dev-sandbox-bulk-survey', // model
    'aae4dbd8-845a-406e-b682-ef01c3497711', // protocol id
    '1', // project id
  )
  // org identifier with newly created survey metadata
  mock.body1.data.collections[0].orgMintedIdentifier = createOrgIdentifier(
    cloneDeep(surveyMetadata),
    new Date(Date.now()), // current time
  )
  mock.body1.data.collections[0][
    'dev-sandbox-bulk-survey'
  ].data.survey_metadata = cloneDeep(surveyMetadata)

  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs3),
    surveyMetadata.survey_details.project_id,
    surveyMetadata.survey_details.protocol_id,
    surveyMetadata.orgMintedUUID,
  )

  mock.body1.data.collections[0][
    'dev-sandbox-bulk-survey'
  ].data.survey_unique_field = uuid()
  childObsUniqueFieldVal =
    mock.body1.data.collections[0]['dev-sandbox-bulk-observation'][0].data
      .child_ob.data.child_obs_unique_field
  mock.body1.data.collections[0][
    'dev-sandbox-bulk-observation'
  ][0].data.child_ob.data.child_obs_unique_field = uuid()

  const msgToExpect = `Observation at index=0 has failure: Fields are not unique: obs_unique_field`
  await verifyFailedTransaction(cloneDeep(mock.body1), 400, msgToExpect)
})

it('db-transaction-03: Test the ability strapi transaction to reroll when failed with duplicated unique text in child observation', async () => {
  // we need new survey metadata with new orgMintedUuid to mock endpoint /status/<org_uuid>
  const surveyMetadata = createSurveyId(
    'dev-sandbox-bulk-survey', // model
    'aae4dbd8-845a-406e-b682-ef01c3497711', // protocol id
    '1', // project id
  )
  // org identifier with newly created survey metadata
  mock.body1.data.collections[0].orgMintedIdentifier = createOrgIdentifier(
    cloneDeep(surveyMetadata),
    new Date(Date.now()), // current time
  )
  mock.body1.data.collections[0][
    'dev-sandbox-bulk-survey'
  ].data.survey_metadata = cloneDeep(surveyMetadata)

  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs3),
    surveyMetadata.survey_details.project_id,
    surveyMetadata.survey_details.protocol_id,
    surveyMetadata.orgMintedUUID,
  )

  mock.body1.data.collections[0][
    'dev-sandbox-bulk-observation'
  ][0].data.obs_unique_field = uuid()

  mock.body1.data.collections[0][
    'dev-sandbox-bulk-observation'
  ][0].data.child_ob.data.child_obs_unique_field = childObsUniqueFieldVal

  const msgToExpect = `Child observation at index=0 for observation at index=0 has failure: Fields are not unique: child_obs_unique_field`
  await verifyFailedTransaction(cloneDeep(mock.body1), 400, msgToExpect)
})

it('db-transaction-04: Test the ability strapi transaction to reroll when failed with duplicated unique field in single general entry', async () => {
  // we need new survey metadata with new orgMintedUuid to mock endpoint /status/<org_uuid>
  const surveyMetadata = createSurveyId(
    'dev-sandbox-bulk-survey', // model
    'aae4dbd8-845a-406e-b682-ef01c3497711', // protocol id
    '1', // project id
  )
  // org identifier with newly created survey metadata
  mock.body1.data.collections[0].orgMintedIdentifier = createOrgIdentifier(
    cloneDeep(surveyMetadata),
    new Date(Date.now()), // current time
  )
  mock.body1.data.collections[0][
    'dev-sandbox-bulk-survey'
  ].data.survey_metadata = cloneDeep(surveyMetadata)

  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs3),
    surveyMetadata.survey_details.project_id,
    surveyMetadata.survey_details.protocol_id,
    surveyMetadata.orgMintedUUID,
  )

  mock.body1.data.collections[0][
    'dev-sandbox-bulk-observation'
  ][0].data.child_ob.data.child_obs_unique_field = uuid()

  const msgToExpect = `General entry with model name 'dev-sandbox-general-entry' at index=0 has failure: Fields are not unique: general_unique_field. General entry with model name 'dev-sandbox-general-multi-entry' at index=0 has failure: Fields are not unique: general_multi_entries_unique_field`
  await verifyFailedTransaction(cloneDeep(mock.body1), 400, msgToExpect)
})

it('db-transaction-05: Test the ability strapi transaction to reroll when failed with duplicated unique field in multiple general entries', async () => {
  // we need new survey metadata with new orgMintedUuid to mock endpoint /status/<org_uuid>
  const surveyMetadata = createSurveyId(
    'dev-sandbox-bulk-survey', // model
    'aae4dbd8-845a-406e-b682-ef01c3497711', // protocol id
    '1', // project id
  )
  // org identifier with newly created survey metadata
  mock.body1.data.collections[0].orgMintedIdentifier = createOrgIdentifier(
    cloneDeep(surveyMetadata),
    new Date(Date.now()), // current time
  )
  mock.body1.data.collections[0][
    'dev-sandbox-bulk-survey'
  ].data.survey_metadata = cloneDeep(surveyMetadata)

  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs3),
    surveyMetadata.survey_details.project_id,
    surveyMetadata.survey_details.protocol_id,
    surveyMetadata.orgMintedUUID,
  )

  mock.body1.data.collections[0][
    'dev-sandbox-general-entry'
  ].data.general_unique_field = uuid()

  const msgToExpect = `General entry with model name 'dev-sandbox-general-multi-entry' at index=0 has failure: Fields are not unique: general_multi_entries_unique_field`
  await verifyFailedTransaction(cloneDeep(mock.body1), 400, msgToExpect)
})

it('db-transaction-06: Test the ability strapi transaction to reroll when failed to submit with failed org', async () => {
  // we need new survey metadata with new orgMintedUuid to mock endpoint /status/<org_uuid>
  const surveyMetadata = createSurveyId(
    'dev-sandbox-bulk-survey', // model
    'aae4dbd8-845a-406e-b682-ef01c3497711', // protocol id
    '1', // project id
  )
  // org identifier with newly created survey metadata
  mock.body1.data.collections[0].orgMintedIdentifier = createOrgIdentifier(
    cloneDeep(surveyMetadata),
    new Date(Date.now()), // current time
  )
  mock.body1.data.collections[0][
    'dev-sandbox-bulk-survey'
  ].data.survey_metadata = cloneDeep(surveyMetadata)

  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs3),
    surveyMetadata.survey_details.project_id,
    surveyMetadata.survey_details.protocol_id,
    surveyMetadata.orgMintedUUID,
  )

  mock.body1.data.collections[0][
    'dev-sandbox-general-multi-entry'
  ][0].data.general_multi_entries_unique_field = uuid()

  mock.body1.data.collections[0][
    'dev-sandbox-bulk-observation'
  ][0].data.child_ob.data.child_obs_unique_field = uuid()

  const msgToExpect = 'Internal Server Error'
  await verifyFailedTransaction(cloneDeep(mock.body1), 500, msgToExpect)
})

const { setTimeout } = require('timers/promises')
test('create a push message which will be published and sent in next 5 seconds', async () => {
  const current = new Date()
  const scheduledAt = new Date(current.getTime() + 5000).toISOString()
  const data = {
    title: 'Hello future me in 5 secs',
    message: 'This should work',
    type: 'positive',
    scheduledAt,
  }
  const uid = 'api::push-notification.push-notification'
  const entry = await strapi.entityService.create(uid, { data })
  await setTimeout(5000)
  const record = await strapi.entityService.findOne(uid, entry.id)

  expect(record.scheduledAt).toBe(scheduledAt)
  expect(record.isSent).toBe(true)
})

test('push message should be sent immediately when it got published even the scheduled is not met', async () => {
  const current = new Date()
  const scheduledAt = new Date(
    current.getTime() + 5 * 24 * 60 * 60 * 1000,
  ).toISOString()

  const data = {
    title: 'Hello future me in 5 days',
    message: 'This should work',
    type: 'positive',
    scheduledAt,
  }
  const uid = 'api::push-notification.push-notification'
  const entry = await strapi.entityService.create(uid, { data })

  await strapi.entityService.update(uid, entry.id, {
    data: {
      publishedAt: new Date().toISOString(),
    },
  })
  await setTimeout(5000)
  const record = await strapi.entityService.findOne(uid, entry.id)

  expect(record.isSent).toBe(true)
})

test('push message should be sent immediately when if the schedule time is outdated', async () => {
  const current = new Date()
  const scheduledAt = new Date(
    current.getTime() - 5 * 24 * 60 * 60 * 1000,
  ).toISOString()

  const data = {
    title: 'Hello from the past',
    message: 'This should work',
    type: 'positive',
    scheduledAt,
  }
  const uid = 'api::push-notification.push-notification'
  const entry = await strapi.entityService.create(uid, { data })

  await setTimeout(5000)
  const record = await strapi.entityService.findOne(uid, entry.id)
  expect(record.isSent).toBe(true)
})


/* Populate the bird-survey */
it('populate a bird-survey', async () => {
    await request(strapi.server) // app server is an instance of Class: http.Server
      .post('/bird-surveys')
      .send({
        "start_time": "2021-07-29T14:30:00.000Z",
        "end_time": "2021-07-29T14:30:00.000Z"
      })
      .expect(200) // Expect response http code 200
      .then(data => {
        data.text = JSON.parse(data.text) 
        expect(data.text.start_time).toBe("2021-07-29T14:30:00.000Z"); // expect the response text
      
    });
  });
  
  
  /*
    Test_ID: Bird-Srvy-001
    Title: Failure to find bird-surveys when not authenticated
    Description: Test the ability of Core to reject find requests when User is not authenticated
  */
  
  it('shoud return 403 if user is not authenticated', async () => {
    await request(strapi.server) // app server is an instance of Class: http.Server
    .get('/bird-surveys')
    .expect(406)
    .then(data => {
      console.log(data)
    })
  
  });
  
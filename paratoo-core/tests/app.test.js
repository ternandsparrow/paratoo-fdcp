const {
  setupStrapi,
  restartStrapi,
  cleanupStrapi,
} = require('./helpers/strapi')

/** this code is called once before any test is called */
beforeAll(async () => {
  await setupStrapi()
})
/** this code is called once before all the tested are finished */
// afterAll(teardownStrapi)

it('strapi instance is defined', () => {
  expect(strapi).toBeDefined()
})
// it('be able to restart strapi in core', async () => {
//   const instance = await restartStrapi()
//   expect(instance).toBeDefined()
// })

afterEach(async () => {
  jest.resetAllMocks()
  // await cleanupStrapi()
})
afterAll(async () => {
  await cleanupStrapi()
})

// /* BULK requests */
require('./bird-survey/bulk.js')
require('./bird-survey/bulk2.js')
require('./bird-survey/bulk3.js')
require('./bird-survey/bulk4.js')
require('./cover-point-intercept/bulk.js')
require('./soils/bulk.js')
require('./floristics/bulk.js')


// /* GET requests */
require('./bird-survey/get.js')


// /* POST requests */
require('./bird-survey/post.js')


// /* VALIDATORS */
require('./validator/ajv_validator.js')
require('./validator/query_params.js')
require('./validator/custom_rules_validator.js')
require('./validator/metadataProtocolIdValidator.js')
require('./validator/unique_data.js')


// /* POLICIES & TRANSACTIONS */
require('./find-controller-with-pdp-filter/pdp-data-filter.js')
require('./db-transaction/db-transaction.js')


// /* ORG requests */
require('./org-api-requests/pagination')
require('./org-api-requests/bulk')


// /* CACHE */
require('./use-cache')

// /* PUSH NOTIFICATIONS */
require('./push-message')

// stale and redundant tests (#1947 & #1949)
// require('./bird-survey/put.js')
// require('./bird-survey/delete.js')
// require('./floristics/get.js') // don't need any more see #1036
// require('./vegtation-mapping/DEL_vegetation_mapping.js')
// require('./invertebrate')

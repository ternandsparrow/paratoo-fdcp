const request = require('supertest')
const { cloneDeep } = require('lodash')
const { registerMockUris } = require('../helpers/mockAdapter')
const mock = require('./mock.json')

/*
  Description: Test the ability of Core to POST a collection of cover full survey using the bulk() method 
*/
test('Cover-point-intercept-full-POST-bulk-001: should successfully POST a collection of cover-point-intercept-full-surveys to Core', async () => {
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs1),
    mock.projectId1,
    mock.protocolId1,
    mock.orgMintedUuid1,
  )

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(mock.body1)
    .then((data) => {
      console.log('returned data 001 ', data.body)
      expect(data.status).toBe(200)
      expect(data.status).toBe(200)
    })
})

/*
  Description: Test the ability of Core to POST a collection of cover lite survey using the bulk() method 
*/
test('Cover-point-intercept-lite-POST-bulk-001: should successfully POST a collection of cover-point-intercept-lite-surveys to Core', async () => {
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs1),
    mock.projectId1,
    mock.protocolId2,
    mock.orgMintedUuid2,
  )

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(mock.body2)
    .then((data) => {
      console.log('returned data 001 ', data.body)
      expect(data.status).toBe(200)
      expect(data.status).toBe(200)
    })
})

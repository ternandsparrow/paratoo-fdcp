/////////////////////////////////////////////////// stale and redundant tests (#1947 & #1949) ///////////////////////////////


// const { cloneDeep } = require('lodash')
// const request = require('supertest')
// import axios from 'axios'
// const endpoints = require('./endpoints')
// jest.mock('axios')
// const mock_pdp_true = { data: { isAuthorised: 'true' } }
// const post_collection_true = { success: true }
// const mock_upload_blob = [
//   {
//     id: 1,
//     name: '93020065.webp',
//     alternativeText: null,
//     caption: null,
//     width: 640,
//     height: 480,
//     formats: {
//       small: {
//         ext: '.webp',
//         url: '/uploads/small_93020065_55fbaa0e07.webp',
//         hash: 'small_93020065_55fbaa0e07',
//         mime: 'image/webp',
//         name: 'small_93020065.webp',
//         path: null,
//         size: 7.74,
//         width: 500,
//         height: 375,
//       },
//       thumbnail: {
//         ext: '.webp',
//         url: '/uploads/thumbnail_93020065_55fbaa0e07.webp',
//         hash: 'thumbnail_93020065_55fbaa0e07',
//         mime: 'image/webp',
//         name: 'thumbnail_93020065.webp',
//         path: null,
//         size: 2.77,
//         width: 208,
//         height: 156,
//       },
//     },
//     hash: '93020065_55fbaa0e07',
//     ext: '.webp',
//     mime: 'image/webp',
//     size: 11.99,
//     url: '/uploads/93020065_55fbaa0e07.webp',
//     previewUrl: null,
//     provider: 'local',
//     provider_metadata: null,
//     createdAt: '2023-01-04T02:32:12.369Z',
//     updatedAt: '2023-01-04T02:32:12.369Z',
//   },
// ]
// const test01_jwt =
//   'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjUxNDcwOTc5LCJleHAiOjE2NTQwNjI5Nzl9.LIDkASm6kbWfR9FbV5Bp48uPkEm3Dw4z0Q9oEoCggXd'
// const validation_true = { data: true }
// /*
//   Test_ID: Invertebrate-POST-bulk-001
//   Description: Test the ability of Core to POST a collection using the bulk() method 
// */
// describe('Invertebrate-POST-bulk-001: should successfully POST a collection of invertebrate protocols to Core', () => {
//   test.each(Object.keys(endpoints))(
//     'ability to do POST a collection of of %s when the User is authenticated',
//     async (endpoint) => {
//       //Mock POST /validate-token
//       axios.post.mockResolvedValueOnce(validation_true)
//       //Axios Get mock function was called 2 times in this file
//       axios.get.mockResolvedValueOnce(mock_pdp_true) //1
//       axios.get.mockResolvedValueOnce(mock_pdp_true) //2
//       //Axios Post mock function was called 1 time in this file
//       axios.post.mockResolvedValueOnce(post_collection_true) //1
//       // mock upload photo response
//       axios.post.mockResolvedValueOnce(mock_upload_blob)
//       // mock validate org minted
//       axios.post.mockResolvedValueOnce(
//         'survey_metadata in the survey and orgMintedIdentifier match',
//       )
//       const data = cloneDeep(endpoints[endpoint].data)
//       await request(global.strapiServer) // app server is an instance of Class: http.Server
//         .post(`/api/${endpoint}/bulk`)
//         .set('accept', 'application/json')
//         .set('Content-Type', 'application/json')
//         .set('authorization', test01_jwt)
//         .send({
//           data: data,
//         })
//         .then((data) => {
//           // console.log('returned data ', data.body)
//           expect(data.status).toBe(200)
//           expect(data.status).toBe(200)
//         })
//     },
//   )
// })

// /*
//   Test_ID: Invertebrate-POST-bulk-002
//   Description: Test the ability of Core to reject requests to POST invertebrate collections when User is not authenticated
// */
// describe('Invertebrate-POST-bulk-002: Test the ability of Core to reject requests to POST invertebrate collections when User is not authenticated', () => {
//   test.each(Object.keys(endpoints))(
//     ' ability of Core to reject POST requests to  %s collection when User is not authenticated',
//     async (endpoint) => {
//       //Mock POST /validate-token
//       axios.post.mockResolvedValueOnce(validation_true)
//       //Axios Get mock function was called 2 times in this file
//       axios.get.mockResolvedValueOnce(mock_pdp_true) //1
//       axios.get.mockResolvedValueOnce(mock_pdp_true) //2
//       //Axios Post mock function was called 1 time in this file
//       axios.post.mockResolvedValueOnce(post_collection_true) //1
//       // mock upload photo response
//       axios.post.mockResolvedValueOnce(mock_upload_blob)
//       const data = cloneDeep(endpoints[endpoint].data)
//       await request(global.strapiServer) // app server is an instance of Class: http.Server
//         .post(`/api/${endpoint}/bulk`)
//         .set('accept', 'application/json')
//         .set('Content-Type', 'application/json')
//         .set('authorization', test01_jwt)
//         .send({
//           data: data,
//         })
//         .then((data) => {
//           // console.log('returned data 002 ', data.body)
//           expect(data.body.error.status).toBe(401)
//           expect(data.body.error.message).toBe(
//             'User is not authenticated (logged in) - no auth token supplied',
//           )
//         })
//     },
//   )
// })

// /*
//   Test_ID: Invertebrate-POST-bulk-003
//   Description: Test the ability of Core to reject requests to POST invertebrate collections when an identifier is not provided
// */
// describe('Invertebrate-POST-bulk-003: Test the ability of Core to reject requests to POST invertebrate collections when an identifier is not provided', () => {
//   test.each(Object.keys(endpoints))(
//     ' ability of Core to reject POST requests to  %s collection when an minted identifier is not provided',
//     async (endpoint) => {
//       //Mock POST /validate-token
//       axios.post.mockResolvedValueOnce(validation_true)
//       //Axios Get mock function was called 2 times in this file
//       axios.get.mockResolvedValueOnce(mock_pdp_true) //1
//       axios.get.mockResolvedValueOnce(mock_pdp_true) //2
//       //Axios Post mock function was called 1 time in this file
//       axios.post.mockResolvedValueOnce(post_collection_true) //1
//       // mock upload photo response
//       axios.post.mockResolvedValueOnce(mock_upload_blob)
//       const data = cloneDeep(endpoints[endpoint].data)
//       delete data.collections[0].orgMintedIdentifier
//       await request(global.strapiServer) // app server is an instance of Class: http.Server
//         .post(`/api/${endpoint}/bulk`)
//         .set('accept', 'application/json')
//         .set('Content-Type', 'application/json')
//         .set('authorization', test01_jwt)
//         .send({
//           data: data,
//         })
//         .then((data) => {
//           // console.log('returned data 003 ', data.body)
//           expect(data.body.error.status).toBe(400)
//           expect(data.body.error.message).toBe(
//             "Invalid: data/data/collections/0 must have required property 'orgMintedIdentifier'",
//           )
//         })
//     },
//   )
// })
// /*
//   Test_ID: Invertebrate-POST-bulk-004
//   Description: Failure to POST a collection of invertebrate surveys to Core when an orgMintedIdentifier could not be decrypted
// */
// describe('Invertebrate-POST-bulk-004: Failure to POST a collection of invertebrate surveys to Core when an orgMintedIdentifier could not be decrypted', () => {
//   test.each(Object.keys(endpoints))(
//     'Failure to POST a collection of %s to Core when an orgMintedIdentifier could not be decrypted',
//     async (endpoint) => {
//       //Mock POST /validate-token
//       axios.post.mockResolvedValueOnce(validation_true)
//       //Axios Get mock function was called 2 times in this file
//       axios.get.mockResolvedValueOnce(mock_pdp_true) //1
//       axios.get.mockResolvedValueOnce(mock_pdp_true) //2
//       //Axios Post mock function was called 1 time in this file
//       axios.post.mockResolvedValueOnce(post_collection_true) //1
//       // mock upload photo response
//       axios.post.mockResolvedValueOnce(mock_upload_blob)
//       const data = cloneDeep(endpoints[endpoint].data)
//       data.collections[0].orgMintedIdentifier = '{"foo": "bar" }'
//       await request(global.strapiServer) // app server is an instance of Class: http.Server
//         .post(`/api/${endpoint}/bulk`)
//         .set('accept', 'application/json')
//         .set('Content-Type', 'application/json')
//         set('authorization', test01_jwt)
//         .send({
//           data: data,
//         })
//         .then((data) => {
//           // console.log('returned data 004 ', data.body)
//           expect(data.body.error.status).toBe(400)
//           expect(data.body.error.message).toBe(
//             'json decrypt: invalid parameters. Could not decode the orgMintedIdentifier',
//           )
//         })
//     },
//   )
// })

// /*
//   Test_ID: Invertebrate-POST-bulk-005
//   Description: Test the ability of Core to reject requests to POST bird survey collections when the collectionIdentifier already exists, i.e., the collection has already been POSTed
// */
// describe('Invertebrate-POST-bulk-005: Failure to POST a collection of invertebrate surveys to Core when an collectionIdentifier already exists', () => {
//   test.each(Object.keys(endpoints))(
//     'Failure to POST a collection of %s to Core when an when an collectionIdentifier already exists',
//     async (endpoint) => {
//       //Mock POST /validate-token
//       axios.post.mockResolvedValueOnce(validation_true)
//       axios.get.mockResolvedValueOnce(mock_pdp_true)
//       axios.get.mockRejectedValue(new Error('Throwing an error'))
//       axios.post.mockResolvedValueOnce(mock_upload_blob)
//       // mock upload photo response
//       const data = cloneDeep(endpoints[endpoint].data)
//       await request(global.strapiServer) // app server is an instance of Class: http.Server
//         .post(`/api/${endpoint}/bulk`)
//         .set('accept', 'application/json')
//         .set('Content-Type', 'application/json')
//         .set('authorization', test01_jwt)
//         .send({
//           data: data,
//         })
//         .then((data) => {
//           // console.log('returned data 005 ', data.body)
//           expect(data.body.error.status).toBe(500)
//           expect(data.body.error.message).toBe('Internal Server Error')
//         })
//     },
//   )
// })

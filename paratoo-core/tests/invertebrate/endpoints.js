/////////////////////////////////////////////////// stale and redundant tests (#1947 & #1949) ///////////////////////////////


// module.exports = {
//   'invertebrate-active-searches': {
//     data: {
//       collections: [
//         {
//           orgMintedIdentifier:
//             '{"iv":"k5oHdNi/gm5j1D3/qjoc7A==","v":1,"iter":10000,"ks":128,"ts":64,"mode":"ccm","adata":"","cipher":"aes","salt":"9sxzRJoD7FY=","ct":"1KRQEM+6jmEHQ3KBrxE2b62t5QLY7lPK5qHbUE979z1RxM9LY5rz/AaI4YrT7+L/U6wODJpo5P0YinqtnOaac5tonHuomtsD0VQotYfBgdi0OWClvwQBiHtT/jTeW2tPO2HFlgcXsqhjegEZMiWBY5QTQUMl5xj9O++6X+RUXEbcE0ht/if23kfpV6XDn6WIFuMPAWlVdjCPCJWHOfGSpq7L3rme04wSjVyg9pCGAl2fsaVBhSdm4E5qmC/5RxZ1zSG7f0yIcZEPTd371PVG"}',
//           'plot-layout': { id: 1 },
//           'plot-visit': { id: 1 },
//           'invertebrate-active-search': {
//             data: {
//               survey_metadata: 'string',
//               plot_visit: 0,
//               search_method: 'DAS',
//               observers: 'string',
//               number_of_observers: 0,
//               search_time: 0,
//               weather: {
//                 precipitation: 'NO',
//                 precipitation_duration: 'I',
//                 wind_description: 'C',
//                 cloud_cover: 'SU',
//                 temperature: 0,
//               },
//               start_time: '2023-03-09T03:10:17.594Z',
//               end_time: '2023-03-09T03:10:17.594Z',
//             },
//           },
//           'invertebrate-active-search-setup': {
//             data: {
//               'invertebrate-active-search': 0,
//               apparatus: [
//                 {
//                   data: {
//                     equipment: 'SN',
//                     photo: 0,
//                     description: 'string',
//                     createdBy: 0,
//                     updatedBy: 0,
//                   },
//                 },
//               ],
//               active_search_photos: [
//                 {
//                   data: {
//                     photo: 0,
//                     description: 'string',
//                     lat: 0,
//                     lng: 0,
//                     location_id: 'string',
//                     createdBy: 0,
//                     updatedBy: 0,
//                   },
//                 },
//               ],
//               samples: [
//                 {
//                   data: {
//                     barcode: 'string',
//                     preservation_type: 'DS',
//                     sample_photo: 0,
//                     photo_description: 'string',
//                     comment: 'string',
//                     createdBy: 0,
//                     updatedBy: 0,
//                   },
//                 },
//               ],
//             },
//           },
//         },
//       ],
//     },
//   },
//   'invertebrate-malaise-trappings': {
//     data: {
//       collections: [
//         {
//           orgMintedIdentifier:
//             '{"iv":"k5oHdNi/gm5j1D3/qjoc7A==","v":1,"iter":10000,"ks":128,"ts":64,"mode":"ccm","adata":"","cipher":"aes","salt":"9sxzRJoD7FY=","ct":"1KRQEM+6jmEHQ3KBrxE2b62t5QLY7lPK5qHbUE979z1RxM9LY5rz/AaI4YrT7+L/U6wODJpo5P0YinqtnOaac5tonHuomtsD0VQotYfBgdi0OWClvwQBiHtT/jTeW2tPO2HFlgcXsqhjegEZMiWBY5QTQUMl5xj9O++6X+RUXEbcE0ht/if23kfpV6XDn6WIFuMPAWlVdjCPCJWHOfGSpq7L3rme04wSjVyg9pCGAl2fsaVBhSdm4E5qmC/5RxZ1zSG7f0yIcZEPTd371PVG"}',
//           'plot-layout': { id: 1 },
//           'plot-visit': { id: 1 },
//           'invertebrate-malaise-trapping': {
//             data: {
//               survey_metadata: 'string',
//               plot_visit: 0,
//               observers: 'string',
//             },
//           },
//           'invertebrate-malaise-trap': [
//             {
//               data: {
//                 'malaise-trapping': 0,
//                 trapId: 'string',
//                 start_date: '2023-03-09T03:07:57.564Z',
//                 direction: 'S',
//                 moth_excluder: true,
//                 location: {
//                   lat: 0,
//                   lng: 0,
//                 },
//                 trap_photo: [
//                   {
//                     photo: 0,
//                     direction: 'S',
//                     description: 'string',
//                   },
//                 ],
//                 habitat_description: 'string',
//                 weather: {
//                   precipitation: 'NO',
//                   precipitation_duration: 'I',
//                   wind_description: 'C',
//                   cloud_cover: 'SU',
//                   temperature: 0,
//                 },
//                 comment: 'string',
//                 duration: 0,
//                 end_date: '2023-03-09T03:07:57.564Z',
//                 barcode: 'string',
//                 sample_photo: [
//                   {
//                     media: 0,
//                     comment: 'string',
//                   },
//                 ],
//                 samples: [
//                   {
//                     data: {
//                       sample_collection_date: '2023-03-09T03:07:57.564Z',
//                       voucher_type: 'LP',
//                       trap_preservative: 'DS',
//                       undiluted_preservative: true,
//                       preservative_concentration: 100,
//                       replace_collection_bottle: true,
//                       empty_filter_weight: 0,
//                       filter_weight_with_sample: 0,
//                       sample_biomass: 0,
//                       weather: {
//                         precipitation: 'NO',
//                         precipitation_duration: 'I',
//                         wind_description: 'C',
//                         cloud_cover: 'SU',
//                         temperature: 0,
//                       },
//                       createdBy: 0,
//                       updatedBy: 0,
//                     },
//                   },
//                 ],
//                 createdBy: 0,
//                 updatedBy: 0,
//               },
//             },
//           ],
//         },
//       ],
//     },
//   },
//   'invertebrate-pan-trappings': {
//     data: {
//       collections: [
//         {
//           orgMintedIdentifier:
//             '{"iv":"k5oHdNi/gm5j1D3/qjoc7A==","v":1,"iter":10000,"ks":128,"ts":64,"mode":"ccm","adata":"","cipher":"aes","salt":"9sxzRJoD7FY=","ct":"1KRQEM+6jmEHQ3KBrxE2b62t5QLY7lPK5qHbUE979z1RxM9LY5rz/AaI4YrT7+L/U6wODJpo5P0YinqtnOaac5tonHuomtsD0VQotYfBgdi0OWClvwQBiHtT/jTeW2tPO2HFlgcXsqhjegEZMiWBY5QTQUMl5xj9O++6X+RUXEbcE0ht/if23kfpV6XDn6WIFuMPAWlVdjCPCJWHOfGSpq7L3rme04wSjVyg9pCGAl2fsaVBhSdm4E5qmC/5RxZ1zSG7f0yIcZEPTd371PVG"}',
//           'plot-layout': { id: 1 },
//           'plot-visit': { id: 1 },
//           'invertebrate-pan-trapping': {
//             data: {
//               survey_metadata: 'string',
//               plot_visit: 0,
//               monitoring_duration: 'Short',
//               repeat_monitoring: true,
//               observers: 'string',
//             },
//           },
//           'invertebrate-pan-trap': [
//             {
//               data: {
//                 barcode: 'string',
//                 start_date: '2023-03-09T03:11:39.906Z',
//                 proposed_duration: 0,
//                 end_date: '2023-03-09T03:11:39.906Z',
//                 duration: 0,
//                 lat: 0,
//                 lng: 0,
//                 trapId: 'string',
//                 trap_photo: [0],
//                 inside_pan_colour: 'W',
//                 outside_pan_colour: 'W',
//                 pan_diameter: 0,
//                 pan_depth: 0,
//                 pan_capacity: 0,
//                 pan_placement: 'OG',
//                 pan_height_above_ground: 0,
//                 liquid_type: 'WDS',
//                 'liquid_amount(estimated)': 0,
//                 stakes_used: true,
//                 trap_not_found: true,
//                 trap_evaporated: true,
//                 rain_impacted: true,
//                 trap_upended: true,
//                 trap_damaged: true,
//                 no_catch: true,
//                 samples: {
//                   media: [
//                     {
//                       media: 'string or id',
//                       comment: 'string',
//                     },
//                   ],
//                   preservation_type: 'DS',
//                   collection_date: [
//                     {
//                       collection_date: '2023-03-09T03:11:39.906Z',
//                     },
//                   ],
//                 },
//                 dominant_species_in_flower: [
//                   {
//                     species_name: 'string',
//                   },
//                 ],
//                 createdBy: 0,
//                 updatedBy: 0,
//               },
//             },
//           ],
//         },
//       ],
//     },
//   },
//   'invertebrate-wet-pitfall-traps': {
//     data: {
//       collections: [
//         {
//           orgMintedIdentifier:
//             '{"iv":"k5oHdNi/gm5j1D3/qjoc7A==","v":1,"iter":10000,"ks":128,"ts":64,"mode":"ccm","adata":"","cipher":"aes","salt":"9sxzRJoD7FY=","ct":"1KRQEM+6jmEHQ3KBrxE2b62t5QLY7lPK5qHbUE979z1RxM9LY5rz/AaI4YrT7+L/U6wODJpo5P0YinqtnOaac5tonHuomtsD0VQotYfBgdi0OWClvwQBiHtT/jTeW2tPO2HFlgcXsqhjegEZMiWBY5QTQUMl5xj9O++6X+RUXEbcE0ht/if23kfpV6XDn6WIFuMPAWlVdjCPCJWHOfGSpq7L3rme04wSjVyg9pCGAl2fsaVBhSdm4E5qmC/5RxZ1zSG7f0yIcZEPTd371PVG"}',
//           'plot-layout': { id: 1 },
//           'plot-visit': { id: 1 },
//           'invertebrate-wet-pitfall-trap': {
//             data: {
//               survey_metadata: 'string',
//               plot_visit: 0,
//               plot_type: 'vegetation',
//               observers: 'string',
//               weather: {
//                 precipitation: 'NO',
//                 precipitation_duration: 'I',
//                 wind_description: 'C',
//                 cloud_cover: 'SU',
//                 temperature: 0,
//               },
//               pitfall_trap_points: [
//                 {
//                   lat: 0,
//                   lng: 0,
//                   trapId: '1A',
//                   proposed_duration: 0,
//                   duration: 0,
//                   start_date: '2023-03-09T03:12:49.017Z',
//                   end_date: '2023-03-09T03:12:49.017Z',
//                   trap_diameter: 0,
//                   trap_depth: 0,
//                   trap_preservative: 'DS',
//                   undiluted_preservative: true,
//                   preservative_concentration: 100,
//                   preservative_volume: 1,
//                   barcode: 'string',
//                   media: [
//                     {
//                       media: 'string or id',
//                       comment: 'string',
//                     },
//                   ],
//                 },
//               ],
//               grid_photo: {
//                 photo: 0,
//                 direction: 'S',
//                 description: 'string',
//               },
//             },
//           },
//         },
//       ],
//     },
//   },
// }

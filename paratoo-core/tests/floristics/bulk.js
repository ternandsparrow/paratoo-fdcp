import * as cc from '../helpers/constants'

const request = require('supertest')
const { cloneDeep } = require('lodash')
const { registerMockUris, generatePDPApiConfigs } = require('../helpers/mockAdapter')
const mock = require('./mock.json')

test(`Floristics Full Setup Data: (Part-1) should successfully POST relevant requests to reserve the plots`, async () => {
  // plot selection prot id to check permission to write
  const plotSelectionProtID = mock.protocolId1
  const plots = mock.body1.data.collections[0]['plot-selection']

  // need to reserve the plots before we create them, as the plot selection controller
  // expects them to be reserved to update a relation to the created plot
  for (const plotData of plots) {
    let apiMockConfigs = cloneDeep(mock.apiConfigs1)
    const projectIds = plotData.data.projects_for_plot.map((p) => p.project_id.id)
    
    // all PDP combinations
    apiMockConfigs = apiMockConfigs.concat(
      generatePDPApiConfigs(
        projectIds,
        [plotSelectionProtID],
        cc.PDP_TYPE.WRITE,
      ),
    )
    // register all apis with axios mock adapter
    registerMockUris(cloneDeep(apiMockConfigs))

    // send request
    console.log('reserve plot label:', plotData.data.plot_label)
    const requestBody = {
      data: {
        plot_label: plotData.data.plot_label,
        projects: projectIds,
      },
    }
    await request(global.strapiServer)
      .post(mock.postEndpoint1)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .send(requestBody)
      .then((data) => {
        console.log(`returned data reserved plot `, data.body)
        expect(data.status).toBe(200)
        expect(data?.body?.data?.attributes?.plot_label).toBe(
          plotData.data.plot_label,
        )
      })
  }
})

test(`Floristics Full Setup Data: (Part-2) should successfully POST relevant requests of plot selection(bulk as many is deprecated) to Core`, async () => {
  let apiMockConfigs = cloneDeep(mock.apiConfigs2)
  // syncPlotSelection function makes GET to org /plot-selections and /user-projects
  apiMockConfigs.push([
    cc.REQUEST_TYPE.GET,
    cc.API.PLOT_SELECTION,
    200,
    cloneDeep(mock.response1),
  ])
  // the projects before we make any updates
  apiMockConfigs.push([
    cc.REQUEST_TYPE.GET,
    cc.API.USER_PROJECT,
    200,
    cloneDeep(mock.response2),
  ])
  // org sends responses to the POST of /plot-selections
  for (const plotSelectionResp of mock.response3) {
    apiMockConfigs.push([
      cc.REQUEST_TYPE.POST,
      cc.API.PLOT_SELECTION,
      200,
      cloneDeep(plotSelectionResp),
    ])
  }
  // org sends responses to the PUT of /projects
  for (const projectPutResp of mock.response4) {
    apiMockConfigs.push([
      cc.REQUEST_TYPE.PUT,
      cc.API.PROJECT,
      200,
      cloneDeep(projectPutResp),
    ])
  }
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(apiMockConfigs),
    mock.projectId1,
    mock.protocolId1,
    mock.orgMintedUuid1,
  )

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(mock.body1)
    .then((data) => {
      console.log('returned data 001 ', data.body)
      expect(data.status).toBe(200)
      expect(data.status).toBe(200)
    })
})

test(`Floristics Full Setup Data: (Part-3) should successfully POST relevant requests of plot definition survey(layout & visit) to Core`, async () => {
  for (const requestBody of mock.body2) {
    const mintedUuid =
      requestBody.data.collections[0]['plot-definition-survey'].data
        .survey_metadata.orgMintedUUID
    registerMockUris(
      cloneDeep(cloneDeep(mock.apiConfigs2)),
      mock.projectId1,
      mock.protocolId2,
      mintedUuid,
    )

    await request(global.strapiServer) // app server is an instance of Class: http.Server
      .post(mock.bulkEndpoint2)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .send(requestBody)
      .then((data) => {
        console.log('returned data 001 ', data.body)
        expect(data.status).toBe(200)
        expect(data.status).toBe(200)
      })
  }
})

test(`Floristics Full Setup Data: (Part-4) should successfully POST collections of floristics-full-surveys to Core`, async () => {
  for (const requestBody of mock.body3) {
    const mintedUuid =
      requestBody.data.collections[0]['floristics-veg-survey-full'].data
        .survey_metadata.orgMintedUUID
    registerMockUris(
      cloneDeep(cloneDeep(mock.apiConfigs2)),
      mock.projectId1,
      mock.protocolId3,
      mintedUuid,
    )

    await request(global.strapiServer) // app server is an instance of Class: http.Server
      .post(mock.bulkEndpoint3)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('authorization', mock.validJwt1)
      .send(requestBody)
      .then((data) => {
        console.log('returned data 001 ', data.body)
        expect(data.status).toBe(200)
        expect(data.status).toBe(200)
      })
  }
})

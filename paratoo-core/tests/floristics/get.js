// /////////////////////////////////////////////////// stale and redundant tests (#1947 & #1949) ///////////////////////////////


// const request = require('supertest')
// const test_data = require('./test-data.json')
// import axios from 'axios'

// jest.mock('axios')
// const validation_true = { data: true }
// const jwt = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZm9yY2VUb2tlbkV4cGlyeSI6eyJmb3JjZUp3dEV4cGlyeSI6bnVsbCwiZm9yY2VSZWZyZXNoRXhwaXJ5IjpudWxsfSwiaWF0IjoxNzA0Njc5NDE0LCJleHAiOjE3MDcyNzE0MTR9.1ik6JSM6W1gQgV7qkHP5LLItdt-mNa4VnK633m8Zw7k'
// const voucherFullSurveyGetEndpoint = '/api/floristics-veg-survey-fulls'
// const voucherFullGetEndpoint = '/api/floristics-veg-voucher-fulls'

// // want to ignore certain keys that might not be static (e.g., time-based)
// // https://stackoverflow.com/a/68891805
// const removeIgnoredKeys = o => {
//   const keysToIgnore = [
//     'createdAt',
//     'updatedAt',
//     'start_date_time',
//     'end_date_time',
//     'time',
//   ]
//   if (o) {
//     switch (typeof o) {
//     case 'object':
//       for (const key of keysToIgnore) {
//         delete o[key]
//       }
//       Object.keys(o).forEach(k => removeIgnoredKeys(o[k]))
//       break;
//     case 'array':
//       o.forEach(a => removeIgnoredKeys(a))
//     }
//   }
// }

// test('Get Floristics Full Surveys WITHOUT project_ids parameter: should return all surveys', async () => {
//   axios.post.mockResolvedValueOnce(validation_true)
//   await request(global.strapiServer)
//     .get(voucherFullSurveyGetEndpoint)
//     .set('authorization', jwt)
//     .then((data) => {
//       console.log(`returned data floristics survey full survey no url param`, JSON.stringify(data.body, null, 2))
//       expect(data.status).toBe(200)
      
//       const actual = data.body
//       const expected  = test_data.get_vouchers_survey_full_no_url_param_resp
//       removeIgnoredKeys(actual)
//       removeIgnoredKeys(expected)
//       // `toBe` won't work because of some weird array comparison issues: https://stackoverflow.com/a/62544617
//       expect(actual).toStrictEqual(expected)
//     })
// })

// test('Get Floristics Full Vouchers WITHOUT project_ids parameter: should return all vouchers', async () => {
//   axios.post.mockResolvedValueOnce(validation_true)
//   await request(global.strapiServer)
//     .get(voucherFullGetEndpoint)
//     .set('authorization', jwt)
//     .then((data) => {
//       console.log(`returned data floristics vouchers full no url param`, JSON.stringify(data.body, null, 2))
//       expect(data.status).toBe(200)

//       const actual = data.body
//       const expected  = test_data.get_vouchers_full_no_url_param_resp
//       removeIgnoredKeys(actual)
//       removeIgnoredKeys(expected)
//       expect(actual).toStrictEqual(expected)
//     })
// })

// test('Get Floristics Full Surveys WITH project_ids parameter: should return only specific surveys', async () => {
//   axios.post.mockResolvedValueOnce(validation_true)
//   //when providing the url param, core gets a list of projects from org that have been
//   //updated with the new plots
//   axios.get.mockResolvedValueOnce(test_data.projects_get_resp_after_update)
//   await request(global.strapiServer)
//     .get(`${voucherFullSurveyGetEndpoint}`)
//     .set('authorization', jwt)
//     .then((data) => {
//       console.log(`returned data floristics survey full survey with url param`, JSON.stringify(data.body, null, 2))
//       expect(data.status).toBe(200)

//       const actual = data.body
//       const expected  = test_data.get_vouchers_survey_full_with_url_param_resp
//       removeIgnoredKeys(actual)
//       removeIgnoredKeys(expected)
//       expect(actual).toStrictEqual(expected)
//     })
// })

// test('Get Floristics Full Vouchers WITH project_ids parameter: should return only specific vouchers', async () => {
//   axios.post.mockResolvedValueOnce(validation_true)
//   //when providing the url param, core gets a list of projects from org that have been
//   //updated with the new plots
//   axios.get.mockResolvedValueOnce(test_data.projects_get_resp_after_update)
//   await request(global.strapiServer)
//     .get(`${voucherFullGetEndpoint}`)
//     .set('authorization', jwt)
//     .then((data) => {
//       console.log(`returned data floristics vouchers full with url param`, JSON.stringify(data.body, null, 2))
//       expect(data.status).toBe(200)

//       const actual = data.body
//       const expected  = test_data.get_vouchers_full_with_url_param_resp
//       removeIgnoredKeys(actual)
//       removeIgnoredKeys(expected)
//       expect(actual).toStrictEqual(expected)
//     })
// })
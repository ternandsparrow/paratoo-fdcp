import axios from 'axios'
import * as cc from '../helpers/constants'

const MockAdapter = require('axios-mock-adapter')

/**
 * Register a list of uris with mock response
 *
 * @param {Array.<Array>} configs list of api configs e.g. [["POST", "<org>/validate-token", 200, { "data": true }]]
 * @param {String} projectId replace <project_id> with projectId from url
 * @param {String} protocolId replace <protocol_id> with protocolId from url
 * @param {String} orgMintedUuid replace <org_minted_uuid> with orgMintedUuid from url
 */
export function registerMockUris(
  configs,
  projectId = null,
  protocolId = null,
  orgMintedUuid = null,
) {
  const mocker = new MockAdapter(axios, { onNoMatch: 'throwException' })
  mocker.onAny().reply((config) => {
    const [method, _url, ...response] = configs.shift()
    const url = replaceUrl(_url, projectId, protocolId, orgMintedUuid)
    console.log(`mock url : ${url}`)
    if (config.url === url && config.method.toUpperCase() === method)
      return response
    // Unexpected request, error out
    const error = `No mock response found, url: ${url}, config: ${JSON.stringify(
      config,
    )}`
    console.error(error)
    return [500, { error: error }]
  })
}

/**
 * generate a list of pdp mock response from projectIds and protocolIds
 *
 * @param {Array.<String|Int>} projectIds list of project ids
 * @param {Array.<String|Int>} protocolIds list of protocol ids
 * @param {String} type pdp request type e.g. 'read' or 'write'
 * @param {String} isSuccess response type e.g. isAuthorised or not
 *
 * @returns {Array.<Array>} a list of apiConfigs to register with axios mock adapter
 */
export function generatePDPApiConfigs(
  projectIds,
  protocolIds,
  type = 'write',
  isSuccess = true,
) {
  let apiConfigs = []
  for (const project of projectIds) {
    for (const protocol of protocolIds) {
      // request with all types of pdp combinations
      apiConfigs.push([
        cc.REQUEST_TYPE.GET,
        `<org>/pdp/${project}/${protocol}/${type}`,
        200,
        { data: { isAuthorised: isSuccess } },
      ])
    }
  }
  console.log(`All registered pdp combinations: ${JSON.stringify(apiConfigs)}`)
  return apiConfigs
}

// replace fields form the url. e.g. replace <protocol_id> with protocolId from url
function replaceUrl(
  url,
  projectId = null,
  protocolId = null,
  orgMintedUuid = null,
) {
  let fixedUrl = url

  // if org url
  if (fixedUrl.includes('<org>')) {
    // e.g. '/api/org' or '/ws/paratoo'
    let apiPrefix = `${process.env.ORG_URL_PREFIX}${process.env.ORG_CUSTOM_API_PREFIX}`
    for (const api of cc.NON_CUSTOM_API) {
      if (!fixedUrl.includes(api)) continue
      // default e.g. '/api' or '/ws/paratoo'
      apiPrefix = `${process.env.ORG_URL_PREFIX}${process.env.ORG_API_PREFIX}`
      break
    }
    fixedUrl = fixedUrl.replace('<org>', apiPrefix)
  }

  if (projectId) fixedUrl = fixedUrl.replace('<project_id>', projectId)
  if (protocolId) fixedUrl = fixedUrl.replace('<protocol_id>', protocolId)
  if (orgMintedUuid)
    fixedUrl = fixedUrl.replace('<org_minted_uuid>', orgMintedUuid)

  return fixedUrl
}

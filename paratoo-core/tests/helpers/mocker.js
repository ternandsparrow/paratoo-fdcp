const request = require('supertest')
import * as cc from './constants'
const {
  cloneDeep,
  upperFirst,
  camelCase,
  lowerCase,
  forEach,
} = require('lodash')
const uuid = require('uuid')
const { customRules } = require('../../src/customRules')

/**
 * Send post request with body.
 *
 * @param {String} testName name of the test
 * @param {Object} server server
 * @param {String} endpoint endpoint
 * @param {Object} mockData request body
 * @param {Integer} respStatus expected response status
 * @param {String} errorMessage expected error message
 * @param {String} token bearer token
 */
export async function postMockData(
  testName,
  server,
  endpoint,
  mockData,
  respStatus,
  errorMessage = null,
  token = null,
) {
  if (!mockData) return
  const jwt = token ? token : cc.TEST_JWT

  // app server is an instance of Class: http.Server
  await request(server)
    .post(endpoint)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', jwt)
    .send(mockData)
    .then((data) => {
      console.log(`test: ${testName} path: ${endpoint} body: `, data.body)
      expect(data.status).toBe(respStatus)

      if (errorMessage) {
        expect(data.body.error.message).toContain(errorMessage)
      }
    })
}

/**
 * Create and store mock data for all the protocols using schema documentation
 *
 * @param {Array} protocols list of protocols
 * @param {Object} documentation documentation
 */
export async function createMockData(
  protocols,
  documentation,
  approved = [],
  project_id = '1',
) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const protocolMocks = {}
  const currentDate = new Date(Date.now())
  const schemas = cloneDeep(documentation['components']['schemas'])
  const pathObjects = cloneDeep(documentation['paths'])
  const paths = Object.keys(pathObjects)
  // if submodule exists we will push submodule too
  let allProtocols = approved
  let subModules = {}
  const subModuleRelations = cloneDeep(cc.SUB_MODULES_RELATION)
  for (const module of Object.keys(subModuleRelations)) {
    if (!allProtocols.includes(module)) continue
    const subModuleId = subModuleRelations[module]
    const allSubs = Object.keys(subModules)
    if (allSubs.includes(subModuleId)) continue
    subModules[subModuleId] = module
    // allSubmodules.push(subModuleId)
  }
  for (const protocol of protocols) {
    const protId = protocol['identifier']
    const allSubs = Object.keys(subModules)
    if (!allProtocols.includes(protId) && !allSubs.includes(protId)) continue

    const collection = {}
    // find all the linked models from protocol workflow
    let workFlow = await helpers.findModelAndChildObsFromProtocolUUID(protId)
    let models = Object.keys(workFlow.raw)
    const endpoint = `${protocol['endpointPrefix']}/bulk`
    if (!paths.includes(endpoint)) continue

    // assign `id: 1` if plot based
    workFlow.plotModels.forEach((m) => {
      collection[m] = { id: 1 }
      models = models.filter((e) => e !== m)
    })

    // usually first model is the survey
    const surveyModel = workFlow.surveyModel
    const surveyMetadata = createSurveyId(
      surveyModel,
      protId,
      project_id,
      cloneDeep(subModules),
    )
    // generate mock using mock-json-schema and create collection object
    models.forEach((model) => {
      // checks the type of object
      const requestBody = pathObjects[endpoint]
      const objectType =
        requestBody.post.requestBody.content['application/json'].schema
          .properties.data.properties.collections.items.properties[model]?.type
      const isArray = objectType == 'array'
      const payload = helpers.generateDummyData(
        model,
        schemas,
        surveyModel,
        cloneDeep(surveyMetadata),
        null,
        isArray,
        workFlow,
      )

      collection[model] = payload
    })

    // assign orgMintedIdentifier
    collection['orgMintedIdentifier'] = createOrgIdentifier(
      surveyMetadata,
      currentDate,
    )
    // final request body with valid data
    const data = {
      data: {
        collections: [collection],
      },
      role: 'collection',
    }

    // temporary saves data for internal operation so that we don't need to create every time
    protocolMocks[protocol['identifier']] = {
      surveyModel: cloneDeep(surveyModel),
      surveyMetadata: cloneDeep(surveyMetadata),
      models: models,
      data: cloneDeep(data),
    }
  }

  return cloneDeep(protocolMocks)
}

export function updateSurveyId(data, surveyModel) {
  const body = cloneDeep(data)
  const currentDate = new Date(Date.now())
  // update only survey ID and orgMintedIdentifier
  for (const collection of body.data.collections) {
    collection[surveyModel].data.survey_metadata.survey_details.uuid = uuid.v4()
    collection[surveyModel].data.survey_metadata.survey_details.time =
      currentDate.toISOString()
    collection.orgMintedIdentifier = createOrgIdentifier(
      collection[surveyModel].data.survey_metadata,
      currentDate,
    )
  }

  return body
}

/**
 * Create invalid request body. e.g. assign text value to an integer
 *
 * @param {Object} protocol protocol info
 * @param {Object} validMock object with valid mock data, associated models and survey model
 * @param {String} type field type e.g. string, int
 * @param {String} format format e.g. date-time, float
 * @param {String} property property e.g. minimum, unique
 * @param {String} flag format e.g. x-model-ref
 * @param {String} isRequired is required
 * @param {String} componentSearch search inside child observation
 * @returns {Object} updated data with invalid value
 */
export async function generateInvalidMockData(
  protocol,
  validMock,
  type,
  format = null,
  property = null,
  flag = null,
  isRequired = false,
  componentSearch = false,
) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const documentation = await helpers.readFullDocumentation()
  const schemas = cloneDeep(documentation['components']['schemas'])
  // valid data, linked models and name of the survey model are required
  if (!validMock.data || !validMock.models || !validMock.surveyModel) return

  // one field with invalid data is enough to make invalid request body
  const dataMock = validMock.data
  let skipModels = []
  // checks all the models to find field with the type and format
  for (const model of validMock.models) {
    if (!isValidModel(model, validMock.surveyModel, skipModels)) {
      skipModels.push(model)
      continue
    }
    const modelRequest = `${upperFirst(camelCase(model))}Request`
    const modelSchema = cloneDeep(schemas[modelRequest])
    let fieldData = getFieldName(
      type,
      modelSchema.properties.data,
      format,
      property,
      flag,
      isRequired,
      componentSearch,
    )
    if (!fieldData.field) {
      skipModels.push(model)
      continue
    }
    console.log(
      `FIELD FOUND fieldData: ${JSON.stringify(fieldData)} in ${modelRequest}`,
    )
    // if field exists then assigns invalid value and returns
    return {
      data: assignInvalidFieldValue(dataMock, model, fieldData),
      fieldData: fieldData,
    }
  }
  console.warn(`FIELD type: ${type} NOT FOUND in protocol: ${protocol.name}`)
  console.warn(`format: ${format} flag: ${flag} required: ${isRequired}`)
  // if no field with specified type found we return null
  return null
}

/**
 * Create invalid request body using custom rule type.
 *
 * @param {Object} protocol protocol info
 * @param {Object} validMock object with valid mock data, associated models and survey model
 * @param {String} ruleType rule type
 *
 * @returns {Object} updated data with invalid value
 */
export function generateInvalidMockDataUsingRule(
  protocol,
  validMock,
  ruleType,
) {
  if (!validMock.data) return null

  const body = cloneDeep(validMock.data)
  const modelsWithRules = Object.keys(customRules)

  // checking 1 conditionalRequiredChecked is enough
  let conditionalRequiredChecked = false

  for (const model of validMock.models.concat(validMock.surveyModel)) {
    if (!modelsWithRules.includes(model)) continue
    for (const rule of customRules[model]) {
      if (rule.ruleType != ruleType) continue

      const collection = body.data.collections[0]
      const isArray = Array.isArray(collection[model])
      const modelData = isArray ? collection[model][0] : collection[model]

      const invalidData = {
        data: null,
        errorCode: 400,
        errorMessage: rule.description,
      }
      // if rule exists
      switch (rule.ruleType) {
        // multipleValuesMinimumCheck e.g. basal-wedge-observation
        case cc.CUSTOM_RULE_TYPES.multipleValuesMinimumCheck:
          modelData['data']['no_species_to_record'] = false
          modelData['data']['in_tree'] = 1
          modelData['data']['borderline_tree'] = 1

          body.data.collections[0] = collection
          invalidData.data = cloneDeep(body)
          return invalidData

        // OneDecimalPlaceCheck e.g. basal-area-dbh-measure-observation
        case cc.CUSTOM_RULE_TYPES.OneDecimalPlaceCheck:
          // so that validator fails to validate
          modelData['data']['stem_1']['POM_single'] = 1.0994545876435435
          body.data.collections[0] = collection

          invalidData.data = cloneDeep(body)
          invalidData.errorMessage =
            'POM_single does not allow more than one decimal point'

          return invalidData

        // maximumDifferenceCheck e.g. soil-lite-sample
        case cc.CUSTOM_RULE_TYPES.maximumDifferenceCheck:
          // so that validator fails to validate
          modelData['data']['upper_depth'] = 0.1
          modelData['data']['lower_depth'] = 3

          body.data.collections[0] = collection
          invalidData.data = cloneDeep(body)
          return invalidData

        // minNumOfReplicates e.g. floristics-veg-genetic-voucher
        case cc.CUSTOM_RULE_TYPES.minNumOfReplicates:
          // so that validator fails to validate
          modelData['data']['has_replicate'] = true
          // minimum 5
          modelData['data']['replicate'] = [modelData['data']['replicate'][0]]

          body.data.collections[0] = collection
          invalidData.data = cloneDeep(body)
          invalidData.errorMessage = 'Field: "Replicate": Minimum 5 per voucher'
          return invalidData

        // distanceBetweenReplicates e.g. floristics-veg-genetic-voucher
        case cc.CUSTOM_RULE_TYPES.distanceBetweenReplicates:
          // so that validator fails to validate
          modelData['data']['has_replicate'] = true
          modelData['data']['replicate'] = [
            {
              genetic_voucher_barcode: 'dev_foo_barcode_0001',
              replicate_number: 1,
            },
            {
              genetic_voucher_barcode: 'dev_foo_barcode_0002',
              replicate_number: 2,
            },
            {
              genetic_voucher_barcode: 'dev_foo_barcode_0003',
              replicate_number: 3,
            },
            {
              genetic_voucher_barcode: 'dev_foo_barcode_0004',
              replicate_number: 4,
            },
            {
              genetic_voucher_barcode: 'dev_foo_barcode_0005',
              replicate_number: 5,
            },
          ]

          // minimum 0
          modelData['data']['min_distance_between_replicates'] = -1

          body.data.collections[0] = collection
          invalidData.data = cloneDeep(body)
          invalidData.errorMessage =
            'Field: "Min Distance Between Replicates": Minimum 0'
          return invalidData

        // ConditionalFieldsCheck e.g. soil-sub-pit-sampling
        case cc.CUSTOM_RULE_TYPES.ConditionalFieldsCheck:
          if (!rule.conditions || conditionalRequiredChecked) continue
          // checking one condition is enough
          const condition = Array.isArray(rule.conditions)
            ? rule.conditions[0]
            : rule.conditions
          let updated = findAndReplaceValue(
            modelData['data'],
            condition.dependOn.field,
            condition.dependOn.value,
          )
          modelData['data'] = updated.data
          updated = findAndReplaceValue(
            modelData['data'],
            condition.requiredFields[0],
            null,
            true,
          )
          modelData['data'] = updated.data
          conditionalRequiredChecked = true
          body.data.collections[0] = collection
          invalidData.data = cloneDeep(body)
          invalidData.errorMessage = `${rule.stepName || model}/${
            condition.requiredFields[0]
          } is required`
          return invalidData
        // extend here to add more rules ..
      }
    }
  }
  console.warn(`NO rule: ${ruleType} FOUND ${protocol.name}`)
  return null
}

/* find and replace field inside object */
function findAndReplaceValue(data, field, replace = null, remove = false) {
  const keys = Object.keys(data)
  const payload = cloneDeep(data)
  const result = {
    data: null,
    fieldExist: false,
    value: null,
    changedValue: null,
  }
  // if field exists
  if (keys.includes(field)) {
    result.fieldExist = true
    result.value = payload[field]

    // if needs to replace or delete
    if (replace != null) {
      payload[field] = replace
      result.changedValue = replace
    }
    if (remove) delete payload[field]
    result.data = cloneDeep(payload)
    return cloneDeep(result)
  }

  // checks all the objects recursively
  for (const key of keys) {
    if (typeof data[key] != 'object') continue
    // if not an array
    if (!Array.isArray(data[key])) {
      const objectValue = findAndReplaceValue(data[key], field, replace, remove)
      if (!objectValue.data) continue

      // if found
      payload[key] = objectValue.data
      result.fieldExist = objectValue.fieldExist
      result.value = objectValue.value
      result.changedValue = objectValue.changedValue
      result.data = cloneDeep(payload)
      return cloneDeep(result)
    }

    // if an array
    let values = []
    let changedValues = []
    for (const [index, obj] of data[key].entries()) {
      const objectValue = findAndReplaceValue(obj, field, replace, remove)
      if (!objectValue.data) continue
      result.fieldExist = objectValue.fieldExist
      values.push(objectValue.value)
      payload[key][index] = objectValue.data
      if (objectValue.changedValue) changedValues.push(objectValue.changedValue)
    }

    if (!result.fieldExist) continue

    // if field found
    result.data = cloneDeep(payload)
    result.value = values
    result.changedValue = changedValues
    return cloneDeep(result)
  }
  return result
}

/*
 * assign invalid value. e.g. assign string to an integer
 */
function assignInvalidFieldValue(data, model, fieldData) {
  const payload = cloneDeep(data)
  const modelData = payload.data.collections[0][model]
  payload.data.collections[0][model] = null

  // if not bulk collection then return
  if (!modelData) return null
  console.log(`fieldData: ${JSON.stringify(fieldData)}`)

  const isArray = Array.isArray(modelData)
  const currentValue = isArray
    ? modelData[0]['data'][
        fieldData.componentData
          ? fieldData.componentData.field
          : fieldData.field
      ]
    : modelData['data'][
        fieldData.componentData
          ? fieldData.componentData.field
          : fieldData.field
      ]

  console.log(`current value: ${JSON.stringify(currentValue)}`)
  if (!currentValue) return null

  const newValue = generateInvalidValue(currentValue, fieldData)
  console.log(`incorrect value: ${JSON.stringify(newValue)}`)

  if (!newValue && !fieldData.componentData) {
    if (isArray) delete modelData[0]['data'][fieldData.field]
    if (!isArray) delete modelData['data'][fieldData.field]
    payload.data.collections[0][model] = modelData
    return cloneDeep(payload)
  }

  if (isArray) modelData[0]['data'][fieldData.field] = newValue
  if (!isArray) modelData['data'][fieldData.field] = newValue

  payload.data.collections[0][model] = modelData
  return cloneDeep(payload)
}

function getFieldStatus(field, property, required) {
  const fieldStatus = {
    length: 0,
    formatExists: false,
    propertyExists: false,
    flagExists: false,
    isRequired: required.includes(field),
  }
  const fieldKeys = Object.keys(property)
  const possiblePropertyTypes = ['enum', 'unique']
  const possibleFormats = ['format']
  fieldKeys.forEach((key) => {
    // possible properties
    if (possiblePropertyTypes.includes(key)) fieldStatus.propertyExists = true
    if (key.includes('min')) fieldStatus.propertyExists = true
    if (key.includes('max')) fieldStatus.propertyExists = true
    // possible properties
    if (possibleFormats.includes(key)) fieldStatus.formatExists = true
    if (key.includes('x-')) fieldStatus.flagExists = true
  })
  return fieldStatus
}

function checkProperty(field, properties, fieldProperty) {
  if (!fieldProperty) return null
  const fieldKeys = Object.keys(properties[field])

  switch (fieldProperty) {
    case cc.PROPERTY_TYPE.MINLENGTH:
      return fieldKeys.includes('minLength')
    case cc.PROPERTY_TYPE.MAXLENGTH:
      return fieldKeys.includes('maxLength')
    case cc.PROPERTY_TYPE.MINITEMS:
      return fieldKeys.includes('minItems')
    case cc.PROPERTY_TYPE.MAXITEMS:
      return fieldKeys.includes('maxItems')
    default:
      return fieldKeys.includes(lowerCase(fieldProperty))
  }
}

/*
 * Find the field with specified type and format from the schema documentation
 */
function getFieldName(
  type,
  schema,
  format = null,
  property = null,
  flag = null,
  isRequired = false,
  componentSearch = false,
) {
  if (!schema.properties) return null
  const properties = schema.properties
  const required = schema.required || []
  if (componentSearch) {
    const componentData = getFieldName(
      cc.FIELD_TYPE.NON_REPEATABLE_COMPONENT,
      schema,
    )
    if (!componentData.field) return componentData
    // if component found we will search inside
    let data = getFieldName(
      type,
      properties[componentData.field],
      format,
      property,
      flag,
      isRequired,
    )
    data['componentData'] = componentData
    return data
  }
  const fields = Object.keys(properties)
  const fieldData = {
    field: null,
    type: type,
    format: format,
    property: property,
    flag: flag,
    propertyValue: null,
    flagValue: null,
    isRequired: isRequired,
  }
  for (const field of fields) {
    fieldData.field = null
    if (!properties[field].type) continue
    if (properties[field].type != lowerCase(type)) continue

    const fieldStatus = getFieldStatus(field, properties[field], required)
    // for unique fields don't need to check required field
    if (fieldStatus.isRequired != isRequired) continue
    if (format && !fieldStatus.formatExists) continue
    if (!format && fieldStatus.formatExists) continue
    if (property && !fieldStatus.propertyExists) continue
    if (!property && fieldStatus.propertyExists) continue
    if (!flag && properties[field]['x-model-ref']) continue
    if (flag && !fieldStatus.flagExists) continue

    fieldData.field = field
    if (property && !checkProperty(field, properties, property)) continue
    if (property) fieldData.propertyValue = properties[field][property]

    if (format) {
      if (!properties[field].format) continue
      // lowerCase cant replace _ with space e.g. 'DATE_TIME' to 'date time'
      if (properties[field].format != lowerCase(format).replace(' ', '-'))
        continue
    }
    if (flag && !properties[field][flag]) continue
    if (flag) fieldData.flagValue = properties[field][flag]

    return fieldData
  }
  return { field: null }
}

/*
 * Create invalid data based upon flag types and values
 */
function generateInvalidValue(currentValue, fieldData) {
  if (currentValue && fieldData.componentData) {
    let newFieldData = cloneDeep(fieldData)
    delete newFieldData.componentData
    const newValue = generateInvalidValue(
      currentValue[fieldData.field],
      newFieldData,
    )
    if (!newValue) {
      delete currentValue[fieldData.field]
      return currentValue
    }

    currentValue[fieldData.field] = newValue
    return currentValue
  }
  // as value should be same
  if (fieldData.property == cc.PROPERTY_TYPE.UNIQUE) return currentValue
  // value should be null
  if (fieldData.isRequired) return null

  let value = null
  // handle types
  if (fieldData.type) {
    switch (fieldData.type) {
      case cc.FIELD_TYPE.NON_REPEATABLE_COMPONENT:
        if (!fieldData.property) value = [currentValue]
        break
      case cc.FIELD_TYPE.REPEATABLE_COMPONENT:
        if (!fieldData.property) value = currentValue[0] || {}
        break
      default:
        value = cc.INVALID_VALUE[fieldData.type]
        break
    }
  }
  // handle formats
  if (fieldData.format) {
    switch (fieldData.format) {
      default:
        value = cc.INVALID_VALUE[fieldData.format]
        break
    }
  }

  // handle properties
  if (fieldData.property) {
    switch (fieldData.property) {
      case cc.PROPERTY_TYPE.MINIMUM:
        value = fieldData.propertyValue - 1
        break
      case cc.PROPERTY_TYPE.MAXIMUM:
        value = fieldData.propertyValue + 1
        break
      case cc.PROPERTY_TYPE.MINITEMS:
        value = null
        break
      case cc.PROPERTY_TYPE.MAXITEMS:
        let currentLength = currentValue.length
        value = currentValue
        while (currentLength <= fieldData.propertyValue) {
          value.push(currentValue[0] ? currentValue[0] : {})
          currentLength += 1
        }
        break
      case cc.PROPERTY_TYPE.MINLENGTH:
        value = ''
        break
      case cc.PROPERTY_TYPE.MAXLENGTH:
        value = ''
        let count = 0
        while (count <= fieldData.propertyValue) {
          value = value + 'x'
          count += 1
        }
        break
      case cc.PROPERTY_TYPE.ENUM:
        value = cc.INVALID_VALUE[fieldData.property]
        break
    }
  }

  // handle flag
  if (!fieldData.flag) return value
  switch (fieldData.flag) {
    case 'x-model-ref':
      value = '-1'
      break
  }

  return value
}
/*
 * Find the best model in workflow
 * NOTE: skip plot models as these are some IDs
 */
function isValidModel(models, surveyModel, skip = []) {
  if (!models) return null
  if (!Array.isArray(models)) models = [models]
  for (const model of models) {
    if (cc.PLOT_MODELS.includes(model)) continue
    if (skip.includes(model)) continue
    if (model == surveyModel) continue
    return model
  }

  return null
}
/*
 * Create encoded identifier so that PME can decode
 */
export function createOrgIdentifier(surveyMetadata, currentDate = null) {
  const rawIdentifier = {
    userId: 1,
    eventTime: currentDate || currentDate.toISOString(),
    survey_metadata: surveyMetadata,
  }
  const encodedIdentifier = Buffer.from(JSON.stringify(rawIdentifier)).toString(
    'base64',
  )

  return encodedIdentifier
}
/*
 * Create survey ID
 */
export function createSurveyId(
  surveyModel,
  protId,
  projectId,
  subModules = {},
) {
  const allSubs = Object.keys(subModules)
  const currentDate = new Date(Date.now())
  return {
    survey_details: {
      survey_model: surveyModel,
      time: currentDate.toISOString(),
      uuid: uuid.v4(),
      project_id: projectId,
      protocol_id: allSubs.includes(protId) ? subModules[protId] : protId,
      protocol_version: '1',
      submodule_protocol_id: allSubs.includes(protId) ? protId : '',
    },
    provenance: {
      version_app: '0.0.1-xxxxx',
      version_core: '0.0.1-xxxxx',
      version_core_documentation: '0.0.1-xxxxx',
      version_org: '4.4-SNAPSHOT',
      system_app: 'monitor-test',
      system_core: 'Monitor FDCP API-test',
      system_org: 'monitor-test',
    },
    //TODO vary this in different tests so we can test uniqueness in surveys without getting collisions here (we want collision in other places to test)
    orgMintedUUID: uuid.v4(),
  }
}

export function prettyFormatFieldName(fieldName) {
  // the regex replaces all underlines and hyphens with spaces
  // The loop capitalises the first letter of every word
  let out = ''
  for (const word of fieldName.replace(/(-|_)/g, ' ').split(' ')) {
    out = `${out} ${word[0].toUpperCase()}${word.substring(1)}`
  }
  // remove leading space
  return out.trim()
}

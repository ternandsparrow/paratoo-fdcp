export const FORMAT = {
  DATE_TIME: 'DATE_TIME',
  EMAIL: 'EMAIL',
  FLOAT: 'FLOAT',
}

export const PROPERTY_TYPE = {
  ENUM: 'ENUM',
  UNIQUE: 'UNIQUE',
  MINIMUM: 'minimum',
  MAXIMUM: 'maximum',
  MINLENGTH: 'minLength',
  MAXLENGTH: 'maxLength',
  MINITEMS: 'minItems',
  MAXITEMS: 'maxItems',
}

export const FIELD_TYPE = {
  STRING: 'STRING',
  INTEGER: 'INTEGER',
  BOOLEAN: 'BOOLEAN',
  NUMBER: 'NUMBER',
  // components are just objects or array of objects
  NON_REPEATABLE_COMPONENT: 'OBJECT',
  REPEATABLE_COMPONENT: 'ARRAY',
}

export const INVALID_VALUE = {
  STRING: 111,
  INTEGER: 'invalidInt',
  BOOLEAN: 'invalidBool',
  ENUM: 'invalidEnum',
  DATE_TIME: 'invalidDateTime',
  FLOAT: 'invalidFloat',
}

export const CUSTOM_RULE_TYPES = {
  multipleValuesMinimumCheck: 'multipleValuesMinimumCheck',
  OneDecimalPlaceCheck: 'OneDecimalPlaceCheck',
  maximumDifferenceCheck: 'maximumDifferenceCheck',
  ConditionalFieldsCheck: 'ConditionalFieldsCheck',
  minNumOfReplicates: 'minNumOfReplicates',
  distanceBetweenReplicates: 'distanceBetweenReplicates',
}

export const PDP_TYPE = {
  READ: 'read',
  WRITE: 'write'
}

export const REQUEST_TYPE = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE'
}

export const API = {
  USER_PROJECT: '<org>/user-projects',
  PLOT_SELECTION: '<org>/plot-selections',
  PROJECT: '<org>/projects'
}

export const NON_CUSTOM_API = ['<org>/plot-selections', '<org>/projects']
export const PLOT_MODELS = ['plot-layout', 'plot-visit', 'plot-selection']

export const TEST_JWT =
  'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjUxNDcwOTc5LCJleHAiOjE2NTQwNjI5Nzl9.LIDkASm6kbWfR9FbV5Bp48uPkEm3Dw4z0Q9oEoCggXc'

export const AJV_VALIDATOR_TEST_PROTOCOLS = [
  '1de5eed1-8f97-431c-b7ca-a8371deb3c28', // Soil Site Observation and Pit Characterisation
  '15ea86ab-22f6-43fa-8cd5-751eab2347ad', // Soil Sample Pit
  '068d17e8-e042-ae42-1e42-cff4006e64b0', // opportune
  '82463b77-aac2-407c-a03c-8669bd73baf0', // Invertebrate Fauna
  'cd2cbbc7-2f17-4b0f-91b4-06f46e9c90f2', // veg mapping
  '4b8b35c7-15ef-4abd-a7b2-2f4e24509b52', // basal wedge
  '5005b0af-4360-4a8c-a203-b2c9e440547e', // basal dbh
]

export const CUSTOM_RULES_VALIDATOR_TEST_PROTOCOLS = [
  '1dd7d3ff-11b5-4690-8167-d8fe148656b9', // Soil Sub-pit and Metagenomics
  '15ea86ab-22f6-43fa-8cd5-751eab2347ad', // Soil Sample Pit
  '4b8b35c7-15ef-4abd-a7b2-2f4e24509b52', // basal wedge
  '5005b0af-4360-4a8c-a203-b2c9e440547e', // basal dbh
  'e15db26f-55de-4459-841b-d7ef87dea5cd', // Floristics - Enhanced
  '068d17e8-e042-ae42-1e42-cff4006e64b0', // opportune
  '82463b77-aac2-407c-a03c-8669bd73baf0', // Invertebrate Fauna
  'cd2cbbc7-2f17-4b0f-91b4-06f46e9c90f2', // veg mapping
]

export const PDP_DATA_FILTER_MIDDLEWARE_PROTOCOLS = [
  '068d17e8-e042-ae42-1e42-cff4006e64b0', // opportune (non plot based bulk)
  'c1b38b0f-a888-4f28-871b-83da2ac1e533', // bird survey (plot based bulk)
  'e15db26f-55de-4459-841b-d7ef87dea5cd', // Floristics - Enhanced (plot based bulk with voucher)
  'd706fd34-2f05-4559-b738-a65615a3d756', // Fauna Ground Counts (plot based bulk with child observation)
  '58f2b4a6-6ce1-4364-9bae-f96fc3f86958', // Cover + Fire - Standard (plot based bulk with subModuleStep)
]

export const PDP_DATA_FILTER_PLOT_SELECTION_PROTOCOLS = [
  'd7179862-1be3-49fc-8ec9-2e219c6f3854', // Plot Layout and Visit
  'a9cb9e38-690f-41c9-8151-06108caf539d', // Plot Selection and Layout
]

export const PDP_DATA_FILTER_MIDDLEWARE_PROJECTS = ['1', '4']

export const SUB_MODULES_RELATION = {
  // "Cover + Fire - standard
  '58f2b4a6-6ce1-4364-9bae-f96fc3f86958':
    '36e9d224-a51f-47ea-9442-865e80144311',
}

//require('dotenv').config({ path: './tests/helpers/.env' })
//console.log(process.env);
const Strapi = require('@strapi/strapi')
const fs = require('fs')

jest.setTimeout(1000000)

let instance
const setupStrapi = async () => {
  if (!instance) {
    process.env.ENABLE_CACHE=true
    await Strapi().load()
    instance = strapi
    // console.log(instance)
    global.strapiServer = instance.server.app.callback()
    await instance.server.mount()
    process.env.ENABLE_CACHE=false
  }
  return instance
}
const cleanupStrapi = async () => {
  const dbSettings = strapi.config.get('database.connection')

  //close server to release the db-file
  await strapi.server.httpServer.close()

  // close the connection to the database before deletion
  await strapi.db.connection.destroy()

  //delete test database after all tests have completed
  if (dbSettings && dbSettings.connection && dbSettings.connection.filename) {
    const tmpDbFile = dbSettings.connection.filename
    if (fs.existsSync(tmpDbFile)) {
      fs.unlinkSync(tmpDbFile)
    }
  }
}
const restartStrapi = async () => {
  // we're not using strapi.destroy()
  // because it will destroy the tmp database
  await strapi.server.destroy()
  await strapi.runLifecyclesFunctions('destroy')
  strapi.eventHub.removeAllListeners()
  strapi.telemetry.destroy()
  strapi.cron.destroy()
  //close server to release the db-file
  await strapi.server.httpServer.close()

  // close the connection to the database before deletion
  await strapi.db.connection.destroy()
  delete global.strapi

  await Strapi().load()
  instance = strapi
  // console.log(instance)
  await instance.server.mount()

  return instance
}

module.exports = { setupStrapi, restartStrapi, cleanupStrapi }

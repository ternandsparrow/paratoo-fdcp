const request = require('supertest')
const { cloneDeep } = require('lodash')
const { registerMockUris } = require('../helpers/mockAdapter')
const mock = require('./mock.json')

/*
  Description: Test the ability of Core to POST a collection of Soil Sub-pit and Metagenomics survey using the bulk() method 
  For soil protocols, we perform the field variant first , then lab later on the same protocol end point
  And the two payloads will have relations to each other
  eg.: Soil sub pit metagenomics
  Field: survey <- soil_sub_pit <- soil_sub_pit_horizon
            ↑            ↑
  Lab: survey <- soil-sub-pit-sampling
  So we need to extract the survey id and soil_sub_pit id in the first test
*/
let fieldSurveyId
let subPitId
test('Soil-Sub-pit-and-Metagenomics-full-POST-bulk-field-survey-001: should successfully POST a collection of Soil Sub-pit and Metagenomics full surveys to Core', async () => {
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs1),
    mock.projectId1,
    mock.protocolId1,
    mock.orgMintedUuid1,
  )
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(mock.body1)
    .then((data) => {
      console.log('returned data 001 ', data.body)
      console.log('returned status 001 ', data.status)
      fieldSurveyId = data.body[0]['soil-sub-pit-and-metagenomics-survey'].id
      subPitId = data.body[0]['soil-sub-pit'][0].id
      expect(data.status).toBe(200)
    })
})

test('Soil-Sub-pit-and-Metagenomics-full-POST-bulk-lab-survey-002: should successfully POST a collection of Soil Sub-pit and Metagenomics full surveys to Core', async () => {
  // register apis with axios mock adapter
  registerMockUris(
    cloneDeep(mock.apiConfigs1),
    mock.projectId1,
    mock.protocolId1,
    mock.orgMintedUuid2,
  )

  const requestBody = mock.body2
  // replace the relation with the id we retrieve above
  requestBody.data.collections[0][
    'soil-sub-pit-and-metagenomics-survey'
  ].data.field_survey_id = fieldSurveyId
  requestBody.data.collections[0][
    'soil-sub-pit-sampling'
  ][0].data.soil_sub_pit = subPitId

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post(mock.bulkEndpoint1)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('authorization', mock.validJwt1)
    .send(requestBody)
    .then((data) => {
      console.log('returned data 002 ', data.body)
      console.log('returned status 002 ', data.status)
      expect(data.status).toBe(200)
    })
})

/////////////////////////////////////////////////// stale and redundant tests (#1947 & #1949) ///////////////////////////////


// const request = require('supertest')
// import axios from 'axios'

// jest.mock('axios')

// var test01_jwt =
//   'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjUxNDcwOTc5LCJleHAiOjE2NTQwNjI5Nzl9.LIDkASm6kbWfR9FbV5Bp48uPkEm3Dw4z0Q9oEoCggXc'

// var pdp_returned_val = {
//   data: {
//     isAuthorised: true,
//   },
// }

// var validation_true = { data: true }

// /*
//   Test_ID: Veg-Mapping-DELETE-001
//   Description: Test the ability of Core to successfully delete a vagetation mapping entry
// */
// test('Veg-Mapping-DELETE-001: Test the ability of Core to successfully delete a Vegetation Mapping survey entry', async () => {
//   axios.post.mockResolvedValueOnce(validation_true)
//   axios.get.mockResolvedValueOnce(pdp_returned_val)
//   await request(global.strapiServer) // app server is an instance of Class: http.Server
//     .delete(`/api/vegetation-mapping-observations/1?auth=${test01_jwt}`)
//     .set('accept', 'application/json')
//     .set('Content-Type', 'application/json')
//     .then((data) => {
//       console.log('Veg-Mapping-DELETE-001 returned data: ', data.body)
//       expect(data.status).toBe(200)
//       expect(data.text).toBeDefined()
//     })
// })

// /*
//   Test_ID: Veg-Mapping-DELETE-002
//   Description: Test the ability of Core to successfully delete a vagetation mapping entry
// */
// test('Veg-Mapping-DELETE-002: Failure to delete an individual vagetation mapping when the User is not authenticated', async () => {
//   axios.post.mockResolvedValueOnce(validation_true)
//   axios.get.mockResolvedValueOnce(pdp_returned_val)
//   await request(global.strapiServer) // app server is an instance of Class: http.Server
//     .delete(`/api/vegetation-mapping-observations/2`)
//     .set('accept', 'application/json')
//     .set('Content-Type', 'application/json')
//     //.set('Authorization', 'Bearer ' + 'i_am_a_random_jwt')
//     .then((data) => {
//       console.log('Veg-Mapping-DELETE-002 returned data: ', data.body)
//       expect(data.body.error.status).toBe(401)
//       expect(data.body.error.message).toBe(
//         'User is not authenticated (logged in) - no auth token supplied',
//       )
//     })
// })

// /*
//   Test_ID: Veg-Mapping-DELETE-003
//   Description: Test the ability of Core to gracefully fail deleting an individual vagetation mapping when the supplied ID does not correspond to a stored vegetation mapping
// */
// test('Veg-Mapping-DELETE-003: Failure to delete an individual vagetation mapping when the supplied ID does not exist', async () => {
//   axios.post.mockResolvedValueOnce(validation_true)
//   axios.get.mockResolvedValueOnce(pdp_returned_val)
//   await request(global.strapiServer) // app server is an instance of Class: http.Server
//     .delete(`/api/vegetation-mapping-observations/100?auth=${test01_jwt}`)
//     .set('accept', 'application/json')
//     .set('Content-Type', 'application/json')
//     .then((data) => {
//       console.log('Veg-Mapping-DELETE-003 returned data: ', data.body)
//       expect(data.body.error.status).toBe(404)
//       expect(data.body.error.message).toBe('Not Found')
//     })
// })

// /*
//   Test_ID: Veg-Mapping-DELETE-004
//   Description: Test the ability of Core to gracefully fail deleting an individual vagetation mapping when the supplied ID is not a valid parameter
// */
// test('Veg-Mapping-DELETE-004: Failure to delete an individual vagetation mapping when the supplied ID is not a valid parameter', async () => {
//   axios.post.mockResolvedValueOnce(validation_true)
//   axios.get.mockResolvedValueOnce(pdp_returned_val)
//   await request(global.strapiServer) // app server is an instance of Class: http.Server
//     .delete(`/api/vegetation-mapping-observations/foo?auth=${test01_jwt}`)
//     .set('accept', 'application/json')
//     .set('Content-Type', 'application/json')
//     .then((data) => {
//       console.log('Veg-Mapping-DELETE-004 returned data: ', data.body)
//       expect(data.body.error.status).toBe(400)
//       expect(data.body.error.message).toBe(
//         `Invalid: query parameter 'id' with value 'foo' is not an integer`,
//       )
//     })
// })

// /*
//   Test_ID: Veg-Mapping-DELETE-005
//   Description: Test the ability of Core to gracefully fail deleting an individual vagetation mapping when no ID is supplied (no path parameters)
// */
// test('Veg-Mapping-DELETE-005:Failure to delete an individual vagetation mapping when no ID is supplied', async () => {
//   axios.post.mockResolvedValueOnce(validation_true)
//   axios.get.mockResolvedValueOnce(pdp_returned_val)
//   await request(global.strapiServer) // app server is an instance of Class: http.Server
//     .delete(`/api/vegetation-mapping-observations?auth=${test01_jwt}`)
//     .set('accept', 'application/json')
//     .set('Content-Type', 'application/json')
//     .then((data) => {
//       console.log('Veg-Mapping-DELETE-005 returned data: ', data.body)
//       expect(data.status).toBe(405)
//       expect(data.text).toBe('Method Not Allowed')
//     })
// })

#!/bin/bash
echo "Running Paratoo Core's Jest Unit Tests"
export ORG_URL_PREFIX="abc123"
export ADMIN_PASS=sadsadasdasds
export ADMIN_EMAIL=tasdasdaa@asdadsa.com.au
# export ENABLE_CACHE=true
export CACHE_REDIS_URL="redis://localhost:6379"

cleanup_redis() {
  docker rm -f $(docker ps -f name=redis -aq)
}

# bring up a redis instance in local environment
if [ -z $CI ]; then
  echo "Starting Redis"
  docker rm -f $(docker ps -f name=redis -aq)
  trap cleanup_redis EXIT
  docker run -d --name redis -p 6379:6379 redis:7.2.4
fi

yarn jest --forceExit --coverage --coverageReporters=cobertura --silent=false

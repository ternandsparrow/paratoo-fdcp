FROM node:18.20.6-alpine3.21 AS with_deps

ENV NODE_OPTIONS="--max-old-space-size=4096"

ARG NODE_ENV
ARG BRANCH_NAME
ARG COMMIT_SHORT_SHA

ENV NODE_ENV=${NODE_ENV:-production} \
  ALT_YARN_RC_FILENAME=docker.yarnrc.yml \
  BRANCH_NAME=$BRANCH_NAME \
  GIT_HASH=$COMMIT_SHORT_SHA

WORKDIR /app
ADD package.json yarn.lock .yarnrc.yml setup-docker.sh ./
ADD .yarn ./.yarn
# the strapi-admin build needs this file
ADD config/ config/
ADD public/ public/
ADD src src
ADD entrypoint-docker.sh entrypoint-docker.sh
COPY env.example .env

RUN touch .env-temp
RUN export VERSION=$(grep '"version"' ./package.json | cut -d '"' -f 4 | head -n 11) \
  && echo "GIT_HASH=${GIT_HASH}" > .env-temp \
  && echo "VERSION=${VERSION}" >> .env-temp
RUN cat .env-temp

# we need git to install at least one npm dependency
# apparently, better-sqlite3 needs python
RUN apk add --no-cache --virtual build-deps git python3 make g++

RUN sh setup-docker.sh

# don't need git any more
RUN apk del build-deps

ADD healthCheck.js ./
# TODO: workout a optimal start period 600s is a bit long
# but too fast will cause the healthcheck to fail
HEALTHCHECK --interval=15s --timeout=5s --start-period=600s \
  CMD node /app/healthCheck.js

FROM with_deps
EXPOSE 1337

#RUN chown -R `stat -c "%u:%g" /app` /app/.yarn

# the 'guest' user is built in to Alpine
USER guest
ADD --chown=guest:users . ./

# set $HOME so yarn commands don't complain about not finding a config file:
#    Error: ENOTDIR: not a directory, open '/dev/null/.config/yarn'
ENV HOME=/tmp \
  YARN_RC_FILENAME=$ALT_YARN_RC_FILENAME
CMD [ "sh", "entrypoint-docker.sh" ]

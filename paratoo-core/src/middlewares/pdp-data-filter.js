module.exports = (config, { strapi }) => {
  return async (ctx, next) => {
    await next()
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const handler = ctx.state.route.handler
    const authToken = ctx.request.headers['authorization']

    // for internal use we don't need to use middleware e.g. data exporter, cypress test.
    // NOTE: strapi(default population) adds extra 'data' field
    // if token is an api token with full access
    const status = await helpers.apiTokenCheck(authToken)
    if (status?.type == 'full-access') return
    const useCache = ctx.request.query['use-cache'] === 'true'

    const modelName = handler.substring(0, handler.lastIndexOf('.'))
    const skips = ['protocol']

    if (skips.includes(modelName) || modelName.includes('lut')) return

    const projectIds = helpers.parseGetReqUrlParam({
      queryParams: ctx.request.query,
      paramKeySingular: 'project_id',
    })
    const protocolUuids = helpers.parseGetReqUrlParam({
      queryParams: ctx.request.query,
      paramKeySingular: 'prot_uuid',
    })
    
    // handle 3 types of requests
    // 1. if one project id and protocol id present then pdf will verify
    // 2. if multiple project ids and multiple prot uuids exist
    //    then middleware will use user-project to verify and populate these projects only
    // 3. if no project id or prot uuid provided then middleware will populate all
    //    all project ids and protocol uuids in user-projects
    const result = await helpers.filterOutgoingData(
      modelName,
      authToken,
      projectIds,
      protocolUuids,
      useCache,
    )
    ctx.send(result)
  }
}

'use strict'

/**
 * sign-based-nearby-track-plot service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::sign-based-nearby-track-plot.sign-based-nearby-track-plot')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/sign-based-nearby-track-plots',
      'handler': 'sign-based-nearby-track-plot.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/sign-based-nearby-track-plots/:id',
      'handler': 'sign-based-nearby-track-plot.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/sign-based-nearby-track-plots',
      'handler': 'sign-based-nearby-track-plot.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/sign-based-nearby-track-plots/:id',
      'handler': 'sign-based-nearby-track-plot.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/sign-based-nearby-track-plots/:id',
      'handler': 'sign-based-nearby-track-plot.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
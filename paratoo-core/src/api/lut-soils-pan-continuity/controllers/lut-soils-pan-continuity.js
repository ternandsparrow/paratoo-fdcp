const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-pan-continuity.lut-soils-pan-continuity')
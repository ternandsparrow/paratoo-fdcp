module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-pan-continuities',
      'handler': 'lut-soils-pan-continuity.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-pan-continuities/:id',
      'handler': 'lut-soils-pan-continuity.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
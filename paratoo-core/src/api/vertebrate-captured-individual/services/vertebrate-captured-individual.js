'use strict'

/**
 * vertebrate-captured-individual service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::vertebrate-captured-individual.vertebrate-captured-individual')

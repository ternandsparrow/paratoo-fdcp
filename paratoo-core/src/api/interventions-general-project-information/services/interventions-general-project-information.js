'use strict'

/**
 * intervention-general-project-information service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::interventions-general-project-information.interventions-general-project-information')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/interventions-general-project-informations',
      'handler': 'interventions-general-project-information.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/interventions-general-project-informations/:id',
      'handler': 'interventions-general-project-information.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/interventions-general-project-informations',
      'handler': 'interventions-general-project-information.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/interventions-general-project-informations/:id',
      'handler': 'interventions-general-project-information.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/interventions-general-project-informations/:id',
      'handler': 'interventions-general-project-information.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/interventions-general-project-informations/bulk',
      'handler': 'interventions-general-project-information.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
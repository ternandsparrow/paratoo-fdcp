module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/herbivory-off-plot-transects',
      'handler': 'herbivory-off-plot-transect.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/herbivory-off-plot-transects/:id',
      'handler': 'herbivory-off-plot-transect.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/herbivory-off-plot-transects',
      'handler': 'herbivory-off-plot-transect.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/herbivory-off-plot-transects/:id',
      'handler': 'herbivory-off-plot-transect.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/herbivory-off-plot-transects/:id',
      'handler': 'herbivory-off-plot-transect.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * herbivory-off-plot-transect service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::herbivory-off-plot-transect.herbivory-off-plot-transect')

'use strict'

/**
 * drone-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::drone-survey.drone-survey')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-evaluation-means',
      'handler': 'lut-soils-evaluation-means.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-evaluation-means/:id',
      'handler': 'lut-soils-evaluation-means.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
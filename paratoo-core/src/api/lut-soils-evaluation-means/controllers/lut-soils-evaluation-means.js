const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-evaluation-means.lut-soils-evaluation-means')
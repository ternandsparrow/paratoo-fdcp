'use strict'

/**
 *  lut-aerial-survey-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-aerial-survey-type.lut-aerial-survey-type')

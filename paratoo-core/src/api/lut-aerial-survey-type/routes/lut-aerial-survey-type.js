module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-aerial-survey-types',
      'handler': 'lut-aerial-survey-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-aerial-survey-types/:id',
      'handler': 'lut-aerial-survey-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-aerial-survey-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-aerial-survey-type.lut-aerial-survey-type')

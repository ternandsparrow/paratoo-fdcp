'use strict'

/**
 *  lut-condition-grazing-severity controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController(
  'api::lut-condition-grazing-severity.lut-condition-grazing-severity',
)

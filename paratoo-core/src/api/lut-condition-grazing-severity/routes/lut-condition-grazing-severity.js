module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-condition-grazing-severities',
      'handler': 'lut-condition-grazing-severity.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-condition-grazing-severities/:id',
      'handler': 'lut-condition-grazing-severity.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
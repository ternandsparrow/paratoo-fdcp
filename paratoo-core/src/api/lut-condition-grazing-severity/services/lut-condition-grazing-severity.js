'use strict'

/**
 * lut-condition-grazing-severity service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService(
  'api::lut-condition-grazing-severity.lut-condition-grazing-severity',
)

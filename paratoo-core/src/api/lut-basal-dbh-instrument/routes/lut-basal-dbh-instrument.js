module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-basal-dbh-instruments',
      'handler': 'lut-basal-dbh-instrument.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-basal-dbh-instruments/:id',
      'handler': 'lut-basal-dbh-instrument.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-basal-dbh-instrument.lut-basal-dbh-instrument')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-effervescences',
      'handler': 'lut-soils-effervescence.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-effervescences/:id',
      'handler': 'lut-soils-effervescence.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
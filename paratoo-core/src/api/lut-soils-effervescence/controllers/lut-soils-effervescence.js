const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-effervescence.lut-soils-effervescence')
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-relative-inclination-of-slope-elements.lut-soils-relative-inclination-of-slope-elements')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-relative-inclination-of-slope-elements',
      'handler': 'lut-soils-relative-inclination-of-slope-elements.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-relative-inclination-of-slope-elements/:id',
      'handler': 'lut-soils-relative-inclination-of-slope-elements.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
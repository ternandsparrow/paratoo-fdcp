module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-bird-survey-types',
      'handler': 'lut-fauna-bird-survey-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-bird-survey-types/:id',
      'handler': 'lut-fauna-bird-survey-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
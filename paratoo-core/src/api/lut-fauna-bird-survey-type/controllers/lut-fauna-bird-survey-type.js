const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-bird-survey-type.lut-fauna-bird-survey-type')
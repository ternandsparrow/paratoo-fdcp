'use strict'

/**
 * invertebrate-rapid-ground-trapping service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::invertebrate-rapid-ground-trapping.invertebrate-rapid-ground-trapping')

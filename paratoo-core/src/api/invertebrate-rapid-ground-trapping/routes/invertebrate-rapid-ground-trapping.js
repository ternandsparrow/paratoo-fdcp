module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/invertebrate-rapid-ground-trappings',
      'handler': 'invertebrate-rapid-ground-trapping.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/invertebrate-rapid-ground-trappings/:id',
      'handler': 'invertebrate-rapid-ground-trapping.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/invertebrate-rapid-ground-trappings',
      'handler': 'invertebrate-rapid-ground-trapping.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/invertebrate-rapid-ground-trappings/:id',
      'handler': 'invertebrate-rapid-ground-trapping.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/invertebrate-rapid-ground-trappings/:id',
      'handler': 'invertebrate-rapid-ground-trapping.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/invertebrate-rapid-ground-trappings/bulk',
      'handler': 'invertebrate-rapid-ground-trapping.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-segregations-form.lut-soils-segregations-form')
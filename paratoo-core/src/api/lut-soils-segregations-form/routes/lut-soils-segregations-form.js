module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-segregations-forms',
      'handler': 'lut-soils-segregations-form.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-segregations-forms/:id',
      'handler': 'lut-soils-segregations-form.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
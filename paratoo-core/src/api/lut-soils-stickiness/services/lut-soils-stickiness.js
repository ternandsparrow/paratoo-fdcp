'use strict'

/**
 * lut-soils-stickiness service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soils-stickiness.lut-soils-stickiness')

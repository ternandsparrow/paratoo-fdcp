'use strict'

/**
 *  lut-soils-stickiness controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-stickiness.lut-soils-stickiness')

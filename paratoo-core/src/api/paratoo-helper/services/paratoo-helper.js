'use strict'

const { amendFullApiDoc } = require('../functions/amendDocumentation')
const { createAdminUser } = require('../functions/createAdminUser')
const { amendAPIRoutes } = require('../functions/amendAPIRoutes')
const { genericBulk } = require('../functions/bulk')
const { genericMany } = require('../functions/many')
const { paratooErrorHandler, paratooWarnHandler } = require('../functions/paratooErrorHandler')
const { paratooDebugMsg } = require('../functions/paratooDebugMsg')
const { underscoreToDash } = require('../functions/underscoreToDash')
const { prettyFormatFieldName, serialisePlotName } = require('../functions/prettyFormatFieldName')
const { readFullDocumentation } = require('../functions/readFullDocumentation')
const { formatApiEndPoint } = require('../functions/formatApiEndPoint')
const { internalValidator, applyCustomRules, getValueFromData, isNullOrEmpty } = require('../functions/internalValidator')
const { syncPlotSelection } = require('../functions/syncPlotSelection')
const { sendCollectionIdentifierToOrg } = require('../functions/sendCollectionIdentifierToOrg')
const { createCollectionBody } = require('../functions/createCollectionBody')
const { modelToApiName, 
  modelAssociations, 
  findAssociationAttributeName,
  generatePlotDataFromPlotSelection,
  findWorkflowUsingProtocolUUID,
  getSurveyMetadataCollections,
  surveyMetadataFromOrgUuid,
  findCollectionIdsGivenProjectIdOrProtocolId,
  deepPopulateFromSchema,
  populateAttribute,
  getAllUserModels,
  pushMockCollections,
  sendFetchWithOption,
  updateMockData,
  getAuthToken,
  deepPopulateQuery,
  findModelAndChildObsFromProtocolUUID,
  findParentModelFromChildModel,
  generateDummyData,
  getAllProtocols,
  getAllUserProjects,
  isPlotBasedProtocol,
  parseGetReqUrlParam,
  getOrgUuids,
  abisExportCollections,
  isExporterRunning,
} = require('../functions/modelFunctions')
const { getRequestParams } = require('../functions/getRequestParams')
const { filterSurveys, filterObservations, parseSpecifiedProjects, handleProjectPlotFilteringIfNecessary } = require('../functions/projectPlotFilters')
const { orgPostRequestWithBody, orgPutRequestWithBody, orgGetRequest, parseResponseData } = require('../functions/orgApiRequest')
const { getSentryService } = require('../functions/getSentryService')
const { createSentryBreadcrumb } = require('../functions/createSentryBreadcrumb')
const { validString, validInt, validBoolean } = require('../functions/parseEnv')
const {
  filterDataUsingProjectIdAndProtUuid,
  filterOutgoingData,
  updateDataByProjectProperty,
  pdpDataFilter,
} = require('../functions/pdpDataFilter')
const {
  redisConnect,
  redisGet,
  redisSet,
  createMd5Hash,
  getTimeToLive,
} = require('../functions/redisClient')
const { apiTokenCheck } = require('../functions/apiToken')
const { findValForKey } = require('../functions/findValForKey')

const webPush = require('../functions/webPush')
const publishCollection = require('../functions/publishCollection')
const schedulePublishCollection = require('../functions/schedulePublishCollection')

/**
 * paratoo-helper service.
 */

module.exports = () => ({
  amendDocumentation: amendFullApiDoc,
  createAdminUser,
  amendAPIRoutes,
  genericBulk,
  genericMany,
  paratooErrorHandler,
  paratooWarnHandler,
  paratooDebugMsg,
  underscoreToDash,
  prettyFormatFieldName,
  serialisePlotName,
  readFullDocumentation,
  formatApiEndPoint,

  //modelFunctions function:
  modelToApiName,
  modelAssociations,
  findAssociationAttributeName,
  findWorkflowUsingProtocolUUID,
  getSurveyMetadataCollections,
  surveyMetadataFromOrgUuid,
  findCollectionIdsGivenProjectIdOrProtocolId,
  deepPopulateFromSchema,
  populateAttribute,
  getAllUserModels,
  deepPopulateQuery,
  findModelAndChildObsFromProtocolUUID,
  findParentModelFromChildModel,
  generateDummyData,
  getAllProtocols,
  getAllUserProjects,
  isPlotBasedProtocol,
  parseGetReqUrlParam,
  getOrgUuids,
  abisExportCollections,
  isExporterRunning,
  
  // for plot selection
  generatePlotDataFromPlotSelection,

  getRequestParams,

  //projectPlotFilters functions:
  filterSurveys,
  filterObservations,
  parseSpecifiedProjects,
  handleProjectPlotFilteringIfNecessary,

  orgPostRequestWithBody,
  orgPutRequestWithBody,
  orgGetRequest,
  parseResponseData,
  internalValidator,
  applyCustomRules,
  getValueFromData,
  syncPlotSelection,
  sendCollectionIdentifierToOrg,
  createCollectionBody,
  isNullOrEmpty,
  pushMockCollections,
  sendFetchWithOption,
  updateMockData,
  getAuthToken,

  getSentryService,
  createSentryBreadcrumb,
  validString,
  validInt,
  validBoolean,

  filterDataUsingProjectIdAndProtUuid,
  filterOutgoingData,
  updateDataByProjectProperty,
  pdpDataFilter,

  redisConnect,
  redisGet,
  redisSet,
  createMd5Hash,
  getTimeToLive,

  apiTokenCheck,

  findValForKey,

  // web push notification
  webPush,
  publishCollection,
  schedulePublishCollection
})

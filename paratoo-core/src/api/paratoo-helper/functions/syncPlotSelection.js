const _ = require('lodash')

module.exports = {
  syncPlotSelection: async function (ctx, modelName, submittedPlotSelections) {
    if (!modelName || submittedPlotSelections.length == 0) return
    if (modelName != 'plot-selection') return

    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    // submitted uuids to find valid plot selection to sync with org
    const submittedUuids = submittedPlotSelections.map((p) => p.uuid)
    const results = []

    // request parameters
    const requestBody = ctx.request.body
    const authToken = ctx.request.headers['authorization']

    //if the below if/elseif doesn't hit any case, we keep this default `null` so the
    //function can return
    let plotSelections = null
    if (requestBody?.data?.collections?.[0]?.['plot-selection']) {
      plotSelections = requestBody.data.collections[0]['plot-selection']
    } else if (requestBody?.data?.['plot-selections']) {
      //backwards compatibility for the old approach using `many`
      helpers.createSentryBreadcrumb({
        message: 'Using old approach for getting plot selections for sync',
      })
      plotSelections = requestBody.data['plot-selections']
    }

    let projectAreas = null
    if (requestBody?.data?.collections?.[0]?.['plot-selection-survey']?.data?.['project-areas']) {
      projectAreas = requestBody.data.collections[0]['plot-selection-survey'].data['project-areas']
    } else if (requestBody?.data?.['project-areas']) {
      //backwards compatibility for the old approach using `many`
      helpers.createSentryBreadcrumb({
        message: 'Using old approach for getting project areas for sync',
      })
      projectAreas = requestBody.data['project-areas']
    }

    let msg = `
    About to attempt syncing Plot Selection: ${JSON.stringify(plotSelections,null,2)}.
    Project area(s): ${JSON.stringify(projectAreas)}.
    authToken: ${authToken}
    `
    helpers.paratooDebugMsg(msg, true)
    // if no plotSelections found we return
    if (!plotSelections) {
      return results
    }

    const allOrgPlotSelections = await helpers.orgGetRequest(
      'plot-selections',
      authToken,
      false,
    )
    const allUserProject = await helpers.getAllUserProjects(authToken)

    const existingPlotSelectionIdsInProjects = {}
    let projectsToUpdate = []
    //keep track of existing plots assigned to the projects that we want to update
    //assigned plots for
    if (
      allOrgPlotSelections?.data &&
      Array.isArray(allOrgPlotSelections.data) &&
      allUserProject?.data?.projects &&
      Array.isArray(allUserProject.data.projects) &&
      Array.isArray(projectAreas)
    ) {
      //iterate over all projects so that we can check for matches in `plotSelections`
      //and `projectAreas`
      for (const userProj of allUserProject.data.projects) {
        //need to check both new project areas and the plots to assign for project
        //references, as the user might provide one, both, or neither
        for (const plotSelection of plotSelections) {
          if (plotSelection?.data?.projects_for_plot) {
            const projectsToAssignPlot =
              plotSelection.data.projects_for_plot.map((p) => p.label)

            if (projectsToAssignPlot.some((pta) => pta === userProj.name)) {
              projectsToUpdate.push(userProj)
            }
          }
        }
        for (const projArea of projectAreas) {
          if (
            projArea?.project?.label &&
            projArea.project.label === userProj.name
          ) {
            projectsToUpdate.push(userProj)
          }
        }
      }
      //FIXME should probably just ensure we don't push duplicates, but this check is
      //being difficult to implement as we're dealing with arrays of arrays of objects
      projectsToUpdate = _.union(projectsToUpdate)

      for (const proj of projectsToUpdate) {
        existingPlotSelectionIdsInProjects[proj.name] =
          proj.plot_selections.reduce((accum, currPlotSelection) => {
            //return of /user-projects' plot selections doesn't have the plot
            //selection's ID, so need to resolve from existing plot selections stored
            //in org
            const currPlotSelectionId = allOrgPlotSelections.data.find(
              (p) => p.attributes.uuid === currPlotSelection.uuid,
            ).id
            accum.push(currPlotSelectionId)

            return accum
          }, [])
      }
    }

    // we need all plot ids and the associated projects
    //   so that we can update project's plot data later.
    // we clone so that we can still check `existingPlotSelectionIdsInProjects` - allows
    //   us to skip updating a project's plot selections if there are no *new* plots
    const plotSelectionIdsInProjects = _.cloneDeep(
      existingPlotSelectionIdsInProjects,
    )
    const orgCompletedPlotSelections = []
    for (let plotSelection of plotSelections) {
      if (!plotSelection) continue
      if (!submittedUuids.includes(plotSelection.data.uuid)) continue

      // sync new plot data with org
      const orgPlotSelectionData = _.cloneDeep(plotSelection.data)
      // as we dont want to add 'projects_for_plot' in plot selection
      if (Object.keys(orgPlotSelectionData).includes('projects_for_plot')) {
        delete orgPlotSelectionData.projects_for_plot
      }
      const resp = await helpers.orgPostRequestWithBody(
        'plot-selections',
        orgPlotSelectionData,
        authToken,
        true,
        false,
      )
      if (resp.status != 200) {
        return helpers.paratooErrorHandler(
          resp.status,
          //`orgPostRequestWithBody` saved the error object in the `data` property
          resp.data,
          'Failed to update plot data',
        )
      }

      // temporarily store plot id so that we can update project plot data
      const submittedPlotID = resp.data.id
      if (!submittedPlotID) {
        return helpers.paratooErrorHandler(
          500,
          //no error object, but the helper can use the message below
          null,
          'Failed to sync plot data with org',
        )
      }

      orgCompletedPlotSelections.push(resp)
      // after successful synchronization
      if (plotSelection?.data?.projects_for_plot) {
        plotSelection.data.projects_for_plot.map((project) => {
          // push new plot id to the associated projects
          plotSelectionIdsInProjects[project.label] =
            !plotSelectionIdsInProjects[project.label]
              ? [submittedPlotID]
              : [...plotSelectionIdsInProjects[project.label], submittedPlotID]
        })
      }
    }
    if (orgCompletedPlotSelections.length > 0) {
      results.push({
        'plot-selection-org': orgCompletedPlotSelections,
      })
    }

    const projectRequests = []
    for (const proj of projectsToUpdate) {
      projectRequests.push({
        projectName: proj.name,
        request: {},
        projectId: proj.id,
      })
    }

    //check for empty (i.e., empty object that it's initialised as) in case they don't
    //provide any plots to assign to projects
    if (!_.isEmpty(plotSelectionIdsInProjects)) {
      for (const [projectName, plotSelectionIdsArr] of Object.entries(
        plotSelectionIdsInProjects,
      )) {
        //we check if the combined existing and new plots (`plotSelectionIdsInProjects`)
        //for this project are different to the existing plots
        //(`existingPlotSelectionIdsInProjects`), as we don't want to update a projects
        //plots if there are not new plots to assign
        if (
          !_.isEqual(
            plotSelectionIdsArr,
            existingPlotSelectionIdsInProjects[projectName],
          )
        ) {
          for (const projReq of projectRequests) {
            if (projReq.projectName === projectName) {
              Object.assign(projReq.request, {
                //`plotSelectionIdsArr` is a combination of existing plots assigned, plus
                //the ones we want to add (as if we just include the ones we want to add,
                //the existing ones get squashed)
                plot_selections: plotSelectionIdsArr,
              })
            }
          }
        }
      }
    }

    for (const projectArea of projectAreas || []) {
      if (projectArea?.points && Array.isArray(projectArea.points)) {
        for (const projReq of projectRequests) {
          if (projectArea.project.label === projReq.projectName) {
            Object.assign(projReq.request, {
              project_area_type: projectArea?.project_area_type
                ? projectArea.project_area_type
                : 'polygon', //fallback to a default if they didn't provide it in the request
              project_area_coordinates: projectArea.points.map((p) => {
                return { lat: p.lat, lng: p.lng }
              }),
            })
          }
        }
      }
    }

    const completedProjectUpdates = []
    for (const req of projectRequests) {
      helpers.paratooDebugMsg(
        constructRequestDebugMsg(
          req,
          plotSelectionIdsInProjects,
          submittedPlotSelections,
        ),
        true,
      )

      const resp = await helpers.orgPutRequestWithBody(
        `projects/${req.projectId}`,
        req.request,
        authToken,
        false,
      )

      if (resp.status != 200) {
        return helpers.paratooErrorHandler(
          resp.status,
          //`orgPostRequestWithBody` saved the error object in the `data` property
          resp.data,
          'Failed to update project definition',
        )
      }

      completedProjectUpdates.push(resp.data)
    }

    if (completedProjectUpdates.length > 0) {
      results.push({
        'project-area-org': completedProjectUpdates,
      })
    }

    return results
  },
}

function constructRequestDebugMsg(
  req,
  plotSelectionIdsInProjects,
  submittedPlotSelections,
) {
  const plotSelectionIdsMappedToNames = (() => {
    if (!plotSelectionIdsInProjects[req.projectName]) {
      return null
    }

    const completed = []
    for (const orgPlotSelectionId of plotSelectionIdsInProjects[
      req.projectName
    ]) {
      const relevantPlotSelection = submittedPlotSelections.find(
        (o) => o.id === orgPlotSelectionId || o.uuid === orgPlotSelectionId,
      )
      if (relevantPlotSelection) {
        completed.push(
          `${relevantPlotSelection.plot_label} (uuid: ${relevantPlotSelection.uuid})`,
        )
      }
    }

    return completed.join(', ')
  })()
  let debugMsg = `Updating Org's project '${req.projectName}' with`
  if (req.request?.project_area_coordinates && req.request?.project_area_type) {
    debugMsg += ' new project area'
  }
  if (req.request?.plot_selections) {
    if (
      req.request?.project_area_coordinates &&
      req.request?.project_area_type
    ) {
      debugMsg += ` and new plots: ${plotSelectionIdsMappedToNames}`
    } else {
      debugMsg += ` new plots: ${plotSelectionIdsMappedToNames}`
    }
  }
  return debugMsg
}

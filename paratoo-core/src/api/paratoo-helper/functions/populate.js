const { createCoreController } = require('@strapi/strapi').factories

function populateAttribute({ components }) {
  if (components) {
    const populate = components.reduce((currentValue, current) => {
      let data = { ...currentValue, [current.split('.').pop()]: { populate: '*' } }
      return data
    }, {})
    return { populate }
  }
  return { populate: '*' }
}

function getPopulateFromSchema(schema) {
  return Object.keys(schema.attributes).reduce((currentValue, current) => {
    const attribute = schema.attributes[current]
    if (!['dynamiczone', 'component', 'relation'].includes(attribute.type)) {
      return currentValue
    }
    return {
      ...currentValue,
      [current]: populateAttribute(attribute),
    }
  }, {})
}

function createPopulatedController(uid, schema, bulk = null) {
  return createCoreController(uid, () => {
    return {
      async find(ctx) {
        ctx.query = {
          ...ctx.query,
          populate: getPopulateFromSchema(schema),
        }
        let data = await super.find(ctx)
        return data
      },
      async findOne(ctx) {
        ctx.query = {
          ...ctx.query,
          populate: getPopulateFromSchema(schema),
        }
        let data = await super.findOne(ctx)
        return data
      },
      bulk
    }
  })
}

module.exports = createPopulatedController
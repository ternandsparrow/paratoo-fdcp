'use strict'
const _ = require('lodash')
const { PLOT_MODELS } = require('../../../constants')

const WORKFLOWS = {}
const PROCESSED_PLOTS = {}

module.exports = {
  filterDataUsingProjectIdAndProtUuid: async function (
    modelName,
    projectIds,
    plotSelections,
    protUuid,
    protWorkflow,
  ) {
    let result = []
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    let workFlow = _.cloneDeep(protWorkflow)
    let plotVisitField = null
    let plotLayoutField = null
    let plotSelectionField = null

    const surveyDbWhere = {
      survey_metadata: {
        survey_details: {},
      },
    }
    const surveyDbPopulate = {
      survey_metadata: {
        populate: {
          survey_details: true,
        },
      },
    }
    // subModules are not linked to the protocol
    if (!workFlow.modelExtension && workFlow.subModules.includes(modelName))
      return result

    if (
      !workFlow.modelExtension &&
      !workFlow.allModels.includes(modelName) &&
      !workFlow.plotModels.includes(modelName)
    )
      return result
    if (workFlow.modelExtension && !workFlow.allModels.includes(modelName)) {
      // if special sub modules like cover and fire
      if (workFlow.modelExtension.allModels.includes(modelName)) {
        workFlow = _.cloneDeep(workFlow.modelExtension)
      }
    }
    const isPlotModel = workFlow.plotModels.includes(modelName)
    const isPlotBased = workFlow.plotModels.length > 0

    if (isPlotModel || isPlotBased) {
      plotVisitField = helpers.findAssociationAttributeName(
        workFlow['surveyModel'],
        helpers.modelToApiName('plot-visit'),
      )
      plotLayoutField = helpers.findAssociationAttributeName(
        'plot-visit',
        helpers.modelToApiName('plot-layout'),
      )
      plotSelectionField = helpers.findAssociationAttributeName(
        'plot-layout',
        helpers.modelToApiName('plot-selection'),
      )
    }

    // if survey includes project ids
    if (!isPlotModel) {
      surveyDbWhere.survey_metadata.survey_details['project_id'] = {
        $in: projectIds,
      }
    }
    surveyDbWhere.survey_metadata.survey_details['protocol_id'] = protUuid

    // if plot models
    if (isPlotModel && plotVisitField) {
      // plot models should be linked to the list of plot selections associated to the project
      surveyDbWhere[plotVisitField] = {
        id: { $notIn: PROCESSED_PLOTS.plotVisits },
        [plotLayoutField]: {
          [plotSelectionField]: {
            plot_label: { $in: plotSelections },
          },
        },
      }
      surveyDbPopulate[plotVisitField] = {
        populate: {
          [plotLayoutField]: {
            populate: {
              [plotSelectionField]: true,
            },
          },
        },
      }
    }
    strapi.log.debug(
      `querying model: ${
        workFlow['surveyModel']
      } with properties, where: ${JSON.stringify(
        surveyDbWhere,
      )}, populate: ${JSON.stringify(surveyDbPopulate)}`,
    )
    // populate survey data
    let surveyData = await helpers.deepPopulateQuery(
      workFlow['surveyModel'],
      _.cloneDeep(surveyDbWhere),
      _.cloneDeep(surveyDbPopulate),
      true,
    )
    // if the model is survey model then we return survey data
    if (workFlow['surveyModel'] == modelName) return surveyData

    // plot-visit can not be null in a plot-based survey
    if (plotVisitField) {
      let filteredSurveys = []
      for (const s of surveyData) {
        if (s[plotVisitField]) {
          filteredSurveys.push(s)
          continue
        }
        // should ignore surveys instead of returning 400 error
        let msg = `Found empty plot data in plot-based survey, model: ${
          workFlow['surveyModel']
        }, data: ${JSON.stringify(s)}`
        helpers.paratooWarnHandler(msg)
      }
      surveyData = filteredSurveys
    }
    const surveyIds = surveyData.map((a) => a.id)
    if (surveyIds.length == 0) {
      // handle spacial conditions. e.g. plot-selection
      const updatedValues = await helpers.updateDataByProjectProperty(
        modelName,
        {
          protUuid: protUuid,
          plotSelections: plotSelections,
        },
      )
      result = result.concat(updatedValues)
      return _.uniqBy(result, 'id')
    }

    // if not survey then we have to extract two kinds of modelData.
    // 1. models which have direct relationship with the surveys e.g. modelsNames from the protocol workflow
    // 2. child observation which don't have direct relationship with the surveys
    //    e.g. newInstanceForRelationOnAttributes/relationOnAttributesModelNames from the protocol workflow
    // 3. plot models are linked to the survey models if the protocol is plot based
    //    e.g. plot-visit, plot-layout and plot-selection
    // models which have direct relationship with the surveys
    if (workFlow['modelNames'].includes(modelName)) {
      const survey_field = helpers.findAssociationAttributeName(
        modelName,
        helpers.modelToApiName(workFlow['surveyModel']),
      )
      if (!survey_field) {
        strapi.log.warn(
          `survey_field is undefined model: ${modelName}, survey model: ${workFlow['surveyModel']}`,
        )
        return result
      }

      result = result.concat(
        await helpers.deepPopulateQuery(
          modelName,
          {
            [survey_field]: {
              id: { $in: surveyIds },
            },
          },
          {
            [survey_field]: true,
          },
          true,
        ),
      )
      return _.uniqBy(result, 'id')
    }

    // child observation which don't have direct relationship with the surveys
    if (workFlow['allModels'].includes(modelName)) {
      // extract parent model info using child model
      const parentModelInfo = helpers.findParentModelFromChildModel(
        modelName,
        workFlow,
      )
      if (!parentModelInfo) return []
      const survey_field = helpers.findAssociationAttributeName(
        parentModelInfo.name,
        helpers.modelToApiName(workFlow['surveyModel']),
      )
      if (!survey_field) {
        strapi.log.warn(
          `survey_field is undefined model: ${parentModelInfo.name}, survey model: ${workFlow['surveyModel']}`,
        )
        return result
      }
      const parentModelData = await helpers.deepPopulateQuery(
        parentModelInfo.name,
        {
          [survey_field]: {
            id: { $in: surveyIds },
          },
        },
        {
          [survey_field]: true,
        },
        true,
      )
      for (const pmd of parentModelData) {
        const childData = pmd[parentModelInfo.associatedField]
        // skip null values
        if (!childData) continue
        
        if (!Array.isArray(childData)) {
          result.push(childData)
          continue
        }
        result = result.concat(childData)
      }
      return _.uniqBy(result, 'id')
    }

    // plot models are linked to the survey models if the protocol is plot based
    if (workFlow['plotModels'].includes(modelName)) {
      // handle spacial conditions. e.g. plot-selection
      const updatedValues = await helpers.updateDataByProjectProperty(
        modelName,
        {
          protUuid: protUuid,
          plotSelections: plotSelections,
        },
      )
      result = result.concat(updatedValues)
      if (!plotVisitField) return _.uniqBy(result, 'id')

      const plotVisits = surveyData.map((a) => a[plotVisitField])

      // temporary stores plot visit ids so that we can query survey data
      // NOTE: reduces processing time substantially form 50 seconds to 500/600 ms
      const plotVisitIds = plotVisits.map((a) => a.id)
      PROCESSED_PLOTS.plotVisits =
        PROCESSED_PLOTS.plotVisits.concat(plotVisitIds)
      PROCESSED_PLOTS.plotVisits = _.union(PROCESSED_PLOTS.plotVisits)

      let selections = []
      let selectionsIds = []
      switch (modelName) {
        case 'plot-visit':
          result = result.concat(plotVisits)
          return _.uniqBy(result, 'id')
        case 'plot-layout':
          result = result.concat(
            surveyData.map((a) => a[plotVisitField][plotLayoutField]),
          )
          return _.uniqBy(result, 'id')
        case 'plot-selection':
          selections = surveyData.map(
            (a) => a[plotVisitField][plotLayoutField][plotSelectionField],
          )
          // temporary stores plot selection ids so that we can query survey data
          selectionsIds = selections.map((a) => a.id)
          PROCESSED_PLOTS.plotSelections =
            PROCESSED_PLOTS.plotSelections.concat(selectionsIds)
          PROCESSED_PLOTS.plotSelections = _.union(
            PROCESSED_PLOTS.plotSelections,
          )
          result = result.concat(selections)
          return _.uniqBy(result, 'id')
      }
    }

    return _.uniqBy(result, 'id')
  },

  updateDataByProjectProperty: async function (modelName, property = {}) {
    let values = []
    if (!modelName) return values

    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

    const protUuid = property.protUuid || ''
    const plotSelections = property.plotSelections || []
    const workFlow = _.cloneDeep(WORKFLOWS[protUuid])
    // protocols where we need all the plot selections assigned by the project
    const plot_surveys = ['plot-selection-survey', 'plot-definition-survey']
    switch (modelName) {
      // add plot selection if generic filter
      //   fails to find one that has been assigned to the project.
      case 'plot-selection':
        if (!workFlow['surveyModel']) return values
        if (!plot_surveys.includes(workFlow['surveyModel'])) return values

        values = await helpers.deepPopulateQuery(
          modelName,
          {
            id: { $notIn: PROCESSED_PLOTS.plotSelections },
            // uuid: { $in: plotSelections },
            plot_label: { $in: plotSelections },
          },
          null,
          true,
        )
        // temporary stores plot selection ids so that we can query survey data
        PROCESSED_PLOTS.plotSelections = PROCESSED_PLOTS.plotSelections.concat(
          values.map((a) => a.id),
        )
        PROCESSED_PLOTS.plotSelections = _.union(PROCESSED_PLOTS.plotSelections)
        return values
      default:
        return values
    }
  },

  filterOutgoingData: async function (
    modelName,
    authToken,
    project_ids = [],
    prot_uuids = [],
    useCache = true,
  ) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const hash = helpers.createMd5Hash(
      `${authToken}${modelName}${JSON.stringify(project_ids)}${JSON.stringify(
        prot_uuids,
      )}`,
    )
    if (useCache) {
      const data = await helpers.redisGet(hash)
      if (data) return data
    }

    let projectIds = Array.isArray(project_ids) ? project_ids : [project_ids]
    let protUuids = Array.isArray(prot_uuids) ? prot_uuids : [prot_uuids]
    let plotSelections = []
    let userProjects = null
    let result = []

    // if plot models, we store already processed plot ids so that we can query on survey data
    if (PLOT_MODELS.includes(modelName)) {
      PROCESSED_PLOTS['plotVisits'] = []
      PROCESSED_PLOTS['plotLayouts'] = []
      PROCESSED_PLOTS['plotSelections'] = []
    }

    userProjects = await helpers.getAllUserProjects(
      authToken,
      modelName,
      useCache,
    )
    if (!userProjects) return result
    if (!userProjects.data) return result
    if (!userProjects.data.projects) return result
    if (userProjects) {
      // all authorised project ids, protocols and plot selections
      let allAuthProjIds = []
      let allAuthProtUuids = []
      let allAuthPlotSelections = []
      for (const project of userProjects.data.projects) {
        const proj = _.cloneDeep(project)
        const id = proj.id.toString()
        allAuthProjIds.push(id)
        allAuthPlotSelections = allAuthPlotSelections.concat(
          proj.plot_selections.map((a) => a.name.toString()),
        )
        allAuthProtUuids = allAuthProtUuids.concat(
          proj.protocols.map((a) => a.identifier.toString()),
        )
        if (!projectIds.includes(id)) continue
        plotSelections = plotSelections.concat(
          proj.plot_selections.map((a) => a.name.toString()),
        )
      }
      allAuthProjIds = _.union(allAuthProjIds)
      allAuthPlotSelections = _.union(allAuthPlotSelections)
      allAuthProtUuids = _.union(allAuthProtUuids)
      plotSelections = _.union(plotSelections)

      if (projectIds.length == 0) {
        projectIds = allAuthProjIds
        protUuids = allAuthProtUuids
        plotSelections = allAuthPlotSelections
      }
    }

    strapi.log.debug(
      `Started filtering outgoing data, model: ${modelName}, projectIds: ${projectIds}, plotSelections: ${plotSelections}, total number of protocols: ${protUuids.length}`,
    )
    for (const protocol of protUuids) {
      // store workflow to reduce processing time
      let workFlow = WORKFLOWS[protocol]
      if (!workFlow) {
        WORKFLOWS[protocol] = _.cloneDeep(
          await helpers.findModelAndChildObsFromProtocolUUID(protocol),
        )
        workFlow = _.cloneDeep(WORKFLOWS[protocol])
      }
      result = result.concat(
        await helpers.filterDataUsingProjectIdAndProtUuid(
          modelName,
          projectIds,
          plotSelections,
          protocol,
          _.cloneDeep(workFlow),
        ),
      )
    }

    result = _.uniqBy(result, 'id')
    helpers.redisSet(hash, result, helpers.getTimeToLive('get'))
    return result
  },

  pdpDataFilter: async function (
    modelName,
    authToken,
    requestQuery,
    tokenStatus,
  ) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const skips = ['protocol']
    strapi.log.debug(
      `processing model: ${modelName}`,
    )
    // return all records
    // 1. if magic jwt is used (for internal use we don't need to use data filter e.g. data exporter, cypress test)
    // 2. models like 'protocols' or lut found
    if (
      tokenStatus?.type == 'full-access' ||
      skips.includes(modelName) ||
      modelName.includes('lut')
    ) {
      const api = `api::${modelName}.${modelName}`
      const records = await strapi.entityService.findMany(api, {
        populate: 'deep',
      })
      strapi.log.debug(
        `api token with full-access or magic jwt is used to populate model: ${modelName}, total record: ${records.length}`,
      )
      return records
    }

    const useCache = requestQuery['use-cache'] === 'true'
    const projectIds = helpers.parseGetReqUrlParam({
      queryParams: requestQuery,
      paramKeySingular: 'project_id',
    })
    const protocolUuids = helpers.parseGetReqUrlParam({
      queryParams: requestQuery,
      paramKeySingular: 'prot_uuid',
    })

    // handle 3 types of requests
    // 1. if one project id and protocol id present then pdpFilter will verify
    // 2. if multiple project ids and multiple prot uuids exist
    //    then pdpFilter will use user-project to verify and populate these projects only
    // 3. if no project id or prot uuid provided then pdpFIlter will populate all
    //    all project ids and protocol uuids in user-projects
    return await helpers.filterOutgoingData(
      modelName,
      authToken,
      projectIds,
      protocolUuids,
      useCache,
    )
  },
}

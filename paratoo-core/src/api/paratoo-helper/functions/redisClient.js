'use strict'
const redis = require('redis')
const crypto = require('crypto')

const REDIS = {}
module.exports = {
  // create redis client
  redisConnect: async function () {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    if (!helpers.validBoolean(process.env.ENABLE_CACHE)) {
      strapi.log.warn('Failed to connect Redis. Cache is disabled')
      return null
    }
    const redisUrl = helpers.validString(process.env.REDIS_URL)
    
    strapi.log.debug(`Trying to connect Redis with url: ${redisUrl}`)
    REDIS['client'] = redis.createClient({
      url: redisUrl,
    })
    REDIS.client
      .connect()
      .then(() => {
        strapi.log.info('Redis is connected')
      })
      .catch((err) => {
        strapi.log.error(
          `Failed to connect redis reason: ${JSON.stringify(err)}`,
        )
      })
  },
  // get cache value
  redisGet: async function (key) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    if (!helpers.validBoolean(process.env.ENABLE_CACHE)) {
      warnRedisDisabled()
      return null
    }
    const data = await REDIS.client.get(key)
    if (!data) return null

    strapi.log.debug(`Cache found key: ${key}`)
    return JSON.parse(data)
  },
  // set cache value
  redisSet: async function (key, value, ttl) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    if (!helpers.validBoolean(process.env.ENABLE_CACHE)) {
      warnRedisDisabled()
      return null
    }
    await REDIS.client.set(key, JSON.stringify(value), {
      EX: ttl,
    })

    strapi.log.debug(`Cache set key: ${key} ttl: ${ttl}`)
  },
  // create md5 hash
  createMd5Hash: function (value) {
    return crypto.createHash('md5').update(value).digest('hex')
  },
  // get defined time to live
  getTimeToLive: function (type) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    if (!helpers.validBoolean(process.env.ENABLE_CACHE)) {
      warnRedisDisabled()
      return null
    }
    // format: user_project:30,protocol:30,get:30
    const timeToLives = helpers.validString(process.env.REDIS_TTL).split(',')
    for (const ttl of timeToLives) {
      if (!ttl.includes(type)) continue
      const time = ttl.split(':')[1]
      strapi.log.debug(`Cache type: ${type} ttl: ${time}`)
      return time
    }
    // default 5(300s) minutes
    strapi.log.debug(`Cache type: ${type} ttl: 300`)
    return 300
  },
}

function warnRedisDisabled() {
  if (process.env.NODE_ENV === 'production') {
    strapi.log.warn('Redis cache is disabled')
  } else {
    strapi.log.verbose('Redis cache is disabled')
  }
}
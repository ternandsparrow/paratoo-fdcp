'use strict'

const _ =  require('lodash')
const pathToRegexp = require('path-to-regexp')

module.exports = {
  // In absense of Strapi Plugin Documentation v3.6.x function
  // use a simplified version
  // This function was removed in v4 rewrite and we need it
  // because Strapi policy ctx stores matched route in format /products/:id
  // but stores paths in documentation as /products/{id}
  /**
   *
   * Wrap endpoints variables in curly braces
   * @param {String} endPoint
   * @returns {String} (/products/{id})
   */
  _formatApiEndPoint: function(endPoint) {
    return pathToRegexp
      .parse(endPoint)
      .map((token) => {
        if (_.isObject(token)) {
          return token.prefix + '{' + token.name + '}' // eslint-disable-line prefer-template
        }

        return token
      })
      .join('')
  },
  /**
   * Wraps Strapi policy context endpoint in curly braces
   * and removes leading '/api'
   *
   * @param {String} endPoint
   * @returns {String} (/products/{id})
   */
  formatApiEndPoint: function(endpoint) {
    return module.exports._formatApiEndPoint(endpoint).slice(4)
  }
}
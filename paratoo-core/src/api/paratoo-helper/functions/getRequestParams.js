module.exports = {
  getRequestParams(ctx) {
    //try access multiple query params (separated by `&`)
    try {
      return ctx.request.url.split('?')[1].split('&')
    } catch {
      //there aren't multiple params, so try access the first
      try {
        return ctx.request.url.split('?')[1]
      } catch {
        //the 0th element is the actual route (e.g., `/api/foo`), which we don't want
        return undefined
      }
    }
  }
}
'use strict'
const fs = require('fs')
const _ = require('lodash')

module.exports = {
  /**
   * Checks that the required policies are added to the appropriate routes. If reWrite = true,
   * The routes will be amended and written over, otherwise raise warnings in console
   * @param {Boolean} [reWrite] Modify source controlled routes files. Should only be
   * used as a devtool and does not work with createCoreRouter
   */
  amendAPIRoutes: async function ({ reWrite = false }) {
    const start = Date.now()
    const lutInterpreterPolicyName = 'global::lut-interpretation'
    const projectMembershipEnforcerPolicyName = 'global::projectMembershipEnforcer'
    const isValidatedPolicyName = 'global::is-validated'
    const generalPolicies = [
      isValidatedPolicyName,
      projectMembershipEnforcerPolicyName,
    ]
    const roleEnforcerPolicyName = 'global::roleEnforcer'

    const ignoreApis = [
      'reserved-plot-label',
      'paratoo-helper',
      'generate-barcode',
      'org-uuid-survey-metadata',
      'debug-client-state-dump',
      'grassy-weeds-export',
      'analytics',
      'data-exporter-status',
      'push-notification-subscription'
    ]

    const allowMethodFlag = [
      {
        model: 'plot-visit',
        method: 'PUT',
        path: '/plot-visits/:id',
      },
    ]
    const fixCorruptedDataPolicy = [
      {
        model: 'fauna-ground-counts-survey',
        method: 'POST',
        path: '/fauna-ground-counts-surveys/bulk',
        policy: 'global::fix-corrupted-data-fgct',
      },
      {
        model: 'coarse-woody-debris-survey',
        method: 'POST',
        path: '/coarse-woody-debris-surveys/bulk',
        policy: 'global::fix-cwd-length-unit',
      },
      {
        model: 'coarse-woody-debris-survey',
        method: 'POST',
        path: '/coarse-woody-debris-surveys/bulk',
        policy: 'global::fix-cwd-and-tree-stumps-in-same-record',
      },
      {
        model: 'cover-point-intercept-survey',
        method: 'POST',
        path: '/cover-point-intercept-surveys/bulk',
        policy: 'global::fix-species-intercept-height',
      },
      {
        model: 'new-targeted-survey',
        method: 'POST',
        path: '/new-targeted-surveys/bulk',
        policy: 'global::fix-reject-old-queued-data',
      },
      {
        model: 'condition-survey',
        method: 'POST',
        path: '/condition-surveys/bulk',
        policy: 'global::fix-condition-pom',
      },
      {
        model: 'plot-selection-survey',
        method: 'POST',
        path: '/plot-selection-surveys/bulk',
        policy: 'global::fix-bioregion-lut-plot-selection',
      },
      {
        model: 'plot-description-standard',
        method: 'POST',
        path: '/plot-description-standards/bulk',
        policy: 'global::fix-revisit-free-text-to-lut',
      },
      {
        model: 'recruitment-survivorship-survey',
        method: 'POST',
        path: '/recruitment-survivorship-surveys/bulk',
        policy: 'global::fix-revisit-free-text-to-lut',
      },
    ]

    //TODO not the best way to handle this - the list was created via trial-and-error when creating the `queryAllEndpoints` helper script
    const alreadyPluralModels = [
      'interventions',
      'lut-plot-dimensions',
      'lut-plot-points',
      'lut-soils-evaluation-means',
      'lut-soils-horizon-details',
      'lut-soils-relative-inclination-of-slope-elements',
      'lut-soils-segregation-magnetic-attributes',
      'lut-soils-void-cracks',
      'lut-tier-3-observation-methods',
      'soil-land-surface-phenomena',
    ]    

    // get list of APIs (avoid plugins like user-permissions and upload)
    // because these policies are not relevant to such
    const apiList = Object.values(
      strapi
        .plugin('documentation')
        .service('documentation')
        .getPluginAndApiInfo(),
    )
      .filter((o) => o.getter === 'api' && !ignoreApis.includes(o.name))
      .map((o) => o.name)
    const protocolDefs = await strapi.entityService.findMany('api::protocol.protocol')

    //see #1686 for the approach that should be taken for route policies
    let endpointWarnings = []
    for (const apiName of apiList) {
      let routeList = {}
      try {
        routeList = strapi.api[apiName].routes[apiName].routes
      } catch (error) {
        strapi.log.debug(`Failed to get routes for ${apiName}`)
        continue
      }
      let isLut = apiName.startsWith('lut-')
      for (const route of routeList) {
        if (apiName === 'protocol') {
          if (
            ![
              '/protocols/reverse-lookup',
              '/protocols/test-token/:orgType',
            ].includes(route.path)
          ) {
            if (route.method !== 'GET') {
              endpointWarnings.push(`Invalid method ${route.method} on path ${route.path}. Only GET is allowed`)
            }
            if (route.config.policies.length > 0) {
              endpointWarnings.push(`Route ${route.path} should not have policies`)
            }
          } else if (route.path !== '/protocols/test-token/:orgType') {
            if (!route.config.policies.includes(projectMembershipEnforcerPolicyName)) {
              endpointWarnings.push(`Route ${route.path} should have ${projectMembershipEnforcerPolicyName} policy`)
            }
            if (!_.isEqual(
              route.config.policies,
              [projectMembershipEnforcerPolicyName],
            )) {
              endpointWarnings.push(`Route ${route.path} should only have policy ${projectMembershipEnforcerPolicyName}, but more were found: ${route.config.policies?.join(', ') || 'none'}`)
            }
          }
        } else if (apiName === 'expose-documentation') {
          if (route.config.policies.length > 0) {
            endpointWarnings.push(`Route ${route.path} should not have policies`)
          }
        } else if (isLut) {
          if (route.method !== 'GET') {
            endpointWarnings.push(`Invalid method ${route.method} on path ${route.path}. Only GET is allowed`)
          } else {
            if (!_.isEqual(
              route.config.policies,
              generalPolicies,
            )) {
              endpointWarnings.push(`LUT read endpoint for path ${route.path} must have ${generalPolicies.join(', ')} but found: ${route.config.policies?.join(', ') || 'none'}`)
            }
          }
        } else {
          if (
            route.method === 'PUT' ||
            route.method === 'POST'
          ) {
            const writePolicies = [...generalPolicies, lutInterpreterPolicyName]
            if (route.handler.endsWith('.many')) {
              writePolicies.push(roleEnforcerPolicyName)
            }

            for (const policy of fixCorruptedDataPolicy) {
              if (
                policy.method === route.method &&
                policy.path === route.path
              ) {
                writePolicies.unshift(policy.policy)
              }
            }

            if (!_.isEqual(
              route.config.policies,
              writePolicies,
            )) {
              endpointWarnings.push(`Write endpoint for method ${route.method} path ${route.path} must have ${writePolicies.join(', ')} but found: ${route.config.policies?.join(', ') || 'none'}`)
            }

            if (
              allowMethodFlag.some(
                p => p.method === route.method && p.path === route.path,
              ) &&
              !route.allowMethod
            ) {
              endpointWarnings.push(`Write endpoint for method ${route.method} path ${route.path} must set the allowMethod flag, but it was not found`)
            }
          } else if (
            route.method === 'GET'
          ) {
            if (!_.isEqual(
              route.config.policies,
              generalPolicies,
            )) {
              endpointWarnings.push(`Read endpoint for method ${route.method} path ${route.path} must have ${generalPolicies.join(', ')} but found: ${route.config.policies?.join(', ') || 'none'}`)
            }
            // if (!_.isEqual(
            //   route.config.middlewares,
            //   middlewares,
            // )) {
            //   endpointWarnings.push(`Read endpoint for method ${route.method} path ${route.path} must have ${middlewares.join(', ')} but found: ${route.config.middlewares?.join(', ') || 'none'}`)
            // }
          } else if (
            route.method === 'DELETE'
          ) {
            if (!_.isEqual(
              route.config.policies,
              generalPolicies,
            )) {
              endpointWarnings.push(`Write endpoint for method ${route.method} path ${route.path} must have ${generalPolicies.join(', ')} but found: ${route.config.policies?.join(', ') || 'none'}`)
            }
          } else {
            strapi.log.warn(`Unknown API: ${apiName} path ${route.path} method ${route.method}`)
          }
        }
      }
      if (reWrite && apiName !== 'expose-documentation') {
        //NOTE: will not write expose-documentation (will only warn, above)
        const schema = strapi.api[apiName].contentTypes[apiName]
        let pluralName = schema.info.pluralName
        if (alreadyPluralModels.includes(schema.info.singularName)) {
          strapi.log.warn(`API ${apiName} model (singular) name is already plural, so will use the singular name for the route`)
          pluralName = schema.info.singularName
        }
        // console.log('schema:', JSON.stringify(schema, null, 2))
        let toWrite = null
        if (apiName === 'protocol') {
          toWrite = [
            {
              method: 'GET',
              path: `/${pluralName}`,
              handler: `${schema.info.singularName}.find`,
              config: {
                policies: [],
              },
            },
            {
              method: 'GET',
              path: `/${pluralName}/:id`,
              handler: `${schema.info.singularName}.findOne`,
              config: {
                policies: [],
              },
            },
            {
              method: 'POST',
              path: `/${pluralName}/reverse-lookup`,
              handler: `${schema.info.singularName}.retrieveCollections`,
              config: {
                policies: [
                  projectMembershipEnforcerPolicyName,
                ],
              },
            },
            {
              method: 'POST',
              path: `/${pluralName}/test-token/:orgType`,
              handler: `${schema.info.singularName}.testToken`,
              config: {
                policies: [],
              },
            },
          ]
        } else if (isLut) {
          toWrite = [
            {
              method: 'GET',
              path: `/${pluralName}`,
              handler: `${schema.info.singularName}.find`,
              config: {
                policies: generalPolicies,
              },
            },
            {
              method: 'GET',
              path: `/${pluralName}/:id`,
              handler: `${schema.info.singularName}.findOne`,
              config: {
                policies: generalPolicies,
              },
            },
          ]
        } else {
          toWrite = [
            {
              method: 'GET',
              path: `/${pluralName}`,
              handler: `${schema.info.singularName}.find`,
              config: {
                policies: generalPolicies,
              },
            },
            {
              method: 'GET',
              path: `/${pluralName}/:id`,
              handler: `${schema.info.singularName}.findOne`,
              config: {
                policies: generalPolicies,
              },
            },
            {
              method: 'POST',
              path: `/${pluralName}`,
              handler: `${schema.info.singularName}.create`,
              config: {
                policies: generalPolicies.concat([lutInterpreterPolicyName]),
              },
            },
            {
              method: 'PUT',
              path: `/${pluralName}/:id`,
              handler: `${schema.info.singularName}.update`,
              config: {
                policies: generalPolicies.concat([lutInterpreterPolicyName]),
              },
            },
            {
              method: 'DELETE',
              path: `/${pluralName}/:id`,
              handler: `${schema.info.singularName}.delete`,
              config: {
                policies: generalPolicies,
              },
            },
          ]
          if (apiName === 'plot-selection') {
            toWrite.push({
              method: 'POST',
              path: `/${pluralName}/many`,
              handler: `${schema.info.singularName}.many`,
              config: {
                policies: generalPolicies.concat([lutInterpreterPolicyName, roleEnforcerPolicyName]),
              },
            })
          }
          //TODO might be worth adding check when there is a bulk controller but no endpoint (sometimes we move a 'survey' model to become an 'observation' model without removing the endpoint)
          if (
            //check if survey model
            protocolDefs.some(p => p.endpointPrefix === `/${pluralName}`) &&
            //check that it is bulkable
            strapi.api[apiName].controllers[apiName].bulk
          ) {
            toWrite.push({
              method: 'POST',
              path: `/${pluralName}/bulk`,
              handler: `${schema.info.singularName}.bulk`,
              config: {
                policies: generalPolicies.concat([lutInterpreterPolicyName]),
              },
            })
          }

          for (const route of toWrite) {
            for (const policy of fixCorruptedDataPolicy) {
              if (
                policy.method === route.method &&
                policy.path === route.path
              ) {
                route.config.policies.unshift(policy.policy)
              }
            }

            for (const flag of allowMethodFlag) {
              if (
                flag.method === route.method &&
                flag.path === route.path &&
                !route.allowMethod
              ) {
                route.allowMethod = true
              }
            }
          }
        }
        if (toWrite) {
          strapi.log.info(`Writing new routes for ${apiName}`)
          const routesPath = `src/api/${apiName}/routes/${apiName}.js`
          //need to replace double quotes with single quotes as linter will complain
          toWrite = `module.exports = ${JSON.stringify({ routes: toWrite }, null, 2)}`.replaceAll('"', '\'')
          fs.writeFileSync(routesPath, toWrite)
        }
      }
    }

    //DO NOT REMOVE THIS WARNING. IF IT APPEARS IT MUST BE ADDRESSED.
    if (endpointWarnings.length > 0 && !reWrite) {
      strapi.log.warn('The following policy errors were found. You may need to pass `true` to amendAPIRoutes to force a rewrite of the routes. Note that policy order matters.')
      strapi.log.warn(endpointWarnings.join('\n'))
    }

    strapi.log.debug(`Took ${Date.now() - start}ms to amend API routes`)
  },
}

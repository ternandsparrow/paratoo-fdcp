'use strict'
const path = require('path')
const fs = require('fs')
const _ = require('lodash')
const swaggerExamples = require('./amendDocumentationSwaggerExamples.json')

//purposefully not being specific, as 'survey_metadata' might change soon and 'collections'
//varies on the particular protocol
const reverseLookupRespProperties = {
  survey_metadata: {
    type: 'object',
    required: [
      'survey_details',
      'provenance',
    ],
    properties: {
      survey_details: {
        type: 'object',
        required: ['survey_model', 'time', 'uuid', 'project_id', 'protocol_id', 'protocol_version'],
        properties: {
          survey_model: {
            type: 'string',
            minLength: 1,
          },
          time: {
            type: 'string',
            format: 'date-time'
          },
          uuid: {
            type: 'string',
            format: 'uuid'
          },
          project_id: {
            type: 'string',
            minLength: 1,
          },
          protocol_id: {
            type: 'string',
            format: 'uuid'
          },
          protocol_version: {
            type: 'string',
            minLength: 1,
          },
          submodule_protocol_id: {
            type: 'string'
          },
        },
      },
      provenance: {
        type: 'object',
        required: [
          'version_app',
          'version_core_documentation',
          'system_app',
          'version_org',
          'system_org',
        ],
        properties: {
          version_app: {
            type: 'string'
          },
          //appended by core
          version_core: {
            type: 'string'
          },
          version_core_documentation: {
            type: 'string',
          },
          //appended by org
          version_org: {
            type: 'string'
          },
          system_app: {
            type: 'string'
          },
          //appended by core
          system_core: {
            type: 'string',
          },
          //appended by org
          system_org: {
            type: 'string',
          },
        },
      },
    },
  },
  collections: {
    type: 'object',
  },
}

const overridePaths = {
  '/protocols/reverse-lookup': {
    post: {
      description: 'Retrieves all collection data based on the provided param (one of): org_minted_uuid, project_id, protocol_id.\n\n - org_minted_uuid returns a single collection\'s data, while the others can return many.\n\n - protocol_id will implicitly filter on the user\'s projects.',
      responses: {
        200: {
          description: 'Returns a single collection (if using `org_minted_uuid`) or up to many. Each collection will contain keys `survey_metadata` (from the survey and/or collection ID, if relevant), and `collections`, which contain the data for each model related to the collection.',
          content: {
            'application/json': {
              schema: {
                oneOf: [
                  {
                    type: 'object',
                    properties: {
                      ...reverseLookupRespProperties,
                    },
                  },
                  {
                    type: 'array',
                    items: {
                      type: 'object',
                      properties: {
                        ...reverseLookupRespProperties,
                      },
                    },
                  },
                ],
              },
              examples: [
                {
                  summary: 'Single collection - i.e., `org_minted_uuid` request',
                  value: swaggerExamples.reverseLookup.responses.single,
                },
                {
                  summary: 'Multiple collections - i.e., `project_id` (the below example), `protocol_id` requests',
                  value: swaggerExamples.reverseLookup.responses.multiple,
                },
              ],
            },
          },
        },
        403: {
          description: 'Forbidden',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 403,
                message: 'Forbidden',
              },
            },
          },
        },
        404: {
          description: 'Not found',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 404,
                message: 'Not found',
              },
            },
          },
        },
      },
      tags: ['Protocol'],
      requestBody: {
        required: true,
        content: {
          'application/json': {
            schema: {
              oneOf: [
                {
                  type: 'object',
                  properties: {
                    org_minted_uuid: {
                      type: 'string',
                    }
                  },
                },
                {
                  type: 'object',
                  properties: {
                    project_id: {
                      type: 'string',
                    }
                  },
                },
                {
                  type: 'object',
                  properties: {
                    protocol_id: {
                      type: 'string',
                    }
                  },
                },
                {
                  type: 'object',
                  properties: {
                    user_id: {
                      type: 'string',
                    }
                  },
                },
              ],
            },
            examples: [
              {
                summary: 'Org Minted UUID',
                value: {
                  org_minted_uuid: '009ce4c5-3ec5-4b61-bf25-43ea969b01e0',
                },
              },
              {
                summary: 'Project ID (int ID or string UUID)',
                value: {
                  project_id: '1',
                },
              },
              {
                summary: 'Protocol ID (string UUID)',
                value: {
                  protocol_id: '068d17e8-e042-ae42-1e42-cff4006e64b0',
                },
              },
              {
                summary: 'User ID (int ID or string ID/UUID)',
                value: {
                  user_id: '1',
                },
              },
            ],
          },
        },
      },
      summary: 'Retrieves all collection data for a given Org Minted UUID, project ID or protocol ID',
    },
  },
  '/generate-barcode': {
    get: {
      description: 'Generates a given number of barcodes based on the project code and barcode suffix and stores them to prevent future collisions',
      tags: ['Generate-barcode'],
      responses: {
        200: {
          description: 'Returns the PDF document containing the barcodes',
          content: {
            'application/pdf': {
              //TODO confirm this is correct
              format: 'blob',
            },
          },
        },
        403: {
          description: 'Forbidden',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 403,
                message: 'Forbidden',
              },
            },
          },
        },
        404: {
          description: 'Not found',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 404,
                message: 'Not found',
              },
            },
          },
        },
      },
      parameters: [
        {
          name: 'project_code',
          in: 'query',
          description: 'The project code (ID or UUID)',
          deprecated: false,
          required: true,
          schema: {
            type: 'string',
          },
        },
        {
          name: 'num_pages',
          in: 'query',
          description: 'The number of pages to generate',
          deprecated: false,
          required: true,
          schema: {
            type: 'string',
          },
        },
        {
          name: 'program',
          in: 'query',
          description: 'The program (e.g., "M" for MERIT)',
          deprecated: false,
          required: true,
          schema: {
            type: 'string',
            //the full list is set below, but include at least this one here too
            enum: ['M']
          },
        },
        {
          name: 'draw_margins',
          in: 'query',
          description: 'Whether to draw the margins around each barcode label. Defaults to false',
          deprecated: false,
          schema: {
            type: 'boolean',
            default: false,
          },
        },
      ],
      summary: 'Generates barcodes for a given project',
    },
  },
}

// this function is to be run on document generation and make necessary
// changes based on our product requirements
// FIXME in the StrapiV4 merge we no longer individually modify each API's
// src/api/[...modelnames]/documentation/1.0.0/[modelname], and instead modify the merged documentation
// (because our documentaton plugin is using a WIP PR that only works in full_documentation.js)
// this is more brittle, and would likely break if 'generate documentation' is activated in the
// admin panel
// TODO so find less brittle way of doing this (or investigate if it's even an issue)
module.exports = {
  /**
   * gets a list of every api and amends their documentation, this is done so that
   * documentation generation will take these changes into account automatically.
   */
  amendFullApiDoc: async function () {
    const start = Date.now()
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const docServices = strapi.plugin('documentation').service('documentation')
    // get a list of all the api's to change
    const apiList = docServices.getPluginAndApiInfo()
    // Get protocols so bulk documentation endpoints can be made
    const protocols = await helpers.getAllProtocols()
    let oldFullDocumentation
    let docpath
    try {
      docpath = path.resolve(
        docServices.getFullDocumentationPath(),
        docServices.getDocumentationVersion(),
        'full_documentation.json',
      )
      oldFullDocumentation = JSON.parse(fs.readFileSync(docpath))
    } catch (error) {
      console.error(error)
      throw new error(
        `Could not make Documentation changes (for bulk routes, etc.) because we cannot read full_documentation.json at '${docpath}'`,
      )
    }
    let accDocumentation = _.cloneDeep(oldFullDocumentation)
    for (const api of apiList) {
      const apiName = api.name
      try {
        accDocumentation = await amendDocObject(accDocumentation, apiName)
      } catch (error) {
        let msg = `Unable to amend documentation for ${api.name}. LUTs will break`
        strapi.log.error(msg)
        console.error(error)
        helpers.paratooErrorHandler(500, error, msg)
        continue
      }
    }
    // make bulk documentation after everything else because we need to copy
    // some fully amended components.schemas to override from id relation to $ref
    // and we don't want to modify the original component.schema
    for (const api of apiList) {
      const apiName = api.name
      const { pathName, bulkDoc } = makeBulkDocumentation(
        apiName,
        protocols,
        accDocumentation.components.schemas,
      )
      if (pathName && bulkDoc) accDocumentation.paths[pathName] = bulkDoc
    }

    // amend documentation for '/many' endpoint
    for (const api of apiList) {
      const apiName = api.name
      const { pathName, bulkDoc } = makeManyDocumentation(
        apiName,
        protocols,
        accDocumentation.components.schemas,
      )
      if (pathName && bulkDoc) accDocumentation.paths[pathName] = bulkDoc
    }

    // allow additional properties on specific schemas
    // applied to the request body
    const requestSchemasAdditionalProperties = [
      'OpportunisticObservationRequest',
      'DevSandboxSurveyRequest',
      'BirdSurveyRequest',
      // 'PlotLocationRequest',
      'PlotSelectionRequest',
      // 'PestFaunaControlActivityRequest',
      // 'PlotDefinitionSurveyRequest',
      // 'PlotVisitRequest',
      'PlotLayoutRequest',
      'MetadataCollectionRequest',
    ]
    for (const schema of requestSchemasAdditionalProperties) {
      if (!accDocumentation.components.schemas[schema].additionalProperties)
        accDocumentation.components.schemas[schema]['additionalProperties'] = true
    }

    const overridePath = Object.keys(overridePaths)
    // override api descriptions
    for (const path of overridePath) {
      accDocumentation.paths[path] = overridePaths[path]
    }

    //custom endpoints extra info
    const programParamIndex = overridePaths['/generate-barcode'].get.parameters.findIndex(p => p.name === 'program')
    const lutProgram = await strapi.db.query('api::lut-program.lut-program').findMany()
    overridePaths['/generate-barcode'].get.parameters[programParamIndex].schema.enum = lutProgram.map(lp => lp.symbol)

    for (const api of apiList) {
      const { path, pathData } = amendQueryParamsForApi({
        api,
        allPaths: accDocumentation.paths,
      })
      if (path && pathData) {
        accDocumentation.paths[path] = pathData
      }
    }

    // update build information
    accDocumentation.info.description = `Build: ${process.env.NODE_ENV} Version: ${process.env.VERSION} GitHash: ${process.env.GIT_HASH}`
    
    // Throw an error if a write is unsuccessful, because this is an essential process
    fs.writeFileSync(docpath, JSON.stringify(accDocumentation, null, 2))
    strapi.log.info('Amended documentation on all APIs for LUTs')
    strapi.log.debug(`Took ${Date.now() - start}ms to amend documentation`)
  },
}
/**
 * Converts the passed jsonDoc's LUT references with Enums. Returns the amended Documentation
 * as an object
 * @param {*} jsonDoc Full_documentation.json (can be partially amended already)
 * @param {String} apiName cross-references with this routes.json, and Core collection protocols to make bulk documentation
 */
async function amendDocObject(jsonDoc, apiName) {
  let fdSchemas = jsonDoc.components.schemas
  // Update LUTs components, and associated custom flags in all the components/schemas/NewSchema-etc
  for (const [key, value] of Object.entries(fdSchemas)) {
    let newValue = value
    // check if model exists, and it's a Post/Put
    if (
      typeof key === 'string' &&
      // remove hyphens because schemas in camelcase
      key.toLowerCase().match(`${apiName.replace(/-/g, '')}request`)
    ) {
      let modelRef
      try {
        modelRef = strapi.api[apiName].contentTypes[apiName]
      } catch (error) {
        // api doesn't exist
      }
      if (!modelRef) {
        strapi.log.warn(`Couldn't find model: ${apiName}`)
        continue
      }
      // add flags to component attributes, and resolve their LUT and reference flags
      newValue = await resolveComponentReferences(newValue, modelRef, key)
      // Resolve top-level LUTs and reference flags
      newValue.properties.data = await resolveModelAssociations(
        newValue.properties.data,
        modelRef,
        key,
      )
    }
    // make top-level data required, and dis-allow extra attributes
    // The top level field is essential so it clearly is not valid if it goes straight into the payload
    newValue.required = ['data']
    newValue.additionalProperties = false
    fdSchemas[key] = newValue
  }
  jsonDoc.components.schemas = fdSchemas
  return jsonDoc
}

/**
 * Gets the routes for a given API - defined in src/api/<apiName>/routes/<apiName>.js
 * 
 * @param {String} apiName the API's model name
 * 
 * @returns the `routes` object from the API
 */
function getRoutesForApi(apiName) {
  let routes = null
  try {
    routes = strapi.api[apiName].routes[apiName].routes
  } catch (error) {
    // no routes for api anyway (sometimes unused APIs will build up in dev instances)
    routes = null
  }
  return routes
}
/**
 * Makes and formats a bulk endpoint
 * @param {*} apiName Modelname with a bulk route in it's routes.json
 * @param {*} protocols Return of a strapi query to the 'protocol' model
 * @param {Object} allComponentSchemas documentation.components.schemas, we copy and modify one of these to override child ob parent schema
 * @returns an object with property "bulkDoc", which should be appended onto path: "pathName"
 */
function makeBulkDocumentation(apiName, protocols, allComponentSchemas) {
  // Check there's an associated endpoint in routes.json
  // And that it uses the .bulk handler
  const routes = getRoutesForApi(apiName)
  if (routes === null) return { pathName: null, bulkDoc: null }
  const bulkRoute = _.find(routes, (o) => {
    // A bulk route ends with /bulk and is a POST
    if (o.method !== 'POST' || o.handler !== `${apiName}.bulk`) return false
    const routeSplit = o.path.split('/')
    return (
      routeSplit &&
      routeSplit.length >= 3 &&
      routeSplit[routeSplit.length - 1] === 'bulk'
    )
  })
  if (!bulkRoute) return { pathName: null, bulkDoc: null }
  const pathName = bulkRoute.path
  const pathPrefix = pathName.split('/bulk')[0]
  // use the workflow in the protocol matching the endpoint prefix
  // (we can't check the protocol directly because it's associated with an endpoint, not a model)
  // Check protocols table in database
  const associatedProtocol = _.find(protocols, (o) => {
    return o.endpointPrefix === pathPrefix
  })
  if (!associatedProtocol) {
    // console.log(protocols)
    // console.log(apiName)
    console.log(bulkRoute)
  }
  // OpenAPI path schema, based on docs/Manually adding protocols with bulk.md, and issue #71
  // initialise common aspects
  let accBulkDoc = {
    post: {
      deprecated: false,
      responses: {
        200: {
          description: 'Echos back the body of the request',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {},
              },
            },
          },
        },
      },
      // the "heading" of the path when using Swagger visualiser
      // FIXME update for Strapi 4 (should be Title Case)
      tags: [
        `${apiName.slice(0, 1).toUpperCase()}${apiName.slice(1).toLowerCase()}`,
      ],
      requestBody: {
        required: true,
        content: {
          'application/json': {
            schema: {
              type: 'object',
              required: ['data'],
              properties: {
                data: {
                  type: 'object',
                  required: ['collections'],
                  additionalProperties: false,
                  properties: {
                    collections: {
                      type: 'array',
                      minItems: 1,
                      items: {
                        type: 'object',
                        required: ['orgMintedIdentifier'],
                        additionalProperties: true,
                        properties: {
                          orgMintedIdentifier: {
                            type: 'string',
                            minLength: 1,
                          },
                        },
                      },
                    },
                  },
                }
              },
            },
          },
        },
      },
    },
  }
  let accBulkDocCollections =
    accBulkDoc.post.requestBody.content['application/json'].schema.properties
      .data.properties.collections
  let accExampleResponseProperties =
    accBulkDoc.post.responses[200].content['application/json'].schema.properties
  for (const workflowItem of Object.values(associatedProtocol.workflow)) {
    //submodule steps should be submit to the submodule's endpoint - e.g., if submitting
    //cover PI with Fire, then each of these should go to their respective bulk endpoints
    if (workflowItem.isSubmoduleStep) continue
    // Any plot locations/layout/visit can be input as ID as well
    const modelName = workflowItem.modelName
    const docRefModelName = modelNameToSchemaName(modelName)
    //if we don't specify required, assume it is
    const workflowItemIsRequired =
      !Object.keys(workflowItem).includes('required') || !!workflowItem.required
    // add it to required list (unless we explicitly say it's not required)
    if (workflowItemIsRequired) {
      accBulkDocCollections.items.required.push(modelName)
    }
    // set body acc to a baseline
    // (the newSchema schema should be formatted like "SchemaRequest", but we might need to confirm)
    let propertyBody = {
      type: 'object',
      $ref: `#/components/schemas/${docRefModelName}`,
    }
    // if we need to allow creation of child observations (handled in bulk controller)
    // then copy the $ref target's body, and modify attributes defined in workflow step
    if (
      Array.isArray(workflowItem.newInstanceForRelationOnAttributes) &&
      workflowItem.newInstanceForRelationOnAttributes.length > 0
    ) {
      let modifiedModelSchema = _.cloneDeep(
        allComponentSchemas[docRefModelName],
      )
      for (const childObAttribute of workflowItem.newInstanceForRelationOnAttributes) {
        // get modelName of target and use multiplicity
        let accAttributeSchema =
          modifiedModelSchema.properties.data.properties[childObAttribute]
        let targetSchemaName
        // handle multiplicity
        // veg-mapping-species-covers can be multiple, is type:array, with items:{type:int}
        const multiple = accAttributeSchema.type === 'array'
        if (multiple) targetSchemaName = accAttributeSchema.items['x-model-ref']
        else targetSchemaName = accAttributeSchema['x-model-ref']
        const targetSchema = `#/components/schemas/${modelNameToSchemaName(
          targetSchemaName,
        )}`
        if (multiple) accAttributeSchema.items = { $ref: targetSchema }
        else accAttributeSchema = { $ref: targetSchema }
        modifiedModelSchema.properties.data.properties[childObAttribute] =
          accAttributeSchema
      }
      propertyBody = modifiedModelSchema
    }
    // these 3 fields should be defined in bulk as allowing an ID in their place instead
    switch (modelName) {
      case 'plot-location':
      case 'plot-layout':
      case 'plot-visit':
        propertyBody = {
          type: 'object',
          anyOf: [
            propertyBody,
            {
              type: 'object',
              required: ['id'],
              properties: {
                id: {
                  type: 'integer',
                },
              },
            },
          ],
        }
        break
      default:
        break
    }
    // i.e. if it's an observations list
    if (workflowItem.multiple) {
      // make it an array
      propertyBody = {
        type: 'array',
        items: propertyBody,
      }
      //accounts for when an item isn't required but the client sends an empty array
      if (workflowItemIsRequired) {
        propertyBody.minItems = 1
      }
      // add it to the 200 response as an array
      accExampleResponseProperties[modelName] = {
        type: 'array',
        items: { $ref: `#/components/schemas/${docRefModelName}` },
      }
      // maximum and minimum items in the array
      if(workflowItem.maximum) {
        propertyBody.maxItems = workflowItem.maximum
      }
      if(workflowItem.minimum) {
        propertyBody.minItems = workflowItem.minimum
      }
    } else {
      // add it to the 200 response as a singular
      accExampleResponseProperties[modelName] = {
        $ref: `#/components/schemas/${docRefModelName}`,
      }
    }

    // override default name of a collection
    if(workflowItem.overrideDisplayName) {
      propertyBody.overrideDisplayName = workflowItem.overrideDisplayName
    }

    accBulkDocCollections.items.properties[modelName] = propertyBody
  }
  accBulkDoc.post.requestBody.content[
    'application/json'
  ].schema.properties.data.properties.collections = accBulkDocCollections
  // TODO build responses + example payloads
  return { pathName: pathName, bulkDoc: accBulkDoc }
}
/**
 * Makes and formats a many endpoint
 * @param {*} apiName Modelname with a bulk route in it's routes.json
 * @returns an object with property "bulkDoc", which should be appended onto path: "pathName"
 */
function makeManyDocumentation(apiName) {
  // Check there's an associated endpoint in routes.json
  // And that it uses the .many handler
  let routes
  try {
    routes = strapi.api[apiName].routes[apiName].routes
  } catch (error) {
    // no routes for api anyway (sometimes unused APIs will build up in dev instances)
    return { pathName: null, bulkDoc: null }
  }
  const manyRoute = _.find(routes, (o) => {
    // A bulk route ends with /many and is a POST
    if (o.method !== 'POST' || o.handler !== `${apiName}.many`) return false
    const routeSplit = o.path.split('/')
    return (
      routeSplit &&
      routeSplit.length >= 3 &&
      routeSplit[routeSplit.length - 1] === 'many'
    )
  })
  if (!manyRoute) return { pathName: null, bulkDoc: null }
  const pathName = manyRoute.path

  let manyDoc = {
    post: {
      deprecated: false,
      responses: {
        200: {
          description: 'Echos back the body of the request',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                  [`${apiName}s`] : {
                    $ref: `#/components/schemas/${modelNameToSchemaName(apiName)}`
                  }
                },
              },
            },
          },
        },
      },
      tags: [
        `${apiName.slice(0, 1).toUpperCase()}${apiName.slice(1).toLowerCase()}`,
      ],
      requestBody: {
        required: true,
        content: {
          'application/json': {
            schema: {
              type: 'object',
              required: ['data'],
              properties: {
                data: {
                  type: 'object',
                  required: [`${apiName}s`],
                  additionalProperties: false,
                  properties: {}
                }
              },
            },
          },
        },
      },
    },
  }
  let accManyDocData =
  manyDoc.post.requestBody.content['application/json'].schema.properties.data
  let accManyDocProperties =
    manyDoc.post.requestBody.content['application/json'].schema.properties
      .data.properties
  // properties should be different based on the modelName
  switch (apiName) {
    case 'plot-selection':
      // additional field for project related info
      accManyDocData.additionalProperties = true
      accManyDocProperties[`${apiName}s`] = {
        type: 'array',
        items: {
          type: 'object',
          $ref: `#/components/schemas/${modelNameToSchemaName(apiName)}`
        },
        minItems: 1
      }
      break
    default:
      accManyDocProperties[`${apiName}s`] = {
        type: 'array',
        items: {
          type: 'object',
          $ref: `#/components/schemas/${modelNameToSchemaName(apiName)}`
        },
      }
      break
  }
  manyDoc.post.requestBody.content['application/json'].schema.properties
    .data = accManyDocData
  manyDoc.post.requestBody.content['application/json'].schema.properties
    .data.properties = accManyDocProperties
  return { pathName: pathName, bulkDoc: manyDoc }
}
/**
 * Amends the documentation's 'paths' so that all GET requests get custom query params.
 * Right now just `project_id(s)`, `prot_uuid(s)`, but can extend for others
 * 
 * @param {String} api the API object, contain the `name`, etc.
 * @param {Object} allPaths the `paths` object from Swagger
 * 
 * @returns {Object} the `path` and amended `pathData`
 */
function amendQueryParamsForApi({ api, allPaths }) {
  let updatedPath = null
  let path = null
  const apiName = api.name
  const routes = getRoutesForApi(apiName)

  const projectIdSingularParam = {
    name: 'project_id',
    in: 'query',
    description: 'The project code (ID or UUID)',
    deprecated: false,
    schema: {
      type: 'string',
      format: 'project_id',   //custom format that we define in `is-validated`
    },
  }
  const protIdSingularParam = {
    name: 'prot_uuid',
    in: 'query',
    description: 'The protocol UUID',
    deprecated: false,
    schema: {
      type: 'string',
      format: 'uuid',
    },
  }
  const projectIdPluralParam = {
    name: 'project_ids',
    in: 'query',
    description: 'The project codes (IDs or UUIDs)',
    deprecated: false,
    schema: {
      oneOf: [
        {
          //param passed as array
          //project_ids=1,project_ids=2 or project_ids[0]=1,project_ids[1]=2
          type: 'array',
          items: projectIdSingularParam.schema,
        },
        {
          //param passed as comma-separated strings
          //project_ids=1,2
          type: 'string',
          format: 'project_ids_str',    //custom format that we define in `is-validated`
        },
      ],
    },
  }
  const protIdPluralParam = {
    name: 'prot_uuids',
    in: 'query',
    description: 'The protocol UUIDs',
    deprecated: false,
    schema: {
      oneOf: [
        {
          type: 'array',
          items: protIdSingularParam.schema,
        },
        {
          type: 'string',
          format: 'protocol_ids_str',    //custom format that we define in `is-validated`
        },
      ],
    },
  }

  for (const route of routes || []) {
    if (route?.method === 'GET') {
      if (route.handler.endsWith('.find')) {
        path = route.path
        updatedPath = allPaths[route.path]
      } else if (route.handler.endsWith('.findOne')) {
        path = route.path.replace(':id', '{id}')
        updatedPath = allPaths[path]
      }

      if (updatedPath?.get) {
        if (!Array.isArray(updatedPath.get?.parameters)) {
          Object.assign(updatedPath.get, {
            parameters: [],
          })
        }
        // findOne don't really need these query params to filter
        if(route.handler.endsWith('.find')) {
          updatedPath.get.parameters.push(...[
            projectIdSingularParam,
            protIdSingularParam,
            projectIdPluralParam,
            protIdPluralParam,
          ])
        }
        amendQueryParamsForApiForDebugClientState(apiName, updatedPath.get.parameters, route.handler.endsWith('.findOne'))
      }
      
    }
  }

  return {
    path: path, //the `path` from swagger.json's `paths` property
    pathData: updatedPath,
  }
}
/**
 * finds properties using component schema
 *
 * @param {String} componentName schema ref
 * @returns properties of components
 */
async function resolveComponentProperties(componentName) {
  let properties = {}
  for (const key of Object.keys(strapi.components)) {
    if (key != componentName) continue
    properties = strapi.components[key]['attributes']
  }
  return properties
}
/**
 * search through the internal model fields
 * in order to find Strapi reusable components and mark them with a tag
 * this is intended to be used in the documentation to suggest methods of data collection
 * (notably, a field using the barcode tag should be collected using a camera in client application)
 *
 * @param {*} inNewValue documentation schema section (documentation.components.schemas[modelName])
 * @param {*} modelRef Strapi content type config (strapi.api[apiName].contentTypes[apiName])
 * @param {String} baseModelName Only passed into resolveModelReferences to track unpopulated LUTs
 * @returns Amended documentation schema
 */
async function resolveComponentReferences(inNewValue, modelRef) {
  let newValue = inNewValue
  for (const [attributeKey, attribute] of Object.entries(modelRef.attributes)) {
    if (attribute.type && attribute.type === 'component') {
      newValue.properties.data.properties[attributeKey] = attribute
    }
  }
  return newValue
}
function resolveComponentAssociation(isMultiComponent, componentName, newValues) {
  const values = _.cloneDeep(newValues)
  values['type'] = 'object'
  
  // remove extra fields 
  let keys = Object.keys(values)
  if (keys.includes('$ref')) delete values['$ref']
  if (keys.includes('repeatable')) delete values['repeatable']
  if (keys.includes('component')) delete values['component']

  if (!isMultiComponent) {
    return {
      ...values,
      'x-paratoo-component': componentName
    }
  }

  return {
    type: 'array',
    items: {
      ...values,
      'x-paratoo-component': componentName
    }
  }
}
function resolveAttributesTypes(attribute) {
  // fix attributes type so that ajv can validate
  if (attribute.type === 'text') {
    return {
      type: 'string',
    }
  }
  // add constraint for string type which varchar(255)
  if (attribute.type === 'string') {
    return {
      type: 'string',
      maxLength: attribute.maxLength ? attribute.maxLength : 255,
    }
  }
  // format: type: 'float'
  // ajv cant validate the format
  //    so, the format needs to be changed
  //    so that ajv can validate
  // TODO: need to check other types as well
  if (attribute.type === 'float' || attribute.type === 'decimal') {
    return {
      type: 'number',
      format: 'float',
    }
  }
  if (attribute.type === 'datetime' || attribute.type === 'dateTime') {
    return {
      type: 'string',
      format: 'date-time',
    }
  }
  if (attribute.type === 'time') {
    return {
      type: 'string',
      format: 'iso-time',
    }
  }
  if (attribute.type === 'enumeration') {
    return {
      type: 'string',
      enum: attribute.enum ? attribute.enum : [],
    }
  }
  if (attribute.type === 'json') {
    return {}
  }
  if (attribute.type === 'integer') {
    return {
      type: 'integer',
    }
  }

  return null
}

function resolveMinMaxUsingType(attribute) {
  const value = _.cloneDeep(attribute)
  const validFormat = resolveAttributesTypes(value)
  const attributeType = validFormat ? validFormat.type : value.type
  const isArray = attributeType == 'array' || value.repeatable == true

  let maxValue = null
  let minValue = null
  const attributeKeys = Object.keys(value)
  const possibleMinKeys = ['min', 'minimum', 'minLength', 'minItems']
  const possibleMaxKeys = ['max', 'maximum', 'maxLength', 'maxItems']
  // removes invalid keys if exists
  attributeKeys.forEach((key)=> {
    let isRemoveKey = false
    if (possibleMaxKeys.includes(key)) {
      maxValue = value[key]
      isRemoveKey = true
    }

    if (possibleMinKeys.includes(key)){
      minValue = value[key]
      isRemoveKey = true
    }

    if (key == 'unique') {
      if (value['unique'] == false) {
        isRemoveKey = true
      }
    }

    if (isRemoveKey) delete value[key]
  })

  // fix invalid keys
  let maxKey = 'maximum'
  let minKey = 'minimum'
  if (attributeType == 'string' || attributeType == 'email' || attributeType == 'password') {
    maxKey = 'maxLength'
    minKey = 'minLength'
  }
  if (isArray) {
    maxKey = 'maxItems'
    minKey = 'minItems'
  }

  if (maxValue!=null) {
    value[maxKey] = maxValue
  }
  if (minValue!=null) {
    value[minKey] = minValue
  }
  if (value.min) delete value.min
  if (value.max) delete value.max
  return value
}

/**
 * Reads modelRef.associations and applies relevant LUT enums and 'x-...' flags to LUTs and model references
 * @param {*} inNewValue JSON schema object for the target model. Should have 'properties' key with model attributes directly inside
 * @param {*} modelRef
 * @param {String} modelName Only used for console trace to track down unpopulated LUTs
 * @returns Amended inNewValue
 */
async function resolveModelAssociations(inNewValue, modelRef, modelName = '', isComponent=false) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  let newValue = inNewValue
  for (let [attributeKey, attribute] of Object.entries(modelRef.attributes)) {
    if (attributeKey == 'createdAt') continue
    if (attributeKey == 'updatedAt') continue
    // fix valid min and max for ajv
    attribute = resolveMinMaxUsingType(attribute)
    const attributeKeys = Object.keys(attribute)
    // if not initialized
    if (!Object.keys(newValue).includes('properties')) {
      newValue['properties'] = {}
    }
    // if key not found in the properties
    if (!Object.keys(newValue.properties).includes(attributeKey)) {
      newValue['properties'][attributeKey] = {}
    }
    const validType = resolveAttributesTypes(attribute)
    if (validType) {
      newValue['properties'][attributeKey] = validType
    }
    
    if (attribute.type === 'json') {
      // as ajv can't validate if unique exists in a json object
      if (attributeKeys.includes('unique')) {
        delete attribute['unique']
      }
    }
    // if nested component found, we resolve recursively
    if (attribute.type === 'component') {
      const componentName = attribute.component
      const isMultiComponent = attribute.repeatable === true
      let newValueField
      const properties = await resolveComponentProperties(componentName)
      newValueField = {
        properties: properties
      }
      
      newValueField = await resolveModelAssociations(
        _.cloneDeep(newValueField),
        strapi.components[componentName],
        modelName,
        true
      )

      newValue.properties[attributeKey] = resolveComponentAssociation(isMultiComponent, componentName, newValueField)
    }
    if (attribute.type && attribute.type === 'relation') {
      // normally attribute.target looks like 'api::protocol.protocol'
      const contentTypeName = attribute.target
        .replace('api::', '')
        .split('.')[0]
      
      // if it's a lut, we need to give a list of available options (with current LUT elements)
      // This is because all of these values relate to an external vocab (and will stay pretty
      // static)
      if (attribute.target.match('^api::lut')) {
        const hasSortOrder = strapi.api?.[contentTypeName]?.contentTypes?.[contentTypeName]?.attributes?.sort_order
        const qResults = await strapi.db.query(attribute.target).findMany()
        let lutEnum = []
        let sortOrder = []
        qResults.forEach((qElement) => {
          lutEnum.push(qElement['symbol'])
          if (hasSortOrder) sortOrder.push(qElement['sort_order'])
        })
        if (lutEnum.length < 1) {
          strapi.log.error(
            `Found unpopulated LUT on model:${modelName}, referring to LUT:${attribute.target}`,
          )
        }
        newValue.properties = newValue.properties || {}
        newValue.properties[attributeKey] = {
          type: 'string',
          enum: lutEnum,
          'x-lut-ref': contentTypeName,
        }
        if (hasSortOrder) {
          newValue.properties[attributeKey].sort_order = sortOrder
        }
      }
      // mark non-lut references for documentation purposes
      // set type to be int (for id).
      else if (attribute.relation === 'oneToOne') {
        newValue.properties[attributeKey] = {
          type: 'integer',
          'x-model-ref': contentTypeName,
        }
      }
      // same but oneToMany refs are structured differently
      else if (attribute.relation === 'oneToMany') {
        newValue.properties[attributeKey] = {
          type: 'array',
          items: {
            type: 'integer',
            'x-model-ref': contentTypeName,
          },
        }
      }
    }
    // Add in media (should look like a relation to 'file')
    if (attribute.type === 'media') {
      const contentTypeName = 'file'
      // we need to make the full thing because the components-->schemas in
      // https://github.com/strapi/strapi/pull/12929 do not generate attributes AT ALL
      // TODO rework if/when fixed
      if (attribute.multiple) {
        newValue.properties[attributeKey] = {
          type: 'array',
          items: {
            type: 'integer',
            'x-paratoo-file-type': getFileType(modelRef, attributeKey),
            'x-model-ref': contentTypeName,
          },
        }
      } else {
        newValue.properties[attributeKey] = {
          type: 'integer',
          'x-paratoo-file-type': getFileType(modelRef, attributeKey),
          'x-model-ref': contentTypeName,
        }
      }
    }
    if (attribute['default']) {
      newValue['default'] = attribute['default']
    }
    // Add flags to indicate an external database is to be suggested in the webapp
    if (attribute['x-paratoo-csv-list-taxa'])
      newValue.properties[attributeKey]['x-paratoo-csv-list-taxa'] =
        attribute['x-paratoo-csv-list-taxa']

    if (attribute['x-paratoo-multi-select-json'])
      newValue.properties[attributeKey]['x-paratoo-multi-select-json'] =
        attribute['x-paratoo-multi-select-json']

    // Check if the x-paratoo-required flag exists for each reference
    // If we are a relation we assume it IS REQUIRED, unless specified otherwise
    // We can't specify it IS required within the Strapi model itself, because Strapi
    // is unable to handle relational fields being required (because of db interface limitation)
    // https://github.com/strapi/strapi/issues/557
    // Specifically, this has a functional difference because our custom validation
    // is able to enforce required
    // Then add required to array if needed
    // we don't need array of required fields inside a component
    // TODO: need to find a way to handle required fields in components/nested components
    const isRequired = attribute['x-paratoo-required'] === true || attribute['required'] === true
    if (isRequired) {
      newValue.required = !newValue.required ? [] : newValue.required
      if (!newValue.required.includes(attributeKey))
        newValue.required.push(attributeKey)
      
      // remove required flag if exists
      const keys = Object.keys(newValue.properties[attributeKey])
      if (keys.includes('required') && !Array.isArray(newValue.properties[attributeKey]['required'])) {
        delete newValue.properties[attributeKey]['required']
      }
      if (keys.includes('x-paratoo-required') && !Array.isArray(newValue.properties[attributeKey]['x-paratoo-required'])) {
        delete newValue.properties[attributeKey]['x-paratoo-required']
      }
    }

    //handle `x-paratoo-description` flag
    if (attribute['x-paratoo-description']) {
      newValue.properties[attributeKey]['x-paratoo-description'] =
        attribute['x-paratoo-description']
    }

    //handle `x-paratoo-unit` flag
    if (attribute['x-paratoo-unit']) {
      newValue.properties[attributeKey]['x-paratoo-unit'] =
        attribute['x-paratoo-unit']
    }
    // handle `x-paratoo-symantec-uri` for RDF
    if (attribute['x-paratoo-symantec-uri']) {
      newValue.properties[attributeKey]['x-paratoo-symantec-uri'] =
        attribute['x-paratoo-symantec-uri']
    }

    // flag to set minted identifier for non-plot based protocol
    if (attribute['x-paratoo-add-minted-identifier']) {
      newValue.properties[attributeKey]['x-paratoo-add-minted-identifier'] =
        attribute['x-paratoo-add-minted-identifier']
      // add property minted identifier so that ajv can validate
      newValue.properties['orgMintedIdentifier'] = {
        type: 'string',
        'x-paratoo-add-minted-identifier': true
      }
      // collection id submission status
      // e.g. for multiple first one should be true and rest should be false
      newValue.properties['collectionIdSubmitted'] = {
        type: 'boolean',
        'x-paratoo-add-minted-identifier': true
      }
      // orgMintedIdentifier and collectionIdSubmitted are required
      newValue.required = newValue.required || []
      newValue.required.push('orgMintedIdentifier')
      newValue.required.push('collectionIdSubmitted')
      
    }

    //handle `x-paratoo-rename` override name of normal fields
    if (attribute['x-paratoo-rename']) {
      newValue.properties[attributeKey]['x-paratoo-rename'] =
        attribute['x-paratoo-rename']
    }

    //handle `x-paratoo-has-dependencies` flag
    //TODO enforce only allowing fields from the same schema to appear in `attribute['x-paratoo-has-dependencies']`
    if (attribute['x-paratoo-has-dependencies']) {
      newValue.properties[attributeKey]['x-paratoo-has-dependencies'] =
        attribute['x-paratoo-has-dependencies']
    }

    //handle `x-paratoo-hint` flag
    if (attribute['x-paratoo-hint']) {
      newValue.properties[attributeKey]['x-paratoo-hint'] =
        attribute['x-paratoo-hint']
    }

    //handle `x-paratoo-regex-message`
    if (attribute.regex) {
      if (attribute['x-paratoo-regex-message']) {
        newValue.properties[attributeKey]['x-paratoo-regex-message'] = attribute['x-paratoo-regex-message']
      } else {
        strapi.log.warn(`Field '${attributeKey}' for model '${modelName}' has regex attribute but no user-facing message defined (x-paratoo-regex-message). Client error messages will not be useful to user`)
      }
    }

    //handle `x-paratoo-soft-relation-target` flag.
    //allows us to indicate to the app which fields might use other model data, but don't
    //create hard relational links
    if (attribute['x-paratoo-soft-relation-target']) {
      newValue.properties[attributeKey]['x-paratoo-soft-relation-target'] =
        attribute['x-paratoo-soft-relation-target']
    }

    for (const newOrExistingHint of [
      //similar to `x-paratoo-rename`, but we're renaming the `site_id` field in the
      //`general.new-or-existing` component
      'x-paratoo-new-or-existing-site-name',
      //sometimes the model is not where the site IDs are, but they are nested in a
      //component, so need to add a hint to know what field to search in when accessing
      //said model
      'x-paratoo-new-or-existing-parent-field',
    ]) {
      if (attribute[newOrExistingHint]) {
        if (attribute?.component === 'general.new-or-existing') {
          newValue.properties[attributeKey][newOrExistingHint] = attribute[newOrExistingHint]
        } else {
          helpers.paratooWarnHandler(
            `Tried to add ${newOrExistingHint} to model '${modelName}' and attribute '${attributeKey}', but the attribute is not a 'general.new-or-existing' component.`,
          )
        }
      }
    }

    // common data properties
    const commonDataProperties = [
      'unique',
      'minimum',
      'maximum',
      'minLength',
      'maxLength',
      'maxItems',
      'minItems',
    ]
    commonDataProperties.forEach((p) => {
      const keys = Object.keys(attribute)
      if (keys.includes(p)) {
        newValue.properties[attributeKey][p] = attribute[p]
      }
    })

    // if regex exists
    if (attribute['regex']) {
      newValue.properties[attributeKey]['pattern'] = attribute['regex']
    }

    // ajv validator expects only an array if required fields
    //   so if required property exists we can delete this
    if (newValue.properties[attributeKey]['repeatable']) {
      delete newValue.properties[attributeKey]['repeatable']
    }
    if (newValue.properties[attributeKey]['component']) {
      delete newValue.properties[attributeKey]['component']
    }
    if (newValue.properties[attributeKey]['regex']) {
      delete newValue.properties[attributeKey]['regex']
    }
    if (newValue.properties[attributeKey]['default']) {
      delete newValue.properties[attributeKey]['default']
    }
    if (newValue.properties[attributeKey]['min']) {
      delete newValue.properties[attributeKey]['min']
    }
    if (newValue.properties[attributeKey]['max']) {
      delete newValue.properties[attributeKey]['max']
    }
  }
  if (!isComponent) {
    newValue['required'] = newValue['required'] ? newValue['required'] : []
  }
  // we added some new attributes, preserve the original order
  // because we currently use this order on the frontend to display fields
  // and want to be able to configure it
  // TODO FIXME, I disabled it because it can't see components (and probably doesn't keep their order anyway)
  // (modelRef.attributes list doesn't even keep them)
  // newValue.properties = reorderAttributes(
  //   modelRef.attributes,
  //   newValue.properties,
  // )
  return newValue
}
/**
 * Returns the content for the x-paratoo-file-type, to notate the allowable types to UI,
 * Validation should eventually be handled separately for each type
 * @param {*} modelRef
 * @param {*} attributeName
 * @returns {Array} An array of types allowable in this attribute
 */
function getFileType(modelRef, attributeName) {
  // https://swagger.io/docs/specification/openapi-extensions/
  // x-flag attributes can be arrays
  //want to differentiate audio from the base 'files' Strapi media type, so the client
  //  knows to render the correct component
  if (modelRef.attributes[attributeName]['x-paratoo-audio-only'])
    return ['audio']
  return modelRef.attributes[attributeName].allowedTypes
}
/**
 * kebab-case to CamelCase + add postfix
 *
 * @param {String} modelName
 */
function modelNameToSchemaName(modelName) {
  return `${_.upperFirst(_.camelCase(modelName))}Request`
}
/**
 * append documentation to schema for debug-client-state-dump findOne
 * @param {*} apiName 
 * @param {*} parameters 
 * @param {*} isFindOne 
 * @returns 
 */
function amendQueryParamsForApiForDebugClientState(apiName, parameters, isFindOne=false) {
  if(apiName !== 'debug-client-state-dump' || !isFindOne) return
  const clientStateUuidSchema = {
    name: 'client_state_uuid',
    in: 'query',
    description: 'UUID of each dump collection',
    deprecated: false,
    schema: {
      type: 'string',
      format: 'client_state_uuid',   //custom format that we define in `is-validated`
    },
  }
  parameters.push(clientStateUuidSchema)

}
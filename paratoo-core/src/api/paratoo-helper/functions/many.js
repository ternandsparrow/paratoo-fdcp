'use strict'
const { defaultSanitizeOutput } = require('@strapi/utils').sanitize.sanitizers

module.exports = {
  /**
   * A generic function for uploading an array of observations
   *
   * @param {Object} ctx Koa context
   * @param {String} modelName name of the model
   * @returns {Array} Objects of the items in the model array that were created
   */
  async genericMany({ ctx, manyModelName, surveyModelName }) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    if(!surveyModelName) {
      const error = new Error('surveyModelName is required')
      return helpers.paratooErrorHandler(500, error)
    }
    const modelName = `${manyModelName}s`
    const results = []

    // request parameters
    const requestBody = ctx.request.body
    const authToken = ctx.request.headers['authorization']
    const observations = requestBody.data[modelName]
      ? requestBody.data[modelName]
      : undefined
    const orgMintedIdentifier = requestBody.data['orgMintedIdentifier']
      ? requestBody.data['orgMintedIdentifier']
      : undefined
    // if no observations found we return
    if (!observations) {
      ctx.body = results
      return results
    }
    const completedObservations = []
    try {
      await strapi.db.transaction(async () => {
        for (let observation of observations) {
          if (!observation) continue

          const entry = await strapi.entityService.create(
            `api::${manyModelName}.${manyModelName}`,
            {
              data: observation.data,
              populate: 'deep',
            },
          )
          if (!entry) continue

          completedObservations.push(
            await defaultSanitizeOutput(
              strapi.getModel(helpers.modelToApiName(manyModelName)),
              entry,
            ),
          )
        }
      })
    } catch (error) {
      let status = error.status || 500
      let errRes = error
      try {
        await removeSurveyWhenFailed(manyModelName, surveyModelName, observations)
      } catch (error2) {
        status = error2.status || 500
        errRes = error2
      } 
      return helpers.paratooErrorHandler(status, errRes)
    }

    if (completedObservations.length > 0) {
      results.push({
        [manyModelName]: completedObservations,
      })
    }

    // if orgMintedIdentifier exists we notify org
    if (orgMintedIdentifier) {
      const result = await helpers.sendCollectionIdentifierToOrg(
        orgMintedIdentifier,
        authToken,
        results,
        false,
      )
      if (result.error) {
        return helpers.paratooErrorHandler(
          400,
          new Error(JSON.stringify(result.error)),
        )
      }
    }

    // if plot selection we sync with org
    if (manyModelName == 'plot-selection') {
      const syncPlotSelection = await helpers.syncPlotSelection(
        ctx,
        manyModelName,
        completedObservations,
      )
      results.push({
        'plot-selection-sync': syncPlotSelection,
      })
    }
    ctx.body = results
    return results
  },
}

async function removeSurveyWhenFailed(
  manyModelName,
  surveyModelName,
  observations,
) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const apiName = `api::${surveyModelName}.${surveyModelName}`
  const surveyReferenceFieldName = helpers.findAssociationAttributeName(
    manyModelName,
    apiName,
  )
  const surveyId = observations[0].data[surveyReferenceFieldName]
  await strapi.db.query(apiName).delete({
    where: { id: surveyId },
  })
 
}

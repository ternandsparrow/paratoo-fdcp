'use strict'

module.exports = {
  findValForKey: findValForKey,
}

function findValForKey(object, key) {
  let value
  Object.keys(object).some(function (k) {
    if (k === key) {
      value = object[k]
      return true
    }
    if (object[k] && typeof object[k] === 'object') {
      value = findValForKey(object[k], key)
      return value !== undefined
    }
  })
  return value
}
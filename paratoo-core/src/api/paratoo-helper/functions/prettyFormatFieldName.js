'use strict'

module.exports = {
  /**
   * Formats a field name to a nicer looking format. Ideally this wouldn't be used
   * and instead there'd be a display name field in the documentation
   */
  prettyFormatFieldName: function(fieldName) {
    // the regex replaces all underlines and hyphens with spaces
    // The loop capitalises the first letter of every word
    let out = ''
    for (const word of fieldName.replace(/(-|_)/g, ' ').split(' ')) {
      out = `${out} ${word[0].toUpperCase()}${word.substring(1)}`
    }
    // remove leading space
    return out.trim()
  },

  /**
 * Serialises the plot name, similar to webapp's `serializePlotLocationName()`
 * 
 * @param {Object} plotNameObj plot name object (same as component)
 * @param {Boolean} idsProvided if the values of the `plotNameObject` are IDs instead of
 * LUT symbols
 * 
 * @returns {String} the serialised plot name
 */
  serialisePlotName: async function(plotNameObj, idsProvided = false) {
    if (idsProvided) {
      const fieldLutMapping = {
        state: 'lut-state',
        program: 'lut-program',
        bioregion: 'lut-bioregion',
      }
  
      let accum = ''
  
      for (const [fieldName, lutModelName] of Object.entries(fieldLutMapping)) {
        const resolvedLut = await strapi.entityService.findOne(`api::${lutModelName}.${lutModelName}`, plotNameObj[fieldName])
  
        accum += resolvedLut.symbol
      }
  
      return accum + plotNameObj.unique_digits
    }
  
    return `${plotNameObj.state}${plotNameObj.program}${plotNameObj.bioregion}${plotNameObj.unique_digits}`
  }
}
'use strict'
const axios = require('axios')
const {
  paratooOrgUrl,
  paratooOrgApiPrefix,
  paratooOrgCustomApiPrefix,
} = require('../../../constants')

module.exports = {
  /**
   * Submit post request
   *
   * @param {String} apiPath request path
   * @param {Object} payload json data
   * @param {String} bearer auth token
   *
   * @returns {Object} response with status code
   */
  orgPostRequestWithBody: async function (
    apiPath,
    payload,
    bearer,
    addDataField = true,
    isCustomApi = true,
  ) {
    const payloadWithData = {
      data: payload,
    }

    const url = isCustomApi
      ? `${paratooOrgUrl}${paratooOrgCustomApiPrefix}/${apiPath}`
      : `${paratooOrgUrl}${paratooOrgApiPrefix}/${apiPath}`

    const payloadToSend = !addDataField ? payload : payloadWithData
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    helpers.paratooDebugMsg(
      `Core is making a POST request to '${url}' with body: ${JSON.stringify(
        payloadToSend,
      )}`,
      true,
    )
    const headers = bearer ? { Authorization: bearer } : {}
    const resp = await axios
      .post(url, payloadToSend, {
        headers: headers,
      })
      .catch((err) => {
        console.log('catch err:', err)
        return {
          status: err.response ? err.response.status : 500,
          data: err,
        }
      })
    const parsedResp = parseResponseData(resp)
    helpers.paratooDebugMsg(
      `Core's POST request (with body) to '${url}' got parsed response data: ${JSON.stringify(
        parsedResp,
      )}`,
      true,
    )
    return {
      status: resp?.status || resp?.data?.status || 200,
      //can be an actual response, or an error response returned by org (if it's an error
      //it's up to the caller of this helper to handle it)
      data: parsedResp,
    }
  },

  /**
   * Submit put request
   *
   * @param {String} apiPath request path
   * @param {Object} payload json data
   * @param {String} bearer auth token
   *
   * @returns {Object} response with status code
   */
  orgPutRequestWithBody: async function (
    apiPath,
    payload,
    bearer,
    isCustomApi = true,
  ) {
    const url = isCustomApi
      ? `${paratooOrgUrl}${paratooOrgCustomApiPrefix}/${apiPath}`
      : `${paratooOrgUrl}${paratooOrgApiPrefix}/${apiPath}`

    const payloadToSend = {
      data: payload,
    }
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    helpers.paratooDebugMsg(
      `Core is making a PUT request to '${url}' with body: ${JSON.stringify(
        payloadToSend,
      )}`,
      true,
    )
    const headers = bearer ? { Authorization: bearer } : {}
    const resp = await axios
      .put(url, payloadToSend, {
        headers: headers,
      })
      .catch((err) => {
        return {
          status: err.response ? err.response.status : 500,
          data: err,
        }
      })
    const parsedResp = parseResponseData(resp)
    helpers.paratooDebugMsg(
      `Core's PUT request (with body) to '${url}' got parsed response data: ${JSON.stringify(
        parsedResp,
      )}`,
      true,
    )

    return {
      status: resp?.status || resp?.data?.status || 200,
      //can be an actual response, or an error response returned by org (if it's an error
      //it's up to the caller of this helper to handle it)
      data: parsedResp,
    }
  },

  orgGetRequest: async function (apiPath, bearer, isCustomApi = true) {
    const url = isCustomApi
      ? `${paratooOrgUrl}${paratooOrgCustomApiPrefix}/${apiPath}`
      : `${paratooOrgUrl}${paratooOrgApiPrefix}/${apiPath}`

    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    helpers.paratooDebugMsg(`Core is making a GET request to '${url}'`, true)
    const headers = bearer ? { Authorization: bearer } : {}
    const resp = await axios
      .get(url, {
        headers: headers,
      })
      .catch((err) => {
        return {
          status: err.response ? err.response.status : 500,
          data: err,
        }
      })

    const parsedResp = parseResponseData(resp)
    

    let meta
    try {
      meta = helpers.findValForKey(resp?.data, 'meta')
    } catch (err) {
      helpers.paratooWarnHandler(`Failed to find meta in response to ${url}`, err)
    }
    if (meta?.pagination?.pageCount && meta.pagination.pageCount > 1) {
      helpers.paratooDebugMsg(
        `Got paginated response to ${url}. Number of pages: ${meta.pagination.pageCount}. Expect to have ${meta.pagination.total} entries in the final response`,
        true,
      )
      helpers.paratooDebugMsg(
        `Initial request got parsed response data: ${JSON.stringify(
          parsedResp,
        )}`,
        true,
      )

      //start `currPage` at 1, as we've already fetched page 0
      for (let currPage = 1; currPage <= meta.pagination.pageCount; currPage++) {
        helpers.paratooDebugMsg(
          `Processing page number/index ${currPage}`,
          true,
        )
        let paginationUrl = String(url)
        //the `start` is the record number/index, so need to multiple by the `pageSize`,
        //which is the number of records per page
        let paginationParam = `pagination[start]=${currPage * meta.pagination.pageSize}`
        if (url.includes('?')) {
          paginationUrl += `&${paginationParam}`
        } else {
          paginationUrl += `?${paginationParam}`
        }
        helpers.paratooDebugMsg(
          `Pagination request URL: ${paginationUrl}`,
          true,
        )
        const paginationResp = await axios
          .get(paginationUrl, {
            headers: headers,
          })
          .catch((err) => {
            return {
              status: err.response ? err.response.status : 500,
              data: err,
            }
          })
        const parsedPaginatedResp = parseResponseData(paginationResp)

        if (parsedPaginatedResp?.status && parsedPaginatedResp.status !== 200) {
          //best we can do is warn here. caller of this function might recover, but if
          //not, there will be cascading affects, so the warning is useful for debugging
          helpers.paratooWarnHandler(
            `Failed to process paginated request: ${paginationUrl}`,
            new Error(`Received response code ${parsedPaginatedResp?.status} with message '${parsedPaginatedResp?.message}'`)
          )
          continue
        }

        helpers.paratooDebugMsg(
          `Request for page number/index=${currPage} got parsed response data: ${JSON.stringify(
            parsedPaginatedResp,
          )}`,
          true,
        )
        parsedResp.push(...parsedPaginatedResp)
      }
    }

    helpers.paratooDebugMsg(
      `Core's GET request to '${url}' got parsed response data: ${JSON.stringify(
        parsedResp,
      )}`,
      true,
    )

    if (meta && parsedResp?.length != meta.pagination.total) {
      helpers.paratooDebugMsg(
        `Paginated responses were processed, but the final response length (${parsedResp?.length}) is not equal to the expected count (${meta.pagination.total})`,
        true,
      )
    }

    return {
      status: resp?.status || resp?.data?.status || 200,
      //can be an actual response, or an error response returned by org (if it's an error
      //it's up to the caller of this helper to handle it)
      data: parsedResp,
    }
  },

  parseResponseData: parseResponseData,  
}

/**
 * Parses an API response by either handling an API error from a catch block, or
 * unwrapping nested `data` object properties. The caller of this function will typically
 * then wrap it in a `data` object, resulting in `{ data: { foo: "bar" } }` (no
 * double-nesting of `data.data...`)
 * 
 * @param {Object} resp API response object
 * 
 * @returns {Object} parsed API response with just the information in the bottom data
 */
function parseResponseData (resp) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  if (resp?.data?.response?.data?.error) {
    helpers.paratooDebugMsg('parseResponseData handling error case 1 for path resp.data.response.data.error', true)
    //a `catch` block returned error data
    return resp.data.response.data.error
  }
  if (
    resp?.data?.response?.data?.message &&
    //need to ensure it's not a false-positive
    resp?.data?.response?.data?.code !== 200
  ) {
    helpers.paratooDebugMsg('parseResponseData handling error case 2 for path resp.data.response.data', true)
    //a `catch` block returned error data from MERIT's org
    return resp.data.response.data
  }
  
  //normal API response
  if (resp.data) {
    if (resp.data.data) {
      return resp.data.data
    }
    if (resp?.data?.response?.data) {
      return resp.data.response.data
    }
    
    return resp.data
  }
  
  return resp
}

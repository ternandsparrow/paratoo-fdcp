const publishCollection = require('./publishCollection')

module.exports = async (
  collectionName,
  collectionData,
  dataToUpdateWhenPublished = {},
) => {
  const scheduledTime = new Date(collectionData.scheduledAt).getTime()
  const currentTime = Date.now()
  if (scheduledTime <= currentTime) {
    strapi.log.info(
      `Outdated scheduled message, publishing ${collectionName} ID: ${collectionData.id} immediately`,
    )
    await publishCollection(
      collectionName,
      collectionData.id,
      dataToUpdateWhenPublished,
    )
  } else {
    strapi.log.info(
      `Publishing ${collectionName} ID: ${collectionData.id} at ${new Date(
        scheduledTime,
      )}`,
    )
    // create a global object to store setTimeout ids
    if (!global.scheduledTasks) {
      global.scheduledTasks = {}
    }
    const timeoutId = setTimeout(
      () =>
        publishCollection(
          collectionName,
          collectionData.id,
          dataToUpdateWhenPublished,
        ),
      scheduledTime - currentTime,
    )
    global.scheduledTasks[collectionData.id] = {
      timeoutId,
      scheduledTime: scheduledTime,
    }
  }
}

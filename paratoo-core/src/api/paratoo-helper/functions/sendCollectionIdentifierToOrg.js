module.exports = {
  sendCollectionIdentifierToOrg: async function (orgMintedIdentifier, aAuthentication, entry, collectionIdSubmitted) {
    if (!entry) {
      return {
        error: 'Filed to store opportune inside core',
      }
    }
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    
    // collection data and decrypted hash
    const collectionData = helpers.createCollectionBody(orgMintedIdentifier)
    // checks whether collection has already been submitted or not
    const existingCollection = await helpers.orgGetRequest(
      `status/${collectionData.orgMintedUUID}`,
      aAuthentication,
    )
    if (existingCollection.status != 200) {
      return helpers.paratooErrorHandler(
        existingCollection.status,
        existingCollection.data,
        'Failed to check status of org minted uuid',
      )
    }
    
    
    const existingCollectionKeys = existingCollection.data
      ? Object.keys(existingCollection.data)
      : []

    // if submitted we returns
    if (existingCollectionKeys.includes('isSubmitted')) {
      existingCollection.data.isSubmitted = existingCollection.data.isSubmitted ? true : false
      if (collectionIdSubmitted != existingCollection.data.isSubmitted) {
        let msg =
          'Collection has already been submitted; the collection identifier already exists'
        return {
          error: msg,
        }
      }
    }

    // POST collection to org to "Notify paratoo-org about submission"
    // if the collection id has not been submitted 
    if (!collectionIdSubmitted) {
      const resp = await helpers.orgPostRequestWithBody(
        'collection',
        collectionData,
        aAuthentication,
        false,
      )
      if (resp.status != 200) {
        // if error occurs
        return {
          error: resp,
        }
      }
    }

    return {
      error: null,
    }
  },
}

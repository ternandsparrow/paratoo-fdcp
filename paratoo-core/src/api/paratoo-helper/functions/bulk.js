'use strict'
const { defaultSanitizeOutput } = require('@strapi/utils').sanitize.sanitizers
const _ = require('lodash')

module.exports = {
  //TODO documentation
  //  should contain collections with an identifier, survey, and obs
  //TODO handle when no location/layout/visit
  //  we assume that these are included, which they usually are. but we should still
  //  handle the cases when they aren't, i.e., they're not required (so don't try create)
  //  anything; or they're required, in which case the validator should 400

  // We probably want to refactor this to make each "section" of the process a separate function

  // Refactoring names to remove references to "weather" might be more descriptive

  /**
   * A generic function for bulk uploading a number of various collections, which don't
   * have to be of the same protocol, but should be formatted according to the
   * documentation defined in paratoo-fdcp/docs/ARCHITECTURE/core-bulk.md
   *
   * It takes the model names of at least the survey that is being created, and optionally,
   * the weather observation and survey observation(s). It will also create a new plot
   * location and plot visit if they are provided, or will insert references if provided
   * with ones that already exist.
   *
   * @param {Object} ctx Koa context
   * @param {String} surveyModelName The model name of the survey, e.g., `bird-survey`
   * @param {String} [obsModelName] The model name of the survey observations, e.g., `bird-survey-observation`
   * @param {String, Array} [generalEntryModelNames] The model names of any generic (non-main/non-multi) model entry, e.g., `weather-observation`
   * @param {String, Array} [childObservationModelName] A single string, or Array of Strings of model names for fields within the observation payload
   * @returns {Array} Objects of the items in the collection that were created
   */
  async genericBulk({
    ctx,
    surveyModelName,
    obsModelName = null,
    generalEntryModelNames = null,
    childObservationModelName = null,
  }) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    let msg = `Bulk submission for survey model name '${surveyModelName}'`
    if (obsModelName) msg += `, observation model name '${obsModelName}'`
    if (childObservationModelName) msg += `, child observation model name '${childObservationModelName}'`
    if (generalEntryModelNames) msg += `, general entry model name '${generalEntryModelNames}'`
    helpers.paratooDebugMsg(msg, true)    //create breadcrumb

    const paratooErrorHandler = (code, err, msg, errorCode = null) => {
      return helpers.paratooErrorHandler(code, err, msg, true, errorCode)
    }
    const handleOrg404 = (resp, survey_metadata, msg) => {
      if (
        resp?.data?.message?.toLowerCase()?.includes(
          'no data set found with mintedcollectionid'
        )
      ) {
        //we send a special error code to the app so it knows to call
        //`/mint-identifier` again
        helpers.paratooDebugMsg(`Collection with orgMintedUUID=${survey_metadata.orgMintedUUID} does not have an orgMintedIdentifier record in org`, true)
        return paratooErrorHandler(
          400,
          new Error(msg),
          null,   //no additional message
          'ERR_MISSING_MINTED_ORG_IDENTIFIER',  //additional error code
        )
      }

      //treat it like the rest of the 4xx status codes above (as it's not a special
      //404 case)
      return paratooErrorHandler(resp.status, new Error(msg))
    }
    
    const requestBody = ctx.request.body
    helpers.paratooDebugMsg(
      `Request body: ${JSON.stringify(requestBody, null, 2)}`,
      true,    //create breadcrumb
    )
    const authToken = ctx.request.headers['authorization']

    const childObsNames = !childObservationModelName
      ? []
      : Array.isArray(childObservationModelName)
        ? childObservationModelName
        : [childObservationModelName]

    // array of results to return as the body in response to the request
    let results = []
    // we know body has collections because it passed the validator
    for (let collection of requestBody.data.collections) {
      const decodedIdentifier = JSON.parse(Buffer.from(collection.orgMintedIdentifier, 'base64'))
      const survey_metadata = decodedIdentifier.survey_metadata

      if (!survey_metadata.provenance) {
        const err = new Error(`provenance not found in survey_metadata:  ${JSON.stringify(survey_metadata)}`)
        return helpers.paratooErrorHandler(400, err)
      }

      // update version
      survey_metadata.provenance['version_core'] = `${process.env.VERSION}-${process.env.GIT_HASH}`
      survey_metadata.provenance['system_core'] = `Monitor FDCP API-${process.env.NODE_ENV}`

      // if not valid provenance
      if (
        !survey_metadata.provenance.version_core ||
        !survey_metadata.provenance.system_core
      ) {
        // don't need to stop processing as these are optional fields but if it happens we need fix this.
        let msg = `version_core/system_core is missing in survey_metadata: ${JSON.stringify(survey_metadata)}`
        msg += `, VERSION: ${process.env.VERSION}, GIT_HASH: ${process.env.GIT_HASH}, Build: ${process.env.NODE_ENV}`
        helpers.paratooWarnHandler(msg)
        strapi.log.error(msg)
      }


      //check if collection has already been POSTed
      let collectionStatusResp = null
      try {
        collectionStatusResp = await helpers.orgGetRequest(
          `status/${survey_metadata.orgMintedUUID}`,
          authToken,
          true,   //`isCustomApi`
        )
      } catch (err) {
        let useMsg = 'Unknown error'
        let useStatus = 500
        if (err?.message) {
          useMsg = err.message
        }
        if (err?.status) {
          useStatus = err.status
        }

        return paratooErrorHandler(useStatus, new Error(useMsg))
      }
      console.log('collectionStatusResp:', JSON.stringify(collectionStatusResp, null, 2))

      if (
        collectionStatusResp?.status &&
        collectionStatusResp.status !== 200
      ) {
        let msg = 'There was an issue looking up if the collection identifier exists'
        if (collectionStatusResp?.data?.message) {
          msg += `. Got error: ${collectionStatusResp.data.message}`
        } else {
          helpers.paratooDebugMsg(`Collection with orgMintedUUID=${survey_metadata.orgMintedUUID} received API error for /status call, but no additional message`, true)
        }

        switch (collectionStatusResp.status) {
          case 400:
          case 401:
          case 403: {
            return paratooErrorHandler(collectionStatusResp.status, new Error(msg))
          }
          case 404: {
            return handleOrg404(collectionStatusResp, survey_metadata, msg)
          }
          default: {
            return paratooErrorHandler(500, new Error(msg))
          }
        }
      } else if (!collectionStatusResp?.status) {
        const msg = `Failed to lookup collection status for orgMintedUUID=${survey_metadata.orgMintedUUID} as no status was provided`
        return paratooErrorHandler(500, new Error(msg))
      } else {
        if (collectionStatusResp.data.isSubmitted) {
          let msg =
            'Collection has already been submitted; the collection identifier already exists'
          return paratooErrorHandler(400, new Error(msg))
        } else {
          helpers.paratooDebugMsg(`Collection with orgMintedUUID=${survey_metadata.orgMintedUUID} has not been submit`, true)
        }
      }

      const surveyModel = strapi.getModel(
        helpers.modelToApiName(surveyModelName),
      )
      const survey = collection[surveyModelName].data
      
      // update core version
      if (survey) {
        survey.survey_metadata.provenance = survey_metadata.provenance
        survey.survey_metadata.orgMintedUUID = survey_metadata.orgMintedUUID
      }

      //check if survey_metadata in decrypted identifier is the same as the one provided in the
      //survey, ensuring correctness. strapi enforces unique constraint
      if (
        survey_metadata.survey_details.survey_model != survey.survey_metadata.survey_details.survey_model ||
        survey_metadata.survey_details.time != survey.survey_metadata.survey_details.time ||
        survey_metadata.survey_details.uuid != survey.survey_metadata.survey_details.uuid
      ) {
        let msg =
          `The survey ID of the survey (${JSON.stringify(
            survey.survey_metadata,
          )}) does` +
          ` not match the one provided in the Org Minted Identifier (${JSON.stringify(
            survey_metadata,
          )})`
        return paratooErrorHandler(400, new Error(msg))
      }
      strapi.log.debug('survey_metadatas in the survey and orgMintedIdentifier match')
      const protocolId = survey_metadata.survey_details.protocol_id
      const isPlotBased = await helpers.isPlotBasedProtocol({ protocolUuid: protocolId })

      //this transaction will consider 4 cases regarding plot layouts, and visits:
      //  - existing layout, existing visit
      //  - existing layout,new visit
      //  - new layout, new visit
      //  - existing layout (core plot only) and (new or existing) visit, new fauna plot
      //it will then create a survey and insert the following (typically) optional fields:
      //  - a 'general entity' (e.g.,weather observation)
      //  - observations (0..* models) pertaining to the survey
      //  - child observations (if applicable in cases such as point intercept)
      //if any insertions fail, the transaction will rollback.
      //it will return only the fields it has inserted in the response
      //FIXME the checks and queries associated with plot layouts/locations/visits can
      //  be generalized and broken out into a separate function (within this file)
      try {
        await strapi.db.transaction(async () => {
          const layout = collection['plot-layout']
          let updatedLayout = null
          if (
            layout &&
            layout.id &&
            layout.fauna_plot_point &&
            !layout.plot_points
          ) {
            strapi.log.debug(
              `Detected new Fauna Plot for existing Veg Plot, so updating Veg Plot=${layout.id} with new Fauna Plot Points`,
            )
            const mappedFaunaPoints = []
            for (const point of layout.fauna_plot_point) {
              let currPoint = point
              //use query engine as entity service is difficult to query a single item using
              //the `where` condition
              const resolved = await strapi.db
                .query('api::lut-fauna-plot-point.lut-fauna-plot-point')
                .findOne({
                  select: ['id', 'symbol', 'label'],
                  where: {
                    symbol: currPoint.name,
                  },
                  populate: true,
                })
                .catch((err) => {
                  throw paratooErrorHandler(500, err)
                })
              currPoint.name = resolved.id
              mappedFaunaPoints.push(currPoint)
            }

            updatedLayout = await strapi.entityService
              .update('api::plot-layout.plot-layout', layout.id, {
                data: {
                  fauna_plot_point: mappedFaunaPoints,
                },
              })
              .catch((err) => {
                throw paratooErrorHandler(500, err)
              })
          }

          const plotLayoutModelName = 'plot-layout'
          const plotLayoutModel = strapi.getModel(
            helpers.modelToApiName(plotLayoutModelName),
          )

          // const plotLocationModelName = 'plot-location'
          const plotVisitModelName = 'plot-visit'
          let hasPlot =
            // _.get(collection, plotLocationModelName) ||
            _.get(collection, plotLayoutModelName) ||
            _.get(collection, plotVisitModelName)
          let plotLayoutEntity, plotVisitEntity, plotVisitId
          if (hasPlot) {
            // const plotLocationModel = strapi.getModel(
            //   helpers.modelToApiName(plotLocationModelName),
            // )
            const plotLayout = collection[plotLayoutModelName].data

            // Create new plot-layout (insert location ID if possible)

            //check if new or existing plot layout was provided
            //if it's only an ID, we know the client sent an existing layout
            
            if (!_.get(collection, 'plot-layout.id')) {
              strapi.log.debug('Provided with a new plot layout, creating entry...')
              await strapi.entityValidator.validateEntityCreation(
                plotLayoutModel,
                plotLayout,
              )

              //create new plot layout
              //use entity service engine so we can populate the `plot_points` component
              plotLayoutEntity = await strapi.entityService.create(
                plotLayoutModel.uid,
                {
                  data: plotLayout,
                  populate: 'deep',
                },
              )
            }

            const plotVisitModel = strapi.getModel(
              helpers.modelToApiName(plotVisitModelName),
            )
            let plotVisit = collection[plotVisitModelName].data
            //check if new or existing plot visit was provided
            //if it's only an ID, we know the client sent an existing visit
            if (!_.get(collection, 'plot-visit.id')) {
              strapi.log.debug('Provided with a new plot visit, creating entry...')
              //double-check that plot-visit doesn't already exist
              let plotLayoutId = null
              if (plotLayoutEntity) {
                plotLayoutId = plotLayoutEntity?.id
              } else {
                plotLayoutId = plotVisit?.plot_layout
              }
              
              await checkDuplicateVisit({
                plotVisitModel,
                plotVisit,
                plotLayoutId,
              })

              await strapi.entityValidator.validateEntityCreation(
                plotVisitModel,
                plotVisit,
              )

              //link the visit to the newly-created layout
              if (plotLayoutEntity) {
                Object.assign(plotVisit, {
                  plot_layout: plotLayoutEntity.id
                })
              }
              //create new plot visit
              plotVisitEntity = await strapi.entityService.create(
                plotVisitModel.uid,
                {
                  data: plotVisit,
                  populate: 'deep',
                },
              )
            } else {
              //get the plot visit ID so we can use it as a reference in the survey
              plotVisitId = _.get(collection, 'plot-visit.id')
            }

            //if we didn't find existing plot visit ID (i.e., the plot visit is new), then
            //we assign the ID of the newly-created plot visit
            if (isPlotBased && survey && surveyModel?.attributes) {
              let surveysVisitFieldName = null
              for (const [attributeName, attributeData] of Object.entries(surveyModel.attributes)) {
                if (
                  attributeData.type === 'relation' &&
                  attributeData.target === plotVisitModel.uid
                ) {
                  surveysVisitFieldName = attributeName
                  break
                }
              }
              if (surveysVisitFieldName) {
                survey[surveysVisitFieldName] = plotVisitId || plotVisitEntity.id
              } else {
                let msg = `Plot-based protocol's survey with identifier ${protocolId} could not create relation to plot visit`
                helpers.paratooDebugMsg(msg, true)    //create breadcrumb
                throw paratooErrorHandler(500, new Error(msg))
              }
            }
          }
          
          helpers.paratooDebugMsg(`Protocol ${isPlotBased ? 'is' : 'is not'} plot-based`, true)
          if (
            isPlotBased &&
            !survey?.plot_visit
          ) {
            let msg = `Plot-based protocol with identifier ${protocolId} could not create relation to plot visit`
            helpers.paratooDebugMsg(msg, true)    //create breadcrumb
            throw paratooErrorHandler(500, new Error(msg))
          }

          //create survey entry
          const surveyFailureMessages = []
          const surveyFailureStatuses = []
          const surveyFailureUniqueFields = []
          const surveyEntity = await strapi.entityService
            .create(surveyModel.uid, {
              data: survey,
              populate: 'deep'
            })
            .catch((err) => {
              const {
                messages,
                statuses,
                uniqueFields,
              } = handleCaughtEntityFailure({ err })
              surveyFailureMessages.push(...messages)
              surveyFailureStatuses.push(...statuses)
              surveyFailureUniqueFields.push(...uniqueFields)
            })

          const finalErrMsgs = []
          const allStatuses = []
          if (
            surveyFailureMessages.length > 0 ||
            surveyFailureStatuses.length > 0 ||
            surveyFailureUniqueFields.length > 0
          ) {
            const {
              errMsgs,
              statuses,
            } = createEntityFailureMessage({
              failure: {
                messages: surveyFailureMessages,
                statuses: surveyFailureStatuses,
                uniqueFields: surveyFailureUniqueFields,
              }
            })
            allStatuses.push(...statuses)

            finalErrMsgs.push(
              `Survey has failure${
                surveyFailureStatuses.length > 1 ? 's' : ''
              }: ${errMsgs.join(', ')}`
            )
          }
          if (finalErrMsgs.length > 0) {
            throw createEntityFailureError({
              finalErrMsgs,
              allStatuses,
            })
          }

          //create observations entries (optional)
          if (!Array.isArray(obsModelName)) {
            //if there's multiple obsModelNames we want to loop over them, but to reduce code
            //duplication we wrap the single same in an array
            obsModelName = [obsModelName]
          }
          //need for child obs, which shouldn't happen when there are multiple `obsModelNames`
          let obsModel = null
          let obsEntities = []
          const childObEntities = {}
          for (let obsItem in obsModelName) {
            let obs
            obsModel = strapi.getModel(
              helpers.modelToApiName(obsModelName[obsItem]),
            )

            if (obsModelName[obsItem]) {
              obs = collection[obsModelName[obsItem]]
              // grab the association's alias for the bird-observation's ref field so we can
              // apply the new survey id as references in the new observation instances
              let surveyReferenceFieldName = helpers.findAssociationAttributeName(
                obsModel.modelName,
                surveyModel.uid,
              )
              if (!surveyReferenceFieldName) {
                let msg = `Could not get survey reference field name for observation model $${obsModelName[obsItem]}`
                helpers.paratooDebugMsg(msg, true)    //create breadcrumb
                throw paratooErrorHandler(500, new Error(msg))
              }

              // for non repeatable single obervation
              // need to be turned to an array
              !Array.isArray(obs) && (obs = [obs])

              const obFailures = {}
              // add observations to database
              for (const [obIdx, o] of obs.entries()) {
                obFailures[obIdx] = {
                  messages: [],
                  statuses: [],
                  uniqueFields: [],
                }
                let modifiedO = _.cloneDeep(o)?.data
                // remove child obs from parent ob body before adding
                for (const childObName of childObsNames) {
                  const childObApiName = helpers.modelToApiName(childObName)
                  // find field name (alias) from target model name
                  delete modifiedO[
                    helpers.findAssociationAttributeName(
                      obsModel.modelName,
                      childObApiName,
                    )
                  ]
                }
                try {
                  // create observation with deep population
                  const obsEntity = await strapi.entityService.create(
                    obsModel.uid,
                    {
                      data: {
                        ...modifiedO,
                        [surveyReferenceFieldName]: surveyEntity.id, // link to new survey
                      },
                      populate: 'deep'
                    }
                  )
                  obsEntities.push({ modelName: obsModel.modelName, ...obsEntity })
                } catch (err) {
                  const {
                    messages,
                    statuses,
                    uniqueFields,
                  } = handleCaughtEntityFailure({ err })
                  obFailures[obIdx].messages.push(...messages)
                  obFailures[obIdx].statuses.push(...statuses)
                  obFailures[obIdx].uniqueFields.push(...uniqueFields)
                }
              }

              const finalErrMsgs = []
              const allStatuses = []
              const obFailuresEntries = Object.entries(obFailures)
              if (obFailuresEntries?.length > 0) {
                for (const [obIdx, failure] of obFailuresEntries) {
                  if (
                    failure?.messages.length > 0 ||
                    failure?.statuses.length > 0 ||
                    failure?.uniqueFields.length > 0
                  ) {
                    const {
                      errMsgs,
                      statuses,
                    } = createEntityFailureMessage({ failure })

                    finalErrMsgs.push(
                      `Observation at index=${obIdx} has failure${
                        failure.statuses.length > 1 ? 's' : ''
                      }: ${errMsgs.join(', ')}`
                    )
                    allStatuses.push(...statuses)
                  }
                }
              }

              if (finalErrMsgs.length > 0) {
                throw createEntityFailureError({
                  finalErrMsgs,
                  allStatuses,
                })
              }
            }

            //handle when observation has child
            //the creation of the parent observation will ignore this child, as it is
            //not in its model
            //save reference for each new ID for each modelName
            await insertChildObsToDb(childObsNames, childObEntities, helpers, obsModel, obs, obsEntities)
          }
          let generalEntities = []
          if (generalEntryModelNames) {
            if (!_.isArray(generalEntryModelNames))
              generalEntryModelNames = [generalEntryModelNames]

            const generalEntryFailures = {}
            for (let name of generalEntryModelNames) {
              generalEntryFailures[name] = {}
              let generalEntryModel = strapi.getModel(helpers.modelToApiName(name))
              let generalEntries
              if (Array.isArray(collection[name])) {
                generalEntries = collection[name]
              } else {
                generalEntries = [collection[name]]
              }
              for (const [geIdx, generalEntry] of generalEntries.entries()) {
                generalEntryFailures[name][geIdx] = {
                  messages: [],
                  statuses: [],
                  uniqueFields: [],
                }
                let generalEntryData = generalEntry.data
                // grab the association's alias for the observation's ref field so we can
                // apply the new survey id as references in the new observation instances
                generalEntryData[
                  helpers.findAssociationAttributeName(
                    generalEntryModel.modelName,
                    surveyModel.uid,
                  )
                ] = surveyEntity.id

                if (
                  !generalEntities[name] ||
                  !Array.isArray(generalEntities[name])
                ) {
                  generalEntities[name] = []
                }

                const generalEntryResp = await strapi.entityService.create(
                  generalEntryModel.uid, {
                    data: generalEntryData,
                    populate: 'deep'
                  })
                  .catch((err) => {
                    const {
                      messages,
                      statuses,
                      uniqueFields,
                    } = handleCaughtEntityFailure({ err })
                    generalEntryFailures[name][geIdx].messages.push(...messages)
                    generalEntryFailures[name][geIdx].statuses.push(...statuses)
                    generalEntryFailures[name][geIdx].uniqueFields.push(...uniqueFields)
                  })


                generalEntities[name].push(
                  generalEntryResp,
                )
              }
            }

            const finalErrMsgs = []
            const allStatuses = []
            const generalEntryFailureKeys = Object.keys(generalEntryFailures)
            if (generalEntryFailureKeys.length > 0) {
              for (const generalEntryModelName of generalEntryFailureKeys) {
                for (const [geIdx, failure] of Object.entries(generalEntryFailures[generalEntryModelName])) {
                  if (
                    failure?.messages.length > 0 ||
                    failure?.statuses.length > 0 ||
                    failure?.uniqueFields.length > 0
                  ) {
                    const {
                      errMsgs,
                      statuses,
                    } = createEntityFailureMessage({ failure })
          
                    finalErrMsgs.push(
                      `General entry with model name '${generalEntryModelName}' at index=${geIdx} has failure${
                        failure.statuses.length > 1 ? 's' : ''
                      }: ${errMsgs.join(', ')}`
                    )
                    allStatuses.push(...statuses)
                  }
                }
              }
            }

            if (finalErrMsgs.length > 0) {
              throw createEntityFailureError({
                finalErrMsgs,
                allStatuses,
              })
            }
          }

          // Construct, sanitize, await return values for generalEntries
          let sanitizedGeneralResults = {}
          if (generalEntryModelNames) {
            for (const generalEntryName of generalEntryModelNames) {
              await defaultSanitizeOutput(
                strapi.getModel(helpers.modelToApiName(generalEntryName)),
                generalEntities[generalEntryName],
              ).then((data) => {
                sanitizedGeneralResults[generalEntryName] = data
              })
            }
          }

          // Construct, sanitize, await return values for child observations
          let sanitizedChildObResults = {}
          for (const childObsName of childObsNames) {
            sanitizedChildObResults[childObsName] = []
            for (const childObEntity of childObEntities[childObsName]) {
              await defaultSanitizeOutput(
                strapi.getModel(helpers.modelToApiName(childObsName)),
                childObEntity,
              ).then((data) => {
                sanitizedChildObResults[childObsName].push(data)
              })
            }
          }
          const result = {
            [plotLayoutModelName]: await defaultSanitizeOutput(
              strapi.getModel(helpers.modelToApiName(plotLayoutModelName)),
              plotLayoutEntity,
            ),
            // [plotFaunaLayoutModelName]: await defaultSanitizeOutput(
            //   strapi.getModel(helpers.modelToApiName(plotFaunaLayoutModelName)),
            //   plotFaunaLayoutEntity,
            // ),
            // [plotLocationModelName]: await defaultSanitizeOutput(
            //   strapi.getModel(helpers.modelToApiName(plotLocationModelName)),
            //   plotLocationEntity,
            // ),
            [plotVisitModelName]: await defaultSanitizeOutput(
              strapi.getModel(helpers.modelToApiName(plotVisitModelName)),
              plotVisitEntity,
            ),
            /**
                [generalEntryModelNames]: await defaultSanitizeOutput(strapi.getModel(helpers.modelToApiName(generalEntryModelNames)),generalEntity),
                */
            ...sanitizedGeneralResults,
            [surveyModelName]: await defaultSanitizeOutput(
              strapi.getModel(helpers.modelToApiName(surveyModelName)),
              surveyEntity,
            ),
            // Find, format, sanitize, return all the child observation returns
            ...sanitizedChildObResults,
          }
          if (updatedLayout) {
            result['plot-layout'] = updatedLayout
          }
          // Format if we have multiple observations
          if (Array.isArray(obsModelName)) {
            for (const index in obsModelName) {
              if (!obsModelName[index]) continue
              const obsEntity = obsEntities.filter(
                (obs) => obs.modelName === obsModelName[index],
              )
              result[obsModelName[index]] = await defaultSanitizeOutput(
                strapi.getModel(helpers.modelToApiName(obsModelName[index])),
                obsEntity,
              )
            }
          } else {
            result[obsModelName] = await defaultSanitizeOutput(
              strapi.getModel(helpers.modelToApiName(obsModelName)),
              obsEntities,
            )
          }
          results.push(result)

          // POST collection to org to "Notify paratoo-org about submission"
          const collectionBody = {
            orgMintedUUID: survey_metadata.orgMintedUUID,
            coreProvenance: {
              system_core: survey_metadata.provenance.system_core,
              version_core: survey_metadata.provenance.version_core,
            },
          }
          strapi.log.debug(`/collection making request at time: ${Date.now()}`)
          //TODO handle when this succeeds but org-uuid-survey-metadata fails (might get addressed by #1854 when we move these org requests outside of the transaction, which will result in only making this /collection call when the insert into the org-uuid table succeeds)
          //  - will result in collection data being rolled-back (which is what we want)
          //  - but org would have already received the 'success' from /collection
          //  - likely want a 'undo' endpoint (or use existing /collection endpoint with
          //    flag) in org to reverse the /collection call. For now, add a warning in
          //    the `catch` block for org-uuid-survey-metadata so that we at least become
          //    aware
          let collectionHookResp = null
          try {
            collectionHookResp = await helpers.orgPostRequestWithBody(
              'collection',
              collectionBody,
              authToken,
              false,    //don't wrap in `data` object
              true,   //`isCustomApi`
            )
          } catch (err) {
            strapi.log.debug(`/collection request error at time: ${Date.now()}`)
            let useMsg = 'Unknown error'
            let useStatus = 500
            if (err?.message) {
              useMsg = err.message
            }
            if (err?.status) {
              useStatus = err.status
            }

            return paratooErrorHandler(useStatus, new Error(useMsg))
          }
          strapi.log.debug(`/collection request finished at time: ${Date.now()}`)
          //only need to check for error response, as this org endpoint won't send a
          //response message/data for an 'ok' (200) response
          if (
            collectionHookResp?.status &&
            collectionHookResp.status !== 200
          ) {
            let msg = 'There was an issue notifying org of the submission'
            if (collectionHookResp?.data?.message) {
              msg += `. Got error: ${collectionHookResp.data.message}`
            } else {
              helpers.paratooDebugMsg(`Collection with orgMintedUUID=${survey_metadata.orgMintedUUID} received API error for /collection call, but no additional message`, true)
            }
            switch (collectionHookResp.status) {
              case 400:
              case 401:
              case 403: {
                return paratooErrorHandler(collectionHookResp.status, new Error(msg))
              }
              case 404: {
                return handleOrg404(collectionHookResp, survey_metadata, msg)
              }
              default: {
                return paratooErrorHandler(500, new Error(msg))
              }
            }
          } else if (!collectionHookResp?.status) {
            const msg = `Failed to notify org of submission for orgMintedUUID=${survey_metadata.orgMintedUUID} as no status was provided`
            return paratooErrorHandler(500, new Error(msg))
          }

          // store uuid and associated survey metadata
          try {
            await strapi.entityService.create(
              'api::org-uuid-survey-metadata.org-uuid-survey-metadata',
              {
                data: {
                  org_minted_uuid: survey_metadata.orgMintedUUID,
                  survey_details: _.cloneDeep(survey.survey_metadata.survey_details),
                  provenance: _.cloneDeep(survey.survey_metadata.provenance),
                  org_opaque_user_id: survey.survey_metadata.org_opaque_user_id
                }
              },
            )
          } catch (error) {
            //see todo above /collection call on why we have this warning (which might get addressed by #1854)
            helpers.paratooWarnHandler(`Failed to create Org UUID survey metadata: ${survey_metadata.orgMintedUUID}. Survey data will be rolled-back, but org needs to be notified that /collection should be reversed.`)
            
            throw paratooErrorHandler(500, error)
          }
        })
      } catch (error) {
        const status = error.status || 500
        return paratooErrorHandler(
          status,
          error,
          null,   //no additional `msg`
          error?.details?.errorCode,  //pass on the `errorCode` if provided
        )
      }
    }
    ctx.body = results
    return results
  },
}
async function insertChildObsToDb(childObsNames, childObEntities, helpers, obsModel, obs, obsEntities) {
  const relatedObsEntities = obsEntities.filter(obsEntity => obsEntity.modelName === obsModel.modelName)
  if(!relatedObsEntities?.length) return
  for (const childObName of childObsNames) {
    if(!childObEntities[childObName]) {
      childObEntities[childObName] = []
    }
    // Check whether we need to add the references to the parent/child/both
    // check whether we need to insert ID of observation in child observation
    const childObModel = strapi.getModel(
      helpers.modelToApiName(childObName)
    )

    let parentsChildFieldName
    let parentsChildFieldNameIsMultiple
    let childsParentFieldName = helpers.findAssociationAttributeName(
      childObModel.modelName,
      obsModel.uid
    )


    // check whether we need to add list of child observations in the parent
    for (const [attributeKey, attribute] of Object.entries(
      helpers.modelAssociations(obsModel.modelName)
    )) {
      if (attribute.target === childObModel.uid) {
        parentsChildFieldName = attributeKey
        parentsChildFieldNameIsMultiple = attribute.relation === 'oneToMany'
        break
      }
    }
    // go through created obs and create childOb
    // Attach created child Ob id to the ob created
    const childObFailures = {}
    for (const [observationIndex, observation] of Object.entries(obs)) {
      childObFailures[observationIndex] = {}
      // TODO confirm if this index lines up
      const backendObsID = relatedObsEntities[observationIndex].id
      let idsToAddToParent = []

      if (!observation.data[parentsChildFieldName]) {
        continue
      }
      // TODO Check if the doco is expecting an array (It should've already been validated however)
      const childrenToAdd = Array.isArray(
        observation.data[parentsChildFieldName]
      )
        ? observation.data[parentsChildFieldName]
        : [observation.data[parentsChildFieldName]]
      
      for (let [childObIdx, childObValue] of childrenToAdd.entries() || [[],[]]) {
        childObFailures[observationIndex][childObIdx] = {
          messages: [],
          statuses: [],
          uniqueFields: [],
        }
        childObValue = childObValue.data
        // put parent ID into child (parent should already exist)
        if (childsParentFieldName) {
          childObValue[childsParentFieldName] = backendObsID
        }
        // in any case, create the child obs
        const childObReturn = await strapi.entityService.create(
          childObModel.uid,
          {
            data: childObValue,
            populate: 'deep'
          }
        )
          .catch((err) => {
            const {
              messages,
              statuses,
              uniqueFields,
            } = handleCaughtEntityFailure({ err })
            childObFailures[observationIndex][childObIdx].messages.push(...messages)
            childObFailures[observationIndex][childObIdx].statuses.push(...statuses)
            childObFailures[observationIndex][childObIdx].uniqueFields.push(...uniqueFields)
          
          })
        // Save id in relation to this parent in case the parent has a reference to child
        idsToAddToParent.push(childObReturn?.id)

        // separately store list of all added children to send to user in response
        childObEntities[childObName].push(childObReturn)
      }

      // update the parent with array of new ids
      if (parentsChildFieldName) {
        await strapi.db.query(obsModel.uid).update({
          where: {
            id: backendObsID,
          },
          data: {
            [parentsChildFieldName]: parentsChildFieldNameIsMultiple
              ? idsToAddToParent
              : idsToAddToParent[0],
          }
        })
        const updateReturn = await strapi.db.query(obsModel.uid).findOne({
          where: {
            id: backendObsID,
          },
          populate: true,
        })
        const obsEntityIndex = obsEntities.findIndex(({ id }) => id === backendObsID)
        obsEntities[obsEntityIndex] = { ...obsEntities[obsEntityIndex], ...updateReturn }
      }
    }

    const finalErrMsgs = []
    const allStatuses = []
    const childObFailureKeys = Object.keys(childObFailures)
    if (childObFailureKeys?.length > 0) {
      for (const obIdx of childObFailureKeys) {
        for (const [childObIdx, failure] of Object.entries(childObFailures[obIdx])) {
          if (
            failure?.messages.length > 0 ||
            failure?.statuses.length > 0 ||
            failure?.uniqueFields.length > 0
          ) {
            const {
              errMsgs,
              statuses,
            } = createEntityFailureMessage({ failure })
  
            finalErrMsgs.push(
              `Child observation at index=${childObIdx} for observation at index=${obIdx} has failure${
                failure.statuses.length > 1 ? 's' : ''
              }: ${errMsgs.join(', ')}`
            )
            allStatuses.push(...statuses)
          }
        }
      }
    }

    if (finalErrMsgs.length > 0) {
      throw createEntityFailureError({
        finalErrMsgs,
        allStatuses,
      })
    }
  }
  return childObEntities
}

/**
 * Handles the error caught when we try to create data with entityService. Returns nicely-formatted messages, statues, and unique fields (if applicable) for each error
 * 
 * @param {Error} err the caught error object. Either with `details.errors` key/property
 * or just `message`
 * 
 * @returns {Object} `messages`, `statuses`, and `uniqueFields` keys w/ array properties
 */
function handleCaughtEntityFailure({ err }) {
  const messages = []
  const statuses = []
  const uniqueFields = []
  const errors = err.details ? err.details.errors : [err]
  for (const error of errors) {
    switch (error.message) {
      case 'This attribute must be unique': {
        //TODO check each path for the values that are not unique, else it will often say all are not unique (e.g., PTV `replicate` - if 2 of 5 are duplicate, says all 5 are not unique)
        uniqueFields.push(error.path.join('/'))
        statuses.push(400)
        break
      }
      case 'replicate field must have at least 5 items': {
        statuses.push(400)
        break
      }
      default: {
        if(error.message) {
          messages.push(error.message)
        }
        statuses.push(500)
        break
      }
    }
  }

  return {
    messages,
    statuses,
    uniqueFields,
  }
}

/**
 * Creates nicely-formatted messages for entity failure
 * 
 * @param {Object} failure the object returned from `handleCaughtEntityFailure`
 * 
 * @returns {Object} `errMsgs` and `statuses` keys w/ array properties
 */
function createEntityFailureMessage({ failure }) {
  const statuses = []
  const errMsgs = []
  if (failure.messages?.length > 0) {
    errMsgs.push(failure.messages.join(', '))
  }
  if(failure.uniqueFields?.length > 0) {
    errMsgs.push(`Fields are not unique: ${failure.uniqueFields.join(', ')}`)
  }
  if (failure.statuses?.length > 0) {
    statuses.push(...failure.statuses)
  }

  //if `msg` didn't get set from any of the error metadata (which shouldn't
  //really happen but handle anyway), then assume unknown, and that we might
  //need to fix/extend this error handling
  if (errMsgs.length === 0) {
    errMsgs.push('Unknown error')
  }

  return {
    errMsgs, 
    statuses,
  }
}

/**
 * Creates the final error message from a collection of entity failures that were
 * collated from 1..* `createEntityFailureMessage()` calls
 * 
 * @param {Array} finalErrMsgs all the collated error messages
 * @param {Array} allStatuses all the collated statuses
 * 
 * @returns {Error} an error object with the appropriate message and status code, which
 * should be returned from the API call
 */
function createEntityFailureError({ finalErrMsgs, allStatuses }) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const allUniqueStatuses = new Set(allStatuses)
  let code = null
  if (allUniqueStatuses.size > 1) {
    //likely don't need to handle other codes here, as codes like 401, 403 should
    //be handled before getting here
    if (allStatuses.includes(400)) {
      code = 400
    } else {
      code = 500
    }
    helpers.paratooWarnHandler(`Multiple error statuses found (${Array.from(allUniqueStatuses).join(', ')}), but can only keep one in the response. Keeping all messages but only status=${code}`)
  } else {
    code = allStatuses[0]
  }

  return helpers.paratooErrorHandler(
    code,
    new Error(finalErrMsgs.join('. ')),
  )
}

async function checkDuplicateVisit({ plotVisitModel, plotVisit, plotLayoutId }) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

  if (Number.isInteger(plotLayoutId)) {
    helpers.paratooDebugMsg(
      `Checking if Layout with ID=${plotLayoutId} has duplicate visit with start date ${plotVisit.start_date}`,
      true,   //create breadcrumb
    )
    const foundPlotVisit = await strapi.db
      .query(plotVisitModel.uid)
      .findOne({
        where: {
          start_date: plotVisit.start_date,
          plot_layout: plotLayoutId,
        },
      })
    if (foundPlotVisit) {
      //TODO (??) convert this error to a sentry warning and just use the found visit
      let msg = 'Plot visit already exists'
      throw helpers.paratooErrorHandler(400, new Error(msg))
    }
  } else {
    //there will be breadcrumbs with request body, etc.
    helpers.paratooWarnHandler(
      'Unable to determine the Layout that this visit should be linked to'
    )
  }
}
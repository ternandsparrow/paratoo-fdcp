'use strict'
module.exports = {
  createCollectionBody: function (orgMintedIdentifier) {
    if (!orgMintedIdentifier) return null
    
    // for org both /collection and /status endpoints
    const decoded = JSON.parse(Buffer.from(orgMintedIdentifier, 'base64'))
    const survey_metadata = decoded.survey_metadata
    // update core version
    survey_metadata.provenance = {
      version_core: `${process.env.VERSION}-${process.env.GIT_HASH}`,
      system_core: `Monitor FDCP API-${process.env.NODE_ENV}`,
      ...survey_metadata.provenance
    }
    return {
      orgMintedUUID: survey_metadata.orgMintedUUID,
      coreProvenance: {
        system_core: survey_metadata.provenance.system_core,
        version_core: survey_metadata.provenance.version_core,
      },
    }
  },
}

module.exports = {
  paratooDebugMsg(msg, createBreadcrumb = false, category = 'breadcrumb') {
    strapi.log.debug(msg)
    if (createBreadcrumb) {
      strapi.service('api::paratoo-helper.paratoo-helper').createSentryBreadcrumb({
        message: msg,
        category: category,
        level: 'debug',
      })
    }
  },
}
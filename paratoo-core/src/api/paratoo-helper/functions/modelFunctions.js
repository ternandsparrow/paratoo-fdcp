const { cloneDeep, upperFirst, camelCase } = require('lodash')
const { mock } = require('mock-json-schema')
const uuid = require('uuid')
const { setTimeout } = require('timers/promises')
const axios = require('axios')
const { PLOT_MODELS } = require('../../../constants')

const KEEP_SUB_MODULES_RELATION = {
  // "Cover + Fire - Enhanced
  '8c47b1f8-fc58-4510-a138-e5592edd2dbc': '36e9d224-a51f-47ea-9442-865e80144311',
  // "Cover + Fire - standard
  '58f2b4a6-6ce1-4364-9bae-f96fc3f86958': '36e9d224-a51f-47ea-9442-865e80144311',
}

module.exports = {
  /**
   * Naively translates an internal API modelName into it's
   * api target/UID
   *
   * Used to access strapi.getModel(UID), which is equivalent to
   * StrapiV3 strapi.models[modelName]
   *
   * @param {String} modelName e.g. plot-location
   * @returns String attempted API target
   */
  modelToApiName: function (modelName) {
    // TODO ideally find an actual way of retrieving this
    return `api::${modelName}.${modelName}`
  },

  /**
   * From a modelName retrieves the model Attributes of type:relation
   * @param {String} modelName
   * @returns
   */
  modelAssociations: function (modelName, deepSearch=false, accObject={}, isComponent=false, relationTree=null) {
    // gets list of attributes
    const attributes = isComponent 
      ? strapi.components[modelName].attributes
      : strapi.getModel(this.modelToApiName(modelName)).attributes

    for (const [key, value] of Object.entries(attributes)) {
      // if relational field 
      if (value.type === 'relation' && value.target.match('^api::')) {
        let field = null
        if (relationTree && deepSearch) {
          const conditions = Object.keys(relationTree)
          switch (true){
            case conditions.includes('multi_luts'): 
              field = ''
              relationTree.multi_luts.forEach((r)=>{
                field += `${r}_`
              })
              field += `multi_${key}`
              accObject[field] = value
              accObject[field]['relationTree'] = relationTree.multi_luts
              break
            default:
              accObject[key] = value
              break
          }
          
        }
        if (!field) {
          accObject[key] = value
        }
      }

      // adds relations in components recursively
      if (value.type === 'component' && deepSearch) {
        // so that we can find relation tree.
        if (value.component.includes('multi-lut')) {
          if (!relationTree) relationTree = {}
          if (!relationTree.multi_luts) relationTree.multi_luts = []
          relationTree.multi_luts.push(key)
        }
        accObject = this.modelAssociations(value.component, true, cloneDeep(accObject), true, relationTree)
        relationTree = null
      }
    }
    return cloneDeep(accObject)
  },

  /*
   * some of the protocols have special conditions. e.g. 'in_tree' should be greater then 5
   */
  updateMockData: function (keys, data, surveyModel, model, survey_metadata, protocolVariant) {
    const currentDate = new Date(Date.now())
    const dateString = currentDate.toISOString()
    const payload = cloneDeep(data)

    let emptyLists = []
    let emptyObjects = []
    let deletes = ['updatedBy', 'createdBy']

    // special conditions for the models
    switch (model) {
      case 'flora-observation':
        emptyLists = emptyLists.concat([
          'flora_vouchers',
          'additional_plant_information',
        ])
        break
      case 'ecological-community-observation':
        emptyLists = emptyLists.concat([
          'community_details',
          'ecological_vouchers',
        ])
        break
      case 'camera-trap-deployment-point':
        emptyLists = emptyLists.concat(['features'])
        emptyObjects = emptyObjects.concat(['camera_trap_information'])
        break
      case 'soil-pit-characterisation-lite':
        deletes = deletes.concat(['field_survey_id'])
        break
      case 'soil-sub-pit-and-metagenomics-survey':
        deletes = deletes.concat(['field_survey_id'])
        break
      case 'soil-classification':
        deletes = deletes.concat(['asc'])
        break
      case 'soil-sub-pit':
        emptyLists = emptyLists.concat(['sub_pit_observation'])
        deletes = deletes.concat(['soil_sub_pits_survey'])
        break
      case 'soil-horizon-observation':
        deletes = deletes.concat(['pans', 'cutans', 'roots', 'consistence'])
        emptyLists = emptyLists.concat([
          'mottles',
          'coarse_fragments',
          'structure',
          'segregation',
          'voids',
        ])
        break        
    }

    keys.forEach((key) => {
      // special conditions for the keys
      switch (key) {
        case 'survey_metadata':
          if (model == surveyModel) payload['data'][key] = survey_metadata
          break
        case 'in_tree':
          if (model == 'basal-wedge-observation') payload['data'][key] = 6
          break
        case 'year_of_fire':
          payload['data'][key] = '2020'
          break
        case 'protocol_variant':
          if (protocolVariant) payload['data'][key] = protocolVariant
          break
        case 'steps':
          if (model === 'photopoints-survey') {
            payload['data'][key] = [
              {
                id: 214,
                barcode_content:
                  '{"plot_name":"QDASEQ0001","visit_start_date":"2021-08-26T12:26:54.317Z","photopoint_position":1,"lat":-34.936075,"lng":138.617835}',
              },
              {
                id: 215,
                barcode_content:
                  '{"plot_name":"QDASEQ0001","visit_start_date":"2021-08-26T12:26:54.317Z","photopoint_position":2,"lat":-34.93900010687492,"lng":138.622742171467}',
              },
              {
                id: 216,
                barcode_content:
                  '{"plot_name":"QDASEQ0001","visit_start_date":"2021-08-26T12:26:54.317Z","photopoint_position":3,"lat":-34.93900010687492,"lng":138.622742171467}',
              },
            ]
          }
          break
        default:
          if (emptyLists.includes(key)) {
            payload['data'][key] = []
            break
          }
          if (emptyObjects.includes(key)) {
            payload['data'][key] = {}
            break
          }
          if (deletes.includes(key)) {
            delete payload['data'][key]
            break
          }
          if (key.includes('plot')) {
            let field = key.replace('_', '-')
            field = key.replace('-', '-')
            if (PLOT_MODELS.includes(field)) payload['data'][key] = 5
            break
          }
          if (key.includes('start_date') || key.includes('end_date')) {
            payload['data'][key] = dateString
            break
          }
          break
      }
    })
    return cloneDeep(payload)
  },

  /**
   * Generate mock data and push collections.
   * @param {Array} protocols
   * @param {Object} documentation
   * @param {Array} protocolsToPush
   *
   * @returns {Object} result
   */
  pushMockCollections: async function (
    protocols,
    documentation,
    protocolsToPush,
  ) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const result = {}
    const schemas = cloneDeep(documentation['components']['schemas'])
    const pathObjects = cloneDeep(documentation['paths'])
    const paths = Object.keys(pathObjects)

    // Remove whitespace
    const oidcEnable = helpers.validBoolean(
      process.env.ENABLE_CRON_EXPORTER_OIDC,
    )
    const projectID = helpers.validString(process.env.MOCK_PROJECT_ID)

    for (const protocol of protocols) {
      if (!protocolsToPush.includes(protocol['identifier'])) continue
      strapi.log.debug('waiting 5 seconds ... ')
      await setTimeout(2000)
      // only bulk collections
      const endpoint = `${protocol['endpointPrefix']}/bulk`
      if (!paths.includes(endpoint)) continue
      result[protocol['name']] = {}

      const jwt = await helpers.getAuthToken(oidcEnable)
      result[protocol['name']]['generate-token'] = jwt.token ? 'success' : jwt.error
      if (!jwt.token) continue

      const currentDate = new Date(Date.now())

      const collection = {}
      // find all the linked models from protocol workflow
      let workFlow = await helpers.findModelAndChildObsFromProtocolUUID(
        protocol['identifier'],
      )
      let models = Object.keys(workFlow.raw)
      // usually first model is the survey
      const surveyModel = workFlow.surveyModel
      const protocolVariant = workFlow.protocolVariant
      // assign `id: 1` if plot based
      workFlow.plotModels.forEach((m) => {
        collection[m] = { id: 1 }
        models = models.filter((e) => e !== m)
      })
      const surveyMetadata = {
        survey_details: {
          survey_model: surveyModel,
          time: currentDate.toISOString(),
          uuid: uuid.v4(),
          project_id: oidcEnable ? projectID : '1',
          protocol_id: protocol['identifier'],
          protocol_version: '1',
        },
        provenance: {
          version_app: '0.0.1-xxxxx',
          version_core: `${process.env.VERSION}-${process.env.GIT_HASH}`,
          version_core_documentation: '0.0.1-xxxxx',
          version_org: '4.4-SNAPSHOT',
          system_app: 'monitor',
          system_core: `Monitor-dummy-data-${process.env.NODE_ENV}`,
          system_org: oidcEnable ? 'MERIT' : 'ORG',
        }
      }
      let response = await helpers.orgPostRequestWithBody(
        'mint-identifier',
        { survey_metadata: surveyMetadata },
        jwt.token,
        false,
      )
      if (response.status != 200) continue
      const mintIdentifier = response.data.orgMintedIdentifier
      result[protocol['name']]['mint-identifier'] = mintIdentifier ? 'success' : 'failed'
      if (!mintIdentifier) continue
      
      models.forEach((model) => {
        const payload = helpers.generateDummyData(
          model,
          schemas,
          surveyModel,
          surveyMetadata,
          protocolVariant,
        )
        const requestBody = pathObjects[endpoint]
        // if model has child observation
        if (workFlow.raw[model]) {
          const childObsKeys = Object.keys(workFlow.raw[model])
          for (const field of childObsKeys) {
            const childObs = helpers.generateDummyData(
              workFlow.raw[model][field],
              schemas,
            )
            payload.data[field] = Array.isArray(payload.data[field])
              ? [childObs]
              : childObs
          }
        }
        // checks the type of object
        const objectType =
          requestBody.post.requestBody.content['application/json'].schema
            .properties.data.properties.collections.items.properties[model]?.type

        if (objectType == 'object' || objectType == 'array') {
          collection[model] = objectType == 'object' ? payload : [payload]
          return
        }
        collection[model] = payload
      })
      collection['orgMintedIdentifier'] = mintIdentifier
      const data = {
        data: {
          collections: [collection],
        },
        role: 'collection',
      }
      strapi.log.debug('mock data ' + JSON.stringify(data))
      response = await helpers.sendFetchWithOption(
        `http://localhost:1337/api${endpoint}`,
        {
          method: 'POST',
          body: JSON.stringify(data),
          headers: {
            'Content-type': 'application/json',
            Authorization: jwt.token,
          },
        },
      )
      if (!response.data) continue

      result[protocol['name']]['post-collection'] = response.data.status == 200 ? 'success' : JSON.stringify(response.error)
    }

    return result
  },

  // fetch request
  sendFetchWithOption: async function (url, option) {
    const resp = fetch(url, option)
      .then(async (resp) => {
        return {data: resp, error: null}
      })
      .catch((reason) => {
        return {data: null, error: reason}
      })
    return resp
  },

  // get oidc or merit token
  getAuthToken: async function (oidcEnable=false) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    
    const exporterEndpoint = helpers.validString(process.env.ABIS_DATA_EXPORT_URL)
    const oidcTokenEndpoint = `${exporterEndpoint}/data-export/v1/oidc-token`
    const userName = helpers.validString(process.env.MOCK_ORG_USER)
    const password = helpers.validString(process.env.MOCK_ORG_PASSWORD)

    let response = null

    if (oidcEnable) {
      strapi.log.debug('oidc token')
      response = await helpers.sendFetchWithOption(oidcTokenEndpoint, {})
      if (!response.data) {
        const msg = `Failed to generate test oidc token for dummy data. user: ${userName} reason: ${JSON.stringify(response.error)}`
        helpers.paratooWarnHandler(msg)
        return {token: null, error: msg}
      }
      const jwt = await response.data.json()
      return {token: jwt, error: null}
    }

    strapi.log.debug('org token')
    response = await helpers.orgPostRequestWithBody(
      'auth/local',
      { identifier: userName, password: password },
      null,
      false,
      false,
    )
    if (response.status != 200) {
      return {token: null}
    }

    return {token: `Bearer ${response.data.jwt}`, error: null}
  },

  /**
   * Finds the attribute name on model: fromModelName, that refers to the targetUID
   *
   * We can use this to find the attribute with a relation to e.g. mainSurvey reference
   * on observation model
   * @param {String} fromModelName
   * @param {String} targetUID
   * @returns
   */
  findAssociationAttributeName: function (fromModelName, targetUID) {
    for (const [attributeKey, association] of Object.entries(
      this.modelAssociations(fromModelName),
    )) {
      if (association.target === targetUID) {
        return attributeKey
      }
    }
  },

  // generate dummy data using mock package
  generateDummyData: function (
    modelName,
    schemas,
    surveyModel = null,
    surveyMetadata = null,
    protocolVariant = null,
    isArray = false,
    workflow,
  ) {
    if(isArray) {
      const relatedWorkflow = workflow.workflow.find(
        (step) => step.modelName === modelName,
      )
      const minimum = relatedWorkflow?.minimum || 1
      let payload = []
      for(let i = 0; i < minimum; i++) {
        payload.push(
          this.generateDummyData(
            modelName,
            schemas,
            surveyModel,
            surveyMetadata,
            protocolVariant,
            false,
            workflow,
          ),
        )
      }
      return payload
    }

    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const modelRequest = `${upperFirst(camelCase(modelName))}Request`
    let modelSchema = cloneDeep(schemas[modelRequest])
    if (modelSchema.properties.data.default) {
      delete modelSchema.properties.data.default
    }
    // default mock using mock-json-schema
    const payload = mock(modelSchema)
    // look for object that have unique attribute modelSchema
    for (const [key, property] of Object.entries(
      modelSchema.properties.data.properties,
    )) {
      if (property.unique) {
        payload['data'][key] = uuid.v4()
      }
    }

    // if model has child observation
    if (workflow?.raw[modelName]) {
      const childObsKeys = Object.keys(workflow.raw[modelName])
      for (const field of childObsKeys) {
        const childObs = this.generateDummyData(
          workflow.raw[modelName][field],
          schemas,
        )
        payload.data[field] = Array.isArray(payload.data[field])
          ? [childObs]
          : childObs
      }
    }
    if (!surveyModel || !surveyMetadata) return payload

    // some of the protocols have special conditions
    return helpers.updateMockData(
      Object.keys(payload['data']),
      payload,
      surveyModel,
      modelName,
      surveyMetadata,
      protocolVariant,
    )
  },
  
  // deep population
  populateAttribute: function ({ components }) {
    if (components) {
      const populate = components.reduce((currentValue, current) => {
        let data = {
          ...currentValue,
          [current.split('.').pop()]: { populate: true },
        }
        return data
      }, {})
      return { populate }
    }
    return { populate: true }
  },
  deepPopulateFromSchema: function (schema) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    return Object.keys(schema.attributes).reduce((currentValue, current) => {
      const attribute = schema.attributes[current]
      if (!['dynamiczone', 'component', 'relation'].includes(attribute.type)) {
        return currentValue
      }
      return {
        ...currentValue,
        [current]: helpers.populateAttribute(attribute),
      }
    }, {})
  },

  /**
   * Finds all the models assigned to an authenticated user
   *
   * @param {Array} protocols
   * @param {Array} userProjects
   * 
   * @returns {Object} all protocols and models assigned to the user
   */
  getAllUserModels: function (protocols, userProjects) {
    let userProtocols = []
    let userModels = []

    const allProjects = userProjects.map((pp) => {
      const id = pp.id
      const name = pp.name
      const prots = pp.protocols.map((p) => {
        return p.identifier
      })
      return {
        id: id,
        name: name,
        protocols: prots,
      }
    })

    allProjects.forEach((p) => {
      p.protocols.forEach(async (id) => {
        if (!userProtocols.includes(id)) {
          userProtocols.push(id)
          const protocol = protocols.filter(function (o) {
            if (o.identifier == id) {
              return o
            }
          })[0]
          if (protocol?.workflow) {
            let models = []
            models = protocol['workflow'].map((o) => {
              return o.modelName
            })
            protocol['workflow'].forEach((wf)=>{
              const model = wf.modelName
              models.push(model)
              const relationAttrs = wf.newInstanceForRelationOnAttributes 
                ? wf.newInstanceForRelationOnAttributes
                : []
              // handle relation attributes
              relationAttrs.forEach((attr) => {
                const attrTarget =  strapi.api[model]['contentTypes'][model]['attributes'][attr]['target']
                const attrModel = attrTarget?.split('.')[1]
                models.push(attrModel)
              })
            })

            models.forEach((m) => {
              if (!userModels.includes(m)) {
                userModels.push(m)
              }
            })
          }
        }
      })
    })

    
    return {
      protocols: userProtocols,
      models: userModels
    }
  },

  getAllProtocols: async function () {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    // if cache exists
    const hash = helpers.createMd5Hash('protocols')
    let protocols = await helpers.redisGet(hash)
    if (protocols) return protocols

    protocols = await strapi.db
      .query(helpers.modelToApiName('protocol'))
      .findMany()

    helpers.redisSet(hash, protocols, helpers.getTimeToLive('protocol'))
    return protocols
  },

  getAllUserProjects: async function (authToken, modelName=null, useCache = true) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    let userProjects = null
    // for non plot models even if useCache is false we still use the cache
    const hash = helpers.createMd5Hash(`${authToken}user-projects`)
    if(useCache || (!useCache && !PLOT_MODELS.includes(modelName))) {
      let userProjects = await helpers.redisGet(hash, useCache)
      // if cache exists 
      if (userProjects) return cloneDeep(userProjects)
    }

    userProjects = await helpers.orgGetRequest('user-projects', authToken)
    if (userProjects.status !== 200 || !userProjects?.data?.projects?.length) {
      return helpers.paratooErrorHandler(
        userProjects.status,
        userProjects.data,
        'Failed to get user projects',
      )
    }

    if (!userProjects) {
      helpers.paratooWarnHandler('No project found')
      console.warn(JSON.stringify(userProjects))
      return null
    } 
    if (!userProjects.data) {
      helpers.paratooWarnHandler('No project found')
      console.warn(JSON.stringify(userProjects))
      return null
    }
    if (!userProjects.data.projects) {
      helpers.paratooWarnHandler('No project found')
      console.warn(JSON.stringify(userProjects))
      return null
    }

    helpers.redisSet(
      hash,
      cloneDeep(userProjects),
      helpers.getTimeToLive('user_project'),
    )
    return cloneDeep(userProjects)
  },

  /**
   * pulls the list of orgUuids from org_uuid_metadata
   * NOTE: returns all collections if startDate or endDate is null
   *
   * @param {String} startDate to filter data by startDate
   * @param {String} endDate to filter data by endDate
   * 
   * @returns {Array} list of survey metadata
   */
  getOrgUuids: async function(startDate=null, endDate=null) {
    let condition = {}
    const org_uuids = []

    if (startDate && endDate) {
      condition = {
        where: {
          createdAt: {
            $between: [startDate, endDate],
          },
        },
      }
    }
    const records = await strapi.db.query(
      'api::org-uuid-survey-metadata.org-uuid-survey-metadata'
    ).findMany(condition)

    if (records.length==0) return org_uuids
    // extracts org_uuids
    return records.map((e)=> e.org_minted_uuid)
  },

  /**
   * calls an exporter api to check the instance is running or not
   */
  isExporterRunning: async function () {
    const result = {
      status: 200,
      err: null,
    }
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const apiEndpoint = `${process.env.ABIS_DATA_EXPORT_URL}/data-export/apidocs`

    await axios
      .get(apiEndpoint)
      .then((resp) => {
        result.status = resp.status || 200
      })
      .catch((err) => {
        helpers.paratooWarnHandler(
          `Exporter instance is down, core: ${process.env.DNS_PARATOO_CORE}, reseaon: ${err}`,
        )

        result.status = err.response ? err.response.status : 500
        result.err = err
      })

    return result
  },

  /**
   * calls exporter api to export list of collections
   *
   * @param {String} org_uuids list of org_uuids
   * @param {Bool} isForce flag to export again
   *
   * @returns {Object} object with response
   */
  abisExportCollections: async function (org_uuids = [], isForce=false) {
    const result = {
      status: 200,
      data: null,
      err: null,
    }
    if (org_uuids.length == 0) return result
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const isRunning = await helpers.isExporterRunning()
    if (isRunning.status != 200) {
      result.status = 502
      result.err = 'Exporter is down'
      return result
    }

    helpers.paratooDebugMsg('Exporter is running')

    const apiEndpoint = `${process.env.ABIS_DATA_EXPORT_URL}/data-export/v1/export-collections`
    const body = { org_uuids: org_uuids, force: isForce}

    helpers.paratooDebugMsg(
      `Calling exporter api, url: ${apiEndpoint} payload: ${JSON.stringify(
        body,
      )}`,
      true,
    )

    // using axios instead of fetch as fetch cant detect network error properly.
    await axios
      .post(apiEndpoint, body)
      .then((resp) => {
        result.status = resp.status || 200
        result.data = resp.data || resp
      })
      .catch((err) => {
        helpers.paratooWarnHandler(`abis data export failed: ${err}`)
        result.status = err.response ? err.response.status : 500
        result.err = err
      })

    return result
  },

  /**
   * Finds all the models in workflow associated with the protocol uuid
   *
   * @param {String} protocol_uuid
   * @returns {Array} list of model names
   */
  findModelAndChildObsFromProtocolUUID: async function (protocol_uuid) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const hash = helpers.createMd5Hash(`protocol${protocol_uuid}`)
    
    const data = await helpers.redisGet(hash)
    if (data) return data

    const workFlow = {}
    const protocols = await helpers.getAllProtocols()

    const protocol = protocols.filter(function (o) {
      if (o.identifier == protocol_uuid) {
        return o
      }
    })[0]
    let modelsAndRelations = {}
    workFlow['workflow'] = protocol.workflow
    workFlow['subModules'] = []
    workFlow['modelExtension'] = null
    const subModuleRelations = cloneDeep(KEEP_SUB_MODULES_RELATION)
    const subModels = Object.keys(subModuleRelations)
    if (subModels.includes(protocol_uuid)){
      workFlow['modelExtension'] = {}
      workFlow['modelExtension'] = cloneDeep(await helpers.findModelAndChildObsFromProtocolUUID(subModuleRelations[protocol_uuid]))
    }
    for (const w of protocol['workflow']) {
      if (Object.keys(w).includes('protocol-variant')) workFlow['protocolVariant'] = w['protocol-variant']
      
      if (w.isSubmoduleStep) {
        workFlow['subModules'].push(w.modelName)
        continue
      }
      
      if (!w.newInstanceForRelationOnAttributes) {
        modelsAndRelations[w.modelName] = null
        continue
      }
      modelsAndRelations[w.modelName] = {}
      for (let i = 0; i < w.newInstanceForRelationOnAttributes.length; i++) {
        modelsAndRelations[w.modelName][w.newInstanceForRelationOnAttributes[i]] = w.relationOnAttributesModelNames[i]
      }
      
    }

    workFlow['raw'] = modelsAndRelations

    let modelNames = Object.keys(modelsAndRelations)
    let allModels = []
    let plotModels = []

    // delete plot models
    PLOT_MODELS.forEach((p) => {
      if (!modelNames.includes(p)) return
      plotModels.push(p)
      modelNames = modelNames.filter((item) => item !== p)
    })
    workFlow['surveyModel'] = modelNames[0]
    workFlow['modelNames'] = modelNames
    workFlow['plotModels'] = plotModels
    if (plotModels.length > 0 && !plotModels.includes('plot-selection')) {
      plotModels = plotModels.concat(['plot-selection'])
      workFlow['plotModels'] = plotModels
    }

    modelNames.forEach((m) => {
      allModels.push(m)
      if (!modelsAndRelations[m]) return
      for (const f of Object.keys(modelsAndRelations[m])) {
        allModels.push(modelsAndRelations[m][f])
      }
    })
    workFlow['allModels'] = allModels
    workFlow['modelsPaths'] = {}
    allModels.forEach((m) => {
      const pluralForm = strapi.api[m]['contentTypes'][m]['info']['pluralName']
      workFlow['modelsPaths'][m] = pluralForm
    })
    helpers.redisSet(hash, workFlow, helpers.getTimeToLive('protocol'))
    return workFlow
  },
  
  /**
   * Finds all the models in workflow associated with the protocol uuid
   *
   * @param {String} protocol_uuid
   * @returns {Array} list of model names
   */
  findWorkflowUsingProtocolUUID: async function (protocol_uuid) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    
    const protocols = await helpers.getAllProtocols()
    const protocol = protocols.filter(function (o) {
      if (o.identifier == protocol_uuid) {
        return o
      }
    })[0]
    let work_flow = []
    work_flow = protocol['workflow'].map((o) => {
      return o.modelName
    })
    return work_flow
  },
  /**
   * retrieve all the collections associated with the org_minted_uuid
   *
   * @param {String} org_minted_uuid org minted uuid
   * @param {Object} survey_metadata survey metadata
   * @returns {Object} all the collections
   */
  getSurveyMetadataCollections: async function (orgMintedUuid=null, surveyMetadata=null) {
    let survey_metadata = surveyMetadata
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

    if (!survey_metadata) {
      survey_metadata = await helpers.surveyMetadataFromOrgUuid(orgMintedUuid)
    }
    if (!survey_metadata) return 'No record found'

    const hash = helpers.createMd5Hash(`orgMintedUuid-${orgMintedUuid}`)
    let lookUpCollection = await helpers.redisGet(hash)
    if (lookUpCollection) return lookUpCollection

    const survey_model = survey_metadata.survey_details.survey_model
    const protocol_id = survey_metadata.survey_details.protocol_id
    const submodule_protocol_id =
      survey_metadata.survey_details.submodule_protocol_id
    const surveyData = await helpers.deepPopulateQuery(
      survey_model,
      {
        survey_metadata: {
          orgMintedUUID: survey_metadata.org_minted_uuid,
        },
      },
      {
        survey_metadata: {
          populate: {
            survey_details: true,
            provenance: true,
          },
        },
      },
      false,
    )
    // workflow to extract all the model associated with survey and protocol
    // we're doing a 'combined' submodule like Cover+Fire and are trying to grab the
    // Fire data (it's collection ID and survey ID will have link to Cover+Fire protocol
    // but the actual data is stored in a placeholder workflow)
    const protocolToGrabWorkflowForId = submodule_protocol_id
      ? submodule_protocol_id
      : protocol_id

    const work_flow = await helpers.findWorkflowUsingProtocolUUID(
      protocolToGrabWorkflowForId,
    )
    const collections = {}
    collections[survey_model] = surveyData

    for (const model of work_flow) {
      if (model == survey_model) continue

      const survey_field = helpers.findAssociationAttributeName(
        model,
        helpers.modelToApiName(survey_model),
      )
      if (!survey_field) continue
      collections[model] = await helpers.deepPopulateQuery(
        model,
        {
          [survey_field]: surveyData.id,
        },
        null,
        true,
      )
    }

    lookUpCollection = {
      survey_metadata: survey_metadata,
      collections: collections,
    }
    // time to live can be same as protocol.
    helpers.redisSet(hash, cloneDeep(lookUpCollection), helpers.getTimeToLive('protocol'))
    return lookUpCollection
  },
  /**
   * Finds all the collection ids associated with the protocol id or project id
   *
   * @param {String} project_id
   * @param {String} protocol_id
   * @param {String} user_id
   * @returns {Array} list of collection ids
   */
  findCollectionIdsGivenProjectIdOrProtocolId: async function (ctx, project_id=null, protocol_id=null) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const authToken = ctx.request.headers['authorization']

    const resp = await helpers.orgGetRequest(
      'get-all-collections',
      authToken,
    )
    if (resp.status != 200) {
      return null
    }
    let project_id_surveys = []
    let protocol_id_surveys = []
    const orgMintedUuids = resp.data.collections ? resp.data.collections : []
    
    for (const orgMintedUuid of orgMintedUuids) {
      const survey_metadata = await helpers.surveyMetadataFromOrgUuid(orgMintedUuid)
      if (!survey_metadata) continue

      const projectId = survey_metadata.survey_details.project_id
      const protocolId = survey_metadata.survey_details.protocol_id

      if (project_id == projectId) {
        project_id_surveys.push(survey_metadata)
      }

      if (protocol_id == protocolId) {
        protocol_id_surveys.push(survey_metadata)
      }

    }

    if (project_id) return project_id_surveys
    if (protocol_id) return protocol_id_surveys

    return null
  },
  // fetch survey metadata using org minted uuid
  surveyMetadataFromOrgUuid: async function (org_minted_uuid) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    return await helpers.deepPopulateQuery(
      'org-uuid-survey-metadata',
      {
        org_minted_uuid: org_minted_uuid,
      },
      {
        survey_details: true,
        provenance: true,
      },
      false,
    )
  },

  // find parent model linked to child observation
  findParentModelFromChildModel: function (childModel, fullWorkflow) {
    // if its not a child observation
    if (fullWorkflow.modelNames.includes(childModel)) return null
    // if model is not linked to the protocol
    if (!fullWorkflow.allModels.includes(childModel)) return null

    const models = Object.keys(fullWorkflow.raw)
    for (const m of models) {
      // model doesn't have any child observation
      if (!fullWorkflow.raw[m]) continue

      // if models has child obs we return matched model and fieldName
      const fields = Object.keys(fullWorkflow.raw[m])
      for (const f of fields) {
        if (fullWorkflow.raw[m][f] != childModel) continue
        return {
          name: m,
          associatedField: f,
        }
        
      }
    }
    return null
  },

  // query with deep population
  deepPopulateQuery: async function (
    model,
    where,
    populate = null,
    isMultiple = false,
  ) {
    const api = `api::${model}.${model}`
    let object = null
    if (!isMultiple) {
      // find object
      object = await strapi.db.query(api).findOne({
        populate: populate ? populate : true,
        where: where,
      })
      if (!object) return null

      // deep populate
      object = await strapi.entityService.findOne(api, object.id, {
        populate: 'deep',
      })
      return object
    }

    // find objects
    const objects = await strapi.db.query(api).findMany({
      populate: populate ? populate : true,
      where: where,
    })
    // deep populate
    let deepObjects = []
    for (const o of objects) {
      deepObjects.push(
        await strapi.entityService.findOne(api, o.id, {
          populate: 'deep',
        }),
      )
    }
    return deepObjects
  },
  
  isPlotBasedProtocol: async function({ protocolUuid }) {
    const protocolEntry = await strapi.db.query('api::protocol.protocol').findOne({
      where: {
        identifier: protocolUuid,
      },
    })

    let hasLayout = false
    let hasVisit = false
    for (const workflowItem of protocolEntry.workflow) {
      if (workflowItem.modelName === 'plot-layout') {
        hasLayout = true
      }
      if (workflowItem.modelName === 'plot-visit') {
        hasVisit = true
      }
    }

    //this will also include when the protocol is Plot Layout and Visit
    //(plot-definition-survey model)
    return hasLayout && hasVisit
  },

  /**
   * Parses the project_id/s or prot_uuid/s params from the query parameters
   * 
   * @param {Object} queryParams the query params - i.e., `ctx.request.query`
   * @param {String} paramKeySingular the singular form of the param key
   *  
   * @returns {Array} an array of project_id/s or prot_uuid/s
   */
  parseGetReqUrlParam: function({ queryParams, paramKeySingular }) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

    const paramKeyPlural = `${paramKeySingular}s`
    const validParams = [
      paramKeySingular,
      paramKeyPlural,
    ]
    const passedParams = Object.keys(queryParams)
    if (!passedParams.some(p => validParams.includes(p))) {
      helpers.paratooDebugMsg(
        `No relevant param to parse. Provided: ${passedParams.join(', ') || 'none'}`,
        true,
      )
      return []
    }

    let paramKey = null
    let resp = null
    if (queryParams?.[paramKeySingular]) {
      resp = [queryParams[paramKeySingular]]
      paramKey = paramKeySingular
    }
    //this is not an `else-if` as if we pass both singular and plural, we ignore the
    //singular for the plural
    if (queryParams?.[paramKeyPlural]) {
      paramKey = paramKeyPlural
      if (Array.isArray(queryParams[paramKeyPlural])) {
        //passed param as <param>[0]=x, <param>[1]=y, etc.
        resp = queryParams[paramKeyPlural]
      } else if (typeof queryParams[paramKeyPlural] === 'string') {
        //passed param as <param>=x,y, etc.
        const stringArray = queryParams[paramKeyPlural]?.split(',')
        if (
          stringArray?.every(
            item => typeof item === 'string'
          )
        ) {
          resp = stringArray
        }
      }
    }
    if (resp === null) {
      helpers.paratooWarnHandler(`Could not parse the provided value '${queryParams?.[paramKey]}'. Defaulting to empty array.`)
    }
    return resp || []
  },
}
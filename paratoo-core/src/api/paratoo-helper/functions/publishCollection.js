/**
 * Programmatically publish a collection without going to the admin panel
 * @param {*} collectionName
 * @param {*} recordId
 * @param {*} dataToUpdateWhenPublished data we want to update when the collection is published
 * eg. { title: "new title" }
 * @returns void
 */

const publishCollection = async (
  collectionName,
  recordId,
  dataToUpdateWhenPublished = {},
) => {
  const { paratooErrorHandler } = strapi.service(
    'api::paratoo-helper.paratoo-helper',
  )
  const modelUid = `api::${collectionName}.${collectionName}`

  try {
    strapi.log.info(`Publishing notification ID: ${recordId}`)
    await strapi.entityService.update(modelUid, recordId, {
      data: { publishedAt: new Date(), ...dataToUpdateWhenPublished },
    })
    strapi.log.info(`Published notification ID: ${recordId}`)
  } catch (err) {
    paratooErrorHandler(
      500,
      err,
      `Failed to publish notification ID: ${recordId}`,
    )
  }
}
module.exports = publishCollection

const uuid = require('uuid')
//TODO handle errors in here and in caller

module.exports = {
  //
  /**
   * Filters the survey data based on the project's plots
   * 
   * Can't just use the project in the survey_metadata, as a given survey might have been
   * collected in a different project, but the plot it was collected under is in one of
   * the projects we care about
   * 
   * @param {Object} ctx Koa context
   * @param {Array.<Object>} data the data returned from the default `find` action
   * @param {Array.<Object>} relevantPlots an array of objects from the `plot_selection`
   * attribute in the project from org
   * @param {String} model the survey model
   * 
   * @returns {Array.<Object>} the filtered observation data structured like the output
   * of the default `find` action
   */
  filterSurveys: async function (ctx, data, relevantPlots, model) {
    let populatedSurveyData = null
    let dataIsAlreadyPopulated = false
    if (
      data.some(
        surveyData =>
          !surveyData?.attributes?.plot_visit ||
          !surveyData?.attributes?.plot_visit?.data?.attributes?.plot_layout ||
          !surveyData?.attributes?.plot_visit?.data?.attributes?.plot_layout?.data?.attributes?.plot_selection
      )
    ) {
      //grab deeply-populated for filtering purposes (will still return the `data`,
      //but just filtered, so that any `populate` params are maintained)
      populatedSurveyData = await strapi.entityService.findMany(`api::${model}.${model}`, {
        populate: 'deep',
      })
      strapi.log.debug(`Filtering surveys for model '${model}' but the queried data doesn't have the Plot Selection populated, so re-querying to get deeply-populated data`)
    } else if (
      data.every(
        surveyData => surveyData?.attributes?.plot_visit?.data?.attributes?.plot_layout?.data?.attributes?.plot_selection?.data?.attributes?.uuid
      )
    ) {
      dataIsAlreadyPopulated = true
      strapi.log.debug(`Filtering surveys for model '${model}' and every queried data has populated relation to Plot Selection, so no need to re-query`)
    }

    const filteredSurveyData = []
    for (const surveyData of data) {
      let currSurveyDataPopulated = null
      if (dataIsAlreadyPopulated) {
        //don't bother trying to find the populated ob data if it's already populated
        currSurveyDataPopulated = surveyData
      } else {
        currSurveyDataPopulated = populatedSurveyData?.find(
          o => o.id === surveyData.id
        )
      }

      if (
        relevantPlots.some(
          p =>
            //used entity service to populate
            p.uuid === currSurveyDataPopulated?.plot_visit?.plot_layout?.plot_selection?.uuid ||
            //API `populate` param
            p.uuid === currSurveyDataPopulated?.attributes?.plot_visit?.data?.attributes?.plot_layout?.data?.attributes?.plot_selection?.data?.attributes?.uuid
        )
      ) {
        filteredSurveyData.push(surveyData)
      }
    }

    return filteredSurveyData
  },
  /**
   * Filters the observation data based on the project's plots
   * 
   * @param {Object} ctx Koa context
   * @param {Array.<Object>} data the data returned from the default `find` action
   * @param {Array.<Object>} relevantPlots an array of objects from the `plot_selection`
   * attribute in the project from org
   * @param {String} model the observation model
   * @param {String} surveyFieldName the field name of the relation to the survey
   * 
   * @returns {Array.<Object>} the filtered observation data structured like the output
   * of the default `find` action
   */
  filterObservations: async function (ctx, data, relevantPlots, model, surveyFieldName) {
    let populatedObData = null
    let dataIsAlreadyPopulated = false
    if (
      data.some(
        obData =>
          !obData?.attributes?.[surveyFieldName]?.data?.attributes?.plot_visit ||
          !obData?.attributes?.[surveyFieldName]?.data?.attributes?.plot_visit?.data?.attributes?.plot_layout ||
          !obData?.attributes?.[surveyFieldName]?.data?.attributes?.plot_visit?.data?.attributes?.plot_layout?.data?.attributes?.plot_selection
      )
    ) {
      //grab deeply-populated for filtering purposes (will still return the `data`,
      //but just filtered, so that any `populate` params are maintained)
      populatedObData = await strapi.entityService.findMany(`api::${model}.${model}`, {
        populate: 'deep',
      })
      strapi.log.debug(`Filtering observations for model '${model}' but the queried data doesn't have the Plot Selection populated, so re-querying to get deeply-populated data`)
    } else if (
      data.every(
        obData => obData?.attributes?.[surveyFieldName]?.data?.attributes?.plot_visit?.data?.attributes?.plot_layout?.data?.attributes?.plot_selection?.data?.attributes?.uuid
      )
    ) {
      dataIsAlreadyPopulated = true
      strapi.log.debug(`Filtering observations for model '${model}' and every queried data has populated relation to Plot Selection, so no need to re-query`)
    }
    
    const filteredObData = []
    for (const obData of data) {
      let currObDataPopulated = null
      if (dataIsAlreadyPopulated) {
        //don't bother trying to find the populated ob data if it's already populated
        currObDataPopulated = obData
      } else {
        currObDataPopulated = populatedObData?.find(
          o => o.id === obData.id
        )
      }
      
      if (
        relevantPlots.some(
          p =>
            //used entity service to populate
            p.uuid === currObDataPopulated?.[surveyFieldName]?.plot_visit?.plot_layout?.plot_selection?.uuid ||
            //API `populate` param
            p.uuid === currObDataPopulated?.attributes?.[surveyFieldName]?.data?.attributes?.plot_visit?.data?.attributes?.plot_layout?.data?.attributes?.plot_selection?.data?.attributes?.uuid
        )
      ) {
        filteredObData.push(obData)
      }
    }

    return filteredObData
  },
  /**
   * Gets the specified projects from the request url parameters and the user's projects
   * by making a request to org's /user-projects
   * 
   * @param {Object} ctx Koa context
   * @param {Array.<String>} requestParams an array of request url parameters,
   * where each string is format like `key=value`
   * 
   * @returns {Object} an object with `specifiedProjects` (array of ints) and
   * `userProjects` (array of objects)
   */
  parseSpecifiedProjects: async function(ctx, requestParams) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

    const userProjects = await helpers.getAllUserProjects(ctx.request.headers['authorization'])
    if (!userProjects) {
      helpers.paratooErrorHandler(500, new Error('Failed to get user projects'))
    }

    const specifiedProjects = []
    for (const param of requestParams) {
      console.log('param:', param)
      if (param.startsWith('project_ids')) {
        console.log(param.split('=')[1])
        console.log(param.split('=')[1].split(','))
        for (const projId of param.split('=')[1].split(',')) {
          if (uuid.validate(projId)) {
            //projects in MERIT are UUIDs
            specifiedProjects.push(projId)
          } else {
            //projects in strapi are numbers, so cast str to int
            specifiedProjects.push(parseInt(projId))
          }
        }
      }
    }

    return {
      specifiedProjects: specifiedProjects,
      userProjects: userProjects.data.projects,
    }
  },
  /**
   * Filters the provided `data` if the `project_ids` URL param is provided.
   * 
   * Should generically work for any plot-based protocol, but only tested for floristics
   * 
   * @param {Object} ctx Koa context
   * @param {Array.<String> | null} requestParams an array of request url parameters,
   * where each string is format like `key=value`. Can be null or undefined when no url
   * parameters are provided in the request
   * @param {Array.<Object>} data the data returned from the default `find` action
   * @param {Object} meta the meta object returned from the default `find` action
   * @param {Object} modelMeta some metadata about the request model for determining
   * which functions to call (based on whether it's a survey or observation)
   * @param {String} modelMeta.model the name of the model
   * @param {String} modelMeta.modelType the type of model - 'survey' or 'observation'
   * @param {String} [modelMeta.surveyModelName] the name of the survey model if the
   * modelMeta.modelType is 'observation'
   * 
   * @returns {Array.<Object>} typical Strapi response for the survey, with `data` items
   * optionally being filtered by the project's plots
   */
  handleProjectPlotFilteringIfNecessary: async function(
    ctx,
    requestParams,
    data,
    meta,
    modelMeta,
  ) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

    if (requestParams?.some(o => o.startsWith('project_ids'))) {
      const { specifiedProjects, userProjects } = await helpers.parseSpecifiedProjects(ctx, requestParams)

      //will be array of objects, where each object has `name` and `uuid`
      const relevantPlots = []
      for (const userProj of userProjects) {
        for (const proj of specifiedProjects) {
          //use weak equality because we can be passed arr of strs (uuids) or ints
          if (proj == userProj.id) {
            // relevantPlots.push(...userProj.plot_selections)
            for (const plotSelection of userProj.plot_selections) {
              //don't push duplicates
              if (!relevantPlots.some(p => p.uuid === plotSelection.uuid)) {
                relevantPlots.push(plotSelection)
              }
            }
          }
        }
      }

      if (modelMeta.modelType === 'survey') {
        data = await helpers.filterSurveys(ctx, data, relevantPlots, modelMeta.model)
      } else if (modelMeta.modelType === 'observation') {
        const associations = helpers.modelAssociations(modelMeta.model)
        const surveyFieldName = Object.keys(associations).find(
          o => associations[o].target === helpers.modelToApiName(modelMeta.surveyModelName)
        )
        data = await helpers.filterObservations(ctx, data, relevantPlots, modelMeta.model, surveyFieldName)
      }

      //modify pagination data since we filtered
      meta.pagination.total = data.length
      meta.pagination.pageCount = Math.ceil(meta.pagination.total / meta.pagination.pageSize)
    }
    return { data, meta }
  },
}
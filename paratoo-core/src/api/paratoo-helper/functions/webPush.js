const webPush = require('web-push')

// VAPID keys should only be generated once.
const vapidPrivateKey = process.env.CORE_PUSH_VAPI_PRIVATE_KEY
const vapidPublicKey = process.env.CORE_PUSH_VAPI_PUBLIC_KEY
if (vapidPrivateKey && vapidPublicKey) {
  webPush.setVapidDetails(
    `mailto:${process.env.ADMIN_EMAIL}`,
    vapidPublicKey,
    vapidPrivateKey,
  )
}

module.exports = async (data) => {
  const { paratooErrorHandler } = strapi.service(
    'api::paratoo-helper.paratoo-helper',
  )

  const updateStatusOfMessage = async (id) => {
    try {
      await strapi.entityService.update(
        'api::push-notification.push-notification',
        id,
        {
          data: {
            isSent: true,
          },
        },
      )
    } catch (error) {
      paratooErrorHandler(
        500,
        error,
        `Failed to update status of message with id ${data.id}. The message was sent successfully, but the status was not updated`,
      )
    }
  }

  const removeOutdatedSubscriptions = async (id) => {
    try {
      await strapi.entityService.delete(
        'api::push-notification-subscription.push-notification-subscription',
        id,
      )
    } catch (error) {
      paratooErrorHandler(
        500,
        error,
        `Failed to remove outdated subscription with id ${id}, we might want to remove it manually`,
      )
    }
  }

  const subscriptions = await strapi.entityService.findMany(
    'api::push-notification-subscription.push-notification-subscription',
  )

  // send push notification to all users
  strapi.log.debug(`Sending push notification to ${subscriptions.length} users`)
  const processStart = Date.now()

  // send a bunch of push notifications at once, but sending all at once might cause memory issues
  const batchSize = 50
  for (let i = 0; i < subscriptions.length; i += batchSize) {
    const batch = subscriptions.slice(i, i + batchSize)
    const promises = batch.map(async (user) => {
      try {
        await webPush.sendNotification(user.subscription, JSON.stringify(data))
      } catch (error) {
        const statusCode = error.statusCode || 500
        const message = error.body || 'Unknown error'
        if (statusCode === 410 || message.includes('unsubscribed or expired')) {
          await removeOutdatedSubscriptions(user.id)
        } else {
          paratooErrorHandler(
            statusCode,
            error,
            `Failed to send push notification for subscription with id ${user.id}`,
          )
        }
      }
    })
    await Promise.all(promises)
  }

  await updateStatusOfMessage(data.id)
  strapi.log.debug(
    `Took ${Date.now() - processStart}ms to send push notification to ${
      subscriptions.length
    } users`,
  )
  strapi.log.info(
    `Push notification with id ${data.id} sent to ${subscriptions.length} users`,
  )
}

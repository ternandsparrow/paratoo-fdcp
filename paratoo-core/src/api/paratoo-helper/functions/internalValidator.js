'use strict'
const { cloneDeep } = require('lodash')
const { customRules } = require('../../../customRules')
const isNil = require('lodash/isNil')

module.exports = {
  /**
   * Validates internal custom logics
   *
   * @param {Object} ctx koa object
   * @returns {Boolean/String} returns true if no error found
   */
  internalValidator: function (ctx) {
    if (ctx.request.method !== 'POST') return true

    const body = ctx.request.body
    if (!body) return false
    if (!customRules) return true

    const data = body.data
    const collections = data.collections ? data.collections : [data]
    let result = null

    for (const collection of collections) {
      const modelNames = Object.keys(collection)
      for (const modelName of modelNames) {
        // if no linked rules found
        if (!customRules[modelName]) continue

        const payload = collection[modelName]
        // returns error message if any rule fails
        for (const assignedRule of customRules[modelName]) {
          result = this.applyCustomRules(assignedRule, payload, modelName)
          if (result !== true) {
            return {
              errorsText: result.toString(),
            }
          }
        }
      }
    }

    return true
  },
  /**
   * Apply rules based on rule type
   *
   * @param {Object} assignedRule
   * @returns {Object} payload
   */
  applyCustomRules: function (assignedRule, payload, modelName) {
    let result = null
    let outputs = null
    const data = Array.isArray(payload) ? payload : [payload]
    switch (assignedRule.ruleType) {
      // validates the total counts of multiple fields together
      case 'multipleValuesMinimumCheck':
        for (const value of data) {
          outputs = 0
          assignedRule.dependentKeys.forEach((dependentKey) => {
            outputs += value.data[dependentKey] ? value.data[dependentKey] : 0
          })
          // if no species to record
          if (value.data.no_species_to_record) continue

          result = assignedRule.rule(outputs)
          // if fails to validate
          if (result !== true) {
            return result
          }
        }
        break
      // validates the difference between two fields cannot be greater than a certain value
      // e.g. upper depth and lower depth of soil pit samples cannot be larger than 30cm
      case 'maximumDifferenceCheck':
        if (assignedRule.dependentKeys.length !== 2) {
          console.log(
            'Required exactly two dependent keys to apply maximumDifferenceCheck rule',
          )
          break
        }
        for (const value of data) {
          const field1Val = value.data[assignedRule.dependentKeys[0]]
          const field2Val = value.data[assignedRule.dependentKeys[1]]
          const diff = Math.abs(field1Val - field2Val)
          result = assignedRule.rule(diff)
          // if fails to validate
          if (result !== true) {
            return result
          }
        }
        break
      // NOTE: latest strapi fixed that already by using regex with integer
      case 'OneDecimalPlaceCheck':
        for (const value of data) {
          outputs = []
          assignedRule.dependentKeys.forEach((dependentKey) => {
            const output = this.getValueFromData(value.data, dependentKey)
            if (output.fieldExist) {
              if (!Array.isArray(output.value)) {
                outputs.push({ value: output.value, field: dependentKey })
                return
              }
              output.value.forEach((v) => {
                outputs.push({ value: v, field: dependentKey })
              })
            }
          })
          if (outputs.length == 0) return true
          for (const output of outputs) {
            result = assignedRule.rule(output.value, output.field)
            // if fails to validate
            if (result !== true) {
              return result
            }
          }
        }
        break
      case 'minNumOfReplicates':
        for (const value of data) {
          result = assignedRule.rule({
            replicateBarcodeLength: value.data.replicate?.length,
            hasReplicate: value.data.has_replicate,
          })
          if (result !== true) {
            return result
          }
        }
        break
      case 'distanceBetweenReplicates':
        for (const value of data) {
          if (value.data.has_replicate) {
            result = assignedRule.rule(
              value.data.min_distance_between_replicates,
            )
            if (result !== true) {
              return result
            }
          }
        }
        break
      // conditional required fields
      case 'ConditionalFieldsCheck': {
        const stepName = assignedRule.stepName || modelName
        for (const condition of assignedRule.conditions) {
          if (!condition.dependOn) continue
          if (!condition.requiredFields) continue

          for (const value of data) {
            // finds dependOn.field into data
            let output = this.getValueFromData(
              value.data,
              condition.dependOn.field,
            )
            let dependOnValues = condition.dependOn.value
            if (!Array.isArray(condition.dependOn.value)) {
              dependOnValues = [dependOnValues]
            }

            if (!output.fieldExist) continue
            if (
              (!dependOnValues.includes('$defined') &&
                !dependOnValues.includes(output.value)) ||
              (dependOnValues.includes('$defined') && isNil(output.value))
            )
              continue

            // check all required fields
            for (const field of condition.requiredFields) {
              output = this.getValueFromData(value.data, field)
              if (output.fieldExist && output.value) continue
              return `${stepName}/${field} is required`
            }
          }
        }
        break
      }
      default:
        // extend new custom types here..
        break
    }
    return true
  },

  // get values from object
  getValueFromData: function (data, field, replace = null, remove = false) {
    const result = {
      data: null,
      fieldExist: false,
      value: null,
      changedValue: null,
    }
    if (!data) return result
    const keys = Object.keys(data)
    const payload = cloneDeep(data)
    // if field exists
    if (keys.includes(field)) {
      result.fieldExist = true
      result.value = payload[field]

      // if needs to replace or delete
      if (replace) {
        payload[field] = replace
        result.changedValue = replace
      }
      if (remove) delete payload[field]
      result.data = cloneDeep(payload)
      return cloneDeep(result)
    }

    // checks all the objects recursively
    for (const key of keys) {
      if (typeof data[key] != 'object') continue
      // if not an array
      if (!Array.isArray(data[key])) {
        const objectValue = this.getValueFromData(
          data[key],
          field,
          replace,
          remove,
        )
        if (!objectValue.data) continue

        // if found
        payload[key] = objectValue.data
        result.fieldExist = objectValue.fieldExist
        result.value = objectValue.value
        result.changedValue = objectValue.changedValue
        result.data = cloneDeep(payload)
        return cloneDeep(result)
      }

      // if an array
      let values = []
      let changedValues = []
      for (const [index, obj] of data[key].entries()) {
        const objectValue = this.getValueFromData(obj, field, replace, remove)
        if (!objectValue.data) continue
        result.fieldExist = objectValue.fieldExist
        values.push(objectValue.value)
        payload[key][index] = objectValue.data
        if (objectValue.changedValue)
          changedValues.push(objectValue.changedValue)
      }

      if (!result.fieldExist) continue

      // if field found
      result.data = cloneDeep(payload)
      result.value = values
      result.changedValue = changedValues
      return cloneDeep(result)
    }
    return result
  },

  // checks empty or null field
  isNullOrEmpty: function (records, field) {
    if (!records || !field) {
      return true
    }
    const recordKeys = Object.keys(records)
    if (!recordKeys.includes(field)) {
      // Added a check in case field is passed in as a string,
      // TODO: maybe convert the string into an int above
      // for now this is fine
      if (recordKeys.includes(field.toString())) {
        return false
      }
      return true
    }
    if (!records[field]) {
      return true
    }

    if (records[field].length == 0) {
      return true
    }

    return false
  },
}

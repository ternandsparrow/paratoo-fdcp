'use strict'
const { cloneDeep } = require('lodash')
module.exports = {
  // checks api tokens and types
  apiTokenCheck: async function (authToken) {
    // all existing tokens
    const apiTokens = await strapi
      .query('admin::api-token')
      .findMany({
        populate: true,
      })
      .catch((err) => {
        strapi.log.error(err)
      })

    // if no token found
    if (!apiTokens) return null
    if (apiTokens.length == 0) return null

    for (const apiToken of apiTokens) {
      if (!apiToken.accessKey) continue
      if (apiToken.type != 'full-access') continue
      const token = `Bearer ${apiToken.accessKey}`
      if (token != authToken) continue

      // returns if token has full access
      strapi.log.debug(
        'Auth token is an api token with full access ' +
          JSON.stringify(apiToken),
      )
      return cloneDeep(apiToken)
    }
    return null
  },
}

'use strict'

/**
 * lut-plot-corner-and-centre service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-plot-corner-and-centre.lut-plot-corner-and-centre')

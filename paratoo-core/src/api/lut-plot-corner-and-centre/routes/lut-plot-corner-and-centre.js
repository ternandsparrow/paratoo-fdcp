module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-plot-corner-and-centres',
      'handler': 'lut-plot-corner-and-centre.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-plot-corner-and-centres/:id',
      'handler': 'lut-plot-corner-and-centre.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
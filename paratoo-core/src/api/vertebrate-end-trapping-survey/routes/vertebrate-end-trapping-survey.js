module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/vertebrate-end-trapping-surveys',
      'handler': 'vertebrate-end-trapping-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/vertebrate-end-trapping-surveys/:id',
      'handler': 'vertebrate-end-trapping-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/vertebrate-end-trapping-surveys',
      'handler': 'vertebrate-end-trapping-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/vertebrate-end-trapping-surveys/:id',
      'handler': 'vertebrate-end-trapping-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/vertebrate-end-trapping-surveys/:id',
      'handler': 'vertebrate-end-trapping-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/vertebrate-end-trapping-surveys/bulk',
      'handler': 'vertebrate-end-trapping-survey.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
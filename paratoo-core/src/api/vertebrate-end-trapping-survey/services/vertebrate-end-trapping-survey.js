'use strict'

/**
 * vertebrate-end-trapping-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::vertebrate-end-trapping-survey.vertebrate-end-trapping-survey')

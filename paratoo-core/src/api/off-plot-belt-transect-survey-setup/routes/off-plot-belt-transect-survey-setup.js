module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/off-plot-belt-transect-survey-setups',
      'handler': 'off-plot-belt-transect-survey-setup.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/off-plot-belt-transect-survey-setups/:id',
      'handler': 'off-plot-belt-transect-survey-setup.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/off-plot-belt-transect-survey-setups',
      'handler': 'off-plot-belt-transect-survey-setup.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/off-plot-belt-transect-survey-setups/:id',
      'handler': 'off-plot-belt-transect-survey-setup.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/off-plot-belt-transect-survey-setups/:id',
      'handler': 'off-plot-belt-transect-survey-setup.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
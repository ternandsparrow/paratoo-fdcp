'use strict'

/**
 * off-plot-belt-transect-survey-setup service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::off-plot-belt-transect-survey-setup.off-plot-belt-transect-survey-setup')

{
  "kind": "collectionType",
  "collectionName": "lut_soils_asc_orders",
  "info": {
    "singularName": "lut-soils-asc-order",
    "pluralName": "lut-soils-asc-orders",
    "displayName": "Lut-soils-asc-order",
    "name": "lut-soils-asc-order",
    "uri": "https://linked.data.gov.au/def/nrm/cd377ef2-2174-4c7b-a6ef-7e0b1f83b85a"
  },
  "options": {
    "increments": true,
    "timestamps": true,
    "draftAndPublish": false
  },
  "attributes": {
    "symbol": {
      "type": "string",
      "required": true,
      "unique": true
    },
    "label": {
      "type": "string",
      "required": true,
      "unique": true
    },
    "description": {
      "type": "text"
    },
    "uri": {
      "type": "string"
    }
  },
  "initialData": [
    [
      "AN",
      "Anthroposols",
      "Refers to the soil orders based on the Australian Soil Classification. Anthroposols are soils resulting from human activities which have led to a profound modification, truncation or burial of the original soil horizons, or the creation of new soil parent materials by a variety of mechanical means. (Reference: The Australian Soil Classification, Third Edition)",
      "https://linked.data.gov.au/def/nrm/4e81f233-436c-5125-a357-d4b49bff333e"
    ],
    [
      "RE",
      "Arenosols",
      "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Arenosols- are of \"deep sandy soils\", with predominantly sandy field textures (10% clay content), and with no horizons containing more than 15% clay within the upper 1.0 m of the profile. The presence of argic horizons are the only exception.(Reference: The Australian Soil Classification, Third Edition).",
      "https://linked.data.gov.au/def/nrm/280331e0-341c-512b-899b-f487bf7eef5f"
    ],
    [
      "CA",
      "Calcarosols",
      "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Calcarosols- are usually calcareous throughout the profile, often highly so. They constitute a widespread and important group of soils in southern Australia. (Reference: The Australian Soil Classification, Third Edition).",
      "https://linked.data.gov.au/def/nrm/5da640ad-ccae-5645-89a5-a8a23b902ebc"
    ],
    [
      "CH",
      "Chromosols",
      "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Chromosols- are soils with a clear or abrupt textural B horizon and in which the major part of the upper 0.2 m of the B2t horizon (or the major part of the entire B2t horizon if it is less than 0.2 m thick) is not strongly acid. (Reference: The Australian Soil Classification, Third Edition).",
      "https://linked.data.gov.au/def/nrm/c095ca69-2abf-5d73-b5ca-495dfb50fd9b"
    ],
    [
      "DE",
      "Dermosols",
      "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Dermosols- are soils with B2 horizons that have grade of pedality greater than weak throughout the major part of the horizon. (Reference: The Australian Soil Classification, Third Edition).",
      "https://linked.data.gov.au/def/nrm/09a09d5a-e23d-5eee-a7a9-9927ebff79dd"
    ],
    [
      "FE",
      "Ferrosols",
      "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Ferrosols- are soils with B2 horizons in which the major part has a free iron oxide content greater than 5% Fe in the fine earth fraction (<2 mm). Soils with a B2 horizon in which at least 0.3m has vertic properties are excluded (see also Comment and footnote in Ferrosols). (Reference: The Australian Soil Classification, Third Edition).",
      "https://linked.data.gov.au/def/nrm/b594e58d-251c-5fcc-b75b-319a7938d793"
    ],
    [
      "HY",
      "Hydrosols",
      "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Hydrosols- are soils that are saturated in the major part of the soil profile for at least 2-3 months in most years (ie. includes tidal waters).(Reference: The Australian Soil Classification, Third Edition).",
      "https://linked.data.gov.au/def/nrm/339a0f8c-b6fe-56dd-b435-d8f0d3c77e3a"
    ],
    [
      "KA",
      "Kandosols",
      "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Kandosols- soils which lack strong texture contrast, have massive or only weakly structured B horizons, and are not calcareous throughout. The soils of this order range throughout the continent, often occurring locally as very large areas (Reference: The Australian Soil Classification, Third Edition).",
      "https://linked.data.gov.au/def/nrm/6a252cf1-d760-5dd5-845f-7c092a612f6a"
    ],
    [
      "KU",
      "Kurosols",
      "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Kurosols- soils with a clear or abrupt textural B horizon and in which the major part of the upper 0.2 m of the B2t horizon (or the major part of the entire B2t horizon if it is less than 0.2 m thick) is strongly acid. (Reference: The Australian Soil Classification, Third Edition).",
      "https://linked.data.gov.au/def/nrm/b5e96d13-44fa-5ada-8da1-46fc4e50b30d"
    ],
    [
      "OR",
      "Organosols",
      "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Organosols- caters for most soils dominated by organic materials. Although they are found from the wet tropics to the alpine regions, areas are mostly small except in south west Tasmania. (Reference: The Australian Soil Classification, Third Edition).",
      "https://linked.data.gov.au/def/nrm/3c0b65ab-b74b-5ccc-972a-71e302e57a63"
    ],
    [
      "PO",
      "Podosols",
      "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Podosols- are soils that have a Bs, Bhs or Bh horizon. These horizons may occur either singly or in combination. (Reference: The Australian Soil Classification, Third Edition).",
      "https://linked.data.gov.au/def/nrm/885c1e7c-c55b-55a4-a139-5efb95cc9e03"
    ],
    [
      "RU",
      "Rudosols",
      "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Rudosols- are soils with negligible (rudimentary), if any, pedologic organisation apart from the minimal development of an A1 horizon or the presence of less than 10% of B horizon material (including pedogenic carbonate) in fissures in the parent rock or saprolite. The soils have a grade of pedality of single grain, massive or weak in the A1 horizon and show no pedological colour change apart from darkening of an A1 horizon. (Reference: The Australian Soil Classification, Third Edition).",
      "https://linked.data.gov.au/def/nrm/dd038cec-458f-532b-97c8-a917ba5d931c"
    ],
    [
      "SO",
      "Sodosols",
      "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Sodosols- are soils with a clear or abrupt textural B horizon and in which the major part of the upper 0.2 m of the B2t horizon (or the major part of the entire B2t horizon if it is less than 0.2 m thick) is sodic and is not strongly subplastic. (Reference: The Australian Soil Classification, Third Edition).",
      "https://linked.data.gov.au/def/nrm/5f8a5a25-7f7a-55ff-8619-2a547a32b443"
    ],
    [
      "TE",
      "Tenosols",
      "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Tenosols- are designed to embrace soils with generally only weak pedologic organisation apart from the A horizons, excluding soils that have deep sandy profiles with a field texture of sand, loamy sand or clayey sand in 80% or more of the upper 1.0 m. (Reference: The Australian Soil Classification, Third Edition).",
      "https://linked.data.gov.au/def/nrm/6e19da3d-db5a-5b5a-b37d-82a86079f73a"
    ],
    [
      "VE",
      "Vertosols",
      "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Vertosols- are clay soils with shrink-swell properties that exhibit strong cracking when dry and at depth have slickensides and/or lenticular peds. (Reference: The Australian Soil Classification, Third Edition).",
      "https://linked.data.gov.au/def/nrm/219f7ae4-4aed-5ffe-8fc9-82cc83f77d7a"
    ],
    [
      "YY",
      "Class Undetermined",
      "Refers to the soils that cannot be determined to a specific soil order, as per the Australian Soil Classification orders.",
      "https://linked.data.gov.au/def/nrm/c3e8a0e8-bdeb-5142-b114-94ae0ff86611"
    ],
    [
      "ZZ",
      "No Class Available",
      "Refers to the soils with no specific soil order, as per the Australian Soil Classification orders.",
      "https://linked.data.gov.au/def/nrm/c786373b-900d-5e82-b438-d8a58f26726e"
    ]
  ]
}
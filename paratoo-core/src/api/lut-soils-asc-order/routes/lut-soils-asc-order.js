module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-asc-orders',
      'handler': 'lut-soils-asc-order.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-asc-orders/:id',
      'handler': 'lut-soils-asc-order.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-asc-order.lut-soils-asc-order')
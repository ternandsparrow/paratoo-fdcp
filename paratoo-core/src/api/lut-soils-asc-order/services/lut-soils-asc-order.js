const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soils-asc-order.lut-soils-asc-order')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/herbivory-off-plot-belt-survey-setups',
      'handler': 'herbivory-off-plot-belt-survey-setup.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/herbivory-off-plot-belt-survey-setups/:id',
      'handler': 'herbivory-off-plot-belt-survey-setup.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/herbivory-off-plot-belt-survey-setups',
      'handler': 'herbivory-off-plot-belt-survey-setup.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/herbivory-off-plot-belt-survey-setups/:id',
      'handler': 'herbivory-off-plot-belt-survey-setup.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/herbivory-off-plot-belt-survey-setups/:id',
      'handler': 'herbivory-off-plot-belt-survey-setup.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * herbivory-off-plot-belt-survey-setup service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::herbivory-off-plot-belt-survey-setup.herbivory-off-plot-belt-survey-setup')

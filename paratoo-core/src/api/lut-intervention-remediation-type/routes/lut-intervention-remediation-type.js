module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-remediation-types',
      'handler': 'lut-intervention-remediation-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-remediation-types/:id',
      'handler': 'lut-intervention-remediation-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-remediation-type.lut-intervention-remediation-type')
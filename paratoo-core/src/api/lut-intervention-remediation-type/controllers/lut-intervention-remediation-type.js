const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-remediation-type.lut-intervention-remediation-type')
const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-access-control-type.lut-intervention-access-control-type')
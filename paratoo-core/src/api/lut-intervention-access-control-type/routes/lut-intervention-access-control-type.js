module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-access-control-types',
      'handler': 'lut-intervention-access-control-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-access-control-types/:id',
      'handler': 'lut-intervention-access-control-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
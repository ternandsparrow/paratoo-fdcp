const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-access-control-type.lut-intervention-access-control-type')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-protocol-variants',
      'handler': 'lut-protocol-variant.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-protocol-variants/:id',
      'handler': 'lut-protocol-variant.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
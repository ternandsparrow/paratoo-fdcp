const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-protocol-variant.lut-protocol-variant')
const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soils-asc-class.lut-soils-asc-class')
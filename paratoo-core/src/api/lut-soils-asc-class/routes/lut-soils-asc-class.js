module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-asc-classs',
      'handler': 'lut-soils-asc-class.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-asc-classs/:id',
      'handler': 'lut-soils-asc-class.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
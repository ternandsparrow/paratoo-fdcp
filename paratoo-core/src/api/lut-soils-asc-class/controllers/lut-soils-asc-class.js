const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-asc-class.lut-soils-asc-class')
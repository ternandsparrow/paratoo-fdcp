'use strict'

/**
 * camera-trap-deployment-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::camera-trap-deployment-survey.camera-trap-deployment-survey')

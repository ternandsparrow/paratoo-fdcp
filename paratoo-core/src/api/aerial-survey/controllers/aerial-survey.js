'use strict'

/**
 *  aerial-survey controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController(
  'api::aerial-survey.aerial-survey',
  ({ strapi }) => ({
    /* Generic find controller with pdp data filter */
    async find(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      
      const authToken = ctx.request.headers['authorization']
      const modelName = ctx.state.route.info.apiName
      const tokenStatus = await helpers.apiTokenCheck(authToken)
      const useDefaultController = ctx.request.query['use-default'] === 'true'
      
      // if we need to use default controller (use magic jwt and use-default flag)
      if (tokenStatus?.type == 'full-access' && useDefaultController) {
        helpers.paratooDebugMsg(`Default controller is used to populate model: ${modelName}`)
        return await super.find(ctx)
      }
      return await helpers.pdpDataFilter(
        modelName,
        authToken,
        ctx.request.query,
        tokenStatus,
      )
    },

    /**
     * Business logic endpoint for performing a cwd survey - uploads a collection of cwd
     * surveys and their observations)
     *
     * @param {Object} ctx Koa context
     * @returns {Array} successfully-created array of collections
     */
    async bulk(ctx) {
      const keys = Object.keys(ctx.request.body.data.collections[0])

      let obsModelName = null
      let childObservationModelName = null

      for (const key of keys) {
        if (key === 'aerial-setup-desktop') {
          obsModelName = key
          break
        }

        if (key === 'aerial-observation') {
          obsModelName = key
          childObservationModelName = ['ferals-aerial-count-survey']
        }
      }

      return strapi.service('api::paratoo-helper.paratoo-helper').genericBulk({
        ctx: ctx,
        surveyModelName: 'aerial-survey',
        obsModelName: obsModelName,
        childObservationModelName: childObservationModelName,
      })
    },
  }),
)

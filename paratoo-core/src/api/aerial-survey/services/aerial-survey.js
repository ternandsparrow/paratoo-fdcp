'use strict'

/**
 * aerial-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::aerial-survey.aerial-survey')

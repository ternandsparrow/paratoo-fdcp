'use strict'

/**
 * lut-vertebrate-class service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-class.lut-vertebrate-class')

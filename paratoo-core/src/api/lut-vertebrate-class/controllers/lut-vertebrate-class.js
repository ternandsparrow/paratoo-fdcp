'use strict'

/**
 *  lut-vertebrate-class controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-class.lut-vertebrate-class')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-classes',
      'handler': 'lut-vertebrate-class.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-classes/:id',
      'handler': 'lut-vertebrate-class.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * recruitment-survivorship-observation service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::recruitment-survivorship-observation.recruitment-survivorship-observation')

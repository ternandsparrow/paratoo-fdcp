const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-invertebrate-pan-trapping-colour.lut-invertebrate-pan-trapping-colour')
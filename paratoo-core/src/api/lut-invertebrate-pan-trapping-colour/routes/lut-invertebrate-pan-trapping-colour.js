module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-invertebrate-pan-trapping-colours',
      'handler': 'lut-invertebrate-pan-trapping-colour.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-invertebrate-pan-trapping-colours/:id',
      'handler': 'lut-invertebrate-pan-trapping-colour.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-invertebrate-pan-trapping-colour.lut-invertebrate-pan-trapping-colour')
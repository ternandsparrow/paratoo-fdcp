'use strict'

/**
 * soil-sub-pit-observation service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-sub-pit-observation.soil-sub-pit-observation')

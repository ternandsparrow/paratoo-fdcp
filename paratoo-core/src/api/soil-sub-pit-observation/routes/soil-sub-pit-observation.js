module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/soil-sub-pit-observations',
      'handler': 'soil-sub-pit-observation.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/soil-sub-pit-observations/:id',
      'handler': 'soil-sub-pit-observation.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/soil-sub-pit-observations',
      'handler': 'soil-sub-pit-observation.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/soil-sub-pit-observations/:id',
      'handler': 'soil-sub-pit-observation.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/soil-sub-pit-observations/:id',
      'handler': 'soil-sub-pit-observation.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * remove-pan-trap service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::remove-pan-trap.remove-pan-trap')

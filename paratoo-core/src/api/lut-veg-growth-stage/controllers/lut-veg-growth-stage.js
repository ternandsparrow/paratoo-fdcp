const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-veg-growth-stage.lut-veg-growth-stage')
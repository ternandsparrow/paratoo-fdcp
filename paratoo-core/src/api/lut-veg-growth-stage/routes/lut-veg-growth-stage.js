module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-veg-growth-stages',
      'handler': 'lut-veg-growth-stage.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-veg-growth-stages/:id',
      'handler': 'lut-veg-growth-stage.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-vertebrate-trap-status controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-trap-status.lut-vertebrate-trap-status')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-trap-statuses',
      'handler': 'lut-vertebrate-trap-status.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-trap-statuses/:id',
      'handler': 'lut-vertebrate-trap-status.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
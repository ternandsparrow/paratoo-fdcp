'use strict'

/**
 * lut-vertebrate-trap-status service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-trap-status.lut-vertebrate-trap-status')

'use strict'

/**
 * lut-location-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-location-type.lut-location-type')

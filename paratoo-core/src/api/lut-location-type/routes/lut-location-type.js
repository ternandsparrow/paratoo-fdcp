module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-location-types',
      'handler': 'lut-location-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-location-types/:id',
      'handler': 'lut-location-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
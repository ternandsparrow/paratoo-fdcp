'use strict'

/**
 *  lut-location-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-location-type.lut-location-type')

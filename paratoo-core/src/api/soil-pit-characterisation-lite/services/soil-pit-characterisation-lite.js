'use strict'

/**
 * soil-pit-characterisation-lite service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-pit-characterisation-lite.soil-pit-characterisation-lite')

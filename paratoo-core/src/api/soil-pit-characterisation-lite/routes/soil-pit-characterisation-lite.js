module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/soil-pit-characterisation-lites',
      'handler': 'soil-pit-characterisation-lite.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/soil-pit-characterisation-lites/:id',
      'handler': 'soil-pit-characterisation-lite.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/soil-pit-characterisation-lites',
      'handler': 'soil-pit-characterisation-lite.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/soil-pit-characterisation-lites/:id',
      'handler': 'soil-pit-characterisation-lite.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/soil-pit-characterisation-lites/:id',
      'handler': 'soil-pit-characterisation-lite.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/soil-pit-characterisation-lites/bulk',
      'handler': 'soil-pit-characterisation-lite.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-teats-condition.lut-fauna-teats-condition')
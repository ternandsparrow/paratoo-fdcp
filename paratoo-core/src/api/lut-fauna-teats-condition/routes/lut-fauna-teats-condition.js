module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-teats-conditions',
      'handler': 'lut-fauna-teats-condition.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-teats-conditions/:id',
      'handler': 'lut-fauna-teats-condition.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
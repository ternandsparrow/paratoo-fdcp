const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-type.lut-intervention-type')
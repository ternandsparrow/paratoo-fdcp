module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-types',
      'handler': 'lut-intervention-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-types/:id',
      'handler': 'lut-intervention-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
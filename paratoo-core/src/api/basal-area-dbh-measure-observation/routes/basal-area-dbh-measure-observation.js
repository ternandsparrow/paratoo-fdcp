module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/basal-area-dbh-measure-observations',
      'handler': 'basal-area-dbh-measure-observation.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/basal-area-dbh-measure-observations/:id',
      'handler': 'basal-area-dbh-measure-observation.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/basal-area-dbh-measure-observations',
      'handler': 'basal-area-dbh-measure-observation.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/basal-area-dbh-measure-observations/:id',
      'handler': 'basal-area-dbh-measure-observation.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/basal-area-dbh-measure-observations/:id',
      'handler': 'basal-area-dbh-measure-observation.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
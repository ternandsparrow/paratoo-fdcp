'use strict'

/**
 * basal-area-dbh-measure-observation service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::basal-area-dbh-measure-observation.basal-area-dbh-measure-observation')

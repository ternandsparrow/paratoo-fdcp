'use strict'

/**
 * grassy-weeds-observation service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::grassy-weeds-observation.grassy-weeds-observation')

'use strict'

/**
 *  lut-drone-micasense-crp controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-drone-micasense-crp.lut-drone-micasense-crp')

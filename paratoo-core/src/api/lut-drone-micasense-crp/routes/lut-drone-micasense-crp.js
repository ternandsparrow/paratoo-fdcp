module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-drone-micasense-crps',
      'handler': 'lut-drone-micasense-crp.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-drone-micasense-crps/:id',
      'handler': 'lut-drone-micasense-crp.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
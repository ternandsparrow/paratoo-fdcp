'use strict'

/**
 * lut-drone-micasense-crp service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-drone-micasense-crp.lut-drone-micasense-crp')

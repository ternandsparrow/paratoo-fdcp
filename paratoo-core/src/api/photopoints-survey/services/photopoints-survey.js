'use strict'

/**
 * photopoints-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::photopoints-survey.photopoints-survey')

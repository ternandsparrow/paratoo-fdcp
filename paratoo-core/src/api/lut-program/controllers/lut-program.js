const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-program.lut-program')
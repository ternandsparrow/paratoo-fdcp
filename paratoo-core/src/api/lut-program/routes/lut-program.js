module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-programs',
      'handler': 'lut-program.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-programs/:id',
      'handler': 'lut-program.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
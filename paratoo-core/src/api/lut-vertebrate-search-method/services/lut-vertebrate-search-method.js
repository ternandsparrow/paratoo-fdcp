'use strict'

/**
 * lut-vertebrate-search-method service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-search-method.lut-vertebrate-search-method')

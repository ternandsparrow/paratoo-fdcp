module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-search-methods',
      'handler': 'lut-vertebrate-search-method.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-search-methods/:id',
      'handler': 'lut-vertebrate-search-method.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-vertebrate-search-method controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-search-method.lut-vertebrate-search-method')

'use strict'

/**
 * lut-track-station-micro-habitat service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-track-station-micro-habitat.lut-track-station-micro-habitat')

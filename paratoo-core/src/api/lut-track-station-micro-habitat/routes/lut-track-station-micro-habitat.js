module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-track-station-micro-habitats',
      'handler': 'lut-track-station-micro-habitat.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-track-station-micro-habitats/:id',
      'handler': 'lut-track-station-micro-habitat.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
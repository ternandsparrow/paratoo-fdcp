'use strict'

/**
 *  lut-track-station-micro-habitat controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-track-station-micro-habitat.lut-track-station-micro-habitat')

'use strict'

/**
 * lut-data-captured-flora service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-data-captured-flora.lut-data-captured-flora')

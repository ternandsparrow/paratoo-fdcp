module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-data-captured-floras',
      'handler': 'lut-data-captured-flora.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-data-captured-floras/:id',
      'handler': 'lut-data-captured-flora.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
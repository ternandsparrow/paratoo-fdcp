'use strict'

/**
 * lut-data-captured-flora controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-data-captured-flora.lut-data-captured-flora')

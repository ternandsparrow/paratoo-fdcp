module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-structure-grades',
      'handler': 'lut-soils-structure-grade.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-structure-grades/:id',
      'handler': 'lut-soils-structure-grade.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
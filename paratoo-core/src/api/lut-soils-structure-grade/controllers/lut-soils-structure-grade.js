const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-structure-grade.lut-soils-structure-grade')
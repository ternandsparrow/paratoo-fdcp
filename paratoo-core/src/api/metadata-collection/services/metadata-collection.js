'use strict'

/**
 * metadata-collection service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::metadata-collection.metadata-collection')

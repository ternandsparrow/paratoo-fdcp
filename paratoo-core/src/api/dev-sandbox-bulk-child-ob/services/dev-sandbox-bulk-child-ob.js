'use strict'

/**
 * dev-sandbox-bulk-child-ob service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::dev-sandbox-bulk-child-ob.dev-sandbox-bulk-child-ob')

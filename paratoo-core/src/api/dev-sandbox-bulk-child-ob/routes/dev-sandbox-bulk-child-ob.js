module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/dev-sandbox-bulk-child-obs',
      'handler': 'dev-sandbox-bulk-child-ob.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/dev-sandbox-bulk-child-obs/:id',
      'handler': 'dev-sandbox-bulk-child-ob.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/dev-sandbox-bulk-child-obs',
      'handler': 'dev-sandbox-bulk-child-ob.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/dev-sandbox-bulk-child-obs/:id',
      'handler': 'dev-sandbox-bulk-child-ob.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/dev-sandbox-bulk-child-obs/:id',
      'handler': 'dev-sandbox-bulk-child-ob.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
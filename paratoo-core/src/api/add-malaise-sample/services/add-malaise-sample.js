'use strict'

/**
 * add-malaise-sample service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::add-malaise-sample.add-malaise-sample')

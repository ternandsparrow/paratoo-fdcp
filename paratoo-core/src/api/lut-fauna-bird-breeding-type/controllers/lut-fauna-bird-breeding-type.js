const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-bird-breeding-type.lut-fauna-bird-breeding-type')
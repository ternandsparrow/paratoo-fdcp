module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-bird-breeding-types',
      'handler': 'lut-fauna-bird-breeding-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-bird-breeding-types/:id',
      'handler': 'lut-fauna-bird-breeding-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
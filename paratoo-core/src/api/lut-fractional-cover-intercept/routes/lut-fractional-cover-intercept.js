module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fractional-cover-intercepts',
      'handler': 'lut-fractional-cover-intercept.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fractional-cover-intercepts/:id',
      'handler': 'lut-fractional-cover-intercept.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-fractional-cover-intercept controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fractional-cover-intercept.lut-fractional-cover-intercept')

'use strict'

/**
 * lut-fractional-cover-intercept service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-fractional-cover-intercept.lut-fractional-cover-intercept')

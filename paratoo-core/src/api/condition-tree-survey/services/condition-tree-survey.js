'use strict'

/**
 * condition-tree-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService(
  'api::condition-tree-survey.condition-tree-survey',
)

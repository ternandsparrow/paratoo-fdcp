module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-veg-associations',
      'handler': 'lut-veg-association.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-veg-associations/:id',
      'handler': 'lut-veg-association.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-veg-association controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-veg-association.lut-veg-association')

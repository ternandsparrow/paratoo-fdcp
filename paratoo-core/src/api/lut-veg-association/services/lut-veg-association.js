'use strict'

/**
 * lut-veg-association service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-veg-association.lut-veg-association')

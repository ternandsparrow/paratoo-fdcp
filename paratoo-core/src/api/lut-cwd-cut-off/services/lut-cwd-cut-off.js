'use strict'

/**
 * lut-cwd-cut-off service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-cwd-cut-off.lut-cwd-cut-off')

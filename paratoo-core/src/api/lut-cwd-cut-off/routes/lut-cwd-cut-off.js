module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-cwd-cut-offs',
      'handler': 'lut-cwd-cut-off.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-cwd-cut-offs/:id',
      'handler': 'lut-cwd-cut-off.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
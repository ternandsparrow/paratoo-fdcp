'use strict'

/**
 *  lut-cwd-cut-off controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-cwd-cut-off.lut-cwd-cut-off')

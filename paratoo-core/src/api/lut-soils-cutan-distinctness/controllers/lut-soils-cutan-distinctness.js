const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-cutan-distinctness.lut-soils-cutan-distinctness')
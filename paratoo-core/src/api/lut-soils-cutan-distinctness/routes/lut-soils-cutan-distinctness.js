module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-cutan-distinctnesss',
      'handler': 'lut-soils-cutan-distinctness.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-cutan-distinctnesss/:id',
      'handler': 'lut-soils-cutan-distinctness.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
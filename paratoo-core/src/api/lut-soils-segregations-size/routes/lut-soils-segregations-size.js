module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-segregations-sizes',
      'handler': 'lut-soils-segregations-size.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-segregations-sizes/:id',
      'handler': 'lut-soils-segregations-size.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
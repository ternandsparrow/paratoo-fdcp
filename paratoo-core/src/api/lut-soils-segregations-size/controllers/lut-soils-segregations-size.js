const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-segregations-size.lut-soils-segregations-size')
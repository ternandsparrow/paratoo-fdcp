module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-mottle-contrasts',
      'handler': 'lut-soils-mottle-contrast.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-mottle-contrasts/:id',
      'handler': 'lut-soils-mottle-contrast.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
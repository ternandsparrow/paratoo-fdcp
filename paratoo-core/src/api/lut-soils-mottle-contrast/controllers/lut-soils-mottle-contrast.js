const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-mottle-contrast.lut-soils-mottle-contrast')
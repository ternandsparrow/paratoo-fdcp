'use strict'

/**
 * lut-hapd-number-of-transect service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-hapd-number-of-transect.lut-hapd-number-of-transect')

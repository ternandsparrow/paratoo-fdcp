module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-hapd-number-of-transects',
      'handler': 'lut-hapd-number-of-transect.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-hapd-number-of-transects/:id',
      'handler': 'lut-hapd-number-of-transect.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
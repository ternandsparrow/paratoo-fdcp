'use strict'

/**
 *  lut-hapd-number-of-transect controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-hapd-number-of-transect.lut-hapd-number-of-transect')

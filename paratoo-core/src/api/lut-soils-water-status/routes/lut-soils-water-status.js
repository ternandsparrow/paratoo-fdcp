module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-water-statuss',
      'handler': 'lut-soils-water-status.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-water-statuss/:id',
      'handler': 'lut-soils-water-status.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-water-status.lut-soils-water-status')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-condition-damage-types',
      'handler': 'lut-condition-damage-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-condition-damage-types/:id',
      'handler': 'lut-condition-damage-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
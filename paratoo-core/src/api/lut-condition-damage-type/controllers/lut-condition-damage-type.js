'use strict'

/**
 * lut-condition-damage-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-condition-damage-type.lut-condition-damage-type')

'use strict'

/**
 * lut-condition-damage-type service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-condition-damage-type.lut-condition-damage-type')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soil-sub-pit-collection-methods',
      'handler': 'lut-soil-sub-pit-collection-method.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soil-sub-pit-collection-methods/:id',
      'handler': 'lut-soil-sub-pit-collection-method.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
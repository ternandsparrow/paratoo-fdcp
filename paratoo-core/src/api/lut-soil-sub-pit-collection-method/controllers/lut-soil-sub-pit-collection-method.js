'use strict'

/**
 *  lut-soil-sub-pit-collection-method controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soil-sub-pit-collection-method.lut-soil-sub-pit-collection-method')

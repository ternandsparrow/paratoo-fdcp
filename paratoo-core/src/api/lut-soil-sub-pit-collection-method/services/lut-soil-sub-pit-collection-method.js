'use strict'

/**
 * lut-soil-sub-pit-collection-method service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soil-sub-pit-collection-method.lut-soil-sub-pit-collection-method')

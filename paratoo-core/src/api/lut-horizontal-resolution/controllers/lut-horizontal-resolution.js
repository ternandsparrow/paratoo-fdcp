'use strict'

/**
 * lut-horizontal-resolution controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-horizontal-resolution.lut-horizontal-resolution')

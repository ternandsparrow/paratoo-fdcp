'use strict'

/**
 * lut-horizontal-resolution service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-horizontal-resolution.lut-horizontal-resolution')

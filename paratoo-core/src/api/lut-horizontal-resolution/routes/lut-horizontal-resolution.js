module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-horizontal-resolutions',
      'handler': 'lut-horizontal-resolution.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-horizontal-resolutions/:id',
      'handler': 'lut-horizontal-resolution.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
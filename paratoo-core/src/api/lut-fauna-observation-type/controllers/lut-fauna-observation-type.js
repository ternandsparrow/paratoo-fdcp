'use strict'

/**
 * lut-fauna-observation-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-observation-type.lut-fauna-observation-type')

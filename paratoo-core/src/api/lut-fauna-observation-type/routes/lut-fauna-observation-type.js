module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-observation-types',
      'handler': 'lut-fauna-observation-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-observation-types/:id',
      'handler': 'lut-fauna-observation-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
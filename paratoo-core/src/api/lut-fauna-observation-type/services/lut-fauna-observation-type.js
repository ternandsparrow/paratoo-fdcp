'use strict'

/**
 * lut-fauna-observation-type service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-fauna-observation-type.lut-fauna-observation-type')

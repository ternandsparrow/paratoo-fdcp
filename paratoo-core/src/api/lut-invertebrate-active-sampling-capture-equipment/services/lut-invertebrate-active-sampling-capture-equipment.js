const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-invertebrate-active-sampling-capture-equipment.lut-invertebrate-active-sampling-capture-equipment')
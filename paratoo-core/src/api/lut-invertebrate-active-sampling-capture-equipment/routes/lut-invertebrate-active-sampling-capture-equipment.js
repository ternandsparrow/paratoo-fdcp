module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-invertebrate-active-sampling-capture-equipments',
      'handler': 'lut-invertebrate-active-sampling-capture-equipment.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-invertebrate-active-sampling-capture-equipments/:id',
      'handler': 'lut-invertebrate-active-sampling-capture-equipment.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
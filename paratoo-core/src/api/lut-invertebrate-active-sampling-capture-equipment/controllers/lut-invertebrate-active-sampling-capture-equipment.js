const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-invertebrate-active-sampling-capture-equipment.lut-invertebrate-active-sampling-capture-equipment')
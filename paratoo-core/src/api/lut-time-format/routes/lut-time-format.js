module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-time-formats',
      'handler': 'lut-time-format.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-time-formats/:id',
      'handler': 'lut-time-format.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-time-format controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-time-format.lut-time-format')

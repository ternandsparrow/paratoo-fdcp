'use strict'

/**
 * lut-time-format service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-time-format.lut-time-format')

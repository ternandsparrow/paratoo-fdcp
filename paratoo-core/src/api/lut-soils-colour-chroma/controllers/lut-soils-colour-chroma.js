const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-colour-chroma.lut-soils-colour-chroma')
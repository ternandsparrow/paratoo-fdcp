module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-colour-chromas',
      'handler': 'lut-soils-colour-chroma.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-colour-chromas/:id',
      'handler': 'lut-soils-colour-chroma.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
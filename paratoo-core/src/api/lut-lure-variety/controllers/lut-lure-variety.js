'use strict'

/**
 *  lut-lure-variety controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-lure-variety.lut-lure-variety')

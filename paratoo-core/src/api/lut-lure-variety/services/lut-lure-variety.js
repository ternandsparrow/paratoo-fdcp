'use strict'

/**
 * lut-lure-variety service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-lure-variety.lut-lure-variety')

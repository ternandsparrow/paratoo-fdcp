module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-lure-varieties',
      'handler': 'lut-lure-variety.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-lure-varieties/:id',
      'handler': 'lut-lure-variety.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/invertebrate-malaise-traps',
      'handler': 'invertebrate-malaise-trap.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/invertebrate-malaise-traps/:id',
      'handler': 'invertebrate-malaise-trap.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/invertebrate-malaise-traps',
      'handler': 'invertebrate-malaise-trap.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/invertebrate-malaise-traps/:id',
      'handler': 'invertebrate-malaise-trap.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/invertebrate-malaise-traps/:id',
      'handler': 'invertebrate-malaise-trap.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
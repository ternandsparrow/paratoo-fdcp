'use strict'

/**
 * invertebrate-malaise-trap service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::invertebrate-malaise-trap.invertebrate-malaise-trap')

'use strict'

/**
 * lut-observer-confidence controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController(
  'api::lut-observer-confidence.lut-observer-confidence',
)

'use strict'

/**
 * lut-observer-confidence service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService(
  'api::lut-observer-confidence.lut-observer-confidence',
)

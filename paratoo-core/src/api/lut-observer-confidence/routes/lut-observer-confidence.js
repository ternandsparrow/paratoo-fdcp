module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-observer-confidences',
      'handler': 'lut-observer-confidence.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-observer-confidences/:id',
      'handler': 'lut-observer-confidence.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
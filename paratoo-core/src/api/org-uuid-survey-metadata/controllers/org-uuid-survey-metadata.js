'use strict'

/**
 * org-uuid-survey-metadata controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::org-uuid-survey-metadata.org-uuid-survey-metadata')

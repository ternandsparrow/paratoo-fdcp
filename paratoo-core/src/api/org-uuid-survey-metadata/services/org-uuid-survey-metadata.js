'use strict'

/**
 * org-uuid-survey-metadata service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::org-uuid-survey-metadata.org-uuid-survey-metadata')

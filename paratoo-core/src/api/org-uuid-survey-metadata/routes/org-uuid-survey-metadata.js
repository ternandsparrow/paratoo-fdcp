module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/org-uuid-survey-metadatas',
      'handler': 'org-uuid-survey-metadata.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/org-uuid-survey-metadatas/:id',
      'handler': 'org-uuid-survey-metadata.findOne',
      'config': {
        'middlewares': [
          'global::pdp-data-filter'
        ],
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/org-uuid-survey-metadatas',
      'handler': 'org-uuid-survey-metadata.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/org-uuid-survey-metadatas/:id',
      'handler': 'org-uuid-survey-metadata.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/org-uuid-survey-metadatas/:id',
      'handler': 'org-uuid-survey-metadata.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
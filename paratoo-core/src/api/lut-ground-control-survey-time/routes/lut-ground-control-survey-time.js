module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-ground-control-survey-times',
      'handler': 'lut-ground-control-survey-time.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-ground-control-survey-times/:id',
      'handler': 'lut-ground-control-survey-time.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
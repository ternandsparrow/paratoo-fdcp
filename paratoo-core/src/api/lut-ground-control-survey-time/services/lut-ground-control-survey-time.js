'use strict'

/**
 * lut-ground-control-survey-time service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-ground-control-survey-time.lut-ground-control-survey-time')

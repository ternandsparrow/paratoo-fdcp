'use strict'

/**
 *  lut-ground-control-survey-time controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-ground-control-survey-time.lut-ground-control-survey-time')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-observer-positions',
      'handler': 'lut-observer-position.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-observer-positions/:id',
      'handler': 'lut-observer-position.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
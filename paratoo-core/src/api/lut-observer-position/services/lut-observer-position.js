'use strict'

/**
 * lut-observer-position service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-observer-position.lut-observer-position')

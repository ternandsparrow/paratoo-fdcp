'use strict'

/**
 *  lut-observer-position controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-observer-position.lut-observer-position')

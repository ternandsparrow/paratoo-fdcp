'use strict'

/**
 * wet-pitfall-weather service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::wet-pitfall-weather.wet-pitfall-weather')

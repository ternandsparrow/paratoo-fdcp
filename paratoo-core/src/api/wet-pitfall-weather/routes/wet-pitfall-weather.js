module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/wet-pitfall-weathers',
      'handler': 'wet-pitfall-weather.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/wet-pitfall-weathers/:id',
      'handler': 'wet-pitfall-weather.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/wet-pitfall-weathers',
      'handler': 'wet-pitfall-weather.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/wet-pitfall-weathers/:id',
      'handler': 'wet-pitfall-weather.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/wet-pitfall-weathers/:id',
      'handler': 'wet-pitfall-weather.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/invertebrate-wet-pitfall-surveys',
      'handler': 'invertebrate-wet-pitfall-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/invertebrate-wet-pitfall-surveys/:id',
      'handler': 'invertebrate-wet-pitfall-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/invertebrate-wet-pitfall-surveys',
      'handler': 'invertebrate-wet-pitfall-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/invertebrate-wet-pitfall-surveys/:id',
      'handler': 'invertebrate-wet-pitfall-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/invertebrate-wet-pitfall-surveys/:id',
      'handler': 'invertebrate-wet-pitfall-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/invertebrate-wet-pitfall-surveys/bulk',
      'handler': 'invertebrate-wet-pitfall-survey.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
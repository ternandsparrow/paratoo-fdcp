'use strict'

/**
 * invertebrate-wet-pitfall-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::invertebrate-wet-pitfall-survey.invertebrate-wet-pitfall-survey')

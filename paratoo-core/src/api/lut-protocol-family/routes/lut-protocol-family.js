module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-protocol-families',
      'handler': 'lut-protocol-family.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-protocol-families/:id',
      'handler': 'lut-protocol-family.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-plot-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-plot-type.lut-plot-type')

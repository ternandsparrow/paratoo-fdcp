'use strict'

/**
 *  lut-plot-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-plot-type.lut-plot-type')

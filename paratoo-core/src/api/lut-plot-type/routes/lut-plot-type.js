module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-plot-types',
      'handler': 'lut-plot-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-plot-types/:id',
      'handler': 'lut-plot-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
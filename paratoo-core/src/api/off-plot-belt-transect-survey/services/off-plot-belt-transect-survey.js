'use strict'

/**
 * off-plot-belt-transect-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::off-plot-belt-transect-survey.off-plot-belt-transect-survey')

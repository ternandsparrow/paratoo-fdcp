module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/off-plot-belt-transect-surveys',
      'handler': 'off-plot-belt-transect-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/off-plot-belt-transect-surveys/:id',
      'handler': 'off-plot-belt-transect-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/off-plot-belt-transect-surveys',
      'handler': 'off-plot-belt-transect-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/off-plot-belt-transect-surveys/:id',
      'handler': 'off-plot-belt-transect-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/off-plot-belt-transect-surveys/:id',
      'handler': 'off-plot-belt-transect-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/off-plot-belt-transect-surveys/bulk',
      'handler': 'off-plot-belt-transect-survey.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
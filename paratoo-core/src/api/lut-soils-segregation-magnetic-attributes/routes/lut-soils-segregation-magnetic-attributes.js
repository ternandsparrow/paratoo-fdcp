module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-segregation-magnetic-attributes',
      'handler': 'lut-soils-segregation-magnetic-attributes.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-segregation-magnetic-attributes/:id',
      'handler': 'lut-soils-segregation-magnetic-attributes.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
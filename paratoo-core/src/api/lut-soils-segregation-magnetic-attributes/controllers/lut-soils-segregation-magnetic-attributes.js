const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-segregation-magnetic-attributes.lut-soils-segregation-magnetic-attributes')
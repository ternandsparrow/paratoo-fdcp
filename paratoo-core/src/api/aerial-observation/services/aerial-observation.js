'use strict'

/**
 * aerial-observation service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::aerial-observation.aerial-observation')

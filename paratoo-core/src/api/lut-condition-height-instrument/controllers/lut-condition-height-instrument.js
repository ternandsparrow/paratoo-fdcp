'use strict'

/**
 *  lut-condition-height-instrument controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-condition-height-instrument.lut-condition-height-instrument')

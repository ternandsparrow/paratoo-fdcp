module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-condition-height-instruments',
      'handler': 'lut-condition-height-instrument.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-condition-height-instruments/:id',
      'handler': 'lut-condition-height-instrument.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
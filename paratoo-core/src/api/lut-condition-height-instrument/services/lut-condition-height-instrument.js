'use strict'

/**
 * lut-condition-height-instrument service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-condition-height-instrument.lut-condition-height-instrument')

'use strict'

/**
 * lut-fauna-survey-intent controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-survey-intent.lut-fauna-survey-intent')

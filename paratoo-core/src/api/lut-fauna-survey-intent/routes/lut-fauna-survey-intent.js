module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-survey-intents',
      'handler': 'lut-fauna-survey-intent.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-survey-intents/:id',
      'handler': 'lut-fauna-survey-intent.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
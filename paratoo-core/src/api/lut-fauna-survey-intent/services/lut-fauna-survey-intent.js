'use strict'

/**
 * lut-fauna-survey-intent service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-fauna-survey-intent.lut-fauna-survey-intent')

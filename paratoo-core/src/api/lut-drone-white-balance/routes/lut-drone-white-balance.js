module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-drone-white-balances',
      'handler': 'lut-drone-white-balance.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-drone-white-balances/:id',
      'handler': 'lut-drone-white-balance.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
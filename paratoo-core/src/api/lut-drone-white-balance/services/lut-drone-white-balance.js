'use strict'

/**
 * lut-drone-white-balance service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-drone-white-balance.lut-drone-white-balance')

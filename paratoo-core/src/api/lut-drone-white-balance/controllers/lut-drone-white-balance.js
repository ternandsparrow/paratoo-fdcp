'use strict'

/**
 *  lut-drone-white-balance controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-drone-white-balance.lut-drone-white-balance')

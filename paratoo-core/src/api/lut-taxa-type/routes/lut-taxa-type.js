module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-taxa-types',
      'handler': 'lut-taxa-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-taxa-types/:id',
      'handler': 'lut-taxa-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-taxa-type.lut-taxa-type')
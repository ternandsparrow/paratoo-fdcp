'use strict'

/**
 * lut-epbc-status controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-epbc-status.lut-epbc-status')

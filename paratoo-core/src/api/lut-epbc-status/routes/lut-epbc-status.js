module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-epbc-statuses',
      'handler': 'lut-epbc-status.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-epbc-statuses/:id',
      'handler': 'lut-epbc-status.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
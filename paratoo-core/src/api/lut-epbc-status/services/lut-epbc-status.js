'use strict'

/**
 * lut-epbc-status service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-epbc-status.lut-epbc-status')

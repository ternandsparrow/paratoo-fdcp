const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-scald-erosion-degree.lut-soils-scald-erosion-degree')
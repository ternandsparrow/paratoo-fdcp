module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-scald-erosion-degrees',
      'handler': 'lut-soils-scald-erosion-degree.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-scald-erosion-degrees/:id',
      'handler': 'lut-soils-scald-erosion-degree.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-fire-management-type.lut-intervention-fire-management-type')
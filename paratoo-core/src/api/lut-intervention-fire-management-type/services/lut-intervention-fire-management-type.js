const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-fire-management-type.lut-intervention-fire-management-type')
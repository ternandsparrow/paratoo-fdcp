module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-fire-management-types',
      'handler': 'lut-intervention-fire-management-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-fire-management-types/:id',
      'handler': 'lut-intervention-fire-management-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * fire-point-intercept-point service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::fire-point-intercept-point.fire-point-intercept-point')

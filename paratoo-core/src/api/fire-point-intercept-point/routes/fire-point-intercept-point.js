module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/fire-point-intercept-points',
      'handler': 'fire-point-intercept-point.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/fire-point-intercept-points/:id',
      'handler': 'fire-point-intercept-point.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/fire-point-intercept-points',
      'handler': 'fire-point-intercept-point.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/fire-point-intercept-points/:id',
      'handler': 'fire-point-intercept-point.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/fire-point-intercept-points/:id',
      'handler': 'fire-point-intercept-point.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
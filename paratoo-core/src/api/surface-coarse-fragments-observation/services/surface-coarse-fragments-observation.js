const { createCoreService } = require('@strapi/strapi').factories
module.exports = createCoreService(
  'api::surface-coarse-fragments-observation.surface-coarse-fragments-observation',
)

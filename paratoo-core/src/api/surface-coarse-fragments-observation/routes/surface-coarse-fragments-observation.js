module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/surface-coarse-fragments-observations',
      'handler': 'surface-coarse-fragments-observation.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/surface-coarse-fragments-observations/:id',
      'handler': 'surface-coarse-fragments-observation.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/surface-coarse-fragments-observations',
      'handler': 'surface-coarse-fragments-observation.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/surface-coarse-fragments-observations/:id',
      'handler': 'surface-coarse-fragments-observation.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/surface-coarse-fragments-observations/:id',
      'handler': 'surface-coarse-fragments-observation.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
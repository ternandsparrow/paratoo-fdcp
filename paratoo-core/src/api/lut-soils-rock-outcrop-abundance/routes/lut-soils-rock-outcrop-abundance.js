module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-rock-outcrop-abundances',
      'handler': 'lut-soils-rock-outcrop-abundance.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-rock-outcrop-abundances/:id',
      'handler': 'lut-soils-rock-outcrop-abundance.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-rock-outcrop-abundance.lut-soils-rock-outcrop-abundance')
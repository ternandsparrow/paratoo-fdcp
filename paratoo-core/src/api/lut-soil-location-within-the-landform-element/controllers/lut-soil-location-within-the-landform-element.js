'use strict'

/**
 *  lut-soil-location-within-the-landform-element controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soil-location-within-the-landform-element.lut-soil-location-within-the-landform-element')

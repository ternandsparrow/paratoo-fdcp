'use strict'

/**
 * lut-soil-location-within-the-landform-element service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soil-location-within-the-landform-element.lut-soil-location-within-the-landform-element')

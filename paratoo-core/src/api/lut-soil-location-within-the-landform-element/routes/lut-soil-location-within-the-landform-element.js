module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soil-location-within-the-landform-elements',
      'handler': 'lut-soil-location-within-the-landform-element.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soil-location-within-the-landform-elements/:id',
      'handler': 'lut-soil-location-within-the-landform-element.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-cutan-type.lut-soils-cutan-type')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-cutan-types',
      'handler': 'lut-soils-cutan-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-cutan-types/:id',
      'handler': 'lut-soils-cutan-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
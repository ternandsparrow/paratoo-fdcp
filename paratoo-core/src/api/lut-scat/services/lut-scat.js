'use strict'

/**
 * lut-scat service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-scat.lut-scat')

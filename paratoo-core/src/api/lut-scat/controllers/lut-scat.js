'use strict'

/**
 *  lut-scat controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-scat.lut-scat')

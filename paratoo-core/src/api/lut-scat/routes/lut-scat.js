module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-scats',
      'handler': 'lut-scat.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-scats/:id',
      'handler': 'lut-scat.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-veg-structural-formation.lut-veg-structural-formation')
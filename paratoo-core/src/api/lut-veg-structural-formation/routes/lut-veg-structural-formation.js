module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-veg-structural-formations',
      'handler': 'lut-veg-structural-formation.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-veg-structural-formations/:id',
      'handler': 'lut-veg-structural-formation.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
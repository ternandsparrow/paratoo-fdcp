'use strict'

/**
 * lut-recruitment-transect service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-recruitment-transect.lut-recruitment-transect')

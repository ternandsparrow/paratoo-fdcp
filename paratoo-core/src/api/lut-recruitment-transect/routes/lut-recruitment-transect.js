module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-recruitment-transects',
      'handler': 'lut-recruitment-transect.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-recruitment-transects/:id',
      'handler': 'lut-recruitment-transect.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
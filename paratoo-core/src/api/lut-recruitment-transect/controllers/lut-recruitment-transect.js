'use strict'

/**
 *  lut-recruitment-transect controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-recruitment-transect.lut-recruitment-transect')

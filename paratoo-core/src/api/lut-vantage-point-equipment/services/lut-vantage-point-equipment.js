'use strict'

/**
 * lut-vantage-point-equipment service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vantage-point-equipment.lut-vantage-point-equipment')

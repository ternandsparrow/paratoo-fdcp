'use strict'

/**
 *  lut-vantage-point-equipment controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vantage-point-equipment.lut-vantage-point-equipment')

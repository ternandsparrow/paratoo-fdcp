module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vantage-point-equipments',
      'handler': 'lut-vantage-point-equipment.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vantage-point-equipments/:id',
      'handler': 'lut-vantage-point-equipment.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/invertebrate-pan-trappings',
      'handler': 'invertebrate-pan-trapping.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/invertebrate-pan-trappings/:id',
      'handler': 'invertebrate-pan-trapping.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/invertebrate-pan-trappings',
      'handler': 'invertebrate-pan-trapping.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/invertebrate-pan-trappings/:id',
      'handler': 'invertebrate-pan-trapping.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/invertebrate-pan-trappings/:id',
      'handler': 'invertebrate-pan-trapping.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/invertebrate-pan-trappings/bulk',
      'handler': 'invertebrate-pan-trapping.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
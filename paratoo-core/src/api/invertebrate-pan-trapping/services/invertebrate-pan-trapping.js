'use strict'

/**
 * invertebrate-pan-trapping service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::invertebrate-pan-trapping.invertebrate-pan-trapping')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-segregations-abundances',
      'handler': 'lut-soils-segregations-abundance.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-segregations-abundances/:id',
      'handler': 'lut-soils-segregations-abundance.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
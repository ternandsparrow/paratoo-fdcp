const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-segregations-abundance.lut-soils-segregations-abundance')
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-coarse-frag-alteration.lut-soils-coarse-frag-alteration')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-coarse-frag-alterations',
      'handler': 'lut-soils-coarse-frag-alteration.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-coarse-frag-alterations/:id',
      'handler': 'lut-soils-coarse-frag-alteration.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
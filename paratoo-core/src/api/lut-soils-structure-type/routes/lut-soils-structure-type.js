module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-structure-types',
      'handler': 'lut-soils-structure-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-structure-types/:id',
      'handler': 'lut-soils-structure-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
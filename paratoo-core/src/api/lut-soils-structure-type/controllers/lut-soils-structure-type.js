const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-structure-type.lut-soils-structure-type')
const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-invertebrate-liquid-type.lut-invertebrate-liquid-type')
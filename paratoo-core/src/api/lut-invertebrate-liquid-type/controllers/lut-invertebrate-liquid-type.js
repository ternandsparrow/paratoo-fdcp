const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-invertebrate-liquid-type.lut-invertebrate-liquid-type')
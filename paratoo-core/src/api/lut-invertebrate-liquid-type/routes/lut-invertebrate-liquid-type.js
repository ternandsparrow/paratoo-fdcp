module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-invertebrate-liquid-types',
      'handler': 'lut-invertebrate-liquid-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-invertebrate-liquid-types/:id',
      'handler': 'lut-invertebrate-liquid-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-plot-dimensions',
      'handler': 'lut-plot-dimensions.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-plot-dimensions/:id',
      'handler': 'lut-plot-dimensions.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
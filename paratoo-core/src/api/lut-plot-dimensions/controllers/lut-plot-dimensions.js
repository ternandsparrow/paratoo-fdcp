const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-plot-dimensions.lut-plot-dimensions')
'use strict'

/**
 * recruitment-growth-stage service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::recruitment-growth-stage.recruitment-growth-stage')

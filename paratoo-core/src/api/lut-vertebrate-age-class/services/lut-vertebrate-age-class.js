'use strict'

/**
 * lut-vertebrate-age-class service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-age-class.lut-vertebrate-age-class')

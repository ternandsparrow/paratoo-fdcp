'use strict'

/**
 *  lut-vertebrate-age-class controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-age-class.lut-vertebrate-age-class')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-age-classes',
      'handler': 'lut-vertebrate-age-class.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-age-classes/:id',
      'handler': 'lut-vertebrate-age-class.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
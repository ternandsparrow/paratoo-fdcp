module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-macropore-diameters',
      'handler': 'lut-soils-macropore-diameter.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-macropore-diameters/:id',
      'handler': 'lut-soils-macropore-diameter.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
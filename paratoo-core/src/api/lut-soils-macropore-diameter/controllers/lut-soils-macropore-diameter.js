const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-macropore-diameter.lut-soils-macropore-diameter')
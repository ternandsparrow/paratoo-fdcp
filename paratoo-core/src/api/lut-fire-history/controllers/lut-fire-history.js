const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fire-history.lut-fire-history')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fire-histories',
      'handler': 'lut-fire-history.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fire-histories/:id',
      'handler': 'lut-fire-history.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
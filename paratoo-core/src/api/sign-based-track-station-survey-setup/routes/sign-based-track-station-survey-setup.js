module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/sign-based-track-station-survey-setups',
      'handler': 'sign-based-track-station-survey-setup.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/sign-based-track-station-survey-setups/:id',
      'handler': 'sign-based-track-station-survey-setup.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/sign-based-track-station-survey-setups',
      'handler': 'sign-based-track-station-survey-setup.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/sign-based-track-station-survey-setups/:id',
      'handler': 'sign-based-track-station-survey-setup.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/sign-based-track-station-survey-setups/:id',
      'handler': 'sign-based-track-station-survey-setup.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
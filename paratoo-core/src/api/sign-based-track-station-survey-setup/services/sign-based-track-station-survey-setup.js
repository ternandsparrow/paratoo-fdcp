'use strict'

/**
 * sign-based-track-station-survey-setup service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::sign-based-track-station-survey-setup.sign-based-track-station-survey-setup')

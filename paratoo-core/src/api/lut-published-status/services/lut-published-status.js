'use strict'

/**
 * lut-published-status service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-published-status.lut-published-status')

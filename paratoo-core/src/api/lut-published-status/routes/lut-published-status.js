module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-published-statuses',
      'handler': 'lut-published-status.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-published-statuses/:id',
      'handler': 'lut-published-status.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
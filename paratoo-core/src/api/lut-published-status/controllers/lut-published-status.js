'use strict'

/**
 * lut-published-status controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-published-status.lut-published-status')

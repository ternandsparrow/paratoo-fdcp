const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-texture-qualification.lut-soils-texture-qualification')
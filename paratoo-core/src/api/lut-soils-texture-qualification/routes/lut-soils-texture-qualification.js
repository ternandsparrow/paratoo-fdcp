module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-texture-qualifications',
      'handler': 'lut-soils-texture-qualification.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-texture-qualifications/:id',
      'handler': 'lut-soils-texture-qualification.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-plot-layout-marker controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-plot-layout-marker.lut-plot-layout-marker')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-plot-layout-markers',
      'handler': 'lut-plot-layout-marker.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-plot-layout-markers/:id',
      'handler': 'lut-plot-layout-marker.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
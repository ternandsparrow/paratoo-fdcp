'use strict'

/**
 * lut-plot-layout-marker service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-plot-layout-marker.lut-plot-layout-marker')

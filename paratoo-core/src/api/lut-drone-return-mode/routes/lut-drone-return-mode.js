module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-drone-return-modes',
      'handler': 'lut-drone-return-mode.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-drone-return-modes/:id',
      'handler': 'lut-drone-return-mode.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
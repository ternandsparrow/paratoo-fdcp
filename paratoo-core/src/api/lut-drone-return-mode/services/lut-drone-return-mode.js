'use strict'

/**
 * lut-drone-return-mode service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-drone-return-mode.lut-drone-return-mode')

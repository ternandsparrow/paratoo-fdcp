'use strict'

/**
 *  lut-drone-return-mode controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-drone-return-mode.lut-drone-return-mode')

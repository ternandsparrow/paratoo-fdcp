module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/sign-based-active-plot-search-surveys',
      'handler': 'sign-based-active-plot-search-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/sign-based-active-plot-search-surveys/:id',
      'handler': 'sign-based-active-plot-search-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/sign-based-active-plot-search-surveys',
      'handler': 'sign-based-active-plot-search-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/sign-based-active-plot-search-surveys/:id',
      'handler': 'sign-based-active-plot-search-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/sign-based-active-plot-search-surveys/:id',
      'handler': 'sign-based-active-plot-search-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/sign-based-active-plot-search-surveys/bulk',
      'handler': 'sign-based-active-plot-search-survey.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
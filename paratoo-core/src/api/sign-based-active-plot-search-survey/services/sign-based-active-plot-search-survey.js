'use strict'

/**
 * sign-based-active-plot-search-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::sign-based-active-plot-search-survey.sign-based-active-plot-search-survey')

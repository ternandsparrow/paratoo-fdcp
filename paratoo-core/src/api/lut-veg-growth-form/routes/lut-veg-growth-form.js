module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-veg-growth-forms',
      'handler': 'lut-veg-growth-form.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-veg-growth-forms/:id',
      'handler': 'lut-veg-growth-form.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
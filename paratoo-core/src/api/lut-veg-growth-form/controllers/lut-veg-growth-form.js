const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-veg-growth-form.lut-veg-growth-form')
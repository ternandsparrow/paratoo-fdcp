module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-control-activity-animal-fates',
      'handler': 'lut-control-activity-animal-fate.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-control-activity-animal-fates/:id',
      'handler': 'lut-control-activity-animal-fate.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-control-activity-animal-fate service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-control-activity-animal-fate.lut-control-activity-animal-fate')

'use strict'

/**
 *  lut-control-activity-animal-fate controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-control-activity-animal-fate.lut-control-activity-animal-fate')

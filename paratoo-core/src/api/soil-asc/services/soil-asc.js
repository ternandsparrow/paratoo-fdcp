'use strict'

/**
 * soil-asc service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-asc.soil-asc')

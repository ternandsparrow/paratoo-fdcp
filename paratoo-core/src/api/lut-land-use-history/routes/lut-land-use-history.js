module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-land-use-histories',
      'handler': 'lut-land-use-history.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-land-use-histories/:id',
      'handler': 'lut-land-use-history.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-land-use-history service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-land-use-history.lut-land-use-history')

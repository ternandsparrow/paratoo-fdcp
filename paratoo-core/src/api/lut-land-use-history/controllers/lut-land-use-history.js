'use strict'

/**
 *  lut-land-use-history controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-land-use-history.lut-land-use-history')

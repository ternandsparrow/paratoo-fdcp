module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/cover-point-intercept-points',
      'handler': 'cover-point-intercept-point.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/cover-point-intercept-points/:id',
      'handler': 'cover-point-intercept-point.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/cover-point-intercept-points',
      'handler': 'cover-point-intercept-point.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/cover-point-intercept-points/:id',
      'handler': 'cover-point-intercept-point.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/cover-point-intercept-points/:id',
      'handler': 'cover-point-intercept-point.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
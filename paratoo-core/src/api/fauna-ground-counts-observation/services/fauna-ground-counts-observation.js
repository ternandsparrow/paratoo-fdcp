'use strict'

/**
 * fauna-ground-counts-observation service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::fauna-ground-counts-observation.fauna-ground-counts-observation')

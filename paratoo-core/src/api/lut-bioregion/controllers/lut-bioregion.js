const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-bioregion.lut-bioregion')
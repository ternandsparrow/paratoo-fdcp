module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-bioregions',
      'handler': 'lut-bioregion.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-bioregions/:id',
      'handler': 'lut-bioregion.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
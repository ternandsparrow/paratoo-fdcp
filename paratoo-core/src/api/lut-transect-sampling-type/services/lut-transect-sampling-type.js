'use strict'

/**
 * lut-transect-sampling-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-transect-sampling-type.lut-transect-sampling-type')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-transect-sampling-types',
      'handler': 'lut-transect-sampling-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-transect-sampling-types/:id',
      'handler': 'lut-transect-sampling-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-transect-sampling-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-transect-sampling-type.lut-transect-sampling-type')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soil-asc-family-details',
      'handler': 'lut-soil-asc-family-detail.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soil-asc-family-details/:id',
      'handler': 'lut-soil-asc-family-detail.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
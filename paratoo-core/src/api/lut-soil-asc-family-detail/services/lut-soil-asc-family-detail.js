'use strict'

/**
 * lut-soil-asc-family-detail service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soil-asc-family-detail.lut-soil-asc-family-detail')

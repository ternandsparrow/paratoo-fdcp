'use strict'

/**
 *  lut-soil-asc-family-detail controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soil-asc-family-detail.lut-soil-asc-family-detail')

'use strict'

/**
 * lut-landscape-category controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-landscape-category.lut-landscape-category')

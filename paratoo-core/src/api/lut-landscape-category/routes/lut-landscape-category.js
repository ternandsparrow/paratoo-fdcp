module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-landscape-categories',
      'handler': 'lut-landscape-category.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-landscape-categories/:id',
      'handler': 'lut-landscape-category.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
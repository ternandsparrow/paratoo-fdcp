'use strict'

/**
 * lut-landscape-category service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-landscape-category.lut-landscape-category')

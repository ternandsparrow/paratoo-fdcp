'use strict'

/**
 * lut-microhabitat service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-microhabitat.lut-microhabitat')

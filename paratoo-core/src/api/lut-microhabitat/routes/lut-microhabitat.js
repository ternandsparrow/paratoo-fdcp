module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-microhabitats',
      'handler': 'lut-microhabitat.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-microhabitats/:id',
      'handler': 'lut-microhabitat.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
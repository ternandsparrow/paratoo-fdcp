'use strict'

/**
 *  lut-microhabitat controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-microhabitat.lut-microhabitat')

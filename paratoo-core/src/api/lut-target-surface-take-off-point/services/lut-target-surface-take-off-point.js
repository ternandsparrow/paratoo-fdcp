'use strict'

/**
 * lut-target-surface-take-off-point service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-target-surface-take-off-point.lut-target-surface-take-off-point')

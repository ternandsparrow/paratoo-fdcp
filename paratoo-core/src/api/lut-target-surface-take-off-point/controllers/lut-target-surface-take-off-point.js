'use strict'

/**
 * lut-target-surface-take-off-point controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-target-surface-take-off-point.lut-target-surface-take-off-point')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-target-surface-take-off-points',
      'handler': 'lut-target-surface-take-off-point.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-target-surface-take-off-points/:id',
      'handler': 'lut-target-surface-take-off-point.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
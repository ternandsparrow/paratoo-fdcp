const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::fire-species-intercept.fire-species-intercept')
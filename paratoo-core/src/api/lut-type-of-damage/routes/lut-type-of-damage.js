module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-type-of-damages',
      'handler': 'lut-type-of-damage.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-type-of-damages/:id',
      'handler': 'lut-type-of-damage.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
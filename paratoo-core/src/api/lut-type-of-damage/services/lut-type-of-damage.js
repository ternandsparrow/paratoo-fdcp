'use strict'

/**
 * lut-type-of-damage service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-type-of-damage.lut-type-of-damage')

'use strict'

/**
 *  lut-type-of-damage controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-type-of-damage.lut-type-of-damage')

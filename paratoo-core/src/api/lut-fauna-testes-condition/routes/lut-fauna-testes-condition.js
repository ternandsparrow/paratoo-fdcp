module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-testes-conditions',
      'handler': 'lut-fauna-testes-condition.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-testes-conditions/:id',
      'handler': 'lut-fauna-testes-condition.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-testes-condition.lut-fauna-testes-condition')
'use strict'

/**
 * herbivory-and-physical-damage-belt-transect-setup service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::herbivory-and-physical-damage-belt-transect-setup.herbivory-and-physical-damage-belt-transect-setup')

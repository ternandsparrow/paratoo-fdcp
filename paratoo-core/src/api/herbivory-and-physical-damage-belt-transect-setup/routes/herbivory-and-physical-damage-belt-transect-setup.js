module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/herbivory-and-physical-damage-belt-transect-setups',
      'handler': 'herbivory-and-physical-damage-belt-transect-setup.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/herbivory-and-physical-damage-belt-transect-setups/:id',
      'handler': 'herbivory-and-physical-damage-belt-transect-setup.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/herbivory-and-physical-damage-belt-transect-setups',
      'handler': 'herbivory-and-physical-damage-belt-transect-setup.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/herbivory-and-physical-damage-belt-transect-setups/:id',
      'handler': 'herbivory-and-physical-damage-belt-transect-setup.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/herbivory-and-physical-damage-belt-transect-setups/:id',
      'handler': 'herbivory-and-physical-damage-belt-transect-setup.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/herbivory-and-physical-damage-belt-transect-setups/bulk',
      'handler': 'herbivory-and-physical-damage-belt-transect-setup.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
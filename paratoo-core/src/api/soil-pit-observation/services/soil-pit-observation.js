'use strict'

/**
 * soil-pit-observation service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-pit-observation.soil-pit-observation')

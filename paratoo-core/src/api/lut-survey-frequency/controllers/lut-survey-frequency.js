'use strict'

/**
 * lut-survey-frequency controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-survey-frequency.lut-survey-frequency')

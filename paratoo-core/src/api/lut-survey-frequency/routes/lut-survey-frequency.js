module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-survey-frequencies',
      'handler': 'lut-survey-frequency.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-survey-frequencies/:id',
      'handler': 'lut-survey-frequency.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
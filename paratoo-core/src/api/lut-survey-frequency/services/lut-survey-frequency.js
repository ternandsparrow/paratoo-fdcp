'use strict'

/**
 * lut-survey-frequency service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-survey-frequency.lut-survey-frequency')

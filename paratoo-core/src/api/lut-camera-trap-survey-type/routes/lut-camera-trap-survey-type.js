module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-camera-trap-survey-types',
      'handler': 'lut-camera-trap-survey-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-camera-trap-survey-types/:id',
      'handler': 'lut-camera-trap-survey-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-camera-trap-survey-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-camera-trap-survey-type.lut-camera-trap-survey-type')

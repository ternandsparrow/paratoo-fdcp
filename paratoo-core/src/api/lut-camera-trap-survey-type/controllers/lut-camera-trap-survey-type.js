'use strict'

/**
 *  lut-camera-trap-survey-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-camera-trap-survey-type.lut-camera-trap-survey-type')

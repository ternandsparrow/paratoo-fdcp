module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/coarse-woody-debris-observations',
      'handler': 'coarse-woody-debris-observation.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/coarse-woody-debris-observations/:id',
      'handler': 'coarse-woody-debris-observation.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/coarse-woody-debris-observations',
      'handler': 'coarse-woody-debris-observation.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/coarse-woody-debris-observations/:id',
      'handler': 'coarse-woody-debris-observation.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/coarse-woody-debris-observations/:id',
      'handler': 'coarse-woody-debris-observation.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
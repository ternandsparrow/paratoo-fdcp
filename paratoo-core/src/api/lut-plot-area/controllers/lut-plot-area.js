'use strict'

/**
 *  lut-plot-area controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-plot-area.lut-plot-area')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-plot-areas',
      'handler': 'lut-plot-area.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-plot-areas/:id',
      'handler': 'lut-plot-area.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
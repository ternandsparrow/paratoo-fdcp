'use strict'

/**
 * lut-plot-area service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-plot-area.lut-plot-area')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-camera-aspect-ratios',
      'handler': 'lut-camera-aspect-ratio.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-camera-aspect-ratios/:id',
      'handler': 'lut-camera-aspect-ratio.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
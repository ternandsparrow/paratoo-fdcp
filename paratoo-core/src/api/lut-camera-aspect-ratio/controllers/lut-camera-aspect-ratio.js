'use strict'

/**
 *  lut-camera-aspect-ratio controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-camera-aspect-ratio.lut-camera-aspect-ratio')

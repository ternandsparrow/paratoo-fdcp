'use strict'

/**
 * lut-camera-aspect-ratio service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-camera-aspect-ratio.lut-camera-aspect-ratio')

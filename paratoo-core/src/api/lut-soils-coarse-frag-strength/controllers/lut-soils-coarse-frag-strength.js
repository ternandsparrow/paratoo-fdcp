const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-coarse-frag-strength.lut-soils-coarse-frag-strength')
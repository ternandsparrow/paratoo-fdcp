module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-drift-fence-names',
      'handler': 'lut-vertebrate-drift-fence-name.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-drift-fence-names/:id',
      'handler': 'lut-vertebrate-drift-fence-name.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
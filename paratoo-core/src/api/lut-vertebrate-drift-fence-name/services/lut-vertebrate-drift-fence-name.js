'use strict'

/**
 * lut-vertebrate-drift-fence-name service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-drift-fence-name.lut-vertebrate-drift-fence-name')

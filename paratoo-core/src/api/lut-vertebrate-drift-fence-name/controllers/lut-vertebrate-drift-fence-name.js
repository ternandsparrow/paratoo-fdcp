'use strict'

/**
 *  lut-vertebrate-drift-fence-name controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-drift-fence-name.lut-vertebrate-drift-fence-name')

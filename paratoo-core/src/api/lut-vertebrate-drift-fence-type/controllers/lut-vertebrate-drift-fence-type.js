'use strict'

/**
 *  lut-vertebrate-drift-fence-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-drift-fence-type.lut-vertebrate-drift-fence-type')

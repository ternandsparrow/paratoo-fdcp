'use strict'

/**
 * lut-vertebrate-drift-fence-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-drift-fence-type.lut-vertebrate-drift-fence-type')

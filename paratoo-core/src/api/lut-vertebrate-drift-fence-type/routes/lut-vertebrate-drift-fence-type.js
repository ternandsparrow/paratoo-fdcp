module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-drift-fence-types',
      'handler': 'lut-vertebrate-drift-fence-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-drift-fence-types/:id',
      'handler': 'lut-vertebrate-drift-fence-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
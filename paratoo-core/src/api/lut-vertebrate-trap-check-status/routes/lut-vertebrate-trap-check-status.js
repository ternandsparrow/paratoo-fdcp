module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-trap-check-statuses',
      'handler': 'lut-vertebrate-trap-check-status.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-trap-check-statuses/:id',
      'handler': 'lut-vertebrate-trap-check-status.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
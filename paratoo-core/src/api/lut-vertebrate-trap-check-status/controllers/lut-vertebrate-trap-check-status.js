'use strict'

/**
 *  lut-vertebrate-trap-check-status controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-trap-check-status.lut-vertebrate-trap-check-status')

'use strict'

/**
 * lut-vertebrate-trap-check-status service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-trap-check-status.lut-vertebrate-trap-check-status')

'use strict'

/**
 * soil-horizon-pan service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-horizon-pan.soil-horizon-pan')

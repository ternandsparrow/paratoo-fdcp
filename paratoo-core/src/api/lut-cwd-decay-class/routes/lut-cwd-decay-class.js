module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-cwd-decay-classs',
      'handler': 'lut-cwd-decay-class.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-cwd-decay-classs/:id',
      'handler': 'lut-cwd-decay-class.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
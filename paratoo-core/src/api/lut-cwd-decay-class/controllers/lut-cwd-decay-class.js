const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-cwd-decay-class.lut-cwd-decay-class')
'use strict'

/**
 * sign-based-vehicle-track-survey service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::sign-based-vehicle-track-survey.sign-based-vehicle-track-survey')

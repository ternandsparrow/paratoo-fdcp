module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/sign-based-vehicle-track-surveys',
      'handler': 'sign-based-vehicle-track-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/sign-based-vehicle-track-surveys/:id',
      'handler': 'sign-based-vehicle-track-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/sign-based-vehicle-track-surveys',
      'handler': 'sign-based-vehicle-track-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/sign-based-vehicle-track-surveys/:id',
      'handler': 'sign-based-vehicle-track-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/sign-based-vehicle-track-surveys/:id',
      'handler': 'sign-based-vehicle-track-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/sign-based-vehicle-track-surveys/bulk',
      'handler': 'sign-based-vehicle-track-survey.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
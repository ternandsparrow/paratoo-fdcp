'use strict'

/**
 *  lut-track-station-substrate controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-track-station-substrate.lut-track-station-substrate')

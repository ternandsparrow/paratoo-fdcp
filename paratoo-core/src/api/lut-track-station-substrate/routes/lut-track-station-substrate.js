module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-track-station-substrates',
      'handler': 'lut-track-station-substrate.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-track-station-substrates/:id',
      'handler': 'lut-track-station-substrate.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
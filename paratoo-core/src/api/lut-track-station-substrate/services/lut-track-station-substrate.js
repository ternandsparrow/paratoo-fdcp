'use strict'

/**
 * lut-track-station-substrate service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-track-station-substrate.lut-track-station-substrate')

'use strict'

/**
 *  lut-camera-trap-mount controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-camera-trap-mount.lut-camera-trap-mount')

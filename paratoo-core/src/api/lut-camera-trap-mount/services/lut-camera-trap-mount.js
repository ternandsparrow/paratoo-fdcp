'use strict'

/**
 * lut-camera-trap-mount service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-camera-trap-mount.lut-camera-trap-mount')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-camera-trap-mounts',
      'handler': 'lut-camera-trap-mount.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-camera-trap-mounts/:id',
      'handler': 'lut-camera-trap-mount.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
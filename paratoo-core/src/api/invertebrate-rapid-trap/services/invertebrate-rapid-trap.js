'use strict'

/**
 * invertebrate-rapid-trap service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::invertebrate-rapid-trap.invertebrate-rapid-trap')

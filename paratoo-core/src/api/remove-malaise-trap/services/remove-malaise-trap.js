'use strict'

/**
 * remove-malaise-trap service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::remove-malaise-trap.remove-malaise-trap')

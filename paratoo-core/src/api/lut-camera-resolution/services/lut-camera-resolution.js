'use strict'

/**
 * lut-camera-resolution service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-camera-resolution.lut-camera-resolution')

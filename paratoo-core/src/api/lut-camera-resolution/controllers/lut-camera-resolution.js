'use strict'

/**
 *  lut-camera-resolution controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-camera-resolution.lut-camera-resolution')

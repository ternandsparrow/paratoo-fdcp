module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-camera-resolutions',
      'handler': 'lut-camera-resolution.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-camera-resolutions/:id',
      'handler': 'lut-camera-resolution.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
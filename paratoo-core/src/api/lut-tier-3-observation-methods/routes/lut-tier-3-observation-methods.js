module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-tier-3-observation-methods',
      'handler': 'lut-tier-3-observation-methods.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-tier-3-observation-methods/:id',
      'handler': 'lut-tier-3-observation-methods.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
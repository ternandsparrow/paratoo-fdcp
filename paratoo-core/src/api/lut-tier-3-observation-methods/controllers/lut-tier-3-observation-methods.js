const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-tier-3-observation-methods.lut-tier-3-observation-methods')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-cwd-transect-numbers',
      'handler': 'lut-cwd-transect-number.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-cwd-transect-numbers/:id',
      'handler': 'lut-cwd-transect-number.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
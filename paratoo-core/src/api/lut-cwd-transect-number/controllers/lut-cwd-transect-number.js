const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-cwd-transect-number.lut-cwd-transect-number')
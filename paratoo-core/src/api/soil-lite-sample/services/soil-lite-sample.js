'use strict'

/**
 * soil-lite-sample service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-lite-sample.soil-lite-sample')

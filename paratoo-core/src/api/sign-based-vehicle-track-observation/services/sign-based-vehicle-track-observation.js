'use strict'

/**
 * sign-based-vehicle-track-observation service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::sign-based-vehicle-track-observation.sign-based-vehicle-track-observation')

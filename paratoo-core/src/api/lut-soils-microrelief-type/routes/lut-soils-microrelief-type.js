module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-microrelief-types',
      'handler': 'lut-soils-microrelief-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-microrelief-types/:id',
      'handler': 'lut-soils-microrelief-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
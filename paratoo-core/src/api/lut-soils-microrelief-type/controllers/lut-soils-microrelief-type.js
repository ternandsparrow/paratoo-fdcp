const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-microrelief-type.lut-soils-microrelief-type')
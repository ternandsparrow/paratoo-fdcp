module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-disturbances',
      'handler': 'lut-disturbance.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-disturbances/:id',
      'handler': 'lut-disturbance.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-disturbance.lut-disturbance')
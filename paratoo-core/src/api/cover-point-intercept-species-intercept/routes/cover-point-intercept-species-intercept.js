module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/cover-point-intercept-species-intercepts',
      'handler': 'cover-point-intercept-species-intercept.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/cover-point-intercept-species-intercepts/:id',
      'handler': 'cover-point-intercept-species-intercept.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/cover-point-intercept-species-intercepts',
      'handler': 'cover-point-intercept-species-intercept.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/cover-point-intercept-species-intercepts/:id',
      'handler': 'cover-point-intercept-species-intercept.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/cover-point-intercept-species-intercepts/:id',
      'handler': 'cover-point-intercept-species-intercept.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
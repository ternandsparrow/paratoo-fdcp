module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/floristics-veg-survey-fulls',
      'handler': 'floristics-veg-survey-full.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/floristics-veg-survey-fulls/:id',
      'handler': 'floristics-veg-survey-full.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/floristics-veg-survey-fulls',
      'handler': 'floristics-veg-survey-full.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/floristics-veg-survey-fulls/:id',
      'handler': 'floristics-veg-survey-full.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/floristics-veg-survey-fulls/:id',
      'handler': 'floristics-veg-survey-full.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/floristics-veg-survey-fulls/bulk',
      'handler': 'floristics-veg-survey-full.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
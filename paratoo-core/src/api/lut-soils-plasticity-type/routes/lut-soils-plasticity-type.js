module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-plasticity-types',
      'handler': 'lut-soils-plasticity-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-plasticity-types/:id',
      'handler': 'lut-soils-plasticity-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
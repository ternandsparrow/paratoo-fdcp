'use strict'

/**
 *  lut-soils-plasticity-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-plasticity-type.lut-soils-plasticity-type')

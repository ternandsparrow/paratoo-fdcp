'use strict'

/**
 * lut-soils-plasticity-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soils-plasticity-type.lut-soils-plasticity-type')

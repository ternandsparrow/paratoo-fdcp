module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/ferals-aerial-count-surveys',
      'handler': 'ferals-aerial-count-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/ferals-aerial-count-surveys/:id',
      'handler': 'ferals-aerial-count-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/ferals-aerial-count-surveys',
      'handler': 'ferals-aerial-count-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/ferals-aerial-count-surveys/:id',
      'handler': 'ferals-aerial-count-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/ferals-aerial-count-surveys/:id',
      'handler': 'ferals-aerial-count-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
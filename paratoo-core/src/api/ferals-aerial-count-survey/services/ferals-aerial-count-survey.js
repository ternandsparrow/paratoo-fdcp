'use strict'

/**
 * ferals-aerial-count-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::ferals-aerial-count-survey.ferals-aerial-count-survey')

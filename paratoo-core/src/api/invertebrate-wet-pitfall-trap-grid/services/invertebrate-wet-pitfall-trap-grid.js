'use strict'

/**
 * invertebrate-wet-pitfall-trap-grid service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::invertebrate-wet-pitfall-trap-grid.invertebrate-wet-pitfall-trap-grid')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/invertebrate-wet-pitfall-trap-grids',
      'handler': 'invertebrate-wet-pitfall-trap-grid.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/invertebrate-wet-pitfall-trap-grids/:id',
      'handler': 'invertebrate-wet-pitfall-trap-grid.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/invertebrate-wet-pitfall-trap-grids',
      'handler': 'invertebrate-wet-pitfall-trap-grid.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/invertebrate-wet-pitfall-trap-grids/:id',
      'handler': 'invertebrate-wet-pitfall-trap-grid.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/invertebrate-wet-pitfall-trap-grids/:id',
      'handler': 'invertebrate-wet-pitfall-trap-grid.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
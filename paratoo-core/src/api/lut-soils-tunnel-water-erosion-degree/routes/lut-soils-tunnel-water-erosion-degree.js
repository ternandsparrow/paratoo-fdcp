module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-tunnel-water-erosion-degrees',
      'handler': 'lut-soils-tunnel-water-erosion-degree.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-tunnel-water-erosion-degrees/:id',
      'handler': 'lut-soils-tunnel-water-erosion-degree.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
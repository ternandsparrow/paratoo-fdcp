const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-tunnel-water-erosion-degree.lut-soils-tunnel-water-erosion-degree')
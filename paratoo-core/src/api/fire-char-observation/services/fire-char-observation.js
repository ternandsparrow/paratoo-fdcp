'use strict'

/**
 * fire-char-observation service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::fire-char-observation.fire-char-observation')

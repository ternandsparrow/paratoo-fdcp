'use strict'

/**
 *  lut-protocol-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-protocol-type.lut-protocol-type')

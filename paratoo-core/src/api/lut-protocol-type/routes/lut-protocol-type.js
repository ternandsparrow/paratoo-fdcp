module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-protocol-types',
      'handler': 'lut-protocol-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-protocol-types/:id',
      'handler': 'lut-protocol-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
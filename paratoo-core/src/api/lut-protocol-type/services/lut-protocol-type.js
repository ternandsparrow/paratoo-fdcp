'use strict'

/**
 * lut-protocol-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-protocol-type.lut-protocol-type')

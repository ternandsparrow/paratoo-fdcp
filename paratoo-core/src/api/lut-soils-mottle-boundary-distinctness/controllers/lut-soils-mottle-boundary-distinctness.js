const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-mottle-boundary-distinctness.lut-soils-mottle-boundary-distinctness')
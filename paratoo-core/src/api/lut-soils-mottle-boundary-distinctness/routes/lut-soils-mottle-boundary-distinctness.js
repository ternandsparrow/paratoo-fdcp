module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-mottle-boundary-distinctnesss',
      'handler': 'lut-soils-mottle-boundary-distinctness.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-mottle-boundary-distinctnesss/:id',
      'handler': 'lut-soils-mottle-boundary-distinctness.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
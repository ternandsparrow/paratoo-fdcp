module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-gully-water-erosion-degrees',
      'handler': 'lut-soils-gully-water-erosion-degree.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-gully-water-erosion-degrees/:id',
      'handler': 'lut-soils-gully-water-erosion-degree.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
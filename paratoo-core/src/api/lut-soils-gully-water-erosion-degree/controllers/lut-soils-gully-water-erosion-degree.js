const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-gully-water-erosion-degree.lut-soils-gully-water-erosion-degree')
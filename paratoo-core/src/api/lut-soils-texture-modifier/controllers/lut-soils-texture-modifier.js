const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-texture-modifier.lut-soils-texture-modifier')
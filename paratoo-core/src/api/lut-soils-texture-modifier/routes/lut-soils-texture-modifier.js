module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-texture-modifiers',
      'handler': 'lut-soils-texture-modifier.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-texture-modifiers/:id',
      'handler': 'lut-soils-texture-modifier.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
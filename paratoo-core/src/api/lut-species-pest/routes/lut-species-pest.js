module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-species-pests',
      'handler': 'lut-species-pest.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-species-pests/:id',
      'handler': 'lut-species-pest.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-species-pest controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-species-pest.lut-species-pest')

'use strict'

/**
 * lut-species-pest service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-species-pest.lut-species-pest')

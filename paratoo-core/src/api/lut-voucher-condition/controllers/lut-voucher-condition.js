const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-voucher-condition.lut-voucher-condition')
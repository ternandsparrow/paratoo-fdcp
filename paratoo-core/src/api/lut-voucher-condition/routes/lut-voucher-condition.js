module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-voucher-conditions',
      'handler': 'lut-voucher-condition.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-voucher-conditions/:id',
      'handler': 'lut-voucher-condition.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
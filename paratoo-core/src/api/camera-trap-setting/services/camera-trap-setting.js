'use strict'

/**
 * camera-trap-setting service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::camera-trap-setting.camera-trap-setting')

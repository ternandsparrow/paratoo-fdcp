module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-segregation-strengths',
      'handler': 'lut-soils-segregation-strength.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-segregation-strengths/:id',
      'handler': 'lut-soils-segregation-strength.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-wave-water-erosion-degree.lut-soils-wave-water-erosion-degree')
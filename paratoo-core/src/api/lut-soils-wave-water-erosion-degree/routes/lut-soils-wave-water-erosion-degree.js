module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-wave-water-erosion-degrees',
      'handler': 'lut-soils-wave-water-erosion-degree.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-wave-water-erosion-degrees/:id',
      'handler': 'lut-soils-wave-water-erosion-degree.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
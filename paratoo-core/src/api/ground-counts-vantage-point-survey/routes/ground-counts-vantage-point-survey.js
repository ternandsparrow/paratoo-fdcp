module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/ground-counts-vantage-point-surveys',
      'handler': 'ground-counts-vantage-point-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/ground-counts-vantage-point-surveys/:id',
      'handler': 'ground-counts-vantage-point-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/ground-counts-vantage-point-surveys',
      'handler': 'ground-counts-vantage-point-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/ground-counts-vantage-point-surveys/:id',
      'handler': 'ground-counts-vantage-point-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/ground-counts-vantage-point-surveys/:id',
      'handler': 'ground-counts-vantage-point-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/ground-counts-vantage-point-surveys/bulk',
      'handler': 'ground-counts-vantage-point-survey.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
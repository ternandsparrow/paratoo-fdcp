'use strict'

/**
 * ground-counts-vantage-point-survey service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::ground-counts-vantage-point-survey.ground-counts-vantage-point-survey')

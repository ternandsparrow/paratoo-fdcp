module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/grassy-weeds-exports',
      'handler': 'grassy-weeds-export.find',
      'config': {
        'middlewares': [
          // 'global::pdp-data-filter'
        ],
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/grassy-weeds-exports/:id',
      'handler': 'grassy-weeds-export.findOne',
      'config': {
        'middlewares': [
          // 'global::pdp-data-filter'
        ],
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/grassy-weeds-exports',
      'handler': 'grassy-weeds-export.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/grassy-weeds-exports/:id',
      'handler': 'grassy-weeds-export.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/grassy-weeds-exports/:id',
      'handler': 'grassy-weeds-export.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
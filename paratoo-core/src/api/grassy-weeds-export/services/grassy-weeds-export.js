'use strict'

/**
 * grassy-weeds-export service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::grassy-weeds-export.grassy-weeds-export')

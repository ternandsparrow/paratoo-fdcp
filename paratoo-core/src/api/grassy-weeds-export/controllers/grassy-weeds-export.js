'use strict'

/**
 * grassy-weeds-export controller
 */

const { createCoreController } = require('@strapi/strapi').factories
const converter = require('json-2-csv')

module.exports = createCoreController('api::grassy-weeds-export.grassy-weeds-export', ({ strapi }) => ({
  async findOne(ctx) {
    const fieldSurvey = await strapi.db.query('api::grassy-weeds-survey.grassy-weeds-survey')
      .findOne({
        populate: {
          survey_type: true,
          desktop_setup: true,
        },
        where: { id: ctx.params.id },
      })
    console.log('fieldSurvey:', JSON.stringify(fieldSurvey, null, 2))

    //user query engine rather than entity service as we can do `findMany` with `where`
    const fieldObservations = await strapi.db.query('api::grassy-weeds-observation.grassy-weeds-observation')
      .findMany({
        populate: {
          survey: {
            populate: {
              id: true,
              survey_name: true,
              observers: '*',
              observer_aircraft_position: true,
            }
          },
          location: true,
          species: true,
          density_category: true,
        },
        where: {
          survey: {
            id: fieldSurvey.id
          },
        },
      })
    console.log('fieldObservations:', JSON.stringify(fieldObservations, null, 2))

    const checkIfArchived = await strapi.db.query('api::grassy-weeds-survey.grassy-weeds-survey').findOne({
      where: { id: fieldSurvey.id },
      select: ['archived'] 
    });

    if (checkIfArchived && checkIfArchived.archived === false) {
      const updatedSurvey = await strapi.db.query('api::grassy-weeds-survey.grassy-weeds-survey').update({
        where: { id: fieldSurvey.id },
        data: { archived: true }
      });
      console.log('updatedSurvey:', JSON.stringify(updatedSurvey, null, 2))
    }


    //TODO fields:
    //  - speed
    //  - heading
    //  - survey port/starboard LUT
    const mappedFieldObservations = fieldObservations.map((observation) => {
      const parsedDate = new Date(observation.date_time)
      //case all these to string to be able to `padStart`
      const utcDate = String(parsedDate.getUTCDate()).padStart(2, '0')
      //need to add 1 to month as it indexes from 0
      const utcMonth = String(parsedDate.getUTCMonth()+1).padStart(2, '0')
      //slice to get just 2-digit post-fix for year
      const utcYear = String(parsedDate.getUTCFullYear()).slice(2, 4)
      const utcHr = String(parsedDate.getUTCHours()).padStart(2, '0')
      const utcMin = String(parsedDate.getUTCMinutes()).padStart(2, '0')
      const utcSec = String(parsedDate.getUTCSeconds()).padStart(2, '0')
      return {
        survey_id: observation.survey.id,
        desktop_survey_name: fieldSurvey?.desktop_setup?.survey_name || 'not collected',
        field_survey_name: observation.survey.field_survey_name,
        observation_number: observation.observation_number,
        lat: observation.location.lat,
        lng: observation.location.lng,
        species: observation.species.label,
        density_category: observation.density_category.label,
        speed: observation.speed,
        heading: observation.heading,
        accuracy: observation.accuracy,
        utc_date: `${utcDate}${utcMonth}${utcYear}`,
        utc_time: `${utcHr}${utcMin}${utcSec}`,
        observer: observation.survey.field_survey_observer,
        observer_aircraft_position: observation.survey.observer_aircraft_position.label,
      }
    })
    const csv = await converter.json2csv(mappedFieldObservations)

    ctx.response.attachment('output.csv')
    ctx.response.status = 200
    ctx.body = csv

    return ctx
  },
}))

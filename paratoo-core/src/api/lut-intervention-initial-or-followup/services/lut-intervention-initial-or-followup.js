'use strict'

/**
 * lut-intervention-initial-or-followup service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-initial-or-followup.lut-intervention-initial-or-followup')

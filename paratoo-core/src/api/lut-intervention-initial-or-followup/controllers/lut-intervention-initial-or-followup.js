'use strict'

/**
 *  lut-intervention-initial-or-followup controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-initial-or-followup.lut-intervention-initial-or-followup')

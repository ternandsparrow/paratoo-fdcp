module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-initial-or-followups',
      'handler': 'lut-intervention-initial-or-followup.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-initial-or-followups/:id',
      'handler': 'lut-intervention-initial-or-followup.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
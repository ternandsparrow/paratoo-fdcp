const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-cover-transect-start-point.lut-cover-transect-start-point')
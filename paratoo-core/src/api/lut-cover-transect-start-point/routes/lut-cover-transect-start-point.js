module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-cover-transect-start-points',
      'handler': 'lut-cover-transect-start-point.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-cover-transect-start-points/:id',
      'handler': 'lut-cover-transect-start-point.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
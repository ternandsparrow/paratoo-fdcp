module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-mottle-colours',
      'handler': 'lut-soils-mottle-colour.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-mottle-colours/:id',
      'handler': 'lut-soils-mottle-colour.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
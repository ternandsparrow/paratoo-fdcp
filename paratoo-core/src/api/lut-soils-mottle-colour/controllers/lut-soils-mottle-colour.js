const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-mottle-colour.lut-soils-mottle-colour')
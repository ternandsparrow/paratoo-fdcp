module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-drone-sampling-rates',
      'handler': 'lut-drone-sampling-rate.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-drone-sampling-rates/:id',
      'handler': 'lut-drone-sampling-rate.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-drone-sampling-rate controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-drone-sampling-rate.lut-drone-sampling-rate')

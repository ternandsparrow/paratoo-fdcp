'use strict'

/**
 * lut-drone-sampling-rate service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-drone-sampling-rate.lut-drone-sampling-rate')

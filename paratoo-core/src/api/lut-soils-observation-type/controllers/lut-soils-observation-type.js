const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-observation-type.lut-soils-observation-type')
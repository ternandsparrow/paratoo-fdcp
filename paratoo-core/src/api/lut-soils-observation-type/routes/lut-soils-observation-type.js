module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-observation-types',
      'handler': 'lut-soils-observation-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-observation-types/:id',
      'handler': 'lut-soils-observation-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
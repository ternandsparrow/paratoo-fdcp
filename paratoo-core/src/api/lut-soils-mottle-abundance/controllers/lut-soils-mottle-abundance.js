const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-mottle-abundance.lut-soils-mottle-abundance')
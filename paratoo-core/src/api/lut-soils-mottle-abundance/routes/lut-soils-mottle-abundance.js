module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-mottle-abundances',
      'handler': 'lut-soils-mottle-abundance.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-mottle-abundances/:id',
      'handler': 'lut-soils-mottle-abundance.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
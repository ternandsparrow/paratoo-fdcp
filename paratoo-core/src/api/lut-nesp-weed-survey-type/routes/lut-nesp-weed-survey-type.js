module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-nesp-weed-survey-types',
      'handler': 'lut-nesp-weed-survey-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-nesp-weed-survey-types/:id',
      'handler': 'lut-nesp-weed-survey-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
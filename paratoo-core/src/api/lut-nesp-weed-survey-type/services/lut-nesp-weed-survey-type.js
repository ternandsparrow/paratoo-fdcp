'use strict'

/**
 * lut-nesp-weed-survey-type service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-nesp-weed-survey-type.lut-nesp-weed-survey-type')

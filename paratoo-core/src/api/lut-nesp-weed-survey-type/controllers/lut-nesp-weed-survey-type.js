'use strict'

/**
 * lut-nesp-weed-survey-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-nesp-weed-survey-type.lut-nesp-weed-survey-type')

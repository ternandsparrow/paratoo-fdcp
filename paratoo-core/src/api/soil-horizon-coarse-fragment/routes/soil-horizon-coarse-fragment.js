module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/soil-horizon-coarse-fragments',
      'handler': 'soil-horizon-coarse-fragment.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/soil-horizon-coarse-fragments/:id',
      'handler': 'soil-horizon-coarse-fragment.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/soil-horizon-coarse-fragments',
      'handler': 'soil-horizon-coarse-fragment.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/soil-horizon-coarse-fragments/:id',
      'handler': 'soil-horizon-coarse-fragment.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/soil-horizon-coarse-fragments/:id',
      'handler': 'soil-horizon-coarse-fragment.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
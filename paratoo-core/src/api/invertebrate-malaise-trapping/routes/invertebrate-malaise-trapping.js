module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/invertebrate-malaise-trappings',
      'handler': 'invertebrate-malaise-trapping.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/invertebrate-malaise-trappings/:id',
      'handler': 'invertebrate-malaise-trapping.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/invertebrate-malaise-trappings',
      'handler': 'invertebrate-malaise-trapping.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/invertebrate-malaise-trappings/:id',
      'handler': 'invertebrate-malaise-trapping.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/invertebrate-malaise-trappings/:id',
      'handler': 'invertebrate-malaise-trapping.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/invertebrate-malaise-trappings/bulk',
      'handler': 'invertebrate-malaise-trapping.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
'use strict'

/**
 * invertebrate-malaise-trapping service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::invertebrate-malaise-trapping.invertebrate-malaise-trapping')

'use strict'

/**
 * invertebrate-active-search-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::invertebrate-active-search-survey.invertebrate-active-search-survey')

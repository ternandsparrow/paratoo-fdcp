'use strict'

/**
 * lut-aircraft-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-aircraft-type.lut-aircraft-type')

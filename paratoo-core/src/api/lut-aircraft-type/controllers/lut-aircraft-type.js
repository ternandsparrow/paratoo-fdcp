'use strict'

/**
 *  lut-aircraft-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-aircraft-type.lut-aircraft-type')

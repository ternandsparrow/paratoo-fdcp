module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-aircraft-types',
      'handler': 'lut-aircraft-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-aircraft-types/:id',
      'handler': 'lut-aircraft-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
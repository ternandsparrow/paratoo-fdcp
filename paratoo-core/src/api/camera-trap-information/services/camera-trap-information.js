'use strict'

/**
 * camera-trap-information service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::camera-trap-information.camera-trap-information')

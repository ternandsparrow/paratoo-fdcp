const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-wind-erosion-degree.lut-soils-wind-erosion-degree')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-wind-erosion-degrees',
      'handler': 'lut-soils-wind-erosion-degree.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-wind-erosion-degrees/:id',
      'handler': 'lut-soils-wind-erosion-degree.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
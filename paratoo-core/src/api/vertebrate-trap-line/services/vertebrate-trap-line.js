'use strict'

/**
 * vertebrate-trap-line service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::vertebrate-trap-line.vertebrate-trap-line')

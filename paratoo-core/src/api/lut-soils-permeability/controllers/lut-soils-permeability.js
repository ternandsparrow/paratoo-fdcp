const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-permeability.lut-soils-permeability')
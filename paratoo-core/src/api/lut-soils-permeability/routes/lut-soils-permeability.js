module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-permeabilities',
      'handler': 'lut-soils-permeability.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-permeabilities/:id',
      'handler': 'lut-soils-permeability.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
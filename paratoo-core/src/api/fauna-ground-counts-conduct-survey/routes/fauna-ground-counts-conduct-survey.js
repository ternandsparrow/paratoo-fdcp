module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/fauna-ground-counts-conduct-surveys',
      'handler': 'fauna-ground-counts-conduct-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/fauna-ground-counts-conduct-surveys/:id',
      'handler': 'fauna-ground-counts-conduct-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/fauna-ground-counts-conduct-surveys',
      'handler': 'fauna-ground-counts-conduct-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/fauna-ground-counts-conduct-surveys/:id',
      'handler': 'fauna-ground-counts-conduct-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/fauna-ground-counts-conduct-surveys/:id',
      'handler': 'fauna-ground-counts-conduct-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * fauna-ground-counts-conduct-survey service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::fauna-ground-counts-conduct-survey.fauna-ground-counts-conduct-survey')

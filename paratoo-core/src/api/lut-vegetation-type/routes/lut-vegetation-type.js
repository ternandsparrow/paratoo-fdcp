module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vegetation-types',
      'handler': 'lut-vegetation-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vegetation-types/:id',
      'handler': 'lut-vegetation-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vegetation-type.lut-vegetation-type')
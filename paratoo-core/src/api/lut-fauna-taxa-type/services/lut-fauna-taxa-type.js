'use strict'

/**
 * lut-fauna-taxa-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-fauna-taxa-type.lut-fauna-taxa-type')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-taxa-types',
      'handler': 'lut-fauna-taxa-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-taxa-types/:id',
      'handler': 'lut-fauna-taxa-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
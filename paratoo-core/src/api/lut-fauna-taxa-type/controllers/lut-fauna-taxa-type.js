'use strict'

/**
 *  lut-fauna-taxa-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-taxa-type.lut-fauna-taxa-type')

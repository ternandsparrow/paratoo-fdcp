'use strict'

/**
 * sign-based-track-station-log service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::sign-based-track-station-log.sign-based-track-station-log')

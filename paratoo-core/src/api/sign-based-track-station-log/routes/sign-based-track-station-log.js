module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/sign-based-track-station-logs',
      'handler': 'sign-based-track-station-log.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/sign-based-track-station-logs/:id',
      'handler': 'sign-based-track-station-log.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/sign-based-track-station-logs',
      'handler': 'sign-based-track-station-log.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/sign-based-track-station-logs/:id',
      'handler': 'sign-based-track-station-log.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/sign-based-track-station-logs/:id',
      'handler': 'sign-based-track-station-log.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-type-of-physical-damage controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-type-of-physical-damage.lut-type-of-physical-damage')

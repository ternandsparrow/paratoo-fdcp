'use strict'

/**
 * lut-type-of-physical-damage service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-type-of-physical-damage.lut-type-of-physical-damage')

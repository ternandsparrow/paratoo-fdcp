module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-type-of-physical-damages',
      'handler': 'lut-type-of-physical-damage.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-type-of-physical-damages/:id',
      'handler': 'lut-type-of-physical-damage.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
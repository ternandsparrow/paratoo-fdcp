module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/within-plot-belt-transects',
      'handler': 'within-plot-belt-transect.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/within-plot-belt-transects/:id',
      'handler': 'within-plot-belt-transect.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/within-plot-belt-transects',
      'handler': 'within-plot-belt-transect.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/within-plot-belt-transects/:id',
      'handler': 'within-plot-belt-transect.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/within-plot-belt-transects/:id',
      'handler': 'within-plot-belt-transect.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * within-plot-belt-transect service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::within-plot-belt-transect.within-plot-belt-transect')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-nesp-weed-aircraft-positions',
      'handler': 'lut-nesp-weed-aircraft-position.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-nesp-weed-aircraft-positions/:id',
      'handler': 'lut-nesp-weed-aircraft-position.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
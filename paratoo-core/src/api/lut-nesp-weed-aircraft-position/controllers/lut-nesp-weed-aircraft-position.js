'use strict'

/**
 * lut-nesp-weed-aircraft-position controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-nesp-weed-aircraft-position.lut-nesp-weed-aircraft-position')

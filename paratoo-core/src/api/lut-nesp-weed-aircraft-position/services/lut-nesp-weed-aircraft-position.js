'use strict'

/**
 * lut-nesp-weed-aircraft-position service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-nesp-weed-aircraft-position.lut-nesp-weed-aircraft-position')

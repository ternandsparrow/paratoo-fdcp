module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-tecs',
      'handler': 'lut-tec.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-tecs/:id',
      'handler': 'lut-tec.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
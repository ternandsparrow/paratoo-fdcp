const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-tec.lut-tec')
'use strict'

/**
 * lut-cat-coat-colour service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-cat-coat-colour.lut-cat-coat-colour')

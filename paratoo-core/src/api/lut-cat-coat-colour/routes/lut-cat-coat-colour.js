module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-cat-coat-colours',
      'handler': 'lut-cat-coat-colour.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-cat-coat-colours/:id',
      'handler': 'lut-cat-coat-colour.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
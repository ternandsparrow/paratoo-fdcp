'use strict'

/**
 *  lut-cat-coat-colour controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-cat-coat-colour.lut-cat-coat-colour')

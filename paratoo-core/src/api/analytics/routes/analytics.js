module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/analytics',
      handler: 'analytics.find',
      config: {
        policies: ['global::is-validated', 'global::projectMembershipEnforcer'],
      },
    },
    {
      method: 'POST',
      path: '/analytics/heartbeat',
      handler: 'analytics.heartbeat',
      config: {
        policies: [],
      },
    }
  ],
}

'use strict'

const { isEmpty } = require('lodash')

/**
 * analytics controller
 */

const { createCoreController } = require('@strapi/strapi').factories
module.exports = createCoreController(
  'api::analytics.analytics',
  ({ strapi }) => ({
    async heartbeat(ctx) {
      let info = ctx.request.body['info']
      const coreBuildInfo = {
        Build: process.env.NODE_ENV,
        Version: process.env.VERSION,
        GitHash: process.env.GIT_HASH,
      }

      // skip saving if no payload.
      // its better to return core build info instead of returning only 'ok' or 'success'.
      if (isEmpty(info)) return coreBuildInfo

      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      info = {
        // core build information
        coreBuildInfo: coreBuildInfo,
        // information from web app
        ...info,
      }
      await strapi.entityService
        .create('api::analytics.analytics', {
          data: {
            info: info,
          },
        })
        .catch((err) => {
          helpers.paratooWarnHandler(
            `Failed to store webApp analytics, reason: ${JSON.stringify(err)}`,
          )
        })
      if (info.webPushEndpoint) {
        coreBuildInfo.webPushSubscriptionStatus =
          await getPushSubscriptionStatus(info.webPushEndpoint)
      }

      return coreBuildInfo
    },
  }),
)

async function getPushSubscriptionStatus(endpoint) {
  const webPushSubscriptionModelName = 'push-notification-subscription'
  const uid = `api::${webPushSubscriptionModelName}.${webPushSubscriptionModelName}`
  const subscriptionStatus = await strapi.db.query(uid).findOne({
    where: { endpoint },
  })
  return Boolean(subscriptionStatus)
}

'use strict'

/**
 * analytics service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::analytics.analytics')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-erosion-treatment-types',
      'handler': 'lut-intervention-erosion-treatment-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-erosion-treatment-types/:id',
      'handler': 'lut-intervention-erosion-treatment-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-erosion-treatment-type.lut-intervention-erosion-treatment-type')
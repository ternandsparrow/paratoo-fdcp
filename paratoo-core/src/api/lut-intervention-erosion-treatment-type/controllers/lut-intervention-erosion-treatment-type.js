const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-erosion-treatment-type.lut-intervention-erosion-treatment-type')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-invertebrate-malaise-trapping-voucher-types',
      'handler': 'lut-invertebrate-malaise-trapping-voucher-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-invertebrate-malaise-trapping-voucher-types/:id',
      'handler': 'lut-invertebrate-malaise-trapping-voucher-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
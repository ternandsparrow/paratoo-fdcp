const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-invertebrate-malaise-trapping-voucher-type.lut-invertebrate-malaise-trapping-voucher-type')
const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-invertebrate-malaise-trapping-voucher-type.lut-invertebrate-malaise-trapping-voucher-type')
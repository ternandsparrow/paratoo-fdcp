module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/hapd-active-search-observations',
      'handler': 'hapd-active-search-observation.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/hapd-active-search-observations/:id',
      'handler': 'hapd-active-search-observation.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/hapd-active-search-observations',
      'handler': 'hapd-active-search-observation.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/hapd-active-search-observations/:id',
      'handler': 'hapd-active-search-observation.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/hapd-active-search-observations/:id',
      'handler': 'hapd-active-search-observation.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
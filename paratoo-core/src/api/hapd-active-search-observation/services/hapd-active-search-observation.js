'use strict'

/**
 * hapd-active-search-observation service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::hapd-active-search-observation.hapd-active-search-observation')

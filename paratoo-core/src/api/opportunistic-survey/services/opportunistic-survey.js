'use strict'

/**
 * opportunistic-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::opportunistic-survey.opportunistic-survey')

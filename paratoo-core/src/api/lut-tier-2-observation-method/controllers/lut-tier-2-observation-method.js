const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-tier-2-observation-method.lut-tier-2-observation-method')
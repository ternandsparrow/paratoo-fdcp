module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-tier-2-observation-methods',
      'handler': 'lut-tier-2-observation-method.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-tier-2-observation-methods/:id',
      'handler': 'lut-tier-2-observation-method.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * malaise-trap-vouchering service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::malaise-trap-vouchering.malaise-trap-vouchering')

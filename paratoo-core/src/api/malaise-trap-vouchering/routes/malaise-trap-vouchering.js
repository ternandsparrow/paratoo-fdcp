module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/malaise-trap-voucherings',
      'handler': 'malaise-trap-vouchering.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/malaise-trap-voucherings/:id',
      'handler': 'malaise-trap-vouchering.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/malaise-trap-voucherings',
      'handler': 'malaise-trap-vouchering.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/malaise-trap-voucherings/:id',
      'handler': 'malaise-trap-vouchering.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/malaise-trap-voucherings/:id',
      'handler': 'malaise-trap-vouchering.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
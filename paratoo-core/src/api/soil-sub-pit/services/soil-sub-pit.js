'use strict'

/**
 * soil-sub-pit service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-sub-pit.soil-sub-pit')

'use strict'

/**
 * dev-sandbox-general-multi-entry service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::dev-sandbox-general-multi-entry.dev-sandbox-general-multi-entry')

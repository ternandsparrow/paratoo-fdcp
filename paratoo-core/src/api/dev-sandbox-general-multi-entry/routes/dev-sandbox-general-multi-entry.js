module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/dev-sandbox-general-multi-entries',
      'handler': 'dev-sandbox-general-multi-entry.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/dev-sandbox-general-multi-entries/:id',
      'handler': 'dev-sandbox-general-multi-entry.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/dev-sandbox-general-multi-entries',
      'handler': 'dev-sandbox-general-multi-entry.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/dev-sandbox-general-multi-entries/:id',
      'handler': 'dev-sandbox-general-multi-entry.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/dev-sandbox-general-multi-entries/:id',
      'handler': 'dev-sandbox-general-multi-entry.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
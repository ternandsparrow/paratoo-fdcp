module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-field-dispersion-scores',
      'handler': 'lut-soils-field-dispersion-score.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-field-dispersion-scores/:id',
      'handler': 'lut-soils-field-dispersion-score.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
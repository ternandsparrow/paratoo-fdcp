const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-field-dispersion-score.lut-soils-field-dispersion-score')
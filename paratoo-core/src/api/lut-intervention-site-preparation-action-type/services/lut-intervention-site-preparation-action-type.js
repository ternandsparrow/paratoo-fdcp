const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-site-preparation-action-type.lut-intervention-site-preparation-action-type')
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-site-preparation-action-type.lut-intervention-site-preparation-action-type')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-site-preparation-action-types',
      'handler': 'lut-intervention-site-preparation-action-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-site-preparation-action-types/:id',
      'handler': 'lut-intervention-site-preparation-action-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-opportune-voucher-type-tier-1 service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-opportune-voucher-type-tier-1.lut-opportune-voucher-type-tier-1')

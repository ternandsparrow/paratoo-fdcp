module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-opportune-voucher-type-tier-1s',
      'handler': 'lut-opportune-voucher-type-tier-1.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-opportune-voucher-type-tier-1s/:id',
      'handler': 'lut-opportune-voucher-type-tier-1.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-opportune-voucher-type-tier-1 controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-opportune-voucher-type-tier-1.lut-opportune-voucher-type-tier-1')

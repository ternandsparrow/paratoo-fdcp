module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/sign-based-track-station-setups',
      'handler': 'sign-based-track-station-setup.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/sign-based-track-station-setups/:id',
      'handler': 'sign-based-track-station-setup.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/sign-based-track-station-setups',
      'handler': 'sign-based-track-station-setup.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/sign-based-track-station-setups/:id',
      'handler': 'sign-based-track-station-setup.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/sign-based-track-station-setups/:id',
      'handler': 'sign-based-track-station-setup.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/sign-based-track-station-setups/bulk',
      'handler': 'sign-based-track-station-setup.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
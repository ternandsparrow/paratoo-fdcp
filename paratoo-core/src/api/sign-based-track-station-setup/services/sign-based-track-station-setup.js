'use strict'

/**
 * sign-based-track-station-setup service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::sign-based-track-station-setup.sign-based-track-station-setup')

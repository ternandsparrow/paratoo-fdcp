module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-disease-management-or-treatment-types',
      'handler': 'lut-intervention-disease-management-or-treatment-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-disease-management-or-treatment-types/:id',
      'handler': 'lut-intervention-disease-management-or-treatment-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
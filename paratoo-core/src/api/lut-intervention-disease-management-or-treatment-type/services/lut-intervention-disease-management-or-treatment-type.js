const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-disease-management-or-treatment-type.lut-intervention-disease-management-or-treatment-type')
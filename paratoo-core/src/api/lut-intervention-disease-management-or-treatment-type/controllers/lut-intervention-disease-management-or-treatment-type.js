const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-disease-management-or-treatment-type.lut-intervention-disease-management-or-treatment-type')
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController(
  'api::soil-bulk-density-survey.soil-bulk-density-survey',
  ({ strapi }) => ({
    /* Generic find controller with pdp data filter */
    async find(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      
      const authToken = ctx.request.headers['authorization']
      const modelName = ctx.state.route.info.apiName
      const tokenStatus = await helpers.apiTokenCheck(authToken)
      const useDefaultController = ctx.request.query['use-default'] === 'true'
      
      // if we need to use default controller (use magic jwt and use-default flag)
      if (tokenStatus?.type == 'full-access' && useDefaultController) {
        helpers.paratooDebugMsg(`Default controller is used to populate model: ${modelName}`)
        return await super.find(ctx)
      }
      return await helpers.pdpDataFilter(
        modelName,
        authToken,
        ctx.request.query,
        tokenStatus,
      )
    },

    /**
     * Business logic endpoint for performing a soil bulk density survey - uploads a collection of plt soil description
     * surveys and their observations)
     *
     * @param {Object} ctx Koa context
     * @returns {Array} successfully-created array of collections
     */
    async bulk(ctx) {
      const obsModelName = 'soil-bulk-density-sample'
      const config = {
        ctx: ctx,
        surveyModelName: 'soil-bulk-density-survey',
      }
      if (obsModelName in ctx.request.body.data.collections[0]) {
        config.obsModelName = obsModelName
      }
      
      return strapi
        .service('api::paratoo-helper.paratoo-helper')
        .genericBulk(config)
    },
  }),
)

'use strict'

/**
 * soil-bulk-density-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-bulk-density-survey.soil-bulk-density-survey')

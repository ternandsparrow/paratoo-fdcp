module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-surveyor-types',
      'handler': 'lut-surveyor-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-surveyor-types/:id',
      'handler': 'lut-surveyor-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
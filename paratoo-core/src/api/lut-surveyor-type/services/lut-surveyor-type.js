'use strict'

/**
 * lut-surveyor-type service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-surveyor-type.lut-surveyor-type')

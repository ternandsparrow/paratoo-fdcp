'use strict'

/**
 * lut-surveyor-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-surveyor-type.lut-surveyor-type')

'use strict'

/**
 * fauna-ground-counts-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::fauna-ground-counts-survey.fauna-ground-counts-survey')

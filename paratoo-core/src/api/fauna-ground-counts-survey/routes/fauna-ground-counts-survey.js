module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/fauna-ground-counts-surveys',
      'handler': 'fauna-ground-counts-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/fauna-ground-counts-surveys/:id',
      'handler': 'fauna-ground-counts-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/fauna-ground-counts-surveys',
      'handler': 'fauna-ground-counts-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/fauna-ground-counts-surveys/:id',
      'handler': 'fauna-ground-counts-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/fauna-ground-counts-surveys/:id',
      'handler': 'fauna-ground-counts-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/fauna-ground-counts-surveys/bulk',
      'handler': 'fauna-ground-counts-survey.bulk',
      'config': {
        'policies': [
          'global::fix-corrupted-data-fgct',
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-plot-points.lut-plot-points')
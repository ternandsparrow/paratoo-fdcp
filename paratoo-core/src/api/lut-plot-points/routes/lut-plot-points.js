module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-plot-points',
      'handler': 'lut-plot-points.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-plot-points/:id',
      'handler': 'lut-plot-points.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-basal-area-factors',
      'handler': 'lut-basal-area-factor.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-basal-area-factors/:id',
      'handler': 'lut-basal-area-factor.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-basal-area-factor.lut-basal-area-factor')
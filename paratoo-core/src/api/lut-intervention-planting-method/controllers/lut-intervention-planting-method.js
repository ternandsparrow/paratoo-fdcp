const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-planting-method.lut-intervention-planting-method')
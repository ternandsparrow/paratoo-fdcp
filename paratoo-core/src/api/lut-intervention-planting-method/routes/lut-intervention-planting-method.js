module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-planting-methods',
      'handler': 'lut-intervention-planting-method.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-planting-methods/:id',
      'handler': 'lut-intervention-planting-method.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
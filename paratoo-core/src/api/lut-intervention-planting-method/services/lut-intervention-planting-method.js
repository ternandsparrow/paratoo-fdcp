const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-planting-method.lut-intervention-planting-method')
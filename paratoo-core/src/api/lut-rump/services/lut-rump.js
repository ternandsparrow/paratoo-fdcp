'use strict'

/**
 * lut-rump service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-rump.lut-rump')

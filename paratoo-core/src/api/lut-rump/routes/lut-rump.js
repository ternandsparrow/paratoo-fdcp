module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-rumps',
      'handler': 'lut-rump.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-rumps/:id',
      'handler': 'lut-rump.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
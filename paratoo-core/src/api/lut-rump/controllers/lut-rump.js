'use strict'

/**
 *  lut-rump controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-rump.lut-rump')

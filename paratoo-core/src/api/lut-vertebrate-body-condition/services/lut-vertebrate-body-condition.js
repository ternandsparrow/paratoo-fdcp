'use strict'

/**
 * lut-vertebrate-body-condition service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-body-condition.lut-vertebrate-body-condition')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-body-conditions',
      'handler': 'lut-vertebrate-body-condition.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-body-conditions/:id',
      'handler': 'lut-vertebrate-body-condition.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
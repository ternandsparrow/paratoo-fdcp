'use strict'

/**
 *  lut-vertebrate-body-condition controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-body-condition.lut-vertebrate-body-condition')

'use strict'

/**
 * soil-landform-element service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-landform-element.soil-landform-element')

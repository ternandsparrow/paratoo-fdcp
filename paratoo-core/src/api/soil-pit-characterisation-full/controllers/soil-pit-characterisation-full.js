'use strict'

/**
 *  soil-pit-characterisation-full controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController(
  'api::soil-pit-characterisation-full.soil-pit-characterisation-full',
  ({ strapi }) => ({
    /* Generic find controller with pdp data filter */
    async find(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      
      const authToken = ctx.request.headers['authorization']
      const modelName = ctx.state.route.info.apiName
      const tokenStatus = await helpers.apiTokenCheck(authToken)
      const useDefaultController = ctx.request.query['use-default'] === 'true'
      
      // if we need to use default controller (use magic jwt and use-default flag)
      if (tokenStatus?.type == 'full-access' && useDefaultController) {
        helpers.paratooDebugMsg(`Default controller is used to populate model: ${modelName}`)
        return await super.find(ctx)
      }
      return await helpers.pdpDataFilter(
        modelName,
        authToken,
        ctx.request.query,
        tokenStatus,
      )
    },

    async bulk(ctx) {
      const fieldObsModelName = [
        'soil-landform-element',
        'soil-land-surface-phenomena',
        'microrelief-observation',
        'erosion-observation',
        'surface-coarse-fragments-observation',
        'rock-outcrop-observation',
        'soil-pit-observation',
        'soil-horizon-observation',
        'soil-asc',
        'soil-classification',
      ]

      const labObsModelName = ['soil-horizon-sample']

      const keys = Object.keys(ctx.request.body.data.collections[0])

      let obsModelName
      if (fieldObsModelName.every((model) => keys.includes(model))) {
        obsModelName = fieldObsModelName
      } else {
        obsModelName = labObsModelName
      }

      return strapi.service('api::paratoo-helper.paratoo-helper').genericBulk({
        ctx: ctx,
        surveyModelName: 'soil-pit-characterisation-full',
        obsModelName,
        childObservationModelName: [
          'soil-horizon-mottle',
          'soil-horizon-coarse-fragment',
          'soil-horizon-structure',
          'soil-horizon-segregation',
          'soil-horizon-void',
          'soil-horizon-pan',
          'soil-horizon-cutan',
          'soil-horizon-root',
          'soil-horizon-consistence',
        ],
      })
    },
  }),
)

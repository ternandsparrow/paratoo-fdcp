'use strict'

/**
 * soil-pit-characterisation-full service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-pit-characterisation-full.soil-pit-characterisation-full')

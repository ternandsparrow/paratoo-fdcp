module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/soil-pit-characterisation-fulls',
      'handler': 'soil-pit-characterisation-full.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/soil-pit-characterisation-fulls/:id',
      'handler': 'soil-pit-characterisation-full.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/soil-pit-characterisation-fulls',
      'handler': 'soil-pit-characterisation-full.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/soil-pit-characterisation-fulls/:id',
      'handler': 'soil-pit-characterisation-full.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/soil-pit-characterisation-fulls/:id',
      'handler': 'soil-pit-characterisation-full.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/soil-pit-characterisation-fulls/bulk',
      'handler': 'soil-pit-characterisation-full.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-tracking-substrate controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-tracking-substrate.lut-tracking-substrate')

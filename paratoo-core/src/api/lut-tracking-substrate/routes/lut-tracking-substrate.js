module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-tracking-substrates',
      'handler': 'lut-tracking-substrate.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-tracking-substrates/:id',
      'handler': 'lut-tracking-substrate.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
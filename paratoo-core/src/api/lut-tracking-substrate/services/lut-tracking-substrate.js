'use strict'

/**
 * lut-tracking-substrate service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-tracking-substrate.lut-tracking-substrate')

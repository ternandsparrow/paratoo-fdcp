'use strict'

/**
 * lut-lure-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-lure-type.lut-lure-type')

'use strict'

/**
 *  lut-lure-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-lure-type.lut-lure-type')

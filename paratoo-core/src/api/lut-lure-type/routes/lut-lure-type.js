module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-lure-types',
      'handler': 'lut-lure-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-lure-types/:id',
      'handler': 'lut-lure-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
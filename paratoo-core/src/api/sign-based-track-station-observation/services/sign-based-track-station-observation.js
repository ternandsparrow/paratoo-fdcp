'use strict'

/**
 * sign-based-track-station-observation service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::sign-based-track-station-observation.sign-based-track-station-observation')

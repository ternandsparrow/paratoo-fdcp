'use strict'

/**
 * lut-condition-human-induced-damage service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-condition-human-induced-damage.lut-condition-human-induced-damage')

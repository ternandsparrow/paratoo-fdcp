'use strict'

/**
 *  lut-condition-human-induced-damage controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-condition-human-induced-damage.lut-condition-human-induced-damage')

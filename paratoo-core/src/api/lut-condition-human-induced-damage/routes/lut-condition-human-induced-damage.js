module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-condition-human-induced-damages',
      'handler': 'lut-condition-human-induced-damage.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-condition-human-induced-damages/:id',
      'handler': 'lut-condition-human-induced-damage.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
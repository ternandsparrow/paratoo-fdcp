module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-confidence-levels',
      'handler': 'lut-soils-confidence-level.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-confidence-levels/:id',
      'handler': 'lut-soils-confidence-level.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-confidence-level.lut-soils-confidence-level')
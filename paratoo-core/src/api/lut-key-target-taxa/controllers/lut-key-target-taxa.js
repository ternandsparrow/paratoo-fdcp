'use strict'

/**
 * lut-key-target-taxa controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-key-target-taxa.lut-key-target-taxa')

'use strict'

/**
 * lut-key-target-taxa service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-key-target-taxa.lut-key-target-taxa')

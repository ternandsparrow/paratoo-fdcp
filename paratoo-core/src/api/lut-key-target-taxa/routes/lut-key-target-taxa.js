module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-key-target-taxas',
      'handler': 'lut-key-target-taxa.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-key-target-taxas/:id',
      'handler': 'lut-key-target-taxa.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
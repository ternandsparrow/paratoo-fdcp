'use strict'

/**
 * soil-classification service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-classification.soil-classification')

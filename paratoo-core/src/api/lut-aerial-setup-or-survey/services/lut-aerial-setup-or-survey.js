'use strict'

/**
 * lut-aerial-setup-or-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-aerial-setup-or-survey.lut-aerial-setup-or-survey')

'use strict'

/**
 *  lut-aerial-setup-or-survey controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-aerial-setup-or-survey.lut-aerial-setup-or-survey')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-aerial-setup-or-surveys',
      'handler': 'lut-aerial-setup-or-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-aerial-setup-or-surveys/:id',
      'handler': 'lut-aerial-setup-or-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
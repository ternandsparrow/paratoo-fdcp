module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-pan-types',
      'handler': 'lut-soils-pan-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-pan-types/:id',
      'handler': 'lut-soils-pan-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
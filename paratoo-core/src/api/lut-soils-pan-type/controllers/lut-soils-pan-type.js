const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-pan-type.lut-soils-pan-type')
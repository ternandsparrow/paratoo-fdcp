const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-direction.lut-direction')
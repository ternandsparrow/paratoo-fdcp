module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-directions',
      'handler': 'lut-direction.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-directions/:id',
      'handler': 'lut-direction.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/camera-trap-retrieval-surveys',
      'handler': 'camera-trap-retrieval-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/camera-trap-retrieval-surveys/:id',
      'handler': 'camera-trap-retrieval-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/camera-trap-retrieval-surveys',
      'handler': 'camera-trap-retrieval-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/camera-trap-retrieval-surveys/:id',
      'handler': 'camera-trap-retrieval-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/camera-trap-retrieval-surveys/:id',
      'handler': 'camera-trap-retrieval-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/camera-trap-retrieval-surveys/bulk',
      'handler': 'camera-trap-retrieval-survey.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
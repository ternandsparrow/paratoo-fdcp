'use strict'

/**
 * camera-trap-retrieval-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::camera-trap-retrieval-survey.camera-trap-retrieval-survey')

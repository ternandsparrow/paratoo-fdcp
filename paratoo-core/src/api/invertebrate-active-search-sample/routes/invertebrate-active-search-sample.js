module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/invertebrate-active-search-samples',
      'handler': 'invertebrate-active-search-sample.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/invertebrate-active-search-samples/:id',
      'handler': 'invertebrate-active-search-sample.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/invertebrate-active-search-samples',
      'handler': 'invertebrate-active-search-sample.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/invertebrate-active-search-samples/:id',
      'handler': 'invertebrate-active-search-sample.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/invertebrate-active-search-samples/:id',
      'handler': 'invertebrate-active-search-sample.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * invertebrate-active-search-sample service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::invertebrate-active-search-sample.invertebrate-active-search-sample')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/plot-visits',
      'handler': 'plot-visit.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/plot-visits/:id',
      'handler': 'plot-visit.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/plot-visits',
      'handler': 'plot-visit.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/plot-visits/:id',
      'handler': 'plot-visit.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      },
      'allowMethod': true
    },
    {
      'method': 'DELETE',
      'path': '/plot-visits/:id',
      'handler': 'plot-visit.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
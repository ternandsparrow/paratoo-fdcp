'use strict'

/**
 * observer-detail service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::observer-detail.observer-detail')

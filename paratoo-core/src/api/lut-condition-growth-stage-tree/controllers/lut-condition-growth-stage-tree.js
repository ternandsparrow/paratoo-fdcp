const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-condition-growth-stage-tree.lut-condition-growth-stage-tree')
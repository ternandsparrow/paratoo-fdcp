module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-condition-growth-stage-trees',
      'handler': 'lut-condition-growth-stage-tree.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-condition-growth-stage-trees/:id',
      'handler': 'lut-condition-growth-stage-tree.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-lithology.lut-soils-lithology')
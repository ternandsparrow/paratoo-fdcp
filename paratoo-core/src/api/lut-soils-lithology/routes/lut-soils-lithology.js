module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-lithologies',
      'handler': 'lut-soils-lithology.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-lithologies/:id',
      'handler': 'lut-soils-lithology.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
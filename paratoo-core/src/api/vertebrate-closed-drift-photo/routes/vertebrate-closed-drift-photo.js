module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/vertebrate-closed-drift-photos',
      'handler': 'vertebrate-closed-drift-photo.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/vertebrate-closed-drift-photos/:id',
      'handler': 'vertebrate-closed-drift-photo.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/vertebrate-closed-drift-photos',
      'handler': 'vertebrate-closed-drift-photo.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/vertebrate-closed-drift-photos/:id',
      'handler': 'vertebrate-closed-drift-photo.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/vertebrate-closed-drift-photos/:id',
      'handler': 'vertebrate-closed-drift-photo.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
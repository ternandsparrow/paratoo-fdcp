'use strict'

/**
 * vertebrate-closed-drift-photo service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::vertebrate-closed-drift-photo.vertebrate-closed-drift-photo')

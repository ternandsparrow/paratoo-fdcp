module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-slope-classes',
      'handler': 'lut-slope-class.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-slope-classes/:id',
      'handler': 'lut-slope-class.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
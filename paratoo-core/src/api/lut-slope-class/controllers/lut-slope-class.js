'use strict'

/**
 * lut-slope-class controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-slope-class.lut-slope-class')

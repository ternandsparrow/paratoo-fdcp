'use strict'

/**
 * lut-slope-class service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-slope-class.lut-slope-class')

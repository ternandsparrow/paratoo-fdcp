'use strict'

/**
 * lut-soils-horizon-subdivision controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-horizon-subdivision.lut-soils-horizon-subdivision')

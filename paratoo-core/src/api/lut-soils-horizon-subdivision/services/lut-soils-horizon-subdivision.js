'use strict'

/**
 * lut-soils-horizon-subdivision service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soils-horizon-subdivision.lut-soils-horizon-subdivision')

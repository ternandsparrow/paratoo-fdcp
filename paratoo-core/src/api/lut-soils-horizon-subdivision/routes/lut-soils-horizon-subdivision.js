module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-horizon-subdivisions',
      'handler': 'lut-soils-horizon-subdivision.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-horizon-subdivisions/:id',
      'handler': 'lut-soils-horizon-subdivision.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-preservation-types',
      'handler': 'lut-preservation-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-preservation-types/:id',
      'handler': 'lut-preservation-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
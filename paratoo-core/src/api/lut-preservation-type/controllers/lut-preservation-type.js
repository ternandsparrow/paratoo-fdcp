const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-preservation-type.lut-preservation-type')
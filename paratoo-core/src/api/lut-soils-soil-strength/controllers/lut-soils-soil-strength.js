const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-soil-strength.lut-soils-soil-strength')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-soil-strengths',
      'handler': 'lut-soils-soil-strength.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-soil-strengths/:id',
      'handler': 'lut-soils-soil-strength.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
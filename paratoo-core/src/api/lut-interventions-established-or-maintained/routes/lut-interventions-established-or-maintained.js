module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-interventions-established-or-maintaineds',
      'handler': 'lut-interventions-established-or-maintained.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-interventions-established-or-maintaineds/:id',
      'handler': 'lut-interventions-established-or-maintained.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
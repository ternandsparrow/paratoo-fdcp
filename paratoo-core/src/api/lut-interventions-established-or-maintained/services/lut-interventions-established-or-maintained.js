'use strict'

/**
 * lut-interventions-established-or-maintained service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-interventions-established-or-maintained.lut-interventions-established-or-maintained')

'use strict'

/**
 *  lut-interventions-established-or-maintained controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-interventions-established-or-maintained.lut-interventions-established-or-maintained')

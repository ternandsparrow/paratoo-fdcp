'use strict'

/**
 * camera-trap-reequipping-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::camera-trap-reequipping-survey.camera-trap-reequipping-survey')

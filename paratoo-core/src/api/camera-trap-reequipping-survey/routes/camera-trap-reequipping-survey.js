module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/camera-trap-reequipping-surveys',
      'handler': 'camera-trap-reequipping-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/camera-trap-reequipping-surveys/:id',
      'handler': 'camera-trap-reequipping-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/camera-trap-reequipping-surveys',
      'handler': 'camera-trap-reequipping-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/camera-trap-reequipping-surveys/:id',
      'handler': 'camera-trap-reequipping-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/camera-trap-reequipping-surveys/:id',
      'handler': 'camera-trap-reequipping-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/camera-trap-reequipping-surveys/bulk',
      'handler': 'camera-trap-reequipping-survey.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
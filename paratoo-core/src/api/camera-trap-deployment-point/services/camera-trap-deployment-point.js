'use strict'

/**
 * camera-trap-deployment-point service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::camera-trap-deployment-point.camera-trap-deployment-point')

{
  "kind": "collectionType",
  "collectionName": "camera_trap_deployment_points",
  "info": {
    "singularName": "camera-trap-deployment-point",
    "pluralName": "camera-trap-deployment-points",
    "displayName": "Camera Trap Deployment Point",
    "description": ""
  },
  "options": {
    "draftAndPublish": false
  },
  "attributes": {
    "camera_trap_survey": {
      "type": "relation",
      "relation": "oneToOne",
      "target": "api::camera-trap-deployment-survey.camera-trap-deployment-survey"
    },
    "camera_trap_point_id": {
      "type": "string",
      "required": true,
      "x-paratoo-hint": "Choose a suitable point ID (e.g. CTP001). Consider if deploying paired camera traps at the point (e.g. CTP001A and CTP001B). Consider the name of any permanent camera trap points that are being used.",
      "x-paratoo-rename": "Camera Trap Point ID"
    },
    "deployment_start_date": {
      "type": "date",
      "required": true,
      "x-paratoo-hint": "Used to generate the Deployment ID and to calculate the deployment period upon retrieval."
    },
    "deployment_id": {
      "type": "string",
      "required": true,
      "unique": true,
      "x-paratoo-rename": "Deployment ID",
      "x-paratoo-hint": "Auto-generated from the Camera Trap Point ID and Deployment Start Date"
    },
    "distance_to_closest_point": {
      "type": "integer",
      "required": false,
      "x-paratoo-unit": "m"
    },
    "transect_number": {
      "type": "integer",
      "required": false
    },
    "fauna_plot": {
      "type": "relation",
      "relation": "oneToOne",
      "target": "api::plot-layout.plot-layout",
      "x-paratoo-hint": "Fauna Plot must be defined for the Core plot"
    },
    "target_taxa_types": {
      "type": "component",
      "repeatable": true,
      "component": "camera-trap.target-taxa-types"
    },
    "target_species": {
      "type": "component",
      "repeatable": true,
      "component": "camera-trap.target-species"
    },
    "camera_trap_mount": {
      "type": "component",
      "repeatable": false,
      "component": "custom-lut.custom-lut-camera-trap-mount",
      "required": true
    },
    "camera_location": {
      "type": "component",
      "repeatable": false,
      "component": "location.location",
      "required": true
    },
    "features": {
      "type": "relation",
      "relation": "oneToMany",
      "target": "api::camera-trap-feature.camera-trap-feature",
      "x-paratoo-required": true
    },
    "camera_trap_number": {
      "type": "string",
      "required": true,
      "x-paratoo-hint": "Unique identification number of the camera trap"
    },
    "SD_card_number": {
      "type": "string",
      "required": true,
      "x-paratoo-hint": "Unique identification number of the SD card"
    },
    "camera_trap_information": {
      "type": "relation",
      "relation": "oneToOne",
      "target": "api::camera-trap-information.camera-trap-information",
      "x-paratoo-required": true
    },
    "camera_trap_settings": {
      "type": "relation",
      "relation": "oneToOne",
      "target": "api::camera-trap-setting.camera-trap-setting",
      "x-paratoo-required": true,
      "x-paratoo-rename": "Camera Trap Settings"
    },
    "camera_trap_height": {
      "type": "integer",
      "required": true,
      "x-paratoo-unit": "cm",
      "x-paratoo-hint": "From the surface of the ground to the camera lens"
    },
    "camera_trap_angle": {
      "type": "integer",
      "required": true,
      "x-paratoo-unit": "degrees",
      "minimum": 0,
      "maximum": 90,
      "x-paratoo-hint": "From vertical (i.e. 0°). If using an angle bracket to orientate the camera trap horizontally, record the angle as '90°'."
    },
    "camera_trap_direction": {
      "type": "integer",
      "required": true,
      "x-paratoo-unit": "degrees",
      "minimum": 0,
      "maximum": 360
    },
    "detection_angle": {
      "type": "integer",
      "x-paratoo-unit": "degrees",
      "x-paratoo-hint": "The angle formed by the direction of the camera trap and the predicted path of the target taxa/species.",
      "minimum": 0,
      "maximum": 90
    },
    "slope": {
      "type": "decimal",
      "default": 0,
      "x-paratoo-unit": "degrees",
      "required": false,
      "x-paratoo-hint": "The rise or fall of the land surface. Measured using the degrees scale of a clinometer over a distance of at least 20 m."
    },
    "aspect": {
      "type": "integer",
      "default": 0,
      "x-paratoo-unit": "degrees",
      "required": false,
      "minimum": 0,
      "maximum": 360,
      "x-paratoo-hint": "Direction that a slope faces. Recorded in the downslope direction."
    },
    "camera_trap_photo": {
      "type": "media",
      "multiple": false,
      "required": true,
      "allowedTypes": [
        "images"
      ]
    },
    "habitat": {
      "type": "relation",
      "relation": "oneToOne",
      "target": "api::lut-mvg.lut-mvg",
      "x-paratoo-rename": "Habitat (Major Vegetation Group)"
    },
    "deployment_comments": {
      "type": "text"
    },
    "abis_export_uuid": {
      "type": "string",
      "unique": true
    }
  },
  "initialData": [
    [
      1,
      "CTP001A",
      "2023-03-27",
      "CTP001A-2023-03-27",
      null,
      null,
      null,
      [
        {
          "taxa_type": 3
        }
      ],
      [
        {
          "species": "Platypus (scientific: Platypus Shaw, 1799)"
        },
        {
          "species": "Crocodylus [Genus] (scientific: Crocodylus Laurenti, 1768)"
        }
      ],
      {
        "camera_trap_mount_text": "custom ct mount"
      },
      {
        "lat": -34.972472,
        "lng": 138.639024
      },
      [
        1,
        2
      ],
      "1",
      "1",
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      {
        "habitat_lut": 7
      },
      "notes\n\nnext line"
    ],
    [
      1,
      "CTP001B",
      "2023-03-27",
      "CTP001B-2023-03-27",
      null,
      null,
      null,
      [],
      [
        {
          "species": "Rhinella [Genus] (scientific: Rhinella Fitzinger, 1826)"
        }
      ],
      {
        "camera_trap_mount_lut": 12
      },
      {
        "lat": -34.972604,
        "lng": 138.638648
      },
      [
        3
      ],
      "2",
      "2",
      2,
      2,
      1,
      1,
      1,
      1,
      1,
      1,
      2,
      {
        "habitat_lut": 9
      },
      ""
    ],
    [
      2,
      "CTP001C",
      "2023-03-27",
      "CTP001C-2023-03-27",
      null,
      null,
      null,
      [
        {
          "taxa_type": 3
        }
      ],
      [
        {
          "species": "Platypus (scientific: Platypus Shaw, 1799)"
        },
        {
          "species": "Crocodylus [Genus] (scientific: Crocodylus Laurenti, 1768)"
        }
      ],
      {
        "camera_trap_mount_text": "custom ct mount"
      },
      {
        "lat": -24.449316,
        "lng": 147.126588
      },
      [
        1,
        2
      ],
      "1",
      "1",
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      {
        "habitat_lut": 7
      },
      "notes\n\nnext line"
    ],
    [
      2,
      "CTP001D",
      "2023-03-27",
      "CTP001D-2023-03-27",
      null,
      null,
      null,
      [],
      [
        {
          "species": "Rhinella [Genus] (scientific: Rhinella Fitzinger, 1826)"
        }
      ],
      {
        "camera_trap_mount_lut": 12
      },
      {
        "lat": -24.359316,
        "lng": 147.326588
      },
      [
        3
      ],
      "2",
      "2",
      2,
      2,
      1,
      1,
      1,
      1,
      1,
      1,
      2,
      {
        "habitat_lut": 9
      },
      ""
    ],
    [
      3,
      "CTP001E",
      "2023-03-27",
      "CTP001E-2023-03-27",
      null,
      null,
      null,
      [
        {
          "taxa_type": 3
        }
      ],
      [
        {
          "species": "Platypus (scientific: Platypus Shaw, 1799)"
        },
        {
          "species": "Crocodylus [Genus] (scientific: Crocodylus Laurenti, 1768)"
        }
      ],
      {
        "camera_trap_mount_text": "custom ct mount"
      },
      {
        "lat": -34.969289,
        "lng": 138.632968
      },
      [
        1,
        2
      ],
      "1",
      "1",
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      {
        "habitat_lut": 7
      },
      "notes\n\nnext line"
    ],
    [
      3,
      "CTP001F",
      "2023-03-27",
      "CTP001F-2023-03-27",
      null,
      null,
      null,
      [],
      [
        {
          "species": "Rhinella [Genus] (scientific: Rhinella Fitzinger, 1826)"
        }
      ],
      {
        "camera_trap_mount_lut": 12
      },
      {
        "lat": -34.969588,
        "lng": 138.632287
      },
      [
        3
      ],
      "2",
      "2",
      2,
      2,
      1,
      1,
      1,
      1,
      1,
      1,
      2,
      {
        "habitat_lut": 9
      },
      ""
    ]
  ]
}
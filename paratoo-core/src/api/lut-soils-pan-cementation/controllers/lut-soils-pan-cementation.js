const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-pan-cementation.lut-soils-pan-cementation')
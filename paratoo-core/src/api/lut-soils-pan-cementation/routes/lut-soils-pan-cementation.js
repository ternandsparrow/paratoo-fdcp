module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-pan-cementations',
      'handler': 'lut-soils-pan-cementation.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-pan-cementations/:id',
      'handler': 'lut-soils-pan-cementation.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
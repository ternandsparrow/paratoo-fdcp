module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-individuals-or-groups',
      'handler': 'lut-intervention-individuals-or-group.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-individuals-or-groups/:id',
      'handler': 'lut-intervention-individuals-or-group.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
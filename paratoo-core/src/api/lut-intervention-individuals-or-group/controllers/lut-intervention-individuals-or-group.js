'use strict'

/**
 *  lut-intervention-individuals-or-group controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-individuals-or-group.lut-intervention-individuals-or-group')

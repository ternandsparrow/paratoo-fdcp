'use strict'

/**
 * lut-intervention-individuals-or-group service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-individuals-or-group.lut-intervention-individuals-or-group')

'use strict'

/**
 * reserved-plot-label controller
 */

const { createCoreController } = require('@strapi/strapi').factories

//only want to populate non-sensitive data (e.g., don't want location)
const populateObj = {
  created_plot_selection: {
    fields: [
      'plot_label',
      'uuid',
    ],
    populate: {
      plot_name: {
        populate: {
          state: true,
          program: true,
          bioregion: true
        },
        fields: [
          'unique_digits',
        ],
      },
    },
  },
  reservation_provenance: true,
}

module.exports = createCoreController(
  'api::reserved-plot-label.reserved-plot-label',
  ({ strapi }) => ({
    async find(ctx) {
      const entries = await strapi.entityService.findMany(
        'api::reserved-plot-label.reserved-plot-label', {
          populate: populateObj,
        })
      const sanitised = await this.sanitizeOutput(entries, ctx)
      return this.transformResponse(sanitised)
    },
    async findOne(ctx) {
      const entry = await strapi.entityService.findOne(
        'api::reserved-plot-label.reserved-plot-label', ctx.params.id, {
          populate: populateObj,
        })
      const sanitised = await this.sanitizeOutput(entry, ctx)
      return this.transformResponse(sanitised)
    },
  })
)

'use strict'

/**
 * reserved-plot-label service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::reserved-plot-label.reserved-plot-label')

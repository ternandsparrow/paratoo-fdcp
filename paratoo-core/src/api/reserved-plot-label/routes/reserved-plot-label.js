/**
 * GETs don't need PME as reading these labels isn't a big deal because:
 *  - we need to get all plot labels to ensure global uniqueness
 *  - we filter out any sensitive data in the relation to the plot selection in the
 *    controller
 */

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/reserved-plot-labels',
      'handler': 'reserved-plot-label.find',
      'config': {
        'policies': [
          'global::is-validated',
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/reserved-plot-labels/:id',
      'handler': 'reserved-plot-label.findOne',
      'config': {
        'policies': [
          'global::is-validated',
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/reserved-plot-labels',
      'handler': 'reserved-plot-label.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation',
        ]
      },
      'allowMethod': true,    //PME will treat this POST different to others
    },
    {
      'method': 'PUT',
      'path': '/reserved-plot-labels/:id',
      'handler': 'reserved-plot-label.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation',
        ]
      },
    },
    {
      'method': 'DELETE',
      'path': '/reserved-plot-labels/:id',
      'handler': 'reserved-plot-label.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
        ]
      },
      'allowMethod': true,
    }
  ]
}
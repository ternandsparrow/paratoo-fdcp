module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-coarse-frag-shapes',
      'handler': 'lut-soils-coarse-frag-shape.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-coarse-frag-shapes/:id',
      'handler': 'lut-soils-coarse-frag-shape.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-coarse-frag-shape.lut-soils-coarse-frag-shape')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-mvgs',
      'handler': 'lut-mvg.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-mvgs/:id',
      'handler': 'lut-mvg.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-mvg controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-mvg.lut-mvg')

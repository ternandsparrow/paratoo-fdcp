'use strict'

/**
 * lut-mvg service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-mvg.lut-mvg')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-breeding-techniques',
      'handler': 'lut-intervention-breeding-technique.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-breeding-techniques/:id',
      'handler': 'lut-intervention-breeding-technique.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
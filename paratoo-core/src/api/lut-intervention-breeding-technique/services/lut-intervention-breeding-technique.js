const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-breeding-technique.lut-intervention-breeding-technique')
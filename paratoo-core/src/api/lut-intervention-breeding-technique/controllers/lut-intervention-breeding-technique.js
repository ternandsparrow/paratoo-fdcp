const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-breeding-technique.lut-intervention-breeding-technique')
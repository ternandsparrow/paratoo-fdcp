module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-subplot-corners',
      'handler': 'lut-subplot-corner.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-subplot-corners/:id',
      'handler': 'lut-subplot-corner.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
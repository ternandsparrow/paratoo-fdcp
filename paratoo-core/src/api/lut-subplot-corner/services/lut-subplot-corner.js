'use strict'

/**
 * lut-subplot-corner service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-subplot-corner.lut-subplot-corner')

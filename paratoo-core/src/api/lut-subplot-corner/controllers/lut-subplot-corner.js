'use strict'

/**
 * lut-subplot-corner controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-subplot-corner.lut-subplot-corner')

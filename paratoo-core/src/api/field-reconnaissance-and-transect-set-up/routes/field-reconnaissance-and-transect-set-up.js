module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/field-reconnaissance-and-transect-set-ups',
      'handler': 'field-reconnaissance-and-transect-set-up.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/field-reconnaissance-and-transect-set-ups/:id',
      'handler': 'field-reconnaissance-and-transect-set-up.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/field-reconnaissance-and-transect-set-ups',
      'handler': 'field-reconnaissance-and-transect-set-up.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/field-reconnaissance-and-transect-set-ups/:id',
      'handler': 'field-reconnaissance-and-transect-set-up.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/field-reconnaissance-and-transect-set-ups/:id',
      'handler': 'field-reconnaissance-and-transect-set-up.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
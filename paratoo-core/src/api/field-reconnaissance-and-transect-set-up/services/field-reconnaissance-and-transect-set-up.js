'use strict'

/**
 * field-reconnaissance-and-transect-set-up service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::field-reconnaissance-and-transect-set-up.field-reconnaissance-and-transect-set-up')

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-horizon-boundary-shape.lut-soils-horizon-boundary-shape')
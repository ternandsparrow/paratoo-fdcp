const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-cwd-method.lut-cwd-method')
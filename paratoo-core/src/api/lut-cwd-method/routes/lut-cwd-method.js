module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-cwd-methods',
      'handler': 'lut-cwd-method.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-cwd-methods/:id',
      'handler': 'lut-cwd-method.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
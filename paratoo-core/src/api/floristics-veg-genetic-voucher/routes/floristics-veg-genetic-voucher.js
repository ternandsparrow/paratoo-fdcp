module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/floristics-veg-genetic-vouchers',
      'handler': 'floristics-veg-genetic-voucher.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/floristics-veg-genetic-vouchers/:id',
      'handler': 'floristics-veg-genetic-voucher.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/floristics-veg-genetic-vouchers',
      'handler': 'floristics-veg-genetic-voucher.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/floristics-veg-genetic-vouchers/:id',
      'handler': 'floristics-veg-genetic-voucher.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/floristics-veg-genetic-vouchers/:id',
      'handler': 'floristics-veg-genetic-voucher.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
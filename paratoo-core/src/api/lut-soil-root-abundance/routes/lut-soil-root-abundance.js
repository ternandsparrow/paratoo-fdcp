module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soil-root-abundances',
      'handler': 'lut-soil-root-abundance.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soil-root-abundances/:id',
      'handler': 'lut-soil-root-abundance.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
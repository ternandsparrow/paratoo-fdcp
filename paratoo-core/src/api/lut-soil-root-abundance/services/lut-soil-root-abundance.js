'use strict'

/**
 * lut-soil-root-abundance service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soil-root-abundance.lut-soil-root-abundance')

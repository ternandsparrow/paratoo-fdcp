'use strict'

/**
 *  lut-soil-root-abundance controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soil-root-abundance.lut-soil-root-abundance')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/herbivory-and-physical-damage-transects',
      'handler': 'herbivory-and-physical-damage-transect.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/herbivory-and-physical-damage-transects/:id',
      'handler': 'herbivory-and-physical-damage-transect.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/herbivory-and-physical-damage-transects',
      'handler': 'herbivory-and-physical-damage-transect.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/herbivory-and-physical-damage-transects/:id',
      'handler': 'herbivory-and-physical-damage-transect.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/herbivory-and-physical-damage-transects/:id',
      'handler': 'herbivory-and-physical-damage-transect.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * herbivory-and-physical-damage-transect service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::herbivory-and-physical-damage-transect.herbivory-and-physical-damage-transect')

'use strict'

/**
 *  lut-vertebrate-search-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-search-type.lut-vertebrate-search-type')

'use strict'

/**
 * lut-vertebrate-search-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-search-type.lut-vertebrate-search-type')

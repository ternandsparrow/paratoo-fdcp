module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-search-types',
      'handler': 'lut-vertebrate-search-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-search-types/:id',
      'handler': 'lut-vertebrate-search-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
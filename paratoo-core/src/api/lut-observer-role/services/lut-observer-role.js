'use strict'

/**
 * lut-observer-role service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-observer-role.lut-observer-role')

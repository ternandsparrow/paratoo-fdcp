'use strict'

/**
 *  lut-observer-role controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-observer-role.lut-observer-role')

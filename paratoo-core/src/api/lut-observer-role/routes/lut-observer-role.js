module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-observer-roles',
      'handler': 'lut-observer-role.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-observer-roles/:id',
      'handler': 'lut-observer-role.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/within-plot-belt-transect-surveys',
      'handler': 'within-plot-belt-transect-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/within-plot-belt-transect-surveys/:id',
      'handler': 'within-plot-belt-transect-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/within-plot-belt-transect-surveys',
      'handler': 'within-plot-belt-transect-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/within-plot-belt-transect-surveys/:id',
      'handler': 'within-plot-belt-transect-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/within-plot-belt-transect-surveys/:id',
      'handler': 'within-plot-belt-transect-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/within-plot-belt-transect-surveys/bulk',
      'handler': 'within-plot-belt-transect-survey.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
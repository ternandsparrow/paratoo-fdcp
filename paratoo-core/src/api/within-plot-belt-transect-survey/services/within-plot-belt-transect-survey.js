'use strict'

/**
 * within-plot-belt-transect-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::within-plot-belt-transect-survey.within-plot-belt-transect-survey')

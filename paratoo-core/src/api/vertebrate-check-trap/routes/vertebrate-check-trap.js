module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/vertebrate-check-traps',
      'handler': 'vertebrate-check-trap.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/vertebrate-check-traps/:id',
      'handler': 'vertebrate-check-trap.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/vertebrate-check-traps',
      'handler': 'vertebrate-check-trap.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/vertebrate-check-traps/:id',
      'handler': 'vertebrate-check-trap.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/vertebrate-check-traps/:id',
      'handler': 'vertebrate-check-trap.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
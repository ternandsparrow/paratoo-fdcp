'use strict'

/**
 * vertebrate-check-trap service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::vertebrate-check-trap.vertebrate-check-trap')

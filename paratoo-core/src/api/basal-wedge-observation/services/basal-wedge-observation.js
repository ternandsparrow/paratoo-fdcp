const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::basal-wedge-observation.basal-wedge-observation')
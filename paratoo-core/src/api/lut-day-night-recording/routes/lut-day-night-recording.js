module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-day-night-recordings',
      'handler': 'lut-day-night-recording.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-day-night-recordings/:id',
      'handler': 'lut-day-night-recording.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
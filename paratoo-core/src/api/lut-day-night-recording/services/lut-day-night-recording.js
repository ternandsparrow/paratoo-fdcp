'use strict'

/**
 * lut-day-night-recording service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-day-night-recording.lut-day-night-recording')

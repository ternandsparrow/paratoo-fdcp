'use strict'

/**
 *  lut-day-night-recording controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-day-night-recording.lut-day-night-recording')

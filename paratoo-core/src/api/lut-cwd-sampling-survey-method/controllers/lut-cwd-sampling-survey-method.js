const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-cwd-sampling-survey-method.lut-cwd-sampling-survey-method')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-cwd-sampling-survey-methods',
      'handler': 'lut-cwd-sampling-survey-method.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-cwd-sampling-survey-methods/:id',
      'handler': 'lut-cwd-sampling-survey-method.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
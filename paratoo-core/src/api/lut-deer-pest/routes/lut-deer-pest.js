module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-deer-pests',
      'handler': 'lut-deer-pest.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-deer-pests/:id',
      'handler': 'lut-deer-pest.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
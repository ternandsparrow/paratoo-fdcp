'use strict'

/**
 *  lut-deer-pest controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-deer-pest.lut-deer-pest')

'use strict'

/**
 * lut-deer-pest service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-deer-pest.lut-deer-pest')

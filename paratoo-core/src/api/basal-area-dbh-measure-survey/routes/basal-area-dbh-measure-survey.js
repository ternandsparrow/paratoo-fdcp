module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/basal-area-dbh-measure-surveys',
      'handler': 'basal-area-dbh-measure-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/basal-area-dbh-measure-surveys/:id',
      'handler': 'basal-area-dbh-measure-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/basal-area-dbh-measure-surveys',
      'handler': 'basal-area-dbh-measure-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/basal-area-dbh-measure-surveys/:id',
      'handler': 'basal-area-dbh-measure-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/basal-area-dbh-measure-surveys/:id',
      'handler': 'basal-area-dbh-measure-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/basal-area-dbh-measure-surveys/bulk',
      'handler': 'basal-area-dbh-measure-survey.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
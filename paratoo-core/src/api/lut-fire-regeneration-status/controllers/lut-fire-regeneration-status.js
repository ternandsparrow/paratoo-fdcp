const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fire-regeneration-status.lut-fire-regeneration-status')
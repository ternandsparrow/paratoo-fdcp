module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fire-regeneration-statuss',
      'handler': 'lut-fire-regeneration-status.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fire-regeneration-statuss/:id',
      'handler': 'lut-fire-regeneration-status.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
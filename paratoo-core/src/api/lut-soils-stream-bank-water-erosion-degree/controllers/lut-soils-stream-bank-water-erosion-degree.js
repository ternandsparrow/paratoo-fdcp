const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-stream-bank-water-erosion-degree.lut-soils-stream-bank-water-erosion-degree')
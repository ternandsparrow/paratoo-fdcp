module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-stream-bank-water-erosion-degrees',
      'handler': 'lut-soils-stream-bank-water-erosion-degree.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-stream-bank-water-erosion-degrees/:id',
      'handler': 'lut-soils-stream-bank-water-erosion-degree.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-battery-types',
      'handler': 'lut-battery-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-battery-types/:id',
      'handler': 'lut-battery-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-battery-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-battery-type.lut-battery-type')

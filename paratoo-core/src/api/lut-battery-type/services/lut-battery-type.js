'use strict'

/**
 * lut-battery-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-battery-type.lut-battery-type')

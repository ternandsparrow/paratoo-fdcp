'use strict'

/**
 * lut-vantage-point-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vantage-point-type.lut-vantage-point-type')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vantage-point-types',
      'handler': 'lut-vantage-point-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vantage-point-types/:id',
      'handler': 'lut-vantage-point-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-vantage-point-type service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vantage-point-type.lut-vantage-point-type')

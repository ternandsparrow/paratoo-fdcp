'use strict'

/**
 * soil-horizon-consistence service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-horizon-consistence.soil-horizon-consistence')

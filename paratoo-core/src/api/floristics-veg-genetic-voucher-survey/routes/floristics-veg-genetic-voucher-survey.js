module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/floristics-veg-genetic-voucher-surveys',
      'handler': 'floristics-veg-genetic-voucher-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/floristics-veg-genetic-voucher-surveys/:id',
      'handler': 'floristics-veg-genetic-voucher-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/floristics-veg-genetic-voucher-surveys',
      'handler': 'floristics-veg-genetic-voucher-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/floristics-veg-genetic-voucher-surveys/:id',
      'handler': 'floristics-veg-genetic-voucher-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/floristics-veg-genetic-voucher-surveys/:id',
      'handler': 'floristics-veg-genetic-voucher-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/floristics-veg-genetic-voucher-surveys/bulk',
      'handler': 'floristics-veg-genetic-voucher-survey.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
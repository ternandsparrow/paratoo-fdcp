module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-corners',
      'handler': 'lut-corner.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-corners/:id',
      'handler': 'lut-corner.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
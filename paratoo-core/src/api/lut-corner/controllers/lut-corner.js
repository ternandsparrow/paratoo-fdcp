'use strict'

/**
 * lut-corner controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-corner.lut-corner')

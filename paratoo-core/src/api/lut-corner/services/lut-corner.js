'use strict'

/**
 * lut-corner service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-corner.lut-corner')

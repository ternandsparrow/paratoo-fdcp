const { sanitize } = require('@strapi/utils')

module.exports = {
  afterCreate(event) {
    const { schedulePublishCollection } = strapi.service(
      'api::paratoo-helper.paratoo-helper',
    )
    const data = event.result
    schedulePublishCollection('push-notification', data)
  },
  async afterUpdate(event) {
    const { webPush, schedulePublishCollection } = strapi.service(
      'api::paratoo-helper.paratoo-helper',
    )
    const data = event.result

    const newScheduledTime = new Date(data.scheduledAt).getTime()

    // clear old setTimeout as there's a change in scheduled time, and create a new one
    if (
      !data.publishedAt &&
      newScheduledTime !== global.scheduledTasks?.[data.id]
    ) {
      strapi.log.info(`reschedule push notification with ID ${data.id} to ${new Date(data.scheduledAt)}`)
      clearScheduledTask(data.id)
      schedulePublishCollection('push-notification', data)
    }

    if (data.publishedAt && !data.isSent) {
      // clear the scheduled task to avoid publishing the same record twice,
      // if we manually publish the record before the scheduled time
      clearScheduledTask(data.id)

      // sanitize the output
      const sanitizedOutput = await sanitize.sanitizers.defaultSanitizeOutput(
        event.model,
        data,
      )
      webPush(sanitizedOutput)
    }
  },
  afterDelete(event) {
    clearScheduledTask(event.result.id)
  },
}

function clearScheduledTask(id) {
  if (global.scheduledTasks?.[id]) {
    const formattedTime = new Date(
      global.scheduledTasks[id].scheduledTime,
    ).toLocaleString()
    strapi.log.info(
      `cancel publishing push notification with ID ${id} at ${formattedTime}`,
    )
    clearTimeout(global.scheduledTasks[id].timeoutId)
    delete global.scheduledTasks[id]
  }
}

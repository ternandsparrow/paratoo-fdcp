'use strict'

/**
 * push-notification router
 */

module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/push-notifications',
      handler: 'push-notification.find',
      config: {
        policies: ['global::is-validated', 'global::projectMembershipEnforcer'],
      },
    },
    {
      method: 'GET',
      path: '/push-notifications/:id',
      handler: 'push-notification.findOne',
      config: {
        policies: ['global::is-validated', 'global::projectMembershipEnforcer'],
      },
    },
    {
      method: 'POST',
      path: '/push-notifications',
      handler: 'push-notification.create',
      config: {
        policies: [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation',
        ],
      },
    },
    {
      method: 'PUT',
      path: '/push-notifications/:id',
      handler: 'push-notification.update',
      config: {
        policies: [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation',
        ],
      },
    },
    {
      method: 'DELETE',
      path: '/push-notifications/:id',
      handler: 'push-notification.delete',
      config: {
        policies: ['global::is-validated', 'global::projectMembershipEnforcer'],
      },
    },
  ],
}

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-herbivory-or-damages',
      'handler': 'lut-herbivory-or-damage.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-herbivory-or-damages/:id',
      'handler': 'lut-herbivory-or-damage.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
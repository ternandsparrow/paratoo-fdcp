'use strict'

/**
 * lut-herbivory-or-damage service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-herbivory-or-damage.lut-herbivory-or-damage')

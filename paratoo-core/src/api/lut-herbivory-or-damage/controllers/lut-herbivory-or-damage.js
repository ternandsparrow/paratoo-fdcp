'use strict'

/**
 * lut-herbivory-or-damage controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-herbivory-or-damage.lut-herbivory-or-damage')

'use strict'

/**
 *  lut-pest-wild-dog controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-pest-wild-dog.lut-pest-wild-dog')

'use strict'

/**
 * lut-pest-wild-dog service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-pest-wild-dog.lut-pest-wild-dog')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-pest-wild-dogs',
      'handler': 'lut-pest-wild-dog.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-pest-wild-dogs/:id',
      'handler': 'lut-pest-wild-dog.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-pouch-conditions',
      'handler': 'lut-fauna-pouch-condition.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-pouch-conditions/:id',
      'handler': 'lut-fauna-pouch-condition.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
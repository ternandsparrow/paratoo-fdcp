const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-pouch-condition.lut-fauna-pouch-condition')
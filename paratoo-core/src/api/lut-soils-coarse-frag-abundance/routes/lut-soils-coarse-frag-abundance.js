module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-coarse-frag-abundances',
      'handler': 'lut-soils-coarse-frag-abundance.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-coarse-frag-abundances/:id',
      'handler': 'lut-soils-coarse-frag-abundance.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
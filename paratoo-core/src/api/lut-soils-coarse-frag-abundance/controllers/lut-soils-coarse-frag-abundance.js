const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-coarse-frag-abundance.lut-soils-coarse-frag-abundance')
# Syncing model with org

As both paratoo-core *and* paratoo-org need to access the protocols, there needs to be some mechanism to keep both of them in sync.

The current mechanism is to manually do it, by runnung a script found in `/paratoo-fdcp/helper-scripts/sync-protocol-models.sh`.

This will sync the paratoo-core definition into paratoo-org, which will be clobbered. To ensure the CI/CD pipeline doesn't fail, make sure you run this script if you change the protocol model.

# Submodule - Fire

The Fire module cannot be collected by itself, and must be collected with Cover (but Cover can be collected by itself). To reduce confusion in the UI (both Monitor app and MERIT), we create 'copies' of the Cover protocols and name them 'Cover + Fire'. These are the protocols that are assigned to projects. To ensure we have workflow information for Fire, we also create a stand-alone 'Fire Survey' that has the `defaultHidden` parameter set. This is *not* assigned to projects, as it is *only* for Core to create bulk documentation and for the app to lookup workflow data.
'use strict'
/**
 *  protocol controller
 */
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController(
  'api::protocol.protocol',
  ({ strapi }) => ({
    async retrieveCollections(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      const requestBody = ctx.request.body
      const requestIDS = requestBody ? Object.keys(requestBody) : []
      let surveys = null
      let response = null

      if (!requestIDS.length) return response

      const request_id = requestIDS[0]
      switch (request_id) {
        case 'org_minted_uuid':
          response = await helpers.getSurveyMetadataCollections(
            requestBody[request_id],
          )
          ctx.body = response
          break
        case 'project_id':
          // finds all the surveys associated to the project_id
          surveys = await helpers.findCollectionIdsGivenProjectIdOrProtocolId(
            ctx,
            requestBody[request_id],
            null,
          )
          response = []
          for (const survey_metadata of surveys) {
            response.push(
              await helpers.getSurveyMetadataCollections(null, survey_metadata),
            )
          }
          ctx.body = response
          break
        case 'protocol_id':
          // finds all the surveys associated to the protocol_id
          surveys = await helpers.findCollectionIdsGivenProjectIdOrProtocolId(
            ctx,
            null,
            requestBody[request_id],
          )
          response = []
          for (const survey_metadata of surveys) {
            response.push(
              await helpers.getSurveyMetadataCollections(null, survey_metadata),
            )
          }
          ctx.body = response
          break
        default:
          break
      }

      return response
    },

    // only for testing e.g. load testing
    async testToken(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      const orgType = ctx.params.orgType
      if (!orgType) return null

      const jwt = await helpers.getAuthToken(orgType == 'merit')
      if (jwt.token) return {token: jwt.token}

      return {token: null, error: 'server down'}
    },

    // call exporter apis to export collections
    async abisExport(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      const requestBody = ctx.request.body
      const startDate = requestBody['startDate']
      const endDate = requestBody['endDate']
      const orgUuids = requestBody['org_uuids']
      const isForce = requestBody['force']
      // if list of org_uuids provided
      if (orgUuids) {
        helpers.paratooDebugMsg(`org_uuids: ${orgUuids}`)
        return await helpers.abisExportCollections(orgUuids, isForce)
      }

      // if dates provided
      if (startDate && endDate) {
        const orgUuids  = await helpers.getOrgUuids(startDate, endDate)
        helpers.paratooDebugMsg(`org_uuids: ${orgUuids}`)
        if(orgUuids.length==0) return 'No collection found to export, please change startDate/endDate'

        return await helpers.abisExportCollections(orgUuids, isForce)
      }
      return 'No collection found to export, reason: no org uuids or dates provided'
    },
  }),
)

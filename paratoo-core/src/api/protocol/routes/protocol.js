module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/protocols',
      'handler': 'protocol.find',
      'config': {
        'policies': []
      }
    },
    {
      'method': 'GET',
      'path': '/protocols/:id',
      'handler': 'protocol.findOne',
      'config': {
        'policies': []
      }
    },
    {
      'method': 'POST',
      'path': '/protocols/reverse-lookup',
      'handler': 'protocol.retrieveCollections',
      'config': {
        'policies': [
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/protocols/test-token/:orgType',
      'handler': 'protocol.testToken',
      'config': {
        'policies': []
      }
    },
    {
      // an endpoint to export collection if required
      'method': 'POST',
      'path': '/protocols/abis-export',
      'handler': 'protocol.abisExport',
      'config': {
        'policies': []
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-habitat-augmentation-types',
      'handler': 'lut-intervention-habitat-augmentation-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-habitat-augmentation-types/:id',
      'handler': 'lut-intervention-habitat-augmentation-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
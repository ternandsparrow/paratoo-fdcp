const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-habitat-augmentation-type.lut-intervention-habitat-augmentation-type')
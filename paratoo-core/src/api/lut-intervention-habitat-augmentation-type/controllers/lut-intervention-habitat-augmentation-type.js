const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-habitat-augmentation-type.lut-intervention-habitat-augmentation-type')
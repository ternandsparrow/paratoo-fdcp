'use strict'

/**
 * remove-wet-pitfall-trap service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::remove-wet-pitfall-trap.remove-wet-pitfall-trap')

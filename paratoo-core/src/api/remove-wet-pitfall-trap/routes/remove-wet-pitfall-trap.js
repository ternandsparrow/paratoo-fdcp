module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/remove-wet-pitfall-traps',
      'handler': 'remove-wet-pitfall-trap.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/remove-wet-pitfall-traps/:id',
      'handler': 'remove-wet-pitfall-trap.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/remove-wet-pitfall-traps',
      'handler': 'remove-wet-pitfall-trap.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/remove-wet-pitfall-traps/:id',
      'handler': 'remove-wet-pitfall-trap.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/remove-wet-pitfall-traps/:id',
      'handler': 'remove-wet-pitfall-trap.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-revisit-reason service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-revisit-reason.lut-revisit-reason')

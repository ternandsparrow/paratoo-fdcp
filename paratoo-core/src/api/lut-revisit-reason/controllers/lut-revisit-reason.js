'use strict'

/**
 * lut-revisit-reason controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-revisit-reason.lut-revisit-reason')

'use strict'

/**
 * lut-revisit-reason router
 */

module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/lut-revisit-reasons',
      handler: 'lut-revisit-reason.find',
      config: {
        policies: ['global::is-validated', 'global::projectMembershipEnforcer'],
      },
    },
    {
      method: 'GET',
      path: '/lut-revisit-reasons/:id',
      handler: 'lut-revisit-reason.findOne',
      config: {
        policies: ['global::is-validated', 'global::projectMembershipEnforcer'],
      },
    },
  ],
}

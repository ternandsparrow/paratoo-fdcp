'use strict'

/**
 * lut-vertebrate-permanent-id-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-permanent-id-type.lut-vertebrate-permanent-id-type')

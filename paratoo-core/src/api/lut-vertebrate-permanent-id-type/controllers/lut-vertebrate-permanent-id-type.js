'use strict'

/**
 *  lut-vertebrate-permanent-id-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-permanent-id-type.lut-vertebrate-permanent-id-type')

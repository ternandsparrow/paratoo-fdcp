module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-permanent-id-types',
      'handler': 'lut-vertebrate-permanent-id-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-permanent-id-types/:id',
      'handler': 'lut-vertebrate-permanent-id-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
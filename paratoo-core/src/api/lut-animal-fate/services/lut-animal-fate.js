'use strict'

/**
 * lut-animal-fate service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-animal-fate.lut-animal-fate')

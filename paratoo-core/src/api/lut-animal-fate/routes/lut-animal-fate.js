module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-animal-fates',
      'handler': 'lut-animal-fate.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-animal-fates/:id',
      'handler': 'lut-animal-fate.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-animal-fate controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-animal-fate.lut-animal-fate')

'use strict'

/**
 * lut-vertical-resolution controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertical-resolution.lut-vertical-resolution')

'use strict'

/**
 * lut-vertical-resolution service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertical-resolution.lut-vertical-resolution')

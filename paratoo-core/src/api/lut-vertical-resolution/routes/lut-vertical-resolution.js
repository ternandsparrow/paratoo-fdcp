module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertical-resolutions',
      'handler': 'lut-vertical-resolution.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertical-resolutions/:id',
      'handler': 'lut-vertical-resolution.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
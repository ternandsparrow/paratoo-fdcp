'use strict'

/**
 * lut-hapd-plot-size service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-hapd-plot-size.lut-hapd-plot-size')

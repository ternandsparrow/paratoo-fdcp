'use strict'

/**
 *  lut-hapd-plot-size controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-hapd-plot-size.lut-hapd-plot-size')

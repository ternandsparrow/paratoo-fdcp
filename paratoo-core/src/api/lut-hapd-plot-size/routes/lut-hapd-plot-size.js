module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-hapd-plot-sizes',
      'handler': 'lut-hapd-plot-size.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-hapd-plot-sizes/:id',
      'handler': 'lut-hapd-plot-size.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
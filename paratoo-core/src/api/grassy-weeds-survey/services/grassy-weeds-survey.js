'use strict'

/**
 * grassy-weeds-survey service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::grassy-weeds-survey.grassy-weeds-survey')

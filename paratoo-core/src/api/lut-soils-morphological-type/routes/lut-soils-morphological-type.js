module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-morphological-types',
      'handler': 'lut-soils-morphological-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-morphological-types/:id',
      'handler': 'lut-soils-morphological-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
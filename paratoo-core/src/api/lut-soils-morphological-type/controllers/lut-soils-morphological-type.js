const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-morphological-type.lut-soils-morphological-type')
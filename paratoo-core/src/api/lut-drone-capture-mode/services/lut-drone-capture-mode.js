'use strict'

/**
 * lut-drone-capture-mode service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-drone-capture-mode.lut-drone-capture-mode')

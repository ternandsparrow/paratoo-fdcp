'use strict'

/**
 *  lut-drone-capture-mode controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-drone-capture-mode.lut-drone-capture-mode')

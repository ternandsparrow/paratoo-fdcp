module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-drone-capture-modes',
      'handler': 'lut-drone-capture-mode.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-drone-capture-modes/:id',
      'handler': 'lut-drone-capture-mode.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
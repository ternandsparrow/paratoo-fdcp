'use strict'

/**
 * invertebrate-wet-pitfall-trap service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::invertebrate-wet-pitfall-trap.invertebrate-wet-pitfall-trap')

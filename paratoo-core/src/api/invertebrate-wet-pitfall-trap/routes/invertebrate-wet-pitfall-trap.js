module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/invertebrate-wet-pitfall-traps',
      'handler': 'invertebrate-wet-pitfall-trap.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/invertebrate-wet-pitfall-traps/:id',
      'handler': 'invertebrate-wet-pitfall-trap.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/invertebrate-wet-pitfall-traps',
      'handler': 'invertebrate-wet-pitfall-trap.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/invertebrate-wet-pitfall-traps/:id',
      'handler': 'invertebrate-wet-pitfall-trap.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/invertebrate-wet-pitfall-traps/:id',
      'handler': 'invertebrate-wet-pitfall-trap.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
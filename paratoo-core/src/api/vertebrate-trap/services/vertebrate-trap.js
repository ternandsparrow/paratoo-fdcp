'use strict'

/**
 * vertebrate-trap service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::vertebrate-trap.vertebrate-trap')

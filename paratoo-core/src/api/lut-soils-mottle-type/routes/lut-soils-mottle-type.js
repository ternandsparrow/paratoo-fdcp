module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-mottle-types',
      'handler': 'lut-soils-mottle-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-mottle-types/:id',
      'handler': 'lut-soils-mottle-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-breeding-codes',
      'handler': 'lut-fauna-breeding-code.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-breeding-codes/:id',
      'handler': 'lut-fauna-breeding-code.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
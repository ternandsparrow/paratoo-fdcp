const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-breeding-code.lut-fauna-breeding-code')
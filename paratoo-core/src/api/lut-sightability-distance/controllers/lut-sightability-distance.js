'use strict'

/**
 * lut-sightability-distance controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-sightability-distance.lut-sightability-distance')

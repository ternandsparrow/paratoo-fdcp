module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-sightability-distances',
      'handler': 'lut-sightability-distance.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-sightability-distances/:id',
      'handler': 'lut-sightability-distance.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
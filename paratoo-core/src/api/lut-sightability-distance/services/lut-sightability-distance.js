'use strict'

/**
 * lut-sightability-distance service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-sightability-distance.lut-sightability-distance')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-bird-activity-types',
      'handler': 'lut-fauna-bird-activity-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-bird-activity-types/:id',
      'handler': 'lut-fauna-bird-activity-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
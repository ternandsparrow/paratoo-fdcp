const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-bird-activity-type.lut-fauna-bird-activity-type')
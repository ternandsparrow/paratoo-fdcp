const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::basal-wedge-survey.basal-wedge-survey')
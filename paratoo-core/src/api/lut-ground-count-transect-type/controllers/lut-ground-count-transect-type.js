'use strict'

/**
 * lut-ground-count-transect-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-ground-count-transect-type.lut-ground-count-transect-type')

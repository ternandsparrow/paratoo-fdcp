module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-ground-count-transect-types',
      'handler': 'lut-ground-count-transect-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-ground-count-transect-types/:id',
      'handler': 'lut-ground-count-transect-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
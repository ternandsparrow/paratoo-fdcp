'use strict'

/**
 * lut-ground-count-transect-type service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-ground-count-transect-type.lut-ground-count-transect-type')

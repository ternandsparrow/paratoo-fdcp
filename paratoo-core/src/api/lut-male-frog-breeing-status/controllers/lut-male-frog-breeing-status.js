'use strict'

/**
 *  lut-male-frog-breeing-status controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-male-frog-breeing-status.lut-male-frog-breeing-status')

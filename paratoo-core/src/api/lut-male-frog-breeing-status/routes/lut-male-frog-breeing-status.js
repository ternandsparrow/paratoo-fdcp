module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-male-frog-breeing-statuses',
      'handler': 'lut-male-frog-breeing-status.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-male-frog-breeing-statuses/:id',
      'handler': 'lut-male-frog-breeing-status.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
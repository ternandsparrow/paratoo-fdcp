'use strict'

/**
 * lut-male-frog-breeing-status service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-male-frog-breeing-status.lut-male-frog-breeing-status')

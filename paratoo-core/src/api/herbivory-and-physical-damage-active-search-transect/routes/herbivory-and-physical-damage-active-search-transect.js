module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/herbivory-and-physical-damage-active-search-transects',
      'handler': 'herbivory-and-physical-damage-active-search-transect.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/herbivory-and-physical-damage-active-search-transects/:id',
      'handler': 'herbivory-and-physical-damage-active-search-transect.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/herbivory-and-physical-damage-active-search-transects',
      'handler': 'herbivory-and-physical-damage-active-search-transect.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/herbivory-and-physical-damage-active-search-transects/:id',
      'handler': 'herbivory-and-physical-damage-active-search-transect.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/herbivory-and-physical-damage-active-search-transects/:id',
      'handler': 'herbivory-and-physical-damage-active-search-transect.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
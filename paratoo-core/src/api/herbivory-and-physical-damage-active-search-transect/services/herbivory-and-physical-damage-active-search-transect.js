'use strict'

/**
 * herbivory-and-physical-damage-active-search-transect service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::herbivory-and-physical-damage-active-search-transect.herbivory-and-physical-damage-active-search-transect')

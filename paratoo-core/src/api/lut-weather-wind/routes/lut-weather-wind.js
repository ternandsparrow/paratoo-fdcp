module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-weather-winds',
      'handler': 'lut-weather-wind.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-weather-winds/:id',
      'handler': 'lut-weather-wind.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
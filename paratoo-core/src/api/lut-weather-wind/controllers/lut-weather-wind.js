const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-weather-wind.lut-weather-wind')
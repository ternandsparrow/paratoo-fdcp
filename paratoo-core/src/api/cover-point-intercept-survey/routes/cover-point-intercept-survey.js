module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/cover-point-intercept-surveys',
      'handler': 'cover-point-intercept-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/cover-point-intercept-surveys/:id',
      'handler': 'cover-point-intercept-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/cover-point-intercept-surveys',
      'handler': 'cover-point-intercept-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/cover-point-intercept-surveys/:id',
      'handler': 'cover-point-intercept-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/cover-point-intercept-surveys/:id',
      'handler': 'cover-point-intercept-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/cover-point-intercept-surveys/bulk',
      'handler': 'cover-point-intercept-survey.bulk',
      'config': {
        'policies': [
          'global::fix-species-intercept-height',
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
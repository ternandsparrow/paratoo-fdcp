const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::cover-point-intercept-survey.cover-point-intercept-survey', ({ strapi }) =>({
  /* Generic find controller with pdp data filter */
  async find(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      
    const authToken = ctx.request.headers['authorization']
    const modelName = ctx.state.route.info.apiName
    const tokenStatus = await helpers.apiTokenCheck(authToken)
    const useDefaultController = ctx.request.query['use-default'] === 'true'
      
    // if we need to use default controller (use magic jwt and use-default flag)
    if (tokenStatus?.type == 'full-access' && useDefaultController) {
      helpers.paratooDebugMsg(`Default controller is used to populate model: ${modelName}`)
      return await super.find(ctx)
    }
    return await helpers.pdpDataFilter(
      modelName,
      authToken,
      ctx.request.query,
      tokenStatus,
    )
  },

  /**
   * TODO
   * 
   * @param {*} ctx 
   */
  async bulk(ctx) {
    //FIXME add back to bulk override documentation: `"minItems": 404` and
    //`"minItems": 1010` to requestBody of cover-point-intercept-point
    const surveyModelName = 'cover-point-intercept-survey'
    const pointModelName = 'cover-point-intercept-point'
    const speciesModelName = 'cover-point-intercept-species-intercept'

    //TODO validate that the points are complete (i.e., 'lite' is 4 transects of 101
    //points and 'heavy' is 10 transects of 101 points)

    return strapi.service('api::paratoo-helper.paratoo-helper').genericBulk({
      ctx: ctx, 
      surveyModelName: surveyModelName, 
      obsModelName: pointModelName, 
      childObservationModelName: speciesModelName
    })
  }

}))
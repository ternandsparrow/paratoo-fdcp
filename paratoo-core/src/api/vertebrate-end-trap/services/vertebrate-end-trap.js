'use strict'

/**
 * vertebrate-end-trap service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::vertebrate-end-trap.vertebrate-end-trap')

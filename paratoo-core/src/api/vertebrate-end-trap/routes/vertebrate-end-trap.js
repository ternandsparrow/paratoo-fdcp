module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/vertebrate-end-traps',
      'handler': 'vertebrate-end-trap.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/vertebrate-end-traps/:id',
      'handler': 'vertebrate-end-trap.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/vertebrate-end-traps',
      'handler': 'vertebrate-end-trap.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/vertebrate-end-traps/:id',
      'handler': 'vertebrate-end-trap.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/vertebrate-end-traps/:id',
      'handler': 'vertebrate-end-trap.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
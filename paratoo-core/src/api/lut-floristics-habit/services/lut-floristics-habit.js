'use strict'

/**
 * lut-floristics-habit service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-floristics-habit.lut-floristics-habit')

'use strict'

/**
 *  lut-floristics-habit controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-floristics-habit.lut-floristics-habit')

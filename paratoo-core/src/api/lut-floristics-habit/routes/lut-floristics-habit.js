module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-floristics-habits',
      'handler': 'lut-floristics-habit.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-floristics-habits/:id',
      'handler': 'lut-floristics-habit.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-chemosis-eye controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-chemosis-eye.lut-chemosis-eye')

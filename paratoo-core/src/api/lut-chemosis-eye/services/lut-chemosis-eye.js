'use strict'

/**
 * lut-chemosis-eye service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-chemosis-eye.lut-chemosis-eye')

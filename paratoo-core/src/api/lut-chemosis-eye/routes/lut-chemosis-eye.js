module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-chemosis-eyes',
      'handler': 'lut-chemosis-eye.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-chemosis-eyes/:id',
      'handler': 'lut-chemosis-eye.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * off-plot-belt-quadrat service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::off-plot-belt-quadrat.off-plot-belt-quadrat')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/off-plot-belt-quadrats',
      'handler': 'off-plot-belt-quadrat.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/off-plot-belt-quadrats/:id',
      'handler': 'off-plot-belt-quadrat.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/off-plot-belt-quadrats',
      'handler': 'off-plot-belt-quadrat.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/off-plot-belt-quadrats/:id',
      'handler': 'off-plot-belt-quadrat.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/off-plot-belt-quadrats/:id',
      'handler': 'off-plot-belt-quadrat.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
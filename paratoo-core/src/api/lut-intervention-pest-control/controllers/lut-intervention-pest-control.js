const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-pest-control.lut-intervention-pest-control')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-pest-controls',
      'handler': 'lut-intervention-pest-control.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-pest-controls/:id',
      'handler': 'lut-intervention-pest-control.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-pest-control.lut-intervention-pest-control')
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-mass-movement-erosion-degree.lut-soils-mass-movement-erosion-degree')
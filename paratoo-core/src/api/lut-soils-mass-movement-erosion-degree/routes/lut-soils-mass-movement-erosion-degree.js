module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-mass-movement-erosion-degrees',
      'handler': 'lut-soils-mass-movement-erosion-degree.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-mass-movement-erosion-degrees/:id',
      'handler': 'lut-soils-mass-movement-erosion-degree.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
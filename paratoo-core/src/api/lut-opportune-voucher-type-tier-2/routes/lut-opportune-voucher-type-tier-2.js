module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-opportune-voucher-type-tier-2s',
      'handler': 'lut-opportune-voucher-type-tier-2.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-opportune-voucher-type-tier-2s/:id',
      'handler': 'lut-opportune-voucher-type-tier-2.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
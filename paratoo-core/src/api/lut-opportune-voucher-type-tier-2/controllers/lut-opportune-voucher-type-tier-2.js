'use strict'

/**
 *  lut-opportune-voucher-type-tier-2 controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-opportune-voucher-type-tier-2.lut-opportune-voucher-type-tier-2')

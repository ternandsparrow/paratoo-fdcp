const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-segregations-nature.lut-soils-segregations-nature')
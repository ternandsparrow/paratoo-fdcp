module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-segregations-natures',
      'handler': 'lut-soils-segregations-nature.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-segregations-natures/:id',
      'handler': 'lut-soils-segregations-nature.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
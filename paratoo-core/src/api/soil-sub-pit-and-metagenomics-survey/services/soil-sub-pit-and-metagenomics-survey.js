'use strict'

/**
 * soil-sub-pit-and-metagenomics-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-sub-pit-and-metagenomics-survey.soil-sub-pit-and-metagenomics-survey')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/soil-sub-pit-and-metagenomics-surveys',
      'handler': 'soil-sub-pit-and-metagenomics-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/soil-sub-pit-and-metagenomics-surveys/:id',
      'handler': 'soil-sub-pit-and-metagenomics-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/soil-sub-pit-and-metagenomics-surveys',
      'handler': 'soil-sub-pit-and-metagenomics-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/soil-sub-pit-and-metagenomics-surveys/:id',
      'handler': 'soil-sub-pit-and-metagenomics-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/soil-sub-pit-and-metagenomics-surveys/:id',
      'handler': 'soil-sub-pit-and-metagenomics-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/soil-sub-pit-and-metagenomics-surveys/bulk',
      'handler': 'soil-sub-pit-and-metagenomics-survey.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
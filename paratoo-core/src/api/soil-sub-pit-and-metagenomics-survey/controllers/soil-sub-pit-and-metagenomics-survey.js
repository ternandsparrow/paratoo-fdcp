'use strict'

/**
 *  soil-sub-pit-and-metagenomics-survey controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController(
  'api::soil-sub-pit-and-metagenomics-survey.soil-sub-pit-and-metagenomics-survey',
  ({ strapi }) => ({
    /* Generic find controller with pdp data filter */
    async find(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      
      const authToken = ctx.request.headers['authorization']
      const modelName = ctx.state.route.info.apiName
      const tokenStatus = await helpers.apiTokenCheck(authToken)
      const useDefaultController = ctx.request.query['use-default'] === 'true'
      
      // if we need to use default controller (use magic jwt and use-default flag)
      if (tokenStatus?.type == 'full-access' && useDefaultController) {
        helpers.paratooDebugMsg(`Default controller is used to populate model: ${modelName}`)
        return await super.find(ctx)
      }
      return await helpers.pdpDataFilter(
        modelName,
        authToken,
        ctx.request.query,
        tokenStatus,
      )
    },

    /**
     * @param {Object} ctx Koa context
     * @returns {Array} successfully-created array of collections
     */
    async bulk(ctx) {
      const fieldObsModelName = ['soil-sub-pit']
      const labObsModelName = ['soil-sub-pit-sampling']

      const keys = Object.keys(ctx.request.body.data.collections[0])

      let obsModelName
      if (fieldObsModelName.every((model) => keys.includes(model))) {
        obsModelName = fieldObsModelName
      } else {
        obsModelName = labObsModelName
      }

      return strapi.service('api::paratoo-helper.paratoo-helper').genericBulk({
        ctx: ctx,
        surveyModelName: 'soil-sub-pit-and-metagenomics-survey',
        obsModelName,
        childObservationModelName: ['soil-sub-pit-observation'],
      })
    },
  }),
)

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/floristics-veg-voucher-lites',
      'handler': 'floristics-veg-voucher-lite.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/floristics-veg-voucher-lites/:id',
      'handler': 'floristics-veg-voucher-lite.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/floristics-veg-voucher-lites',
      'handler': 'floristics-veg-voucher-lite.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/floristics-veg-voucher-lites/:id',
      'handler': 'floristics-veg-voucher-lite.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/floristics-veg-voucher-lites/:id',
      'handler': 'floristics-veg-voucher-lite.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
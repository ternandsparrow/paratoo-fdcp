module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-skin-conditions',
      'handler': 'lut-skin-condition.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-skin-conditions/:id',
      'handler': 'lut-skin-condition.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
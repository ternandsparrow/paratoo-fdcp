'use strict'

/**
 * lut-skin-condition service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-skin-condition.lut-skin-condition')

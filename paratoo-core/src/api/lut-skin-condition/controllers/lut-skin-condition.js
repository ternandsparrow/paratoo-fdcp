'use strict'

/**
 *  lut-skin-condition controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-skin-condition.lut-skin-condition')

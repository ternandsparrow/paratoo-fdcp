'use strict'

/**
 * plot-definition-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::plot-definition-survey.plot-definition-survey')

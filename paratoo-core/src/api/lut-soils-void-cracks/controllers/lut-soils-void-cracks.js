const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-void-cracks.lut-soils-void-cracks')
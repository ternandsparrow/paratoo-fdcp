module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-void-cracks',
      'handler': 'lut-soils-void-cracks.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-void-cracks/:id',
      'handler': 'lut-soils-void-cracks.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
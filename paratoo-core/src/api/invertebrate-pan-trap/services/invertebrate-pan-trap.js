'use strict'

/**
 * invertebrate-pan-trap service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::invertebrate-pan-trap.invertebrate-pan-trap')

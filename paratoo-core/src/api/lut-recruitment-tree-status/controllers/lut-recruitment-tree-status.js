'use strict'

/**
 *  lut-recruitment-tree-status controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-recruitment-tree-status.lut-recruitment-tree-status')

'use strict'

/**
 * lut-recruitment-tree-status service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-recruitment-tree-status.lut-recruitment-tree-status')

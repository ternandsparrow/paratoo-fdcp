module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-recruitment-tree-statuses',
      'handler': 'lut-recruitment-tree-status.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-recruitment-tree-statuses/:id',
      'handler': 'lut-recruitment-tree-status.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
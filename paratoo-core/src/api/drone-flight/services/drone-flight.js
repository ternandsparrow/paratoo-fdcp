'use strict'

/**
 * drone-flight service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::drone-flight.drone-flight')

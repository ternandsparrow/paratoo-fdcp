'use strict'

/**
 * lut-soils-root-size service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soils-root-size.lut-soils-root-size')

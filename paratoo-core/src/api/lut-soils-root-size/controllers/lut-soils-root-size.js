'use strict'

/**
 *  lut-soils-root-size controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-root-size.lut-soils-root-size')

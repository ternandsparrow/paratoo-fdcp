module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-root-sizes',
      'handler': 'lut-soils-root-size.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-root-sizes/:id',
      'handler': 'lut-soils-root-size.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-erosion-type.lut-soils-erosion-type')
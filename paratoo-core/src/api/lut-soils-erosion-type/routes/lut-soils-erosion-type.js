module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-erosion-types',
      'handler': 'lut-soils-erosion-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-erosion-types/:id',
      'handler': 'lut-soils-erosion-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
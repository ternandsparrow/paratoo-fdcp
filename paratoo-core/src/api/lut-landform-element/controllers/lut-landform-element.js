const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-landform-element.lut-landform-element')
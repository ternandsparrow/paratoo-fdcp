module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-landform-elements',
      'handler': 'lut-landform-element.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-landform-elements/:id',
      'handler': 'lut-landform-element.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
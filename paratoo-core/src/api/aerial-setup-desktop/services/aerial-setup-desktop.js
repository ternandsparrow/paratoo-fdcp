'use strict'

/**
 * aerial-setup-desktop service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::aerial-setup-desktop.aerial-setup-desktop')

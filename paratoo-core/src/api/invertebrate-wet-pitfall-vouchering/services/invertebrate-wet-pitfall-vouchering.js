'use strict'

/**
 * invertebrate-wet-pitfall-vouchering service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::invertebrate-wet-pitfall-vouchering.invertebrate-wet-pitfall-vouchering')

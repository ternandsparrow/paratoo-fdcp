module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/invertebrate-wet-pitfall-voucherings',
      'handler': 'invertebrate-wet-pitfall-vouchering.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/invertebrate-wet-pitfall-voucherings/:id',
      'handler': 'invertebrate-wet-pitfall-vouchering.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/invertebrate-wet-pitfall-voucherings',
      'handler': 'invertebrate-wet-pitfall-vouchering.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/invertebrate-wet-pitfall-voucherings/:id',
      'handler': 'invertebrate-wet-pitfall-vouchering.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/invertebrate-wet-pitfall-voucherings/:id',
      'handler': 'invertebrate-wet-pitfall-vouchering.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
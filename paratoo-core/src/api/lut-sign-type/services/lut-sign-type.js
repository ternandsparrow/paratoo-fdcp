'use strict'

/**
 * lut-sign-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-sign-type.lut-sign-type')

'use strict'

/**
 *  lut-sign-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-sign-type.lut-sign-type')

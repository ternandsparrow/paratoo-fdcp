module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-sign-types',
      'handler': 'lut-sign-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-sign-types/:id',
      'handler': 'lut-sign-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
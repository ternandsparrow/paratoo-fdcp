'use strict'

/**
 *  lut-soils-coarse-frag-distribution controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-coarse-frag-distribution.lut-soils-coarse-frag-distribution')

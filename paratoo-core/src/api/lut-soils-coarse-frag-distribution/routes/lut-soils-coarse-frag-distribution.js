module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-coarse-frag-distributions',
      'handler': 'lut-soils-coarse-frag-distribution.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-coarse-frag-distributions/:id',
      'handler': 'lut-soils-coarse-frag-distribution.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-soils-coarse-frag-distribution service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soils-coarse-frag-distribution.lut-soils-coarse-frag-distribution')

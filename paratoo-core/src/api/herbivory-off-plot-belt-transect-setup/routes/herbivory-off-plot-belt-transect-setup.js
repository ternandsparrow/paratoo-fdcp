module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/herbivory-off-plot-belt-transect-setups',
      'handler': 'herbivory-off-plot-belt-transect-setup.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/herbivory-off-plot-belt-transect-setups/:id',
      'handler': 'herbivory-off-plot-belt-transect-setup.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/herbivory-off-plot-belt-transect-setups',
      'handler': 'herbivory-off-plot-belt-transect-setup.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/herbivory-off-plot-belt-transect-setups/:id',
      'handler': 'herbivory-off-plot-belt-transect-setup.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/herbivory-off-plot-belt-transect-setups/:id',
      'handler': 'herbivory-off-plot-belt-transect-setup.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/herbivory-off-plot-belt-transect-setups/bulk',
      'handler': 'herbivory-off-plot-belt-transect-setup.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
'use strict'

/**
 * herbivory-off-plot-belt-transect-setup service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::herbivory-off-plot-belt-transect-setup.herbivory-off-plot-belt-transect-setup')

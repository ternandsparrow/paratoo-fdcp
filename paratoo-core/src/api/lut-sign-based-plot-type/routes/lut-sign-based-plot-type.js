module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-sign-based-plot-types',
      'handler': 'lut-sign-based-plot-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-sign-based-plot-types/:id',
      'handler': 'lut-sign-based-plot-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-sign-based-plot-type service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-sign-based-plot-type.lut-sign-based-plot-type')

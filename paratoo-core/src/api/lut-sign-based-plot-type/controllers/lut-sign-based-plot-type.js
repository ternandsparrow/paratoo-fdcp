'use strict'

/**
 * lut-sign-based-plot-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-sign-based-plot-type.lut-sign-based-plot-type')

'use strict'

/**
 * soil-bulk-density-sample service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService(
  'api::soil-bulk-density-sample.soil-bulk-density-sample',
)

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/pest-fauna-control-activities',
      'handler': 'pest-fauna-control-activity.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/pest-fauna-control-activities/:id',
      'handler': 'pest-fauna-control-activity.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/pest-fauna-control-activities',
      'handler': 'pest-fauna-control-activity.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/pest-fauna-control-activities/:id',
      'handler': 'pest-fauna-control-activity.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/pest-fauna-control-activities/:id',
      'handler': 'pest-fauna-control-activity.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/pest-fauna-control-activities/bulk',
      'handler': 'pest-fauna-control-activity.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
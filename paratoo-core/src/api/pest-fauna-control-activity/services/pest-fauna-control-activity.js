'use strict'

/**
 * pest-fauna-control-activity service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::pest-fauna-control-activity.pest-fauna-control-activity')

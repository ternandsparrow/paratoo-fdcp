module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-floristics-phenologies',
      'handler': 'lut-floristics-phenology.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-floristics-phenologies/:id',
      'handler': 'lut-floristics-phenology.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
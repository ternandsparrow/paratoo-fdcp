'use strict'

/**
 *  lut-floristics-phenology controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-floristics-phenology.lut-floristics-phenology')

'use strict'

/**
 * lut-floristics-phenology service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-floristics-phenology.lut-floristics-phenology')

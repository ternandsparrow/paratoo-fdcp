const _ = require('lodash')

/**
 * this mapper is to deal with weird cases when converting camel case schema names to
 * kebab case model names. For example LutSoils30CmSamplingIntervalRequest should be
 * lut-soils-30cm-sampling-interval but the default handler (using lodash) will get
 * lut-soils-30-cm-sampling-interval
 *
 * also mirrored in app's helpers
 *
 * TODO this is not the preferred way, ideally we want to use a single generic handler, or apply a policy in how LUTs containing numbers are named
 */
const schemaToModelNameExceptions = {
  LutSoils30CmSamplingIntervalRequest: 'lut-soils-30cm-sampling-interval',
}

// TODO ideally this wouldn't be our own custom controller, but this is used pending
// Strapi review of https://github.com/strapi/strapi/pull/11958
module.exports = {
  index: async (ctx) => {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    try {
      // Read latest documentation version with helper function
      const latestFullDoc = await helpers.readFullDocumentation()

      // TODO add API doc versions
      // based on https://github.com/strapi/strapi/issues/10734#issuecomment-901943169
      ctx.send(latestFullDoc)
    } catch (error) {
      helpers.paratooErrorHandler(
        400,
        new Error('Unable to retrieve documentation'),
      )
    }
  },

  createAuthorisedDocumentation: async (ctx) => {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const authToken = ctx.request.headers['authorization']

    // if cache exists
    const hash = helpers.createMd5Hash(`${authToken}swagger-user.json`)
    const doc = await helpers.redisGet(hash)
    if (doc) return doc

    const allUserProject = await helpers.getAllUserProjects(authToken)
    if (!allUserProject) {
      helpers.paratooErrorHandler(500, new Error('Failed to get user projects'))
    }

    const protocols = await helpers.getAllProtocols()
    const data = helpers.getAllUserModels(
      protocols,
      allUserProject.data.projects,
    )

    const customDoc = {}
    customDoc['info'] = {}
    customDoc['components'] = {}
    const latestFullDoc = await helpers.readFullDocumentation()
    customDoc['info'] = _.cloneDeep(latestFullDoc['info'])
    customDoc['components']['schemas'] = _.cloneDeep(
      latestFullDoc['components']['schemas'],
    )
    customDoc['paths'] = _.cloneDeep(latestFullDoc['paths'])

    // update schema
    const schemaLists = Object.keys(customDoc.components.schemas)
    for (let schema of schemaLists) {
      if (schema.toLocaleLowerCase().includes('error')) continue
      if (schema.toLocaleLowerCase().includes('protocol')) {
        customDoc.components.schemas[schema]['x-paratoo-endpoint-prefix'] =
          modelNameToEndpointPrefix('protocol')
        continue
      }
      if (schema.toLocaleLowerCase().includes('plotselectionsurvey')) {
        customDoc.components.schemas[schema]['x-paratoo-endpoint-prefix'] =
          modelNameToEndpointPrefix('plot-selection-survey')
        continue
      }
      if (schema.toLocaleLowerCase().includes('plotselection')) {
        customDoc.components.schemas[schema]['x-paratoo-endpoint-prefix'] =
          modelNameToEndpointPrefix('plot-selection')
        continue
      }
      if (schema.toLocaleLowerCase().includes('firesurvey')) {
        customDoc.components.schemas[schema]['x-paratoo-endpoint-prefix'] =
          modelNameToEndpointPrefix('fire-survey')
        continue
      }
      if (schema.toLocaleLowerCase().includes('firepointinterceptpoint')) {
        customDoc.components.schemas[schema]['x-paratoo-endpoint-prefix'] =
          modelNameToEndpointPrefix('fire-point-intercept-point')
        continue
      }
      if (schema.toLocaleLowerCase().includes('firespeciesintercept')) {
        customDoc.components.schemas[schema]['x-paratoo-endpoint-prefix'] =
          modelNameToEndpointPrefix('fire-species-intercept')
        continue
      }
      if (schema.toLocaleLowerCase().includes('firecharobservation')) {
        customDoc.components.schemas[schema]['x-paratoo-endpoint-prefix'] =
          modelNameToEndpointPrefix('fire-char-observation')
        continue
      }

      if (!schema.includes('Request')) {
        delete customDoc.components.schemas[schema]
        continue
      }
      if (schema.toLocaleLowerCase().includes('lut')) {
        delete customDoc.components.schemas[schema]
        continue
      }

      const modelName = schemaNameToModelName({
        schemaName: schema,
      })

      if (data['models'].includes(modelName)) {
        customDoc.components.schemas[schema]['x-paratoo-endpoint-prefix'] =
          modelNameToEndpointPrefix(modelName)
        continue
      }
      delete customDoc.components.schemas[schema]
    }

    // update paths
    const paths = Object.keys(customDoc.paths)
    for (let path of paths) {
      if (path.includes('/bulk')) continue

      delete customDoc.paths[path]
    }
    customDoc.info.description = `Build: ${process.env.NODE_ENV}-Version: ${process.env.VERSION}-GitHash: ${process.env.GIT_HASH}`
    helpers.redisSet(hash, customDoc, helpers.getTimeToLive('get'))
    ctx.send(customDoc)
  },

  getAllLuts: async (ctx) => {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    // Read latest documentation version with helper function
    const latestFullDoc = await helpers.readFullDocumentation()
    const allLuts = []

    const schemas = _.cloneDeep(latestFullDoc['components']['schemas'])
    const names = Object.keys(schemas)
    
    for (const name of names) {
      if (!isLut(name)) continue
      
      const modelName = schemaNameToModelName({
        schemaName: name,
      })
      if (!isValidModel(modelName)) continue

      allLuts.push(modelName)
    }
    ctx.send(allLuts)
  },

  getAllLutsEndpointPrefix: async (ctx) => {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    // Read latest documentation version with helper function
    const latestFullDoc = await helpers.readFullDocumentation()
    const allLutsEndpointPrefix = []

    const schemas = _.cloneDeep(latestFullDoc['components']['schemas'])
    const names = Object.keys(schemas)
    for (const name of names) {
      if (!isLut(name)) continue

      const modelName = schemaNameToModelName({
        schemaName: name,
      })
      if (!isValidModel(modelName)) continue
      
      allLutsEndpointPrefix.push(modelNameToEndpointPrefix(modelName))
    }
    ctx.send(allLutsEndpointPrefix)
  },

  buildInfo: async (ctx) => {
    ctx.send({
      Build: process.env.NODE_ENV,
      Version: process.env.VERSION,
      GitHash: process.env.GIT_HASH,
    })
  },

  allTargets: async (ctx) => {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const model = ctx.request.body['model']
    const deepSearch = ctx.request.body['deepSearch']

    // all the associated targets
    const result = {}
    const relations = helpers.modelAssociations(model, deepSearch)
    for (const [key, value] of Object.entries(relations)) {
      if (!value.target) continue
      result[key] = _.cloneDeep(value)
      const modelName = value.target.split('.')[1]
      result[key]['model'] = modelName
      result[key]['endpointPrefix'] = modelNameToEndpointPrefix(modelName)
    }

    ctx.send(result)
  },
}

function modelNameToEndpointPrefix(modelName) {
  const routes = strapi.api[modelName]['routes'][modelName]['routes']
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  // parse one of the routes to extract endpoint
  for (let route of routes) {
    if (!route.handler.includes(`${modelName}.find`)) continue

    const prefix = route.path.replace('/', '')
    if (route.handler == `api::${modelName}.${modelName}.find`) return prefix
    if (route.handler == `${modelName}.find`) return prefix
    continue
  }
  // if {modelName}.find not found or config error in routes
  helpers.paratooWarnHandler(
    `can not parse endpoint prefix modelName: ${modelName}`,
  )
  return `${modelName}-endpoint-prefix-not-found`
}

function schemaNameToModelName({ schemaName }) {
  if (Object.keys(schemaToModelNameExceptions).includes(schemaName)) {
    return schemaToModelNameExceptions[schemaName]
  }

  return _.kebabCase(_.clone(schemaName).replace(/Request$/, ''))
}

function isLut(schemaName) {
  // Ignore request and response as luts don't have 
  //   post/put so there is no schema with request postfix.
  if (schemaName.includes('Response')) return false
  if (schemaName.includes('Request')) return false
  // CustomLuts and MultiLuts are not luts
  if (schemaName.includes('CustomLut')) return false
  if (schemaName.includes('MultiLut')) return false
  if (!schemaName.includes('Lut')) return false

  return true
}

function isValidModel(modelName) {
  if (!strapi.api?.[modelName]?.['routes']?.[modelName]?.['routes']) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    helpers.paratooWarnHandler(
      `No content-type/routes found, modelName: ${modelName}`,
    )
    return false
  }

  return true
}

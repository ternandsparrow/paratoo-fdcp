module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/documentation/swagger.json',
      handler: 'expose-documentation.index',
      config: {
        policies: [],
      },
    },
    {
      method: 'GET',
      path: '/documentation/swagger-user.json',
      handler: 'expose-documentation.createAuthorisedDocumentation',
      config: {
        policies: [],
      },
    },
    {
      method: 'GET',
      path: '/documentation/get-all-luts',
      handler: 'expose-documentation.getAllLuts',
      config: {
        policies: [],
      },
    },
    {
      method: 'GET',
      path: '/documentation/get-all-luts-endpoint-prefix',
      handler: 'expose-documentation.getAllLutsEndpointPrefix',
      config: {
        policies: [],
      },
    },
    {
      method: 'GET',
      path: '/documentation/swagger-build-info',
      handler: 'expose-documentation.buildInfo',
      config: {
        policies: [],
      },
    },
    {
      method: 'POST',
      path: '/documentation/all_targets',
      handler: 'expose-documentation.allTargets',
      config: {
        policies: [],
      },
    },
  ],
}

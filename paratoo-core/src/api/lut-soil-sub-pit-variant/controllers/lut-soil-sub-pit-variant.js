const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soil-sub-pit-variant.lut-soil-sub-pit-variant')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soil-sub-pit-variants',
      'handler': 'lut-soil-sub-pit-variant.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soil-sub-pit-variants/:id',
      'handler': 'lut-soil-sub-pit-variant.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreService } = require('@strapi/strapi').factories
module.exports = createCoreService(
  'api::lut-soil-sub-pit-variant.lut-soil-sub-pit-variant',
)

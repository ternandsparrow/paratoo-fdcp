module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-weather-precipitations',
      'handler': 'lut-weather-precipitation.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-weather-precipitations/:id',
      'handler': 'lut-weather-precipitation.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
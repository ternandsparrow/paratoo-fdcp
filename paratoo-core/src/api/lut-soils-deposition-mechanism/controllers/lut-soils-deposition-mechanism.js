'use strict'

/**
 * lut-soils-deposition-mechanism controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-deposition-mechanism.lut-soils-deposition-mechanism')

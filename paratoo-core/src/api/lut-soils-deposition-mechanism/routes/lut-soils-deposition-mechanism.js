module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-deposition-mechanisms',
      'handler': 'lut-soils-deposition-mechanism.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-deposition-mechanisms/:id',
      'handler': 'lut-soils-deposition-mechanism.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
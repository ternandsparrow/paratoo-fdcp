'use strict'

/**
 * lut-soils-deposition-mechanism service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soils-deposition-mechanism.lut-soils-deposition-mechanism')

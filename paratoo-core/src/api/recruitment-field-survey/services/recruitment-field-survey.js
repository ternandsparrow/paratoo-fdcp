'use strict'

/**
 * recruitment-field-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::recruitment-field-survey.recruitment-field-survey')

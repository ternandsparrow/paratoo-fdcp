const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-tier-1-observation-method.lut-tier-1-observation-method')
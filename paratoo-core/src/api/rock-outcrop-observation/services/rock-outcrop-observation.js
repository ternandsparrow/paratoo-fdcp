'use strict'

/**
 * rock-outcrop-observation service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::rock-outcrop-observation.rock-outcrop-observation')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-condition-disturbance-types',
      'handler': 'lut-condition-disturbance-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-condition-disturbance-types/:id',
      'handler': 'lut-condition-disturbance-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-condition-disturbance-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-condition-disturbance-type.lut-condition-disturbance-type')

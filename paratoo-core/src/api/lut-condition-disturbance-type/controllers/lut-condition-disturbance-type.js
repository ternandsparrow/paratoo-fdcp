'use strict'

/**
 *  lut-condition-disturbance-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-condition-disturbance-type.lut-condition-disturbance-type')

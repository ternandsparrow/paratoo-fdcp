'use strict'

/**
 *  lut-bait-station-mount controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-bait-station-mount.lut-bait-station-mount')

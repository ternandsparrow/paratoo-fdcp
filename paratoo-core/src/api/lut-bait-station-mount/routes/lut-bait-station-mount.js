module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-bait-station-mounts',
      'handler': 'lut-bait-station-mount.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-bait-station-mounts/:id',
      'handler': 'lut-bait-station-mount.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
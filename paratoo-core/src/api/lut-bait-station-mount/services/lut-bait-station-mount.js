'use strict'

/**
 * lut-bait-station-mount service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-bait-station-mount.lut-bait-station-mount')

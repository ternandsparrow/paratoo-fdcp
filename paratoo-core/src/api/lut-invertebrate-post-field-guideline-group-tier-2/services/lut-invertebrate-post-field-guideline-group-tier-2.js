const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-invertebrate-post-field-guideline-group-tier-2.lut-invertebrate-post-field-guideline-group-tier-2')
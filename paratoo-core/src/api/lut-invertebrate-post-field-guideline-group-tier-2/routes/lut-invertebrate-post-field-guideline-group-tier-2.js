module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-invertebrate-post-field-guideline-group-tier-2s',
      'handler': 'lut-invertebrate-post-field-guideline-group-tier-2.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-invertebrate-post-field-guideline-group-tier-2s/:id',
      'handler': 'lut-invertebrate-post-field-guideline-group-tier-2.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
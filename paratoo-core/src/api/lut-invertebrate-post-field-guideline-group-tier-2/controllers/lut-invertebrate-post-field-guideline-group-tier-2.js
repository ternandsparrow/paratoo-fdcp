const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-invertebrate-post-field-guideline-group-tier-2.lut-invertebrate-post-field-guideline-group-tier-2')
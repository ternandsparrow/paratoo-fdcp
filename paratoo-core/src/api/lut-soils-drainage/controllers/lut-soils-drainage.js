const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-drainage.lut-soils-drainage')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-drainages',
      'handler': 'lut-soils-drainage.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-drainages/:id',
      'handler': 'lut-soils-drainage.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
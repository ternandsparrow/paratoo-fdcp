module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-invertebrate-post-field-guideline-group-tier-1s',
      'handler': 'lut-invertebrate-post-field-guideline-group-tier-1.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-invertebrate-post-field-guideline-group-tier-1s/:id',
      'handler': 'lut-invertebrate-post-field-guideline-group-tier-1.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
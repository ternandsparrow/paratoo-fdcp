'use strict'

/**
 * vertebrate-active-passive-search-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::vertebrate-active-passive-search-survey.vertebrate-active-passive-search-survey')

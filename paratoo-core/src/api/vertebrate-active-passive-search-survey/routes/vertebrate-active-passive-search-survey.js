module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/vertebrate-active-passive-search-surveys',
      'handler': 'vertebrate-active-passive-search-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/vertebrate-active-passive-search-surveys/:id',
      'handler': 'vertebrate-active-passive-search-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/vertebrate-active-passive-search-surveys',
      'handler': 'vertebrate-active-passive-search-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/vertebrate-active-passive-search-surveys/:id',
      'handler': 'vertebrate-active-passive-search-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/vertebrate-active-passive-search-surveys/:id',
      'handler': 'vertebrate-active-passive-search-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/vertebrate-active-passive-search-surveys/bulk',
      'handler': 'vertebrate-active-passive-search-survey.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
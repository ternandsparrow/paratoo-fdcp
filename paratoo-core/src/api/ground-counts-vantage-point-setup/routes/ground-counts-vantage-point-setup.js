module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/ground-counts-vantage-point-setups',
      'handler': 'ground-counts-vantage-point-setup.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/ground-counts-vantage-point-setups/:id',
      'handler': 'ground-counts-vantage-point-setup.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/ground-counts-vantage-point-setups',
      'handler': 'ground-counts-vantage-point-setup.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/ground-counts-vantage-point-setups/:id',
      'handler': 'ground-counts-vantage-point-setup.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/ground-counts-vantage-point-setups/:id',
      'handler': 'ground-counts-vantage-point-setup.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
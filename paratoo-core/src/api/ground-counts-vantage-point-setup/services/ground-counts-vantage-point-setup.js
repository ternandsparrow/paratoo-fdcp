'use strict'

/**
 * ground-counts-vantage-point-setup service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::ground-counts-vantage-point-setup.ground-counts-vantage-point-setup')

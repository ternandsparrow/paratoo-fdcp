const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-pan-structure.lut-soils-pan-structure')
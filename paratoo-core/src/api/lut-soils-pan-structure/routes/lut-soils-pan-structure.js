module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-pan-structures',
      'handler': 'lut-soils-pan-structure.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-pan-structures/:id',
      'handler': 'lut-soils-pan-structure.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
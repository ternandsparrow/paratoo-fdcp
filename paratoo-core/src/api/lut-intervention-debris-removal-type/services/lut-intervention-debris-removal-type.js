const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-debris-removal-type.lut-intervention-debris-removal-type')
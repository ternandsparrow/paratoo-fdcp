module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-debris-removal-types',
      'handler': 'lut-intervention-debris-removal-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-debris-removal-types/:id',
      'handler': 'lut-intervention-debris-removal-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
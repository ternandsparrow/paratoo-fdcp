const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-debris-removal-type.lut-intervention-debris-removal-type')
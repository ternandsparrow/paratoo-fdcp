const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-coarse-frag-size.lut-soils-coarse-frag-size')
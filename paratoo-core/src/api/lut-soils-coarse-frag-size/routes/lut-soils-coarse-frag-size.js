module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-coarse-frag-sizes',
      'handler': 'lut-soils-coarse-frag-size.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-coarse-frag-sizes/:id',
      'handler': 'lut-soils-coarse-frag-size.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * new-targeted-survey service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::new-targeted-survey.new-targeted-survey')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/new-targeted-surveys',
      'handler': 'new-targeted-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/new-targeted-surveys/:id',
      'handler': 'new-targeted-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/new-targeted-surveys',
      'handler': 'new-targeted-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/new-targeted-surveys/:id',
      'handler': 'new-targeted-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/new-targeted-surveys/:id',
      'handler': 'new-targeted-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/new-targeted-surveys/bulk',
      'handler': 'new-targeted-survey.bulk',
      'config': {
        'policies': [
          'global::fix-reject-old-queued-data',
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
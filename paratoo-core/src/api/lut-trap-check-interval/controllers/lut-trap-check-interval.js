'use strict'

/**
 *  lut-trap-check-interval controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-trap-check-interval.lut-trap-check-interval')

'use strict'

/**
 * lut-trap-check-interval service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-trap-check-interval.lut-trap-check-interval')

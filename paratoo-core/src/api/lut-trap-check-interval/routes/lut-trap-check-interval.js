module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-trap-check-intervals',
      'handler': 'lut-trap-check-interval.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-trap-check-intervals/:id',
      'handler': 'lut-trap-check-interval.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-select-protocol-variant service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-select-protocol-variant.lut-select-protocol-variant')

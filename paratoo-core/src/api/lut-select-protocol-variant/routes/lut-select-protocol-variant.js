module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-select-protocol-variants',
      'handler': 'lut-select-protocol-variant.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-select-protocol-variants/:id',
      'handler': 'lut-select-protocol-variant.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-select-protocol-variant controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-select-protocol-variant.lut-select-protocol-variant')

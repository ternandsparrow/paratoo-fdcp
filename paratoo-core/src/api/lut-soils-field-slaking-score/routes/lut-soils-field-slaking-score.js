module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-field-slaking-scores',
      'handler': 'lut-soils-field-slaking-score.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-field-slaking-scores/:id',
      'handler': 'lut-soils-field-slaking-score.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
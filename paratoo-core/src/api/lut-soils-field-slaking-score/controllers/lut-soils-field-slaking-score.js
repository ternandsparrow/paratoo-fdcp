const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-field-slaking-score.lut-soils-field-slaking-score')
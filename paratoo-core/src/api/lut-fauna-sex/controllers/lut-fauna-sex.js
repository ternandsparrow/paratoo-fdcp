const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-sex.lut-fauna-sex')
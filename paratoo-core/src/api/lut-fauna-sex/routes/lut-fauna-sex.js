module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-sexes',
      'handler': 'lut-fauna-sex.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-sexes/:id',
      'handler': 'lut-fauna-sex.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
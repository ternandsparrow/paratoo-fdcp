const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-fauna-sex.lut-fauna-sex')
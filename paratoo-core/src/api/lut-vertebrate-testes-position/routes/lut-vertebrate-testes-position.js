module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-testes-positions',
      'handler': 'lut-vertebrate-testes-position.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-testes-positions/:id',
      'handler': 'lut-vertebrate-testes-position.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
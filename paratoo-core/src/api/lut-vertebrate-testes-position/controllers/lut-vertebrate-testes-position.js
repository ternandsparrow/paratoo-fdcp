'use strict'

/**
 *  lut-vertebrate-testes-position controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-testes-position.lut-vertebrate-testes-position')

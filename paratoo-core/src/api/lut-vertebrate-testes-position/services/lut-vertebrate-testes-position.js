'use strict'

/**
 * lut-vertebrate-testes-position service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-testes-position.lut-vertebrate-testes-position')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/floristics-veg-voucher-fulls',
      'handler': 'floristics-veg-voucher-full.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/floristics-veg-voucher-fulls/:id',
      'handler': 'floristics-veg-voucher-full.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/floristics-veg-voucher-fulls',
      'handler': 'floristics-veg-voucher-full.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/floristics-veg-voucher-fulls/:id',
      'handler': 'floristics-veg-voucher-full.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/floristics-veg-voucher-fulls/:id',
      'handler': 'floristics-veg-voucher-full.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
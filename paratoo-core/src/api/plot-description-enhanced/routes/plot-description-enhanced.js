module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/plot-description-enhanceds',
      'handler': 'plot-description-enhanced.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/plot-description-enhanceds/:id',
      'handler': 'plot-description-enhanced.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/plot-description-enhanceds',
      'handler': 'plot-description-enhanced.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/plot-description-enhanceds/:id',
      'handler': 'plot-description-enhanced.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/plot-description-enhanceds/:id',
      'handler': 'plot-description-enhanced.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/plot-description-enhanceds/bulk',
      'handler': 'plot-description-enhanced.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
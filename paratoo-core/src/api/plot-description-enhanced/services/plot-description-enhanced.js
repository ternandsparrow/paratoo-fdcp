'use strict'

/**
 * plot-description-enhanced service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::plot-description-enhanced.plot-description-enhanced')

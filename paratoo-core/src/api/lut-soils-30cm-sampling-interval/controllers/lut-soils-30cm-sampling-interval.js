'use strict'

/**
 * lut-soils-30cm-sampling-interval controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-30cm-sampling-interval.lut-soils-30cm-sampling-interval')

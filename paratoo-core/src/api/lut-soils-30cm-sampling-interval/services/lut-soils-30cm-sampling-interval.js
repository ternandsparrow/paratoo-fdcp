'use strict'

/**
 * lut-soils-30cm-sampling-interval service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soils-30cm-sampling-interval.lut-soils-30cm-sampling-interval')

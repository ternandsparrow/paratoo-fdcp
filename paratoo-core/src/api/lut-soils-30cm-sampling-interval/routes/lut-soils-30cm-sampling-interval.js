module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-30cm-sampling-intervals',
      'handler': 'lut-soils-30cm-sampling-interval.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-30cm-sampling-intervals/:id',
      'handler': 'lut-soils-30cm-sampling-interval.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
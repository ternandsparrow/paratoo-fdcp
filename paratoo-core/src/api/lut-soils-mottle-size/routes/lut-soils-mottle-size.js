module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-mottle-sizes',
      'handler': 'lut-soils-mottle-size.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-mottle-sizes/:id',
      'handler': 'lut-soils-mottle-size.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
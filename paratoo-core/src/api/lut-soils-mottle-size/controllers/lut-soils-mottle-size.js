const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-mottle-size.lut-soils-mottle-size')
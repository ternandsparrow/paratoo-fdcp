'use strict'

/**
 * sign-based-vehicle-track-set-up service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::sign-based-vehicle-track-set-up.sign-based-vehicle-track-set-up')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/sign-based-vehicle-track-set-ups',
      'handler': 'sign-based-vehicle-track-set-up.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/sign-based-vehicle-track-set-ups/:id',
      'handler': 'sign-based-vehicle-track-set-up.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/sign-based-vehicle-track-set-ups',
      'handler': 'sign-based-vehicle-track-set-up.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/sign-based-vehicle-track-set-ups/:id',
      'handler': 'sign-based-vehicle-track-set-up.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/sign-based-vehicle-track-set-ups/:id',
      'handler': 'sign-based-vehicle-track-set-up.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
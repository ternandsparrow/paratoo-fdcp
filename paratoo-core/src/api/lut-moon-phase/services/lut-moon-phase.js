'use strict'

/**
 * lut-moon-phase service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-moon-phase.lut-moon-phase')

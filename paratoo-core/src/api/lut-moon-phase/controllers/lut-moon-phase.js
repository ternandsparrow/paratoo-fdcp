'use strict'

/**
 *  lut-moon-phase controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-moon-phase.lut-moon-phase')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-moon-phases',
      'handler': 'lut-moon-phase.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-moon-phases/:id',
      'handler': 'lut-moon-phase.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-camera-activation-mechanisms',
      'handler': 'lut-camera-activation-mechanism.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-camera-activation-mechanisms/:id',
      'handler': 'lut-camera-activation-mechanism.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-camera-activation-mechanism controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-camera-activation-mechanism.lut-camera-activation-mechanism')

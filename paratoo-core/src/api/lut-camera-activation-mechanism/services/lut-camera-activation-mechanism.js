'use strict'

/**
 * lut-camera-activation-mechanism service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-camera-activation-mechanism.lut-camera-activation-mechanism')

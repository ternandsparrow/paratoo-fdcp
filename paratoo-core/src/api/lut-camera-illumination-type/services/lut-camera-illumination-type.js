'use strict'

/**
 * lut-camera-illumination-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-camera-illumination-type.lut-camera-illumination-type')

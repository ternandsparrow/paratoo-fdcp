'use strict'

/**
 *  lut-camera-illumination-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-camera-illumination-type.lut-camera-illumination-type')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-camera-illumination-types',
      'handler': 'lut-camera-illumination-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-camera-illumination-types/:id',
      'handler': 'lut-camera-illumination-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
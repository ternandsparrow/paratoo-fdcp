module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-camera-media-types',
      'handler': 'lut-camera-media-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-camera-media-types/:id',
      'handler': 'lut-camera-media-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
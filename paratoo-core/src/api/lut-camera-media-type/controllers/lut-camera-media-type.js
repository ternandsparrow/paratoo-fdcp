'use strict'

/**
 *  lut-camera-media-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-camera-media-type.lut-camera-media-type')

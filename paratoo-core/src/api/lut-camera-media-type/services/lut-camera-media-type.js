'use strict'

/**
 * lut-camera-media-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-camera-media-type.lut-camera-media-type')

'use strict'

/**
 * lut-plot-nearest-infrastructure service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-plot-nearest-infrastructure.lut-plot-nearest-infrastructure')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-plot-nearest-infrastructures',
      'handler': 'lut-plot-nearest-infrastructure.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-plot-nearest-infrastructures/:id',
      'handler': 'lut-plot-nearest-infrastructure.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
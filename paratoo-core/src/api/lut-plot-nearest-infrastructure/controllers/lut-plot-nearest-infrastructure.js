'use strict'

/**
 *  lut-plot-nearest-infrastructure controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-plot-nearest-infrastructure.lut-plot-nearest-infrastructure')

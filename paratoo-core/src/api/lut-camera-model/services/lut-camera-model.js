'use strict'

/**
 * lut-camera-model service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-camera-model.lut-camera-model')

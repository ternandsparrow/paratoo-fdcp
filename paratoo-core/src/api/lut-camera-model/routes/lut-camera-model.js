module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-camera-models',
      'handler': 'lut-camera-model.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-camera-models/:id',
      'handler': 'lut-camera-model.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
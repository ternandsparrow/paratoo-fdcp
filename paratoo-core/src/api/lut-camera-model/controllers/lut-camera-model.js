'use strict'

/**
 *  lut-camera-model controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-camera-model.lut-camera-model')

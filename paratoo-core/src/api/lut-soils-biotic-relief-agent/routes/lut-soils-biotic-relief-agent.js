module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-biotic-relief-agents',
      'handler': 'lut-soils-biotic-relief-agent.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-biotic-relief-agents/:id',
      'handler': 'lut-soils-biotic-relief-agent.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
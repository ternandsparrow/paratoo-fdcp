const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-biotic-relief-agent.lut-soils-biotic-relief-agent')
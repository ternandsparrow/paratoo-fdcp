module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/plot-selection-surveys',
      'handler': 'plot-selection-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/plot-selection-surveys/:id',
      'handler': 'plot-selection-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/plot-selection-surveys',
      'handler': 'plot-selection-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/plot-selection-surveys/:id',
      'handler': 'plot-selection-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/plot-selection-surveys/:id',
      'handler': 'plot-selection-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/plot-selection-surveys/bulk',
      'handler': 'plot-selection-survey.bulk',
      'config': {
        'policies': [
          'global::fix-bioregion-lut-plot-selection',
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
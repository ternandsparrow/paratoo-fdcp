'use strict'

/**
 * plot-selection-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::plot-selection-survey.plot-selection-survey')

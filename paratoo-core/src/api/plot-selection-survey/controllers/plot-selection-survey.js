'use strict'

/**
 *  plot-selection-survey controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController(
  'api::plot-selection-survey.plot-selection-survey',
  ({ strapi }) => ({
    /* Generic find controller with pdp data filter */
    async find(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      
      const authToken = ctx.request.headers['authorization']
      const modelName = ctx.state.route.info.apiName
      const tokenStatus = await helpers.apiTokenCheck(authToken)
      const useDefaultController = ctx.request.query['use-default'] === 'true'
      
      // if we need to use default controller (use magic jwt and use-default flag)
      if (tokenStatus?.type == 'full-access' && useDefaultController) {
        helpers.paratooDebugMsg(`Default controller is used to populate model: ${modelName}`)
        return await super.find(ctx)
      }
      return await helpers.pdpDataFilter(
        modelName,
        authToken,
        ctx.request.query,
        tokenStatus,
      )
    },

    async bulk(ctx) {
      //besides the typical flow for a bulk submission, we also grab the plots that we're
      //creating to link to the table of reserved plots

      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      const surveyModelName = 'plot-selection-survey'
      const obsModelName = 'plot-selection'

      const reservedPlotsToUpdate = []
      for (const collection of ctx?.request?.body?.data?.collections || []) {
        const survey = collection[surveyModelName]
        for (const plot of collection[obsModelName] || []) {
          const plotName = plot?.data?.plot_label
          const reservedPlotNameToUpdate = await strapi.db.query('api::reserved-plot-label.reserved-plot-label',)
            .findOne({
              where: {
                plot_label: plotName,
              },
              populate: true,
            })
          if (reservedPlotNameToUpdate) {
            if (reservedPlotNameToUpdate?.created_plot_selection) {
              //user submit a Plot Selection that someone else already did. to get to
              //this case the user would have had to be offline when we checked for
              //reserved plots, which is why the app would let them continue.
              //though they will likely end up being rejected by generic bulk
              helpers.paratooWarnHandler(
                `Plot with name ${plotName} already as a Plot Selection. Will continue but won't update the reserved plot entry`,
              )
            } else {
              reservedPlotsToUpdate.push(reservedPlotNameToUpdate)
            }
          } else {
            //plot wasn't reserved for some reason (might have been offline, even though
            //we assume this protocol is desktop/online)
            helpers.paratooWarnHandler(`Plot with name ${plotName} has not been reserved. Will attempt to reserve the plot now`)

            let errMsg = `Failed to reserve plot with name ${plotName}`

            const reservationProvenance = {
              project_id: survey?.data?.survey_metadata?.survey_details?.project_id,
              org_opaque_user_id: survey?.data?.survey_metadata?.org_opaque_user_id,
              version_app: survey?.data?.survey_metadata?.provenance?.version_app,
              version_core_documentation: survey?.data?.survey_metadata?.provenance?.version_core_documentation,
              is_core_generated: true,
            }
            helpers.paratooDebugMsg(
              `Will use reservation provenance: ${JSON.stringify(reservationProvenance)}`,
              true,
              'plotSelectionSurveyController',
            )
            const reservedPlotResp = await strapi.entityService.create(
              'api::reserved-plot-label.reserved-plot-label',
              {
                data: {
                  plot_label: plotName,
                  reservation_provenance: reservationProvenance,
                },
                populate: '*',
              }
            )
              .catch((err) => {
                helpers.paratooWarnHandler(
                  errMsg,
                  err,
                )
              })
            if (reservedPlotResp) {
              reservedPlotsToUpdate.push(reservedPlotResp)
            } else {
              helpers.paratooWarnHandler(
                errMsg,
              )
            }
          }
        }
      }
      
      helpers.paratooDebugMsg(
        `List of reserved plots to update later: ${JSON.stringify(reservedPlotsToUpdate)}`,
        true,
        'plotSelectionSurveyController',
      )

      const bulkResp = await strapi.service('api::paratoo-helper.paratoo-helper').genericBulk({
        ctx: ctx,
        surveyModelName: surveyModelName,
        obsModelName: obsModelName,
      })

      const syncPlotSelection = await helpers.syncPlotSelection(
        ctx,
        obsModelName,
        bulkResp[0][obsModelName],
      )

      Object.assign(bulkResp[0], ...syncPlotSelection)
      //genericBulk set this initially (same as `bulkResp`), but since we've synced plot
      //selections we need to also append the API response from org
      ctx.body = bulkResp

      //want to track the plots that were reserved that were actually created.
      //in the future we might want to use this to free-up plots that were reserved but
      //never created
      for (const plot of reservedPlotsToUpdate) {
        let relevantCreatedPlotSelection = null
        for (const collection of bulkResp) {
          relevantCreatedPlotSelection = collection?.[obsModelName]?.find(
            s => s.plot_label === plot.plot_label
          )
        }
        if (!relevantCreatedPlotSelection?.id) {
          helpers.paratooWarnHandler(`No created plot selection found for plot ${plot.plot_label}`)
        } else {
          const dataObj = {
            created_plot_selection: relevantCreatedPlotSelection.id,
          }
          if (plot?.reservation_provenance?.id) {
            //to prevent component getting squashed, the update needs at least the ID of
            //the existing component
            Object.assign(dataObj, {
              reservation_provenance: {
                id: plot.reservation_provenance.id,
              },
            })
          } else {
            helpers.paratooWarnHandler(`Want to re-append reservation provenance to record for plot ${plot.plot_label}, but there is none`)
          }

          helpers.paratooDebugMsg(
            `Updating reserved plot '${plot.plot_label}' (ID=${plot.id}) with relation to actual created plot '${relevantCreatedPlotSelection.plot_label}' (ID=${relevantCreatedPlotSelection.id}). Full data object: ${JSON.stringify(dataObj, null, 2)}`,
            true,
            'plotSelectionSurveyController',
          )
          
          await strapi.entityService.update(
            'api::reserved-plot-label.reserved-plot-label',
            plot.id,
            {
              data: dataObj,
            },
          )
        }
        
      }

      return bulkResp
    },
  }),
)

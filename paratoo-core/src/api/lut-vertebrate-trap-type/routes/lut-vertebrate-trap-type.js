module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-trap-types',
      'handler': 'lut-vertebrate-trap-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-trap-types/:id',
      'handler': 'lut-vertebrate-trap-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
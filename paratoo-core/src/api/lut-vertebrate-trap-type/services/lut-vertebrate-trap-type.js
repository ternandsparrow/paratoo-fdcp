'use strict'

/**
 * lut-vertebrate-trap-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-trap-type.lut-vertebrate-trap-type')

'use strict'

/**
 *  lut-vertebrate-trap-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-trap-type.lut-vertebrate-trap-type')

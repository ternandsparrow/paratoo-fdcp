const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-invertebrate-pan-trapping-duration.lut-invertebrate-pan-trapping-duration')
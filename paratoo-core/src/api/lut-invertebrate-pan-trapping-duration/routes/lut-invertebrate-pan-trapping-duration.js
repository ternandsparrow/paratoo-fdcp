module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-invertebrate-pan-trapping-durations',
      'handler': 'lut-invertebrate-pan-trapping-duration.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-invertebrate-pan-trapping-durations/:id',
      'handler': 'lut-invertebrate-pan-trapping-duration.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
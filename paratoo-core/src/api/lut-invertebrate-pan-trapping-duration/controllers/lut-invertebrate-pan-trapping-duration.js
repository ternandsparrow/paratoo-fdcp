const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-invertebrate-pan-trapping-duration.lut-invertebrate-pan-trapping-duration')
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-condition-life-stage.lut-condition-life-stage')
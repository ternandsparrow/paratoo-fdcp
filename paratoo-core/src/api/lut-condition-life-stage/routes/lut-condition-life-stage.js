module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-condition-life-stages',
      'handler': 'lut-condition-life-stage.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-condition-life-stages/:id',
      'handler': 'lut-condition-life-stage.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-cutan-abundances',
      'handler': 'lut-soils-cutan-abundance.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-cutan-abundances/:id',
      'handler': 'lut-soils-cutan-abundance.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-cutan-abundance.lut-soils-cutan-abundance')
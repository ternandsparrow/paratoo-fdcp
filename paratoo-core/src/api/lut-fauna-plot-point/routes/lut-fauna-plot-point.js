module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-plot-points',
      'handler': 'lut-fauna-plot-point.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-plot-points/:id',
      'handler': 'lut-fauna-plot-point.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
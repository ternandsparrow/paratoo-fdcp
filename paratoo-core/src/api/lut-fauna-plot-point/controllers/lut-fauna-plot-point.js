'use strict'

/**
 *  lut-fauna-plot-point controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-plot-point.lut-fauna-plot-point')

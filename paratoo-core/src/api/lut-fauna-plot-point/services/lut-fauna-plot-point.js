'use strict'

/**
 * lut-fauna-plot-point service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-fauna-plot-point.lut-fauna-plot-point')

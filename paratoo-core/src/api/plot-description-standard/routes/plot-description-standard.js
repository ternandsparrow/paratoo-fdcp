module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/plot-description-standards',
      'handler': 'plot-description-standard.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/plot-description-standards/:id',
      'handler': 'plot-description-standard.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/plot-description-standards',
      'handler': 'plot-description-standard.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/plot-description-standards/:id',
      'handler': 'plot-description-standard.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/plot-description-standards/:id',
      'handler': 'plot-description-standard.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/plot-description-standards/bulk',
      'handler': 'plot-description-standard.bulk',
      'config': {
        'policies': [
          'global::fix-revisit-free-text-to-lut',
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
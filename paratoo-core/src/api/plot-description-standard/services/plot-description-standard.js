'use strict'

/**
 * plot-description-standard service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::plot-description-standard.plot-description-standard')

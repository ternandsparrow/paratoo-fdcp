'use strict'

/**
 * condition-survey service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::condition-survey.condition-survey')

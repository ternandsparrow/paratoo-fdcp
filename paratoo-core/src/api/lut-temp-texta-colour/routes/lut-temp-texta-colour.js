module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-temp-texta-colours',
      'handler': 'lut-temp-texta-colour.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-temp-texta-colours/:id',
      'handler': 'lut-temp-texta-colour.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  lut-temp-texta-colour controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-temp-texta-colour.lut-temp-texta-colour')

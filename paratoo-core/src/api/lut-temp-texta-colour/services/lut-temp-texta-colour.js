'use strict'

/**
 * lut-temp-texta-colour service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-temp-texta-colour.lut-temp-texta-colour')

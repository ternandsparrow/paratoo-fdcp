'use strict'

/**
 * data-exporter-status service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::data-exporter-status.data-exporter-status')

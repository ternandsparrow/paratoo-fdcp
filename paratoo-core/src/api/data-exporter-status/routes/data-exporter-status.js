module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/data-exporter-statuses',
      handler: 'data-exporter-status.find',
      config: {
        policies: ['global::is-validated', 'global::projectMembershipEnforcer'],
      },
    },
    {
      method: 'POST',
      path: '/data-exporter-statuses',
      handler: 'data-exporter-status.create',
      config: {
        policies: ['global::is-validated', 'global::projectMembershipEnforcer'],
      },
    },
  ],
}

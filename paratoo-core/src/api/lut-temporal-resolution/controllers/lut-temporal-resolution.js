'use strict'

/**
 * lut-temporal-resolution controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-temporal-resolution.lut-temporal-resolution')

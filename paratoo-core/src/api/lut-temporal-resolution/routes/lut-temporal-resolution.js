module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-temporal-resolutions',
      'handler': 'lut-temporal-resolution.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-temporal-resolutions/:id',
      'handler': 'lut-temporal-resolution.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
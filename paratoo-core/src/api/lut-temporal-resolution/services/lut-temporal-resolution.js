'use strict'

/**
 * lut-temporal-resolution service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-temporal-resolution.lut-temporal-resolution')

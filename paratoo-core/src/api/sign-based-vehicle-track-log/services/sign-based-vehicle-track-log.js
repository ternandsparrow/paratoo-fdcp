'use strict'

/**
 * sign-based-vehicle-track-log service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::sign-based-vehicle-track-log.sign-based-vehicle-track-log')

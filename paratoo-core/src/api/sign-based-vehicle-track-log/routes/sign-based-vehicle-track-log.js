module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/sign-based-vehicle-track-logs',
      'handler': 'sign-based-vehicle-track-log.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/sign-based-vehicle-track-logs/:id',
      'handler': 'sign-based-vehicle-track-log.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/sign-based-vehicle-track-logs',
      'handler': 'sign-based-vehicle-track-log.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/sign-based-vehicle-track-logs/:id',
      'handler': 'sign-based-vehicle-track-log.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/sign-based-vehicle-track-logs/:id',
      'handler': 'sign-based-vehicle-track-log.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
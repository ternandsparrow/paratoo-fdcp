module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-trap-specifics',
      'handler': 'lut-vertebrate-trap-specific.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-trap-specifics/:id',
      'handler': 'lut-vertebrate-trap-specific.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
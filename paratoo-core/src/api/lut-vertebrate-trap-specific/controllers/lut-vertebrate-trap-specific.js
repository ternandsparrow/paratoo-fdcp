'use strict'

/**
 *  lut-vertebrate-trap-specific controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-trap-specific.lut-vertebrate-trap-specific')

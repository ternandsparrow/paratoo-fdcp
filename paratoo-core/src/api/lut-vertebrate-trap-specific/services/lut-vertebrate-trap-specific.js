'use strict'

/**
 * lut-vertebrate-trap-specific service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-trap-specific.lut-vertebrate-trap-specific')

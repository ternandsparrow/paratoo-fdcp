'use strict'

/**
 * dev-sandbox-general-entry service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::dev-sandbox-general-entry.dev-sandbox-general-entry')

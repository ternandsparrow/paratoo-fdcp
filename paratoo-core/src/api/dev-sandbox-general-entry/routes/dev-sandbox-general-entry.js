module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/dev-sandbox-general-entries',
      'handler': 'dev-sandbox-general-entry.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/dev-sandbox-general-entries/:id',
      'handler': 'dev-sandbox-general-entry.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/dev-sandbox-general-entries',
      'handler': 'dev-sandbox-general-entry.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/dev-sandbox-general-entries/:id',
      'handler': 'dev-sandbox-general-entry.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/dev-sandbox-general-entries/:id',
      'handler': 'dev-sandbox-general-entry.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-drone-sky-code controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-drone-sky-code.lut-drone-sky-code')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-drone-sky-codes',
      'handler': 'lut-drone-sky-code.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-drone-sky-codes/:id',
      'handler': 'lut-drone-sky-code.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-drone-sky-code service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-drone-sky-code.lut-drone-sky-code')

'use strict'

/**
 * vertebrate-trap-check-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::vertebrate-trap-check-survey.vertebrate-trap-check-survey')

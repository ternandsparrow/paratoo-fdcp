'use strict'

/**
 * ground-counts-vantage-point-observation service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::ground-counts-vantage-point-observation.ground-counts-vantage-point-observation')

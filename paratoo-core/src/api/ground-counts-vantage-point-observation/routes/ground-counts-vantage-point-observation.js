module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/ground-counts-vantage-point-observations',
      'handler': 'ground-counts-vantage-point-observation.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/ground-counts-vantage-point-observations/:id',
      'handler': 'ground-counts-vantage-point-observation.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/ground-counts-vantage-point-observations',
      'handler': 'ground-counts-vantage-point-observation.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/ground-counts-vantage-point-observations/:id',
      'handler': 'ground-counts-vantage-point-observation.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/ground-counts-vantage-point-observations/:id',
      'handler': 'ground-counts-vantage-point-observation.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
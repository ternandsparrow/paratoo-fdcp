module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/vantage-point-conduct-surveys',
      'handler': 'vantage-point-conduct-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/vantage-point-conduct-surveys/:id',
      'handler': 'vantage-point-conduct-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/vantage-point-conduct-surveys',
      'handler': 'vantage-point-conduct-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/vantage-point-conduct-surveys/:id',
      'handler': 'vantage-point-conduct-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/vantage-point-conduct-surveys/:id',
      'handler': 'vantage-point-conduct-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
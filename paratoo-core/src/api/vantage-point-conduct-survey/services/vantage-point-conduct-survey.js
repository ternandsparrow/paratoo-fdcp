'use strict'

/**
 * vantage-point-conduct-survey service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::vantage-point-conduct-survey.vantage-point-conduct-survey')

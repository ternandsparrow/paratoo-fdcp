module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-basal-sweep-sampling-points',
      'handler': 'lut-basal-sweep-sampling-point.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-basal-sweep-sampling-points/:id',
      'handler': 'lut-basal-sweep-sampling-point.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-basal-sweep-sampling-point.lut-basal-sweep-sampling-point')
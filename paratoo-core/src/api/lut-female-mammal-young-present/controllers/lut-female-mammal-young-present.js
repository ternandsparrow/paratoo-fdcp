'use strict'

/**
 *  lut-female-mammal-young-present controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-female-mammal-young-present.lut-female-mammal-young-present')

'use strict'

/**
 * lut-female-mammal-young-present service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-female-mammal-young-present.lut-female-mammal-young-present')

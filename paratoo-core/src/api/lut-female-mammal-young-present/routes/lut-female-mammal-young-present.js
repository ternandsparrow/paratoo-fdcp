module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-female-mammal-young-presents',
      'handler': 'lut-female-mammal-young-present.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-female-mammal-young-presents/:id',
      'handler': 'lut-female-mammal-young-present.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
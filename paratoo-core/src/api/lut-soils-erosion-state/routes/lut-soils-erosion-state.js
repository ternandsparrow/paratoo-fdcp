module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-erosion-states',
      'handler': 'lut-soils-erosion-state.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-erosion-states/:id',
      'handler': 'lut-soils-erosion-state.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
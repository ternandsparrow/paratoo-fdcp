const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-erosion-state.lut-soils-erosion-state')
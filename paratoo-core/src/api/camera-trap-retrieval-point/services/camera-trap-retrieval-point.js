'use strict'

/**
 * camera-trap-retrieval-point service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::camera-trap-retrieval-point.camera-trap-retrieval-point')

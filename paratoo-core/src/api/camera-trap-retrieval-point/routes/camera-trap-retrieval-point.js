module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/camera-trap-retrieval-points',
      'handler': 'camera-trap-retrieval-point.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/camera-trap-retrieval-points/:id',
      'handler': 'camera-trap-retrieval-point.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/camera-trap-retrieval-points',
      'handler': 'camera-trap-retrieval-point.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/camera-trap-retrieval-points/:id',
      'handler': 'camera-trap-retrieval-point.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/camera-trap-retrieval-points/:id',
      'handler': 'camera-trap-retrieval-point.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
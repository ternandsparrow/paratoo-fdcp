module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-drone-scanning-modes',
      'handler': 'lut-drone-scanning-mode.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-drone-scanning-modes/:id',
      'handler': 'lut-drone-scanning-mode.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
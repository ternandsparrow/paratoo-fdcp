'use strict'

/**
 *  lut-drone-scanning-mode controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-drone-scanning-mode.lut-drone-scanning-mode')

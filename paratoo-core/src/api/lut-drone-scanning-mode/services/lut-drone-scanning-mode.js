'use strict'

/**
 * lut-drone-scanning-mode service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-drone-scanning-mode.lut-drone-scanning-mode')

'use strict'

/**
 * lut-condition-disturbance-category service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-condition-disturbance-category.lut-condition-disturbance-category')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-condition-disturbance-categories',
      'handler': 'lut-condition-disturbance-category.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-condition-disturbance-categories/:id',
      'handler': 'lut-condition-disturbance-category.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
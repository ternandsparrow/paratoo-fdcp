'use strict'

/**
 *  lut-condition-disturbance-category controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-condition-disturbance-category.lut-condition-disturbance-category')

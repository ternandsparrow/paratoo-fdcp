'use strict'

/**
 * soil-horizon-cutan service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-horizon-cutan.soil-horizon-cutan')

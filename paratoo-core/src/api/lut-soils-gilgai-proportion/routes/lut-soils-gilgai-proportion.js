module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-gilgai-proportions',
      'handler': 'lut-soils-gilgai-proportion.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-gilgai-proportions/:id',
      'handler': 'lut-soils-gilgai-proportion.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
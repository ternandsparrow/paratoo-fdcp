const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-structure-size.lut-soils-structure-size')
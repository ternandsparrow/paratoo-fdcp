module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-structure-sizes',
      'handler': 'lut-soils-structure-size.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-structure-sizes/:id',
      'handler': 'lut-soils-structure-size.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
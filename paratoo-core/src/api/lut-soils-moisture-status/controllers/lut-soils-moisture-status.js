const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-moisture-status.lut-soils-moisture-status')
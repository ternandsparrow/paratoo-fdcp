module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-moisture-statuss',
      'handler': 'lut-soils-moisture-status.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-moisture-statuss/:id',
      'handler': 'lut-soils-moisture-status.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-surface-soil-conditions',
      'handler': 'lut-soils-surface-soil-condition.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-surface-soil-conditions/:id',
      'handler': 'lut-soils-surface-soil-condition.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-targeted-trap-types',
      'handler': 'lut-targeted-trap-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-targeted-trap-types/:id',
      'handler': 'lut-targeted-trap-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-targeted-trap-type service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-targeted-trap-type.lut-targeted-trap-type')

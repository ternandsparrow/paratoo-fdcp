'use strict'

/**
 * lut-targeted-trap-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-targeted-trap-type.lut-targeted-trap-type')

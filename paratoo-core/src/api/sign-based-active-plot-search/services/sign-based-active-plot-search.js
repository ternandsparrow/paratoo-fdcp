'use strict'

/**
 * sign-based-active-plot-search service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::sign-based-active-plot-search.sign-based-active-plot-search')

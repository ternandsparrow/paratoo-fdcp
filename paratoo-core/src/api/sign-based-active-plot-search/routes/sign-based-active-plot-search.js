module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/sign-based-active-plot-searches',
      'handler': 'sign-based-active-plot-search.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/sign-based-active-plot-searches/:id',
      'handler': 'sign-based-active-plot-search.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/sign-based-active-plot-searches',
      'handler': 'sign-based-active-plot-search.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/sign-based-active-plot-searches/:id',
      'handler': 'sign-based-active-plot-search.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/sign-based-active-plot-searches/:id',
      'handler': 'sign-based-active-plot-search.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

const uuid = require('uuid')
const PDFDocument = require('pdfkit')
const blobStream = require('blob-stream')
const QRCode = require('qrcode')

const COLUMNS_PER_PAGE = 2
const ROWS_PER_PAGE = 7
//set by URL param (if it's provided)
let DRAW_BARCODE_LABEL_MARGINS = null

const opts = {
  errorCorrectionLevel: 'L',
  type: 'image/jpeg',
  quality: 1,
  margin: 0,
}

/**
 * generate-barcode controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController(
  'api::generate-barcode.generate-barcode',
  ({ strapi }) => ({
    /**
     * Generates a PDF of barcodes based on a given project code
     *
     * URL param project_code
     * URL param num_pages
     *
     * @param {*} ctx Koa context
     *
     * @returns PDF file of barcodes
     */
    async generateBarcodes(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      helpers.paratooDebugMsg(
        `generateBarcodes - ctx request-url is: ${ctx.request.url}`,
        true,
      )
      helpers.paratooDebugMsg(
        `generateBarcodes - ctx request-header-authorization is: ${ctx.request.header.authorization}`,
        true,
      )
      helpers.paratooDebugMsg(
        `generateBarcodes - ctx request-header-host is: ${ctx.request.header.host}`,
        true,
      )

      helpers.paratooDebugMsg(
        `generateBarcodes - ctx app-env is: ${ctx.app.env}`,
        true,
      )

      const requestParams = helpers.getRequestParams(ctx)

      let projectCode = null
      let numPages = null
      let program = null
      DRAW_BARCODE_LABEL_MARGINS = false
      for (const param of requestParams || []) {
        if (param.startsWith('project_code')) {
          projectCode = param.split('=')[1]
        } else if (param.startsWith('num_pages')) {
          numPages = param.split('=')[1]
        } else if (param.startsWith('program')) {
          program = param.split('=')[1]
        } else if (param.startsWith('draw_margins')) {
          const drawMargins = param.split('=')[1].toLowerCase()
          if (drawMargins !== 'true' && drawMargins !== 'false') {
            return ctx.badRequest(
              `draw_margins must be 'true' or 'false'. Provided '${drawMargins}'`,
            )
          }
          DRAW_BARCODE_LABEL_MARGINS = JSON.parse(drawMargins)
        }
      }

      const missingParams = []
      if (!projectCode) {
        missingParams.push('project_code')
      }
      if (!numPages) {
        missingParams.push('num_pages')
      }
      if (!program) {
        missingParams.push('program')
      }
      if (missingParams.length > 0) {
        return ctx.badRequest(`Missing params: ${missingParams.join(', ')}`)
      }

      const programLut = await strapi.entityService.findMany(
        'api::lut-program.lut-program',
      )
      //`findOne` expects the ID, but we've only got the symbol, so need to manually find
      //it (also so that we can pass to `generateBarcode`)
      const specifiedProgram = programLut.find((p) => p.symbol === program)
      if (!specifiedProgram) {
        return ctx.badRequest(`Program '${program}' not found`)
      }

      const relevantProject = await getProject(
        projectCode,
        ctx.request.headers.authorization,
      )

      const isProjAdmin = await isProjectAdmin({
        relevantProject,
      })
      if (isProjAdmin === 'not assigned') {
        return ctx.unauthorized(
          `User is not assigned to the project '${projectCode}'`,
        )
      } else if (!isProjAdmin) {
        return ctx.unauthorized('User is not a project admin')
      }

      helpers.paratooDebugMsg(
        `generateBarcodes - projectCode is: ${JSON.stringify(
          projectCode,
        )}\n numPages is: ${JSON.stringify(
          numPages,
        )}\n program is: ${JSON.stringify(program)}`,
        true,
      )
      /*
      - page 1
      [
        - row 1
        [
          {
            "barcode_text": "barcode1",
            "qr_url": "<base64 encoded QR image>",
          },
          {
            "barcode_text": "barcode2",
            "qr_url": "<base64 encoded QR image>",
          },
        ],
        - rows 2 to 7
        ...
      ],
      - page 2
      [
        - row 1
        [
          {
            "barcode_text": "barcode15",
            "qr_url": "<base64 encoded QR image>",
          },
          {
            "barcode_text": "barcode16",
            "qr_url": "<base64 encoded QR image>",
          },
        ],
        - rows 2 to 7
        ...
      ],
      - subsequent pages (based on num_pages param)
      ...
      */
      const generatedBarcodes = []
      for (let p = 0; p < numPages; p++) {
        generatedBarcodes[p] = []
        for (let r = 0; r < ROWS_PER_PAGE; r++) {
          generatedBarcodes[p][r] = []
          for (let c = 0; c < COLUMNS_PER_PAGE; c++) {
            generatedBarcodes[p][r][c] = await generateBarcode({
              relevantProject,
              program: specifiedProgram,
            })
            const qrUrl = await qrDataUrl(
              generatedBarcodes[p][r][c].barcode_text,
              opts,
            )
            generatedBarcodes[p][r][c].qr_url = qrUrl
          }
        }
      }

      helpers.paratooDebugMsg(
        `generateBarcodes - generatedBarcodes array is: ${JSON.stringify(
          generatedBarcodes,
        )}}`,
        true,
      )

      ctx.response.attachment(`${projectCode}.pdf`)
      ctx.response.status = 200
      ctx.body = await generatePDF(ctx, relevantProject, generatedBarcodes)
      helpers.paratooDebugMsg(
        `generateBarcodes - ctx (before return) response-status is: ${ctx.response.status}`,
        true,
      )
      return ctx
    },
  }),
)

async function qrDataUrl(text, opts) {
  try {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    helpers.paratooDebugMsg(
      `qrDataUrl - text is: ${JSON.stringify(text)}\n opts is: ${JSON.stringify(
        opts,
      )}`,
      true,
    )
    return await QRCode.toDataURL(text, opts)
  } catch (err) {
    console.error(err)
  }
}

function truncateProjectTitle(projectText) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  helpers.paratooDebugMsg(
    `truncateProjectTitle - projectText is: ${JSON.stringify(projectText)}`,
    true,
  )
  if (projectText.length > 20) {
    return `${projectText.slice(0, 20).trim()}...`
  }
  return projectText
}

async function generatePDF(ctx, relevantProject, generatedBarcodes) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  helpers.paratooDebugMsg(
    `generatePDF - relevantProject is: ${JSON.stringify(
      relevantProject,
    )}\n generatedBarcodes is: ${JSON.stringify(generatedBarcodes)}`,
    true,
  )
  const doc = new PDFDocument({ size: 'A4' })
  const stream = doc.pipe(blobStream())
  const cellWidth = 280.91 // 99.1mm
  const cellHeight = 108 //38.1mm
  const marginY = 42.52 //15mm

  const marginMiddle = 7.94 //2.8mm
  const marginX = 14.17 //0.5mm
  const projectTitleFontSize = 11
  let barcodeTextFontSize = 20
  const rectWidth = cellWidth - marginY - cellHeight + marginX + 10
  const rectHeight = 20 * 2.5 + 6

  const pageNum = generatedBarcodes.length

  for (let p = 0; p < pageNum; p++) {
    if (p != 0) {
      doc.addPage({ size: 'A4' })
    }

    for (let r = 0; r < ROWS_PER_PAGE; r++) {
      for (let c = 0; c < COLUMNS_PER_PAGE; c++) {
        const x =
          c % 2 !== 0
            ? marginX + c * cellWidth + marginMiddle
            : marginX + c * cellWidth
        const y = marginY + r * cellHeight

        // doc.rect(x, y, cellWidth, cellHeight).stroke('black')

        if (DRAW_BARCODE_LABEL_MARGINS) {
          doc.rect(x, y, cellWidth, cellHeight).stroke('black')
        }

        // Add QR Code to document
        doc.image(generatedBarcodes[p][r][c].qr_url, x + marginX, y + marginX, {
          fit: [cellHeight - marginX * 2, cellHeight - marginX * 2],
        })

        const projectNameX = x + cellHeight // Adjust the x-coordinate for project Name
        const projectNameY = y + marginX // Adjust the y-coordinate for project Name
        const textWidth = cellWidth - cellHeight - marginX - marginY

        const projectProgramText = `${truncateProjectTitle(
          relevantProject.name,
        )}/${generatedBarcodes[p][r][c].program.symbol}`

        doc
          .fontSize(projectTitleFontSize)
          .fillColor('#000000')
          .text(projectProgramText, projectNameX, projectNameY, {
            width: textWidth,
            height: projectTitleFontSize * 3,
          })

        const barcodeTextY = projectNameY + marginX + projectTitleFontSize // Adjust the y-coordinate for additional text

        doc.image(
          'src/asset/monitor.png',
          projectNameX + textWidth,
          projectNameY,
          {
            fit: [(cellHeight - marginX * 2) / 2, 2 * marginX - 8],
            align: 'center',
            valign: 'center',
          },
        )

        //test long barcode text
        // generatedBarcodes[p][r][c].barcode_text =
        // 'SNS-PPP-MU41-RP1-QWER-ERTG-JUY5-BFN-EKROWER-FJDIGTRHU-000000'

        const textStartY = barcodeTextY + projectTitleFontSize - 8
        // border of barcode text
        // doc.rect(projectNameX, textStartY, rectWidth, rectHeight).stroke()

        const calculateTextHeight = (fontSize, text, width) => {
          doc.fontSize(fontSize)
          const textHeight = doc.heightOfString(text, {
            width,
          })
          return textHeight
        }

        let textHeight = calculateTextHeight(
          barcodeTextFontSize,
          generatedBarcodes[p][r][c].barcode_text,
          rectWidth,
        )

        while (textHeight > rectHeight && barcodeTextFontSize > 0) {
          barcodeTextFontSize--
          textHeight = calculateTextHeight(
            barcodeTextFontSize,
            generatedBarcodes[p][r][c].barcode_text,
            rectWidth,
          )
        }

        doc
          .fontSize(barcodeTextFontSize)
          .text(
            `${generatedBarcodes[p][r][c].barcode_text}`,
            projectNameX,
            textStartY,
            {
              width: rectWidth,
              height: rectHeight,
              lineGap: 0,
            },
          )
      }
    }
  }
  return new Promise((resolve) => {
    doc.pipe(ctx.res)
    doc.end()
    stream.on('finish', function () {
      stream.toBlobURL('application/pdf')
      const url = stream.toBlobURL('application/pdf')
      resolve(url)
    })
  })
}

/**
 * Generates a single barcode based on a given project code and stores it to prevent
 * future collisions of barcodes for the project. The barcode is in the format
 * <project_code><barcode_suffix>, where the barcode_suffix is auto-incremented based
 * on the stored barcodes for that project. If the project_code is a UUID, then we grab
 * just the last 3 characters
 *
 * @param {Object} relevantProject the project entry from /uses-projects, containing the
 * `id` (UUID, which is the `project_code`)
 * @param {Object} program the entry from the lut-program LUT
 *
 * @returns {String} a barcode
 */
async function generateBarcode({ relevantProject, program }) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  helpers.paratooDebugMsg(
    `generateBarcode - relevantProject is: ${JSON.stringify(
      relevantProject,
    )}\n program is: ${JSON.stringify(program)}`,
    true,
  )
  let parsedProjectCode = null
  //TODO should probably move these warnings and errors before the call to generateBarcode, so that we can do the check once and warn/error once
  if (uuid.validate(relevantProject.id)) {
    //it's a UUID (probably MERIT project), so grab grant ID or last 4 characters of UUID
    //if grant ID not available
    if (!relevantProject?.grantID) {
      helpers.paratooWarnHandler(
        `No grant ID found for project with ID '${relevantProject.id}'. Falling back to last 4 digits of UUID`,
      )
      parsedProjectCode = relevantProject.id.slice(-4).toUpperCase()
    } else {
      parsedProjectCode = relevantProject.grantID
    }
  } else if (relevantProject.id) {
    parsedProjectCode = String(relevantProject.id).toUpperCase()
  } else {
    helpers.paratooErrorHandler(500, new Error('No project code found'))
  }

  const fields = [
    'project_code',
    'barcode_prefix',
    'barcode_suffix',
    'barcode_text',
  ]

  const existingBarcodesForProject = await strapi.entityService.findMany(
    'api::generate-barcode.generate-barcode',
    {
      fields: fields,
      filters: { project_code: parsedProjectCode },
      sort: { barcode_suffix: 'DESC' },
    },
  )

  helpers.paratooDebugMsg(
    `existingBarcodesForProject - existingBarcodesForProject.length is: ${existingBarcodesForProject.length}`,
    true,
  )

  let barcodeSuffix = null
  if (existingBarcodesForProject.length > 0) {
    //first item in the array is the highest barcode suffix number (as we sorted
    //descending), so convert to int so we can increment
    const newBarcodeSuffixNum =
      Number.parseInt(existingBarcodesForProject[0].barcode_suffix) + 1
    barcodeSuffix = newBarcodeSuffixNum.toString().padStart(6, '0')
  } else {
    barcodeSuffix = '0'.padStart(6, '0')
  }
  const createdBarcode = await strapi.entityService.create(
    'api::generate-barcode.generate-barcode',
    {
      data: {
        project_code: parsedProjectCode,
        barcode_prefix: parsedProjectCode,
        barcode_suffix: barcodeSuffix,
        program: program.id,
        barcode_text: `${parsedProjectCode}-${barcodeSuffix}`,
      },
      fields: fields,
      populate: {
        program: {
          fields: ['symbol', 'label'],
        },
      },
    },
  )

  return createdBarcode
}

async function getProject(project_code, authToken) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  helpers.paratooDebugMsg(
    `getProject - project_code is: ${JSON.stringify(
      project_code,
    )}\n authToken is: ${JSON.stringify(authToken)}`,
    true,
  )

  const userProjects = await helpers.getAllUserProjects(authToken)
  if (!userProjects) {
    helpers.paratooErrorHandler(500, new Error('Failed to get user projects'))
  }
  const relevantProject = userProjects.data.projects.find(
    (o) => o.id == project_code,
  )
  return relevantProject
}

/**
 * Determines if the user is a project admin of the given project by hitting org's
 * /user-projects endpoint
 *
 * @param {Object} relevantProject the project entry from /uses-projects
 *
 * @returns {Boolean} whether the user is a project admin
 */
async function isProjectAdmin({ relevantProject }) {
  if (!relevantProject) {
    return 'not assigned'
  }
  return relevantProject.role === 'project_admin'
}

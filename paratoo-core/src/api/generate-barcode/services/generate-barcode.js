'use strict'

/**
 * generate-barcode service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::generate-barcode.generate-barcode')

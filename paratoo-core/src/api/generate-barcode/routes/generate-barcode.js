module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/generate-barcode',
      'handler': 'generate-barcode.generateBarcodes',
      'config': {
        'policies': [
          //TODO investigate why validator won't work (even though docco is overridden to include URL params)
          // 'global::is-validated',
          //will check if authenticated but not if authorised (as a GET to a custom
          //endpoint will not be able to leverage the PDP that requires project and
          //protocol information). Authorisation will be checked in the controller
          'global::projectMembershipEnforcer',
        ],
      },
      'allowMethod': true,
    },
  ]
}
'use strict'

/**
 * lut-cwd-cut-off-length service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-cwd-cut-off-length.lut-cwd-cut-off-length')

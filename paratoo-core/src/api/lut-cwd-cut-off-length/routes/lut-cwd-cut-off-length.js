module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-cwd-cut-off-lengths',
      'handler': 'lut-cwd-cut-off-length.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-cwd-cut-off-lengths/:id',
      'handler': 'lut-cwd-cut-off-length.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-cwd-cut-off-length controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-cwd-cut-off-length.lut-cwd-cut-off-length')

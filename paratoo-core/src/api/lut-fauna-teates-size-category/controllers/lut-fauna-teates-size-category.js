'use strict'

/**
 *  lut-fauna-teates-size-category controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-teates-size-category.lut-fauna-teates-size-category')

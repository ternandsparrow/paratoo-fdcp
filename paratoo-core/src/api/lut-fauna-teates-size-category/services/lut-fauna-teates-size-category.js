'use strict'

/**
 * lut-fauna-teates-size-category service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-fauna-teates-size-category.lut-fauna-teates-size-category')

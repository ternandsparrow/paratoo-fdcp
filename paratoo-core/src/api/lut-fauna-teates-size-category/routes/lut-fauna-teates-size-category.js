module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-teates-size-categories',
      'handler': 'lut-fauna-teates-size-category.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-teates-size-categories/:id',
      'handler': 'lut-fauna-teates-size-category.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 *  opportunistic-observation controller
 */
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController(
  'api::opportunistic-observation.opportunistic-observation',
  ({ strapi }) => ({
    /* Generic find controller with pdp data filter */
    async find(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      
      const authToken = ctx.request.headers['authorization']
      const modelName = ctx.state.route.info.apiName
      const tokenStatus = await helpers.apiTokenCheck(authToken)
      const useDefaultController = ctx.request.query['use-default'] === 'true'
      
      // if we need to use default controller (use magic jwt and use-default flag)
      if (tokenStatus?.type == 'full-access' && useDefaultController) {
        helpers.paratooDebugMsg(`Default controller is used to populate model: ${modelName}`)
        return await super.find(ctx)
      }
      return await helpers.pdpDataFilter(
        modelName,
        authToken,
        ctx.request.query,
        tokenStatus,
      )
    },

    async create(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      const requestData = ctx.request.body.data
      const authToken = ctx.request.headers['authorization']
      const entry = await strapi.entityService.create(
        'api::opportunistic-observation.opportunistic-observation',
        {
          data: requestData,
          populate: '*',
        },
      )
      ctx.body = entry
      // POST collection to org to "Notify paratoo-org about submission"
      const result = await helpers.sendCollectionIdentifierToOrg(
        requestData.orgMintedIdentifier, 
        authToken, 
        entry,
        requestData.collectionIdSubmitted
      )
      if (result.error) {
        return helpers.paratooErrorHandler(
          400,
          new Error(JSON.stringify(result.error)),
        )
      }
      return entry
    },
    /**
     * Uploads an array of observations
     *
     * @param {Object} ctx Koa context
     * @returns {Array} successfully-created array of observations
     */
    async many(ctx) {
      const manyModelName = 'opportunistic-observation'

      return strapi.service('api::paratoo-helper.paratoo-helper').genericMany({
        ctx: ctx,
        manyModelName: manyModelName,
      })
    },
  }),
)

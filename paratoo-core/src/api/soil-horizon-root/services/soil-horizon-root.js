'use strict'

/**
 * soil-horizon-root service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-horizon-root.soil-horizon-root')

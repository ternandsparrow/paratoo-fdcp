const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-habitat.lut-habitat')
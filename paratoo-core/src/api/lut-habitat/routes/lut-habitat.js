module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-habitats',
      'handler': 'lut-habitat.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-habitats/:id',
      'handler': 'lut-habitat.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-sheet-water-erosion-degree.lut-soils-sheet-water-erosion-degree')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-sheet-water-erosion-degrees',
      'handler': 'lut-soils-sheet-water-erosion-degree.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-sheet-water-erosion-degrees/:id',
      'handler': 'lut-soils-sheet-water-erosion-degree.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
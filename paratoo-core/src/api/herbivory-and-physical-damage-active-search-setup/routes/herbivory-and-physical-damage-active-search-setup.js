module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/herbivory-and-physical-damage-active-search-setups',
      'handler': 'herbivory-and-physical-damage-active-search-setup.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/herbivory-and-physical-damage-active-search-setups/:id',
      'handler': 'herbivory-and-physical-damage-active-search-setup.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/herbivory-and-physical-damage-active-search-setups',
      'handler': 'herbivory-and-physical-damage-active-search-setup.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/herbivory-and-physical-damage-active-search-setups/:id',
      'handler': 'herbivory-and-physical-damage-active-search-setup.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/herbivory-and-physical-damage-active-search-setups/:id',
      'handler': 'herbivory-and-physical-damage-active-search-setup.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/herbivory-and-physical-damage-active-search-setups/bulk',
      'handler': 'herbivory-and-physical-damage-active-search-setup.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
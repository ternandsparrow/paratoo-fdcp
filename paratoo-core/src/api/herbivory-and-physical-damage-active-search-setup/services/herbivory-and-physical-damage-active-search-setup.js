'use strict'

/**
 * herbivory-and-physical-damage-active-search-setup service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::herbivory-and-physical-damage-active-search-setup.herbivory-and-physical-damage-active-search-setup')

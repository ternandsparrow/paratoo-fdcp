module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-structure-compound-pedalities',
      'handler': 'lut-soils-structure-compound-pedality.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-structure-compound-pedalities/:id',
      'handler': 'lut-soils-structure-compound-pedality.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
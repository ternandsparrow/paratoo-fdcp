const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-structure-compound-pedality.lut-soils-structure-compound-pedality')
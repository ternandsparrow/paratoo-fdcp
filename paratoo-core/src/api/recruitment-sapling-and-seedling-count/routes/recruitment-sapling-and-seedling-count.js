module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/recruitment-sapling-and-seedling-counts',
      'handler': 'recruitment-sapling-and-seedling-count.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/recruitment-sapling-and-seedling-counts/:id',
      'handler': 'recruitment-sapling-and-seedling-count.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/recruitment-sapling-and-seedling-counts',
      'handler': 'recruitment-sapling-and-seedling-count.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/recruitment-sapling-and-seedling-counts/:id',
      'handler': 'recruitment-sapling-and-seedling-count.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/recruitment-sapling-and-seedling-counts/:id',
      'handler': 'recruitment-sapling-and-seedling-count.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * recruitment-sapling-and-seedling-count service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::recruitment-sapling-and-seedling-count.recruitment-sapling-and-seedling-count')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-horizon-boundary-distinctnesss',
      'handler': 'lut-soils-horizon-boundary-distinctness.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-horizon-boundary-distinctnesss/:id',
      'handler': 'lut-soils-horizon-boundary-distinctness.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
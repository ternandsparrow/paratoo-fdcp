const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-horizon-boundary-distinctness.lut-soils-horizon-boundary-distinctness')
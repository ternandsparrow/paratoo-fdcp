'use strict'

/**
 *  lut-status-of-helicopter-door controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-status-of-helicopter-door.lut-status-of-helicopter-door')

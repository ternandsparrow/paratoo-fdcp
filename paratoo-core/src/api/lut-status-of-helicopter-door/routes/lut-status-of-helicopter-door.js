module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-status-of-helicopter-doors',
      'handler': 'lut-status-of-helicopter-door.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-status-of-helicopter-doors/:id',
      'handler': 'lut-status-of-helicopter-door.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-status-of-helicopter-door service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-status-of-helicopter-door.lut-status-of-helicopter-door')

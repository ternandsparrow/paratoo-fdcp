const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-colour-value.lut-soils-colour-value')
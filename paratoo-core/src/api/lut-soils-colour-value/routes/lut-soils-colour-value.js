module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-colour-values',
      'handler': 'lut-soils-colour-value.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-colour-values/:id',
      'handler': 'lut-soils-colour-value.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
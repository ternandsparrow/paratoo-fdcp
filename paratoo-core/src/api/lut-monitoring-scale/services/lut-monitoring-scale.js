'use strict'

/**
 * lut-monitoring-scale service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-monitoring-scale.lut-monitoring-scale')

'use strict'

/**
 * lut-monitoring-scale controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-monitoring-scale.lut-monitoring-scale')

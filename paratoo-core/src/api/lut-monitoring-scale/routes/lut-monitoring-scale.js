module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-monitoring-scales',
      'handler': 'lut-monitoring-scale.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-monitoring-scales/:id',
      'handler': 'lut-monitoring-scale.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
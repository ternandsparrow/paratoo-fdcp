const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-texture-grade.lut-soils-texture-grade')
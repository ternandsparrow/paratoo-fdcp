module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-texture-grades',
      'handler': 'lut-soils-texture-grade.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-texture-grades/:id',
      'handler': 'lut-soils-texture-grade.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
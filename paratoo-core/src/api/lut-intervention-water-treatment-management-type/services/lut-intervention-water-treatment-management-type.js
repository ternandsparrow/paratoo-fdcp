'use strict'

/**
 * lut-intervention-water-treatment-management-type service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-water-treatment-management-type.lut-intervention-water-treatment-management-type')

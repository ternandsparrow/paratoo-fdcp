module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-water-treatment-management-types',
      'handler': 'lut-intervention-water-treatment-management-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-water-treatment-management-types/:id',
      'handler': 'lut-intervention-water-treatment-management-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
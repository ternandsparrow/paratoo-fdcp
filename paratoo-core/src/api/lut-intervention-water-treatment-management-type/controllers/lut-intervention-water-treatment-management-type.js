'use strict'

/**
 * lut-intervention-water-treatment-management-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-water-treatment-management-type.lut-intervention-water-treatment-management-type')

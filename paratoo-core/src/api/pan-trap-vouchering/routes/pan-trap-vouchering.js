module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/pan-trap-voucherings',
      'handler': 'pan-trap-vouchering.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/pan-trap-voucherings/:id',
      'handler': 'pan-trap-vouchering.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/pan-trap-voucherings',
      'handler': 'pan-trap-vouchering.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/pan-trap-voucherings/:id',
      'handler': 'pan-trap-vouchering.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/pan-trap-voucherings/:id',
      'handler': 'pan-trap-vouchering.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
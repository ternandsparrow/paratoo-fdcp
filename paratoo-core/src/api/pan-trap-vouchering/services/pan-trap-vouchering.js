'use strict'

/**
 * pan-trap-vouchering service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::pan-trap-vouchering.pan-trap-vouchering')

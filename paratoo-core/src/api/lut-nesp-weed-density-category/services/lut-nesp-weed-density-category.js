'use strict'

/**
 * lut-nesp-weed-density-category service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-nesp-weed-density-category.lut-nesp-weed-density-category')

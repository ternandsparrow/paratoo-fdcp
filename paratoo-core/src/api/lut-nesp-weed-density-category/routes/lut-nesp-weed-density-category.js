module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-nesp-weed-density-categories',
      'handler': 'lut-nesp-weed-density-category.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-nesp-weed-density-categories/:id',
      'handler': 'lut-nesp-weed-density-category.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
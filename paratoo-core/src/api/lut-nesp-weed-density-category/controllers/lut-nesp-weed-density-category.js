'use strict'

/**
 * lut-nesp-weed-density-category controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-nesp-weed-density-category.lut-nesp-weed-density-category')

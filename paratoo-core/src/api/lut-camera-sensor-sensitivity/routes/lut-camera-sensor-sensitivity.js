module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-camera-sensor-sensitivities',
      'handler': 'lut-camera-sensor-sensitivity.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-camera-sensor-sensitivities/:id',
      'handler': 'lut-camera-sensor-sensitivity.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
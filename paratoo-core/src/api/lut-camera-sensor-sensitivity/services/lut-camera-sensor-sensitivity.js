'use strict'

/**
 * lut-camera-sensor-sensitivity service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-camera-sensor-sensitivity.lut-camera-sensor-sensitivity')

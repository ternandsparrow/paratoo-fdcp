'use strict'

/**
 *  lut-camera-sensor-sensitivity controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-camera-sensor-sensitivity.lut-camera-sensor-sensitivity')

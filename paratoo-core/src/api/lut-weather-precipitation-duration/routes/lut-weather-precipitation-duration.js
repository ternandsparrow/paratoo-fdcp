module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-weather-precipitation-durations',
      'handler': 'lut-weather-precipitation-duration.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-weather-precipitation-durations/:id',
      'handler': 'lut-weather-precipitation-duration.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
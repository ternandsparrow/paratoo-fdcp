const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-weather-precipitation-duration.lut-weather-precipitation-duration')
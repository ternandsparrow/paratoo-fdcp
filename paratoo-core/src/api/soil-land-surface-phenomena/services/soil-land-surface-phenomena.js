'use strict'

/**
 * land-surface-phenomena service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-land-surface-phenomena.soil-land-surface-phenomena')

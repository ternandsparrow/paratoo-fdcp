module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-drone-sensors',
      'handler': 'lut-drone-sensor.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-drone-sensors/:id',
      'handler': 'lut-drone-sensor.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
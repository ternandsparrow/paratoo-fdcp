'use strict'

/**
 *  lut-drone-sensor controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-drone-sensor.lut-drone-sensor')

'use strict'

/**
 * lut-drone-sensor service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-drone-sensor.lut-drone-sensor')

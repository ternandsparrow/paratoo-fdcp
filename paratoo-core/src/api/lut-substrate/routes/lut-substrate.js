module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-substrates',
      'handler': 'lut-substrate.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-substrates/:id',
      'handler': 'lut-substrate.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
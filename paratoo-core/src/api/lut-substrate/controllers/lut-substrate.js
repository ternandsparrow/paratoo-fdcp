'use strict'

/**
 *  lut-substrate controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-substrate.lut-substrate')

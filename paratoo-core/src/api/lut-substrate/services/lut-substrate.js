'use strict'

/**
 * lut-substrate service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-substrate.lut-substrate')

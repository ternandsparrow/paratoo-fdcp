module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-horizon-suffixs',
      'handler': 'lut-soils-horizon-suffix.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-horizon-suffixs/:id',
      'handler': 'lut-soils-horizon-suffix.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-horizon-suffix.lut-soils-horizon-suffix')
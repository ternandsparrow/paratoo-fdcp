module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/within-plot-belt-quadrats',
      'handler': 'within-plot-belt-quadrat.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/within-plot-belt-quadrats/:id',
      'handler': 'within-plot-belt-quadrat.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/within-plot-belt-quadrats',
      'handler': 'within-plot-belt-quadrat.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/within-plot-belt-quadrats/:id',
      'handler': 'within-plot-belt-quadrat.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/within-plot-belt-quadrats/:id',
      'handler': 'within-plot-belt-quadrat.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
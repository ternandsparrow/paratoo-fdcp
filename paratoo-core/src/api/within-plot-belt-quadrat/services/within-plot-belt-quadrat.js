'use strict'

/**
 * within-plot-belt-quadrat service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::within-plot-belt-quadrat.within-plot-belt-quadrat')

'use strict'

/**
 * lut-invertebrate-pan-placement service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-invertebrate-pan-placement.lut-invertebrate-pan-placement')

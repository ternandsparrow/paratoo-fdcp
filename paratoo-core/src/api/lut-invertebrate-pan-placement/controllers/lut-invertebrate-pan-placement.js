'use strict'

/**
 *  lut-invertebrate-pan-placement controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-invertebrate-pan-placement.lut-invertebrate-pan-placement')

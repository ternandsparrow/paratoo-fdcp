module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-invertebrate-pan-placements',
      'handler': 'lut-invertebrate-pan-placement.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-invertebrate-pan-placements/:id',
      'handler': 'lut-invertebrate-pan-placement.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
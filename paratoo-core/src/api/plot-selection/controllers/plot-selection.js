'use strict'
/**
 *  plot-selection controller
 */
// const schema = require('../content-types/plot-selection/schema.json')
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController(
  'api::plot-selection.plot-selection',
  ({ strapi }) => ({
    /* Generic find controller with pdp data filter */
    async find(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      
      const authToken = ctx.request.headers['authorization']
      const modelName = ctx.state.route.info.apiName
      const tokenStatus = await helpers.apiTokenCheck(authToken)
      const useDefaultController = ctx.request.query['use-default'] === 'true'
      
      // if we need to use default controller (use magic jwt and use-default flag)
      if (tokenStatus?.type == 'full-access' && useDefaultController) {
        helpers.paratooDebugMsg(`Default controller is used to populate model: ${modelName}`)
        return await super.find(ctx)
      }
      return await helpers.pdpDataFilter(
        modelName,
        authToken,
        ctx.request.query,
        tokenStatus,
      )
    },

    //TODO deprecate this endpoint (we use bulk on plot-selection-survey)
    async many(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      
      helpers.paratooWarnHandler('Deprecated Plot Selection /many endpoint has been hit. This is likely due to an old client that hasn\'t updated properly')
      
      const modelName = 'plot-selection'
      const surveyModelName = 'plot-selection-survey'
      const reservedPlotsToUpdate = []
      for (const plot of ctx?.request?.body?.data?.[`${modelName}s`] || []) {
        const plotName = plot?.data?.plot_label

        const existingPlotSelection = await strapi.db.query(`api::${modelName}.${modelName}`)
          .findOne({
            where: {
              plot_label: plotName,
            },
          })

        if (existingPlotSelection) {
          return helpers.paratooErrorHandler(
            400,
            new Error(`Cannot reserve plot with name ${plotName} as a Plot Selection with the same name already exists. Please contact support`),
          )
        }

        const reservedPlotNameToUpdate = await strapi.db.query('api::reserved-plot-label.reserved-plot-label')
          .findOne({
            where: {
              plot_label: plotName,
            },
          })

        if (reservedPlotNameToUpdate?.created_plot_selection) {
          return helpers.paratooErrorHandler(
            400,
            new Error(`Cannot reserve plot with name ${plotName} as a Plot Selection with the same name already exists. Please contact support`),
          )
        }

        if (reservedPlotNameToUpdate) {
          reservedPlotsToUpdate.push(reservedPlotNameToUpdate)
        } else {
          //we get here if the user has already queued a Plot Selection before the update
          //with the reserving of plots was applied, so need to create the entry.
          //but this might not work if the Plot already exists
          helpers.paratooWarnHandler(`Plot with name ${plotName} has not been reserved. Will attempt to reserve the plot now`)
          
          const reservedPlotResp = await strapi.db.query('api::reserved-plot-label.reserved-plot-label')
            .create({
              data: {
                plot_label: plotName,
              },
            })
            .catch((err) => {
              helpers.paratooErrorHandler(400, err)
            })
          if (reservedPlotResp) {
            reservedPlotsToUpdate.push(reservedPlotResp)
          }
          
        }
      }

      const createdPlotSelections = await helpers.genericMany({
        ctx: ctx,
        manyModelName: modelName,
        surveyModelName,
      })

      //want to track the plots that were reserved that were actually created.
      //in the future we might want to use this to free-up plots that were reserved but
      //never created
      for (const plot of reservedPlotsToUpdate) {
        const relevantCreatedPlotSelection = createdPlotSelections?.[0]?.[modelName]?.find(
          s => s.plot_label === plot.plot_label
        )
        await strapi.entityService.update(
          'api::reserved-plot-label.reserved-plot-label',
          plot.id,
          {
            data: {
              created_plot_selection: relevantCreatedPlotSelection.id,
            },
          },
        )
      }

      return createdPlotSelections
    },
  }),
)

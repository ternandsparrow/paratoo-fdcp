'use strict'

/**
 * lut-species-determiner controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-species-determiner.lut-species-determiner')

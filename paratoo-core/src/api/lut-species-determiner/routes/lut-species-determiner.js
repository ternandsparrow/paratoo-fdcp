module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-species-determiners',
      'handler': 'lut-species-determiner.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-species-determiners/:id',
      'handler': 'lut-species-determiner.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
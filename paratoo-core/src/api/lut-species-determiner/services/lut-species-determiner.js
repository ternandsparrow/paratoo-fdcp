'use strict'

/**
 * lut-species-determiner service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-species-determiner.lut-species-determiner')

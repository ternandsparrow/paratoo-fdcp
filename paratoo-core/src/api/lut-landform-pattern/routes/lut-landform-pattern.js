module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-landform-patterns',
      'handler': 'lut-landform-pattern.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-landform-patterns/:id',
      'handler': 'lut-landform-pattern.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
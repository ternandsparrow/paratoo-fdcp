const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-landform-pattern.lut-landform-pattern')
'use strict'

/**
 *  lut-aircraft-model controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-aircraft-model.lut-aircraft-model')

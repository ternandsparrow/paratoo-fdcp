module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-aircraft-models',
      'handler': 'lut-aircraft-model.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-aircraft-models/:id',
      'handler': 'lut-aircraft-model.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
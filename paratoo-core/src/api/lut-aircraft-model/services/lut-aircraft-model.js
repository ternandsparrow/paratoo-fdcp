'use strict'

/**
 * lut-aircraft-model service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-aircraft-model.lut-aircraft-model')

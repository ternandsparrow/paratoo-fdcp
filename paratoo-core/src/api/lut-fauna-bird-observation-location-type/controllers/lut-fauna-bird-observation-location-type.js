const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-bird-observation-location-type.lut-fauna-bird-observation-location-type')
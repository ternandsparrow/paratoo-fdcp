module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-bird-observation-location-types',
      'handler': 'lut-fauna-bird-observation-location-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-bird-observation-location-types/:id',
      'handler': 'lut-fauna-bird-observation-location-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
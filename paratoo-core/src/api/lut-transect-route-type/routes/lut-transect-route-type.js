module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-transect-route-types',
      'handler': 'lut-transect-route-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-transect-route-types/:id',
      'handler': 'lut-transect-route-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
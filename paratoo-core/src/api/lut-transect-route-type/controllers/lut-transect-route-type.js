'use strict'

/**
 *  lut-transect-route-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController(
  'api::lut-transect-route-type.lut-transect-route-type',
)

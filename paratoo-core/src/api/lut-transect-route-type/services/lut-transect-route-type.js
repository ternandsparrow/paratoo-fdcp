'use strict'

/**
 * lut-transect-route-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService(
  'api::lut-transect-route-type.lut-transect-route-type',
)

'use strict'

/**
 * malaise-biomass service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::malaise-biomass.malaise-biomass')

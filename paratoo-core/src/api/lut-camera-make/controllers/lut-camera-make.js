'use strict'

/**
 *  lut-camera-make controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-camera-make.lut-camera-make')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-camera-makes',
      'handler': 'lut-camera-make.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-camera-makes/:id',
      'handler': 'lut-camera-make.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
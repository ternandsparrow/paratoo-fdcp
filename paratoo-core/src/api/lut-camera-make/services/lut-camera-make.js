'use strict'

/**
 * lut-camera-make service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-camera-make.lut-camera-make')

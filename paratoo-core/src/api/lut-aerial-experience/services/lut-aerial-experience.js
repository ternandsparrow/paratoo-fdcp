'use strict'

/**
 * lut-aerial-experience service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-aerial-experience.lut-aerial-experience')

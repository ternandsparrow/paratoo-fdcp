'use strict'

/**
 *  lut-aerial-experience controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-aerial-experience.lut-aerial-experience')

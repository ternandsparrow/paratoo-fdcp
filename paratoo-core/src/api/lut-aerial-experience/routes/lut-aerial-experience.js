module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-aerial-experiences',
      'handler': 'lut-aerial-experience.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-aerial-experiences/:id',
      'handler': 'lut-aerial-experience.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
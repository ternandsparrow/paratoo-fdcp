module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vectors',
      'handler': 'lut-vector.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vectors/:id',
      'handler': 'lut-vector.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-vector service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vector.lut-vector')

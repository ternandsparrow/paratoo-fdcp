'use strict'

/**
 * lut-vector controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vector.lut-vector')

'use strict'

/**
 * cron-jobs-status service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::cron-jobs-status.cron-jobs-status')

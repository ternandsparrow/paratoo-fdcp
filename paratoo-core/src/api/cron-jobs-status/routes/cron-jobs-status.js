module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/cron-jobs-statuses',
      'handler': 'cron-jobs-status.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/cron-jobs-statuses/:id',
      'handler': 'cron-jobs-status.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/cron-jobs-statuses',
      'handler': 'cron-jobs-status.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/cron-jobs-statuses/:id',
      'handler': 'cron-jobs-status.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/cron-jobs-statuses/:id',
      'handler': 'cron-jobs-status.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
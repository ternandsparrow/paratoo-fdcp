'use strict'

/**
 * lut-condition-disturbance-temporal-scale service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-condition-disturbance-temporal-scale.lut-condition-disturbance-temporal-scale')

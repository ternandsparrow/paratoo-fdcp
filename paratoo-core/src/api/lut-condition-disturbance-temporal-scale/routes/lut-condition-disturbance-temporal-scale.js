module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-condition-disturbance-temporal-scales',
      'handler': 'lut-condition-disturbance-temporal-scale.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-condition-disturbance-temporal-scales/:id',
      'handler': 'lut-condition-disturbance-temporal-scale.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
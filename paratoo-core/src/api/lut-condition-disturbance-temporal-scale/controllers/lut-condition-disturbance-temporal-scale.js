'use strict'

/**
 *  lut-condition-disturbance-temporal-scale controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-condition-disturbance-temporal-scale.lut-condition-disturbance-temporal-scale')

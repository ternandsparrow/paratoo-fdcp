'use strict'

/**
 * ground-counts-vantage-point service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::ground-counts-vantage-point.ground-counts-vantage-point')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/ground-counts-vantage-points',
      'handler': 'ground-counts-vantage-point.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/ground-counts-vantage-points/:id',
      'handler': 'ground-counts-vantage-point.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/ground-counts-vantage-points',
      'handler': 'ground-counts-vantage-point.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/ground-counts-vantage-points/:id',
      'handler': 'ground-counts-vantage-point.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/ground-counts-vantage-points/:id',
      'handler': 'ground-counts-vantage-point.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
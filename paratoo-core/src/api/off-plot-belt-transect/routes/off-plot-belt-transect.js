module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/off-plot-belt-transects',
      'handler': 'off-plot-belt-transect.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/off-plot-belt-transects/:id',
      'handler': 'off-plot-belt-transect.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/off-plot-belt-transects',
      'handler': 'off-plot-belt-transect.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/off-plot-belt-transects/:id',
      'handler': 'off-plot-belt-transect.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/off-plot-belt-transects/:id',
      'handler': 'off-plot-belt-transect.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * off-plot-belt-transect service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::off-plot-belt-transect.off-plot-belt-transect')

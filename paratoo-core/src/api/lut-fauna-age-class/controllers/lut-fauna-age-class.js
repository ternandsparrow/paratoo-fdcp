const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-fauna-age-class.lut-fauna-age-class')
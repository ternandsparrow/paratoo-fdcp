module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-fauna-age-classes',
      'handler': 'lut-fauna-age-class.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-fauna-age-classes/:id',
      'handler': 'lut-fauna-age-class.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
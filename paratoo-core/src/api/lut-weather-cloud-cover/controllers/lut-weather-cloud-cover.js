const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-weather-cloud-cover.lut-weather-cloud-cover')
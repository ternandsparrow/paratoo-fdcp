module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-weather-cloud-covers',
      'handler': 'lut-weather-cloud-cover.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-weather-cloud-covers/:id',
      'handler': 'lut-weather-cloud-cover.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
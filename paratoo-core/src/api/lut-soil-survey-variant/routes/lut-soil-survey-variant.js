module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soil-survey-variants',
      'handler': 'lut-soil-survey-variant.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soil-survey-variants/:id',
      'handler': 'lut-soil-survey-variant.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
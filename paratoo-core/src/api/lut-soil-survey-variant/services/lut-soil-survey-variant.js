'use strict'

/**
 * lut-soil-survey-variant service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soil-survey-variant.lut-soil-survey-variant')

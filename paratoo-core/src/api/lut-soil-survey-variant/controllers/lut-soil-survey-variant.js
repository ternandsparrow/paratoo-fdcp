'use strict'

/**
 * lut-soil-survey-variant controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soil-survey-variant.lut-soil-survey-variant')

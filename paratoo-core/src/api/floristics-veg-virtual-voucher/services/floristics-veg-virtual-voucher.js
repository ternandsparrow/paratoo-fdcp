'use strict'

/**
 * floristics-veg-virtual-voucher service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::floristics-veg-virtual-voucher.floristics-veg-virtual-voucher')

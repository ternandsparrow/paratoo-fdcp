module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/floristics-veg-virtual-vouchers',
      'handler': 'floristics-veg-virtual-voucher.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/floristics-veg-virtual-vouchers/:id',
      'handler': 'floristics-veg-virtual-voucher.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/floristics-veg-virtual-vouchers',
      'handler': 'floristics-veg-virtual-voucher.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/floristics-veg-virtual-vouchers/:id',
      'handler': 'floristics-veg-virtual-voucher.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/floristics-veg-virtual-vouchers/:id',
      'handler': 'floristics-veg-virtual-voucher.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
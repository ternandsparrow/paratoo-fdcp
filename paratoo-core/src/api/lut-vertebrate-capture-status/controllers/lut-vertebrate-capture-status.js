'use strict'

/**
 *  lut-vertebrate-capture-status controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-capture-status.lut-vertebrate-capture-status')

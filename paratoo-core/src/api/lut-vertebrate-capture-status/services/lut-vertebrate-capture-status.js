'use strict'

/**
 * lut-vertebrate-capture-status service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-capture-status.lut-vertebrate-capture-status')

module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-capture-statuses',
      'handler': 'lut-vertebrate-capture-status.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-capture-statuses/:id',
      'handler': 'lut-vertebrate-capture-status.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * invertebrate-active-search service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::invertebrate-active-search.invertebrate-active-search')

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-digging-stopped-by.lut-soils-digging-stopped-by')
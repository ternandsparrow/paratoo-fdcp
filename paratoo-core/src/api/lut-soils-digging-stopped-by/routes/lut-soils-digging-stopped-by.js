module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-digging-stopped-bies',
      'handler': 'lut-soils-digging-stopped-by.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-digging-stopped-bies/:id',
      'handler': 'lut-soils-digging-stopped-by.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
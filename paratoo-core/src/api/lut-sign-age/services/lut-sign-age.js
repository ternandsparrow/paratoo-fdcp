'use strict'

/**
 * lut-sign-age service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-sign-age.lut-sign-age')

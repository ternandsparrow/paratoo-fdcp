'use strict'

/**
 *  lut-sign-age controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-sign-age.lut-sign-age')

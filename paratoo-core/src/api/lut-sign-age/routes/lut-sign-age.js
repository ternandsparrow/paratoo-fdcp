module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-sign-ages',
      'handler': 'lut-sign-age.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-sign-ages/:id',
      'handler': 'lut-sign-age.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
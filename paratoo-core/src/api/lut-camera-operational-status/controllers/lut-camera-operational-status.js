'use strict'

/**
 *  lut-camera-operational-status controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-camera-operational-status.lut-camera-operational-status')

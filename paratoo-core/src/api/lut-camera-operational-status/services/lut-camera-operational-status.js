'use strict'

/**
 * lut-camera-operational-status service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-camera-operational-status.lut-camera-operational-status')

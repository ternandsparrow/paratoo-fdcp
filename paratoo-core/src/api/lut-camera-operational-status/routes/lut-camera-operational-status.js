module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-camera-operational-statuses',
      'handler': 'lut-camera-operational-status.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-camera-operational-statuses/:id',
      'handler': 'lut-camera-operational-status.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
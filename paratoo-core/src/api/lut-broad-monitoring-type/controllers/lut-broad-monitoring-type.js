'use strict'

/**
 * lut-broad-monitoring-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-broad-monitoring-type.lut-broad-monitoring-type')

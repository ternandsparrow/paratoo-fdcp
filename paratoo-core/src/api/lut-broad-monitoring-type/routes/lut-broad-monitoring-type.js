module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-broad-monitoring-types',
      'handler': 'lut-broad-monitoring-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-broad-monitoring-types/:id',
      'handler': 'lut-broad-monitoring-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-broad-monitoring-type service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-broad-monitoring-type.lut-broad-monitoring-type')

'use strict'

/**
 * vertebrate-trapping-setup-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::vertebrate-trapping-setup-survey.vertebrate-trapping-setup-survey')

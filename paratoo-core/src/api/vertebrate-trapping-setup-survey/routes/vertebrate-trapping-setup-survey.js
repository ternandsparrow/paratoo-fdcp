module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/vertebrate-trapping-setup-surveys',
      'handler': 'vertebrate-trapping-setup-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/vertebrate-trapping-setup-surveys/:id',
      'handler': 'vertebrate-trapping-setup-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/vertebrate-trapping-setup-surveys',
      'handler': 'vertebrate-trapping-setup-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/vertebrate-trapping-setup-surveys/:id',
      'handler': 'vertebrate-trapping-setup-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/vertebrate-trapping-setup-surveys/:id',
      'handler': 'vertebrate-trapping-setup-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/vertebrate-trapping-setup-surveys/bulk',
      'handler': 'vertebrate-trapping-setup-survey.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-rill-water-erosion-degree.lut-soils-rill-water-erosion-degree')
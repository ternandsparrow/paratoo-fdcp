module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-rill-water-erosion-degrees',
      'handler': 'lut-soils-rill-water-erosion-degree.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-rill-water-erosion-degrees/:id',
      'handler': 'lut-soils-rill-water-erosion-degree.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
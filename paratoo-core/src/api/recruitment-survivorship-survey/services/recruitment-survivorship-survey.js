'use strict'

/**
 * recruitment-survivorship-survey service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::recruitment-survivorship-survey.recruitment-survivorship-survey')

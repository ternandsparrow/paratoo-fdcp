module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/recruitment-survivorship-surveys',
      'handler': 'recruitment-survivorship-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/recruitment-survivorship-surveys/:id',
      'handler': 'recruitment-survivorship-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/recruitment-survivorship-surveys',
      'handler': 'recruitment-survivorship-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/recruitment-survivorship-surveys/:id',
      'handler': 'recruitment-survivorship-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/recruitment-survivorship-surveys/:id',
      'handler': 'recruitment-survivorship-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/recruitment-survivorship-surveys/bulk',
      'handler': 'recruitment-survivorship-survey.bulk',
      'config': {
        'policies': [
          'global::fix-revisit-free-text-to-lut',
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
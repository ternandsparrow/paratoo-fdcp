const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-intervention-land-management-industry-type.lut-intervention-land-management-industry-type')
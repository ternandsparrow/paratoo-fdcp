module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-intervention-land-management-industry-types',
      'handler': 'lut-intervention-land-management-industry-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-intervention-land-management-industry-types/:id',
      'handler': 'lut-intervention-land-management-industry-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
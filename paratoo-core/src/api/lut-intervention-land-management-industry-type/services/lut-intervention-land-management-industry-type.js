const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-intervention-land-management-industry-type.lut-intervention-land-management-industry-type')
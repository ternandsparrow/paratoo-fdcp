'use strict'

/**
 * lut-vertebrate-sex service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-vertebrate-sex.lut-vertebrate-sex')

'use strict'

/**
 *  lut-vertebrate-sex controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-vertebrate-sex.lut-vertebrate-sex')

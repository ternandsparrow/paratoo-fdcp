module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-vertebrate-sexes',
      'handler': 'lut-vertebrate-sex.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-vertebrate-sexes/:id',
      'handler': 'lut-vertebrate-sex.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
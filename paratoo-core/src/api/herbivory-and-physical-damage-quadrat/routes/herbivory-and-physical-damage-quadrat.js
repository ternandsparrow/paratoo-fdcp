module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/herbivory-and-physical-damage-quadrats',
      'handler': 'herbivory-and-physical-damage-quadrat.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/herbivory-and-physical-damage-quadrats/:id',
      'handler': 'herbivory-and-physical-damage-quadrat.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/herbivory-and-physical-damage-quadrats',
      'handler': 'herbivory-and-physical-damage-quadrat.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/herbivory-and-physical-damage-quadrats/:id',
      'handler': 'herbivory-and-physical-damage-quadrat.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/herbivory-and-physical-damage-quadrats/:id',
      'handler': 'herbivory-and-physical-damage-quadrat.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
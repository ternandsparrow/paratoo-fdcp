'use strict'

/**
 * herbivory-and-physical-damage-quadrat service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::herbivory-and-physical-damage-quadrat.herbivory-and-physical-damage-quadrat')

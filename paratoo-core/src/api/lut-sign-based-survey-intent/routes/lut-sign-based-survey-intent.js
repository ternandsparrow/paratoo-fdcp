module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-sign-based-survey-intents',
      'handler': 'lut-sign-based-survey-intent.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-sign-based-survey-intents/:id',
      'handler': 'lut-sign-based-survey-intent.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
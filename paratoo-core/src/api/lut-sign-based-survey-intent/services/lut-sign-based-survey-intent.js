'use strict'

/**
 * lut-sign-based-survey-intent service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-sign-based-survey-intent.lut-sign-based-survey-intent')

'use strict'

/**
 *  lut-sign-based-survey-intent controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-sign-based-survey-intent.lut-sign-based-survey-intent')

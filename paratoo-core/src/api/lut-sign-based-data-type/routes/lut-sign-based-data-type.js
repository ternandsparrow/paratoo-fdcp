module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-sign-based-data-types',
      'handler': 'lut-sign-based-data-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-sign-based-data-types/:id',
      'handler': 'lut-sign-based-data-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
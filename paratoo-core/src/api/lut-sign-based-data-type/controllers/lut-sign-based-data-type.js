'use strict'

/**
 *  lut-sign-based-data-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-sign-based-data-type.lut-sign-based-data-type')

'use strict'

/**
 * lut-sign-based-data-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-sign-based-data-type.lut-sign-based-data-type')

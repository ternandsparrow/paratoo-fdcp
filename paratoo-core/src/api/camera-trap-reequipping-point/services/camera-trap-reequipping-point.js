'use strict'

/**
 * camera-trap-reequipping-point service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::camera-trap-reequipping-point.camera-trap-reequipping-point')

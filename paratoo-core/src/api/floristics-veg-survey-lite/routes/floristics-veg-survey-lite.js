module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/floristics-veg-survey-lites',
      'handler': 'floristics-veg-survey-lite.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/floristics-veg-survey-lites/:id',
      'handler': 'floristics-veg-survey-lite.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/floristics-veg-survey-lites',
      'handler': 'floristics-veg-survey-lite.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/floristics-veg-survey-lites/:id',
      'handler': 'floristics-veg-survey-lite.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/floristics-veg-survey-lites/:id',
      'handler': 'floristics-veg-survey-lite.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/floristics-veg-survey-lites/bulk',
      'handler': 'floristics-veg-survey-lite.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
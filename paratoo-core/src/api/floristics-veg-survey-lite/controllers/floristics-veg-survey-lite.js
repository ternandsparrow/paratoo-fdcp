const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::floristics-veg-survey-lite.floristics-veg-survey-lite', ({ strapi }) => ({
  /* Generic find controller with pdp data filter */
  async find(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      
    const authToken = ctx.request.headers['authorization']
    const modelName = ctx.state.route.info.apiName
    const tokenStatus = await helpers.apiTokenCheck(authToken)
    const useDefaultController = ctx.request.query['use-default'] === 'true'
      
    // if we need to use default controller (use magic jwt and use-default flag)
    if (tokenStatus?.type == 'full-access' && useDefaultController) {
      helpers.paratooDebugMsg(`Default controller is used to populate model: ${modelName}`)
      return await super.find(ctx)
    }
    return await helpers.pdpDataFilter(
      modelName,
      authToken,
      ctx.request.query,
      tokenStatus,
    )
  },

  /**
   * TODO 
   * 
   * @param {*} ctx 
   */
  async bulk(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const surveyModelName = 'floristics-veg-survey-lite'
    const voucherModelName = []

    const collectionKeys = Object.keys(ctx.request.body.data.collections[0])

    //we need AT LEAST a virtual voucher OR lite voucher (or both)
    //TODO have the validator do this (probably using Swagger `anyOf` or `oneOf`)    
    if (
      !collectionKeys.includes('floristics-veg-voucher-lite') &&
      !collectionKeys.includes('floristics-veg-virtual-voucher')
    ) {
      const err = new Error('You must provide vouchers you have seen before and/or new lite vouchers')
      return helpers.paratooErrorHandler(400, err)
    }

    //user can provide lite vouchers and/or virtual vouchers
    if (collectionKeys.includes('floristics-veg-voucher-lite')) {
      voucherModelName.push('floristics-veg-voucher-lite')
    }
    if (
      collectionKeys.includes('floristics-veg-virtual-voucher') &&
      //do an array check because the client can send an empty array of virtual vouchers
      //when none are collected
      Array.isArray(ctx.request.body.data.collections[0]['floristics-veg-virtual-voucher']) &&
      ctx.request.body.data.collections[0]['floristics-veg-virtual-voucher'].length > 0
    ) {
      voucherModelName.push('floristics-veg-virtual-voucher')
    }

    return strapi.service('api::paratoo-helper.paratoo-helper').genericBulk({
      ctx: ctx,
      surveyModelName: surveyModelName,
      obsModelName: voucherModelName
    })
  },
}))
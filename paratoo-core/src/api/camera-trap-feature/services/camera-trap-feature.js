'use strict'

/**
 * camera-trap-feature service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::camera-trap-feature.camera-trap-feature')

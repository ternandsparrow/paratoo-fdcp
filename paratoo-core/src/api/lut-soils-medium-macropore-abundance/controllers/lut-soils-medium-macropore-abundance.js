const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-medium-macropore-abundance.lut-soils-medium-macropore-abundance')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-medium-macropore-abundances',
      'handler': 'lut-soils-medium-macropore-abundance.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-medium-macropore-abundances/:id',
      'handler': 'lut-soils-medium-macropore-abundance.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
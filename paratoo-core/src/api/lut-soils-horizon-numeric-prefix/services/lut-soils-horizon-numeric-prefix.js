'use strict'

/**
 * lut-soils-horizon-numeric-prefix service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-soils-horizon-numeric-prefix.lut-soils-horizon-numeric-prefix')

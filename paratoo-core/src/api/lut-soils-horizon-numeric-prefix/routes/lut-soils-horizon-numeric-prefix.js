module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-horizon-numeric-prefixes',
      'handler': 'lut-soils-horizon-numeric-prefix.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-horizon-numeric-prefixes/:id',
      'handler': 'lut-soils-horizon-numeric-prefix.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
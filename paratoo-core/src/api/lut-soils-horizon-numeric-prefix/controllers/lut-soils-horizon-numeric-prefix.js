'use strict'

/**
 * lut-soils-horizon-numeric-prefix controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-horizon-numeric-prefix.lut-soils-horizon-numeric-prefix')

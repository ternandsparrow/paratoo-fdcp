'use strict'

/**
 *  lut-recruitment-study-area-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-recruitment-study-area-type.lut-recruitment-study-area-type')

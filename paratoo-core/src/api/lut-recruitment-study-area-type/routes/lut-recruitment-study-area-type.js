module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-recruitment-study-area-types',
      'handler': 'lut-recruitment-study-area-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-recruitment-study-area-types/:id',
      'handler': 'lut-recruitment-study-area-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
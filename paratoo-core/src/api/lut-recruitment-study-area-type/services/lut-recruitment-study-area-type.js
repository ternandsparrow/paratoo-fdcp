'use strict'

/**
 * lut-recruitment-study-area-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-recruitment-study-area-type.lut-recruitment-study-area-type')

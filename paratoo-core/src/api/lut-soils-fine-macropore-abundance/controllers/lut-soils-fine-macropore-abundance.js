const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-fine-macropore-abundance.lut-soils-fine-macropore-abundance')
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-fine-macropore-abundances',
      'handler': 'lut-soils-fine-macropore-abundance.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-fine-macropore-abundances/:id',
      'handler': 'lut-soils-fine-macropore-abundance.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
'use strict'

/**
 * lut-exact-or-estimate service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-exact-or-estimate.lut-exact-or-estimate')

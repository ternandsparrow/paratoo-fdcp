'use strict'

/**
 *  lut-exact-or-estimate controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-exact-or-estimate.lut-exact-or-estimate')

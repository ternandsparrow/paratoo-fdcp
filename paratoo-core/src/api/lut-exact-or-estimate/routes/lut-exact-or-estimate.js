module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-exact-or-estimates',
      'handler': 'lut-exact-or-estimate.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-exact-or-estimates/:id',
      'handler': 'lut-exact-or-estimate.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
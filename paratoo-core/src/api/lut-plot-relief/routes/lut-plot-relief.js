module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-plot-reliefs',
      'handler': 'lut-plot-relief.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-plot-reliefs/:id',
      'handler': 'lut-plot-relief.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
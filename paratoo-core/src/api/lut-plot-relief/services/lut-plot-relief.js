'use strict'

/**
 * lut-plot-relief service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-plot-relief.lut-plot-relief')

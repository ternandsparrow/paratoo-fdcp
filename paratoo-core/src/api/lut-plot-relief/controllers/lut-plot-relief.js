'use strict'

/**
 *  lut-plot-relief controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-plot-relief.lut-plot-relief')

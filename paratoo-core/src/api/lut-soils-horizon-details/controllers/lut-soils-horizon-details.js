const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-horizon-details.lut-soils-horizon-details')
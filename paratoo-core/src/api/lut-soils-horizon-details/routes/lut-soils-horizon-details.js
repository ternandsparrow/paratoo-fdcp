module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-horizon-details',
      'handler': 'lut-soils-horizon-details.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-horizon-details/:id',
      'handler': 'lut-soils-horizon-details.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-employed-methods',
      'handler': 'lut-employed-method.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-employed-methods/:id',
      'handler': 'lut-employed-method.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
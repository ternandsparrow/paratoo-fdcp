'use strict'

/**
 * lut-employed-method service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-employed-method.lut-employed-method')

'use strict'

/**
 * lut-employed-method controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-employed-method.lut-employed-method')

'use strict'

/**
 *  lut-feature controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-feature.lut-feature')

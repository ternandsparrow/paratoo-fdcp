'use strict'

/**
 * lut-feature service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-feature.lut-feature')

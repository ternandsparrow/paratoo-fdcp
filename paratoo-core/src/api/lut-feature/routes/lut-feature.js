module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-features',
      'handler': 'lut-feature.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-features/:id',
      'handler': 'lut-feature.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
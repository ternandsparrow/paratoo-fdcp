module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-microrelief-components',
      'handler': 'lut-soils-microrelief-component.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-microrelief-components/:id',
      'handler': 'lut-soils-microrelief-component.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-microrelief-component.lut-soils-microrelief-component')
'use strict'

/**
 * push-notification-subscription service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::push-notification-subscription.push-notification-subscription')

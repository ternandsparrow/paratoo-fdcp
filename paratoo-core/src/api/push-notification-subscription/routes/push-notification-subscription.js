module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/push-notification-subscriptions',
      handler: 'push-notification-subscription.find',
      config: {
        policies: ['global::is-validated', 'global::projectMembershipEnforcer'],
      },
    },
    {
      method: 'GET',
      path: '/push-notification-subscriptions/:id',
      handler: 'push-notification-subscription.findOne',
      config: {
        policies: ['global::is-validated', 'global::projectMembershipEnforcer'],
      },
    },
    {
      method: 'POST',
      path: '/push-notification-subscriptions',
      handler: 'push-notification-subscription.create',
      config: {
        policies: ['global::is-validated', 'global::lut-interpretation'],
      },
    },
    {
      method: 'PUT',
      path: '/push-notification-subscriptions/:id',
      handler: 'push-notification-subscription.update',
      config: {
        policies: [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation',
        ],
      },
    },
    {
      method: 'DELETE',
      path: '/push-notification-subscriptions/:id',
      handler: 'push-notification-subscription.delete',
      config: {
        policies: ['global::is-validated', 'global::projectMembershipEnforcer'],
      },
    },
  ],
}

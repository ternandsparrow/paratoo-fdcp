'use strict'

/**
 *  debug-client-state-dump controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController(
  'api::debug-client-state-dump.debug-client-state-dump',
  ({ strapi }) => ({
    async create(ctx) {
      const { data } = ctx.request.body
      const createdClientStateDump = await strapi.entityService.create(
        'api::debug-client-state-dump.debug-client-state-dump',
        {
          data,
        },
      )
      // send dump file id to slack if the webhook is configured
      if (process.env.CORE_SLACK_WEBHOOK_URL) {
        const slackUrl = process.env.CORE_SLACK_WEBHOOK_URL
        const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
        const url = `${process.env.DNS_PARATOO_CORE}/admin/content-manager/collection-types/api::debug-client-state-dump.debug-client-state-dump/${createdClientStateDump.id}`
        const origin = ctx.request.body.data.origin
        const text = `New client state dump created with ID: ${createdClientStateDump.id}\nOrigin: ${origin}\nURL: ${url}`
        const options = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ text }),
        }
        helpers.sendFetchWithOption(slackUrl, options)
      }

      return { data: createdClientStateDump }
    },
    async findOne(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      const authToken = ctx.request.headers['authorization']
      const modelName = ctx.state.route.info.apiName
      const tokenStatus = await helpers.apiTokenCheck(authToken)

      // if we need to use default controller
      if (tokenStatus?.type == 'full-access') {
        helpers.paratooDebugMsg(
          `Default controller is used to populate model: ${modelName}`,
        )
        return await super.find(ctx)
      }

      const { client_state_uuid } = ctx.query

      const clientStateDump = await super.findOne(ctx)
      if (!clientStateDump) return

      const actualDumpUuid = helpers.findValForKey(
        clientStateDump,
        'client_state_uuid',
      )
      if (actualDumpUuid && actualDumpUuid !== client_state_uuid) {
        return helpers.paratooErrorHandler(
          403,
          new Error('You do not have access to this dump'),
        )
      }
      return clientStateDump
    },
    async find(ctx) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

      const authToken = ctx.request.headers['authorization']
      const modelName = ctx.state.route.info.apiName
      const tokenStatus = await helpers.apiTokenCheck(authToken)

      if (tokenStatus?.type == 'full-access') {
        helpers.paratooDebugMsg(
          `Default controller is used to populate model: ${modelName}`,
        )
        return await super.find(ctx)
      } else
        return helpers.paratooErrorHandler(
          403,
          new Error('You do not have access to this endpoint'),
        )
    },
  }),
)

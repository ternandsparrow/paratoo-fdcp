module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/debug-client-state-dumps',
      'handler': 'debug-client-state-dump.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/debug-client-state-dumps/:id',
      'handler': 'debug-client-state-dump.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/debug-client-state-dumps',
      'handler': 'debug-client-state-dump.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation',
        ],
      },
      allowMethod: true,
    },
    {
      'method': 'PUT',
      'path': '/debug-client-state-dumps/:id',
      'handler': 'debug-client-state-dump.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation',
        ],
      },
      allowMethod: true,
    },
    {
      'method': 'DELETE',
      'path': '/debug-client-state-dumps/:id',
      'handler': 'debug-client-state-dump.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
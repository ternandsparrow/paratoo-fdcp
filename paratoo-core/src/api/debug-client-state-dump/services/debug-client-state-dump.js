'use strict'

/**
 * debug-client-state-dump service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::debug-client-state-dump.debug-client-state-dump')

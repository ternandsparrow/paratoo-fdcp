module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-soils-colour-hues',
      'handler': 'lut-soils-colour-hue.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-soils-colour-hues/:id',
      'handler': 'lut-soils-colour-hue.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
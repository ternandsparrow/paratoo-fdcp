const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-soils-colour-hue.lut-soils-colour-hue')
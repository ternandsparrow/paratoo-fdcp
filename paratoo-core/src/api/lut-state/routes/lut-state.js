module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-states',
      'handler': 'lut-state.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-states/:id',
      'handler': 'lut-state.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
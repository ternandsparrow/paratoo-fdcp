'use strict'

/**
 * lut-uav-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-uav-type.lut-uav-type')

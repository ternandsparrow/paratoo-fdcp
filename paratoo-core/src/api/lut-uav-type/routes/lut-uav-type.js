module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-uav-types',
      'handler': 'lut-uav-type.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-uav-types/:id',
      'handler': 'lut-uav-type.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
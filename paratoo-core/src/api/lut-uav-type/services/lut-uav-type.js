'use strict'

/**
 * lut-uav-type service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-uav-type.lut-uav-type')

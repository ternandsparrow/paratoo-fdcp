module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-nesp-weed-targets',
      'handler': 'lut-nesp-weed-target.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-nesp-weed-targets/:id',
      'handler': 'lut-nesp-weed-target.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
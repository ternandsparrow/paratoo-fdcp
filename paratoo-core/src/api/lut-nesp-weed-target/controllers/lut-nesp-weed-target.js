'use strict'

/**
 * lut-nesp-weed-target controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-nesp-weed-target.lut-nesp-weed-target')

'use strict'

/**
 * lut-nesp-weed-target service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-nesp-weed-target.lut-nesp-weed-target')

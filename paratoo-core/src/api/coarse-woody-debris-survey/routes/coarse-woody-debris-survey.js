module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/coarse-woody-debris-surveys',
      'handler': 'coarse-woody-debris-survey.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/coarse-woody-debris-surveys/:id',
      'handler': 'coarse-woody-debris-survey.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/coarse-woody-debris-surveys',
      'handler': 'coarse-woody-debris-survey.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/coarse-woody-debris-surveys/:id',
      'handler': 'coarse-woody-debris-survey.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/coarse-woody-debris-surveys/:id',
      'handler': 'coarse-woody-debris-survey.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/coarse-woody-debris-surveys/bulk',
      'handler': 'coarse-woody-debris-survey.bulk',
      'config': {
        'policies': [
          'global::fix-cwd-and-tree-stumps-in-same-record',
          'global::fix-cwd-length-unit',
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    }
  ]
}
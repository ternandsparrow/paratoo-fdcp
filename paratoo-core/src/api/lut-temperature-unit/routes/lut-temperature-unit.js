module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-temperature-units',
      'handler': 'lut-temperature-unit.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-temperature-units/:id',
      'handler': 'lut-temperature-unit.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
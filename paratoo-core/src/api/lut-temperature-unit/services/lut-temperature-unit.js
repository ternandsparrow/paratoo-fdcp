'use strict'

/**
 * lut-temperature-unit service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-temperature-unit.lut-temperature-unit')

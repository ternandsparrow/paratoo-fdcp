'use strict'

/**
 *  lut-temperature-unit controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-temperature-unit.lut-temperature-unit')

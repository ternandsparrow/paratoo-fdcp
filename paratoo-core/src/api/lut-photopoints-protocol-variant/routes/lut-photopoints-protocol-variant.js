module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-photopoints-protocol-variants',
      'handler': 'lut-photopoints-protocol-variant.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-photopoints-protocol-variants/:id',
      'handler': 'lut-photopoints-protocol-variant.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
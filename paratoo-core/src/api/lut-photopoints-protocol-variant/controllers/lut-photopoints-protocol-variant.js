const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-photopoints-protocol-variant.lut-photopoints-protocol-variant')
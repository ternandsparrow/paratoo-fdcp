'use strict'

/**
 * soil-sub-pit-sampling service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::soil-sub-pit-sampling.soil-sub-pit-sampling')

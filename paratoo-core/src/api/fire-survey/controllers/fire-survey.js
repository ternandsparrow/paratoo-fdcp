const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::fire-survey.fire-survey', ({ strapi }) =>({
  /* Generic find controller with pdp data filter */
  async find(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      
    const authToken = ctx.request.headers['authorization']
    const modelName = ctx.state.route.info.apiName
    const tokenStatus = await helpers.apiTokenCheck(authToken)
    const useDefaultController = ctx.request.query['use-default'] === 'true'
      
    // if we need to use default controller (use magic jwt and use-default flag)
    if (tokenStatus?.type == 'full-access' && useDefaultController) {
      helpers.paratooDebugMsg(`Default controller is used to populate model: ${modelName}`)
      return await super.find(ctx)
    }
    return await helpers.pdpDataFilter(
      modelName,
      authToken,
      ctx.request.query,
      tokenStatus,
    )
  },

  /**
   * TODO
   * 
   * @param {*} ctx 
   */
  async bulk(ctx) {
    const surveyModelName = 'fire-survey'
    const pointModelName = 'fire-point-intercept-point'
    const charHeightModelName = 'fire-char-observation'
    const speciesModelName = 'fire-species-intercept'

    return strapi.service('api::paratoo-helper.paratoo-helper').genericBulk({
      ctx: ctx, 
      surveyModelName: surveyModelName,
      obsModelName: pointModelName,
      //we use `generalEntryModelNames` for the char ob as using many `obsModelName` WITH
      //child obs is not supported (i.e., more complex to implement)
      generalEntryModelNames: charHeightModelName,
      childObservationModelName: speciesModelName,
    })
  }

}))
const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::fire-survey.fire-survey')
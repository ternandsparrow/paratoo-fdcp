'use strict'

/**
 * vertebrate-active-passive-search service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::vertebrate-active-passive-search.vertebrate-active-passive-search')

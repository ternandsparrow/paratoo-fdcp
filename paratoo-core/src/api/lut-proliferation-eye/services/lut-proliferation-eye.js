'use strict'

/**
 * lut-proliferation-eye service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-proliferation-eye.lut-proliferation-eye')

'use strict'

/**
 *  lut-proliferation-eye controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-proliferation-eye.lut-proliferation-eye')

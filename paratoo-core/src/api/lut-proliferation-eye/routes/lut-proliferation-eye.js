module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/lut-proliferation-eyes',
      'handler': 'lut-proliferation-eye.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/lut-proliferation-eyes/:id',
      'handler': 'lut-proliferation-eye.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
  ]
}
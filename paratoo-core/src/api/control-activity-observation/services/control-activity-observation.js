'use strict'

/**
 * control-activity-observation service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::control-activity-observation.control-activity-observation')

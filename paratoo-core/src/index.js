const strapiInitialData = require('strapi-initial-data')
const pluralize = require('pluralize')
const crypto = require('crypto')
const startTotal = new Date()
const branchName = process.env.BRANCH_NAME
//production branch that we don't want testing data initialised
const prodBranch = process.env.PROD_BRANCH || 'main'
/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/v3.x/concepts/configurations.html#bootstrap
 */

// Some nice user info..
const figlet = require('figlet')
console.log(
  figlet.textSync('Strapi CORE (Paratoo/Monitor)', {
    horizontalLayout: 'default',
    verticalLayout: 'default',
    width: 120,
    whitespaceBreak: true,
  }),
)

module.exports = {
  register() {
    const registerStart = Date.now()
    strapi.log.debug('Strapi instance is performing register hook..')

    //extend the upload plugin's attributes to ensure name is unique (uid) -
    //  `amendDocumentation()` will handle the putting this in the documentation
    //spread operation creates a clone of the object (rather than a reference)
    const uploadAttributes = { ...strapi.contentType('plugin::upload.file').attributes }
    uploadAttributes.name.unique = true
    uploadAttributes.name.type = 'uid'
    strapi.contentType('plugin::upload.file').attributes = uploadAttributes

    //need to override how we check files, as Samsung devices can have issues generating
    //metadata, which causes issue with `sharp` lib. this override is the same as the
    //base Strapi one, but we are more forgiving of errors for bad metadata (the files
    //are still usable)
    //see: https://sebscholl.medium.com/overriding-service-methods-in-strapi-v4s-upload-plugin-cf97cb66e85
    //(you will need an account to view)
    //we're addressing issue that is caused by the call Strapi makes to Sharp: https://github.com/strapi/strapi/issues/16838
    const overrideUploadServices = [
      'isFaultyImage',
      'isOptimizableImage',
      'isResizableImage',
      'isImage',
      'getDimensions',
      'optimize',
    ]
    for (const service of overrideUploadServices) {
      strapi.log.info(`Overridden Upload Plugin's image-manipulation service method '${service}'`)
      try {
        strapi.services['plugin::upload.image-manipulation'][service] = require('./extensions/upload/overrides')[service]
      } catch (err) {
        strapi.service('api::paratoo-helper.paratoo-helper').paratooWarnHandler(
          `Failed to override Upload Plugin's image-manipulation service method '${service}'`,
          err,
        )
      }
    }

    strapi.log.info('Overridden Upload Plugin\'s upload service method \'uploadFileAndPersist\'')
    try {
      strapi.services['plugin::upload.upload'].uploadFileAndPersist = require('./extensions/upload/overrides').uploadFileAndPersist
    } catch (err) {
      strapi.service('api::paratoo-helper.paratoo-helper').paratooWarnHandler(
        'Failed to override Upload Plugin\'s upload service method \'uploadFileAndPersist\'',
        err,
      )
    }

    strapi.log.debug(`Took ${Date.now() - registerStart}ms to register`)
  },
  async bootstrap({ strapi }) {
    const bootstrapStart = Date.now()
    strapi.log.debug(`Core Strapi instance is bootstrapping.. version: ${process.env.VERSION} git_hash: ${process.env.GIT_HASH}`)
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

    const hash = (accessKey) => {
      return crypto
        .createHmac('sha512', strapi.config.get('admin.apiToken.salt'))
        .update(accessKey)
        .digest('hex')
    }

    await helpers.redisConnect()
    const keyDrone = hash(process.env.DRONE_ACCESS_KEY)
    strapi.log.info(`Using drone access key: ${keyDrone}`)

    const apiTokens = await strapi
      .query('admin::api-token')
      .findMany({
        populate: true,
      })
      .catch((err) => {
        strapi.log.error(err)
      }) 
    // delete all existing drone tokens
    apiTokens.forEach(async (apiToken)=>{
      if (!apiToken.name == 'Drone') return
      await strapi.entityService.delete('admin::api-token', apiToken.id) 
    })

    await strapi.query('admin::api-token').create({
      select: ['id', 'name', 'description', 'type', 'createdAt'],
      data: {
        description: 'Drone',
        name: 'Drone',
        type: 'full-access',
        accessKey: keyDrone,
      },
    })

    const keyParatoo = hash(process.env.PARATOO_ACCESS_KEY)
    strapi.log.info(`Using paratoo access key: ${keyParatoo}`)

    // delete all existing Paratoo tokens
    apiTokens.forEach(async (apiToken)=>{
      if (!apiToken.name == 'Paratoo') return
      await strapi.entityService.delete('admin::api-token', apiToken.id)
    })

    await strapi.query('admin::api-token').create({
      select: ['id', 'name', 'description', 'type', 'createdAt'],
      data: {
        description: 'Paratoo Token',
        name: 'Paratoo',
        type: 'full-access',
        accessKey: keyParatoo,
      },
    })

    strapi.log.info(`Using exporter access key: ${process.env.EXPORTER_JWT}`)

    // delete all existing Paratoo tokens
    apiTokens.forEach(async (apiToken)=>{
      if (!apiToken.name == 'Exporter') return
      await strapi.entityService.delete('admin::api-token', apiToken.id)
    })

    await strapi.query('admin::api-token').create({
      select: ['id', 'name', 'description', 'type', 'createdAt'],
      data: {
        description: 'Exporter Token',
        name: 'Exporter',
        type: 'full-access',
        accessKey: process.env.EXPORTER_JWT,
      },
    })

    // different file for different database
    const dbInfo = `${process.env.DATABASE_HOST}${process.env.DATABASE_PORT}${process.env.DATABASE_NAME}`
    const initDataBackUpFileName = helpers.createMd5Hash(dbInfo)
    const initDataBackUpFilePath = `./.initDataBackUp/${initDataBackUpFileName}.json`

    // Wait for the initial data to finish so that strapi queries in
    // amendDocumentation use up-to-date LUT data
    await strapiInitialData(strapi, initDataBackUpFilePath, branchName, prodBranch).catch((err) => {
      // using console over strapi.log because strapi doesn't handle errors well
      helpers.paratooErrorHandler(500, err, 'Failed to load all LUT data')
    })

    //creates user for Strapi admin panel
    await helpers.createAdminUser()

    // To automatically add all intended Policies, set the first parameter of the below function call to "true"
    // This is not default because it modifies version-controlled files (routes.json)

    // TODO we probably want to cancel boot process instead of warn if this fails
    await helpers.amendAPIRoutes({ reWrite: false })

    await helpers.amendDocumentation()

    await configurePerms()

    schedulePushNotifications()

    strapi.log.debug(`Took ${Date.now() - bootstrapStart}ms to bootstrap`)
    strapi.log.info(`Took ${Date.now() - startTotal}ms to setup the entire instance`)
  },
}

async function configurePerms() {
  const start = Date.now()

  const roles = await strapi.db
    .query('plugin::users-permissions.role')
    .findMany()
    .catch((err) => {
      strapi.log.error(err)
    })
  const publicRoleId = roles.find(
    (o) => o.type === 'public' && o.name === 'Public',
  ).id

  //get current list of permissions (only those that are enabled already)
  const permissions = await strapi.db
    .query('plugin::users-permissions.permission')
    .findMany({
      populate: true,
    })
    .catch((err) => {
      strapi.log.error(err)
    })

  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  //create list of models that will need to have bulk permissions set
  const modelsWithBulk = await (async () => {
    const protocols = await helpers.getAllProtocols()
    return protocols.map((o) =>
      pluralize.singular(o.endpointPrefix).replace('/', ''),
    )
  })()

  let permsCreated = []
  const actionSuffixes = [
    'find',
    'findOne',
    'count',
    'create',
    'delete',
    'update',
  ]
  for (const model of strapi.db.config.models) {
    //only want to create perms for API models
    if (model.uid.startsWith('api::')) {
      //set read-only perms on LUTs and `protocol`
      if (
        model.modelName.startsWith('lut-') ||
        model.modelName === 'protocol'
      ) {
        for (const readOnlyActionSuffix of ['find', 'findOne', 'count']) {
          if (
            !permissions.find(
              (o) =>
                o.action === `${model.uid}.${readOnlyActionSuffix}` &&
                o.role.id === publicRoleId,
            )
          ) {
            // strapi.log.info(`Creating read-only permission for public role and action '${model.uid}.${readOnlyActionSuffix}'`)
            await strapi.db
              .query('plugin::users-permissions.permission')
              .create({
                data: {
                  action: `${model.uid}.${readOnlyActionSuffix}`,
                  role: publicRoleId,
                },
              })
              .catch((err) => {
                strapi.log.error(err)
              })
            permsCreated.push(
              `Type: read-only. Role: public. Action: ${model.uid}.${readOnlyActionSuffix}`,
            )
          }
        }
      }
      //allow all actions for non-luts (authorization and authentication is controlled by
      //the project membership enforcer that consults Org)
      else {
        for (const actionSuffix of actionSuffixes) {
          if (
            !permissions.find(
              (o) =>
                o.action === `${model.uid}.${actionSuffix}` &&
                o.role.id === publicRoleId,
            )
          ) {
            // strapi.log.info(`Creating full read/write permission for public role and action '${model.uid}.${actionSuffix}'`)
            await strapi.db
              .query('plugin::users-permissions.permission')
              .create({
                data: {
                  action: `${model.uid}.${actionSuffix}`,
                  role: publicRoleId,
                },
              })
              .catch((err) => {
                strapi.log.error(err)
              })
            permsCreated.push(
              `Type: read/write. Role: public. Action: ${model.uid}.${actionSuffix}`,
            )
          }
        }

        //set bulk permissions
        const modelName = model.uid.split('::')[1].split('.')[0]
        if (modelsWithBulk.includes(modelName)) {
          // strapi.log.info(`Setting bulk permission for model with name '${modelName}'`)
          // TODO don't need set action for all the protocols
          await strapi.db
            .query('plugin::users-permissions.permission')
            .create({
              data: {
                action: `${model.uid}.bulk`,
                role: publicRoleId,
              },
            })
            .catch((err) => {
              strapi.log.error(err)
            })
          permsCreated.push(
            `Type: write. Role: public. Action: ${model.uid}.bulk`,
          )
          // TODO don't need set action for all the protocols
          await strapi.db
            .query('plugin::users-permissions.permission')
            .create({
              data: {
                action: `${model.uid}.many`,
                role: publicRoleId,
              },
            })
            .catch((err) => {
              strapi.log.error(err)
            })
          permsCreated.push(
            `Type: write. Role: public. Action: ${model.uid}.many`,
          )
        }
      }
    }
  }
  // strapi.log.info('Setting index permission for expose-documentation')

  // expose swagger.json documentation endpoint to be public
  await strapi.db
    .query('plugin::users-permissions.permission')
    .create({
      data: {
        action: 'api::expose-documentation.expose-documentation.index',
        role: publicRoleId,
      },
    })
    .catch((err) => {
      strapi.log.error(err)
    })
  permsCreated.push(
    'Type: read. Role: public. Action: api::expose-documentation.expose-documentation.index',
  )

  // expose upload plugin's upload to be public
  // (same as configuration in StrapiV3)
  await strapi.db
    .query('plugin::users-permissions.permission')
    .create({
      data: {
        action: 'plugin::upload.content-api.upload',
        role: publicRoleId,
      },
    })
    .catch((err) => {
      strapi.log.error(err)
    })
  permsCreated.push(
    'Type: write. Role: public. Action: plugin::upload.content-api.upload',
  )

  strapi.log.verbose('Created permissions: ', permsCreated)
  strapi.log.info(`Took ${Date.now() - start}ms to set all perms`)
}

async function schedulePushNotifications() {
  const { schedulePublishCollection } = strapi.service(
    'api::paratoo-helper.paratoo-helper',
  )
  const notifications = await strapi.entityService.findMany(
    'api::push-notification.push-notification',
    {
      filters: { isSent: false },
    },
  )
  if (notifications.length) {
    strapi.log.info(`Found ${notifications.length} scheduled notifications`)
  }

  notifications.forEach((notification) => {
    const collectionName = 'push-notification'

    schedulePublishCollection(collectionName, notification)
  })
}
const authenticate = async (ctx) => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

  const url = ctx._matchedRoute
  const skips = [
    'lut', 
    'swagger-build-info', 
    'swagger.json', 
    'metrics', 
    'analytics/heartbeat'
  ]
  for (const s of skips) {
    if (url.includes(s)) {
      return { authenticated: true }
    } 
  }
  const auth = ctx.request.headers['authorization']
  if (!auth) {
    let msg = 'User is not authenticated (logged in) - no auth token supplied'
    return helpers.paratooErrorHandler(401, new Error(msg))
  }

  // if token is an api token with full access
  const status = await helpers.apiTokenCheck(auth)
  if (status?.type == 'full-access') return { authenticated: true }

  const hash = helpers.createMd5Hash(auth)
  const data = await helpers.redisGet(hash)
  if (data) return { authenticated: true }
  
  // note: paratoo-core must stay agnostic to the implementation of
  // paratoo-org. To achieve this here, we'll blindly forward on the auth token
  // that the user has sent us.
  let errMsg = 'There was an issue validating the auth token'
  let isValidResp = false
  try {
    isValidResp = await helpers.orgPostRequestWithBody(
      'validate-token',
      {
        //can't send a bad token in the header as it will be rejected before getting to
        //handler function
        token: auth,
      },
      null,   //no `bearer` param
      false,    //`addDataField`
      true,   //isCustomApi
    )
  } catch (err) {
    let useMsg = 'Unknown error'
    let useStatus = 500
    if (err?.message) {
      useMsg = err.message
    }
    if (err?.status) {
      useStatus = err.status
    }

    return helpers.paratooErrorHandler(useStatus, err, `${errMsg}. ${useMsg}`)
  }

  if (isValidResp?.status !== 200) {
    if (isValidResp?.data?.message) {
      errMsg += `. Got error: ${isValidResp.data.message}`
    } else {
      helpers.paratooDebugMsg(
        'No message provided in API response error',
        true,
      )
    }
    // if org is unavailable
    if (!isValidResp.status) {
      return helpers.paratooErrorHandler(
        502,
        new Error(errMsg),
        'Org is unavailable',
      )
    }
    if (isValidResp.status > 501 && isValidResp.status <= 504) {
      return helpers.paratooErrorHandler(
        isValidResp.status,
        new Error(errMsg),
        'Org is unavailable',
      )
    }
    if (isValidResp.status == 401) {
      //unlike the rest of the cases, we don't want to append `errMsg`, as it's concerned
      //with when something went wrong, but a 401 is an acceptable 'ok' response (as it
      //indicates the token isn't valid)
      return helpers.paratooErrorHandler(
        isValidResp.status,
        new Error('Auth token is not valid'),
      )
    }
    if (isValidResp.status == 403) {
      return helpers.paratooErrorHandler(
        isValidResp.status,
        new Error(errMsg),
        'Forbidden',
      )
    }
    if (isValidResp.status == 400) {
      return helpers.paratooErrorHandler(
        isValidResp.status,
        new Error(errMsg),
        'Bad Request',
      )
    }

    // if unknown error found
    return helpers.paratooErrorHandler(
      isValidResp.status,
      new Error(errMsg),
      'An unknown error occurred',
    )
  }

  let valid = isValidResp.data  //save value for the cache (will only be kept if valid)
  if (
    (
      typeof isValidResp.data === 'boolean' &&
      !isValidResp.data
    ) ||
    (
      typeof isValidResp.data === 'object' &&
      !isValidResp.data?.valid
    )
  ) {
    //don't need to save an invalid token response in the cache
    return helpers.paratooErrorHandler(401, new Error('Auth token is not valid'))
  }
  strapi.log.info('Consulted with Org, token is valid')
  helpers.redisSet(hash, {valid: valid}, helpers.getTimeToLive('token'))
  return { authenticated: true }
}

const verify = async () => {
  return
}

module.exports = (plugin) => {
  strapi.log.debug('updated user-permission plugin ')
  // overrides default token validation of strapi.
  strapi.container.get('auth').register('content-api', {
    name: 'users-permissions',
    authenticate,
    verify,
  })
  return plugin
}

//https://docs.strapi.io/developer-docs/latest/development/plugins-extension.html#within-the-extensions-folder

const _ = require('lodash')

module.exports = (plugin) => {
  //add the projectMembershipEnforcer to the /upload route
  let uploadRoute = plugin.routes['content-api'].routes.find(
    (o) => o.method === 'POST' && o.path === '/',
  )
  uploadRoute['config'] = {
    policies: [
      'global::is-unique-file-name',
    ],
  }

  let fileUploadSchema = plugin.contentTypes.file.schema

  //NOTE: this is setup to be easily extendable
  const additionalAttributes = {
    hasExifIssue: {
      type: 'boolean',
      default: false,
    },
  }
  for (const [attributeName, attributeValue] of Object.entries(additionalAttributes)) {
    try {
      if (
        !Object.keys(fileUploadSchema.attributes).includes(attributeName) &&
        !_.isEqual(fileUploadSchema.attributes[attributeName], attributeValue)
      ) {
        strapi.log.info(
          `Appending attribute '${attributeName}' to file upload schema: ${JSON.stringify(attributeValue)}`,
        )
        Object.assign(fileUploadSchema.attributes, {
          [attributeName]: attributeValue,
        })
      } else {
        strapi.log.info(
          `Skipping appending attribute '${attributeName}' to file upload schema: ${JSON.stringify(attributeValue)}`,
        )
      }
    } catch (err) {
      strapi.log.warn(
        `Failed to append attribute '${attributeName}' to file upload schema: ${JSON.stringify(attributeValue)}`,
      )
      strapi.log.warn(err)
    }
  }
  
  return plugin
}

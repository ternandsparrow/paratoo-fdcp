const fs = require('fs')
const { join } = require('path')
const sharp = require('sharp')
const exif = require('exif-reader')
const {
  sanitize,
  contentTypes: contentTypesUtils,
  file: { bytesToKbytes },
} = require('@strapi/utils')
const _ = require('lodash')

const FORMATS_TO_RESIZE = ['jpeg', 'png', 'webp', 'tiff', 'gif']
const FORMATS_TO_PROCESS = ['jpeg', 'png', 'webp', 'tiff', 'svg', 'gif', 'avif']
const FORMATS_TO_OPTIMIZE = ['jpeg', 'png', 'webp', 'tiff', 'avif']
const { UPDATED_BY_ATTRIBUTE, CREATED_BY_ATTRIBUTE } = contentTypesUtils.constants
const FILE_MODEL_UID = 'plugin::upload.file'
const MEDIA_CREATE = 'media.create'

const getService = (serviceName) => {
  return strapi.plugin('upload').service(serviceName)
}

const uploadFileAndPersist = async (fileData, { user } = {}) => {
  const ignoreCase = await checkExifForIgnoreCases({
    file: fileData,
    error: null,
    caller: 'uploadFileAndPersist',
  })

  const config = strapi.config.get('plugin.upload')
  const { isImage } = await getService('image-manipulation')

  await getService('provider').checkFileSize(fileData)

  if (await isImage(fileData)) {
    const uploadImage = await getService('upload').uploadImage
    await uploadImage(fileData)
  } else {
    await getService('provider').upload(fileData)
  }

  _.set(fileData, 'provider', config.provider)

  if (ignoreCase) {
    //there will be breadcrumbs from all the calls above (that call
    //`checkExifForIgnoreCases`) and the above call to `checkExifForIgnoreCases`,
    //so generate this warning to ensure those breadcrumbs are included in a single event
    //(this upload)
    strapi.service('api::paratoo-helper.paratoo-helper').paratooWarnHandler(
      `Got an ignore case when uploading file with info: ${JSON.stringify(fileData)}`
    )

    Object.assign(fileData, {
      hasExifIssue: true,
    })
  }

  return add(fileData, { user })
}

const add = async (values, { user } = {}) => {
  const fileValues = { ...values }
  if (user) {
    fileValues[UPDATED_BY_ATTRIBUTE] = user.id
    fileValues[CREATED_BY_ATTRIBUTE] = user.id
  }

  sendMediaMetrics(fileValues)

  const res = await strapi.query(FILE_MODEL_UID).create({ data: fileValues })

  await emitEvent(MEDIA_CREATE, res)

  return res
}

const emitEvent = async (event, data) => {
  const modelDef = strapi.getModel(FILE_MODEL_UID)
  const sanitizedData = await sanitize.sanitizers.defaultSanitizeOutput(modelDef, data)

  strapi.eventHub.emit(event, { media: sanitizedData })
}

const sendMediaMetrics = (data) => {
  if (_.has(data, 'caption') && !_.isEmpty(data.caption)) {
    strapi.telemetry.send('didSaveMediaWithCaption')
  }

  if (_.has(data, 'alternativeText') && !_.isEmpty(data.alternativeText)) {
    strapi.telemetry.send('didSaveMediaWithAlternativeText')
  }
}

const isFaultyImage = async (file) => {  
  if (!file.filepath) {
    return new Promise((resolve, reject) => {
      const pipeline = sharp()
      pipeline.stats().then(resolve).catch(reject)
      file.getStream().pipe(pipeline)
    })
  }

  try {
    await sharp(file.filepath).stats()
    return false
  } catch (e) {
    if (await checkExifForIgnoreCases({
      file: file,
      error: e,
      caller: 'isFaultyImage',
    })) {
      return false
    } else {
      return true
    }
  }
}

const isOptimizableImage = async (file) => {
  let format
  try {
    const metadata = await getMetadata(file)
    format = metadata.format
  } catch (e) {
    if (!await checkExifForIgnoreCases({
      file: file,
      error: e,
      caller: 'isOptimizableImage',
    })) {
      // throw when the file is not a supported image
      return false
    }
  }
  return format && FORMATS_TO_OPTIMIZE.includes(format)
}

const isResizableImage = async (file) => {
  let format
  try {
    const metadata = await getMetadata(file)
    format = metadata.format
  } catch (e) {
    if (!await checkExifForIgnoreCases({
      file: file,
      error: e,
      caller: 'isResizableImage',
    })) {
      // throw when the file is not a supported image
      return false
    }
  }
  return format && FORMATS_TO_RESIZE.includes(format)
}

const isImage = async (file) => {
  let format
  try {
    const metadata = await getMetadata(file)
    format = metadata.format
  } catch (e) {
    if (!await checkExifForIgnoreCases({
      file: file,
      error: e,
      caller: 'isImage',
    })) {
      // throw when the file is not a supported image
      return false
    }
  }
  return format && FORMATS_TO_PROCESS.includes(format)
}

const getDimensions = async (file) => {
  //don't need to do anything to the base implementation, as we've modified `getMetadata`
  //to control the error
  const { width = null, height = null } = await getMetadata(file)

  return { width, height }
}

const optimize = async (file) => {
  const { sizeOptimization = false, autoOrientation = false } = await strapi.plugin('upload').service('upload').getSettings()

  const { format, size } = await getMetadata(file)

  if (sizeOptimization || autoOrientation) {
    let transformer
    //pass `failOn: 'truncated'` to sharp
    if (!file.filepath) {
      transformer = sharp({ failOn: 'truncated' })
    } else {
      transformer = sharp(file.filepath, { failOn: 'truncated' })
    }

    // reduce image quality
    transformer[format]({ quality: sizeOptimization ? 80 : 100 })
    // rotate image based on EXIF data
    if (autoOrientation) {
      transformer.rotate()
    }
    const filePath = join(file.tmpWorkingDirectory, `optimized-${file.hash}`)

    let newInfo
    if (!file.filepath) {
      transformer.on('info', (info) => {
        newInfo = info
      })

      await writeStreamToFile(file.getStream().pipe(transformer), filePath)
    } else {
      newInfo = await transformer.toFile(filePath)
    }

    const { width: newWidth, height: newHeight, size: newSize } = newInfo

    const newFile = { ...file }

    newFile.getStream = () => fs.createReadStream(filePath)
    newFile.filepath = filePath

    if (newSize > size) {
      // Ignore optimization if output is bigger than original
      return file
    }

    return Object.assign(newFile, {
      width: newWidth,
      height: newHeight,
      size: bytesToKbytes(newSize),
      sizeInBytes: newSize,
    })
  }

  return file
}

//this is a private function in @strapi/plugin-upload/server/services/image-manipulation
//that we need to re-create for the other overridden service methods
const getMetadata = async (file) => {
  if (!file.filepath) {
    return new Promise((resolve, reject) => {
      const pipeline = sharp()
      pipeline.metadata().then(resolve).catch(reject)
      file.getStream().pipe(pipeline)
    })
  }

  //the only difference is that we pass `failOn: 'truncated'`
  return sharp(file.filepath, { failOn: 'truncated' }).metadata()
}

const writeStreamToFile = (stream, path) =>
  new Promise((resolve, reject) => {
    const writeStream = fs.createWriteStream(path)
    // Reject promise if there is an error with the provided stream
    stream.on('error', reject)
    stream.pipe(writeStream)
    writeStream.on('close', resolve)
    writeStream.on('error', reject)
  })

//returns true if we are going to ignore the error
const checkExifForIgnoreCases = async ({
  file,
  error,
  caller,
}) => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  let metadata
  try {
    metadata = await sharp(file.filepath, { failOn: 'truncated' }).metadata()
  } catch (err) {
    helpers.paratooDebugMsg(
      `Failed to get metadata: ${err.message}`,
      true,
      'checkExifForIgnoreCases'
    )
  }

  let exifInfo
  if (metadata?.exif) {
    try {
      exifInfo = exif(metadata?.exif)
    } catch (err) {
      console.error(err)
      helpers.paratooDebugMsg(
        `Failed to get exif info: ${err.message}. Metadata: ${JSON.stringify(metadata)}`,
        true,
        'checkExifForIgnoreCases'
      )
    }
  }
  
  //if we get here without catching the error above, we didn't have `exif` in `metadata`
  helpers.paratooDebugMsg(
    `exif info: ${JSON.stringify(exifInfo)}`,
    true,
    'checkExifForIgnoreCases'
  )
  //NOTE: can extend as-needed if there are more edge cases to ignore
  if (exifInfo?.['Image']?.['Make'] === 'samsung') {
    //want here and don't return to fall-through rest of function
    helpers.paratooDebugMsg(
      `Upload plugin failed to ${caller}(), but we detected that it was created by a Samsung device. Will continue with upload by ignoring the error. Got error message: ${error?.message || error?.msg || 'none'}`,
      true,
      'checkExifForIgnoreCases',
    )
    return true
  }

  return false
}

module.exports = {
  isFaultyImage,
  isOptimizableImage,
  isResizableImage,
  isImage,
  getDimensions,
  optimize,

  uploadFileAndPersist,
}
'use strict'

module.exports = async (ctx) => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

  if (ctx.request.url === '/api/condition-surveys/bulk') {
    const requestBody = ctx.request.body.data.collections[0]
    const currSurveyMeta =
      requestBody['condition-survey']?.data?.survey_metadata
    let currProvenanceArr = []
    for (const [provenanceType, provenanceVal] of Object.entries(
      currSurveyMeta?.provenance,
    ) || [[], []]) {
      currProvenanceArr.push(`${provenanceType}=${provenanceVal}`)
    }
    const currProvenance = currProvenanceArr.join(', ')
    const warn = (message) =>
      helpers.paratooWarnHandler(
        `[fix-corrupted-data]-app version: 1.0.4- provenance: ${currProvenance} ${message}`,
      )

    const replaceIncorrectValue = (body, path = '') => {
      if (!body || typeof body !== 'object') return
      Object.keys(body).forEach((key) => {
        // remove empty string or null
        if (body[key] === '' || body[key] == null) {
          warn(`found empty string or null in ${path}.${key}, removing it`)
          delete body[key]
          return
        }
        replaceIncorrectValue(body[key], `${path}.${key}`)
      })
    }
    replaceIncorrectValue(requestBody)
  }
}

'use strict'

const { isNil } = require('lodash')

module.exports = async (ctx) => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const warn = (message) =>
    helpers.paratooWarnHandler(`[fix-corrupted-data]-app version: 1.0.1+1-a452a59a ${message}`)

  switch (ctx.request.url) {
    case '/api/fauna-ground-counts-surveys/bulk': {
      let foundNullSpeed = 0
      const replaceIncorrectValue = (body, path = '') => {
        if (Array.isArray(body)) {
          body.forEach((item, index) =>
            replaceIncorrectValue(item, `${path}[${index}]`),
          )
        } else if (typeof body === 'object') {
          for (const key in body) {
            // https://gitlab.com/ternandsparrow/paratoo-fdcp/-/issues/1745
            // remove empty string in transect_spacing
            if (key === 'transect_spacing' && body[key] === '') {
              warn(`found empty string in ${path}.${key}, removing it`)
              delete body[key]
            }
            // https://gitlab.com/ternandsparrow/paratoo-fdcp/-/issues/1682
            // replace null speed with 0
            else if (key === 'speed' && isNil(body[key])) {
              foundNullSpeed++
              body[key] = 0
            } else if (typeof body[key] === 'object') {
              // add missing 'end' component as it now  required
              // https://gitlab.com/ternandsparrow/paratoo-fdcp/-/work_items/1717
              if (key === 'fauna-ground-counts-conduct-survey') {
                body[key].forEach((survey, index) => {
                  if (!survey.data?.end) {
                    warn(
                      `found missing 'end' in ${path}.${key}[${index}], adding it`,
                    )
                    survey.data.end = {}
                  }
                })
              }
              replaceIncorrectValue(body[key], `${path}.${key}`)
            }
          }
        }
      }
      replaceIncorrectValue(ctx.request.body)
      // don't this warning too noisy so we only do in once after finish the replacing
      if (foundNullSpeed)
        warn(
          `found ${foundNullSpeed} null speed and replace them with 0 in fauna-ground-counts-surveys's track_log`,
        )

      break
    }

    default:
      warn(`unable to fix incorrect data for fauna ground count transect protocol, because ${ctx.request.url} mismatch`)
      break
  }
}

'use strict'

module.exports = async (ctx) => {
  const surveyAndRelatedField = {
    'plot-description-standard': 'revisit_reason',
    'recruitment-survivorship-survey': 'reason_for_revisit',
  }

  let surveyName
  for (const item of Object.keys(surveyAndRelatedField)) {
    if (ctx.request.url.includes(item)) {
      surveyName = item
      break
    }
  }
  const { paratooWarnHandler, paratooErrorHandler } = strapi.service(
    'api::paratoo-helper.paratoo-helper',
  )

  if (!surveyName)
    return paratooErrorHandler(
      500,
      null,
      `unsupported collection, because ${ctx.request.url} mismatch`,
    )

  const surveyData = ctx.request.body.data.collections[0][surveyName].data

  const currSurveyMeta = surveyData.survey_metadata

  let currProvenanceArr = []
  for (const [provenanceType, provenanceVal] of Object.entries(
    currSurveyMeta?.provenance,
  ) || [[], []]) {
    currProvenanceArr.push(`${provenanceType}=${provenanceVal}`)
  }
  const currProvenance = currProvenanceArr.join(', ')
  const warn = (message) =>
    paratooWarnHandler(
      `[update-revisit-free-text-to-lut]- provenance: ${currProvenance} ${message}`,
    )

  const fieldName = surveyAndRelatedField[surveyName]
  const relatedFieldValue = surveyData[fieldName]

  if (relatedFieldValue && typeof relatedFieldValue === 'string') {
    warn(`update field ${fieldName} in ${surveyName} to Other in custom lut`)
    const newData = {
      revisit_reason_lut: 'O',
      revisit_reason_text: relatedFieldValue,
    }
    surveyData[fieldName] = newData
  }
}

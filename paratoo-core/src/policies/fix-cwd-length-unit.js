const { clone } = require('lodash')

module.exports = async (ctx) => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const warn = (message) =>
    helpers.paratooWarnHandler(`[fix-corrupted-data]-app version: 1.0.3: ${message}`)
  const requestBody = ctx.request.body

  try {
    for (const collection of requestBody?.data?.collections || []) {
      const currSurveyMeta = collection?.['coarse-woody-debris-survey']?.data?.survey_metadata
      let currProvenanceArr = []
      for (const [provenanceType, provenanceVal] of Object.entries(currSurveyMeta?.provenance) || [[],[]]) {
        currProvenanceArr.push(`${provenanceType}=${provenanceVal}`)
      }
      const currProvenance = currProvenanceArr.join(', ')

      for (const [obIndex, cwdOb] of (collection?.['coarse-woody-debris-observation'] || []).entries()) {

        if (Array.isArray(cwdOb?.data?.CWD_section)) {
          for (const [cwdSectionIndex, cwdSection] of cwdOb.data.CWD_section.entries()) {
            if (cwdSection?.length_m) {
              warn(`Found 'length_m' for obIndex=${obIndex} and cwdSectionIndex=${cwdSectionIndex} field that needs to be corrected. Survey provenance: ${currProvenance}`)
              cwdSection.length_cm = clone(cwdSection.length_m)
              delete cwdSection.length_m
            }
          }
        }
      }
    }
  } catch (err) {
    helpers.paratooWarnHandler('Tried to fix CWD length unit', err)
  }
}
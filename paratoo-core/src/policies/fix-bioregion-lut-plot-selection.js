'use strict'

module.exports = async (ctx) => {
  if (ctx.request.url !== '/api/plot-selection-surveys/bulk') {
    helpers.paratooWarnHandler(
      `unable to fix incorrect data for plot selection protocol protocol, because ${ctx.request.url} mismatch`,
    )
    return
  }

  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const requestBody = ctx.request.body.data.collections[0]
  const currSurveyMeta =
    requestBody['plot-selection-survey']?.data?.survey_metadata

  let currProvenanceArr = []
  for (const [provenanceType, provenanceVal] of Object.entries(
    currSurveyMeta?.provenance,
  ) || [[], []]) {
    currProvenanceArr.push(`${provenanceType}=${provenanceVal}`)
  }
  const currProvenance = currProvenanceArr.join(', ')
  const warn = (message) =>
    helpers.paratooWarnHandler(
      `[update-bioregion-lut]- provenance: ${currProvenance} ${message}`,
    )
  // v6 -> v7
  const lutsToUpdate = {
    VVP: 'SVP',
    FLI: 'FUR',
  }
  for (const plot of requestBody['plot-selection']) {
    const oldBioregion = plot.data.plot_name.bioregion
    const newBioregion = lutsToUpdate[oldBioregion]
    if (!newBioregion) continue

    plot.data.plot_name.bioregion = newBioregion
    const oldPlotLabel = plot.data.plot_label
    const relatedProject = plot.data.projects_for_plot.map(
      (project) => project.label,
    )
    // replace substring from -7 to -4 with new bioregion in plot label
    const newPlotLabel = plot.data.plot_label.replace(
      oldBioregion,
      plot.data.plot_name.bioregion,
    )
    plot.data.plot_label = newPlotLabel

    // find and update reserved-plot-labels in database
    const reservedPlotLabel = await strapi.db.query('api::reserved-plot-label.reserved-plot-label')
      .findOne({
        where: {
          plot_label: oldPlotLabel,
        },
      })
    await strapi.db.query('api::reserved-plot-label.reserved-plot-label')
      .update({
        where: {
          id: reservedPlotLabel.id,
        },
        data: {
          plot_label: newPlotLabel,
        },
      })
    
    warn(
      `updated bioregion from ${oldBioregion} to ${newBioregion}, renamed plot label from ${oldPlotLabel} to ${newPlotLabel}, related projects: ${relatedProject.toString()}`,
    )
  }
}

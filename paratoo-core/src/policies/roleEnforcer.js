module.exports = (ctx) => {
  const requestBody = ctx.request.body
  const role = requestBody.role
  const projectArea = requestBody.data['project-areas']
  if (role !== 'project_admin' && projectArea?.length) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    return helpers.paratooErrorHandler(403, new Error('User is not authorised'))
  }
  return true
}

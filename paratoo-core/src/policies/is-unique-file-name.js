const _ = require('lodash')

module.exports = async (ctx) => {
  //this is how Strapi destructures the files object, so do the same
  //see: paratoo-core/node_modules/@strapi/plugin-upload/server/controllers/content-api.js
  const {
    request: { files: { files } = {} },
  } = ctx

  if (!_.isEmpty(files)) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const existingFiles = await strapi.db.query('plugin::upload.file').findMany()

    let filesArr = null
    if (Array.isArray(files)) {
      filesArr = files
    } else {
      filesArr = [files]
    }

    for (const file of filesArr) {
      if (existingFiles.some((f) => f.name === file.name)) {
        helpers.paratooErrorHandler(
          400,
          new Error(`File with name ${file.name} already exists`),
        )
        return false
      }
    }
  }

  return true
}
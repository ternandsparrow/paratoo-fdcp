'use strict'

const lutMainField = 'symbol'
const _ = require('lodash')

/**
 * This policy grabs the model of the associated request and converts associations to LUTs
 * to it's internal ID. This policy should only be added to POST and PUT
 * @param {*} ctx
 * @returns
 */
module.exports = async (ctx) => {
  // reads the existing documentation
  // use the associated route in conjunction with it's path tag to determine model

  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

  const fullDoc = helpers.readFullDocumentation()

  const requestBody = ctx.request.body.data

  // check if bulk request because they are formatted entirely differently
  // According to paratoo-fdcp/ARCHITECTURE/core-bulk.md, bulk requests are
  // identified by their path ending with '/bulk'
  const isBulk = ctx._matchedRoute.split('/').pop() === 'bulk'
  const isMany = ctx._matchedRoute.split('/').pop() === 'many'
  if (isBulk) {
    let newCollectionArray = []
    // according to architecture, bulk requests have an array called collections
    // TODO iterate through the expected values in protocol definitions, even
    // though the payload should still be validated
    for (const collection of requestBody.collections) {
      for (const [key, value] of Object.entries(collection)) {
        // So every field apart from orgMintedIdentifier, refers to a model.
        if (key === 'orgMintedIdentifier') {
          collection[key] = value
          continue
        }
        //check for observations
        collection[key] = await replaceInNestedObservations(
          key,
          collection[key],
          fullDoc,
        )
      }

      newCollectionArray.push(collection)
    }
    requestBody.collections = newCollectionArray
    ctx.request.body.data = requestBody
    return true
  }

  if (isMany) {
    let modelName = ctx.state.route.info.apiName
    // an array of multiple observations
    for (let observation of requestBody[modelName + 's']) {
      observation = await replaceEnum(modelName, { data: observation.data })
    }
    ctx.request.body.data = requestBody
    return true
  }
  // find Strapi internal model for that schema
  // since it's not a bulk of multiple endpoints we know
  // the intended modelname from the request context
  let modelName
  try {
    modelName = ctx.state.route.info.apiName
  } catch (error) {
    strapi.log.error('Could not resolve model name for LUT interpretation')
  }

  ctx.request.body = await replaceEnum(modelName, { data: requestBody })

  return true
}

/**
 * Calls ReplaceEnum recursively with appropriate modelName, to search for nested
 * Observations fields that are included in the documentation
 *
 * @param {String} modelName Where to look in #/components/schemas/ for the associated schema
 * @param {*} data User input payload
 * @param {*} documentation full_documentation.json reference
 */
async function replaceInNestedObservations(modelName, data, documentation) {
  if (Array.isArray(data)) {
    // if data is array, run recursively on each item in the array
    for (let obs in data) {
      data[obs] = await replaceInNestedObservations(
        modelName,
        data[obs],
        documentation,
      )
    }
    return data
  } else if (!data || !data.data) {
    // do nothing
    return data
  } else {
    let newData = await replaceEnum(modelName, data)
    const schemaName = `${_.upperFirst(_.camelCase(modelName))}Request`
    const schemaProperties =
      documentation.components.schemas[schemaName].properties.data.properties

    // run recursively on each property or array of properties with an x-model-ref
    // i.e. each property that is a model (and therefore could contain a LUT reference)
    for (const [propertyName, propertyValue] of Object.entries(
      schemaProperties,
    )) {
      if (propertyValue['x-model-ref']) {
        newData.data[propertyName] = await replaceInNestedObservations(
          propertyValue['x-model-ref'],
          newData.data[propertyName],
          documentation,
        )
      } else if (propertyValue.items && propertyValue.items['x-model-ref']) {
        newData.data[propertyName] = await replaceInNestedObservations(
          propertyValue.items['x-model-ref'],
          newData.data[propertyName],
          documentation,
        )
      }
    }

    return newData
  }
}

/**
 * Checks if the body in a single layer data payload contains any luts
 * with context from the api modelName
 *
 * @param {*} modelName Strapi internal API name. (should exist as strapi.api[name])
 * @param {*} inputBody
 * @returns
 */
async function replaceEnum(modelName, inputBody) {
  let outputBody = inputBody.data
  if (!inputBody.data) return inputBody
  let modelAttributes
  try {
    modelAttributes = strapi.api[modelName].contentTypes[modelName].attributes
  } catch {
    strapi.log.error(
      `Lut interpretation cannot find Strapi model: ${modelName}`,
    )
    return inputBody
  }

  for (const [attributeName, element] of Object.entries(modelAttributes)) {
    if (element.type !== 'relation') continue

    let relationTarget = element.target
    // target in format of     target: 'api::lut-fauna-maturity.lut-fauna-maturity'
    // remove leading 'api::'
    if (
      typeof relationTarget === 'string' &&
      relationTarget.slice(5).split('.')[0].match('^lut-')
    ) {
      const id = await findIdForSymbol(
        outputBody[attributeName],
        relationTarget,
      )
      if (Number.isInteger(id)) outputBody[attributeName] = id
      // strapi.log.info(
      //   `LUT interpretation interpreted value in ${attributeName} from relationTarget:'${relationTarget}' as internal id: ${outputBody[attributeName]}`,
      // )
    }
  }

  // Find components from attributes (where type=component)
  for (const [key, field] of Object.entries(modelAttributes)) {
    if (
      field.type !== 'component' ||
      !outputBody?.[key] ||
      typeof outputBody?.[key] !== 'object'
    )
      continue

    const componentName = field.component
    // Search for LUT fields in the base component

    // Change those values in the requestBody
    const componentAttributes = strapi.components[componentName].attributes
    const componentAssociations = []

    //this loop does two things:
    //  - finds relations in the component's fields (`componentAssociations`) (previous
    //    versions of this policy used `Array.filter`)
    //  - checks/handles nested components
    for (const [k, o] of Object.entries(componentAttributes)) {
      if (
        o.type === 'relation' &&
        o.target.slice(5).split('.')[0].match('^lut-')
      ) {
        componentAssociations.push([k, o])
      }

      if (o.type === 'component') {
        strapi.log.debug(
          `Found nested component '${k}' (${
            o?.repeatable ? 'repeatable' : 'single'
          })`,
        )
        const nestedComponentAttributes =
          strapi.components[o.component].attributes
        const nestedComponentAssociations = []
        for (const [n_k, n_o] of Object.entries(nestedComponentAttributes)) {
          if (
            n_o.type === 'relation' &&
            n_o.target.slice(5).split('.')[0].match('^lut-')
          ) {
            nestedComponentAssociations.push([n_k, n_o])
          }
        }

        if (Array.isArray(outputBody?.[key])) {
          for (const [index] of Object.entries(outputBody?.[key])) {
            outputBody[key][index] = await resolveComponentAssociations(
              nestedComponentAssociations,
              o,
              outputBody?.[key][index],
              k,
            )
          }
        } else {
          outputBody[key] = await resolveComponentAssociations(
            nestedComponentAssociations,
            o,
            outputBody?.[key],
            k,
          )
        }
      }
    }

    outputBody = await resolveComponentAssociations(
      componentAssociations,
      field,
      outputBody,
      key,
    )
  }

  return { data: outputBody }
}

/**
 * Resolves component associations in the given outputBody using the provided
 * componentAssociations and field
 *
 * @param {Array.<Array>} componentAssociations the component associations to be resolved
 * @param {Object} field the field of the component
 * @param {Object} outputBody the output body object
 * @param {String} key the first key to access in the outputBody
 * @return {Object} The resolved output body
 */
async function resolveComponentAssociations(
  componentAssociations,
  field,
  outputBody,
  key,
) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  if (outputBody[key]) {
    for (const [componentAttributeName, element] of componentAssociations) {
      // case of repeatable component (fields in an array)
      if (field.repeatable === true) {
        for (const [index] of Object.entries(outputBody[key])) {
          const id = await findIdForSymbol(
            outputBody[key][index][componentAttributeName],
            element.target,
          )
          if (Number.isInteger(id))
            outputBody[key][index][componentAttributeName] = id
        }
      }
      // case of single component
      else if (field.repeatable === false) {
        const id = await findIdForSymbol(
          outputBody?.[key]?.[componentAttributeName],
          element.target,
        )
        if (Number.isInteger(id)) outputBody[key][componentAttributeName] = id
      } else {
        helpers.paratooDebugMsg(
          `${componentAttributeName} couldn't find repeatable keyword, defaulting to false`,
          true,
          'warning',
        )
        const id = await findIdForSymbol(
          outputBody?.[key][componentAttributeName],
          element.target,
        )
        if (Number.isInteger(id)) outputBody[key][componentAttributeName] = id
      }
    }
  }

  return outputBody
}

async function findIdForSymbol(symbol, apiTarget) {
  if (!symbol || !apiTarget) {
    return
  }

  const dbReturn = await strapi.db
    .query(apiTarget)
    .findOne({ where: { [lutMainField]: symbol } })
  return dbReturn.id
}

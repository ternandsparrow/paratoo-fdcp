'use strict'

module.exports = async (ctx) => {
  if (ctx.request.url !== '/api/coarse-woody-debris-surveys/bulk') {
    helpers.paratooWarnHandler(
      `unable to fix incorrect data for plot selection protocol protocol, because ${ctx.request.url} mismatch`,
    )
    return
  }

  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const requestBody = ctx.request.body.data.collections[0]
  const currSurveyMeta =
    requestBody['coarse-woody-debris-survey']?.data?.survey_metadata

  let currProvenanceArr = []
  for (const [provenanceType, provenanceVal] of Object.entries(
    currSurveyMeta?.provenance,
  ) || [[], []]) {
    currProvenanceArr.push(`${provenanceType}=${provenanceVal}`)
  }
  const currProvenance = currProvenanceArr.join(', ')
  const warn = (message) =>
    helpers.paratooWarnHandler(
      `[fix-cwd-tree-stumps]- provenance: ${currProvenance} ${message}`,
    )

  const obsData = requestBody['coarse-woody-debris-observation']
  obsData.forEach((obs, index) => {
    if (!obs?.data) return
    if (obs.data.CWD_section?.length && obs.data.tree_stump?.length) {
      warn(
        `record with id: ${obs.data.id} has both CWD_Section and tree_stump data`,
      )
      const obsWithCWD = structuredClone(obs)
      delete obsWithCWD.data.tree_stump
      obsWithCWD.data.tree_stumps = false
      obsData[index] = obsWithCWD

      const obsWithTreeStump = structuredClone(obs)
      delete obsWithTreeStump.data.CWD_section
      obsWithTreeStump.data.CWD = false
      obsData.push(obsWithTreeStump)
    }
  })
}

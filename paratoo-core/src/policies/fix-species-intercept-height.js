module.exports = async (ctx) => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const warn = (message) =>
    helpers.paratooWarnHandler(`[fix-corrupted-data]-app version: 1.0.3+1: ${message}`)

  const requestBody = ctx.request.body

  //will handle Cover and Cover+Fire (standard and enhanced for both modules)
  try {
    for (const collection of requestBody?.data?.collections || []) {
      const currSurveyMeta = collection?.['cover-point-intercept-survey']?.data?.survey_metadata
      let currProvenanceArr = []
      for (const [provenanceType, provenanceVal] of Object.entries(currSurveyMeta?.provenance) || [[],[]]) {
        currProvenanceArr.push(`${provenanceType}=${provenanceVal}`)
      }
      const currProvenance = currProvenanceArr.join(', ')

      const points = collection?.['cover-point-intercept-point']
      for (const [pointIndex, point] of points.entries() || [[], []]) {
        if (point?.data?.species_intercepts?.length > 0) {
          for (let [interceptIndex, intercept] of point?.data?.species_intercepts.entries() || [[], []]) {
            if (
              typeof intercept?.data?.height === 'string' &&
              //checks for floats and ints
              Number.isFinite(Number(intercept.data.height))
            ) {
              warn(`Intercept for point index=${pointIndex} (point number ${point?.data?.point_number} of  transect ${point?.data?.cover_transect_start_point}; intercept index=${interceptIndex}) 'height' (val=${intercept.data.height}) must be cast to number (float/int). Survey provenance: ${currProvenance}`)

              //we have a reference to the value, so can just re-assign at this array
              //item, rather than squashing the whole array
              intercept.data.height = Number(intercept.data.height)
            }
          }
        }
      }
    }
  } catch (err) {
    helpers.paratooWarnHandler('Tried to fix species intercept height', err)
  }
}
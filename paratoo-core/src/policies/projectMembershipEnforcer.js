'use strict'
const {
  validateSingleProjectIdCb,
} = require('../constants')
//TODO use a user-based cache, right now we clear the cache for every request
let authorisedProjects = {}

/**
 * Policy to ensure the User is authorized to access the resource on the endpoint this is
 * applied to. Will attempt to resolve the project and protocol ID then use these to make
 * a request of the Org's PDP which will determine authorization
 *
 * @param {Object} ctx Koa context
 * @returns error if not authorized, else goes to next policy
 */
module.exports = async (ctx) => {
  authorisedProjects = {}
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const endpointPrefix = `/${ctx._matchedRoute.split('/')[2]}`
  const isBulk = ctx._matchedRoute.split('/')[3] == 'bulk'
  strapi.log.info(`Checking permissions for endpoint: ${endpointPrefix}`)
  const requestBody = ctx.request.body
  const singularModelName = ctx.state.route.info.apiName

  // helpers.paratooDebugMsg('PME requestBody: ' + JSON.stringify(requestBody, null, 2), true)

  //allow access to LUTs
  if (endpointPrefix.startsWith('/lut-')) {
    if (ctx.request.method == 'GET') {
      return true
    }

    //we shouldn't get here as we've removed LUT write routes
    let msg = 'Only read permissions is allowed for LUTs'
    return helpers.paratooErrorHandler(403, new Error(msg))
  }

  const auth = ctx.request.headers['authorization']
  if (!auth) {
    let msg = 'User is not authenticated (logged in) - no auth token supplied'
    return helpers.paratooErrorHandler(401, new Error(msg))
  }
  // if token is an api token with full access
  const status = await helpers.apiTokenCheck(auth)
  if (status?.type == 'full-access') return true
  
  const version = parseInt(ctx.request.header['x-version']) || 1
  if (!parseInt(ctx.request.header['x-version'])) {
    strapi.log.debug(
      'No protocol version was explicitly supplied, defaulting to version 1',
    )
  } else {
    strapi.log.debug(
      `Protocol version has been explicitly supplied as version ${version}`,
    )
  }

  strapi.log.debug(`Request ${isBulk ? 'is': 'is NOT'} a bulk collection submission`)
  const writeFrag = (ctx.request.method == 'GET' ? 'read' : 'write')
  
  if (
    ctx.request.method == 'GET' &&
    //special case for the GET request
    ctx.state.route?.allowMethod !== true
  ) {
    //GET requests can pass singular or plural project/protocol IDs, so easier to treat
    //as multiple, even if singular
    const projectIds = helpers.parseGetReqUrlParam({
      queryParams: ctx.request.query,
      paramKeySingular: 'project_id',
    })
    const protocolUuids = helpers.parseGetReqUrlParam({
      queryParams: ctx.request.query,
      paramKeySingular: 'prot_uuid',
    })

    if (projectIds?.length > 0 && protocolUuids?.length > 0) {
      strapi.log.debug(`Checking with PDP for passed project(s) (${projectIds}) and protocol(s) ${protocolUuids}`)
      //in get request if project id and protocol id exist, check pdp permission
      return await checkMultipleProjectProtocols({
        projectIds,
        protocolUuids,
        writeFrag,
        auth,
      })
    } else {
      strapi.log.debug('No project/protocol passed, checking against user projects/models')
      //check user projects if they can access model at all (similarly to pdp-data-filter
      //middleware)
      //if they're authorised the access the model generally, we can let the request
      //happen with the assumption the pdp-data-filter will remove any instances of data
      //they cannot access for the authorised model
      const userProjects = helpers.parseResponseData(
        await helpers.getAllUserProjects(auth),
      )?.projects || []
      const allProtocols = await helpers.getAllProtocols()
      const userModels = await helpers.getAllUserModels(allProtocols, userProjects)

      if (
        userModels?.models?.length > 0 &&
        !userModels.models.includes('plot-selection') &&
        //can access plot layout, so should be able to read plot selection
        userModels.models.includes('plot-layout')
      ) {
        //edge case to allow non-admin users to read plot selection (as they need it for
        //plot layout)
        userModels.models.push('plot-selection')
      }

      if(singularModelName === 'debug-client-state-dump') {
        //returns true as the authorisation check is done in the controller
        return true
      }

      if (singularModelName === 'org-uuid-survey-metadata')
        return checkOrgUuidSurveyMetadataAuthorisation(
          ctx.state.route.method,
          auth,
        )

      if (!userModels.models?.includes(singularModelName)) {
        //TODO should probably be 403 - #1634
        return helpers.paratooErrorHandler(401, new Error(`No user projects that authorize accessing '${singularModelName}'`))
      } else {
        return true
      }
    }
    //we shouldn't get here, but if we do we have an unhandled instance of GET requests,
    //so assume it's not allowed (we'll fall-though past the POST case to the bottom
    //`return true`)
  }


  if (
    ctx.request.method === 'POST' &&
    //if a POST has passed this flag, it's a special case and isn't handled here
    ctx.state.route?.allowMethod !== true
  ) {
    let protocolId, projectId
    let collections = isBulk ? requestBody.data.collections : requestBody.data
    collections = Array.isArray(collections) ? collections : [collections]

    if (
      //bulk should always include orgMintedIdentifier
      isBulk ||
      //plot selection is not bulk but does include orgMintedIdentifier. This protocol
      //should be bulked (#1664), but in the meantime it POSTs to non-bulk survey
      //endpoint then POSTs plot selections to the /many endpoint
      ['/plot-selections', '/plot-selection-surveys'].includes(endpointPrefix)
    ) {
      // bulk collection POST requests
      const decodedIdentifier = []
      //a bulk request *can* have multiple collections of the *same* protocol (as the bulk
      //endpoint is scoped to protocols), but possibly many projects. right now, the app
      //doesn't support that, but since the API does we need to handle that case
      projectId = []

      for(let i = 0; i < collections.length; i++) {
        // if mintedIdentifier doest exist
        if (!collections[i]) continue
        if (!collections[i].orgMintedIdentifier) continue

        await confirmProtocolIdConsistency(collections[i], endpointPrefix, helpers, ctx.request.url)

        helpers.paratooDebugMsg(`orgMintedIdentifier exists in ${isBulk ? 'bulk': 'non bulk'} submission`, true)
        try {
          decodedIdentifier.push(JSON.parse(Buffer.from(collections[i].orgMintedIdentifier, 'base64')))
        } catch(err) {
          let msg = 'Could not decode the orgMintedIdentifier'
          helpers.paratooDebugMsg('Error: ' + err + '. Message: ' + msg, true)
          return helpers.paratooErrorHandler(400, err, msg)
        }
        projectId.push(decodedIdentifier[i].survey_metadata.survey_details.project_id)
      }
      
      if (projectId.length == 0) {
        let msg = 'No project ID found in orgMintedIdentifier'
        return helpers.paratooErrorHandler(400, new Error(msg))
      }

      // user is POSTing to a particular protocol so we assume that protocol ID will be the
      // same for the whole collection
      protocolId = decodedIdentifier[0].survey_metadata.survey_details.protocol_id
      helpers.paratooDebugMsg('decodedIdentifier: ' + JSON.stringify(decodedIdentifier) + '\nprojectId: ' + JSON.stringify(projectId) + '\nprotocolId: ' + JSON.stringify(protocolId), true)

      let url, resp
      for(let i = 0; i < projectId.length; i++) {
        url = `pdp/${projectId[i]}/${protocolId}/${writeFrag}`
        resp = await sendPDPRequest(url, auth)
        if (!resp.data.isAuthorised) {
          //TODO should probably be 403 - #1634
          return helpers.paratooErrorHandler(401, new Error(createUnauthorisedResponse({
            isAuthorised: resp.data.isAuthorised,
            writeFrag,
            projectId: projectId[i],
            protocolUuid: protocolId,
          })))
        }
      }
    }

    //none of the PDP requests for this POST returned false, so assume authorized
    return true
  }

  if (ctx.state.route.allowMethod) {
    strapi.log.debug(`${ctx.state.route.method} request at ${endpointPrefix} is allowed. Checking authorisation...`)
    //while a generic solution is desired, it's not required as there are no other known
    //cases to implement. So go with a semi-hard-coded solution where we handle each
    //`allowMethod` cases individually. If we need to, we can come up with something
    //generic that might leverage the PDP data filter
    switch (singularModelName) {
      case 'plot-visit':
        //only update (PUT) is set to `allowMethod`
        return await checkPlotVisitUpdateAuthorisation({
          visitId: ctx.params.id,
          auth,
        })
      case 'reserved-plot-label':
        return await checkReservedPlotLabelAuthorisation({
          method: ctx.state.route.method, //multiple methods have the flag
          requestContext: ctx.request,
          auth,
        })
      case 'generate-barcode':
        //the controller is checking authorisation, so just check authentication
        //TODO move the authorisation checks from the controller to here
        return !!auth
      case 'debug-client-state-dump':
        //enough to check that they're authenticated
        return !!auth
      default:
        helpers.paratooWarnHandler(`${ctx.state.route.method} request at ${endpointPrefix} is allowed, but an unknown model was passed so cannot continue`)
        return false
    }
  }

  //none of the above cases returned true, so assume not authorized
  //could get here if:
  //  1. we've implicitly handled the rejection of the request by not explicitly handling
  //     it above
  //  2. it's an unhandled case
  //because of (2), we want to warn when this does happen. might become too chatty though
  //so need to keep an eye on it for tweaks
  helpers.paratooWarnHandler(`${ctx.state.route.method} request at ${endpointPrefix} is not allowed`)
  return false
}

async function sendPDPRequest(url, auth) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  helpers.paratooDebugMsg(`PME making PDP Request at URL endpoint: ${url}`)

  let resp = null
  try {
    resp = await helpers.orgGetRequest(
      url,
      auth,
      true,   //`isCustomApi`
    )
  } catch (err) {
    console.error(err)
    let useMsg = 'Unknown error'
    let useStatus = 500
    if (err?.message) {
      useMsg = err.message
    }
    if (err?.status) {
      useStatus = err.status
    }

    return helpers.paratooErrorHandler(useStatus, useMsg)
  }
  console.log(resp)
  if (
    resp?.status &&
    //caller handles these codes, so we only want to handle other codes
    ![200, 401, 403].includes(resp.status)
  ) {
    let msg = 'There was an issue checking permissions'
    if (resp?.data?.message) {
      msg += `. Got error: ${resp.data.message}`
    } else {
      helpers.paratooDebugMsg(`PDP call (${url}) received API error, but no additional message`, true)
    }
    
    return helpers.paratooErrorHandler(resp?.status, new Error(msg))
  }

  return resp
}

/**
 * Checks the authorisation of multiple project(s)/protocol(s). Will do PDP calls for all
 * combinations, but won't validate until the end; if a given protocol/project combo is
 * not authorised, it checks if that protocol *is* authorised for another of the user's
 * projects
 * 
 * @param {Array.<Number | String>} projectIds array of project IDs (paratoo) or UUIDs
 * (Monitor/MERIT)
 * @param {Array.<String>} protocolUuids array of protocol UUIDs
 * @param {'read' | 'write'} writeFrag whether we're checking read or write perms
 * @param {String} auth bearer token
 * 
 * @returns {Boolean | Error} `true` is authorised, else an error
 */
async function checkMultipleProjectProtocols({
  projectIds,
  protocolUuids,
  writeFrag,
  auth,
}) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  for (const projId of projectIds) {
    for (const protUuid of protocolUuids) {
      const url = `pdp/${projId}/${protUuid}/${writeFrag}`
      const resp = await sendPDPRequest(url, auth)
      //don't do check until the end as a given project/protocol combo might be
      //invalid in isolation, but there might be another project that has been
      //requested where the protocol is valid
      addAuthorisedProjectProtocol(
        projId,
        { [protUuid]: resp.data.isAuthorised },
      )
    }
  }

  const unauthorised = []   //objects with `projectId` and `protUuid` keys
  for (const [projectId, protAuthArr] of Object.entries(authorisedProjects)) {
    for (const protAuth of protAuthArr) {
      // console.log('protAuth:', protAuth)
      //object is only ever 1 key/value pair, so can check 0th array item
      const protUuid = Object.keys(protAuth)[0]
      const protIsAuth = protAuth[protUuid]

      if (
        !protIsAuth &&
        !protIsAuthorisedForDifferentProject(projectId, protUuid)
      ) {
        unauthorised.push({
          projectId: projectId,
          protUuid: protUuid,
        })
      }
    }
  }
  if (unauthorised.length > 0) {
    const msg = `User is NOT authorized to read projects/protocols: ${
      unauthorised.map(e => `${e.projectId}/${e.protUuid}`).join(', ')
    }`
    //TODO should probably be 403 - #1634
    return helpers.paratooErrorHandler(401, new Error(msg))
  }
  //none of the project/s or protocol/s returned an error, so assume authorized
  return true
}

function createUnauthorisedResponse({ isAuthorised, writeFrag, projectId, protocolUuid }) {
  return `User ${
    isAuthorised ? 'is' : 'is NOT'
  } authorized to ${writeFrag}` + ` project/protocol ${projectId}/${protocolUuid}`
}

/**
 * Adds a project/protocol pair to the running list of `authorisedProjects`
 * 
 * @param {*} project 
 * @param {*} protocolAuth 
 */
function addAuthorisedProjectProtocol(project, protocolAuth) {
  if (authorisedProjects[project]) {
    authorisedProjects[project].push(protocolAuth)
  } else {
    authorisedProjects[project] = [protocolAuth]
  }
}

function protIsAuthorisedForDifferentProject(projectId, protocolUuid) {
  for (const [projId, protAuthArr] of Object.entries(authorisedProjects)) {
    //don't need to check same project
    if (projId === projectId) continue

    for (const protAuth of protAuthArr) {
      const protUuid = Object.keys(protAuth)[0]
      const protIsAuth = protAuth[protUuid]

      if (protUuid === protocolUuid && protIsAuth) return true
    }
  }
  //didn't find the protocol in a different project, so assume not authorised
  return false
}

/**
 * Checks if the provided visit is assigned to one of the user's projects.
 * Follows the relation plot-visit --> plot-layout --> plot-selection then checks if that
 * selection is in a project
 * 
 * @param {Number} visitId the visit ID
 * @param {String} auth the auth token
 * 
 * @returns {Boolean | Error} true if the user is authorised, else an error
 */
async function checkPlotVisitUpdateAuthorisation({ visitId, auth }) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const currVisitData = await strapi.entityService.findOne(
    'api::plot-visit.plot-visit',
    visitId,
    {
      populate: 'deep',
    },
  )
  const plotUuid = currVisitData?.plot_layout?.plot_selection?.uuid

  const userProjects = helpers.parseResponseData(
    await helpers.getAllUserProjects(auth),
  )?.projects || []

  let isAuthorised = false
  for (const proj of userProjects) {
    if (
      proj?.plot_selections.some(
        p => p.uuid === plotUuid
      )
    ) {
      isAuthorised = true
      break
    }
  }

  let msg = `User ${isAuthorised ? 'is' : 'is not'} authorised to update plot-visit with ID=${visitId}`
  strapi.log.debug(msg)

  if (isAuthorised) {
    return true
  }
  return helpers.paratooErrorHandler(403, new Error(msg))
}

async function checkReservedPlotLabelAuthorisation({ method, requestContext, auth }) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  if (['POST', 'DELETE'].includes(method)) {
    //need to do check manually, as this data isn't part of the schema and thus cannot be
    //picked up by the validator
    let isValid = false
    let projectIds = null
    if (method === 'POST') {
      isValid = checkReservedPlotLabelBody({ requestBody: requestContext.body })
      projectIds = requestContext?.body?.data?.projects
    } else if (method === 'DELETE') {
      projectIds = helpers.parseGetReqUrlParam({
        queryParams: requestContext.query,
        paramKeySingular: 'project_id',
      })
      isValid = checkReservedPlotLabelParams({ params: { project_ids: projectIds } })
    }
    if (!isValid) {
      return helpers.paratooErrorHandler(400, new Error('Invalid: request variables must contain array of projects'))
    }

    const protocols = await helpers.getAllProtocols()
    const plotSelectionProtocol = protocols.find(
      p => p.identifier === 'a9cb9e38-690f-41c9-8151-06108caf539d'
    )

    return await checkMultipleProjectProtocols({
      projectIds,
      protocolUuids: [plotSelectionProtocol?.identifier],
      writeFrag: 'write',
      auth,
    })
  } else {
    let msg = `Method ${method} for endpoint /reserved-plot-labels is not allowed`
    //likely won't get here (as this function only gets called when we have `allowMethod`
    //flag), but worth handling anyway
    return helpers.paratooErrorHandler(403, new Error(msg))
  }
}

function checkReservedPlotLabelBody({ requestBody }) {
  return (
    requestBody?.data &&
    Object.keys(requestBody?.data)?.includes('projects') &&
    Array.isArray(requestBody?.data?.projects) &&
    requestBody?.data?.projects?.length > 0 &&
    requestBody?.data?.projects?.every(validateSingleProjectIdCb)
  )
}

function checkReservedPlotLabelParams({ params }) {
  return (
    params &&
    Object.keys(params).includes('project_ids') &&
    Array.isArray(params.project_ids) &&
    params.project_ids.every(validateSingleProjectIdCb)
  )
}

function getProtocolIdFromPayloadCollection(collection) {
  // plot selection case
  if(collection.survey_metadata) {
    const protocolIdFromPayload = collection.survey_metadata.survey_details.protocol_id
    const protocolVariantFromPayload = collection.protocol_variant
    return { protocolIdFromPayload, protocolVariantFromPayload }
  }
  for (const key in collection) {
    if (collection[key]?.data?.survey_metadata) {
      const protocolIdFromPayload = collection[key].data.survey_metadata.survey_details.protocol_id
      const subProtocolIdFromPayload = collection[key].data.survey_metadata.survey_details.submodule_protocol_id
      let protocolVariantFromPayload = collection[key].data.protocol_variant
      
      // edge case for photopoints
      if(collection[key].data.photopoints_protocol_variant) {
        const photopointsVariantMap = {
          'full': 'DSLR Full',
          'lite': 'Compact Lite',
          'ODL' : 'On-device Lite'
        }
        protocolVariantFromPayload = photopointsVariantMap[collection[key].data.photopoints_protocol_variant]
      }

      return { protocolIdFromPayload, subProtocolIdFromPayload, protocolVariantFromPayload }
    }
  }
}

/**
 * double check if the protocol ID in metadata match the actual protocol ID, as this can exploited to send to
 * an incorrect protocol
 * @param {Object} collection 
 * @param {String} endpointPrefix 
 * @returns 
 */
async function confirmProtocolIdConsistency(
  collection,
  endpointPrefix,
  helpers,
  requestUrl
) {
  const payload = structuredClone(collection)
  console.log('requestUrl', requestUrl)
  // this does not have a survey_metadata
  if (endpointPrefix === '/plot-selections') return
  const {
    protocolIdFromPayload,
    protocolVariantFromPayload,
    subProtocolIdFromPayload,
  } = getProtocolIdFromPayloadCollection(payload, endpointPrefix)

  // edge case for plot selection, the plot-selection-surveys with survey_metadata is sent separately, with plot-selection come after
  // TODO: remove this one, for now we need this to handle any pending /many collections sitting in frontend, but this should be removed in the future
  if(requestUrl === '/plot-selection-surveys') {
    endpointPrefix = '/plot-selections'
  }
  let protocols = await strapi.db.query('api::protocol.protocol').findMany({
    where: { endpointPrefix },
  })
  console.log('protocolVariant', protocolVariantFromPayload)
  console.log('protocolVariant', protocolVariantFromPayload)
  let protocol
  if (protocols.length > 1) {
    protocols = protocols.filter((protocol) => {
      const match = protocol.workflow.some(
        (step) =>
          endpointPrefix.includes(step.modelName) &&
          protocolVariantFromPayload === step['protocol-variant'],
      )
      return match
    })
    if (protocols.length > 1) {
      // FIXME: Cover protocols - we resue the same survey model for fire, there's would be two matched indentifiers
      // not sure if this is a good way to check correct uuid , atm there's no other way to look for the correct protocol
      const identifiers = protocols.map(({ identifier }) => identifier)
      console.log(identifiers)
      if (!identifiers.includes(protocolIdFromPayload)) {
        let msg = `Protocol ID ${protocolIdFromPayload} in metadata does not match the actual protocol ID of "${name}"`
        throw helpers.paratooErrorHandler(400, new Error(msg))
      } else return
    }
  }

  protocol = protocols[0]
  const { identifier, name } = protocol

  const checkUUIDMismatch = (uuid, expectedId, errorMsg) => {
    if (uuid !== expectedId) {
      throw helpers.paratooErrorHandler(400, new Error(errorMsg))
    }
  }

  // for fire-survey case it use uuid from cover+fire
  if (subProtocolIdFromPayload) {
    checkUUIDMismatch(
      identifier,
      subProtocolIdFromPayload,
      `Protocol ID in metadata does not match the actual protocol ID of "${name}"`,
    )
  } else if (identifier !== protocolIdFromPayload) {
    checkUUIDMismatch(
      identifier,
      protocolIdFromPayload,
      `Protocol ID in metadata does not match the actual protocol ID of "${name}"`,
    )
  }
}

/**
 * 
 * @param {*} method http methods
 * @param {*} request http request object 
 * @param {*} auth auth token
 * @returns {Boolean} whether the user is authorised
 */
function checkOrgUuidSurveyMetadataAuthorisation(method, auth) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  if(method !== 'GET') {
    throw helpers.paratooErrorHandler(405, new Error('Method not allowed'))
  }
  return Boolean(auth)
}
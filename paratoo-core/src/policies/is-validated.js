'use strict'

const { performance } = require('perf_hooks')
const Ajv = require('ajv')
const ajv = new Ajv({ strictSchema: false })
const addFormats = require('ajv-formats')
const {
  uuidRegex,
  validateSingleProjectIdCb,
} = require('../constants')
const { validate : isUuid} = require('uuid')
const { isNil } = require('lodash')
const { setTimeout } = require('node:timers/promises')

addFormats(ajv)

const queryParamsForAjv = [
  'project_id',
  'prot_uuid',
  'project_ids',
  'prot_uuids',
  'projectId',    //org pdp
  'protocolUuid',   //org pdp
  'client_state_uuid'
]

//FIXME want to use `oneOf` (type string format uuid and type number), but AJV doesn't seem to want to validate this, so use a custom format
ajv.addFormat('project_id', {
  type: 'string',
  validate: validateSingleProjectIdCb,
})

ajv.addFormat('project_ids_str', {
  type: 'string',
  //validate comma-separated strings by seeing if the helper can parse
  validate: (p) => {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const resp = helpers.parseGetReqUrlParam({
      queryParams: {
        project_ids: p,
      },
      paramKeySingular: 'project_id',
    })
    return Array.isArray(resp) && resp.every(validateSingleProjectIdCb)
  },
})

ajv.addFormat('protocol_ids_str', {
  type: 'string',
  //validate comma-separated strings by seeing if the helper can parse
  validate: (p) => {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const resp = helpers.parseGetReqUrlParam({
      queryParams: {
        prot_uuids: p,
      },
      paramKeySingular: 'prot_uuid',
    })
    return Array.isArray(resp) && resp.every(o => uuidRegex.test(o))
  },
})

ajv.addFormat('client_state_uuid', {
  type: 'string',
  validate: (val) => isUuid(val),
})

module.exports = async (ctx) => {
  // we don't need to check type of all path perimeters
  // e.g. org/status/{identifier}
  // by default type of identifier is number
  //   but the collection identifier can be string as well
  //   so we skip type checking
  // FIXME path param handler and relevant tests
  const skipTypeChecking = [
    'get/org/status/{identifier}', 
    'get/org/pdp/{projectId}/{protocolUuid}/read',
    'get/org/pdp/{projectId}/{protocolUuid}/write'
  ]
  var t0 = performance.now()
  // grab the whole documentation (with overrides)
  // we dereference the OpenAPI references so that we can access the individual schema we need
  // another method would be better
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

  // delay api response time 
  // NOTE: it's helpful for local testing e.g. merit apis are slower than org apis
  const waitTime = process.env.DELAY_RESPONSE_TIME
  if (waitTime>0 && !isNil(waitTime) && process.env.NODE_ENV != 'production') {
    helpers.paratooWarnHandler(`Waiting ${waitTime} milliseconds to start processing`)
    await setTimeout(waitTime)
  } 

  const fullDoc = helpers.readFullDocumentation()
  let associatedRoute = ctx.routerPath

  // Converts the route into the format used in strapi-plugin-documentation. For example '/name/:id' to '/name/{id}'
  associatedRoute = helpers.formatApiEndPoint(associatedRoute)

  // Get strapi's documentation for this API interface
  const thisPath =
    fullDoc.paths[associatedRoute][ctx.request.method.toLowerCase()]
  // ---------- Validate path/query parameters ----------
  if (
    Object.keys(thisPath).includes('parameters') &&
    Array.isArray(thisPath.parameters) &&
    thisPath.parameters.length > 0
  ) {
    for (const expectedParam of thisPath.parameters) {
      const queryParam = ctx?.params?.[expectedParam.name] || ctx?.request?.query?.[expectedParam.name]
      if (!queryParam) continue
      if (
        //the param is an integer, or it's a string with ID (which should be a number)
        (expectedParam.schema.type === 'integer' ||
          // from strapi 4.4.5 'number' is also used for 'id'
          expectedParam.schema.type === 'number' ||
          (expectedParam.schema.type === 'string' &&
            expectedParam.name === 'id')) &&
        Object.keys(ctx).includes('params') &&
        Object.keys(ctx.params).includes(expectedParam.name)
      ) {
        if (skipTypeChecking.includes(thisPath.operationId)) continue
        let valid
        //parse as int or float and check if weakly-equal to the queryParam,
        //since parseInt on a float will cause the decimal to be dropped
        if (parseInt(queryParam) == queryParam) {
          valid = true
        } else if (
          parseFloat(queryParam) == queryParam &&
          //https://stackoverflow.com/a/12467708/6476994
          queryParam.indexOf('.') != -1
        ) {
          valid = false
        } else {
          valid = false
        }

        if (!valid) {
          helpers.paratooErrorHandler(
            400,
            new Error(
              `Invalid: query parameter '${expectedParam.name}' with value '${queryParam}' is not an integer`,
            ),
          )
          return false
        }
      }
      if (queryParamsForAjv.includes(expectedParam.name)) {
        const paramSchema = thisPath.parameters.find(p => p.name === expectedParam.name)?.schema
        const validate = ajv.compile(paramSchema)
        let valid = validate(queryParam)
  
        if (!valid) {
          helpers.paratooErrorHandler(
            400,
            new Error(`Invalid: parameter '${expectedParam.name}' with value '${queryParam}' ${ajv.errorsText(validate.errors)}`),
          )
        }
      }
    }
  }

  let docParameterBodySchema = thisPath.requestBody


  // ---------- Validate Body parameters ----------
  if (!!docParameterBodySchema && typeof docParameterBodySchema === 'object') {
    docParameterBodySchema =
      docParameterBodySchema.content['application/json']['schema']

    docParameterBodySchema = dereferenceSection(docParameterBodySchema, fullDoc)

    // If it's a PUT, the entry already has all required fields
    // Therefore nothing is required but fields that do exist are still validated
    if (ctx.request.method === 'PUT') {
      delete docParameterBodySchema.properties.data.required
    }
    // TODO cache the compiled schemas (optimisation)
    const validate = ajv.compile(docParameterBodySchema)
    let valid = validate(ctx.request.body)

    if (!valid) {
      strapi.log.debug(
        'is-validated caught this validation error. errorText:',
        ajv.errorsText(validate.errors),
      )
      helpers.paratooErrorHandler(
        400,
        new Error(`Invalid: ${ajv.errorsText(validate.errors)}`),
      )
      return false
    }
  }

  // validates internal conditions
  // e.g in Basal Wedge Observation, total of in_tree And borderline_tree
  //    together should be greater than 7
  let valid = await helpers.internalValidator(ctx)

  if (valid !== true) {
    strapi.log.debug(
      'internal validator caught this validation error. errorText:' +
        valid.errorsText,
    )
    helpers.paratooErrorHandler(
      400,
      new Error(`Invalid: ${valid.errorsText}`),
    )
    return false
  }
  
  const t1 = performance.now()
  // make an indication in the console about whether the validation allowed something for debug purposes
  strapi.log.debug(
    `API level validation accepted (took ${Math.round(t1 - t0)} ms)`,
  )

  return true
}

/**
 * Dereferences an object's $ref's based on relative paths in 'source'. Returns the dereferenced object.
 * This is used for performance and practicality reasons. TODO configure AJV to treat references to current JSON
 * as references to fullDoc
 * @param {*} section
 * @param {*} source
 */
function dereferenceSection(section, source) {
  let newSection = section

  if (!(section && typeof section === 'object')) {
    // section is not valid, ignore
    return section
  }

  // Iterating through object
  for (const [key, property] of Object.entries(section)) {
    if (typeof property === 'object') {
      newSection[key] = dereferenceSection(property, source)
    }

    if (key === '$ref') {
      let ref = property
      ref = ref.split('/')
      let accObj = source
      // find the part of the documentation the ref refers to (by iterating through)
      for (const value of ref) {
        if (value === '#') continue
        accObj = accObj[value]
      }

      // Dereference nested references
      accObj = dereferenceSection(accObj, source)

      // we replace the whole object because a ref will always be the only value of an object
      newSection = accObj
    }
  }
  return newSection
}

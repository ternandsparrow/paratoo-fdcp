const semver = require('semver')

module.exports = async (ctx) => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const requestBody = ctx.request.body

  let currentVersion = helpers.findValForKey(
    requestBody,
    'version_core_documentation',
  )
  const targetVersion = '1.0.4'

  function compareVersionsWithBuild(v1, v2) {
    if (!v1 || typeof v1 !== 'string') {
      return -1 // Handle cases where one or both versions are undefined
    } else {
      const [coreV1, buildV1] = v1.split('+')
      const [coreV2, buildV2] = v2.split('+')

      const coreCompare = semver.compare(coreV1, coreV2)
      if (coreCompare !== 0) {
        return coreCompare
      }
      // compare the content after +
      const buildNum1 = buildV1 ? parseInt(buildV1.match(/\d+/)[0], 10) : 0
      const buildNum2 = buildV2 ? parseInt(buildV2.match(/\d+/)[0], 10) : 0

      return buildNum1 - buildNum2
    }
  }

  if (currentVersion && currentVersion !== '0.0.0-xxxx') {
    currentVersion = currentVersion.split('-')[0]
    const result = compareVersionsWithBuild(currentVersion, targetVersion)
    if (result < 0) {
      let msg =
        'Unable to process Targeted Survey due to field changes. Please contact the EMSA help desk.'
      return helpers.paratooErrorHandler(400, new Error(msg))
    }
  }
}

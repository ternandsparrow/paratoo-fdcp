// the URL where paratoo-core can reach paratoo-org. It only needs to be
// resolvable to this server so docker-compose internal hostnames, etc are
// fine. e.g.
//    http://localhost:1338
//    http://org:1337
const uuidRegex = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/i
const positiveIntRegex = /^\d+$/
module.exports = {
  paratooOrgUrl: reqEnv('ORG_URL_PREFIX'),
  paratooOrgApiPrefix: reqEnv('ORG_API_PREFIX'),
  paratooOrgCustomApiPrefix: reqEnv('ORG_CUSTOM_API_PREFIX'),
  positiveIntRegex: positiveIntRegex,
  //need to use regex for UUID, even though `uuid.validate()` is preferred as some
  //protocol IDs are not technically valid, even though they were generated with the same
  //library. Likely that some of them were edited after being generated
  uuidRegex: uuidRegex,
  validateSingleProjectIdCb: (p) => uuidRegex.test(p) || positiveIntRegex.test(p),
  PLOT_MODELS: ['plot-visit', 'plot-layout', 'plot-selection'],
  //special error codes to send to the client, which might provide additional information
  //about how to handle, etc.
  ERROR_CODES: [
    'ERR_MISSING_MINTED_ORG_IDENTIFIER',
  ],
}

function reqEnv(name) {
  const result = process.env[name]
  if (result) {
    return result
  }
  throw new Error(`No value found for required env var ${name}`)
}

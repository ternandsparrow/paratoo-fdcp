# Features this demonstrates
  - multiple models
  - relationships between models
  - a "business logic" endpoint (`/bird_survey/bulk`) that builds on the existing CRUD endpoints
  - OpenAPI v3 documentation
  - postgres connection
  - admin dashboard
  - file upload, although no models have files in their schema to demonstrate that

See the [ARCHITECTURE README](../ARCHITECTURE/README.md) for the pros/cons of using Strapi as our API framework of choice.


# Running this API locally
While doing local development, you'll need some required supporting services
(database, S3 compatible storage, etc). We have a script that will handle
creating them then configuring and launching paratoo-core in dev mode.

 1. make sure you have the dependencies installed
    ```bash
    yarn install
    ```
 1. start the server
    ```bash
    ./pcore
    ```
  1. you can stop the strapi server with Ctrl-c
  1. run the help command to learn how to override config and generally interact
     with the stack
    ```bash
    ./pcore --help # or
    ./pcore help # or
    ./pcore -h
    ```

## How to change config
This script prints all available configuration options as it starts. If you wish
to override a value, simply create an empty `localdev.env` file and add the
override. For example, to change the port the DB listens on:
```bash
echo "DATABASE_PORT=45555" >> localdev.env
```

## When do the changes take effect?
At the very least, you'll need to run `./pcore` after overriding config items.

**Important**: some of the config values are used at creation time of the
supporting docker containers. If you change a value, you'll be safest if you
remove all supporting containers and thier volumes/data, then recreate with the
new values.
```bash
./pcore down --volumes
./pcore
```

# Model initial data
The `config/functions/bootstrap.js` uses
[`strapi-initial-data`](https://gitlab.com/ternandsparrow/strapi-initial-data/)
to keep the LUTs in sync with values for the table that are stored in the
`initialData` key in the `models/<name>.settings.json` files.

# Authentication
This is handled at a few different levels:
  1. using permissions for roles to call controller actions
  1. using a policy enforcement point to ensure users accessing appropriate protocol data
  1. using a policy decision point

## Permissions for roles to call controller actions
This is setting the ability for a role to ever be allowed to call a controller
action (AKA hit a route). This is configured on every server start in the `config/functions/bootstrap.js` file. You will never have to manually configure this in the UI, it's all handled in code.

## Project membership policy
It is important that a user can only access data for protocols that they're
assigned to (via a project). We achieve this with the
`config/policies/projectMembershipEnforcer.js` policy. This policy is configured
in the `routes.json` for each model. It is **essential** that when new protocols
are added, they are protected by this policy.

This policy is simply an *enforcer*, and relies on `paratoo-org` to make policy decisions. See the [Org Interface Spec](../ARCHITECTURE/org-interface.md).

#!/bin/sh
set -euxo pipefail
cd `dirname "$0"`

rm -rf build/
rm -rf node_modules/
rm -rf .cache/

yarn install

# don't know why the strapi-admin build leaves this rubbish behind, but we don't
# need it; remove it if the cache exists (required for Strapi bump)

# latest starpi needs .cache to build admin panel
# if [ -d ".cache" ]; then rm -Rf .cache; fi

# regenerate typescript typings
# see migration guide https://docs.strapi.io/dev-docs/migration/v4/migration-guide-4.11.4-to-4.14.0
yarn strapi ts:generate-types

# this just builds the strapi-admin package. At the time of writing,
# strapi-admin still uses webpack 4 so we can't run in yarn's pnp mode while
# building; that is, we need the node_modules directory present.
yarn build

# now the strapi-admin build is done, we can do a serious cleanup to
# reduce disk usage. Here we swap to yarn pnp mode, remove node_modules and make
# sure the yarn cache is up-to-date (it should be from the first install)
sed 's/nodeLinker.*/nodeLinker: "pnp"/' .yarnrc.yml > $ALT_YARN_RC_FILENAME
rm -fr node_modules/
YARN_RC_FILENAME=$ALT_YARN_RC_FILENAME yarn install --immutable

# don't know why the strapi-admin build leaves this rubbish behind, but we don't
# need it.
#rm -r .cache

# it seems we can delete this without breaking anything, and we save ~30MB in
# image size. Hopefully this doesn't come back to bite us.
rm -fr .yarn/unplugged/@fortawesome-*

# yarn shouldn't be writing to these dirs as we have:
#   enableGlobalCache: false
#   cacheFolder: /app/.yarn/cache
# The cache is also written to the project dir, so we'll just kill those other
# locations
rm -fr /root/.npm /root/.yarn

rm -fr /tmp/*

# the documentation plugin will need to update the generated .json file
chown -R guest:users .
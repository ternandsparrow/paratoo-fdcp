module.exports = {
  async up(knex) {
    try {
      // monitoring summary is from data_identification so use its id
      const data_identification_id = await knex(
        'metadata_collections_components',
      ).where({ field: 'data_identification' })

      // insert monitoring_summary into metadata_collections_components

      for (let index = 0; index < data_identification_id.length; index++) {
        const res = {
          entity_id: data_identification_id[index].entity_id,
          component_id: data_identification_id[index].component_id,
          component_type: 'targeted.monitoring-summary',
          field: 'monitoring_summary',
        }
        await knex('metadata_collections_components').insert(res)
      }
      // join submitter_first_name and submitter_surname to submitter_name in data_providers
      const hasColumn = await knex.schema.hasColumn(
        'data_providers',
        'submitter_name',
      )

      const has_data_licencing_comments_column = await knex.schema.hasColumn(
        'data_providers',
        'data_licencing_comments',
      )

      if (!has_data_licencing_comments_column) {
        await knex.schema.table('data_providers', (table) => {
          table.string('data_licencing_comments')
        })
      }

      const comments = await knex('data_licences').select(
        knex.raw(
          'concat(organisation_name, \' \', key_contact_name, \' \', key_contact_role, \' \', key_contact_email, \' \', phone_number, \' \', data_owners, \' \', data_licencing_comments) as comment',
        ),
      )

      for (let index = 0; index < comments.length; index++) {
        await knex('data_providers')
          .where({ id: index + 1 })
          .update({
            data_licencing_comments: comments[index].comment,
          })
      }

      if (!hasColumn) {
        await knex.schema.table('data_providers', (table) => {
          table.string('submitter_name')
        })
      }
      await knex('data_providers').update({
        submitter_name: knex.raw('?? || \' \' || ??', [
          'submitter_first_name',
          'submitter_surname',
        ]),
      })

      const hasMonitoringSummarysComponentTable = await knex.schema.hasTable(
        'monitoring_summarys_components',
      )
      const hasMonitoringSummarysTable = await knex.schema.hasTable(
        'monitoring_summarys',
      )

      if (!hasMonitoringSummarysTable) {
        await knex.schema.createTable('monitoring_summarys', (table) => {
          table.increments('id')
          table.integer('test')
        })
      }

      await knex('monitoring_summarys').insert(data_identification_id.map(() => ({test:1})))

      if (!hasMonitoringSummarysComponentTable) {
        await knex.schema.createTable(
          'monitoring_summarys_components',
          (table) => {
            table.increments('id')
            table.integer('entity_id')
            table.integer('component_id')
            table.string('component_type')
            table.string('field')
            table.float('order')
          },
        )
      }
      const monitoring_summarys_components = await knex
        .from('data_identifications_components')
        .select(
          'entity_id',
          'component_id',
          'component_type',
          'field',
          'order',
        )

      const monitoring_summarys_id = await knex
        .from('monitoring_summarys')
        .select('id')

      const monitoring_summarys_componentsData =
        monitoring_summarys_components.map((row,index) => ({
          entity_id: monitoring_summarys_id[index].id,
          component_id: row.component_id,
          component_type: row.component_type,
          field: row.field,
          order: row.order,
        }))

      await knex('monitoring_summarys_components').insert(
        monitoring_summarys_componentsData,
      )

      // map data_identifications_broad_monitoring_type_links
      const hasMonitoringSummarysBroadMonitoringTypeLinksTable =
        await knex.schema.hasTable(
          'monitoring_summarys_broad_monitoring_type_links',
        )

      if (!hasMonitoringSummarysBroadMonitoringTypeLinksTable) {
        await knex.schema.createTable(
          'monitoring_summarys_broad_monitoring_type_links',
          (table) => {
            table.increments('id')
            table.integer('monitoring_summary_id')
            table.integer('lut_broad_monitoring_type_id')
          },
        )
      }

      const broad_monitoring_type = await knex
        .from('data_identifications_broad_monitoring_type_links')
        .select('id', 'data_identification_id', 'lut_broad_monitoring_type_id')

      const renamedData = broad_monitoring_type.map((row,index) => ({
        monitoring_summary_id: monitoring_summarys_id[index].id,
        lut_broad_monitoring_type_id: row.lut_broad_monitoring_type_id,
      }))

      await knex('monitoring_summarys_broad_monitoring_type_links').insert(
        renamedData,
      )

      // map data_identifications_monitoring_scale_links
      const hasMonitoringSummaryMonitoringScaleLinksTable =
        await knex.schema.hasTable('monitoring_summarys_monitoring_scale_links')

      if (!hasMonitoringSummaryMonitoringScaleLinksTable) {
        await knex.schema.createTable(
          'monitoring_summarys_monitoring_scale_links',
          (table) => {
            table.increments('id')
            table.integer('monitoring_summary_id')
            table.integer('lut_monitoring_scale_id')
          },
        )
      }

      const monitoring_scale = await knex
        .from('data_identifications_monitoring_scale_links')
        .select('id', 'data_identification_id', 'lut_monitoring_scale_id')

      const monitoring_scale_renamedData = monitoring_scale.map((row,index) => ({
        monitoring_summary_id: monitoring_summarys_id[index].id,
        lut_monitoring_scale_id: row.lut_monitoring_scale_id,
        // a lut value is added so no.5 is now no.6
      }))

      await knex('monitoring_summarys_monitoring_scale_links').insert(
        monitoring_scale_renamedData,
      )
    } catch (error) {
      strapi.log.warn('Data migration failed: ', error)
    }
  },
}

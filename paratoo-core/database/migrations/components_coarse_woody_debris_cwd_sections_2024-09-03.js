module.exports = {
  async up(knex) {
    try {
      await knex.transaction(async (trx) => {
        await trx.schema.table('components_coarse_woody_debris_cwd_sections', table => {
          table.renameColumn('length_m', 'length_cm')
        })
      })
    } catch (error) {
      strapi.log.warn('Data migration failed: ', error)
    }

    // return true
  }
}
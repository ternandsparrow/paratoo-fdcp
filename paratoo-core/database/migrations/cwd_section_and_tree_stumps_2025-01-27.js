module.exports = {
  async up() {
    const uid =
      'api::coarse-woody-debris-observation.coarse-woody-debris-observation'
    let records = []
    try {
      records = await strapi.entityService.findMany(uid, {
        filters: {
          $and: [
            {
              CWD_section: { $notNull: true },
              tree_stump: { $notNull: true },
            },
          ],
        },
        populate: {
          coarse_woody_debris_survey: true,
          Location: true,
          CWD: true,
          tree_stumps: true,
          CWD_section: {
            populate: '*',
          },
          tree_stump: {
            populate: '*',
          },
          comments: true,
          date_time: true,
        },
      })
    } catch {
      strapi.log.info('CWD collection not exist nothing todo')
    }

    if (records.length) {
      strapi.log.info(
        `Found CWD records with CWD_section and tree_stump fields. Doing migration for ${records.length} records`,
      )
    } else return

    try {
      await strapi.db.transaction(async () => {
        for (const record of records) {
          const recordWithCWD = structuredClone(record)
          recordWithCWD.tree_stump = []
          recordWithCWD.tree_stumps = false

          const recordWithTreeStump = structuredClone(record)
          delete recordWithTreeStump.CWD_section
          delete recordWithTreeStump.id
          recordWithTreeStump.CWD = false

          await strapi.entityService.update(uid, record.id, {
            data: recordWithCWD,
          })
          await strapi.entityService.create(uid, { data: recordWithTreeStump })
        }

        strapi.log.info(
          'Migration for CWD records having both CWD_section and tree_stump successful',
        )
      })
    } catch {
      strapi.log.error(
        'Migration for CWD records having both CWD_section and tree_stump failed',
      )
    }
  },
}

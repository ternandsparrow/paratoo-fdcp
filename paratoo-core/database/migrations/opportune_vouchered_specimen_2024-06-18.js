module.exports = {
  async up(knex) {
    try {
      await knex.transaction(async (trx) => {
        // parent component/collection containing the component
        const parentComponentCategoryName = 'opportunistic_observations'

        // component
        const newComponentCategoryName = 'opportune'
        // collection might be rename so sometimes so it may be different from component schema's name
        const collectionName = 'cmpnts_oprtne_obs_vchrd_specimen'
        // old and new names of the component schema file's name
        const oldCustomComponentName =
          'opportunistic-observation-vouchered-specimen'
        const newCustomComponentName =
          'opportunistic-observation-vouchered-specimen'

        if (oldCustomComponentName !== newCustomComponentName) {
          // // rename column name for relation between lut and and related component
          const lutRelationTableName = `${collectionName}_${collectionName}_lut_links`
          const oldColumnName = `${oldCustomComponentName.replaceAll(
            '-',
            '_',
          )}_id`
          const hasColumn = await trx.schema.hasColumn(
            lutRelationTableName,
            oldColumnName,
          )
          //  in old production database the column exists
          //  but in development or new instance , the column won't exist
          if (!hasColumn) {
            strapi.log.info('Column not found, nothing to do')
            return
          }
          await trx.schema.table(lutRelationTableName, (t) => {
            t.renameColumn(
              oldColumnName,
              `${newCustomComponentName.replaceAll('-', '_')}_id`,
            )
          })
        }

        // update related component relation field
        await trx.table(`${parentComponentCategoryName}_components`).update({
          component_type: `${newComponentCategoryName}.${newCustomComponentName.replaceAll(
            '-',
            '_',
          )}`,
        })
      })
    } catch (error) {
      strapi.log.warn('Data migration failed: ', error)
    }
  },
}

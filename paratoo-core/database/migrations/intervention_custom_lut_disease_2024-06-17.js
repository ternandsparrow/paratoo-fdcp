// this more like an example for data migration in case of changing a component schema file's name
// as the moment this protocol is not being chosen in production, but it's being tested in development
// the reason the this renaming is possibly that Pg is unable to handle attribute that's is longer than 63 characters
// so renaming custom-lut-invertebrate-active-sampling-capture-equipment.json to invert-active-equipment.json
// and update the attribute in apparatus.json from 'custom-lut.custom-lut-invertebrate-active-sampling-capture-equipment'
// to 'custom-lut.invert-active-equipment'
// this file will be executed only once per database that's mean it will be called only once in production
// unless change this file name's, that's why date time should be included in the name or at least version number
module.exports = {
  async up(knex) {
    try {
      await knex.transaction(async (trx) => {
        const parentComponentCategoryName = 'interventions'

        const componentCategoryName = 'custom-lut'
        // collection might be rename so sometimes so it may be different from component schema's name
        const collectionName = 'intervention_d_m_t_t'
        // old and new names of the component schema file's name
        const oldCustomComponentName =
          'custom-lut-intervention-disease-management-or-treatment-type'
        const newCustomComponentName =
          'intervention-disease-management-or-treatment-type'

        // // rename column name for relation between lut and and related component
        const lutRelationTableName = `${collectionName}_${collectionName}_lut_links`
        const oldColumnName = `${oldCustomComponentName.replaceAll(
          '-',
          '_',
        )}_id`
        const hasColumn = await trx.schema.hasColumn(
          lutRelationTableName,
          oldColumnName,
        )
        //  in old production database the column exists
        //  but in development or new instance , the column won't exist
        if (!hasColumn) {
          strapi.log.info('Column not found, nothing to do')
          return
        }
        await trx.schema.table(lutRelationTableName, (t) => {
          t.renameColumn(
            oldColumnName,
            `${newCustomComponentName.replaceAll('-', '_')}_id`,
          )
        })
        // update related component relation field
        await trx.table(`${parentComponentCategoryName}_components`).update({
          component_type: `${componentCategoryName}.${newCustomComponentName.replaceAll(
            '-',
            '_',
          )}`,
        })
      })
    } catch (error) {
      strapi.log.warn('Data migration failed: ', error)
    }
  },
}

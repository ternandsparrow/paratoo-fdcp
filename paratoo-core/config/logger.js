'use strict'
const { winston } = require('@strapi/logger')

let logLevel = process.env.STRAPI_LOG_LEVEL || 'info'
// Remove whitespace
logLevel = logLevel.trim().replace(/^\s+|\s+$/gm, '')
console.log(`Using log level: ${logLevel}`)

let logFormat = winston.format.combine(
  winston.format.label({
    label: logLevel,
  }),
  winston.format.timestamp({
    format: 'YY-MM-DD HH:mm:ss',
  }),
  winston.format.printf(
    (log) => ` [${log.timestamp}] ${log.level} : ${log.message}`,
  ),
)

module.exports = {
  level: logLevel,
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(logFormat),
    }),
  ],
}

const { apolloPrometheusPlugin } = require('strapi-prometheus')
const { version } = require('../package.json')
module.exports = ({ env }) => {
  // we want to enforce the required env vars for running the server, but when
  // building the strapi admin dashboard, this code is still executed. So we
  // need a way to switch off the checks during that build.
  const isBuildMode = ['true', '1', 'yes'].includes(env('IS_BUILD_MODE'))
  if (isBuildMode) {
    console.info('Build mode ON, short circuiting plugin init script')
    return {}
  }
  // According to strapi, there are 3 type of upload providers local, aws and cloudinary
  // https://docs.strapi.io/developer-docs/latest/plugins/upload.html#configuration
  // for production we are going to use aws provider
  // and for development or testing local provider

  let configObject = {}
  // update documentation info and contract
  configObject['documentation'] = {
    enabled: true,
    config: {
      openapi: '3.0.0',
      info: {
        version: '1.0.0',
        title: 'Monitor FDCP API',
        description: `Build: ${process.env.NODE_ENV}-Version: ${process.env.VERSION}-GitHash: ${process.env.GIT_HASH}`,
        termsOfService: 'YOUR_TERMS_OF_SERVICE_URL',
        contact: {
          name: 'TERN and Sparrow',
          email: 'tech.contact@environmentalmonitoringgroup.com.au',
          url: 'https://www.environmentalmonitoringgroup.com.au/',
        },
        license: {
          name: 'MIT',
          url: 'https://www.environmentalmonitoringgroup.com.au/',
        },
      },
      servers: [
        {
          url: env('CORE_SWAGGER_SERVER'),
          description: env('CORE_SWAGGER_DESCRIPTION'),
        },
      ],
      externalDocs: {
        description: 'Find out more',
        url: 'https://www.environmentalmonitoringgroup.com.au/',
      },
    },
  }

  // Setup deep population
  configObject['strapi-plugin-populate-deep'] = {
    config: {
      defaultDepth: 10 // Default is 5
    },
  }

  configObject['strapi-prometheus'] = {
    config: {
      // add prefix to all the prometheus metrics names.
      prefix: '',

      // use full url instead of matched url
      // true  => path label: `/api/models/1`
      // false => path label: `/api/models/:id`
      fullURL: false,

      // include url query in the url label
      // true  => path label: `/api/models?limit=1`
      // false => path label: `/api/models`
      includeQuery: false,

      // metrics that will be enabled, by default they are all enabled.
      enabledMetrics: {
        koa: true, // koa metrics
        process: true, // metrics regarding the running process
        http: true, // http metrics like response time and size
        apollo: true, // metrics regarding graphql
      },

      // interval at which rate metrics are collected in ms
      interval: 10_000,

      // set custom/default labels to all the prometheus metrics
      customLabels: {
        name: 'strapi-prometheus',
      },
    },
  }

  configObject.graphql = {
    config: {
      endpoint: '/graphql',
      shadowCRUD: true,
      playgroundAlways: false,
      depthLimit: 7,
      amountLimit: 100,
      apolloServer: {
        plugins: [apolloPrometheusPlugin], // add the plugin to get apollo metrics
        tracing: true, // this must be true to get some of the data needed to create the metrics
      },
    },
  }

  // Setup caching - same for all deployment options
  configObject['rest-cache'] = {
    config: {
      provider: {
        name: 'memory',
        getTimeout: 500, // in milliseconds (default: 500)
        options: {
          max: 32767,
          maxAge: 3600000,
        },
      },
      strategy: {
        debug: false,
        contentTypes: [
          // list of Content-Types UID to cache
          //manual process to create this list (should create script):
          //  - get the keys of paratoo-webapp/src/assets/stateInits/apiModels.js `models` object
          //  - `Array.map(o => `api::${o}.${o}`)`
          'api::lut-aerial-experience.lut-aerial-experience',
          'api::lut-aerial-setup-or-survey.lut-aerial-setup-or-survey',
          'api::lut-aerial-survey-type.lut-aerial-survey-type',
          'api::lut-aircraft-model.lut-aircraft-model',
          'api::lut-aircraft-type.lut-aircraft-type',
          'api::lut-animal-fate.lut-animal-fate',
          'api::lut-bait-station-mount.lut-bait-station-mount',
          'api::lut-basal-area-factor.lut-basal-area-factor',
          'api::lut-basal-dbh-instrument.lut-basal-dbh-instrument',
          'api::lut-basal-sweep-sampling-point.lut-basal-sweep-sampling-point',
          'api::lut-battery-type.lut-battery-type',
          'api::lut-bioregion.lut-bioregion',
          'api::lut-broad-monitoring-type.lut-broad-monitoring-type',
          'api::lut-camera-activation-mechanism.lut-camera-activation-mechanism',
          'api::lut-camera-aspect-ratio.lut-camera-aspect-ratio',
          'api::lut-camera-illumination-type.lut-camera-illumination-type',
          'api::lut-camera-make.lut-camera-make',
          'api::lut-camera-media-type.lut-camera-media-type',
          'api::lut-camera-model.lut-camera-model',
          'api::lut-camera-operational-status.lut-camera-operational-status',
          'api::lut-camera-resolution.lut-camera-resolution',
          'api::lut-camera-sensor-sensitivity.lut-camera-sensor-sensitivity',
          'api::lut-camera-trap-mount.lut-camera-trap-mount',
          'api::lut-camera-trap-survey-type.lut-camera-trap-survey-type',
          'api::lut-cat-coat-colour.lut-cat-coat-colour',
          'api::lut-chemosis-eye.lut-chemosis-eye',
          'api::lut-condition-damage-type.lut-condition-damage-type',
          'api::lut-condition-disturbance-category.lut-condition-disturbance-category',
          'api::lut-condition-disturbance-temporal-scale.lut-condition-disturbance-temporal-scale',
          'api::lut-condition-disturbance-type.lut-condition-disturbance-type',
          'api::lut-condition-grazing-severity.lut-condition-grazing-severity',
          'api::lut-condition-growth-stage-tree.lut-condition-growth-stage-tree',
          'api::lut-condition-height-instrument.lut-condition-height-instrument',
          'api::lut-condition-human-induced-damage.lut-condition-human-induced-damage',
          'api::lut-condition-life-stage.lut-condition-life-stage',
          'api::lut-control-activity-animal-fate.lut-control-activity-animal-fate',
          'api::lut-intervention-pest-control.lut-intervention-pest-control',
          'api::lut-corner.lut-corner',
          'api::lut-cover-transect-start-point.lut-cover-transect-start-point',
          'api::lut-cwd-cut-off.lut-cwd-cut-off',
          'api::lut-cwd-cut-off-length.lut-cwd-cut-off-length',
          'api::lut-cwd-decay-class.lut-cwd-decay-class',
          'api::lut-cwd-method.lut-cwd-method',
          'api::lut-cwd-sampling-survey-method.lut-cwd-sampling-survey-method',
          'api::lut-cwd-transect-number.lut-cwd-transect-number',
          'api::lut-data-captured-flora.lut-data-captured-flora',
          'api::lut-day-night-recording.lut-day-night-recording',
          'api::lut-deer-pest.lut-deer-pest',
          'api::lut-direction.lut-direction',
          'api::lut-disturbance.lut-disturbance',
          'api::lut-drone-capture-mode.lut-drone-capture-mode',
          'api::lut-drone-micasense-crp.lut-drone-micasense-crp',
          'api::lut-drone-return-mode.lut-drone-return-mode',
          'api::lut-drone-sampling-rate.lut-drone-sampling-rate',
          'api::lut-drone-scanning-mode.lut-drone-scanning-mode',
          'api::lut-drone-sensor.lut-drone-sensor',
          'api::lut-drone-white-balance.lut-drone-white-balance',
          'api::lut-epbc-status.lut-epbc-status',
          'api::lut-employed-method.lut-employed-method',
          'api::lut-exact-or-estimate.lut-exact-or-estimate',
          'api::lut-fauna-age-class.lut-fauna-age-class',
          'api::lut-fauna-bird-activity-type.lut-fauna-bird-activity-type',
          'api::lut-fauna-bird-breeding-type.lut-fauna-bird-breeding-type',
          'api::lut-fauna-bird-observation-location-type.lut-fauna-bird-observation-location-type',
          'api::lut-fauna-bird-survey-type.lut-fauna-bird-survey-type',
          'api::lut-fauna-breeding-code.lut-fauna-breeding-code',
          'api::lut-fauna-plot-point.lut-fauna-plot-point',
          'api::lut-fauna-pouch-condition.lut-fauna-pouch-condition',
          'api::lut-fauna-sex.lut-fauna-sex',
          'api::lut-fauna-taxa-type.lut-fauna-taxa-type',
          'api::lut-fauna-teates-size-category.lut-fauna-teates-size-category',
          'api::lut-fauna-teats-condition.lut-fauna-teats-condition',
          'api::lut-fauna-testes-condition.lut-fauna-testes-condition',
          'api::lut-feature.lut-feature',
          'api::lut-female-mammal-young-present.lut-female-mammal-young-present',
          'api::lut-subplot-corner.lut-subplot-corner',
          'api::lut-fire-history.lut-fire-history',
          'api::lut-fire-regeneration-status.lut-fire-regeneration-status',
          'api::lut-floristics-habit.lut-floristics-habit',
          'api::lut-floristics-phenology.lut-floristics-phenology',
          'api::lut-fractional-cover-intercept.lut-fractional-cover-intercept',
          'api::lut-ground-control-survey-time.lut-ground-control-survey-time',
          'api::lut-fauna-observation-type.lut-fauna-observation-type',
          'api::lut-ground-count-transect-type.lut-ground-count-transect-type',
          'api::lut-habitat.lut-habitat',
          'api::lut-hapd-number-of-transect.lut-hapd-number-of-transect',
          'api::lut-hapd-plot-size.lut-hapd-plot-size',
          'api::lut-herbivory-or-damage.lut-herbivory-or-damage',
          'api::lut-intervention-access-control-type.lut-intervention-access-control-type',
          'api::lut-intervention-breeding-technique.lut-intervention-breeding-technique',
          'api::lut-intervention-debris-removal-type.lut-intervention-debris-removal-type',
          'api::lut-intervention-disease-management-or-treatment-type.lut-intervention-disease-management-or-treatment-type',
          'api::lut-intervention-erosion-treatment-type.lut-intervention-erosion-treatment-type',
          'api::lut-intervention-fire-management-type.lut-intervention-fire-management-type',
          'api::lut-intervention-habitat-augmentation-type.lut-intervention-habitat-augmentation-type',
          'api::lut-intervention-individuals-or-group.lut-intervention-individuals-or-group',
          'api::lut-intervention-initial-or-followup.lut-intervention-initial-or-followup',
          'api::lut-intervention-land-management-industry-type.lut-intervention-land-management-industry-type',
          'api::lut-intervention-pest-control.lut-intervention-pest-control',
          'api::lut-intervention-planting-method.lut-intervention-planting-method',
          'api::lut-intervention-remediation-type.lut-intervention-remediation-type',
          'api::lut-intervention-site-preparation-action-type.lut-intervention-site-preparation-action-type',
          'api::lut-intervention-type.lut-intervention-type',
          'api::lut-intervention-water-treatment-management-type.lut-intervention-water-treatment-management-type',
          'api::lut-interventions-established-or-maintained.lut-interventions-established-or-maintained',
          'api::lut-invertebrate-active-sampling-capture-equipment.lut-invertebrate-active-sampling-capture-equipment',
          'api::lut-invertebrate-liquid-type.lut-invertebrate-liquid-type',
          'api::lut-invertebrate-malaise-trapping-voucher-type.lut-invertebrate-malaise-trapping-voucher-type',
          'api::lut-invertebrate-pan-placement.lut-invertebrate-pan-placement',
          'api::lut-invertebrate-pan-trapping-colour.lut-invertebrate-pan-trapping-colour',
          'api::lut-invertebrate-pan-trapping-duration.lut-invertebrate-pan-trapping-duration',
          'api::lut-invertebrate-post-field-guideline-group-tier-1.lut-invertebrate-post-field-guideline-group-tier-1',
          'api::lut-invertebrate-post-field-guideline-group-tier-2.lut-invertebrate-post-field-guideline-group-tier-2',
          'api::lut-key-target-taxa.lut-key-target-taxa',
          'api::lut-land-use-history.lut-land-use-history',
          'api::lut-landform-element.lut-landform-element',
          'api::lut-landform-pattern.lut-landform-pattern',
          'api::lut-landscape-category.lut-landscape-category',
          'api::lut-location-type.lut-location-type',
          'api::lut-lure-type.lut-lure-type',
          'api::lut-lure-variety.lut-lure-variety',
          'api::lut-male-frog-breeing-status.lut-male-frog-breeing-status',
          'api::lut-microhabitat.lut-microhabitat',
          'api::lut-monitoring-scale.lut-monitoring-scale',
          'api::lut-moon-phase.lut-moon-phase',
          'api::lut-observer-confidence.lut-observer-confidence',
          'api::lut-observer-position.lut-observer-position',
          'api::lut-observer-role.lut-observer-role',
          'api::lut-opportune-voucher-type-tier-1.lut-opportune-voucher-type-tier-1',
          'api::lut-opportune-voucher-type-tier-2.lut-opportune-voucher-type-tier-2',
          'api::lut-pest-wild-dog.lut-pest-wild-dog',
          'api::lut-photopoints-protocol-variant.lut-photopoints-protocol-variant',
          'api::lut-plot-area.lut-plot-area',
          'api::lut-plot-dimensions.lut-plot-dimensions',
          'api::lut-plot-corner-and-centre.lut-plot-corner-and-centre',
          'api::lut-plot-layout-marker.lut-plot-layout-marker',
          'api::lut-plot-nearest-infrastructure.lut-plot-nearest-infrastructure',
          'api::lut-plot-points.lut-plot-points',
          'api::lut-plot-relief.lut-plot-relief',
          'api::lut-fauna-survey-intent.lut-fauna-survey-intent',
          'api::lut-plot-type.lut-plot-type',
          'api::lut-preservation-type.lut-preservation-type',
          'api::lut-program.lut-program',
          'api::lut-proliferation-eye.lut-proliferation-eye',
          'api::lut-protocol-type.lut-protocol-type',
          'api::lut-protocol-variant.lut-protocol-variant',
          'api::lut-published-status.lut-published-status',
          'api::lut-recruitment-study-area-type.lut-recruitment-study-area-type',
          'api::lut-recruitment-transect.lut-recruitment-transect',
          'api::lut-recruitment-tree-status.lut-recruitment-tree-status',
          'api::lut-rump.lut-rump',
          'api::lut-scat.lut-scat',
          'api::lut-select-protocol-variant.lut-select-protocol-variant',
          'api::lut-sightability-distance.lut-sightability-distance',
          'api::lut-sign-age.lut-sign-age',
          'api::lut-sign-based-data-type.lut-sign-based-data-type',
          'api::lut-sign-based-survey-intent.lut-sign-based-survey-intent',
          'api::lut-sign-type.lut-sign-type',
          'api::lut-skin-condition.lut-skin-condition',
          'api::lut-soil-asc-family-detail.lut-soil-asc-family-detail',
          'api::lut-soils-30cm-sampling-interval.lut-soils-30cm-sampling-interval',
          'api::lut-soil-location-within-the-landform-element.lut-soil-location-within-the-landform-element',
          'api::lut-soil-root-abundance.lut-soil-root-abundance',
          'api::lut-soil-sub-pit-collection-method.lut-soil-sub-pit-collection-method',
          'api::lut-soil-sub-pit-variant.lut-soil-sub-pit-variant',
          'api::lut-soil-survey-variant.lut-soil-survey-variant',
          'api::lut-soils-asc-class.lut-soils-asc-class',
          'api::lut-soils-asc-order.lut-soils-asc-order',
          'api::lut-soils-biotic-relief-agent.lut-soils-biotic-relief-agent',
          'api::lut-soils-coarse-frag-abundance.lut-soils-coarse-frag-abundance',
          'api::lut-soils-coarse-frag-alteration.lut-soils-coarse-frag-alteration',
          'api::lut-soils-coarse-frag-distribution.lut-soils-coarse-frag-distribution',
          'api::lut-soils-coarse-frag-shape.lut-soils-coarse-frag-shape',
          'api::lut-soils-coarse-frag-size.lut-soils-coarse-frag-size',
          'api::lut-soils-coarse-frag-strength.lut-soils-coarse-frag-strength',
          'api::lut-soils-colour-chroma.lut-soils-colour-chroma',
          'api::lut-soils-colour-hue.lut-soils-colour-hue',
          'api::lut-soils-colour-value.lut-soils-colour-value',
          'api::lut-soils-confidence-level.lut-soils-confidence-level',
          'api::lut-soils-cutan-abundance.lut-soils-cutan-abundance',
          'api::lut-soils-cutan-distinctness.lut-soils-cutan-distinctness',
          'api::lut-soils-cutan-type.lut-soils-cutan-type',
          'api::lut-soils-digging-stopped-by.lut-soils-digging-stopped-by',
          'api::lut-soils-drainage.lut-soils-drainage',
          'api::lut-soils-effervescence.lut-soils-effervescence',
          'api::lut-soils-erosion-state.lut-soils-erosion-state',
          'api::lut-soils-erosion-type.lut-soils-erosion-type',
          'api::lut-soils-evaluation-means.lut-soils-evaluation-means',
          'api::lut-soils-field-dispersion-score.lut-soils-field-dispersion-score',
          'api::lut-soils-field-slaking-score.lut-soils-field-slaking-score',
          'api::lut-soils-fine-macropore-abundance.lut-soils-fine-macropore-abundance',
          'api::lut-soils-gilgai-proportion.lut-soils-gilgai-proportion',
          'api::lut-soils-gully-water-erosion-degree.lut-soils-gully-water-erosion-degree',
          'api::lut-soils-horizon-boundary-distinctness.lut-soils-horizon-boundary-distinctness',
          'api::lut-soils-horizon-boundary-shape.lut-soils-horizon-boundary-shape',
          'api::lut-soils-lithology.lut-soils-lithology',
          'api::lut-soils-macropore-diameter.lut-soils-macropore-diameter',
          'api::lut-soils-mass-movement-erosion-degree.lut-soils-mass-movement-erosion-degree',
          'api::lut-soils-medium-macropore-abundance.lut-soils-medium-macropore-abundance',
          'api::lut-soils-microrelief-component.lut-soils-microrelief-component',
          'api::lut-soils-microrelief-type.lut-soils-microrelief-type',
          'api::lut-soils-moisture-status.lut-soils-moisture-status',
          'api::lut-soils-morphological-type.lut-soils-morphological-type',
          'api::lut-soils-mottle-abundance.lut-soils-mottle-abundance',
          'api::lut-soils-mottle-boundary-distinctness.lut-soils-mottle-boundary-distinctness',
          'api::lut-soils-mottle-colour.lut-soils-mottle-colour',
          'api::lut-soils-mottle-contrast.lut-soils-mottle-contrast',
          'api::lut-soils-mottle-size.lut-soils-mottle-size',
          'api::lut-soils-mottle-type.lut-soils-mottle-type',
          'api::lut-soils-observation-type.lut-soils-observation-type',
          'api::lut-soils-pan-cementation.lut-soils-pan-cementation',
          'api::lut-soils-pan-continuity.lut-soils-pan-continuity',
          'api::lut-soils-pan-structure.lut-soils-pan-structure',
          'api::lut-soils-pan-type.lut-soils-pan-type',
          'api::lut-soils-permeability.lut-soils-permeability',
          'api::lut-soils-plasticity-type.lut-soils-plasticity-type',
          'api::lut-soils-relative-inclination-of-slope-elements.lut-soils-relative-inclination-of-slope-elements',
          'api::lut-soils-rill-water-erosion-degree.lut-soils-rill-water-erosion-degree',
          'api::lut-soils-rock-outcrop-abundance.lut-soils-rock-outcrop-abundance',
          'api::lut-soils-root-size.lut-soils-root-size',
          'api::lut-soils-scald-erosion-degree.lut-soils-scald-erosion-degree',
          'api::lut-soils-segregation-magnetic-attributes.lut-soils-segregation-magnetic-attributes',
          'api::lut-soils-segregation-strength.lut-soils-segregation-strength',
          'api::lut-soils-segregations-abundance.lut-soils-segregations-abundance',
          'api::lut-soils-segregations-form.lut-soils-segregations-form',
          'api::lut-soils-segregations-nature.lut-soils-segregations-nature',
          'api::lut-soils-segregations-size.lut-soils-segregations-size',
          'api::lut-soils-sheet-water-erosion-degree.lut-soils-sheet-water-erosion-degree',
          'api::lut-slope-class.lut-slope-class',
          'api::lut-soils-soil-strength.lut-soils-soil-strength',
          'api::lut-soils-stickiness.lut-soils-stickiness',
          'api::lut-soils-stream-bank-water-erosion-degree.lut-soils-stream-bank-water-erosion-degree',
          'api::lut-soils-structure-compound-pedality.lut-soils-structure-compound-pedality',
          'api::lut-soils-structure-grade.lut-soils-structure-grade',
          'api::lut-soils-structure-size.lut-soils-structure-size',
          'api::lut-soils-structure-type.lut-soils-structure-type',
          'api::lut-soils-surface-soil-condition.lut-soils-surface-soil-condition',
          'api::lut-soils-texture-grade.lut-soils-texture-grade',
          'api::lut-soils-texture-modifier.lut-soils-texture-modifier',
          'api::lut-soils-texture-qualification.lut-soils-texture-qualification',
          'api::lut-soils-tunnel-water-erosion-degree.lut-soils-tunnel-water-erosion-degree',
          'api::lut-soils-void-cracks.lut-soils-void-cracks',
          'api::lut-soils-water-status.lut-soils-water-status',
          'api::lut-soils-wave-water-erosion-degree.lut-soils-wave-water-erosion-degree',
          'api::lut-soils-wind-erosion-degree.lut-soils-wind-erosion-degree',
          'api::lut-species-determiner.lut-species-determiner',
          'api::lut-species-pest.lut-species-pest',
          'api::lut-state.lut-state',
          'api::lut-status-of-helicopter-door.lut-status-of-helicopter-door',
          'api::lut-substrate.lut-substrate',
          'api::lut-survey-frequency.lut-survey-frequency',
          'api::lut-surveyor-type.lut-surveyor-type',
          'api::lut-taxa-type.lut-taxa-type',
          'api::lut-tec.lut-tec',
          'api::lut-temp-texta-colour.lut-temp-texta-colour',
          'api::lut-temperature-unit.lut-temperature-unit',
          'api::lut-tier-1-observation-method.lut-tier-1-observation-method',
          'api::lut-tier-2-observation-method.lut-tier-2-observation-method',
          'api::lut-tier-3-observation-methods.lut-tier-3-observation-methods',
          'api::lut-time-format.lut-time-format',
          'api::lut-track-station-micro-habitat.lut-track-station-micro-habitat',
          'api::lut-track-station-substrate.lut-track-station-substrate',
          'api::lut-tracking-substrate.lut-tracking-substrate',
          'api::lut-transect-route-type.lut-transect-route-type',
          'api::lut-transect-sampling-type.lut-transect-sampling-type',
          'api::lut-trap-check-interval.lut-trap-check-interval',
          'api::lut-type-of-damage.lut-type-of-damage',
          'api::lut-type-of-physical-damage.lut-type-of-physical-damage',
          'api::lut-vantage-point-equipment.lut-vantage-point-equipment',
          'api::lut-vantage-point-type.lut-vantage-point-type',
          'api::lut-veg-association.lut-veg-association',
          'api::lut-veg-growth-form.lut-veg-growth-form',
          'api::lut-veg-growth-stage.lut-veg-growth-stage',
          'api::lut-veg-structural-formation.lut-veg-structural-formation',
          'api::lut-vegetation-type.lut-vegetation-type',
          'api::lut-vertebrate-age-class.lut-vertebrate-age-class',
          'api::lut-vertebrate-body-condition.lut-vertebrate-body-condition',
          'api::lut-vertebrate-capture-status.lut-vertebrate-capture-status',
          'api::lut-vertebrate-class.lut-vertebrate-class',
          'api::lut-vertebrate-drift-fence-name.lut-vertebrate-drift-fence-name',
          'api::lut-vertebrate-drift-fence-type.lut-vertebrate-drift-fence-type',
          'api::lut-vertebrate-permanent-id-type.lut-vertebrate-permanent-id-type',
          'api::lut-vertebrate-search-method.lut-vertebrate-search-method',
          'api::lut-vertebrate-search-type.lut-vertebrate-search-type',
          'api::lut-vertebrate-sex.lut-vertebrate-sex',
          'api::lut-vertebrate-testes-position.lut-vertebrate-testes-position',
          'api::lut-vertebrate-trap-check-status.lut-vertebrate-trap-check-status',
          'api::lut-vertebrate-trap-specific.lut-vertebrate-trap-specific',
          'api::lut-vertebrate-trap-status.lut-vertebrate-trap-status',
          'api::lut-vertebrate-trap-type.lut-vertebrate-trap-type',
          'api::lut-voucher-condition.lut-voucher-condition',
          'api::lut-weather-cloud-cover.lut-weather-cloud-cover',
          'api::lut-weather-precipitation.lut-weather-precipitation',
          'api::lut-weather-precipitation-duration.lut-weather-precipitation-duration',
          'api::lut-weather-wind.lut-weather-wind',
        ],
      },
    },
  }
  let environment = env('NODE_ENV')
  if (env('DNS_PARATOO_CORE')) {
    // drop the http or https if any is present
    environment = env('DNS_PARATOO_CORE').replace(/^(https?:)?\/\//, '')
    // overkill safeguard but just in case, environment's length can't be longer than 64
    if (environment.length > 63) {
      environment = environment.substring(0, 62)
    }
  }
  configObject.sentry = {
    enabled: false,
    config: {
      dsn: env('SENTRY_DSN'),
      init: {
        environment,
        release: version,
      },
      sendMetadata: true,
    },
  }
  // upload provider for production only
  if (env('NODE_ENV') == 'production') {
    configObject.upload = getUploadProviderAWS(env)
    configObject.sentry.enabled = true
    // turn off debug in production
    configObject['rest-cache'].config.debug = false
  } else {
    // upload provider for localdev or test environment
    configObject.upload = getUploadProviderDevOrTest()
  }
  console.info(
    `Using ${configObject.upload.config.paratooPrettyName} mode for upload provider`,
  )
  console.info('REST API Caching has been enabled')

  return configObject

  // generate production configurations
  function getUploadProviderAWS(env) {
    const isLocalS3 = env('IS_LOCAL_S3') === 'true'

    if (isLocalS3) {
      return {
        // minio for starpi version 4.x.x
        // https://www.npmjs.com/package/strapi-provider-upload-minio-ce
        config: {
          provider: 'strapi-provider-upload-minio-ce',
          paratooPrettyName: 'local Minio',
          providerOptions: {
            accessKey: reqEnv('MINIO_ACCESS_KEY'),
            secretKey: reqEnv('MINIO_SECRET_KEY'),
            bucket: reqEnv('MINIO_BUCKET'),
            endPoint: reqEnv('MINIO_ENDPOINT'),
            port: parseInt(env('MINIO_PORT'), 10) || 49000,
            useSSL: false,
            folder: 'cms',
            isDocker: true,
            overridePath: env('MINIO_OVERRIDE_PATH'),
            host: `${env('MINIO_ENDPOINT', 'localhost')}:${
              parseInt(env('MINIO_PORT'), 10) || 49000
            }`,
          },
        },
      }
    }

    return {
      // https://www.npmjs.com/package/@strapi/provider-upload-aws-s3
      config: {
        provider: 'aws-s3',
        paratooPrettyName: 'AWS S3 Bucket',
        providerOptions: {
          s3Options: {
            accessKeyId: env('AWS_ACCESS_KEY_ID'),
            secretAccessKey: env('AWS_ACCESS_SECRET'),
            region: env('S3_REGION'),
            params: {
              Bucket: env('S3_BUCKET'),
            },
          },
        },
        actionOptions: {
          upload: {},
          uploadStream: {},
          delete: {},
        },
      },
    }
  }

  // generate development configuration
  function getUploadProviderDevOrTest() {
    // https://www.npmjs.com/package/@strapi/provider-upload-local
    return {
      config: {
        paratooPrettyName: 'local server',
        provider: 'local',
        providerOptions: {},
      },
    }
  }

  function reqEnv(name) {
    const result = env(name)
    if (result) {
      return result
    }
    throw new Error(`No value found for required env var ${name}`)
  }
}

module.exports = ({ env }) => {
  // FIXME: work around for unit test, somehow strapi still use this config for sqlite during test mode
  if(env('NODE_ENV') === 'test') return
  return {
    connection: {
      client: 'postgres',
      connection: {
        host: env('DATABASE_HOST', '127.0.0.1'),
        port: env.int('DATABASE_PORT', 45433),
        database: env('DATABASE_NAME', 'app'),
        user: env('DATABASE_USERNAME', 'user'),
        password: env('DATABASE_PASSWORD', 'password'),
        ssl: env.bool('DATABASE_SSL', false) && {
          rejectUnauthorized: env.bool(
            'DATABASE_SSL_REJECT_UNAUTHORIZED',
            false,
          ),
        },
      },
      pool: {
        //FIXME: refactor as env variable, as different timeouts will make sense on different hardware
        // https://github.com/strapi/strapi/issues/8117 Connection terminated unexpectedly
        acquireTimeoutMillis: env.int('DATABASE_ACQUIRE_TIMEOUT', 600000),
        // source: https://stackoverflow.com/questions/73519391/strapi-responds-with-401-after-some-time-of-inactivity-works-after-reload
        min: env.int('DATABASE_POOL_MIN', 2),
        max: env.int('DATABASE_POOL_MAX', 10),
        createTimeoutMillis: env.int('DATABASE_CREATE_TIMEOUT', 600000),
        idleTimeoutMillis: env.int('DATABASE_IDLE_TIMEOUT', 600000),
        reapIntervalMillis: env.int('DATABASE_REAP_INTERVAL', 1000),
        createRetryIntervalMillis: env.int(
          'DATABASE_CREATE_RETRY_INTERVAL',
          100,
        ),
      },
      acquireConnectionTimeout: env.int(
        'DATABASE_ACQUIRE_CONNECTION_TIMEOUT',
        600000,
      ),
    },
  }
}

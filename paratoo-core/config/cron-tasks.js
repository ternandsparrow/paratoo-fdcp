const axios = require('axios')

const paresEnvInt = (envInt) => {
  if (!envInt) return false

  let value = envInt.toString()
  // Remove whitespace
  value = value.trim().replace(/^\s+|\s+$/gm, '')
  console.debug(`Cron job paresEnvInt result : ${value}`)
  try {
    return parseInt(value)
  } catch (error) {
    console.warn(`failed to parse env: ${envInt}, defaulting to 60`)
    return 60
  }
}

const abisDataExport = async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const currentTime = new Date(Date.now()).toISOString()
  helpers.paratooDebugMsg(
    `Cron job(data export) started at ${currentTime} ${process.env.NODE_ENV}`,
    true,
  )

  const dt = new Date()
  let abis_s3_schedule = paresEnvInt(process.env.ABIS_S3_SCHEDULE)

  // for dev testing
  if (process.env.NODE_ENV == 'development') abis_s3_schedule = 60

  dt.setMinutes(dt.getMinutes() - abis_s3_schedule)
  const startDate = dt.toISOString()
  const endDate = currentTime

  const orgUuids  = await helpers.getOrgUuids(startDate, endDate)
  if(orgUuids.length==0) {
    const msg = 'No new collections found to export'
    helpers.paratooDebugMsg(msg)
    return {
      status: 200,
      data: msg,
    }
  }
  
  helpers.paratooDebugMsg(`org_uuids: ${orgUuids}`)
  return await helpers.abisExportCollections(orgUuids)
}

module.exports = {
  cronJob: {
    task: async ({ strapi }) => {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      const isExporterOn = helpers.validBoolean(process.env.CRON_EXPORTER)
      // no further processing required.
      if (!isExporterOn) return

      // stop processing if exporter is not available
      const isRunning = await helpers.isExporterRunning()
      if (isRunning.status != 200) {
        helpers.paratooWarnHandler(`Exporter instance is down, core: ${process.env.DNS_PARATOO_CORE}`)
        return 
      }

      helpers.paratooDebugMsg('Exporter is running')
      const result = {
        start_date: new Date(Date.now()).toISOString(),
        is_active_Job_abis_data_export: isExporterOn,
      }

      if (isExporterOn) {
        result['abis_data_export_result'] = await abisDataExport()
        const status = result['abis_data_export_result']?.status
        helpers.paratooDebugMsg(
          `Cron job(abisDataExport) status: ${status}`,
          true,
        )
        // saves final output
        await strapi.entityService.create(
          'api::cron-jobs-status.cron-jobs-status',
          {
            data: result,
          },
        )
        // TODO: update abis exporter uuid
        // TODO: update exporter flag to org_uuid table
      }
    },
    options: {
      rule: `*/${paresEnvInt(process.env.ABIS_S3_SCHEDULE)} * * * *`,
    },
  },
}

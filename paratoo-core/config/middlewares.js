module.exports = ({ env }) => [
  'strapi::errors',
  {
    name: 'strapi::security',
    config: {
      contentSecurityPolicy: {
        useDefaults: true,
        directives: {
          'connect-src': ['"self"', 'https:'],
          'img-src': [
            '"self"',
            'data:',
            'blob:',
            'dl.airtable.com',
            `${env('MINIO_ENDPOINT', 'localhost')}:${
              parseInt(env('MINIO_PORT'), 10) || 49000
            }`,
            `https://${env('S3_BUCKET')}.s3.${env('S3_REGION')}.amazonaws.com`,
          ],
          'media-src': [
            '"self"',
            'data:',
            'blob:',
            'dl.airtable.com',
            `${env('MINIO_ENDPOINT', 'localhost')}:${
              parseInt(env('MINIO_PORT'), 10) || 49000
            }`,
            `https://${env('S3_BUCKET')}.s3.${env('S3_REGION')}.amazonaws.com`,
          ],
          upgradeInsecureRequests: null,
        },
      },
    },
  },
  'strapi::cors',
  {
    name: 'strapi::poweredBy',
    config: {
      poweredBy: 'TERN and Sparrow',
    },
  },
  'strapi::logger',
  'strapi::query',
  {
    name: 'strapi::body',
    // prettier-ignore
    config: {
      textLimit: env('CORE_MAX_PAYLOAD_SIZE_MB')*1048576,
      formLimit: env('CORE_MAX_PAYLOAD_SIZE_MB')*1048576,
      jsonLimit: env('CORE_MAX_PAYLOAD_SIZE_MB')*1048576,
      // strict: false,
      formidable: {
        maxFileSize: env('CORE_MAX_PAYLOAD_SIZE_MB')*1048576, // files can be up to 20mb
      },
    },
  },
  'strapi::session',
  'strapi::favicon',
  'strapi::public',
  // README: adding custom middlewares in here like global::pdp-data-filter will work but it will apply to everything including strapi admin dashboard's endpoints
]

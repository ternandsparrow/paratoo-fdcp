// path: ./config/api.js

module.exports = () => ({
  rest: {
    // Strapi V3 allowed requests to grab everything by default
    // so emulate that here with a large number
    defaultLimit: 1000,
  },
})

module.exports = () => ({
  'users-permissions': {
    config: {
      jwt: {
        expiresIn: '7d',
      },
      jwtSecret: '88b1d8b8-c686-4824-9f2d-f70a8880d21e',
      sentry: {
        enabled: true
      },
    },
  },
})

module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '88b1d8b8-c686-4824-9f2d-f70a8880d21e'),
  },
  apiToken: { salt: env('API_TOKEN_SALT', 'default_salt') },
  transfer: {
    token: {
      salt: env('API_TOKEN_SALT', 'default_salt'),
    },
  },
})

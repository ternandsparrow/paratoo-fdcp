// Health check script for docker Strapi being responsive
//
// https://github.com/strapi/strapi-docker/blob/3101684710ad7424d4aaf530e7d8878c9c8f7da8/healthcheck.js
//
const http = require('http')

const options = {
  // localhost sometimes is resolved to IPv6 (::1) instead of IPv4 (127.0.0.1),
  // so we need to use 127.0.0.1
  // https://github.com/node-fetch/node-fetch/issues/1624#issuecomment-1235826631
  host: '127.0.0.1',
  port: '1338',
  path: '/_health',
  method: 'HEAD',
  timeout: 2000,
}

const request = http.get(options, (res) => {
  console.log(`STATUS: ${res.statusCode}`)
  if (res.statusCode == 204) {
    process.exit(0)
  } else {
    process.exit(1)
  }
})

request.on('error', function (err) {
  console.error(`ERROR: ${err.message}`)
  process.exit(1)
})

request.end()

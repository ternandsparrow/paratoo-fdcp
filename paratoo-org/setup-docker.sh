#!/bin/sh
set -euxo pipefail
cd `dirname "$0"`

rm -rf build/
rm -rf node_modules/
rm -rf .cache/

# we need git to install at least one npm dependency
apk add --no-cache --virtual build-deps \
  git

yarn install


# regenerate typescript typings
# see migration guide https://docs.strapi.io/dev-docs/migration/v4/migration-guide-4.11.4-to-4.14.0
yarn strapi ts:generate-types

# this just builds the strapi-admin package. At the time of writing,
# strapi-admin still uses webpack 4 so we can't run in yarn's pnp mode while
# building; that is, we need the node_modules directory present.
yarn build

# don't know why the strapi-admin build leaves this rubbish behind, but we don't
# need it.
# latest starpi needs .cache to build admin panel
# rm -r .cache

# now the strapi-admin build is done, we can do a serious cleanup to
# reduce disk usage. Here we swap to yarn pnp mode, remove node_modules and make
# sure the yarn cache is up-to-date (it should be from the first install)
sed 's/nodeLinker.*/nodeLinker: "pnp"/' .yarnrc.yml > $ALT_YARN_RC_FILENAME
rm -fr node_modules/
export YARN_RC_FILENAME=$ALT_YARN_RC_FILENAME # does NOT survive this RUN expr
yarn install --immutable

# we need to install koa because nothing explicitly depends on it so it gets
# left out. The reason paratoo-core doesn't have this problem is because this
# project uses strapi-plugin-users-permissions, which depends on `grant`. If we
# don't do this, we get this error:
#
#  Error: grant tried to access koa, but it isn't declared in its dependencies;
#  this makes the require call ambiguous and unsound.
#
#  Required package: koa (via "koa/package.json")
#  Required by: grant@npm:5.4.9 (via /app/.yarn/cache/grant-npm-5.4.9-5341e33cc2-645b396084.zip/node_modules/grant/)
#  ...
yarn add koa

# don't need git any more
apk del build-deps

# it seems we can delete this without breaking anything, and we save ~30MB in
# image size. Hopefully this doesn't come back to bite us.
rm -fr .yarn/unplugged/@fortawesome-*

# yarn shouldn't be writing to these dirs as we have:
#   enableGlobalCache: false
#   cacheFolder: /app/.yarn/cache
# The cache is also written to the project dir, so we'll just kill those other
# locations
rm -fr /root/.npm /root/.yarn

rm -fr /tmp/*

# the documentation plugin will need to update the generated .json file
chown guest:users .

const { setupStrapi, teardownStrapi } = require('./helpers/strapi')

/** this code is called once before any test is called */
beforeAll(setupStrapi)

/** this code is called once before all the tested are finished */
// afterAll(teardownStrapi)

it('strapi instance is defined', () => {
  expect(global.strapiInstance).toBeDefined()
})

require('./test_suite/mint-identifier/mint-identifier.js')
require('./test_suite/user-projects/user-projects.js')
require('./test_suite/pdp-read/pdp-read.js')
require('./test_suite/pdp-write/pdp-write.js')
require('./test_suite/status/status.js')
require('./test_suite/collection/collection.js')
require('./test_suite/challenge/challenge.js')
require('./test_suite/jwt_and_token_refresh/jwt_and_token_refresh.js')
require('./test_suite/validator/ajv_validator.js')
require('./test_suite/validator/custom_rule_validator.js')

// TODO `device` needs to be ported for Strapi 4
// require('./test_suite/devices/devices.js')
# Org Unit Test Cases
>TODO Kira: write more guidelines on specificity of test cases

If you write a test case, make yourself as an author.<br>
If you edit/amend a test case, append your name to the list of authors
<br><br>

**Ensure correct usage of 'authorize' and 'authenticate'.<br>**
*Authorize* means to give access or permission to something. It is usually associated with access control or client privilege.
*Authenticate* means to validate the identity of a user. It is usually associated with login functionalities.

## 1. Device
>TODO: tests for 19-point validation (not yet implemented)
### 1.1 POST ​/devices
|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-POST-001 |
| Author            | Luke |
| Title             | Successfully notify server of the registration of a WebAuthn credential |
| Description       | Test the ability of Device to accept and store the WebAuthn credential. WebAuthn credentials are stored as a User's registered device. <br>Note: this test is just testing the endpoint, not the actual creation of the WebAuthn credential itself |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-13 <br> -submit a POST request to: http://localhost:1338/devices |
| Success Criteria  | -JSON: `{"success":true}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-POST-002 |
| Author            | Luke |
| Title             | Failure to store a device when said device already exists |
| Description       | Test the ability of Device to reject requests to store a device when said device already exists |
| Initial Condition | -User is authenticated (logged in) <br>-the provided UUID already exists in the database |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-18 <br>-submit a POST request to http://localhost:1338/devices |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Duplicate entry. This device is already registered for this user"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-POST-003 |
| Author            | Luke |
| Title             | Failure to store a device when the User is not authenticated |
| Description       | Test the ability of Device to reject requests to store a device when the User is not authenticated |
| Initial Condition | -User is not authenticated |
| Testing Steps     | -submit a POST request to http://localhost:1338/devices |
| Success Criteria  | -JSON: `{"statusCode":403,"error":"Forbidden","message":"Forbidden"}` |
| Data              | -n/a |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-POST-004 |
| Author            | Luke |
| Title             | Failure to store a device when the provided UUID is not valid |
| Description       | Test the ability of Device to reject requests to store a device when the provided UUID is empty (i.e., not valid) |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-19 <br>-submit a POST request to http://localhost:1338/devices |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/params/longLivedCookie must match format \"uuid\""}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-POST-005 |
| Author            | Luke |
| Title             | Failure to store a device when the provided challenge is not valid |
| Description       | Test the ability of Device to reject requests to store a device when the provided challenge is empty (i.e., not valid) |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-20 <br>-submit a POST request to http://localhost:1338/devices |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/params/challenge must match pattern \"[A-Za-z0-9]{16}\""}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-POST-006 |
| Author            | Luke |
| Title             | Failure to store a device when the provided public key is not valid |
| Description       | Test the ability of Device to reject requests to store a device when the provided public key is not valid (i.e., **A**: does not contain the field `type`, **B**: does not contain the field `data`) <br>-note: the validator does not enforce the string provided to `type`, nor the contents of `data` |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-21 <br>-submit a POST request to http://localhost:1338/devices |
| Success Criteria  | -A: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/params/publicKey must have required property 'type'"}` <br>-B: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/params/publicKey must have required property 'data'"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-POST-007 |
| Author            | Luke |
| Title             | Failure to store a device when the provided credential ID is not valid |
| Description       | Test the ability of Device to reject requests to store a device when the provided credential ID is not valid (i.e., **A**: does not contain the field `type`, **B**: does not contain the field `data`) <br>-note: the validator does not enforce the string provided to `type`, nor the contents of `data` |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-22 <br>-submit a POST request to http://localhost:1338/devices |
| Success Criteria  | -A: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/params/credentialId must have required property 'type'"}` <br>-B: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/params/credentialId must have required property 'data'"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-POST-008 |
| Author            | Luke |
| Title             | Failure to store a device when the provided client data JSON is not valid |
| Description       | Test the ability of Device to reject requests to store a device when the provided client data JSON is not valid (i.e., **A**: does not contain the field `type`, **B**: does not contain the field `challenge`, **C**: does not contain the field `origin`) <br>-note: the field `crossOrigin` is not required |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-23 <br>-submit a POST request to http://localhost:1338/devices |
| Success Criteria  | -A: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/params/clientDataJSON must have required property 'type'"}` <br>-B: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/params/clientDataJSON must have required property 'challenge'"}` <br>-C: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/params/clientDataJSON must have required property 'origin'"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-POST-009 |
| Author            | Luke |
| Title             | Failure to store a device when the provided User ID is not valid |
| Description       | Test the ability of Device to reject requests to store a device when the provided User ID is not an integer (A: string/char, B: decimal, C: big number - 308 digits long) |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-24 <br>-submit a POST request to http://localhost:1338/devices |
| Success Criteria  | -A/B/C: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/params/user must be integer"}` |
| Data              | -a User account for authentication <br> -A: "foo" <br> -B: "1.5" <br> -C: "3e308" |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-POST-010 |
| Author            | Luke |
| Title             | Failure to store a device when no parameters are passed to the `params` object |
| Description       | Test the ability of Device to reject requests to store a device when the `params` object is empty |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON <br>-submit a POST request to http://localhost:1338/devices |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/params must have required property 'longLivedCookie'"}` |
| Data              | -a User account for authentication <br>-request body JSON: `{"params":{}}` |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-POST-011 |
| Author            | Luke |
| Title             | Failure to store a device when no parameters are passed to the request body |
| Description       | Test the ability of Device to reject requests to store a device when the request body is empty |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input an empty request body <br>-submit a POST request to http://localhost:1338/devices |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data must have required property 'params'"}` |
| Data              | -a User account for authentication |


### 1.2 GET /devices
|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-GET-001 |
| Author            | Luke |
| Title             | Successfully check for the existence of a device in the database |
| Description       | Test the ability of Device to successfully check for the existence of a device in the database <br>-note: this request only checks if a device of the provided UUID is stored in the database, it does not retrieve the entry |
| Initial Condition | -User is authenticated (logged in) <br>-a device is stored in the database |
| Testing Steps     | -submit a GET request to http://localhost:1338/devices/{uuid} and provide the UUID of the device that is stored |
| Success Criteria  | -JSON: `{"found":true}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-GET-002 |
| Author            | Luke |
| Title             | Successfully confirm that a device does not exist |
| Description       | Test the ability of Device to successfully check that the provided UUID does not correspond to a stored device (i.e., the device has not been stored) |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -submit a GET request to http://localhost:1338/devices/abc123 |
| Success Criteria  | -JSON: `{"found":false}` |
| Data              | -a User account for authentication |

<br>

>note: unauthenticated users can currently perform GET requests on this endpoint. This is an issue that will be fixed

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-GET-003 |
| Author            | Luke |
| Title             | Failure to check if a device exists when the User is not authenticated |
| Description       | Test the ability of Device to reject requests to check for a stored device when the User is not authenticated |
| Initial Condition | -User is not authenticated |
| Testing Steps     | -submit a GET request to http://localhost:1338/devices/abc123 |
| Success Criteria  | -JSON: `{"statusCode":403,"error":"Forbidden","message":"Forbidden"}` |
| Data              | -n/a |

### 1.3 DELETE /devices
|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-DELETE-001 |
| Author            | Luke |
| Title             | Successfully delete a device from the database |
| Description       | Test the ability of Device to successfully delete a stored device of the provided UUID |
| Initial Condition | -User is authenticated (logged in) <br>-a device is stored in the database |
| Testing Steps     | -submit a DELETE request to http://localhost:1338/devices/{uuid} and provide the UUID of the device that is stored |
| Success Criteria  | -JSON: `{"success":true}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Device-DELETE-002 |
| Author            | Luke |
| Title             | Failure to delete device when User is not authenticated |
| Description       | Test the ability of Device to reject attempts to delete a device when the user is not authenticated |
| Initial Condition | -User is not authenticated |
| Testing Steps     | -submit a DELETE request to http://localhost:1338/devices/abc123 |
| Success Criteria  | -JSON: `{"statusCode":403,"error":"Forbidden","message":"Forbidden"}` |
| Data              | -n/a |

## 2. Project
>TODO: include tests for once implemented

## 3. Protocol
>TODO: include tests for once implemented

## 4. Upload - File
>TODO: include tests for once implemented

## 5. UsersPermissions - Role
>TODO: include tests for once implemented

## 6. UsersPermissions - User
>TODO: include tests for once implemented

## 7. Org-Interface
### 7.1 GET ​/pdp​/{projectId}​/{protocolId}​/read
|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-PDP-read-001 |
| Author            | Luke |
| Title             | Failure to authorize read permissions using the PDP when the User is not authenticated |
| Description       | Test the ability of the Org Interface to reject attempts to authorize read permissions using the PDP when the User is not authenticated |
| Initial Condition | -User is not authenticated (not logged in) |
| Testing Steps     | -submit a GET request to http://localhost:1338/pdp/1/1/read |
| Success Criteria  | -JSON: `{"statusCode":403,"error":"Forbidden","message":"Forbidden"}`
| Data              | n/a  |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-PDP-read-002 |
| Author            | Luke, Kira |
| Title             | Failure to authorize read permissions using the PDP when user is not assigned to that project |
| Description       | Test the ability of the Org Interface to reject attempts to authorize read permissions using the PDP |
| Initial Condition | -User is logged in but NOT assigned the project|
| Testing Steps     | -submit a GET request to http://localhost:1338/pdp/2/3/read |
| Success Criteria  | -JSON: `{ "isAuthorised": false }` |
| Data              | -a User account for authentication <br>-a Project with ID 2 <br> -a Protocol with ID 3 and Version 1, assigned to the Project <br> -a User that has not been assigned to the Project  |
<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-PDP-read-003 |
| Author            | Luke |
| Title             | Successful authorization of read permissions using the PDP |
| Description       | Test the ability of the Org Interface to authorize read permissions using the PDP |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -submit a GET request to http://localhost:1338/pdp/1/2/read |
| Success Criteria  | -JSON: `{ "isAuthorised": true }` |
| Data              | -a User account for authentication <br>-a Project with ID 1 <br> -a Protocol with ID 2 and Version 1, assigned to the Project <br> -a User that has been assigned to the Project  |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-PDP-read-004 |
| Author            | Michael, Luke |
| Title             | Failure to authorize read permissions using the PDP when the project ID is not an integer (A: string/char, B: decimal, C: big number - 308 digits long) |
| Description       | Tests the ability of the GET PDP Read endpoint in properly catching and descriptively notifying the reasons of failure for incorrect use of it's ProjectId field |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -submit a GET request to http://localhost:1338/pdp/\<data>/1/read |
| Success Criteria  | -A: ``{"statusCode":400,"error":"Bad Request","message":"projectId must be a `number` type, but the final value was: `NaN` (cast from the value `\"foo\"`)."}`` <br> -B/C: ``{"statusCode":400,"error":"Bad Request","message":"projectId must be an integer"}`` |
| Data              | -a User account for authentication <br> -A: "foo" <br> -B: "1.5" <br> -C: "3e308" |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-PDP-read-005 |
| Author            | Michael, Luke |
| Title             | Failure to authorize read permissions using the PDP when the protocol ID is not an integer (A: string/char, B: decimal, C: big number - 308 digits long) |
| Description       | Tests the ability of the GET PDP Read endpoint in properly catching and descriptively notifying the reasons of failure for incorrect use of it's ProtocolId field |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -submit a GET request to http://localhost:1338/pdp/1/\<data>/read |
| Success Criteria  | -A: ``{"statusCode":400,"error":"Bad Request","message":"protocolId must be a `number` type, but the final value was: `NaN` (cast from the value `\"foo\"`)."}`` <br> -B/C: ``{"statusCode":400,"error":"Bad Request","message":"protocolId must be an integer"}`` |
| Data              | -a User account for authentication <br> -A: "foo" <br> -B: "1.5" <br> -C: "3e308" |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-PDP-read-006 |
| Author            | Luke |
| Title             | Failure to authorize read permissions using the PDP when the protocol does not exist |
| Description       | Test the ability of the Org Interface PDP to reject attempts to authorize when the supplied protocol ID does not exist <br>-note: this doesn't also require a test for write, as in both read/write, the check is performed before PDP authorization begins |
| Initial Condition | -User is authenticated (logged in) <br>-the supplied protocol does not exist in the database |
| Testing Steps     | -submit a GET request to http://localhost:1338/pdp/1/100/read |
| Success Criteria  | -JSON: `{"isAuthorised":false,"message":"Protocol 100 does not exist"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-PDP-read-007 |
| Author            | Luke |
| Title             | Failure to authorize read permissions using the PDP when the project does not exist |
| Description       | Test the ability of the Org Interface PDP to reject attempts to authorize when the supplied project ID does not exist <br>-note: this doesn't also require a test for write, as in both read/write, the check is performed before PDP authorization begins |
| Initial Condition | -User is authenticated (logged in) <br>-the supplied project does not exist in the database |
| Testing Steps     | -submit a GET request to http://localhost:1338/pdp/100/1/read |
| Success Criteria  | -JSON: `{"isAuthorised":false,"message":"Project 100 does not exist"}` |
| Data              | -a User account for authentication <br>-a protocol with ID 1 |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-PDP-read-008 |
| Author            | Luke |
| Title             | Failure to authorize read permissions using the PDP when protocol is not assigned to the project |
| Description       | Test the ability of the Org Interface PDP to reject attempts to authorize when the supplied protocol is not assigned to the supplied project <br>-note: this doesn't also require a test for write, as in both read/write, the check is performed before PDP authorization begins |
| Initial Condition | -User is authenticated (logged in) <br>-the supplied protocol is not assigned to the supplied project |
| Testing Steps     | -submit a GET request to http://localhost:1338/pdp/1/2/read |
| Success Criteria  | -JSON: `{"isAuthorised":false,"message":"Could not find protocol 2 assigned to project 1"}` |
| Data              | -a User account for authentication <br>-a project with ID 1 <br>-a protocol with ID 1, assigned to the project <br>-a protocol with ID 2, NOT assigned to the project |

### 7.2 GET ​/pdp​/{projectId}​/{protocolId}​/write
|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-PDP-write-001 |
| Author            | Luke |
| Title             | Failure to authorize write permissions using the PDP when user is not authenticated |
| Description       | Test the ability of the Org Interface to reject attempts to authorize write permissions using the PDP when the User is not authenticated |
| Initial Condition | -User is not authenticated (not logged in) |
| Testing Steps     | -submit a GET request to http://localhost:1338/pdp/1/1/write |
| Success Criteria  | -JSON: `{"statusCode":403,"error":"Forbidden","message":"Forbidden"}`
| Data              | n/a  |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-PDP-write-002 |
| Author            | Luke, Kira |
| Title             | Failure to authorize write permissions using the PDP when user is NOT assigned to the project|
| Description       | Test the ability of the Org Interface to reject attempts to authorize write permissions using the PDP |
| Initial Condition | -User is Authenticated but NOT assigned to the project |
| Testing Steps     | -submit a GET request to http://localhost:1338/pdp/2/3/write |
| Success Criteria  | -JSON: `{ "isAuthorised": false }` |
| Data              | -a User account for authentication <br>-a Project with ID 2 <br> -a Protocol with ID 3 and Version 1, assigned to the Project <br> -a User that has not been assigned to the Project  |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-PDP-write-003 |
| Author            | Luke |
| Title             | Failure to authorize write permissions using the PDP when the protocol does not have write permissions |
| Description       | Test the ability of the Org Interface to reject attempts to authorize write permissions using the PDP when the protocol is not writable |
| Initial Condition | -User is Authenticated (logged in)  |
| Testing Steps     | -submit a GET request to http://localhost:1338/pdp/1/2/write <br>-note: it may be useful to check the console debug is outputting that `User x DOES have access to project 1/protocol 2` and then outputs `Protocol 2 does NOT have write permission`. This indicates that the PDP is first checking that the User has permission and THEN checks if the protocol has write permission |
| Success Criteria  | -JSON: `{ "isAuthorised": false }` |
| Data              | -a User account for authentication <br>-a Project with ID 1 <br>-a Protocol with ID 2 and version 1, assigned to the Project, and with isWrite set to false <br>-a User that has been assigned to the Project |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-PDP-write-004 |
| Author            | Luke |
| Title             | Successful authorization of write permissions using the PDP |
| Description       | Test the ability of the Org Interface to authorize write permissions using the PDP |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -submit a GET request to http://localhost:1338/pdp/1/1/write |
| Success Criteria  | -JSON: `{ "isAuthorised": true }` |
| Data              | -a User account for authentication <br>-a Project with ID 1 <br> -a Protocol with ID and Version 1, assigned to the Project <br> -a User that has been assigned to the Project  |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-PDP-write-005 |
| Author            | Michael, Luke |
| Title             | Failure to authorize write permissions using the PDP when the project ID is not an integer (A: string/char, B: decimal, C: big number - 308 digits long) |
| Description       | Tests the ability of the GET PDP Write endpoint in properly catching and descriptively notifying the reasons of failure for incorrect use of it's ProjectId field |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -submit a GET request to http://localhost:1338/pdp/\<data>/1/write |
| Success Criteria  | -A: ``{"statusCode":400,"error":"Bad Request","message":"projectId must be a `number` type, but the final value was: `NaN` (cast from the value `\"foo\"`)."}`` <br> -B/C: ``{"statusCode":400,"error":"Bad Request","message":"projectId must be an integer"}`` |
| Data              | -a User account for authentication <br> -A: "foo" <br> -B: "1.5" <br> -C: "3e308" |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-PDP-write-006 |
| Author            | Michael, Luke |
| Title             | Failure to authorize write permissions using the PDP when the protocol ID is not an integer (A: string/char, B: decimal, C: big number - 308 digits long) |
| Description       | Tests the ability of the GET PDP Write endpoint in properly catching and descriptively notifying the reasons of failure for incorrect use of it's ProjectId field |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -submit a GET request to http://localhost:1338/pdp/1/\<data>/write |
| Success Criteria  | -A: ``{"statusCode":400,"error":"Bad Request","message":"protocolId must be a `number` type, but the final value was: `NaN` (cast from the value `\"foo\"`)."}`` <br> -B/C: ``{"statusCode":400,"error":"Bad Request","message":"protocolId must be an integer"}`` |
| Data              | -a User account for authentication <br>-A: "foo" <br> -B: "1.5" <br> -C: "3e308" |

### 7.3 GET /user-projects
|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-UserProj-001 |
| Author            | Luke |
| Title             | Failure to get User projects when not authenticated |
| Description       | Test the ability of the Org Interface to reject requests for User projects when the User is not authenticated |
| Initial Condition | -User is not authenticated (not logged in) |
| Testing Steps     | -submit a GET request to: http://localhost:1338/user-projects |
| Success Criteria  | -JSON: `{"statusCode":403,"error":"Forbidden","message":"Forbidden"}` |
| Data              | n/a |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-UserProj-002 |
| Author            | Luke |
| Title             | Successful retrieval of User projects when authenticated |
| Description       | Test the ability of the Org Interface to retrieve User projects when the User is authenticated |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -submit a GET request to: http://localhost:1338/user-projects |
| Success Criteria  | -JSON: `{ "projects": [{ \<project-1> }, { \<project-2> }, ... ,{ \<project-n> }] }` <br> -note: each Project object should also contain Protocol(s) |
| Data              | -a User account for authentication <br> -a User account with at least one Project assigned to it |

### 7.4 POST ​/mint-identifier
|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-MintIdentifier-001 |
| Author            | Luke |
| Title             | Failure to mint identifier when not authenticated |
| Description       | Test the ability of the Org Interface to reject requests for identifiers when the User is not authenticated |
| Initial Condition | -User is not authenticated (not logged in) <br> -User has a projectId, protocolId, and protocol version to supply as the body of the request |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-1 <br> -submit a POST request to: http://localhost:1338/mint-identifier (ensure that no authentication is provided, i.e., no bearer token) |
| Success Criteria  | -JSON: `{"statusCode":403,"error":"Forbidden","message":"Forbidden"}` |
| Data              | -a Project with ID 1 <br> -a Protocol with ID and Version 1, assigned to the Project <br> -a User that has been assigned to the Project |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-MintIdentifier-002 |
| Author            | Luke |
| Title             | Successful minting of identifier |
| Description       | Test the ability of the Org Interface to mint identifiers when a correctly-formatted request is sent |
| Initial Condition | -User is authenticated (logged in) <br> -User has a projectId, protocolId, and protocol version to supply as the body of the request |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-1 <br> -submit a POST request to: http://localhost:1338/mint-identifier |
| Success Criteria  | -JSON: `{ "orgMintedIdentifier": <encrypted-identifier> }` |
| Data              | -a User account for authentication <br>-a Project with ID 1 <br> -a Protocol with ID and Version 1, assigned to the Project <br> -a User that has been assigned to the Project |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-MintIdentifier-003 |
| Author            | Luke |
| Title             | Failure to mint identifier when provided with non-integer projectId (A: string/char, B: decimal, C: big number - 308 digits long) |
| Description       | Test the ability of the Org Interface to reject requests for identifiers when provided with a non-integer projectId |
| Initial Condition | -User is authenticated (logged in) <br> -User has an invalid projectId, but a valid protocolId and protocol version to supply as the body of the request |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-2 <br> -submit a POST request to: http://localhost:1338/mint-identifier |
| Success Criteria  | -A/B/C: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/projectId must be integer"}` |
| Data              | -a User account for authentication <br>-A: "foo" <br> -B: "1.5" <br> -C: "3e308" |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-MintIdentifier-004 |
| Author            | Luke |
| Title             | Failure to mint identifier when provided with non-integer protocolId (A: string/char, B: decimal, C: big number - 308 digits long) |
| Description       | Test the ability of the Org Interface to reject requests for identifiers when provided with a non-integer protocolId |
| Initial Condition | -User is authenticated (logged in) <br> -User has a valid projectId, an invalid valid protocolId, and a valid protocol version to supply as the body of the request |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-3 <br> -submit a POST request to: http://localhost:1338/mint-identifier |
| Success Criteria  | -A/B/C: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/protocol/id must be integer"}` |
| Data              | -a User account for authentication <br> -A: "foo" <br> -B: "1.5" <br> -C: "3e308" |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-MintIdentifier-005 |
| Author            | Luke |
| Title             | Failure to mint identifier when provided with non-integer protocol version (A: string/char, B: decimal, C: big number - 308 digits long) |
| Description       | Test the ability of the Org Interface to reject requests for identifiers when provided with a non-integer protocol version |
| Initial Condition | -User is authenticated (logged in) <br> -User has a valid projectId and protocolId, and an invalid valid protocol version to supply as the body of the request |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-4 <br> -submit a POST request to: http://localhost:1338/mint-identifier |
| Success Criteria  | -A/B/C: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/protocol/version must be integer"}` |
| Data              | -a User account for authentication <br> -A: "foo" <br> -B: "1.5" <br> -C: "3e308" |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-MintIdentifier-006 |
| Author            | Michael, Luke |
| Title             | Failure to mint identifier when provided with an empty request body |
| Description       | Test the capability of paratoo-org in detecting and providing descriptive responses to invalid data |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in the Data field. <br> -submit a POST request to: http://localhost:1338/mint-identifier |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data must have required property 'projectId'"}` |
| Data              | -a User account for authentication <br>-``{  }`` |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-MintIdentifier-007 |
| Author            | Luke, Michael |
| Title             | Failure to mint identifier when provided with an invalid survey ID |
| Description       | Test the ability of the Org Interface to reject attempts to mint identifiers when provided with an invalid survey ID (A: `surveyType` is not a string, B: `time` is not type `date-time`, C: `uuid` is not an integer) |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-5 <br> -submit a POST request to: http://localhost:1338/mint-identifier |
| Success Criteria  | -A: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/survey_metadata/survey_details/survey_model must be string"}`<br>-B: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/survey_metadata/survey_details/time must match format \"date-time\""}`<br>-C: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/survey_metadata/survey_details/uuid must be uuid"}` |
| Data              | -a User account for authentication |

### 7.5 POST ​/collection
|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-Collection-001 |
| Author            | Luke |
| Title             | Successful notification of a collection submission |
| Description       | Test the ability of Core to notify Org of the submission of a collection. Core will give Org an encrypted collection identifier to store as a cross-reference. |
| Initial Condition | -User is authenticated (logged in) <br> -an identifier has been minted <br> -a collection has been successfully submitted |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-6 <br> -submit a POST request to http://localhost:1338/collection |
| Success Criteria  | -JSON: `{ "success": true }` |
| Data              | -a User account for authentication <br>-a Project with ID 1 <br> -a Protocol with ID and Version 1, assigned to the Project <br> -a User that has been assigned to the Project |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-Collection-002 |
| Author            | Luke |
| Title             | Failure to notify a collection submission when entry already exists |
| Description       | Test the ability of the Org Interface to reject the creation of an identifier entry when said entry already exists. |
| Initial Condition | -User is authenticated (logged in) <br> -an identifier has been minted <br> -a collection has been successfully submitted |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-6 <br> -submit a POST request to http://localhost:1338/collection |
| Success Criteria  | -JSON: `{ "statusCode": 400, "error": "Bad Request", "message": Duplicate entry. Collection identifier entry already exists" }` |
| Data              | -a User account for authentication <br>-a Project with ID 1 <br> -a Protocol with ID and Version 1, assigned to the Project <br> -a User that has been assigned to the Project |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-Collection-003 |
| Author            | Luke |
| Title             | Failure to notify a collection submission when the identifier field is empty |
| Description       | Test the ability of the Org Interface to reject the creation of an identifier entry when the collection identifier field is empty |
| Initial Condition | -User is authenticated (logged in) <br> -a collection has been successfully submitted |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-7 <br> -submit a POST request to http://localhost:1338/collection |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/orgMintedIdentifier must NOT have fewer than 1 characters"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-Collection-004 |
| Author            | Luke |
| Title             | Failure to notify a collection submission when the event time is not of type date-time |
| Description       | Test the ability of the Org Interface to reject the creation of an identifier entry when the event time is not of type date-time<br>Note: a blank string is given, but anything that doesn't follow [RFC 3339](https://datatracker.ietf.org/doc/html/rfc3339#section-5.6) will be rejected |
| Initial Condition | -User is authenticated (logged in) <br> -a collection has been successfully submitted |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-8 <br> -submit a POST request to http://localhost:1338/collection |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/eventTime must match format \"date-time\""}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-Collection-005 |
| Author            | Luke |
| Title             | Failure to notify a collection submission when the user ID is not an integer (A: string/char, B: decimal, C: big number - 308 digits long) |
| Description       | Test the ability of the Org Interface to reject the creation of an identifier entry when the supplied user ID is not an integer |
| Initial Condition | -User is authenticated (logged in) <br> -an identifier has been minted <br> -a collection has been successfully submitted |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-9 <br> -submit a POST request to http://localhost:1338/collection |
| Success Criteria  | -A/B/C: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/userId must be integer"}` |
| Data              | -a User account for authentication <br> -A: "foo" <br> -B: "1.5" <br> -C: "3e308" |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-Collection-006 |
| Author            | Luke |
| Title             | Failure to notify a collection submission when the project ID is not an integer (A: string/char, B: decimal, C: big number - 308 digits long) |
| Description       | Test the ability of the Org Interface to reject the creation of an identifier entry when the supplied project ID is not an integer |
| Initial Condition | -User is authenticated (logged in) <br> -an identifier has been minted <br> -a collection has been successfully submitted |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-10 <br> -submit a POST request to http://localhost:1338/collection |
| Success Criteria  | -A/B/C: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/projectId must be integer"}` |
| Data              | -a User account for authentication <br> -A: "foo" <br> -B: "1.5" <br> -C: "3e308" |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-Collection-007 |
| Author            | Luke |
| Title             | Failure to notify a collection submission when the protocol ID is not an integer (A: string/char, B: decimal, C: big number - 308 digits long) |
| Description       | Test the ability of the Org Interface to reject the creation of an identifier entry when the supplied protocol ID is not an integer |
| Initial Condition | -User is authenticated (logged in) <br> -an identifier has been minted <br> -a collection has been successfully submitted |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-11 <br> -submit a POST request to http://localhost:1338/collection |
| Success Criteria  | -A/B/C: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/protocol/id must be integer"}` |
| Data              | -a User account for authentication <br> -A: "foo" <br> -B: "1.5" <br> -C: "3e308" |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-Collection-008 |
| Author            | Luke |
| Title             | Failure to notify a collection submission when the protocol version is not an integer (A: string/char, B: decimal, C: big number - 308 digits long) |
| Description       | Test the ability of the Org Interface to reject the creation of an identifier entry when the supplied protocol version is not an integer |
| Initial Condition | -User is authenticated (logged in) <br> -an identifier has been minted <br> -a collection has been successfully submitted |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-12 <br> -submit a POST request to http://localhost:1338/collection |
| Success Criteria  | -A/B/C: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/protocol/version must be integer"}` |
| Data              | -a User account for authentication <br> -A: "foo" <br> -B: "1.5" <br> -C: "3e308" |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-Collection-009 |
| Author            | \<author-1>, \<author-2>, etc. |
| Title             | Failure to notify a collection submission when the User is not authenticated |
| Description       | \<description> |
| Initial Condition | \<init-cond.> |
| Testing Steps     | \<steps> |
| Success Criteria  | \<criteria> |
| Data              | \<data> |

### 7.6 POST ​/challenge

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-challenge-001 |
| Author            | Luke |
| Title             | Successful return of a generated challenge |
| Description       | Test the ability of Org to successfully generate a challenge when supplied with a correctly-formatted request body |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-14 <br>-submit a POST request to http://localhost:1338/challenge |
| Success Criteria  | -JSON: `{"challenge":"<random-string>","rp":{"name":"Paratoo-Org"},"user":{"id":<user-id>,"name":"<username>","displayName":"<user-email>"}}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-challenge-002 |
| Author            | Luke |
| Title             | Failure to generate a challenge when the supplied user ID is not an integer (A: string/char, B: decimal, C: big number - 308 digits long) |
| Description       | Test the ability of Org to reject requests to generate a challenge when the supplied User ID is not an integer |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-15 <br>-submit a POST request to http://localhost:1338/challenge |
| Success Criteria  | -A/B/C: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/userInfo/userId must be integer"}` |
| Data              | -a User account for authentication <br> -A: "foo" <br> -B: "1.5" <br> -C: "3e308" |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-challenge-003 |
| Author            | Luke |
| Title             | Failure to generate a challenge when the supplied username is empty |
| Description       | Test the ability of Org to reject requests to generate a challenge when the supplied username is empty |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-16 <br>-submit a POST request to http://localhost:1338/challenge |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/userInfo/username must NOT have fewer than 1 characters"}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-challenge-004 |
| Author            | Luke |
| Title             | Failure to generate a challenge when the supplied user email is not of type email |
| Description       | Test the ability of Org to reject requests to generate a challenge when the supplied email is not of type email |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-17 <br>-submit a POST request to http://localhost:1338/challenge |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/userInfo/userEmail must match format \"email\""}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-challenge-005 |
| Author            | Luke |
| Title             | Failure to generate a challenge when the User is not authenticated |
| Description       | Test the ability of Org to reject requests to generate a challenge when the User is not authenticated |
| Initial Condition | -User is NOT authenticated |
| Testing Steps     | -submit a POST request to http://localhost:1338/challenge |
| Success Criteria  | -JSON: `{"statusCode":403,"error":"Forbidden","message":"Forbidden"}` |
| Data              | n/a |

### 7.7 POST ​/login-challenge
>TODO (when WebAuthn is fully implemented)

### 7.8 POST /verify-register
>TODO (when WebAuthn is fully implemented)

### 7.9 POST /verify-login
>TODO (when WebAuthn is fully implemented)

### 7.10 GET /status
>Note: `/status` is explicitly checking if a `collection-identifier` exists, where as `/collection` is throwing an error when a `collection-identifier` already exists. As such `/status` uses the `identifier_hash` to do this lookup.

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-status-001 |
| Author            | Luke |
| Title             | Successfully return true when a `collection-identifier` has already been submitted |
| Description       | Test the ability of the Org Interface to correctly lookup a `collection-identifier` that has been submitted using the provided `identifier_hash` |
| Initial Condition | -User is authenticated (logged in) <br>-a collection is stored in the database |
| Testing Steps     | -submit a GET request to http://localhost:1338/status/<hashed_identifier>, with the `hashed_identifier` of the collection that is stored in Org |
| Success Criteria  | -JSON: `{"isSubmitted":true}` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-GET-status-002 |
| Author            | Luke |
| Title             | Successfully return false when a `collection-identifier` has not been submitted |
| Description       | Test the ability of the Org Interface to correctly lookup a `collection-identifier` that has been submitted using the provided `identifier_hash` |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -submit a GET request to http://localhost:1338/status/foo |
| Success Criteria  | -JSON: `{"isSubmitted":false}` |
| Data              | -a User account for authentication |

### 7.11 POST /validate-token
>NOTE: currently Core's project membership enforcer needs to check that the supplied token is valid. Because Org issues them we provide this endpoint. However, due to limitations with Jest, we cannot have the enforcer simply hit this endpoint for testing. So we test the enforcer's ability to handle the error by mocking Org's response, and test Org's ability to validate tokens by hitting this endpoint. Also, the `/validation-token` endpoint doesn't technically require an auth header, but since we need to supply one in the request body, it is considered an initial condition to be authenticated.

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-token-001 |
| Author            | Luke |
| Title             | Successfully validate a token when provided with a valid one |
| Description       | Test the ability of the Org Interface to validate a JWT when the provided token is valid |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-25, using the same token as given in the request header <br>-submit a POST request to http://localhost:1338/validate-token |
| Success Criteria  | -returns `true` |
| Data              | -a User account for authentication |

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | Org-Intfc-POST-token-002 |
| Author            | Luke |
| Title             | Failure to validate a token when provided with a garbage token |
| Description       | Test the ability of the Org Interface to successfully validate that a garbage token is not valid |
| Initial Condition | -User is authenticated (logged in) |
| Testing Steps     | -input a body with the supplied JSON in Appendix 1-26 <br>-submit a POST request to http://localhost:1338/validate-token |
| Success Criteria  | -JSON: `{"statusCode":400,"error":"Bad Request","message":"Invalid: data/token must match pattern \"^Bearer \""}` |
| Data              | -a User account for authentication |


# Appendices
## Appendix 1: Code for Tests
>Add code for tests here that won't format correctly inside the tables
### Appendix 1-1: Org-Intfc-POST-MintIdentifier-001 & Org-Intfc-POST-MintIdentifier-002 POST request JSON
```
{
    "survey_metadata": {
      "survey_details": {
        "survey_model": 1,
        "time": "2024-03-04T05:52:28.193Z",
        "uuid": "ea344b36-859a-42a7-bc09-61cc2783e3e2",
        "project_id": "1",
        "protocol_id": 1,
        "protocol_version": "1"
      },
      "user_details": {
        "role": "collector",
        "email": "testuser@email.com",
        "username": "TestUser"
      },
      "provenance": {
        "version_app": "0.0.1-xxxxx",
        "version_core_documentation": "0.0.0-xxxx",
        "system_app": "Monitor FDCP API--localdev",
      }
	  }
}
```

### Appendix 1-2: Org-Intfc-POST-MintIdentifier-003 POST request JSON
```
{
    "survey_metadata": {
      "survey_details": {
        "survey_model": 1,
        "time": "2024-03-04T05:52:28.193Z",
        "uuid": "ea344b36-859a-42a7-bc09-61cc2783e3e2",
        "project_id": "1",
        "protocol_id": 1,
        "protocol_version": "1"
      },
      "user_details": {
        "role": "collector",
        "email": "testuser@email.com",
        "username": "TestUser"
      },
      "provenance": {
        "version_app": "0.0.1-xxxxx",
        "version_core_documentation": "0.0.0-xxxx",
        "system_app": "Monitor FDCP API--localdev",
      }
	  }
}
```

### Appendix 1-3: Org-Intfc-POST-MintIdentifier-004 POST request JSON
```
{
    "survey_metadata": {
      "survey_details": {
        "survey_model": 1,
        "time": "2024-03-04T05:52:28.193Z",
        "uuid": "ea344b36-859a-42a7-bc09-61cc2783e3e2",
        "project_id": "1",
        "protocol_id": <data>,
        "protocol_version": "1"
      },
      "user_details": {
        "role": "collector",
        "email": "testuser@email.com",
        "username": "TestUser"
      },
      "provenance": {
        "version_app": "0.0.1-xxxxx",
        "version_core_documentation": "0.0.0-xxxx",
        "system_app": "Monitor FDCP API--localdev",
        "version_org": "0.0.0-xxxx",
        "system_org": "Monitor FDCP API-development"
      }
	  }
}
```

### Appendix 1-4: Org-Intfc-POST-MintIdentifier-005 POST request JSON
```
{
    "survey_metadata": {
      "survey_details": {
        "survey_model": 1,
        "time": "2024-03-04T05:52:28.193Z",
        "uuid": "ea344b36-859a-42a7-bc09-61cc2783e3e2",
        "project_id": "1",
        "protocol_id": 1,
        "protocol_version": <data>
      },
      "user_details": {
        "role": "collector",
        "email": "testuser@email.com",
        "username": "TestUser"
      },
      "provenance": {
        "version_app": "0.0.1-xxxxx",
        "version_core_documentation": "0.0.0-xxxx",
        "system_app": "Monitor FDCP API--localdev",
        "version_org": "0.0.0-xxxx",
        "system_org": "Monitor FDCP API-development"
      }
	  }
}
```

### Appendix 1-5: Org-Intfc-POST-MintIdentifier-007 POST request JSON
```
{
    "survey_metadata": {
      <data>
	  }
}
```

#### Org-Intfc-POST-MintIdentifier-007 Case A
```
"survey_metadata": {
      "survey_details": {
        "survey_model": 1,
        "time": "2024-03-04T05:52:28.193Z",
        "uuid": "ea344b36-859a-42a7-bc09-61cc2783e3e2",
        "project_id": "1",
        "protocol_id": 1,
        "protocol_version": "1"
      },
      "user_details": {
        "role": "collector",
        "email": "testuser@email.com",
        "username": "TestUser"
      },
      "provenance": {
        "version_app": "0.0.1-xxxxx",
        "version_core_documentation": "0.0.0-xxxx",
        "system_app": "Monitor FDCP API--localdev",
        "version_org": "0.0.0-xxxx",
        "system_org": "Monitor FDCP API-development"
      }
	  }
```

#### Org-Intfc-POST-MintIdentifier-007 Case B
```
"survey_metadata": {
  "survey_details": {
    "survey_model": "bird-survey",
    "time": "blah",
    "uuid": "ea344b36-859a-42a7-bc09-61cc2783e3e2",
    "project_id": "1",
    "protocol_id": 1,
    "protocol_version": "1"
  },
  "user_details": {
    "role": "collector",
    "email": "testuser@email.com",
    "username": "TestUser"
  },
  "provenance": {
    "version_app": "0.0.1-xxxxx",
    "version_core_documentation": "0.0.0-xxxx",
    "system_app": "Monitor FDCP API--localdev",
    "version_org": "0.0.0-xxxx",
    "system_org": "Monitor FDCP API-development"
  }
}
```

#### Org-Intfc-POST-MintIdentifier-007 Case C
```
"survey_metadata": {
  "survey_details": {
    "survey_model": "bird-survey",
    "time": "blah",
    "uuid": "1234",
    "project_id": "1",
    "protocol_id": 1,
    "protocol_version": "1"
  },
  "user_details": {
    "role": "collector",
    "email": "testuser@email.com",
    "username": "TestUser"
  },
  "provenance": {
    "version_app": "0.0.1-xxxxx",
    "version_core_documentation": "0.0.0-xxxx",
    "system_app": "Monitor FDCP API--localdev",
    "version_org": "0.0.0-xxxx",
    "system_org": "Monitor FDCP API-development"
  }
}
```

### Appendix 1-6: Org-Intfc-POST-Collection-001 & Org-Intfc-POST-Collection-002 POST request JSON
```
{
  "orgMintedIdentifier": <hashed identifier>,
  "eventTime": "2021-08-10T05:50:53.622Z",
  "userId": 1,
}
```

### Appendix 1-7: Org-Intfc-POST-Collection-003 POST request JSON
```
{
  "orgMintedIdentifier": "",
  "eventTime": "2021-08-10T05:50:53.622Z",
  "userId": 1,
}
```

### Appendix 1-8: Org-Intfc-POST-Collection-004 POST request JSON
```
{
  "orgMintedIdentifier": <hashed identifier>,
  "eventTime": "",
  "userId": 1,
}
```

### Appendix 1-9: Org-Intfc-POST-Collection-005 POST request JSON
```
{
  "orgMintedIdentifier": <hashed identifier>,
  "eventTime": "2021-08-10T05:50:53.622Z",
  "userId": <data>,
}
```

### Appendix 1-10: Org-Intfc-POST-Collection-006 POST request JSON
```
{
  "orgMintedIdentifier": <hashed identifier>,
  "eventTime": "2021-08-10T05:50:53.622Z",
  "userId": 1,
}
```

### Appendix 1-11: Org-Intfc-POST-Collection-007 POST request JSON
```
{
  "orgMintedIdentifier": <hashed identifier>,
  "eventTime": "2021-08-10T05:50:53.622Z",
  "userId": 1,
}
```

### Appendix 1-12: Org-Intfc-POST-Collection-008 POST request JSON
```
{
  "orgMintedIdentifier": <hashed identifier>,
  "eventTime": "2021-08-10T05:50:53.622Z",
  "userId": 1,
}
```

### Appendix 1-13: Device-POST-001 POST request JSON
>Note: typically the `data` arrays for publicKey and credentialId would contain a list of buffered bytes (ints), but for this test they are left out.
```
{
  "params": {
    "longLivedCookie": "1fc1e4d5-2e9b-4218-9ea2-9cc118b91e7f",
    "challenge": "MSz8EzSI33BIA3fS",
    "publicKey": {"type":"Buffer","data":[]}, 
    "credentialId": {"type":"Buffer","data":[]},
    "clientDataJSON": {"type":"webauthn.create", "challenge":"QzE4ZkZRY2Z1b1l6RGZnRw", "origin":"http://localhost:8080", "crossOrigin":false}, 
    "user": 1 
  }
}
```

### Appendix 1-14: Org-Intfc-POST-challenge-001 POST request JSON
```
{
  "userInfo": {
    "userId": 1,
    "username": "TestUser",
    "userEmail": "testuser@email.com"
  }
}
```

### Appendix 1-15: Org-Intfc-POST-challenge-002 POST request JSON
```
{
  "userInfo": {
    "userId": <data>,
    "username": "TestUser",
    "userEmail": "testuser@email.com"
  }
}
```

### Appendix 1-16: Org-Intfc-POST-challenge-003 POST request JSON
```
{
  "userInfo": {
    "userId": 1,
    "username": "",
    "userEmail": "testuser@email.com"
  }
}
```

### Appendix 1-17: Org-Intfc-POST-challenge-004 POST request JSON
```
{
  "userInfo": {
    "userId": 1,
    "username": "TestUser",
    "userEmail": ""
  }
}
```

### Appendix 1-18: Device-POST-002 POST request JSON
```
{
  "params": {
    "longLivedCookie": "1fc1e4d5-2e9b-4218-9ea2-9cc118b91e7f",
    "challenge": "MSz8EzSI33BIA3fS",
    "publicKey": {"type":"Buffer","data":[]}, 
    "credentialId": {"type":"Buffer","data":[]},
    "clientDataJSON": {"type":"webauthn.create","challenge":"QzE4ZkZRY2Z1b1l6RGZnRw","origin":"http://localhost:8080","crossOrigin":false}, 
    "user": 1 
  }
}
```

### Appendix 1-19: Device-POST-004 POST request JSON
```
{
  "params": {
    "longLivedCookie": "",
    "challenge": "MSz8EzSI33BIA3fS",
    "publicKey": {"type":"Buffer","data":[]}, 
    "credentialId": {"type":"Buffer","data":[]},
    "clientDataJSON": {"type":"webauthn.create","challenge":"QzE4ZkZRY2Z1b1l6RGZnRw","origin":"http://localhost:8080","crossOrigin":false}, 
    "user": 1 
  }
}
```

### Appendix 1-20: Device-POST-005 POST request JSON
```
{
  "params": {
    "longLivedCookie": "1fc1e4d5-2e9b-4218-9ea2-9cc118b91e7f",
    "challenge": "",
    "publicKey": {"type":"Buffer","data":[]}, 
    "credentialId": {"type":"Buffer","data":[]},
    "clientDataJSON": {"type":"webauthn.create","challenge":"QzE4ZkZRY2Z1b1l6RGZnRw","origin":"http://localhost:8080","crossOrigin":false}, 
    "user": 1 
  }
}
```

### Appendix 1-21: Device-POST-006 POST request JSON
```
{
  "params": {
    "longLivedCookie": "1fc1e4d5-2e9b-4218-9ea2-9cc118b91e7f",
    "challenge": "MSz8EzSI33BIA3fS",
    "publicKey": <data>, 
    "credentialId": {"type":"Buffer","data":[]},
    "clientDataJSON": {"type":"webauthn.create","challenge":"QzE4ZkZRY2Z1b1l6RGZnRw","origin":"http://localhost:8080","crossOrigin":false}, 
    "user": 1 
  }
}
```

### Appendix 1-22: Device-POST-007 POST request JSON
```
{
  "params": {
    "longLivedCookie": "1fc1e4d5-2e9b-4218-9ea2-9cc118b91e7f",
    "challenge": "MSz8EzSI33BIA3fS",
    "publicKey": {"type":"Buffer","data":[]}, 
    "credentialId": <data>,
    "clientDataJSON": {"type":"webauthn.create","challenge":"QzE4ZkZRY2Z1b1l6RGZnRw","origin":"http://localhost:8080","crossOrigin":false}, 
    "user": 1 
  }
}
```

### Appendix 1-23: Device-POST-008 POST request JSON
```
{
  "params": {
    "longLivedCookie": "1fc1e4d5-2e9b-4218-9ea2-9cc118b91e7f",
    "challenge": "MSz8EzSI33BIA3fS",
    "publicKey": {"type":"Buffer","data":[]}, 
    "credentialId": {"type":"Buffer","data":[]},
    "clientDataJSON": <data>, 
    "user": 1 
  }
}
```

### Appendix 1-24: Device-POST-009 POST request JSON
```
{
  "params": {
    "longLivedCookie": "1fc1e4d5-2e9b-4218-9ea2-9cc118b91e7f",
    "challenge": "MSz8EzSI33BIA3fS",
    "publicKey": {"type":"Buffer","data":[]}, 
    "credentialId": {"type":"Buffer","data":[]},
    "clientDataJSON": {"type":"webauthn.create","challenge":"QzE4ZkZRY2Z1b1l6RGZnRw","origin":"http://localhost:8080","crossOrigin":false}, 
    "user": <data>
  }
}
```

### Appendix 1-25: Org-Intfc-POST-token-001 POST request JSON
```
{
  "token": "Bearer <token>"
}
```

### Appendix 1-26: Org-Intfc-POST-token-002 POST request JSON
```
{
  "token": "garbage token"
}
```

## Appendix 2: Template:
|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | \<id> |
| Author            | \<author-1>, \<author-2>, etc. |
| Title             | \<title> |
| Description       | \<description> |
| Initial Condition | \<init-cond.> |
| Testing Steps     | \<steps> |
| Success Criteria  | \<criteria> |
| Data              | \<data> |

Test case ID's should be based on the content type and endpoint. For example, the Org-Interface (content type) GET /user-projects (endpoint) should look like: `Org-Intfc-GET-UserProj-XYZ`
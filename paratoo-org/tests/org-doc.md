# Paratoo-ORG API implementation

Please update this document every time you make changes to org interface

### POST /mint-identifier
#### Example of Expected input format:

```
{
    "headers": "Bearer " <jwt>
}
{
    "projectId": 1,
    "protocol": {
        "id": 2,
        "version": 1
    },
    "survey_metadata": "123"
}

```
#### Example of Success Response 

Status code: 200
Response: 
```
{ 
  "orgMintedIdentifier": "{\"iv\":\"QCMAf4crj9+l57+ZiQh0yg==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"mUnDCVq0T8w=\",\"ct\":\"7hNGD9dWfd+h/S41MrsW4k+M9BvJAx8GEtgATBGax50HzBCMh18j+hnFBnyJUV7zBiv/T7ZUVLRx8v8rRmX76GBf2HE0R84gUinCzJSGagzNtHhDEwMuxmTJDlLkoJLeDdj3Ggj84dV4HJSlf0ZTyjDBiBaUJS8eyDWZgk8=\"}" 
}
```

#### Expected error code

Error Code | Message | Reason | Implemented | Tested | TestID
-----------|---------|--------|-------------|--------|-------
403 | Forbidden | User is not authenticated | [x] | [x] | Org-Intfc-POST-MintIdentifier-001
400 | Invalid: data/project_id must be string | project_id is not a string | [x] | [x] | Org-Intfc-POST-MintIdentifier-003
400 | Invalid: data/protocol_id must be string | protocol_id is not a string | [x] | [x] | Org-Intfc-POST-MintIdentifier-004
400 | Invalid: data/protocol_version must be string | protocol_version is not a string | [x] | [x] | Org-Intfc-POST-MintIdentifier-005
400 | Invalid: data must have required property 'projectId' | User provided an empty request body | [x] | [x] | Org-Intfc-POST-MintIdentifier-006
400 | Invalid: data/survey_metadata/survey_details/survey_model must be string<br>or Invalid: data/survey_metadata/survey_details/time must match format \"date-time\"<br>or Invalid: data/survey_metadata/survey_details/uuid must be integer | user provide empty survey metadata string| [x] | [] | Org-Intfc-POST-MintIdentifier-007


### GET /user-projects

#### Example of Expected input format:

```
{
  "headers": "Bearer " <jwt>
}
```
#### Example of Success Response 

Status code: 200
Response: 
```
{ 
  "projects": [{ \<project-1> }, { \<project-2> }, ... ,{ \<project-n> }] 
}

```
#### Expected error code

Error Code | Message | Reason | Implemented | Tested | TestID
-----------|---------|--------|-------------|--------|--------
403 | Forbidden | User is not authenticated | [x] | [x] | Org-Intfc-GET-UserProj-001

### GET ​/pdp​/{projectId}​/{protocolId}​/read

Prerequisite: User must be assigned to at least a project and a protocol
#### Example of Expected input format:

```
{
  "headers": "Bearer " <jwt>
}
```
#### Example of Success Response 

Status code: 200
Response: 
```
{
  "isAuthorised" : true
}
``` 
or 
```
{
  "isAuthorised" : false
}
```

#### Expected error code

Error Code | Message | Reason | Implemented | Tested | TestID
-----------|---------|--------|-------------|--------|-------
403 | Forbidden | User is not authenticated | [x] | [x] | Org-Intfc-GET-PDP-read-001
200 | "isAuthorised": false | User is logged in but NOT assigned the project | [x] | [x] | Org-Intfc-GET-PDP-read-002
400 | "projectId must be an integer" or "projectId must be a `number` type, but the final value was: `NaN`| the project ID is not an integer | [x] | [x] | Org-Intfc-GET-PDP-read-004
400 | "protocolId must be an integer" or "protocolId must be a `number` type, but the final value was: `NaN`| the protocol ID is not an integer | [x] | [x] | Org-Intfc-GET-PDP-read-005
200 | "isAuthorised":false,"message":"Protocol 100 does not exist" | Protocol does not exist | [x] | [x] | Org-Intfc-GET-PDP-read-006
200 | "isAuthorised":false,"message":"Project 100 does not exist" | Project does not exist | [x] | [x] | Org-Intfc-GET-PDP-read-007
200 | "isAuthorised":false,"message":"Could not find protocol 2 assigned to project 1" | Protocol is not assigned to the project | [x] | [x] | Org-Intfc-GET-PDP-read-008

### GET ​/pdp​/{projectId}​/{protocolId}​/write

Prerequisite: User must be assigned to at least a project and a protocol
#### Example of Expected input format:

```
{
  "headers": "Bearer " <jwt>
}
```
#### Example of Success Response 

Status code: 200
Response: 
```
{
  "isAuthorised" : true
}
``` 
or 
```
{
  "isAuthorised" : false
}
```

#### Expected error code

Error Code | Message | Reason | Implemented | Tested | TestID
-----------|---------|--------|-------------|--------|-------
403 | Forbidden | User is not authenticated | [x] | [x] | Org-Intfc-GET-PDP-write-001
200 | "isAuthorised": false | User is logged in but NOT assigned the project | [x] | [x] | Org-Intfc-GET-PDP-write-002
| 200 | "isAuthorised": false | Failure to authorize write permissions using the PDP when the protocol does not have write permissions | [x] | [x] | Org-Intfc-GET-PDP-write-003 |
400 | "projectId must be an integer" or "projectId must be a `number` type, but the final value was: `NaN`| the project ID is not an integer | [x] | [x] | Org-Intfc-GET-PDP-write-005
400 | "protocolId must be an integer" or "protocolId must be a `number` type, but the final value was: `NaN`| the protocol ID is not an integer | [x] | [x] | Org-Intfc-GET-PDP-write-006

### POST /collection

#### Example of Expected input format:

```
{
    "headers": "Bearer " <jwt>
}
{
    "orgMintedIdentifier": <encrypted-identifier>,
    "eventTime": "2021-08-10T05:50:53.622Z",
    "userId": "1",
    "projectId": "1",
    "protocol": {
      "id": "1",
      "version": "1"
    }
}
  
```
#### Example of Success Response 

Status code: 200
Response: 
```
{
  "success" : true
}
``` 
or 
```
{
  "success" : false
}
```

#### Expected error code

Error Code | Message | Reason | Implemented | Tested | TestID
-----------|---------|--------|-------------|--------|-------
403 | Forbidden | User is not authenticated | [x] | [x] | Org-Intfc-POST-Collection-009
400 | Duplicate entry. Collection identifier entry already exists | Entry already exists | [x] | [x] | Org-Intfc-POST-Collection-002
400 | Invalid: data/orgMintedIdentifier must NOT have fewer than 1 characters | Identifier field is empty | [x] | [x] | Org-Intfc-POST-Collection-003
400 | Invalid: data/eventTime must match format \"date-time\" | Event time is not of type date-time | [x] | [x] | Org-Intfc-POST-Collection-004
400 | Invalid: data/userId must be integer | User ID is not an integer | [x] | [x] |  Org-Intfc-POST-Collection-005
400 | Invalid: data/projectId must be integer | Project ID is not an integer  | [x] | [x] | Org-Intfc-POST-Collection-006
400 | Invalid: data/protocol/id must be integer | Protocol ID is not an integer  | [x] | [x] | Org-Intfc-POST-Collection-007
400 | Invalid: data/protocol/version must be integer | Protocol version is not an integer  | [x] | [x] | Org-Intfc-POST-Collection-008



### POST /challenge

#### Example of Expected input format:

```
{
  "userInfo": {
    "userId": 1,
    "username": "TestUser",
    "userEmail": "testuser@email.com"
  }
}
```
#### Example of Success Response 

Status code: 200
Response: 
```
{
  "challenge": "i2PwyC09NU22XKPl",
  "rp": {
    "name": "Paratoo-Org"
  },
  "user": {
    "id": 1,
    "name": "TestUser",
    "displayName": "testuser@email.com"
  }
}
```

#### Expected error code

Error Code | Message | Reason | Implemented | Tested | TestID
-----------|---------|--------|-------------|--------|-------
400 | Invalid: data/userInfo/userId must be integer | User ID is not an integer | [x] | [x] | Org-Intfc-POST-challenge-002
400 | Invalid: data/userInfo/username must NOT have fewer than 1 characters | Username is empty | [x] | [x] | Org-Intfc-POST-challenge-003
400 | Invalid: data/userInfo/userEmail must match format \"email\" | Email is not of type email | [x] | [x] | Org-Intfc-POST-challenge-004
403 | Forbidden | User is not authenticated | [x] | [x] | Org-Intfc-POST-challenge-005

### POST /devices
>These are currently disabled as the implementation of WebAuthn keeps changing. I'll re-enable and update the test cases when WebAuthn is done.

#### Example of Expected input format:
>Note: typically the data arrays for publicKey and credentialId would contain a list of buffered bytes (ints)
```
{
  "params": {
    "longLivedCookie": "a7e8b0e8-883c-4b52-b293-743c70435f78",
    "challenge": "MSz8EzSI33BIA3fS",
    "publicKey": {
      "type": "buffer",
      "data": []
    },
    "credentialId": {
      "type": "buffer",
      "data": []
    },
    "clientDataJSON": {
      "type": "webauthn.create",
      "challenge": "QzE4ZkZRY2Z1b1l6RGZnRw",
      "origin": "http://localhost:8080",
      "crossOrigin": false
    },
    "user": 1
  }
}
```
#### Example of Success Response 

Status code: 200
Response: 
```
{
  "success": true
}
```

#### Expected error code

Error Code | Message | Reason | Implemented | Tested | TestID
-----------|---------|--------|-------------|--------|-------
400 | Duplicate entry. This device is already registered for this user | Device already exists | [x] | [x] | Device-POST-002
403 | Forbidden | User is not authenticated | [x] | [x] | Device-POST-003
400 | Invalid: data/params/longLivedCookie must match format \"uuid\" | Provided UUID is empty (i.e., not valid) | [x] | [x] | Device-POST-004
400 | Invalid: data/params/challenge must match pattern \"[A-Za-z0-9]{16}\" | Provided challenge is empty (i.e., not valid) | [x] | [x] | Device-POST-005
400 | "Invalid: data/params/publicKey must have required property 'type'" or "Invalid: data/params/publicKey must have required property 'data'" | Provided public key is not valid | [x] | [x] | Device-POST-006
400 | "Invalid: data/params/credentialId must have required property 'type'" or "Invalid: data/params/credentialId must have required property 'data'" | Provided credential ID is not valid | [x] | [x] | Device-POST-007
400 | "Invalid: data/params/clientDataJSON must have required property 'type'" or "Invalid: data/params/clientDataJSON must have required property 'challenge'" or "Invalid: data/params/clientDataJSON must have required property 'origin'" | Provided client data JSON is not valid | [x] | [x] | Device-POST-008
400 | Invalid: data/params/user must be integer | Provided User ID is not an integer | [x] | [x] | Device-POST-009
400 | Invalid: data/params must have required property 'longLivedCookie' | No parameters are passed to the `params` object | [x] | [x] | Device-POST-010
400 | Invalid: data must have required property 'params' | No parameters are passed to the request body | [x] | [x] | Device-POST-011

### GET /devices
>These are currently disabled as the implementation of WebAuthn keeps changing. I'll re-enable and update the test cases when WebAuthn is done.
#### Example of Expected input format:

```
{
  "headers": "Bearer " <jwt>
}
```
#### Example of Success Response 

Status code: 200
Response: 
```
{
  "found": true
}
```
```
{
  "found": false
}
```

#### Expected error code

Error Code | Message | Reason | Implemented | Tested | TestID
-----------|---------|--------|-------------|--------|-------
403 | Forbidden | User is not authenticated | [x] | [] | Device-GET-003

### DELETE /devices{uuid}
>These are currently disabled as the implementation of WebAuthn keeps changing. I'll re-enable and update the test cases when WebAuthn is done.
#### Example of Expected input format:

```
{
  "headers": "Bearer " <jwt>
}
```
#### Example of Success Response 

Status code: 200
Response: 
```
{
  "success": true
}
```

#### Expected error code

Error Code | Message | Reason | Implemented | Tested | TestID
-----------|---------|--------|-------------|--------|-------
403 | Forbidden | User is not authenticated | [x] | [] | Device-DELETE-002

### GET /status{hashed_identifier}
#### Example of Expected input format:

```
{
  "headers": "Bearer " <jwt>
}
```
#### Example of Success Response 

Status code: 200
Response: 
```
{
  {"isSubmitted":true}
}
```
or
```
{
  {"isSubmitted":false}
}
```
#### Expected error code
n/a

### POST /validate-token
#### Example of Expected input format:
```
{
  "headers": "Bearer " <jwt>
}
```

#### Example of Success Response 
Status code: 200
Response: 
```
true
```
or
```
false
```

#### Expected error code
n/a
const request = require('supertest')
const mock = require('./mock.json')

var jwt = ''
var user_data

it('Log in for POST /challenge', async () => {
  // uses user-permission plugin to create user with collector role as 
  //   /auth/local/register doest support any other roles except "authenticate"
  const helpers = global.strapiInstance.service('api::paratoo-helper.paratoo-helper')
  helpers.createUserWithRole(mock.userData.username,
    mock.userData.properties,
    mock.userData.role
  )

  //Log user in
  await request(global.strapiServer)
    .post('/api/auth/local')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send(mock.body1)
    .expect('Content-Type', /json/)
    .expect(200)
    .then((data) => {
      expect(data.body.jwt).toBeDefined()
      jwt = data.body.jwt
      user_data = data.body.user
    })
})

/* Org-Intfc-POST-challenge-001 */
// FIXME is-validated can't validate url with multiple params
// it('Org-Intfc-POST-challenge-001: Successful return of a generated challenge', async () => {
//   //Log user in
//   await request(global.strapiServer)
//     .post('/api/org/challenge')
//     .set('accept', 'application/json')
//     .set('Content-Type', 'application/json')
//     .set('Authorization', 'Bearer ' + jwt)
//     .send({
//       userInfo: {
//         userId: user_data.id,
//         username: user_data.username,
//         userEmail: user_data.email,
//       },
//     })
//     .expect('Content-Type', /json/)
//     .expect(200)
//     .then((data) => {
//       console.log('Org-Intfc-POST-challenge-001 ', data.body)
//       expect(data.body.challenge).toBeDefined()
//       expect(data.body.rp.name).toBe('Paratoo-Org')
//       expect(data.body.user.id).toBe(user_data.id)
//       //console.log('Org-Intfc-POST-challenge-001 ', data.body)
//     })
// })

/* Org-Intfc-POST-challenge-002-1 */
it('Org-Intfc-POST-challenge-002-1: Failure to generate a challenge when the supplied user ID is not an integer - input: string', async () => {
  //Log user in
  await request(global.strapiServer)
    .post('/api/org/challenge')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      userInfo: {
        userId: 'foo',
        username: user_data.username,
        userEmail: user_data.email,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.error.message).toBe(
        'Invalid: data/userInfo/userId must be integer',
      )
    })
})

/* Org-Intfc-POST-challenge-002-2*/
it('Org-Intfc-POST-challenge-002-2: Failure to generate a challenge when the supplied user ID is not an integer - input: 1.5', async () => {
  //Log user in
  await request(global.strapiServer)
    .post('/api/org/challenge')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      userInfo: {
        userId: 'foo',
        username: user_data.username,
        userEmail: user_data.email,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      console.log('Org-Intfc-POST-challenge-002-2 body: ', data.body)
      expect(data.body.error.message).toBe(
        'Invalid: data/userInfo/userId must be integer',
      )
    })
})

/* Org-Intfc-POST-challenge-002-3*/
it('Org-Intfc-POST-challenge-002-3: Failure to generate a challenge when the supplied user ID is not an integer - input: 3e308', async () => {
  //Log user in
  await request(global.strapiServer)
    .post('/api/org/challenge')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      userInfo: {
        userId: '3e308',
        username: user_data.username,
        userEmail: user_data.email,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.error.message).toBe(
        'Invalid: data/userInfo/userId must be integer',
      )
    })
})

/* Org-Intfc-POST-challenge-003*/
it('Org-Intfc-POST-challenge-003: Failure to generate a challenge when the supplied username is empty', async () => {
  //Log user in
  await request(global.strapiServer)
    .post('/api/org/challenge')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      userInfo: {
        userId: user_data.id,
        username: '',
        userEmail: user_data.email,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.error.message).toBe(
        'Invalid: data/userInfo/username must NOT have fewer than 1 characters',
      )
    })
})

/* Org-Intfc-POST-challenge-004*/
it('Org-Intfc-POST-challenge-004: Failure to generate a challenge when the supplied user email is not of type email', async () => {
  //Log user in
  await request(global.strapiServer)
    .post('/api/org/challenge')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      userInfo: {
        userId: user_data.id,
        username: user_data.username,
        userEmail: '1234dfkdi',
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.error.message).toBe(
        'Invalid: data/userInfo/userEmail must match format "email"',
      )
    })
})

/* Org-Intfc-POST-challenge-005*/
it('Org-Intfc-POST-challenge-005: Failure to generate a challenge when the User is not authenticated', async () => {
  //Log user in
  await request(global.strapiServer)
    .post('/api/org/challenge')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send({
      userInfo: {
        userId: user_data.id,
        username: user_data.username,
        userEmail: user_data.email,
      },
    })
    .expect('Content-Type', /json/)
    .expect(403)
    .then((data) => {
      expect(data.body.error.message).toBe('Forbidden')
      expect(data.body.error.name).toBe('ForbiddenError')
    })
})

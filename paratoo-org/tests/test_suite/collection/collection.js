const request = require('supertest')
const mock = require('./mock.json')
const uuid = require('uuid')

var jwt = ''
var org_minted_uuid = ''
var user_data

it('Log in to test for POST collection', async () => {
  const helpers = global.strapiInstance.service('api::paratoo-helper.paratoo-helper')

  // create mock user with role
  helpers.createUserWithRole(mock.userData.username,
    mock.userData.properties,
    mock.userData.role
  )

  //Log user in
  await request(global.strapiServer)
    .post('/api/auth/local')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send(mock.body1)
    .expect('Content-Type', /json/)
    .expect(200)
    .then((data) => {
      expect(data.body.jwt).toBeDefined()
      jwt = data.body.jwt
      user_data = data.body.user
    })
})

it('Org-Intfc-POST-Collection-000: reject notifying org of collection when no orgMintedUUID', async () => {
  const randUuid = uuid.v4()
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/collection')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      orgMintedUUID: randUuid,
      coreProvenance: {
        system_core: 'Monitor FDCP API-development',
        version_core: '0.0.0-xxxx',
      },
    })
    .expect(404)
    .then(async (data) => {
      // expect(data.body.success).toBe(true)
      console.log('Org-Intfc-POST-Collection-000 data.body:', data.body)
      expect(data.body.error.message).toBe(
        `No data set found with mintedCollectionId=${randUuid}. Cannot update the success of a collection that does not exist`
      )
    })
})

//Post mint identifier
//sets up a minted identifier for subsequent tests
it('Mint Identifier for POST collection', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/mint-identifier')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send(mock.body2)
    .expect(200)
    .then(async (data) => {
      expect(data.body.orgMintedIdentifier).toBeDefined()
      const decodedIdentifier = JSON.parse(Buffer.from(data.body.orgMintedIdentifier, 'base64'))
      console.log('decodedIdentifier:', JSON.stringify(decodedIdentifier, null, 2))
      org_minted_uuid = decodedIdentifier.survey_metadata.orgMintedUUID
      console.log('org_minted_uuid:', org_minted_uuid)
      const helpers = global.strapiInstance.service(
        'api::paratoo-helper.paratoo-helper',
      )
      // orgMintedUUID exists
      await helpers
        .deepPopulateQuery(
          'collection-identifier',
          {
            survey_metadata: {
              orgMintedUUID: org_minted_uuid,
            },
          },
          {
            survey_metadata: {
              populate: {
                survey_details: true,
                provenance: true,
              },
            },
          },
          false,
        )
        .then((data) => {
          // core_submit_time should be null but org_mint_time should be defined
          expect(data.core_submit_time).toBe(null)
          expect(data.org_mint_time).toBeDefined()
        })
    })
})

/* Org-Intfc-POST-Collection-001 */
it('Org-Intfc-POST-Collection-001: should successfully notify a collection submission ', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/collection')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      orgMintedUUID: org_minted_uuid,
      coreProvenance: {
        system_core: 'Monitor FDCP API-development',
        version_core: '0.0.0-xxxx',
      },
    })
    .expect(200)
    .then(async (data) => {
      expect(data.body.success).toBe(true)
      const helpers = global.strapiInstance.service(
        'api::paratoo-helper.paratoo-helper',
      )
      // orgMintedUUID exists
      await helpers
        .deepPopulateQuery(
          'collection-identifier',
          {
            survey_metadata: {
              orgMintedUUID: org_minted_uuid,
            },
          },
          {
            survey_metadata: {
              populate: {
                survey_details: true,
                provenance: true,
              },
            },
          },
          false,
        )
        .then((data) => {
          // both core_submit_time and org_mint_time should be defined
          expect(data.core_submit_time).toBeDefined()
          expect(data.org_mint_time).toBeDefined()
        })
    })
})

/* Org-Intfc-POST-Collection-002 */
it('Org-Intfc-POST-Collection-002: should reject the creation of an identifier entry when said entry already exists ', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/collection')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      orgMintedUUID: org_minted_uuid,
      coreProvenance: {
        system_core: 'Monitor FDCP API-development',
        version_core: '0.0.0-xxxx',
      },
    })
    // .expect(400)
    .then((data) => {
      console.log('Org-Intfc-POST-Collection-002:' + JSON.stringify(data.body))
      expect(data.body.error.status).toBe(400)

      //FIXME strapi no longer exposing error message? (probs due to how paratooErrorHandler returns them)
      // expect(data.body.error.message).toBe(
      //   `Collection with orgMintedUUID '${org_minted_uuid}' has already been submitted.`,
      // )
    })
})

/* Org-Intfc-POST-Collection-003 */
it('Org-Intfc-POST-Collection-003: should return error when identifier is empty ', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/collection')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      orgMintedUUID: '',
      coreProvenance: {
        system_core: 'Monitor FDCP API-development',
        version_core: '0.0.0-xxxx',
      },
    })
    .expect(400)
    .then((data) => {
      expect(data.body.error.message).toBe(
        'Invalid: data/orgMintedUUID must NOT have fewer than 1 characters',
      )
    })
})

/* Org-Intfc-POST-Collection-006 */
it('Org-Intfc-POST-Collection-006: Failure to notify a collection submission when the User is not authenticated ', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/collection')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send({
      orgMintedUUID: org_minted_uuid,
      coreProvenance: {
        system_core: 'Monitor FDCP API-development',
        version_core: '0.0.0-xxxx',
      },
    })
    .expect(403)
    .then((data) => {
      expect(data.body.error.message).toBe('Forbidden')
    })
})

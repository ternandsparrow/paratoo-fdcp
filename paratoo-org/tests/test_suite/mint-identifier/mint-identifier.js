const request = require('supertest')
const mock = require('./mock.json')

var jwt = ''
var mint_identifier = ''
var user_data

it('should create a user and log in', async () => {
  const helpers = global.strapiInstance.service('api::paratoo-helper.paratoo-helper')

  // create mock user with role
  helpers.createUserWithRole(mock.userData.username,
    mock.userData.properties,
    mock.userData.role
  )
 
  //Log user in
  await request(global.strapiServer)
    .post('/api/auth/local')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send(mock.body1)
    .expect('Content-Type', /json/)
    .expect(200)
    .then((data) => {
      expect(data.body.jwt).toBeDefined()
      jwt = data.body.jwt
      user_data = data.body.user
    })
})

/* Org-Intfc-POST-MintIdentifier-001 */
it('Org-Intfc-POST-MintIdentifier-001: should fail to mint identifier when not authenticated', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/mint-identifier')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send(mock.body2)
    .expect(403)
    .then((data) => {
      expect(data.body.error.message).toBe('Forbidden')
    })
})

/* Org-Intfc-POST-MintIdentifier-002 */
it('Org-Intfc-POST-MintIdentifier-002: should successfully mint identifier when user is authenticated', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/mint-identifier')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send(mock.body2)
    .expect(200)
    .then(async (data) => {
      expect(data.body.orgMintedIdentifier).toBeDefined()
      mint_identifier = data.body.orgMintedIdentifier

      const helpers = global.strapiInstance.service(
        'api::paratoo-helper.paratoo-helper',
      )
      const decoded = JSON.parse(Buffer.from(mint_identifier, 'base64'))
      // orgMintedUUID exists
      await helpers
        .deepPopulateQuery(
          'collection-identifier',
          {
            survey_metadata: {
              orgMintedUUID: decoded.survey_metadata.orgMintedUUID,
            },
          },
          {
            survey_metadata: {
              populate: {
                survey_details: true,
                provenance: true,
              },
            },
          },
          false,
        )
        .then((data) => {
          // orgMintedUUID should be the same one
          expect(data.survey_metadata.orgMintedUUID).toBe(decoded.survey_metadata.orgMintedUUID)
          // core_submit_time should be null but org_mint_time should be defined
          expect(data.core_submit_time).toBe(null)
          expect(data.org_mint_time).toBeDefined()
        })
    })
})

/* Org-Intfc-POST-MintIdentifier-003 */
it('Org-Intfc-POST-MintIdentifier-003: should fail to mint identifier when provided with non-string projectId', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/mint-identifier')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send(mock.body3)
    .expect(400)
    .then((data) => {
      expect(data.body.error.message).toBe(
        'Invalid: data/survey_metadata/survey_details/project_id must be string',
      )
    })
})


/* Org-Intfc-POST-MintIdentifier-004 */
it('Org-Intfc-POST-MintIdentifier-004: should fail to mint identifier when provided with non-UUID protocolId', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/mint-identifier')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send(mock.body4)
    .expect(400)
    .then((data) => {
      expect(data.body.error.message).toBe(
        'Invalid: data/survey_metadata/survey_details/protocol_id must match format \"uuid\"',
      )
    })
})

/* Org-Intfc-POST-MintIdentifier-005 */
it('Org-Intfc-POST-MintIdentifier-005: should fail to mint identifier when provided with non-string protocol version', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/mint-identifier')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send(mock.body5)
    .expect(400)
    .then((data) => {
      expect(data.body.error.message).toBe(
        'Invalid: data/survey_metadata/survey_details/protocol_version must be string',
      )
    })
})

/* Org-Intfc-POST-MintIdentifier-006 */
it('Org-Intfc-POST-MintIdentifier-006: should fail to mint identifier when provided with an empty request body', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/mint-identifier')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .expect(400)
    .then((data) => {
      expect(data.body.error.message).toBe(
        "Invalid: data must have required property 'survey_metadata'",
      )
    })
})

/* Org-Intfc-POST-MintIdentifier-007 */
it('Org-Intfc-POST-MintIdentifier-007: should fail to mint identifier when provided with an invalid survey metadata (as string)', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/mint-identifier')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send(mock.body6)
    .expect(400)
    .then((data) => {
      expect(data.body.error.message).toBe(
        'Invalid: data/survey_metadata must be object',
      )
    })
})

/* Org-Intfc-POST-MintIdentifier-003 */
it('Org-Intfc-POST-MintIdentifier-008: should fail to mint identifier when provided with invalid ip address', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/mint-identifier')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send(mock.body7)
    .expect(400)
    .then((data) => {
      expect(data.body.error.message).toBe(
        'submitter_ip_address \'xxx\' must be a valid ip address',
      )
    })
})
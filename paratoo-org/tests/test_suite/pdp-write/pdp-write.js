const request = require('supertest')
const mock = require('./mock.json')

var jwt = ''
var user_data
it('Log in user to test pdp/<projectid>/<protocolId/write', async () => {
  const helpers = global.strapiInstance.service('api::paratoo-helper.paratoo-helper')

  // create mock user with role
  helpers.createUserWithRole(mock.userData.username,
    mock.userData.properties,
    mock.userData.role
  )
  
  //Log user in
  await request(global.strapiServer)
    .post('/api/auth/local')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send(mock.body1)
    .expect('Content-Type', /json/)
    .expect(200)
    .then((data) => {
      expect(data.body.jwt).toBeDefined()
      jwt = data.body.jwt
      user_data = data.body.user
    })
})

/* Org-Intfc-GET-PDP-write-001 */
it('Org-Intfc-GET-PDP-write-001: should fail to authorize write permissions using the PDP when user is not logged in ', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/pdp/1/2/write')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .expect(403)
})

/* Org-Intfc-GET-PDP-write-002 */
it('Org-Intfc-GET-PDP-write-002: should successfully authorize write permissions using the PDP when user is logged in and NOT assigned to the project ', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/pdp/3/068d17e8-e042-ae42-1e42-cff4006e64b0/write')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .then((data) => {
      console.log('Org-Intfc-GET-PDP-write-002 data.body:', data.body)
      expect(data.status).toBe(200)
      expect(data.body.isAuthorised).toBe(false)
    })
})


var jwt_003 = ''
var user_data_003 = ''
it('Org-Intfc-GET-PDP-write-003 - Creating user details', async () => {
  const helpers = global.strapiInstance.service('api::paratoo-helper.paratoo-helper')

  // create mock user with role
  helpers.createUserWithRole(mock.userData2.username,
    mock.userData2.properties,
    mock.userData2.role
  )

  //Log user in
  await request(global.strapiServer)
    .post('/api/auth/local')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send(mock.body2)
    .expect('Content-Type', /json/)
    .expect(200)
    .then((data) => {
      expect(data.body.jwt).toBeDefined()
      jwt_003 = data.body.jwt
      user_data_003 = data.body.user
    })

  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/user-projects')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt_003)
    .expect(200)
    .then((data) => {
      expect(data.body.projects[0].id == mock.userData2.properties.projects)
    })
})

// FIXME - add this text back in, whwn the PolicyEnforcer is updated.  It needed to be loosened for Drones image upload vio API-Key
/* Org-Intfc-GET-PDP-write-003a */
// it('Org-Intfc-GET-PDP-write-003a: Failure to authorize write permissions using the PDP when the protocol does not have write permissions ', async () => {
//   await request(global.strapiServer) // app server is an instance of Class: http.Server
//     .get('/api/org/pdp/1/3/write')
//     .set('accept', 'application/json')
//     .set('Content-Type', 'application/json')
//     .set('Authorization', 'Bearer ' + jwt_003)
//     .expect(200)
//     .then((data) => {
//       expect(data.body.isAuthorised).toBe(false)
//     })
// })

// FIXME - Finish this test (API-key)
/* Org-Intfc-GET-PDP-write-003b */
// it('Org-Intfc-GET-PDP-write-003b: Failure to authorize write permissions using the PDP when the protocol does not have write permissions - API-KEY mode', async () => {
//   await request(global.strapiServer) // app server is an instance of Class: http.Server
//     .get('/api/org/pdp/1/3/write')
//     .set('accept', 'application/json')
//     .set('Content-Type', 'application/json')
//     .set('Authorization', 'Bearer ' + jwt_003)
//     .expect(200)
//     .then((data) => {
//       expect(data.body.isAuthorised).toBe(false)
//     })
// })

/* Org-Intfc-GET-PDP-write-004 */
it('Org-Intfc-GET-PDP-write-004: should successfully authorize write permissions using the PDP when user is logged in and assigned to the project ', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/pdp/1/c1b38b0f-a888-4f28-871b-83da2ac1e533/write')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .expect(200)
    .then((data) => {
      expect(data.body.isAuthorised).toBe(true)
    })
})

/* Org-Intfc-GET-PDP-write-005 - 1 */
it('Org-Intfc-GET-PDP-write-005 - 1: should fail to authorize write permissions when projectID is not an integer - input string foo ', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/pdp/foo/c1b38b0f-a888-4f28-871b-83da2ac1e533/write')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .expect(400)
    .then((data) => {
      expect(data.body.error.message).toBe(
        `Invalid: parameter 'projectId' with value 'foo' data must match format \"project_id\"`,
      )
    })
})

/* Org-Intfc-GET-PDP-write-005 - 2 */
it('Org-Intfc-GET-PDP-write-005 - 2 : should fail to authorize write permissions when projectID is not an integer input 1.5 ', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/pdp/1.5/c1b38b0f-a888-4f28-871b-83da2ac1e533/write')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .then((data) => {
      console.log('Org-Intfc-GET-PDP-write-005 - 2 data.body:', data.body)
      expect(data.status).toBe(400)
      expect(data.body.error.message).toBe(
        `Invalid: parameter 'projectId' with value '1.5' data must match format \"project_id\"`,
      )
    })
})

/* Org-Intfc-GET-PDP-write-006 */
it('Org-Intfc-GET-PDP-write-006: should fail when protocolId is not a valid UUID', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/pdp/1/foo/write')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .then((data) => {
      console.log('Org-Intfc-GET-PDP-write-006 data.body:', data.body)
      expect(data.status).toBe(400)
      expect(data.body.error.message).toBe(
        "Invalid: parameter 'protocolUuid' with value 'foo' data must match format \"uuid\"",
      )
    })
})

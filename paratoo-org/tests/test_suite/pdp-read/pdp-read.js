const request = require('supertest')
const mock = require('./mock.json')

var jwt = ''
var user_data

it('Log in user to test pdp/<projectid>/<protocolId/read', async () => {
  const helpers = global.strapiInstance.service('api::paratoo-helper.paratoo-helper')

  // create mock user with role
  helpers.createUserWithRole(mock.userData.username,
    mock.userData.properties,
    mock.userData.role
  )

  //Log user in
  await request(global.strapiServer)
    .post('/api/auth/local')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send(mock.body1)
    .expect('Content-Type', /json/)
    .expect(200)
    .then((data) => {
      expect(data.body.jwt).toBeDefined()
      jwt = data.body.jwt
      user_data = data.body.user
    })
})

/* Org-Intfc-GET-PDP-read-001 */
it('Org-Intfc-GET-PDP-read-001: should fail to authorize read permissions when user is NOT authenticated', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/pdp/1/c1b38b0f-a888-4f28-871b-83da2ac1e533/read')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .expect(403)
})

/* Org-Intfc-GET-PDP-read-002 */
it('Org-Intfc-GET-PDP-read-002: should fail to authorize read permissions using the PDP when user is NOT assigned to that project ', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/pdp/2/c1b38b0f-a888-4f28-871b-83da2ac1e533/read')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .expect(200)
    .then((data) => {
      expect(data.body.isAuthorised).toBe(false)
    })
})

/* Org-Intfc-GET-PDP-read-003 */
it('Org-Intfc-GET-PDP-read-003: should successfully authorize read permissions using the PDP -(user logged in and assigned to the project) ', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/pdp/1/c1b38b0f-a888-4f28-871b-83da2ac1e533/read')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .expect(200)
    .then((data) => {
      expect(data.body.isAuthorised).toBe(true)
    })
})

/* Org-Intfc-GET-PDP-read-004 */
it('Org-Intfc-GET-PDP-read-004: should fail to authorize read permissions using the PDP when the project ID is not an integer - input is a string ', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/pdp/hithere/c1b38b0f-a888-4f28-871b-83da2ac1e533/read')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .expect(400)
    .then((data) => {
      expect(data.body.error.message).toBe(
        `Invalid: parameter 'projectId' with value 'hithere' data must match format \"project_id\"`,
      )
    })
})

/* Org-Intfc-GET-PDP-read-006 */
it('Org-Intfc-GET-PDP-read-006: Failure to authorize read permissions using the PDP when the protocol does not exist', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/pdp/1/37dd8471-8a22-49c5-b4e1-048c24e6bffd/read')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .expect(200)
    .then((data) => {
      expect(data.body.message).toBe('Protocol with UUID 37dd8471-8a22-49c5-b4e1-048c24e6bffd does not exist')
      expect(data.body.isAuthorised).toBe(false)
    })
})

/* Org-Intfc-GET-PDP-read-007 */
it('Org-Intfc-GET-PDP-read-007: Failure to authorize read permissions using the PDP when the project does not exist', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/pdp/100/c1b38b0f-a888-4f28-871b-83da2ac1e533/read')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .expect(200)
    .then((data) => {
      expect(data.body.message).toBe('Project 100 does not exist')
      expect(data.body.isAuthorised).toBe(false)
    })
})

/* Org-Intfc-GET-PDP-read-008 */
it('Org-Intfc-GET-PDP-read-008: Failure to authorize read permissions using the PDP when protocol is not assigned to the project', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/pdp/2/39da41f1-dd45-4838-ae57-ea50588fd2bc/read')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .expect(200)
    .then((data) => {
      expect(data.body.message).toBe(
        'Could not find protocol 29 assigned to project 2',
      )
      expect(data.body.isAuthorised).toBe(false)
    })
})

const request = require('supertest')
const mock = require('./mock.json')
const uuid = require('uuid')

var jwt = ''
var user_data
var org_minted_uuid
it('Log in to test for GET status', async () => {
  const helpers = global.strapiInstance.service('api::paratoo-helper.paratoo-helper')

  // create mock user with role
  helpers.createUserWithRole(mock.userData.username,
    mock.userData.properties,
    mock.userData.role
  )

  //Log user in
  await request(global.strapiServer)
    .post('/api/auth/local')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send(mock.body1)
    .expect('Content-Type', /json/)
    .expect(200)
    .then((data) => {
      expect(data.body.jwt).toBeDefined()
      jwt = data.body.jwt
      user_data = data.body.user
    })
})

//no /mint-identifier, so no stored `orgMintedUUID`, resulting in 404
it('Org-Intfc-GET-Status-000: reject checking collection status when no orgMintedUUID', async () => {
  const randUuid = uuid.v4()
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get(`/api/org/status/${randUuid}`)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send()
    .expect(404)
    .then(async (data) => {
      // expect(data.body.success).toBe(true)
      console.log('Org-Intfc-GET-Status-000 data.body:', data.body)
      expect(data.body.error.message).toBe(
        `No data set found with mintedCollectionId=${randUuid}. Unable to check the collection submission status`
      )
    })
})

//Post mint identifier
//sets up a minted identifier for subsequent tests (subsequent /status calls will return
//`isSubmitted: false`)
it('Mint Identifier for GET status (is not submit)', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/mint-identifier')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send(mock.body2)
    .expect(200)
    .then(async (data) => {
      expect(data.body.orgMintedIdentifier).toBeDefined()
      const decodedIdentifier = JSON.parse(Buffer.from(data.body.orgMintedIdentifier, 'base64'))
      console.log('decodedIdentifier:', JSON.stringify(decodedIdentifier, null, 2))
      org_minted_uuid = decodedIdentifier.survey_metadata.orgMintedUUID
      console.log('org_minted_uuid:', org_minted_uuid)
      const helpers = global.strapiInstance.service(
        'api::paratoo-helper.paratoo-helper',
      )
      // orgMintedUUID exists
      await helpers
        .deepPopulateQuery(
          'collection-identifier',
          {
            survey_metadata: {
              orgMintedUUID: org_minted_uuid,
            },
          },
          {
            survey_metadata: {
              populate: {
                survey_details: true,
                provenance: true,
              },
            },
          },
          false,
        )
        .then((data) => {
          // core_submit_time should be null but org_mint_time should be defined
          expect(data.core_submit_time).toBe(null)
          expect(data.org_mint_time).toBeDefined()
        })
    })
})

it('Org-Intfc-GET-Status-001: check collection status for collection that has not been submit', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get(`/api/org/status/${org_minted_uuid}`)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send()
    .expect(200)
    .then(async (data) => {
      // expect(data.body.success).toBe(true)
      console.log('Org-Intfc-GET-Status-001 data.body:', data.body)
      expect(data.body.isSubmitted).toBe(false)
    })
})

//hitting /collection will notify or of submission, so subsequent /status calls will
//return `isSubmitted: true`
it('Notify Org of submission for GET status (is submit)', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .post('/api/org/collection')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      orgMintedUUID: org_minted_uuid,
      coreProvenance: {
        system_core: 'Monitor FDCP API-development',
        version_core: '0.0.0-xxxx',
      },
    })
    .expect(200)
    .then(async (data) => {
      expect(data.body.success).toBe(true)
      const helpers = global.strapiInstance.service(
        'api::paratoo-helper.paratoo-helper',
      )
      // orgMintedUUID exists
      await helpers
        .deepPopulateQuery(
          'collection-identifier',
          {
            survey_metadata: {
              orgMintedUUID: org_minted_uuid,
            },
          },
          {
            survey_metadata: {
              populate: {
                survey_details: true,
                provenance: true,
              },
            },
          },
          false,
        )
        .then((data) => {
          // both core_submit_time and org_mint_time should be defined
          expect(data.core_submit_time).toBeDefined()
          expect(data.org_mint_time).toBeDefined()
        })
    })
})

it('Org-Intfc-GET-Status-002: check collection status for collection that has not been submit', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get(`/api/org/status/${org_minted_uuid}`)
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send()
    .expect(200)
    .then(async (data) => {
      // expect(data.body.success).toBe(true)
      console.log('Org-Intfc-GET-Status-002 data.body:', data.body)
      expect(data.body.isSubmitted).toBe(true)
    })
})
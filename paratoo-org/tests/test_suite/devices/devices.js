const request = require('supertest')
const mock = require('./mock.json')
import { v4 as uuidv4 } from 'uuid'

var jwt = ''
var user_data

/* Challenge generator (copy from org-interface.js generateLoginChallenge) */
function generateChallenge() {
  var challenge = ''
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  for (var i = 0; i < 16; i++) {
    challenge += characters.charAt(
      Math.floor(Math.random() * characters.length),
    )
  }
  return challenge
}

it('Log in for POST /devices', async () => {
  const helpers = global.strapiInstance.service('api::paratoo-helper.paratoo-helper')

  // create mock user with role
  helpers.createUserWithRole(mock.userData.username,
    mock.userData.properties,
    mock.userData.role
  )
  
  //Log user in
  await request(global.strapiServer)
    .post('/api/auth/local')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send(mock.body1)
    .expect('Content-Type', /json/)
    .expect(200)
    .then((data) => {
      expect(data.body.jwt).toBeDefined()
      jwt = data.body.jwt
      user_data = data.body.user
    })
})

/* Device-POST-001*/
it('Device-POST-001: Successful POST /devices', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      params: {
        longLivedCookie: 'a7e8b0e8-883c-4b52-b293-743c70435f78',
        challenge: 'MSz8EzSI33BIA3fS',
        publicKey: {
          type: 'buffer',
          data: [],
        },
        credentialId: {
          type: 'buffer',
          data: [],
        },
        clientDataJSON: {
          type: 'webauthn.create',
          challenge: 'QzE4ZkZRY2Z1b1l6RGZnRw',
          origin: 'http://localhost:8080',
          crossOrigin: false,
        },
        user: 1,
      },
    })
    .expect('Content-Type', /json/)
    .expect(200)
    .then((data) => {
      expect(data.body.success).toBe(true)
    })
})

/* Device-POST-002*/
it('Device-POST-001: Device already exists', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      params: {
        longLivedCookie: 'a7e8b0e8-883c-4b52-b293-743c70435f78',
        challenge: 'MSz8EzSI33BIA3fS',
        publicKey: {
          type: 'buffer',
          data: [],
        },
        credentialId: {
          type: 'buffer',
          data: [],
        },
        clientDataJSON: {
          type: 'webauthn.create',
          challenge: 'QzE4ZkZRY2Z1b1l6RGZnRw',
          origin: 'http://localhost:8080',
          crossOrigin: false,
        },
        user: 1,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.message).toBe(
        'Duplicate entry. This device is already registered for this user',
      )
    })
})

/* Device-POST-003 */
it('Device-POST-003: Fail because user is not authenticated', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send({
      params: {
        longLivedCookie: uuidv4(),
        challenge: generateChallenge(),
        publicKey: {
          type: 'buffer',
          data: [],
        },
        credentialId: {
          type: 'buffer',
          data: [],
        },
        clientDataJSON: {
          type: 'webauthn.create',
          challenge: generateChallenge(),
          origin: 'http://localhost:8080',
          crossOrigin: false,
        },
        user: 1,
      },
    })
    .expect('Content-Type', /json/)
    .expect(403)
    .then((data) => {
      expect(data.body.message).toBe('Forbidden')
    })
})

/* Device-POST-004 */
it('Device-POST-004: Should fail because UUID is empty', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      params: {
        longLivedCookie: '',
        challenge: generateChallenge(),
        publicKey: {
          type: 'buffer',
          data: [],
        },
        credentialId: {
          type: 'buffer',
          data: [],
        },
        clientDataJSON: {
          type: 'webauthn.create',
          challenge: generateChallenge(),
          origin: 'http://localhost:8080',
          crossOrigin: false,
        },
        user: 1,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.message).toBe(
        'Invalid: data/params/longLivedCookie must match format "uuid"',
      )
    })
})

/* Device-POST-005 */
it('Device-POST-005: Should fail because provided challenge is empty', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      params: {
        longLivedCookie: uuidv4(),
        challenge: '',
        publicKey: {
          type: 'buffer',
          data: [],
        },
        credentialId: {
          type: 'buffer',
          data: [],
        },
        clientDataJSON: {
          type: 'webauthn.create',
          challenge: generateChallenge(),
          origin: 'http://localhost:8080',
          crossOrigin: false,
        },
        user: 1,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.message).toBe(
        'Invalid: data/params/challenge must match pattern "[A-Za-z0-9]{16}"',
      )
    })
})

/* Device-POST-006-1 */
it('Device-POST-006: Should fail because provided public key is not valid - no type', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      params: {
        longLivedCookie: uuidv4(),
        challenge: generateChallenge(),
        publicKey: {
          data: [],
        },
        credentialId: {
          type: 'buffer',
          data: [],
        },
        clientDataJSON: {
          type: 'webauthn.create',
          challenge: generateChallenge(),
          origin: 'http://localhost:8080',
          crossOrigin: false,
        },
        user: 1,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.message).toBe(
        "Invalid: data/params/publicKey must have required property 'type'",
      )
    })
})

/* Device-POST-006-2*/
it('Device-POST-006: Should fail because provided public key is not valid - no data', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      params: {
        longLivedCookie: uuidv4(),
        challenge: generateChallenge(),
        publicKey: {
          type: 'buffer',
        },
        credentialId: {
          type: 'buffer',
          data: [],
        },
        clientDataJSON: {
          type: 'webauthn.create',
          challenge: generateChallenge(),
          origin: 'http://localhost:8080',
          crossOrigin: false,
        },
        user: 1,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.message).toBe(
        "Invalid: data/params/publicKey must have required property 'data'",
      )
    })
})

/* Device-POST-007*/
it('Device-POST-007: Should fail because provided credential ID is not valid - no type', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      params: {
        longLivedCookie: uuidv4(),
        challenge: generateChallenge(),
        publicKey: {
          type: 'buffer',
          data: [],
        },
        credentialId: {
          data: [],
        },
        clientDataJSON: {
          type: 'webauthn.create',
          challenge: generateChallenge(),
          origin: 'http://localhost:8080',
          crossOrigin: false,
        },
        user: 1,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.message).toBe(
        "Invalid: data/params/credentialId must have required property 'type'",
      )
    })
})

/* Device-POST-007*/
it('Device-POST-007: Should fail because provided credential ID is not valid - no data', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      params: {
        longLivedCookie: uuidv4(),
        challenge: generateChallenge(),
        publicKey: {
          type: 'buffer',
          data: [],
        },
        credentialId: {
          type: 'buffer',
        },
        clientDataJSON: {
          type: 'webauthn.create',
          challenge: generateChallenge(),
          origin: 'http://localhost:8080',
          crossOrigin: false,
        },
        user: 1,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.message).toBe(
        "Invalid: data/params/credentialId must have required property 'data'",
      )
    })
})

/* Device-POST-008*/
it('Device-POST-008: Should fail because provided client data JSON is not valid - no type', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      params: {
        longLivedCookie: uuidv4(),
        challenge: generateChallenge(),
        publicKey: {
          type: 'buffer',
          data: [],
        },
        credentialId: {
          type: 'buffer',
          data: [],
        },
        clientDataJSON: {
          challenge: generateChallenge(),
          origin: 'http://localhost:8080',
          crossOrigin: false,
        },
        user: 1,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.message).toBe(
        "Invalid: data/params/clientDataJSON must have required property 'type'",
      )
    })
})

/* Device-POST-008*/
it('Device-POST-008: Should fail because provided client data JSON is not valid - no challenge', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      params: {
        longLivedCookie: uuidv4(),
        challenge: generateChallenge(),
        publicKey: {
          type: 'buffer',
          data: [],
        },
        credentialId: {
          type: 'buffer',
          data: [],
        },
        clientDataJSON: {
          type: 'webauthn.create',
          origin: 'http://localhost:8080',
          crossOrigin: false,
        },
        user: 1,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.message).toBe(
        "Invalid: data/params/clientDataJSON must have required property 'challenge'",
      )
    })
})

/* Device-POST-008*/
it('Device-POST-008: Should fail because provided client data JSON is not valid - no origin', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      params: {
        longLivedCookie: uuidv4(),
        challenge: generateChallenge(),
        publicKey: {
          type: 'buffer',
          data: [],
        },
        credentialId: {
          type: 'buffer',
          data: [],
        },
        clientDataJSON: {
          type: 'webauthn.create',
          challenge: generateChallenge(),
          crossOrigin: false,
        },
        user: 1,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.message).toBe(
        "Invalid: data/params/clientDataJSON must have required property 'origin'",
      )
    })
})

/* Device-POST-009*/
it('Device-POST-009: Should fail because provided user ID is not an integer', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      params: {
        longLivedCookie: uuidv4(),
        challenge: generateChallenge(),
        publicKey: {
          type: 'buffer',
          data: [],
        },
        credentialId: {
          type: 'buffer',
          data: [],
        },
        clientDataJSON: {
          type: 'webauthn.create',
          challenge: generateChallenge(),
          origin: 'http://localhost:8080',
          crossOrigin: false,
        },
        user: 'foo',
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.message).toBe(
        'Invalid: data/params/user must be integer',
      )
    })
})

/* Device-POST-010*/
it('Device-POST-010: Should fail because no longLivedCookie passed to the params object', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      params: {
        challenge: generateChallenge(),
        publicKey: {
          type: 'buffer',
          data: [],
        },
        credentialId: {
          type: 'buffer',
          data: [],
        },
        clientDataJSON: {
          type: 'webauthn.create',
          challenge: generateChallenge(),
          origin: 'http://localhost:8080',
          crossOrigin: false,
        },
        user: 1,
      },
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.message).toBe(
        "Invalid: data/params must have required property 'longLivedCookie'",
      )
    })
})

/* Device-POST-011*/
it('Device-POST-011: Should fail because no parameters are passed to the params object', async () => {
  await request(global.strapiServer)
    .post('/api/devices')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .send({
      longLivedCookie: uuidv4(),
      challenge: generateChallenge(),
      publicKey: {
        type: 'buffer',
        data: [],
      },
      credentialId: {
        type: 'buffer',
        data: [],
      },
      clientDataJSON: {
        type: 'webauthn.create',
        challenge: generateChallenge(),
        origin: 'http://localhost:8080',
        crossOrigin: false,
      },
      user: 1,
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.message).toBe(
        "Invalid: data must have required property 'params'",
      )
    })
})

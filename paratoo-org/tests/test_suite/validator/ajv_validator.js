import * as cc from '../../helpers/constants'
const mock = require('./mock.json')
const {
  generateInvalidMockData,
  sendMockData,
  loginAndGenerateJwt,
  createMock,
} = require('../../helpers/mocker')

var API_PATHS = null
var JWT = ''

it('Ajv-Validator-Test-001: Should successfully POST data with correct values', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  // create mock user with role
  helpers.createUserWithRole(mock.userData1.username,
    mock.userData1.properties,
    mock.userData1.role
  )
  // Log user in
  JWT = await loginAndGenerateJwt(
    global.strapiServer,
    mock.userData1.properties.email,
    mock.userData1.properties.password,
  )

  const documentation = await helpers.readFullDocumentation()
  API_PATHS = documentation['paths']
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    const apiMock = createMock(API_PATHS, api)
    await sendMockData(
      `Ajv-Validator-Test-001-${api}`,
      global.strapiServer,
      apiMock.endpoint,
      apiMock.mockData,
      200,
      null,
      null,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-002: Test the ability of ajv validator to reject requests to POST data with invalid string', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()

    const apiMock = createMock(API_PATHS, api)
    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.STRING,
    )
    // if no string type field found
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-002-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be string`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-003: Test the ability of ajv validator to reject requests to POST data with invalid invalid integer', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()

    const apiMock = createMock(API_PATHS, api)
    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.INTEGER,
    )
    // if no string type field found
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-003-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be string`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-004: Test the ability of ajv validator to reject requests to POST data with invalid invalid boolean', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()

    const apiMock = createMock(API_PATHS, api)
    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.BOOLEAN,
    )
    // if no string type field found
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-004-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be string`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-005: Test the ability of ajv validator to reject requests to POST data with invalid invalid enum', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()

    const apiMock = createMock(API_PATHS, api)
    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.STRING,
      null,
      cc.PROPERTY_TYPE.ENUM,
    )
    // if no string type field found
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-005-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be string`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-006: Test the ability of ajv validator to reject requests to POST data with invalid invalid float', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()

    const apiMock = createMock(API_PATHS, api)
    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
    )
    // if no string type field found
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-006-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be string`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-007: Test the ability of ajv validator to reject requests to POST data with invalid model reference', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()

    const apiMock = createMock(API_PATHS, api)
    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.INTEGER,
      null,
      null,
      'x-model-ref',
    )
    // if no string type field found
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-007-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be integer`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-008: Test the ability of ajv validator to reject requests to POST data with missing required integer field', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()

    const apiMock = createMock(API_PATHS, api)
    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.INTEGER,
      null,
      null,
      null,
      true,
    )
    // if no string type field found
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-008-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `must have required property '${invalidMock.fieldData.field}'`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-009: Test the ability of ajv validator to reject requests to POST data with missing required float field', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()

    const apiMock = createMock(API_PATHS, api)
    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
      null,
      null,
      true,
    )
    // if no string type field found
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-009-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `must have required property '${invalidMock.fieldData.field}'`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-010: Test the ability of ajv validator to reject requests to POST data with missing required string field', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)
    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.STRING,
      null,
      null,
      null,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-010-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `must have required property '${invalidMock.fieldData.field}'`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-011: Test the ability of ajv validator to reject requests to POST data with missing required boolean field', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)
    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.BOOLEAN,
      null,
      null,
      null,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-011-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `must have required property '${invalidMock.fieldData.field}'`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-012: Test the ability of ajv validator to reject requests to POST data with missing required string with date-time format', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.STRING,
      cc.FORMAT.DATE_TIME,
      null,
      null,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-012-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `must have required property '${invalidMock.fieldData.field}'`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-013: Test the ability of ajv validator to reject requests to POST data when a float number is less then its minimum', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
      cc.PROPERTY_TYPE.MINIMUM,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-013-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be >= ${invalidMock.fieldData.propertyValue}`,
      JWT,
    )
  }
})
it('Ajv-Validator-Test-014: Test the ability of ajv validator to reject requests to POST data when a float number is greater then its maximum', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
      cc.PROPERTY_TYPE.MAXIMUM,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-014-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be <= ${invalidMock.fieldData.propertyValue}`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-015: Test the ability of ajv validator to reject requests to POST data when an integer is less then its minimum', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.INTEGER,
      null,
      cc.PROPERTY_TYPE.MINIMUM,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-015-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be >= ${invalidMock.fieldData.propertyValue}`,
      JWT,
    )
  }
})
it('Ajv-Validator-Test-016: Test the ability of ajv validator to reject requests to POST data when an integer is greater then its maximum', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.INTEGER,
      null,
      cc.PROPERTY_TYPE.MAXIMUM,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-016-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be <= ${invalidMock.fieldData.propertyValue}`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-017: Test the ability of ajv validator to reject requests to POST data with same unique value', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    // as most of the unique fields are required e.g. voucher_barcode
    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.STRING,
      null,
      cc.PROPERTY_TYPE.UNIQUE,
      null,
      true,
    )
    if (!invalidMock) continue

    const field = prettyFormatFieldName(invalidMock.fieldData.field)
    await sendMockData(
      `Ajv-Validator-Test-017-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `This attribute must be unique. The following observation field must be unique: ${field}`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-018: Test the ability of ajv validator to reject requests to POST data when a string is greater then maxLength', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.STRING,
      null,
      cc.PROPERTY_TYPE.MAXLENGTH,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-018-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must NOT have more than ${invalidMock.fieldData.propertyValue} characters`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-019: Test the ability of ajv validator to reject requests to POST data with an invalid component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.NON_REPEATABLE_COMPONENT,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-019-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be object`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-020: Test the ability of ajv validator to reject requests to POST data with an invalid repeatable component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.REPEATABLE_COMPONENT,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-020-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be array`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-021: Test the ability of ajv validator to reject requests to POST data when number of components is greater then maxItems', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.REPEATABLE_COMPONENT,
      null,
      cc.PROPERTY_TYPE.MAXITEMS,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-021-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must NOT have more than ${invalidMock.fieldData.propertyValue} items`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-022: Test the ability of ajv validator to reject requests to POST data with invalid enum inside a component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.STRING,
      null,
      cc.PROPERTY_TYPE.ENUM,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-022-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be equal to one of the allowed values`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-023: Test the ability of ajv validator to reject requests to POST data with missing required enum inside a component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.STRING,
      null,
      cc.PROPERTY_TYPE.ENUM,
      null,
      true,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-023-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `must have required property '${invalidMock.fieldData.field}`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-024: Test the ability of ajv validator to reject requests to POST data with invalid string inside component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)
    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.STRING,
      null,
      null,
      null,
      false,
      true,
    )
    // if no string type field found
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-024-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be string`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-025: Test the ability of ajv validator to reject requests to POST data with invalid integer inside component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.INTEGER,
      null,
      null,
      null,
      false,
      true,
    )
    // if no integer type field found
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-025-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be integer`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-026: Test the ability of ajv validator to reject requests to POST data with invalid boolean inside component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.BOOLEAN,
      null,
      null,
      null,
      false,
      true,
    )
    // if no boolean type field found
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-026-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be boolean`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-027: Test the ability of ajv validator to reject requests to POST data with invalid float inside component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
      null,
      null,
      false,
      true,
    )
    // if no float field found
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-027-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be number`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-028: Test the ability of ajv validator to reject requests to POST data with missing required integer field inside component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.INTEGER,
      null,
      null,
      null,
      true,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-028-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `must have required property '${invalidMock.fieldData.field}'`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-029: Test the ability of ajv validator to reject requests to POST data with missing required float field inside component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
      null,
      null,
      true,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-029-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `must have required property '${invalidMock.fieldData.field}'`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-030: Test the ability of ajv validator to reject requests to POST data with missing required string field inside component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.STRING,
      null,
      null,
      null,
      true,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-030-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `must have required property '${invalidMock.fieldData.field}'`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-031: Test the ability of ajv validator to reject requests to POST data with missing required boolean field inside component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.BOOLEAN,
      null,
      null,
      null,
      true,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-031-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `must have required property '${invalidMock.fieldData.field}'`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-032: Test the ability of ajv validator to reject requests to POST data when a float number is less then its minimum inside component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
      cc.PROPERTY_TYPE.MINIMUM,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-032-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be >= ${invalidMock.fieldData.propertyValue}`,
      JWT,
    )
  }
})
it('Ajv-Validator-Test-033: Test the ability of ajv validator to reject requests to POST data when a float number is greater then its maximum', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.NUMBER,
      cc.FORMAT.FLOAT,
      cc.PROPERTY_TYPE.MAXIMUM,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-033-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be <= ${invalidMock.fieldData.propertyValue}`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-034: Test the ability of ajv validator to reject requests to POST data when an integer is less then its minimum inside component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.INTEGER,
      null,
      cc.PROPERTY_TYPE.MINIMUM,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-034-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be >= ${invalidMock.fieldData.propertyValue}`,
      JWT,
    )
  }
})
it('Ajv-Validator-Test-035: Test the ability of ajv validator to reject requests to POST data when an integer is greater then its maximum inside component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.INTEGER,
      null,
      cc.PROPERTY_TYPE.MAXIMUM,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-035-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be <= ${invalidMock.fieldData.propertyValue}`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-036: Test the ability of ajv validator to reject requests to POST data with same unique value inside component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    // as most of the unique fields are required e.g. voucher_barcode
    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.STRING,
      null,
      cc.PROPERTY_TYPE.UNIQUE,
      null,
      true,
      true,
    )
    if (!invalidMock) continue

    const field = prettyFormatFieldName(invalidMock.fieldData.field)
    await sendMockData(
      `Ajv-Validator-Test-036-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `This attribute must be unique. The following observation field must be unique: ${field}`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-037: Test the ability of ajv validator to reject requests to POST data when a string in component is greater then maxLength', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.STRING,
      null,
      cc.PROPERTY_TYPE.MAXLENGTH,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-037-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must NOT have more than ${invalidMock.fieldData.propertyValue} characters`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-038: Test the ability of ajv validator to reject requests to POST data with an invalid nested component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.NON_REPEATABLE_COMPONENT,
      null,
      null,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-038-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be object`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-039: Test the ability of ajv validator to reject requests to POST data with an invalid repeatable nested component', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.REPEATABLE_COMPONENT,
      null,
      null,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-039-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must be array`,
      JWT,
    )
  }
})

it('Ajv-Validator-Test-040: Test the ability of ajv validator to reject requests to POST data when number of components is greater then maxItems', async () => {
  for (const api of cc.AJV_VALIDATOR_TEST_ROUTES) {
    expect(API_PATHS).toBeDefined()
    const apiMock = createMock(API_PATHS, api)

    const invalidMock = await generateInvalidMockData(
      api,
      apiMock.mockData,
      apiMock.schema,
      cc.FIELD_TYPE.REPEATABLE_COMPONENT,
      null,
      cc.PROPERTY_TYPE.MAXITEMS,
      null,
      false,
      true,
    )
    if (!invalidMock) continue

    await sendMockData(
      `Ajv-Validator-Test-040-${api}`,
      global.strapiServer,
      `/api${api}`,
      invalidMock.data,
      400,
      null,
      `${invalidMock.fieldData.field} must NOT have more than ${invalidMock.fieldData.propertyValue} items`,
      JWT,
    )
  }
})

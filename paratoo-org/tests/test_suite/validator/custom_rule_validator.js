
import * as cc from '../../helpers/constants'
const mock = require('./mock.json')
const {
  sendMockData,
  loginAndGenerateJwt,
  createMock,
  generateInvalidMockDataUsingRule,
} = require('../../helpers/mocker')

// Temporary cache so that we don't need to generate every time
const API_SCHEMA = {}
var JWT = ''
it('Custom-Rules-Validator-Test-001: Should successfully post/get requests', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  // create mock user with role
  helpers.createUserWithRole(mock.userData2.username,
    mock.userData2.properties,
    mock.userData2.role
  )
  // Log user in
  JWT = await loginAndGenerateJwt(
    global.strapiServer,
    mock.userData2.properties.email,
    mock.userData2.properties.password,
  )
  const documentation = await helpers.readFullDocumentation()
  for (const api of cc.CUSTOM_RULE_VALIDATOR_TEST_ROUTES) {
    const apiMock = createMock(documentation.paths, api)
    await sendMockData(
      `Custom-Rules-Validator-Test-001-${api}`,
      global.strapiServer,
      apiMock.endpoint,
      apiMock.mockData,
      api.includes('status')
        ? 404
        : 200,
      null,
      null,
      JWT,
      apiMock.method,
    )
  }
})

it('Custom-Rules-Validator-Test-002: Test the ability of validator to reject requests to GET/POST collections which fail to satisfy rule uuidIdentifierCheck', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const documentation = await helpers.readFullDocumentation()
  for (const api of cc.CUSTOM_RULE_VALIDATOR_TEST_ROUTES) {
    let apiMock = createMock(documentation.paths, api)
    apiMock = generateInvalidMockDataUsingRule(
      api,
      apiMock,
      cc.CUSTOM_RULE_TYPES.uuidIdentifierCheck,
    )
    if (!apiMock) continue

    await sendMockData(
      `Custom-Rules-Validator-Test-002-${api}`,
      global.strapiServer,
      apiMock.endpoint,
      apiMock.mockData,
      400,
      null,
      apiMock.errorMsg,
      JWT,
      apiMock.method,
    )
  }
})

it('Custom-Rules-Validator-Test-003: Test the ability of validator to reject requests to GET/POST collections which fail to satisfy rule surveyMetadataProjectCheck', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const documentation = await helpers.readFullDocumentation()
  for (const api of cc.CUSTOM_RULE_VALIDATOR_TEST_ROUTES) {
    let apiMock = createMock(documentation.paths, api)

    apiMock = generateInvalidMockDataUsingRule(
      api,
      apiMock,
      cc.CUSTOM_RULE_TYPES.surveyMetadataProjectCheck,
    )
    if (!apiMock) continue
    await sendMockData(
      `Custom-Rules-Validator-Test-003-${api}`,
      global.strapiServer,
      apiMock.endpoint,
      apiMock.mockData,
      400,
      null,
      apiMock.errorMsg,
      JWT,
      apiMock.method,
    )
  }
})

it('Custom-Rules-Validator-Test-004: Test the ability of validator to reject requests to GET/POST collections which fail to satisfy rule surveyMetadataProtocolCheck', async () => {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const documentation = await helpers.readFullDocumentation()
  for (const api of cc.CUSTOM_RULE_VALIDATOR_TEST_ROUTES) {
    let apiMock = createMock(documentation.paths, api)

    apiMock = generateInvalidMockDataUsingRule(
      api,
      apiMock,
      cc.CUSTOM_RULE_TYPES.surveyMetadataProtocolCheck,
    )
    if (!apiMock) continue
    await sendMockData(
      `Custom-Rules-Validator-Test-004-${api}`,
      global.strapiServer,
      apiMock.endpoint,
      apiMock.mockData,
      400,
      null,
      apiMock.errorMsg,
      JWT,
      apiMock.method,
    )
  }
})

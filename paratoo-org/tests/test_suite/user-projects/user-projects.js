const request = require('supertest')
const mock = require('./mock.json')

var jwt = ''
var user_data

it('create user to test /user-projects', async () => {
  const helpers = global.strapiInstance.service('api::paratoo-helper.paratoo-helper')

  // create mock user with role
  helpers.createUserWithRole(mock.userData.username,
    mock.userData.properties,
    mock.userData.role
  )
  
  //Log user in
  await request(global.strapiServer)
    .post('/api/auth/local')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send(mock.body1)
    .expect('Content-Type', /json/)
    .expect(200)
    .then((data) => {
      expect(data.body.jwt).toBeDefined()
      jwt = data.body.jwt
      user_data = data.body.user
    })
})

/* Org-Intfc-GET-UserProj-001 */
it('Org-Intfc-GET-UserProj-001: should fail to get User projects when not authenticated', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/user-projects')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .expect(403)
})

/* Org-Intfc-GET-UserProj-002 */
it('Org-Intfc-GET-UserProj-002: should return users projects and protocols', async () => {
  await request(global.strapiServer) // app server is an instance of Class: http.Server
    .get('/api/org/user-projects')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    .expect(200)
    .then((data) => {
      // console.log(`Org-Intfc-GET-UserProj-002 data: ${JSON.stringify(data.body, null, 2)}`)
      //https://medium.com/@andrei.pfeiffer/jest-matching-objects-in-array-50fe2f4d6b98
      expect(data.body.projects[0].protocols).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            name: 'Photopoints - DSLR Panorama',
          }),
        ]),
      )
      //console.log('user projects details: ', data.text)
    })
})

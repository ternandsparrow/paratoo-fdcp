const request = require('supertest')
const mock = require('./mock.json')

let jwt = null
let refreshToken = null
let loginTime = null
let refreshTime = null

it('fail to get User projects when not authenticated', async () => {
  await request(global.strapiServer)
    .get('/api/org/user-projects')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .expect(403)
})

it('create testing user', async () => {
  const helpers = global.strapiInstance.service('api::paratoo-helper.paratoo-helper')

  // create mock user with role
  helpers.createUserWithRole(mock.userData.username,
    mock.userData.properties,
    mock.userData.role
  )
})

it('login as testing user', async () => {
  //we login even though register gets a JWT too, as registration is typically not
  //allowed without admin intervention
  await request(global.strapiServer)
    .post('/api/auth/local')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send(mock.body1)
    .expect('Content-Type', /json/)
    .expect(200)
    .then((data) => {
      console.log('login - data: ', data.body)
      expect(data.body.jwt).toBeDefined()
      expect(data.body.refreshToken).toBeDefined()
      loginTime = Date.now()
      jwt = data.body.jwt
      refreshToken = data.body.refreshToken
    })
})

it('get projects with valid JWT', async () => {
  await request(global.strapiServer)
    .get('/api/org/user-projects')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    //don't need to assert returned data, as it's enough to ensure we get a 200 resp
    .expect(200)
})

it('wait for JWT to expire', async () => {
  await new Promise((r) => setTimeout(r, 61000))
  expect((Date.now() - loginTime)).toBeGreaterThan(60000)
})

it('get projects with invalid JWT', async () => {
  await request(global.strapiServer)
    .get('/api/org/user-projects')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    //don't need to assert returned data, as it's enough to ensure we get a 401 resp
    .expect(401)
})

it('refreshes token', async () => {
  console.log('about to refresh token with refreshToken: ', refreshToken)
  await request(global.strapiServer)
    .post('/api/token/refresh')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send({
      refreshToken: refreshToken,
      forceJwtExpiry: '1m',
      forceRefreshExpiry: '2m',
    })
    .expect('Content-Type', /json/)
    .expect(200)
    .then((data) => {
      console.log('refresh - data: ', data.body)
      expect(data.body.jwt).toBeDefined()
      expect(data.body.refreshToken).toBeDefined()
      refreshTime = Date.now()
      jwt = data.body.jwt
      refreshToken = data.body.refreshToken
    })
})

it('get projects with new valid JWT', async () => {
  await request(global.strapiServer)
    .get('/api/org/user-projects')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + jwt)
    //don't need to assert returned data, as it's enough to ensure we get a 200 resp
    .expect(200)
})

it('wait for JWT and refresh token to expire', async () => {
  await new Promise((r) => setTimeout(r, 121000))
  expect((Date.now() - refreshTime)).toBeGreaterThan(120000)
})

it('refreshes token with invalid refresh token', async () => {
  await request(global.strapiServer)
    .post('/api/token/refresh')
    .set('accept', 'application/json')
    .set('Content-Type', 'application/json')
    .send({
      refreshToken: refreshToken,
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then((data) => {
      expect(data.body.error.message).toBe('Error: Invalid token.')
    })
})
### Note 1:
user-projects.js, pdp-read.js, pdp-write.js, collection.js uses mockUserData2 - which is created in user-projects.js

### Note 2: 
as of 6th September
ORG tests passed for the users with:
- 1 project, 1 protocol (default)
- 1 project, multiple protocols
- 2 projects, each have 1 protocols
- 2 projects, each have multiple protocols

### Note 3:
to change the intiailize projects and protocols, goes to:
/paratoo-fdcp/paratoo-org/api/project/models/project.settings.json
/paratoo-fdcp/paratoo-org/api/protocol/models/protocol.settings.json
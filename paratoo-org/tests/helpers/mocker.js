const request = require('supertest')
import { customRules } from '../../src/customRules'
import * as cc from './constants'
const { cloneDeep, lowerCase } = require('lodash')
const { mock } = require('mock-json-schema')
const uuid = require('uuid')

/**
 * Send post request with body.
 *
 * @param {String} testName name of the test
 * @param {Object} server server
 * @param {String} endpoint endpoint
 * @param {Object} mockData request body
 * @param {Integer} respStatus expected response status
 * @param {Object} header header object
 * @param {String} errorMessage expected error message
 * @param {String} token bearer token
 */
export async function sendMockData(
  testName,
  server,
  endpoint,
  mockData,
  respStatus,
  header = null,
  errorMessage = null,
  token = null,
  method = 'post',
  skipToken = false
) {
  if (!mockData && method == 'post') return

  header = header || cc.HEADER
  if (token) {
    header['Authorization'] = `Bearer ${token}`
  }
  if (skipToken) delete header['Authorization']

  let resp = null
  console.log(`test: ${testName} path: ${endpoint} header: ${JSON.stringify(header)}`)
  if (method == 'get') {
    resp = await request(server)
      .get(endpoint)
      .set(header)
      .then((data) => {
        console.log(`test: ${testName} path: ${endpoint} body: `, data.body)
        expect(data.status).toBe(respStatus)

        if (errorMessage) {
          expect(data.body.error.message).toContain(errorMessage)
        }

        return data
      })
    return resp
  }

  // app server is an instance of Class: http.Server
  // console.log(`test: ${testName} path: ${endpoint} `)
  resp = await request(server)
    .post(endpoint)
    .set(header)
    .send(mockData)
    .then((data) => {
      console.log(`test: ${testName} path: ${endpoint} body: `, data.body)
      expect(data.status).toBe(respStatus)

      if (errorMessage) {
        expect(data.body.error.message).toContain(errorMessage)
      }

      return data
    })
  return resp
}

/* create mock*/
export function createMock(paths, api) {
  let result = {}
  const methods = Object.keys(paths[api])
  const method = methods.includes('post') ? 'post' : 'get'
  result['endpoint'] = `/api${api}`
  result['method'] = method

  switch (method) {
    case 'post':
      result['schema'] =
        paths[api]['post']['requestBody']['content']['application/json'][
          'schema'
        ]

      result['mockData'] = updateMockData(method, mock(result.schema))
      break

    case 'get':
      result['mockData'] = null
      result['endpoint'] = updateMockData(method, null, result.endpoint)
      break
  }
  return result
}

/* generate invalid mock */
export function generateInvalidMockDataUsingRule(api, validData, ruleType) {
  if (!validData) return null
  const apiName = api
    .replace('/api/', '')
    .replace('/org/', '')
    .replace('/{identifier}', '/:identifier')

  if (!customRules[apiName]) {
    console.warn(`Rule type: ${ruleType} not found in api: ${apiName}`)
    return null
  }

  const data = cloneDeep(validData)
  data['apiName'] = apiName
  for (const rule of customRules[apiName]) {
    if (rule.ruleType != ruleType) continue
    switch (ruleType) {
      case cc.CUSTOM_RULE_TYPES.uuidIdentifierCheck:
        data.endpoint = '/api/org/status/1234' // invalid uuid
        data['errorMsg'] = 'Invalid: Invalid UUID'
        return data

      case cc.CUSTOM_RULE_TYPES.surveyMetadataProjectCheck:
        // valid string type but project does'nt exist
        data.mockData.survey_metadata.survey_details.project_id = 'xxx'
        data['errorMsg'] =
          'Invalid: Invalid Project ID or User is not authorised to write xxx'
        return data

      case cc.CUSTOM_RULE_TYPES.surveyMetadataProtocolCheck:
        // valid uuid but protocol does'nt exist
        const invalidId = uuid.v4()
        data.mockData.survey_metadata.survey_details.protocol_id = invalidId
        data[
          'errorMsg'
        ] = `Invalid: Invalid Protocol ID or User is not authorised to write ${invalidId}`
        return data
    }
  }

  console.warn(`Rule type: ${ruleType} not found in api: ${apiName}`)
  return null
}

/* login in*/
export async function loginAndGenerateJwt(server, email, password) {
  const resp = await sendMockData(
    'login',
    server,
    cc.LOGIN_API,
    {
      identifier: email,
      password: password,
    },
    200,
    null,
    null,
    null,
    null,
    true
  )
  return resp.body.jwt
}

/**
 * Create invalid request body. e.g. assign text value to an integer
 *
 * @param {Object} protocol protocol info
 * @param {Object} validMock object with valid mock data, associated models and survey model
 * @param {String} type field type e.g. string, int
 * @param {String} format format e.g. date-time, float
 * @param {String} property property e.g. minimum, unique
 * @param {String} flag format e.g. x-model-ref
 * @param {String} isRequired is required
 * @param {String} componentSearch search inside child observation
 * @returns {Object} updated data with invalid value
 */
export async function generateInvalidMockData(
  api,
  validMock,
  schema,
  type,
  format = null,
  property = null,
  flag = null,
  isRequired = false,
  componentSearch = false,
) {
  // valid data, linked models and name of the survey model are required
  if (!validMock) return

  // one field with invalid data is enough to make invalid request body
  let fieldData = getFieldName(
    type,
    schema,
    format,
    property,
    flag,
    isRequired,
    componentSearch,
    true,
  )
  // if no field with specified type found we return null
  if (!fieldData.field) {
    console.warn(`FIELD type: ${type} not found in ${api}`)
    return null
  }

  console.log(`FIELD FOUND fieldData: ${JSON.stringify(fieldData)} in ${api}`)
  // if field exists then assigns invalid value and returns
  return {
    data: assignInvalidFieldValue(validMock, fieldData),
    fieldData: fieldData,
  }
}

/*
 * assign invalid value. e.g. assign string to an integer
 */
function assignInvalidFieldValue(data, fieldData) {
  const payload = cloneDeep(data)

  console.log(`fieldData: ${JSON.stringify(fieldData)}`)
  if (!payload) return null

  if (!fieldData.componentData) {
    const currentValue = payload[fieldData.field]
    console.log(`current value: ${JSON.stringify(currentValue)}`)
    const newValue = generateInvalidValue(currentValue, fieldData)
    console.log(`incorrect value: ${JSON.stringify(newValue)}`)
    payload[fieldData.field] = newValue
    return cloneDeep(payload)
  }

  let relations = []
  // if deeply nested component
  relations = relations.concat(fieldData.componentData.componentRelations)
  relations.push(fieldData.field)
  payload[fieldData.componentData.field] = updateNestedComponent(
    payload[fieldData.componentData.field],
    relations,
    fieldData,
  )
  console.log(`incorrect value: ${JSON.stringify(payload)}`)
  return cloneDeep(payload)
}

function updateNestedComponent(component, nestedFields, fieldData) {
  if (nestedFields.length == 1) {
    const newValue = generateInvalidValue(
      component[nestedFields[0]],
      fieldData,
    )
    if (!newValue) {
      delete component[nestedFields[0]]
      return component
    }
    component[nestedFields[0]] = newValue
    return component
  }
  const parent = nestedFields.shift()
  component[parent] = updateNestedComponent(
    component[parent],
    nestedFields,
    fieldData,
  )
  return component
}

function getFieldStatus(field, property, required) {
  const fieldStatus = {
    length: 0,
    formatExists: false,
    propertyExists: false,
    flagExists: false,
    isRequired: required.includes(field),
    isComponent: property.type == 'object',
  }
  const fieldKeys = Object.keys(property)
  const possiblePropertyTypes = ['enum', 'unique']
  const possibleFormats = ['format']
  fieldKeys.forEach((key) => {
    // possible properties
    if (possiblePropertyTypes.includes(key)) fieldStatus.propertyExists = true
    if (key.includes('min')) fieldStatus.propertyExists = true
    if (key.includes('max')) fieldStatus.propertyExists = true
    // possible properties
    if (possibleFormats.includes(key)) fieldStatus.formatExists = true
    if (key.includes('x-')) fieldStatus.flagExists = true
  })
  return fieldStatus
}

function checkProperty(field, properties, fieldProperty) {
  if (!fieldProperty) return null
  const fieldKeys = Object.keys(properties[field])

  switch (fieldProperty) {
    case cc.PROPERTY_TYPE.MINLENGTH:
      return fieldKeys.includes('minLength')
    case cc.PROPERTY_TYPE.MAXLENGTH:
      return fieldKeys.includes('maxLength')
    case cc.PROPERTY_TYPE.MINITEMS:
      return fieldKeys.includes('minItems')
    case cc.PROPERTY_TYPE.MAXITEMS:
      return fieldKeys.includes('maxItems')
    default:
      return fieldKeys.includes(lowerCase(fieldProperty))
  }
}

export function updateMockData(method, data = null, endpoint = null) {
  if (method == 'get') {
    switch (true) {
      case endpoint.includes('status'):
        return endpoint.replace('{identifier}', uuid.v4())
      default:
        break
    }
    return endpoint
  }
  const mockData = cloneDeep(data)
  if (!mockData.survey_metadata) return mockData
  mockData.survey_metadata.survey_details.project_id = '1'
  mockData.survey_metadata.survey_details.protocol_id =
    'c1b38b0f-a888-4f28-871b-83da2ac1e533'
  return mockData
}

/*
 * Find the field with specified type and format from the schema documentation
 */
function getFieldName(
  type,
  schema,
  format = null,
  property = null,
  flag = null,
  isRequired = false,
  componentSearch = false,
  deepNested = false,
) {
  if (!schema.properties) return null
  const properties = schema.properties
  const required = schema.required || []
  if (componentSearch) {
    const componentData = getFieldName(
      cc.FIELD_TYPE.NON_REPEATABLE_COMPONENT,
      schema,
    )
    componentData['componentRelations'] = []
    // if component found we will search inside
    if (componentData.field) {
      const data = getFieldName(
        type,
        properties[componentData.field],
        format,
        property,
        flag,
        isRequired,
      )
      // relation hierarchy

      if (data.field) {
        data['componentData'] = componentData
        return data
      }

      // if deeply nested e.g. survey-metadata
      if (deepNested && data.components.length > 0) {
        for (const component of data.components) {
          const data = getFieldName(
            type,
            properties[componentData.field].properties[component],
            format,
            property,
            flag,
            isRequired,
          )
          if (!data.field) continue
          componentData['componentRelations'].push(component)
          data['componentData'] = componentData
          return data
        }
      }
    }

    return { field: null }
  }
  const fields = Object.keys(properties)
  const fieldData = {
    field: null,
    type: type,
    format: format,
    property: property,
    flag: flag,
    propertyValue: null,
    flagValue: null,
    isRequired: isRequired,
    components: [],
  }
  let components = []
  for (const field of fields) {
    fieldData.field = null
    const fieldStatus = getFieldStatus(field, properties[field], required)
    if (fieldStatus.isComponent) components.push(field)
    if (!properties[field].type) continue
    if (properties[field].type != lowerCase(type)) continue

    // for unique fields don't need to check required field
    if (
      type != cc.FIELD_TYPE.NON_REPEATABLE_COMPONENT &&
      fieldStatus.isRequired != isRequired
    )
      continue
    if (format && !fieldStatus.formatExists) continue
    if (!format && fieldStatus.formatExists) continue
    if (property && !fieldStatus.propertyExists) continue
    if (!property && fieldStatus.propertyExists) continue
    if (!flag && properties[field]['x-model-ref']) continue
    if (flag && !fieldStatus.flagExists) continue

    fieldData.field = field
    if (property && !checkProperty(field, properties, property)) continue
    if (property) fieldData.propertyValue = properties[field][property]

    if (format) {
      if (!properties[field].format) continue
      // lowerCase cant replace _ with space e.g. 'DATE_TIME' to 'date time'
      if (properties[field].format != lowerCase(format).replace(' ', '-'))
        continue
    }
    if (flag && !properties[field][flag]) continue
    if (flag) fieldData.flagValue = properties[field][flag]

    fieldData.components = components
    return fieldData
  }
  return { field: null, components: components }
}

/*
 * Create invalid data based upon flag types and values
 */
function generateInvalidValue(currentValue, fieldData) {
  // as value should be same
  if (fieldData.property == cc.PROPERTY_TYPE.UNIQUE) return currentValue
  // value should be null
  if (fieldData.isRequired) return null

  let value = null
  // handle types
  if (fieldData.type) {
    switch (fieldData.type) {
      case cc.FIELD_TYPE.NON_REPEATABLE_COMPONENT:
        if (!fieldData.property) value = [currentValue]
        break
      case cc.FIELD_TYPE.REPEATABLE_COMPONENT:
        if (!fieldData.property) value = currentValue[0] || {}
        break
      default:
        value = cc.INVALID_VALUE[fieldData.type]
        break
    }
  }
  // handle formats
  if (fieldData.format) {
    switch (fieldData.format) {
      default:
        value = cc.INVALID_VALUE[fieldData.format]
        break
    }
  }

  // handle properties
  if (fieldData.property) {
    switch (fieldData.property) {
      case cc.PROPERTY_TYPE.MINIMUM:
        value = fieldData.propertyValue - 1
        break
      case cc.PROPERTY_TYPE.MAXIMUM:
        value = fieldData.propertyValue + 1
        break
      case cc.PROPERTY_TYPE.MINITEMS:
        value = null
        break
      case cc.PROPERTY_TYPE.MAXITEMS:
        let currentLength = currentValue.length
        value = currentValue
        while (currentLength <= fieldData.propertyValue) {
          value.push(currentValue[0] ? currentValue[0] : {})
          currentLength += 1
        }
        break
      case cc.PROPERTY_TYPE.MINLENGTH:
        value = ''
        break
      case cc.PROPERTY_TYPE.MAXLENGTH:
        value = ''
        let count = 0
        while (count <= fieldData.propertyValue) {
          value = value + 'x'
          count += 1
        }
        break
      case cc.PROPERTY_TYPE.ENUM:
        value = cc.INVALID_VALUE[fieldData.property]
        break
    }
  }

  // handle flag
  if (!fieldData.flag) return value
  switch (fieldData.flag) {
    case 'x-model-ref':
      value = '-1'
      break
  }

  return value
}

export function prettyFormatFieldName(fieldName) {
  // the regex replaces all underlines and hyphens with spaces
  // The loop capitalises the first letter of every word
  let out = ''
  for (const word of fieldName.replace(/(-|_)/g, ' ').split(' ')) {
    out = `${out} ${word[0].toUpperCase()}${word.substring(1)}`
  }
  // remove leading space
  return out.trim()
}

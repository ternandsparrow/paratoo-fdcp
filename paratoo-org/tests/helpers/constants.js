export const FORMAT = {
  DATE_TIME: 'DATE_TIME',
  EMAIL: 'EMAIL',
  FLOAT: 'FLOAT',
}

export const PROPERTY_TYPE = {
  ENUM: 'ENUM',
  UNIQUE: 'UNIQUE',
  MINIMUM: 'minimum',
  MAXIMUM: 'maximum',
  MINLENGTH: 'minLength',
  MAXLENGTH: 'maxLength',
  MINITEMS: 'minItems',
  MAXITEMS: 'maxItems',
}

export const FIELD_TYPE = {
  STRING: 'STRING',
  INTEGER: 'INTEGER',
  BOOLEAN: 'BOOLEAN',
  NUMBER: 'NUMBER',
  // components are just objects or array of objects
  NON_REPEATABLE_COMPONENT: 'OBJECT',
  REPEATABLE_COMPONENT: 'ARRAY',
}

export const INVALID_VALUE = {
  STRING: 111,
  INTEGER: 'invalidInt',
  BOOLEAN: 'invalidBool',
  ENUM: 'invalidEnum',
  DATE_TIME: 'invalidDateTime',
  FLOAT: 'invalidFloat',
}

export const HEADER = {
  accept: 'application/json',
  'Content-Type': 'application/json',
}

export const LOGIN_API = '/api/auth/local'

export const CUSTOM_RULE_TYPES = {
  surveyMetadataProjectCheck: 'surveyMetadataProjectCheck',
  surveyMetadataProtocolCheck: 'surveyMetadataProtocolCheck',
  uuidIdentifierCheck: 'uuidIdentifierCheck',
}

// post requests
export const AJV_VALIDATOR_TEST_ROUTES = [
  // only 1 api which doesn't have any dependencies.
  '/org/mint-identifier',
]

// get/post requests
export const CUSTOM_RULE_VALIDATOR_TEST_ROUTES = [
  '/org/mint-identifier',
  '/org/status/{identifier}',
]

//require('dotenv').config({ path: './tests/helpers/.env' })
//console.log(process.env);
const strapi = require('@strapi/strapi')
const fs = require('fs')

jest.setTimeout(200000)

const setupStrapi = async () => {
  global.strapiInstance = await strapi().load()
  console.log('got a handle on Strapi')
  global.strapiInstance.server.mount()
  global.strapiServer = global.strapiInstance.server.app.callback()
}

const teardownStrapi = () => {
  global.strapiInstance.destroy().then(() => {
    //delete test database after all tests
    // Fixme
    // strapi instance becomes invalid
    // const dbSettings = strapi.config.get('database.connection')
    // if (dbSettings && dbSettings.filename) {
    //   const tmpDbFile = `${__dirname}/../${dbSettings.filename}`
    //   if (fs.existsSync(tmpDbFile)) {
    //     fs.unlinkSync(tmpDbFile)
    //   }
    // }
  })
}

module.exports = { setupStrapi, teardownStrapi }

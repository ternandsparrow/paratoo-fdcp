#!/bin/bash
echo "Running Paratoo Org's Jest Unit Tests"
NODE_ENV=test yarn jest --forceExit --coverage --coverageReporters=cobertura

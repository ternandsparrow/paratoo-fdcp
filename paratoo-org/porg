#!/bin/bash
# create required supporting services as docker containers and start app in dev
# mode on this machine (not in a docker container)
set -euo pipefail
cd `dirname "$0"`

envVarPrefix=PO_
overrideFile=localdev.env

if [ -f $overrideFile ]; then
  echo "[INFO] loading config overrides from $overrideFile"
  tmpLocalEnv=$(mktemp)
  # remove comments and blank lines
  grep -v -e '^$' -e '^[# ]' $overrideFile \
    | sed 's/^/export /' > $tmpLocalEnv

  if [ -f "test_users.env" ]; then
    echo "[INFO] Found test user credentials"
    # Append the formatted JSON string to the temporary environment file
    grep -v -e '^$' test_users.env | sed 's/^/export /; s/\$/\\$/g' >> "$tmpLocalEnv"
  fi
  source $tmpLocalEnv
  rm -f $tmpLocalEnv
fi

# we use the $envVarPrefix so we can dynamically find all the relevant vars
# later in the script.
export PO_DB_CONTAINER_NAME=${DB_CONTAINER_NAME:-paratoo-org-db-localdev}
dbVolName=$PO_DB_CONTAINER_NAME-data
export PO_DATABASE_HOST=${DATABASE_HOST:-localhost}
export PO_DATABASE_PORT=${DATABASE_PORT:-45433}
export PO_DATABASE_NAME=${DATABASE_NAME:-app}
export PO_DATABASE_USERNAME=${DATABASE_USERNAME:-user}
export PO_DATABASE_PASSWORD=${DATABASE_PASSWORD:-password}

export PO_PORT=${PORT:-1338} # don't conflict with paratoo-core

export PO_STRAPI_LOG_LEVEL=${STRAPI_LOG_LEVEL:-info}
export PO_SENTRY_DSN=${ORG_SENTRY_DSN:-sentrydsn}

# check admin password and email as we need an admin account
ADMIN_PASS=${ADMIN_PASS:-}
ADMIN_EMAIL=${ADMIN_EMAIL:-}
if [ -z "$ADMIN_PASS" ] || [ -z "$ADMIN_EMAIL" ]; then
  echo "[ERROR] ADMIN_PASS and ADMIN_EMAIL must be set"
  exit 1
fi

echo "[INFO] Config dump"
env | grep "^$envVarPrefix" | sed "s/^$envVarPrefix/  /" | sort
echo "\
note1: define any config overrides in $overrideFile.
note2: changing config isn't always guaranteed to update the stack. Things
       like port numbers *will*, but things like DB credentials will *not*.
       If in doubt, kill the whole stack and start fresh. See the output further
       down for commands to cleanup.
"

function printCleanupHelpBody {
  cat <<EOF
Stop the supporting docker containers, but keep data with:
  $0 down

Stop and remove the supporting docker containers and KILL data with:
  $0 down --volumes
EOF
}

function printHelp {
  cat <<EOF
Run with no args to start supporting docker containers and then dev mode strapi
  $0

Enable NodeJS debugger (and wait for connection)
  $0 develop:debug

Launch strapi console/REPL (also runs API server at the same time)
  $0 strapi console

Launch psql shell inside DB container
  $0 psql

Various docker-compose commands
  $0 ps
  $0 up  # i.e. no detach

EOF
  printCleanupHelpBody
}


# we use docker-compose because get all the built-in smarts for free: create
# stack if it doesn't exist, update if config changes, delete everything.
tmpDc=.tmp.docker-compose.yml
tempInitData=./.initDataBackUp
cat <<EOF > $tmpDc
services:
  orgdb:
    image: postgres:14.8
    container_name: '$PO_DB_CONTAINER_NAME'
    ports:
      - $PO_DATABASE_PORT:5432
    environment:
      POSTGRES_DB: '$PO_DATABASE_NAME'
      POSTGRES_USER: '$PO_DATABASE_USERNAME'
      POSTGRES_PASSWORD: '$PO_DATABASE_PASSWORD'
    volumes:
      - $dbVolName:/var/lib/postgresql/data

volumes:
  $dbVolName:
EOF

# Allow for the next-gen V2 docker compose command
dockerComposeCMD="docker-compose"
if ! command -v $dockerComposeCMD &> /dev/null
then
    echo "Using V2 docker compose command"
    dockerComposeCMD="docker compose"
fi

secondParam=${2:-}
function doDc {
    case $secondParam in
    --volumes)
      echo "[INFO] Removing volumes"
      if [ -d "$tempInitData" ] ; then
        echo "[INFO] Removing .tempInitData"
        rm -Rf "$tempInitData"
      fi
      $dockerComposeCMD \
      --project-name paratoo_org \
      --file $tmpDc \
      down \
      --volumes
      ;;
     "")
      dcCmd=${1:-up -d}
      $dockerComposeCMD \
        --project-name paratoo_org \
        --file $tmpDc \
        $dcCmd
          rm -f $tmpDc
  esac
}

function printCleanupHelp {
  echo "==== Remember: the docker container(s) are still running ===="
  printCleanupHelpBody
}

function enableTrap {
  trap "printCleanupHelp" EXIT
}

firstParam=${1:-}
case $firstParam in
  -h | --help | help )
    printHelp
    exit
    ;;
  down | ps | up )
    echo "[INFO] running docker-compose $firstParam command"
    doDc $@
    ;;
  psql )
    echo "[INFO] dropping to psql shell in docker DB container"
    doDc
    enableTrap
    docker exec -it $PO_DB_CONTAINER_NAME psql \
      -U $PO_DATABASE_USERNAME \
      -d $PO_DATABASE_NAME
    ;;
  * )
    echo "[INFO] running yarn/strapi command"
    doDc
    # dynamically export all the required env vars, so we don't have to maintain
    # the list by hand. We use a (hopefully) unique prefix to only grab "our"
    # vars.  We're using a temp file because we're still supporting bash 3.2,
    # which macOS still seems to ship with :'(
    tmpEnv=$(mktemp)
    env | grep "^$envVarPrefix" | sed "s/^$envVarPrefix/export /" > $tmpEnv
    source $tmpEnv
    rm -f $tmpEnv
    enableTrap
    yarn ${@:-develop}
    ;;
esac

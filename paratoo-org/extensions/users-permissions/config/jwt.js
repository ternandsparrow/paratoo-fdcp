module.exports = {
  //FIXME defaulting to the second static secret. need a docker compose file that we can
  //use to link to ${ORG_STRAPI_SECRET} like in:
  //paratoo-fdcp-runner/docker-compose.org-webapp.yml
  jwtSecret: process.env.JWT_SECRET || 'e17d5151-a4f5-4feb-9430-6f4b14c16646'
}
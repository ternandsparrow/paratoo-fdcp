'use strict'

const lutMainField = 'symbol'
const _ = require('lodash')

/**
 * This policy grabs the model of the associated request and converts associations to LUTs
 * to it's internal ID. This policy should only be added to POST and PUT
 * @param {*} ctx
 * @returns
 */
module.exports = async (ctx) => {
  // reads the existing documentation
  // use the associated route in conjunction with it's path tag to determine model

  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

  const fullDoc = helpers.readFullDocumentation()

  const requestBody = ctx.request.body.data

  // check if bulk request because they are formatted entirely differently
  // According to paratoo-fdcp/ARCHITECTURE/core-bulk.md, bulk requests are
  // identified by their path ending with '/bulk'
  const isBulk = ctx._matchedRoute.split('/').pop() === 'bulk'
  if (isBulk) {
    let newCollectionArray = []
    // according to architecture, bulk requests have an array called collections
    // TODO iterate through the expected values in protocol definitions, even
    // though the payload should still be validated
    for (const collection of requestBody.collections) {
      for (const [key, value] of Object.entries(collection)) {
        // So every field apart from orgMintedIdentifier, refers to a model.
        if (key === 'orgMintedIdentifier') {
          collection[key] = value
          continue
        }
        //check for observations
        collection[key] = await replaceInNestedObservations(
          key,
          collection[key],
          fullDoc,
        )
      }

      newCollectionArray.push(collection)
    }
    requestBody.collections = newCollectionArray
    ctx.request.body.data = requestBody
    return true
  } else {
    // find Strapi internal model for that schema
    // since it's not a bulk of multiple endpoints we know
    // the intended modelname from the request context
    let modelName
    try {
      modelName = ctx.state.route.info.apiName
    } catch (error) {
      strapi.log.error('Couldn\'t resolve model name for LUT interpretation')
    }

    ctx.request.body = await replaceEnum(modelName, { data: requestBody })

    return true
  }
}

/**
 * Calls ReplaceEnum recursively with appropriate modelName, to search for nested
 * Observations fields that are included in the documentation
 *
 * @param {String} modelName Where to look in #/components/schemas/ for the associated schema
 * @param {*} data User input payload
 * @param {*} documentation full_documentation.json reference
 */
async function replaceInNestedObservations(modelName, data, documentation) {
  if (!data) return data

  const schemaName = `${_.upperFirst(_.camelCase(modelName))}Request`
  // find which fields SHOULD be new, uninterpreted data (from the documentation)
  // i.e. in the "NewSchema-observations" etc. where there is a $ref to ANOTHER "NewSchema-covers"
  let newRefFields = []
  // make list of all $ref NewSchema fields
  const schemaProperties =
    documentation.components.schemas[schemaName].properties
  for (const [attributeName, attributeValue] of Object.entries(
    schemaProperties,
  )) {
    if (attributeValue.type === 'object' && attributeValue['$ref']) {
      // TODO find a way of passing the modelName into the next replaceInNestedObservations
      // TODO since we call replaceEnum from here which needs a reference to the internal apiName at that level
      // you can probably use x-model-ref but I haven't tested because the $ref situation only occurs
      // with documentation overrides, and overrides don't work in Strapi4 yet https://github.com/strapi/strapi/issues/12522
      newRefFields.push({
        fieldName: attributeName,
        schemaName: attributeValue['$ref'].replace('#/components/schemas/', ''),
        multiple: false,
      })
    } else if (
      attributeValue.type === 'array' &&
      attributeValue.items &&
      attributeValue.items.type === 'object' &&
      attributeValue.items['$ref']
    ) {
      newRefFields.push({
        fieldName: attributeName,
        schemaName: attributeValue.items['$ref'].replace(
          '#/components/schemas/',
          '',
        ),
        multiple: true,
      })
    } else {
      continue
    }
  }
  let newData
  if (Array.isArray(data)) {
    newData = []
    for (const [index, value] of Object.entries(data)) {
      newData[index] = await replaceEnum(modelName, value)

      for (const newRefField of newRefFields) {
        newData[index][newRefField.fieldName] =
          await replaceInNestedObservations(
            newRefField.schemaName,
            data[index][newRefField.fieldName],
            documentation,
          )
      }
    }
  } else {
    newData = await replaceEnum(modelName, data)
    for (const newRefField of newRefFields) {
      newData[newRefField.fieldName] = await replaceInNestedObservations(
        newRefField.schemaName,
        data[newRefField.fieldName],
        documentation,
      )
    }
  }
  return newData
}

/**
 * Checks if the body in a single layer data payload contains any luts
 * with context from the api modelName
 *
 * @param {*} modelName Strapi internal API name. (should exist as strapi.api[name])
 * @param {*} inputBody
 * @returns
 */
async function replaceEnum(modelName, inputBody) {
  let outputBody = inputBody.data
  if (!inputBody.data) return inputBody
  let modelAttributes
  try {
    modelAttributes = strapi.api[modelName].contentTypes[modelName].attributes
  } catch {
    strapi.log.error(
      `Lut interpretation cannot find Strapi model: ${modelName}`,
    )
    return inputBody
  }

  for (const [attributeName, element] of Object.entries(modelAttributes)) {
    if (element.type !== 'relation') continue

    let relationTarget = element.target
    // target in format of     target: 'api::lut-fauna-maturity.lut-fauna-maturity'
    // remove leading 'api::'
    if (
      typeof relationTarget === 'string' &&
      relationTarget.slice(5).split('.')[0].match('^lut-')
    ) {
      const id = await findIdForSymbol(
        outputBody[attributeName],
        relationTarget,
      )
      if (Number.isInteger(id)) outputBody[attributeName] = id
      // strapi.log.info(
      //   `LUT interpretation interpreted value in ${attributeName} from relationTarget:'${relationTarget}' as internal id: ${outputBody[attributeName]}`,
      // )
    }
  }

  // Find components from attributes (where type=component)
  for (const [key, field] of Object.entries(modelAttributes)) {
    if (
      field.type !== 'component' ||
      !outputBody[key] ||
      typeof outputBody[key] !== 'object'
    )
      continue

    const componentName = field.component
    // Search for LUT fields in the base component

    // Change those values in the requestBody
    const componentAttributes = strapi.components[componentName].attributes
    // filter for only LUT associations
    const componentAssociations = Object.values(componentAttributes).filter(
      (o) =>
        o.type === 'relation' && o.target.slice(5).split('.')[0].match('^lut-'),
    )

    for (const [componentAttributeName, element] of componentAssociations) {
      // case of repeatable component (fields in an array)
      if (field.repeatable === true) {
        for (const [index] of Object.entries(outputBody[key])) {
          const id = await findIdForSymbol(
            outputBody[key][index][componentAttributeName],
            element.target,
          )
          if (Number.isInteger(id))
            outputBody[key][index][componentAttributeName] = id
        }
      }
      // case of single component
      else if (field.repeatable === false) {
        const id = await findIdForSymbol(
          outputBody[key][componentAttributeName],
          element.target,
        )
        if (Number.isInteger(id)) outputBody[key][componentAttributeName] = id
      }
    }
  }

  return { data: outputBody }
}

async function findIdForSymbol(symbol, apiTarget) {
  if (!symbol || !apiTarget) {
    return
  }

  const dbReturn = await strapi.db
    .query(apiTarget)
    .findOne({ where: { [lutMainField]: symbol } })

  return dbReturn.id
}

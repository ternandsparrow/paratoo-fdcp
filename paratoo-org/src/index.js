'use strict'
const crypto = require('crypto')
const strapiInitialData = require('strapi-initial-data')

const adminRoleType = process.env.ADMIN_ROLE_TYPE || 'paratoo_admin'

// Add test users' credentials from test_users.env
const testUsersCredentials = []
for (const key of Object.keys(process.env)) {
  if (/\b(TEST_USER\d+)\b/g.test(key)) {
    testUsersCredentials.push({
      userName: process.env[key],
      password: process.env[`${key}_PASS`],
      email: process.env[`${key}_EMAIL`],
      projects: process.env[`${key}_PROJECTS`].split(','),
      role: process.env[`${key}_ROLE`]
    })
  }
}
const figlet = require('figlet')
console.log(
  figlet.textSync('Strapi ORG (Paratoo/Monitor)', {
    horizontalLayout: 'default',
    verticalLayout: 'default',
    width: 120,
    whitespaceBreak: true,
  }),
)
/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/v3.x/concepts/configurations.html#bootstrap
 */

module.exports = {
  /**
   * An asynchronous register function that runs before
   * your application is initialized.
   *
   * This gives you an opportunity to extend code.
   */
  register(/*{ strapi }*/) {},

  /**
   * An asynchronous bootstrap function that runs before
   * your application gets started.
   *
   * This gives you an opportunity to set up your data model,
   * run jobs, or perform some special logic.
   */
  async bootstrap({ strapi }) {
    console.log(`ORG Strapi instance is bootstrapping .. version: ${process.env.VERSION} git_hash: ${process.env.GIT_HASH}`)
    console.log(`ORG Strapi database .. name: ${process.env.DATABASE_NAME} username: ${process.env.DATABASE_USERNAME}`)
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    await assertAdminExists()
    await helpers.configurePerms()
    
    // different file for different database
    const dbInfo = `${process.env.DATABASE_HOST}${process.env.DATABASE_PORT}${process.env.DATABASE_NAME}`
    const initDataBackUpFileName = crypto.createHash('md5').update(dbInfo).digest('hex')
    const initDataBackUpFilePath = `./.initDataBackUp/${initDataBackUpFileName}.json`
    // no need to wait for data to be loaded, so we don't
    //make sure to `await` as to ensure the initial data is done before creating test users
    await strapiInitialData(strapi, initDataBackUpFilePath).catch((err) => {
      // using console over strapi.log because strapi doesn't handle errors well
      helpers.paratooErrorHandler(500, err, 'Failed to load all LUT data')
      console.trace()
    })

    //creates user for Strapi admin panel (not the same as assertAdminExists())
    await helpers.createAdminUser()
    //create a user that can be used for testing (when auth is required)
    for (const user of testUsersCredentials) {
      strapi.log.verbose(`create test user ${user.uname}`)
      await createTestUser(...Object.values(user))
    }
    await helpers.amendDocumentation()
  },
}

async function assertAdminExists() {
  const roleOrm = strapi.query('plugin::users-permissions.role')
  const found = await roleOrm.findOne({ where: { type: adminRoleType } })
  if (found) {
    strapi.log.verbose(
      `Admin role with type=${adminRoleType} already exists, nothing to do.`,
    )
    return
  }
  strapi.log.verbose(
    `Admin role with type=${adminRoleType} does not exist, creating.`,
  )

  const upServices = strapi.plugins['users-permissions'].services
  // following the same approach that the UsersPermissions plugin UI uses
  // itself, where we provide values for all the permissions:
  // https://github.com/strapi/strapi/blob/v3.4.4/packages/strapi-plugin-users-permissions/admin/src/containers/Roles/CreatePage/index.js#L61-L66.
  // If you don't, our other bootstrap code won't create them for us, it only
  // updates existing values.
  const permissions = await upServices['users-permissions'].getActions()
  await upServices.role.createRole({
    name: adminRoleType,
    description: 'Paratoo Admin role',
    type: adminRoleType,
    permissions,
  })
  strapi.log.info(`Admin role with type=${adminRoleType} created!`)
}

//disable lint as this method is commented out to prevent tests from failing
//but we want to keep the method in to re-implement when tests account for it
/*eslint-disable */
async function createTestUser(
  testUserUName,
  testUserPass,
  testUserEmail,
  assignedProjects,
  roleTypeOverride = null,
) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  //check if user already exists
  let user = await strapi.query('plugin::users-permissions.user').findOne({
    where: { username: testUserUName },
  })
  //get the role ID for `collector`
  const roleOrm = strapi.query('plugin::users-permissions.role')
  const role = await roleOrm.findOne({
    where: { type: roleTypeOverride || 'collector' },
  })

  if (user) {
    strapi.log.verbose(
      `Testing user with username ${testUserUName} already exists, nothing to do.`,
    )
  } else {
    strapi.log.verbose(
      `Creating user for testing with username ${testUserUName}`,
    )

    //create the user
    const testUserObj = {
      username: testUserUName,
      email: testUserEmail,
      password: testUserPass,
      confirmed: true,
      blocked: false,
      role: role.id,
      provider: 'local',
      projects: assignedProjects, //just give user a bunch of projects from initial data
      //we can't do a query on them as they don't exist yet
    }
    await strapi.plugins['users-permissions'].services.user
      .add(testUserObj)
      .catch((err) => {
        let msg = `Couldn't create user ${testUserUName} for the ${role.name} role`
        helpers.paratooErrorHandler(500, err, msg)
      })
    strapi.log.info(
      `Successfully created user ${testUserUName} for the ${role.name} role, ` +
        `with password=${testUserPass}`,
    )
  }
}

/*eslint-enable */

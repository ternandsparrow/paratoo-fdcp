const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::protocol.protocol')
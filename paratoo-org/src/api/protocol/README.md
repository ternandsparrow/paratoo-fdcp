As both paratoo-core *and* paratoo-org need to access the protocols, there needs to be some mechanism to keep both of them in sync.

The current mechanism is to manually do it, by runnung a script found in `/paratoo-fdcp/helper-scripts/sync-protocol-models.sh`.

This will sync the paratoo-core definition into paratoo-org, which will be clobbered. To ensure the CI/CD pipeline doesn't fail, make sure you run this script if you change the protocol model.
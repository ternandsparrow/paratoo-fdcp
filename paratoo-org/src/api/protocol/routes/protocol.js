module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/protocols',
      'handler': 'protocol.find',
      'config': {
        'policies': [
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/protocols/:id',
      'handler': 'protocol.findOne',
      'config': {
        'policies': [
        ]
      }
    },
  ]
}
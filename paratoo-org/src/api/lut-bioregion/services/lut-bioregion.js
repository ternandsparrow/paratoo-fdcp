'use strict'

/**
 * lut-bioregion service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-bioregion.lut-bioregion')

'use strict'

/**
 * lut-bioregion router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories

module.exports = createCoreRouter('api::lut-bioregion.lut-bioregion')

module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/org/pdp/:projectId/:protocolUuid/read',
      handler: 'org.decideRead',
      config: { policies: [ 'global::is-validated' ] }
    },
    {
      method: 'GET',
      path: '/org/pdp/:projectId/:protocolUuid/write',
      handler: 'org.decideWrite',
      config: { policies: [ 'global::is-validated' ] }
    },
    {
      method: 'GET',
      path: '/org/user-projects',
      handler: 'org.userProjects',
      config: { policies: [ 'global::is-validated' ] }
    },
    {
      method: 'POST',
      path: '/org/mint-identifier',
      handler: 'org.mintIdentifier',
      config: { policies: [ 'global::is-validated' ] }
    },
    {
      method: 'POST',
      path: '/org/collection',
      handler: 'org.collectionHook',
      config: { policies: [ 'global::is-validated' ] }
    },
    {
      method: 'GET',
      path: '/org/get-all-collections',
      handler: 'org.getAllCollections',
      config: { policies: ['global::is-validated'] }
    },
    {
      method: 'GET',
      path: '/org/status/:identifier',
      handler: 'org.identifierStatus',
      config: { policies: [ 'global::is-validated' ] }
    },
    {
      method: 'POST',
      path: '/org/validate-token',
      handler: 'org.validateToken',
      config: { policies: [ 'global::is-validated' ] }
    },
    {
      method: 'GET',
      path: '/org/user-role',
      handler: 'org.userRole',
      config: { policies: [ /* 'global::is-validated' */ ] }
    },
    {
      method: 'POST',
      path: '/org/challenge',
      handler: 'org.generateChallenge',
      config: { policies: [ 'global::is-validated' ] }
    },
    {
      method: 'POST',
      path: '/org/verify-register',
      handler: 'org.verifyWebAuthnRegister',
      config: { policies: [ 'global::is-validated' ] }
    },
    {
      method: 'POST',
      path: '/org/login-challenge',
      handler: 'org.generateLoginChallenge',
      config: { policies: [ 'global::is-validated' ] }
    },
    {
      method: 'POST',
      path: '/org/verify-login',
      handler: 'org.verifyWebAuthnLogin',
      config: { policies: [ 'global::is-validated' ] }
    }
  ]
}
'use strict'

/**
 * org controller
 */
const {
  decideRead,
  decideWrite,
  userProjects,
  mintIdentifier,
  collectionHook,
  identifierStatus,
  validateToken,
  generateChallenge,
  verifyWebAuthnRegister,
  generateLoginChallenge,
  verifyWebAuthnLogin,
  getAllCollections,
  userRole,
} = require('./org-interface')
const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::org.org', () => ({
  // Method 1: Creating an entirely custom action
  async decideRead(ctx) {
    return decideRead(ctx)
  },
  async decideWrite(ctx) {
    return decideWrite(ctx)
  },
  async userProjects(ctx) {
    return await userProjects(ctx)
  },
  async mintIdentifier(ctx) {
    return await mintIdentifier(ctx)
  },
  async collectionHook(ctx) {
    // FIXME: helpers.paratooErrorHandler not working
    return await collectionHook(ctx)
  },
  async identifierStatus(ctx) {
    return await identifierStatus(ctx)
  },
  async validateToken(ctx) {
    return await validateToken(ctx)
  },
  async generateChallenge(ctx) {
    return await generateChallenge(ctx)
  },
  async verifyWebAuthnRegister(ctx) {
    return await verifyWebAuthnRegister(ctx)
  },
  async generateLoginChallenge(ctx) {
    return await generateLoginChallenge(ctx)
  },
  async verifyWebAuthnLogin(ctx) {
    return await verifyWebAuthnLogin(ctx)
  },
  async getAllCollections(ctx) {
    return await getAllCollections(ctx)
  },
  async userRole(ctx) {
    return await userRole(ctx)
  },
}))

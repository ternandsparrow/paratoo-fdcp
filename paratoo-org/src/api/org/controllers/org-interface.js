'use strict'
const _ = require('lodash')
const webauthnController = require('./webauthn')
const uuid = require('uuid')
const PERMISSION_TYPE = {
  read: 'read',
  write: 'write'
}
/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

/**
 * The main PDP functionality that performs a series of checks before making a decision.
 * These checks are:
 *  
 * - if the protocol exists
 * - if the project exists
 * - if the protocol is assigned to the project
 * - if the User (based on the context) exists
 * - if the project is assigned to the User
 * - if isWrite, if the provided protocol has write permission
 * 
 * @param {Object} ctx Koa context
 * @param {Boolean} isWrite Whether is PDP request is checking for write permission
 * @returns {Boolean} If the User is authorized
 */
async function decide(ctx, isWrite) {
  // if we get to here, the JWT is valid
  const projectId = parseInt(ctx.params.projectId)
  const protocol = await strapi.db.query('api::protocol.protocol').findOne({
    where: {
      identifier: ctx.params.protocolUuid
    }
  })
  if (!protocol) {
    const msg = `Protocol with UUID ${ctx.params.protocolUuid} does not exist`
    strapi.log.debug(msg)
    return decideResult(false, {
      message: msg
    })
  }
  const protocolId = protocol.id

  //check if protocol exists
  const prot = await checkProtocol(protocolId)
  if(!prot) {
    const msg = `Protocol ${protocolId} does not exist`
    strapi.log.debug(msg)
    return decideResult(false, {
      message: msg
    })
  }

  //check if project exists
  const proj = await checkProject(projectId)
  if(!proj) {
    const msg = `Project ${projectId} does not exist`
    strapi.log.debug(msg)
    return decideResult(false, {
      message: msg
    })
  } else {
    //project and protocol exist, but check if protocol is assigned to project
    var found = false
    for(var i = 0; i < proj.protocols.length; i++) {
      //check if supplied protocol is in the array of project's protocols
      if(proj.protocols[i].id == protocolId) {
        strapi.log.debug(`Protocol ${protocolId} is assigned to project ${projectId}`)
        found = true
        break   //don't need to keep searching
      }
    }
    if(!found) {
      const msg = `Could not find protocol ${protocolId} assigned to project ${projectId}`
      strapi.log.debug(msg)
      return decideResult(false, {
        message: msg
      })
    }
  }
  const userId = _.get(ctx.state.user, 'id')
  //check if user exits
  if (!userId) {
    const msg =
      `Could not find user with ID=${userId}, which is weird ` +
      'because the Strapi route level auth succeeded for us to get to ' +
      'this point.'
    strapi.log.debug(msg)
    return decideResult(false, {
      message: msg,
    })
  }
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const projAndProto = await helpers.getProjAndProtoForUser(userId, isWrite ? PERMISSION_TYPE.write : PERMISSION_TYPE.read)
  if (projAndProto == 'no user projects') {
    strapi.log.error(`user ${userId} does not have ${isWrite ? 'write' : 'read'} access`)
    return decideResult(false)
  }
  //check if user can access project/protocol
  const isAllowed = (() => {
    const proj = projAndProto.projects.find(e => e.id === projectId)
    if (!proj) {
      strapi.log.debug(`Project ${projectId} is not assigned to user ${userId}`)
      return false
    }
    const proto = proj.protocols.find(e => e.id === protocolId)
    return !!proto
  })()
  strapi.log.debug(
    `User ${userId} ${isAllowed ? 'DOES' : 'does NOT'} have access to ` +
      `project ${projectId}/protocol ${protocolId}`,
  )
  if(isAllowed) {
    //check if read/write
    if(isWrite) {
      //check write
      const writePermission = await strapi.db.query('api::protocol.protocol')
        .findOne({
          where: { id: protocolId }
        })
      //isWritable should be set to true by default but we check for null just in case
      if(writePermission.isWritable || writePermission.isWritable === null) {
        strapi.log.debug(`Protocol ${protocolId} DOES have write permission`)
        return decideResult(true)
      }
      let msg = `Protocol ${protocolId} does NOT have write permission`
      strapi.log.debug(msg)
      return decideResult(false, {
        message: msg
      })
    } else {
      //read, so just return result of isAllowed
      return decideResult(isAllowed)
    }    
  } else {
    //user does not have access to project/protocol, so just return result of isAllowed
    return decideResult(isAllowed)
  }
}

function decideResult(isAuthorised, extra) {
  return {
    isAuthorised,
    ...extra,
  }
}

//check if protocol exists
//note: users could keep hitting PDP to see if project/protocol exists
//this may have potential security issues
async function checkProtocol(protocolId) { 
  return await strapi.db
    .query('api::protocol.protocol')
    .findOne({
      where: { id: protocolId }
    })
}

//check if project exists
async function checkProject(projectId) {
  return await strapi.db
    .query('api::project.project')
    .findOne({
      where: { id: projectId },
      populate: true
    }) 
}

module.exports = {
  /**
   * Handler function for PDP read endpoint
   * 
   * @param {Object} ctx Koa context
   * @returns {Boolean} if the User is authorized for read permissions
   */
  decideRead(ctx) {
    const isWrite = false
    return decide(ctx, isWrite)
  },
  /**
   * Handler function for PDP write endpoint
   * 
   * @param {Object} ctx Koa context
   * @returns {Boolean} if the User is authorized for write permissions
   */
  decideWrite(ctx) {
    const isWrite = true
    return decide(ctx, isWrite)
  },
  /**
   * Handler function for user projects endpoint
   * 
   * @param {Object} ctx Koa context
   * @returns {Object} projects assigned to the User with associated protocols
   */
  async userProjects(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const userId = ctx.state.user.id
    const projAndProto = await helpers.getProjAndProtoForUser(userId, PERMISSION_TYPE.read)
    switch(projAndProto) {
      case 'no user': {
        let msg = 'User not found'
        return helpers.paratooErrorHandler(404, new Error(msg))
      }
      case 'no user projects': {
        let msg = 'User projects not found'
        return helpers.paratooErrorHandler(404, new Error(msg))
      }
      default: {
        return projAndProto
      }
    }
  },
  /**
   * Handler function for the mint identifier endpoint. Creates an identifier from the
   * survey metadata, the user's ID, and the event time. Stores the identifier and 
   * returns a base64 encoded version as the response.
   * 
   * @param {Object} ctx Koa context
   * @param {number} ctx.state.user.id the User's ID
   * @param {Object} ctx.request.body.survey_metadata the ID created by the client locally
   * 
   * @returns {String} base64 encoded stringified JSON
   */
  async mintIdentifier(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const requestBody = ctx.request.body
    const survey_metadata = requestBody.survey_metadata
    // update org version
    survey_metadata.provenance['version_org'] = `${process.env.VERSION}-${process.env.GIT_HASH}`
    survey_metadata.provenance['system_org'] = `Monitor FDCP API-${process.env.NODE_ENV}`
    survey_metadata.orgMintedUUID = uuid.v4()
    const orgMintTime = new Date(Date.now())

    const rawIdentifier = {
      userId: ctx.state.user.id,
      eventTime: orgMintTime,
      survey_metadata: survey_metadata,
    }

    // webapp ipAddress
    // format "monitor webApp-localdev-129.127.180.3-Mozilla"
    const system_app_fields = survey_metadata.provenance.system_app.split('-')
    const submitter_ip_address = system_app_fields.length > 3 ? system_app_fields[2] : '127.0.0.1'

    if (submitter_ip_address === '127.0.0.1') {
      helpers.paratooWarnHandler('No IP address was provided or it could not be parsed, so falling back to loopback IP')
    }

    const createdIdentifier = await strapi.entityService
      .create('api::collection-identifier.collection-identifier', {
        data: {
          survey_metadata: rawIdentifier.survey_metadata,
          user_id: rawIdentifier.userId,   //FK to Users
          org_mint_time: rawIdentifier.eventTime,
          submitter_ip_address: submitter_ip_address,
        },
        populate: '*',
      })
      .catch(err => {
        let msg = 'Failed to store the org identifier (orgMintedIdentifier, orgMintedUUID, survey_metadata)'
        switch(err.status) {
          case 400: {
            return helpers.paratooErrorHandler(400, err, msg)
          }
          default: {
            switch(err.name) {
              case 'ValidationError': {
                if (err.message.includes('submitter_ip_address')) {
                  msg = `submitter_ip_address '${submitter_ip_address}' must be a valid ip address`
                  return helpers.paratooErrorHandler(400, new Error(msg))
                }
                return helpers.paratooErrorHandler(400, err, msg)
              }
              default: {
                return helpers.paratooErrorHandler(500, err)
              }
            }
          }
        }
      })
    
    if (!createdIdentifier) {
      let msg = 'Failed to store the org identifier (orgMintedIdentifier, orgMintedUUID, survey_metadata)'
      return helpers.paratooErrorHandler(500, new Error(msg))
    }

    const encodedIdentifier = Buffer.from(
      JSON.stringify(rawIdentifier)
    ).toString('base64')

    return { orgMintedIdentifier: encodedIdentifier }
  },
  /**
   * returns the list of all collection-identifiers submitted by the user.
   *
   * @param {Object} ctx Koa context
   * @returns {Array} an array of collection-identifiers
   */
  async getAllCollections(ctx) {
    const userId = _.get(ctx.state.user, 'id')

    // if we use query to extract all the collections published by this user
    //   we don't need to filter by user-projects.
    const userCollections = await strapi.db.query('api::collection-identifier.collection-identifier').findMany({
      populate: true,
      where: {
        $and: [
          { user_id: { id: userId } },
          { core_submit_time: { $notNull: true, } },
        ],
      },
      orderBy: 'id',
    })
    const collections = userCollections.map((o)=>{
      return o.survey_metadata.orgMintedUUID
    })
    return {
      collections: collections
    }
  },
  /**
   * Updates the orgMintedUUID entry with a success.
   * 
   * Used to allow Core to notify Org of successful collection submission
   * 
   * @param {Object} ctx Koa context
   * @param {String} ctx.request.body.orgMintedUUID the orgMintedUUID to append a success
   * @param {Object} ctx.request.body.coreProvenance the coreProvenance object
   * @param {String} ctx.request.body.coreProvenance.system_core the deployed core
   * @param {String} ctx.request.body.coreProvenance.version_core the version of core
   * 
   * @returns {Boolean} Success of operation
   */
  async collectionHook(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    
    const requestBody = ctx.request.body
    const orgMintedUUID = requestBody.orgMintedUUID
    const coreProvenance = requestBody.coreProvenance
    const coreSubmitTime = new Date(Date.now())

    const relevantEntry = await strapi.db
      .query('api::collection-identifier.collection-identifier')
      .findOne({
        populate: {
          survey_metadata: {
            populate: {
              provenance: true,
            },
          },
        },
        where: {
          survey_metadata: {
            orgMintedUUID: orgMintedUUID,
          },
        },
      })
      .catch(err => {
        switch(err.status) {
          case 400: {
            return helpers.paratooErrorHandler(400, err)
          }
          default: {
            return helpers.paratooErrorHandler(500, err)
          }
        }
      })
    if (!relevantEntry) {
      return helpers.paratooErrorHandler(404, new Error(`No data set found with mintedCollectionId=${orgMintedUUID}. Cannot update the success of a collection that does not exist`))
    }
    if (relevantEntry?.core_submit_time) {
      return helpers.paratooErrorHandler(400, new Error(`Collection with orgMintedUUID '${orgMintedUUID}' has already been submitted.`))
    }

    //want to sure query engine to do the update as you can do a `where` on the
    //orgMintedUUID (whereas the entityService needs a reference to the PK ID), but it
    //can't do partial updates on the survey_metadata component, so stick with this
    //approach where we to a findOne to get the PK ID first then do the update
    const updatedIdentifier = await strapi.entityService
      .update('api::collection-identifier.collection-identifier', relevantEntry.id, {
        populate: {
          survey_metadata: {
            populate: {
              provenance: true,
            },
          },
        },
        data: {
          core_submit_time: coreSubmitTime,
          survey_metadata: {
            //need to append the id of the existing instance of the survey_metadata
            //component to ensure it does an in-place update
            //https://forum.strapi.io/t/partial-update-of-single-component-field-results-in-invalid-data/23195
            id: relevantEntry.survey_metadata.id,
            provenance: {
              ...relevantEntry.survey_metadata.provenance,
              system_core: coreProvenance.system_core,
              version_core: coreProvenance.version_core,
            },
          },
          //TODO submitter_ip_address (from app)
        },
      })
      .catch(err => {
        let msg = `Failed to update the success status of the submitted collection with orgMintedUUID '${orgMintedUUID}'`
        switch(err.status) {
          case 400: {
            return helpers.paratooErrorHandler(400, err, msg)
          }
          default: {
            return helpers.paratooErrorHandler(err?.status || 500, err)
          }
        }
      })
    
    if (!updatedIdentifier) {
      let msg = `Failed to update the success status of the submitted collection with orgMintedUUID '${orgMintedUUID}'`
      return helpers.paratooErrorHandler(500, new Error(msg))
    }

    //no errors so assume it's good
    return { success: true }
  },
  /** 
   * Handler function for identifier status endpoint, used for checking if a collection 
   * has been marked as submitted, i.e., the collection's orgMintedUUID has an associated
   * core submit time.
   * 
   * @param {Object} ctx Koa context
   * @param {String} ctx.params.identifier the orgMintedUUID
   * @returns {Boolean} true or false if the identifier has been stored or not
   */
  async identifierStatus(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const found = await strapi.db.query('api::collection-identifier.collection-identifier')
      .findOne({
        where: {
          survey_metadata: {
            orgMintedUUID: ctx.params.identifier,
          },
        },
      })
      .catch(err => {
        //we don't get provided useful messages to check, so we just check the status
        switch(err.status) {
          case 400: {
            return helpers.paratooErrorHandler(400, err)
          }
          default: {
            return helpers.paratooErrorHandler(err?.status || 500, err)
          }
        }
      })
    if(found) {
      if (found?.core_submit_time) {
        //collection has occurred
        return { isSubmitted: true }
      } else {
        return { isSubmitted: false }
      }
    }

    //cannot check the submissions status when there's no /mint-identifier call
    return helpers.paratooErrorHandler(
      404,
      new Error(`No data set found with mintedCollectionId=${ctx.params.identifier}`),
      'Unable to check the collection submission status',
    )
  },
  /**
   * Checks the supplied JWT is valid
   * 
   * @param {Object} ctx Koa context
   * @param {String} ctx.request.body.token JWT Token
   */
  async validateToken(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const token = ctx.request.body.token
    if (!token) return false
    try {
      //strapi doc (https://strapi.io/documentation/developer-docs/latest/guides/jwt-validation.html#write-our-own-logic)
      //says we could use jwt.getToken, but that expects the token to be attached to the
      //header, which we can't do as that request would be rejected before getting here.
      //so we directly use jwt.verify, which is the same function that jwt.getToken uses
      
      // remove bearer prefix, validate just the token
      await strapi.plugins['users-permissions'].services.jwt.verify(token.replace('Bearer ', ''))
    } catch (err) {
      switch(err.message) {
        case 'Invalid token.': {
          ctx.response.status = 401
          return { valid: false }
        }
        //should throw `invalid token.`, but if it doesn't then we handle
        default: {
          return helpers.paratooErrorHandler(500, err)
        }
      }
    }
    //didn't throw an error so token is valid
    return { valid: true }
  },
  /**
   * @deprecated
   * 
   * Retrieves the user's roles on a per-project basis. While Strapi has global roles for
   * users, MERIT does not, so we implement it like this such that 3rd party
   * implementations of this interface can have the flexibility to define role son a
   * per-project basis
   *
   * @param {Object} ctx Koa context
   * @returns {Object} an object with key(s) corresponding to the project ID (or UUID),
   * and value being an object containing the user's role (keys `name` and `type`)
   */
  async userRole(ctx) {
    const userId = ctx.state.user.id
    const userRecord = await strapi.db
      .query('plugin::users-permissions.user')
      .findOne({
        where: { id: userId },
        populate: true
      })
    if (userRecord?.role?.id) {
      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
      const userProjAndProt = await helpers.getProjAndProtoForUser(userId, PERMISSION_TYPE.read)
      const roleForProjects = {}
      for (const projProt of userProjAndProt.projects) {
        roleForProjects[projProt.id] = userRecord.role
      }
      //use `isEmpty` for easy empty object check
      if (!_.isEmpty(roleForProjects)) {
        return roleForProjects
      }
    }
    return null
  },
  /*
   * Imported WebAuthn methods
   * They're in their own controller to keep this one tidy, but if we
   * don't import them then documentation (and thus validator) breaks
   */
  async generateChallenge(ctx) {
    return webauthnController.generateChallenge(ctx)
  },
  async verifyWebAuthnRegister(ctx) {
    return webauthnController.verifyWebAuthnRegister(ctx)
  },
  async generateLoginChallenge(ctx) {
    return webauthnController.generateLoginChallenge(ctx)
  },
  async verifyWebAuthnLogin(ctx) {
    return webauthnController.verifyWebAuthnLogin(ctx)
  }
}
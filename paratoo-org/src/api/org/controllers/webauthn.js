'use strict'
const crypto = require('crypto')
const utf8Decoder = new TextDecoder('utf-8')
const utf8Encoder = new TextEncoder('utf-8')
const cbor = require('cbor')
const coseToJwk = require('cose-to-jwk')
const jwkToPem = require('jwk-to-pem')
//which public key types the server deems acceptable
const pubKeyCredParams = [{alg: -7, type: 'public-key'}]

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  /**
   * Handler function for generate challenge endpoint. Generates a challenge, which is 
   * used for the creation of a credential. Creates object of challenge (random 16-byte 
   * string), relying party, user (id, username, email), and public key credential 
   * parameters (which public key types the server deems acceptable)
   * 
   * @param {Object} ctx Koa context
   * @returns {Object} PublicKeyCredentialsCreationsOptions
   */
  async generateChallenge(ctx) {
    //generate challenge (random info, at least 16 bytes)
    var challenge = ''
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    for(var i = 0; i < 16; i++) {
      challenge += characters.charAt(Math.floor(Math.random() * characters.length))
    }

    //construct user data
    const userInfo = ctx.request.body.data.userInfo
    const user = { 'id': userInfo.userId, 'name': userInfo.username, 
      'displayName': userInfo.userEmail }

    //construct return object
    //FIXME may need to include RP ID
    const PublicKeyCredentialsCreationsOptions = { 'challenge': challenge, 
      'rp': { 'name': 'Paratoo-Org' }, 'user': user, 'pubKeyCredParams': pubKeyCredParams }

    //return challenge, user info, and relying party info
    return PublicKeyCredentialsCreationsOptions
  },
  /**
   * Handler function for verify WebAuthn registration endpoint. Performs the registration
   * ceremony as dictated by the [WebAuthn specification](https://www.w3.org/TR/webauthn-2/#sctn-registering-a-new-credential).
   * If the credential is valid, it will be stored for use in the authentication ceremony.
   * 
   * FIXME: this is implemented such that is *works*, but it is not up to specification.
   * 
   * @param {Object} ctx Koa Context
   * @returns {Boolean} true if the ceremony is successful
   */
  async verifyWebAuthnRegister(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const requestBody = ctx.request.body
    // console.log('ctx verify: ' + JSON.stringify(requestBody))

    const credResp = requestBody.encodedCredential.response

    //spec step 5-6
    const decodedClientDataJSON = utf8Decoder.decode(helpers.strToBin(credResp.clientDataJSON))
    console.log('decoded client data: ' + decodedClientDataJSON)

    //spec step 7: verify that the value of decodedClientDataJSON.type is webauthn.create
    if(JSON.parse(decodedClientDataJSON).type != 'webauthn.create') {
      strapi.log.debug('client data type is NOT ok')  //FIXME
    } else {
      strapi.log.debug('client data type is ok')
    }

    //spec step 8: verify that the value of decodedClientDataJSON.challenge equals the
    //base64url encoding of options.challenge
    //FIXME the challenge that's embeded in the client data seems to be different than
    //the one supplied in publicKeyCredentialCreationOptions
    //https://chromium.googlesource.com/chromium/src/+/refs/heads/main/content/browser/webauth/client_data_json.md
    console.log('challenge bits: ' + utf8Encoder.encode(JSON.parse(decodedClientDataJSON).challenge))

    //spec step 9: verify that the value of decodedClientDataJSON.origin matches the 
    //Relying Party's origin
    //FIXME origin is going to be the webapp's not RP's origin

    //spec step 10
    const clientDataHash = crypto.createHash('sha256')
      .update(credResp.clientDataJSON).digest()
    console.log('clientDataHash: ' + helpers.binToStr(clientDataHash))

    //spec step 11: to obtain the attestation statement format fmt, the authenticator 
    //data authData, and the attestation statement attStmt
    const decodedAttestationObject = cbor.decode(helpers.strToBin(credResp.attestationObject))
    // console.log('decodedAttestationObject: ' + JSON.stringify(decodedAttestationObject))

    //spec step 12: verify that the rpIdHash in authData is the SHA-256 hash of the 
    //RP ID expected by the Relying Party
    //FIXME origin is going to be the webapp's not RP's origin
    const authDataBits = helpers.strToBin(decodedAttestationObject.authData)
    console.log('authDataBits: ' + authDataBits)
    const rpIdHashBits = authDataBits.slice(0, 32)
    console.log('rpIdHashBits: ' + rpIdHashBits)

    //get the auth data flag field in binary
    const flagsBits = authDataBits.slice(32,33)
    const flagsBinaryArr = Array.from((flagsBits >>> 0).toString(2))
    console.log('flagsBinaryArr: ' + flagsBinaryArr)

    //TODO properly implement (SHOULD be used)
    //https://www.w3.org/TR/webauthn-2/#signature-counter
    const signCountBits = authDataBits.slice(33, 37)
    console.log(`signCountBits: ${signCountBits}`)

    //attestedCredentialData
    const aaguid = authDataBits.slice(37, 37 + 16)
    console.log(`aaguid: ${aaguid}`)
    const credentialIdLengthBinArr = authDataBits.slice(37 + 16, 37 + 16 + 2)
    console.log(`credentialIdLengthBinArr: ${credentialIdLengthBinArr}`)
    console.log(`credentialIdLengthBinArr length: ${credentialIdLengthBinArr.length}`)
    const credentialIdLengthBin = 
      (credentialIdLengthBinArr[0] >>> 0).toString(2) +
      (credentialIdLengthBinArr[1] >>> 0).toString(2)
    console.log(`credentialIdLengthBin: ${credentialIdLengthBin}`)
    const credentialIdLength = parseInt(credentialIdLengthBin, 2)
    console.log(`credentialIdLength: ${credentialIdLength}`)
    const credentialIdBinArr = authDataBits.slice(55,55 + credentialIdLength)
    console.log(`credentialIdBinArr: ${credentialIdBinArr}`)
    console.log(`credentialIdBinArr str: ${helpers.binToStr(credentialIdBinArr)}`)
    const credentialPublicKeyLength = 43    //FIXME (see extensions)
    const credentialPublicKeyBinArr	= authDataBits.slice(55 + credentialIdLength, 
      55 + credentialIdLength + credentialPublicKeyLength)
    console.log(`credentialPublicKeyBinArr: ${credentialPublicKeyBinArr}`)
    const credentialPublicKey = helpers.binToStr(credentialPublicKeyBinArr)
    console.log(`credentialPublicKey: ${credentialPublicKey}`)

    //extensions
    //FIXME want to use this to detect if authenticator is platform (e.g., fingerprint,
    //Apple FaceID, etc.) or cross-platform.
    //not sure where attestedCredentialData and extensions begins, so can't do slice at
    //the correct point. currently using fixed value `43` as that appears to be the
    //lengths of credentialPublicKey, which I got through trial-and-error.
    //this get's us to the extensions, but doing a cbor.decode doesn't really give
    //anything useful
    const extensionsBinArr = authDataBits.slice(
      55 + credentialIdLength + credentialPublicKeyLength, authDataBits.length
    )
    console.log(`extensionsBinArr: ${extensionsBinArr}`)
    const extensions = cbor.decode(extensionsBinArr)
    console.log(extensions)
    console.log(JSON.stringify(extensions))

    //spec step 13: verify that the User Present bit of the flags in authData is set
    const userPresentBit = flagsBinaryArr[flagsBinaryArr.length - 1]
    if(userPresentBit != 1) {
      strapi.log.debug('user is NOT present')   //FIXME
    } else {
      strapi.log.debug('user is present')
    }

    //spec step 14: verify that the User Verified bit of the flags in authData is set
    //(if required)
    //NOTE WebAuthn devtool option 'supports user verification' should be checked
    const userVerificationBit = flagsBinaryArr[flagsBinaryArr.length - 3]
    if(userVerificationBit != 1) {
      strapi.log.debug('user is NOT verified')    //FIXME
    } else {
      strapi.log.debug('user is verified')
    }

    //spec step 15: verify that the "alg" parameter in the credential public key in 
    //authData matches the alg attribute of one of the items in options.pubKeyCredParams
    //FIXME iterate over pubKeyCredParams array (in case we specify more)
    if(decodedAttestationObject.attStmt.alg != pubKeyCredParams[0].alg) {
      strapi.log.debug('alg is NOT valid')    //FIXME
    } else {
      strapi.log.debug('alg is valid')
    }

    //spec step 16: verify that the values of the client extension outputs and the 
    //authenticator extension outputs in the extensions in authData are as expected
    //FIXME this is returning an empty object even when provided with extensions
    //it's supposed to return an ArrayBuffer
    console.log('clientExtensionResult: ' + JSON.stringify(requestBody.clientExtensionResult))

    //spec step 17: determine the attestation statement format by performing a USASCII 
    //case-sensitive match on fmt against the set of supported WebAuthn Attestation 
    //Statement Format Identifier values. Up-to-date list at:
    //https://www.iana.org/assignments/webauthn/webauthn.xhtml
    const supportedFmt = ['packed', 'tpm', 'android-key', 'android-safetynet', 'fido-u2f']
    if(!supportedFmt.includes(decodedAttestationObject.fmt)) {
      strapi.log.debug('fmt is NOT valid')    //FIXME
    } else {
      strapi.log.debug('fmt is valid')
    }

    //spec step 18: verify that attStmt is a correct attestation statement, 
    //conveying a valid attestation signature
    //FIXME
    const attStmtSig = helpers.binToStr(decodedAttestationObject.attStmt.sig)
    console.log('attStmtSig: ' + attStmtSig)

    //spec step 19: obtain a list of acceptable trust anchors (i.e. attestation root 
    //certificates) for that attestation type and attestation statement format fmt, 
    //from a trusted source or from policy
    //FIXME relied on output of step 18

    //spec step 20: assess the attestation trustworthiness using the outputs of the 
    //verification procedure in step 19
    //FIXME relies on output of step 18

    //spec step 21: check that the credentialId is not yet registered to any other user
    //TODO how will this be tested?
    const storedCredential = await strapi.query('device')
      .findOne({
        'user': ctx.state.user.id,
        'credentialId': requestBody.encodedCredential.rawId
      })
      .catch(err => {
        return helpers.paratooErrorHandler(500, err)
      })
    console.log('storedCredential: ' + JSON.stringify(storedCredential))
    if(storedCredential) {
      let msg = 'Another user has already stored this credential'
      return helpers.paratooErrorHandler(406, new Error(msg))
    } else {
      strapi.log.debug('supplied credential is unique')
    }

    //spec step 23: if the attestation statement attStmt successfully verified but is 
    //not trustworthy per step 21 above, throw error
    //FIXME relies on output of step 18

    //if we get here then none of the above checks failed, so return true
    strapi.log.debug('all checks passes, credential is valid')
    return true
  },
  /**
   * Handler function for generate login challenge endpoint. Generates a challenge for 
   * WebAuthn login (random 16-byte string) and looks up the User's existing credential.
   * Generates an object of challenge with the challenge string and stored credential ID
   * 
   * @param {Object} ctx Koa context
   * @returns {Object} publicKeyCredentialRequestOptions
   */
  async generateLoginChallenge(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    console.log('ctx: ' + JSON.stringify(ctx.request.body))
    //generate challenge (random info, at least 16 bytes)
    var challenge = ''
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    for(var i = 0; i < 16; i++) {
      challenge += characters.charAt(Math.floor(Math.random() * characters.length))
    }
    const found = await strapi.query('device')
      .findOne({
        'longLivedCookie': ctx.request.body.data.uuid
      })
      .catch(err => {
        return helpers.paratooErrorHandler(500, err)
      })
    if(!found) {
      let msg = 'Could not find the credential ID for this device'
      return helpers.paratooErrorHandler(404, new Error(msg))
    }
    const credentialId = found.credentialId

    //construct return object
    const publicKeyCredentialRequestOptions = 
      { 'challenge': challenge, 'credentialId': credentialId }

    //return challenge and user's credentialId
    return publicKeyCredentialRequestOptions
  },
  /**
   * Handler function for verify WebAuthn login endpoint. Performs the authentication
   * ceremony as dictated by the [WebAuthn specification](https://w3c.github.io/webauthn/#sctn-verifying-assertion).
   * Validates the provided login credential with the stored credential created during
   * registration.
   * 
   * FIXME: this is implemented such that is *works*, but it is not up to specification.
   * 
   * @param {Object} ctx Koa context
   * @returns {Object} JWT and user profile if the ceremony is successful
   */
  async verifyWebAuthnLogin(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const requestBody = ctx.request.body
    // console.log(`requestBody: ${JSON.stringify(requestBody)}`)

    //spec step 6: identify the user being authenticated and verify that this user 
    //is the owner of the public key credential source credentialSource identified
    //by credential.id
    //TODO

    //find user from supplied username
    //TODO added catch but haven't checked it doesn't bust
    const userHandle = requestBody.PublicKeyCredential.response.userHandle
    const user = await strapi.query('user', 'users-permissions')
      .findOne({
        username: userHandle
      })
      .catch(err => {
        return helpers.paratooErrorHandler(500, new Error(err))
      })
    console.log(`User: ${JSON.stringify(user)}`)

    //spec step 7: Using credential.rawId, look up the corresponding credential public
    //key and let storedCredential be that credential public key
    //TODO added catch but haven't checked it doesn't bust
    const rawIdView = new Uint8Array(helpers.strToBin(requestBody.PublicKeyCredential.rawId))
    // console.log(`rawIdView: ${helpers.binToStr(rawIdView)}`)
    const storedCredential = await strapi.query('device')
      .findOne({
        'user': user.id,
        'credentialId': helpers.binToStr(rawIdView)
      })
      .catch(err => {
        return helpers.paratooErrorHandler(500, new Error(err))
      })
    // console.log(`storedCredential: ${JSON.stringify(storedCredential)}`)

    const pubKeyCredResp = requestBody.PublicKeyCredential.response

    //spec step 8-10: get to clientDataJSON, authenticatorData, and signature
    const decodedClientDataJSON = utf8Decoder.decode(helpers.strToBin(pubKeyCredResp.clientDataJSON))
    console.log('decodedClientDataJSON: ' + decodedClientDataJSON)

    //spec step 11: verify that the value of decodedClientDataJSON.type is the string 
    //webauthn.get
    if(JSON.parse(decodedClientDataJSON).type != 'webauthn.get') {
      strapi.log.debug('client data type is NOT ok')  //FIXME
    } else {
      strapi.log.debug('client data type is ok')
    }

    //spec step 12: verify that the value of decodedClientDataJSON.challenge equals the
    //base64url encoding of options.challenge
    //FIXME options.challenge is never going to be the same as 
    //decodedClientDataJSON.challenge the supplied options.challenge gets changed when 
    //being placed into the assertion (which is where decodedClientDataJSON.challenge 
    //is derived)
    console.log('supplied client data: ' + JSON.parse(decodedClientDataJSON).challenge)
    console.log('stored client data: ' + storedCredential.clientDataJSON.challenge)
    if(JSON.parse(decodedClientDataJSON).challenge != storedCredential.clientDataJSON.challenge) {
      strapi.log.debug('client data challenge is NOT ok')  //FIXME
    } else {
      strapi.log.debug('client data challenge is ok')
    }

    //spec step 13: verify that the value of decodedClientDataJSON.origin matches 
    //the Relying Party's origin
    if(JSON.parse(decodedClientDataJSON).origin != storedCredential.clientDataJSON.origin) {
      strapi.log.debug('client data origin is NOT ok')  //FIXME
    } else {
      strapi.log.debug('client data origin is ok')
    }

    const authDataBits = helpers.strToBin(pubKeyCredResp.authenticatorData)

    //spec step 14: verify that the rpIdHash in authData is the SHA-256 hash of the 
    //RP ID expected by the Relying Party
    const rpIdHashBits = authDataBits.slice(0, 32)
    const rpIdHashStr = helpers.binToStr(rpIdHashBits)
    const storedRpId = storedCredential.clientDataJSON.origin.split('//')[1]
    const hashedStoredRpId = helpers.binToStr(crypto.createHash('sha256').update(storedRpId).digest())
    if(rpIdHashStr != hashedStoredRpId) {
      strapi.log.debug('client data RP ID is NOT ok')  //FIXME
    } else {
      strapi.log.debug('client data RP ID is ok')
    }

    //get the auth data flag field in binary
    const flagsBits = authDataBits.slice(32,33)
    const flagsBinaryArr = Array.from((flagsBits >>> 0).toString(2))
    
    //spec step 15: verify that the User Present bit of the flags in authData is set
    const userPresentBit = flagsBinaryArr[flagsBinaryArr.length - 1]
    if(userPresentBit != 1) {
      strapi.log.debug('user is NOT present')   //FIXME
    } else {
      strapi.log.debug('user is present')
    }

    //spec step 16: verify that the User Verified bit of the flags in authData is set
    const userVerificationBit = flagsBinaryArr[flagsBinaryArr.length - 3]
    if(userVerificationBit != 1) {
      strapi.log.debug('user is NOT verified')    //FIXME
    } else {
      strapi.log.debug('user is verified')
    }

    //spec step 17: Verify that the values of the client extension outputs in 
    //clientExtensionResults and the authenticator extension outputs in the extensions
    //in authData are as expected
    //FIXME our implementation doesn't use extensions, but the spec says we should handle

    //spec step 18-19: verify the stored public key matches the signature
    //the signature was generated using theauthenticatorData bytes and a SHA-256 
    //hash of the clientDataJSON
    const clientDataHash = crypto.createHash('sha256')
      .update(helpers.strToBin(pubKeyCredResp.clientDataJSON)).digest()
    const signatureBase = Buffer.concat([authDataBits, clientDataHash])
    const signatureBuff = helpers.strToBin(pubKeyCredResp.signature)
    const jwk = coseToJwk(helpers.strToBin(storedCredential.publicKey))
    const pem = jwkToPem(jwk)
    const verify = crypto.verify('sha256', signatureBase, pem, signatureBuff)

    //spec step 20: if authData.signCount is nonzero or storedSignCount is nonzero, 
    //and if authData.signCount is:
    //  -greater than storedSignCount: update storedSignCount to be the value of 
    //   authData.signCount
    //  -less than or equal to storedSignCount: this is a signal that the authenticator
    //   may be cloned. We could still update the storedSignCount, or fail the ceremony
    //FIXME

    //spec step 21: return success/failure to client
    strapi.log.debug(`User is ${verify ? 'valid' : 'NOT valid'}`)
    if(verify) {
      let jwt
      try {
        jwt = strapi.plugins['users-permissions'].services.jwt.issue({
          id: user.id,
        })
        strapi.log.debug('JWT is issued to user')
      } catch(err) {
        return helpers.paratooErrorHandler(500, err)
      }
      return { jwt: jwt, user: user }
    }
    return ctx.throw(403, 'Could not verify user credentials')
  },
}
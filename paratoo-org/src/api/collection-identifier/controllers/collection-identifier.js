'use strict'

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 * 
 * We have a Strapi collection type to store collection identifiers but don't use this
 * controller as we just do a direct strapi.query()
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::collection-identifier.collection-identifier')

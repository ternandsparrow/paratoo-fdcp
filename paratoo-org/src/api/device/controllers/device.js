'use strict'
const CBOR = require('cbor')

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  /**
   * Overridden findOne controller function, used for finding a particular stored 
   * WebAuthn device.
   * 
   * @param {Object} ctx Koa context
   * @returns if the device was found or not
   */
  async findOne(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const uuid = (ctx.request.url).split('/')[2]
    const found = await strapi.query('device')
      .findOne({
        longLivedCookie: uuid
      })
      .catch(err => {
        switch(err.message) {
          default: {
            return helpers.paratooErrorHandler(500, err)
          }
        }
      })
    if(found) {
      ctx.body = { 'found': true }   //to keep the webapp happy, else it returns 404
    } else {
      ctx.body = { 'found': false }
    }
    
    return found    //FIXME what is this doing?
  },
  /**
   * Overridden create controller function, used to store a registered WebAuthn device
   * 
   * @param {Object} ctx Koa context
   * @returns {Object} the stored entry
   */
  async create(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const requestBody = ctx.request.body.params
    // console.log('requestBody: ' + JSON.stringify(requestBody))
    // console.log('decoded rawId: ' + helpers.strToBin(requestBody.credential.rawId))

    //parsing clientDataJSON
    const utf8Decoder = new TextDecoder('utf-8')
    const decodedClientData = utf8Decoder.decode(helpers.strToBin(requestBody.credential.response.clientDataJSON))
    const clientDataObj = JSON.parse(decodedClientData)
    // console.log('clientDataObj: ' + JSON.stringify(clientDataObj))

    //parsing attestationObject
    const decodedAttestationObject = CBOR.decode(helpers.strToBin(requestBody.credential.response.attestationObject))
    // console.log('decodedAttestationObject: ' + JSON.stringify(decodedAttestationObject))

    //parsing authenticator data
    const { authData } = decodedAttestationObject
    const dataView = new DataView(new ArrayBuffer(2))
    const idLenBytes = authData.slice(53, 55)
    idLenBytes.forEach((value, index) => dataView.setUint8(index, value))
    const credentialIdLength = dataView.getUint16()
    const credentialIdBytes = authData.slice(55, 55 + credentialIdLength)
    const publicKeyBytes = authData.slice(55 + credentialIdLength)
    // console.log('credentialIdBytes: ' + JSON.stringify(credentialIdBytes))
    // console.log('publicKeyBytes: ' + JSON.stringify(publicKeyBytes))
    // console.log('helpers.binToStr(publicKeyBytes): ' + helpers.binToStr(publicKeyBytes))
    // console.log('helpers.binToStr(credentialIdBytes): ' + helpers.binToStr(credentialIdBytes))

    // const isDraft_ = isDraft(requestBody, strapi.models.device)
    // const validData = await strapi.entityValidator.validateEntityCreation(
    //   strapi.models.device,
    //   requestBody,
    //   { isDraft_ }
    // )
    // const entry = await strapi.query('device').create(validData)
    const entry = await strapi.query('device').create({
      'longLivedCookie': requestBody.longLivedCookie,
      'challenge': requestBody.challenge,
      'user': requestBody.user,
      // 'publicKey': JSON.stringify(publicKeyBytes),
      // 'credentialId': JSON.stringify(credentialId),
      'publicKey': helpers.binToStr(publicKeyBytes),
      'credentialId': helpers.binToStr(credentialIdBytes),
      'clientDataJSON': clientDataObj
    })
      .catch(err => {
        ctx.body = { 'success': false }
        switch(err.message) {
          case 'Duplicate entry': {
            let msg = 'This device is already registered for this user'
            return helpers.paratooErrorHandler(400, err, msg)
          }
          default: {
            return helpers.paratooErrorHandler(500, err)
          }
        }
      })
    ctx.body = { 'success': true }   //to keep the webapp happy, else it returns 404
    return entry
  },
  /**
   * Overridden delete controller function, use to remove a stored WebAuthn device
   * 
   * @param {Object} ctx Koa context
   */
  async delete(ctx) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const uuid = (ctx.request.url).split('/')[2]
    await strapi.query('device')
      .delete({
        longLivedCookie: uuid
      })
      .catch(err => {
        switch(err.message) {
          default: {
            return helpers.paratooErrorHandler(500, err)
          }
        }
      })
    ctx.body = { 'success': true }   //to keep the webapp happy, else it returns 404
  }
}

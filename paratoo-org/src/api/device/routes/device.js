module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/devices/:uuid',
      handler: 'device.findOne',
      config: { policies: [] }
    },
    {
      method: 'POST',
      path: '/devices',
      handler: 'device.create',
      config: { policies: [] }
    },
    {
      method: 'DELETE',
      path: '/devices/:uuid',
      handler: 'device.delete',
      config: { policies: [] }
    }
  ]
}
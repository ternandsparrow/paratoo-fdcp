'use strict'

/**
 * lut-protocol-family controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-protocol-family.lut-protocol-family')

'use strict'

/**
 * lut-protocol-family router
 */

const { createCoreRouter } = require('@strapi/strapi').factories

module.exports = createCoreRouter('api::lut-protocol-family.lut-protocol-family')

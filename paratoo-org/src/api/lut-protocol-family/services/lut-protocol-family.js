'use strict'

/**
 * lut-protocol-family service
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-protocol-family.lut-protocol-family')

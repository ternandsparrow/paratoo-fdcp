'use strict'

module.exports = {
  //disable lint warnings for 'unused' atob and btoa
  /*eslint-disable */
  binToStr: function(bin) {
    return btoa(new Uint8Array(bin).reduce(
      (s, byte) => s + String.fromCharCode(byte), ''
    ))
  },
  /*eslint-enable */
}
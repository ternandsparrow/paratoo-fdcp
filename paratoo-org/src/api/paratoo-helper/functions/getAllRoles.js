module.exports = {
  getAllRoles: async function getAllRoles() {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    return await strapi.db
      .query('plugin::users-permissions.role')
      .findMany()
      .catch((err) => {
        helpers.paratooErrorHandler(500, err)
      })
  }
}
'use strict'
const { customRules } = require('../../../customRules')
const PERMISSION_TYPE = {
  read: 'read',
  write: 'write',
}

module.exports = {
  /**
   * Validates internal custom logics
   *
   * @param {Object} ctx koa object
   * @returns {Boolean/String} returns true if no error found
   */
  internalValidator: async function (ctx) {
    if (!customRules) return true

    const handler = ctx.state.route.path
    const api = handler.replace('/api/', '').replace('/org/', '')
    if (!customRules[api]) return true

    const payload = ctx.request.body
    const userId = ctx.state.user.id
    const params = ctx.params
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

    // returns error message if any rule fails
    for (const assignedRule of customRules[api]) {
      const result = await helpers.applyCustomRules(
        assignedRule,
        payload,
        userId,
        params,
      )
      if (result !== true) {
        return {
          errorsText: result.toString(),
        }
      }
    }
    return true
  },

  /**
   * Apply rules based on rule type
   *
   * @param {Object} assignedRule
   * @returns {Object} payload
   */
  applyCustomRules: async function (assignedRule, payload, userId, params) {
    const data = Array.isArray(payload) ? payload : [payload]
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')

    let result = null
    let userProjAndProt = null
    let userProjectIds = []
    let userProtocolIds = []
    let projectId = null
    let protocolId = null

    if (userId) {
      userProjAndProt = await helpers.getProjAndProtoForUser(
        userId,
        PERMISSION_TYPE.write,
      )
      userProjectIds = userProjAndProt.projects.map((e) => e.id.toString())
      userProjAndProt.projects.forEach((p) => {
        const protocols = p.protocols.map((e) => e.identifier)
        userProtocolIds = userProtocolIds.concat(protocols)
      })
    }
    switch (assignedRule.ruleType) {
      // validates project ID
      case 'surveyMetadataProjectCheck':
        for (const value of data) {
          projectId = value.survey_metadata.survey_details.project_id
          result = assignedRule.rule(projectId, userProjectIds)
          // if fails to validate
          if (result !== true) {
            return result
          }
        }
        break
      // validates protocol ID
      case 'surveyMetadataProtocolCheck':
        for (const value of data) {
          protocolId = value.survey_metadata.survey_details.protocol_id
          result = assignedRule.rule(protocolId, userProtocolIds)
          // if fails to validate
          if (result !== true) {
            return result
          }
        }
        break
      case 'uuidIdentifierCheck':
        result = assignedRule.rule(params.identifier)
        // if fails to validate
        if (result !== true) {
          return result
        }
        break
      default:
        // extend new custom types here..
        break
    }
    return true
  },
}

'use strict'

module.exports = {
  underscoreToDash: function(str) {
    return str.replace(/_/g, '-')
  }
}
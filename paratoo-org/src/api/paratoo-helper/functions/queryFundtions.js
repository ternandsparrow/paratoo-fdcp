module.exports = {
  // query with deep population
  deepPopulateQuery: async function (
    model,
    where,
    populate = null,
    isMultiple = false,
  ) {
    const api = `api::${model}.${model}`
    let object = null
    if (!isMultiple) {
      // find object
      object = await strapi.db.query(api).findOne({
        populate: populate ? populate : true,
        where: where,
      })
      if (!object) return null

      // deep populate
      object = await strapi.entityService.findOne(api, object.id, {
        populate: 'deep',
      })
      return object
    }

    // find objects
    const objects = await strapi.db.query(api).findMany({
      populate: populate ? populate : true,
      where: where,
    })
    // deep populate
    let deepObjects = []
    for (const o of objects) {
      deepObjects.push(
        await strapi.entityService.findOne(api, o.id, {
          populate: 'deep',
        }),
      )
    }
    return deepObjects
  },
}

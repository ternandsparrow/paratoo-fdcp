'use strict'

const path = require('path')
const fs = require('fs-extra')
const docCache = {}

module.exports = {
  /**
   * Finds and returns the combined documentation object (for local use)
   * The equivalent functionality in documentation services expects a ctx
   *
   * @returns An object of the OpenAPI schema for all APIs
   */
  readFullDocumentation: function() {
    const docServices = strapi.plugin('documentation').service('documentation')

    const docPath = path.resolve(
      docServices.getFullDocumentationPath(),
      docServices.getDocumentationVersion(),
      'full_documentation.json',
    )
    
    if (docCache.cache) {
      strapi.log.debug('Using documentation cache')
      return docCache.cache
    }

    const documentation = fs.readFileSync(docPath, 'utf8')
    const parsed = JSON.parse(documentation)
    docCache.cache = parsed
    return parsed
  }
}
'use strict'

module.exports = {
  validInt: function (value) {
    if (!value) return false

    let v = value.toString()
    // Remove whitespace
    v = v.trim().replace(/^\s+|\s+$/gm, '')
    strapi.log.verbose(`parse env integer result : ${v}`)
    try {
      return parseInt(v)
    } catch (error) {
      console.warn(`failed to parse env: ${value}, defaulting to 0`)
      return 0
    }
  },
  validBoolean: function (value) {
    if (!value) return false

    let v = value.toString()
    // Remove whitespace
    v = v.trim().replace(/^\s+|\s+$/gm, '')
    if (v == 'true') return true
    return false
  },
  validString: function (value) {
    if (!value) return ''
    return value.trim().replace(/^\s+|\s+$/gm, '')
  },
}

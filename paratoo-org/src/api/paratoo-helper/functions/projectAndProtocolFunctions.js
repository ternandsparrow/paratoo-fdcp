'use strict'
const { roles } = require('../../../roles')
const { roleRestrictions } = require('../../../roleRestrictions')

module.exports = {
  /**
   * Queries the data base and gets the projects for a User and associated protocols
   *
   * @param {number} userId
   * @param {string} permissionType read or write
   * @returns {Object} projects assigned to the User with associated protocols
   */
  getProjAndProtoForUser: async function (userId, permissionType) {
    const userRecord = await strapi.db
      .query('plugin::users-permissions.user')
      .findOne({
        where: { id: userId },
        populate: true,
      })
    if (!userRecord) {
      return 'no user'
    }
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const userProjectRecords = await helpers.getAuthorisedProjects(
      userRecord,
      permissionType,
    )
    if (!userProjectRecords) {
      return 'no user projects'
    }
    return {
      projects: userProjectRecords.map((proj) => ({
        id: proj.id,
        name: proj.name,
        protocols: proj.protocols.map((prot) => ({
          id: prot.id,
          identifier: prot.identifier,
          name: prot.name,
          version: prot.version,
          module: prot.module,
        })),
        project_area: {
          type: proj.project_area_type,
          coordinates: proj.project_area_coordinates.map((plot) => ({
            lat: plot.lat,
            lng: plot.lng,
          })),
        },
        plot_selections: proj.plot_selections.map((plot) => ({
          name: plot.plot_label,
          uuid: plot.uuid,
        })),
        //the same role for each project, but other implementations may have roles on a
        //per-project basis
        role: userRecord.role.type,
      })),
    }
  },

  // get the list of all authorized protocols
  getAuthorisedProjects: async function (userRecord, permissionType) {
    const roleConfigs = roles.find(
      (r) => r.name === userRecord.role.name && r.type === userRecord.role.type,
    )
    // if doest have read access we will return null
    if (!roleConfigs.isActive) return null
    if (!roleConfigs.actions.includes(permissionType)) return null
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const userProjectIds = userRecord.projects.map((e) => e.id)
    const userProjectRecords = await strapi.db
      .query('api::project.project')
      .findMany({
        where: { id: userProjectIds },
        populate: true,
      })

    // checks projects authorizations
    const authorisedProjects = userProjectRecords.filter(function (o) {
      const permission = helpers.checkProjectAuthorisation(o.name, userRecord.role.type)
      if (permission.authorised) {
        return o
      }
    })

    // checks authorizations protocols including all the associated models
    let authorisedProjectsWithProtocols = []
    authorisedProjects.forEach((project) => {
      let authorisedProtocols = []
      project.protocols.forEach((protocol) => {
        const protPermission = helpers.checkProtocolAuthorisation(
          protocol.identifier,
          userRecord.role.type,
        )
        if (protPermission.authorised) {
          // checks permissions of all linked models as well
          let allModels = protocol['workflow'].map(function (o) {
            return o.modelName ? o.modelName : ''
          })
          // concat all the models
          protocol['workflow'].forEach((o) => {
            if (!o.relationOnAttributesModelNames) return
            allModels = allModels.concat(o.relationOnAttributesModelNames)
          })

          const modelsPermission = helpers.checkModelsAuthorisation(
            allModels,
            userRecord.role.type,
          )
          if (modelsPermission.authorised) {
            authorisedProtocols.push(protocol)
          }
        }
      })
      project.protocols = authorisedProtocols
      authorisedProjectsWithProtocols.push(project)
    })

    return authorisedProjectsWithProtocols
  },

  // check if the project is authorised or not
  checkProjectAuthorisation: function (projectname, roleType) {
    const projectConfig = roleRestrictions.projects.find(
      (r) => r.name === projectname,
    )
    // if no restrictions found
    if (!projectConfig) return { authorised: true }
    // checks whether the role is authorised or not
    if (projectConfig.authorisedRoles.includes(roleType))
      return { authorised: true }

    return { authorised: false }
  },

  // check if the protocol is authorised or not
  checkProtocolAuthorisation: function (uuid, roleType) {
    const protocolConfig = roleRestrictions.protocols.find(
      (r) => r.uuid === uuid,
    )
    // if no restriction found
    if (!protocolConfig) return { authorised: true }
    // checks whether the role is authorised or not
    if (protocolConfig.authorisedRoles.includes(roleType))
      return { authorised: true }

    return { authorised: false }
  },

  // check if the models associated with protocol are authorised or not
  checkModelsAuthorisation: function (models, roleType) {
    for (const model of models) {
      const modelConfig = roleRestrictions.models.find((r) =>
        r.api.includes(model),
      )
      if (!modelConfig) continue
      // checks whether the role is authorised or not
      if (modelConfig.authorisedRoles.includes(roleType)) continue

      return { authorised: false }
    }
    return { authorised: true }
  },
}

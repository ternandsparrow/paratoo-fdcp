'use strict'

module.exports = {
  createUserWithRole: async function (username, userData, roleType) {
    let user = await strapi.query('plugin::users-permissions.user').findOne({
      where: { username: username },
    })
    if (user) return
    
    const roleOrm = strapi.query('plugin::users-permissions.role')
    const role = await roleOrm.findOne({
      where: { type: roleType ? roleType : 'collector' },
    })
    userData.role = role.id
    const result = await strapi.plugins['users-permissions'].services.user.add(
      userData,
    )
    return result
  },
}

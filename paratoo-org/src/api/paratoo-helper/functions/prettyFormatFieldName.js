'use strict'

module.exports = {
  /**
   * Formats a field name to a nicer looking format. Ideally this wouldn't be used
   * and instead there'd be a display name field in the documentation
   */
  prettyFormatFieldName: function(fieldName) {
    // the regex replaces all underlines and hyphens with spaces
    // The loop capitalises the first letter of every word
    let out = ''
    for (const word of fieldName.replace(/(-|_)/g, ' ').split(' ')) {
      out = `${out} ${word[0].toUpperCase()}${word.substring(1)}`
    }
    // remove leading space
    return out.trim()
  }
}
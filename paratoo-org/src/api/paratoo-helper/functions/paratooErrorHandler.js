const {
  ERROR_CODES,
} = require('../../../constants')

module.exports = {
  /**
   * Method that will cleanly construct an error that can be sent to the User
   * 
   * @param {number} code the HTTP status code
   * @param {Error} err the Error object
   * @param {String} [msg] optional message to append
   * @param {Boolean} [silent] whether to suppress logging the error in the console
   * @param {String} [errorCode] an optional error code to provide mode information to
   * the client about handling
   * 
   * @returns {Object} an error
   */
  paratooErrorHandler(code, err, msg, silent = false, errorCode = null) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const sentryService = helpers.getSentryService()
    helpers.paratooDebugMsg(
      `paratooErrorHandler called with code '${code}'${errorCode ? ` and '${errorCode}'`: ''}.\nGot error message: ${JSON.stringify(err?.message, null, 2)}.\nGot message: ${JSON.stringify(msg, null, 2)}`,
      true,
    )

    let message = 'An unknown error occurred'   //default if no matches for cases below
    const errorPath = JSON.stringify(err?.path)
    if(!msg && err?.message) {
      //if no message is provided then we use the Error obj message
      message = err.message
    } else if (msg && err?.message) {
      message = err.message + '. ' + msg
    } else if (msg && !err?.message) {
      message = msg
    }

    if(errorPath) {
      message += '. path: ' + errorPath
    }

    //even if the `silent` flag is set, we will do this log when it's a 500, as Strapi
    //doesn't preserve the message for 500 errors very well
    if(!silent || (silent && code === 500)) {
      strapi.log.error('statusCode: ' + code + '. message: ' + message)
    }
    // ctx.throw no longer available but equivalent to exposing and throwing error
    // https://koajs.com/#context see heading for ctx.throw
    // construct a new error with only equivalent aspects to ctx.throw
    let exposedErr = new Error(message)

    if (sentryService) {
      sentryService.sendError(exposedErr)
    }

    exposedErr.status = code

    if (errorCode) {
      const errorCodeIsStr = typeof errorCode === 'string'
      const errorCodeIsSupported = ERROR_CODES.includes(errorCode)
      if (!errorCodeIsStr) {
        helpers.paratooWarnHandler(`Programmer error: paratooErrorHandler provided with non-string errorCode ${errorCode}`)
      }
      if (!errorCodeIsSupported) {
        helpers.paratooWarnHandler(`Programmer error: paratooErrorHandler provided with unsupported errorCode ${errorCode}`)
      }
      if (
        errorCodeIsStr &&
        errorCodeIsSupported
      ) {
        let errDetails = {}
        if (typeof exposedErr?.details === 'object') {
          helpers.paratooDebugMsg(`Preserving existing error details ${JSON.stringify(exposedErr.details)}`, true)
          errDetails = {
            ...exposedErr.details
          }
        }

        errDetails.errorCode = errorCode
        exposedErr.details = errDetails
      }
    }

    exposedErr.expose = true

    throw exposedErr
  },
  paratooWarnHandler(msg, err = null) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const sentryService = helpers.getSentryService()

    let message = msg
    if (err !== null && err?.message) {
      message += '. ' + err.message
    }

    strapi.service('api::paratoo-helper.paratoo-helper').createSentryBreadcrumb({
      message: `paratooWarnHandler called.\nMessage: ${message}`,
    })

    strapi.log.warn(message)
    if (sentryService) {
      sentryService.sendError(new Error(`Warning: ${message}`))
    }
  },
}
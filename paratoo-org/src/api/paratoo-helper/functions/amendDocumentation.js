'use strict'
const path = require('path')
const fs = require('fs')
const _ = require('lodash')


const overridePaths = {
  '/org/pdp/{projectId}/{protocolUuid}/read': {
    get: {
      description:
        'Checks that a user has read permissions for the particular project and protocol',
      responses: {
        200: {
          description:
            'Returns if user has read permission for supplied project and protocol',
          content: {
            'application/json': {
              schema: {
                properties: {
                  isAuthorised: {
                    type: 'boolean',
                  },
                },
              },
            },
          },
        },
        403: {
          description: 'Forbidden',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 403,
                message: 'Forbidden',
              },
            },
          },
        },
        404: {
          description: 'Not found',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 404,
                message: 'Not found',
              },
            },
          },
        },
      },
      tags: ['Org'],
      summary: 'For authorizing with the PDP which checks read permissions',
      parameters: [
        {
          name: 'projectId',
          in: 'path',
          description: '',
          deprecated: false,
          required: true,
          schema: {
            type: 'string',
            format: 'project_id',   //custom format that we define in `is-validated`
          },
        },
        {
          name: 'protocolUuid',
          in: 'path',
          description: 'The protocols UUID `identifier`',
          deprecated: false,
          required: true,
          schema: {
            type: 'string',
            format: 'uuid',
          },
        },
      ],
    },
  },
  '/org/pdp/{projectId}/{protocolUuid}/write': {
    get: {
      description:
        'Checks that a user has write permissions for the particular project and protocol',
      responses: {
        200: {
          description:
            'Returns if user has write permission for supplied project and protocol',
          content: {
            'application/json': {
              schema: {
                properties: {
                  isAuthorised: {
                    type: 'boolean',
                  },
                },
              },
            },
          },
        },
        403: {
          description: 'Forbidden',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 403,
                message: 'Forbidden',
              },
            },
          },
        },
        404: {
          description: 'Not found',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 404,
                message: 'Not found',
              },
            },
          },
        },
      },
      tags: ['Org'],
      summary: 'For authorizing with the PDP which checks write permissions',
      parameters: [
        {
          name: 'projectId',
          in: 'path',
          description: '',
          deprecated: false,
          required: true,
          schema: {
            type: 'string',
            format: 'project_id',   //custom format that we define in `is-validated`
          },
        },
        {
          name: 'protocolUuid',
          in: 'path',
          description: 'The protocols UUID `identifier`',
          deprecated: false,
          required: true,
          schema: {
            type: 'string',
            format: 'uuid',
          },
        },
      ],
    },
  },
  '/org/user-projects': {
    get: {
      description: 'Gets all projects that a user is assigned to',
      responses: {
        200: {
          description: 'Returns all projects for an authenticated user',
          content: {
            'application/json': {
              schema: {
                properties: {
                  projects: {
                    type: 'array',
                    items: {
                      type: 'object',
                      properties: {
                        id: {
                          type: 'integer',
                        },
                        name: {
                          type: 'string',
                        },
                        protocols: {
                          type: 'array',
                          items: {
                            type: 'object',
                            properties: {
                              id: {
                                type: 'integer',
                              },
                              identifier: {
                                type: 'string',
                                format: 'uuid',
                              },
                              name: {
                                type: 'string',
                              },
                              version: {
                                type: 'integer',
                              },
                              module: {
                                type: 'string',
                              },
                            },
                          },
                        },
                        project_area: {
                          type: 'object',
                          properties: {
                            type: {
                              type: 'string',
                            },
                            coordinates: {
                              type: 'array',
                              items: {
                                type: 'object',
                                properties: {
                                  lat: {
                                    type: 'integer',
                                  },
                                  lng: {
                                    type: 'integer',
                                  },
                                },
                              },
                            },
                          },
                        },
                        plot_selections: {
                          type: 'array',
                          items: {
                            type: 'object',
                            properties: {
                              uuid: {
                                type: 'string',
                                format: 'uuid',
                              },
                              name: {
                                type: 'string',
                              },
                            },
                          },
                        },
                        role: {
                          type: 'string'
                        },
                      },
                    },
                  },
                },
              },
              example: {
                projects: [
                  {
                    id: 2,
                    name: 'Bird survey TEST Project',
                    protocols: [
                      {
                        id: 4,
                        identifier: 'd7179862-1be3-49fc-8ec9-2e219c6f3854',
                        name: 'Plot Layout and Visit',
                        version: 1,
                        module: 'Plot Selection and Layout',
                      },
                      {
                        id: 26,
                        identifier: 'c1b38b0f-a888-4f28-871b-83da2ac1e533',
                        name: 'Vertebrate Fauna - Bird Survey',
                        version: 1,
                        module: 'Vertebrate Fauna',
                      },
                    ],
                    project_area: {
                      type: 'Polygon',
                      coordinates: [
                        {
                          lat: -27.388252,
                          lng: 152.880694,
                        },
                        {
                          lat: -27.388336,
                          lng: 152.880651,
                        },
                        {
                          lat: -27.388483,
                          lng: 152.880518,
                        },
                      ],
                    },
                    plot_selections: [
                      {
                        name: 'QDASEQ0001',
                        uuid: 'a6f84a0e-2239-4bfa-aac6-6d2c99e252f1',
                      },
                      {
                        name: 'QDASEQ0003',
                        uuid: 'a6f84a0e-2239-4bfa-aac6-6d2c99e252f32',
                      },
                      {
                        name: 'QDASEQ0005',
                        uuid: 'a6f84a0e-2239-4bfa-aac6-6d2c99e252f3',
                      },
                      {
                        name: 'SATFLB0001',
                        uuid: 'a6f84a0e-2239-4bfa-aac6-6d2c99e252f4',
                      },
                    ],
                    role: 'collector',
                  },
                ],
              },
            },
          },
        },
        403: {
          description: 'Forbidden',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 403,
                message: 'Forbidden',
              },
            },
          },
        },
        404: {
          description: 'Not found',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 404,
                message: 'Not found',
              },
            },
          },
        },
      },
      tags: ['Org'],
      summary: 'Gets all projects for an authenticated user',
    },
  },
  '/org/mint-identifier': {
    post: {
      description:
        'Creates an identifier that is stored in Org as a cross-reference to Core, i.e., stores the survey metadata (surveys details, provenance, and orgMintedUUID), org mint time (eventTime), and reference to the user.\n\nAllows a particular survey to be derived from the data contained in the identifier. Users may not have connection to the server when they are performing a collection, so an identifier is created locally (the survey-metadata). When they are ready to submit the collection (i.e., have access to the server), they hit this endpoint to have the actual identifier minted. \n\n Org appends its provenance information (version and system) and orgMintedUUID to the identifier before the identifier (which is encoded as base64 stringified JSON) is returned as the API response. ',
      responses: {
        200: {
          description:
            'Returns the base64 encoded stringified JSON. The survey ID also contains the Org provenance information (version and system) and orgMintedUUID that Org appended. Before encoding, this JSON is created with keys: survey_metadata, eventTime (datetime), and userId.',
          content: {
            'application/json': {
              schema: {
                properties: {
                  orgMintedIdentifier: {
                    type: 'string',
                  },
                },
              },
              examples: [
                {
                  summary: 'Encoded identifier object that is returned as the response',
                  value: {
                    orgMintedIdentifier:
                    'eyJzdXJ2ZXlfbWV0YWRhdGEiOiB7ICJzdXJ2ZXlfZGV0YWlscyI6IHsgInN1cnZleV9tb2RlbCI6ICJiaXJkLXN1cnZleSIsICJ0aW1lIjogIjIwMjQtMDEtMDhUMDI6MDM6NTcuNzQzWiIsICJ1dWlkIjogImQyM2M1ZDZmLTkwMGMtNDY0OC1hODgxLTk3OThkNjVkMjBmMCIsICJwcm9qZWN0X2lkIjogIjEiLCAicHJvdG9jb2xfaWQiOiAiYTljYjllMzgtNjkwZi00MWM5LTgxNTEtMDYxMDhjYWY1MzlkIiwgInByb3RvY29sX3ZlcnNpb24iOiAiMSIgfSwgInVzZXJfZGV0YWlscyI6IHsgInJvbGUiOiAiY29sbGVjdG9yIiwgImVtYWlsIjogInRlc3R1c2VyQGVtYWlsLmNvbSIsICJ1c2VybmFtZSI6ICJ0ZXN0dXNlciIgfSwgInByb3ZlbmFuY2UiOiB7ICJ2ZXJzaW9uX2FwcCI6ICIwLjAuMS14eHh4eCIsICJ2ZXJzaW9uX2NvcmUiOiAiMC4wLjEteHh4eHgiLCAidmVyc2lvbl9jb3JlX2RvY3VtZW50YXRpb24iOiAiMC4wLjEteHh4eHgiLCAidmVyc2lvbl9vcmciOiAiNC40LVNOQVBTSE9UIiwgInN5c3RlbV9hcHAiOiAibW9uaXRvciIsICJzeXN0ZW1fY29yZSI6ICJNb25pdG9yIEZEQ1AgQVBJIiwgInN5c3RlbV9vcmciOiAibW9uaXRvciIgfSB9fQ==',
                  },
                },
                {
                  summary: 'The unencoded identifier',
                  value: {
                    orgMintedIdentifier: {
                      userId: 1,
                      eventTime: '2024-03-07T02:03:23.465Z',
                      survey_metadata: {
                        survey_details: {
                          survey_model: 'bird-survey',
                          time: '2024-03-06T23:30:12.304Z',
                          uuid: 'fdaf8903-e141-4f3a-9225-5ffc72de4d10',
                          project_id: '1',
                          protocol_id: 'c1b38b0f-a888-4f28-871b-83da2ac1e533',
                          protocol_version: '1',
                          submodule_protocol_id: ''
                        },
                        provenance: {
                          version_app: '0.0.1-xxxxx',
                          version_core_documentation: '0.0.0-xxxx',
                          system_app: 'Monitor FDCP API--localdev',
                          version_org: '0.0.0-xxxx',
                          system_org: 'Monitor FDCP API-development'
                        },
                        orgMintedUUID: '3e70eafb-73b8-4e57-864c-7faa4bcadcce'
                      }
                    },
                  },
                },
              ],
            },
          },
        },
        403: {
          description: 'Forbidden',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 403,
                message: 'Forbidden',
              },
            },
          },
        },
        404: {
          description: 'Not found',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 404,
                message: 'Not found',
              },
            },
          },
        },
      },
      tags: ['Org'],
      requestBody: {
        required: true,
        content: {
          'application/json': {
            schema: {
              required: ['survey_metadata'],
              type: 'object',
              properties: {
                survey_metadata: {
                  type: 'object',
                  required: [
                    'survey_details',
                    'provenance',
                  ],
                  properties: {
                    survey_details: {
                      type: 'object',
                      required: ['survey_model', 'time', 'uuid', 'project_id', 'protocol_id', 'protocol_version'],
                      properties: {
                        survey_model: {
                          type: 'string',
                          minLength: 1,
                        },
                        time: {
                          type: 'string',
                          format: 'date-time'
                        },
                        uuid: {
                          type: 'string',
                          format: 'uuid'
                        },
                        project_id: {
                          type: 'string',
                          minLength: 1,
                        },
                        protocol_id: {
                          type: 'string',
                          format: 'uuid'
                        },
                        protocol_version: {
                          type: 'string',
                          minLength: 1,
                        },
                        submodule_protocol_id: {
                          type: 'string'
                        },
                      },
                    },
                    provenance: {
                      type: 'object',
                      required: [
                        'version_app',
                        'version_core_documentation',
                        'system_app',
                      ],
                      properties: {
                        version_app: {
                          type: 'string'
                        },
                        //appended by core
                        version_core: {
                          type: 'string'
                        },
                        version_core_documentation: {
                          type: 'string',
                        },
                        //appended by org
                        version_org: {
                          type: 'string'
                        },
                        system_app: {
                          type: 'string'
                        },
                        //appended by core
                        system_core: {
                          type: 'string',
                        },
                        //appended by org
                        system_org: {
                          type: 'string',
                        },
                      },
                    },
                  },
                },
              },
            },
            example: {
              survey_metadata: {
                survey_details: {
                  survey_model: 'bird-survey',
                  time: '2024-03-06T23:30:12.304Z',
                  uuid: 'fdaf8903-e141-4f3a-9225-5ffc72de4d10',
                  project_id: '1',
                  protocol_id: 'c1b38b0f-a888-4f28-871b-83da2ac1e533',
                  protocol_version: '1',
                },
                provenance: {
                  version_app: '0.0.1-xxxxx',
                  version_core_documentation: '0.0.0-xxxx',
                  system_app: 'Monitor FDCP API--localdev'
                }
              }
            },
          },
        },
      },
      summary:
        'For creating and storing an identifier which is based off the userId, event time, and survey metadata (survey_details, provenance, orgMintedUUID)',
    },
  },
  '/org/collection': {
    post: {
      description: '',
      responses: {
        200: {
          description:
            'Returns true when it updates the record for the given orgMintedUUID\'s success.',
          content: {
            'application/json': {
              schema: {
                properties: {
                  success: {
                    type: 'boolean',
                  },
                },
              },
            },
          },
        },
        403: {
          description: 'Forbidden',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 403,
                message: 'Forbidden',
              },
            },
          },
        },
        404: {
          description: 'Not found',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 404,
                message: 'Not found',
              },
            },
          },
        },
      },
      tags: ['Org'],
      requestBody: {
        description:
          'Provide the orgMintedUUID of the collection that was submitted to record the successful submission of the collection. That is, append to the record of the orgMintedUUID the Core submission time and the Core provenance.',
        required: true,
        content: {
          'application/json': {
            schema: {
              required: [
                'orgMintedUUID',
                'coreProvenance',
              ],
              type: 'object',
              properties: {
                orgMintedUUID: {
                  type: 'string',
                  minLength: 1,
                },
                coreProvenance: {
                  required: [
                    'system_core',
                    'version_core',
                  ],
                  type: 'object',
                  properties: {
                    system_core: {
                      type: 'string',
                    },
                    version_core: {
                      type: 'string'
                    },
                  },
                }
              },
            },
            example: {
              orgMintedUUID: '3e70eafb-73b8-4e57-864c-7faa4bcadcce',
              coreProvenance: {
                system_core: 'Monitor FDCP API-development',
                version_core: '0.0.0-xxxx',
              },
            },
          },
        },
      },
      summary:
        'Allows Core to notify Org of a successful submitted collection.',
    },
  },
  '/org/get-all-collections': {
    get: {
      description:
        'Gets the list of all org minted uuids submitted by an authenticated user.',
      responses: {
        200: {
          description:
            'Returns the list of all org minted uuids submitted by the user.',
          content: {
            'application/json': {
              schema: {
                properties: {
                  collections: {
                    type: 'array',
                    items: {
                      type: 'string',
                    },
                  },
                },
              },
              example: {
                collections: [
                  '7fcddf5b-b58f-44de-98c6-f4e197266750',
                  '19e72d91-7abb-4d4c-ae75-488554835b84',
                  '009ce4c5-3ec5-4b61-bf25-43ea969b01e0',
                ],
              },
            },
          },
        },
        403: {
          description: 'Forbidden',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 403,
                message: 'Forbidden',
              },
            },
          },
        },
        404: {
          description: 'Not found',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 404,
                message: 'Not found',
              },
            },
          },
        },
      },
      tags: ['Org'],
      summary:
        'Gets the list of all org minted uuids submitted by an authenticated user.',
    },
  },
  '/org/status/{identifier}': {
    get: {
      deprecated: false,
      description:
        'Uses the orgMintedUUID to check if a collection has been successfully submitted. That is, Core has notified Org of a successful submission and Org has stored this success.',
      responses: {
        200: {
          description: 'Returns true if the collection has been successfully submit.',
          content: {
            'application/json': {
              schema: {
                properties: {
                  isSubmitted: {
                    type: 'boolean',
                  },
                },
              },
            },
          },
        },
        403: {
          description: 'Forbidden',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 403,
                message: 'Forbidden',
              },
            },
          },
        },
        404: {
          description: 'Not found',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 404,
                message: 'Not found',
              },
            },
          },
        },
      },
      tags: ['Org'],
      summary:
        'Allows Core to check if Org has been notified of a successful collection submission',
      parameters: [
        {
          name: 'identifier',
          in: 'path',
          description: 'The orgMintedUUID',
          deprecated: false,
          required: true,
          schema: {
            type: 'string',
            format: 'uuid'
          },
        },
      ],
    },
  },
  '/org/challenge': {
    post: {
      description:
        'The returned user ID is a string, as this is what WebAuthn expects. But remember that Paratoo expects user IDs to be integers.',
      responses: {
        200: {
          description:
            'Returns a generated challenge, replying party (rp) name, and user data',
          content: {
            'application/json': {
              schema: {
                properties: {
                  challenge: {
                    type: 'string',
                  },
                  rp: {
                    type: 'object',
                    properties: {
                      name: {
                        type: 'string',
                      },
                    },
                  },
                  user: {
                    type: 'object',
                    properties: {
                      id: {
                        type: 'string',
                      },
                      name: {
                        type: 'string',
                      },
                      displayName: {
                        type: 'string',
                      },
                    },
                  },
                },
              },
              example: {
                challenge: 'MSz8EzSI33BIA3fS',
                rp: {
                  name: 'Paratoo-Org',
                },
                user: {
                  id: 9081,
                  name: 'l_skyw@lker',
                  displayName: 'skywalker@tatooine.com',
                },
              },
            },
          },
        },
        403: {
          description: 'Forbidden',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 403,
                message: 'Forbidden',
              },
            },
          },
        },
        404: {
          description: 'Not found',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 404,
                message: 'Not found',
              },
            },
          },
        },
      },
      tags: ['Org'],
      requestBody: {
        description:
          'WebAuthn requires user info to be a user ID, name (not username), and display name. Given that Org does not use all these fields, we\'ve mapped them to fit. \n\nUser ID remains the same, the supplied username will be mapped to WebAuthn\'s \'name\' field, and the supplied email will be mapped to WebAuthn\'s \'display name\' field.',
        required: true,
        content: {
          'application/json': {
            schema: {
              required: ['userInfo'],
              type: 'object',
              properties: {
                userInfo: {
                  type: 'object',
                  required: ['userId', 'username', 'userEmail'],
                  properties: {
                    userId: {
                      type: 'integer',
                    },
                    username: {
                      type: 'string',
                      minLength: 1,
                    },
                    userEmail: {
                      type: 'string',
                      format: 'email',
                    },
                  },
                },
              },
            },
            example: {
              userInfo: {
                userId: 3154,
                username: 'l_skywalker',
                userEmail: 'skywalker@tatooine.com',
              },
            },
          },
        },
      },
      summary: 'Generates a challenge for use by WebAuthn when registering',
    },
  },
  '/org/login-challenge': {
    post: {
      description: '',
      responses: {
        200: {
          description:
            'Returns a generated challenge and credential ID (as a Uint8Array)',
          content: {
            'application/json': {
              schema: {
                properties: {
                  challenge: {
                    type: 'string',
                  },
                  credentialId: {
                    type: 'array',
                    items: {
                      type: 'integer',
                    },
                  },
                },
              },
            },
          },
        },
        403: {
          description: 'Forbidden',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 403,
                message: 'Forbidden',
              },
            },
          },
        },
        404: {
          description: 'Not found',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 404,
                message: 'Not found',
              },
            },
          },
        },
      },
      tags: ['Org'],
      requestBody: {
        description: '',
        required: true,
        content: {
          'application/json': {
            schema: {
              properties: {
                uuid: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
      summary: 'Generates a challenge for use by WebAuthn when authenticating',
    },
  },
  '/org/validate-token': {
    post: {
      description:
        'Before Core makes a PDP request via the project membership enforcer, it must check that the JWT it was provided is valid. As Org issues these JWTs, Core must check with Org to ensure the validity.',
      responses: {
        200: {
          description: '',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                  valid: {
                    type: 'boolean',
                  }
                },
              },
            },
          },
        },
        401: {
          description: '',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                  valid: {
                    type: 'boolean',
                  }
                },
              },
            },
          },
        },
      },
      tags: ['Org'],
      requestBody: {
        description: '',
        required: true,
        content: {
          'application/json': {
            schema: {
              required: ['token'],
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                  pattern: '^Bearer ',
                },
              },
            },
          },
        },
      },
      summary: 'Validates JWT tokens issued by Org',
    },
  },
  '/org/user-role': {
    get: {
      deprecated: true,
      description: 'While Strapi has global roles for users, MERIT does not, so we implement it like this such that 3rd party implementations of this interface can have the flexibility to define role son a per-project basis',
      responses: {
        200: {
          description: 'Returns the user\'s role(s) on a per-project basis. The type is an object, where the keys are the project ID or UUID, and the value is an object containing that role, with keys (at minimum) `name` (value is human-readable role name, e.g., \'Authenticated\' or \'Project Admin\') and `type` (value is raw role name, e.g., \'authenticated\' or \'project_admin\').\n\nThe example/schema below includes the response of a single project with ID \'1\'.',
          content: {
            'application/json': {
              schema: {
                properties: {
                  1: {
                    type: 'object',
                    required: [
                      'name',
                      'type',
                    ],
                    properties: {
                      id: {
                        type: 'integer',
                      },
                      name: {
                        type: 'string',
                      },
                      description: {
                        type: 'string',
                      },
                      type: {
                        type: 'string',
                      },
                    },
                  },
                },
              },
            },
          },
        },
        403: {
          description: 'Forbidden',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 403,
                message: 'Forbidden',
              },
            },
          },
        },
        404: {
          description: 'Not found',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 404,
                message: 'Not found',
              },
            },
          },
        },
      },
      tags: ['Org'],
      summary: 'Retrieves the user\'s role(s) on a per-project basis',
    },
  },
  '/token/refresh': {
    post: {
      responses: {
        200: {
          content: {
            'application/json': {
              schema: {
                properties: {
                  jwt: {
                    type: 'string',
                  },
                  refreshToken: {
                    type: 'string',
                  },
                },
              },
            },
          },
        },
        403: {
          description: 'Forbidden',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 403,
                message: 'Forbidden',
              },
            },
          },
        },
        404: {
          description: 'Not found',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                $ref: '#/components/schemas/Error',
              },
              example: {
                code: 404,
                message: 'Not found',
              },
            },
          },
        },
      },
      tags: ['Users-Permissions - Auth'],
      requestBody: {
        required: true,
        content: {
          'application/json': {
            schema: {
              required: ['refreshToken'],
              type: 'object',
              properties: {
                refreshToken: {
                  type: 'string'
                }
              },
            },
          },
        },
      },
      summary: 'Refreshes (re-issues) the user\'s JWT and creates a new refresh token',
    },
  },
}
// this function is to be run on document generation and make necessary
// changes based on our product requirements

// FIXME in the StrapiV4 merge we no longer individually modify each API's
// src/api/[...modelnames]/documentation/1.0.0/[modelname], and instead modify the merged documentation
// (because our documentaton plugin is using a WIP PR that only works in full_documentation.js)
// this is more brittle, and would likely break if 'generate documentation' is activated in the
// admin panel
// TODO so find less brittle way of doing this (or investigate if it's even an issue)

module.exports = {
  /**
   * gets a list of every api and amends their documentation, this is done so that
   * documentation generation will take these changes into account automatically.
   */
  amendFullApiDoc: async function () {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const docServices = strapi.plugin('documentation').service('documentation')
    // get a list of all the api's to change
    const apiList = docServices.getPluginAndApiInfo()

    // Get protocols so bulk documentation endpoints can be made
    const protocols = await strapi.db.query('api::protocol.protocol').findMany()

    let oldFullDocumentation
    let docpath
    try {
      docpath = path.resolve(
        docServices.getFullDocumentationPath(),
        docServices.getDocumentationVersion(),
        'full_documentation.json',
      )
      oldFullDocumentation = JSON.parse(fs.readFileSync(docpath))
    } catch (error) {
      console.error(error)
      throw new error(
        `Could not make Documentation changes (for bulk routes, etc.) because we cannot read full_documentation.json at '${docpath}'`,
      )
    }
    let accDocumentation = _.cloneDeep(oldFullDocumentation)

    for (const api of apiList) {
      const apiName = api.name

      try {
        accDocumentation = await amendDocObject(accDocumentation, apiName)
      } catch (error) {
        let msg = `Unable to amend documentation for ${api.name}. LUTs will break`
        strapi.log.error(msg)
        console.error(error)
        helpers.paratooErrorHandler(500, error, msg)
        continue
      }
    }

    // make bulk documentation after everything else because we need to copy
    // some fully amended components.schemas to override from id relation to $ref
    // and we don't want to modify the original component.schema
    for (const api of apiList) {
      const apiName = api.name

      const { pathName, bulkDoc } = makeBulkDocumentation(
        apiName,
        protocols,
        accDocumentation.components.schemas,
      )
      if (pathName && bulkDoc) accDocumentation.paths[pathName] = bulkDoc
    }
    // allow additional properties on specific schemas, which will allow `orgAuth` to be
    // applied to the request body
    const requestSchemasAdditionalProperties = ['OrgRequest']
    for (const schema of requestSchemasAdditionalProperties) {
      accDocumentation.components.schemas[schema].additionalProperties = true
    }
    const overridePath = Object.keys(overridePaths)
    // override api descriptions
    for (const path of overridePath) {
      accDocumentation.paths[path] = overridePaths[path]
    }

    //edge case for how we override the /auth/local endpoint to include a refresh token.
    //we cannot use `overridePaths`, as this will squash what Strapi generates by
    //default, as Strapi uses a $ref to `Users-Permissions-UserRegistration`
    Object.assign(accDocumentation.components.schemas['Users-Permissions-UserRegistration'].properties, {
      refreshToken: {
        type: 'string',
      },
    })
    // update build information
    accDocumentation.info.description = `Build: ${process.env.NODE_ENV} Version: ${process.env.VERSION} GitHash: ${process.env.GIT_HASH}`

    // Throw an error if a write is unsuccessful, because this is an essential process
    fs.writeFileSync(docpath, JSON.stringify(accDocumentation, null, 2))
    strapi.log.info('Amended documentation on all APIs for LUTs')
  },
}

/**
 * Converts the passed jsonDoc's LUT references with Enums. Returns the amended Documentation
 * as an object
 * @param {*} jsonDoc Full_documentation.json (can be partially amended already)
 * @param {String} apiName cross-references with this routes.json, and Core collection protocols to make bulk documentation
 */
async function amendDocObject(jsonDoc, apiName) {
  let fdSchemas = jsonDoc.components.schemas

  // Update LUTs components, and associated custom flags in all the components/schemas/NewSchema-etc
  for (const [key, value] of Object.entries(fdSchemas)) {
    let newValue = value
    // check if model exists, and it's a Post/Put
    if (
      typeof key === 'string' &&
      // remove hyphens because schemas in camelcase
      key.toLowerCase().match(`${apiName.replace(/-/g, '')}request`)
    ) {
      let modelRef
      try {
        modelRef = strapi.api[apiName].contentTypes[apiName]
      } catch (error) {
        // api doesn't exist
      }
      if (!modelRef) {
        strapi.log.warn(`Couldn't find model: ${apiName}`)
        continue
      }

      // add flags to component attributes, and resolve their LUT and reference flags
      newValue = await resolveComponentReferences(
        newValue,
        modelRef,
        key,
        apiName,
      )

      // Resolve top-level LUTs and reference flags
      newValue.properties.data = await resolveModelAssociations(
        newValue.properties.data,
        modelRef,
        key,
        apiName,
      )
    }
    // make top-level data required, and dis-allow extra attributes
    // The top level field is essential so it clearly is not valid if it goes straight into the payload
    newValue.required = ['data']
    newValue.additionalProperties = false
    fdSchemas[key] = newValue
  }

  jsonDoc.components.schemas = fdSchemas

  return jsonDoc
}

/**
 * Makes and formats a bulk endpoint
 * @param {*} apiName Modelname with a bulk route in it's routes.json
 * @param {*} protocols Return of a strapi query to the 'protocol' model
 * @param {Object} allComponentSchemas documentation.components.schemas, we copy and modify one of these to override child ob parent schema
 * @returns an object with property "bulkDoc", which should be appended onto path: "pathName"
 */
function makeBulkDocumentation(apiName, protocols, allComponentSchemas) {
  // Check there's an associated endpoint in routes.json
  // And that it uses the .bulk handler
  let routes
  try {
    routes = strapi.api[apiName].routes[apiName].routes
  } catch (error) {
    // no routes for api anyway (sometimes unused APIs will build up in dev instances)
    return { pathName: null, bulkDoc: null }
  }

  const bulkRoute = _.find(routes, (o) => {
    // A bulk route ends with /bulk and is a POST
    if (o.method !== 'POST' || o.handler !== `${apiName}.bulk`) return false
    const routeSplit = o.path.split('/')
    return (
      routeSplit &&
      routeSplit.length >= 3 &&
      routeSplit[routeSplit.length - 1] === 'bulk'
    )
  })
  if (!bulkRoute) return { pathName: null, bulkDoc: null }
  const pathName = bulkRoute.path
  const pathPrefix = pathName.split('/bulk')[0]

  // use the workflow in the protocol matching the endpoint prefix

  // (we can't check the protocol directly because it's associated with an endpoint, not a model)
  // Check protocols table in database
  const associatedProtocol = _.find(protocols, (o) => {
    return o.endpointPrefix === pathPrefix
  })

  // OpenAPI path schema, based on docs/Manually adding protocols with bulk.md, and issue #71
  // initialise common aspects
  let accBulkDoc = {
    post: {
      deprecated: false,
      responses: {
        200: {
          description: 'Echos back the body of the request',
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {},
              },
            },
          },
        },
      },
      // the "heading" of the path when using Swagger visualiser
      // FIXME update for Strapi 4 (should be Title Case)
      tags: [
        `${apiName.slice(0, 1).toUpperCase()}${apiName.slice(1).toLowerCase()}`,
      ],
      requestBody: {
        required: true,
        content: {
          'application/json': {
            schema: {
              type: 'object',
              required: ['data'],
              properties: {
                data: {
                  type: 'object',
                  required: ['collections'],
                  additionalProperties: false,
                  properties: {
                    collections: {
                      type: 'array',
                      minItems: 1,
                      items: {
                        type: 'object',
                        required: ['orgMintedIdentifier'],
                        additionalProperties: false,
                        properties: {
                          orgMintedIdentifier: {
                            type: 'string',
                            minLength: 1,
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  }
  let accBulkDocCollections =
    accBulkDoc.post.requestBody.content['application/json'].schema.properties
      .data.properties.collections
  let accExampleResponseProperties =
    accBulkDoc.post.responses[200].content['application/json'].schema.properties

  for (const workflowItem of Object.values(associatedProtocol.workflow)) {
    // Any plot locations/layout/visit can be input as ID as well
    const modelName = workflowItem.modelName

    const docRefModelName = modelNameToSchemaName(modelName)
    // add it to required list
    accBulkDocCollections.items.required.push(modelName)

    // set body acc to a baseline
    // (the newSchema schema should be formatted like "SchemaRequest", but we might need to confirm)
    let propertyBody = {
      type: 'object',
      $ref: `#/components/schemas/${docRefModelName}`,
    }

    // if we need to allow creation of child observations (handled in bulk controller)
    // then copy the $ref target's body, and modify attributes defined in workflow step
    if (
      Array.isArray(workflowItem.newInstanceForRelationOnAttributes) &&
      workflowItem.newInstanceForRelationOnAttributes.length > 0
    ) {
      let modifiedModelSchema = _.cloneDeep(
        allComponentSchemas[docRefModelName],
      )
      for (const childObAttribute of workflowItem.newInstanceForRelationOnAttributes) {
        // get modelName of target and use multiplicity
        let accAttributeSchema =
          modifiedModelSchema.properties.data.properties[childObAttribute]

        let targetSchemaName

        // handle multiplicity
        // veg-mapping-species-covers can be multiple, is type:array, with items:{type:int}
        const multiple = accAttributeSchema.type === 'array'
        if (multiple) targetSchemaName = accAttributeSchema.items['x-model-ref']
        else targetSchemaName = accAttributeSchema['x-model-ref']

        const targetSchema = `#/components/schemas/${modelNameToSchemaName(
          targetSchemaName,
        )}`

        if (multiple) accAttributeSchema.items = { $ref: targetSchema }
        else accAttributeSchema = { $ref: targetSchema }

        modifiedModelSchema.properties.data.properties[childObAttribute] =
          accAttributeSchema
      }
      propertyBody = modifiedModelSchema
    }

    // these 3 fields should be defined in bulk as allowing an ID in their place instead
    switch (modelName) {
      case 'plot-layout':
      case 'plot-visit':
        propertyBody = {
          type: 'object',
          anyOf: [
            propertyBody,
            {
              type: 'object',
              required: ['id'],
              properties: {
                id: {
                  type: 'integer',
                },
              },
            },
          ],
        }
        break

      default:
        break
    }
    // i.e. if it's an observations list
    if (workflowItem.multiple) {
      // make it an array
      propertyBody = {
        type: 'array',
        minItems: 1,
        items: propertyBody,
      }

      // add it to the 200 response as an array
      accExampleResponseProperties[modelName] = {
        type: 'array',
        items: { $ref: `#/components/schemas/${docRefModelName}` },
      }
    } else {
      // add it to the 200 response as a singular
      accExampleResponseProperties[modelName] = {
        $ref: `#/components/schemas/${docRefModelName}`,
      }
    }
    accBulkDocCollections.items.properties[modelName] = propertyBody
  }
  accBulkDoc.post.requestBody.content[
    'application/json'
  ].schema.properties.data.properties.collections = accBulkDocCollections

  // TODO build responses + example payloads

  return { pathName: pathName, bulkDoc: accBulkDoc }
}

/**
 * search through the internal model fields
 * in order to find Strapi reusable components and mark them with a tag
 * this is intended to be used in the documentation to suggest methods of data collection
 * (notably, a field using the barcode tag should be collected using a camera in client application)
 *
 * @param {*} inNewValue documentation schema section (documentation.components.schemas[modelName])
 * @param {*} modelRef Strapi content type config (strapi.api[apiName].contentTypes[apiName])
 * @param {String} baseModelName Only passed into resolveModelReferences to track unpopulated LUTs
 * @param {String} apiName Only passed into resolveModelReferences to add fields for custom endpoints
 * @returns Amended documentation schema
 */
async function resolveComponentReferences(
  inNewValue,
  modelRef,
  baseModelName,
  apiName,
) {
  let newValue = inNewValue
  // search through the internal model fields
  // in order to find Strapi reusable components and mark them with a tag
  // this is intended to be used in the documentation to suggest methods of data collection
  // (notably, a field using the barcode tag should be collected using a camera in client application)

  for (const [attributeKey, attribute] of Object.entries(modelRef.attributes)) {
    if (attribute.type && attribute.type === 'component') {
      // yes it's a component, tag it and resolve the component fields here
      // Need to recursively resolve components/modelAssociations because component fields
      // are added dereferenced to their containing attribute
      const componentName = attribute.component

      // Properties is in a different spot if it's multiple components
      // Single component: type:object. with 'properties'
      // multiple components: type:array. with 'items'
      let newValueField
      const isMultiComponent =
        newValue.properties.data.properties[attributeKey].type === 'array'
      if (isMultiComponent) {
        newValueField = newValue.properties.data.properties[attributeKey].items
      } else newValueField = newValue.properties.data.properties[attributeKey]

      newValueField = await resolveModelAssociations(
        _.cloneDeep(newValueField),
        strapi.components[componentName],
        baseModelName,
        apiName,
      )

      newValueField['x-paratoo-component'] = componentName

      // put the field back into it's (possibly) nested slot
      if (isMultiComponent) {
        newValue.properties.data.properties[attributeKey].items = newValueField
      } else newValue.properties.data.properties[attributeKey] = newValueField
    }
  }
  return newValue
}

/**
 * Reads modelRef.associations and applies relevant LUT enums and 'x-...' flags to LUTs and model references
 * @param {*} inNewValue JSON schema object for the target model. Should have 'properties' key with model attributes directly inside
 * @param {*} modelRef
 * @param {String} modelName Only used for console trace to track down unpopulated LUTs
 * @returns Amended inNewValue
 */
async function resolveModelAssociations(
  inNewValue,
  modelRef,
  modelName = '',
  apiName = null,
) {
  let newValue = inNewValue

  for (const [attributeKey, attribute] of Object.entries(modelRef.attributes)) {
    if (attributeKey == 'createdAt') continue
    if (attributeKey == 'updatedAt') continue
    // if not initialized
    if (!Object.keys(newValue).includes('properties')) {
      newValue['properties'] = {}
    }
    if (!Object.keys(newValue.properties).includes(attributeKey)) {
      newValue['properties'][attributeKey] = {}
      newValue['properties'][attributeKey] = attribute
    }
    if (attribute.type === 'text') {
      newValue['properties'][attributeKey] = {
        type: 'string',
      }
    }
    // format: type: 'float'
    // ajv cant validate the format
    //    so, the format needs to be changed
    //    so that ajv can validate
    // TODO: need to check other types as well
    if (attribute.type === 'float') {
      newValue['properties'][attributeKey] = {
        type: 'number',
        format: 'float',
      }
    }
    // ajv can validate without specifying json type
    if (attribute.type === 'json') {
      newValue['properties'][attributeKey] = {}
    }
    if (attribute.type && attribute.type === 'relation') {
      // normally attribute.target looks like 'api::protocol.protocol'
      const contentTypeName = attribute.target
        .replace('api::', '')
        .split('.')[0]

      // if it's a lut, we need to give a list of available options (with current LUT elements)
      // This is because all of these values relate to an external vocab (and will stay pretty
      // static)
      if (attribute.target.match('^api::lut')) {
        const qResults = await strapi.db.query(attribute.target).findMany()
        let lutEnum = []
        qResults.forEach((qElement) => {
          lutEnum.push(qElement['symbol'])
        })
        if (lutEnum.length < 1) {
          strapi.log.error(
            `Found unpopulated LUT on model:${modelName}, referring to LUT:${attribute.target}`,
          )
        }
        newValue.properties = newValue.properties || {}
        newValue.properties[attributeKey] = {
          type: 'string',
          enum: lutEnum,
          'x-lut-ref': contentTypeName,
        }
      }

      // mark non-lut references for documentation purposes
      // set type to be int (for id).
      else if (attribute.relation === 'oneToOne') {
        newValue.properties[attributeKey] = {
          type: 'integer',
          'x-model-ref': contentTypeName,
        }
      }

      // same but oneToMany refs are structured differently
      else if (attribute.relation === 'oneToMany') {
        newValue.properties[attributeKey] = {
          type: 'array',
          items: {
            type: 'integer',
            'x-model-ref': contentTypeName,
          },
        }
      }
    }

    // Add in media (should look like a relation to 'file')
    else if (attribute.type === 'media') {
      const contentTypeName = 'file'

      // we need to make the full thing because the components-->schemas in
      // https://github.com/strapi/strapi/pull/12929 do not generate attributes AT ALL
      // TODO rework if/when fixed
      if (attribute.multiple) {
        newValue.properties[attributeKey] = {
          type: 'array',
          items: {
            type: 'integer',
            'x-paratoo-file-type': getFileType(modelRef, attributeKey),
            'x-model-ref': contentTypeName,
          },
        }
      } else {
        newValue.properties[attributeKey] = {
          type: 'integer',
          'x-paratoo-file-type': getFileType(modelRef, attributeKey),
          'x-model-ref': contentTypeName,
        }
      }
    }

    // Add flags to indicate an external database is to be suggested in the webapp
    if (attribute['x-paratoo-csv-list-taxa'])
      newValue.properties[attributeKey]['x-paratoo-csv-list-taxa'] =
        attribute['x-paratoo-csv-list-taxa']

    // handle `x-paratoo-symantec-uri` flag
    if (attribute['x-paratoo-symantec-uri'])
      newValue.properties[attributeKey]['x-paratoo-symantec-uri'] =
        attribute['x-paratoo-symantec-uri']

    // Check if the x-paratoo-required flag exists for each reference
    // If we are a relation we assume it IS REQUIRED, unless specified otherwise
    // We can't specify it IS required within the Strapi model itself, because Strapi
    // is unable to handle relational fields being required (because of db interface limitation)
    // https://github.com/strapi/strapi/issues/557

    // Specifically, this has a functional difference because our custom validation
    // is able to enforce required

    // Then add required to array if needed
    if (attribute['x-paratoo-required'] === true) {
      newValue.required = newValue.required || []
      newValue.required.push(attributeKey)
    }

    //handle `x-paratoo-description` flag
    if (attribute['x-paratoo-description']) {
      newValue.properties[attributeKey]['x-paratoo-description'] =
        attribute['x-paratoo-description']
    }
    //handle `x-paratoo-unit` flag
    if (attribute['x-paratoo-unit']) {
      newValue.properties[attributeKey]['x-paratoo-unit'] =
        attribute['x-paratoo-unit']
    }
    //handle `x-paratoo-regex-message`
    if (attribute.regex) {
      if (attribute['x-paratoo-regex-message']) {
        newValue.properties[attributeKey]['x-paratoo-regex-message'] = attribute['x-paratoo-regex-message']
      } else {
        strapi.log.warn(`Field '${attributeKey}' for model '${modelName}' has regex attribute but no user-facing message defined (x-paratoo-regex-message). Client error messages will not be useful to user`)
      }
    }
  }

  // we added some new attributes, preserve the original order
  // because we currently use this order on the frontend to display fields
  // and want to be able to configure it
  // TODO FIXME, I disabled it because it can't see components (and probably doesn't keep their order anyway)
  // (modelRef.attributes list doesn't even keep them)
  // newValue.properties = reorderAttributes(
  //   modelRef.attributes,
  //   newValue.properties,
  // )
  return apiName == 'org'
    ? addAdditionalRules(_.cloneDeep(newValue), apiName)
    : newValue
}

// add additional fields to validate custom endpoints(for org only)
//   as we don't have all the fields in the content-type/schema
//   e.g. projectID for org/mint-identifier or orgMintedIdentifier for org/collection
function addAdditionalRules(newValue, apiName) {
  if (apiName != 'org') return newValue
  newValue.properties['survey_metadata'] = {
    type: 'object',
    properties: {
      survey_details: {
        type: 'object',
        properties: {
          survey_model: {
            type: 'string',
          },
          time: {
            type: 'string',
          },
          uuid: {
            type: 'string',
            format: 'uuid'
          },
          project_id: {
            type: 'string'
          },
          protocol_id: {
            type: 'string',
            format: 'uuid'
          },
          protocol_version: {
            type: 'string'
          },
          submodule_protocol_id: {
            type: 'string'
          }
        },
      },
      provenance: {
        type: 'object',
        properties: {
          version_app: {
            type: 'string',
          },
          version_core: {
            type: 'string',
          },
          version_core_documentation: {
            type: 'string',
          },
          version_org: {
            type: 'string',
          },
          system_app: {
            type: 'string',
          },
          system_core: {
            type: 'string',
          },
          system_org: {
            type: 'string',
          },
        },
      },
      orgMintedUUID: {
        type: 'string',
      },
    },
  }
  newValue.properties['token'] = {
    type: 'string',
  }
  newValue.properties['orgMintedIdentifier'] = {
    type: 'string',
    minLength: 1
  }
  newValue.properties['userId'] = {
    type: 'integer',
  }
  newValue.properties['eventTime'] = {
    type: 'string',
    format: 'date-time',
  }
  newValue.properties['userInfo'] = {
    type: 'object',
    properties: {
      userId: {
        type: 'integer',
      },
      username: {
        type: 'string',
        minLength: 1
      },
      userEmail: {
        type: 'string',
        format: 'email'
      },
    },
  }
  newValue.properties['uuid'] = {
    type: 'string',
    format: 'uuid'
  }
  return newValue
}

/**
 * Returns the content for the x-paratoo-file-type, to notate the allowable types to UI,
 * Validation should eventually be handled separately for each type
 * @param {*} modelRef
 * @param {*} attributeName
 * @returns {Array} An array of types allowable in this attribute
 */
function getFileType(modelRef, attributeName) {
  // https://swagger.io/docs/specification/openapi-extensions/
  // x-flag attributes can be arrays
  //want to differentiate audio from the base 'files' Strapi media type, so the client
  //  knows to render the correct component
  if (modelRef.attributes[attributeName]['x-paratoo-audio-only'])
    return ['audio']
  return modelRef.attributes[attributeName].allowedTypes
}

/**
 * kebab-case to CamelCase + add postfix
 *
 * @param {String} modelName
 */
function modelNameToSchemaName(modelName) {
  return `${_.upperFirst(_.camelCase(modelName))}Request`
}

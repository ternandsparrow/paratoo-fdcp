'use strict'

global.atob = require('atob')
global.btoa = require('btoa')

//FIXME change usage of deprecated atob and btoa
module.exports = {
  //disable lint warnings for 'unused' atob and btoa
  /*eslint-disable */
  strToBin: function(str) {
    return Uint8Array.from(atob(str), c => c.charCodeAt(0))
  },
  /*eslint-enable */
}
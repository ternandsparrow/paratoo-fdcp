module.exports = {
  createSentryBreadcrumb({ message, level = 'debug', category = 'breadcrumb' }) {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const sentryService = helpers.getSentryService()
    if (sentryService) {
      sentryService.getInstance().addBreadcrumb({
        message: message,
        level: level,
        category: category,
      })
    }
  }
}
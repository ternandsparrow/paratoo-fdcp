module.exports = {
  getSentryService() {
    const sentryPlugin = strapi.plugin('sentry')
    if (sentryPlugin) {
      return sentryPlugin.service('sentry')
    }
    strapi.log.debug('Sentry plugin not found. Is it disabled?')
    return null
  }
}
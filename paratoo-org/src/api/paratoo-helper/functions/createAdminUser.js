'use strict'

module.exports = {
  /**
   * Create admin user for Strapi admin panel
   * Note: we the Core/Org implementations in sync using the
   * script `helper-scripts/diff-create-admin-user.sh`
   * https://github.com/strapi/strapi/issues/3365#issuecomment-648380879
   */
  createAdminUser: async function () {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    const start = Date.now()

    //construct params to create admin user
    const adminUserParams = {
      firstname: process.env.ADMIN_FNAME || 'Paratoo',
      lastname: process.env.ADMIN_LNAME || 'Admin',
      username: process.env.ADMIN_USER || 'admin',
      password: helpers.validString(process.env.ADMIN_PASS),
      email: helpers.validString(process.env.ADMIN_EMAIL),
      blocked: false,
      isActive: true.valueOf,
      registrationToken: null,
    }
    // https://forum.strapi.io/t/hard-code-the-strapi-administrator/1074/12?u=jakubgs
    // Check if admin user exists
    const hasAdmin = await strapi.service('admin::user').exists()
    const isOverrideAdminPass = helpers.validBoolean(
      process.env.RESET_ADMIN_USER_PASS,
    )
    if (hasAdmin && !isOverrideAdminPass) {
      strapi.log.info('Admin role exists, nothing to do')
      return
    } else if (hasAdmin && isOverrideAdminPass) {
      // Overriding admin user's password if the env var is set
      strapi.log.info('Overriding admin user\'s password')
      try {
        // found this function by using this Object.getOwnPropertyNames(strapi.service('admin::user'))
        await strapi
          .service('admin::user')
          .resetPasswordByEmail(adminUserParams.email, adminUserParams.password)
        strapi.log.info('Admin user\'s password has been reset')
      } catch (error) {
        const message = error?.message || 'Unknown error'
        strapi.log.warn('Unable to reset password with error: ' + message)
      }
      return
    }

    // Check is super admin role exists
    let superAdminRole = await strapi.service('admin::role').getSuperAdmin()
    if (!superAdminRole?.id) {
      try {
        strapi.log.verbose('Admin role does not exist, creating...')
        await strapi.service('admin::role').create({
          name: 'Super Admin',
          code: 'strapi-super-admin',
          description:
            'Super Admins can access and manage all features and settings.',
        })
      } catch (error) {
        strapi.log.error(
          'Could not create Admin role: ' + JSON.stringify(error),
        )
      }

      superAdminRole = await strapi.service('admin::role').getSuperAdmin()
      if (!superAdminRole?.id) {
        strapi.log.warn(
          'Could not create the Admin role. Skipping Admin user creation',
        )
        return
      }
    }
    adminUserParams.roles = [superAdminRole.id]

    try {
      // Create admin account
      strapi.log.verbose('Setting up Admin user...')
      await strapi.service('admin::user').create({
        ...adminUserParams,
      })
      strapi.log.verbose('Admin Account created')
    } catch (error) {
      strapi.log.error('Could not create admin user: ' + JSON.stringify(error))
    }

    strapi.log.info(
      `Successfully created admin account for username=${adminUserParams.username}.`,
    )
    let isDefaultCred = false
    if (!process.env.ADMIN_USER && !process.env.ADMIN_PASS) {
      isDefaultCred = true
    }
    //if credentials defaulted, we're likely in a dev space so logging the username
    //and password to console is safe
    if (isDefaultCred) {
      strapi.log.info(
        'No username and password were supplied in the environment variables so it has defaulted. ',
      )
      strapi.log.info(
        `The admin account email is: ${adminUserParams.email} and the password is: ${adminUserParams.password}`,
      )
    }
    strapi.log.debug(`Took ${Date.now() - start}ms to create admin user`)
  },
}

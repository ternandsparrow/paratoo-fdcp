const { roles } = require('../../../roles')
module.exports = {
  /**
   * Configures the endpoint permissions
   */
  configurePerms: async function configurePerms() {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    let existingRoles = await helpers.getAllRoles()
    const adminRoleType = process.env.ADMIN_ROLE_TYPE || 'paratoo_admin'
    const paratooAdminRoleId = existingRoles.find(
      (o) => o.type === adminRoleType && o.name === adminRoleType,
    ).id
    const permissions = await strapi.db
      .query('plugin::users-permissions.permission')
      .findMany({
        populate: true,
      })
    roles.forEach(async (role) => {
      if (!role.isActive) return
      const roleDetails = await getOrCreateRole(
        role.type,
        role.name,
        role.description,
      )
      // set perms for plugins
      if (role.plugins.length > 0) {
        role.plugins.forEach(async (plugin) => {
          const action = `plugin::${plugin.roleType}.${plugin.prefix}.${plugin.action}`
          await setPermission(
            action,
            roleDetails.id,
            permissions,
            paratooAdminRoleId,
          )
        })
      }

      // set perms for apis
      if (role.APIs.length > 0) {
        role.APIs.forEach(async (api) => {
          const splits = api.split('.')
          const action = `api::${splits[0]}.${api}`
          await setPermission(
            action,
            roleDetails.id,
            permissions,
            paratooAdminRoleId,
          )
        })
      }

      // set perms for customEndpoints
      if (role.customEndpoints.length > 0) {
        role.customEndpoints.forEach(async (endpoint) => {
          const splits = endpoint.split('.')
          const action = `api::${splits[0]}.${endpoint}`
          await setPermission(
            action,
            roleDetails.id,
            permissions,
            paratooAdminRoleId,
          )
        })
      }

      // set perms for luts
      if (role.luts.length > 0) {
        role.luts.forEach(async (lut) => {
          role.lutActions.forEach(async (lutAction) => {
            const action = `api::${lut}.${lut}.${lutAction}`
            await setPermission(
              action,
              roleDetails.id,
              permissions,
              paratooAdminRoleId,
            )
          })
        })
      }
    })
  },
}

async function getOrCreateRole(roleType, roleName, roleDescription) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const roles = await helpers.getAllRoles()
  const roleFound = roles.find(
    (o) => o.type === roleType && o.name === roleName,
  )
  if (!roleFound) {
    strapi.log.verbose(`${roleName} role not defined, creating...`)
    const entry = await strapi.db
      .query('plugin::users-permissions.role')
      .create({
        data: {
          name: roleName,
          description: roleDescription,
          type: roleType,
        },
      })
      .catch((err) => {
        helpers.paratooErrorHandler(500, err)
      })

    return entry
  }
  // if description is different
  if (roleFound.description != roleDescription) {
    await strapi.entityService.update(
      'plugin::users-permissions.role',
      roleFound.id,
      {
        data: {
          description: roleDescription,
        },
      },
    )
  }
  return roleFound
}

async function setPermission(
  action,
  roleId,
  permissions,
  pararoAdminId = null,
) {
  const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
  const isExist = permissions.find(
    (o) => o.action === action && o.role.id === roleId,
  )
  if (isExist) return

  await strapi.db
    .query('plugin::users-permissions.permission')
    .create({
      data: {
        action: action,
        role: roleId,
      },
    })
    .catch((err) => {
      helpers.paratooErrorHandler(500, err)
    })
  // if paratoo admin exists, we will set perms for that action too
  if (pararoAdminId) {
    await strapi.db
      .query('plugin::users-permissions.permission')
      .create({
        data: {
          action: action,
          role: pararoAdminId,
        },
      })
      .catch((err) => {
        helpers.paratooErrorHandler(500, err)
      })
  }
}

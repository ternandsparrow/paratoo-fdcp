'use strict'

const { binToStr } = require('../functions/binToStr')
const { createAdminUser } = require('../functions/createAdminUser')
const { paratooErrorHandler, paratooWarnHandler } = require('../functions/paratooErrorHandler')
const { paratooDebugMsg } = require('../functions/paratooDebugMsg')
const { prettyFormatFieldName } = require('../functions/prettyFormatFieldName')
const { strToBin } = require('../functions/strToBin')
const { underscoreToDash } = require('../functions/underscoreToDash')
const { readFullDocumentation } = require('../functions/readFullDocumentation')
const { formatApiEndPoint } = require('../functions/formatApiEndPoint')
const { amendFullApiDoc } = require('../functions/amendDocumentation')
const { internalValidator, applyCustomRules } = require('../functions/internalValidator')
const { configurePerms } = require('../functions/configurePerms')
const { getAllRoles } = require('../functions/getAllRoles')
const { createUserWithRole } = require('../functions/createUserWithRole')
const { getSentryService } = require('../functions/getSentryService')
const { createSentryBreadcrumb } = require('../functions/createSentryBreadcrumb')
const { getProjAndProtoForUser,
  getAuthorisedProjects,
  checkProjectAuthorisation,
  checkProtocolAuthorisation,
  checkModelsAuthorisation,
} = require('../functions/projectAndProtocolFunctions')
const { deepPopulateQuery } = require('../functions/queryFundtions')
const { validString, validInt, validBoolean } = require('../functions/parseEnv')

/**
 * paratoo-helper service.
 */

module.exports = () => ({
  binToStr,
  createAdminUser,
  paratooErrorHandler,
  paratooWarnHandler,
  paratooDebugMsg,
  prettyFormatFieldName,
  strToBin,
  underscoreToDash,
  readFullDocumentation,
  formatApiEndPoint,
  amendDocumentation: amendFullApiDoc,
  internalValidator,
  configurePerms,
  getAllRoles,
  createUserWithRole,
  getSentryService,
  createSentryBreadcrumb,
  applyCustomRules,
  getProjAndProtoForUser,
  getAuthorisedProjects,
  checkProjectAuthorisation,
  checkProtocolAuthorisation,
  checkModelsAuthorisation,
  deepPopulateQuery,
  validString,
  validInt,
  validBoolean,
})

'use strict'

/**
 * lut-plot-corner-and-centre router
 */

const { createCoreRouter } = require('@strapi/strapi').factories

module.exports = createCoreRouter('api::lut-plot-corner-and-centre.lut-plot-corner-and-centre')

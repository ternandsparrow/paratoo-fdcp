'use strict'

/**
 * lut-plot-corner-and-centre controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-plot-corner-and-centre.lut-plot-corner-and-centre')

'use strict'

/**
 * lut-plot-point router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories

module.exports = createCoreRouter('api::lut-plot-point.lut-plot-point')

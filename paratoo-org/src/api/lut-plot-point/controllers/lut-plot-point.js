'use strict'

/**
 *  lut-plot-point controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::lut-plot-point.lut-plot-point')

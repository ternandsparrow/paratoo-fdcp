'use strict'

/**
 * lut-plot-point service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-plot-point.lut-plot-point')

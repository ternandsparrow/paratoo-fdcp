module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/projects',
      handler: 'project.find',
      config: { policies: [] }
    },
    {
      method: 'GET',
      path: '/projects/:id',
      handler: 'project.findOne',
      config: { policies: ['global::lut-interpretation'] }
    },
    {
      method: 'POST',
      path: '/projects',
      handler: 'project.create',
      config: { policies: ['global::lut-interpretation'] }
    },
    {
      method: 'PUT',
      path: '/projects/:id',
      handler: 'project.update',
      config: { policies: ['global::lut-interpretation'] }
    },
    {
      method: 'DELETE',
      path: '/projects/:id',
      handler: 'project.delete',
      config: { policies: ['global::lut-interpretation'] }
    }
  ]
}
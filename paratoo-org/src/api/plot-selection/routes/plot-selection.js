module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/plot-selections',
      handler: 'plot-selection.find',
      config: { policies: ['global::lut-interpretation'] },
    },
    {
      method: 'GET',
      path: '/plot-selections/:id',
      handler: 'plot-selection.findOne',
      config: { policies: ['global::lut-interpretation'] },
    },
    {
      method: 'POST',
      path: '/plot-selections',
      handler: 'plot-selection.create',
      config: { policies: [] },
    },
    {
      method: 'PUT',
      path: '/plot-selections/:id',
      handler: 'plot-selection.update',
      config: { policies: ['global::lut-interpretation'] },
    },
    {
      method: 'DELETE',
      path: '/plot-selections/:id',
      handler: 'plot-selection.delete',
      config: { policies: ['global::lut-interpretation'] },
    },
  ],
}

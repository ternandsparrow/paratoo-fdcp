'use strict'

/**
 *  plot-selection controller
 */

const { createCoreController } = require('@strapi/strapi').factories

module.exports = createCoreController('api::plot-selection.plot-selection')

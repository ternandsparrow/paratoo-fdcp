'use strict'

/**
 * plot-selection service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::plot-selection.plot-selection')

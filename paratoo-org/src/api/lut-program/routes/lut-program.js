'use strict'

/**
 * lut-program router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories

module.exports = createCoreRouter('api::lut-program.lut-program')

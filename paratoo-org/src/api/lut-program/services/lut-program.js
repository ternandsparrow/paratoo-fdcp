'use strict'

/**
 * lut-program service.
 */

const { createCoreService } = require('@strapi/strapi').factories

module.exports = createCoreService('api::lut-program.lut-program')

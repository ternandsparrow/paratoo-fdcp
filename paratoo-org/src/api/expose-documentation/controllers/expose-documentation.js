// TODO ideally this wouldn't be our own custom controller, but this is used pending
// Strapi review of https://github.com/strapi/strapi/pull/11958
module.exports = {
  index: async (ctx) => {
    const helpers = strapi.service('api::paratoo-helper.paratoo-helper')
    try {
      // Read latest documentation version with helper function
      const latestFullDoc = await helpers.readFullDocumentation()

      // TODO add API doc versions
      // based on https://github.com/strapi/strapi/issues/10734#issuecomment-901943169
      ctx.send(latestFullDoc)
    } catch (error) {
      helpers.paratooErrorHandler(
        400,
        new Error('Unable to retrieve documentation'),
      )
    }
  },
  buildInfo: async (ctx) => {
    ctx.send({
      Build: process.env.NODE_ENV,
      Version: process.env.VERSION,
      GitHash: process.env.GIT_HASH,
    })
  },
}

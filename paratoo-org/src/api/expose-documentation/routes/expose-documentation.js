module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/documentation/swagger.json',
      handler: 'expose-documentation.index',
      config: {
        policies: [],
      },
    },
    {
      method: 'GET',
      path: '/documentation/swagger-build-info',
      handler: 'expose-documentation.buildInfo',
      config: {
        policies: [],
      },
    },
  ],
}

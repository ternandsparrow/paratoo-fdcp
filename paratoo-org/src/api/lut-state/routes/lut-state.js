'use strict'

/**
 * lut-state router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories

module.exports = createCoreRouter('api::lut-state.lut-state')

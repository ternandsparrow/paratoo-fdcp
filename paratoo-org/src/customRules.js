const { validate } = require('uuid')
// custom rules to validate
module.exports = {
  customRules: {
    // APIs
    'mint-identifier': [
      {
        ruleType: 'surveyMetadataProjectCheck',
        rule: (projectId, validProjects) =>
          validProjects.includes(projectId) ||
          `Invalid Project ID or User is not authorised to write ${projectId}`,
      },
      {
        ruleType: 'surveyMetadataProtocolCheck',
        rule: (protocolId, validProtocols) =>
          validProtocols.includes(protocolId) ||
          `Invalid Protocol ID or User is not authorised to write ${protocolId}`,
      },
    ],
    'status/:identifier': [
      {
        ruleType: 'uuidIdentifierCheck',
        rule: (value) => validate(value) || 'Invalid UUID',
      },
    ],
  },
}

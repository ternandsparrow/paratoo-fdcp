module.exports = {
  roleRestrictions: {
    projects: [
      /* add project names and authorisedRoles e.g.
        {
          if we add this then this project will be authorised for only project admin
          name: 'Project Area & Plot Selection', // Plot Selection and Layout
          authorisedRoles: ['project_admin'],
        },
      */
    ],
    protocols: [
      {
        // this protocol is authorised for only project admin
        uuid: 'a9cb9e38-690f-41c9-8151-06108caf539d',
        name: 'Plot Selection and Layout',
        authorisedRoles: ['project_admin'],
      },
    ],
    models: [
      /* add model apis and authorisedRoles e.g.
      {
        // this api will be authorised for only collector
        api: 'opportunistic-observation.create',
        authorisedRoles: ['collector'],
      },
      */
    ],
  },
}

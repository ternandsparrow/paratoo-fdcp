//logic for creating refresh token from: https://strapi.io/blog/how-to-create-a-refresh-token-feature-in-your-strapi-application
//mostly copies with mods from: node_modules/@strapi/plugin-users-permissions/server/services/jwt.js

const utils = require('@strapi/utils')
const { getService } = require('../users-permissions/utils')
const jwt = require('jsonwebtoken')
const _ = require('lodash')
const {
  validateCallbackBody,
} = require('../users-permissions/controllers/validation/auth')

const { sanitize } = utils
const { ApplicationError, ValidationError } = utils.errors

const sanitizeUser = (user, ctx) => {
  const { auth } = ctx.state
  const userSchema = strapi.getModel('plugin::users-permissions.user')
  return sanitize.contentAPI.output(user, userSchema, { auth })
}

const issueJWT = (payload, jwtOptions = {}) => {
  //this is a workaround to allow specific tests to change the expiry, which is why we
  //check both the NODE_ENV and the param for overridden expiry
  if (
    process.env.NODE_ENV === 'test' &&
    payload?.forceTokenExpiry?.forceJwtExpiry
  ) {
    jwtOptions.expiresIn = payload.forceTokenExpiry.forceJwtExpiry
  }
  _.defaults(jwtOptions, strapi.config.get('plugin.users-permissions.jwt'))
  return jwt.sign(
    _.clone(payload.toJSON ? payload.toJSON() : payload),
    strapi.config.get('plugin.users-permissions.jwtSecret'),
    jwtOptions,
  )
}

const verifyRefreshToken = (token) => {
  return new Promise(function (resolve, reject) {
    jwt.verify(
      token,
      strapi.config.get('plugin.users-permissions.jwtSecret'),
      {},
      function (err, tokenPayload = {}) {
        if (err) {
          return reject(new Error('Invalid token.'))
        }
        resolve(tokenPayload)
      },
    )
  })
}

const issueRefreshToken = (payload, jwtOptions = {}) => {
  _.defaults(jwtOptions, strapi.config.get('plugin.users-permissions.jwt'))
  let expiresIn = strapi.config.get('plugin.users-permissions.refreshToken.expiresIn')
  //this is a workaround to allow specific tests to change the expiry, which is why we
  //check both the NODE_ENV and the param for overridden expiry
  if (process.env.NODE_ENV === 'test') {
    if (payload?.forceTokenExpiry?.forceJwtExpiry) {
      jwtOptions.expiresIn = payload.forceTokenExpiry.forceJwtExpiry
    }
    if (payload?.forceTokenExpiry?.forceRefreshExpiry) {
      expiresIn = payload.forceTokenExpiry.forceRefreshExpiry
    }
  }
  return jwt.sign(
    _.clone(payload.toJSON ? payload.toJSON() : payload),
    strapi.config.get('plugin.users-permissions.jwtSecret'),
    { expiresIn: expiresIn },
  )
}

module.exports = (plugin) => {
  const configMsg = `Users and Permissions Plugin using config:
  - JWT expiry: ${plugin.config.jwt.expiresIn}
  - Refresh token expiry: ${plugin.config.refreshToken.expiresIn}
  `
  strapi.log.debug(configMsg)
  plugin.controllers.auth.callback = async (ctx) => {
    const provider = ctx.params.provider || 'local'
    const params = ctx.request.body
    const store = strapi.store({ type: 'plugin', name: 'users-permissions' })
    const grantSettings = await store.get({ key: 'grant' })
    const grantProvider = provider === 'local' ? 'email' : provider
    if (!_.get(grantSettings, [grantProvider, 'enabled'])) {
      throw new ApplicationError('This provider is disabled')
    }

    let forceTokenExpiry = {
      forceJwtExpiry: null,
      forceRefreshExpiry: null,
    }
    if (ctx.request.body.forceJwtExpiry) {
      forceTokenExpiry.forceJwtExpiry = ctx.request.body.forceJwtExpiry
    }
    if (ctx.request.body.forceRefreshExpiry) {
      forceTokenExpiry.forceRefreshExpiry = ctx.request.body.forceRefreshExpiry
    }

    if (provider === 'local') {
      await validateCallbackBody(params)
      const { identifier } = params
      // Check if the user exists.
      const user = await strapi
        .query('plugin::users-permissions.user')
        .findOne({
          where: {
            provider,
            $or: [
              { email: identifier.toLowerCase() },
              { username: identifier },
            ],
          },
        })
      if (!user) {
        throw new ValidationError('Invalid identifier or password')
      }
      if (!user.password) {
        throw new ValidationError('Invalid identifier or password')
      }
      const validPassword = await getService('user').validatePassword(
        params.password,
        user.password,
      )
      const refreshToken = issueRefreshToken({ id: user.id })
      if (!validPassword) {
        throw new ValidationError('Invalid identifier or password')
      } else {
        ctx.send({
          jwt: issueJWT({
            id: user.id,
            forceTokenExpiry: forceTokenExpiry,
          }),
          refreshToken: refreshToken,
          user: await sanitizeUser(user, ctx),
        })
      }
      const advancedSettings = await store.get({ key: 'advanced' })
      const requiresConfirmation = _.get(advancedSettings, 'email_confirmation')
      if (requiresConfirmation && user.confirmed !== true) {
        throw new ApplicationError('Your account email is not confirmed')
      }
      if (user.blocked === true) {
        throw new ApplicationError(
          'Your account has been blocked by an administrator',
        )
      }
      return ctx.send({
        jwt: issueJWT({
          id: user.id,
          forceTokenExpiry: forceTokenExpiry,
        }),
        refreshToken: refreshToken,
        user: await sanitizeUser(user, ctx),
      })
    }
    // Connect the user with a third-party provider.
    try {
      const user = await getService('providers').connect(provider, ctx.query)
      return ctx.send({
        jwt: issueJWT({
          id: user.id,
          forceTokenExpiry: forceTokenExpiry,
        }),
        user: await sanitizeUser(user, ctx),
      })
    } catch (error) {
      throw new ApplicationError(error.message)
    }
  }

  plugin.controllers.auth['refreshToken'] = async (ctx) => {
    const store = await strapi.store({
      type: 'plugin',
      name: 'users-permissions',
    })
    const { refreshToken } = ctx.request.body

    if (!refreshToken) {
      return ctx.badRequest('No Authorization')
    }

    let forceTokenExpiry = {
      forceJwtExpiry: null,
      forceRefreshExpiry: null,
    }
    if (ctx.request.body.forceJwtExpiry) {
      forceTokenExpiry.forceJwtExpiry = ctx.request.body.forceJwtExpiry
    }
    if (ctx.request.body.forceRefreshExpiry) {
      forceTokenExpiry.forceRefreshExpiry = ctx.request.body.forceRefreshExpiry
    }

    try {
      const obj = await verifyRefreshToken(refreshToken)
      const user = await strapi
        .query('plugin::users-permissions.user')
        .findOne({ where: { id: obj.id } })
      if (!user) {
        throw new ValidationError('Invalid identifier or password')
      }
      if (
        _.get(await store.get({ key: 'advanced' }), 'email_confirmation') &&
        user.confirmed !== true
      ) {
        throw new ApplicationError('Your account email is not confirmed')
      }
      if (user.blocked === true) {
        throw new ApplicationError(
          'Your account has been blocked by an administrator',
        )
      }
      const newRefreshToken = issueRefreshToken({ id: user.id, forceTokenExpiry })
      ctx.send({
        jwt: issueJWT(
          { id: obj.id, forceTokenExpiry: forceTokenExpiry },
          { expiresIn: strapi.config.get('plugin.users-permissions.jwt.expiresIn') },
        ),
        refreshToken: newRefreshToken,
      })
    } catch (err) {
      return ctx.badRequest(err.toString())
    }
  }

  plugin.routes['content-api'].routes.push({
    method: 'POST',
    path: '/token/refresh',
    handler: 'auth.refreshToken',
    config: {
      policies: [],
      prefix: '',
    },
  })

  return plugin
}

const uuidRegex = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/i
const positiveIntRegex = /^\d+$/
module.exports = {
  positiveIntRegex: positiveIntRegex,
  //need to use regex for UUID, even though `uuid.validate()` is preferred as some
  //protocol IDs are not technically valid, even though they were generated with the same
  //library. Likely that some of them were edited after being generated
  uuidRegex: uuidRegex,
  validateSingleProjectIdCb: (p) => uuidRegex.test(p) || positiveIntRegex.test(p),
  ERROR_CODES: [
    //extend as-needed
  ],
}
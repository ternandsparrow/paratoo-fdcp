#!/bin/sh
set -euxo pipefail
cd `dirname "$0"`

# ensure this dir doesn't exist, so it will be regenerated
rm -fr ./src/extensions/documentation/documentation/

export VERSION=`sed -n 's/^VERSION=\(.*\)/\1/p' < ./.env-temp`
export GIT_HASH=`sed -n 's/^GIT_HASH=\(.*\)/\1/p' < ./.env-temp`
yarn start

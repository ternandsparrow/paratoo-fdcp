// path: ./config/plugins.js
const { version } = require('../package.json')
module.exports = ({ env }) => {
  // overkill safeguard but just in case, environment's length can't be longer than 64
  let environment = env('DNS_PARATOO_ORG') || env('NODE_ENV')
  if(environment.length > 63) { 
    environment = environment.substring(0, 62)
  }
  const configObject = {
    'users-permissions': {
      config: {
        jwt: {
          expiresIn: env('JWT_EXPIRY', '30d'),
        },
        jwtSecret: env('JWT_SECRET', '58ae728d-0322-4ca0-93de-73e030b799a7'),
        refreshToken: {
          expiresIn: env('REFRESH_TOKEN_EXPIRY', '30d'),
        },
        ratelimit: {
          interval: 60000,
          max: 100000,
        },
      },
    },
    sentry: {
      enabled: env('NODE_ENV') == 'production',
      config: {
        dsn: env('SENTRY_DSN'),
        init: {
          environment,
          release: version,
        },
        sendMetadata: true,
      },
    },
    // Setup deep population
    'strapi-plugin-populate-deep': {
      config: {
        defaultDepth: 3 // Default is 5
      },
    },
    documentation: {
      enabled: true,
      config: {
        openapi: '3.0.0',
        info: {
          version: '1.0.0',
          title: 'Monitor FDCP API',
          description: `Build: ${process.env.NODE_ENV}-Version: ${process.env.VERSION}-GitHash: ${process.env.GIT_HASH}`,
          termsOfService: 'YOUR_TERMS_OF_SERVICE_URL',
          contact: {
            name: 'TERN and Sparrow',
            email: 'tech.contact@environmentalmonitoringgroup.com.au',
            url: 'https://www.environmentalmonitoringgroup.com.au/',
          },
          license: {
            name: 'MIT',
            url: 'https://www.environmentalmonitoringgroup.com.au/',
          },
        },
        'x-strapi-config': {
          path: '/documentation',
          showGeneratedFiles: true,
          generateDefaultResponse: true,
          plugins: ['upload', 'users-permissions'],
        },
        servers: [
          {
            url: env('ORG_SWAGGER_SERVER'),
            description: env('ORG_SWAGGER_DESCRIPTION'),
          },
        ],
        externalDocs: {
          description: 'Find out more',
          url: 'https://www.environmentalmonitoringgroup.com.au/',
        },
      },
    },
  }
  return configObject
}

module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', 'de7b472377d5c7452709482e22f337cc'),
  },
  apiToken: { salt: env('API_TOKEN_SALT', 'default_salt') },
})

module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1338),
  app: {
    keys: env.array('APP_KEYS', [
      '71ac087b1c9d93819f780425a16128fc',
      'testKey2',
    ]),
  },
})

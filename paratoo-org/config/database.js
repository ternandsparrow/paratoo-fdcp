module.exports = ({ env }) => ({
  connection: {
    client: 'postgres',
    connection: {
      host: env('DATABASE_HOST', '127.0.0.1'),
      port: env.int('DATABASE_PORT', 45433),
      database: env('DATABASE_NAME', 'app'),
      user: env('DATABASE_USERNAME', 'user'),
      password: env('DATABASE_PASSWORD', 'password'),
      ssl: env.bool('DATABASE_SSL', false) && {
        rejectUnauthorized: env.bool('DATABASE_SSL_REJECT_UNAUTHORIZED', false),
      },
    },
    pool: {
      //FIXME: refactor as env variable, as different timeouts will make sense on different hardware
      acquireTimeoutMillis: 100000,
      // source: https://stackoverflow.com/questions/73519391/strapi-responds-with-401-after-some-time-of-inactivity-works-after-reload
      min: 0,
    },
  },
})

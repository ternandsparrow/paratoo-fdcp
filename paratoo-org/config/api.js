module.exports = ({ env }) => ({
  rest: {
    //changes the `pageSize`/`limit` from the pagination metadata on REST requests
    defaultLimit: 2000,
  },
})
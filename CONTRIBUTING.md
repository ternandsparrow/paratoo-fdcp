>this document is a DRAFT. Recommend any changes and additional commit types you think work in our context.

<br><br>

# Conventional Commits
>All this information is derrived from [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0) and [Semantic Versioning](https://semver.org/)  (SemVer)

We want to structure our commits for consistency, to improve human and machine readability. This will also allow automated tools to extract data from these commit messages in the future.

A commit message should be structured like:

```
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

Types can be mapped to SemVer's specification. See section on Semantic Versioning further down this document. The first two types (fix and feat) are defined by Conventional Commits, while the others are based on [Angular Conventions](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#-commit-message-guidelines) (as recommended by Conventional Commits).

| Type          | Meaning                                                          | SemVer Correlation |
| ------------- | ---------------------------------------------------------------- | ------------------ |
| fix           | Bug fix                                                          | PATCH              |
| chore         | Small changes that aren't new features or bug fixes              | PATCH              |
| feat          | New feature                                                      | MINOR              |
| perf          | Performance fixes                                                | MINOR              |
| refactor      | Not a bug fix nor added feature                                  | n/a                |
| wip           | Work in progress. Commiting changes that are not fixes features, or refactors | n/a   |
| docs          | Documentation-only changes                                       | n/a                |
| style         | Code styling changes that don't affect the code's meaning        | n/a                |
| test          | Adding or changing tests                                         | n/a                |

**Note:** a footer beginning with '`BREAKING CHANGES:`' indicates the introduction of breaking API change, correlating to SemVer MAJOR. 

**Note:** appending '!' to type/scope indicates breaking API change, which correlates to SemVer MAJOR. You can comment further on this with a `BREAKING CHANGES` footer

**Note:** Footers other than `BREAKING CHANGES` <description> may be provided and follow a convention similar to [git trailer format](https://git-scm.com/docs/git-interpret-trailers) 

**Example 1:**

```
feat: added ability to reset password
note that users can only update their passwords once per day
reviewed by: John Doe
```
The second line in the body, while the third is the footer.

**Example 2:**

```
fix!: changed way authentication is handled
BREAKING CHANGE: this causes trouble on some platforms
```
The second line is a footer.

See more [examples](https://www.conventionalcommits.org/en/v1.0.0/#examples) of commit messages.


# Semantic Versioning
SemVer is a set of system versioning rules to prevent "version lock" (when dependencies are too tight) and "version promiscuity" (when dependencies are too loose)

In summary, given a version number **MAJOR.MINOR.PATCH**, increment the:

1. MAJOR version when you make incompatible API changes,
2. MINOR version when you add functionality in a backwards compatible manner, and
3. PATCH version when you make backwards compatible bug fixes.

Given that this system doesn't follow SemVer, <span style="color:red">a starting version should be decided on</span>.

# Issues: Bug Reports and Feature Request

Issues should be formatted in a unified way to effectively identify the problem and solutions. The reason we prefer to use GitLab issues is so that we can internally track code changes easily and have discussion around proposed changes. You'll see the 'activity' section in issues/tasks will often refer to commits (i.e., code changes) and cross-references to other issues. It essentially enables an efficient collaboration between both developers and non-developers.

When creating a new issue, you'll first land at this page:

!['Blank issues page'](./docs/screenshots/contrib_issue_page_blank.png)

You'll need to enter a title and select a template ('description' dropdown). The 'feedback' one is probably best for your case, even when reporting bugs; in that case you can refer to the 'general issues' template for a rough idea of what we expect, but the main thing is the **steps you took to get the problem (reproduction)** and the **expected behaviour**. You'll notice selecting a template pre-fills the text box for you and has a bunch of syntax. If you 'switch to rich text editing' you'll have a more friendly interface.

Please also add label(s) to the issue; at minimum the 'DCCEEW Feedback' one (for DCCEEW testers), but also any of the 'Feedback - <module>' ones, or the 'Feedback - General' for issues not specific to a given module (e.g., the Projects page). Don't worry about any of the other fields.

One of our tech team members can then review your issues(s) and cross-check them with existing ones. You'll notice then when you start typing your title it will also suggest some relevant issues, so that might be able to indicate if your feedback item is already addressed; or you can search our issues list. However, if you're not sure, just create the issue and we can double-check - rather have duplicates than having anything fall through the cracks!

Another thing that might making organising a bit easier is to create tasks within an issue, which you can think of as sub-issues.

!['Issues page'](./docs/screenshots/contrib_issue_page.png)

You can see that in the issue above, there's a 'tasks' section where we can add new tasks to this issue. When there's a lot of feedback, we'll often create an issue for each module, with tasks for each feedback item. But this will heavily depend on the sort of feedback you provide, so we'll leave it up to you on how you want to structure issues/tasks. It also might be the case that you create an issue, but then one of us breaks it down into action items in the form of tasks.

Have a look [here](https://gitlab.com/ternandsparrow/paratoo-fdcp/-/issues/?sort=created_date&state=opened&author_username=mark-laws&type%5B%5D=issue&first_page_size=20) at all the issues (not tasks, I excluded those to make the list more concise) that Mark has authored, it might help as a good set of examples.

#/bin/bash
echo "Deleting existing dawe-rlp-spec...."
rm -rf dawe-rlp-spec
rm -rf protocol_shapes

echo "Cloning dawe-rlp-spec...."
git clone -n --depth=1 --filter=tree:0 \
  https://github.com/ternaustralia/dawe-rlp-spec.git
cd dawe-rlp-spec
git sparse-checkout set --no-cone shapes
git checkout
rm -rf .git
cd ..

mkdir protocol_shapes
cp -a dawe-rlp-spec/shapes/. protocol_shapes/
rm -rf dawe-rlp-spec

echo "Updating protocol specific shapes...."
python3 push_updated_shapes.py
rm -rf protocol_shapes

##########################################
echo "Deleting existing ontology_tern...."
rm -rf ontology_tern

echo "Cloning ontology_tern...."
git clone -n --depth=1 --filter=tree:0 \
  git@github.com:ternaustralia/ontology_tern.git
cd ontology_tern
git sparse-checkout set --no-cone docs
git checkout
rm -rf .git
cd ..

cp -a ontology_tern/docs/tern.ttl resources/tern_ontology.ttl
rm -rf ontology_tern
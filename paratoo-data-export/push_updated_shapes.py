
import os
from rdflib import Graph
from data_export.utilities import Utilities as utility

def main():
    protocols = utility.load_json_file(file_name="resources/protocols.json", include_root_dir=False)
    for protocol, properties in protocols.items():
        if "protocol_shape" not in properties: continue
        shape_path = "protocol_shapes/" + properties["protocol_shape"]
        
        shape_name = properties["protocol_shape"].split("/")[-1]
        output = f"resources/{shape_name}"
        methods = os.listdir(shape_path)
        if "shapes.ttl" in methods:
            shape = f"{output}.ttl"
            g = Graph()
            g.parse(f"{shape_path}/shapes.ttl", format="ttl")
            g.serialize(shape, format="ttl")
            print(f"{shape} exists don't need to create aggregated shapes")
            continue
        
        graph = None
        # if protocol shape doesn't exist we will create one by merging all the shape files
        for m in methods:
            if ".ttl" in m: continue
            
            shape = f"{shape_path}/{m}/shapes.ttl"
            g = Graph()
            g.parse(shape, format="ttl")
            if not graph:
                graph = g
                continue
            graph = graph + g

        shape = f"{output}.ttl"
        graph.serialize(shape, format="ttl")
        print(f"successfully created {shape}")
    
    ## to update EMSA spreed sheed
    # first download spreedsheet as csv in download directory then run the script.
    spreed_sheet_file = '/home/walid/Downloads/EMSA - Resolve diffs between Paratoo and CV - EMSA-LUTs.csv'
    if not os.path.exists(spreed_sheet_file):
        print("EMSA spreed sheet doesn't exist")
        return
    import csv
    with open(spreed_sheet_file) as f:
            reader = csv.DictReader(f)
            rows = list(reader)
    print("Successfully loaded spreed sheet")
    
    json_path = "resources/emsa-resolve-diffs-between-paratoo-and-cv-luts.json"
    import json
    with open(json_path, 'w+') as f:
        json.dump(rows, f, ensure_ascii=False, indent=1)
    print("Successfully created emsa-resolve-diffs-between-paratoo-and-cv-luts.json")
    os.remove(spreed_sheet_file)
    
if __name__ == "__main__":
    main()
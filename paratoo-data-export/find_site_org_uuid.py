from environs import Env
import json
import requests
import copy
import data_export.utilities as U
import os
from dotenv import load_dotenv

load_dotenv(".env.local", override=True)

CORE_URL = os.getenv("CORE_URL")
RANDOM_JWT = os.getenv("EXPORTER_JWT")

# dotenv_file = dotenv.find_dotenv()


HEADERS = {"Authorization": f"Bearer {RANDOM_JWT}"}

plot_selection_protocol_id = "a9cb9e38-690f-41c9-8151-06108caf539d"
plot_definition_protocol_id = "d7179862-1be3-49fc-8ec9-2e219c6f3854"

# SITE_ID= "49c791a2-4730-44be-a4cc-9c6059694970"
# SITE_VISIT_ID= "f3f39c2b-8f60-f88d-4517-c87f63456743"

SITE_ID = "d8b88ca3-5eb6-483d-8907-f640283bdb92"
SITE_VISIT_ID = "5e638a09-44e3-c760-cafb-4d22f6543737"


def replace(any_dict):
    for k, v in any_dict.items():
        if v is None:
            any_dict[k] = ""
        elif type(v) == type(any_dict):
            replace(v)
    return any_dict


################## plot selection survey ###################################
def check_metadata(plot_selection: dict, doFix=False):
    headers = {"Authorization": "Bearer 1234"}
    result = {}
    survey = plot_selection["plot_selection_survey"]
    result["id"] = survey["id"]
    result["orgMintedUUID"] = survey["survey_metadata"]["orgMintedUUID"]
    result["uuid"] = survey["survey_metadata"]["survey_details"]["uuid"]
    result["valid_protocol_id"] = True
    if (
        survey["survey_metadata"]["survey_details"]["protocol_id"]
        != plot_selection_protocol_id
    ):
        result["valid_protocol_id"] = False

    if not doFix:
        return dict(result)

    # fix plot selection survey
    payload = replace(survey)
    survey_id = survey["id"]
    if not result["valid_protocol_id"]:
        payload["survey_metadata"]["survey_details"][
            "protocol_id"
        ] = plot_selection_protocol_id
    if not result["orgMintedUUID"]:
        payload["survey_metadata"]["orgMintedUUID"] = result["uuid"]

    del payload["id"]
    del payload["startdate"]

    # update plot selection survey
    print("updating plot selection survey")
    url = f"https://test.core-api.monitor.tern.org.au/api/plot-selection-surveys/{survey_id}"

    r = requests.request(
        method="PUT",
        url=url,
        headers=headers,
        json=dict({"data": payload}),
        timeout=500,
    )
    data = r.json()
    print(json.dumps(data))

    # org uuid metadata
    print("creating org uuid metadata")
    url = f"https://test.core-api.monitor.tern.org.au/api/org-uuid-survey-metadatas?populate=deep&use-default=true"
    
    r = requests.request(
        method="GET",
        url=url,
        headers=headers,
        timeout=500,
    )
    data = r.json()
    org_uuid_record = None
    for d in data["data"]:
        if (
            d["attributes"]["org_minted_uuid"]
            != payload["survey_metadata"]["orgMintedUUID"]
        ):
            continue
        org_uuid_record = dict(d)

    if org_uuid_record:
        updated = replace(org_uuid_record["attributes"])
        record_id = org_uuid_record["id"]

        updated["survey_details"]["protocol_id"] = payload["survey_metadata"][
            "survey_details"
        ]["protocol_id"]
        print("org uuid metadata exists, so updating")
        url = f"https://test.core-api.monitor.tern.org.au/api/org-uuid-survey-metadatas/{record_id}"
        r = requests.request(
            method="PUT",
            url=url,
            headers=headers,
            json=dict({"data": updated}),
            timeout=500,
        )
        data = r.json()
        print(json.dumps(data))
        return result

    updated = replace(payload["survey_metadata"])
    updated["org_minted_uuid"] = updated["orgMintedUUID"]
    del updated["orgMintedUUID"]
    del updated["id"]
    url = f"https://test.core-api.monitor.tern.org.au/api/org-uuid-survey-metadatas"
    r = requests.request(
        method="POST",
        url=url,
        headers=headers,
        json=dict({"data": updated}),
        timeout=500,
    )
    data = r.json()
    print(json.dumps(data))
    return result


def get_plot_selection_metadata(site_id):
    url = f"https://test.core-api.monitor.tern.org.au/api/plot-selections"
    r = requests.request(
        method="GET",
        url=url,
        headers=HEADERS,
        timeout=500,
    )
    data = r.json()
    for d in data:
        if d["uuid"] != site_id:
            continue
        return dict(d)


################## plot definition survey ###################################
def get_plot_visit_metadata(plot_visit_id, site_id, site_visit_id):
    url = f"{CORE_URL}/api/plot-definition-surveys"
    
    r = requests.request(
        method="GET",
        url=url,
        headers=HEADERS,
        timeout=500,
    )
    data = r.json()
    
    for d in data:
        # id
        if d["plot_visit"]["id"] != plot_visit_id: continue
        
        # plot selection uuid
        if d["plot_visit"]["plot_layout"]["plot_selection"]["uuid"] != site_id:
            continue
        # site visit uuid
        site_name = d["plot_visit"]["plot_layout"]["plot_selection"]["plot_label"]
        visit_start_date = d["plot_visit"]["start_date"]
        visit_id = U.Utilities.compute_md5_hash(value=f"{visit_start_date}{site_name}")
        if visit_id != site_visit_id: continue
        return dict(d)

def add_record_to_org_uuids(survey_metadata: dict, doFix=False):
    url = f"{CORE_URL}/api/org-uuid-survey-metadatas"
    payload = {
        "org_minted_uuid": survey_metadata["orgMintedUUID"],
        "org_opaque_user_id": survey_metadata["org_opaque_user_id"],
        "survey_details": dict(survey_metadata["survey_details"]),
        "provenance": dict(survey_metadata["provenance"]),
    }
    payload = replace(payload)
    r = requests.request(
        method="GET",
        url=f"{url}?populate=deep",
        headers=HEADERS,
        timeout=500,
    )
    data = r.json()
    for d in data["data"]:
        if d["attributes"]["org_minted_uuid"] != payload["org_minted_uuid"]: continue
        print("data exists ")
        return dict(d)
    
    print("data not exist in org-uuid-survey-metadata")
    print(json.dumps(payload))
    if not payload["org_minted_uuid"]:
        print("org_minted_uuid is null")
        
    if not doFix: return
    
    if not payload["org_minted_uuid"]:  
        payload["org_minted_uuid"] = survey_metadata["survey_details"]["uuid"]
    
    r = requests.request(
        method="POST",
        url=url,
        headers=HEADERS,
        json=dict({"data": payload}),
        timeout=500,
    )
    data = r.json()
    print(json.dumps(data))
    return data

def get_org_uuids(protocol_id, store_json=False):
    url = f"{CORE_URL}/api/org-uuid-survey-metadatas?populate=deep"
    r = requests.request(
        method="GET",
        url=url,
        headers=HEADERS,
        timeout=500,
    )
    data = r.json()

    surveys = []
    for d in data["data"]:
        if d["attributes"]["survey_details"]["protocol_id"] != protocol_id: continue
        surveys.append(d)
    print("total found")
    print(len(surveys))
    if store_json:
        with open(f"{protocol_id}_surveys.json", "w", encoding="utf-8") as f:
            json.dump(surveys, f, ensure_ascii=False, indent=2)

def main():
    # plot_selection = get_plot_selection_metadata(site_id=SITE_ID)
    # status = check_metadata(plot_selection=dict(plot_selection), doFix=True)
    # print(status)
    
    # plot = get_plot_visit_metadata(plot_visit_id=81, site_id=SITE_ID, site_visit_id=SITE_VISIT_ID)
    # print(json.dumps(plot["survey_metadata"]))
    # add_record_to_org_uuids(survey_metadata=plot["survey_metadata"], doFix=False)
    # print(plot["survey_metadata"])

    # find org uuids based on survey model
    get_org_uuids(protocol_id="a76dac21-94f4-4851-af91-31f6dd00750f", store_json=True)

if __name__ == "__main__":
    main()

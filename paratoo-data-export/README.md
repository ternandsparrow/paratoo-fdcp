# Features this demonstrates
  - create turtle or json-ld
  - relationships between protocols and DCCEEW vocabularies
  - endpoint (`/data-export/v1/upload-all`) to create and upload turtle/json-ld
  - swagger dashboard
  - redis cache
  - validate RDF data
  - file upload to aws

# Running this APP locally
While doing local development, you'll need redis cache. We have a bash CLI script
that will create a Dockerised Redis then configure and start the Flask app
in dev mode.
 1. clone the repo
 2. make sure you have the dependencies installed
    ```bash
    pip install -r requirements.txt
    ```
 3. optionally override any config items you need by creating a `.env.local`
     file and adding overrides to values you find in `.env`.
     ```bash
     touch .env.local
     vim .env.local
     ```
 4. start the server
    ```bash
    ./pdataexport
    ```
  5. you can stop the data-export server with Ctrl-c

## When do the changes take effect?
At the very least, you'll need to run `./pdataexport` after overriding config items.

**Important**: some of the config values are used at creation time of the
supporting docker containers. If you change a value, you'll be safest if you
remove all supporting containers and their volumes/data, then recreate with the
new values.
```bash
./pdataexport down --volumes
./pdataexport
```

## Protocols Mappings
Make sure all the fields in the `data-export/protocols.json` are up to date.
1. `uuid` : extracts all the models and associated fields from the Monitor using this.
2. `survey-model` : e.g. bird-survey is the survey model of "Vertebrate Fauna - Bird Survey".
3. `custom-configs`: configs to handle custom tasks. e.g. handle-barcode or handle-media
4. `feature-types` : list of all the feature types associated with the protocol.
5. `uri.attributes` : uri of the list of attributes.
6. `uri.properties` : uri of the list of properties.
7. `uri.procedure` : uri of the procedure.

## Fields Mappings
Some of the fields have different names. For example, "air temperature" can be "temperature" and "barcode" can be "voucher_barcode" or "genetic_voucher_barcode". We handle these with the
`data-export/names_map.json` fields mappings.


#/bin/bash
PACKAGE_NAME=paratoo-data-export

DOCKER_REG_URL=mdwalidalnaim/${PACKAGE_NAME}:latest

echo "Building Docker Image with Params"
echo " > Package Name: ${PACKAGE_NAME}"
echo

docker build \
    --label PACKAGE_NAME=${PACKAGE_NAME} \
    -t ${DOCKER_REG_URL} \
    -f Dockerfile \
    .

# tag the image
docker tag ${DOCKER_REG_URL}

# runing docker image
docker run --network=host \
-e LOG_LEVEL='verbose' \
-e CACHE_REDIS_URL='redis://localhost:6379/0' \
-- mdwalidalnaim/paratoo-data-export:latest

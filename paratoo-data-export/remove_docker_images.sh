
#/bin/bash
docker history mdwalidalnaim/paratoo-data-export:latest | awk '!/IMAGE|<missing>/ {print $1}' | xargs -I {} docker rmi {} --force
docker history paratoo-data-export_paratoo-data-export:latest | awk '!/IMAGE|<missing>/ {print $1}' | xargs -I {} docker rmi {} --force
docker history paratoo-data-export_app:latest | awk '!/IMAGE|<missing>/ {print $1}' | xargs -I {} docker rmi {} --force
docker stop redis
docker stop data-export-redis
docker history redis:latest | awk '!/IMAGE|<missing>/ {print $1}' | xargs -I {} docker rmi {} --force
docker system prune
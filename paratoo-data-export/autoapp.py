import logging.config

LOG_COLOR = {
    "INFO": "\033[1;32m%s\033[1;0m",
    "WARN": "\033[1;33m%s\033[1;0m",
    "DEBUG": "\033[1;34m%s\033[1;0m",
    "ERROR": "\033[1;31m%s\033[1;0m",
    'WARNING': "\033[1;33m%s\033[1;0m",
}
logging.addLevelName(
    logging.INFO, LOG_COLOR["INFO"] % logging.getLevelName(logging.INFO)
)
logging.addLevelName(
    logging.WARN, LOG_COLOR["WARN"] % logging.getLevelName(logging.WARN)
)
logging.addLevelName(
    logging.WARNING, LOG_COLOR["WARNING"] % logging.getLevelName(logging.WARNING)
)
logging.addLevelName(
    logging.DEBUG, LOG_COLOR["DEBUG"] % logging.getLevelName(logging.DEBUG)
)
logging.addLevelName(
    logging.ERROR, LOG_COLOR["ERROR"] % logging.getLevelName(logging.ERROR)
)

from data_export.app import create_app

app = create_app()

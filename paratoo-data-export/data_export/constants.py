from environs import Env
import os

env = Env()
env.read_env()

# endpoints
TERN_RESOURCE_ENDPOINT = "https://linkeddata.tern.org.au/api/v2.0/viewer/resource"
DAWE_VOCABS_ENDPOINT = "https://graphdb.tern.org.au/repositories/dawe_vocabs_core"
MONITOR_PREFIX = "https://linked.data.gov.au/dataset/bdr"
MONITOR_PREFIX_GLOBAL = "https://linked.data.gov.au/dataset/bdr/org/merit"
SENTRY_DNS = env.str("SENTRY_DNS", default=None)

# rdf ids
PLOT_LAYOUT_RDF_ID = MONITOR_PREFIX + "plot-layout/"
PLOT_SELECTION_RDF_ID = MONITOR_PREFIX + "plot-selection/"
SITE_RDF_ID = MONITOR_PREFIX + "site/"
DATASET_RDF_ID = MONITOR_PREFIX + "dataset/"
PROJECT_RDF_ID = MONITOR_PREFIX + "relevant-project/"
PROCEDURE_RDF_ID = MONITOR_PREFIX + "procedure/"
OBSERVATION_COLLECTION_RDF_ID = MONITOR_PREFIX + "observation-collection/"
OBSERVATION_RDF_ID = MONITOR_PREFIX + "observation/"
FEATURE_OF_INTEREST_RDF_ID = MONITOR_PREFIX + "feature-of-interest/"

# rdf types
GEO_POINT_TYPE = "<http://www.opengis.net/def/crs/EPSG/0/7844>"

# agent and roles
AGENT = env.str("AGENT", default="monitor")
DEFAULT_ROLE = env.str("DEFAULT_ROLE", default="collector")

# api path
CORE_URL = env.str("CORE_URL", default="http://localhost:1337")
PROTOCOL_API = "api/protocols"

# file path
FILE_ROOT_PATH = env.str("FILE_ROOT_PATH", default="paratoo-data-export/data_export/")
RESOURCE_DIRECTORY = os.environ["RESOURCE_DIRECTORY"]

# models
PLOT_MODELS = ["plot-visit", "plot-layout", "plot-location"]
DEFAULT_SURVEY_MODEL = "plot-visit"
PROTOCOL_SETS = env.str("PROTOCOL_SETS", default="1,2,3")
REUSABLE_CONCEPTS = env.str("REUSABLE_CONCEPTS", default="")
RESOURCE_TAG = env.str("RESOURCE_TAG", default="")

# api settings
API_TIMEOUT = 60  # seconds
ENABLE_CACHE = env.bool("ENABLE_CACHE", default=False)
EXPORTER_DEPLOYED_ENV_NAME = env.str("EXPORTER_DEPLOYED_ENV_NAME", default="dev")
FLASK_ENV = env.str("FLASK_ENV", default="development")
EMSA_RESOVED_LUT_CONCEPTS = env.str(
    "EMSA_RESOVED_LUT_CONCEPTS",
    default="https://monitor-static-resources.s3.ap-southeast-2.amazonaws.com/exporter/emsa-resolve-diffs-between-paratoo-and-cv-luts.json",
)

# browser
CROME_HUB_URL = env.str("CROME_HUB_URL", default="http://127.0.0.1:4444/wd/hub")

# oidc settings
OIDC_URL = env.str("OIDC_URL", default="https://dev.app.monitor.tern.org.au")
OIDC_USER = env.str("OIDC_USER", default="test")
OIDC_PASS = env.str("OIDC_PASS", default="test")

# vocabs
VOCAB_SERVER = env.str("VOCAB_SERVER", default="https://core.vocabs.paratoo.tern.org.au")

# store response mock
STORE_MOCK_HTTP_OUTPUT = env.bool("STORE_MOCK_HTTP_OUTPUT", default=False)
MOCK_OUTPUT_PREFIX = env.str("MOCK_OUTPUT_PREFIX", default="")

# token
VALIDATE_TOKEN_API = env.str("VALIDATE_TOKEN_API", default="/api/validate-token")

# settings and formats
API_DATE_FORMATE = "%Y/%m/%d %H:%M:%S"
DB_DATE_FORMATE = "%Y-%m-%dT%H:%M:%S.%fZ"
RDF_FORMAT = "ttl"

# uri
GRAPH_URI = "https://linked.data.gov.au/def/nrm"
CAMERA_IMPLEMENTS_URI = (
    "https://linked.data.gov.au/def/nrm/05669173-2fe7-4b70-ba67-2e09fbe87de9"
)
CAMERA_SAMPLER_URI = (
    "http://linked.data.gov.au/def/tern-cv/11e03f36-7ada-4333-88e2-38c9205f2749"
)
PLOT_ATTRIBUTE_URI = [
    # one uri for plot location and another for plot layout
    "https://linked.data.gov.au/def/nrm/0183ecf9-7e7a-4481-ba43-a926dfc638f9",
    "https://linked.data.gov.au/def/nrm/d1e0ed8d-04a4-43de-87be-e6863de148cb",
]
PLOT_PROPERTIES_URI = [
    "https://linked.data.gov.au/def/nrm/bfac1b1f-a14e-4e9a-ab7f-c43a8bc1a312"
]

# ttls
OUTPUT_TTL = "output.ttl"
TERN_ONTOLOGY_TTL = "tern_ontology.ttl"
PROTOCOL_SHAPE_TTL = "protocol_shape.ttl"

# query output types
# "https://w3id.org/tern/ontologies/tern/hasCategoricalValuesCollection" or
# "https://w3id.org/tern/ontologies/tern/hasCategoricalCollection"
LUT_QUERY_TYPES = ["Categorical", "Collection"]
FEATURE_QUERY_TYPE = "FeatureType"

# enums
from enum import Enum


class FieldType(Enum):
    ATTRIBUTE = "attribute"
    PROPERTY = "property"


class SampleType(Enum):
    FEATURE = "feature"
    MATERIAL = "material"
    IMAGE = "image"
    DEFAULT = "default"


class FileType(Enum):
    JSON = "json"
    TURTLE = "ttl"


class BarcodeType(Enum):
    PLANT = "plant"
    ANIMAL = "animal"
    
class DataType(Enum):
    STR = "string"
    INT = "integer"
    FLOAT = "number"
    BOOL = "boolean"
    DICT = "object"


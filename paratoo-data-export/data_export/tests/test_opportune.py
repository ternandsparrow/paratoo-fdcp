import requests_mock
import webtest

from tests.test_helper import load_protocol_mock, assert_body


class TestOpportune:
    def test_opportune_success(self, testapp_no_auth: webtest.app.TestApp):
        org_uuid = "e34a0058-82c6-47e7-9e2d-22037e941a41"
        with requests_mock.Mocker() as mock:
            # load mock response
            load_protocol_mock(
                org_uuid=org_uuid,
                mock=mock,
            )

            r = testapp_no_auth.post_json(
                url="/data-export/v1/export-collection", params={"org_uuid": org_uuid, "force": True}
            )

            assert r.status_code == 200
            assert_body(body=r.json, org_uuid=org_uuid)

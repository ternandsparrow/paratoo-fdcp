import re
import os
import json
import typing
import from_root

import requests_mock

from data_export.utilities import Utilities as utility

# Set mock responses
CONFIGS = utility.load_json_file(file_name="tests/config.json")
TEMP = ""


def load_file_with_prefix(prefix):
    files = from_root.from_root(f"paratoo-data-export/data_export/tests/mock/")
    for file in os.listdir(files):
        if "json" not in file:
            continue
        if prefix not in file:
            continue
        return load_config_from_file(name=f"{file}")
    return {}


def load_protocol_mock(
    org_uuid: str,
    mock: requests_mock.Mocker,
):

    files = from_root.from_root(f"paratoo-data-export/data_export/tests/mock/")
    for file in os.listdir(files):
        if "json" not in file:
            continue
        if "global" not in file and org_uuid not in file:
            continue

        data = load_config_from_file(name=f"{file}")
        mock_data = {}
        mock_data["method"] = data["method"].upper()
        mock_data["url"] = data["url"]
        
        if data["body"] or data["params"]:
            mock_data["json"] = response_callback

        if not data["body"] and not data["params"]:
            mock_data["json"] = data["response"]

        mock_data["status_code"] = 200
        mock_data["complete_qs"] = True
        mock_data["request_headers"] = data["headers"]
        mock.register_uri(**mock_data)


def response_callback(request, context):
    prefix = ""
    data = {}

    if "store-key" in request.headers:
        prefix = request.headers["store-key"]
    if not prefix:
        prefix = request.url
        data = request.json()

    file_name = utility.url_to_file_name(
        url=prefix, payload=data, org_uuid=None)
    res = load_file_with_prefix(prefix=file_name)
    return dict(res["response"])


def load_config_from_file(name):
    test_file_root = "tests/mock/"
    return utility.load_json_file(file_name=test_file_root + name)


def assert_body(
    body: dict,
    org_uuid: str
):
    assert body[org_uuid]["upload_status"]["create_ttl"] == True
    assert body[org_uuid]["upload_status"]["upload_ttl"] == False
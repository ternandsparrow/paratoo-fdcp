# -*- coding: utf-8 -*-
"""Application configuration.

Most configuration is set via environment variables.

For local development, use a .env file to set
environment variables.
"""
from environs import Env
import os

env = Env()
env.read_env()

ENV = env.str("FLASK_ENV", default="production")
DEBUG = ENV == "development"

APPLICATION_ROOT = "/data-export"

# paratoo configuration
SERVICE_ENABLED = env.bool("SERVICE_ENABLED", default=True)
CORE_URL = env.str("CORE_URL", default="http://localhost:1337")
# as merit doesn't have org
ENABLE_ORG_AUTH = env.bool("ENABLE_ORG_AUTH", default=False)
RANDOM_JWT = env.str("EXPORTER_JWT", default="1234")
ORG_URL = env.str("ORG_URL", default="http://localhost:1338")

ENABLE_SHACL_VALIDATOR = env.bool("ENABLE_SHACL_VALIDATOR", default=True)

ENABLE_UPLOAD = env.bool("ENABLE_UPLOAD", default=False)
S3_BUCKET = env.str("AWS_BUCKET", default="paratoo-data-export-abis")
AWS_KEY = os.environ["AWS_ACCESS_KEY_ID"]
AWS_SECRET = os.environ["AWS_SECRET_ACCESS_KEY"]
AWS_RESOURCE_BUCKET = os.environ["AWS_RESOURCE_BUCKET"]
AWS_RESOURCE_PROTOCOL = None

RESOURCE_DIRECTORY = os.environ["RESOURCE_DIRECTORY"]

ENABLE_CACHE = env.bool("ENABLE_CACHE", default=False)
CACHE_TYPE = os.environ["CACHE_TYPE"]
CACHE_REDIS_HOST = os.environ["CACHE_REDIS_HOST"]
CACHE_REDIS_PORT = os.environ["CACHE_REDIS_PORT"]
CACHE_REDIS_DB = os.environ["CACHE_REDIS_DB"]
CACHE_REDIS_URL = os.environ["CACHE_REDIS_URL"]
CACHE_DEFAULT_TIMEOUT = os.environ["CACHE_DEFAULT_TIMEOUT"]

SWAGGER = {
    "title": "Monitor Data Export",
    "version": "1.0.2",
    "uiversion": 3,
    "specs": [{"endpoint": "apispec", "route": APPLICATION_ROOT + "/v1/apispec.json"}],
    "specs_route": APPLICATION_ROOT + "/apidocs/",
    "openapi": "3.0.2",
    "static_url_path": APPLICATION_ROOT + "/flasgger_static",
    "doc_expansion": "none",
    "components": {"schemas": {}},
}

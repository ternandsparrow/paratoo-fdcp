"""Defines fixtures available to all tests."""
import logging
import pytest

from webtest import TestApp
from flask_caching import Cache
from data_export.app import create_app


@pytest.fixture
def app():
    """Create application for the tests."""
    _app = create_app("tests.settings")
    _app.logger.setLevel(logging.ERROR)
    ctx = _app.test_request_context()
    ctx.push()

    yield _app

    ctx.pop()


@pytest.fixture
def testapp_no_auth(app):
    """Create Webtest app."""
    return TestApp(app)
import requests_mock
import webtest

from tests.test_helper import load_protocol_mock, assert_body


class TestCovers:
    def test_plot_description_full_success(self, testapp_no_auth: webtest.app.TestApp):
        org_uuid = "b4aef641-4f89-4aaf-b182-3c455370b3cd"
        with requests_mock.Mocker() as mock:
            # load mock response
            load_protocol_mock(
                org_uuid=org_uuid,
                mock=mock,
            )

            r = testapp_no_auth.post_json(
                url="/data-export/v1/export-collection", params={"org_uuid": org_uuid, "force": True}
            )
            assert r.status_code == 200
            assert_body(body=r.json, org_uuid=org_uuid)
    
    def test_plot_description_lite_success(self, testapp_no_auth: webtest.app.TestApp):
        org_uuid = "df85a371-de7a-45f3-9a15-e442f8ce2627"
        with requests_mock.Mocker() as mock:
            # load mock response
            load_protocol_mock(
                org_uuid=org_uuid,
                mock=mock,
            )

            r = testapp_no_auth.post_json(
                url="/data-export/v1/export-collection", params={"org_uuid": org_uuid, "force": True}
            )
            assert r.status_code == 200
            assert_body(body=r.json, org_uuid=org_uuid)

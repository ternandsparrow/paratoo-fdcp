import flask

from flask_restful import Resource, Api
from data_export.schema import JSONSCHEMA_ORG_UUIDS, JSONSCHEMA_ORG_UUID, JSONSCHEMA_SKIPS, JSONSCHEMA_BUCKET_NAME, JSONSCHEMA_BUCKET_PREFIX

import data_export.extensions as extensions


def register_blueprint(app):
    api_bp = flask.Blueprint(
        "api",
        __name__,
        url_prefix=app.config.get("APPLICATION_ROOT", "") + "/v1",
        static_folder="../static",
    )
    api = Api(api_bp)

    # exporter apis
    api.add_resource(ExportCollections, "/export-collections")
    api.add_resource(ExportCollection, "/export-collection")
    api.add_resource(ExportAllCollections, "/export-all-collections")
    api.add_resource(UpdateExportedTurtles, "/update-exporter-bucket")

    # misc
    api.add_resource(ActiveProtocols, "/active-protocols")
    api.add_resource(GenearateOidcToken, "/oidc-token")
    api.add_resource(ExportedCollectionUuids, "/exported-collection-uuids")
    api.add_resource(UploadAppProperties, "/upload-app-configs")
    api.add_resource(DownloadAppProperties, "/download-app-configs")

    app.register_blueprint(api_bp)


class ExportCollections(Resource):
    def post(self):
        """Send request to generate RDF
        ---
        tags: ["Abis-Export"]
        requestBody:
          required: true
          content:
            application/json:
              schema:
                type: object
                properties:
                  org_uuids:
                    type: array
                    items:
                        type: string
                    example: ['e34a0058-82c6-47e7-9e2d-22037e941a41', 'e34a0058-82c6-47e7-9e2d-22037e941a41']
                  force:
                    type: boolean
                    example: false

        responses:
          '200':
            response: ok
        """
        # Now apply json schema validation
        r = extensions.validator_service.validate_json_schema(
            JSONSCHEMA_ORG_UUIDS, payload_expected=True
        )
        org_uuids = r["org_uuids"]
        force = r["force"]
        result = extensions.export_service.generate_ttls_from_org_uuids(
            org_uuids=org_uuids, force=force)
        return dict(result)


class ExportCollection(Resource):
    def post(self):
        """Send request to generate RDF
        ---
        tags: ["Abis-Export"]
        requestBody:
          required: true
          content:
            application/json:
              schema:
                type: object
                properties:
                  org_uuid:
                    type: string
                    example: e34a0058-82c6-47e7-9e2d-22037e941a41
                  force:
                    type: boolean
                    example: false

        responses:
          '200':
            response: ok
        """
        # Now apply json schema validation
        r = extensions.validator_service.validate_json_schema(
            JSONSCHEMA_ORG_UUID, payload_expected=True
        )
        org_uuid = r["org_uuid"]
        force = r["force"]
        result = extensions.export_service.generate_ttls_from_org_uuids(org_uuids=[
                                                                        org_uuid], force=force)
        
        return dict(result)


class ExportAllCollections(Resource):
    def post(self):
        """Send request to generate RDF
        ---
        tags: ["Abis-Export"]
        requestBody:
          required: true
          content:
            application/json:
              schema:
                type: object
                properties:
                  skips:
                    type: array
                    items:
                        type: string
                    example: ['e34a0058-82c6-47e7-9e2d-22037e967a41']
                  force:
                    type: boolean
                    example: false

        responses:
          '200':
            response: ok
        """
        # Now apply json schema validation
        r = extensions.validator_service.validate_json_schema(
            JSONSCHEMA_SKIPS, payload_expected=True
        )
        skips = r["skips"]
        force = r["force"]
        org_uuids = extensions.protocol_service.get_all_org_uuids(skips)
        result = extensions.export_service.generate_ttls_from_org_uuids(
            org_uuids=org_uuids, force=force)
        return dict(result)


class UpdateExportedTurtles(Resource):
    def post(self):
        """Send request to generate RDF
        ---
        tags: ["Abis-Export"]
        requestBody:
          required: true
          content:
            application/json:
              schema:
                type: object
                properties:
                  bucket:
                    type: string
                    example: 'internal-test-abis-export'
                  force:
                    type: boolean
                    example: false

        responses:
          '200':
            response: ok
        """
        # Now apply json schema validation
        r = extensions.validator_service.validate_json_schema(
            JSONSCHEMA_BUCKET_NAME, payload_expected=True
        )
        force = r["force"]
        org_uuids = extensions.aws_service.get_exported_uuids(
            bucket=r["bucket"])
        result = extensions.export_service.generate_ttls_from_org_uuids(
            org_uuids=org_uuids, force=force)

        return dict(result)


class ActiveProtocols(Resource):
    def get(self):
        """List of active protocols
        ---
        tags: ["Misc"]
        responses:
          '200':
            response: ok
        """
        protocols = extensions.protocol_service.get_list_of_protocol()
        if not protocols:
            protocols = []
        return flask.jsonify({"protocols": protocols})


class GenearateOidcToken(Resource):
    def get(self):
        """generate oidc token using headless browser
        ---
        tags: ["Misc"]
        responses:
          '200':
            response: ok
        """
        auth_token = extensions.web_driver_service.generate_ala_token()
        return auth_token


class ExportedCollectionUuids(Resource):
    def post(self):
        """get the list of all exported uuids
        ---
        tags: ["Misc"]
        requestBody:
          required: true
          content:
            application/json:
              schema:
                type: object
                properties:
                  bucket:
                    type: string
                    example: internal-test-abis-export 

        responses:
          '200':
            response: ok
        """
        # Now apply json schema validation
        r = extensions.validator_service.validate_json_schema(
            JSONSCHEMA_BUCKET_NAME, payload_expected=True
        )
        bucket = r["bucket"]

        return extensions.aws_service.get_exported_uuids(bucket=bucket)


class UploadAppProperties(Resource):
    def post(self):
        """Upload app configs to cloud
        ---
        tags: ["Misc"]
        requestBody:
          required: true
          content:
            application/json:
              schema:
                type: object
                properties:
                  bucket_prefix:
                    type: string
                    example: localdev
                  force:
                    type: boolean
                    example: false

        responses:
          '200':
            response: ok
        """
        # Now apply json schema validation
        r = extensions.validator_service.validate_json_schema(
            JSONSCHEMA_BUCKET_PREFIX, payload_expected=True
        )
        bucket_prefix = r["bucket_prefix"]
        force = r["force"]
        files = extensions.aws_service.upload_app_configs(prefix=bucket_prefix, force=force)
        return {"uploaded": files}


class DownloadAppProperties(Resource):
    def post(self):
        """Upload app configs to cloud
        ---
        tags: ["Misc"]
        requestBody:
          required: true
          content:
            application/json:
              schema:
                type: object
                properties:
                  bucket_prefix:
                    type: string
                    example: localdev
                  force:
                    type: boolean
                    example: false

        responses:
          '200':
            response: ok
        """
        # Now apply json schema validation
        r = extensions.validator_service.validate_json_schema(
            JSONSCHEMA_BUCKET_PREFIX, payload_expected=True
        )
        bucket_prefix = r["bucket_prefix"]
        force = r["force"]
        result = extensions.aws_service.app_properties(
            bucket_prefix=bucket_prefix, force_download=force)
        return result

import sys
import json
import os
import flask
import flask.logging
import requests
import shutil
from flask_caching import Cache
from typing import Optional, List, Union
from copy import copy
from uuid import uuid4
from datetime import datetime
from rdflib import Graph

from data_export.constants import (
    OUTPUT_TTL,
    FileType,
    DB_DATE_FORMATE,
    FILE_ROOT_PATH,
    FieldType,
    API_DATE_FORMATE,
    ENABLE_CACHE,
    TERN_ONTOLOGY_TTL,
    RDF_FORMAT,
    PROTOCOL_SHAPE_TTL,
    API_TIMEOUT,
    DataType,
    CORE_URL,
    REUSABLE_CONCEPTS,
    RESOURCE_DIRECTORY
)


class Utilities:
    FIELD_VISIBILITY = {}

    def __init__(self):
        self.enabled = True

    @staticmethod
    def generate_uuid():
        return str(uuid4())

    @staticmethod
    def uuid(data: dict):
        if not isinstance(data, dict) or not data:
            flask.current_app.logger.error(
                f"Can not find uuid, data is not an object")
            return None

        if "uuid" in data:
            flask.current_app.logger.debug(f"UUID exists")
            if data["uuid"]:
                return data["uuid"]
        if "id" in data:
            flask.current_app.logger.debug(f"ID exist")
            return Utilities.compute_md5_hash(value=str(data["id"]))

        flask.current_app.logger.warn(f"UUID/ID doesn't exist")
        return Utilities.generate_uuid()

    @staticmethod
    def find_object_by_key(object: dict, keys: List[str]):
        """finds matched object using a List of possible keys"""
        data = dict(object)
        for key in keys:
            if key not in list(data.keys()):
                continue
            return {"object": data[key], "key": key}

        return None

    @staticmethod
    def get_uuid_from_uri(uri: str):
        if not uri:
            return Utilities.generate_uuid()

        splits = uri.split("/")
        return splits[-1]

    @staticmethod
    def fix_graph_properties(graph_data: any):
        if not graph_data:
            return

        graph_data.serialize(OUTPUT_TTL, format="turtle")
        from pathlib import Path

        file = Path(OUTPUT_TTL)
        file.write_text(
            "@prefix unit: <http://qudt.org/vocab/unit/> .\n" + file.read_text()
        )
        # hacky solution to fix properties e.g. tern:unit
        file.write_text(file.read_text().replace('"tern_unit:', ""))
        file.write_text(file.read_text().replace(':tern_unit"', ""))

        g = Graph()
        g.parse(OUTPUT_TTL, format="turtle")
        return g

    @staticmethod
    def is_output_already_created(output: str):
        o = f"outputs/{output}"

        if not o:
            return False
        if not os.path.exists(o):
            return False

        return True

    @staticmethod
    def generate_output_name(
        protocol: dict,
        protocol_name: str,
        survey: dict
    ):
        org_uuid = survey["survey_metadata"]["orgMintedUUID"]
        start_date = survey["survey_metadata"]["survey_details"]["time"]
        output = f"{protocol_name}-org-uuid-{org_uuid}-start-date-{start_date}"

        if "plot_visit" not in survey:
            return output

        if not Utilities.has_custom_config(protocol=protocol, key="output-site-name"):
            return output

        site_name = survey["plot_visit"]["plot_layout"]["plot_selection"]["plot_label"]
        if "value" in site_name:
            site_name = site_name["value"]
        output = f"site-{site_name.lower()}"
        return output

    @staticmethod
    def get_start_end_from_resolve_point(data: dict, plot_data: dict, key_to_find: str, resolved_fields: list):
        transect_details = Utilities.find_property_with_key(
            object=data,
            field_name=None,
            key_to_find=key_to_find,
            model=None,
            outputs=[],
        )
        result = {
            "start": None,
            "start_position": None,
            "end": None,
            "end_position": None
        }
        for td in transect_details:
            if td["field_name"] not in resolved_fields:
                continue
            result["start"] = td["value"]["value"]
            result["start_position"] = Utilities.get_symbol_position(
                plot_points=plot_data["plot_layout"]["plot_points"], symbol=result["start"]
            )
            end_direction = Utilities.get_end_direction_from_start(
                start_obj={"symbol": result["start"]}, orientation=None
            )
            result["end"] = end_direction["end"]
            result["end_position"] = Utilities.get_symbol_position(
                plot_points=plot_data["plot_layout"]["plot_points"], symbol=result["end"]
            )
            return dict(result)
        return result

    @staticmethod
    def get_end_direction_from_start(start_obj: dict, orientation: str, ):
        end = None
        direction = None

        start = start_obj["symbol"]
        direction_attr = dict(start_obj)

        if isinstance(orientation, dict):
            if "value" in orientation:
                orientation = orientation["value"]
                
        # between -45° and -1° for plots orientated left,
        #   and between 1° and 45° for plots orientated right.
        if not orientation:
            orientation = 0

        

        if "N" in start:
            end = start.replace("N", "S")
            direction = "South"
            if orientation > 0:
                direction = "South East"
            if orientation < 0:
                direction = "South West"

        if "S" in start:
            end = start.replace("S", "N")
            direction = "North"
            if orientation > 0:
                direction = "North West"
            if orientation < 0:
                direction = "North East"

        if "W" in start:
            end = start.replace("W", "E")
            direction = "East"
            if orientation > 0:
                direction = "North East"
            if orientation < 0:
                direction = "South East"

        if "E" in start:
            end = start.replace("E", "W")
            direction = "West"
            if orientation > 0:
                direction = "South West"
            if orientation < 0:
                direction = "North West"

        direction_attr["symbol"] = direction
        direction_attr["label"] = direction
        direction_attr["api_source"] = "https://core.vocabs.paratoo.tern.org.au/api/lut-directions"

        return {"end": end, "direction": dict(direction_attr)}

    @staticmethod
    def get_symbol_position(plot_points: list, symbol: str):

        for point in plot_points:
            if point["name"]["symbol"] == symbol:
                return dict(point)
            if point["name"]["symbol"] == f"{symbol}1":
                return dict(point)

        return None

    @staticmethod
    def is_setup_id_or_model_based_obs(protocol: dict, data: dict):
        flask.current_app.logger.debug(f"checking  setup id based or not ")
        if not protocol:
            return False
        is_skip_operation = Utilities.is_skip_operation(
            protocol=protocol, data=data)
        if "setup-id-based-protocol" in protocol:
            if not is_skip_operation:
                return protocol["setup-id-based-protocol"]

        if "model-based-protocol" in protocol:
            if not is_skip_operation:
                return protocol["model-based-protocol"]

        return False

    @staticmethod
    def is_skip_operation(protocol: dict, data: dict):
        if "skip-operation-expected-value" not in protocol:
            return False

        for k, v in protocol["skip-operation-expected-value"].items():
            if k not in data:
                continue
            if data[k] != v:
                continue
            return True

        return False

    @staticmethod
    def grouped_value(data: dict, field: dict, grouped_list: list):
        if not data:
            return None
        if not field:
            return None
        if not isinstance(data, dict):
            return None

        if field not in grouped_list:
            return None

        if "value" in data:
            return {"name": data["value"]}

        # new or existing
        if "site_id" in data:
            return {"name": data["site_id"]["value"]}
        return None

    @staticmethod
    def is_null_or_empty(object: dict, field: str):
        """checks wether the field in the object is null or empty"""
        if not object:
            return True
        if not field:
            return True
        if field not in object:
            return True
        if not object[field]:
            return True

        return False

    @staticmethod
    def find_text_by_keys(text: str, keys: List[str]):
        """finds matched string using a List of possible keys"""
        for key in keys:
            if key not in text:
                continue
            return {"object": text, "key": key}

        return None

    @staticmethod
    def minify_label(value: str):
        if not value:
            return ""
        label = (
            value.lower()
            .replace("_", "")
            .replace("-", "")
            .replace(" ", "")
            .replace("(", "")
            .replace(")", "")
            .replace("/", "")
        )
        return label

    @staticmethod
    def get_custom_configs(object: dict, field_name: str, protocol: dict, ignore: list = []):
        """checks whether custom flags exist or not"""
        if not isinstance(object, dict):
            return ""
        keys = list(object.keys())
        skips = []
        # don't need to skip
        skip_handlers = ["handle-specimen-type-fields"]
        skip_handlers = skip_handlers + ignore

        if "skip-fields" in protocol:
            skips = skips + protocol["skip-fields"]

        if "custom-configs" in protocol:
            if "skip-fields" in protocol["custom-configs"]:
                skips = skips + protocol["custom-configs"]["skip-fields"]
        for k in keys:
            if k in skip_handlers:
                continue
            if "handle" in k:
                return k
            if k in skips:
                return k

        return ""

    @staticmethod
    def get_feature_type_index(obs_with_all: dict, feature_types: list, protocol: dict):
        feature_index = 0

        if "conditional-feature-types" in protocol:
            for condition in protocol["conditional-feature-types"]:
                if condition["field"] not in obs_with_all:
                    continue
                if not obs_with_all[condition["field"]]:
                    continue
                conditional_Type = None
                if "symbol" in obs_with_all[condition["field"]]:
                    if (
                        obs_with_all[condition["field"]]["symbol"]
                        in condition["values"]
                    ):
                        conditional_Type = condition["type"]
                if obs_with_all[condition["field"]] in condition["values"]:
                    conditional_Type = condition["type"]

                if conditional_Type:
                    for idx, f in enumerate(feature_types):
                        if f["value"] != conditional_Type:
                            continue
                        feature_index = idx

        return feature_index

    @staticmethod
    def find_object_using_id(id: str, objects: List[dict]):
        """finds matched object from a List of objects using id"""
        for object in objects:
            if object["id"] != id:
                continue

            return dict(object)
        return None

    @staticmethod
    def find_objects_using_id(objects: List[dict], field_names: List[dict], id: str):
        """finds a List of matched objects from a List of objects using a List of possible fields and id"""
        matched_objects = []
        matched_key = None
        for object in objects:
            temp_object = dict(object)
            result = Utilities.find_object_by_key(
                object=temp_object, keys=field_names)
            if not result:
                continue
            if not result["object"]:
                continue

            survey = result["object"]
            matched_key = result["key"]
            if survey["id"] != id:
                continue
            matched_objects.append(temp_object)

        return {"objects": matched_objects, "key": matched_key}

    @staticmethod
    def find_object_using_value(objects: List[dict], field_name: str, value: str):
        """finds matched objects from a List of objects using a field name and expected value"""
        for object in objects:
            obj = dict(object)
            if obj[field_name] != value:
                continue

            return obj
        return None

    @staticmethod
    def store_file(data: any, protocol_name: str, file_name: str, file_type: any):
        if not file_type:
            return
        # creates directory
        output_directory = "outputs/" + protocol_name
        if not os.path.exists(output_directory):
            os.makedirs(output_directory)

        file = output_directory + "/" + file_name + "." + file_type.value
        if file_type == FileType.JSON:
            with open(file, "w+", encoding="utf-8") as f:
                json.dump(data, f, ensure_ascii=False, indent=2)
            return

        if file_type == FileType.TURTLE:
            data.serialize(file, format="turtle")
            return

    @staticmethod
    def get_data_type_str(val: any):
        value_type = str(type(val))

        if "str" in value_type:
            return DataType.STR.value
        if "int" in value_type:
            return DataType.INT.value
        if "float" in value_type:
            return DataType.FLOAT.value
        if "bool" in value_type:
            return DataType.BOOL.value
        if "dict" in value_type:
            return DataType.DICT.value

        return value_type

    @staticmethod
    def get_value_type(field: str, value: any, object: dict):
        if not object:
            return Utilities.get_data_type_str(val=value)

        value_type = Utilities.get_data_type_str(val=object["value"])

        if not field:
            return value_type
        if "protocol" not in object:
            return value_type

        protocol_name = object["protocol"]
        filename = f"outputs/{protocol_name}/attributes.json"
        attributes = Utilities.load_json_file(
            file_name=filename, include_root_dir=False
        )
        key = field
        if "__" in key:
            key = key.split("__")[-1]
        if key not in attributes:
            return value_type
        if "enum" in attributes[key]:
            return "lut/enum"
        return attributes[key]["type"]

    @staticmethod
    def get_shape_file(dir: str):
        shape_name = dir.split("/")[-1]
        shape = f"{RESOURCE_DIRECTORY}/{shape_name}.ttl"
        graph = Graph()
        graph.parse(shape, format=RDF_FORMAT)

        return graph

    @staticmethod
    def is_api_match(input_source: str, remote_source: str):
        if not input_source or not remote_source:
            return False

        prefix = "api/"
        if prefix not in input_source:
            return False
        if prefix not in remote_source:
            return False

        input_api = input_source.split(prefix)[-1]
        remote_api = remote_source.split(prefix)[-1]

        if input_api != remote_api:
            return False

        return True

    @staticmethod
    def json_dump(data: any, file: str):
        if not data or not file:
            return

        with open(file, "w+", encoding="utf-8") as f:
            json.dump(data, f, ensure_ascii=False, indent=2)
        return
    
    @staticmethod
    def minify_label(value: str):
        value = value.replace(" photo ID", "")
        return value

    @staticmethod
    def url_to_file_name(
        url: str,
        payload: str,
        org_uuid: str
    ):
        name = url
        name = name.replace("/", "-").replace("https:",
                                              "").replace("http:", "").replace("?", "")
        uuid = f"{Utilities.compute_md5_hash(json.dumps(payload))}"

        file_name = f"{name}-{uuid}.json"

        if org_uuid:
            file_name = f"org_uuid-{org_uuid}{name}-{uuid}.json"
        if "swagger" in url:
            file_name = f"global{name}-{uuid}.json"

        return file_name

    @staticmethod
    def copy_mock_app_resources():
        flask.current_app.logger.debug("Copying all mock properties")
        mock_directory = f"data_export/tests/mock"

        if not os.path.exists(RESOURCE_DIRECTORY):
            os.makedirs(RESOURCE_DIRECTORY)

        for file in os.listdir(mock_directory):
            if "app-resource" not in file:
                continue
            file_name = file.replace("app-resource-", "")
            shutil.copy2(f"{mock_directory}/{file}",
                         f"{RESOURCE_DIRECTORY}/{file_name}",)

        flask.current_app.logger.debug("Copied all mock properties")

    @staticmethod
    def store_mock_app_resources():
        output_directory = f"data_export/tests/mock"
        if not os.path.exists(output_directory):
            os.makedirs(output_directory)
        for file in os.listdir(RESOURCE_DIRECTORY):
            shutil.copy2(f"{RESOURCE_DIRECTORY}/{file}",
                         f"{output_directory}/app-resource-{file}")

        flask.current_app.logger.debug("Saved all mock properties")

    @staticmethod
    def has_custom_config(protocol, key):
        if not protocol:
            return None
        if key in protocol:
            if not protocol[key]:
                return None
            return protocol[key]

        if "custom-configs" not in protocol:
            return None
        configs = protocol["custom-configs"]
        if not configs:
            return None
        if key in configs:
            if not configs[key]:
                return None
            return configs[key]

        return None

    @staticmethod
    def store_mock_data(
        url: str,
        params: dict,
        body: dict,
        method: str,
        headers: dict,
        response: dict,
        api_name: str = ""
    ):
        test_metadata = Utilities.load_json_file(
            file_name="outputs/test_metadata.json", include_root_dir=False
        )
        org_uuid = None
        if "org_uuid" in test_metadata:
            org_uuid = test_metadata["org_uuid"]

        # creates directory
        output_directory = f"data_export/tests/mock"
        if not os.path.exists(output_directory):
            os.makedirs(output_directory)
        prefix = url
        if "store-key" in headers:
            prefix = headers["store-key"]
        file_name = Utilities.url_to_file_name(
            url=prefix, payload=body, org_uuid=org_uuid)
        data = {
            "url": url,
            "method": method,
            "params": params,
            "body": body,
            "headers": headers,
            "response": response
        }
        Utilities.json_dump(data=dict(data), file=f"{
                            output_directory}/{file_name}")
        flask.current_app.logger.debug(
            f"response mock is saved, url: {
                url}, filename: {file_name}"
        )

    @staticmethod
    def pretty_print_error_report(report_text: str, uuid: str = None):
        texts = report_text.replace("\t", "")
        violations = texts.split("Constraint Violation")
        results = []
        for violation in violations:
            lines = violation.split("\n")
            result = {}
            for l in lines:
                if "Conforms" in l:
                    continue
                values = l.split(": ")
                if len(values) < 2:
                    continue

                result[values[0]] = "".join(values[1:])

            if result:
                results.append(result)

        return results

    @staticmethod
    def add_error_report(
        existings: dict,
        violations: dict,
        org_minted_uuid: str,
    ):
        for error in violations["Reports"]:
            source = error["Source Shape"].replace(
                "<urn:shapes:", "").replace(">", "")
            if source not in existings:
                existings[source] = dict(error)
                existings[source]["org_minted_uuid"] = org_minted_uuid
                existings[source]["server"] = CORE_URL

        return dict(existings)

    @staticmethod
    def get_photo_sample_sampling_labels(
        protocol: dict,
        nrm_label: str = None,
    ):
        sample_label, sampling_label = "photos", "taking-photos"

        if "photo-sample-label" in protocol:
            sample_label = protocol["photo-sample-label"]
        if "photo-sampling-label" in protocol:
            sampling_label = protocol["photo-sampling-label"]

        if "(field)" in sample_label and nrm_label:
            sample_label = sample_label.replace(
                "(field)", nrm_label)
            sample_label = sample_label.replace(" ", "-")
        if "(field)" in sampling_label and nrm_label:
            sampling_label = sampling_label.replace(
                "(field)", nrm_label)

        sample_label = sampling_label.replace(" ", "-").lower()
        sampling_label = sampling_label.replace(" ", "-").lower()

        return sample_label, sampling_label

    @staticmethod
    def is_setup_id_based_transect(protocol: dict):
        if not protocol:
            return False
        if "custom-configs" not in protocol:
            return False

        resolve_transect_point = Utilities.has_custom_config(
            protocol=protocol,
            key="resolve-transect-point"
        )
        flask.current_app.logger.debug(
            f"No transect with {resolve_transect_point}")
        if resolve_transect_point:
            return True

        custom_configs = protocol["custom-configs"].keys()
        # required fields for setup id based transect
        transect_keys = [
            "handle-transect-start",
            "handle-transect-end"
        ]
        for k in transect_keys:
            if k in custom_configs:
                continue
            flask.current_app.logger.debug(f"No transect with attributes")
            return False
        flask.current_app.logger.debug(f"Transect with attributes")
        return True

    @staticmethod
    def is_vantage_with_attributes(protocol: dict):
        if not protocol:
            return False
        if "custom-configs" not in protocol:
            return False

        custom_configs = protocol["custom-configs"].keys()
        # required fields for transect
        transect_keys = [
            "handle-vantage-start",
            "handle-vantage-end",
            "handle-outlook-zone",
        ]
        for k in transect_keys:
            if k in custom_configs:
                continue
            flask.current_app.logger.debug(f"No vantage point with attributes")
            return False
        flask.current_app.logger.debug(f"Outlook zone with attributes")
        return True

    @staticmethod
    def is_track_station_attributes(protocol: dict):
        if not protocol:
            return False
        if "custom-configs" not in protocol:
            return False

        custom_configs = protocol["custom-configs"].keys()
        # required fields for track station
        keys = [
            "handle-track-station-attribute",
            "handle-track-station-attribute-photo",
            "handle-track-station-habitat-attribute",
        ]
        for k in keys:
            if k in custom_configs:
                continue
            flask.current_app.logger.debug(f"No track station with attributes")
            return False
        flask.current_app.logger.debug(f"Track station with attributes")
        return True

    # load names map
    @staticmethod
    def get_app_config_versions(local: str = "version.json", cloud: str = "cloud_version.json"):
        local = Utilities.load_json_file(
            file_name=f"{RESOURCE_DIRECTORY}/{local}", include_root_dir=False
        )
        cloud = Utilities.load_json_file(
            file_name=f"{RESOURCE_DIRECTORY}/{cloud}", include_root_dir=False
        )
        return {
            "local": local,
            "cloud": cloud
        }

    # increment version string
    @staticmethod
    def increment_ver(version):
        version = version.split('.')
        version[2] = str(int(version[2]) + 1)
        return '.'.join(version)

    # load names map
    @staticmethod
    def get_names_map_data():
        return Utilities.load_json_file(
            file_name=f"{RESOURCE_DIRECTORY}/fields_map.json", include_root_dir=False
        )

    @staticmethod
    def get_conditional_values():
        return Utilities.load_json_file(
            file_name=f"{RESOURCE_DIRECTORY}/conditional_values.json", include_root_dir=False
        )

    @staticmethod
    def feature_type_prefix(name: str, position: dict, id: str = None):
        type = name.replace(" ", "-")
        if position:
            # print("position found ")
            # print(str(position.as_wkt))
            loc = Utilities.compute_md5_hash(str(position.as_wkt))
            type = f"{type}-{loc}"
        if id:
            s = Utilities.compute_md5_hash(str(id))
            # s = id
            type = f"{type}-{s}"
        return type

    # load feature type attribute map
    @staticmethod
    def get_feature_type_attributes_data():
        return Utilities.load_json_file(
            file_name=f"{RESOURCE_DIRECTORY}/feature_types_attributes.json", include_root_dir=False
        )

    # load feature of interest attribute map
    @staticmethod
    def get_feature_of_interest_attributes_data():
        return Utilities.load_json_file(
            file_name=f"{
                RESOURCE_DIRECTORY}/feature_of_interest_attributes.json",
            include_root_dir=False,
        )

    # load feature type attribute map
    @staticmethod
    def is_linked_to_feature_type(field: str, protocol: dict):
        types = []
        feature_type_attributes_map = Utilities.get_feature_type_attributes_data()

        for f_type in protocol["feature-types"]:
            if f_type not in feature_type_attributes_map:
                continue
            if field not in feature_type_attributes_map[f_type]:
                continue
            types.append(f_type)
        return types

    @staticmethod
    def foi_attributes_types(field: str, protocol: dict):
        types = []
        foi_attributes_attributes_map = (
            Utilities.get_feature_of_interest_attributes_data()
        )

        for f_type in protocol["feature-types"]:
            if f_type not in foi_attributes_attributes_map:
                continue
            if field not in foi_attributes_attributes_map[f_type]:
                continue
            types.append(f_type)
        return types

    # load json file
    @staticmethod
    def load_json_file(file_name: str, include_root_dir: bool = True):
        file_path = FILE_ROOT_PATH + file_name
        if not include_root_dir:
            file_path = file_name
        if not os.path.exists(file_path):
            return None
        with open(file_path, "r") as json_file:
            json_data = json.load(json_file)
            return json_data

    # download json file
    @staticmethod
    def download_json_file(url: str):
        try:
            r = requests.get(url, timeout=API_TIMEOUT)
            res = r.json()
            return res
        except Exception as e:
            flask.current_app.logger.error(
                f"Unable to download json file - {e}",
                extra={"context": {"url": url}},
            )
        return {}

    # download json file
    @staticmethod
    def is_approved_reusable_concepts(concept: str):
        if not concept:
            return False
        if concept not in REUSABLE_CONCEPTS:
            return False
        return True

    @staticmethod
    def all_possible_keys(key: str):
        possible_keys = []

        if key:
            possible_keys.append(key)
            # cloud_cover can be cloud cover as well
            if "_" in key:
                possible_keys.append(key.replace("_", " "))
                possible_keys.append(key.replace("_", "-"))
            if "-" in key:
                possible_keys.append(key.replace("-", " "))
                possible_keys.append(key.replace("-", "_"))
            if "__" in key:
                possible_keys.append(key.replace("__", " "))
                possible_keys.append(key.replace("__", "-"))

        names_map = Utilities.get_names_map_data()

        for field in list(names_map.keys()):
            if field in possible_keys:
                # fauna_maturity can be maturity or species can be field species name
                possible_keys = possible_keys + names_map[field]

        return possible_keys

    @staticmethod
    def fetch_start_date(object: dict):
        if not object:
            return None
        updated_object = dict(object)

        # if survey id exists
        if "survey_metadata" in updated_object:
            start_date = updated_object["survey_metadata"]["survey_details"]["time"]
            start_date_db = datetime.strptime(
                start_date,
                DB_DATE_FORMATE,
            )
            return {
                "db_date": start_date_db,
                "pretty_date": Utilities.pretty_print_date(date=start_date),
                "raw": start_date,
            }

        # if start date/date time exists
        value = Utilities.get_matched_date(
            object=updated_object, date_key="start_date")
        if value:
            return value

        # if createdAt time exists
        value = Utilities.get_matched_date(
            object=updated_object, date_key="createdAt")
        if value:
            return value

        # if no date exists we will check plot visit start date
        possible_keys = Utilities.all_possible_keys(key="plot-visit")
        result = Utilities.find_object_by_key(
            object=object, keys=possible_keys)
        if Utilities.is_null_or_empty(object=result, field="object"):
            return None

        value = Utilities.get_matched_date(
            object=result["object"], date_key="start_date"
        )
        if value:
            return value

        return None

    @staticmethod
    def get_matched_date(object: str, date_key: str):
        possible_keys = Utilities.all_possible_keys(key=date_key)
        result = Utilities.find_object_by_key(
            object=object, keys=possible_keys)
        if Utilities.is_null_or_empty(object=result, field="object"):
            return None

        date_value = result["object"]
        date_value_db = datetime.strptime(
            date_value,
            DB_DATE_FORMATE,
        )

        return {
            "db_date": date_value_db,
            "pretty_date": Utilities.pretty_print_date(date=date_value),
            "raw": date_value,
        }

    @staticmethod
    def pretty_print_date(date: str):
        # e.g. 2023-12-05T02:56:00.005Z to 2023/12/05 02:56:00
        d = date.replace("T", " ")
        d = d.replace("Z", "")
        d = d.replace("-", "/")
        if "." in d:
            d = d.split(".")[0]
        return d

    @staticmethod
    def date_string_to_timestamp(date: str):
        d = datetime.strptime(date, API_DATE_FORMATE)
        return int(datetime.timestamp(d))

    @staticmethod
    def get_cache_data(caching: Cache, key: str):
        if not ENABLE_CACHE:
            # flask.current_app.logger.warning(f"Caching is not enabled")
            return None
        value = caching.get(key)

        if value:
            # flask.current_app.logger.debug(f"cache found key: {key}")
            return value
        return None

    @staticmethod
    def store_cache_data(caching: Cache, key: str, value: dict, timeout: int = 30):
        if not ENABLE_CACHE:
            # flask.current_app.logger.warning(f"Caching is not enabled")
            return None

        # flask.current_app.logger.debug(f"Cache miss - key: " + key)
        caching.set(key, value, timeout)
        value = caching.get(key)

    @staticmethod
    def compute_md5_hash(value: str):
        import hashlib
        import uuid
        v = value
        if not value:
            v = "None"
        hash = hashlib.sha256(v.encode("utf-8"))
        hash_uuid = uuid.UUID(hash.hexdigest()[::2])
        return str(hash_uuid)

    @staticmethod
    def get_survey_start_date(survey: dict):
        start_date_time = None
        if "start_date_time" in survey:
            if survey["start_date_time"]:
                start_date_time = survey["start_date_time"]
        if not start_date_time:
            start_date_time = survey["survey_metadata"]["survey_details"]["time"]
        return start_date_time

    @staticmethod
    def get_survey_end_date(survey: dict):
        end_date_time = None
        if "end_date_time" in survey:
            if survey["end_date_time"]:
                end_date_time = survey["end_date_time"]
        return end_date_time

    @staticmethod
    def create_object_to_process(object: dict, handle_photo_attr: bool = False):
        result = {"values": None, "label": None,
                  "field_name": None, "model": None}
        if not object:
            flask.current_app.logger.error(
                f"Failed to create attribute object")
            return result
        if "field_name" in object:
            result["field_name"] = object["field_name"]
        if "model" in object:
            result["model"] = object["model"]
        if "nrm_label" in object:
            result["label"] = object["nrm_label"]

        if "value" not in object:
            result["values"] = dict(object)
            if handle_photo_attr:
                result["photo_name"] = object["name"]
                result["photo_hash"] = object["hash"]
                result["photo_url"] = object["url"]
            return result

        result["values"] = dict(object["value"])
        if "nrm_label" in object["value"]:
            result["label"] = object["value"]["nrm_label"]

        if handle_photo_attr:
            result["photo_name"] = object["value"]["name"]
            result["photo_hash"] = object["value"]["hash"]
            result["photo_url"] = object["value"]["url"]

        return result

    @staticmethod
    def update_model_fields(models: List[dict], forced_models: dict):
        """sometime strapi fails to deep populate all the models"""
        final_models = []
        for model in models:
            updated_model = dict(model)
            keys = list(updated_model.keys())
            for key in keys:
                # field should be an object with id field in it
                if not isinstance(updated_model[key], dict):
                    continue
                if "id" not in updated_model[key]:
                    continue

                # find model linked with the key
                possible_keys = Utilities.all_possible_keys(key=key)
                result = Utilities.find_object_by_key(
                    object=forced_models, keys=possible_keys
                )
                if not result:
                    continue
                if not result["object"]:
                    continue

                # find value using id if exists
                value = Utilities.find_object_using_id(
                    id=updated_model[key]["id"], objects=result["object"]
                )
                if not value:
                    continue
                # replace values
                updated_model[key] = value

            final_models.append(updated_model)

        return final_models

    @staticmethod
    def find_property_with_key(
        object: any,
        field_name: str,
        key_to_find: str,
        model: str = None,
        outputs: list = [],
    ):
        """finds value with the key"""
        if not isinstance(object, dict) and not isinstance(object, list):
            return outputs

        if isinstance(object, dict):
            if key_to_find in object:
                outputs.append(
                    {"value": object, "field_name": field_name, "model": model}
                )
                return outputs

        objects = object if isinstance(object, list) else [object]
        for obs in objects:
            if not isinstance(obs, dict):
                continue
            if key_to_find in obs:
                outputs.append(
                    {"value": obs, "field_name": field_name, "model": model})
                continue

            obs_keys = list(obs.keys())
            for k in obs_keys:

                if isinstance(obs[k], dict):
                    if key_to_find in obs[k]:
                        outputs.append(
                            {"value": obs[k], "field_name": k, "model": model}
                        )
                        continue
                Utilities.find_property_with_key(
                    object=obs[k],
                    field_name=k,
                    key_to_find=key_to_find,
                    outputs=outputs,
                    # relations=relations,
                )
        return outputs

    # filter data by key/field
    @staticmethod
    def filter_data_by_type(object: dict, field: str, value: str):
        if not object:
            return object

        type_keys = Utilities.all_possible_keys(key=field)
        new_object = {}
        obs_keys = list(object.keys())

        for key in obs_keys:
            if not isinstance(object[key], list):
                continue
            new_object[key] = []
            for data in object[key]:
                type = Utilities.find_object_by_key(
                    object=data, keys=type_keys)
                if not type:
                    continue
                if not type["object"]:
                    continue
                if type["object"]["symbol"] != value:
                    continue
                new_object[key].append(data)
        return new_object

    @staticmethod
    def find_property_by_field(object: any, field_name: str, outputs=[]):
        """finds value with the field"""
        if not isinstance(object, dict) and not isinstance(object, list):
            return outputs

        if isinstance(object, list):
            for obj in object:
                Utilities.find_property_by_field(
                    object=dict(obj),
                    field_name=field_name,
                    outputs=outputs,
                )
            return outputs

        if isinstance(object, dict):
            keys = list(object.keys())
            for key in keys:
                if key == field_name:
                    outputs.append({"value": object})
                    continue
                Utilities.find_property_by_field(
                    object=object[key],
                    field_name=field_name,
                    outputs=outputs,
                )

        return outputs

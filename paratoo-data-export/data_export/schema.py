JSONSCHEMA_ORG_UUIDS = {
    "type": "object",
    "properties": {
            "org_uuids": {
                "type": "array",
                "items": {
                    "type": "string",
                    "pattern": "^[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}\Z",
                },
                "minItems": 1
            },
            "force": {
                "type": "boolean",
            },
    },
    "additionalProperties": False,
}
JSONSCHEMA_ORG_UUID = {
    "type": "object",
    "properties": {
            "org_uuid": {
                "type": "string",
                "pattern": "^[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}\Z",
            },
            "force": {
                "type": "boolean",
            },
    },
    "additionalProperties": False,
}
JSONSCHEMA_SKIPS = {
    "type": "object",
    "properties": {
            "skips": {
                "type": "array",
                "items": {
                    "type": "string",
                    "pattern": "^[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}\Z",
                },
            },
            "force": {
                "type": "boolean",
            },
    },
    "additionalProperties": False,
}
JSONSCHEMA_BUCKET_NAME = {
    "type": "object",
    "properties": {
            "bucket": {
                "type": "string",
            },
            "force": {
                "type": "boolean",
            },
    },
    "additionalProperties": False,
}

JSONSCHEMA_BUCKET_PREFIX = {
        "type": "object",
        "properties": {
            "bucket_prefix": {
                "type": "string",
            },
            "force": {
                "type": "boolean",
            },
        },
        "additionalProperties": False,
    }
from typing import Optional, List, Union, Any

from pydantic import BaseModel, Field
from rdflib import SOSA, RDFS, SDO, PROV, VOID, RDF, TIME, XSD

from data_export.models.links import (
    GEO,
    WGS,
    TERN_LOC,
    TERN,
    DWC,
    SSN,
    PROV,
    FOAF,
    SCHEMA,
    ABIS,
    SF,
    TERN_ORG,
)

from data_export.utilities import Utilities as utility


class Base(BaseModel):
    id: str = Field(alias="@id")
    type: str = Field(alias="@type")
    in_dataset: Optional["RDFDataset"] = Field(alias=str(VOID.inDataset))
    is_part_of: Optional[str] = Field(alias=str(SCHEMA.isPartOf))

    class Config:
        allow_population_by_field_name = True
        allow_sample = True


class Value(Base):
    type: str = Field(TERN.Value, alias="@type")


class Text(Value):
    type: str = Field([TERN.Text, TERN.Value], alias="@type")
    value: str = Field(alias=str(RDF.value))


class Int(Value):
    type: str = Field([TERN.Integer, TERN.Value], alias="@type")
    value: str = Field(alias=str("intValue"))
    unit: Optional[str] = Field(alias=TERN.unit)


class Float(Value):
    type: str = Field([TERN.Float, TERN.Value], alias="@type")
    value: str = Field(alias=str("floatValue"))
    unit: Optional[str] = Field(alias=TERN.unit)


class Bool(Value):
    type: str = Field([TERN.Boolean, TERN.Value], alias="@type")
    value: bool = Field(alias=str(RDF.value))


class IRI(Value):
    type: str = Field([TERN.IRI, TERN.Value], alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    value: str = Field(alias=str("IRIValue"))


class Geometry(Base):
    type: str = Field(GEO.Geometry, alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    as_wkt: Optional[str] = Field(alias=str(GEO.asWKT))

class Points(Base):
    type: str = Field(RDFS.Resource, alias="@type")
    has_geometry: List[Geometry] = Field(alias=str(GEO.hasGeometry))


class Agent(Base):
    type: str = Field([SCHEMA.Person], alias="@type")
    sdo_identifier: Optional[str] = Field(alias=str("MERITCreatorID"))


class QualifiedAttribute(Base):
    type: str = Field(PROV.qualifiedAttribution)
    agent: Union[List[Agent], List[str], str] = Field(alias=str(PROV.agent))
    had_role: Optional[str] = Field(alias=str(PROV.hadRole))


class GeneratedBy(Base):
    type: str = Field(ABIS.Project, alias="@type")
    schema_identifier: Optional[str] = Field(alias=str("meritProjectID"))


class Attribute(Base):
    type: str = Field(TERN.Attribute, alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    attribute: str = Field(alias=str(TERN.attribute))
    has_value: Optional[Union[Value, Geometry]] = Field(alias=str(TERN.hasValue))
    has_simple_value: Optional[Any] = Field(alias=str(TERN.hasSimpleValue))
    has_value_iri: Optional[str] = Field(alias=str("IRIHasValue"))
    has_simple_value_iri: Optional[str] = Field(alias=str("IRISampleValue"))


class RDFDataset(Base):
    type: str = Field(TERN.RDFDataset, alias="@type")
    identifier: str = Field(alias=str(SCHEMA.identifier))
    title: Optional[str] = Field(alias=str(SCHEMA.name))
    issued: Optional[str] = Field(alias=str(SCHEMA.dateCreated))
    description: Optional[str] = Field(alias=str(SCHEMA.description))
    wasGeneratedBy: Optional[str] = Field(alias=str(PROV.wasGeneratedBy))
    qualifiedAttribution: List[QualifiedAttribute] = Field(
        alias=str(PROV.qualifiedAttribution)
    )


class RelevantProject(Base):
    type: str = Field(ABIS.Project, alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    schema_identifier: Optional[str] = Field(alias=str("meritProjectID"))
    generated: Optional[str] = Field(alias=str(PROV.generated))
    has_attribute: Optional[
        Union[Union[str, Attribute], List[Union[str, Attribute]]]
    ] = Field(alias=str(TERN.hasAttribute))


class AttributeWithHasAttributes(Attribute):
    type: str = Field(TERN.Attribute)
    has_attribute: Optional[
        Union[Union[str, Attribute, Geometry], List[Union[str, Attribute, Geometry]]]
    ] = Field(alias=str(TERN.hasAttribute))


class InputVariables(Base):
    type: str = Field(TERN.Input, alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    # survey_id: str = Field(alias=str(XSD.string))
    has_attribute: Optional[
        Union[Union[str, Attribute], List[Union[str, Attribute]]]
    ] = Field(alias=str(TERN.hasAttribute))


class Observer(Base):
    type: str = Field(TERN_ORG.Person, alias="@type")
    name: Optional[str] = Field(alias=str(SCHEMA.name))
    was_associated_with: Optional[str] = Field(
        alias=str(PROV.wasAssociatedWith)
    )
    has_attribute: Optional[
        Union[Union[str, Attribute], List[Union[str, Attribute]]]
    ] = Field(alias=str(TERN.hasAttribute))


class Survey(Base):
    type: str = Field(TERN.Survey, alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    identifier: str = Field(alias=str(SCHEMA.identifier))
    survey_start_time: Optional[str] = Field(alias=str(PROV.startedAtTime))
    survey_end_time: Optional[str] = Field(alias=str(PROV.endedAtTime))
    was_associated_with: Optional[str] = Field(
        alias=str(PROV.wasAssociatedWith)
    )
    has_site_visit: Optional[str] = Field(alias=str(TERN.hasSiteVisit))


class Transect(Base):
    type: str = Field(TERN.Transect, alias="@type")
    start_point: Optional[Geometry] = Field(alias=str(TERN.transectStartPoint))
    end_point: Optional[Geometry] = Field(alias=str(TERN.transectEndPoint))
    is_sample_of: Optional[str] = Field(alias=str(SOSA.isSampleOf))
    feature_type: Optional[str] = Field(alias=str(TERN.featureType))
    has_attribute: Optional[
        Union[Union[str, Attribute], List[Union[str, Attribute]]]
    ] = Field(alias=str(TERN.hasAttribute))


class Procedure(Base):
    type: str = Field(TERN.Procedure, alias="@type")
    has_survey: Survey = Field(alias=str(TERN.hasSurvey))
    has_has_input: Optional[InputVariables] = Field(alias=str(SSN.hasInput))
    tern_method_type: str = Field(alias=str(TERN.methodType))
class TimeInstant(Base):
    type: str = Field(TIME.Instant, alias="@type")
    date_timestamp: Optional[str] = Field(alias=str(TIME.inXSDDateTimeStamp))

class FeatureOfInterest(Base):
    type: Optional[str] = Field(TERN.FeatureOfInterest, alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    # is_result_of: str = Field(alias=str(SOSA.isResultOf))
    schema_spatial: Optional[Geometry] = Field(alias=str(SCHEMA.spatial))
    feature_type: Optional[str] = Field(alias=str(TERN.featureType))
    is_sample_of: Optional[Union[str, Transect]] = Field(alias=str(SOSA.isSampleOf))
    has_site: Optional[Union[str, Transect]] = Field(alias=str(TERN.hasSite))
    has_phenomenon_time: Optional[TimeInstant] = Field(alias=str(SOSA.phenomenonTime))
    has_attribute: Optional[
        Union[Union[str, Attribute], List[Union[str, Attribute]]]
    ] = Field(alias=str(TERN.hasAttribute))


class Site(Base):
    type: str = Field(TERN.Site, alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    identifier: str = Field(alias=str(SCHEMA.identifier))
    schema_spatial: Optional[Union[Geometry, List[Geometry]]] = Field(alias=str(SCHEMA.spatial))
    default_geometry: Optional[Union[Geometry, List[Geometry]]] = Field(alias=str(GEO.hasDefaultGeometry))
    feature_type: Optional[str] = Field(alias=str(TERN.featureType))
    is_sample_of: Optional[Union[str, FeatureOfInterest]] = Field(
        alias=str(SOSA.isSampleOf)
    )
    has_feature_of_interest: Optional[Union[str, FeatureOfInterest]] = Field(
        alias=str(SOSA.hasFeatureOfInterest)
    )
    has_outlook_zones: Optional[List[Union[str, FeatureOfInterest]]] = Field(
        alias=str(TERN.hasOutlookZone)
    )
    used_procedure: Optional[Union[str, Procedure]] = Field(
        alias=str(SOSA.usedProcedure)
    )
    # has_result: Value = Field(alias=str(SOSA.hasResult))
    has_attribute: Optional[
        Union[Union[str, Attribute], List[Union[str, Attribute]]]
    ] = Field(alias=str(TERN.hasAttribute))


class SiteVisit(Base):
    type: str = Field(TERN.SiteVisit, alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    identifier: str = Field(alias=str(SCHEMA.identifier))
    started_at_time: str = Field(alias=str(PROV.startedAtTime))
    ended_at_time: Optional[str] = Field(alias=str(PROV.endedAtTime))
    has_attribute: Optional[List[Attribute]] = Field(alias=str(TERN.hasAttribute))
    has_site: Union[str, Site] = Field(alias=str(TERN.hasSite))


class FeatureOfInterestSample(FeatureOfInterest):
    type: Optional[str] = Field(TERN.FeatureOfInterest, alias="@type")
    is_sample_of: Optional[Optional[Union[Site, str]]] = Field(
        alias=str(SOSA.isSampleOf)
    )



class Observation(Base):
    type: str = Field(TERN.Observation, alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    schema_spatial: Optional[Geometry] = Field(alias=str(SCHEMA.spatial))
    has_feature_of_interest: Optional[Union[str, FeatureOfInterest, Site]] = Field(
        alias=str(SOSA.hasFeatureOfInterest)
    )
    has_result: Value = Field(alias=str(SOSA.hasResult))
    has_simple_result: Optional[Any] = Field(alias=str(SOSA.hasSimpleResult))
    has_simple_result_iri: Optional[str] = Field(alias=str("IRISampleResult"))
    has_site_visit: Optional[Union[str, SiteVisit]] = Field(
        alias=str(TERN.hasSiteVisit)
    )
    has_attribute: Optional[List[Attribute]] = Field(alias=str(TERN.hasAttribute))
    observed_property: str = Field(alias=str(SOSA.observedProperty))
    has_phenomenon_time: Optional[TimeInstant] = Field(alias=str(SOSA.phenomenonTime))
    used_procedure: Optional[Union[str, Procedure]] = Field(
        alias=str(SOSA.usedProcedure)
    )
    result_date_time: Optional[str] = Field(alias=str(TERN.resultDateTime))


class ObservationCollection(Base):
    type: str = Field(TERN.ObservationCollection, alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    schema_spatial: Optional[Geometry] = Field(alias=str(SCHEMA.spatial))
    has_feature_of_interest: Optional[Union[str, FeatureOfInterest, Site]] = Field(
        alias=str(SOSA.hasFeatureOfInterest)
    )
    has_member: Optional[List[str]] = Field(alias=str(SOSA.hasMember))
    has_phenomenon_time: Optional[TimeInstant] = Field(alias=str(SOSA.phenomenonTime))
    used_procedure: Optional[Union[str, Procedure]] = Field(
        alias=str(SOSA.usedProcedure)
    )
    has_attribute: Optional[
        Union[Union[str, Attribute], List[Union[str, Attribute]]]
    ] = Field(alias=str(TERN.hasAttribute))
    has_site_visit: Optional[Union[str, SiteVisit]] = Field(
        alias=str(TERN.hasSiteVisit)
    )
    has_transect: Optional[Union[str, Transect, Site]] = Field(alias=str(TERN.hasSite))
    has_vantage_point: Optional[Union[str, FeatureOfInterest, Site]] = Field(
        alias=str(SOSA.isSampleOf)
    )
    result_date_time: Optional[str] = Field(alias=str(TERN.resultDateTime))
    is_observed_by: Optional[
        Union[Union[str, Observer], List[Union[str, Observer]]]
    ] = Field(alias=str(SOSA.isObservedBy))


# material sample
class Sample(Base):
    type: Optional[str] = Field(TERN.Sample, alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    identifier: Optional[str] = Field(alias=str(SCHEMA.identifier))
    is_result_of: Optional[str] = Field(alias=str(SOSA.isResultOf))
    schema_spatial: Optional[Geometry] = Field(alias=str(SCHEMA.spatial))
    is_sample_of: Optional[Union[str, FeatureOfInterest, Site]] = Field(
        alias=str(SOSA.isSampleOf)
    )
    feature_type: Optional[str] = Field(alias=str(TERN.featureType))
    archived_at: Optional[str] = Field(alias=str(SCHEMA.archivedAt))
    has_attribute: Optional[List[Attribute]] = Field(alias=str(TERN.hasAttribute))
    schema_spatial: Optional[Geometry] = Field(alias=str(SCHEMA.spatial))
    has_site: Optional[Union[str, Site, Transect]] = Field(alias=str(TERN.hasSite))


# image sample
class FeatureSample(Base):
    type: Optional[str] = Field([TERN.FeatureOfInterest, TERN.Sample], alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    identifier: Optional[str] = Field(alias=str(SCHEMA.identifier))
    is_result_of: Optional[str] = Field(alias=str(SOSA.isResultOf))
    schema_spatial: Optional[Geometry] = Field(alias=str(SCHEMA.spatial))
    is_sample_of: Optional[str] = Field(alias=str(SOSA.isSampleOf))
    feature_type: Optional[str] = Field(alias=str(TERN.featureType))


class MaterialSample(Base):
    type: Optional[str] = Field(TERN.MaterialSample, alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    material_sample_id: Optional[str] = Field(alias=str(DWC.materialSampleID))
    schema_spatial: Optional[Geometry] = Field(alias=str(SCHEMA.spatial))
    is_result_of: Optional[str] = Field(alias=str(SOSA.isResultOf))
    is_sample_of: Optional[str] = Field(alias=str(SOSA.isSampleOf))
    feature_type: Optional[str] = Field(alias=str(TERN.featureType))


class Sampler(Base):
    type: Optional[str] = Field(TERN.Sampler, alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    implements: Optional[Union[str, Procedure]] = Field(alias=str(SSN.implements))
    has_attribute: Optional[List[Attribute]] = Field(alias=str(TERN.hasAttribute))
    sampler_type: Optional[str] = Field(alias=str(TERN.systemType))


# sampling
class Sampling(Base):
    type: str = Field(TERN.Sampling, alias="@type")
    label: Optional[str] = Field(alias=str(RDFS.label))
    schema_spatial: Optional[Geometry] = Field(alias=str(SCHEMA.spatial))
    geometries: Optional[list[Geometry]] = Field(alias=str(SCHEMA.spatial))
    has_plot_points: Optional[list[Geometry]] = Field(alias=str(GEO.hasGeometry))
    has_fauna_plot_points: Optional[list[Geometry]] = Field(alias=str(GEO.hasGeometry))
    has_feature_of_interest: Optional[
        Union[FeatureOfInterest, Sample, MaterialSample, FeatureSample, Site, str, Transect]
    ] = Field(alias=str(SOSA.hasFeatureOfInterest))
    has_result: Optional[Union[Sample, MaterialSample, FeatureSample, str, Site, FeatureOfInterest, list[str]]] = (
        Field(alias=str(SOSA.hasResult))
    )
    made_by_sampler: Optional[Union[Sampler, str]] = Field(
        alias=str(SOSA.madeBySampler)
    )
    used_procedure: Optional[Union[str, Procedure]] = Field(
        alias=str(SOSA.usedProcedure)
    )
    implements: Optional[Union[str, Procedure]] = Field(alias=str(SSN.implements))
    has_site_visit: Optional[Union[str, SiteVisit]] = Field(alias=str(TERN.hasSiteVisit))
    result_date_time: Optional[str] = Field(alias=str(TERN.resultDateTime))
    has_attribute: Optional[
        Union[Union[str, Attribute, Geometry], List[Union[str, Attribute, Geometry]]]
    ] = Field(alias=str(TERN.hasAttribute))


Attribute.update_forward_refs()
RDFDataset.update_forward_refs()
Geometry.update_forward_refs()
Site.update_forward_refs()
SiteVisit.update_forward_refs()
Observation.update_forward_refs()
ObservationCollection.update_forward_refs()
Sample.update_forward_refs()
FeatureSample.update_forward_refs()
MaterialSample.update_forward_refs()
Sampling.update_forward_refs()
FeatureOfInterest.update_forward_refs()
IRI.update_forward_refs()
Points.update_forward_refs()

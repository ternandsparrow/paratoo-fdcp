from rdflib import Namespace

EX = Namespace("https://example.com/")
TERN = Namespace("https://w3id.org/tern/ontologies/tern/")
TERN_ORG = Namespace("https://w3id.org/tern/ontologies/org/")
TERN_LOC = Namespace("https://w3id.org/tern/ontologies/loc/")
GEO = Namespace("http://www.opengis.net/ont/geosparql#")
WGS = Namespace("http://www.w3.org/2003/01/geo/wgs84_pos#")
BDR_CV = Namespace("https://linked.data.gov.au/def/bdr-cv/")
DWC = Namespace("http://rs.tdwg.org/dwc/terms/")
SF = Namespace("http://www.opengis.net/ont/sf#")
SSN = Namespace("http://www.w3.org/ns/ssn/")
PROV = Namespace("http://www.w3.org/ns/prov#")
SSN = Namespace("http://www.w3.org/ns/ssn/")
FOAF = Namespace("http://xmlns.com/foaf/0.1/")

ABIS = Namespace("https://linked.data.gov.au/def/abis/")
MONITOR = Namespace("https://linked.data.gov.au/dataset/bdr/monitor/")
SCHEMA = Namespace("https://schema.org/")

# not active
ROLECODE = Namespace("http://def.isotc211.org/iso19115/-1/2018/CitationAndResponsiblePartyInformation/code/CI_RoleCode/")
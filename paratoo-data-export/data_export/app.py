import flask
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from flask import Flask

from data_export.constants import SENTRY_DNS, FLASK_ENV, EXPORTER_DEPLOYED_ENV_NAME
from data_export import api
from data_export.extensions import (
    swagger,
    export_service,
    validator_service,
    core_service,
    org_service,
    protocol_service,
    graph_service,
    turtle_service,
    redis_caching,
    aws_service,
    web_driver_service
)

def create_app(config_object="data_export.settings"):
    if FLASK_ENV != "development":
        sentry_sdk.init(
            dsn=SENTRY_DNS,
            # Set traces_sample_rate to 1.0 to capture 100%
            # of transactions for performance monitoring.
            traces_sample_rate=1.0,
            # Set profiles_sample_rate to 1.0 to profile 100%
            # of sampled transactions.
            # We recommend adjusting this value in production.
            profiles_sample_rate=1.0,
            attach_stacktrace=True,
            enable_tracing=True,
            environment=FLASK_ENV,
            server_name=EXPORTER_DEPLOYED_ENV_NAME,
            integrations=[
                FlaskIntegration(
                    transaction_style="url",
                ),
            ],
        )
    app = Flask(__name__)
    app.config.from_object(config_object)

    register_extensions(app)
    register_blueprints(app)

    return app


def register_extensions(app):
    """Register Flask extensions."""
    load_swagger_component_schemas(app)
    swagger.init_app(app)
    export_service.init_app(app)
    validator_service.init_app(app)
    core_service.init_app(app)
    org_service.init_app(app)
    protocol_service.init_app(app)
    graph_service.init_app(app)
    turtle_service.init_app(app)
    redis_caching.init_app(app)
    aws_service.init_app(app)
    web_driver_service.init_app(app)


def register_blueprints(app):
    """Register Flask blueprints."""
    api.controllers.register_blueprint(app)

    @app.route("/")
    def home():
        return flask.redirect(app.config.get("APPLICATION_ROOT") + "/apidocs", 307)

    return None


def load_swagger_component_schemas(app):
    swagger_config = app.config.get("SWAGGER", {})
    if "components" not in swagger_config:
        swagger_config["components"] = {}
    if "schemas" not in swagger_config["components"]:
        swagger_config["components"]["schemas"] = {}

    app.config["SWAGGER"] = swagger_config

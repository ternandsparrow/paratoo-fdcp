import typing
import requests
import flask
import json


class APIException(Exception):
    """We use this to indicate a generic issue with a remote API. Generates a 400 if not caught"""

    def __init__(self, message):
        super().__init__(message)


class OrgService:
    DEFAULT_TIMEOUT = 5  # seconds

    def __init__(self, app=None):
        self.enabled = True

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.enabled = app.config.get("SERVICE_ENABLED", True)
        if self.enabled:
            self.org_url = app.config["ORG_URL"]

    def get_response(self, api, method):
        if not self.org_url:
            flask.current_app.logger.info(f"Org url is not provided")
            return

        url = f"{self.core_url}/{api}"
        try:
            r = requests.get(url, timeout=OrgService.DEFAULT_TIMEOUT)
        except Exception as e:
            flask.current_app.logger.error(
                f"Unable to send request to org - {e}",
                exc_info=e,
                extra={"context": {"url": url}},
            )
            raise

        res = r.json()
        return res

    def post_response(self, api: str, params: typing.Optional[dict]):
        if not self.org_url:
            flask.current_app.logger.info(f"Org url is not provided")
            return

        url = f"{self.org_url}/{api}"
        try:
            r = requests.post(url, data=params, timeout=OrgService.DEFAULT_TIMEOUT)
        except Exception as e:
            flask.current_app.logger.error(
                f"Unable to send request to org - {e}",
                exc_info=e,
                extra={"context": {"url": url}},
            )
            raise

        res = r.json()
        return res

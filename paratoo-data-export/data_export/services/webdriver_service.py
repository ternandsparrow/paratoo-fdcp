import flask
import json
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

import data_export.extensions as extensions
from data_export.utilities import Utilities as utility
from data_export.constants import (
    CROME_HUB_URL,
    OIDC_URL,
    OIDC_PASS,
    OIDC_USER,
)


class APIException(Exception):
    """We use this to indicate a generic issue with a remote API. Generates a 400 if not caught"""

    def __init__(self, message):
        super().__init__(message)


class WebDriverService:
    DEFAULT_TIMEOUT = 5  # seconds

    def __init__(self, app=None):
        self.enabled = True

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.enabled = app.config.get("SERVICE_ENABLED", False)
        self.options = None
        self.driver = None
        if self.enabled:
            self.options = Options()
            # headless mode
            self.options.add_argument("--headless=new")
            self.options.add_argument("--no-sandbox")
            self.options.add_argument("--disable-dev-shm-usage")
            # self.driver = webdriver.Remote(
            #     command_executor=CROME_HUB_URL, options=self.options
            # )

    def create_web_driver(self):
        self.driver = webdriver.Remote(
            command_executor=CROME_HUB_URL, options=self.options
        )

    def generate_ala_token(self):
        """
        Helper to generate token using headless browser
        """
        cache_key = utility.compute_md5_hash(f"{OIDC_URL}{OIDC_USER}{OIDC_PASS}")
        token = utility.get_cache_data(caching=extensions.redis_caching, key=cache_key)
        if token:
            return token
        try:
            # if not self.driver:
            flask.current_app.logger.debug(f"creating web driver ...... ")
            self.create_web_driver()
            flask.current_app.logger.debug(f"web driver found ...... ")
            self.driver.get(OIDC_URL)
            buttons = self.driver.find_elements(By.CSS_SELECTOR, "button")
            bIn = -1
            for index in range(len(buttons)):
                inner = buttons[index].get_attribute("outerHTML")
                if "login" not in str(inner):
                    continue
                if "loginSubmit" not in str(inner):
                    continue
                if bIn >= 0:
                    continue
                bIn = index
            buttons[bIn].click()
            flask.current_app.logger.debug(f"redirecting ...... ")
            time.sleep(5)

            token = utility.get_cache_data(
                caching=extensions.redis_caching, key=cache_key
            )
            if token:
                self.driver.quit()
                return token

            inputs = self.driver.find_elements(By.CSS_SELECTOR, "input")
            userDone = False
            passDone = False
            for index in range(len(inputs)):
                inner = inputs[index].get_attribute("outerHTML")
                if "username" not in str(inner) and "password" not in str(inner):
                    continue
                if "username" in str(inner) and not userDone:
                    inputs[index].send_keys(OIDC_USER)
                    userDone = True
                if "password" in str(inner) and not passDone:
                    inputs[index].send_keys(OIDC_PASS)
                    passDone = True

            submitDone = False
            signButtons = self.driver.find_elements(By.NAME, "signInSubmitButton")
            for index in range(len(signButtons)):
                if submitDone:
                    continue
                inner = signButtons[index].get_attribute("outerHTML")
                if "signInSubmitButton" in str(inner) and not submitDone:
                    submitDone = True
                    signButtons[index].click()

            flask.current_app.logger.debug(f"login ...... ")
            time.sleep(5)
            token = utility.get_cache_data(
                caching=extensions.redis_caching, key=cache_key
            )
            if token:
                self.driver.quit()
                return token

            auth = self.driver.execute_script(
                "return window.localStorage.getItem('auth');"
            )
            if not auth:
                self.driver.quit()
                flask.current_app.logger.warning(f"No token found")
                return None

            flask.current_app.logger.info(f"token found")

            auth = json.loads(auth)
            token = auth["token"]
            utility.store_cache_data(
                caching=extensions.redis_caching,
                key=cache_key,
                value=f"Bearer {token}",
                timeout=300,
            )

            # takes too much time to validate
            # data = {
            #     "token": f"Bearer {token}",
            # }
            # response = requests.post(VALIDATE_TOKEN_API, json=data)
            # valid = response.json()["valid"]
            # if not valid:
            #     driver.quit()
            #     flask.current_app.logger.warning(f"Token is invalid")
            #     return None

            self.driver.quit()
            return f"Bearer {token}"
        except Exception as e:
            self.driver.quit()
            flask.current_app.logger.error(e)
            return None

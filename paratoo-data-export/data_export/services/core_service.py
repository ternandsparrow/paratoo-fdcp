import typing
import requests
import flask
import datetime
import json
import os
import data_export.extensions as extensions
from data_export.utilities import Utilities as utility

from data_export.constants import STORE_MOCK_HTTP_OUTPUT, MOCK_OUTPUT_PREFIX, RESOURCE_TAG, FileType


class APIException(Exception):
    """We use this to indicate a generic issue with a remote API. Generates a 400 if not caught"""

    def __init__(self, message):
        super().__init__(message)


class CoreService:
    DEFAULT_TIMEOUT = 555  # seconds

    def __init__(self, app=None):
        self.enabled = True
        self.counter = 0
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.enabled = app.config.get("SERVICE_ENABLED", True)
        if self.enabled:
            self.enable_org_auth = app.config["ENABLE_ORG_AUTH"]
            self.random_jwt = app.config["RANDOM_JWT"]
            self.core_url = app.config.get("CORE_URL")
            self.token = None
            self.dumps = None

    def get_response(
        self,
        api: str,
        query: typing.Optional[dict] = None,
        server: str = None,
        use_cache: bool = False,
    ):
        if not self.core_url:
            flask.current_app.logger.info(f"Core url is not provided")
            return

        if not self.token:
            self.update_auth_token()
        self.counter += 1
        headers = {"Authorization": "Bearer " + self.token}
        query_string = query if query else ""
        url = f"{self.core_url}/{api}?{query_string}"
        if server:
            url = f"{server}/{api}?{query_string}"
        import json
        cache_key = f"{utility.compute_md5_hash(url)}-{utility.compute_md5_hash(json.dumps(headers))}"
        if use_cache:
            data = utility.get_cache_data(
                caching=extensions.redis_caching, key=cache_key
            )
            if data:
                return data
        try:
            start_time = datetime.datetime.now()
            r = requests.get(url, headers=headers, timeout=CoreService.DEFAULT_TIMEOUT)

        except Exception as e:
            flask.current_app.logger.error(
                f"Unable to send request to core - {e}",
                extra={"context": {"url": url}},
            )
            raise
        if r.status_code != 200:
            import json

            msg = f"Unable to send request to {url}, status code: {r.status_code}, reason: {r.reason}"
            flask.current_app.logger.error(msg)
            return flask.abort(r.status_code, description=msg)

        if "text/plain" in r.headers.get("Content-Type", ""):
            flask.current_app.logger.error(
                f"Unable to send request to core - {r}",
                extra={"context": {"url": url}},
            )
            return None

        res = r.json()
        diff = datetime.datetime.now() - start_time
        flask.current_app.logger.debug(
            f"total core api: {self.counter} took time: {diff.total_seconds()}s to get response url: /{api}?{query_string} method: GET"
        )

        if STORE_MOCK_HTTP_OUTPUT:
            utility.store_mock_data(
                url=r.url.replace(self.core_url, "http://core-test"),
                params={},
                body={},
                method="get",
                headers=headers,
                response=res,
            )
        if use_cache:
            utility.store_cache_data(
                caching=extensions.redis_caching,
                key=cache_key,
                value=dict(res),
            )
 
        return res

    def post_response(
        self,
        api: str,
        params: typing.Optional[dict],
        query: typing.Optional[str] = None,
        server: str = None,
        use_cache: bool = False,
        saved_response_key: str = None
    ):
        if not self.core_url:
            flask.current_app.logger.info(f"Core url is not provided")
            return

        if not self.token:
            self.update_auth_token()

        start_time = datetime.datetime.now()

        headers = {"Authorization": f"Bearer {self.token}"}
        query_string = query if query else ""
        url = f"{self.core_url}/{api}?{query_string}"
        if server:
            url = f"{server}/{api}?{query_string}"

        import json

        if saved_response_key:
            k = f"api_responses/{RESOURCE_TAG}_{saved_response_key}.json"
            dump = utility.load_json_file(file_name=k, include_root_dir=False)
            if dump:
                flask.current_app.logger.debug(
                    f"key {k} found in saved api responses, url: /{api}?{query_string} method: POST"
                )
                if STORE_MOCK_HTTP_OUTPUT:
                    utility.store_mock_data(
                        url=url.replace(self.core_url, "http://core-test"),
                        params={},
                        body=params,
                        method="post",
                        headers=headers,
                        response=dump,
                    ) 
                return dump

        cache_key = f"{utility.compute_md5_hash(url)}-{utility.compute_md5_hash(json.dumps(headers))}-{utility.compute_md5_hash(json.dumps(params))}"
        if use_cache:
            data = utility.get_cache_data(
                caching=extensions.redis_caching, key=cache_key
            )
            if data:
                return data
        self.counter += 1
        try:
            r = requests.request(
                method="POST",
                url=url,
                headers=headers,
                json=dict(params),
                timeout=CoreService.DEFAULT_TIMEOUT,
            )

        except Exception as e:
            flask.current_app.logger.error(
                f"Unable to send request to core - {e}",
                exc_info=e,
                extra={"context": {"url": url}},
            )
            raise
        if r.status_code != 200:
            import json

            msg = f"Unable to send request to {url}, request body: {params}, status code: {r.status_code}, reason: {r.reason}"
            flask.current_app.logger.error(msg)
            return flask.abort(r.status_code, description=msg)
        if "text/plain" in r.headers.get("Content-Type", ""):
            return r.text

        res = r.json()

        if saved_response_key:
            k = f"api_responses/{RESOURCE_TAG}_{saved_response_key}.json"

            if not os.path.exists("api_responses"):
                os.makedirs("api_responses")

            with open(k, "w", encoding="utf-8") as f:
                json.dump(res, f, ensure_ascii=False, indent=2)
                flask.current_app.logger.debug(f"saved dumps, key: {saved_response_key} url: /{api}?{query_string} method: POST")

        diff = datetime.datetime.now() - start_time
        flask.current_app.logger.debug(
            f"total core api: {self.counter} took time: {diff.total_seconds()}s to get response url: /{api}?{query_string} method: POST"
        )
        if STORE_MOCK_HTTP_OUTPUT:
            utility.store_mock_data(
                url=r.url.replace(self.core_url, "http://core-test"),
                params={},
                body=params,
                method="post",
                headers=headers,
                response=res,
            )
        if use_cache:
            utility.store_cache_data(
                caching=extensions.redis_caching,
                key=cache_key,
                value=dict(res),
            )
        return res

    def update_auth_token(self):
        if not self.enable_org_auth:
            self.token = self.random_jwt
            return
        # update token
        flask.current_app.logger.info(f"updating auth token..")

        params = {"identifier": "TestUser", "password": "password"}
        body = extensions.org_service.post_response(api="api/auth/local", params=params)

        if "jwt" in body:
            self.token = body["jwt"]

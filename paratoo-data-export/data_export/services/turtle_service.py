from typing import List
import json
import flask

from rdflib import BNode, Graph

import data_export.models.tern_fields as fields
import data_export.extensions as extensions
from data_export.utilities import Utilities as utility
from data_export.constants import (
    FieldType,
    SampleType,
    MONITOR_PREFIX,
    SITE_RDF_ID,
    DATASET_RDF_ID,
    OBSERVATION_COLLECTION_RDF_ID,
    OBSERVATION_RDF_ID,
    FEATURE_OF_INTEREST_RDF_ID,
    CAMERA_IMPLEMENTS_URI,
    CAMERA_SAMPLER_URI,
    PLOT_MODELS,
    AGENT,
    DEFAULT_ROLE,
    PLOT_LAYOUT_RDF_ID,
    PROJECT_RDF_ID,
    PLOT_SELECTION_RDF_ID,
    PROCEDURE_RDF_ID,
    BarcodeType,
    DataType,
    GEO_POINT_TYPE,
    MONITOR_PREFIX_GLOBAL,
    FileType
)


class APIException(Exception):
    """We use this to indicate a generic issue with a remote API. Generates a 400 if not caught"""

    def __init__(self, message):
        super().__init__(message)


class TurtleService:
    def __init__(self, app=None):
        self.enabled = True

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.enabled = app.config.get("SERVICE_ENABLED", False)
        if self.enabled:
            self.graph = None

    def create_plot_definition_survey_ttl(
        self,
        graph: Graph,
        procedure_ttl: dict,
        plot_ttl: dict,
        survey_data: dict,
    ):
        names_map = utility.get_names_map_data()

        record_id = plot_ttl["rt"].id

        plot_visit = dict(survey_data["plot_visit"])
        plot_layout = dict(survey_data["plot_visit"]["plot_layout"])
        plot_selection = dict(plot_visit["plot_layout"]["plot_selection"])
        plot_selection_id = plot_selection["uuid"]
        plot_layout_id = plot_selection_id

        # create site and site visit
        site_name = plot_selection["plot_label"]["value"]
        site_name_uuid = utility.compute_md5_hash(value=site_name)
        visit_start_date = plot_visit["start_date"]

        # as there is no unique field in plot visit we will use site name + visit field name
        plot_visit_id = utility.compute_md5_hash(
            value=f"{visit_start_date}{site_name}")
        plot_selection_id = utility.uuid(dict(plot_selection))

        result_time = plot_layout["createdAt"]

        # site attributes are linked to both plot layout and plot selection
        plot_label_attrs = names_map["plot_label_attributes"]
        plot_label_attributes = self.get_attributes_list(
            data=dict(plot_selection["skipped_plot_name"]),
            dataset_ttl=plot_ttl["rt"],
            key_to_find="plot_attribute_uri",
            approved=plot_label_attrs,
            existing_attributes=[],
            skip_fields=[],
            add_is_part_of=False,
        )
        site_attrs = names_map["site_attributes"]
        site_attributes = self.get_attributes_list(
            data=dict(plot_layout),
            dataset_ttl=plot_ttl["rt"],
            key_to_find="plot_attribute_uri",
            approved=site_attrs,
            existing_attributes=[],
            skip_fields=["plot_label"],
            add_is_part_of=False,
        )
        site_attributes = self.get_attributes_list(
            data=dict(plot_selection),
            dataset_ttl=plot_ttl["rt"],
            key_to_find="plot_attribute_uri",
            approved=site_attrs,
            existing_attributes=site_attributes,
            skip_fields=[],
            add_has_attributes={"plot_label": plot_label_attributes},
            add_is_part_of=False,
        )
        # attributes with special cases (e.g. permanently_marked in plot-selection).
        # in Merit plot marker location/permanently_marked is categorical but in monitor its checkboxes with different values

        # south west from the plot points
        location_data = plot_layout["plot_points"]["value"][0]
        schema_spatials = []
        for location_data in plot_layout["plot_points"]["value"]:
            schema_spatials.append(self.create_schema_spacial(
                dataset_ttl=plot_ttl["rt"], 
                lat=location_data["lat"], 
                lng=location_data["lng"],
                label=location_data["name"]["description"]
            )
            )

        site_ttl = fields.Site(
            id=plot_ttl["st"],
            identifier=plot_selection_id,
            label=f"site {site_name}",
            in_dataset=plot_ttl["rt"],
            schema_spatial=schema_spatials,
            default_geometry=schema_spatials,
            # has_feature_of_interest=plot_ttl["alu"],
            feature_type="http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4",
            has_attribute=site_attributes,
        )
        # insert site
        graph = extensions.graph_service.add_data(
            data=site_ttl.dict(by_alias=True))

        # plot layout attributes
        plot_layout_attrs = names_map["plot_layout_attributes"]
        plot_layout_attributes = self.get_attributes_list(
            data=dict(plot_layout),
            dataset_ttl=plot_ttl["rt"],
            key_to_find="attribute_uri",
            approved=plot_layout_attrs,
            existing_attributes=[],
            skip_fields=[],
            add_is_part_of=False,
        )

        plot_layout_rdf_id = f"{record_id}/plot-layout/{plot_layout_id}"

        # plot points
        plot_points = self.create_plot_layout_points(
            graph=graph,
            plot_layout=dict(plot_layout),
            plot_ttl=plot_ttl,
            uuid=plot_selection_id,
            key_to_find="handle-plot-points",
        )
        graph = plot_points["graph"]
        if plot_points["attr"]:
            plot_layout_attributes.append(plot_points["attr"])

        # fauna plot points
        fauna_plot_points = self.create_plot_layout_points(
            graph=graph,
            plot_layout=dict(plot_layout),
            plot_ttl=plot_ttl,
            uuid=plot_selection_id,
            key_to_find="handle-fauna-plot-points",
        )
        graph = fauna_plot_points["graph"]
        if fauna_plot_points["attr"]:
            plot_layout_attributes.append(fauna_plot_points["attr"])

        ausland_uri = f"{record_id}/australia-land/{site_name_uuid}"
        plot_layout_ttl = fields.Sampling(
            id=plot_layout_rdf_id,
            identifier=plot_layout_id,
            label=f"plot layout {site_name}",
            in_dataset=plot_ttl["rt"],
            # schema_spatial=schema_spatial,
            has_feature_of_interest=ausland_uri,
            has_result=site_ttl,
            used_procedure=procedure_ttl,
            has_attribute=plot_layout_attributes,
            result_date_time=result_time,
        )

        # insert plot layout
        graph = extensions.graph_service.add_data(
            data=plot_layout_ttl.dict(by_alias=True)
        )

        plot_selection_ttl = self.create_plot_selection(
            graph=graph,
            data=dict(plot_selection),
            plot_ttl=dict(plot_ttl),
            procedure_ttl=procedure_ttl,
            site_ttl=site_ttl,
            result_time=result_time
        )
        graph = extensions.graph_service.add_data(
            data=plot_selection_ttl.dict(by_alias=True)
        )

        return graph

    def create_plot_selection_survey_ttl(
        self,
        graph: Graph,
        procedure_ttl: dict,
        plot_ttl: dict,
        survey_data: dict,
    ):
        if "plot-selection" not in survey_data:
            return graph
        if not survey_data["plot-selection"]:
            return graph

        # plot selections can be multiple
        for p in survey_data["plot-selection"]:
            plot_selection_ttl = self.create_plot_selection(
                graph=graph,
                data=dict(p),
                plot_ttl=dict(plot_ttl),
                procedure_ttl=procedure_ttl
            )
            graph = extensions.graph_service.add_data(
                data=plot_selection_ttl.dict(by_alias=True)
            )
        return graph

    def create_plot_selection(
        self,
        graph: Graph,
        data: dict,
        plot_ttl: dict,
        procedure_ttl: dict,
        result_time: str,
        names_map: dict = None,
        site_ttl: dict = None,
        site_visit_ttl: dict = None
    ):
        if not names_map:
            names_map = utility.get_names_map_data()

        site_ttl, site_visit_ttl = None, None
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]
        record_id = plot_ttl["rt"].id

        plot_selection = dict(data)
        plot_selection_id = utility.uuid(dict(plot_selection))
        plot_name = plot_selection["plot_label"]["value"]

        plot_selection_rdf_id = f"{
            record_id}/plot-selection/{plot_selection_id}"

        # plot layout attributes
        plot_label_attrs = names_map["plot_label_attributes"]
        plot_selection_attrs = names_map["plot_selection_attributes"]

        plot_selection_attributes = self.get_attributes_list(
            data=dict(plot_selection),
            dataset_ttl=record_id,
            key_to_find="attribute_uri",
            approved=plot_selection_attrs,
            existing_attributes=[],
            skip_fields=plot_label_attrs,
            # add_has_attributes={"plot_label": plot_label_attributes},
            add_is_part_of=False,
        )

        site_name_uuid = utility.compute_md5_hash(value=plot_name)
        ausland_uri = f"{record_id}/australia-land/{site_name_uuid}"
        sample_ttl = fields.FeatureOfInterest(
            id=ausland_uri,
            label=f"Australia Land {plot_name}",
            in_dataset=plot_ttl["rt"],
            feature_type="http://linked.data.gov.au/def/tern-cv/4381b1b0-d432-4764-aec3-a26d2b8d85af",
        )

        plot_selection_ttl = fields.Sampling(
            id=plot_selection_rdf_id,
            identifier=plot_selection_id,
            label=f"plot selection {plot_name}",
            in_dataset=plot_ttl["rt"],
            result_date_time=result_time,
            used_procedure=procedure_ttl,
            has_feature_of_interest=sample_ttl,
            has_attribute=plot_selection_attributes,
            has_result=site_ttl,
            has_site_visit=site_visit_ttl
        )

        return plot_selection_ttl

    def create_plot_layout_points(
        self,
        graph: Graph,
        plot_layout: dict,
        plot_ttl: dict,
        uuid: str,
        key_to_find: str,
    ):
        # plot points
        plot_points = utility.find_property_with_key(
            object=dict(plot_layout),
            field_name=None,
            key_to_find=key_to_find,
            outputs=[],
        )
        if plot_points:
            plot_points_attr = plot_points[0]["value"]
            points = plot_points_attr["value"]
            points_attrs = []
            for p in points:
                points_attrs.append(
                    self.create_schema_spacial(
                        dataset_ttl=plot_ttl["rt"],
                        lat=p["lat"],
                        lng=p["lng"],
                        label=p["name"]["description"],
                    )
                )
            if points_attrs:
                label = plot_points_attr["nrm_label"]
                plot_points_values_id = (
                    plot_ttl["rt"].id
                    + f"/attribute/{label.replace(" ", "-")}/value/{uuid.lower()}"
                )
                plot_points_points_id = f"{plot_points_values_id}/points"

                plot_points_ttl = fields.Points(
                    id=plot_points_points_id,
                    in_dataset=plot_ttl["rt"],
                    has_geometry=points_attrs,
                )
                graph = extensions.graph_service.add_data(
                    data=plot_points_ttl.dict(by_alias=True)
                )

                plot_point_value_ttl = fields.IRI(
                    id=plot_points_values_id,
                    in_dataset=plot_ttl["rt"],
                    value=plot_points_points_id,
                )
                graph = extensions.graph_service.add_data(
                    data=plot_point_value_ttl.dict(by_alias=True)
                )
                attr = fields.Attribute(
                    id=BNode().n3(),
                    in_dataset=plot_ttl["rt"],
                    label=label,
                    attribute=plot_points_attr["attribute_uri"],
                    has_value_iri=plot_points_values_id,
                    has_simple_value_iri=plot_points_points_id,
                )
                return {"graph": graph, "attr": attr}

        return {"graph": graph, "attr": None}

    def create_site_visit_ttl(self):
        # site visit attributes
        site_visit_attrs = names_map["site_visit_attributes"]
        site_visit_attributes = self.get_attributes_list(
            data=dict(plot_visit),
            dataset_ttl=plot_ttl["rt"],
            key_to_find="plot_attribute_uri",
            approved=site_visit_attrs,
            existing_attributes=[],
            skip_fields=[],
            add_is_part_of=False,
        )
        # site visit
        site_visit_ttl = fields.SiteVisit(
            id=plot_ttl["svt"],
            identifier=plot_visit_id,
            label=f"site visit {visit_field_name}",
            in_dataset=plot_ttl["rt"],
            started_at_time=plot_visit["start_date"],
            ended_at_time=plot_visit["end_date"],
            has_site=site_ttl,
            has_attribute=site_visit_attributes,
        )
        # insert site visit
        graph = extensions.graph_service.add_data(
            data=site_visit_ttl.dict(by_alias=True)
        )

    def create_site_ttl(
        self,
        graph: Graph,
        dataset_ttl: dict,
        survey_data: dict,
        protocol: dict
    ):
        """generates turtle for plot layout and plot visit"""
        names_map = utility.get_names_map_data()

        plot_visit = dict(survey_data["plot_visit"])
        plot_selection = dict(plot_visit["plot_layout"]["plot_selection"])
        site_name = plot_selection["plot_label"]
        if "value" in site_name:
            site_name = site_name["value"]
        visit_start_date = plot_visit["start_date"]
        plot_selection_id = plot_selection["uuid"]

        # as there is no unique field in plot visit we will use site name + visit field name
        plot_visit_id = utility.compute_md5_hash(
            value=f"{visit_start_date}{site_name}")
        site_name_uuid = utility.compute_md5_hash(value=site_name)
        visit_field_name = plot_visit["visit_field_name"]
        if "value" in visit_field_name:
            visit_field_name = visit_field_name["value"]

        # existing site
        site_uri = f"{MONITOR_PREFIX}/sites/{plot_selection_id}"
        if utility.has_custom_config(protocol=protocol, key="skip-site-visit"):
            return {
                "graph": graph,
                "plot_ttl": {
                    "st": site_uri,
                    "svt": None,
                    "rt": dataset_ttl,
                },
            }

        site_visit_uri = f"{dataset_ttl.id}/sites-visit/{plot_visit_id}"

        # site visit attributes
        site_visit_attrs = names_map["site_visit_attributes"]
        site_visit_attributes = self.get_attributes_list(
            data=dict(plot_visit),
            dataset_ttl=dataset_ttl,
            key_to_find="plot_attribute_uri",
            approved=site_visit_attrs,
            existing_attributes=[],
            skip_fields=[],
            add_is_part_of=False,
        )
        # site visit
        site_visit_ttl = fields.SiteVisit(
            id=site_visit_uri,
            identifier=plot_visit_id,
            label=f"site visit {visit_field_name}",
            in_dataset=dataset_ttl,
            started_at_time=plot_visit["start_date"],
            ended_at_time=plot_visit["end_date"],
            has_site=site_uri,
            has_attribute=site_visit_attributes,
        )
        # insert site visit
        graph = extensions.graph_service.add_data(
            data=site_visit_ttl.dict(by_alias=True)
        )

        return {
            "graph": graph,
            "plot_ttl": {
                "st": site_uri,
                "svt": site_visit_uri,
                "rt": dataset_ttl,
            },
        }

    def get_attributes_list(
        self,
        data: dict,
        dataset_ttl: dict,
        key_to_find: str,
        approved: list = [],
        existing_attributes: list = [],
        skip_fields: list = [],
        handle_photo: bool = False,
        add_has_attributes: dict = {},
        add_is_part_of: bool = True,
    ):
        attributes = existing_attributes
        processed_keys = []
        obs_keys = list(data.keys())
        has_attributes_keys = list(add_has_attributes.keys())
        for key in obs_keys:
            attr_val = utility.find_property_with_key(
                object=data[key],
                field_name=key,
                key_to_find=key_to_find,
                outputs=[],
            )
            for attr in attr_val:
                attr_object = utility.create_object_to_process(
                    object=dict(attr), handle_photo_attr=handle_photo
                )
                if not attr_object["values"]:
                    continue
                if attr_object["field_name"] in processed_keys:
                    continue
                if attr_object["field_name"] in skip_fields:
                    continue
                if attr_object["field_name"] not in approved:
                    continue

                processed_keys.append(attr_object["field_name"])
                object_to_process = attr_object["values"]
                if key_to_find in object_to_process:
                    attribute = self.create_attribute_ttl(
                        object=dict(object_to_process),
                        label=attr_object["label"],
                        field_type=FieldType.ATTRIBUTE,
                        dataset_ttl=dataset_ttl,
                        field=attr_object["field_name"],
                        add_is_part_of=add_is_part_of,
                    )
                    if key in has_attributes_keys:
                        attr = dict(attribute)
                        attr_with_has_attr = fields.AttributeWithHasAttributes(
                            **attr, has_attribute=add_has_attributes[key]
                        )
                        if attr_with_has_attr:
                            attributes.append(attr_with_has_attr)
                            continue
                    if attribute:
                        attributes.append(attribute)
        return attributes

    def create_survey_data_ttl(
        self,
        graph: Graph,
        protocol: dict,
        survey_data: dict,
        plot_ttl: dict,
        procedure_ttl: dict,
    ):
        if protocol["survey-model"] == "plot-definition-survey":
            # special condition to handle plot-definition-survey
            graph = self.create_plot_definition_survey_ttl(
                graph=graph,
                plot_ttl=plot_ttl,
                survey_data=survey_data,
                procedure_ttl=procedure_ttl,
            )
        if protocol["survey-model"] == "plot-selection-survey":
            # special condition to handle plot-definition-survey
            graph = self.create_plot_selection_survey_ttl(
                graph=graph,
                plot_ttl=plot_ttl,
                survey_data=survey_data,
                procedure_ttl=procedure_ttl,
            )

        return graph

    def create_observation_ttl(
        self,
        graph: Graph,
        protocol: dict,
        observations: dict,
        survey_data: dict,
        plot_ttl: dict,
        procedure_ttl: dict,
        protocol_name: str,
        setup_id: str = None,

        vantage_start: list = [],
        vantage_end: list = [],
        vantage_point_val: list = [],
        vantage_point_outlook_photos: list = [],

        transect_start: list = [],
        transect_end: list = [],
        transect_attrs_photo: list = [],

        track_station_attributes: list = [],
        track_station_photo_attributes: list = [],
        track_station_habitat_attributes: list = [],
        track_station_habitat_photo_attributes: list = [],
    ):

        site_ttl, site_visit_ttl, record_ttl = None, None, None
        if protocol["plot-based-protocol"]:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]

        record_ttl = plot_ttl["rt"]
        names_map = utility.get_names_map_data()

        schema_spatial = None
        transect_ttl = None
        near_by_transect_ttl = None
        set_id_transect_ttl = None
        location_data = None
        phenomenon_time = None

        track_logs_ttls = []
        transect_feature_types = []
        observer_ttls = []
        observer_details = {}

        outlook_zone_ttls = None
        vantage_point_ttl = None
        sampler_ttl = None

        survey_date = utility.fetch_start_date(object=survey_data)
        phenomenon_uri = f"{
            record_ttl.id}/phenomenon-time/{utility.compute_md5_hash(value=survey_date["raw"])}"
        phenomenon_time_ttl = fields.TimeInstant(
            id=phenomenon_uri,
            date_timestamp=survey_date["raw"],
        )

        if "transect-feature-types" in protocol:
            transect_feature_types = protocol["transect-feature-types"]

        if utility.has_custom_config(protocol=protocol, key="handle-observer"):
            obs_details = utility.find_property_with_key(
                object=observations,
                field_name=None,
                key_to_find="handle-observer",
                model=None,
                outputs=[],
            )
            if not obs_details:
                obs_details = utility.find_property_with_key(
                    object=survey_data,
                    field_name=None,
                    key_to_find="handle-observer",
                    model=None,
                    outputs=[],
                )
            if obs_details:
                # handle transect with attributes e.g. third sets
                result = self.create_observers_roles_ttl(
                    graph=graph,
                    schema_spatial=schema_spatial,
                    names_map=names_map,
                    procedure=procedure_ttl,
                    result_time=survey_date["raw"],
                    protocol=protocol,
                    plot_ttl=plot_ttl,
                    values=obs_details,
                )
                graph = result["graph"]
                observer_ttls = result["observer_ttls"]
                observer_details = result["observer_details"]

        if setup_id:
            # handle vantage point with attributes e.g. third sets
            if utility.is_vantage_with_attributes(protocol=protocol):
                outlook_zones = utility.find_property_with_key(
                    object=observations,
                    field_name=None,
                    key_to_find="handle-outlook-zone",
                    model=None,
                    outputs=[],
                )
                schema_spatial = self.create_schema_spacial(
                    dataset_ttl=record_ttl, 
                    lat=vantage_start[0]["value"]["lat"], 
                    lng=vantage_start[0]["value"]["lng"]
                )
                result = self.create_vantage_point_with_attributes_ttl(
                    graph=graph,
                    schema_spatial=schema_spatial,
                    procedure=procedure_ttl,
                    result_time=survey_date["raw"],
                    protocol=protocol,
                    plot_ttl=plot_ttl if plot_ttl else None,
                    phenomenon_time=phenomenon_time_ttl,
                    data=dict(observations),
                    vantage_id=setup_id,
                    vantage_start=vantage_start,
                    vantage_end=vantage_end,
                    vantage_point_val=vantage_point_val,
                    outlook_zones=outlook_zones,
                    outlook_photo_attrs=vantage_point_outlook_photos
                )
                graph = result["graph"]
                vantage_point_ttl = result["vantage_point_ttl"]
                outlook_zone_ttls = result["outlook_zone_ttls"]

            # handle transect with attributes e.g. third sets
            if utility.is_setup_id_based_transect(protocol=protocol):
                result = self.create_setup_id_based_transect_ttl(
                    graph=graph,
                    schema_spatial=schema_spatial,
                    procedure=procedure_ttl,
                    result_time=survey_date["raw"],
                    protocol=protocol,
                    phenomenon_time=phenomenon_time_ttl,
                    plot_ttl=plot_ttl if plot_ttl else None,
                    plot_data=(
                                dict(survey_data["plot_visit"]
                                     ) if site_ttl else None
                            ),
                    data=dict(observations),
                    transect_id=setup_id,
                    transect_val_start=transect_start,
                    transect_val_end=transect_end,
                    transect_attrs_photo=transect_attrs_photo,
                )
                graph = result["graph"]
                set_id_transect_ttl = result["set_id_transect_ttl"]

            # handle track station with attributes e.g. third sets
            if utility.is_track_station_attributes(protocol=protocol):
                result = self.create_track_station_with_attributes_ttl(
                    graph=graph,
                    schema_spatial=schema_spatial,
                    procedure=procedure_ttl,
                    result_time=survey_date["raw"],
                    protocol=protocol,
                    plot_ttl=plot_ttl if plot_ttl else None,
                    data=dict(observations),
                    station_id=setup_id,
                    track_station_attributes=track_station_attributes,
                    track_station_photo_attributes=track_station_photo_attributes,
                    track_station_habitat_attributes=track_station_habitat_attributes,
                    track_station_habitat_photo_attributes=track_station_habitat_photo_attributes,
                )
                graph = result["graph"]
                # vantage_point_ttl = result["vantage_point_ttl"]

        obs_collections = {}
        obs_members = {}
        attributes = []
        relational_attributes = {}
        feature_type_attributes = {}
        feature_of_interests_attributes = {}
        feature_type_ttls = {}
        feature_type_phenomenon_times = {}

        obs_objects = dict(observations)
        obs_keys = list(obs_objects.keys())
        observations_exist = True
        if not obs_keys:
            observations_exist = False
            obs_objects = {"survey_data": [survey_data]}
            obs_keys = list(obs_objects.keys())
        for model in obs_keys:

            if len(obs_objects[model]) == 0:
                continue

            barcode_ttl = []
            feature_type_ttl = None
            for observation in obs_objects[model]:
                # some of protocols have attributes in the survey
                # so we need those attributes in observation collection as well
                obs_with_all = (
                    {**observation, **survey_data}
                    if observations_exist
                    else {**observation}
                )
                # remove all the plot data if exist
                for p in PLOT_MODELS:
                    if p in obs_with_all:
                        del obs_with_all[p]
                    if p.replace("-", "_") in obs_with_all:
                        del obs_with_all[p.replace("-", "_")]
                obs_keys = list(obs_with_all.keys())
                feature_group_value = None
                processed_keys = []

                possible_location_Keys = extensions.protocol_service.all_possible_keys(
                    key="location"
                )
                possible_phenomenon_time_Keys = (
                    extensions.protocol_service.all_possible_keys(
                        key="phenomenon_time")
                )
                location_data = utility.find_object_by_key(
                    object=obs_with_all, keys=possible_location_Keys
                )
                phenomenon_time = utility.find_object_by_key(
                    object=obs_with_all, keys=possible_phenomenon_time_Keys
                )
                if location_data and not schema_spatial:
                    location_data = location_data
                    if "object" in location_data:
                        location_data = location_data["object"]

                    lat = location_data["lat"]
                    lng = location_data["lng"]
                    schema_spatial = self.create_schema_spacial(
                        dataset_ttl=record_ttl, lat=lat, lng=lng
                    )
                if not schema_spatial and site_ttl:
                    plot_points = survey_data["plot_visit"]["plot_layout"]["plot_points"]
                    if "value" in plot_points:
                        location_data = plot_points["value"][0]
                    if isinstance(plot_points, list):
                        location_data = plot_points[0]
                    lat = location_data["lat"]
                    lng = location_data["lng"]
                    schema_spatial = self.create_schema_spacial(
                        dataset_ttl=record_ttl, lat=lat, lng=lng
                    )

                if "reset-linked-attributes" in protocol:
                    if protocol["reset-linked-attributes"]:
                        feature_type_attributes = {}
                        feature_of_interests_attributes = {}

                if phenomenon_time:
                    phenomenon_time = phenomenon_time
                    if "object" in phenomenon_time:
                        phenomenon_time = phenomenon_time["object"]

                    phenomenon_uri = f"{
                        record_ttl.id}/phenomenon-time/{utility.compute_md5_hash(value=phenomenon_time)}"
                    phenomenon_time_ttl = fields.TimeInstant(
                        id=phenomenon_uri,
                        date_timestamp=phenomenon_time,
                    )

                feature_group_value = None
                if "feature-types-group-key" in protocol:
                    if protocol["feature-types-group-key"] in observation:
                        feature_group_value = observation[
                            protocol["feature-types-group-key"]
                        ]
                        if isinstance(feature_group_value, dict):
                            if "value" in feature_group_value:
                                feature_group_value = str(
                                    feature_group_value["value"])
                if feature_group_value:
                    feature_group_value = str(feature_group_value)

                voucher_val = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=model,
                    key_to_find="handle-voucher",
                    model=model,
                    outputs=[],
                )

                quadrat_val = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=model,
                    key_to_find="handle-quadrat",
                    model=model,
                    outputs=[],
                )

                near_by_transect_val = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=model,
                    key_to_find="handle-nearby-track-transect-attr",
                    model=model,
                    outputs=[],
                )
                near_by_transect_positions = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=model,
                    key_to_find="handle-nearby-track-transect-position",
                    model=model,
                    outputs=[],
                )
                track_logs = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=model,
                    key_to_find="handle-track-log",
                    model=model,
                    outputs=[],
                )

                handle_sample_obs_collections = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=model,
                    key_to_find="handle-sample-obs-collection",
                    model=model,
                    outputs=[],
                )
                equipments_sampler = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=model,
                    key_to_find="handle-equipments-sampler",
                    model=model,
                    outputs=[],
                )
                handle_multi_feature_types = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=model,
                    key_to_find="handle-multi-feature-types",
                    model=model,
                    outputs=[],
                )
                handle_multi_obs_collections = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=model,
                    key_to_find="handle-multi-obs-collection-types",
                    model=model,
                    outputs=[],
                )
                additional_multi_obs_members = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=model,
                    key_to_find="handle-additional-multi-obs-member",
                    model=model,
                    outputs=[],
                )

                plot_establishment_val = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=model,
                    key_to_find="handle-plot-establishment",
                    model=model,
                    outputs=[],
                )
                photo_position_establishment_val = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=model,
                    key_to_find="handle-photo-position-establishment",
                    model=model,
                    outputs=[],
                )
                transect_val = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=None,
                    key_to_find="handle-transect",
                    model=model,
                    outputs=[],
                )
                transect_attributes_photo_val = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=None,
                    key_to_find="handle-transect-attributes-photo",
                    model=model,
                    outputs=[],
                )

                sample_trees = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=None,
                    key_to_find="handle-sample-tree",
                    model=model,
                    outputs=[],
                )
                sample_stems = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=None,
                    key_to_find="handle-sample-stem",
                    model=model,
                    outputs=[],
                )

                tree_bool_iris = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=None,
                    key_to_find="handle-tree-bool-iri",
                    model=model,
                    outputs=[],
                )
                dbh_samplers = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=None,
                    key_to_find="handle-dbh-sampler",
                    model=model,
                    outputs=[],
                )
                basal_area_val = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=None,
                    key_to_find="handle-basal-area",
                    model=model,
                    outputs=[],
                )
                replicates = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=None,
                    key_to_find="handle-replicate",
                    model=model,
                    outputs=[],
                )
                replace_foi_values = utility.find_property_with_key(
                    object=obs_with_all,
                    field_name=None,
                    key_to_find="handle-replace-foi",
                    model=model,
                    outputs=[],
                )

                if track_logs:
                    result = self.create_track_logs_ttl(
                        graph=graph,
                        schema_spatial=schema_spatial,
                        procedure=procedure_ttl,
                        result_time=survey_date["raw"],
                        protocol=protocol,
                        feature_type_ttls=feature_type_ttls,
                        plot_ttl=plot_ttl if plot_ttl else None,
                        values=track_logs,
                    )
                    graph = result["graph"]
                    track_logs_ttls = result["track_logs_ttls"]

                if transect_val:
                    # handle simple transects e.g. cover
                    for transect in transect_val:
                        t_object = utility.create_object_to_process(
                            object=dict(transect)
                        )
                        if not t_object["values"]:
                            continue
                        if t_object["field_name"] in processed_keys:
                            continue

                        processed_keys.append(t_object["field_name"])
                        object_to_process = dict(t_object["values"])
                        # special condition to handle transect
                        result = self.create_transect_ttl(
                            graph=graph,
                            plot_ttl=plot_ttl if plot_ttl else None,
                            feature_type_ttls=feature_type_ttls,
                            schema_spatial=schema_spatial,
                            transect_data=object_to_process,
                            procedure=procedure_ttl,
                            result_time=survey_date["raw"],
                            transects=transect_val,
                            transect_attributes_photos=transect_attributes_photo_val,
                            plot_data=(
                                dict(survey_data["plot_visit"]
                                     ) if site_ttl else None
                            ),
                            protocol=protocol,
                            protocol_name=protocol_name
                        )
                        graph = result["graph"]
                        transect_ttl = result["transect_ttl"]

                # handle near by transect
                if near_by_transect_val and near_by_transect_positions:
                    result = self.create_nearby_transect_ttl(
                        graph=graph,
                        schema_spatial=schema_spatial,
                        procedure=procedure_ttl,
                        result_time=survey_date["raw"],
                        protocol=protocol,
                        feature_type_ttls=feature_type_ttls,
                        plot_ttl=plot_ttl if plot_ttl else None,
                        values=near_by_transect_val,
                        transect_positions=near_by_transect_positions
                    )
                    graph = result["graph"]
                    near_by_transect_ttl = result["near_by_transect_ttl"]

                for key in obs_keys:
                    if not isinstance(obs_with_all[key], dict) and not isinstance(
                        obs_with_all[key], list
                    ):
                        continue
                    attr_val = utility.find_property_with_key(
                        object=obs_with_all[key],
                        field_name=key,
                        key_to_find="attribute_uri",
                        model=model,
                        outputs=[],
                    )
                    for attr in attr_val:
                        attr_object = utility.create_object_to_process(
                            object=dict(attr)
                        )
                        if not attr_object["field_name"]:
                            import json

                            flask.current_app.logger.error(
                                f"Failed to find attribute name, object: {
                                    json.dumps(attr_object)}"
                            )
                        if not attr_object["values"]:
                            continue
                        # if attr_object["field_name"] in processed_keys: continue
                        if (
                            attr_object["field_name"]
                            in names_map["global_skip_attributes"]
                        ):
                            continue

                        object_to_process = dict(attr_object["values"])

                        if "attribute_uri" in object_to_process:
                            # if "status" in object_to_process: continue

                            custom_handlers = utility.get_custom_configs(
                                object=object_to_process,
                                field_name=attr_object["field_name"],
                                protocol=dict(protocol),
                                ignore=["handle-basal-area"]
                            )
                            if custom_handlers:
                                continue

                            attribute = self.create_attribute_ttl(
                                object=dict(object_to_process),
                                label=attr_object["label"],
                                field_type=FieldType.ATTRIBUTE,
                                dataset_ttl=record_ttl,
                                field=attr_object["field_name"],
                            )

                            if attribute:
                                # processed_keys.append(attr_object["field_name"])
                                f_types = utility.is_linked_to_feature_type(
                                    field=attr_object["field_name"],
                                    protocol=dict(protocol),
                                )
                                foi_attr_types = utility.foi_attributes_types(
                                    field=attr_object["field_name"],
                                    protocol=dict(protocol),
                                )
                                for ft in f_types:
                                    f_atttr_name = utility.feature_type_prefix(
                                        name=ft, position=schema_spatial
                                    )
                                    if "id-based-foi" in protocol:
                                        if protocol["id-based-foi"]:
                                            f_atttr_name = utility.feature_type_prefix(
                                                name=ft,
                                                position=None,
                                                id=observation["id"],
                                            )
                                    if feature_group_value:
                                        f_atttr_name = utility.feature_type_prefix(
                                            name=ft,
                                            position=None,
                                            id=str(feature_group_value),
                                        )
                                    if f_atttr_name not in feature_type_attributes:
                                        feature_type_attributes[f_atttr_name] = [
                                        ]
                                    feature_type_attributes[f_atttr_name].append(
                                        attribute
                                    )

                                for ft in foi_attr_types:
                                    f_atttr_name = utility.feature_type_prefix(
                                        name=ft, position=schema_spatial
                                    )
                                    if "id-based-foi" in protocol:
                                        if protocol["id-based-foi"]:
                                            f_atttr_name = utility.feature_type_prefix(
                                                name=ft,
                                                position=None,
                                                id=observation["id"],
                                            )
                                    if feature_group_value:
                                        f_atttr_name = utility.feature_type_prefix(
                                            name=ft,
                                            position=None,
                                            id=str(feature_group_value),
                                        )
                                    if (
                                        f_atttr_name
                                        not in feature_of_interests_attributes
                                    ):
                                        feature_of_interests_attributes[
                                            f_atttr_name
                                        ] = []
                                    feature_of_interests_attributes[
                                        f_atttr_name
                                    ].append(attribute)

                                relational_obs = self.get_relational_obs(
                                    attribute_uri=object_to_process["attribute_uri"],
                                    protocol=protocol,
                                    protocol_name=protocol_name
                                )
                                for relational_ob in relational_obs:
                                    relational_uri = relational_ob
                                    if feature_group_value:
                                        relational_uri = relational_uri + feature_group_value

                                    if relational_uri not in relational_attributes:
                                        relational_attributes[relational_uri] = [
                                        ]
                                    relational_attributes[relational_uri].append(
                                        attribute)

                if handle_multi_obs_collections or additional_multi_obs_members:
                    
                    result = self.create_multi_obs_collections_ttl(
                        graph=graph,
                        schema_spatial=schema_spatial,
                        procedure=procedure_ttl,
                        protocol_name=protocol_name,
                        result_time=survey_date["raw"],
                        protocol=protocol,
                        plot_ttl=plot_ttl if plot_ttl else None,
                        values=handle_multi_obs_collections,
                        names_map=names_map,
                        all_feature_type_ttls=feature_type_ttls,
                        phenomenon_time=phenomenon_time_ttl,
                        feature_group_value=feature_group_value,
                        additional_multi_obs_members=additional_multi_obs_members,
                        all_feature_type_attributes=feature_type_attributes,
                        all_feature_of_interests_attributes=feature_of_interests_attributes,
                        transect_ttl=transect_ttl,
                        obs_id=observation["id"],
                    )
                    graph = result["graph"]
                    feature_type_ttls = result["all_feature_type_ttls"]

                for key in obs_keys:
                    ptr_val = utility.find_property_with_key(
                        object=obs_with_all[key],
                        field_name=key,
                        key_to_find="property_uri",
                        model=model,
                        outputs=[],
                    )

                    for ptr in ptr_val:
                        ptr_object = utility.create_object_to_process(
                            object=dict(ptr))
                        if not ptr_object["values"]:
                            continue
                        if (
                            ptr_object["field_name"]
                            in names_map["global_skip_attributes"]
                        ):
                            continue

                        object_to_process = ptr_object["values"]
                        if "feature_type" not in object_to_process:
                            continue
                        if "property_uri" in object_to_process :
                            custom_handlers = utility.get_custom_configs(
                                object=object_to_process,
                                field_name=ptr_object["field_name"],
                                protocol=dict(protocol),
                            )
                            if custom_handlers:
                                continue
                            id_label = (
                                ptr_object["label"].replace(
                                    "_", "-").replace(" ", "-")
                            )
                            if "feature_type" in object_to_process:
                                feature_index = utility.get_feature_type_index(
                                    obs_with_all=dict(obs_with_all),
                                    feature_types=object_to_process["feature_type"],
                                    protocol=dict(protocol),
                                )
                                feature_name = object_to_process["feature_type"][
                                    feature_index
                                ]["value"]

                                f_name = utility.feature_type_prefix(
                                    name=feature_name, position=schema_spatial
                                )
                                if "id-based-foi" in protocol:
                                    if protocol["id-based-foi"]:
                                        f_name = utility.feature_type_prefix(
                                            name=feature_name,
                                            position=None,
                                            id=str(observation["id"]),
                                        )

                                if feature_group_value:
                                    f_name = utility.feature_type_prefix(
                                        name=feature_name,
                                        position=None,
                                        id=feature_group_value,
                                    )
                                uuid = utility.generate_uuid()
                                member_uri = (
                                    f"{record_ttl.id}/observation/{id_label}/{uuid}"
                                )
                                if f_name not in obs_members:
                                    obs_members[f_name] = []
                                obs_members[f_name].append(
                                    {
                                        "member_uri": member_uri,
                                        "feature_type": object_to_process[
                                            "feature_type"
                                        ][feature_index],
                                    }
                                )
                                if f_name not in feature_type_ttls:
                                    foi_attrs = []
                                    if f_name in feature_of_interests_attributes:
                                        foi_attrs = feature_of_interests_attributes[
                                            f_name
                                        ]
                                    feature_type_ttls[f_name] = (
                                        self.create_feature_of_interest_ttl(
                                            type=f_name,
                                            value=object_to_process["feature_type"][
                                                feature_index
                                            ],
                                            protocol=protocol,
                                            record_ttl=record_ttl,
                                            schema_spatial=schema_spatial,
                                            phenomenon_time=phenomenon_time_ttl,
                                            plot_data=(
                                                dict(survey_data["plot_visit"]
                                                     ) if site_ttl else None
                                            ),
                                            has_attributes=foi_attrs,
                                            site_ttl=site_ttl if site_ttl else None,
                                            site_visit_ttl=site_visit_ttl,
                                            procedure=procedure_ttl,
                                            result_time=survey_date["raw"],
                                            transect_ttl=transect_ttl,
                                            replace_foi_values=replace_foi_values
                                        )
                                    )
                                    if "sampling_ttl" in feature_type_ttls[f_name]:
                                        s_ttl = feature_type_ttls[f_name]["sampling_ttl"]
                                        graph = extensions.graph_service.add_data(
                                            data=s_ttl.dict(by_alias=True)
                                        )

                                    feature_type_phenomenon_times[f_name] = (
                                        phenomenon_time_ttl
                                    )
                                value = dict(
                                    self.create_attribute_ttl(
                                        object=dict(object_to_process),
                                        label=ptr_object["label"],
                                        field_type=FieldType.PROPERTY,
                                        dataset_ttl=record_ttl,
                                        field=ptr_object["field_name"],
                                    )
                                )
                                has_result, has_simple_result, has_simple_result_iri = (
                                    None,
                                    None,
                                    None,
                                )
                                if value:
                                    has_result = value["has_result"]
                                    has_simple_result = value["has_simple_value"]
                                    has_simple_result_iri = value[
                                        "has_simple_value_iri"
                                    ]
                                has_attributes = []
                                relational_uri = object_to_process["property_uri"]
                                if feature_group_value:
                                    relational_uri = relational_uri + feature_group_value
                                if relational_uri in relational_attributes:
                                    has_attributes = relational_attributes[relational_uri]

                                observation_ttl = fields.Observation(
                                    id=member_uri,
                                    label=ptr_object["label"],
                                    in_dataset=record_ttl,
                                    is_part_of=record_ttl.is_part_of,
                                    schema_spatial=schema_spatial,
                                    has_feature_of_interest=feature_type_ttls[f_name][
                                        "object"
                                    ],
                                    has_result=has_result,
                                    has_site_visit=site_visit_ttl,
                                    has_simple_result=has_simple_result,
                                    has_simple_result_iri=has_simple_result_iri,
                                    has_attribute=has_attributes,
                                    observed_property=object_to_process["property_uri"],
                                    has_phenomenon_time=phenomenon_time_ttl,
                                    used_procedure=procedure_ttl,
                                    result_date_time=survey_date["raw"],
                                )
                                graph = extensions.graph_service.add_data(
                                    data=observation_ttl.dict(by_alias=True)
                                )
                                processed_keys.append(ptr["field_name"])

                if sample_trees:
                    result = self.create_tree_ttl(
                        graph=graph,
                        schema_spatial=schema_spatial,
                        procedure=procedure_ttl,
                        protocol_name=protocol_name,
                        names_map=names_map,
                        result_time=survey_date["raw"],
                        protocol=protocol,
                        plot_ttl=plot_ttl if plot_ttl else None,
                        sample_trees=sample_trees,
                        sample_stems=sample_stems,
                        dbh_sampler=dbh_samplers,
                        tree_bool_iris=tree_bool_iris,
                        phenomenon_time=phenomenon_time_ttl,
                        relational_attributes=relational_attributes,
                        feature_group_value=feature_group_value
                    )
                    graph = result["graph"]

                if equipments_sampler:
                    # handle sampling equipments
                    result = self.create_equipment_ttl(
                        graph=graph,
                        schema_spatial=schema_spatial,
                        plot_ttl=plot_ttl,
                        procedure=procedure_ttl,
                        result_time=survey_date["raw"],
                        protocol=protocol,
                        equipments=equipments_sampler,
                    )
                    graph = result["graph"]
                    sampler_ttl = result["equipment_sampler_ttl"]

                if handle_sample_obs_collections:
                    result = self.create_sample_obs_collections_ttl(
                        graph=graph,
                        schema_spatial=schema_spatial,
                        procedure=procedure_ttl,
                        protocol_name=protocol_name,
                        names_map=names_map,
                        result_time=survey_date["raw"],
                        protocol=protocol,
                        plot_ttl=plot_ttl if plot_ttl else None,
                        values=handle_sample_obs_collections,
                        phenomenon_time=phenomenon_time_ttl,
                        relational_attributes=relational_attributes,
                        feature_group_value=feature_group_value,
                        vantage_point=vantage_point_ttl,
                        outlook_zones=outlook_zone_ttls,
                        set_id_transect_ttl=set_id_transect_ttl,
                        near_by_transect_ttl=near_by_transect_ttl,
                        sampler=sampler_ttl,
                        track_logs_ttls=track_logs_ttls,
                        observer_details=observer_details,
                        observer_ttls=observer_ttls,

                    )
                    graph = result["graph"]

                if handle_multi_feature_types:
                    result = self.create_multi_feature_types_ttl(
                        graph=graph,
                        schema_spatial=schema_spatial,
                        procedure=procedure_ttl,
                        protocol_name=protocol_name,
                        result_time=survey_date["raw"],
                        protocol=protocol,
                        plot_ttl=plot_ttl if plot_ttl else None,
                        values=handle_multi_feature_types,
                        names_map=names_map,
                        all_feature_type_ttls=feature_type_ttls,
                        phenomenon_time=phenomenon_time_ttl,
                        all_feature_type_attributes=feature_type_attributes,
                        relational_attributes=relational_attributes,
                        feature_group_value=feature_group_value

                    )
                    graph = result["graph"]
                    feature_type_ttls = result["all_feature_type_ttls"]

                if replicates:
                    result = self.create_replicates_ttl(
                        graph=graph,
                        plot_ttl=plot_ttl if plot_ttl else None,
                        feature_type_ttls=feature_type_ttls,
                        schema_spatial=schema_spatial,
                        values=replicates,
                        procedure=procedure_ttl,
                        result_time=survey_date["raw"],
                        protocol=protocol,
                        feature_group_value=feature_group_value,
                    )
                    graph = result["graph"]

                for key in obs_keys:
                    if not isinstance(obs_with_all[key], dict) and not isinstance(
                        obs_with_all[key], list
                    ):
                        continue
                    barcode_val = utility.find_property_with_key(
                        object=obs_with_all[key],
                        field_name=key,
                        key_to_find="handle-barcode",
                        model=model,
                        outputs=[],
                    )
                    barcode_attribute_val = utility.find_property_with_key(
                        object=obs_with_all[key],
                        field_name=key,
                        key_to_find="handle-barcode-attributes",
                        model=model,
                        outputs=[],
                    )
                    barcode_observations_val = utility.find_property_with_key(
                        object=obs_with_all[key],
                        field_name=key,
                        key_to_find="handle-barcode-observations",
                        model=model,
                        outputs=[],
                    )
                    image_val = utility.find_property_with_key(
                        object=obs_with_all[key],
                        field_name=key,
                        key_to_find="handle-media",
                        model=model,
                        outputs=[],
                    )

                    photo_sampler_val = utility.find_property_with_key(
                        object=obs_with_all,
                        field_name=key,
                        key_to_find="handle-photo-sampler",
                        model=model,
                        outputs=[],
                    )
                    specimen_type_val = utility.find_property_with_key(
                        object=obs_with_all,
                        field_name=key,
                        key_to_find="handle-specimen-type-fields",
                        model=model,
                        outputs=[],
                    )

                    # if "handle-barcode" in obs_with_all[key]:
                    for barcode in barcode_val:
                        b_object = utility.create_object_to_process(
                            object=dict(barcode)
                        )
                        if not b_object["values"]:
                            continue
                        if b_object["field_name"] in processed_keys:
                            continue

                        processed_keys.append(b_object["field_name"])
                        # special condition to handle barcode
                        result = self.create_barcode_ttl(
                            graph=graph,
                            plot_ttl=plot_ttl if plot_ttl else None,
                            feature_type_ttls=feature_type_ttls,
                            schema_spatial=schema_spatial,
                            barcode_data=dict(b_object),
                            procedure=procedure_ttl,
                            result_time=survey_date["raw"],
                            vouchers=voucher_val,
                            barcode_attributes=barcode_attribute_val,
                            barcode_observations=barcode_observations_val,
                            specimen_type_fields=specimen_type_val,
                            protocol=protocol,
                            feature_group_value=feature_group_value,
                        )
                        graph = result["graph"]
                        barcode_ttl = result["barcode_ttls"]

                    for photo_sampler in photo_sampler_val:
                        ps_object = utility.create_object_to_process(
                            object=dict(photo_sampler)
                        )
                        if not ps_object["values"]:
                            continue
                        if ps_object["field_name"] in processed_keys:
                            continue

                        processed_keys.append(ps_object["field_name"])
                        object_to_process = dict(ps_object["values"])

                        photo_samplers = dict(object_to_process)
                        if not isinstance(photo_samplers, list):
                            photo_samplers = [photo_samplers]

                        for ps in photo_samplers:
                            # special condition to handle image
                            graph = self.create_photo_sampler_ttl(
                                graph=graph,
                                plot_ttl=plot_ttl,
                                feature_sample_ttl=(
                                    barcode_ttl[0] if barcode_ttl else None
                                ),
                                schema_spatial=schema_spatial,
                                photo_sampler=ps,
                                procedure=procedure_ttl,
                                result_time=survey_date["raw"],
                                photo_sampler_name=key,
                                sample_type=SampleType.IMAGE,
                            )

                    for image in image_val:
                        if not image["value"]:
                            continue
                        if image["field_name"] in processed_keys:
                            continue
                        processed_keys.append(image["field_name"])
                        object_to_process = image["value"] if image["value"] else {
                        }

                        images = object_to_process
                        if not isinstance(images, List):
                            images = [images]

                        for image in images:
                            # special condition to handle image
                            graph = self.create_image_ttl(
                                graph=graph,
                                plot_ttl=plot_ttl,
                                feature_ttls=feature_type_ttls,
                                schema_spatial=schema_spatial,
                                data=obs_with_all[key],
                                protocol=protocol,
                                image=image,
                                procedure=procedure_ttl,
                                result_time=survey_date["raw"],
                                image_name=key,
                                specimen_type_fields=specimen_type_val,
                                feature_type_ttls=feature_type_ttls,
                                sample_type=SampleType.IMAGE,
                                feature_group_value=feature_group_value,
                            )

                # handle quadrate
                if quadrat_val:
                    result = self.create_quadrat_ttl(
                        graph=graph,
                        schema_spatial=schema_spatial,
                        procedure=procedure_ttl,
                        protocol_name=protocol_name,
                        result_time=survey_date["raw"],
                        protocol=protocol,
                        plot_ttl=plot_ttl if plot_ttl else None,
                        values=quadrat_val,
                        observer_ttls=observer_ttls,
                        has_phenomenon_time_ttl=phenomenon_time,
                        relational_attributes=relational_attributes,
                        feature_group_value=feature_group_value,
                        observer_details=observer_details,
                    )
                    graph = result["graph"]

                if plot_establishment_val:
                    # special condition to handle photo position establishment
                    result = self.create_photo_position_ttl(
                        graph=graph,
                        plot_ttl=plot_ttl,
                        procedure=procedure_ttl,
                        result_time=survey_date["raw"],
                        plot_establishments=plot_establishment_val,
                        photo_position_establishments=photo_position_establishment_val,
                    )
                    graph = result["graph"]

        for feature_type, ttl in feature_type_ttls.items():
            if not ttl["create_obs_collections"]:
                continue
            obs_collection_uri = (
                f"{record_ttl.id}/observation-collection/{feature_type}"
            )
            label = f"Observation collection of {
                feature_type.replace("-", " ")}"

            members = []
            # ObservationCollection requires at least 1 member
            if len(obs_members[feature_type]) == 0:
                continue
            for member in obs_members[feature_type]:
                members.append(member["member_uri"])
            f_attrs = []
            if feature_type in feature_type_attributes:
                f_attrs = feature_type_attributes[feature_type]
            # create observation collection
            observation_collection = fields.ObservationCollection(
                id=obs_collection_uri,
                identifier=survey_data["id"],
                label=label,
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                schema_spatial=schema_spatial,
                used_procedure=procedure_ttl,
                has_phenomenon_time=feature_type_phenomenon_times[feature_type],
                has_site_visit=site_visit_ttl,
                # has_transect=transect_ttl,
                has_vantage_point=vantage_point_ttl,
                has_member=members,
                has_feature_of_interest=ttl["object"],
                has_attribute=f_attrs,
                result_date_time=phenomenon_time,
                is_observed_by=observer_ttls,
            )
            graph = extensions.graph_service.add_data(
                data=observation_collection.dict(by_alias=True)
            )
        return graph

    def create_basal_area_ttl(
        self,
        graph: Graph,
        schema_spatial: dict,
        names_map: dict,
        procedure: dict,
        protocol_name: str,
        result_time: str,
        phenomenon_time_ttl: dict,
        protocol: dict,
        plot_ttl: dict,
        basal_area: list,
        tree_obs: list,
        relational_attributes: dict,
        feature_group_value: str
    ):
        site_ttl, site_visit_ttl = None, None
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]
        record_ttl = plot_ttl["rt"]

        basal_area_factor = basal_area[0]
        baf = basal_area_factor["value"]["label"]
        in_trees = None
        borderline_tree = None
        total_species = 0
        for v in tree_obs:
            if v["field_name"] == "in_tree":
                in_trees = v["value"]["value"]
            if v["field_name"] == "borderline_tree":
                borderline_tree = v["value"]["value"]
            if v["value"]["nrm_label"] == "field species name":
                total_species = total_species + 1
        # print(baf)
        basal_area = ((in_trees*1) + (borderline_tree*0.5)) * float(baf)
        # print(f"in_trees: {in_trees}, borderline_tree: {borderline_tree}, total species: {total_species}, basal area factor: {baf}")
        # print(f"(({in_trees}*1) + ({borderline_tree}*0.5)) * {float(baf)} = {basal_area}")
        extra_fields = utility.load_json_file(
            file_name=f"outputs/{protocol_name}/extra_fields.json", include_root_dir=False
        )
        basal_concept = extra_fields["basal_area"]
        basal_concept["value"] = basal_area

        # observation
        f_name = basal_concept["feature_type"][0]["value"].replace(" ", "-")
        f_name = utility.feature_type_prefix(
            name=f_name, position=None, id=result_time)
        feature_type_ttl = self.create_feature_of_interest_ttl(
            type=f_name,
            value=basal_concept["feature_type"][0],
            protocol=protocol,
            record_ttl=record_ttl,
            schema_spatial=schema_spatial,
            phenomenon_time=phenomenon_time_ttl,
            # has_attributes=foi_attrs,
            site_ttl=site_ttl if site_ttl else None,
            # transect_ttl=transect_ttl,
        )

        unit = extensions.protocol_service.get_field_unit(
            nrm_field=basal_concept["nrm_label"]
        )
        value = self.create_attribute_ttl(
            object=dict(basal_concept),
            label=basal_concept["nrm_label"],
            field_type=FieldType.PROPERTY,
            dataset_ttl=record_ttl,
            field=None,
            unit=unit
        )
        uuid = utility.generate_uuid()
        member_uri = (f"{record_ttl.id}/observation/basal-area/{uuid}")
        has_result, has_simple_result, has_simple_result_iri = (
            None,
            None,
            None,
        )
        if value:
            has_result = value["has_result"]
            has_simple_result = value["has_simple_value"]
            has_simple_result_iri = value["has_simple_value_iri"]

        has_attributes = []
        relational_uri = basal_concept["property_uri"]
        if feature_group_value:
            relational_uri = relational_uri + feature_group_value
        if relational_uri in relational_attributes:
            has_attributes = relational_attributes[relational_uri]

        observation_ttl = fields.Observation(
            id=member_uri,
            label=basal_concept["nrm_label"],
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            schema_spatial=schema_spatial,
            has_feature_of_interest=feature_type_ttl["object"],
            has_result=has_result,
            has_site_visit=site_visit_ttl,
            has_simple_result=has_simple_result,
            has_simple_result_iri=has_simple_result_iri,
            has_attribute=has_attributes,
            observed_property=basal_concept["property_uri"],
            has_phenomenon_time=phenomenon_time_ttl,
            used_procedure=procedure,
            result_date_time=result_time,
        )
        graph = extensions.graph_service.add_data(
            data=observation_ttl.dict(by_alias=True)
        )

        return {"graph": graph}

    def create_boolean_irs(
        self,
        graph: Graph,
        schema_spatial: dict,
        names_map: dict,
        procedure: dict,
        protocol_name: str,
        has_feature_of_interest: dict,
        result_time: str,
        protocol: dict,
        plot_ttl: dict,
        tree_bool_iris: list,
        phenomenon_time: dict,
        relational_attributes: dict,
        feature_group_value: str
    ):
        site_ttl, site_visit_ttl = None, None
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]
        record_ttl = plot_ttl["rt"]

        values = {}
        members = []
        flags = {}
        for tbi in tree_bool_iris:
            property_uri = tbi["value"]["property_uri"]
            if property_uri not in values:
                values[property_uri] = extensions.graph_service.get_categorical_value(
                    concept_uri=property_uri
                )

            field = tbi["field_name"]
            value = str(tbi["value"]["value"]).lower()
            flags[field] = value
            name = f"{field}_{value}"
            if name not in names_map:
                continue
            for symbol, uri in values[property_uri].items():
                if symbol not in names_map[name]:
                    continue
                obj = dict(tbi["value"])
                del obj["value"]
                obj["symbol"] = symbol
                obj["label"] = symbol
                obj["description"] = symbol
                obj["uri"] = uri
                id_label = obj["nrm_label"].replace("_", "-").replace(" ", "-")
                uuid = utility.generate_uuid()
                member_uri = f"{record_ttl.id}/observation/{id_label}/{uuid}"
                members.append(member_uri)
                value = dict(
                    self.create_attribute_ttl(
                        object=dict(obj),
                        label=obj["nrm_label"],
                        field_type=FieldType.PROPERTY,
                        dataset_ttl=record_ttl,
                        field=field,
                        mine_uri=False
                    )
                )
                has_result, has_simple_result, has_simple_result_iri = (
                    None,
                    None,
                    None,
                )
                if value:
                    has_result = value["has_result"]
                    has_simple_result = value["has_simple_value"]
                    has_simple_result_iri = value["has_simple_value_iri"]

                has_attributes = []
                relational_uri = property_uri
                if feature_group_value:
                    relational_uri = relational_uri + feature_group_value
                if relational_uri in relational_attributes:
                    has_attributes = relational_attributes[relational_uri]
                observation_ttl = fields.Observation(
                    id=member_uri,
                    label=obj["nrm_label"],
                    in_dataset=record_ttl,
                    is_part_of=record_ttl.is_part_of,
                    schema_spatial=schema_spatial,
                    has_feature_of_interest=has_feature_of_interest,
                    has_result=has_result,
                    has_site_visit=site_visit_ttl,
                    has_simple_result=has_simple_result,
                    has_simple_result_iri=has_simple_result_iri,
                    has_attribute=has_attributes,
                    observed_property=property_uri,
                    has_phenomenon_time=phenomenon_time,
                    used_procedure=procedure,
                    result_date_time=result_time,
                )
                graph = extensions.graph_service.add_data(
                    data=observation_ttl.dict(by_alias=True)
                )

        return {"graph": graph, "members": members, "flags": flags}

    def get_sample_properties(
        self,
        record_ttl: dict,
        protocol: dict,
        plot_data: dict,
        schema_spatial: dict,
        foi_attributes: list,
        replace_foi_values: list
    ):
        sampling_attributes = []
        position = None
        for v in replace_foi_values:
            if "handle-resolve-symbol-position" in protocol["custom-configs"]:
                # get position from the plot points in plot layout
                position = utility.get_symbol_position(
                    plot_points=plot_data["plot_layout"]["plot_points"],
                    symbol=v["value"]["symbol"]
                )
            attr_object = utility.create_object_to_process(
                object=dict(v)
            )
            object_to_process = attr_object["values"]
            if "attribute_uri" in object_to_process:
                attribute = self.create_attribute_ttl(
                    object=dict(object_to_process),
                    label=attr_object["label"],
                    field_type=FieldType.ATTRIBUTE,
                    dataset_ttl=record_ttl,
                    field=attr_object["field_name"],
                )
                if attribute:
                    sampling_attributes.append(attribute)

        if position:
            schema_spatial = self.create_schema_spacial(
                dataset_ttl=record_ttl,
                lat=position["lat"],
                lng=position["lng"],
                label=position["name"]["label"]
            )

        return {
            "schema_spatial": schema_spatial,
            "sample_attributes": [],
            "sampling_attributes": sampling_attributes
        }

    def create_tree_sample(
        self,
        record_ttl: dict,
        schema_spatial: dict,
        site_ttl: dict,
        site_visit_ttl: dict,
        feature_type: str,
        procedure: dict,
        result_time: str,
        sampler_ttl: dict = None,
        is_sample_of: dict = None,
        has_feature_of_interest: dict = None,
        sample_attributes: list = [],
        sampling_attributes: list = [],
        sample_name: str = "sample",
        sampling_name: str = "sampling",
    ):
        uuid = utility.generate_uuid()
        sample_uri = f"{record_ttl.id}/{sample_name}/{uuid}"
        sampling_uri = f"{record_ttl.id}/{sampling_name}/{uuid}"

        if not is_sample_of:
            is_sample_of = site_ttl
        if not has_feature_of_interest:
            has_feature_of_interest = site_ttl

        sample_ttl = fields.Sample(
            id=sample_uri,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            schema_spatial=schema_spatial,
            is_sample_of=is_sample_of,
            feature_type=feature_type,
            has_attribute=sample_attributes
        )
        sampling_ttl = fields.Sampling(
            id=sampling_uri,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            schema_spatial=schema_spatial,
            has_feature_of_interest=has_feature_of_interest,
            used_procedure=procedure,
            has_site_visit=site_visit_ttl,
            result_date_time=result_time,
            has_result=sample_ttl,
            made_by_sampler=sampler_ttl,
            has_attribute=sampling_attributes,
        )
        return {"sample_ttl": sample_ttl, "sampling_ttl": sampling_ttl}

    def create_sample_obs_collections_ttl(
        self,
        graph: Graph,
        schema_spatial: dict,
        names_map: dict,
        procedure: dict,
        protocol_name: str,
        result_time: str,
        protocol: dict,
        plot_ttl: dict,
        values: list,
        phenomenon_time: dict,
        relational_attributes: dict,
        feature_group_value: str,
        vantage_point: dict = None,
        outlook_zones: dict = None,
        set_id_transect_ttl: dict = None,
        near_by_transect_ttl: dict = None,
        sampler: dict = None,
        track_logs_ttls: list = [],
        observer_details: dict = {},
        observer_ttls: list = []

    ):
        
        flask.current_app.logger.info(f"Creating sample obs collections")
        site_ttl, site_visit_ttl = None, None
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]
        record_ttl = plot_ttl["rt"]

        uuid = utility.generate_uuid()
        feature_types_samplings = utility.has_custom_config(
            protocol=protocol, key="feature-types-to-sampling"
        )
        if not feature_types_samplings:
            feature_types_samplings = []

        feature_types_samples = utility.has_custom_config(
            protocol=protocol, key="feature-types-to-sample"
        )
        if not feature_types_samples:
            feature_types_samples = []

        all_values = []
        # stem values based on multi_stemmed flag
        for v in values:
            value = dict(v["value"])
            if not value["handle-sample-obs-collection"]:
                continue
            if "value" not in value:
                all_values.append(value)
                continue
            if isinstance(value["value"], list):
                all_values = all_values + value["value"]
                continue
        count = 1
        for data in all_values:
            if not feature_group_value:
                group_key_value = utility.has_custom_config(
                    protocol=protocol, key="feature-types-group-key"
                )
                if group_key_value:
                    if group_key_value in data:
                        feature_group_value = data[group_key_value]
                        if isinstance(feature_group_value, dict):
                            if "value" in feature_group_value:
                                feature_group_value = str(
                                    feature_group_value["value"])
                if feature_group_value:
                    feature_group_value = str(feature_group_value)
            relational_attributes = self.create_attributes_ttl(
                values=dict(data),
                names_map=names_map,
                protocol=protocol,
                record_ttl=record_ttl,
                schema_spatial=schema_spatial,
                feature_group_value=feature_group_value,
                protocol_name=protocol_name,
                phenomenon_time=phenomenon_time,
                procedure=procedure,
                result_time=result_time,
                ignore_custom_flags=["handle-sample-obs-collection-attr"]
            )
            result = self.create_members_ttl(
                graph=graph,
                values=dict(data),
                record_ttl=record_ttl,
                schema_spatial=schema_spatial,
                has_feature_of_interest=None,
                site_visit_ttl=site_visit_ttl,
                feature_group_value=feature_group_value,
                phenomenon_time=phenomenon_time,
                procedure=procedure,
                names_map=names_map,
                protocol=protocol,
                result_time=result_time,
                relational_attributes=relational_attributes,
                ignore_custom_flags=["handle-sample-obs-collection-obs"]
            )
            has_site = site_ttl
            is_sample_of = site_ttl
            if set_id_transect_ttl:
                has_site = set_id_transect_ttl
                is_sample_of = None
            is_observed_by = observer_ttls
            for k in names_map["personnel_name"]:
                if k not in data:
                    continue
                if not data[k]:
                    continue
                if data[k] not in observer_details:
                    continue
                is_observed_by = observer_details[data[k]]

            for f_name, obs in result["obs_ttls"].items():
                if vantage_point:
                    is_sample_of = vantage_point
                outlook_handler = utility.has_custom_config(
                    protocol=protocol, key="handle-outlook-zone"
                )
                if outlook_handler and outlook_zones and feature_group_value:
                    is_sample_of = outlook_zones[feature_group_value]
                if near_by_transect_ttl:
                    is_sample_of = near_by_transect_ttl

                name = f_name.replace(" ", "-")
                feature_ttl = None

                temp_feature_ttl = self.create_feature_of_interest_ttl(
                    type=f_name,
                    value=result["feature_types"][f_name],
                    protocol=protocol,
                    record_ttl=record_ttl,
                    schema_spatial=schema_spatial,
                    phenomenon_time=phenomenon_time,
                    site_ttl=site_ttl,
                    transect_ttl=set_id_transect_ttl,
                    has_attributes=[],
                )
                feature_ttl = temp_feature_ttl["object"]
                if f_name in feature_types_samples:
                    sample_title = f"{name}-sample"
                    sampling_title = f"{name}-sampling"
                    sample_uri = f"{
                        record_ttl.id}/{uuid}/{count}/{sample_title}"
                    sampling_uri = f"{
                        record_ttl.id}/{uuid}/{count}/{sampling_title}"
                    feature_type = result["feature_types"][f_name]["uri"]

                    feature_ttl = fields.Sample(
                        id=sample_uri,
                        in_dataset=record_ttl,
                        is_part_of=record_ttl.is_part_of,
                        schema_spatial=schema_spatial,
                        is_sample_of=is_sample_of,
                        feature_type=feature_type,
                        has_site=has_site
                    )
                graph = extensions.graph_service.add_data(
                    data=feature_ttl.dict(by_alias=True)
                )
                if f_name in feature_types_samplings:
                    sampling_ttl = fields.Sampling(
                        id=sampling_uri,
                        in_dataset=record_ttl,
                        is_part_of=record_ttl.is_part_of,
                        schema_spatial=schema_spatial,
                        has_feature_of_interest=is_sample_of.id if near_by_transect_ttl else is_sample_of,
                        used_procedure=procedure,
                        result_date_time=result_time,
                        has_result=feature_ttl,
                        made_by_sampler=sampler,
                        geometries=track_logs_ttls
                    )
                    graph = extensions.graph_service.add_data(
                        data=sampling_ttl.dict(by_alias=True)
                    )

                members = []
                for o in obs:
                    members.append(o.id)
                    o.has_feature_of_interest = feature_ttl
                    graph = extensions.graph_service.add_data(
                        data=o.dict(by_alias=True)
                    )
                obs_collection_uri = (
                    f"{record_ttl.id}/observation-collection/{name}/{count}/{uuid}")
                label = f"Observation collection of {f_name} {count}-{uuid}"

                observation_collection = fields.ObservationCollection(
                    id=obs_collection_uri,
                    identifier=uuid,
                    label=label,
                    in_dataset=record_ttl,
                    is_part_of=record_ttl.is_part_of,
                    schema_spatial=schema_spatial,
                    used_procedure=procedure,
                    has_phenomenon_time=phenomenon_time,
                    has_site_visit=site_visit_ttl,
                    has_member=members,
                    has_feature_of_interest=feature_ttl,
                    result_date_time=result_time,
                    is_observed_by=is_observed_by
                )
                graph = extensions.graph_service.add_data(
                    data=observation_collection.dict(by_alias=True)
                )
            count = count + 1
            graph = result["graph"]

        return {"graph": graph}

    def create_tree_ttl(
        self,
        graph: Graph,
        schema_spatial: dict,
        names_map: dict,
        procedure: dict,
        protocol_name: str,
        result_time: str,
        protocol: dict,
        plot_ttl: dict,
        sample_trees: dict,
        sample_stems: dict,
        dbh_sampler: list,
        tree_bool_iris: list,
        phenomenon_time: dict,
        relational_attributes: dict,
        feature_group_value: str
    ):
        site_ttl, site_visit_ttl = None, None
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]
        record_ttl = plot_ttl["rt"]

        uuid = utility.generate_uuid()

        dbh_sampler_ttl = None
        tree_members = []
        if dbh_sampler:
            dbh_sampler = dbh_sampler[0]
            sampler_type = dbh_sampler["value"]["symbol"].lower()
            dbh_sampler_ttl = self.create_sampler_ttl(
                record_ttl=record_ttl,
                procedure=procedure,
                label=dbh_sampler["value"]["label"],
                type=sampler_type,
                sampler_uri="https://linked.data.gov.au/def/nrm/237b799a-a1f3-5a1e-b88a-69429d05523a"
            )

        bool_obs = {}
        result = self.create_tree_sample(
            record_ttl=record_ttl,
            schema_spatial=schema_spatial,
            site_ttl=site_ttl,
            site_visit_ttl=site_visit_ttl,
            procedure=procedure,
            result_time=result_time,
            sampler_ttl=dbh_sampler_ttl,
            feature_type="http://linked.data.gov.au/def/tern-cv/b311c0d3-4a1a-4932-a39c-f5cdc1afa611",
            sample_name="trees",
            sampling_name="dbh-measurement-for-trees"
        )
        tree_sample_ttl = result["sample_ttl"]
        tree_sampling_ttl = result["sampling_ttl"]
        graph = extensions.graph_service.add_data(
            data=tree_sampling_ttl.dict(by_alias=True)
        )

        tree_members = []

        relational_attributes = self.create_attributes_ttl(
            values=dict(sample_trees[0]["value"]),
            names_map=names_map,
            protocol=protocol,
            record_ttl=record_ttl,
            schema_spatial=schema_spatial,
            feature_group_value=feature_group_value,
            protocol_name=protocol_name,
            phenomenon_time=phenomenon_time,
            procedure=procedure,
            result_time=result_time,
            ignore_custom_flags=["handle-tree-attr"],
            attr_member_map={
                "reach_POM": "reach_DBH",
                "POM_50_CM_above_buttress": "DBH_50_CM_above_buttress"
            }
        )

        result = self.create_members_ttl(
            graph=graph,
            values=dict(sample_trees[0]["value"]),
            record_ttl=record_ttl,
            schema_spatial=schema_spatial,
            has_feature_of_interest=tree_sample_ttl,
            site_visit_ttl=site_visit_ttl,
            feature_group_value=feature_group_value,
            phenomenon_time=phenomenon_time,
            procedure=procedure,
            names_map=names_map,
            protocol=protocol,
            result_time=result_time,
            relational_attributes=relational_attributes,
            ignore_custom_flags=["handle-tree-obs"],
            member_attr_map={
                "reach_DBH": "reach_POM",
                "DBH_50_CM_above_buttress": "POM_50_CM_above_buttress"
            }
        )
        graph = result["graph"]
        tree_members = tree_members + result["members"]
        if tree_bool_iris:
            bool_obs = self.create_boolean_irs(
                graph=graph,
                schema_spatial=schema_spatial,
                procedure=procedure,
                protocol_name=protocol_name,
                names_map=names_map,
                result_time=feature_group_value,
                has_feature_of_interest=tree_sample_ttl,
                protocol=protocol,
                plot_ttl=plot_ttl if plot_ttl else None,
                tree_bool_iris=tree_bool_iris,
                phenomenon_time=phenomenon_time,
                relational_attributes=relational_attributes,
                feature_group_value=feature_group_value
            )
            graph = bool_obs["graph"]
            tree_members = tree_members + bool_obs["members"]

        obs_collection_uuid = utility.generate_uuid()
        obs_collection_uri = (
            f"{record_ttl.id}/observation-collection/tress/{obs_collection_uuid}")
        label = f"Observation collection of trees {obs_collection_uuid}"

        observation_collection = fields.ObservationCollection(
            id=obs_collection_uri,
            identifier=obs_collection_uuid,
            label=label,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            schema_spatial=schema_spatial,
            used_procedure=procedure,
            has_phenomenon_time=phenomenon_time,
            has_site_visit=site_visit_ttl,
            has_member=tree_members,
            has_feature_of_interest=tree_sample_ttl,
            result_date_time=result_time,
        )
        graph = extensions.graph_service.add_data(
            data=observation_collection.dict(by_alias=True)
        )

        # if stem exists
        if sample_stems:
            is_multi_stem = None
            if "multi_stemmed" in bool_obs["flags"]:
                is_multi_stem = bool_obs["flags"]["multi_stemmed"]

            result = self.create_tree_sample(
                record_ttl=record_ttl,
                schema_spatial=schema_spatial,
                site_ttl=site_ttl,
                site_visit_ttl=site_visit_ttl,
                is_sample_of=tree_sample_ttl,
                procedure=procedure,
                result_time=result_time,
                sampler_ttl=dbh_sampler_ttl,
                feature_type="http://linked.data.gov.au/def/tern-cv/b311c0d3-4a1a-4932-a39c-f5cdc1afa611",
                sample_name="stems",
                sampling_name="dbh-measurement-for-stems"
            )
            stem_sample_ttl = result["sample_ttl"]
            stem_sampling_ttl = result["sampling_ttl"]
            graph = extensions.graph_service.add_data(
                data=stem_sampling_ttl.dict(by_alias=True)
            )

            stem_values = []
            # stem values based on multi_stemmed flag
            for ss in sample_stems:
                if is_multi_stem:
                    if ss["field_name"] == "stem_1":
                        continue
                    stem_values = ss["value"]["value"]
                    continue

                if ss["field_name"] == "stem":
                    continue
                stem_values = [ss["value"]]

            # create attribute ans members
            for stem_value in stem_values:
                relational_attributes = self.create_attributes_ttl(
                    values=dict(stem_value),
                    names_map=names_map,
                    protocol=protocol,
                    record_ttl=record_ttl,
                    schema_spatial=schema_spatial,
                    feature_group_value=feature_group_value,
                    protocol_name=protocol_name,
                    phenomenon_time=phenomenon_time,
                    procedure=procedure,
                    result_time=result_time,
                    ignore_custom_flags=["handle-stem-attr"]
                )
                result = self.create_members_ttl(
                    graph=graph,
                    values=dict(stem_value),
                    record_ttl=record_ttl,
                    schema_spatial=schema_spatial,
                    has_feature_of_interest=stem_sample_ttl,
                    site_visit_ttl=site_visit_ttl,
                    feature_group_value=feature_group_value,
                    phenomenon_time=phenomenon_time,
                    procedure=procedure,
                    names_map=names_map,
                    protocol=protocol,
                    result_time=result_time,
                    relational_attributes=relational_attributes,
                    ignore_custom_flags=["handle-stem-obs"]
                )
                graph = result["graph"]

        return {"graph": graph}

    def create_attributes_ttl(
        self,
        values: dict,
        names_map: dict,
        protocol: dict,
        record_ttl: dict,
        schema_spatial: dict,
        feature_group_value: str,
        protocol_name: str,
        phenomenon_time: dict,
        procedure: dict,
        result_time: str,
        ignore_custom_flags: list = [],
        attr_member_map: dict = {},
        ignore_relational_attributes: bool = False
    ):
        attr_val = utility.find_property_with_key(
            object=values,
            field_name="",
            key_to_find="attribute_uri",
            model="",
            outputs=[],
        )
        relational_attributes = {}
        all_attributes = []
        for attr in attr_val:
            attr_object = utility.create_object_to_process(
                object=dict(attr)
            )
            if not attr_object["values"]:
                continue
            # if attr_object["field_name"] in processed_keys: continue
            if (
                attr_object["field_name"]
                in names_map["global_skip_attributes"]
            ):
                continue

            object_to_process = dict(attr_object["values"])

            if "attribute_uri" in object_to_process:
                custom_handlers = utility.get_custom_configs(
                    object=object_to_process,
                    field_name=attr_object["field_name"],
                    protocol=dict(protocol),
                    ignore=ignore_custom_flags
                )
                if custom_handlers:
                    continue

                attribute = self.create_attribute_ttl(
                    object=dict(object_to_process),
                    label=attr_object["label"],
                    field_type=FieldType.ATTRIBUTE,
                    dataset_ttl=record_ttl,
                    field=attr_object["field_name"],
                )

                if attribute:
                    all_attributes.append(attribute)
                    f_types = utility.is_linked_to_feature_type(
                        field=attr_object["field_name"],
                        protocol=dict(protocol),
                    )
                    relational_obs = self.get_relational_obs(
                        attribute_uri=object_to_process["attribute_uri"],
                        protocol=protocol,
                        protocol_name=protocol_name
                    )
                    for relational_ob in relational_obs:
                        relational_uri = relational_ob
                        # if relational fields provided
                        if attr_object["field_name"] in attr_member_map:
                            pre = attr_object["field_name"] + \
                                attr_member_map[attr_object["field_name"]]
                            relational_uri = relational_uri + pre

                        if feature_group_value:
                            relational_uri = relational_uri + feature_group_value

                        if relational_uri not in relational_attributes:
                            relational_attributes[relational_uri] = [
                            ]
                        relational_attributes[relational_uri].append(
                            attribute)

        if ignore_relational_attributes:
            return all_attributes

        return relational_attributes

    def create_members_ttl(
        self,
        graph: Graph,
        values: dict,
        names_map: dict,
        protocol: dict,
        record_ttl: dict,
        schema_spatial: dict,
        has_feature_of_interest: dict,
        feature_group_value: str,
        site_visit_ttl: dict,
        phenomenon_time: dict,
        procedure: dict,
        result_time: str,
        relational_attributes: dict = {},
        ignore_custom_flags: list = [],
        member_attr_map: dict = {},

    ):
        members = []
        obs_ttls = {}
        feature_types = {}
        ptr_val = utility.find_property_with_key(
            object=values,
            field_name="",
            key_to_find="property_uri",
            model="",
            outputs=[],
        )

        for ptr in ptr_val:
            ptr_object = utility.create_object_to_process(
                object=dict(ptr))
            if not ptr_object["values"]:
                continue
            if (
                ptr_object["field_name"]
                in names_map["global_skip_attributes"]
            ):
                continue

            object_to_process = ptr_object["values"]
            if "feature_type" not in object_to_process:
                continue
            if "property_uri" in object_to_process:
                custom_handlers = utility.get_custom_configs(
                    object=object_to_process,
                    field_name=ptr_object["field_name"],
                    protocol=dict(protocol),
                    ignore=ignore_custom_flags
                )
                if custom_handlers:
                    continue
                uuid = utility.generate_uuid()
                id_label = ptr_object["label"].replace(
                    "_", "-").replace(" ", "-")
                member_uri = f"{record_ttl.id}/observation/{id_label}/{uuid}"

                members.append(member_uri)

                value = dict(
                    self.create_attribute_ttl(
                        object=dict(object_to_process),
                        label=ptr_object["label"],
                        field_type=FieldType.PROPERTY,
                        dataset_ttl=record_ttl,
                        field=ptr_object["field_name"],
                    )
                )
                has_result, has_simple_result, has_simple_result_iri = (
                    None,
                    None,
                    None,
                )
                if value:
                    has_result = value["has_result"]
                    has_simple_result = value["has_simple_value"]
                    has_simple_result_iri = value[
                        "has_simple_value_iri"
                    ]
                has_attributes = []
                relational_uri = object_to_process["property_uri"]
                if ptr_object["field_name"] in member_attr_map:
                    pre = member_attr_map[ptr_object["field_name"]
                                          ] + ptr_object["field_name"]
                    relational_uri = relational_uri + pre
                if feature_group_value:
                    relational_uri = relational_uri + feature_group_value
                if relational_uri in relational_attributes:
                    has_attributes = relational_attributes[relational_uri]

                observation_ttl = fields.Observation(
                    id=member_uri,
                    label=ptr_object["label"],
                    in_dataset=record_ttl,
                    is_part_of=record_ttl.is_part_of,
                    schema_spatial=schema_spatial,
                    has_feature_of_interest=has_feature_of_interest,
                    has_result=has_result,
                    has_site_visit=site_visit_ttl,
                    has_simple_result=has_simple_result,
                    has_simple_result_iri=has_simple_result_iri,
                    has_attribute=has_attributes,
                    observed_property=object_to_process["property_uri"],
                    has_phenomenon_time=phenomenon_time,
                    used_procedure=procedure,
                    result_date_time=result_time,
                )
                print(object_to_process)
                feature_name = object_to_process["feature_type"][0]["value"]
                if feature_name not in obs_ttls:
                    obs_ttls[feature_name] = []
                    feature_types[feature_name] = object_to_process["feature_type"][0]
                obs_ttls[feature_name].append(observation_ttl)
                if has_feature_of_interest:
                    graph = extensions.graph_service.add_data(
                        data=observation_ttl.dict(by_alias=True)
                    )

        return {"graph": graph, "members": members, "obs_ttls": obs_ttls, "feature_types": feature_types}

    def create_multi_obs_collections_ttl(
        self,
        graph: Graph,
        schema_spatial: dict,
        procedure: dict,
        protocol_name: str,
        result_time: str,
        protocol: dict,
        plot_ttl: dict,
        values: list,
        names_map: dict,
        all_feature_type_ttls: dict,
        phenomenon_time: dict,
        feature_group_value: str,
        additional_multi_obs_members: list,
        all_feature_type_attributes: dict,
        all_feature_of_interests_attributes: dict,
        transect_ttl: dict,
        obs_id: str,
    ):
        import json
        flask.current_app.logger.info(f"Creating multi observation collections")
        feature_type_ttls = {}
        relational_attributes = {}
        feature_type_phenomenon_times = {}
        obs_members = {}
        feature_type_attributes = {}
        feature_of_interests_attributes = {}
        record_ttl = plot_ttl["rt"]
        site_ttl, site_visit_ttl = None, None
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]

        obs_collections_ids = []
        for v in values:
            if "value" not in v:
                continue
            if not v["value"]["handle-multi-obs-collection-types"]:
                continue
            components = v["value"]["value"]
            if not isinstance(components, list):
                components = [components]
            for c in components:
                id = c["id"]
                obs_collections_ids.append(id)
                processed_keys = []
                attr_val = utility.find_property_with_key(
                    object=dict(c),
                    field_name=None,
                    key_to_find="attribute_uri",
                    model=v["model"],
                    outputs=[],
                )
                ptr_val = utility.find_property_with_key(
                    object=dict(c),
                    field_name=None,
                    key_to_find="property_uri",
                    model=v["model"],
                    outputs=[],
                )
                if additional_multi_obs_members:
                    ptr_val = ptr_val + additional_multi_obs_members
                for attr in attr_val:
                    attr_object = utility.create_object_to_process(
                        object=dict(attr))
                    if not attr_object["field_name"]:
                        import json

                        flask.current_app.logger.error(
                            f"Failed to find attribute name, object: {
                                json.dumps(attr_object)}"
                        )
                    if not attr_object["values"]:
                        continue
                    if attr_object["field_name"] in names_map["global_skip_attributes"]:
                        continue

                    object_to_process = dict(attr_object["values"])

                    if "attribute_uri" in object_to_process:
                        attribute = self.create_attribute_ttl(
                            object=dict(object_to_process),
                            label=attr_object["label"],
                            field_type=FieldType.ATTRIBUTE,
                            dataset_ttl=record_ttl,
                            field=attr_object["field_name"],
                        )
                        if attribute:
                            relational_obs = self.get_relational_obs(
                                attribute_uri=object_to_process["attribute_uri"],
                                protocol=protocol,
                                protocol_name=protocol_name
                            )
                            for relational_ob in relational_obs:
                                if relational_ob not in relational_attributes:
                                    relational_attributes[relational_ob] = []
                                relational_attributes[relational_ob].append(
                                    attribute)

                            # processed_keys.append(attr_object["field_name"])
                            f_types = utility.is_linked_to_feature_type(
                                field=attr_object["field_name"],
                                protocol=dict(protocol),
                            )
                            foi_attr_types = utility.foi_attributes_types(
                                field=attr_object["field_name"],
                                protocol=dict(protocol),
                            )
                            for ft in f_types:
                                f_atttr_name = utility.feature_type_prefix(
                                    name=ft, position=schema_spatial
                                )
                                if "id-based-foi" in protocol:
                                    if protocol["id-based-foi"]:
                                        f_atttr_name = utility.feature_type_prefix(
                                            name=ft, position=None, id=str(obs_id)
                                        )
                                if feature_group_value:
                                    f_atttr_name = utility.feature_type_prefix(
                                        name=ft,
                                        position=None,
                                        id=str(feature_group_value),
                                    )
                                f_atttr_name = f"{f_atttr_name}-{id}"
                                if f_atttr_name not in feature_type_attributes:
                                    feature_type_attributes[f_atttr_name] = []
                                feature_type_attributes[f_atttr_name].append(
                                    attribute)

                            for ft in foi_attr_types:
                                f_atttr_name = utility.feature_type_prefix(
                                    name=ft, position=schema_spatial
                                )
                                if "id-based-foi" in protocol:
                                    if protocol["id-based-foi"]:
                                        f_atttr_name = utility.feature_type_prefix(
                                            name=ft, position=None, id=str(obs_id)
                                        )
                                if feature_group_value:
                                    f_atttr_name = utility.feature_type_prefix(
                                        name=ft,
                                        position=None,
                                        id=str(feature_group_value),
                                    )
                                if f_atttr_name not in feature_of_interests_attributes:
                                    feature_of_interests_attributes[f_atttr_name] = [
                                    ]
                                feature_of_interests_attributes[f_atttr_name].append(
                                    attribute
                                )

                for ptr in ptr_val:
                    ptr_object = utility.create_object_to_process(
                        object=dict(ptr))
                    if not ptr_object["values"]:
                        continue
                    if ptr_object["field_name"] in names_map["global_skip_attributes"]:
                        continue

                    object_to_process = ptr_object["values"]
                    custom_handlers = utility.get_custom_configs(
                        object=object_to_process,
                        field_name=ptr_object["field_name"],
                        protocol=dict(protocol),
                        ignore=[
                            "handle-additional-multi-obs-member",
                            "handle-create-multi-obs-collection"
                        ]
                    )
                    if custom_handlers:
                        continue
                    if "feature_type" not in object_to_process:
                        continue
                    if "property_uri" in object_to_process:
                        id_label = (
                            ptr_object["label"].replace(
                                "_", "-").replace(" ", "-")
                        )
                        if "feature_type" in object_to_process:
                            feature_index = utility.get_feature_type_index(
                                obs_with_all=dict(c),
                                feature_types=object_to_process["feature_type"],
                                protocol=dict(protocol),
                            )
                            feature_name = object_to_process["feature_type"][
                                feature_index
                            ]["value"]
                            type = feature_name.replace(" ", "-")
                            f_name = utility.feature_type_prefix(
                                name=feature_name, position=schema_spatial
                            )
                            if "id-based-foi" in protocol:
                                if protocol["id-based-foi"]:
                                    f_name = utility.feature_type_prefix(
                                        name=feature_name, position=None, id=str(obs_id)
                                    )
                            if feature_group_value:
                                f_name = utility.feature_type_prefix(
                                    name=feature_name,
                                    position=None,
                                    id=str(feature_group_value),
                                )
                            uuid = utility.generate_uuid()
                            member_uri = (
                                f"{record_ttl.id}/observation/{id_label}/{uuid}"
                            )
                            if f_name not in obs_members:
                                obs_members[f_name] = {}

                            if f"{id}" not in obs_members[f_name]:
                                obs_members[f_name][f"{id}"] = []
                            obs_members[f_name][f"{id}"].append(
                                {
                                    "member_uri": member_uri,
                                    "feature_type": object_to_process["feature_type"][
                                        feature_index
                                    ],
                                }
                            )
                            if f_name not in feature_type_ttls:
                                foi_attrs = []
                                if f_name in feature_of_interests_attributes:
                                    foi_attrs = feature_of_interests_attributes[f_name]
                                if f_name in all_feature_of_interests_attributes:
                                    foi_attrs = (
                                        foi_attrs
                                        + all_feature_of_interests_attributes[f_name]
                                    )
                                feature_type_ttls[f_name] = (
                                    self.create_feature_of_interest_ttl(
                                        type=f_name,
                                        value=object_to_process["feature_type"][
                                            feature_index
                                        ],
                                        protocol=protocol,
                                        record_ttl=record_ttl,
                                        schema_spatial=schema_spatial,
                                        phenomenon_time=phenomenon_time,
                                        site_ttl=site_ttl if site_ttl else None,
                                        transect_ttl=transect_ttl,
                                        has_attributes=foi_attrs,
                                    )
                                )
                                feature_type_phenomenon_times[f_name] = phenomenon_time
                            value = dict(
                                self.create_attribute_ttl(
                                    object=dict(object_to_process),
                                    label=ptr_object["label"],
                                    field_type=FieldType.PROPERTY,
                                    dataset_ttl=record_ttl,
                                    field=ptr_object["field_name"],
                                )
                            )
                            has_result, has_simple_result, has_simple_result_iri = (
                                None,
                                None,
                                None,
                            )
                            if value:
                                has_result = value["has_result"]
                                has_simple_result = value["has_simple_value"]
                                has_simple_result_iri = value["has_simple_value_iri"]
                            has_attributes = []
                            relational_uri = object_to_process["property_uri"]
                            if feature_group_value:
                                relational_uri = relational_uri + feature_group_value
                            if relational_uri in relational_attributes:
                                has_attributes = relational_attributes[relational_uri]
                            observation_ttl = fields.Observation(
                                id=member_uri,
                                label=ptr_object["label"],
                                in_dataset=record_ttl,
                                is_part_of=record_ttl.is_part_of,
                                schema_spatial=schema_spatial,
                                has_feature_of_interest=feature_type_ttls[f_name][
                                    "object"
                                ],
                                has_result=has_result,
                                has_site_visit=site_visit_ttl,
                                has_simple_result=has_simple_result,
                                has_simple_result_iri=has_simple_result_iri,
                                has_attribute=has_attributes,
                                observed_property=object_to_process["property_uri"],
                                has_phenomenon_time=phenomenon_time,
                                used_procedure=procedure,
                                result_date_time=result_time,
                            )
                            graph = extensions.graph_service.add_data(
                                data=observation_ttl.dict(by_alias=True)
                            )
                            processed_keys.append(ptr["field_name"])

        if not obs_members and additional_multi_obs_members:
            id = utility.generate_uuid()
            obs_collections_ids.append(id)
            processed_keys = []
            for ptr in additional_multi_obs_members:
                ptr_object = utility.create_object_to_process(object=dict(ptr))
                if not ptr_object["values"]:
                    continue
                if ptr_object["field_name"] in names_map["global_skip_attributes"]:
                    continue

                object_to_process = ptr_object["values"]
                custom_handlers = utility.get_custom_configs(
                    object=object_to_process,
                    field_name=ptr_object["field_name"],
                    protocol=dict(protocol),
                    ignore=["handle-additional-multi-obs-member"]
                )
                if custom_handlers:
                    continue
                if "feature_type" not in object_to_process:
                    continue
                if "property_uri" in object_to_process:
                    id_label = ptr_object["label"].replace(
                        "_", "-").replace(" ", "-")
                    if "feature_type" in object_to_process:
                        feature_index = utility.get_feature_type_index(
                            obs_with_all=dict(object_to_process),
                            feature_types=object_to_process["feature_type"],
                            protocol=dict(protocol),
                        )
                        feature_name = object_to_process["feature_type"][feature_index][
                            "value"
                        ]
                        type = feature_name.replace(" ", "-")
                        f_name = utility.feature_type_prefix(
                            name=feature_name, position=schema_spatial
                        )
                        if "id-based-foi" in protocol:
                            if protocol["id-based-foi"]:
                                f_name = utility.feature_type_prefix(
                                    name=feature_name, position=None, id=str(obs_id)
                                )
                        if feature_group_value:
                            f_name = utility.feature_type_prefix(
                                name=feature_name,
                                position=None,
                                id=str(feature_group_value),
                            )
                        uuid = utility.generate_uuid()
                        member_uri = f"{
                            record_ttl.id}/observation/{id_label}/{uuid}"
                        if f_name not in obs_members:
                            obs_members[f_name] = {}

                        if f"{id}" not in obs_members[f_name]:
                            obs_members[f_name][f"{id}"] = []
                        obs_members[f_name][f"{id}"].append(
                            {
                                "member_uri": member_uri,
                                "feature_type": object_to_process["feature_type"][
                                    feature_index
                                ],
                            }
                        )
                        if f_name not in feature_type_ttls:
                            foi_attrs = []
                            if f_name in feature_of_interests_attributes:
                                foi_attrs = feature_of_interests_attributes[f_name]
                            if f_name in all_feature_of_interests_attributes:
                                foi_attrs = (
                                    foi_attrs
                                    + all_feature_of_interests_attributes[f_name]
                                )
                            feature_type_ttls[f_name] = (
                                self.create_feature_of_interest_ttl(
                                    type=f_name,
                                    value=object_to_process["feature_type"][
                                        feature_index
                                    ],
                                    protocol=protocol,
                                    record_ttl=record_ttl,
                                    schema_spatial=schema_spatial,
                                    phenomenon_time=phenomenon_time,
                                    site_ttl=site_ttl if site_ttl else None,
                                    transect_ttl=transect_ttl,
                                    has_attributes=foi_attrs,
                                )
                            )
                            feature_type_phenomenon_times[f_name] = phenomenon_time
                        value = dict(
                            self.create_attribute_ttl(
                                object=dict(object_to_process),
                                label=ptr_object["label"],
                                field_type=FieldType.PROPERTY,
                                dataset_ttl=record_ttl,
                                field=ptr_object["field_name"],
                            )
                        )
                        has_result, has_simple_result, has_simple_result_iri = (
                            None,
                            None,
                            None,
                        )
                        if value:
                            has_result = value["has_result"]
                            has_simple_result = value["has_simple_value"]
                            has_simple_result_iri = value["has_simple_value_iri"]
                        has_attributes = []
                        relational_uri = object_to_process["property_uri"]
                        if feature_group_value:
                            relational_uri = relational_uri + feature_group_value
                        if relational_uri in relational_attributes:
                            has_attributes = relational_attributes[relational_uri]
                        observation_ttl = fields.Observation(
                            id=member_uri,
                            label=ptr_object["label"],
                            in_dataset=record_ttl,
                            is_part_of=record_ttl.is_part_of,
                            schema_spatial=schema_spatial,
                            has_feature_of_interest=feature_type_ttls[f_name]["object"],
                            has_result=has_result,
                            has_site_visit=site_visit_ttl,
                            has_simple_result=has_simple_result,
                            has_simple_result_iri=has_simple_result_iri,
                            has_attribute=has_attributes,
                            observed_property=object_to_process["property_uri"],
                            has_phenomenon_time=phenomenon_time,
                            used_procedure=procedure,
                            result_date_time=result_time,
                        )
                        graph = extensions.graph_service.add_data(
                            data=observation_ttl.dict(by_alias=True)
                        )
                        processed_keys.append(ptr["field_name"])

        for feature_type, ttl in feature_type_ttls.items():
            if not ttl["create_obs_collections"]:
                continue
            if feature_type not in all_feature_type_ttls:
                all_feature_type_ttls[feature_type] = ttl
                all_feature_type_ttls[feature_type]["create_obs_collections"] = False
            for index in obs_collections_ids:
                i = f"{index}"
                obs_collection_uri = (
                    f"{record_ttl.id}/observation-collection/{feature_type}-{index}"
                )
                label = f"Observation collection of {
                    feature_type.replace("-", " ")}-{index}"

                members = []
                # ObservationCollection requires at least 1 member
                if len(obs_members[feature_type][i]) == 0:
                    continue
                for member in obs_members[feature_type][i]:
                    members.append(member["member_uri"])
                attrs = []
                if feature_type in all_feature_type_attributes:
                    attrs = all_feature_type_attributes[feature_type]
                feature_type_attr_name = f"{feature_type}-{i}"
                if feature_type_attr_name in feature_type_attributes:
                    attrs = attrs + \
                        feature_type_attributes[feature_type_attr_name]
                # create observation collection
                observation_collection = fields.ObservationCollection(
                    id=obs_collection_uri,
                    identifier=utility.generate_uuid(),
                    label=label,
                    in_dataset=record_ttl,
                    is_part_of=record_ttl.is_part_of,
                    schema_spatial=schema_spatial,
                    used_procedure=procedure,
                    has_phenomenon_time=feature_type_phenomenon_times[feature_type],
                    has_site_visit=site_visit_ttl,
                    has_member=members,
                    has_feature_of_interest=ttl["object"],
                    # result_date_time=phenomenon_time,
                    has_attribute=attrs,
                )
                graph = extensions.graph_service.add_data(
                    data=observation_collection.dict(by_alias=True)
                )

        return {"graph": graph, "all_feature_type_ttls": all_feature_type_ttls}

    def create_multi_feature_types_ttl(
        self,
        graph: Graph,
        schema_spatial: dict,
        procedure: dict,
        protocol_name: str,
        result_time: str,
        protocol: dict,
        plot_ttl: dict,
        values: list,
        names_map: dict,
        all_feature_type_ttls: dict,
        phenomenon_time: dict,
        all_feature_type_attributes: dict,
        relational_attributes: dict,
        feature_group_value: str
    ):
        import json

        feature_type_ttls = {}
        feature_type_phenomenon_times = {}
        obs_members = {}
        record_ttl = plot_ttl["rt"]
        site_ttl, site_visit_ttl = None, None
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]
        for v in values:
            if "value" not in v:
                continue
            if not v["value"]["handle-multi-feature-types"]:
                continue
            components = v["value"]["value"]
            if not isinstance(components, list):
                components = [components]
            for c in components:
                id = c["id"]
                processed_keys = []
                ptr_val = utility.find_property_with_key(
                    object=dict(c),
                    field_name=None,
                    key_to_find="property_uri",
                    model=v["model"],
                    outputs=[],
                )
                for ptr in ptr_val:
                    ptr_object = utility.create_object_to_process(
                        object=dict(ptr))
                    if not ptr_object["values"]:
                        continue
                    if ptr_object["field_name"] in names_map["global_skip_attributes"]:
                        continue

                    object_to_process = ptr_object["values"]
                    if "feature_type" not in object_to_process:
                        continue
                    if "property_uri" in object_to_process:
                        id_label = (
                            ptr_object["label"].replace(
                                "_", "-").replace(" ", "-")
                        )
                        if "feature_type" in object_to_process:
                            feature_index = utility.get_feature_type_index(
                                obs_with_all=dict(c),
                                feature_types=object_to_process["feature_type"],
                                protocol=dict(protocol),
                            )
                            feature_name = object_to_process["feature_type"][
                                feature_index
                            ]["value"]
                            type = feature_name.replace(" ", "-")
                            f_name = utility.feature_type_prefix(
                                name=feature_name, position=None, id=id
                            )
                            uuid = utility.generate_uuid()
                            member_uri = (
                                f"{record_ttl.id}/observation/{id_label}/{uuid}"
                            )
                            if f_name not in obs_members:
                                obs_members[f_name] = []
                            obs_members[f_name].append(
                                {
                                    "member_uri": member_uri,
                                    "feature_type": object_to_process["feature_type"][
                                        feature_index
                                    ],
                                }
                            )
                            if f_name not in feature_type_ttls:
                                feature_type_ttls[f_name] = (
                                    self.create_feature_of_interest_ttl(
                                        type=f_name,
                                        value=object_to_process["feature_type"][
                                            feature_index
                                        ],
                                        protocol=protocol,
                                        record_ttl=record_ttl,
                                        schema_spatial=schema_spatial,
                                        phenomenon_time=phenomenon_time,
                                        site_ttl=site_ttl if site_ttl else None,
                                        # transect_ttl=transect_ttl,
                                    )
                                )
                                feature_type_phenomenon_times[f_name] = phenomenon_time
                            value = dict(
                                self.create_attribute_ttl(
                                    object=dict(object_to_process),
                                    label=ptr_object["label"],
                                    field_type=FieldType.PROPERTY,
                                    dataset_ttl=record_ttl,
                                    field=ptr_object["field_name"],
                                )
                            )
                            has_result, has_simple_result, has_simple_result_iri = (
                                None,
                                None,
                                None,
                            )
                            if value:
                                has_result = value["has_result"]
                                has_simple_result = value["has_simple_value"]
                                has_simple_result_iri = value["has_simple_value_iri"]

                            has_attributes = []
                            relational_uri = object_to_process["property_uri"]
                            if feature_group_value:
                                relational_uri = relational_uri + feature_group_value
                            if relational_uri in relational_attributes:
                                has_attributes = relational_attributes[relational_uri]

                            observation_ttl = fields.Observation(
                                id=member_uri,
                                label=ptr_object["label"],
                                in_dataset=record_ttl,
                                is_part_of=record_ttl.is_part_of,
                                schema_spatial=schema_spatial,
                                has_feature_of_interest=feature_type_ttls[f_name][
                                    "object"
                                ],
                                has_attribute=has_attributes,
                                has_result=has_result,
                                has_site_visit=site_visit_ttl,
                                has_simple_result=has_simple_result,
                                has_simple_result_iri=has_simple_result_iri,
                                observed_property=object_to_process["property_uri"],
                                has_phenomenon_time=phenomenon_time,
                                used_procedure=procedure,
                                result_date_time=result_time,
                            )
                            graph = extensions.graph_service.add_data(
                                data=observation_ttl.dict(by_alias=True)
                            )
                            processed_keys.append(ptr["field_name"])

        for feature_type, ttl in feature_type_ttls.items():
            if not ttl["create_obs_collections"]:
                continue
            if feature_type not in all_feature_type_ttls:
                all_feature_type_ttls[feature_type] = ttl
                all_feature_type_ttls[feature_type]["create_obs_collections"] = False

            obs_collection_uri = (
                f"{record_ttl.id}/observation-collection/{feature_type}"
            )
            label = f"Observation collection of {
                feature_type.replace("-", " ")}"

            members = []
            # ObservationCollection requires at least 1 member
            if len(obs_members[feature_type]) == 0:
                continue
            for member in obs_members[feature_type]:
                members.append(member["member_uri"])

            # create observation collection
            observation_collection = fields.ObservationCollection(
                id=obs_collection_uri,
                identifier=utility.generate_uuid(),
                label=label,
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                schema_spatial=schema_spatial,
                used_procedure=procedure,
                has_phenomenon_time=feature_type_phenomenon_times[feature_type],
                has_site_visit=site_visit_ttl,
                has_member=members,
                has_feature_of_interest=ttl["object"],
                # result_date_time=phenomenon_time,
            )
            graph = extensions.graph_service.add_data(
                data=observation_collection.dict(by_alias=True)
            )
        return {"graph": graph, "all_feature_type_ttls": all_feature_type_ttls}

    def create_observers_roles_ttl(
        self,
        graph: Graph,
        schema_spatial: dict,
        names_map: dict,
        plot_ttl: dict,
        procedure: dict,
        result_time: str,
        protocol: dict,
        values: list,
    ):
        flask.current_app.logger.info(f"Creating observers roles")
        record_ttl = plot_ttl["rt"]
        observer_ttls = []
        observer_details = {}

        # create multiple roles linked to the observation
        all_values = []
        # stem values based on multi_stemmed flag
        for v in values:
            value = dict(v["value"])
            if not value["handle-observer"]:
                continue
            if "value" not in value:
                all_values.append(value)
                continue
            if isinstance(value["value"], list):
                all_values = all_values + value["value"]
                continue
        
        all_observers = {}
        for data in all_values:
            obs_name = None
            for field, v in data.items():
                if field in names_map["personnel_name"]:
                    obs_name = v
                    if "value" in v:
                        obs_name = v["value"]

                    if obs_name not in all_observers:
                        all_observers[obs_name] = {}
                    continue

            if not obs_name:
                continue
            attr_val = utility.find_property_with_key(
                object=data,
                field_name=None,
                key_to_find="attribute_uri",
                outputs=[],
            )
            for attr in attr_val:
                if not attr["value"]: continue
                if "symbol" not in attr["value"]: continue
                if attr["value"]["symbol"] in all_observers[obs_name]: continue
                attr_object = utility.create_object_to_process(
                    object=dict(attr))
                if not attr_object["values"]:
                    continue
                if attr_object["field_name"] in names_map["global_skip_attributes"]:
                    continue
                object_to_process = dict(attr_object["values"])

                if "attribute_uri" in object_to_process:
                    attribute = self.create_attribute_ttl(
                        object=dict(object_to_process),
                        label=attr_object["label"],
                        field_type=FieldType.ATTRIBUTE,
                        dataset_ttl=record_ttl,
                        field=attr_object["field_name"],
                    )
                    if attribute:
                        all_observers[obs_name][attr["value"]["symbol"]] = attribute

        for name, properties in all_observers.items():
            observer_uri = f"{record_ttl.id}/observer/{utility.compute_md5_hash(value=name)}/{utility.generate_uuid()}"
            attrs = []
            for key, a in properties.items():
                attrs.append(a)

            observer_ttl = fields.Observer(
                id=observer_uri,
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                name=name,
                was_associated_with=procedure.id,
                has_attribute=attrs,
            )
            observer_ttls.append(observer_ttl)

            if name not in observer_details:
                observer_details[name] = []
            observer_details[name].append(observer_ttl)

            graph = extensions.graph_service.add_data(
                data=observer_ttl.dict(by_alias=True)
            )

        return {"graph": graph, "observer_ttls": observer_ttls, "observer_details": observer_details}

    def create_quadrat_ttl(
        self,
        graph: Graph,
        schema_spatial: dict,
        procedure: dict,
        protocol_name: str,
        has_phenomenon_time_ttl: dict,
        observer_ttls: list,
        result_time: str,
        protocol: dict,
        plot_ttl: dict,
        values: list,
        relational_attributes: dict,
        feature_group_value: str,
        observer_details: dict
    ):
        site_ttl, site_visit_ttl = None, None
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]

        names_map = utility.get_names_map_data()
        record_ttl = plot_ttl["rt"]

        for q in values:
            for quadrat in q["value"]["value"]:
                id = quadrat["id"]
                relational_attributes = self.create_attributes_ttl(
                    values=dict(quadrat),
                    names_map=names_map,
                    protocol=protocol,
                    record_ttl=record_ttl,
                    schema_spatial=schema_spatial,
                    feature_group_value=feature_group_value,
                    protocol_name=protocol_name,
                    phenomenon_time=has_phenomenon_time_ttl,
                    procedure=procedure,
                    result_time=result_time,
                    ignore_relational_attributes=True,
                    ignore_custom_flags=["handle-quadrat-attr"]
                )
                identifier = utility.generate_uuid()
                quadrat_uri = f"{record_ttl.id}/quadrat/{id}/{identifier}"

                quadrat_ttl = fields.Site(
                    id=quadrat_uri,
                    identifier=identifier,
                    in_dataset=record_ttl,
                    is_part_of=record_ttl.is_part_of,
                    schema_spatial=schema_spatial,
                    feature_type="http://linked.data.gov.au/def/tern-cv/4362c8f2-b3cc-4816-b5a2-fb7bb4c0cff5",
                    has_attribute=relational_attributes,
                )
                graph = extensions.graph_service.add_data(
                    data=quadrat_ttl.dict(by_alias=True)
                )
                result = self.create_members_ttl(
                    graph=graph,
                    values=dict(quadrat),
                    record_ttl=record_ttl,
                    schema_spatial=schema_spatial,
                    has_feature_of_interest=quadrat_ttl,
                    site_visit_ttl=site_visit_ttl,
                    feature_group_value=feature_group_value,
                    phenomenon_time=has_phenomenon_time_ttl,
                    procedure=procedure,
                    names_map=names_map,
                    protocol=protocol,
                    result_time=result_time,
                    relational_attributes=relational_attributes,
                    ignore_custom_flags=["handle-quadrat-obs"]
                )
                graph = result["graph"]

                sign_obs_values = utility.find_property_with_key(
                    object=dict(quadrat),
                    field_name=None,
                    key_to_find="handle-quadrat-sample-obs-collection",
                    model=None,
                    outputs=[],
                )
                for sv in sign_obs_values:
                    if not sv["value"]["value"]:
                        continue
                    for sign in sv["value"]["value"]:
                        if not sign:
                            continue
                        i = sign["id"]
                        is_observed_by = observer_ttls
                        for k in names_map["personnel_name"]:
                            if k not in sign:
                                continue
                            if not sign[k]:
                                continue
                            if sign[k] not in observer_details:
                                continue
                            is_observed_by = observer_details[sign[k]]

                        r = self.create_members_ttl(
                            graph=graph,
                            values=dict(sign),
                            record_ttl=record_ttl,
                            schema_spatial=schema_spatial,
                            has_feature_of_interest=None,
                            site_visit_ttl=site_visit_ttl,
                            feature_group_value=None,
                            phenomenon_time=has_phenomenon_time_ttl,
                            procedure=procedure,
                            names_map=names_map,
                            protocol=protocol,
                            result_time=result_time,
                            relational_attributes={},
                            ignore_custom_flags=[
                                "handle-quadrat-sample-obs-collection-obs"]
                        )
                        for f_name, obs in r["obs_ttls"].items():

                            uuid = utility.generate_uuid()
                            foi_title = f_name.replace(" ", "-")
                            # if f_name == ""
                            foi_uri = f"{record_ttl.id}/{foi_title}/{i}/{uuid}"
                            foi_ttl = fields.FeatureOfInterest(
                                id=foi_uri,
                                label=foi_title,
                                feature_type=r["feature_types"][f_name]["uri"],
                                has_site=quadrat_ttl.id,
                            )
                            graph = extensions.graph_service.add_data(
                                data=foi_ttl.dict(by_alias=True)
                            )
                            members = []
                            for o in obs:
                                o.has_feature_of_interest = foi_ttl
                                members.append(o.id)
                                graph = extensions.graph_service.add_data(
                                    data=o.dict(by_alias=True)
                                )

                            obs_collection_uuid = utility.generate_uuid()
                            obs_collection_uri = (
                                f"{record_ttl.id}/observation-collection/{foi_title}/{obs_collection_uuid}")
                            label = f"Observation collection of {
                                f_name} {obs_collection_uuid}"

                            observation_collection = fields.ObservationCollection(
                                id=obs_collection_uri,
                                identifier=obs_collection_uuid,
                                label=label,
                                in_dataset=record_ttl,
                                is_part_of=record_ttl.is_part_of,
                                schema_spatial=schema_spatial,
                                used_procedure=procedure,
                                has_phenomenon_time=has_phenomenon_time_ttl,
                                has_site_visit=site_visit_ttl,
                                has_member=members,
                                has_feature_of_interest=foi_ttl,
                                result_date_time=result_time,
                                is_observed_by=is_observed_by,
                            )
                            graph = extensions.graph_service.add_data(
                                data=observation_collection.dict(by_alias=True)
                            )

        return {"graph": graph}

    def create_equipment_ttl(
        self,
        graph: Graph,
        schema_spatial: dict,
        plot_ttl: dict,
        procedure: dict,
        result_time: str,
        protocol: dict,
        equipments: list,
    ):
        record_ttl = plot_ttl["rt"]
        equipment_sampler_uri = f"{
            record_ttl.id}/equipment-used/{utility.generate_uuid()}"
        attributes = []
        for attr in equipments:
            attr_object = utility.create_object_to_process(object=dict(attr))
            if not attr_object["values"]:
                continue

            object_to_process = attr_object["values"]
            if "handle-equipments-sampler" not in object_to_process:
                continue
            if "attribute_uri" not in object_to_process:
                continue

            attribute = self.create_attribute_ttl(
                object=dict(object_to_process),
                label=attr_object["label"],
                field_type=FieldType.ATTRIBUTE,
                dataset_ttl=record_ttl,
                field=attr_object["field_name"],
            )
            if attribute:
                attributes.append(attribute)

        equipment_sampler_ttl = fields.Sampler(
            id=equipment_sampler_uri,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            has_attribute=attributes,
            implements=procedure,
        )
        graph = extensions.graph_service.add_data(
            data=equipment_sampler_ttl.dict(by_alias=True)
        )
        return {"graph": graph, "equipment_sampler_ttl": equipment_sampler_ttl}

    def create_track_station_with_attributes_ttl(
        self,
        graph: Graph,
        plot_ttl: dict,
        schema_spatial: dict,
        procedure: dict,
        result_time: str,
        protocol: dict,
        data: dict,
        station_id: str,
        track_station_attributes: list,
        track_station_photo_attributes: list,
        track_station_habitat_attributes: list,
        track_station_habitat_photo_attributes: list,
    ):
        if not track_station_attributes:
            return {"graph": graph, "track_station_ttl": None}
        record_ttl = plot_ttl["rt"]
        track_station_uri = f"{
            record_ttl.id}/track-station/{station_id.lower()}"

        names_map = utility.get_names_map_data()
        attr_fields = names_map["track_station_attribute_concepts"]
        attrs = self.get_attributes_list(
            data=dict(data),
            dataset_ttl=record_ttl,
            key_to_find="handle-track-station-attribute",
            approved=attr_fields,
            existing_attributes=[],
            skip_fields=[],
            handle_photo=False,
        )
        track_station_ttl = fields.Site(
            id=track_station_uri,
            label=f"track station {station_id}",
            identifier=station_id,
            in_dataset=record_ttl,
            schema_spatial=schema_spatial,
            feature_type="http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4",
            has_attribute=attrs,
            used_procedure=procedure,
        )
        graph = extensions.graph_service.add_data(
            data=track_station_ttl.dict(by_alias=True)
        )

        for image in track_station_photo_attributes:
            image_object = utility.create_object_to_process(object=dict(image))
            object_to_process = image_object["values"]

            # special condition to handle image
            graph = self.create_image_ttl(
                graph=graph,
                plot_ttl=plot_ttl,
                feature_type_ttl=track_station_ttl,
                feature_ttls=[],
                schema_spatial=schema_spatial,
                protocol=protocol,
                image=image,
                data=data,
                procedure=procedure,
                result_time=result_time,
                image_name=object_to_process["name"],
                sample_type=SampleType.IMAGE,
                photo_sample_label="photos-for-track-stations",
                photo_sampling_label="taking-photos-for-track-stations",
            )

        if track_station_habitat_attributes:
            uuid = utility.generate_uuid()
            sample_uri = f"{record_ttl.id}/track-station-microhabitat/{uuid}"
            sampling_uri = f"{
                record_ttl.id}/search-track-station-microhabitat/{uuid}"
            habitat_ttl = self.create_sample_ttl(
                sample_uri=sample_uri,
                sample_label="track-station-microhabitat",
                feature_sample_ttl=track_station_ttl,
                record_ttl=record_ttl,
                sampling_uri=sampling_uri,
                # feature_type_uri=url,
                # sample_type=sample_type,
                # has_attribute=image_attributes,
            )

            graph = extensions.graph_service.add_data(
                data=habitat_ttl.dict(by_alias=True)
            )

            habitat_ttl_sampling_ttl = fields.Sampling(
                id=sampling_uri,
                label="search-track-station-microhabitat",
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                schema_spatial=schema_spatial,
                has_feature_of_interest=str(track_station_ttl.id),
                has_result=habitat_ttl,
                used_procedure=procedure,
                has_site_visit=site_visit_ttl,
                result_date_time=result_time,
            )

            graph = extensions.graph_service.add_data(
                data=habitat_ttl_sampling_ttl.dict(by_alias=True)
            )
            for image in track_station_habitat_photo_attributes:
                image_object = utility.create_object_to_process(
                    object=dict(image))
                object_to_process = image_object["values"]

                # special condition to handle image
                graph = self.create_image_ttl(
                    graph=graph,
                    plot_ttl=plot_ttl,
                    feature_type_ttl=track_station_ttl,
                    feature_ttls=[],
                    schema_spatial=schema_spatial,
                    protocol=protocol,
                    image=image,
                    data=data,
                    procedure=procedure,
                    result_time=result_time,
                    image_name=object_to_process["name"],
                    sample_type=SampleType.IMAGE,
                    photo_sample_label="track-station-microhabitat-photos",
                    photo_sampling_label="taking-photos-for-track-stations-microhabitat",
                )
        return {"graph": graph, "track_station_ttl": track_station_ttl}

    def create_vantage_point_with_attributes_ttl(
        self,
        graph: Graph,
        plot_ttl: dict,
        procedure: dict,
        schema_spatial: dict,
        result_time: str,
        protocol: dict,
        phenomenon_time: dict,
        data: dict,
        vantage_id: str,
        vantage_start: list,
        vantage_end: list,
        vantage_point_val: list,
        outlook_zones: list,
        outlook_photo_attrs: list = []
    ):
        flask.current_app.logger.info(f"Creating create vantage point with attributes")
        record_ttl = plot_ttl["rt"]
        names_map = utility.get_names_map_data()
        outlook_zone_ttls = {}
        has_outlook_zones = []
        if vantage_point_val:
            attr_fields = names_map["vantage_point_concepts"]
            attrs = self.get_attributes_list(
                data=dict(data),
                dataset_ttl=record_ttl,
                key_to_find="handle-vantage-point",
                approved=attr_fields,
                existing_attributes=[],
                skip_fields=["setup_ID"],
                handle_photo=False,
            )
            vantage_point_uri = f"{
                record_ttl.id}/vantage-point/{vantage_id.lower()}"
            schema_spatial = None
            if vantage_start:
                lat = vantage_start[0]["value"]["lat"]
                lng = vantage_start[0]["value"]["lng"]
                schema_spatial = self.create_schema_spacial(
                    dataset_ttl=record_ttl, lat=lat, lng=lng
                )

            vantage_point_ttl = fields.Site(
                id=vantage_point_uri,
                label="vantage point",
                identifier=vantage_id,
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                schema_spatial=schema_spatial,
                feature_type="http://linked.data.gov.au/def/tern-cv/6f3aedf7-4ab8-4ba5-82af-928ec9d51c7f",
                has_attribute=attrs,
            )
            if outlook_zones:
                outlook_zones = dict(outlook_zones[0])
                for values in outlook_zones["value"]["value"]:

                    if "outlook_zone_name" not in values:
                        continue
                    zone_name = values["outlook_zone_name"]["value"]
                    outlook_zone_uri = f"{
                        record_ttl.id}/{zone_name.replace(" ", "-")}/{vantage_id.lower()}"
                    outlook_zone_ttls[values["outlook_zone_name"]
                                      ["value"]] = outlook_zone_uri

                    all_attributes = []
                    all_attributes = self.create_attributes_ttl(
                        values=dict(values),
                        names_map=names_map,
                        protocol=protocol,
                        record_ttl=record_ttl,
                        schema_spatial=schema_spatial,
                        feature_group_value=None,
                        protocol_name=None,
                        phenomenon_time=phenomenon_time,
                        procedure=procedure,
                        result_time=result_time,
                        ignore_custom_flags=["handle-outlook-zone-attr"],
                        ignore_relational_attributes=True
                    )
                    if all_attributes:
                        photo_sample_samples = []
                        for photo in outlook_photo_attrs:
                            attr_object = utility.create_object_to_process(
                                object=dict(photo), handle_photo_attr=True)
                            photo_attr = self.create_photo_attr(
                                attr_object=attr_object,
                                record_ttl=record_ttl
                            )
                            if photo_attr:
                                photo_sample_samples.append(attr_object)
                        outlook_zone_ttl = fields.FeatureOfInterest(
                            id=outlook_zone_uri,
                            in_dataset=record_ttl,
                            schema_spatial=schema_spatial,
                            feature_type="http://linked.data.gov.au/def/tern-cv/2090cfd9-8b6b-497b-9512-497456a18b99",
                            has_attribute=all_attributes,
                        )
                        has_outlook_zones.append(outlook_zone_ttl)
                        result = self.create_members_ttl(
                            graph=graph,
                            values=dict(values),
                            record_ttl=record_ttl,
                            schema_spatial=schema_spatial,
                            has_feature_of_interest=None,
                            site_visit_ttl=None,
                            feature_group_value=None,
                            phenomenon_time=phenomenon_time,
                            procedure=procedure,
                            names_map=names_map,
                            protocol=protocol,
                            result_time=result_time,
                            ignore_custom_flags=["handle-outlook-zone-obs"],
                        )
                        fois = {}
                        for f_name, obs in result["obs_ttls"].items():
                            if f_name not in fois:
                                foi_title = f_name.replace(" ", "-")
                                foi_uri = f"{
                                    record_ttl.id}/{zone_name.replace(" ", "-")}/{vantage_id.lower()}/{foi_title}"
                                foi_ttl = fields.FeatureOfInterest(
                                    id=foi_uri,
                                    in_dataset=record_ttl,
                                    feature_type=result["feature_types"][f_name]["uri"],
                                    is_sample_of=outlook_zone_ttl,
                                )
                                fois[f_name] = foi_ttl
                                graph = extensions.graph_service.add_data(
                                    data=foi_ttl.dict(by_alias=True)
                                )

                                for p in photo_sample_samples:
                                    result = self.create_photo_sample_and_sampling(
                                        graph=graph,
                                        plot_ttl=plot_ttl,
                                        schema_spatial=schema_spatial,
                                        protocol=protocol,
                                        procedure=procedure,
                                        result_time=result_time,
                                        has_feature_of_interest=foi_ttl,
                                        image_hash=p["photo_hash"],
                                        image_url=p["photo_url"],
                                        nrm_label=utility.minify_label(value=p["label"]),
                                    )
                                    graph = result["graph"]
                            for o in obs:
                                o.has_feature_of_interest = fois[f_name]
                                graph = extensions.graph_service.add_data(
                                    data=o.dict(by_alias=True)
                                )

            vantage_point_ttl.has_outlook_zones = has_outlook_zones
            graph = extensions.graph_service.add_data(
                data=vantage_point_ttl.dict(by_alias=True)
            )
        return {
            "graph": graph,
            "outlook_zone_ttls": outlook_zone_ttls,
            "vantage_point_ttl": vantage_point_ttl,
        }

    def create_track_logs_ttl(
        self,
        graph: Graph,
        schema_spatial: dict,
        plot_ttl: dict,
        procedure: dict,
        result_time: str,
        protocol: dict,
        feature_type_ttls: dict,
        values: list, 
    ):
        record_ttl = plot_ttl["rt"]
        track_logs_ttls = []
        for v in values:
            for point in v["value"]["value"]:
                track_logs_ttls.append(self.create_schema_spacial(
                    dataset_ttl=record_ttl, lat=point["lat"], lng=point["lng"]
                ))
        return { "graph": graph, "track_logs_ttls": track_logs_ttls }

    def create_nearby_transect_ttl(
        self,
        graph: Graph,
        schema_spatial: dict,
        plot_ttl: dict,
        procedure: dict,
        result_time: str,
        protocol: dict,
        feature_type_ttls: dict,
        values: list,
        transect_positions: list
    ):
        record_ttl = plot_ttl["rt"]
        names_map = utility.get_names_map_data()
        start_point, end_point = None, None

        for position in transect_positions:
            if position["field_name"] in names_map["transect_start_location"]:
                start_point = self.create_schema_spacial(
                    dataset_ttl=record_ttl, lat=position["value"]["lat"], lng=position["value"]["lng"]
                )
            if position["field_name"] in names_map["transect_end_location"]:
                end_point = self.create_schema_spacial(
                    dataset_ttl=record_ttl, lat=position["value"]["lat"], lng=position["value"]["lng"]
                )
        transect_uri = f"{record_ttl.id}/nearby-transect/{utility.generate_uuid()}"
        print("transect_uri ")
        print(values)
        attributes = []
        for attr in values:
            attr_object = utility.create_object_to_process(
                object=dict(attr)
            )
            if not attr_object["values"]:
                continue
            object_to_process = dict(attr_object["values"])

            if "attribute_uri" in object_to_process:
                attribute = self.create_attribute_ttl(
                    object=dict(object_to_process),
                    label=attr_object["label"],
                    field_type=FieldType.ATTRIBUTE,
                    dataset_ttl=record_ttl,
                    field=attr_object["field_name"],
                )

                if attribute: attributes.append(attribute)

        transect_ttl = fields.Transect(
            id=transect_uri,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            start_point=start_point,
            end_point=end_point,
            has_attribute=attributes,
        )
        graph = extensions.graph_service.add_data(
            data=transect_ttl.dict(by_alias=True))
        
        return {"graph": graph, "near_by_transect_ttl": transect_ttl}

    def create_setup_id_based_transect_ttl(
        self,
        graph: Graph,
        schema_spatial: dict,
        plot_ttl: dict,
        procedure: dict,
        result_time: str,
        protocol: dict,
        phenomenon_time: dict,
        plot_data: dict,
        data: dict,
        transect_id: str,
        transect_val_start: list,
        transect_val_end: list,
        transect_attrs_photo: list,
    ):
        flask.current_app.logger.info(f"Creating setup id based transect")
        record_ttl = plot_ttl["rt"]
        names_map = utility.get_names_map_data()
        start, end = None, None
        site_ttl, site_visit_ttl = None, None
        # existing site data
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]

        if transect_val_start and transect_val_end:
            # we assume only one start and end point
            start = utility.create_object_to_process(object=transect_val_start[0])
            end = utility.create_object_to_process(object=transect_val_end[0])

        resolve_transect_point = utility.has_custom_config(
            protocol=protocol,
            key="resolve-transect-point"
        )
        if resolve_transect_point:
            r = utility.get_start_end_from_resolve_point(
                data=data,
                plot_data=plot_data,
                key_to_find="handle-setup-id-based-observations",
                resolved_fields=names_map["resolve-transect-point"]
            )
            start = { "values": r["start_position"] }
            end = { "values": r["end_position"] }

        if not start or not end:
            return {"graph": graph, "set_id_transect_ttl": None}
        
        transect_uri = f"{record_ttl.id}/transect/{transect_id.lower()}"

        
        attr_fields = names_map["setup_id_transect_attr_concepts"]
        attrs = self.get_attributes_list(
            data=dict(data),
            dataset_ttl=record_ttl,
            key_to_find="handle-setup-id-transect-attr",
            approved=attr_fields,
            existing_attributes=[],
            skip_fields=[],
            handle_photo=False,
        )

        photo_sample_samples = []
        for photo in transect_attrs_photo:
            attr_object = utility.create_object_to_process(
                object=dict(photo), handle_photo_attr=True)
            photo_attr = self.create_photo_attr(
                attr_object=attr_object,
                record_ttl=record_ttl
            )
            if photo_attr:
                photo_sample_samples.append(attr_object)

        start_point, end_point = None, None
        if start:
            lat = start["values"]["lat"]
            lng = start["values"]["lng"]
            start_point = self.create_schema_spacial(
                dataset_ttl=record_ttl, lat=lat, lng=lng
            )
        if end:
            lat = end["values"]["lat"]
            lng = end["values"]["lng"]
            end_point = self.create_schema_spacial(
                dataset_ttl=record_ttl, lat=lat, lng=lng
            )
        set_id_transect_ttl = fields.Transect(
            id=transect_uri,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            start_point=start_point,
            end_point=end_point,
            has_attribute=attrs,
            is_sample_of=site_ttl
        )
        graph = extensions.graph_service.add_data(
            data=set_id_transect_ttl.dict(by_alias=True))

        for p in photo_sample_samples:
            result = self.create_photo_sample_and_sampling(
                graph=graph,
                plot_ttl=plot_ttl,
                schema_spatial=schema_spatial,
                protocol=protocol,
                procedure=procedure,
                result_time=result_time,
                has_feature_of_interest=set_id_transect_ttl,
                image_hash=p["photo_hash"],
                image_url=p["photo_url"],
                nrm_label=utility.minify_label(value=p["label"]),
            )
            graph = result["graph"]

        result = self.create_members_ttl(
            graph=graph,
            values=dict(data),
            record_ttl=record_ttl,
            schema_spatial=schema_spatial,
            has_feature_of_interest=None,
            site_visit_ttl=None,
            feature_group_value=None,
            phenomenon_time=phenomenon_time,
            procedure=procedure,
            names_map=names_map,
            protocol=protocol,
            result_time=result_time,
            ignore_custom_flags=["handle-setup-id-transect-obs"],
        )
        fois = {}
        for f_name, obs in result["obs_ttls"].items():
            if f_name not in fois:
                foi_title = f_name.replace(" ", "-")
                foi_uri = f"{record_ttl.id}/{transect_id.lower()}/feature-of-interest/{foi_title}"
                foi_ttl = fields.FeatureOfInterest(
                    id=foi_uri,
                    in_dataset=record_ttl,
                    feature_type=result["feature_types"][f_name]["uri"],
                    has_site=set_id_transect_ttl,
                )
                fois[f_name] = foi_ttl
                graph = extensions.graph_service.add_data(
                    data=foi_ttl.dict(by_alias=True)
                )
            for o in obs:
                o.has_feature_of_interest = fois[f_name]
                graph = extensions.graph_service.add_data(
                    data=o.dict(by_alias=True)
                )

        return {"graph": graph, "set_id_transect_ttl": set_id_transect_ttl}

    def create_photo_attr(
        self,
        attr_object: dict,
        record_ttl: dict
    ):
        if not attr_object:
            return None
        object_to_process = attr_object["values"]
        object = {
            "value": object_to_process["hash"],
            "attribute_uri": object_to_process["attribute_uri"],
            "nrm_label": object_to_process["nrm_label"],
            "protocol": object_to_process["protocol"],
        }
        return self.create_attribute_ttl(
            object=dict(object),
            label=attr_object["label"],
            field_type=FieldType.ATTRIBUTE,
            dataset_ttl=record_ttl,
            field=attr_object["field_name"],
        )

    def create_photo_sample_and_sampling(
        self,
        graph: Graph,
        plot_ttl: dict,
        protocol: dict,
        schema_spatial: dict,
        procedure: dict,
        result_time: str,
        image_url: str,
        image_hash: str,
        has_feature_of_interest: dict = None,
        nrm_label: str = None
    ):
        site_ttl, site_visit_ttl = None, None
        # existing site data
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]

        record_ttl = plot_ttl["rt"]
        uuid = utility.generate_uuid()

        sample_label, sampling_label = utility.get_photo_sample_sampling_labels(
            protocol=protocol,
            nrm_label=nrm_label,
        )
        sample_uri = f"{record_ttl.id}/{sample_label}/{uuid}"
        sampling_uri = f"{record_ttl.id}/{sampling_label}/{uuid}"

        sample_ttl = fields.Sample(
            id=sample_uri,
            label=sample_label,
            identifier=image_hash,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            schema_spatial=schema_spatial,
            archived_at=image_url,
            # feature_type=feature_type_uri,
            # is_sample_of=feature_type_ttl,
            # has_attribute=image_attributes,
        )

        if not has_feature_of_interest:
            has_feature_of_interest = site_ttl

        sampling_ttl = fields.Sampling(
            id=sampling_uri,
            label=sampling_label,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            schema_spatial=schema_spatial,
            has_feature_of_interest=has_feature_of_interest,
            has_result=sample_ttl,
            used_procedure=procedure,
            has_site_visit=site_visit_ttl,
            result_date_time=result_time,
        )

        graph = extensions.graph_service.add_data(
            data=sampling_ttl.dict(by_alias=True))

        return {"graph": graph, "sampling_ttl": sampling_ttl}

    def create_transect_ttl(
        self,
        graph: Graph,
        plot_ttl: dict,
        feature_type_ttls: dict,
        schema_spatial: dict,
        transect_data: dict,
        transect_attributes_photos: list,
        procedure: dict,
        result_time: str,
        transects: list,
        plot_data: dict,
        protocol: dict,
        protocol_name: str
    ):
        site_ttl, site_visit_ttl = None, None
        resolve_transect_point = False
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]
            if "custom-configs" in protocol:
                if "resolve-transect-point" in protocol["custom-configs"]:
                    resolve_transect_point = protocol["custom-configs"][
                        "resolve-transect-point"
                    ]

        record_ttl = plot_ttl["rt"]
        transect_ttl = None
        existing = utility.load_json_file(
            file_name="outputs/temporary_concepts.json", include_root_dir=False
        )
        temp_values = utility.load_json_file(
            file_name=f"outputs/{protocol_name}/temporary_values.json", include_root_dir=False
        )

        if resolve_transect_point and site_ttl:
            start = dict(transect_data)
            if "symbol" in start:
                start = start["symbol"]
            plot_name = plot_data["plot_layout"]["plot_selection"]["plot_label"]
            if "value" in plot_name:
                plot_name = plot_name["value"]

            temp_value_prefix = f"{start}_{plot_name}"
            transect_uri = None
            if temp_value_prefix in temp_values:
                # flask.current_app.logger.debug("Using existing transect")
                transect_uri = temp_values[temp_value_prefix]["transect_uri"]

            if temp_value_prefix not in temp_values:
                flask.current_app.logger.debug("Creating new transect")
                orientation = plot_data["plot_layout"]["orientation"]
                end_direction = utility.get_end_direction_from_start(
                    start_obj=dict(transect_data), orientation=orientation
                )
                end = end_direction["end"]
                direction = end_direction["direction"]["symbol"]
                direction_attr = end_direction["direction"]
                position_before = f"{start}{end}"
                start_position = utility.get_symbol_position(
                    plot_points=plot_data["plot_layout"]["plot_points"], symbol=start
                )
                end_position = utility.get_symbol_position(
                    plot_points=plot_data["plot_layout"]["plot_points"], symbol=end
                )
                __s = start_position["name"]["symbol"]
                __e = end_position["name"]["symbol"]
                position_after = f"{__s}{__e}"
                flask.current_app.logger.debug(
                    f"transect start: {start} end: {end} orientation: {
                        orientation} direction: {direction}"
                )

                if position_before != position_after:
                    flask.current_app.logger.error(
                        f"Failed to get transect positions before: {
                            position_before}  after: {position_after}"
                    )

                start_point, end_point = None, None
                if start_position:
                    lat = start_position["lat"]
                    lng = start_position["lng"]
                    start_point = self.create_schema_spacial(
                        dataset_ttl=record_ttl, lat=lat, lng=lng, label=start
                    )
                if end_position:
                    lat = end_position["lat"]
                    lng = end_position["lng"]
                    end_point = self.create_schema_spacial(
                        dataset_ttl=record_ttl, lat=lat, lng=lng, label=end
                    )

                postfix = (
                    f"{start.lower()}{end.lower()}{start_point.as_wkt}{
                        end_point.as_wkt}"
                )
                transect_uuid = utility.compute_md5_hash(postfix)
                transect_postfix = f"{start.lower()}-{end.lower()}-{transect_uuid}"
                transect_uri = f"{record_ttl.id}/transect/{transect_postfix}"

                temp_values[temp_value_prefix] = {
                    "start": start,
                    "end": end,
                    "start_position": start_position,
                    "end_position": end_position,
                    "transect_uri": transect_uri
                }
                utility.store_file(
                    data=dict(temp_values),
                    protocol_name=protocol_name,
                    file_name="temporary_values",
                    file_type=FileType.JSON,
                )
            
            if "temporary-save-concepts" in protocol:
                if transect_uri in existing:
                    return {"graph": graph, "transect_ttl": fields.Transect(id=transect_uri)}

            flask.current_app.logger.debug(
                f"new transect start: {start} end: {end} orientation: {
                    orientation} transect_uri: {transect_uri}"
            )
            transect_ttl = fields.Transect(
                id=transect_uri,
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                start_point=start_point,
                end_point=end_point,
                is_sample_of=str(site_ttl),
                feature_type="http://linked.data.gov.au/def/tern-cv/de46fa49-d1c9-4bef-8462-d7ee5174e1e1",
            )
            graph = extensions.graph_service.add_data(
                data=transect_ttl.dict(by_alias=True)
            )

            for image in transect_attributes_photos:
                image_object = utility.create_object_to_process(
                    object=dict(image))
                object_to_process = image_object["values"]

                # special condition to handle image
                graph = self.create_image_ttl(
                    graph=graph,
                    plot_ttl=plot_ttl,
                    feature_type_ttl=transect_ttl,
                    feature_ttls=[],
                    schema_spatial=schema_spatial,
                    protocol=protocol,
                    image=image,
                    data=None,
                    procedure=procedure,
                    result_time=result_time,
                    image_name=object_to_process["name"],
                    sample_type=SampleType.IMAGE,
                )

            if "temporary-save-concepts" in protocol:
                existing[transect_uri] = True
                utility.store_file(
                    data=dict(existing),
                    protocol_name="",
                    file_name="temporary_concepts",
                    file_type=FileType.JSON,
                )
        return {"graph": graph, "transect_ttl": transect_ttl}

    def get_relational_obs(self, attribute_uri: str, protocol: dict, protocol_name: str):
        if "skip-adding-obs-attr" in protocol:
            if protocol["skip-adding-obs-attr"]:
                return []

        filename = f"outputs/{protocol_name}/relations.json"
        relations = utility.load_json_file(
            file_name=filename, include_root_dir=False)

        if not relations:
            return []
        if attribute_uri not in relations:
            return []

        return relations[attribute_uri]

    def create_photo_position_ttl(
        self,
        graph: Graph,
        plot_ttl: dict,
        procedure: dict,
        result_time: str,
        plot_establishments: list,
        photo_position_establishments: list,
    ):
        site_ttl, site_visit_ttl = None, None
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]
        record_ttl = plot_ttl["rt"]

        # plot establishment
        plot_establishment = plot_establishments[0]
        plot_establishment_object = utility.create_object_to_process(
            object=dict(plot_establishment)
        )
        plot_establishment_data = dict(plot_establishment_object["values"])

        schema_spatial = None
        lat = plot_establishment_data["lat"]
        lng = plot_establishment_data["lng"]
        schema_spatial = self.create_schema_spacial(
            dataset_ttl=record_ttl, lat=lat, lng=lng
        )
        plot_establishment_uri = (
            f"{record_ttl.id}/plot-centre-post-establishment/{utility.generate_uuid()}"
        )
        plot_establishment_ttl = fields.Sampling(
            id=plot_establishment_uri,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            schema_spatial=schema_spatial,
            has_feature_of_interest=site_ttl,
            used_procedure=procedure,
            has_site_visit=site_visit_ttl,
            result_date_time=result_time,
        )

        if not photo_position_establishments:
            graph = extensions.graph_service.add_data(
                data=plot_establishment_ttl.dict(by_alias=True)
            )
            return {"graph": graph}
        has_results = []
        for value in photo_position_establishments[0]["value"]["value"]:
            obj = None
            if "barcode_content" in value:
                obj = json.loads(value["barcode_content"]["value"])
            if not obj:
                continue

            symbol = obj["photopoint_position"]
            object = {
                "symbol": f"photopoint position- {symbol}",
                "label": f"photopoint position- {symbol}",
                "uri": None,
                "attribute_uri": value["barcode_content"]["attribute_uri"],
                "nrm_label": value["barcode_content"]["nrm_label"],
                "protocol": value["barcode_content"]["protocol"],
                "api_source": None,
            }
            attribute = self.create_attribute_ttl(
                object=dict(object),
                label=f"photopoint position- {symbol}",
                field_type=FieldType.ATTRIBUTE,
                dataset_ttl=record_ttl,
                field=None,
            )

            lat = obj["lat"]
            lng = obj["lng"]
            schema_spatial = self.create_schema_spacial(
                dataset_ttl=record_ttl, lat=lat, lng=lng
            )
            photo_position_uri = f"{
                record_ttl.id}/photopoints-position-{symbol}-establishment/{utility.generate_uuid()}"
            has_results.append(photo_position_uri)
            photo_position_ttl = fields.Sample(
                id=photo_position_uri,
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                schema_spatial=schema_spatial,
                is_sample_of=site_ttl,
                has_attribute=[attribute],
            )

            graph = extensions.graph_service.add_data(
                data=photo_position_ttl.dict(by_alias=True)
            )

        plot_establishment_ttl.has_result = has_results
        graph = extensions.graph_service.add_data(
            data=plot_establishment_ttl.dict(by_alias=True)
        )
        return {"graph": graph}

    def create_attribute_ttl(
        self,
        object: dict,
        label: str,
        field_type: str,
        field: str,
        dataset_ttl: dict,
        add_is_part_of: bool = True,
        id: str = "",
        unit: str = None,
        mine_uri: bool = True
    ):
        if "uri" in object:
            attr = self.create_lut(
                object=dict(object),
                label=label,
                field_type=field_type,
                dataset_ttl=dataset_ttl,
                add_is_part_of=add_is_part_of,
                id=id,
                mine_uri=mine_uri
            )
            if attr:
                return attr

            return self.create_non_lut(
                object=dict(object),
                label=label,
                field_type=field_type,
                value=object["label"],
                dataset_ttl=dataset_ttl,
                field=field,
                add_is_part_of=add_is_part_of,
                id=id,
                unit=unit
            )

        if "value" in object:
            return self.create_non_lut(
                object=dict(object),
                label=label,
                field_type=field_type,
                dataset_ttl=dataset_ttl,
                field=field,
                add_is_part_of=add_is_part_of,
                id=id,
                unit=unit
            )

        return None

    def get_spieces_material_sample_type(
        self,
        specimen_type_fields: list,
        feature_type_ttls: dict,
        schema_spatial: dict,
        plot_ttl: dict,
        protocol: dict,
        feature_group_value: str = None,
    ):
        site_ttl, site_visit_ttl = None, None
        # existing site data
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]
        record_ttl = plot_ttl["rt"]
        # type_default = BarcodeType.PLANT.value
        sample_label = f"specimen"
        sampling_label = f"vouchering"
        # default type
        feature_type_ttl = None
        if not feature_type_ttls:
            return None
        if not specimen_type_fields:
            feature_name = None
            if "specimen-feature-type" in protocol:
                feature_name = utility.feature_type_prefix(
                    name=protocol["specimen-feature-type"], position=schema_spatial
                )
                if feature_group_value:
                    feature_name = utility.feature_type_prefix(
                        name=protocol["specimen-feature-type"], position=None, id=feature_group_value
                    )
            if not feature_name:
                feature_name = list(feature_type_ttls.keys())
                feature_name = feature_name[0]

            feature_type_ttl = feature_type_ttls[feature_name]
            if "object" in feature_type_ttl:
                feature_type_ttl = feature_type_ttl["object"]

        # handling first one should be enough
        for type in specimen_type_fields:
            temp_object = {type["field_name"]: dict(type["value"])}
            feature_index = utility.get_feature_type_index(
                obs_with_all=dict(temp_object),
                feature_types=type["value"]["feature_type"],
                protocol=dict(protocol),
            )
            feature_name = utility.feature_type_prefix(
                name=type["value"]["feature_type"][feature_index]["value"],
                position=schema_spatial,
            )
            if feature_group_value:
                feature_name = utility.feature_type_prefix(
                    name=feature_name, position=None, id=feature_group_value
                )
            feature_type_ttl = feature_type_ttls[feature_name]
            if "object" in feature_type_ttl:
                feature_type_ttl = feature_type_ttl["object"]

        if BarcodeType.PLANT.value in feature_name:
            sample_label = f"{BarcodeType.PLANT.value}-specimen"
            sampling_label = f"{BarcodeType.PLANT.value}-vouchering"
        if BarcodeType.ANIMAL.value in feature_name:
            sample_label = f"{BarcodeType.ANIMAL.value}-specimen"
            sampling_label = "fauna-vouchering"

        return {
            "feature_type": feature_type_ttl,
            # "feature_type_uri": feature_of_interest_uri,
            "sample_label": sample_label,
            "sampling_label": sampling_label,
        }

    def create_replicates_ttl(
        self,
        graph: Graph,
        plot_ttl: dict,
        feature_type_ttls: dict,
        schema_spatial: dict,
        procedure: dict,
        result_time: str,
        values: list,
        protocol: dict,
        feature_group_value: str = None,
        obs_id: str = None,
    ):
        site_ttl, site_visit_ttl = None, None
        # existing site data
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]
        record_ttl = plot_ttl["rt"]

        # should be the first one
        replicates = values[0]
        if "value" in replicates:
            replicates = dict(replicates["value"])
        if "handle-replicate" not in replicates:
            return {"graph": graph}

        f_name = utility.feature_type_prefix(
            name=protocol["specimen-feature-type"],
            position=schema_spatial,
        )
        if "id-based-foi" in protocol:
            if protocol["id-based-foi"]:
                f_name = utility.feature_type_prefix(
                    name=protocol["specimen-feature-type"],
                    position=None,
                    id=obs_id,
                )
        if feature_group_value:
            f_name = utility.feature_type_prefix(
                name=protocol["specimen-feature-type"],
                position=None,
                id=str(feature_group_value),
            )
        feature_type_ttl = feature_type_ttls[f_name]
        sample_label = "plant-specimen"
        sampling_label = "plant-specimen-collection"

        if "replicate-sample-label" in protocol:
            sample_label = protocol["replicate-sample-label"]
        if "replicate-sampling-label" in protocol:
            sampling_label = protocol["replicate-sampling-label"]

        # barcode_sample_uri = f"{record_ttl.id}/{sample_label}/{barcode_value}"
        replicate_sampling_uri = f"{
            record_ttl.id}/{sampling_label}/{utility.generate_uuid()}"

        replicate_sampling_ttl = fields.Sampling(
            id=replicate_sampling_uri,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            schema_spatial=schema_spatial,
            has_feature_of_interest=feature_type_ttl["object"],
            # has_result=barcode_ttl,
            used_procedure=procedure,
            has_site_visit=site_visit_ttl,
            result_date_time=result_time,
        )
        has_results = []
        for r in replicates["value"]:
            attrs = []
            uuid = ""
            for field, property in r.items():
                if not isinstance(property, dict):
                    continue
                if "attribute_uri" not in property:
                    continue
                v = property["value"]
                uuid = f"{uuid}-{v}"
                attribute = self.create_attribute_ttl(
                    object=dict(property),
                    label=property["nrm_label"],
                    field_type=FieldType.ATTRIBUTE,
                    dataset_ttl=record_ttl,
                    field=field
                )
                if attribute:
                    attrs.append(attribute)
            if not attrs:
                continue
            replicate_sample_uri = f"{record_ttl.id}/{sample_label}/{uuid}"
            replicate_sample_ttl = fields.Sample(
                id=replicate_sample_uri,
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                schema_spatial=schema_spatial,
                is_sample_of=feature_type_ttl["object"],
                feature_type="http://linked.data.gov.au/def/tern-cv/2e122e23-881c-43fa-a921-a8745f016ceb",
                has_attribute=attrs,
            )
            graph = extensions.graph_service.add_data(
                data=replicate_sample_ttl.dict(by_alias=True)
            )
            has_results.append(replicate_sample_uri)
        replicate_sampling_ttl.has_result = has_results
        graph = extensions.graph_service.add_data(
            data=replicate_sampling_ttl.dict(by_alias=True)
        )

        return {"graph": graph}

    def create_barcode_ttl(
        self,
        graph: Graph,
        plot_ttl: dict,
        feature_type_ttls: dict,
        schema_spatial: dict,
        barcode_data: dict,
        procedure: dict,
        result_time: str,
        barcode_attributes: list,
        barcode_observations: list,
        specimen_type_fields: list,
        protocol: dict,
        vouchers: dict = None,
        feature_group_value: str = None,
    ):
        site_ttl, site_visit_ttl = None, None
        # existing site data
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]
        record_ttl = plot_ttl["rt"]
        specimen_type = self.get_spieces_material_sample_type(
            specimen_type_fields=specimen_type_fields,
            feature_type_ttls=feature_type_ttls,
            schema_spatial=schema_spatial,
            plot_ttl=plot_ttl,
            protocol=protocol,
            feature_group_value=feature_group_value,
        )
        if not specimen_type:
            return {"graph": graph, "barcode_ttls": None}
        feature_type_ttl = specimen_type["feature_type"]
        # feature_of_interest_uri = specimen_type["feature_type_uri"]
        sample_label = specimen_type["sample_label"]
        sampling_label = specimen_type["sampling_label"]

        if "specimen-sample-label" in protocol:
            sample_label = protocol["specimen-sample-label"]
        if "specimen-sampling-label" in protocol:
            sampling_label = protocol["specimen-sampling-label"]

        barcode_ttls = []
        barcode_value = barcode_data["values"]["value"].lower()
        barcode_sample_uri = f"{record_ttl.id}/{sample_label}/{barcode_value}"
        barcode_sampling_uri = (
            f"{record_ttl.id}/{sampling_label}/{utility.generate_uuid()}"
        )

        barcode_feature_type_uri = (
            "http://linked.data.gov.au/def/tern-cv/2e122e23-881c-43fa-a921-a8745f016ceb"
        )
        attributes = []
        attribute = self.create_non_lut(
            object=dict(barcode_data["values"]),
            label=barcode_data["label"],
            dataset_ttl=record_ttl,
            field_type=FieldType.ATTRIBUTE,
            field=barcode_data["field_name"],
        )
        attributes.append(attribute)

        for b in barcode_attributes:
            b_object = utility.create_object_to_process(object=dict(b))
            attr = self.create_attribute_ttl(
                object=dict(b_object["values"]),
                label=b_object["label"],
                field_type=FieldType.ATTRIBUTE,
                dataset_ttl=record_ttl,
                field=b_object["field_name"],
            )
            attributes.append(attr)

        phenomenon_uri = f"{
            record_ttl.id}/phenomenon-time/{utility.generate_uuid()}"
        has_phenomenon_time_ttl = fields.TimeInstant(
            id=phenomenon_uri,
            date_timestamp=result_time,
        )
        barcode_ttl = fields.MaterialSample(
            id=barcode_sample_uri,
            label=f"barcode-{barcode_value}",
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            material_sample_id=barcode_value,
            schema_spatial=schema_spatial,
            # is_result_of=barcode_sampling_uri,
            is_sample_of=str(feature_type_ttl.id),
            feature_type=barcode_feature_type_uri,
            has_attribute=attributes,
        )

        graph = extensions.graph_service.add_data(
            data=barcode_ttl.dict(by_alias=True))

        barcode_sampling_ttl = fields.Sampling(
            id=barcode_sampling_uri,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            schema_spatial=schema_spatial,
            has_feature_of_interest=feature_type_ttl,
            has_result=barcode_ttl,
            used_procedure=procedure,
            has_site_visit=site_visit_ttl,
            result_date_time=result_time,
        )

        graph = extensions.graph_service.add_data(
            data=barcode_sampling_ttl.dict(by_alias=True)
        )

        observations = []
        for obs in barcode_observations:
            obs_object = utility.create_object_to_process(object=dict(obs))
            label = obs_object["label"].replace(" ", "-")
            obs_uri = f"{
                record_ttl.id}/observation/{label}/{utility.generate_uuid()}"
            v = self.create_attribute_ttl(
                object=dict(obs_object["values"]),
                label=obs_object["label"],
                field_type=FieldType.PROPERTY,
                dataset_ttl=record_ttl,
                field=obs_object["field_name"],
            )
            has_result, has_simple_result, has_simple_result_iri = None, None, None
            if v:
                has_result = v["has_result"]
                has_simple_result = v["has_simple_value"]
                has_simple_result_iri = v["has_simple_value_iri"]
            observation_ttl = fields.Observation(
                id=obs_uri,
                label=obs_object["label"],
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                schema_spatial=schema_spatial,
                has_feature_of_interest=barcode_sample_uri,
                has_result=has_result,
                has_simple_result=has_simple_result,
                has_simple_result_iri=has_simple_result_iri,
                observed_property=obs["value"]["property_uri"],
                has_phenomenon_time=has_phenomenon_time_ttl,
                used_procedure=procedure,
                result_date_time=result_time,
            )
            graph = extensions.graph_service.add_data(
                data=observation_ttl.dict(by_alias=True)
            )

        # if material sample flag is on. e.g. plant-tissue
        if vouchers:
            voucher = dict(vouchers[0]["value"])

            voucher_sample_label = "plant-tissue-voucher"
            voucher_sampling_label = "plant-tissue-vouchering"

            if "voucher-sample-label" in protocol:
                voucher_sample_label = protocol["voucher-sample-label"]
            if "voucher-sampling-label" in protocol:
                voucher_sampling_label = protocol["voucher-sampling-label"]

            plant_tissue_voucher_uri = (
                f"{record_ttl.id}/{voucher_sample_label}/{utility.generate_uuid()}"
            )
            plant_tissue_vouchering_uri = (
                f"{record_ttl.id}/{voucher_sampling_label}/{utility.generate_uuid()}"
            )

            attributes = []
            if "attribute_uri" in voucher:
                attribute = self.create_non_lut(
                    object={
                        "value": voucher["value"],
                        "attribute_uri": voucher["attribute_uri"],
                    },
                    label="Plant tissue ID",
                    dataset_ttl=record_ttl,
                    field_type=FieldType.ATTRIBUTE,
                    field=None,
                )
                attributes.append(attribute)

            plant_tissue_voucher_ttl = fields.Sample(
                id=plant_tissue_voucher_uri,
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                schema_spatial=schema_spatial,
                material_sample_id=voucher["value"],
                is_sample_of=str(feature_type_ttl.id),
                feature_type=barcode_feature_type_uri,
                has_attribute=attributes,
            )
            graph = extensions.graph_service.add_data(
                data=plant_tissue_voucher_ttl.dict(by_alias=True)
            )
            plant_tissue_vouchering_ttl = fields.Sampling(
                id=plant_tissue_vouchering_uri,
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                schema_spatial=schema_spatial,
                has_feature_of_interest=barcode_ttl,
                has_result=plant_tissue_voucher_ttl,
                used_procedure=procedure,
                has_site_visit=site_visit_ttl,
                result_date_time=result_time,
            )

            graph = extensions.graph_service.add_data(
                data=plant_tissue_vouchering_ttl.dict(by_alias=True)
            )
        barcode_ttls.append(barcode_ttl)
        return {"graph": graph, "barcode_ttls": barcode_ttls}

    def create_feature_sample_ttl(
        self,
        graph: Graph,
        plot_ttl: dict,
        schema_spatial: dict,
        result_time: str,
        procedure: str,
        field_name: str = None,
    ):
        site_ttl, site_visit_ttl = None, None
        # existing site data
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]

        record_ttl = plot_ttl["rt"]

        feature_uri = f"{record_ttl.id}/{field_name}"
        feature_sampling_uri = feature_uri + "/sampling"
        feature_type_uri = (
            MONITOR_PREFIX
            + "non-created-feature-type/"
            + field_name
            + "/"
            + utility.generate_uuid()
        )

        feature_sample_ttl = fields.FeatureSample(
            id=feature_uri,
            label=field_name.replace("_", " "),
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            is_result_of=feature_sampling_uri,
            is_sample_of=site_ttl if site_ttl else None,
            feature_type=feature_type_uri,
        )

        graph = extensions.graph_service.add_data(
            data=feature_sample_ttl.dict(by_alias=True)
        )

        feature_sampling_ttl = fields.Sampling(
            id=feature_sampling_uri,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            schema_spatial=schema_spatial,
            has_feature_of_interest=feature_sample_ttl,
            has_result=feature_sample_ttl,
            has_site_visit=site_visit_ttl,
            result_date_time=result_time,
            used_procedure=procedure,
        )

        graph = extensions.graph_service.add_data(
            data=feature_sampling_ttl.dict(by_alias=True)
        )

        return {
            "graph": graph,
            "feature_sample_ttl": feature_sample_ttl,
            "feature_sampling_ttl": feature_sampling_ttl,
        }

    def create_photo_sampler_ttl(
        self,
        graph: Graph,
        plot_ttl: dict,
        feature_sample_ttl: dict,
        schema_spatial: dict,
        photo_sampler: dict,
        procedure: dict,
        result_time: str,
        sample_type: str,
        photo_sampler_name: str,
    ):
        if not photo_sampler:
            return graph

        site_ttl, site_visit_ttl = None, None
        # existing site data
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]

        record_ttl = plot_ttl["rt"]

        photo_samplers = utility.find_property_by_field(
            object=dict(photo_sampler), field_name="id", outputs=[]
        )
        for photo_sampler in photo_samplers:
            value = dict(photo_sampler["value"])
            url = value["url"]
            id = value["id"]
            name = value["name"]
            url = url if "http" in url else f"http://localhost{url}"

            image_label = f"{name} {str(id)}"
            uuid = utility.generate_uuid()

            image_uri = f"{record_ttl.id}/photos/{uuid}"
            image_sampling_uri = f"{record_ttl.id}/taking-photos/{uuid}"

            image_ttl = fields.Sample(
                id=image_uri,
                label=f"{photo_sampler_name}-{image_label}",
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                identifier=name.split(".")[0],
                archived_at=url,
                is_sample_of=site_ttl,
            )

            graph = extensions.graph_service.add_data(
                data=image_ttl.dict(by_alias=True)
            )
            create_sampler_ttl = self.create_sampler_ttl(
                record_ttl=record_ttl,
                procedure=procedure,
                label="Canon EOS 550D DSLR cropped sensor camera",
                type="camera"
            )
            image_sampling_ttl = fields.Sampling(
                id=image_sampling_uri,
                label="taking photos",
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                has_feature_of_interest=site_ttl,
                has_result=image_ttl,
                used_procedure=procedure,
                has_site_visit=site_visit_ttl,
                result_date_time=result_time,
                made_by_sampler=create_sampler_ttl,
            )

            graph = extensions.graph_service.add_data(
                data=image_sampling_ttl.dict(by_alias=True)
            )

        return graph

    def get_spieces_sample_type(
        self,
        specimen_type_fields: list,
        feature_type_ttls: dict,
        schema_spatial: dict,
        plot_ttl: dict,
        protocol: dict,
        feature_group_value: str = None,
    ):
        site_ttl, site_visit_ttl = None, None
        # existing site data
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]
        record_ttl = plot_ttl["rt"]
        # type_default = BarcodeType.PLANT.value
        sample_label = f"specimen"
        sampling_label = f"vouchering"
        # default type
        feature_type_ttl = None
        if not specimen_type_fields:
            feature_name = utility.feature_type_prefix(
                name=protocol["specimen-feature-type"], position=schema_spatial
            )
            if feature_group_value:
                feature_name = utility.feature_type_prefix(
                    name=protocol["specimen-feature-type"], position=None, id=feature_group_value
                )
            if feature_name in feature_type_ttls:
                feature_type_ttl = feature_type_ttls[feature_name]
        if feature_type_ttl:
            if "object" in feature_type_ttl:
                feature_type_ttl = feature_type_ttl["object"]

        # handling first one should be enough
        for type in specimen_type_fields:
            temp_object = {type["field_name"]: dict(type["value"])}
            feature_index = utility.get_feature_type_index(
                obs_with_all=dict(temp_object),
                feature_types=type["value"]["feature_type"],
                protocol=dict(protocol),
            )
            feature_name = utility.feature_type_prefix(
                name=type["value"]["feature_type"][feature_index]["value"],
                position=schema_spatial,
            )
            if feature_group_value:
                feature_name = utility.feature_type_prefix(
                    name=feature_name, position=None, id=feature_group_value
                )
            feature_type_ttl = feature_type_ttls[feature_name]
            if "object" in feature_type_ttl:
                feature_type_ttl = feature_type_ttl["object"]

        if BarcodeType.PLANT.value in feature_name:
            sample_label = f"{BarcodeType.PLANT.value}-specimen"
            sampling_label = f"{BarcodeType.PLANT.value}-vouchering"
        if BarcodeType.ANIMAL.value in feature_name:
            sample_label = f"{BarcodeType.ANIMAL.value}-specimen"
            sampling_label = "fauna-vouchering"

        return {
            "feature_type": feature_type_ttl,
            # "feature_type_uri": feature_of_interest_uri,
            "sample_label": sample_label,
            "sampling_label": sampling_label,
        }

    def create_image_ttl(
        self,
        graph: Graph,
        plot_ttl: dict,
        feature_ttls: list,
        schema_spatial: dict,
        image: dict,
        data: dict,
        procedure: str,
        result_time: str,
        sample_type: str,
        image_name: str,
        protocol: dict,
        specimen_type_fields: list = [],
        feature_type_ttls: list = [],
        feature_type_ttl: dict = None,
        photo_sample_label: str = None,
        photo_sampling_label: str = None,
        feature_group_value: str = None,
    ):
        if not image:
            return graph

        site_ttl, site_visit_ttl = None, None
        # existing site data
        if "st" in plot_ttl:
            site_ttl = plot_ttl["st"]
            site_visit_ttl = plot_ttl["svt"]

        record_ttl = plot_ttl["rt"]
        images = utility.find_property_by_field(
            object=dict(image), field_name="id", outputs=[]
        )
        if feature_type_ttls:
            specimen_type = self.get_spieces_sample_type(
                specimen_type_fields=specimen_type_fields,
                feature_type_ttls=feature_type_ttls,
                schema_spatial=schema_spatial,
                plot_ttl=plot_ttl,
                protocol=protocol,
                feature_group_value=feature_group_value,
            )
            feature_type_ttl = specimen_type["feature_type"]

        sample_label, sampling_label = "photos", "taking-photos"
        if "photo-sample-label" in protocol:
            sample_label = protocol["photo-sample-label"]
        if "photo-sampling-label" in protocol:
            sampling_label = protocol["photo-sampling-label"]

        if photo_sample_label:
            sample_label = photo_sample_label
        if photo_sampling_label:
            sampling_label = photo_sampling_label

        names_map = utility.get_names_map_data()
        image_attributes = []
        if data:
            image_attrs = names_map["media_attributes_concepts"]
            if isinstance(data, dict):
                image_attributes = self.get_attributes_list(
                    data=dict(data),
                    dataset_ttl=record_ttl,
                    key_to_find="handle-media-attributes",
                    approved=image_attrs,
                    existing_attributes=[],
                    skip_fields=[],
                )
            if isinstance(data, list):
                for d in data:
                    if not d:
                        continue
                    if isinstance(d, dict):
                        continue
                    image_attributes = self.get_attributes_list(
                        data=dict(d),
                        dataset_ttl=record_ttl,
                        key_to_find="handle-media-attributes",
                        approved=image_attrs,
                        existing_attributes=image_attributes,
                        skip_fields=[],
                    )
        for image in images:
            value = dict(image["value"])
            if "url" not in value:
                continue
            url = value["url"]
            id = value["id"]
            name = value["name"]
            hash = value["hash"]
            url = url if "http" in url else f"http://localhost{url}"

            image_label = f"{name} {str(id)}"
            if "(field)" in sample_label and "nrm_label" in value:
                sample_label = sample_label.replace(
                    "(field)", value["nrm_label"])
                sample_label = sample_label.replace(" ", "-")
            if "(field)" in sampling_label and "nrm_label" in value:
                sampling_label = sampling_label.replace(
                    "(field)", value["nrm_label"])
                sampling_label = sampling_label.replace(" ", "-")
            uuid = utility.generate_uuid()
            image_uri = f"{record_ttl.id}/{sample_label}/{uuid}"
            image_sampling_uri = f"{record_ttl.id}/{sampling_label}/{uuid}"

            feature_type = None
            if feature_type_ttl.feature_type:
                feature_type = str(feature_type_ttl.feature_type)

            image_ttl = fields.Sample(
                id=image_uri,
                label=f"{image_name}-{image_label}",
                identifier=hash,
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                has_attribute=image_attributes,
                schema_spatial=schema_spatial,
                feature_type=feature_type,
                archived_at=url,
                is_sample_of=feature_type_ttl,
            )

            graph = extensions.graph_service.add_data(
                data=image_ttl.dict(by_alias=True)
            )

            image_sampling_ttl = fields.Sampling(
                id=image_sampling_uri,
                label=sampling_label.replace("-", " "),
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                schema_spatial=schema_spatial,
                has_feature_of_interest=feature_type_ttl,
                has_result=image_ttl,
                used_procedure=procedure,
                has_site_visit=site_visit_ttl,
                result_date_time=result_time,
            )

            graph = extensions.graph_service.add_data(
                data=image_sampling_ttl.dict(by_alias=True)
            )

        return graph

    def create_sample_ttl(
        self,
        sample_uri: str,
        sample_label: str,
        record_ttl: dict,
        feature_sample_ttl: dict = None,
        sampling_uri: str = None,
        feature_type_uri: str = None,
        archived_at: str = None,
        sample_type: str = None,
        has_attribute: list = [],
    ):
        if sample_type == SampleType.FEATURE:
            return fields.FeatureSample(
                id=sample_uri,
                label=sample_label,
                identifier=sample_label,
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                is_result_of=sampling_uri,
                is_sample_of=feature_sample_ttl.id if feature_sample_ttl else None,
                feature_type=feature_type_uri,
            )

        if sample_type == SampleType.MATERIAL:
            return fields.MaterialSample(
                id=sample_uri,
                label=sample_label,
                material_sample_id=sample_label,
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                is_result_of=sampling_uri,
                is_sample_of=feature_sample_ttl.id if feature_sample_ttl else None,
                feature_type=feature_type_uri,
            )

        if sample_type == SampleType.IMAGE:
            return fields.Sample(
                id=sample_uri,
                label=sample_label,
                identifier=sample_label,
                in_dataset=record_ttl,
                is_part_of=record_ttl.is_part_of,
                feature_type=feature_type_uri,
                archived_at=archived_at,
                is_sample_of=feature_sample_ttl.id if feature_sample_ttl else None,
                has_attribute=has_attribute,
            )

        return fields.Sample(
            id=sample_uri,
            label=sample_label,
            identifier=sample_label,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            is_result_of=sampling_uri,
            is_sample_of=feature_sample_ttl.id if feature_sample_ttl else None,
            feature_type=feature_type_uri,
            archived_at=archived_at,
        )

    def create_sampler_ttl(self, record_ttl: dict, procedure: dict, type: str, label: str, sampler_uri: str = None, attributes: list = []):
        camera_uri = f"{record_ttl.id}/{type}/{utility.generate_uuid()}"
        if not sampler_uri:
            sampler_uri = CAMERA_SAMPLER_URI
        create_sampler_ttl = fields.Sampler(
            id=camera_uri,
            label=label,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            implements=procedure,
            sampler_type=sampler_uri,
            has_attribute=attributes
        )

        return create_sampler_ttl

    def create_lut(
        self,
        object: dict,
        label: str,
        field_type: str,
        dataset_ttl: dict,
        add_is_part_of: bool = True,
        id: str = "",
        mine_uri: bool = True
    ):
        # add prefix iri so that we can change value later
        # if lut uri doest exist we will pull one from bdr
        uri = self.get_uri(
            object=object, dataset_ttl=dataset_ttl, mine_uri=mine_uri)
        if not uri:
            return None

        has_value = fields.IRI(
            id=BNode().n3(),
            label=object["label"],
            value=uri,
        )

        attr_uri = None
        if "attribute_uri" in object:
            attr_uri = object["attribute_uri"]
        if "plot_attribute_uri" in object:
            attr_uri = object["plot_attribute_uri"]

        if field_type == FieldType.ATTRIBUTE:
            attr = fields.Attribute(
                id=id if id else BNode().n3(),
                in_dataset=dataset_ttl,
                label=label if label else uri,
                attribute=attr_uri,
                has_simple_value_iri=uri,
                has_value=has_value,
            )
            if add_is_part_of:
                attr.is_part_of = dataset_ttl.is_part_of

            return attr

        return {
            "has_result": has_value,
            "has_simple_value": None,
            "has_simple_value_iri": uri,
        }

    def create_non_lut(
        self,
        object: dict,
        label: str,
        field_type: str,
        field: str,
        dataset_ttl: dict = {},
        value=None,
        add_is_part_of: bool = True,
        id: str = "",
        unit: str = None
    ):
        # if not object:
        #     return ""
        val = object["value"]
        if val is None:
            return None
        has_value = None

        value_type = utility.get_value_type(
            field=field, value=value, object=object)

        if value_type == DataType.INT.value:
            if not unit:
                unit = extensions.protocol_service.get_field_unit(
                    nrm_field=object["nrm_label"]
                )
            has_value = fields.Int(
                id=BNode().n3(),
                value=val,
                unit=unit,
            )

        if value_type == DataType.FLOAT.value:
            if not unit:
                unit = extensions.protocol_service.get_field_unit(
                    nrm_field=object["nrm_label"]
                )
            has_value = fields.Float(
                id=BNode().n3(),
                value=val,
                unit=unit,
            )

        if value_type == DataType.STR.value:
            has_value = fields.Text(
                id=BNode().n3(),
                value=val,
            )
        if value_type == DataType.BOOL.value:
            has_value = fields.Bool(
                id=BNode().n3(),
                value=val,
            )
        uri = None
        if "attribute_uri" in object:
            uri = object["attribute_uri"]
        if "plot_attribute_uri" in object:
            uri = object["plot_attribute_uri"]
        if field_type == FieldType.ATTRIBUTE:
            attr = fields.Attribute(
                id=id if id else BNode().n3(),
                in_dataset=dataset_ttl,
                label=label if label else val,
                attribute=uri,
                has_simple_value=val,
                has_value=has_value,
            )
            if add_is_part_of:
                attr.is_part_of = dataset_ttl.is_part_of
            return attr

        return {
            "has_result": has_value,
            "has_simple_value": val,
            "has_simple_value_iri": None,
        }

    def create_procedure_ttl(
        self,
        protocol: dict,
        metadata: dict,
        record_ttl: dict,
        has_survey: dict,
        data: dict,
    ):
        procedure_id = f"{record_ttl.id}/procedure/{utility.generate_uuid()}"
        survey_model = protocol["survey-model"].replace(
            "-", " ").replace("_", " ")
        protocol_name = f"{survey_model} id"
        names_map = utility.get_names_map_data()
        procedure_inputs_attrs = names_map["procedure_inputs_attributes"]
        procedure_inputs_attributes = self.get_attributes_list(
            data=dict(data),
            dataset_ttl=record_ttl,
            key_to_find="handle-procedure-inputs",
            approved=procedure_inputs_attrs,
            existing_attributes=[],
            skip_fields=[],
        )
        has_has_input = None
        if procedure_inputs_attributes:
            has_has_input = fields.InputVariables(
                id=BNode().n3(),
                label=protocol_name,
                has_attribute=procedure_inputs_attributes,
            )

        return fields.Procedure(
            id=procedure_id,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            has_survey=has_survey,
            tern_method_type=protocol["uri"]["procedure"],
            has_has_input=has_has_input,
        )

    def create_survey_ttl(
        self,
        metadata: dict,
        record_ttl: dict,
        project_ttl: dict,
        start_date_time: str,
        end_date_time: str,
        protocol: dict,
        plot_visit: dict,
    ):
        survey_uuid = metadata["survey_details"]["uuid"]
        survey_uri = f"{record_ttl.id}/survey/{survey_uuid}"

        agent = self.create_creator_ttl(
            minted_uuid=metadata["orgMintedUUID"], survey_data=dict(metadata)
        )

        project = project_ttl.id
        if project_ttl.label:
            project = project_ttl
        survey_ttl = fields.Survey(
            id=survey_uri,
            identifier=survey_uuid,
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            label=metadata["survey_details"]["survey_model"].replace("-", " "),
            was_associated_with=str(agent.id),
            survey_start_time=start_date_time,
            survey_end_time=end_date_time,
        )
        if not plot_visit:
            return survey_ttl
        if "skip-site-visit" in protocol:
            return survey_ttl

        plot_selection = dict(plot_visit["plot_layout"]["plot_selection"])
        site_name = plot_selection["plot_label"]
        if "value" in site_name:
            site_name = site_name["value"]
        visit_start_date = plot_visit["start_date"]
        plot_visit_id = utility.compute_md5_hash(
            value=f"{visit_start_date}{site_name}")
        survey_ttl.has_site_visit = f"{
            record_ttl.id}/sites-visit/{plot_visit_id}"

        return survey_ttl

    def post_exported_conceptInfo(self, concept_ttl: dict, record_ttl: dict = None):
        # payload with all metadata
        dataset_info = {}
        if record_ttl:
            dataset_info = {
                "identifier": record_ttl.identifier,
                "title": record_ttl.title,
                "description": record_ttl.description,
            }

        data = {
            "data": {
                "uuid": utility.get_uuid_from_uri(uri=concept_ttl.id),
                "info": {
                    "uri": str(concept_ttl.id),
                    "type": str(concept_ttl.type),
                    "dataset": dict(dataset_info),
                },
            }
        }
        # store records to core
        data = extensions.core_service.post_response(
            api="api/data-exporter-statuses",
            params=dict(data),
            query=None,
            # for local dev only
            # server="http://localhost:1337",
        )
        return data

    def is_concept_exported(self, concept_name: str, uuid: str):
        if not uuid or not concept_name:
            return None

        flask.current_app.logger.debug(
            f"trying to find reusable concept: {concept_name}, uuid: {uuid}"
        )

        query = f"api/data-exporter-statuses?populate=deep&use-default=true&filters[uuid][$eq]={
            uuid}"
        resp = extensions.core_service.get_response(
            api="api/data-exporter-statuses",
            query=query,
            # for local dev only
            # server="http://localhost:1337",
            use_cache=True,
        )
        uri = f"{MONITOR_PREFIX}/org/merit/{concept_name}/{uuid}"
        if "data" not in resp:
            return None
        for d in resp["data"]:
            if "attributes" not in d:
                continue
            if "uuid" not in d["attributes"]:
                continue
            if "info" not in d["attributes"]:
                continue
            if "uri" not in d["attributes"]["info"]:
                continue

            if uuid != d["attributes"]["uuid"]:
                continue
            if uri != d["attributes"]["info"]["uri"]:
                continue
            return dict(d["attributes"])

        return None

    def create_creator_ttl(self, minted_uuid: str, survey_data: dict):
        creator_id = minted_uuid
        if "org_opaque_user_id" in survey_data:
            if survey_data["org_opaque_user_id"]:
                creator_id = survey_data["org_opaque_user_id"]

        concept_name = "creator"
        uuid = utility.compute_md5_hash(f"{concept_name}{creator_id}")
        # id allowed to reuse existing concept
        if utility.is_approved_reusable_concepts(concept=concept_name):
            isExported = self.is_concept_exported(
                concept_name=concept_name, uuid=uuid)
            if isExported:
                concept_uri = isExported["info"]["uri"]
                flask.current_app.logger.info(
                    f"reusing existing concept, name: {
                        concept_name} concept_uri:{concept_uri}"
                )
                return fields.Agent(id=concept_uri)

        creator_uri = f"{MONITOR_PREFIX}/org/merit/{concept_name}/{uuid}"
        agent_ttl = fields.Agent(
            id=creator_uri,
            sdo_identifier=creator_id,
        )
        if utility.is_approved_reusable_concepts(concept=concept_name):
            self.post_exported_conceptInfo(
                concept_ttl=agent_ttl, record_ttl=None)

        return agent_ttl

    def create_relevant_project_ttl(
        self, graph: Graph, record_ttl: dict, survey_data: dict
    ):
        project_id = survey_data["survey_details"]["project_id"]
        default_uri = (
            "https://linked.data.gov.au/def/nrm/00df82dc-6851-57d3-9e25-9a8a1b3d5245"
        )
        if "value" not in project_id:
            project_id = {
                "value": project_id,
                "attribute_uri": default_uri,  # default value
            }
        if "attribute_uri" not in project_id:
            project_id["attribute_uri"] = default_uri

        project_code = project_id["value"]
        concept_name = "relevant-project"
        is_reusable = utility.is_approved_reusable_concepts(
            concept=concept_name)
        # id allowed to reuse existing concept
        if is_reusable:
            isExported = self.is_concept_exported(
                concept_name=concept_name, uuid=project_code
            )
            if isExported:
                concept_uri = isExported["info"]["uri"]
                flask.current_app.logger.info(
                    f"reusing existing concept, name: {
                        concept_name} concept_uri:{concept_uri}"
                )
                return {
                    "graph": graph,
                    "project_ttl": fields.RelevantProject(id=concept_uri),
                }

        attributes = []
        attributes.append(
            self.create_non_lut(
                object=dict(project_id),
                label="project code",
                field_type=FieldType.ATTRIBUTE,
                dataset_ttl=record_ttl,
                field="project_id",
            )
        )

        relevant_project_ttl = fields.RelevantProject(
            id=f"{MONITOR_PREFIX}/org/merit/{concept_name}/{project_code}",
            generated=str(record_ttl.id),
            label="Relevant project",
            schema_identifier=project_code,
            # has_attribute=attributes,
        )
        graph = extensions.graph_service.add_data(
            data=relevant_project_ttl.dict(by_alias=True)
        )
        if is_reusable:
            self.post_exported_conceptInfo(
                concept_ttl=relevant_project_ttl, record_ttl=record_ttl
            )
        return {"graph": graph, "project_ttl": relevant_project_ttl}

    def create_feature_of_interest_ttl(
        self,
        type: str,
        value: dict,
        protocol: dict,
        record_ttl: dict,
        schema_spatial: dict,
        phenomenon_time: dict,
        plot_data: dict = {},
        replace_foi_values: dict = [],
        site_ttl: dict = None,
        site_visit_ttl: dict = None,
        procedure: dict = None,
        result_time: str = None,
        transect_ttl: dict = None,
        has_attributes: list = [],
        create_obs_collections: bool = True,
    ):
        if replace_foi_values and "replace-feature-type" in protocol:
            for p in protocol["replace-feature-type"]:
                p_name = p.replace(" ", "-")
                sample_properties = self.get_sample_properties(
                    record_ttl=record_ttl,
                    protocol=protocol,
                    plot_data=plot_data,
                    schema_spatial=schema_spatial,
                    foi_attributes=has_attributes,
                    replace_foi_values=replace_foi_values
                )
                result = self.create_tree_sample(
                    record_ttl=record_ttl,
                    schema_spatial=sample_properties["schema_spatial"],
                    site_ttl=site_ttl,
                    site_visit_ttl=site_visit_ttl,
                    feature_type=value["uri"],
                    procedure=procedure,
                    result_time=result_time,
                    sample_attributes=sample_properties["sample_attributes"],
                    sampling_attributes=sample_properties["sampling_attributes"],
                    sample_name="trees",
                    sampling_name="sampling-plant-population-tress"
                )
                return {
                    "object": result["sample_ttl"],
                    "create_obs_collections": create_obs_collections,
                    "sampling_ttl": result["sampling_ttl"]
                }

        existing = {}
        if "temporary-save-concepts" in protocol:
            existing = utility.load_json_file(
                file_name="outputs/temporary_concepts.json", include_root_dir=False
            )

        prefix = f"{record_ttl.id}/feature-of-interest/{type}/"
        postfix = utility.get_uuid_from_uri(uri=value["uri"])
        if site_ttl:
            postfix = utility.get_uuid_from_uri(uri=site_ttl)
        if schema_spatial:
            postfix = utility.get_uuid_from_uri(
                utility.compute_md5_hash(value=schema_spatial.as_wkt)
            )

        feature_of_interest_uri = f"{prefix}{postfix}"

        feature_type_ttl = fields.FeatureOfInterest(
            id=feature_of_interest_uri,
            label=value["value"],
            in_dataset=record_ttl,
            is_part_of=record_ttl.is_part_of,
            feature_type=value["uri"],
            schema_spatial=schema_spatial,
            has_attribute=has_attributes,
            # has_phenomenon_time=phenomenon_time,
        )
        if site_ttl:
            feature_type_ttl.has_site = site_ttl

        transect_feature_types = []
        if "transect-feature-types" in protocol:
            transect_feature_types = protocol["transect-feature-types"]
        if transect_feature_types:
            for ft in transect_feature_types:
                f_name = ft.replace(" ", "-")
                if f_name not in type:
                    continue
                if not transect_ttl:
                    continue
                feature_type_ttl.has_site = transect_ttl
                postfix = utility.get_uuid_from_uri(uri=str(transect_ttl.id))
                feature_of_interest_uri = f"{prefix}{postfix}"

                if "temporary-save-concepts" in protocol:
                    if feature_of_interest_uri in existing:
                        return {
                            "create_obs_collections": create_obs_collections,
                            "object": fields.FeatureOfInterest(id=feature_of_interest_uri)
                        }

                feature_type_ttl.id = feature_of_interest_uri

                if "temporary-save-concepts" in protocol:
                    existing[feature_of_interest_uri] = True
                    utility.store_file(
                        data=dict(existing),
                        protocol_name="",
                        file_name="temporary_concepts",
                        file_type=FileType.JSON,
                    )
                return {
                    "object": feature_type_ttl,
                    "create_obs_collections": create_obs_collections,
                }

        if not transect_feature_types and transect_ttl:
            postfix = utility.get_uuid_from_uri(uri=str(transect_ttl.id))
            feature_of_interest_uri = f"{prefix}{postfix}"
            feature_type_ttl.id = feature_of_interest_uri
            feature_type_ttl.has_site = transect_ttl

        if "temporary-save-concepts" in protocol:
            if feature_of_interest_uri in existing:
                return {
                    "create_obs_collections": create_obs_collections,
                    "object": fields.FeatureOfInterest(id=feature_of_interest_uri)
                }

        if "temporary-save-concepts" in protocol:
            existing[feature_of_interest_uri] = True
            utility.store_file(
                data=dict(existing),
                protocol_name="",
                file_name="temporary_concepts",
                file_type=FileType.JSON,
            )
        return {
            "object": feature_type_ttl,
            "create_obs_collections": create_obs_collections,
        }

    def create_record_ttl(self, survey_data: dict):

        project_id = survey_data["survey_details"]["project_id"]

        if "value" in project_id:
            project_id = project_id["value"]
        minted_uuid = "xxxx"
        if "orgMintedUUID" in survey_data:
            minted_uuid = survey_data["orgMintedUUID"]

        title = f"Record {minted_uuid}"

        dataset_uri = f"{MONITOR_PREFIX}/{project_id}/submission/{minted_uuid}"
        survey_model = (
            survey_data["survey_details"]["survey_model"]
            .replace("-", " ")
            .replace("_", " ")
        )
        description = f"Record of {survey_model} protocol"

        qualified_attributions = []
        agent = self.create_creator_ttl(
            minted_uuid=minted_uuid, survey_data=dict(survey_data)
        )
        person = agent.id
        if agent.sdo_identifier:
            person = agent

        qualified_attribution1 = fields.QualifiedAttribute(
            id=BNode().n3(),
            agent=[person],
            had_role=f"https://linked.data.gov.au/def/data-roles/creator",
        )
        qualified_attributions.append(qualified_attribution1)

        qualified_attribution2 = fields.QualifiedAttribute(
            id=BNode().n3(),
            agent="https://linked.data.gov.au/org/dcceew",
            had_role=f"https://linked.data.gov.au/def/data-roles/sponsor",
        )
        qualified_attributions.append(qualified_attribution2)

        return fields.RDFDataset(
            id=dataset_uri,
            identifier=minted_uuid,
            title=title,
            issued=survey_data["survey_details"]["time"],
            description=description,
            is_part_of=f"{MONITOR_PREFIX}/{project_id}",
            qualifiedAttribution=qualified_attributions,
        )

    def create_schema_spacial(
        self, dataset_ttl: dict, lat: str, lng: str, label: str = None
    ):
        if not lat or not lng:
            return None

        as_wkt = f"{GEO_POINT_TYPE} POINT ({lat} {lng})"
        geometry_uri = (
            f"{dataset_ttl.id}/geometry/{
                utility.compute_md5_hash(value=as_wkt)}"
        )

        return fields.Geometry(id=geometry_uri, label=label, as_wkt=as_wkt)

    def get_uri(self, object: dict, dataset_ttl: dict, mine_uri: bool = True):
        if not mine_uri:
            if object["uri"]:
                return object["uri"]

        uri = extensions.graph_service.get_lut_uri(
            object=object, dataset_ttl=dataset_ttl
        )
        return uri

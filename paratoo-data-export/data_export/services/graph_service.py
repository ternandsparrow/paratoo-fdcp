import typing
from typing import Optional, List, Union
import requests
import flask
import json

from urllib.parse import quote_plus

from rdflib import Graph, DCTERMS, SOSA, PROV, SDO, VOID, TIME, Namespace, XSD, URIRef
import data_export.models.links as links
import data_export.models.jsonld_context as jsonLD
from rdflib.term import _is_valid_uri

import data_export.extensions as extensions
from data_export.utilities import Utilities as utility
from data_export.constants import (
    API_TIMEOUT,
    GRAPH_URI,
    DAWE_VOCABS_ENDPOINT,
    TERN_RESOURCE_ENDPOINT,
    LUT_QUERY_TYPES,
    FileType,
    STORE_MOCK_HTTP_OUTPUT,
    MOCK_OUTPUT_PREFIX,
    EMSA_RESOVED_LUT_CONCEPTS,
    RESOURCE_DIRECTORY,
)


class APIException(Exception):
    """We use this to indicate a generic issue with a remote API. Generates a 400 if not caught"""

    def __init__(self, message):
        super().__init__(message)


class GraphService:
    def __init__(self, app=None):
        self.enabled = True

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.enabled = app.config.get("SERVICE_ENABLED", False)
        if self.enabled:
            self.jsonld_context = jsonLD.jsonld_context
            self.graph = None
            self.resolved_diffs = None

    def create_graph(self):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return

        g = Graph()
        g.bind("ex", links.EX)
        g.bind("tern", links.TERN)
        g.bind("tern-loc", links.TERN_LOC)
        g.bind("geo", links.GEO)
        g.bind("wgs", links.WGS)
        g.bind("dcterms", DCTERMS)
        g.bind("sosa", SOSA)
        g.bind("prov", PROV)
        g.bind("sdo", SDO)
        g.bind("dwc", links.DWC)
        g.bind("void", VOID)
        g.bind("time", TIME)
        g.bind("sf", links.SF)
        g.bind("ssn", links.SSN)

        self.graph = g
        return self.graph

    def add_data(self, data: dict, is_iri: bool = False):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return

        if not self.graph:
            flask.current_app.logger.info(f"Creating new RDF graph")
            self.create_graph()

        # json_context = self.jsonld_context if not is_iri else jsonLD.jsonld_context_iri
        data_with_context = {**self.jsonld_context, **data}
        self.graph = self.graph.parse(
            data=json.dumps(data_with_context), format="json-ld"
        )

        return self.graph

    def create_uri_ref(self, uri):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return
        """Create a URIRef with the same validation func used by URIRef"""
        if _is_valid_uri(uri):
            return URIRef(uri)
        return URIRef(quote_plus(uri))

    def get_json_context(self):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return

        return self.jsonld_context

    def graph_db_observable_properties(self, collection_uri: str):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return

        cache_key = f"{collection_uri}-graph_db_observable_properties"
        data = utility.get_cache_data(caching=extensions.redis_caching, key=cache_key)
        if data:
            return data

        sparql_query = f"""\n  PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n  PREFIX tern: <https://w3id.org/tern/ontologies/tern/>\n  select ?concept ?featureType (sample(?__label) as ?label) (sample(?_featureTypeLabel) as ?featureTypeLabel) ?valueType (sample(?_valueTypeLabel) as ?valueTypeLabel) ?categoricalCollection (sample(?_categoricalCollectionLabel) as ?categoricalCollectionLabel)\n  from <http://www.ontotext.com/explicit>\n  from <{GRAPH_URI}>\n  where {{ \n      # Collection of observable properties.\n      <{collection_uri}> skos:member ?concept .\n      \n      ?concept skos:prefLabel ?_label .\n      bind(str(?_label) as ?__label)\n  \n      optional {{ \n          ?concept tern:hasFeatureType ?featureType .\n          service <repository:tern_vocabs_core> {{\n              ?featureType skos:prefLabel ?_featureTypeLabel .\n          }}\n      }}\n  \n      optional {{\n        ?concept tern:valueType ?valueType .\n        service <repository:knowledge_graph_core> {{\n          ?valueType skos:prefLabel ?_valueTypeLabel .\n        }}\n      }}\n  \n      optional {{\n        ?concept tern:hasCategoricalCollection ?categoricalCollection .\n        optional {{\n            {{\n                service <repository:tern_vocabs_core> {{\n                    ?categoricalCollection skos:prefLabel ?_categoricalCollectionLabel .\n                }}\n            }}\n            union {{\n                service <repository:ausplots_vocabs_core> {{\n                    ?categoricalCollection skos:prefLabel ?_categoricalCollectionLabel .\n                }}\n            }}\n            union {{\n                ?categoricalCollection skos:prefLabel ?_categoricalCollectionLabel .\n            }}\n        }}\n    }}\n  }} \n  group by ?concept ?featureType ?valueType ?categoricalCollection\n  order by lcase(?label)\n  
        """
        headers = {
            "content-type": "application/x-www-form-urlencoded",
            "accept": "application/sparql-results+json",
            "store-key": cache_key
        }
        try:
            r = requests.post(
                DAWE_VOCABS_ENDPOINT,
                params={"query": sparql_query},
                headers=headers,
                timeout=API_TIMEOUT,
            )
        except Exception as e:
            flask.current_app.logger.error(
                f"Unable to send request to linked.data.gov - {e}",
                exc_info=e,
                extra={"context": {"url": DAWE_VOCABS_ENDPOINT}},
            )
            raise
        if r.status_code != 200:
            flask.current_app.logger.error(
                f"Unable to send request to {DAWE_VOCABS_ENDPOINT}, status code: {r.status_code}"
            )
            raise
        res = r.json()
        if STORE_MOCK_HTTP_OUTPUT:
            utility.store_mock_data(
                url=r.url,
                params={"query": sparql_query},
                body={},
                method="post",
                headers=headers,
                response=res,
                api_name="graphdb"
            )

        utility.store_cache_data(
            caching=extensions.redis_caching,
            key=cache_key,
            value=res["results"]["bindings"],
        )
        return res["results"]["bindings"]

    def sparql_query(self, collection_uri: str):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return

        cache_key = f"{collection_uri}-sparql_query"
        data = utility.get_cache_data(caching=extensions.redis_caching, key=cache_key)
        if data:
            return data

        sparql_query = f"""\nSELECT ?title ?value\n
            {{ \n
            <{collection_uri}> ?title ?value\n
            }}"""

        headers = {
            "content-type": "application/x-www-form-urlencoded",
            "accept": "application/sparql-results+json",
            "store-key": cache_key
        }

        try:
            r = requests.post(
                DAWE_VOCABS_ENDPOINT,
                params={"query": sparql_query},
                headers=headers,
                timeout=API_TIMEOUT,
            )
        except Exception as e:
            flask.current_app.logger.error(
                f"Unable to send request to linked.data.gov - (test-){e}",
                exc_info=e,
                extra={"context": {"url": DAWE_VOCABS_ENDPOINT}},
            )
            return None
        
        if r.status_code != 200:
            flask.current_app.logger.error(
                f"Unable to send request to {DAWE_VOCABS_ENDPOINT}, status code: {r.status_code}"
            )
            raise
        res = r.json()
        if STORE_MOCK_HTTP_OUTPUT:
            utility.store_mock_data(
                url=r.url,
                params={"query": sparql_query},
                body={},
                method="post",
                headers=headers,
                response=res,
                api_name="graphdb"
            )

        utility.store_cache_data(
            caching=extensions.redis_caching,
            key=cache_key,
            value=res["results"]["bindings"],
        )
        return res["results"]["bindings"]

    def get_attribute(self, property_uri: str, property_collection_uri: str):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return

        cache_key = f"{property_uri}-{property_collection_uri}-get_attribute"
        data = utility.get_cache_data(caching=extensions.redis_caching, key=cache_key)
        if data:
            return data

        sparql_query = f"""PREFIX urnp: <urn:property:>
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        select ?attribute {{
            ?s urnp:observableProperty <{property_uri}> ;
                urnp:observablePropertiesCollection <{property_collection_uri}> ;
            urnp:hasAttribute ?attribute .
        }}"""

        headers = {
            "content-type": "application/x-www-form-urlencoded",
            "accept": "application/sparql-results+json",
            "store-key": cache_key
        }

        try:
            r = requests.post(
                DAWE_VOCABS_ENDPOINT,
                params={"query": sparql_query},
                headers=headers,
                timeout=API_TIMEOUT,
            )
        except Exception as e:
            flask.current_app.logger.error(
                f"Unable to send request to linked.data.gov - (test-){e}",
                exc_info=e,
                extra={"context": {"url": DAWE_VOCABS_ENDPOINT}},
            )
            return None
        
        
        
        if r.status_code != 200:
            flask.current_app.logger.error(
                f"Unable to send request to {DAWE_VOCABS_ENDPOINT}, status code: {r.status_code}"
            )
            raise
        res = r.json()
        if STORE_MOCK_HTTP_OUTPUT:
            utility.store_mock_data(
                url=r.url,
                params={"query": sparql_query},
                body={},
                method="post",
                headers=headers,
                response=res,
                api_name="graphdb"
            )
        utility.store_cache_data(
            caching=extensions.redis_caching,
            key=cache_key,
            value=res["results"]["bindings"],
        )
        return res["results"]["bindings"]

    def get_resource_label(self, uri: str):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return

        cache_key = f"{TERN_RESOURCE_ENDPOINT}-{DAWE_VOCABS_ENDPOINT}-{uri}-get_resource_label"

        headers = { "store-key": cache_key }

        url = f"{TERN_RESOURCE_ENDPOINT}?uri={uri}&sparql_endpoint={DAWE_VOCABS_ENDPOINT}&format=application/json"

        try:
            r = requests.get(url, timeout=API_TIMEOUT)
            
        except Exception as e:
            flask.current_app.logger.error(
                f"Unable to send request to core - {e}",
                exc_info=e,
                extra={"context": {"url": url}},
            )
            return None
        
        if r.status_code != 200:
            flask.current_app.logger.error(
                f"Unable to send request to {TERN_RESOURCE_ENDPOINT}, status code: {r.status_code}"
            )
            raise
        res = r.json()
        if STORE_MOCK_HTTP_OUTPUT:
            utility.store_mock_data(
                url=r.url,
                params={},
                body={},
                method="get",
                headers=headers,
                response=res,
                api_name="graphdb"
            )
        if "label" in res:
            return res["label"]

        return None

    def get_api_values(self, api: str):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return
        try:
            r = requests.get(api, timeout=API_TIMEOUT)
        except Exception as e:
            flask.current_app.logger.error(
                f"Unable to send request to core - {e}",
                exc_info=e,
                extra={"context": {"url": api}},
            )
            return None
        
        if r.status_code != 200:
            flask.current_app.logger.error(
                f"Unable to send request to {api}, status code: {r.status_code}"
            )
            raise
        res = r.json()
        if STORE_MOCK_HTTP_OUTPUT:
            utility.store_mock_data(
                url=r.url,
                params={},
                body={},
                method="get",
                headers={},
                response=res,
                api_name="core"
            )
        return res
    
    def get_categorical_value(self, concept_uri: str):
        result = {}
        properties = self.sparql_query(collection_uri=concept_uri)

        if not properties:
            return None

        for property in properties:
            if "title" not in property:
                continue
            if "value" not in property:
                continue

            if property["title"]["type"] != "uri":
                continue
            value_Type = property["title"]["value"].lower()
            if "categorical" not in value_Type and "collection" not in value_Type:
                continue
            values = self.graph_db_observable_properties(
                collection_uri=property["value"]["value"]
            )
            for v in values:
                if "concept" not in v: continue
                if "label" not in v: continue
                result[v["label"]["value"]] = v["concept"]["value"]
            return dict(result)

        return result
    
    def get_lut_uri(self, object: str, dataset_ttl: dict):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return

        uri = None
        field_type = None
        if "property_uri" in object:
            uri = object["property_uri"]
            field_type = "property"

        if "attribute_uri" in object:
            uri = object["attribute_uri"]
            field_type = "attribute"

        if "plot_attribute_uri" in object:
            uri = object["plot_attribute_uri"]
            field_type = "attribute"
        cache_key = uri + object["label"] + object["symbol"]
        lut_uri = utility.get_cache_data(
            caching=extensions.redis_caching, key=cache_key
        )
        if lut_uri:
            return lut_uri

        properties = self.sparql_query(collection_uri=uri)
        if not properties:
            return None

        for property in properties:
            if "title" not in property:
                continue
            if "value" not in property:
                continue

            if property["title"]["type"] != "uri":
                continue
            value_Type = property["title"]["value"].lower()
            if "categorical" not in value_Type and "collection" not in value_Type:
                continue
            
            if "api_source" not in object:
                flask.current_app.logger.error(f"Lut source not found values: {json.dumps(object)}")
                return "https://vocabs-lut-source-not-found"
            
            lut_uri = self.get_matched_uri(
                concept_uri=uri,
                field_type=field_type,
                categorical_types_uri=property["value"]["value"],
                field=object["nrm_label"],
                label=object["label"],
                symbol=object["symbol"],
                api_source=object["api_source"],
                protocol_name=object["protocol"],
            )

            utility.store_cache_data(
                caching=extensions.redis_caching, key=cache_key, value=lut_uri
            )
            return lut_uri

        # categorical values dont exist
        return "https://monitor.tern.org.au/uri-not-found"

    def get_matched_uri(
        self,
        concept_uri: str,
        categorical_types_uri: str,
        field: str,
        field_type: str,
        label: str,
        symbol: str,
        api_source: str,
        protocol_name: str,
    ):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return

        properties = self.graph_db_observable_properties(
            collection_uri=categorical_types_uri
        )

        if not self.resolved_diffs:
            # map generated from EMSA - Resolve diffs between Paratoo and CV - set1-LUTs
            # source https://docs.google.com/spreadsheets/d/1pwZMpUOevx3xcrM0DYBHTdlssXFl6hOPLIOBWueQUS4/edit?gid=1843516922#gid=1843516922
            resolved_diffs = utility.load_json_file(
                file_name=f"{RESOURCE_DIRECTORY}/emsa-resolve-diffs-between-paratoo-and-cv-luts.json",
                include_root_dir=False,
            )
            self.resolved_diffs = list(resolved_diffs)
        resolved_values = {}

        for r in self.resolved_diffs:
            obj = dict(r)
            import json

            if not obj["NRM Collection URI"] and not obj["paratoo-source"]:
                continue

            is_matched = False
            if obj["NRM Collection URI"] == categorical_types_uri:
                is_matched = True
                
            if utility.is_api_match(
                input_source=api_source, remote_source=obj["paratoo-source"]
            ):
                is_matched = True

            if not is_matched:
                continue

            resolved_uri = None
            if "http" in obj["NRM Concept URI"]:
                resolved_uri = obj["NRM Concept URI"]
            if not resolved_uri and "http" in obj["New_URI"]:
                resolved_uri = obj["New_URI"]

            if resolved_uri:
                resolved_uri = resolved_uri.replace("<", "").replace(">", "")
                missing_field = utility.minify_label(value=obj["missing values"])
                resolved_values[missing_field] = resolved_uri

        if api_source:
            api_values = self.get_api_values(api=api_source)

            self.record_missing_lut(
                concept_uri=concept_uri,
                categorical_types_uri=categorical_types_uri,
                field=field,
                field_type=field_type,
                api_source=api_source,
                protocol_name=protocol_name,
                properties=properties,
                api_values=api_values["data"],
                resolved_values=dict(resolved_values),
            )

        monitor_field = utility.minify_label(value=label)
        for property in properties:
            if "label" not in property:
                continue

            if property["label"]["type"] != "literal":
                continue

            nrm_field = utility.minify_label(value=property["label"]["value"])
            if monitor_field != nrm_field:
                continue

            return property["concept"]["value"]

        if monitor_field in resolved_values:
            return resolved_values[monitor_field]

        return "https://categorical-value-not-found"

    def record_missing_lut(
        self,
        concept_uri: str,
        categorical_types_uri: str,
        field: str,
        field_type: str,
        api_source: str,
        protocol_name: str,
        force_push: bool = False,
        msg: str = None,
        properties: list = [],
        api_values: list = [],
        resolved_values: dict = {},
    ):
        nrm_values = []
        for p in properties:
            if "label" not in p:
                continue
            if p["label"]["type"] != "literal":
                continue

            nrm_field = utility.minify_label(value=p["label"]["value"])
            nrm_values.append(nrm_field)

        missing_values = []
        for v in api_values:
            label = v["attributes"]["label"]
            trimmed = utility.minify_label(value=label)
            if trimmed in nrm_values:
                continue
            if trimmed in resolved_values:
                continue
            missing_values.append(label)

        if not missing_values and not force_push:
            return

        filename = f"outputs/{protocol_name}/{protocol_name}-diffs.json"
        diffs = utility.load_json_file(file_name=filename, include_root_dir=False)

        if "incorrect_fields" not in diffs["luts"]:
            diffs["luts"]["incorrect_fields"] = []
        if "diffs" not in diffs["luts"]:
            diffs["luts"]["diffs"] = []
        if field in diffs["luts"]["incorrect_fields"]:
            return

        diffs["luts"]["incorrect_fields"].append(field)

        diff = {
            "field": field,
            "field_type": field_type,
            "concept_uri": concept_uri,
            "categorical_types_uri": categorical_types_uri,
            "source": api_source,
        }
        if msg:
            diff["message"] = msg
        if missing_values:
            diff["missing_values"] = missing_values

        import json

        flask.current_app.logger.error(
            f"One of the categorical values is missing, protocol name: {protocol_name}, context: {json.dumps(dict(diff))}",
        )

        diffs["luts"]["diffs"].append(diff)
        utility.store_file(
            data=dict(diffs),
            protocol_name=protocol_name,
            file_name=f"{protocol_name}-diffs",
            file_type=FileType.JSON,
        )

    def create_observations(self, data: dict, feature_types: List[dict]):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return

        observations = {}
        updated_data = dict(data)
        keys = list(updated_data.keys())
        for feature in feature_types:
            observations[feature] = []
            temp = {}
            for key in keys:
                if not updated_data[key]:
                    continue
                if not isinstance(updated_data[key], dict):
                    continue
                if "feature_type" not in updated_data[key]:
                    continue
                if updated_data[key]["feature_type"]["value"] != feature:
                    continue
                temp[key] = updated_data[key]
            observations[feature].append(temp)

        return observations

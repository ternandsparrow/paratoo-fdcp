import typing
import requests
import flask
import boto3
import json
import os
from packaging.version import Version, parse

import data_export.extensions as extensions

from data_export.utilities import Utilities as utility
from data_export.constants import FLASK_ENV, STORE_MOCK_HTTP_OUTPUT, RESOURCE_DIRECTORY, EXPORTER_DEPLOYED_ENV_NAME, RESOURCE_TAG


class APIException(Exception):
    """We use this to indicate a generic issue with a remote API. Generates a 400 if not caught"""

    def __init__(self, message):
        super().__init__(message)


class AWSService:
    DEFAULT_TIMEOUT = 555  # seconds

    def __init__(self, app=None):
        self.enabled = True

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.enabled = app.config.get("SERVICE_ENABLED", True)
        self.enable_upload = app.config["ENABLE_UPLOAD"]
        self.protocol_map_api = app.config["AWS_RESOURCE_PROTOCOL"]
        self.core_url = app.config.get("CORE_URL")

        if self.enabled:
            self.aws_key = app.config["AWS_KEY"]
            self.aws_secret = app.config["AWS_SECRET"]
            self.s3_resources_bucket = app.config["AWS_RESOURCE_BUCKET"]
            self.s3_resources = None
            self.s3_bucket = app.config["S3_BUCKET"]
            self.s3_connection = None

    def create_s3_connection(self):
        if not self.aws_key or not self.aws_secret:
            flask.current_app.logger.info(f"aws key required ")
            return None

        if self.s3_connection:
            return self.s3_connection

        self.s3_connection = boto3.client(
            "s3",
            aws_access_key_id=self.aws_key,
            aws_secret_access_key=self.aws_secret,
        )

        return self.s3_connection

    def create_s3_resources(self):
        if not self.aws_key or not self.aws_secret:
            flask.current_app.logger.info(f"aws key required ")
            return None

        if self.s3_resources:
            return self.s3_resources

        self.s3_resources = boto3.resource(
            "s3",
            aws_access_key_id=self.aws_key,
            aws_secret_access_key=self.aws_secret,
        )

        return self.s3_resources

    def upload_file(self, file_path: str, file_name: str, bucket: str = None, force_upload: bool = False):
        if not self.enable_upload and not force_upload:
            flask.current_app.logger.warning(
                "uploading to s3 bucket is disabled")
            return {
                "create_ttl": True,
                "upload_ttl": False,
                "reason": "uploading to s3 bucket is disabled",
            }

        if not self.s3_connection:
            self.s3_connection = self.create_s3_connection()

        if not bucket:
            bucket = self.s3_bucket
        try:
            self.s3_connection.upload_file(
                file_path, bucket, file_name)
        except Exception as e:
            flask.current_app.logger.error(
                f"Unable to upload file to aws bucket - {e}",
                exc_info=e,
                extra={"context": {"file_path": file_path, "file_name": file_name}},
            )
            return {
                "create_ttl": True,
                "upload_ttl": False,
                "reason": f"{e}",
            }

        flask.current_app.logger.info(f"uploaded {file_name} successfully")
        return {"create_ttl": True, "upload_ttl": True}

    def update_version(self, version: dict):
        if not os.path.exists(RESOURCE_DIRECTORY):
            os.makedirs(RESOURCE_DIRECTORY)
        version_file = f"{RESOURCE_DIRECTORY}/version.json"
        utility.json_dump(data=dict(version), file=version_file)

    def get_versions(self, bucket_prefix: str = RESOURCE_TAG):
        version_file = "version.json"
        file_name = self.download_file_from_bucket(
            file_name=version_file,
            file_path=f"{RESOURCE_DIRECTORY}/cloud_{version_file}",
            bucket_prefix=bucket_prefix
        )
        if not file_name:
            return None
        versions = utility.get_app_config_versions()
        if not versions["cloud"]:
            return None
        if "version" not in versions["cloud"]:
            return None

        return versions

    def upload_app_configs(self, prefix: str = "localdev", force: bool = False):
        if EXPORTER_DEPLOYED_ENV_NAME == "test":
            return

        uploaded = []
        if not os.path.exists(RESOURCE_DIRECTORY):
            msg = f"directory {RESOURCE_DIRECTORY} is empty"
            flask.current_app.logger.warning(msg)
            return msg

        versions = self.get_versions(bucket_prefix=prefix)
        if not force:
            local = Version(versions["local"]["version"])
            cloud = Version(versions["cloud"]["version"])
            is_needed = local > cloud
            flask.current_app.logger.debug(
                f"local version: {local}, cloud version: {cloud}")
            if not is_needed:
                msg = f"don't need to upload app configs as local version is not greater than cloud version"
                flask.current_app.logger.debug(msg)
                return msg

        # updating version
        new_version = {}
        new_version["source"] = prefix
        new_version["version"] = "1.0.0"
        if versions:
            new_version = dict(versions["local"])
            new_version["version"] = utility.increment_ver(
                version=new_version["version"])
            flask.current_app.logger.debug(
                f"uploading app configs, new version: {new_version["version"]} and cloud version: {versions["cloud"]["version"]}")

        self.update_version(version=dict(new_version))

        uploaded = []
        # upload configs to s3 if we change something
        for file in os.listdir(RESOURCE_DIRECTORY):
            self.upload_file(
                file_name=f"exporter-{prefix}/{file}",
                file_path=f"{RESOURCE_DIRECTORY}/{file}",
                bucket=self.s3_resources_bucket,
                force_upload=True
            )
            uploaded.append(f"exporter-{prefix}/{file}")
        return uploaded

    def download_file_from_bucket(self, file_name: str, file_path: str, bucket_prefix: str = None):
        if not self.s3_resources:
            self.s3_resources = self.create_s3_resources()

        try:
            resources_bucket = self.s3_resources.Bucket(
                self.s3_resources_bucket)
            prefix = f"exporter-{RESOURCE_TAG}"
            if bucket_prefix:
                prefix = f"exporter-{bucket_prefix}/"

            # download file into resources directory
            for s3_object in resources_bucket.objects.filter(Prefix=prefix):
                if ".json" not in s3_object.key and ".ttl" not in s3_object.key:
                    continue
                f_name = s3_object.key.replace(prefix, "")
                if f_name != file_name:
                    continue
                resources_bucket.download_file(s3_object.key, file_path)
                flask.current_app.logger.info(
                    f"Successfully downloaded {f_name} path: {file_path}")
                return file_path

            return None
        except Exception as e:
            msg = f"Unable to download resources - {e}"
            flask.current_app.logger.error(msg, exc_info=e)
            return None

    def app_properties(self, bucket_prefix: str = None, force_download: bool = False):
        flask.current_app.logger.info(f"downloading app resources for {
                                      EXPORTER_DEPLOYED_ENV_NAME}")

        result = {
            "downloaded": [],
            "error": None,
            "warning": None,
        }
        if EXPORTER_DEPLOYED_ENV_NAME == "test":
            utility.copy_mock_app_resources()
            result["warning"] = "don't need to download app cofigs for test"
            flask.current_app.logger.warning(result["warning"])
            return dict(result)

        if not force_download and FLASK_ENV == "development":
            if STORE_MOCK_HTTP_OUTPUT:
                utility.store_mock_app_resources()
            result["warning"] = "Skip downloading app configs as the environment is localdev"
            flask.current_app.logger.warning(result["warning"])
            return dict(result)

        if not self.s3_resources:
            self.s3_resources = self.create_s3_resources()

        try:
            resources_bucket = self.s3_resources.Bucket(
                self.s3_resources_bucket)
            if resources_bucket:
                if not os.path.exists(RESOURCE_DIRECTORY):
                    os.makedirs(RESOURCE_DIRECTORY)

                prefix = f"exporter-{RESOURCE_TAG}"
                if bucket_prefix:
                    prefix = f"exporter-{bucket_prefix}/"

            # download file into resources directory
            for s3_object in resources_bucket.objects.filter(Prefix=prefix):
                if ".json" not in s3_object.key and ".ttl" not in s3_object.key:
                    continue
                file_name = s3_object.key.replace(prefix, "")
                file_path = f"{RESOURCE_DIRECTORY}/{file_name}"
                resources_bucket.download_file(s3_object.key, file_path)
                result["downloaded"].append(file_path)
                flask.current_app.logger.info(
                    f"Successfully downloaded {prefix}{file_name}")

            if STORE_MOCK_HTTP_OUTPUT:
                utility.store_mock_app_resources()

            return dict(result)
        except Exception as e:
            msg = f"Unable to download resources - {e}"
            flask.current_app.logger.error(msg, exc_info=e)
            result["error"] = msg
            return dict(result)

    def get_list_of_protocol(self):
        if not self.protocol_map_api:
            resources = extensions.protocol_service.refresh_documentation_and_resources()
            return resources

        flask.current_app.logger.info(f"Protocol map api found url: {
                                      self.protocol_map_api}")

        try:
            # mainly for test environment
            r = requests.get(self.protocol_map_api,
                             timeout=AWSService.DEFAULT_TIMEOUT)
            res = r.json()
            if res:
                import os
                resources_dir = "resources"
                if not os.path.exists(resources_dir):
                    os.makedirs(resources_dir)
                with open(f"{resources_dir}/protocols.json", 'w') as f:
                    json.dump(res, f, ensure_ascii=False, indent=1)
                return "done"
        except Exception as e:
            flask.current_app.logger.error(
                f"Unable to download resources - {e}",
                exc_info=e,
            )
            return None

    def get_exported_uuids(self, bucket):
        if not self.s3_resources:
            self.s3_resources = self.create_s3_resources()

        org_uuids = []
        try:
            resources_bucket = self.s3_resources.Bucket(bucket)
            for s3_object in resources_bucket.objects.filter(Prefix=""):
                if "ttl" not in s3_object.key:
                    continue
                if "archived" in s3_object.key:
                    continue
                org_uuid = s3_object.key.split(
                    "org-uuid-")[1].split("-start-date-")[0]
                org_uuids.append(org_uuid)

            return org_uuids
        except Exception as e:
            flask.current_app.logger.error(
                f"Unable to extract org uuids - {e}",
                exc_info=e,
            )
            return org_uuids

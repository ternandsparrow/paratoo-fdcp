import typing
import flask
import time

import data_export.extensions as extensions

from data_export.utilities import Utilities as utility
from data_export.constants import (
    OUTPUT_TTL,
    RDF_FORMAT,
    STORE_MOCK_HTTP_OUTPUT,
    FileType,
)


class APIException(Exception):
    """We use this to indicate a generic issue with a remote API. Generates a 400 if not caught"""

    def __init__(self, message):
        super().__init__(message)


class ExportService:
    def __init__(self, app=None):
        self.enabled = True

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.enabled = app.config.get("SERVICE_ENABLED", False)
        if self.enabled:
            self.rdf_graph = None
            self.output_filename = None

    def convert_survey_data(
        self,
        data: typing.Optional[dict],
        protocol: dict,
        protocol_name: str,
        has_plot_data: bool = True,
        plot_visit_field: str = "plot_visit",
        plot_layout_field: str = "plot_layout",
    ):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return

        if not data:
            flask.current_app.logger.info(f"Dataset is empty")
            return None

        self.rdf_graph = extensions.graph_service.create_graph()
        # it should be a loop
        for survey in data:
            plot_visit = None
            plot_layout = None
            if has_plot_data and plot_visit_field in survey:
                plot_visit = survey[plot_visit_field]
                plot_layout = survey[plot_visit_field][plot_layout_field]

            # dataset record
            dataset_ttl = extensions.turtle_service.create_record_ttl(
                survey_data=survey["survey_metadata"]
            )
            relevant_project = extensions.turtle_service.create_relevant_project_ttl(
                graph=self.rdf_graph,
                record_ttl=dataset_ttl,
                survey_data=dict(survey["survey_metadata"]),
            )
            self.rdf_graph = relevant_project["graph"]
            project_ttl = relevant_project["project_ttl"]
            dataset_ttl.wasGeneratedBy = project_ttl.id
            survey_ttl = extensions.turtle_service.create_survey_ttl(
                metadata=survey["survey_metadata"],
                record_ttl=dataset_ttl,
                project_ttl=project_ttl,
                start_date_time=utility.get_survey_start_date(
                    survey=dict(survey)),
                end_date_time=utility.get_survey_end_date(survey=dict(survey)),
                protocol=protocol,
                plot_visit=plot_visit,
            )

            procedure_ttl = extensions.turtle_service.create_procedure_ttl(
                protocol=protocol,
                metadata=dict(survey["survey_metadata"]),
                record_ttl=dataset_ttl,
                has_survey=survey_ttl,
                data=survey,
            )

            self.create_ttl(
                protocol=protocol,
                data=survey,
                dataset_ttl=dataset_ttl,
                procedure_ttl=procedure_ttl,
                site=plot_layout,
                site_visit=plot_visit,
                survey_ttl=survey_ttl,
                protocol_name=protocol_name,
            )
        self.rdf_graph = utility.fix_graph_properties(
            graph_data=self.rdf_graph)
        
        return { "graph": self.rdf_graph, "output": self.output_filename }

    def generate_ttls_from_org_uuids(self, org_uuids: list, force: bool = False):
        extensions.core_service.counter = 0
        start = time.time()
        final_result = {}
        if not org_uuids:
            final_result["error"] = "No org uuid provided"
            final_result["code"] = 404
            flask.current_app.logger.info(
                f"total processing time {time.time() - start} seconds"
            )
            return dict(final_result)
        
        if STORE_MOCK_HTTP_OUTPUT:
            utility.store_file(
                data={},
                protocol_name="",
                file_name="test_metadata",
                file_type=FileType.JSON,
            )

        map_data = extensions.protocol_service.get_protocols_map_data()
        if map_data["error"]:
            final_result["error"] = map_data["error"]
            final_result["code"] = 404
            flask.current_app.logger.info(
                f"total processing time {time.time() - start} seconds"
            )
            return dict(final_result)

        status = []
        violation_reports = {}
        all_missing_iris = {}
        total_collection = 0
        uuid_map = {}

        protocol_map = map_data["protocols"]
        for org_uuid in org_uuids:    
            final_result[org_uuid] = {}
            start = time.time()

            if STORE_MOCK_HTTP_OUTPUT:
                utility.store_file(
                    data={"org_uuid": org_uuid},
                    protocol_name="",
                    file_name="test_metadata",
                    file_type=FileType.JSON,
                )

            data = extensions.protocol_service.get_org_uuid_collection(
                org_uuid=org_uuid, protocols=protocol_map, force=force
            )
            if data["error"]:
                final_result[org_uuid]["error"] = data["error"]
                flask.current_app.logger.info(
                    f"total processing time {time.time() - start} seconds"
                )
                continue

            protocol_info = dict(data["protocol"])
            p = data["protocol_name"]
            final_result[org_uuid]["protocol"] = p

            if "data" not in data:
                final_result[org_uuid]["error"] = f"No record found"
                flask.current_app.logger.info(
                    f"total processing time {time.time() - start} seconds"
                )
                continue

            collections = data["data"]
            uuid_map = dict(data["uuid_map"])
            if not collections:
                final_result[org_uuid]["error"] = f"No data available(please change start date)"
                flask.current_app.logger.info(
                    f"total processing time {time.time() - start} seconds"
                )
                continue

            for collection in collections:
                if "survey_metadata" not in collection:
                    continue
                survey_metadata = collection["survey_metadata"]
                if "orgMintedUUID" not in survey_metadata:
                    continue
                org_minted_uuid = survey_metadata["orgMintedUUID"]
                id = collection["id"]
                start_date = survey_metadata["survey_details"]["time"]
                diff_file = f"outputs/{p}/{p}-diffs.json"
                

                # saves valid inputs
                utility.store_file(
                    data=[dict(collection)],
                    protocol_name=p,
                    file_name=f"input-{id}-{org_minted_uuid}",
                    file_type=FileType.JSON,
                )

                converted_data = self.convert_survey_data(
                    data=[dict(collection)], protocol=protocol_info, protocol_name=p
                )
                graph = converted_data["graph"]
                output = converted_data["output"]
                if not output:
                    output = f"{p}-org-uuid-{org_minted_uuid}-start-date-{start_date}"

                if not graph:
                    final_result[org_uuid]["error"] = "No associated BDR property or attribute found"
                    continue

                utility.store_file(
                    data=graph,
                    protocol_name=p,
                    file_name=output,
                    file_type=FileType.TURTLE,
                )

                violations = extensions.validator_service.validate_data(
                    protocol=protocol_info,
                    protocol_name=p,
                    collection_index=id,
                )
                if violations:
                    final_result[org_uuid]["violations"] = dict(violations)
                    violation_reports = utility.add_error_report(
                        existings=dict(violation_reports),
                        violations=dict(violations),
                        org_minted_uuid=org_minted_uuid,
                    )
                    flask.current_app.logger.warning(
                        {
                            **violations,
                            "protocol": p,
                            "org_minted_uuid": org_minted_uuid,
                            "survey_index": id,
                        }
                    )

                    diffs = utility.load_json_file(
                    file_name=diff_file, include_root_dir=False)
                    if diffs["luts"]:
                        all_missing_iris[p] = dict(diffs["luts"])
                    continue

                # if success we are going to upload to aws
                file_path = f"outputs/{p}/{output}.ttl"
                file_name = f"{output}.ttl"
                upload_status = extensions.aws_service.upload_file(
                    file_path=file_path, file_name=file_name
                )
                
                if "updates" in data:
                    update_required = data["updates"]
                    if update_required:
                        upload_status["update_required"] = {}
                        upload_status["update_required"] = update_required
                final_result[org_uuid]["upload_status"] = dict(upload_status)

                diffs = utility.load_json_file(
                    file_name=diff_file, include_root_dir=False)
                if diffs["luts"]:
                    all_missing_iris[p] = dict(diffs["luts"])
                
        # combine all violations
        utility.store_file(
            data=dict(violation_reports),
            protocol_name="",
            file_name="violations",
            file_type=FileType.JSON,
        )
        # combine all missing values
        utility.store_file(
            data=dict(all_missing_iris),
            protocol_name="",
            file_name="missing_values",
            file_type=FileType.JSON,
        )

        # final_result["abis_exported_uuid_map"] = dict(uuid_map)
        # extensions.aws_service.upload_app_configs()

        flask.current_app.logger.info(
            f"total processing time {time.time() - start} seconds"
        )

        return dict(final_result)

    def create_ttl(
        self,
        protocol: dict,
        data: dict,
        dataset_ttl: dict,
        procedure_ttl: dict,
        survey_ttl: dict,
        site: dict,
        site_visit: dict,
        protocol_name: str,
    ):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return
        # if survey associated with the data
        survey_data = dict(data)
        self.output_filename = utility.generate_output_name(
            protocol=protocol, 
            protocol_name=protocol_name, 
            survey=dict(survey_data)
        )

        # separates survey and observations
        if "observations" in survey_data:
            observations = dict(data["observations"])
            if protocol["survey-model"] == "plot-selection-survey":
                plot_selections = dict(survey_data["observations"])
                survey_data = dict({**survey_data, **plot_selections})
            del survey_data["observations"]
        plot_ttl = None

        # if plot based protocol
        if "plot-based-protocol" in protocol:
            if protocol["plot-based-protocol"]:
                result = extensions.turtle_service.create_site_ttl(
                    graph=self.rdf_graph,
                    dataset_ttl=dataset_ttl,
                    survey_data=survey_data,
                    protocol=protocol,
                )
                self.rdf_graph = result["graph"]
                plot_ttl = result["plot_ttl"]

        if not plot_ttl:
            plot_ttl = {}
            plot_ttl["rt"] = dataset_ttl

        if utility.is_null_or_empty(object=protocol, field="survey-model"):
            observations = extensions.graph_service.create_observations(
                data=survey_data, feature_types=protocol["feature-types"]
            )

        # convert survey data
        self.rdf_graph = extensions.turtle_service.create_survey_data_ttl(
            graph=self.rdf_graph,
            protocol=protocol,
            survey_data=survey_data,
            plot_ttl=plot_ttl,
            procedure_ttl=procedure_ttl,
        )
        if utility.has_custom_config(protocol=protocol, key="skip-observations"):
            return self.rdf_graph
        
        if not utility.is_setup_id_or_model_based_obs(protocol=protocol, data=survey_data):
            self.rdf_graph = extensions.turtle_service.create_observation_ttl(
                graph=self.rdf_graph,
                protocol=protocol,
                observations=dict(observations),
                survey_data=dict(survey_data),
                plot_ttl=plot_ttl,
                procedure_ttl=procedure_ttl,
                protocol_name=protocol_name
            )
            return self.rdf_graph

        setup_ids = list(observations.keys())
        flask.current_app.logger.debug(setup_ids)
        for s_id in setup_ids:
            procedure_data = dict(survey_data["survey_metadata"])
            if isinstance(observations[s_id], dict):
                procedure_data = {**procedure_data, **observations[s_id]}
                all_s_id_obs = dict(observations[s_id])

            if isinstance(observations[s_id], list):
                # TODO handle multiple data (model based protocols)
                procedure_data = {**procedure_data, **observations[s_id][0]}
                all_s_id_obs = {}
                all_s_id_obs[s_id] = observations[s_id]

            procedure_ttl = extensions.turtle_service.create_procedure_ttl(
                protocol=protocol,
                metadata=dict(survey_data["survey_metadata"]),
                record_ttl=dataset_ttl,
                has_survey=survey_ttl,
                data=dict(procedure_data),
            )

            # setup id based vantage point
            vantage_val_start = utility.find_property_with_key(
                object=observations,
                field_name=s_id,
                key_to_find="handle-vantage-start",
                outputs=[],
            )
            vantage_val_end = utility.find_property_with_key(
                object=observations,
                field_name=s_id,
                key_to_find="handle-vantage-end",
                outputs=[],
            )
            vantage_point_val = utility.find_property_with_key(
                object=observations,
                field_name=s_id,
                key_to_find="handle-vantage-point",
                outputs=[],
            )
            vantage_point_outlook_photos = utility.find_property_with_key(
                object=observations,
                field_name=s_id,
                key_to_find="handle-outlook-zone-attributes-photo",
                outputs=[],
            )

            # setup id based transect
            transect_val_start = utility.find_property_with_key(
                object=observations,
                field_name=s_id,
                key_to_find="handle-transect-start",
                outputs=[],
            )
            transect_val_end = utility.find_property_with_key(
                object=observations,
                field_name=s_id,
                key_to_find="handle-transect-end",
                outputs=[],
            )
            transect_val_attr_photo = utility.find_property_with_key(
                object=observations,
                field_name=s_id,
                key_to_find="handle-transect-attributes-photo",
                outputs=[],
            )

            # setup id based track station
            track_station_attributes = utility.find_property_with_key(
                object=observations,
                field_name=s_id,
                key_to_find="handle-track-station-attribute",
                outputs=[],
            )
            track_station_photo_attributes = utility.find_property_with_key(
                object=observations,
                field_name=s_id,
                key_to_find="handle-track-station-attribute-photo",
                outputs=[],
            )
            track_station_habitat_attributes = utility.find_property_with_key(
                object=observations,
                field_name=s_id,
                key_to_find="handle-track-station-habitat-attribute",
                outputs=[],
            )
            track_station_habitat_photo_attributes = utility.find_property_with_key(
                object=observations,
                field_name=s_id,
                key_to_find="handle-track-station-habitat-attribute-photo",
                outputs=[],
            )

            self.rdf_graph = extensions.turtle_service.create_observation_ttl(
                graph=self.rdf_graph,
                protocol=protocol,
                observations=dict(all_s_id_obs),
                survey_data=dict(survey_data),
                plot_ttl=plot_ttl,
                procedure_ttl=procedure_ttl,
                protocol_name=protocol_name,
                setup_id=s_id,

                # vantage point as setup id
                vantage_start=vantage_val_start,
                vantage_end=vantage_val_end,
                vantage_point_val=vantage_point_val,
                vantage_point_outlook_photos=vantage_point_outlook_photos,

                # transect id as setup id
                transect_start=transect_val_start,
                transect_end=transect_val_end,
                transect_attrs_photo=transect_val_attr_photo,

                track_station_attributes=track_station_attributes,
                track_station_photo_attributes=track_station_photo_attributes,
                track_station_habitat_attributes=track_station_habitat_attributes,
                track_station_habitat_photo_attributes=track_station_habitat_photo_attributes,
            )

        return self.rdf_graph

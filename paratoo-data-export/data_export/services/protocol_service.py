import typing
from typing import List
import flask
import humps
from datetime import datetime

import data_export.extensions as extensions

from data_export.utilities import Utilities as utility
from data_export.constants import (
    PLOT_ATTRIBUTE_URI,
    PLOT_PROPERTIES_URI,
    PROTOCOL_API,
    PLOT_MODELS,
    DB_DATE_FORMATE,
    FileType,
    VOCAB_SERVER,
    PROTOCOL_SETS,
    RESOURCE_DIRECTORY,
    FLASK_ENV,
    STORE_MOCK_HTTP_OUTPUT,
)


class APIException(Exception):
    """We use this to indicate a generic issue with a remote API. Generates a 400 if not caught"""

    def __init__(self, message):
        super().__init__(message)


class ProtocolService:
    def __init__(self, app=None):
        self.enabled = True
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.enabled = app.config.get("SERVICE_ENABLED", True)
        if self.enabled:
            self.core_url = app.config.get("CORE_URL")
            self.org_url = app.config.get("ORG_URL")
            self.enable_cache = app.config.get("ENABLE_CACHE", False)
            self.paratoo_protocols = None
            self.names_map = None
            self.documentation = None
            self.schemas = None

    def get_list_of_protocol(self):
        resource = extensions.aws_service.get_list_of_protocol()
        if not resource:
            return None
        protocols = utility.load_json_file(
            file_name=f"{RESOURCE_DIRECTORY}/protocols.json", include_root_dir=False
        )
        result = {}
        for protocol, properties in protocols.items():
            version_set = "inactive"
            if "set" in properties:
                version_set = properties["set"]
                version_set = f"set-{version_set}"
            if version_set not in result:
                result[version_set] = []
            result[version_set].append(protocol)

        return dict(result)

    # load protocol map
    def get_protocols_map_data(self):
        result = {
            "app-configs": False,
            "documentation": False,
            "protocols": None,
            "error": None
        }
        try:
            result = self.refresh_documentation_and_resources()
            if not result["error"]:
                result["protocols"] = utility.load_json_file(
                    file_name=f"{RESOURCE_DIRECTORY}/protocols.json", include_root_dir=False
                )
                return dict(result)
        except Exception as e:
            msg = f"Unable to download resources - {e}"
            flask.current_app.logger.error(
                msg,
                exc_info=e,
            )
            result["protocols"] = None
            result["error"] = "App config error, reason: One of the app configs is missing, file: protocols.json"
            return result
        return result

    # load all protocols from core/org
    def load_paratoo_protocols(self):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return
        cache_key = "protocol_data"
        protocols = utility.get_cache_data(
            caching=extensions.redis_caching, key=cache_key
        )
        if protocols:
            self.paratoo_protocols = protocols["data"]
            return protocols["data"]

        protocols = extensions.core_service.get_response(api=PROTOCOL_API)
        utility.store_cache_data(
            caching=extensions.redis_caching, key=cache_key, value=protocols
        )
        self.paratoo_protocols = protocols["data"]
        return protocols["data"]

    # get specific protocol details
    def get_protocol_workflow(self, protocol_uuid: str):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return
        if not self.paratoo_protocols:
            self.load_paratoo_protocols()

        for protocol in self.paratoo_protocols:
            if not protocol:
                continue

            keys = protocol.keys()
            if "attributes" not in keys:
                return None

            if not protocol["attributes"]:
                continue

            if protocol["attributes"]["identifier"] != protocol_uuid:
                continue

            # matched workflow
            return protocol["attributes"]["workflow"]

        return None

    def get_all_org_uuids(self, skips):
        org_uuids = []
        all_metadata = extensions.core_service.get_response(
            api="api/org-uuid-survey-metadatas"
        )
        if "data" in all_metadata:
            for d in all_metadata["data"]:
                org_uuid = d["attributes"]["org_minted_uuid"]
                if not org_uuid:
                    continue
                if org_uuid in skips:
                    continue
                org_uuids.append(org_uuid)
        flask.current_app.logger.info(f"Total number od org uuids {len(org_uuids)}")
        return org_uuids

    def get_org_uuid_collection(self, org_uuid: str, protocols: dict, uuid_map: dict = {}, force: bool = False):
        data = self.get_collection(org_uuid=org_uuid, protocols=protocols, force=force)
        if data["error"]:
            return dict(data)
        
        flask.current_app.logger.info(
            f"Pulling data from core, org-uuid: {org_uuid}")
        collection = data["collection"]
        uuid_map = dict(data["uuid_map"])

        protocol_name = data["protocol_name"]
        protocol = data["protocol"]

        flask.current_app.logger.info(
            f"Data population is done protocol_name: {protocol_name}"
        )

        self.create_protocol_relational_attributes(
            prot_name=protocol_name, models=data["models"], protocol=protocol
        )

        extra_fields = {}
        if "extra-fields" in protocol:
            for e in protocol["extra-fields"]:
                f = e.replace(" ", "_")
                extra_fields[f] = "test"
            utility.store_file(
                data=extra_fields,
                protocol_name=protocol_name,
                file_name="extra_fields",
                file_type=FileType.JSON,
            )

        # reset temporary values
        utility.store_file(
            data={},
            protocol_name=protocol_name,
            file_name="temporary_values",
            file_type=FileType.JSON,
        )

        # create file `diff` to see any fields which are not mapped
        utility.store_file(
            data={
                "protocol": protocol_name,
                "current_time": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                "attributes": {},
                "properties": {},
                "luts": {},
            },
            protocol_name=protocol_name,
            file_name=f"{protocol_name}-diffs",
            file_type=FileType.JSON,
        )
        filtered_data = self.pull_all_concepts_uri(
            collections=[collection],
            protocol=protocol,
            protocol_name=protocol_name,
        )
        # if setup id based protocol
        if "setup-id-based-protocol" in protocol:
            if protocol["setup-id-based-protocol"]:
                filtered_data = self.fixed_setup_id_protocol(
                    data=filtered_data, protocol=protocol
                )

        return {
            "data": filtered_data,
            "updates": [dict(data["update_collection"])],
            "uuid_map": dict(data["uuid_map"]),
            "protocol_name": protocol_name,
            "protocol": dict(protocol),
            "error": None
        }

    def get_data_within_date_range(
        self,
        start_date: str,
        end_date: str,
        protocol: dict,
        protocol_name: str,
        uuid_map: dict,
        work_flow: typing.Optional[dict],
    ):
        if "survey-model" not in protocol:
            return None
        flask.current_app.logger.info("Pulling data from core")
        data = self.get_all_collections(
            protocol=protocol,
            start_date=start_date,
            protocol_name=protocol_name,
            uuid_map=dict(uuid_map),
        )
        collections = data["collections"]
        uuid_map = dict(data["uuid_map"])
        flask.current_app.logger.info("Data population is done")

        # create file `diff` to see any fields which are not mapped
        utility.store_file(
            data={
                "protocol": protocol_name,
                "current_time": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                "attributes": {},
                "properties": {},
                "luts": {},
            },
            protocol_name=protocol_name,
            file_name=f"{protocol_name}-diffs",
            file_type=FileType.JSON,
        )
        filtered_data = self.pull_all_concepts_uri(
            collections=collections,
            protocol=protocol,
            protocol_name=protocol_name,
        )
        # if setup id based protocol
        if "setup-id-based-protocol" in protocol:
            if protocol["setup-id-based-protocol"]:
                filtered_data = self.fixed_setup_id_protocol(
                    data=filtered_data, protocol=protocol
                )

        return {
            "data": filtered_data,
            "updates": dict(data["update_collections"]),
            "uuid_map": dict(data["uuid_map"]),
        }

    def fixed_setup_id_protocol(self, data: list, protocol: dict):
        for updated in data:
            is_skipped = utility.is_skip_operation(
                protocol=protocol, data=dict(updated)
            )
            if is_skipped:
                continue

            new_observations = {}
            models = updated["observations"].keys()
            transect_ids = utility.find_property_with_key(
                object=dict(updated),
                field_name="observations",
                key_to_find="handle-setup-id-based-observations",
                outputs=[],
            )
            survey_transect_fields = utility.find_property_with_key(
                object=dict(updated),
                field_name="observations",
                key_to_find="handle-survey-transect-fields",
                outputs=[],
            )
            for t in transect_ids:
                if not t:
                    continue
                id = t["value"]["value"]
                if id in new_observations:
                    continue
                new_observations[id] = {}
                for m in models:
                    new_observations[id][m] = []
                for v in survey_transect_fields:
                    new_observations[id][v["field_name"]] = []

            self.names_map = utility.get_names_map_data()

            transects_fields = self.names_map["setup_id_based_observations_concepts"]
            transects_fields.append("new_or_existing")
            nested_fields = None
            if "nested-setup-id-fields" in protocol:
                nested_fields = protocol["nested-setup-id-fields"]
            if "non-null-models" in protocol:
                for m, v in protocol["non-null-models"].items():
                    if m not in updated["observations"]:
                        continue
                    updated["observations"][m] = updated[v]
                    if isinstance(updated[v], dict):
                        updated["observations"][m] = [updated[v]]
            for model in models:
                nested_values = {}
                for values in updated["observations"][model]:
                    keys = list(values.keys())
                    if not nested_fields:
                        for k, v in values.items():
                            if k not in transects_fields:
                                continue
                            if not isinstance(v, dict):
                                continue
                            if "value" in v:

                                new_observations[v["value"]
                                                 ][model].append(values)
                            # new or existing
                            if "site_id" in v:
                                new_observations[v["site_id"]["value"]][model].append(
                                    values
                                )

                    if nested_fields:
                        # for parents only
                        for k, v in values.items():
                            if k in nested_fields:
                                continue
                            group = utility.grouped_value(
                                data=v, field=k, grouped_list=transects_fields
                            )

                            if group:
                                name = group["name"]
                                if name not in nested_values:
                                    nested_values[name] = {}
                                nested_values[name][k] = v
                                continue
                            nested_values[k] = v

                        # for nested childs only
                        for k, v in values.items():
                            if k not in nested_fields:
                                continue
                            # TODO other data types
                            if isinstance(v, list):
                                for c in v:
                                    if not isinstance(c, dict):
                                        continue
                                    for f, sv in c.items():
                                        g = utility.grouped_value(
                                            data=sv,
                                            field=f,
                                            grouped_list=transects_fields,
                                        )
                                        if not g:
                                            continue
                                        name = g["name"]
                                        if name not in nested_values:
                                            nested_values[name] = {}
                                        if f not in nested_values[name]:
                                            nested_values[name][f] = []
                                        nested_values[name][f].append(c)
                                        break

                if nested_fields:
                    for t in transect_ids:
                        id = t["value"]["value"]
                        d = {}
                        for k, v in nested_values.items():
                            if k != id:
                                d[k] = v
                                continue
                            d = {**d, **nested_values[id]}
                        if not d:
                            continue
                        new_observations[id][m].append(d)

            for v in survey_transect_fields:
                field_name = v["field_name"]
                for values in v["value"]["value"]:
                    keys = list(values.keys())

                    for k in keys:
                        if k not in transects_fields:
                            continue

                        if not isinstance(values[k], dict):
                            continue
                        if "value" in values[k]:
                            new_observations[values[k]["value"]][field_name].append(
                                values
                            )
                        # new or existing
                        if "site_id" in values[k]:
                            new_observations[values[k]["site_id"]["value"]][
                                field_name
                            ].append(values)
            updated["observations"] = dict(new_observations)
        return data

    def is_lut(self, keys: list):
        lut_keys = ["symbol", "label", "uri"]
        for k in lut_keys:
            if k not in keys:
                continue
            return True
        return False

    def check_attribute_type(self, data, field=None, protocol=None):
        """recursive method to remove all attributes and data field"""

        if not data:
            return data

        self.names_map = utility.get_names_map_data()

        if isinstance(data, int):
            return data

        if isinstance(data, float):
            return data

        if isinstance(data, str):
            return data

        if isinstance(data, list):
            temp_object = []
            for obj in data:
                if "lut" in obj:
                    obj[f"{field}_multi_lut"] = obj["lut"]
                    del obj["lut"]

                new_obj = {}
                obs_keys = list(obj.keys())
                for key in obs_keys:
                    field_name = key
                    filter_object_fields = utility.has_custom_config(
                        protocol=protocol, 
                        key="filter-object-fields"
                    )
                    if not filter_object_fields: filter_object_fields = {}
                    if key in filter_object_fields:
                        temp = self.set_filtered_values(
                            key=key, 
                            object=obj[key], 
                            filter_object_fields=dict(filter_object_fields)
                        )
                        new_obj[key] = temp
                        obj[key] = temp
                    if "skip-fields" in protocol:
                        for skip in protocol["skip-fields"]:
                            if skip == field_name:
                                field_name = f"skipped_{skip}"
                                if isinstance(obj[skip], dict):
                                    if skip in obj[skip]:
                                        field_name = f"parent_{skip}"

                    for skip in self.names_map["global_skip_fields"]:
                        if skip == field_name:
                            field_name = f"skipped_{skip}"
                            if isinstance(obj[skip], dict):
                                if skip in obj[skip]:
                                    field_name = f"parent_{skip}"

                    if "delete-fields" in protocol:
                        if key in protocol["delete-fields"]:
                            continue

                    if field_name in self.names_map["add_prefix_attributes"]:
                        field_name = f"{field}__{field_name}"

                    if "add_prefix_attributes" in protocol:
                        if field_name in protocol["add_prefix_attributes"]:
                            field_name = f"{field}__{field_name}"

                    new_obj[field_name] = self.check_attribute_type(
                        data=obj[key], field=key, protocol=protocol
                    )
                temp_object.append(new_obj)
            return temp_object

        if isinstance(data, dict):
            keys = list(data.keys())
            temp_object = data if "attributes" not in keys else {}
            is_lut = self.is_lut(keys=keys)
            # if data key exists
            if "data" in keys:
                temp_object = self.check_attribute_type(
                    data["data"], field="data", protocol=protocol
                )
                return temp_object

            # if custom lut
            if "lut" in keys:
                temp_object[f"{field}_multi_lut"] = temp_object["lut"]
                del temp_object["lut"]

            for k in keys:
                filter_object_fields = utility.has_custom_config(
                    protocol=protocol, 
                    key="filter-object-fields"
                )
                
                if not filter_object_fields: filter_object_fields = {}
                if k in filter_object_fields:
                    temp = self.set_filtered_values(
                        key=k, 
                        object=data[k], 
                        filter_object_fields=dict(filter_object_fields)
                    )
                    temp_object[k] = temp
                    data[k] = temp
                if k in self.names_map["fields_with_plot_prefix"]:
                    # some attributes need prefix to identify (e.g. 'comment' should be plot-layout-comment)
                    temp_object[f"plot_{k}"] = temp_object[k]
                    del temp_object[k]
                    temp_object["handle-plot-marker"] = True
                    temp_object["plot_marker_location"] = {}
                    temp_object["plot_marker_location"]["value"] = True
                    temp_object["plot_marker_location"]["handle-plot-marker"] = True

                if k in self.names_map["add_prefix_attributes"] and not is_lut:
                    temp_object[f"{field}__{k}"] = temp_object[k]
                    del temp_object[k]

                if "add_prefix_attributes" in protocol:
                    if k in protocol["add_prefix_attributes"] and not is_lut:
                        temp_object[f"{field}__{k}"] = temp_object[k]
                        del temp_object[k]

                if "delete-fields" in protocol:
                    if k in protocol["delete-fields"]:
                        del temp_object[k]

            # if attributes key exists
            if "attributes" in keys:
                temp_object = data["attributes"]
                temp_object["id"] = data["id"]

            if "skip-fields" in protocol:
                for skip in protocol["skip-fields"]:
                    if skip in temp_object:
                        field_name = f"skipped_{skip}"
                        if isinstance(temp_object[skip], dict):
                            if skip in temp_object[skip]:
                                field_name = f"parent_{skip}"
                        temp_object[field_name] = temp_object[skip]
                        del temp_object[skip]

            for skip in self.names_map["global_skip_fields"]:
                if skip in temp_object:
                    field_name = f"skipped_{skip}"
                    if isinstance(temp_object[skip], dict):
                        if skip in temp_object[skip]:
                            field_name = f"parent_{skip}"
                    temp_object[field_name] = temp_object[skip]
                    del temp_object[skip]

            if "delete-fields" in protocol:
                for d in protocol["delete-fields"]:
                    if d in temp_object:
                        del temp_object[d]

            # we check all the attributes
            new_keys = list(temp_object.keys())
            for key in new_keys:
                temp_object[key] = self.check_attribute_type(
                    temp_object[key], field=key, protocol=protocol
                )

            return temp_object

    def set_filtered_values(self, key: str, object: dict, filter_object_fields: dict):
        temp_object = {}
        if not object: return temp_object
        o = dict(object)
        for k in filter_object_fields[key]:
            temp_object[k] = o[k]
        return dict(temp_object)

    def fix_attributes_pattern(self, data: dict, protocol: dict = None):
        obs_keys = list(data.keys())
        all_obs_fixed_data = {}
        self.names_map = utility.get_names_map_data()
        conditional_values = utility.get_conditional_values()

        for key in obs_keys:
            field_name = key
            filter_object_fields = utility.has_custom_config(
                protocol=protocol, 
                key="filter-object-fields"
            )
            if not filter_object_fields: filter_object_fields = {}
            if key in filter_object_fields:
                temp = self.set_filtered_values(
                    key=key, 
                    object=data[key], 
                    filter_object_fields=dict(filter_object_fields)
                )
                data[key] = temp
                
            if "skip-fields" in protocol:
                for skip in protocol["skip-fields"]:
                    if skip == field_name:
                        field_name = f"skipped_{skip}"
                        if isinstance(data[skip], dict):
                            if skip in data[skip]:
                                field_name = f"parent_{skip}"

            for skip in self.names_map["global_skip_fields"]:
                if skip == field_name:
                    field_name = f"skipped_{skip}"
                    if isinstance(data[skip], dict):
                        if skip in data[skip]:
                            field_name = f"parent_{skip}"

            if "delete-fields" in protocol:
                if key in protocol["delete-fields"]:
                    continue

            if field_name in self.names_map["add_prefix_attributes"]:
                field_name = f"root__{field_name}"

            if "add_prefix_attributes" in protocol:
                if field_name in protocol["add_prefix_attributes"]:
                    field_name = f"root__{field_name}"

            if field_name in conditional_values:
                if conditional_values[field_name]["if_value"] == data[key]:
                    data[key] = conditional_values[field_name]["replace_with"]

            all_obs_fixed_data[field_name] = self.check_attribute_type(
                data=data[key], field=key, protocol=protocol
            )

        return dict(all_obs_fixed_data)

    def pull_all_concepts_uri(
        self,
        collections: List[dict],
        protocol: dict,
        protocol_name: str,
    ):

        # flask.current_app.logger.info("Pulling concepts uri")
        property_metadata = self.get_property_metadata(
            protocol=protocol, protocol_name=protocol_name
        )
        final_collections = []
        for c in collections:
            collection = dict(c)

            flask.current_app.logger.info("Pulling observable properties")
            # assign uri observable properties
            if protocol["uri"]["properties"]:
                collection = dict(
                    self.assign_all_uri(
                        data=dict(collection),
                        collection_uri=protocol["uri"]["properties"],
                        uri_name="property_uri",
                        protocol=protocol,
                        property_metadata=dict(property_metadata),
                        protocol_name=protocol_name
                    )
                )
            # assign uri attributes
            flask.current_app.logger.info("Pulling all attributes")
            if protocol["uri"]["attributes"]:
                collection = dict(
                    self.assign_all_uri(
                        data=dict(collection),
                        collection_uri=protocol["uri"]["attributes"],
                        uri_name="attribute_uri",
                        protocol=protocol,
                        property_metadata=dict(property_metadata),
                        protocol_name=protocol_name
                    )
                )
            # if plot based protocol then we will assign uri
            #   for plot attributes and plot properties
            if utility.has_custom_config(protocol=protocol, key="pull-plot-uri"):
                flask.current_app.logger.info("Pulling all plot attributes")
                collection = dict(
                    self.assign_all_plot_uri(
                        data=dict(collection),
                        protocol=protocol,
                        property_metadata=dict(property_metadata),
                        protocol_name=protocol_name,
                    )
                )

            collection = self.remove_field_from_object(
                objects=dict(collection), field="updatedAt"
            )
            final_collections.append(dict(collection))

        return final_collections

    def refresh_documentation_and_resources(self):
        result = {
            "app-configs": False,
            "documentation": False,
            "error": None
        }
        # pull configs from s3
        app_resources = extensions.aws_service.app_properties()
        if not app_resources:
            result["msg"] = app_resources
            return result
        result["app-configs"] = True

        # pull doc from core
        self.documentation = extensions.core_service.get_response(
            api="api/documentation/swagger.json"
        )
        if not self.documentation:
            result["msg"] = "App config error, reason: failed to pull documentation"
            return result
        result["documentation"] = True

        self.names_map = utility.get_names_map_data()
        if not self.names_map:
            result["app-configs"] = False
            result["msg"] = "App config error, reason: One of the app configs is missing, file: fields_map.json"
            return result

        self.schemas = self.documentation["components"]["schemas"]
        utility.store_file(
            data={},
            protocol_name="",
            file_name="temporary_concepts",
            file_type=FileType.JSON,
        )
        return result

    def get_all_attributes(self, model: str, skips: list = [], result: dict = {}):
        schema_name = f"{humps.pascalize(model)}Request"
        return self.get_attribute(
            schema=dict(self.schemas[schema_name]),
            skips=skips,
            processed_models=[],
            result=dict(result),
        )

    def get_attribute(
        self,
        schema: dict,
        skips: list = [],
        processed_models: list = [],
        result: dict = {},
    ):
        skip_fields = ["createdBy", "updatedBy", "survey_metadata"]
        skip_refs = ["file"]

        for field, value in schema["properties"].items():
            if field in skip_fields:
                continue
            if not value:
                continue
            if "x-model-ref" in value:
                model_ref = value["x-model-ref"]
                if model_ref in skips:
                    continue
                if model_ref in skip_refs:
                    continue
                if model_ref in processed_models:
                    continue
                schema_name = f"{humps.pascalize(model_ref)}Request"
                processed_models.append(model_ref)
                result = self.get_attribute(
                    schema=dict(self.schemas[schema_name]),
                    skips=skips,
                    processed_models=processed_models,
                    result=dict(result),
                )
                continue

            if value["type"] == "object":
                result = self.get_attribute(
                    schema=dict(value),
                    skips=skips,
                    processed_models=processed_models,
                    result=dict(result),
                )
                continue

            if value["type"] == "array":
                object = dict(value["items"])
                if "properties" not in object:
                    object = {}
                    object["properties"] = {}
                    object["properties"][field] = dict(value["items"])
                result = self.get_attribute(
                    schema=dict(object),
                    skips=skips,
                    processed_models=processed_models,
                    result=dict(result),
                )
                continue

            result[field] = dict(value)

        return dict(result)

    def get_all_custom_luts(
        self, properties: dict, parent: str = None, result: dict = {}
    ):
        fields = properties.keys()
        for field in fields:
            obj = dict(properties[field])
            if not obj:
                continue
            if field == "lut":
                lut_model = obj["x-lut-ref"]
                result[f"{parent}_multi_lut"] = {
                    "model": lut_model,
                    "endpointPrefix": f"{lut_model}s",
                }
            if obj["type"] == "object":
                result = self.get_all_custom_luts(
                    properties=dict(obj["properties"]),
                    parent=field,
                    result=dict(result),
                )
                continue

            if obj["type"] == "array":
                object = dict(obj["items"])
                if "properties" not in object:
                    object = {}
                    object["properties"] = {}
                    object["properties"][field] = dict(obj["items"])
                result = self.get_all_custom_luts(
                    properties=dict(object["properties"]),
                    parent=field,
                    result=dict(result),
                )
                continue

        return dict(result)

    def get_associated_targets(
        self,
        model: str,
        deep_search: bool = False,
        skips: list = [],
        relations: dict = {},
    ):
        
        if model in skips:
            return dict(relations)
        targets = extensions.core_service.post_response(
            api=f"api/documentation/all_targets",
            params={"model": model, "deepSearch": True},
            use_cache=True,
            saved_response_key=utility.compute_md5_hash(value=f"api/documentation/all_targets_{model}")
        )
        if not targets:
            return dict(relations)
        if not isinstance(targets, dict):
            return dict(relations)

        attributes = dict(targets)
        skips.append(model)
        if not deep_search:
            return dict(attributes)
        for key, v in attributes.items():
            value = dict(v)
            if "lut" in value["model"]:
                relations[key] = {}
                relations[key] = dict(value)
                continue
            if value["model"] in skips:
                continue
            if value["type"] == "relation":
                relations = self.get_associated_targets(
                    model=value["model"],
                    deep_search=True,
                    skips=skips,
                    relations=dict(relations),
                )
        # if failed to resolve any custom luts
        schema_name = f"{humps.pascalize(model)}Request"
        properties = self.schemas[schema_name]["properties"]["data"]["properties"]
        custom_luts = self.get_all_custom_luts(properties=dict(properties))
        luts = custom_luts.keys()
        for k in luts:
            if k in relations:
                continue
            relations[k] = dict(custom_luts[k])
        return dict(relations)

    def get_target_field_name(self, model: str, target: str):
        targets = self.get_associated_targets(
            model=model, deep_search=False, skips=[], relations={}
        )
        if not targets:
            return None
        if not isinstance(targets, dict):
            return None

        attributes = dict(targets)
        for key, v in attributes.items():
            value = dict(v)
            if value["model"] != target:
                continue
            return key
        return None

    def generate_model_export_uuid(
        self,
        model: str,
        model_data: dict,
        uuid_map: dict = {},
        skips: list = [],
        result: dict = {},
    ):
        data = dict(model_data)
        if "abis_export_uuid" in data:
            if data["abis_export_uuid"]:
                return {
                    "data": data,
                    "result": dict(result),
                    "uuid_map": dict(uuid_map),
                }

        if model not in uuid_map:
            uuid_map[model] = {}

        if model not in result:
            result[model] = []
        id = data["id"]

        uuid = utility.generate_uuid()
        if str(id) in uuid_map[model]:
            uuid = uuid_map[model][str(id)]

        data["abis_export_uuid"] = uuid
        result[model].append({"id": id, "abis_export_uuid": uuid})
        uuid_map[model][str(id)] = uuid
        
        return {"data": dict(data), "result": dict(result), "uuid_map": dict(uuid_map)}

    def get_protocol_info(self, protocols: dict, prot_id: str):
        result = {
            "name": None,
            "protocol": None,
            "error": ""
        }
        approved_set = PROTOCOL_SETS.split(",")
        for p, v in protocols.items():
            if v["uuid"] != prot_id:
                continue
            result["name"] = p
            result["protocol"] = dict(v)

            if v["set"] not in approved_set:
                msg = v["set"] + "set is not enabled"
                if result["error"]:
                    msg = f"and {msg}"
                result["error"] = msg
                
            if not v["active"]:
                msg = f"{p} is disabled"
                if result["error"]:
                    msg = f"and {msg}"
                result["error"] = msg
            return dict(result)


        return dict(result)

    def create_protocol_relational_attributes(self, prot_name: str, models: list, protocol: dict):
        
        # checks already created or not
        targets_file_path = f"outputs/{prot_name}/targets.json"
        targets_file = utility.load_json_file(
            file_name=targets_file_path, include_root_dir=False
        )

        attributes_file_path = f"outputs/{prot_name}/attributes.json"
        attributes_file = utility.load_json_file(
            file_name=attributes_file_path, include_root_dir=False
        )
        if targets_file and attributes_file and not STORE_MOCK_HTTP_OUTPUT:
            flask.current_app.logger.warning(f"Found schema targets and attributes for protocol: {prot_name}")
            return
        
        # usually the first one is the survey model
        survey_model = models[0]
        target_models = [survey_model]
        if not targets_file:
            flask.current_app.logger.warning(f"Pulling all schema targets for protocol: {prot_name}")
            targets = self.get_associated_targets(
                model=survey_model, deep_search=True, skips=[], relations={}
            )
            for m in models:
                # update lut relations
                if m in target_models:
                    continue
                targets = self.get_associated_targets(
                    model=m,
                    deep_search=True,
                    relations=dict(targets),
                    skips=[survey_model],
                )
                target_models.append(m)

            # store model names and targets
            if targets:
                utility.store_file(
                    data=dict(targets),
                    protocol_name=prot_name,
                    file_name="targets",
                    file_type=FileType.JSON,
                )

        if not attributes_file:
            flask.current_app.logger.warning(f"Pulling all schema attributes for protocol: {prot_name}")
            attributes = self.get_all_attributes(model=survey_model, skips=[])
            for m in models:
                # update lut relations
                if m in target_models:
                    continue
                attributes = self.get_all_attributes(
                    model=m, skips=[survey_model], result=dict(attributes)
                )
                target_models.append(m)
            # store model names and attributes
            if attributes:
                utility.store_file(
                    data=dict(attributes),
                    protocol_name=prot_name,
                    file_name="attributes",
                    file_type=FileType.JSON,
                )

    def get_collection(self, org_uuid: str, protocols: dict, force: bool = False):
        collection_data = { "error": None }
        flask.current_app.logger.info(
            f"Finding metadata information for org_minted_uuid={org_uuid}"
        )
        # get information form org metadata table so that we don't need to reverse lookup if protocol is disabled
        uuid_metadata = extensions.core_service.get_response(
            api=f"api/org-uuid-survey-metadatas",
            query=f"populate=deep&use-default=true&filters[org_minted_uuid][$eq]={org_uuid}",
            use_cache=True,
        )
        if not uuid_metadata["data"]:
            collection_data["error"] = f"No metadata information found for org_uuid={org_uuid}"
            flask.current_app.logger.warning(collection_data["error"])
            return dict(collection_data)
        uuid_metadata = dict(uuid_metadata["data"][0]["attributes"])
        survey_model = uuid_metadata["survey_details"]["survey_model"]
        protocol_details = self.get_protocol_info(
            protocols=protocols,
            prot_id=uuid_metadata["survey_details"]["protocol_id"],
        )
        if protocol_details["error"]:
            collection_data["error"] = protocol_details["error"]
            flask.current_app.logger.warning(collection_data["error"])
            return dict(collection_data)
        
        protocol_name = protocol_details["name"]
        protocol = protocol_details["protocol"]
        if not protocol_name or not protocol:
            msg = f"missing protocol information, org_uuid={org_uuid}"
            collection_data["error"] = msg
            flask.current_app.logger.warning(msg)
            return dict(collection_data)

        flask.current_app.logger.info(
            f"Attempting reverse-lookup org_minted_uuid={org_uuid}"
        )
        data = extensions.core_service.post_response(
            api=f"api/protocols/reverse-lookup",
            params={"org_minted_uuid": org_uuid},
            use_cache=True,
            saved_response_key=org_uuid
        )
        if not isinstance(data, dict) or not data:
            collection_data["error"] = f"{data} for org_uuid={org_uuid}"
            flask.current_app.logger.warning(collection_data["error"])
            return dict(collection_data)
        
        flask.current_app.logger.debug(
            f"Processing org_minted_uuid={org_uuid}")

        collection = {}
        uuid_map = {}
        survey_fields = {}
        result = {}
        models = list(data["collections"].keys())

        collection_data["collection"] = collection
        collection_data["update_collection"] = result
        collection_data["uuid_map"] = uuid_map
        collection_data["protocol_name"] = protocol_name
        collection_data["protocol"] = protocol
        collection_data["models"] = models
    
        # checks already created or not
        if data["collections"][survey_model] and not force:
            output_name = utility.generate_output_name(
                protocol=protocol, 
                protocol_name=protocol_name, 
                survey=dict(data["collections"][survey_model])
            )
            if utility.is_output_already_created(output=f"{protocol_name}/{output_name}.ttl"):
                msg = f"Already exported, clear {output_name} from outputs/{protocol_name} to generate turtle file again or enable flag force"
                collection_data["error"] = msg
                flask.current_app.logger.warning(msg)

                return dict(collection_data)
        
        for model in models:
            if model == survey_model:
                if not data["collections"][model]:
                    msg = f"found empty {model}, org_uuid={org_uuid}"
                    collection_data["error"] = msg
                    return dict(collection_data)
                flask.current_app.logger.info(f"Fixing attributes for model: {model}")
                model_data = dict(
                    self.fix_attributes_pattern(
                        data=dict(data["collections"][model]), protocol=protocol
                    )
                )
                # it should not be an array
                output = self.generate_model_export_uuid(
                    model=model,
                    model_data=dict(model_data),
                    uuid_map=dict(uuid_map),
                    skips=[],
                    result=dict(result),
                )
                result = dict(output["result"])
                uuid_map = dict(output["uuid_map"])
                collection = dict(output["data"])
                collection["observations"] = {}
                continue
            # find survey field name so that we can delete survey from observation if exists
            if model not in survey_fields:
                survey_field = self.get_target_field_name(
                    model=model, target=survey_model
                )
                if survey_field:
                    survey_fields[model] = survey_field

            observations = []
            model_length = len(data["collections"][model])
            flask.current_app.logger.info(f"Fixing attributes and generating export uuid for model: {model}, model length {model_length}")
            for obs in data["collections"][model]:
                
                model_data = dict(
                    self.fix_attributes_pattern(
                        data=dict(obs), protocol=protocol)
                )
                output = self.generate_model_export_uuid(
                    model=model,
                    model_data=dict(model_data),
                    uuid_map=dict(uuid_map),
                    skips=[survey_model],
                    result=dict(result),
                )
                result = dict(output["result"])
                uuid_map = dict(output["uuid_map"])
                observation = dict(output["data"])
                # delete survey field
                if model in survey_fields:
                    del observation[survey_fields[model]]
                observations.append(observation)

            collection["observations"][model] = observations

        collection_data = {
            "collection": dict(collection),
            "update_collection": dict(result),
            "uuid_map": dict(uuid_map),
            "protocol_name": protocol_name,
            "protocol": dict(protocol),
            "models": models,
            "error": None
        }
        return dict(collection_data)

    def get_all_collections(
        self,
        protocol: dict,
        start_date: str,
        protocol_name: str,
        uuid_map: dict,
    ):

        survey_model = protocol["survey-model"]
        survey_query = f"populate=deep&use-default=true&filters[survey_metadata][survey_details][time][$gte]={
            start_date}"
        flask.current_app.logger.info(f"Populating survey: {survey_model}")
        org_uuids = []
        resp = extensions.core_service.get_response(
            api=f"api/{survey_model}s", query=survey_query
        )
        if "data" in resp:
            for d in resp["data"]:
                uuid = d["attributes"]["survey_metadata"]["orgMintedUUID"]
                if (
                    d["attributes"]["survey_metadata"]["survey_details"]["protocol_id"]
                    != protocol["uuid"]
                ):
                    continue
                if uuid and uuid not in org_uuids:
                    org_uuids.append(uuid)

        targets = self.get_associated_targets(
            model=survey_model, deep_search=True, skips=[], relations={}
        )
        attributes = self.get_all_attributes(model=survey_model, skips=[])
        collections = []
        update_collections = {}
        survey_fields = {}
        target_models = [survey_model]
        flask.current_app.logger.info(
            f"Found total {len(org_uuids)} orgMintedUUID(s), protocol: {
                protocol_name}"
        )
        uuid_with_record = 0
        for org_minted_uuid in org_uuids:
            testing = [
                "d37e8395-8aa6-4e3f-bae4-78fbc75ec2dd",  # plot selection test
                "3f50d678-0d45-4303-a235-77ac099bd169",  # plot selection staging
                "48f7ae1e-6785-4afe-8213-4da9c6355227",  # plot layout staging
                "9a3cceb0-192a-4a6c-96a8-3daadbe2668a",  # plot layout test
                "1c57d8f2-d37c-4e58-84cf-518b7ec1c7ef",  # plot description enhanced staging
                "b4aef641-4f89-4aaf-b182-3c455370b3cd",  # plot description enhanced test
                "1ee804d8-4fee-48df-b8cc-770542d02df2",  # plot description standard test
                "dde011f1-1a2d-47ce-8843-d525f6810558",  # floristic full test
                "e34a0058-82c6-47e7-9e2d-22037e941a41",  # opportune test
                "f4b6b130-fa9a-4a38-8408-14f3b934c496",  # floristic lite staging
                # fauna-ground-counts-transects testing
                "c1b922cf-41df-45e3-874d-36dcd5358397",
                "53c96375-688a-4e76-9b80-6c6317627acc",  # fauna-ground-counts-transects local
                "9711233a-c549-4650-8a19-201b508a5ad7",  # within plot belt transect test
                "996accf2-6cf3-4135-a3d1-bb45b2576a2b",  # fauna areal survey test
                # "416895ba-4d52-4ef3-be53-924c7d5d6bcd", # fauna areal survey desktop local
                "c2ee3f78-573b-4528-95cb-21df54693b39",  # fauna areal survey survey local
                "f540af25-8f69-46f9-9624-aaf4122ad4e3",  # off-plot-belt-transect test
                "e797ee8d-fd6b-401c-954b-fae3350f9e45",  # sign-based-fauna-vehicle-track test
                "e5380946-3eb7-4d1b-8756-b1cd88979f5e",  # sign-based-fauna-track-station test
                "962dd648-684d-40c5-b707-b784a92c7d42",  # sign-based-plot-sign-search test
            ]
            # if org_minted_uuid not in testing:
            #     continue

            flask.current_app.logger.debug(
                f"Attempting reverse-lookup org_minted_uuid={
                    org_minted_uuid}, protocol: {protocol_name}"
            )
            data = extensions.core_service.post_response(
                api=f"api/protocols/reverse-lookup",
                params={"org_minted_uuid": org_minted_uuid},
                use_cache=True,
            )

            if "No record found" in data:
                flask.current_app.logger.debug(
                    f"No record found for org_minted_uuid={org_minted_uuid}"
                )
                continue
            if "collections" not in data:
                flask.current_app.logger.debug(
                    f"No collections found for org_minted_uuid={
                        org_minted_uuid}"
                )
                continue
            flask.current_app.logger.debug(
                f"Processing org_minted_uuid={org_minted_uuid}"
            )
            uuid_with_record += 1
            # collections with valid orgMintedUuid
            collection = {}
            update_collections[org_minted_uuid] = {}
            result = {}
            models = list(data["collections"].keys())
            for model in models:
                if model == survey_model:
                    model_data = dict(
                        self.fix_attributes_pattern(
                            data=dict(data["collections"][model]), protocol=protocol
                        )
                    )
                    # it should not be an array
                    output = self.generate_model_export_uuid(
                        model=model,
                        model_data=dict(model_data),
                        uuid_map=dict(uuid_map),
                        skips=[],
                        result=dict(result),
                    )
                    result = dict(output["result"])
                    uuid_map = dict(output["uuid_map"])
                    collection = dict(output["data"])
                    collection["observations"] = {}
                    continue
                # find survey field name so that we can delete survey from observation if exists
                if model not in survey_fields:
                    survey_field = self.get_target_field_name(
                        model=model, target=survey_model
                    )
                    if survey_field:
                        survey_fields[model] = survey_field

                observations = []
                for obs in data["collections"][model]:
                    model_data = dict(
                        self.fix_attributes_pattern(
                            data=dict(obs), protocol=protocol)
                    )
                    output = self.generate_model_export_uuid(
                        model=model,
                        model_data=dict(model_data),
                        uuid_map=dict(uuid_map),
                        skips=[survey_model],
                        result=dict(result),
                    )
                    result = dict(output["result"])
                    uuid_map = dict(output["uuid_map"])
                    observation = dict(output["data"])
                    # delete survey field
                    if model in survey_fields:
                        del observation[survey_fields[model]]
                    observations.append(observation)

                # update lut relations
                if model not in target_models:
                    targets = self.get_associated_targets(
                        model=model,
                        deep_search=True,
                        relations=dict(targets),
                        skips=[survey_model],
                    )
                    attributes = self.get_all_attributes(
                        model=model, skips=[
                            survey_model], result=dict(attributes)
                    )
                    target_models.append(model)

                collection["observations"][model] = observations
            update_collections[org_minted_uuid] = dict(result)
            collections.append(collection)
        # store model names and targets
        utility.store_file(
            data=dict(targets),
            protocol_name=protocol_name,
            file_name="targets",
            file_type=FileType.JSON,
        )

        # store model names and attributes
        utility.store_file(
            data=dict(attributes),
            protocol_name=protocol_name,
            file_name="attributes",
            file_type=FileType.JSON,
        )
        flask.current_app.logger.info(
            f"Found total {uuid_with_record} orgMintedUUID(s) with records"
        )
        return {
            "collections": collections,
            "update_collections": dict(update_collections),
            "uuid_map": dict(uuid_map),
        }

    def populate_all_data(
        self,
        model_names: List[str],
        protocol: dict,
        filter_key: str = None,
        fixed_pattern: bool = True,
        query: str = None,
    ):
        filter = "populate=deep&use-default=true"
        if query:
            filter = filter + "&" + query
        all_models_data = {}
        for model in model_names:
            model_data = extensions.core_service.get_response(
                api=f"api/{model}s", query=filter
            )
            all_models_data[model] = model_data["data"]
        # removes attributes and data fields recursively
        if fixed_pattern:
            all_models_data = dict(
                self.fix_attributes_pattern(
                    data=all_models_data, protocol=protocol)
            )
        if filter_key:
            possible_keys = utility.all_possible_keys(key=filter_key)
            filtered_data = {}

            all_data = dict(all_models_data)
            all_keys = list(all_data.keys())
            for key in all_keys:
                filtered_data[key] = []
                for data in all_data[key]:
                    object = dict(data)
                    valid_data = utility.find_object_by_key(
                        object=object, keys=possible_keys
                    )
                    if not valid_data:
                        continue
                    if not valid_data["object"]:
                        continue

                    filtered_data[key].append(object)
            return filtered_data
        return all_models_data

    def assign_all_plot_uri(
        self,
        data: typing.Optional[dict],
        protocol: dict,
        property_metadata: dict,
        protocol_name: str,
    ):
        # all plot attributes
        for uri in PLOT_ATTRIBUTE_URI:
            data = self.assign_all_uri(
                data=data,
                collection_uri=uri,
                uri_name="plot_attribute_uri",
                protocol=protocol,
                property_metadata=dict(property_metadata),
                protocol_name=protocol_name,
            )

        # all plot properties
        for uri in PLOT_PROPERTIES_URI:
            data = self.assign_all_uri(
                data=data,
                collection_uri=uri,
                uri_name="plot_property_uri",
                protocol=protocol,
                property_metadata=dict(property_metadata),
                protocol_name=protocol_name,
            )

        return data

    def get_property_metadata(self, protocol: dict, protocol_name: str):
        collection_uri = protocol["uri"]["properties"]
        if not collection_uri:
            return {}
        cache_key = protocol["uuid"] + collection_uri
        metadata = utility.get_cache_data(
            caching=extensions.redis_caching, key=cache_key
        )
        if metadata:
            return metadata

        property_metadata = {}
        published_properties = extensions.graph_service.graph_db_observable_properties(
            collection_uri=collection_uri
        )
        if not published_properties:
            return property_metadata

        linkedUris = []
        for p in published_properties:
            p_uri = p["concept"]["value"]
            label = p["label"]["value"]

            attr = extensions.graph_service.get_attribute(
                property_uri=p_uri, property_collection_uri=collection_uri
            )
            if attr:
                values = []
                attributes = []
                for a in attr:
                    if not a:
                        continue
                    if "attribute" not in a:
                        continue
                    if "type" not in a["attribute"]:
                        continue
                    if a["attribute"]["type"] != "uri":
                        continue
                    attr_prop = extensions.graph_service.sparql_query(
                        collection_uri=a["attribute"]["value"]
                    )
                    for ap in attr_prop:
                        if "title" not in ap:
                            continue
                        if "value" not in ap["title"]:
                            continue
                        if "prefLabel" not in ap["title"]["value"]:
                            continue
                        if "value" not in ap:
                            continue
                        values.append(
                            {a["attribute"]["value"]: ap["value"]["value"]})
                        attributes.append(a["attribute"]["value"])
                        if a["attribute"]["value"] not in linkedUris:
                            linkedUris.append(a["attribute"]["value"])
                property_metadata[p_uri] = {}
                property_metadata[p_uri]["name"] = label
                property_metadata[p_uri]["attributes"] = attributes
                property_metadata[p_uri]["values"] = values

        property_metadata["allAttributes"] = linkedUris
        property_metadata["protocol"] = protocol_name
        utility.store_cache_data(
            caching=extensions.redis_caching,
            key=cache_key,
            value=dict(property_metadata),
        )
        # an empty file so that we can update later
        utility.store_file(
            data={},
            protocol_name=protocol_name,
            file_name="relations",
            file_type=FileType.JSON,
        )
        return dict(property_metadata)

    def get_field_unit(self, nrm_field: str):
        field_map = utility.get_names_map_data()
        tern_unit = field_map["tern_unit"]
        for unit, value in tern_unit.items():
            if nrm_field not in value:
                continue
            return f"tern_unit:unit:{unit}:tern_unit"
        return None

    # assigns observable properties.
    def assign_all_uri(
        self,
        data: typing.Optional[dict],
        collection_uri: str,
        uri_name: str,
        protocol_name: str,
        property_metadata: dict = {},
        protocol: dict = None,
    ):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return
        
        start_time = datetime.now()
        published_properties = extensions.graph_service.graph_db_observable_properties(
            collection_uri=collection_uri
        )

        data = self.assign_uri(
            data=data,
            properties=published_properties,
            uri_name=uri_name,
            protocol=protocol,
            property_metadata=dict(property_metadata),
            protocol_name=protocol_name,
        )

        if "extra-fields" in protocol:
            attributes_file_path = f"outputs/{protocol_name}/extra_fields.json"
            extra_fields = utility.load_json_file(
                file_name=attributes_file_path, include_root_dir=False
            )
            extra_fields = self.assign_uri(
                data=dict(extra_fields),
                properties=published_properties,
                uri_name=uri_name,
                protocol=protocol,
                property_metadata=dict(property_metadata),
                protocol_name=protocol_name,
            )
            utility.store_file(
                data=dict(extra_fields),
                protocol_name=protocol_name,
                file_name="extra_fields",
                file_type=FileType.JSON,
            )
        diff = datetime.now() - start_time
        flask.current_app.logger.info(
            f"took time: {diff.total_seconds()}s to pull all {uri_name}s for protocol: {protocol_name}"
        )
        return data

    # If no uri found we generate one for the property
    def assign_uri(
        self,
        data: any,
        properties: dict,
        uri_name: str,
        protocol_name: str,
        property_metadata: dict = {},
        key: str = None,
        protocol: dict = None,
    ):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return
        
        data = self.set_custom_flags(
            data=data, key=key, protocol=dict(protocol))

        # some protocols have values with different data types.
        # e.g. insect_damage is a lut but in BDR its a string.
        # so we skip unmatched fields.
        if self.is_skip_adding(key=key, protocol=dict(protocol)):
            return data

        if isinstance(data, List):
            # data = self.set_custom_flags(data=data, key=key, configs=configs)
            updated_objects = []
            for obj in data:
                updated_objects.append(
                    self.assign_uri(
                        data=dict(obj),
                        properties=properties,
                        uri_name=uri_name,
                        protocol=protocol,
                        property_metadata=dict(property_metadata),
                        protocol_name=protocol_name,
                    )
                )
            return updated_objects

        # find uri
        uri = self.find_uri(
            key=key,
            protocol_name=protocol_name,
            uri_name=uri_name,
            properties=properties,
            protocol=protocol,
            property_metadata=dict(property_metadata),
        )

        # if uri:
        #     flask.current_app.logger.debug(f"Adding {uri_name} to the field: {key}")


        if isinstance(data, dict):
            updated_objects = dict(data)
            if uri and not self.is_skip_adding_uri(key=key, protocol=dict(protocol)):
                updated_objects[uri_name] = uri["uri"]
                updated_objects["nrm_label"] = uri["nrm_label"]
                updated_objects["protocol"] = protocol_name
                api_source = self.get_field_source(
                    key=key, protocol_name=protocol_name)
                if api_source:
                    updated_objects["api_source"] = api_source
                self.add_diff_metadata(
                    field=key,
                    nrm_label=uri["nrm_label"],
                    uri_type=uri_name,
                    uri=uri["uri"],
                    protocol_name=protocol_name,
                    data=data,
                )
                if "feature_type" in uri:
                    updated_objects["feature_type"] = uri["feature_type"]
                # if relationships between observation and attributes exist
                uri_metadata = self.get_uri_metadata(
                    uri=uri["uri"],
                    property_metadata=dict(property_metadata),
                    data=dict(updated_objects),
                )
                if uri_metadata:
                    updated_objects["status"] = uri_metadata["status"]
                return updated_objects
            
            keys = list(updated_objects.keys())
            for key in keys:
                updated_objects[key] = self.assign_uri(
                    data=updated_objects[key],
                    key=key,
                    properties=properties,
                    uri_name=uri_name,
                    protocol=protocol,
                    property_metadata=dict(property_metadata),
                    protocol_name=protocol_name,
                )

            return updated_objects

        if not uri:
            return data

        if data == None:
            return data
        
        if self.is_skip_adding_uri(key=key, protocol=dict(protocol)):
            return data

        object = {}
        if isinstance(data, dict):
            object = dict(data)
        if not isinstance(data, dict):
            object["value"] = data
        object[uri_name] = uri["uri"]
        object["nrm_label"] = uri["nrm_label"]
        object["protocol"] = protocol_name
        api_source = self.get_field_source(
            key=key, protocol_name=protocol_name)
        if api_source:
            updated_objects["api_source"] = api_source
        self.add_diff_metadata(
            field=key,
            nrm_label=uri["nrm_label"],
            uri_type=uri_name,
            uri=uri["uri"],
            protocol_name=protocol_name,
            data=data,
        )
        if "feature_type" in uri:
            object["feature_type"] = uri["feature_type"]

        # if relationships between observation and attributes exist
        uri_metadata = self.get_uri_metadata(
            uri=uri["uri"], property_metadata=dict(property_metadata), data=dict(object)
        )
        if uri_metadata:
            object["status"] = uri_metadata["status"]

        return object

    def get_field_source(self, key: str, protocol_name: str):
        filename = f"outputs/{protocol_name}/targets.json"
        targets = utility.load_json_file(
            file_name=filename, include_root_dir=False)
        field = key
        if "__" in key:
            field = key.split("__")[-1]
        if field not in list(targets.keys()):
            return None
        # if source exists
        endpointPrefix = targets[field]["endpointPrefix"]
        return f"{VOCAB_SERVER}/api/{endpointPrefix}"

    def add_diff_metadata(
        self,
        field: str,
        nrm_label: dict,
        uri_type: str,
        uri: str,
        protocol_name: str,
        data: any,
    ):
        filename = f"outputs/{protocol_name}/{protocol_name}-diffs.json"
        diffs = utility.load_json_file(
            file_name=filename, include_root_dir=False)

        monitor_field = field.lower().replace("_", "").replace("-", "").replace(" ", "")
        nrm_field = nrm_label.lower().replace("_", "").replace("-", "").replace(" ", "")
        if monitor_field == nrm_field:
            return

        field_type = None
        if "attribute" in uri_type:
            field_type = "attributes"
        if "property" in uri_type:
            field_type = "properties"

        if not field_type:
            return
        if monitor_field == nrm_field:
            return

        if "incorrect_fields" not in diffs[field_type]:
            diffs[field_type]["incorrect_fields"] = []
        if "diffs" not in diffs[field_type]:
            diffs[field_type]["diffs"] = []

        diff = {}
        diff["monitor_field"] = field
        nrm_fields = []
        nrm_fields.append(nrm_label)
        trimmed = []
        trimmed.append(nrm_field)
        nrm_data_type = None
        if nrm_label not in diffs[field_type]["incorrect_fields"]:
            properties = extensions.graph_service.sparql_query(
                collection_uri=uri)
            for p in properties:
                if "title" not in p:
                    continue
                if "value" not in p:
                    continue
                if "value" not in p["title"]:
                    continue
                if "value" not in p["value"]:
                    continue

                if "altLabel" in p["title"]["value"]:
                    nrm_fields.append(p["value"]["value"])
                if "valueType" in p["title"]["value"]:
                    nrm_data_type = p["value"]["value"].split("/")[-1]
                trimmed.append(
                    p["value"]["value"]
                    .replace("_", "")
                    .replace("-", "")
                    .replace(" ", "")
                )
            if monitor_field in trimmed:
                return

            diffs[field_type]["incorrect_fields"].append(nrm_label)
            diff["nrm_fields"] = nrm_fields
            diff["monitor_data_type"] = utility.get_value_type(
                field=field,
                value=data,
                object={"value": data, "protocol": protocol_name},
            )
            diff["nrm_data_type"] = nrm_data_type
            diff["concept_uri"] = uri
            diffs[field_type]["diffs"].append(diff)
            # import json
            # flask.current_app.logger.error(
            #     f"Failed to map {field}, protocol_name: {protocol_name}, context: {json.dumps(dict(diff))}",
            # )
            utility.store_file(
                data=dict(diffs),
                protocol_name=protocol_name,
                file_name=f"{protocol_name}-diffs",
                file_type=FileType.JSON,
            )

    def get_uri_metadata(self, uri: str, property_metadata: dict, data: dict):
        metadata = dict(property_metadata)
        linked_obs = list(metadata.keys())
        linked_attrs = []
        if "allAttributes" in property_metadata:
            linked_attrs = property_metadata["allAttributes"]
        if uri not in linked_obs and uri not in linked_attrs:
            return None

        object = {}

        if uri in linked_obs:
            object["status"] = "hasAttributes"
            return dict(object)

        if uri in linked_attrs:
            object["status"] = "linkedToObservation"
            filename = "outputs/" + metadata["protocol"] + "/relations.json"
            relations = utility.load_json_file(
                file_name=filename, include_root_dir=False
            )
            # if "observations" not in relations:
            #     relations["observations"] = {}
            for obs in linked_obs:
                if "attributes" not in metadata[obs]:
                    continue
                if uri not in metadata[obs]["attributes"]:
                    continue
                for v in metadata[obs]["values"]:
                    if uri not in v:
                        continue

                    if uri not in relations:
                        relations[uri] = []
                    # if obs not in relations["observations"]:
                    #     relations["observations"][obs] = {}
                    #     relations["observations"][obs]["attributes"] = []
                        # relations["observations"][obs]["values"] = []

                    if obs in relations[uri]:
                        continue
                    # relations["observations"][obs]["name"] = metadata[obs]["name"]
                    relations[uri].append(obs)
                    # relations["observations"][obs]["values"].append(dict(data))
            utility.store_file(
                data=dict(relations),
                protocol_name=property_metadata["protocol"],
                file_name="relations",
                file_type=FileType.JSON,
            )
            return dict(object)

        return object

    def is_skip_adding(self, key: str, protocol: dict):
        self.names_map = utility.get_names_map_data()

        # if protocol has the list of skip fields
        skip_fields = []
        if "skip-fields" in protocol:
            skip_fields = protocol["skip-fields"]
        if "custom-configs" in protocol:
            custom_configs = dict(protocol["custom-configs"])
            if "skip-fields" in custom_configs:
                skip_fields = custom_configs["skip-fields"]

        skip_fields = skip_fields + self.names_map["global_ignore"]
        if not skip_fields:
            return False

        possible_keys = utility.all_possible_keys(key=key)
        for field in skip_fields:
            if field not in possible_keys:
                continue
            return True
        return False

    def is_skip_adding_uri(self, key: str, protocol: dict):
        self.names_map = utility.get_names_map_data()
        skip_fields = self.names_map["global_skip_adding_uri"]
        
        if "custom-configs" in protocol:
            custom_configs = dict(protocol["custom-configs"])
            if "skip-adding-uri" in custom_configs:
                skip_fields = skip_fields + custom_configs["skip-adding-uri"]

        if not skip_fields or not isinstance(skip_fields, list):
            return False

        possible_keys = utility.all_possible_keys(key=key)
        for field in skip_fields:
            if field not in possible_keys:
                continue
            return True
        return False

    def set_custom_flags(self, data: any, key: str, protocol: dict):
        self.names_map = utility.get_names_map_data()

        if data == None:
            return data

        prefix = "handle"
        object = {}
        if isinstance(data, dict):
            object = dict(data)
        if not isinstance(data, dict):
            object["value"] = data

        # handle global handler e.g. "handle-observer"
        global_handlers = self.names_map["global_handlers"]
        for global_handler_key in global_handlers:

            if isinstance(data, dict) and global_handler_key in data:
                return data

            # e.g if handle is "handle-equipment" then attribute key should be equipment_attributes
            attributes_key = global_handler_key.replace(
                "handle-", "").replace("-", "_")
            attributes_key = f"{attributes_key}_attributes"
            if key in self.names_map[attributes_key]:
                object[global_handler_key] = True
                return dict(object)

        if "custom-configs" not in protocol:
            return data
        custom_configs = dict(protocol["custom-configs"])
        custom_config_keys = list(custom_configs.keys())
        for custom_key in custom_config_keys:
            if isinstance(data, dict) and custom_key in data:
                return data
            if prefix not in custom_key:
                continue
            if data == None:
                continue
            # e.g if handle is "handle-equipment" then attribute key should be equipment_attributes
            attributes_key = custom_key.replace(
                "handle-", "").replace("-", "_")
            attributes_key = f"{attributes_key}_concepts"
            if attributes_key not in self.names_map:
                continue

            if key in self.names_map[attributes_key]:
                # special cases
                if isinstance(data, dict):
                    if "voucher_full" in data:
                        continue
                    if "voucher_lite" in data:
                        continue
                object[custom_key] = True
                return dict(object)

        return data

    def find_uri(
        self, key: str, protocol_name: str, uri_name: str,  properties: dict, protocol: dict, property_metadata: dict
    ):
        if not properties:
            return None
        
        symantec_file = f"outputs/{protocol_name}/symantec_{uri_name}.json"
        symantec_properties = utility.load_json_file(file_name=symantec_file, include_root_dir=False)
        if not symantec_properties: symantec_properties = {}

        if key in symantec_properties:
            return dict(symantec_properties[key])
        
        # assign property uri if has any
        property_uri = self.find_property_uri(
            key=key,
            properties=properties,
            protocol=protocol,
            property_metadata=dict(property_metadata),
        )

        if property_uri:
            symantec_properties[key] = dict(property_uri)
            utility.store_file(
                data=dict(symantec_properties),
                protocol_name=protocol_name,
                file_name=f"symantec_{uri_name}",
                file_type=FileType.JSON,
            )
            return property_uri
        return None

    def find_property_uri(
        self, key: str, properties: dict, protocol: dict, property_metadata: dict
    ):
        if not properties or not protocol:
            return None
        possible_keys = utility.all_possible_keys(key=key)

        for property in properties:
            if not property:
                continue
            if property["label"]["value"] not in possible_keys:
                continue

            # returns feature type info and uri
            uri_object = {}
            uri_object["uri"] = property["concept"]["value"]
            uri_object["nrm_label"] = property["label"]["value"]

            if utility.is_null_or_empty(object=protocol, field="feature-types"):
                return uri_object

            # find associated feature type
            isMultiple = False
            if "feature-add-multiple" in protocol:
                isMultiple = protocol["feature-add-multiple"]

            feature_type = self.find_feature_type(
                key=property["label"]["value"],
                properties=properties,
                protocol=dict(protocol),
                isMultiple=isMultiple,
            )
            if feature_type:
                if isinstance(feature_type, list):
                    uri_object["feature_type"] = feature_type
                if isinstance(feature_type, dict):
                    uri_object["feature_type"] = [dict(feature_type)]

            return uri_object

        return None

    def find_feature_type(
        self,
        key: str,
        properties: dict,
        protocol: dict,
        isMultiple: bool = False,
    ):
        f_types = []
        f_type = {}

        p_f_types = []
        p_f_type = {}

        feature_types = []
        priorities = []
        if "feature-types" in protocol:
            feature_types = protocol["feature-types"]
        if "priority-feature-types" in protocol:
            priorities = protocol["priority-feature-types"]

        for property in properties:
            updated_property = dict(property)
            if updated_property["label"]["value"] != key:
                continue

            if "featureType" not in updated_property:
                continue
            f_value = updated_property["featureTypeLabel"]["value"]
            f_uri = property["featureType"]["value"]
            obj = {"value": f_value, "uri": f_uri}

            if f_value in priorities:
                if not p_f_type and not isMultiple:
                    p_f_type = dict(obj)
                p_f_types.append(dict(obj))

            if f_value in feature_types:
                if not f_type and not isMultiple:
                    f_type = dict(obj)
                f_types.append(dict(obj))

        if not isMultiple:
            if p_f_type:
                return p_f_type
            if f_type:
                return f_type

        if p_f_types:
            return p_f_types
        if f_types:
            return f_types
        return {}

    def all_possible_keys(self, key: str):
        possible_keys = []

        if key:
            possible_keys.append(key)
            # cloud_cover can be cloud cover as well
            if "_" in key:
                possible_keys.append(key.replace("_", " "))
                possible_keys.append(key.replace("_", "-"))
            if "-" in key:
                possible_keys.append(key.replace("-", " "))
                possible_keys.append(key.replace("-", "_"))

        self.names_map = utility.get_names_map_data()

        for field in list(self.names_map.keys()):
            if field in possible_keys:
                # fauna_maturity can be maturity or species can be field species name
                possible_keys = possible_keys + self.names_map[field]

        return possible_keys

    def object_exist_in_objects(
        self, objects: typing.Optional[dict], survey_name: str, id: str
    ):
        # find matched objects
        matched_objects = []
        for object in objects:
            if object["attributes"][survey_name]["data"]["id"] != id:
                continue
            matched_objects.append(object)
        return matched_objects

    def remove_field_from_object(self, objects: any, field: str):
        if isinstance(objects, dict):
            updated_objects = dict(objects)
            if field in updated_objects:
                del updated_objects[field]

            keys = list(updated_objects.keys())
            for key in keys:
                updated_objects[key] = self.remove_field_from_object(
                    objects=updated_objects[key], field=field
                )

            return updated_objects

        if isinstance(objects, List):
            updated_objects = []
            for obj in objects:
                updated_objects.append(
                    self.remove_field_from_object(
                        objects=dict(obj), field=field)
                )
            return updated_objects

        return objects

    # get all the observation models from workflow
    def get_all_observation_models(
        self,
        survey_model: str,
        workflow: typing.Optional[dict],
        protocol_info: dict = None,
    ):
        if not self.enabled:
            flask.current_app.logger.warning(f"Services are not enabled")
            return

        if not workflow:
            return None

        models = []
        for obj in workflow:
            if not obj:
                continue
            if "modelName" not in obj:
                continue
            # as survey model and obs model should be different
            if obj["modelName"] == survey_model:
                continue
            if obj["modelName"] in PLOT_MODELS:
                continue

            if "custom-configs" in protocol_info:
                config = protocol_info["custom-configs"]
                if "skip-obs-model" in config:
                    if obj["modelName"] in config["skip-obs-model"]:
                        continue

            models.append(obj["modelName"])

        # remove additional models from the List of observations
        if not protocol_info:
            return models
        if utility.is_null_or_empty(object=protocol_info, field="additional-models"):
            return models
        for add_model in protocol_info["additional-models"]:
            if add_model["model"] not in models:
                continue
            models.remove(add_model["model"])

        return models

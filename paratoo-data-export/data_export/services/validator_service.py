import flask
import jsonschema
import pyshacl
import os
from werkzeug.exceptions import BadRequest
from jsonschema.exceptions import ValidationError

import data_export.extensions as extensions
from data_export.utilities import Utilities as utility
from data_export.constants import (
    OUTPUT_TTL,
    TERN_ONTOLOGY_TTL,
    RDF_FORMAT,
    RESOURCE_DIRECTORY,
)


class APIException(Exception):
    """We use this to indicate a generic issue with a remote API. Generates a 400 if not caught"""

    def __init__(self, message):
        super().__init__(message)


class ValidatorService:
    DEFAULT_TIMEOUT = 5  # seconds

    def __init__(self, app=None):
        self.enabled = True

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.enabled = app.config.get("SERVICE_ENABLED", False)
        self.enable_shacl = app.config.get("ENABLE_SHACL_VALIDATOR", False)
        if self.enabled:
            self.core_url = app.config["CORE_URL"]
            self.org_url = app.config["ORG_URL"]

    def validate_json_schema(self, schema, payload_expected=True):
        """
        Helper to validate json schema and perform basic non-null checks
        """
        r = flask.request.get_json()
        if payload_expected:
            if not r:
                raise BadRequest("JSON payload expected")
        else:
            if r is None:
                return {}

        # Now apply json schema validation
        try:
            jsonschema.validate(instance=r, schema=schema)
        except ValidationError as e:
            raise BadRequest("Invalid payload format - %s" % e.message)
        return r

    def validate_data(self, protocol: dict, protocol_name: str, collection_index: int):
        """
        Helper to validate ttl with tern ontology
        """
        if not self.enable_shacl:
            os.remove(OUTPUT_TTL)
            return {
                "conforms": "shacl validator is disabled",
                "report_text": None,
                "report_graph": None,
            }
        try:
            # tern ontology
            (
                conforms,
                report_graph,
                report_text,
            ) = pyshacl.validate(
                OUTPUT_TTL,
                advanced=True,
                shacl_graph=f"{RESOURCE_DIRECTORY}/{TERN_ONTOLOGY_TTL}",
                data_graph_format=RDF_FORMAT,
                shacl_graph_format=RDF_FORMAT,
                inference="rdfs",
                debug=False,
                serialize_report_graph=True,
            )

            flask.current_app.logger.info(f"Shacle Shape: Tern Ontology, report: {report_text}")
            if not conforms:
                os.remove(OUTPUT_TTL)
                msg = "Failed to validate output with tern ontology"
                return {"Message": msg, "Reports": utility.pretty_print_error_report(report_text=report_text)}

            # protocol shapes
            # if "protocol_shape" in protocol:
            #     shape_file = utility.get_shape_file(dir=protocol["protocol_shape"])
            #     (
            #         conforms,
            #         report_graph,
            #         report_text,
            #     ) = pyshacl.validate(
            #         OUTPUT_TTL,
            #         advanced=True,
            #         shacl_graph=shape_file,
            #         data_graph_format=RDF_FORMAT,
            #         shacl_graph_format=RDF_FORMAT,
            #         inference="rdfs",
            #         debug=False,
            #         serialize_report_graph=True,
            #     )
            #     flask.current_app.logger.info(f"Shacle Shape: Protocol Shape, report: {report_text}")
            #     if not conforms:
            #         os.remove(OUTPUT_TTL)
            #         msg = "Failed to validate turtle with protocol shape"
            #         return {"Message": msg, "Reports": utility.pretty_print_error_report(report_text=report_text)}

            flask.current_app.logger.info(
                f"Successfully validated {protocol_name} collection index={collection_index} at the project, protocol, and observation levels"
            )
            os.remove(OUTPUT_TTL)
            return None
        except Exception as e:
            os.remove(OUTPUT_TTL)
            raise BadRequest(f"Failed to validate output with pyshacl - {e}")

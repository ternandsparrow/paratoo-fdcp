"""Extensions module. Each extension is initialized in the app factory located in app.py."""
from flasgger import Swagger
from flask_caching import Cache

import data_export.services.export_service as exp_service
import data_export.services.validator_service as v_service
import data_export.services.core_service as c_service
import data_export.services.org_service as o_service
import data_export.services.protocol_service as p_service
import data_export.services.graph_service as g_service
import data_export.services.turtle_service as t_service
import data_export.services.aws_service as a_service
import data_export.services.webdriver_service as w_service

# all static instances
swagger = Swagger()
redis_caching = Cache()

export_service = exp_service.ExportService()
validator_service = v_service.ValidatorService()
core_service = c_service.CoreService()
org_service = o_service.OrgService()
protocol_service = p_service.ProtocolService()
graph_service = g_service.GraphService()
turtle_service = t_service.TurtleService()
aws_service = a_service.AWSService()
web_driver_service = w_service.WebDriverService()

#!/bin/bash
echo "Running Paratoo load tester"
# dev instance can handle only 200 user concurrently
# TODO: needs new instance
# dashoard http://3.27.228.61:3000/d/kQby-RnIz/k6-load-testing-results?orgId=1&refresh=5s&var-Measurement=All
# k6 run --vus 200 --iterations 200 --out influxdb=http://3.27.228.61:8086 ./load-test/loadTesting.js

# no browser to test frontend
# k6 run --vus 200 --iterations 200 ./load-test/loadTesting.js

# curl --location --request POST 'http://localhost:1337/api/protocols/test-token/merit' \
# --header 'Authorization: Bearer 1234'
# headless browser to test frontend
K6_BROWSER_HEADLESS=true k6 run -e TEST_CORE_AUTH_TOKEN=1234 ./load-test/loadTestingBrowser.js
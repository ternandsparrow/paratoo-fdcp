import http from 'k6/http'
import { check, group } from 'k6'
import { Rate } from 'k6/metrics'
import { v4 as uuidv4 } from 'uuid'

// flag to enable dev
const ENABLE_DEV = true

// url
const WEBAPP_URL = ENABLE_DEV
  ? 'https://dev.app.paratoo.tern.org.au/'
  : 'http://localhost:8080'
const ORG_URL = ENABLE_DEV
  ? 'https://dev.org-api.paratoo.tern.org.au'
  : 'http://localhost:1338'
const CORE_URL = ENABLE_DEV
  ? 'https://dev.core-api.paratoo.tern.org.au'
  : 'http://localhost:1337'

const LOGIN_USER = {
  identifier: 'TestUser',
  password: 'password',
}

const DISABLE_TEST = {
  WEBAPP_URL: false,
  ORG_URL: false,
  CORE_URL: false,
}
export let options = {
  stages: [
      // Ramp-up from 1 to 30 VUs in 30s
      { duration: "30s", target: 200 },

      // Stay on 30 VUs for 60s
      { duration: "60s", target: 200 },

      // Ramp-down from 30 to 0 VUs in 10s
      { duration: "10s", target: 0 }
  ]
}

// agent info
const params = {
  'sec-ch-ua':
    '"Chromium";v="94", "Google Chrome";v="94", ";Not A Brand";v="99"',
  'accept-encoding': 'gzip, deflate, br',
  'accept-language': 'en-GB,en;q=0.9',
}

// custom metrics
let successfulLogins = new Rate('paratoo_successful_logins')
let successBulkRequest = new Rate('paratoo_successful_bulk_request')
let tooManyRequest = new Rate('paratoo_too_many_request_error')
let checkFailureRate = new Rate('paratoo_check_failure_rate')

// the main function is what the virtual users will loop over during test execution.
export default function () {
  // fetches the homepage, along with resources embedded into the page
  group('Frontend', function () {
    if (DISABLE_TEST.WEBAPP_URL) return

    // loads html contents
    group('Contents', function () {
      let res = http.get(WEBAPP_URL, params)

      let checkRes = check(res, {
        "Title 'Monitor' exists": (r) =>
          r.body ? r.body.indexOf('Monitor') !== -1 : false,
      })

      // Record check failures
      checkFailureRate.add(!checkRes)
    })

    // Load some static assets
    group('Assets', function () {
      let res = http.batch([
        [
          'GET',
          WEBAPP_URL + '/icons/dev-icon-128x128.png',
          {},
          { tags: { staticAsset: 'yes' } },
        ],
        [
          'GET',
          WEBAPP_URL + '/icons/apple-launch-828x1792.png',
          {},
          { tags: { staticAsset: 'yes' } },
        ],
        [
          'GET',
          WEBAPP_URL + '/icons/apple-launch-1668x2224.png',
          {},
          { tags: { staticAsset: 'yes' } },
        ],
        [
          'GET',
          WEBAPP_URL + '/icons/apple-launch-2048x2732.png',
          {},
          { tags: { staticAsset: 'yes' } },
        ],
      ])

      // Record check failures
      let checkRes = check(res[0], {
        // as we know the size of dev-icon-128x128.png
        'size of icon dev-icon-128x128.png is 12336 bytes': (r) =>
          r.body ? r.body.length > 100 : false,
      })
      checkFailureRate.add(!checkRes)

      checkRes = check(res[0], {
        'size of icon apple-launch-828x1792.png is 15608 bytes': (r) =>
          r.body ? r.body.length > 100 : false,
      })
      checkFailureRate.add(!checkRes)
    })
  })

  // calls backend apis
  group('Backend', function () {
    let AUTH_TOKEN = ''
    let ALL_PROTOCOLS = []

    // calls org apis
    group('Paratoo org', function () {
      if (DISABLE_TEST.ORG_URL) return

      let res = http.post(ORG_URL + '/api/auth/local', LOGIN_USER)
      // console.log('body  ' + JSON.stringify(res.body))
      if (res.status != 200)
        console.log('org /api/auth/local res ' + JSON.stringify(res.body))
      let keys = res.body ? Object.keys(JSON.parse(res.body)) : []

      // check whether jwt present or not
      console.log('Authenticating...')
      const jwt = res.body ? JSON.parse(res.body).jwt : ''
      let checkRes = check(res, {
        '(api/auth/local) jwt present in auth response': (r) =>
          keys.includes('jwt'),
      })
      // if new jwt exists
      successfulLogins.add(checkRes)

      // if no jwt present, we use the old one
      // we store token as global so that we can use
      //   that in core
      AUTH_TOKEN = jwt ? jwt : AUTH_TOKEN

      // sometimes it fails to get jwt
      //   because of sending to many request
      tooManyRequest.add(keys.includes('message'))

      // header with token
      const options = {
        headers: {
          Authorization: `Bearer ${AUTH_TOKEN}`,
        },
      }

      res = http.get(ORG_URL + `/api/org/user-projects`, options)
      if (res.status != 200)
        console.log('org /api/org/user-projects res ' + JSON.stringify(res.body))
      // successful response has projects filed
      let body = JSON.parse(res.body)
      let projects = body.projects ? body.projects : []

      // check whether the total number of projects is greater then zero
      checkRes = check(res, {
        '(api/user-projects) more than one project found': (r) =>
          projects.length > 0,
      })
      checkFailureRate.add(!checkRes)

      // check whether the Kitchen Sink TEST Project exists or not
      const index = projects.findIndex(
        (x) => x.name === 'Kitchen Sink TEST Project',
      )
      checkRes = check(res, {
        '(api/user-projects) "Kitchen Sink TEST Project" found in project list':
          (r) => index != -1,
      })
      checkFailureRate.add(!checkRes)

      res = http.get(
        ORG_URL + `/api/protocols?pagination[pageSize]=50`,
        options,
      )
      if (res.status != 200)
        console.log('org /api/protocols res ' + JSON.stringify(res.body))
      body = JSON.parse(res.body)
      const protocols = body.data ? body.data : []

      // skip if already assigned
      if (ALL_PROTOCOLS.length == 0) {
        ALL_PROTOCOLS = protocols ? protocols : []
      }

      checkRes = check(res, {
        '(api/protocols) "more than one protocol found': (r) =>
          protocols.length > 0,
      })
      checkFailureRate.add(!checkRes)

      // check whether opportunistic observation exists or not
      const opportuneIndex = ALL_PROTOCOLS.findIndex((x) =>
        x.attributes.name.toLowerCase().includes('opportun'),
      )
      checkRes = check(res, {
        '(api/protocols) "opportunistic observation present in protocol list': (
          r,
        ) => opportuneIndex != -1,
      })
      checkFailureRate.add(!checkRes)
    })

    // calls core apis
    group('Paratoo core', function () {
      if (DISABLE_TEST.CORE_URL) return
      // header with bearer token
      const options = {
        timeout: '120s',
        headers: {
          Authorization: `Bearer ${AUTH_TOKEN}`,
          'Content-Type': 'application/json',
        },
      }
      

      // get bird protocol id, as this may change
      //   when new protocols are added
      const birdIndex = ALL_PROTOCOLS.findIndex(
        (x) => x.attributes.identifier === 'c1b38b0f-a888-4f28-871b-83da2ac1e533',
      )
      // survey id to send child observations
      const surveyID = {
        surveyType: 'bird-survey',
        time: new Date(Date.now()),
        uuid: uuidv4(),
        projectId: 1,
        protocol: {
          id: "c1b38b0f-a888-4f28-871b-83da2ac1e533",
          version: 1,
        }
      }

      // payload to get org Identifier
      const payloadToGetMintIdentifier = {
        surveyId: surveyID,
      }

      let res = http.post(
        ORG_URL + '/api/org/mint-identifier',
        JSON.stringify(payloadToGetMintIdentifier),
        options,
      )
      if (res.status != 200)
        console.log('org /api/org/mint-identifier res ' + JSON.stringify(res.body))
      // check whether the org identifier is generated or not
      const orgMintedIdentifier = res.body
        ? JSON.parse(res.body).orgMintedIdentifier
        : null
      let checkRes = check(res, {
        'generate org identifier for bird-survey': (r) =>
          orgMintedIdentifier != null,
      })
      checkFailureRate.add(!checkRes)

      // generate payload using mint identifier & surveyID
      const birdSurveyPayload = generateBirdSurveyPayload(
        orgMintedIdentifier,
        surveyID,
      )

      // sending bulk request
      console.log('calling bird survey api ')
      res = http.post(
        CORE_URL + '/api/bird-surveys/bulk',
        JSON.stringify(birdSurveyPayload),
        {
          timeout: '500s',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${AUTH_TOKEN}`,
          },
        },
      )
      if (res.status != 200)
        console.log(
          'core /api/bird-surveys/bulk res ' + JSON.stringify(res.body),
        )
      // if successful we add status
      checkRes = check(res, {
        '(api/bird-surveys/bulk) successful bulk upload': (r) =>
          res.status == 200,
      })
      successBulkRequest.add(checkRes)
    })

    function generateBirdSurveyPayload(mintIdentifier, surveyId) {
      return {
        data: {
          collections: [
            {
              orgMintedIdentifier: mintIdentifier,
              'plot-layout': {
                id: 1,
              },
              'plot-visit': {
                id: 1,
              },
              'bird-survey': {
                data: {
                  start_date: '2021-08-26T00:26:54.317Z',
                  end_date: '2021-08-26T00:26:54.317Z',
                  playback_used: true,
                  survey_type: '202',
                  plot_type: 'F',
                  number_of_surveyors: 2,
                  surveyor_names: [
                    { observer: 'John Doe' },
                    { observer: 'Jane Smith' },
                  ],
                  surveyId: surveyId,
                  site_location_id: 2,
                },
              },
              'weather-observation': {
                data: {
                  temperature: 10,
                  precipitation: 'NO',
                  precipitation_duration: 'I',
                  wind_description: 'C',
                  cloud_cover: 'O',
                },
              },
              'bird-survey-observation': [
                {
                  data: {
                    species: 'magpie',
                    count: 464,
                    observation_type: 'S',
                    activity_type: 'FO',
                    observation_location_type: 'WS',
                    breeding_type: 'NE',
                    fauna_maturity: 'A',
                  },
                },
              ],
            },
          ],
        },
      }
    }

    function generateOpportunePayload() {
      const obs_id = 'OPP' + Math.floor(Math.random() * 100000000)
      return {
        data: {
          observation_id: obs_id.toString(),
          location: {
            lat: Math.floor(Math.random() * 100000000),
            lng: Math.floor(Math.random() * 100000000),
          },
          taxa_type: 'VP',
          species: 'AMPHIBIA [Class] (scientific: AMPHIBIA)',
          confident: true,
          date_time: '2023-03-08T04:34:25.075Z',
          number_of_individuals: 1
        }
      }
    }
  })
}

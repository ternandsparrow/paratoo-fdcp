const { createServer } = require('node:http')

const hostname = '127.0.0.1'
const port = 3000
ENABLE_DEV = false
const CORE_URL = ENABLE_DEV
  ? 'https://dev.core-api.paratoo.tern.org.au'
  : 'http://localhost:1337'

const server = createServer(async (req, res) => {
  console.log('generating token ')
  const resp = await fetch(`${CORE_URL}/api/protocols/test-token/merit`, {
    method: 'POST',
    headers: {
      'Content-type': 'application/json',
      Authorization: 'Bearer 1234',
    },
  })
  const body = await resp.json()
  res.end(body.token)
})
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`)
})

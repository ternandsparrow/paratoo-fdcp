import { browser } from 'k6/experimental/browser'
import { check, sleep } from 'k6'
import http from 'k6/http'
import { Rate } from 'k6/metrics'

const ENABLE_DEV = true
// url
const WEBAPP_URL = ENABLE_DEV
  ? 'https://dev.app.monitor.tern.org.au/'
  : 'http://localhost:8080'
const ORG_URL = ENABLE_DEV
  ? 'https://api.test.ala.org.au/ecodata'
  : 'http://localhost:1338'
const CORE_URL = ENABLE_DEV
  ? 'https://dev.core-api.monitor.tern.org.au'
  : 'http://localhost:1337'

const CORE_OPTION = {
  headers: {
    Authorization: `Bearer ${__ENV.TEST_CORE_AUTH_TOKEN}`,
  },
}

const OIDC_URL = 'http://localhost:7095/data-export/v1/oidc-token'

export const options = {
  scenarios: {
    browser: {
      executor: 'constant-vus',
      exec: 'webApp',
      vus: 10,
      duration: '20s',
      options: {
        browser: {
          type: 'chromium',
        },
      },
    },
    core: {
      executor: 'constant-vus',
      exec: 'core',
      vus: 20, // can handle 200 concurrent
      duration: '200s',
    },
  },
}

const checkFailureRate = new Rate('WebApp_check_failure_rate')
const generateAuthToken = new Rate(
  'Headless_crome_generate_auth_token_failure_rate',
)
const coreGetRequest = new Rate('Core_get_request_failure_rate')
const coreGetValidResponse = new Rate('Core_get_valid_response_failure_rate')
const meritPostRequest = new Rate('Merit_post_request_failure_rate')
const meritPostValidResponse = new Rate(
  'Merit_post_valid_response_failure_rate',
)
const corePostRequest = new Rate('Core_post_request_failure_rate')
const corePostValidResponse = new Rate('Core_post_valid_response_failure_rate')

export async function webApp() {
  const page = browser.newPage()
  let checkRes
  // TODO: use k6-browser instead of selenium
  // NOTE: k6-browser doest support SSO https://github.com/grafana/xk6-browser/issues/1011
  try {
    await page.goto(WEBAPP_URL)

    /////////////////////////////////////////////
    // checks ui. e.g. titles, name of assests //
    /////////////////////////////////////////////
    let content = JSON.stringify(page.content())
    checkRes = check(content, {
      "WebApp Title 'Monitor' exists": (c) =>
        c ? c.includes('Monitor') !== -1 : false,
    })
    checkFailureRate.add(!checkRes)

    checkRes = check(content, {
      "WebApp Button 'Project' exists": (c) =>
        c ? c.includes('Monitor') !== -1 : false,
    })
    checkFailureRate.add(!checkRes)
    sleep(1)

    //////////////////////////////////////////////////
    // checks local storage. e.g. workbox pre-cache //
    //////////////////////////////////////////////////
    const caches = await page.evaluate(async () => {
      const keys = await caches.keys()
      return keys
    })
    checkRes = check(caches, {
      "WebApp Cache 'Public Assets' exists": (c) =>
        c ? c.includes('public-assets') !== -1 : false,
    })
    checkFailureRate.add(!checkRes)
    sleep(1)

    ///////////////////////////////////////////////////////////
    // checks ui activities. e.g. home/profile button action //
    ///////////////////////////////////////////////////////////
    await page.click('a[href="/home"]')
    sleep(1)
    content = JSON.stringify(page.content())
    checkRes = check(content, {
      "WebApp Button 'Home' Action 'Welcome to Monitor' exists": (c) =>
        c ? c.includes('Welcome to Monitor') !== -1 : false,
    })
    checkFailureRate.add(!checkRes)

    checkRes = check(content, {
      "WebApp Button 'Home' Action 'tern@adelaide.edu.au' exists": (c) =>
        c ? c.includes('tern@adelaide.edu.au') !== -1 : false,
    })
    checkFailureRate.add(!checkRes)
  } finally {
    page.close()
  }

  /////////////////////////////////////////
  // OIDC login check e.g. login with ALA//
  /////////////////////////////////////////
  const res = http.get(OIDC_URL)
  const token = res.body ? JSON.parse(res.body) : ''
  checkRes = check(res.body, {
    "WebApp 'OIDC' Login check": token != null,
  })
  checkFailureRate.add(!checkRes)
  checkRes = check(res.body, {
    'Headless Crome generate token': token != null,
  })
  generateAuthToken.add(!checkRes)
  sleep(1)
}

export async function core() {
  let checkRes
  let res
  let values

  ///////////////////////////////////////////////////////////////
  // public get request to core. e.g. populate one of the luts //
  ///////////////////////////////////////////////////////////////
  res = http.get(`${CORE_URL}/api/lut-preservation-types`)
  checkRes = check(res, {
    'Core public get request - status 200': (r) => r.status === 200,
  })
  coreGetRequest.add(!checkRes)

  values = res.body ? JSON.parse(res.body).data : []
  checkRes = check(values, {
    'Core public get request - valid reponse': (r) => r.length > 0,
  })
  coreGetValidResponse.add(!checkRes)

  ////////////////////////////////////////////////////////////
  // get request with auth token. e.g. populate bird-survey //
  ////////////////////////////////////////////////////////////
  res = http.get(OIDC_URL, { timeout: '900000000s' })
  const token = res.body ? JSON.parse(res.body) : null
  checkRes = check(res.body, {
    "WebApp 'OIDC' Login check": token != null,
  })
  checkFailureRate.add(!checkRes)
  checkRes = check(res.body, {
    'Headless crome generate token': token != null,
  })
  generateAuthToken.add(!checkRes)

  const headers = {
    headers: {
      Authorization: token,
      timeout: '900000000s',
    },
  }
  res = http.get(`${CORE_URL}/api/bird-surveys`, headers)
  checkRes = check(res, {
    'Core get request - status 200': (r) => r.status == 200,
  })
  coreGetRequest.add(!checkRes)

  values = res.body ? JSON.parse(res.body) : []
  // console.log('private api ' + JSON.stringify(values[0]))
  checkRes = check(values, {
    'Core get request - valid reponse': (r) => r.length > 0,
  })
  coreGetValidResponse.add(!checkRes)

  ///////////////////////////////////////////////////////////////////
  // post request with auth token. e.g. bulk request e.g. opportune//
  ///////////////////////////////////////////////////////////////////
  const currentDate = new Date(Date.now())
  const opportuneSurveyMetadata = {
    survey_details: {
      survey_model: 'opportunistic-survey',
      time: currentDate.toISOString(),
      uuid: '0c7886bb-c468-4535-af76-0f25a5dd0e91',
      project_id: '0d02b422-5bf7-495f-b9f2-fa0a3046937f',
      protocol_id: '068d17e8-e042-ae42-1e42-cff4006e64b0',
      protocol_version: '1',
      submodule_protocol_id: '',
    },
    provenance: {
      version_app: '1.0.0-beta.0-e8f6e3c9',
      version_core_documentation: '1.0.0-beta.0-e8f6e3c9',
      system_app:
        'Atlas of Living Australia-develop-129.127.180.3-Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36',
    },
  }
  // payload to get org Identifier
  const payloadToGetMintIdentifier = {
    survey_metadata: opportuneSurveyMetadata,
  }
  headers.headers['Content-Type'] = 'application/json'
  res = http.post(
    `${ORG_URL}/ws/paratoo/mint-identifier`,
    JSON.stringify(payloadToGetMintIdentifier),
    headers,
  )
  checkRes = check(res, {
    'Merit post request "mint-identifier" - status 200': (r) => r.status == 200,
  })
  meritPostRequest.add(!checkRes)

  const orgMintedIdentifier = res.body
    ? JSON.parse(res.body).orgMintedIdentifier
    : null
  checkRes = check(orgMintedIdentifier, {
    'Merit post api request - valid response': (r) => r != null,
  })
  meritPostValidResponse.add(!checkRes)

  // post bulk collection opportune
  const obs_id = 'OPP' + Math.floor(Math.random() * 100000000)
  const opportune = {
    data: {
      collections: [
        {
          'opportunistic-survey': {
            data: {
              start_date_time: '2024-04-12T01:56:59.893Z',
              survey_metadata: opportuneSurveyMetadata,
              end_date_time: '2024-04-12T01:57:29.541Z',
            },
          },
          'opportunistic-observation': [
            {
              data: {
                observation_id: obs_id,
                date_time: currentDate.toISOString(),
                location: {
                  lat: -34.9765632,
                  lng: 138.6315776,
                },
                taxa_type: 'BI',
                species: 'Ostrich [sp] (scientific: Struthio camelus)',
                confident: true,
                number_of_individuals: 2,
                exact_or_estimate: 'Exact',
                observation_method_tier_1: 'SE',
                observation_method_tier_2: 'RC',
                observers: [
                  {
                    observer: 'test',
                  },
                ],
                habitat_description: 'MVG7',
                comments: 'test',
                add_additional_fauna_data: false,
              },
            },
          ],
          orgMintedIdentifier: orgMintedIdentifier,
        },
      ],
    },
    role: 'project_admin',
  }
  res = http.post(
    `${CORE_URL}/api/opportunistic-surveys/bulk`,
    JSON.stringify(opportune),
    headers,
  )
  checkRes = check(res, {
    'Core post request "opportune bulk" - status 200': (r) => r.status == 200,
  })
  corePostRequest.add(!checkRes)

  const data = res.body ? JSON.parse(res.body) : []
  checkRes = check(data, {
    'Core post request - valid response': (r) => r.length > 0,
  })
  corePostValidResponse.add(!checkRes)
}

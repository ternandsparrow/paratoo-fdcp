import logging

import django
from django import forms
from django.conf import settings
from django.contrib.auth import authenticate
from django.utils.translation import gettext_lazy as _
from constant import CLIENT_ID


logger = logging.getLogger(__name__)


class LoginForm(forms.Form):
    """Standard username and password authentication form."""
    username = forms.CharField(label=_("Username"),
                               error_messages={'required':
                                               _("Please enter your username")})
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput,
                               error_messages={'required':
                                               _("Please enter your password")})

    if django.VERSION >= (1, 9):
        password.strip = False

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(LoginForm, self).__init__(*args, **kwargs)
        if getattr(settings, 'MAMA_CAS_ALLOW_AUTH_WARN', False):
            self.fields['warn'] = forms.BooleanField(
                widget=forms.CheckboxInput(),
                label=_("Warn before automatic login to other services"),
                required=False)

    def org_login(self, request, username, password):
        from constant import ORG_URL
        import requests
        request_body = {
            "identifier": username,
            "password": password
        }
        url = f"{ORG_URL}/api/auth/local"
        r = requests.post(url, data=request_body, timeout=500)
        if r.status_code != 200:
            return None

        res = r.json()
        if "jwt" not in res:
            return None

        from Utilites import store_user

        store_user(username=username, password=password,
                   api_response=dict(res))

        from django.contrib.auth.models import User
        created = User.objects.filter(username=username).exists()
        if not created:
            User.objects.create_user(username=res["user"]["username"],
                                     email=res["user"]["email"],
                                     password=password)

        return authenticate(request=request, username=username, password=password)

    def clean(self):
        """
        Pass the provided username and password to the active
        authentication backends and verify the user account is
        not disabled. If authentication succeeds, the ``User`` object
        is assigned to the form so it can be accessed in the view.
        """
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            try:
                self.user = self.org_login(
                    request=self.request, username=username, password=password)
            except Exception:
                logger.exception("Error authenticating %s" % username)
                error_msg = _('Internal error while authenticating user')
                raise forms.ValidationError(error_msg)

            if self.user is None:
                logger.warning("Failed authentication for %s" % username)
                error_msg = _('The username or password is not correct')
                raise forms.ValidationError(error_msg)
            else:
                if not self.user.is_active:
                    logger.warning("User account %s is disabled" % username)
                    error_msg = _('This user account is disabled')
                    raise forms.ValidationError(error_msg)

        return self.cleaned_data


class LoginFormEmail(LoginForm):
    """
    Subclass of ``LoginForm`` that extracts the username if an email
    address is provided.
    """

    def clean_username(self):
        """
        Remove an '@<domain>' suffix if present and return only
        the username.
        """
        username = self.cleaned_data.get('username').split('@')[0]
        if not username:
            raise forms.ValidationError(_('Invalid username provided'))
        return username

The PARATOO CAS 5 deployment is based on [django-mama-cas
](https://github.com/jbittel/django-mama-cas).

CAS documentation is available [here](https://apereo.github.io/cas/5.3.x/index.html)

# Features this demonstrates
  - validate users/tokens with paratoo-org

# Running this APP locally
While doing local development, you'll need `paratoo-org`. We have a bash CLI script
that will start the Django app
in dev mode.
 1. clone the repo
 2. override any config items you need by creating a `.env.local`
     file and adding overrides to values you find in `.env`.
     ```bash
     cp .env .env.local
 4. start the server
    ```bash
    ./pcas
    ```
  5. you can stop the data-export server with Ctrl-c
     
# Running docker build locally
 1. clone the repo
 2. override any config items you need by creating a `.env.local`
     file and adding overrides to values you find in `.env`.
     ```bash
     cp .env .env.local
 4. start the server
    ```bash
    ./pcas -docker
    ```
  5. you can stop the data-export server with Ctrl-c


**Important**: If the port is being used.
```bash
./pcas down
./pcas
```


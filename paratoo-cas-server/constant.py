from environs import Env
import os

env = Env()
env.read_env()

LOG_LEVEL = env.str("LOG_LEVEL", default="debug")
CAS_HOST = env.str("CAS_HOST", default="http://127.0.0.1")
CAS_PORT = env.str("CAS_PORT", default="8000")
ORG_URL = env.str("ORG_URL", default="http://localhost:1338")
ISSUER = env.str("ISSUER", default="http://127.0.0.1:6060")
CLIENT_ID = env.str("CLIENT_ID", default="test1234")

APP_ORIGINS = [
    'http://localhost:8080',
    "https://test.app.monitor.tern.org.au",
    "https://staging.app.monitor.tern.org.au",
    "https://app.monitor.tern.org.au",
    "https://dev.app.paratoo.tern.org.au",
    "https://beta.app.paratoo.tern.org.au",
    "https://app.paratoo.tern.org.au"
]

OPENID_CONFIG = {
    "issuer": f"{ISSUER}/cas/oidc",
    "scopes_supported": [
        "openid",
        "profile",
        "email",
        "offline_access",
        "ala",
        "roles",
        "ala/internal",
        "users/read",
        "users/write",
        "profile-service/write",
        "profile-service/read",
        "doi/write",
        "doi/read",
        "image-service/write",
        "image-service/read"
    ],
    "response_types_supported": [
        "code",
        "token",
        "id_token token"
    ],
    "subject_types_supported": [
        "public",
        "pairwise"
    ],
    "claim_types_supported": [
        "normal"
    ],
    "claims_supported": [
        "sub",
        "name",
        "preferred_username",
        "family_name",
        "given_name",
        "profile",
        "locale",
        "updated_at",
        "email",
        "email_verified",
        "organisation",
        "role",
        "authority",
        "city",
        "state",
        "country",
        "userid"
    ],
    "grant_types_supported": [
        "authorization_code",
        "password",
        "client_credentials",
        "refresh_token"
    ],
    "id_token_signing_alg_values_supported": [
        "RS256",
        "RS384",
        "RS512",
        "PS256",
        "PS384",
        "PS512",
        "ES256",
        "ES384",
        "ES512",
        "HS256",
        "HS384",
        "HS512"
    ],
    "id_token_encryption_alg_values_supported": [
        "RSA1_5",
        "RSA-OAEP",
        "RSA-OAEP-256",
        "A128KW",
        "A192KW",
        "A256KW",
        "A128GCMKW",
        "A192GCMKW",
        "A256GCMKW",
        "ECDH-ES",
        "ECDH-ES+A128KW",
        "ECDH-ES+A192KW",
        "ECDH-ES+A256KW"
    ],
    "id_token_encryption_enc_values_supported": [
        "A128CBC-HS256",
        "A192CBC-HS384",
        "A256CBC-HS512",
        "A128GCM",
        "A192GCM",
        "A256GCM"
    ],
    "userinfo_signing_alg_values_supported": [
        "RS256",
        "RS384",
        "RS512",
        "PS256",
        "PS384",
        "PS512",
        "ES256",
        "ES384",
        "ES512",
        "HS256",
        "HS384",
        "HS512"
    ],
    "userinfo_encryption_alg_values_supported": [
        "RSA1_5",
        "RSA-OAEP",
        "RSA-OAEP-256",
        "A128KW",
        "A192KW",
        "A256KW",
        "A128GCMKW",
        "A192GCMKW",
        "A256GCMKW",
        "ECDH-ES",
        "ECDH-ES+A128KW",
        "ECDH-ES+A192KW",
        "ECDH-ES+A256KW"
    ],
    "userinfo_encryption_enc_values_supported": [
        "A128CBC-HS256",
        "A192CBC-HS384",
        "A256CBC-HS512",
        "A128GCM",
        "A192GCM",
        "A256GCM"
    ],
    "request_object_signing_alg_values_supported": [
        "RS256",
        "RS384",
        "RS512",
        "PS256",
        "PS384",
        "PS512",
        "ES256",
        "ES384",
        "ES512",
        "HS256",
        "HS384",
        "HS512"
    ],
    "request_object_encryption_alg_values_supported": [
        "RSA1_5",
        "RSA-OAEP",
        "RSA-OAEP-256",
        "A128KW",
        "A192KW",
        "A256KW",
        "A128GCMKW",
        "A192GCMKW",
        "A256GCMKW",
        "ECDH-ES",
        "ECDH-ES+A128KW",
        "ECDH-ES+A192KW",
        "ECDH-ES+A256KW"
    ],
    "request_object_encryption_enc_values_supported": [
        "A128CBC-HS256",
        "A192CBC-HS384",
        "A256CBC-HS512",
        "A128GCM",
        "A192GCM",
        "A256GCM"
    ],
    "introspection_endpoint_auth_methods_supported": [
        "client_secret_basic"
    ],
    "token_endpoint_auth_methods_supported": [
        "client_secret_basic",
        "client_secret_post",
        "client_secret_jwt",
        "private_key_jwt"
    ],
    "code_challenge_methods_supported": [
        "plain",
        "S256"
    ],
    "claims_parameter_supported": True,
    "request_uri_parameter_supported": True,
    "request_parameter_supported": True,
    "backchannel_logout_supported": True,
    "frontchannel_logout_supported": True,
    "jwks_uri": f"{ISSUER}/cas/oidc/jwks",
    "authorization_endpoint": f"{ISSUER}/cas/oidc/oidcAuthorize",
    "token_endpoint": f"{ISSUER}/cas/oidc/oidcAccessToken",
    "userinfo_endpoint": f"{ISSUER}/cas/oidc/oidcProfile",
    "pushed_authorization_request_endpoint": f"{ISSUER}cas/oidc/oidcPushAuthorize",
    "registration_endpoint": f"{ISSUER}/cas/oidc/register",
    "end_session_endpoint": f"{ISSUER}/cas/oidc/oidcLogout",
    "introspection_endpoint": f"{ISSUER}/cas/oidc/introspect",
    "revocation_endpoint": f"{ISSUER}/cas/oidc/revoke",
    "backchannel_logout_session_supported": True,
    "frontchannel_logout_session_supported": True
}

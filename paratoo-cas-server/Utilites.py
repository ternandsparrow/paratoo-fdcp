
import hashlib
import uuid
import os
import json


def compute_md5_hash(value: str):
    v = value
    if not value:
        v = "None"
    hash = hashlib.sha256(v.encode("utf-8"))
    hash_uuid = uuid.UUID(hash.hexdigest()[::2])
    return str(hash_uuid)

def store_user(username: str, password: str, api_response: dict):
    dir = "users"
    if not os.path.exists(dir):
        os.makedirs(dir)
    if not username:
        username = api_response["user"]["username"]
    file_name = f"{dir}/{compute_md5_hash(value=username)}.json"
    with open(file_name, "w+", encoding="utf-8") as f:
        json.dump(api_response, f, ensure_ascii=False, indent=2)

def remove_user(code: str):
    dir = "users"
    file_name = f"{dir}/{code}.json"
    if not os.path.exists(file_name):
        return None
    print("removing user " + code)
    os.remove(file_name)
    


def get_user_details(username: str = None, code: str = None):
    dir = "users"

    if username:
        file_name = f"{dir}/{compute_md5_hash(value=username)}.json"
    if code:
        file_name = f"{dir}/{code}.json"

    if not os.path.exists(file_name):
        return {}

    with open(file_name, "r") as json_file:
        json_data = json.load(json_file)
        return dict(json_data)
    
def get_user_details_from_token(token: str, field="jwt"):
    dir = "users"
    if not os.path.exists(dir):
        return None
    if "Bearer " in token:
        token = token.replace("Bearer ", "")

    for file in os.listdir(dir):
        if "json" not in file:
            continue
        with open(f"{dir}/{file}", "r") as json_file:
            user_details = json.load(json_file)
            if field not in user_details: continue
            if not user_details[field]: continue
            if user_details[field] != token: continue

            return dict(user_details)
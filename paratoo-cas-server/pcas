#!/bin/bash
# create required supporting services as docker containers and start app in dev
# mode on this machine (not in a docker container)
set -euo pipefail
cd $(dirname "$0")

overrideFile=.env.local
envVarPrefix=PO_

if [ -f $overrideFile ]; then
    echo "[INFO] loading config overrides from $overrideFile"
    tmpLocalEnv=$(mktemp)
    # remove comments and blank lines
    grep -v -e '^$' -e '^[# ]' $overrideFile |
      sed 's/^/export /' >$tmpLocalEnv
    source $tmpLocalEnv
    rm -f $tmpLocalEnv
fi

# we use the $envVarPrefix so we can dynamically find all the relevant vars
# later in the script.
export PO_LOG_LEVEL=${LOG_LEVEL:-debug}
export PO_CAS_HOST=${CAS_HOST:-127.0.0.1}
export PO_CAS_PORT=${CAS_PORT:-6060}
export PO_ISSUER=${ISSUER:-http://127.0.0.1:6060}
export PO_ORG_URL=${ORG_URL:-http://localhost:1338}

echo "[INFO] Config dump"
env | grep "^$envVarPrefix" | sed "s/^$envVarPrefix/  /" | sort
echo "\
note1: define any config overrides in $overrideFile."


function printHelp {
  cat <<EOF
Stop the supporting docker containers, but keep data with:
  $0 down

EOF
}


function printCleanupHelp {
  echo "==== Remember: the docker container(s) are still running ===="
  printHelp
}

function enableTrap {
  trap "printCleanupHelp" EXIT
}

function clearCache {
  find . | grep -E "(/__pycache__$|\.pyc$|\.pyo$)" | xargs rm -rf
  rm -rf test_resources
}

firstParam=${1:-}
case $firstParam in
-d | --down | down)
  echo "[INFO] stopping docker container"
  docker rm cas_server --force
  docker rmi paratoo-cas-server:latest --force
  exit
  ;;
-docker | --docker | docker)
  echo "[INFO] running docker build"
  ls
  ./run_docker_build
  exit
  ;;
clearCache | cc)
  echo "[INFO] clearing cache"
  clearCache
  ;;
*)
  echo "[INFO] running python3/flusk command"

  # dynamically export all the required env vars, so we don't have to maintain
  # the list by hand. We use a (hopefully) unique prefix to only grab "our"
  # vars.  We're using a temp file because we're still supporting bash 3.2,
  # which macOS still seems to ship with :'(
  tmpEnv=$(mktemp)
  env | grep "^$envVarPrefix" | sed "s/^$envVarPrefix/export /" >$tmpEnv
  source $tmpEnv
  rm -f $tmpEnv
  enableTrap
  rm -rf venv
  python3 -m venv venv
  source venv/bin/activate
  pip3 install --force-reinstall -U setuptools
  # pip3 install --upgrade pip wheel setuptools python-requests==2.27.1
  pip3 install -r requirements.txt
  python3 manage.py migrate
  python3 manage.py runserver $CAS_HOST:$CAS_PORT
  ;;
esac

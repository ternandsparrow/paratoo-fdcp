#!/bin/sh

export LOG_LEVEL=`sed -n 's/^LOG_LEVEL=\(.*\)/\1/p' < ./.env-temp`
export CAS_HOST=`sed -n 's/^CAS_HOST=\(.*\)/\1/p' < ./.env-temp`
export CAS_PORT=`sed -n 's/^CAS_PORT=\(.*\)/\1/p' < ./.env-temp`
export CLIENT_ID=`sed -n 's/^CLIENT_ID=\(.*\)/\1/p' < ./.env-temp`

export ISSUER=`sed -n 's/^ISSUER=\(.*\)/\1/p' < ./.env-temp`
export ORG_URL=`sed -n 's/^ORG_URL=\(.*\)/\1/p' < ./.env-temp`
echo $ISSUER
echo $ORG_URL

python manage.py migrate
python manage.py runserver $CAS_HOST:$CAS_PORT

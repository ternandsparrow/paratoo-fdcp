# How to add help button to a field 

1. Create a Vue file "helpInfo" button inside "components" folder , and named it similar to the field name in camel case, and add the related information into that Vue file.
1. In index.js file inside "helpInfo" add 
```js
export { default as <FileName>} from '../helpInfo/<FileName>'
```
1. Go to the schema.json file of that survey or observation, and a new prop "x-paratoo-description" to that related field with the name of the Vue file just created. And now you can see the help button with the info you just added
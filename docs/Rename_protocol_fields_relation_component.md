Sometimes we may get feedback from protocol team that the name of protocol/field/relation/component should be changed, but creating a new collection/component in strapi and delete the old one may trigger some weird issue.

Here are some examples to rename:

- protocol:
  - in protocol/schema.json, add "overrideDisplayName" to the protocol that you wanna rename
    - ```js
        {
          "modelName": "targeted-fauna-survey-detail",
          "usesCustomComponent": "true",
          "overrideDisplayName": "Targeted Fauna Survey Details"
        },
      ```
- add "x-paratoo-rename" to the field/relation/component that you wanna rename
  - field: in fauna-passive-observation - schema.json
    - ```js
        "length": {
          "type": "decimal",
          "x-paratoo-rename": "Length"
        },
      ```

  - relation: fauna-passive-check - schema.json
    - ```js
        "observations": {
          "type": "relation",
          "relation": "oneToMany",
          "target": "api::fauna-passive-observation.fauna-passive-observation",
          "x-paratoo-rename": "Passive fauna observations"
        }
      ```

  - component: fauna-passive-check - schema.json
    - ```js
        "fauna_passive_collection": {
          "type": "component",
          "repeatable": true,
          "component": "targeted.fauna-passive-collection",
          "x-paratoo-rename": "Passive fauna trap/equipment collection/check"
        },
      ```

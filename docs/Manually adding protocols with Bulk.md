To be used until automation is implemented, and to aid in automation

- Create protocol initial data (alternatively add protocol entry within Strapi UI, but will get cleared if database is dropped)
  - Go to paratoo-core/api/protocol/models/protocol.settings.json
  - Line up fields with above attributes
  - The endpoint prefix should be the associated path for the new protocol
  - The workflow is a JSON array, with each entry a JSON object of {“modelName”: \<insert internal model name\>}
- Sync protocols between core and org with `/paratoo-fdcp/helper-scripts/sync-protocol-models.sh`
- In Org strapi admin UI, assign protocol to a project (`Kitchen sink Test project` for testing)
- Go to Strapi UI → content-types builder
  - Add models and field types according to external (expert) specifications
- Add bulk handler
  - Go to: /paratoo-core/src/api/creating-new-protocol-example/controllers/creating-new-protocol-example.js
  - Inside export.default add an async function “bulk”:
  ```js
  module.exports = createCoreController('api::creating-new-protocol-example.creating-new-protocol-example', ({ strapi }) => ({
  /**
   * Business logic endpoint for performing a creating-new-protocol-example - uploads a collection of creating-new-protocol-example
   * surveys and their observations)
   * 
   * @param {Object} ctx Koa context
   * @returns {Array} successfully-created array of collections
   */
  async bulk(ctx) {
    const surveyModelName = 'creating-new-protocol-example-survey'
    const observationModelName = 'creating-new-protocol-example-observation'

    return strapi.service('api::paratoo-helper.paratoo-helper').genericBulk({
      ctx: ctx, 
      surveyModelName: surveyModelName,
      obsModelName: observationModelName, 
    })
   }
  }))
  ``` 

  - Function parameters documented in `paratoo-core/config/functions/bulk.js`
- Add bulk route
  - Go to: /paratoo-core/src/api/creating-new-protocol-example/routes/creating-new-protocol-example.js
  - Replace everything in file with:
  - Remember Purals are Important!
  ```js
  module.exports = {
  'routes': [
    {
      'method': 'GET',
      'path': '/creating-new-protocol-examples',
      'handler': 'creating-new-protocol-example.find',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'GET',
      'path': '/creating-new-protocol-examples/:id',
      'handler': 'creating-new-protocol-example.findOne',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/creating-new-protocol-examples',
      'handler': 'creating-new-protocol-example.create',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'POST',
      'path': '/creating-new-protocol-examples/bulk',
      'handler': 'creating-new-protocol-example.bulk',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'PUT',
      'path': '/creating-new-protocol-examples/:id',
      'handler': 'creating-new-protocol-example.update',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer',
          'global::lut-interpretation'
        ]
      }
    },
    {
      'method': 'DELETE',
      'path': '/creating-new-protocol-examples/:id',
      'handler': 'creating-new-protocol-example.delete',
      'config': {
        'policies': [
          'global::is-validated',
          'global::projectMembershipEnforcer'
        ]
      }
    }
   ]
  }
  ```
  - Add policies to routes as advised in the console on startup

  OR

  - in `paratoo-core/config/functions/bootstrap.js`, set the first parameter of amendAPIRoutes to be true, to automatically add policies `strapi.config.functions.addPoliciesToRoutes.amendAPIRoutes(true)`
<br>
- It may be nessesary to restart the server/clear site data in the browser accessing the webapp

<details>
<summary>If making new LUTs:</summary>


ensure they are populated with at least one entry in initialData
- Go to paratoo-core/api/lut-XYZ/models/lut-XYZ.settings.json
  - In the top level object, add a key “initialData”, as an Array
  - Add a sub-array for each entry to make, with fields lining up with above LUT data types
</details>
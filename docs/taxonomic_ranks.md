# Taxonomic Ranks

This document outlines the taxonomic ranks we use. Because different organizations refer to different lists, this combined them all such that we can correctly use the terms.

The basic hierarchy of biological classification's eight major taxonomic rank (Intermediate minor rankings are not shown):

![basic hierarchy of biological classification's eight major taxonomic rank](Biological_classification_L_Pengo_vflip.svg)

Categorization layout (parenthesis indicate latin name):

- primary ranks
  - secondary ranks (if none, indicate with `...`)
    - further ranks

---

- kingdom (regnum)
  - ...
    - subkingdom (subregnum)
- division or phylum (divisio, phylum)
  - ...
    - subdivision or subphylum (subdivisio or subphylum)
- class (classis)
  - ...
    - subclass (subclassis)
- superorder (superordo)
- order (ordo)
  - ...
    - suborder (subordo)
- family (familia)
  - ...
    - subfamily (subfamilia)
  - tribe (tribus)
    - subtribe (subtribus)
- genus (genus)
  - ...
    - subgenus (subgenus)
  - section (sectio)
    - subsection (subsectio)
  - series (series)
    - subseries
- superspecies
- species (species)
  - ...
    - subspecies
  - variety (varietas)
    - subvariety (subvarietas)
  - form (forma)
    - subform (subforma)

# Sources

https://en.wikipedia.org/wiki/Taxonomic_rank

Superorder explanation: https://en.wikipedia.org/wiki/Order_(biology)

Superspecies explanation: https://en.wikipedia.org/wiki/Species_complex, https://www.jstor.org/stable/2411398?seq=1

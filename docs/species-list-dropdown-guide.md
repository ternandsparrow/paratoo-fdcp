# Species List Dropdown Guide

The species lists are fairly large CSV files that cannot be derived from Strapi LUTs. As such, we bake in these CSV files into the webapp, which are loaded into [Dexie.js](https://dexie.org/). When the species list is setup for a given schema, Dexie will load them as options in a typical `q-select`. The options presented essentially act as a suggestive auto-complete, as we typically only apply these species lists to fields that are free text (and are usually field names).

## Process

For any given schema attribute, add the `x-paratoo-csv-list-taxa` key, with an array as the value. The array is populated with the taxa(s) that we want to be populated in the dropdown. The taxa(s) are derived from the `lut-taxa-type` LUT, which we provide the symbols for.

![step 1](./screenshots/species-list-dropdown-step-1.png)

That's it! Refresh the client and the species lists should be populated.
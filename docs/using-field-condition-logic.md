# Using field condition logic

> NOTE: this guide applies to the current basic implementation (i.e., a majority of the implementation is front-end derived) - once this has been translated to be backend-derived (see #329), this document should be reviewed.

Some fields require restrictions or filtering based on inputs or selections in other fields. Thus, we have a generic way of applying `conditional logic` to fields, where we simply provide some JSON (and additional parameters) to a callback that handles the conditions. The main use cases are:

1. LUTs that filter based on another LUT's selection. For example, `lut-camera-make` depends on the `lut-camera-model` selection
2. Showing/hiding fields with arbitrary dependencies. For example, selecting a `carcass` from `lut-feature` requires a new field to be shown where the user can specify the species of the carcass feature.

Case (1) is handled by the `createDependentFieldConfig()` [helper](https://gitlab.com/ternandsparrow/paratoo-fdcp/-/blob/develop/paratoo-webapp/src/misc/helpers.js#L3051), in which we specify which fields have dependencies. This requires the dependent field (`lut-camera-model` in the example above) to have relations to their 'valid' fields. It must be used in combination with case (2).

Case (2) is handled by the `createDependentFieldCallback()` [helper](https://gitlab.com/ternandsparrow/paratoo-fdcp/-/blob/develop/paratoo-webapp/src/misc/helpers.js#L3094), which is a highly-generic handler. This callback is passed down the ApiModelCrudUi component hierarchy, and is applied at the relevant 'level' of said hierarchy by Crud's `newRecordWatcher`. 

> NOTE: case (2)'s helper is mostly implemented, but there may be edge cases where it needs extending. If modifications need to be made, proceed with caution, as this callback is used in a lot of places where regression may occur.

## Example 1 - `x-paratoo-has-dependencies`

The most simple case is using the custom flag in the schema attribute (this is the starting point for #329), which takes an array of strings, representing the field name(s) of the dependency/s. This scenario is a simple show/hide case (only tested for boolean 'depends on' fields) and is applied to a given schema attribute:

```json
"camera_needs_replacing": {
  "type": "boolean",
  "default": false,
  "x-paratoo-has-dependencies": [
    "camera_trap_information",
    "camera_trap_settings",
    "camera_trap_height",
    "camera_trap_direction",
    "camera_trap_angle"
  ]
},
```

## Example 2

In the most simple case not derived from the custom flag (like when combining with case (1) with case (2)), we define the JSON for `createDependentFieldCallback()` like:

```json
{
  field: 'camera_model',
  dependsOn: 'camera_make',
},
```
In which we also call `createDependentFieldConfig()` like:

```js
createDependentFieldConfig(
  //dependent field's model config (passed as a reference so it can be modified)
  confCtInfoCameraModel,
  //dependent field's model name / options key
  confCtInfoCameraMake.optionsKey,
  //the field/attribute name used in the 'depends on' schema (which is the key used when
  //adding to the store)
  'camera_make',
  //the field/attribute name used in the dependent field's schema (the field name used
  //in the relation of the dependent field)
  'camera_make',
  //whether the dependent field is a LUT with custom input `filteredSelectWithCustomInput`
  true,
  //a reference to the store that holds the LUT data
  this.apiModelsStore,
)
```

## Example 3

When a field depends on a LUT selection, but is not a LUT itself, we cannot use `createDependentFieldConfig()`. Instead, we specify the `dependsOnSelection` key, which takes an array of LUT symbol strings as values. For example, for one of the `camera-trap-settings` child observation fields:

```json
{
  field: 'images_per_trigger',
  dependsOn: 'camera_media_type',
  dependsOnSelection: ['I', 'IV'],
},
```

This means if 'Image' or 'Image + Video' is selected from the 'Camera Media Type' LUT, is should show the 'Images per trigger' field.

## Example 4

There might be a case similar to [Example 3](#example-3), but the 'dependent' field is below the 'depends on' field in the component hierarchy, or the conditional case is more complex than a simple [`Array.some()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some).

For example, in the latter case, we might want to check when a selection is *not* made we might use:

```json
{
  field: 'camera_needs_replacing',
  dependsOn: 'operational_status',
  //using `dependsOnSelection` won't allow checking for 'not in' condition.
  //assume `o` is the model value of `field` (object with keys label, value)
  dependsOnCb: o => o.label !== 'Operational'
},
```

The `dependsOnCb` is a callback function that is provided to `Array.some(callbackFn)`.

## Example 5

There may be situations where we want to some other custom logic, such as autofilling collection data. In this case, we can apply a `fieldCb` that is executed based on the `dependsOn` field being shown. For example, during Camera Trap Reequipping, the user may specify a camera needs to be replaced, in which case we autofill the camera trap's information based on the Camera Trap Deployment. This situation is handled by the `fieldCb` and is specified as JSON:

```json
{
  field: 'camera_trap_information',
  fieldCb: this.autoFillCb,
  dependsOn: 'camera_needs_replacing',
},
```

Where `autoFillCb` is a `method` defined on the component that takes an object parameter containing: `newVal`, `oldVal`, `field`, `inputDef`, `inputDefs`. Not all these fields need to be used when implementing an arbitrary `fieldCb`, only the ones you need. The callback can return an object, which contains the updated new record (`record` key) and whether to suppress the dependency (conditional fields) handler (`suppressDependencyHandler` key). It can also instead return a boolean, which will be used to refresh the fields if said boolean is `true`.
# Rationale

The Swagger documentation is needed by many Vue components when calling the `makeModelConfig` helper.

In the past, we've imported the `documentationStore` in the component then accessed the `documentationStore.documentation` directly, which has caused issues when the documentation is missing (see issue #1458).

Often the cause is a hard refresh or a browser restart, which clears the `documentation` from the store, as the `documentationStore` is not persisted. Instead, the workbox cache is what is persisted, and when we hit the network using the `documentationStore.getDocumentation()` method, we can serve it up to the store from the cache. However, the old approach of accessing the `documentation` directly doesn't hit workbox and thus often has undefined documentation.

We also cannot have each Vue component use the `documentationStore.getDocumentation()` as it is async and adds too much complexity to components that are not setup to handle async (by having async `setup()` function and wrapping the component call in a `<Suspense>` block.

# New Approach

`Workflow.vue` already has async handling as it's at the top-level. So, we can have it access the `documentationStore.getDocumentation()` getter and `provide` it to the child components. Each vue file can then `inject` the provided data into the component.

See the [Vue dev docs](https://vuejs.org/guide/components/provide-injecthttps:/) for more info.

Note that if we need access to the item from `inject` in the `setup()` function, we need to use the Vue `inject()` function, as `setup()` doesn't have access to `this` yet. See [this](https://stackoverflow.com/a/72253376https:/) Stackoverflow thread for more details.

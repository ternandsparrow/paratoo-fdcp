> ### This document is a DRAFT. Please consider contributing to ensure the best clarity possible.

> TODO: UPDATE FOR STRAPI 4

<br>

# Error Handling

We want to ensure errors are caught and thrown in a consistent manner, such that the User is provided enough context of what went wrong. Some errors Users may not be able to fix, but other could be resolved if they are given a good error message. 

>Note: we will be integrating sentry to allow these errors to be sent to a centralized source for easy management.

There are three main error handling functions each named `paratooErrorHandler`. Two have identical implementation and are found in [Core's helpers](../paratoo-core/config/functions/helpers.js) and [Org's helpers.](../paratoo-org/config/functions/helpers.js); and a separate one with different implementation in the [WebApp's helpers](../paratoo-webapp/src/misc/helpers.js), due to the mechanism for handling WebApp errors being a little different.

## 1. Handling Errors in the Backend
### 1.1 **Example** - backend error handling
Consider the following scenario:

- `A`, which is some arbitrary function, makes a call on `B` for data.

- `B` makes a call on `C` who knows where the data is located.

- `C` then queries the database `D`.

- However, `D` has a problem with the request and throws an error.

The call chain looks like:

`A` -> `B` -> `C` -> `D`

And the error chain looks like:

`D` -> `C` -> `B` -> `A`

`C` is the first 'node' in the chain that `catch`es the error - we designate it as the ***`error origin`***. Therefore, it should specify each potential error that `D` could throw. It would be best to use a `switch` statement to do this with a `default` case that throws a 500. 

Thus, the `catch` block of `C` may look like:

```js
.catch(err => {
  switch(err.message) {
    case 'Duplicate entry': {
      let msg = 'Could not create an entry that already exists'
      return paratooErrorHandler(ctx, 400, err, msg)
    }
    case 'Invalid syntax': {
      let msg = 'The request does not have valid syntax'
      return paratooErrorHandler(ctx, 400, err, msg)
    }
    case 'Not authorized': {
      let msg = 'User is not authorized to access the requested data'
      return paratooErrorHandler(ctx, 403, err, msg)
    }
    case 'Not found': {
      let msg = 'The requested resources could not be found'
      return paratooErrorHandler(ctx, 404, err, msg)
    }
    default: {
      return paratooErrorHandler(ctx, 500, err)
    }
  }
})
```

This is just an example and the actual errors thrown by '`D`' will vary.

Note that it's important to include the `default` case. Though we're throwing a `500`, `paratooErrorHandler()` contains Sentry integration, so this `500` will be logged. We don't include a `msg` as `500`'s override this.

---

When `B` receives this error from `C`, it doesn't have to check what the error is and rethrow, so it will simply rethrow the error without checking it.

Thus, the `catch` block of `B` would look like:

```js
.catch(err => {
  let msg = err.response.data.message
  return paratooErrorHandler(ctx, err.response.status, err, msg)
})
```

- `ctx` is the context, which is required to perform the `ctx.throw()`.
- `err.response.status` is the HTTP statusCode, e.g., 401, 403, 404, 500, 502, etc.
- `err` is the `Error` object. `paratooErrorHandler()` needs this to access the `err.message` which is the generic HTTP message. 
  - For example `404`'s message is `"Request failed with status code 404"`.
- `err.response.data.message` is the custom message (`msg`) string provided to the last `paratooErrorHandler()` call in the chain. 
  - For example, if `D` threw a `Not Authorized` error, then the value of `err.response.data.message` in `B`'s `catch` block would be 'User is not authorized to access the requested data'
  - Note that the HTTP message (discussed in the point above) and our `msg` are different.

In the above `catch` block for `B`, we simply pass on the `msg`. But if we wanted, `B` could append additional data to the `msg` chain. As such, the `catch` block of `B` could also look like:

```js
.catch(err => {
  let msg = err.response.data.message + '. This is some extra info that B is attaching'
  return paratooErrorHandler(ctx, err.response.status, err, msg)
})
```

---

Finally, `A` will catch/throw the message like `B` did. In this example, `A` also attaches some extra information so its `catch` block looks like:

```js
.catch(err => {
  let msg = err.response.data.message + '. An error just occurred at A'
  return paratooErrorHandler(ctx, err.response.status, err, msg)
})
```

---

Let's say that C threw a `404`. Thus, the final error displayed to the User would then look like:

```json
{
  "statusCode": 404,
  "error": "Not Found",
  "message": "Request failed with status code 404. The requested resources could not be found. This is some extra info that B is attaching. An error just occurred at A"
}
```

Notice the structure is: \<generic HTTP message> \<message sent by DB> \<subsequence chained `msg`>

## 1.2 Semantics of `paratooErrorHandler()`
> `paratooErrorHandler(ctx, code, err, msg)`

As discussed in the above example, we pass in `err` so that we can get to `err.message`, which allows us to include the generic HTTP message. We then take the passed `msg` and concatenate it, like so:

```js
var message
if(!msg) {
  //if no message is provided then we use the Error obj message
  message = err.message
} else {
  message = err.message + '. ' + msg
}
```

## 1.3 Semantics of `ctx.throw()`
> `ctx.throw(code, message)`

After constructing the `message`, `paratooErrorHandler()` will return a `ctx.throw()`

The `code` is the HTTP status code that was provided to `paratooErrorHandler()`. The `message` will typically include the generic HTTP `err.message` and potentially additional information if `msg` was passed.

<br>

# 2. Error Handling in the WebApp
The WebApp's error handling differs as we typically want to display the errors to the user with Quasar's `notify`.

## 2.1 **Example** - WebApp error handling for server requests
Let's say that a page (`A`) in the WebApp `dispatch`es a request to a function in `B`, which then `dispatch`es to `C`, which then makes a call on the server `D`.

`D` might do some error chaining as discussed in section 1, above, and finally return an error to `C`. 

Thus, the request chain would look like:

`A` -> `B` -> `C` -> `D`

And the error chain would look like:

`D` -> `C` -> `B` -> `A`

Unlike the backend, however, we don't need check the error and each node in the chain will simply forward the error onwards. Providing additional information to `err.response.data.message` is option, as with the backend error handling.

---

Therefore, the `catch` block of `C` who performed a request of the server would look like:

```js
.catch(err => {
  let msg = 'C encountered a problem with the server request. ' + err.response.data.message
  throw paratooErrorHandler(msg, err)
})
```
As with the backend error handling, `err.response.data.message` is the chained message generated by one or many calls to the server's implementation of `paratooErrorHandler()`.

---

The `catch` block of `B` would then look like:

```js
.catch(err => {
  let msg = 'There was a problem doing action B. ' + err.response.data.message
  throw paratooErrorHandler(msg, err)
})
```

---

It is then up to the last node in the chain to also make a call to `notifyHandler()` which will display a popup message to the User. Thus, `A`'s `catch` block would look like:

```js
.catch(err => {
  const msg = 'Action A generated an error'
  notifyHandler('negative', msg, err)
  throw paratooErrorHandler(msg, err)
})
```

---

Thus, the `notify` error would look like:

```
Error: C encountered a problem with the server request. <server error message>. Caused by: Request failed with a status code <HTTP response code>. There was a problem doing action B. Action A generated an error
```

The reason that The server error message is after `C`'s message is because of the concatenation with `err.response.data.message`. 'Caused by...' is generated by `chainedError()` which is called by `paratooErrorHandler()` and includes the HTTP message. The rest of the error chain's messages are then included.

While this may provide dense error messages, it wil enable these errors to include as much detail as possible in all steps of the process. Of course, each node in the error chain doesn't have to include additional `msg` information, and can just pass on the `err.response.data.message` from the previous error.

## 2.2 **Example** - General WebApp error handling
Consider the request and error chains from section 2.1:

Request chain:

`A` -> `B` -> `C` -> `D`

Error chain:

`D` -> `C` -> `B` -> `A`

In situations where `D` is not a server, it is considered the ***`error origin`***. The simple solution is to just `throw` a string with an initial error message. Subsequent nodes in the chain will catch/throw in the same way as the example in section 2.1, with the final `throw` also calling `notifyHandler()`

---

If the ***`error origin`*** is also the only node in the chain, then it will simply call both `notifyHandler()` and `paratooErrorHandler()`. 

## 2.3 Semantics of `notifyHandler()`
> `notifyHandler(type, msg, err)`

We use Quasar's built in notification plugin:

```js
Notify.create({
  type: {('positive' | 'negative' | 'warning' | 'info' | 'ongoing')},
  message: {String}
})
```

For simplicity, we use some predefined out of the box 'types'. Errors will usually include the `'negative'` type which will set the notification red with an error icon.

As with `paratooErrorHandler()`, it will construct a `message` with the `err` and `msg` provided as parameters, with `err` being optional.
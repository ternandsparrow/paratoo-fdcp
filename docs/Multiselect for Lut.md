# Multiselect for lut


In some cases we want to select multiple values for LUT, like in the following example:

![example](./screenshots/lut_multiselect_1.png)

---

To do this, create a component with "multi-lut" category in strapi admin dashboard.

![example2](./screenshots/lut_multiselect.png)

Then add the following attribute to the schema of the component (json file), the name of the relation is not compulsory to be 'lut' but it is recommended to maintain the consistency for data export in the future:

```json
{
  "collectionName": "components_multi_lut_floristics_phenologies",
  "info": {
    "displayName": "floristics-phenology"
  },
  "options": {},
  "attributes": {
    "lut": {
      "type": "relation",
      "relation": "oneToOne",
      "target": "api::lut-floristics-phenology.lut-floristics-phenology"
    }
  }
}
```

Then add the component to the collection like other normal components, not that the "repeatable" attribute must be set to **"true"**


```json
    ...
    "phenology": {
      "type": "component",
      "repeatable": true,
      "component": "multi-lut.floristics-phenology"
    },
    ...
```

Note: the logic of this functionality can be found in the [Multiselect.vue](/paratoo-webapp/src/components/MultiLutSelect.vue) component , and in the makeConfigFromSchema function in [helper.js](https://gitlab.com/ternandsparrow/paratoo-fdcp/-/blob/d2b29d9d8c8fc82b242010205658d8d6be698d45/paratoo-webapp/src/misc/helpers.js#L1176)
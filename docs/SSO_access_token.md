# Developing a Native Version of the App with Open ID Connect support
The current version of the app supports both jwt and SSO access token for authorization.
It uses the authentication code flow with PKCE to handle OIDC authentication.

## Build 
There are some environment variables need to be changed to enable OIDC support.

1. enbale oidc flag
```
VUE_APP_ENABLE_OIDC=true
```

2. define core and org base urls
```
CORE_API_URL_BASE=http://localhost:1337
ORG_API_URL_BASE=http://localhost:1338 
```

3. define api prefixes. for example: "/api" or "/ws/paratoo"
```
CORE_API_PREFIX='/api'
ORG_API_PREFIX='/api'
```

4. If the app uses oidc token then we dont need to validate token using validate-token api.
we can disable it. <br> We may need to find a way to validate sso access token.
```
ORG_ENABLE_TOKEN_VALIDATION=false 
```

5. Define OIDC provider informations like client name, authority and client id.
```
VUE_APP_OIDC_CLIENT='insert_client_name_here'
VUE_APP_OIDC_AUTHORITY='insert_authority_here'
VUE_APP_OIDC_CLIENT_ID='insert_client_id_here'
```
# Protocol suggestion after submission

- In some cases, we may want to suggest to users that they perform another protocol after submitting the first one. For instance, once the user successfully submits the "Soil pit characterisation" protocol, the application will prompt them to consider doing "Soil Bulk Density" as well.

- To implement this, simply include the UUID of the protocol you wish to suggest to the "nextProtSuggestion" of the protocol, which you want the prompt to appear, [schema.json](../paratoo-core/src/api/protocol/content-types/protocol/schema.json) of the protocols . Keep in my there are "hidden" fields before the "nextProtSuggestion", so it should look like the example below:

**Before**
```json
[
      "1de5eed1-8f97-431c-b7ca-a8371deb3c28",
      "Soil Observation and pit characterisation",
      "soils",
      "/soil-pit-characterisation-fulls",
      "1",
      "Enhanced",
      "true",
      [
       ... workflow
      ],
      false,
      "39da41f1-dd45-4838-ae57-ea50588fd2bc"
    ],
```
**After**
```json
[
      "1de5eed1-8f97-431c-b7ca-a8371deb3c28",
      "Soil Observation and pit characterisation",
      "soils",
      "/soil-pit-characterisation-fulls",
      "1",
      "Enhanced",
      "true",
      [
       ... workflow
      ],
      false,
      "39da41f1-dd45-4838-ae57-ea50588fd2bc"
    ],
```

# Setting up and Developing a Native Version of the App with Capacitor

## Requisites
- The current version of the app is designed specifically for Android, and may not function properly on iOS.

## Current issues with the native build
- The Geolocation Plugin's watchPosition function can only update the current location every 5 seconds in native mode. However, it works without any issues on PWA. Additionally, the current version of the plugin is not official and requires updating to the latest official version when it is released.
-  The official Camera Plugin from Capacitor doesn't have a record video feature, we are using an open-source plugin that is no longer maintained.
- While crashing during development mode is occasionally observed, it hasn't been witnessed in production.

## Preparation 
1. Download Android Studio from https://developer.android.com/studio/index.html and save it in your Downloads folder if you're on Linux. If not, follow the steps in this link https://quasar.dev/quasar-cli-webpack/developing-capacitor-apps/preparation to download and install it.
1. Run the "setup_cap.sh" bash script located in the "helper-scripts" folder. This will install Android Studio for you

**Using with a android mobile device**: go About Phone > Build number, and tap it until you see 'You are now a developer', then you will see the 'Developer options' tab appear in the setting, open it and enable the 'USB debugging' or 'Wifi debugging'. Now you can debug the app with Android Studio 

**Using Android Studio's emulator(not recommended)**: choose "Create device" in Android Studio and chose a device of your choice, then choose 'Run app' or 'Debug app'. Warning: this may freeze your pc as it usually consume nearly 100% CPU. This can slightly improve by doing following, but freezing may occasionally happens:
- turning 'Power saving mode' on by going to File > Power saving mode.
- A turn off audio of the emulated device, by opening config file at '~/.android/avd/<AVD_Name>.avd/config.ini' and change those two lines:
```
hw.audioInput=no
hw.audioOutput=no
```
## Adding new package 
- In addition to adding the package to the main "paratoo-webapp" folder, you also need to add it to "paratoo-webapp/src-capacitor" because Quasar runs the web and native versions separately. To do this, run the following commands:
```
yarn add <package_name>
npx cap sync 
```
- Then in Android Studio, do File > Sync Project with Gradle File

## Bumping to new version of Capacitor
To update to a new version of Capacitor, run:
```
npx cap update
```

## Developing 
To start a development server, run yarn start:cap. This will start the development server for Capacitor and launch Android Studio.

## Build 
1. Generate a key for signing the APK file using the following command outside the repository folder, as a keystore file will be created in the current location:
```
keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 20000
```
2. Change to the paratoo-web folder and execute the command:
```
./buildNative <path/to/the/key_file.jks>
```
3. The compiled APK file "paratoo_fdcp.apk" will be located at "paratoo-webapp/dist/capacitor/android/apk/release/paratoo_fdcp.apk".

Note: You can also compile the APK file directly in Android Studio, but it will release a build at the development state.
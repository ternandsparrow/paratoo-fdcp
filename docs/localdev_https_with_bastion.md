# Overview

To access certain browser APIs (e.g., `navigator` for media and location) when pointing other devices to our localdev instance, we need a [bastion host]('https://en.wikipedia.org/wiki/Bastion_host') to server as a HTTPS proxy, since these sorts of APIs only work over secure connections. Thus, we use a VM to with an Ngnix proxy that can use an SSH tunnel to our localdev machines.

# Setup

There are some steps and configurations that need to be made before opening the SSH tunnel:

- change the app's environment variables from localhost to the bastion host:

```
CORE_API_URL_BASE=https://core.bastion.paratoo.tern.org.au
ORG_API_URL_BASE=https://org.bastion.paratoo.tern.org.au
```

- set the app's dev server to use HTTPS, in the `quasar.conf.js`:

```json
devServer: {
  https: true,
  port: 8080,
  open: false,
},
```

- restart the webapp for these changes to take affect
- start core and org

# Open the SSH tunnel

- Run the following SSH command (where the value for the `-i` flag is your SSH key (will need to be added to remote host)):

```
sudo ssh -v -N -R 8080:localhost:8080 -R 1337:localhost:1337 -R 1338:localhost:1338 bastion.paratoo.tern.org.au -l ubuntu -i ~/.ssh/paratoodev &
```

- keep the terminal open

- navigate to the app at: `https://app.bastion.paratoo.tern.org.au/`

Other devices, such as mobiles and tablets should be able to access the localdev instance in a secure context. Accessing without the bastion proxy (i.e., hitting the IP address / port of the App running on a localdev machine) will usually prevent the use of the camera and location.

# Close the SSH tunnel

The tunnel can be a little flaxy. Typically executing the `exit` command in the SSH connection's terminal should stop it, but often the process will partially crash. You can try:

- [checking open SSH processes](https://cplusprogrammer.wordpress.com/2016/10/17/how-to-check-if-ssh-is-running-on-linux/) with `ps aux | grep sshd`
- [killing all processes open on the SSH port](https://stackoverflow.com/a/9346231) (including backticks): ``` sudo kill -9 `sudo lsof -t -i:22` ```
- [restarting the SSH service](https://unix.stackexchange.com/a/683102)
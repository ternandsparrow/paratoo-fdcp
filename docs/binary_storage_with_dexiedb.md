# Binary File Storage with DexieDB

- Binary files like photos, videos, and audio are stored in indexedDb with DexieDB due to the 5mb limit of localStorage.

- When a collection is sent to bulk store through `addGeneralModelEntry` function, the `findAndSyncBlobInCollectionWithDexie` function in there will recursively looks for any Blob objects within the collections, saves them to the 'binary' table in indexedDb, and replaces the Blob with its file_identifier (uuid). The blob uuid, modelName, and surveyId (as we may have multiple submission of the same protocol in the queue) are used as keys for data lookup.

- To handle file deletion when editing the collection, `findAndSyncBlobInCollectionWithDexie` compares the files in the collection with files in the indexedDb of the same modelName. The files are only synced when the user presses submit and the data is emitted to bulk store.

- Since the blob is replaced with a uuid, the Vue component can't render these files to the UI. Therefore, components like AsyncImg.vue, AsyncAudio.vue, and AsyncVideo.vue are used to handle these uuids. These components retrieve the Blob file uuid from Dexie and render them.

- When uploading the related collection, these files are retrieved and freed from the storage when uploading them to CORE.

- All the utils related to handling binary files can found in the [binary.js](../paratoo-webapp/src/misc/binary.js)

# App Store Data Conventions

## Overview

The app uses Pinia to store persistent data client-side, such that we reduce API calls for certain data. These stores are:

- `apiModels`: contains LUT data and collection data that has done a 'round trip', i.e., has been submit and then retrieved from the Core API
- `auth`: stores authentication/authorisation data and user project/protocol information
- `bulk`: tracks collected data on a per-protocol basis
- `documentation`: contains the swagger documentation, which is used to generate the UI programmatically
- `dataManager`: a generic and opaque mechanism to interact with various types of data: online-submit collection data that has 'round trip' (apiModels) and pre-published collection data either collected online or offline (dataManager's `publicationQueue`). Note that regardless of network connectivity, all collection data is 'queued' such that it is treated the same, simplifying the workflow of syncing said data with cloud.

The data flows between the stores and API like so:

![app-collection-data-flow-sequence](ARCHITECTURE/diagrams/app-collection-data-flow-sequence.png)

## General conventions

To ensure consistent behaviour and to make the job of the dataManager easier, we ensure certain conventions are followed when interacting with the stores. These conventions are:

- (1) reading data from the store should be done with `getters`. Examples:
  - attempting to retrieve an in-progress bird survey:
    - (a) good: `bulkStore.collectionGetForProt({ protId: <id> })['bird-survey']`
    - (b) bad: `bulkStore.collections[<id>]['bird-survey']`
  - attempting to retrieve a previous bird survey:
    - (c) good: `apiModelsStore.cachedModel({ modelName: 'bird-survey' })`
    - (d) bad: `apiModelsStore.models['bird-survey']`
- (2) writing data to the bulk store (apiModels is typically read-only, except when initialising/populating it from the Core API itself) should be done with `actions`. Collection data should **never** be directly written to, only via using the `$emit` component event, as we need to ensure all Vue components are interacting with the data consistently. However, time constraints make if difficult to do this without major, time-consuming refactors; these cases we need to document in [#791]('https://gitlab.com/ternandsparrow/paratoo-fdcp/-/issues/791').

> TODO extend this list - it is likely not complete and currently covers the most relevant cases for offline support

## Data Manager and offline collections

### Data Manager overview

The data manager is a store that we can use to abstract interactions with the apiModels and bulk stores. It allows us to opaquely read from and write to the stores without concerning ourselves with handling links between dependent models. That is, models (or protocols) collected are often used as part of other protocols (e.g., Floristics vouchers being used in Cover PI). When online, the dependent models (e.g., Floristics) have already performed the round-trip to the apiModels store, and thus we assume that's where those vouchers will be and are accessed via `apiModelsStore.cachedModel` getter. However, when offline these vouchers have not round-tripped and are thus going to throw errors when we try access this data.

The solution is to have the data manager 'override' these sorts of getters; `cachedModel` being an example of such a getter. Collected 'submit' offline go into a `publicationQueue` which can then be accessed by `dataManager.cachedModel` (also accesses apiModels for historic, round-tripped data).

### Data Manager patterns - queuing offline collections for later submission

When submitting a collection we send *all* collections to the `dataManager.publicationQueue`

1. resolves decencies the collection may have on other already-queued collections
2. queue the collection by moving it from the `bulkStore.collections[<id>]` to the publication queue, which is an array
3. queue the collection's submodule, if applicable
4. handle assigning temporary relations (`temp_offline_id`) to various models. We choose to manually implement these cases as we know there are only a limited number; a completely generic implementation *would* be desired only if we were unsure of future dependent protocol models. These cases are:
   - creating temporary links between 'step' models (i.e., `plot-layout` and `plot-visit`) and setting the `bulkStore.staticPlotContext` using an `action`
   - creating temporary links for depended-on models (i.e., `floristics-veg-voucher-full`, `floristics-veg-voucher-lite`, `vertebrate-trapping-setup-survey`). These models are 'depended-on' as other protocols (e.g., Cover PI) may use these models and thus need a temporary link to resolve

The `handleSubmission` helper (called by `submitCollection`, which it called from `Workflow.vue`) determines whether to queue a collection (called by `Workflow.vue`), or whether a particular collection needs to be published (called by `dataManager.submitPublicationQueue()`) to the API/cloud.

### Data manager patterns - submitting/publishing the offline queue

The publication queue is sent to the Core API in the order they were queued, which ensures that depended-on models are published and round-tripped before the dependent models attempt to access backend relational data.

Every time we submit an item from the queue, we track the submission and map any temporary IDs it had assigned. For example, if we submit Floristics Vouchers, we assign the temporary IDs to the API response in a separate tracking array. If the collection has a temporary relation to another model (i.e., a 'dependent' model), we resolve these references before submission, such that the relations are created on the backend correctly. For example, if we submit Cover PI, we resolve all temporary relations to the Floristics Vouchers (would have already been submit and tracked) before submitting Cover PI.

During this process, errors are handled such that any collections the failed collection submission depend on are skipped. For example, if Floristics Vouchers failed to submit, Cover PI is skipped (if it used any of those vouchers) and both are kept in the queue to retry later.

### Transferring existing implementation to work offline

Protocols that will need to be handled are those that use data from other models that have already round-tripped. In these cases, we need to (no particular order as it's often iterative):

1. specify any *soft* links between models using `x-paratoo-soft-relation-target` flag. Normal relations ('hard' links) are taken care of in the below steps, but 'soft' links - where we re-use data from previous collections but don't make relations to said data - need an additional 'hint'.
2. handle creating the temporary offline IDs in `dataManager.queueSubmission`.
3. handle resolving temporary offline ID links in `dataManager.submitPublicationQueue` .(will usually require existing existing helpers - e.g., `resolveVoucherRelation`)
4. refactor client-side store access to use the conventions laid out above (general and data manager). This is often the largest body of work, as we often perform custom logic in Vue components that don't consider temporary links (e.g., we often search/filter Floristics Vouchers based on their `id`, which only exists after a completed round-trip).
5. write offline-specific tests. While it is possible to re-use existing tests with an 'offline' flag (see [#909]('https://gitlab.com/ternandsparrow/paratoo-fdcp/-/issues/182?work_item_iid=909')), the offline tests should perform additional logic to explicitly check the data manager is correctly creating and resolving links throughout the workflow. That is, after queuing a collection (tests use the `workflowPublishOffline` Cypress command), we use a custom callback to check that collection created all temporary links for all its models. Also, when we submit the offline queue (tests use the `submitOfflineCollections` Cypress command), we create various network requests to ensure relations were created correctly.

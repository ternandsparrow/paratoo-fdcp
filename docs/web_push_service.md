# Web Push Service

## Overview

We use a web push service to send push notifications to users. Unlike native push notifications on mobile devices, web push notifications are handled by the browser.

The web push service is distinct from the `Notification API`. In [PushNotification.vue](../paratoo-webapp/src/components/PushNotification.vue), the service worker creates a subscription, prompting the browser to generate an endpoint for the app. This endpoint persists unless manually unsubscribed or app data is wiped. The endpoint is then sent to CORE's `push-notification-subscription` for future notifications.

We use a key pair called "VAPI key" consisting of a private and public key to enhance the security of the web push service. To create this key pair, navigate to the `paratoo-core` folder and run:

```bash
yarn web-push generate-vapid-keys --json > vapikey.json
```

Use the generated keys for `CORE_PUSH_VAPI_PRIVATE_KEY` and `CORE_PUSH_VAPI_PUBLIC_KEY` in `Core`, and `VUE_APP_WEB_PUSH_PUBLIC_KEY`.

The current scheduling feature is inspired by the [Scheduler Plugin](https://market.strapi.io/plugins/@webbio-strapi-plugin-scheduler), which has not been maintained for 2 years. We have a similar [feature](https://docs.strapi.io/user-docs/releases/managing-a-release) in the Strapi business version, but not in the free version.

## How to Create a Push Message

1. On Core's admin dashboard, select `push-notification` in `Content Manager`, then `Create new entry`.
2. Enter the `title`, `message`, and `type`.
  - The `scheduledAt` field is the time the message will be sent to users.
  - **Important!** The `isSent` field should be left as `false`, otherwise the message won't be sent.
3. When the scheduled time is met, a `setTimeout` will trigger a function to update the `publishAt` attribute of the record to the `scheduledAt` time. This triggers the `afterUpdate` event in [lifecycle.js](../paratoo-core/src/api/push-notification/content-types/push-notification/lifecycles.js), which then calls [webPush](../paratoo-core/src/api/paratoo-helper/functions/webPush.js) to send the data.
4. The message will be sent to all subscriptions stored in `push-notification-subscription`. The service worker receives the message through an event listener for the `push` event. If there is no open tab, a notification will be shown if the user has allowed the app to show notifications. Otherwise, a notification in the app will be shown immediately using the BroadcastChannel API to send data from the service worker to the client.

## Special Cases

### 1. Unsent Message with Outdated Schedule

This occurs when Core is offline during the scheduled time. When Core starts up again, all outdated messages will be sent immediately.

### 2. Required Fields Not Enforced

Currently, the admin dashboard does not enforce all required fields, allowing the creation of a notification without the `scheduledAt` field. However, the logic in the lifecycle will handle this and publish the notification immediately. This can be used to publish messages ASAP.

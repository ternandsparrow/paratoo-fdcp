# Add test users to org

During testing, our aim is to include initial accounts when setting up the stack for testing purposes. However, we aim to avoid hardcoding this information into the codebase, as well as the manual addition of these accounts each time we run tests.

To address this, the Paratoo ORG now provides the option to add test users by adding a test_users.env file containing user credentials, as demonstrated below:

```env
  TEST_USER5=weqwewqewqeq
  TEST_USER5_PASS=dsadsadad
  TEST_USER5_EMAIL=tech.contact@environmentalmonitoringgroup.com.au
  TEST_USER5_PROJECTS=11

  TEST_USER6=TESMaweqwewewqenager
  TEST_USER6_PASS=sdadada
  TEST_USER6_EMAIL=tech.contact@environmentalmonitoringgroup.com.au
  TEST_USER6_PROJECTS=11
  TEST_USER6_ROLE=project_admin
```   
**NOTE:** 

- TEST_USER_{index}_ROLE is OPTIONAL
- The credentials for test users must adhere to the following format. In this format, {index} represents the user's index number, which does not necessarily need to start from 0.
- The PROJECTS list can be multiple but need be separate but separated by commas.
  ```env
  TEST_USER{index}=eqwewqeq
  TEST_USER{index}_PASS=weqwewqewq
  TEST_USER{index}_EMAIL=tech.contact@environmentalmonitoringgroup.com.au
  TEST_USER{index}_PROJECTS=1,2,3,4,11
  TEST_USER{index}_ROLE=project_admin
  ```  
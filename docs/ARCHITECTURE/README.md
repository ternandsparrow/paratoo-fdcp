# Roles in the system
The system needs to support the followng roles:
 - public
    - any unauthenticated user
    - read any LUT
    - maybe read site data
 - collector
    - tasked with collecting data for the protocol assigned to them
    - will be assigned to 0-m project(s)
    - has access to the collection UI
 - curator
    - tasked with QC of data for projects assigned to them
    - will be assigned to 0-m project(s)
    - has access to the curation UI
 - system-curator
    - tasked with maintaining records not tied to a protocol, like Site Locations
 - machine2machine-read
    - can read anything in the system
    - used for the upstream publishing system to *pull* data
 - system-admin
    - god mode
 - project-admin
    - authenticated user
    - tasked with maintaining project area and plot selection tied to a project.

# Assumptions
1. the system can have multiple copies deployed
1. the system will *not* be multi-tenanted
1. protocols are encoded into the system so if other groups deploy an instance
   of this stack, they won't be touching the protocols. Or if they do, they
   forfeit the ability to easily deploy updates from us.
1. we will distribute the system in a way that makes it easy to receive
   updates. For example, a Docker image that you configure and run or possibly
   making it into a Strapi plugin so to use it you simply need to create a new
   NodeJS project and `npm install` our plugin.

# Choosing a REST API
There are many options for creating a RESTful HTTP API. If we choose a solution
that requires some handcoding, we want it to be in the JavaScript language. The
options that we assessed are:
  - https://strapi.io/
  - https://feathersjs.com/
  - https://resthapi.com/
  - https://docs.getodk.org/getting-started/
  - https://www.fastify.io/
  - https://github.com/akvo/akvo-flow
  - https://loopback.io/
  - http://postgrest.org/
  - https://github.com/squidex/squidex

This list was narrowed down to Strapi, Loopback and PostgREST. PostgREST is
quite different to the other options, and you can see the comparison below. In
the end we chose Strapi. Prototyping with Loopback was turning up too many
places where it didn't work or we were fighting with the framework to make it
work like we need (producing the DB schema we want). PostgREST made it very
easy to create a CRUD API on all the models but dealing with non-trivial auth
and adding business logic weren't that easy.

## Strapi
Pros:
  - RBAC support
  - convention-over-configuration for a minimal project and easy upgrades
  - default controllers with no code
  - easy to tweak the system during bootstrap
  - no burden of TypeScript
  - the option to ship our "app" as a Strapi plugin
  - out-of-the-box admin UI that can be the curation interface
  - policies make it easy to implement our auth
  - all models get `created_{by,at}` and `updated_{by,at}`, which is nice for
       auditing
  - endpoints have paging and filtering by default
  - responses for REST calls embed 1-to-1 relationships by default
  - all models have an `id` PK column that's generated. I don't think we can
      control the name of it though, which is fine.
  - adding a field to a model does auto-migrate the schema and keeps the data
  - changes to a model (add and remove fields) *does* automatically update the
      generated OpenAPI documentation fragments for that model
  - we can configure endpoint permissions in the bootstrap script
  - GraphQL plugin available

Cons:
  - it's a fast moving project so there'll be a bit of work to keep up
  - documentation doesn't go into massive detail but the code is very easy to read and debug
  - filtering system apparently isn't OpenAPI compliant
      ([link](https://github.com/strapi/strapi/pull/8486#pullrequestreview-517776297))
  - OpenAPI generation correctly identifies POST bodies but can't figure out GET
      response bodies even for simple models with no relationships. It just puts
      `{foo:"string"}`. That will no doubt be fixed soon and even if
      it isn't, there's a mechanism for us to adjust the generated doco.
  - GraphQL plugin seems to require hand defining what the graph looks like.
      Admitedly it won't change that often but it's a bit boring and error prone
      to do. I'm not sure if
      [this](https://github.com/strapi/strapi/blob/77be3ea4c596543d63e244a3bd7e92b486e2ba23/packages/strapi-plugin-users-permissions/config/schema.graphql.js#L18)
      is a way of semi-dynamically defining the schema.
  - the generated DB schema contains indexes but no FKs. It seems
      Bookshelf/Knex is capable of creating them but it must be turned off.
  - removing a field from a model does *not* remove it from the schema (at least
      when there's existing data, haven't tested without). It does stop asking for
      it for new records but it still serves the field up in responses with new
      records have `null` for a value.
  - no support for API versioning yet, but [it's on the
      roadmap](https://portal.productboard.com/strapi/1-roadmap/c/17-versioning).

## Loopback
Pros:
  - Loopback gives us the option to do whatever we want as we can code it all to
      match our requirements.
  - easy to write custom auth validation
  - extra descriptive detail in OpenAPI is written in code, but we still have to
      author it all.
  - we get OpenAPI v3

Cons:
  - need to generate everything (controllers, repositories, models), which is a
      fair bit of code. Even though most of it is generated, we still have to
      maintain it all going forward.
  - we found ourselves fighting with the framework to get the schema we want.
      Foreign key support isn't great and trying to get a postgres schema
      generated that enforces our contraints at the DB level was proving a
      challenge.
  - versioning the API is probably possible but might be clunky. Possibly we'll
      need to run one instance of loopback per version, which feels hacky.
  - "try me" queries in Swagger UI don't work. The generated requests contain
      example field names that don't exist, and this causes a 500 on the server.
  - hard to upgrade to a new version, and there will be new versions. There will
      be migration guides but compared with PostgREST, which is a 30 second job
      to upgrade, this will be much more effort.
  - during prototyping, we've seen a number of 500 responses for various
      unhandled situations. This is because the generated code doesn't handle it
      gracefully so we'd have to fix all that by hand.
  - in theory we could change to another DB vendor but in practice it's still a
      lot of work as our code is tightly coupled to postgres.
  - the loopback discovery process doesn't create any foreign keys for us, we
      have to do that all by hand. See [this
      question](https://stackoverflow.com/a/58238728/1410035) for more on
      this.
  - our original schema had unbounded `character varying` fields. Loopback's DB
      adapter, `juggler`, cannot deal with this. It tries to add a limit to the
      field and if we don't supply a value it will try to use `undefined` and
      fail. There is no difference between unbounded `varchar` and `text`, so
      we're swapping to `text` in all these instances.
  - TypeScript. For a small project, the extra overhead it adds slows development
      speed for little to no benefit.

## PostgREST
Pros:
  - no code, the DB schema is the source of truth
  - auth can be done by granting roles in the DB and creating and assigning
      those roles in Keycloak.
  - it is performant
  - easy to upgrade to a new version, just change the version tag on the Docker
      image.
  - extra swagger detail is written in comments on schema, tables and columns.
      This means we can manage it with the DB schema as part of migrations.
  - we can inline relationships in requests to reduce the number of HTTP
      requests required.
  - no support for API versioning out-of-the-box but we can handle it with a
      facade (probably Express or similar) and for older versions routing to
      PostgREST endpoints that are DB views that keep that old schema. We may be
      able to create a schema-per-version and use the facade to set the
      `(Accept|Content)-Profile` header from [here](http://postgrest.org/en/v7.0.0/api.html#switching-schemas).

Cons:
  - kind of locked-in to postgREST but that's not a big issue seeing as we're
      all-in on postgres anyway.
  - only get Swagger v2, not OpenAPI v3
  - more complicated business logic means we either need to handle it in pl/sql,
      (or one of the other PL languages), or by creating a facade in front of
      PostgREST that adds its own endpoints to the API. This latter approach is
      probably no more difficult than coding them up in Loopback.
  - it may not be possible to work with DB transactions from any facade we
      create. Assume we want to create an A record and a number of linked B
      records all in one HTTP call. We want it to happen inside a transaction
      but if we make separate HTTP calls from the facade, we might not be able
      to use a single transaction. It seems likely we'll need some sort of
      stored procedure on the DB side to help.


## Versioning
Changes to the protocols will be rare but the system must support it. We must
version both the protocols *and* the look-up-tables (LUTs).

This means that protocols will reference the version of the LUTs they use, and
projects will reference the version of the protocols they use. Collections
submitted by the user will also reference the version of the protocol they used
to collect.

Version numbers will be semantic so the largest value will be the newest
version.

The versioning of protocols will be done at the top level for the protocol, not
for each model that makes up the protocol.

There's a fair bit of effort to build the version system so we'll defer most of
it until the second version is needed. Initially we'll make sure the DB model
can support versioning and that will be enough.

We need a plan for deprecating older versions of a protocol. For instance, a
user may start a collection for protocol v1 but before they get a chance to
submit, the project moves to protocol v2. The system should still accept the
submission of this already started collection using v1, but the system should
force all collections started from this point on to use v2.

`paratoo-core` must have an endpoint exposed that serves the JSON schema for a
given protocol. This endpoint should accept a parameter to specify the version
of the protocol, but if not supplied will default to the latest version.

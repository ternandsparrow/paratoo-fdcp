# Paratoo Org Interface specification v1.6

## What is this document

This document is a specification for the interface, "Paratoo Org Interface",
that the `paratoo-org` system component needs to implement. See the Swagger documentation for request bodies, parameters, responses, etc.

### Glossary

- `paratoo-core`: component of the system that deals with observation data
- `paratoo-org`: component of the system that conforms to the "Paratoo Org
  Interface" to control the organisation structure (users in projects,
  protocols assigned to projects, etc)
- `client-app`: UI that a user interacts with to perform a `collection`.
- `collection`: a user going out, making a series of observations, and
  submitting (uploading) them to `paratoo-core`

Trust relationship diagram between the three main entities:

![Trust relationship diagram](diagrams/Trust%20Relationship%20Diagram.drawio.png)

# 1. Policy decision point

The user must be logged in to use the system but `paratoo-core` will _not_
handle login. How the login process happens is not specified, that is left as an
implementation detail for `paratoo-org` and `client-app`.

The only thing that matters is that HTTP requests made to `paratoo-core` have a
JWT for authorization, and the JWT contains a `preferred_username` claim.
`paratoo-core` is only a Policy Enforcement Point, it defers to `paratoo-org`,
which will act as the Policy Decision Point.

This means `paratoo-org`, as an implementor of the "Paratoo Org Interface" must
expose an endpoint to allow the Policy Decision Point check.

Definitions:

- projectId: a project ID from the "Projects and protocols" endpoint
- protocolId: a protocol ID from the "Projects and protocols" endpoint
- protocolUuid: a protocol UUID from the "Projects and protocols" endpoint. We distinguish from the ID, as for long-running stacks, the actual ID used by Strapi as a FK *may* change. Thus we also assign a static UUID to each protocol
- action: the action the user is trying to perform, one of:
  - `write` - most protocols have write permission, but some will not, so we must check
  - `read` - all protocols will at least be readable
- JWT: the auth token for the current user
- role: the name of the role the user has, which is specified on a per-project basis and ensures that any 'privileged' operations for a given project are restricted to roles that can perform said operations

The call looks like:

```
GET /pdp/<projectId>/<protocolUuid>/<action>
Header - Authorization: Bearer <JWT>
```

```JSON
{ "isAuthorised": true }
```

We cannot be certain if the supplied project/protocol ID exists, so we check:

- if the protocol exists
- if the project exists
- if the protocol is assigned to the project

Once this check is done, we continue with the normal policy decision for the User.

# 2. Project, protocols, and roles

The `client-app` needs to know which projects the user has access to, and in turn,
which protocols are assigned to those projects, and what the user's role is for that
project. It will achieve this by making a HTTP call to the component (`paratoo-org`) that
implements the "Paratoo Org Interface". The auth header passed identifies the current
user so the response will be tailored to suit. The `client-app` also needs to know which
plot-selections are assigned to the selected projects.

Roles ensure that particular actions are only performed by authorised users for each project they are assigned (see Appendix A.3 for example mappings between Monitor's Strapi roles and MERIT's internal roles). The `client-app` assumes the roles for each project are 'valid' according to the PDP, i.e., we do not assign a protocol to a project if the user's role for said project does not permit collection of said protocol. Thus, the returned response to `/user-projects` will likely need to do it's own internal PDP check. By including these roles and ensuring the protocols are valid according to said roles, we ensure that the `client-app` only presents protocols that the user is allowed to submit collections under; but including role checks in the PDP acts as a fall-back if this constraint is not followed, or when the `paratoo-core` API is hit directly.

```
GET /user-projects
Authorization: Bearer <JWT>
```

Example output:

```json
{
  "projects": [
    {
      "id": 2,
      "name": "Bird survey TEST Project",
      "protocols": [
        {
          "id": 4,
          "identifier": "d7179862-1be3-49fc-8ec9-2e219c6f3854",
          "name": "Plot Layout and Visit",
          "version": 1,
          "module": "Plot Selection and Layout"
        },
        {
          "id": 26,
          "identifier": "c1b38b0f-a888-4f28-871b-83da2ac1e533",
          "name": "Vertebrate Fauna - Bird Survey",
          "version": 1,
          "module": "Vertebrate Fauna"
        }
      ],
      "project_area": {
        "type": "Polygon",
        "coordinates": [
          {
            "lat": -27.388252,
            "lng": 152.880694
          },
          {
            "lat": -27.388336,
            "lng": 152.880651
          },
          {
            "lat": -27.388483,
            "lng": 152.880518
          }
        ]
      },
      "plot_selections": [
        {
          "name": "QDASEQ0001",
          "uuid": "a6f84a0e-2239-4bfa-aac6-6d2c99e252f1"
        },
        {
          "name": "QDASEQ0003",
          "uuid": "a6f84a0e-2239-4bfa-aac6-6d2c99e252f32"
        },
        {
          "name": "QDASEQ0005",
          "uuid": "a6f84a0e-2239-4bfa-aac6-6d2c99e252f3"
        },
        {
          "name": "SATFLB0001",
          "uuid": "a6f84a0e-2239-4bfa-aac6-6d2c99e252f4"
        }
      ],
      "role": "collector"
    },
    {
      "id": 2,
      "name": "Bird survey TEST Project 2",
      "protocols": [
        {
          "id": 4,
          "identifier": "d7179862-1be3-49fc-8ec9-2e219c6f3854",
          "name": "Plot Layout and Visit",
          "version": 1,
          "module": "Plot Selection and Layout"
        },
        {
          "id": 26,
          "identifier": "c1b38b0f-a888-4f28-871b-83da2ac1e533",
          "name": "Vertebrate Fauna - Bird Survey",
          "version": 1,
          "module": "Vertebrate Fauna"
        }
      ],
      "project_area": null,
      "plot_selections": [],
      "role": "project_admin"
    }
  ]
}
```

The `client-app` can now present all available projects to the user, where they can
select one. With a project selected, the `client-app` can see which protocols are
required to be collected. Selecting a protocol will start a `collection` workflow.

The data about the protocols (the JSON schema, vocabs) comes from the
`paratoo-core` system.

# 3. Project Areas and Plot Selection

> TODO tweak some wording re:roles

> TODO maybe make more generic beyond project areas / plot selection (e.g., 'privileged' actions)

While plots are not the concern of the `org-interface`, there is a need to include them when integrating with 3rd party systems such as MERIT. Additionally, there is a need to specify the area a given project takes place; a project area, which will allow us to link disconnected plots and collections by their project (see section 3 for additional information on the latter). For example, this will allow us to obtain floristics vouchers for more than just a single plot, but all plots within the project area, reducing the need to re-collect these vouchers at new plots within the same project area. 

Therefore, a `Project Admin` user with the role `project_admin` (same as `collector` role but can also make `PUT` requests to the `project` collection) shall be able to:
- define project areas for existing projects
- create plot selections, which involves specifying the name of the plot and it's approximate recommend location, in addition to other fields. It is assumed the user is online for this.
- link projects to their respective plot(s), allowing a given project to have any number of plots, and plots to belong to any number of projects

Project areas *must* be defined before creating plot selections, as the plot selection process involves specifying which projects to associate the plots with and thus we need to enforce that these plots are within the project area's bounds.

See Appendix A.4 for how MERIT 'sites' map onto our various concepts for plots.

# 4. Get ID for collection

`collection`s in `paratoo-core` need a way to be retrieved. `paratoo-core`
does _not_ store any organisation information as that is all delegated to
`paratoo-org`. This means a query like "get all collections for project A"
cannot work.

The solution is for `paratoo-org` to issue opaque identifiers. A mapping between
those identifiers and the projects, protocols and users is maintained _inside_
`paratoo-org`. This separation has the benefits:

- no flow-on effects from changes to organisation structure
- privacy: the data can be published without exposing organisation information

`paratoo-org`, as the "Paratoo Org Interface" implementor, **must** expose an
endpoint to issue identifiers.

Definitions:

- projectId: a project ID from the "Projects and protocols" endpoint
- protocolId: a protocol ID from the "Projects and protocols" endpoint
- protocolUuid: a protocol UUID from the "Projects and protocols" endpoint. We distinguish from the ID, as for long-running stacks, the actual ID used by Strapi as a FK *may* change. Thus we also assign a static UUID to each protocol
- version: version of the protocol that will be collected
- survey_metadata: the ID for a particular survey, which observations use as a reference, created by the `client-app` as JSON with keys: `survey_details` (i.e., the `survey_model` model name, such as 'bird-survey', `time` (e.g., created using `new Date(Date.now())`), `uuid`, `project_id`, `protocol_id`), `provenance` (which contains the `version` and `system`, which is the implementor of the Org Interface), and `orgMintedUUID`
- JWT: the auth token for the current user
- orgMintedUUID: a UUID generated by the Org Interface when creating the `orgMintedIdentifier`, which uniquely identifies a collection
- orgMintedIdentifier: composite data containing the survey metadata (details about the survey and provence information), user identifier, and event time

An example request:

```JSON
POST /mint-identifier
Authorization: Bearer <JWT>
{
  "survey_metadata": {
    "survey_details": {
      "survey_model": "<survey-model>",
      "time": "<time>",
      "uuid": "<uuid>",
      "project_id": "<project-id>",
      "protocol_id": "<protocol-uuid>",
      "protocol_version": "<protocol-version>"
    },
    "provenance": {
      "version_app": "<web-app-version>",
      "version_core_documentation": "<web-app-documentation-version>",
      "system_app": "<web-app-info>",
    },
  }
}
```

Org shall add it's provenance information to the survey metadata before encoding, which is structured like:

```JSON
"provenance": {
  "version_org": "0.0.0-xxxx",
  "system_org": "monitor-FDCP-development",
},
```

Note that in the above example the digits at the end of the `version` correspond with the commit SHA. The version doesn't need to conform exactly to this structure (our reference implementation will), but needs to at least identify the 'build' of the system.

Org shall append an `orgMintedUUID` to the survey metadata. For example:

```JSON
"orgMintedUUID": "e98b74db-0eab-4ed1-b322-5910ffa42fbe"
```

Org shall append the User's ID (some unique identifier for that user) to the orgMintedIdentifier (not the survey metadata).

The final orgMintedIdentifier (unencoded) will look like:

```JSON
{
  "userId": 1,
  "eventTime": "2024-03-07T02:03:23.465Z",
  "survey_metadata": {
    "survey_details": {
      "survey_model": "bird-survey",
      "time": "2024-03-06T23:30:12.304Z",
      "uuid": "fdaf8903-e141-4f3a-9225-5ffc72de4d10",
      "project_id": "1",
      "protocol_id": "c1b38b0f-a888-4f28-871b-83da2ac1e533",
      "protocol_version": "1",
      "submodule_protocol_id": ""
    },
    "provenance": {
      "version_app": "0.0.1-xxxxx",
      "version_core_documentation": "0.0.0-xxxx",
      "system_app": "Monitor FDCP API--localdev",
      "version_org": "0.0.0-xxxx",
      "system_org": "Monitor FDCP API-development"
    },
    "orgMintedUUID": "3e70eafb-73b8-4e57-864c-7faa4bcadcce"
  }
}
```

Org shall store this identifier (particularly the orgMintedUUID), the time it was
minted, and the associated user.

The returned Identifier is a stringified object that has been encoded as base64. 

```JSON
{ "orgMintedIdentifier": "eyJzdXJ2ZXlfbWV0YWRhdGEiOiB7ICJzdXJ2ZXlfZGV0YWlscyI6IHsgInN1cnZleV9tb2RlbCI6ICJiaXJkLXN1cnZleSIsICJ0aW1lIjogIjIwMjQtMDEtMDhUMDI6MDM6NTcuNzQzWiIsICJ1dWlkIjogImQyM2M1ZDZmLTkwMGMtNDY0OC1hODgxLTk3OThkNjVkMjBmMCIsICJwcm9qZWN0X2lkIjogIjEiLCAicHJvdG9jb2xfaWQiOiAiYTljYjllMzgtNjkwZi00MWM5LTgxNTEtMDYxMDhjYWY1MzlkIiwgInByb3RvY29sX3ZlcnNpb24iOiAiMSIgfSwgInVzZXJfZGV0YWlscyI6IHsgInJvbGUiOiAiY29sbGVjdG9yIiwgImVtYWlsIjogInRlc3R1c2VyQGVtYWlsLmNvbSIsICJ1c2VybmFtZSI6ICJ0ZXN0dXNlciIgfSwgInByb3ZlbmFuY2UiOiB7ICJ2ZXJzaW9uX2FwcCI6ICIwLjAuMS14eHh4eCIsICJ2ZXJzaW9uX2NvcmUiOiAiMC4wLjEteHh4eHgiLCAidmVyc2lvbl9jb3JlX2RvY3VtZW50YXRpb24iOiAiMC4wLjEteHh4eHgiLCAidmVyc2lvbl9vcmciOiAiNC40LVNOQVBTSE9UIiwgInN5c3RlbV9hcHAiOiAibW9uaXRvciIsICJzeXN0ZW1fY29yZSI6ICJNb25pdG9yIEZEQ1AgQVBJIiwgInN5c3RlbV9vcmciOiAibW9uaXRvciIgfSB9fQ==" }
```

Thus, collections can be retrieved for a given collection such as org_minted_uuid, project_id, protocol_id

An example request:

```
POST /api/protocols/reverse-lookup
Authorization: Bearer <JWT>
{
  "org_minted_uuid": <orgMintedUUID>
  or
  "project_id": <projectId>
  or
  "protocol_id": <protocolUuid>
}
```

Returns a list of collections with all the surveys and observations published by the an authenticated user. 

# 5. Collection event hook

When a `collection` is submitted from a user, `paratoo-core` should let the
"Paratoo Org Interface" know about this event. This is basically a webhook where
`paratoo-core` makes a HTTP call on `paratoo-org`.

```JSON
POST /collection
Authorization: Bearer <JWT>
{
  "orgMintedUUID": "<orgMintedUUID>",
  "coreProvenance": {
    "system_core": "<system-core>",
    "version_core": "<core-version>",
  },
}
```

Org shall update the entry for the stored orgMintedUUID created during
`/mint-identifier` and append when Core made the submission and Core's provenance
information.

```JSON
{ "success": true }
```

This call will need to be secured. To keep things simple, `paratoo-core` will
supply an API key in the `Authorization` header. The [OAuth Client Credentials
Flow](https://auth0.com/docs/flows/client-credentials-flow) could also be used
here, but as that is more onerous on the `paratoo-org` implementation, we
decided against it.

# 6. Identifier status

The `paratoo-org` component will issue and store `orgMintedUUID`s, which
`paratoo-core` might want to know the status of. Thus, `paratoo-org` will implement
an endpoint that `paratoo-core` can use to check this status:

```
GET /status/<orgMintedUUID>
```

```json
{ "isSubmitted": false }
```

# 6. WebAuthn

A way to authenticate using device credentials, such as fingerprints.

> TODO when WebAuthn is fully implemented (currently disabled as the move from Strapi 3->4 broke the proof-of-concept implementation)

See Appendix A.1 and A.2 for sequence diagrams.

# 7. Endpoint Access Control

> TODO update (and roll-up with section 2 - make subsection - remove this section) - what roles have access to certain *types* of endpoints (both org and core)

To ensure we apply the principle of least privilege, each user role (public, collector, and paratoo-admin) should only have access to the endpoints they need. As such, the permissions for Strapi's auth-generated endpoints can be changed in the admin panel. However, for custom endpoints, `paratoo-org/src/index.js`'s `customEndpointsPerms` variable must be modified to give particular roles the correct permissions. For example:

```
const customEndpointsPerms = {
  'api::org-interface.org-interface.generateLoginChallenge': ['public'],
  'api::org-interface.org-interface.decideRead': ['collector'],
  'api::org-interface.org-interface.decideWrite': ['collector'],
}
```

This will allow public users to generate a login challenge required for logging in with WebAuthn. It will also give collector users access to the Org Interfaces PDP.

By default Strapi gives public users access to `count`, `findOne`, and `find` for collection types, which we don't want.

**`Public`** users should have access to:

| Collection/Interface | Method                   | Endpoint                |
| -------------------- | ------------------------ | ----------------------- |
| WebAuthn             | `generateLoginChallenge` | `POST /login-challenge` |
| WebAuthn             | `verifyWebAuthnLogin`    | `POST /verify-login`    |
| WebAuthn             | `validateToken`          | `POST /validate-token`  |

Note: `/validate-token` is given public access as we do not want to attach the JWT to the typical `Authorization` header, as Strapi will attempt to validate the token itself. However, we want to do manual validation of the token, so we provide it in the body. There is one environment variable named 'ORG_ENABLE_TOKEN_VALIDATION' to disable or enable this validation.

<br>

**`collector`** users should have access to:
| Collection/Interface | Method | Endpoint |
|----------------------|--------------------------|--------------------------|
| Device | `findOne` | `GET /devices/{uuid}` |
| Device | `create` | `POST ​/devices` |
| Device | `delete` | `DELETE /devices/{uuid}` |
| Org-Interface | `decideRead` | `GET ​/pdp​/{projectId}​/{protocolId}​/read` |
| Org-Interface | `decideWrite` | `GET ​/pdp​/{projectId}​/{protocolId}​/write` |
| Org-Interface | `userProjects` | `GET ​/user-projects` |
| Org-Interface | `identifierStatus` | `GET /status/{identifier}` |
| Org-Interface | `mintIdentifier` | `POST ​/mint-identifier` |
| Org-Interface | `collectionHook` | `POST ​/collection` |
| Org-Interface | `generateChallenge` | `POST ​/challenge` |
| Org-Interface | `generateLoginChallenge` | `POST ​/login-challenge` |
| Org-Interface | `verifyWebAuthnLogin` | `POST ​/verify-login` |
| Org-Interface | `verifyWebAuthnRegister` | `POST ​/verify-register`
| Org-Interface | `validateToken` | `POST /validate-token` |
| Project | `findOne` | `GET ​/projects​/{id}` |
| Project | `find` | `GET ​/projects` |
| Project | `count` | `GET ​/projects​/count` |
| Protocols | `findOne` | `GET ​/protocols/{id}` |
| Protocols | `find` | `GET ​/protocols` |
| Protocols | `count` | `GET ​/protocols/count` |

<br>

**`Paratoo-admin`** should have access to **all** endpoints

> UNIMPLEMENTED: additional roles (e.g., a 'curator' that can setup projects) and stricter role-based access control. See [README](./README.md#roles-in-the-system)

# 8. Acceptance Tests

> Note these still need to be executed and pass/fail must be filled out

We define the acceptance tests based upon the above specification (section's 1-5, all other sections are nice-to-haves and are not required). We test user stories for typical usage and do not include edge-case failures (such as invalid data), as these are covered by the unit tests integrated into the CI/CD pipeline. The following criteria are considered for passing acceptance tests:

- does it work
- has it achieved the functionality outlined by the specification
- is it robust, i.e., can it handle most real-world situations

Acceptance tests are structured with a GIVEN-WHEN-THEN structure, as outlined in [this](https://openclassrooms.com/en/courses/4544611-write-agile-documentation-user-stories-acceptance-tests/4810081-writing-acceptance-tests) article.

## 8.1 Register

Scenario:

```
"As a data collector, I want to be able to create an account so that I can be associated
with the data I collect."
```

| ID   | Given                                                       | When                                                                          | Then                                                                                                       |
| ---- | ----------------------------------------------------------- | ----------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------- |
| 1-01 | The account for the provided credentials does not yet exist | The User submits the registration form AND the provided credentials are valid | `paratoo-org`'s `/auth/local/register` endpoint registers the given credentials and authenticates the User |
| 1-02 | The account for the provided credentials already exists     | The User submits the registration form AND the provided credentials are valid | `paratoo-org`'s `/auth/local/register` endpoint rejects the registration attempt                           |

```
Pass/fail
```

## 8.2 Login

Scenario:

```
"As a data collector, I want to be able to login with my account so that I can be
associated with the data I collection."
```

| ID   | Given                                                   | When                                                                   | Then                                                                           |
| ---- | ------------------------------------------------------- | ---------------------------------------------------------------------- | ------------------------------------------------------------------------------ |
| 2-01 | The account for the provided credentials exists         | The User submits the login form AND the provided credentials are valid | The `paratoo-org`'s `/auth/local` endpoint authenticates the given credentials |
| 2-02 | The account for the provided credentials does not exist | The User submits the login form AND the provided credentials are valid | The `paratoo-org`'s `/auth/local` endpoint rejects the login attempt           |

```
Pass/fail
```

## 8.3 User Projects

Scenario:

```
"As a data collector, I want to be able to view the projects I'm assigned so that I can easily begin a collection for that protocol.
```

| ID   | Given                                                                                         | When                                                                                                        | Then                                                                                                   |
| ---- | --------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------ |
| 3-01 | The User has at least one project assigned to them AND the User is authenticated              | The User access the `/user-projects` endpoint (whether via the Webapp or directly through the endpoint)     | The User is returned an array of all projects that are assigned to them, with the associated protocols |
| 3-02 | The User has no projects assigned to them AND the User is authenticated                       | The The User access the `/user-projects` (whether via the Webapp or directly through the endpoint) endpoint | The User is returned an empty array                                                                    |
| 3-03 | The User is not authenticated (not logged in)                                                 | The User access the `/user-projects` (whether via the Webapp or directly through the endpoint) endpoint     | The User is returned a `forbidden` message                                                             |
| 3-04 | The User has at least one project assigned to them AND the project has an associated protocol | The User accesses the `/user-projects` endpoint via the Webapp AND selects a protocol to be collected       | The User is directed to the appropriate page to begin collecting                                       |

```
Pass/fail
```
## 8.4 PDP

Scenario:

```
As a data collector, I want to be able to see the projects I am associated with and allowed to see.
```

| ID   | Given                                                                                                                        | When                                                                                          | Then                                                   |
| ---- | ---------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- | ------------------------------------------------------ |
| 4-01 | The User has at least one project assigned to them AND that project has an associated protocol AND the User is authenticated | The User accesses their projects, which will check with the PDP before fulfilling the request | The User is returned the projects they are assigned to |

```
Pass/fail
```

Scenario:

```
As a data collector, I want to be able to create collections for the projects I am associated with and am allowed to edit.
```

| ID   | Given                                                                                                                                                               | When                                                                                                                 | Then                                       |
| ---- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------- | ------------------------------------------ |
| 4-02 | The User has at least one project assigned to them AND that project has an associated protocol AND that protocol has write permission AND the User is authenticated | The User creates, updates, or deletes a collection, which will be checked with the PDP before fulfilling the request | The collection is created/updated/deleted. |

## 8.5 Org Minted Identifier & Collection Hook

Scenario:

```
As a data collector, I want to be able to keep track of my collections and their associated project and protocol so that I can easily retrieve them in the future.
```

| ID   | Given                                                                                                                                                                                   | When                          | Then                                                                                                                                              |
| ---- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| 5-01 | The User is authenticated AND the User has at least one project assigned to them AND that project has an associated protocol AND the User has made a collection that is ready to submit | The User submits a collection | An org minted identifier is created, used to create the collection entry in `paratoo-core`, and stores the collection identifier in `paratoo-org` |

```
Pass/fail
```

## 8.6 Identifier Status

Scenario:

```
As a collector, I want to be able to check if I've already submitted a collection.
```

Note: There are likely other uses for this use case, but we just stick with this one for now.

| ID   | Given                                                                                                                                                                                        | When                          | Then                                                               |
| ---- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------- | ------------------------------------------------------------------ |
| 6-01 | The User is authenticated AND the User has at least one project assigned to them AND that project has an associated protocol AND the User has made a collection that has already been stored | The User submits a collection | The User is notified that the collection identifier already exists |

```
Pass/fail
```

# Appendix

## A.1 WebAuthn registration sequence diagram

![registration sequence diagram (sunshine scenario)](diagrams/webauth-registration-sequence-sunshine-scenario.png)

## A.2 WebAuthn disable sequence diagram

![disable sequence diagram (sunshine scenario)](diagrams/webauth-disable-sequence-sunshine-scenario.png)

## A.2 Plot Selection & Plot layout diagram

High-level flow (including client interactions):

![Plot selection sequence diagram (sunshine scenario)](diagrams/proejct-area-&-plot-selection.png)

Low-level, API flow:

![Plot selection and project area sequence diagram (sunshine scenario)](diagrams/plot-selection-and-project-area-flow.png)

## A.3 Role mapping between Monitor's Strapi org and MERIT's org

Each role gets more permissions as you move down the table, inheriting the permissions of the role above.

| MERIT Role | Monitor Role | Permissions |
| ---------- | ------------ | ----------- |
| Project Field Data Collector<br>(FC_USER)<br>External | collector | <ul><li>Access MERIT project in Monitor</li><li>Monitor: Conduct & Submit Survey</li><li>Monitor: Plot Layout</li><br><li>MERIT: User authentication for access to Monitor only, no ability to view/edit reports (or anything else)</li></ul> |
| Project Editor<br>(FC_USER)<br>External | collector | <ul><li>Access MERIT project in Monitor</li><li>Monitor: Conduct & Submit Survey</li><li>Monitor: Plot Layout</li><br><li>MERIT: Edit Project Reports</li></ul> |
| Project Admin<br>(FC_USER)<br>External | project_admin | <ul><li>Access MERIT project in Monitor</li><li>Monitor: Plot Layout</li><li>Monitor: Plot Selection</li><li>Monitor: Project Area</li><li>Monitor: Conduct & Submit Survey</li><br><li>MERIT: Edit Project Reports</li><li>MERIT: Submit Project Reports</li><li>MERIT: Edit Dataset Summary</li></ul> |
| Project Grant Manager<br>(FC_OFFICER)<br>Internal | project_admin | <ul><li>Access MERIT project in Monitor</li><li>Monitor: Plot Layout</li><li>Monitor: Plot Selection</li><li>Monitor: Project Area</li><li>Monitor: Conduct & Submit Survey</li><br><li>MERIT: Edit Project Reports</li><li>MERIT: Submit Project Reports</li><li>MERIT: Edit Dataset Summary</li><br><li>MERIT: Return/Approve Project Reports</li></ul> |
| MERIT System Admin<br>(FC_ADMIN)<br>Internal – MERIT Team | system-admin (not yet implemented) | Everything |

## A.4 MERIT sites mappings

| MERIT Concept | MERIT 'type' | Monitor Concept |
| ------------- | ------------ | --------------- |
| Project Extent / Project Area | P | the project area defined in Monitor as part of the Plot Selection protocol. Returned in GET /user-projects |
| Works Areas | P | n/a - internal MERIT site |
| Survey Areas | E | 'Plots' defined in Monitor during the Plot Layout and Visit protocol. Contains the 25 spatial points for the plot |
| Reporting sites | R | n/a - internal MERIT concept for reporting |
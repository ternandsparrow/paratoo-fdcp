# Diagrams

### Sequence diagrams
These diagrams are authored as text (as one diagram per file in this directory)
and rendered using the tool at
[websequencediagram.com](https://www.websequencediagrams.com/). If that site
ever disappears, you should be able to get a diagram rendered using
[sequencediagram.com](https://sequencediagram.org/) with minimal changes to the
syntax.

### Deployment diagrams
These diagrams are authored as text using the [PlantText](https://www.planttext.com/) tool.

## Deployment diagram
Everything is deployed as a docker-compose stack on a single VM. Caddy acts as a
reverse proxy to do hostname based routing and provide HTTPS termination.

The webapp might be different to what you've encountered before. It's an
isomorphic/universal app.  This means we have a web server that will pre-render
the requested page and send it down the wire fully (or almost fully) hydrated
with data. This is excellent for SEO purposes and improving the "time to
content" for the user. Once the user has the page loaded, it acts like as normal
SPA (single page app) from there on.

![Deployment diagram](deployment-diagram.png)

# Sequence diagram
![High level sequence diagram](high-level-sequence-diagram.png)

# Purpose

In general, bulk is used to easily handle the upload of huge surveys with many observations at the same time. This includes reducing the amount of network usage complexity, and easily handling ID relations for entries that do not exist prior to this request. 

Also see [instructions](../Manually%20adding%20protocols%20with%20Bulk.md) for adding protocols that leverage bulk.


# Path definition
A bulk request is included for every `survey` collection-type.

The path is always equal to `/<survey-type-name>/bulk`

# General Schema

> see the [JSDoc](../../paratoo-core/src/api/paratoo-helper/functions/bulk.js) specified in `genericBulk()`.

A bulk request within a post body is an array called `collections`

Each entry of the `collections` array is an object with exactly three values:

## Minted Identifier

`orgMintedIdentifier` is technically arbitrary as it is stored as text. However, our Org Interface implementation encodes the user's ID, project ID, protocol ID, protocol version, time (at time of minting), and the survey ID (which is JSON containing the survey type, time, and a random number).

## Survey Type

`<survey-type-name>` should be the name of a valid Strapi model

Refers to the `survey` type that should be the parent of the attached `observations`.

Always refers to creating a new survey instance, referenced in `New<survey-type-name>`

## Observations

`<survey-type-name>-observation` should be the name of a Strapi model

Refers to an array of object child observations that should be referenced in the `survey` when the survey is created.

The schema is `New<survey-type-name>-observations`

There can be models defined as 'observations'.

## Child observations

No real constraints on the naming. The field name contained in the observation's relation to the child observation is what's used to detect child observations.

Can have multiple models, but only if there is a single 'observation' model.

## General models

Some other model(s) associated with the survey. In most cases, you can get away with specifying these sorts of models with the rest of the observations' models, but in cases where there are child observations (and thus only a single observation model), we can specify any other observation-like models as general models.

# Diagrams

We represent the structure of our models in an abstract manner - not with an ER diagram, but in a similar manner Strapi abstracts its relations. That is, we show how any given models *points* to another model (as a relation), which will have a strict ER model underlying it (due to the use of a PostgreSQL DB)

## Generic

This is the generic pattern we follow, in which all modelling conforms to.

![Generic](diagrams/Core%20Collection%20Model%20Pattern-Generic.png)

## Example - Bird Surveys

Here, `weather-observation` is a 'generic' model

![Bird Surveys](diagrams/Core%20Collection%20Model%20Pattern-Example%20-%20bird%20survey.png)

## Example - Cover PI

![Cover PI](diagrams/Core%20Collection%20Model%20Pattern-Example%20-%20Cover%20PI.png)

## Example - Vegetation Mapping

![Veg Mapping](diagrams/Core%20Collection%20Model%20Pattern-Example%20-%20Veg%20Mapping.png)

## Example - Fauna Ground Counts Transects

A similar pattern also applies to many other 3rd set protocols that follow this 'site setup', 'survey setup', 'conduct survey' pattern.

![Fauna Ground Counts Transects](diagrams/Core%20Collection%20Model%20Pattern-Example%20-%20Fauna%20Ground%20Counts%20Transects.drawio.png)

# Filtered Select With Custom Input - Guide

Sometimes we want LUTs to be more of a suggestion, or to allow free text instead of a LUT entry. The `filteredSelectWithCustomInput` is such a way, as it can allow the user to pick from the LUT entries, or specify their own input.

> NOTE: this is *not* the same as the [CSV species list dropdowns](species-list-dropdown-guide.md), which derive their options from CSV species lists baked into the client.

## Backend

In Strapi there is a component category called `Custom-Lut` which contains the set of models to enable `filteredSelectWithCustomInput`. The steps to define one are as follows:

1. Create a new component under the component category `Custom-Lut` with the name `Custom-Lut-<model-name>`, where `<model-name>` is the Collection type model name defined by Strapi. So for example, a custom LUT for observations methods would have the component name `Custom-Lut-Observation-Methods` (see image below). Note: you must select an icon, but this doesn't affect anything. Click `Continue`.
   ![step 1](screenshots/filteredSelectWithCustomInput-step1.png)
1. When presented with the 'select a field for your component', create a relation field to the respective LUT in the format `<model_name>_lut` (see image below). Click `Add Another Field`.
   ![step 2](screenshots/filteredSelectWithCustomInput-step2.png)
1. Add a `short text` field in the format `<model_name>_text`. Click `Finish`.
   ![step 2](screenshots/filteredSelectWithCustomInput-step3.png)
1. Save the component.

This component can then be added to any protocol model it's needed in. It can be as a repeatable or single component. The field name must be the base field name, e.g., `observation_methods`.

## Client

The custom LUT must now be registered in the client. In `src/misc/helpers.js`'s `modelConfigFromSchema()` there are two switch cases; one for single components that switches on `property['x-paratoo-component']` and another for repeatable components that switches on `property.items['x-paratoo-component']`.

1. Create a case with the component name listed in the model that is using the custom LUT, e.g., `custom-lut.custom-lut-fauna-bird-species-name`.
1. Set the fields to look like:

```js
case 'custom-lut.custom-lut-fauna-bird-species-name':
  fieldData.type = 'filteredSelectWithCustomInput'
  fieldData.lutFlag = false
  customLutConfig = modelConfigFromSchema(
    property,
    documentation,
    currentProtId,
    prettyFormatFieldName(key),
  )
  fieldData.select = customLutConfig.fields.fauna_bird_species_name_lut
  fieldData.input = customLutConfig.fields.fauna_bird_species_name_text
  break
```

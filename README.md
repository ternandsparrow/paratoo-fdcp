> The main repo for the Paratoo Field Data Collection Platform.  An open-source
> software system for collecting and managing field data, e.g.  an ecological
> survey collecting flora, soil and landscape data.

# Quickstart

This repository contains the code that builds the docker images. There is a
separate repository that contains the code to run a complete stack:
[paratoo-fdcp-runner](https://gitlab.com/ternandsparrow/paratoo-fdcp-runner).
The runner repo is also a submodule of this repository to make development
easier, but if you're just looking to deploy the stack, not develop it, go
direct to the runner repo and read the instructions there.

# Developers

  1. clone this repo
  1. init git submodules

      ```bash
      git submodule init   # only need to run once for a clone
      git submodule update # run this after every `git pull`
      ```

  1. view the ["Quickstart - developers" section of the paratoo-fdcp-runner
     README.md](/ternandsparrow/paratoo-fdcp-runner/-/blob/develop/README.md#quickstart-developers)
     on the `develop` branch

# Keeping `protocol` models in sync

See [this README.md](./paratoo-core/api/protocol/README.md) for details about
why both components have their own protocol model.

Use the `./helper-scripts/sync-protocol-models.sh` script to keep them in sync
if you make changes. Be sure to change the `paratoo-core` model and then run the
sync script.

# Sponsorship

The project is hosted on gitlab (under the gitlab for Open Source programme).
This project is tested with BrowserStack.

# Plan for V1
- The admin UI can only handle:
  - Projects creation
  - Linking projects, protocols and users together
- as much as possible is pushed into Keycloak
- protocols are defined by programmers either as JSON schema or directly as SQL
    schema. Each protocol will have its own single uber document.
- MVP use case:
  1. login as admin user on desktop device
    1. create project
    1. create protocols
    1. link together, and add user(s)
  1. login as regular user on mobile device
    1. select project
    1. collect data

import os
import json
import sys

# check if any component relation is longer than 63 characters

def check_component_relation_length(folder, is_component=True):
    for root, dirs, files in os.walk(folder):
        for file in files:
            check = file.endswith('.json') if is_component else file == 'schema.json'
            if check:
                file_path = os.path.join(root, file)
                with open(file_path, 'r') as f:
                    try:
                        json_data = json.load(f)
                        # Iterate through all keys and values in the JSON data
                        for value in json_data['attributes'].values():
                            path = None
                            if 'component' in value :
                                path = value['component']
                                if path is not None and len(path) > 63:
                                        raise Exception(f'Error: Path {path} in {file_path} is longer than 63 characters')
                    except json.JSONDecodeError as e:
                        raise Exception(f'Error decoding JSON from {file_path}: {e}')

components_folder= './paratoo-core/src/components'
api_folder= './paratoo-core/src/api'

check_component_relation_length(components_folder)
check_component_relation_length(api_folder, False)



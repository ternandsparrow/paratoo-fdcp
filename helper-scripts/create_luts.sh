#!/bin/sh

#yarn strapi generate:api "LUT IBRA" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Datum" description_geoscience_australia:string inverse_flattening:string equatorial_radius_in_m:integer reference_spheroid:string year_of_acceptance:string datum:string description:text
yarn strapi generate:api "LUT Disturbance" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Landform Element" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Landform Pattern" symbol:string label:string description:text typical_elements:text common_elements:string occasional_elements:string compare_with:string uri:string
yarn strapi generate:api "LUT Location Method" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Microrelief" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Observer" full_name:string affiliation:string uri:string
yarn strapi generate:api "LUT State" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT MGA Zone" zone:string datum:string projection:string eastern_boundary:string western_boundary:string
yarn strapi generate:api "LUT Site Slope" symbol:string label:string description:text uri:string tangent_boundary:integer

yarn strapi generate:api "LUT Veg Basal Point" symbol:string label:string distance_from_sw_corner:integer description:text
yarn strapi generate:api "LUT Veg Growth Form" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Veg Growth Stage" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Veg Structural Formation Class" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Veg Cover Characterisation" symbol:string label:string description:text uri:string

yarn strapi generate:api "LUT Weather Temperature" symbol:string label:string description:text
yarn strapi generate:api "LUT Weather Precipitation" symbol:string label:string description:text
yarn strapi generate:api "LUT Weather Precipitation Duration" symbol:string label:string description:text
yarn strapi generate:api "LUT Weather Wind" symbol:string label:string description:text beaufort_scale:string
yarn strapi generate:api "LUT Weather Cloud Cover" symbol:string label:string description:text

#yarn strapi generate:api "LUT IBRA" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Fauna Bird Activity Type" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Fauna Bird Breeding Type" symbol:string label:string description:text uri:string
#yarn strapi generate:api "LUT Fauna Bird Observation Location Type" symbol:string label:string description:text uri:string
#yarn strapi generate:api "LUT Fauna Bird Observation Type" symbol:string label:string description:text uri:string
#yarn strapi generate:api "LUT Fauna Bird Survey Type" symbol:string label:string description:text uri:string

#yarn strapi generate:api "LUT Fauna Birds Population"
yarn strapi generate:api "LUT Fauna Birds Species" TaxonSortV3:integer TaxonLevel:string SpNo:integer TaxonID:string TaxonName:string TaxonScientificName:string FamilyCommonName:string FamilyScientificName:string order:string TaxonomicNote:text population:string IUCNStatus:string
#yarn strapi generate:api "LUT Veg NSL Names" scientificName:string TaxonScientificName:string FamilyCommonName:string FamilyScientificName:string order:string TaxonomicNote:text population:string IUCNStatus:string
yarn strapi generate:api "LUT Veg NSL Canonical Names" canonicalName:string

yarn strapi generate:api "LUT Veg Structural Formation" symbol:string label:string description:text uri:string

yarn strapi generate:api "LUT Fauna Gender" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Fauna Maturity" symbol:string label:string description:text uri:string

yarn strapi generate:api "LUT Fire History" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Homogeneity Measure" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Climatic Condition" symbol:string label:string description:text uri:string


yarn strapi generate:api "LUT Soils Coarse Frag Abundance" symbol:string type:string description:text
yarn strapi generate:api "LUT Soils Coarse Frag Shape" symbol:string type:string description:text
yarn strapi generate:api "LUT Soils Coarse Frag Size" symbol:string type:string description:text
yarn strapi generate:api "LUT Soils Drainage" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Effervescence" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Erosion Abundance" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Erosion State" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Erosion Type" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Lithology" symbol:string label:string rock_type:string description:text uri:string
yarn strapi generate:api "LUT Soils Mottle Abundance" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Mottle Colour" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Mottle Size" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Observation Type" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Pedality Fabric" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Pedality Grade" symbol:string pedality:string grade:string description:text uri:string
yarn strapi generate:api "LUT Soils Pedality Type" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Segregations Abundance" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Segregations Form" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Segregations Nature" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Segregations Size" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Structure Size" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Substrate" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Surface Soil Condition" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Surface Strew Size" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Texture Grade" symbol:string label:string description:text bolus_description:text clay_content:string uri:string
yarn strapi generate:api "LUT Soils Texture Modality" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Texture Quality" symbol:string label:string description:text uri:string

yarn strapi generate:api "LUT Plot Dimensions" symbol:string label:string description:text x_in_m:integer y_in_m:integer uri:string

yarn strapi generate:api "Floristics Full Veg Voucher" 
yarn strapi generate:api "Floristics Lite Veg Voucher" 
yarn strapi generate:api "Floristics Genetic Voucher" 

yarn strapi generate:api "LUT Cover Transect Start Point" symbol:string label:string description:text uri:string
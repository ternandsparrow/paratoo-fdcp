import requests


class Query:
    def allValuesUsingSparqlQuery(self, collectionUri):
        namedGraph = "https://linked.data.gov.au/def/nrm"
        url = "https://graphdb.tern.org.au/repositories/dawe_vocabs_core"

        sparqlQuery = f"""\n  PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n  PREFIX tern: <https://w3id.org/tern/ontologies/tern/>\n  select ?concept ?featureType (sample(?__label) as ?label) (sample(?_featureTypeLabel) as ?featureTypeLabel) ?valueType (sample(?_valueTypeLabel) as ?valueTypeLabel) ?categoricalCollection (sample(?_categoricalCollectionLabel) as ?categoricalCollectionLabel)\n  from <http://www.ontotext.com/explicit>\n  from <{namedGraph}>\n  where {{ \n      # Collection of observable properties.\n      <{collectionUri}> skos:member ?concept .\n      \n      ?concept skos:prefLabel ?_label .\n      bind(str(?_label) as ?__label)\n  \n      optional {{ \n          ?concept tern:hasFeatureType ?featureType .\n          service <https://graphdb.tern.org.au/repositories/tern_vocabs_core> {{\n              ?featureType skos:prefLabel ?_featureTypeLabel .\n          }}\n      }}\n  \n      optional {{\n        ?concept tern:valueType ?valueType .\n        service <https://graphdb.tern.org.au/repositories/knowledge_graph_core?context=%3Chttps%3A%2F%2Fw3id.org%2Ftern%2Fontologies%2Ftern%2F%3E&infer=false> {{\n          ?valueType skos:prefLabel ?_valueTypeLabel .\n        }}\n      }}\n  \n      optional {{\n        ?concept tern:hasCategoricalCollection ?categoricalCollection .\n        optional {{\n            {{\n                service <https://graphdb.tern.org.au/repositories/tern_vocabs_core> {{\n                    ?categoricalCollection skos:prefLabel ?_categoricalCollectionLabel .\n                }}\n            }}\n            union {{\n                service <https://graphdb.tern.org.au/repositories/ausplots_vocabs_core> {{\n                    ?categoricalCollection skos:prefLabel ?_categoricalCollectionLabel .\n                }}\n            }}\n            union {{\n                ?categoricalCollection skos:prefLabel ?_categoricalCollectionLabel .\n            }}\n        }}\n    }}\n  }} \n  group by ?concept ?featureType ?valueType ?categoricalCollection\n  order by lcase(?label)\n  
        """
        headers = {
            "content-type": "application/x-www-form-urlencoded",
            "accept": "application/sparql-results+json",
        }
        res = None
        try:
            res = requests.post(url, params={"query": sparqlQuery}, headers=headers)
            results = res.json()["results"]["bindings"]
        except Exception as e:
            print(e)
            return None
        return results

    def sparql_query_all(self, collection_uri: str):
        url = "https://graphdb.tern.org.au/repositories/dawe_vocabs_core"
        sparql_query = f"""\nSELECT ?title ?value\n
            {{ \n
            <{collection_uri}> ?title ?value\n
            }}"""

        headers = {
            "content-type": "application/x-www-form-urlencoded",
            "accept": "application/sparql-results+json",
        }

        try:
            r = requests.post(
                url,
                params={"query": sparql_query},
                headers=headers,
                timeout=10,
            )
        except Exception as e:
            raise
        res = r.json()["results"]["bindings"]
        return res
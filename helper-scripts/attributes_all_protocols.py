import dummyValuesBasedOnSchema as dv
from dawe_vocabs import DaweVocabs, LUTSchema
from SparqlQuery import Query
import os
import json
import requests
import re
from typing import Optional, List, Union
import csv
import uuid
import copy
import base64

CORE_SOURCE = "paratoo-core/src"
ORG_SOURCE = "paratoo-org/src"

RESOURCE_PATH = "https://raw.githubusercontent.com/ternaustralia/dawe-rlp-vocabs/main/src/dawe_nrm/api/categorical_values/endpoints.py"

allLutsInfo = {}

# vocab_doc = "https://staging.core-api.monitor.tern.org.au/api/documentation/swagger.json"
vocab_doc = "http://localhost:1337/api/documentation/swagger.json"

third_sets = [
    "90c0f4cc-a22a-4820-9a8b-a01564bc197a",
    "648d545a-cdae-4c19-bc65-0c9f93d9c0eb",
    "2cd7b489-b582-41f6-9dcc-264f6ea7801a",
    "cc826a19-a1e7-4dfe-8d6e-f135d258d7f9",
    "0c5d1d14-c71b-467f-aced-abe1c83c15d3",
    "685b5e9b-20c2-4688-9b04-b6caaf084aad",
    "a76dac21-94f4-4851-af91-31f6dd00750f",
    "d706fd34-2f05-4559-b738-a65615a3d756",
    "80360ceb-bd6d-4ed4-b2ea-9bd45d101d0e",
    "06cd903e-b8b3-40a5-add4-f779739cce35",
    "49d02f5d-b148-4b5b-ad6a-90e48c81b294",
    "228e5e1e-aa9f-47a3-930b-c1468757f81d",
]


class AttributesAllProtocol:
    def __init__(self, source_path: str, out_file: str):
        self.source_path = source_path
        self.out_file = out_file
        
        self.auto_values = dv.DummyValuesBasedOnSchema(self.source_path, dv.INIT_DATA)
        self.queryVocabs = Query()

    def replaceRefs(self, properties, full_doc):
        fields = list(properties.keys())
        for field in fields:
            keys = list(properties[field].keys())
            if "$ref" in keys:
                m = properties[field]["$ref"].split("/")[-1]
                properties[field] = full_doc["components"]["schemas"][m]
                continue

            if properties[field]["type"] == "array":
                keys = list(properties[field]["items"].keys())
                if "$ref" in keys:
                    m = properties[field]["items"]["$ref"].split("/")[-1]
                    properties[field]["items"] = full_doc["components"]["schemas"][m]

        return properties

    def csvDumpFields(
        self, attributes, protocol, plotBased, model, writer, prefix=None
    ):
        attrs = list(attributes.keys())
        for attr in attrs:
            fieldType = attributes[attr]["type"]
            relation = ""
            component = ""
            if "target" in attributes[attr]:
                relation = attributes[attr]["target"].split(".")[-1]
            if "component" in attributes[attr]:
                component = attributes[attr]["component"]

            if relation and "lut" not in relation:
                fieldType = "integer"
            if "lut" in relation:
                fieldType = "enum"
            if component:
                fieldType = "json"
            if prefix:
                attr = f"{prefix}/{attr}"
            writer.writerow(
                {
                    "protocol": protocol,
                    "plotBased": plotBased,
                    "model": model,
                    "field": attr,
                    "fieldType": fieldType,
                    "relation": relation,
                    "component": component,
                }
            )
            if component:
                content = self.auto_values.get_schema_content(
                    content_type=dv.COMPONENT, component=component
                )
                self.csvDumpFields(
                    attributes=content["attributes"],
                    protocol=protocol,
                    plotBased=plotBased,
                    model=model,
                    writer=writer,
                    prefix=attr,
                )

    def generateRelations(self):
        doc = requests.get(vocab_doc)
        full_doc = doc.json()
        requestsBodies = full_doc["paths"]
        content = self.auto_values.get_schema_content(
            content_type=dv.API, model_name="protocol"
        )
        attributes = list(content["attributes"].keys())
        uuidIndex = attributes.index("identifier")
        workflowIndex = attributes.index("workflow")
        endpointIndex = attributes.index("endpointPrefix")
        nameIndex = attributes.index("name")
        with open(self.out_file, "w", newline="") as csvfile:
            fieldnames = [
                "protocol",
                "plotBased",
                "model",
                "field",
                "fieldType",
                "relation",
                "component",
            ]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            skips = [
                "3cbc5277-45fb-4e7a-8f33-19d9bff4cd78", # Drone Survey
                "3d2eaa76-a610-4575-ac30-abf40e57b68a", # Dev sandbox
                "0628e486-9b33-4d86-98db-c6d3f10f7744", # Vertebrate Fauna (not completed)
                "aa64fd4d-2c5a-4f84-a197-9f3ce6409152", # Invertebrate Fauna - Post-field Sampling Curation (not completed)
                "7405bd3c-5811-4d9b-aacc-9d279a613b16", # Grassy Weeds Aerial Survey 1
                "aae4dbd8-845a-406e-b682-ef01c3497711", # Dev Sandbox Bulk Survey
                "75600f67-49f2-4c9a-98af-9cde2d020680", # Grassy Weeds Aerial Survey 2
                "9a0aab22-19b8-49e9-b0fa-262b4cf58e8e", # Grassy Weeds Aerial Survey 3
            ]
            for initialData in content["initialData"]:
                if initialData[uuidIndex] in skips:
                    continue

                # print(initialData[nameIndex])
                models = []
                relations = []
                for workflow in initialData[workflowIndex]:
                    models.append(workflow["modelName"])
                    if "relationOnAttributesModelNames" in workflow:
                        relations = (
                            relations + workflow["relationOnAttributesModelNames"]
                        )
                    if initialData[uuidIndex] == "a9cb9e38-690f-41c9-8151-06108caf539d": # plot selection survey
                        models.append("plot-selection-survey")

                endpointPostfix = "bulk"
                # if initialData[uuidIndex] == "a9cb9e38-690f-41c9-8151-06108caf539d":
                #     endpointPostfix = "many"

                body = requestsBodies[
                    f"{initialData[endpointIndex]}/{endpointPostfix}"
                ]["post"]["requestBody"]

                properties = dict(body["content"]["application/json"]["schema"][
                    "properties"
                ]["data"]["properties"])
                if endpointPostfix == "bulk":
                    properties = properties["collections"]["items"]["properties"]

                if endpointPostfix == "many":
                    properties = {
                        "plot-selection-survey": {
                            "$ref": "#/components/schemas/PlotSelectionSurveyRequest"
                        },
                        "plot-selections": {
                            "type": "array",
                            "items": {
                                "type": "object",
                                "$ref": "#/components/schemas/PlotSelectionRequest",
                            },
                            "minItems": 1,
                        }
                    }

                properties = dict(self.replaceRefs(dict(properties), full_doc))
                plotBased = False
                if "plot-layout" in models or "plot-visit" in models:
                    plotBased = True
                for m in models:
                    model_content = self.auto_values.get_schema_content(
                        content_type=dv.API, model_name=m
                    )
                    # print(m)
                    self.csvDumpFields(
                        attributes=model_content["attributes"],
                        protocol=initialData[nameIndex],
                        plotBased=plotBased,
                        model=m,
                        writer=writer,
                    )

                for m in relations:
                    model_content = self.auto_values.get_schema_content(
                        content_type=dv.API, model_name=m
                    )
                    self.csvDumpFields(
                        attributes=model_content["attributes"],
                        protocol=initialData[nameIndex],
                        plotBased=plotBased,
                        model=m,
                        writer=writer,
                    )

    
    def findProtocols(self, lut, relations):
        protocols = None
        for r in relations:
            if not r["relation"]: continue
            if "lut" not in r["relation"]: continue
            if lut != r["relation"]: continue
            if not protocols: 
                protocols = r["protocol"]
                continue
            protocols += ", " + r["protocol"]
        # print (protocols)
        return protocols
    def lutSymbolCheck(self):
        with open('attributes_all_protocol.csv') as f:
            reader = csv.DictReader(f)
            rows = list(reader)
        with open('temp.json', 'w') as f:
            json.dump(rows, f)
            
        relations = json.load(open('temp.json'))
        
        os.remove('temp.json')
        
        models = self.auto_values.get_all_models(including_luts=True)
        symbolsNeedToUpdate = []
        total = 0
        for model in models:
            if "lut" not in model:
                continue
            # if model != "lut-camera-resolution": continue
            content = self.auto_values.get_schema_content(
                content_type=dv.API, model_name=model
            )

            if "attributes" not in content:
                continue
            if "initialData" not in content:
                continue
            initialData = content["initialData"]
            if not initialData:
                continue
            attributes = list(content["attributes"].keys())
            symbolIndex = attributes.index("symbol")
            labelIndex = attributes.index("label")
            modelHasIncorrectSymbol = False
            for data in initialData:
                if data[symbolIndex].lower() != data[labelIndex].lower(): continue
                object = {}
                object["lut"] = model
                object["symbol"] = data[symbolIndex]
                object["label"] = data[labelIndex]
                object["protocols"] = self.findProtocols(lut=model, relations=relations)
                symbolsNeedToUpdate.append(dict(object))
                modelHasIncorrectSymbol = True
            if modelHasIncorrectSymbol:
                total += 1
                print(model)
        
        import pandas as pd
        df = pd.json_normalize(symbolsNeedToUpdate)
        df.to_csv('symbolsNeedToUpdate.csv', index=False)
    
    # def routeCheck(self):
                

def main():
    # attributesAllProtocol = AttributesAllProtocol(
    #     source_path=CORE_SOURCE, out_file="attributes_all_protocol.csv"
    # )
    # # attributesAllProtocol.generateRelations()
    # # attributesAllProtocol.lutSymbolCheck()
    # attributesAllProtocol.routeCheck()
    data = json.load(open('datasetSummary.json'))
    success = 0
    collected = 0 
    for k,v in data.items():
        if v["collected_by_monitor_app"]:
            collected += v["collected_by_monitor_app"]
        if v["total_synced_collections"]:
            success += v["total_synced_collections"]
    print(collected)
    print(success)

if __name__ == "__main__":
    main()

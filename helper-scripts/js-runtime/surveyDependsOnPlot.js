const {
  parseSystemUrl,
  getDataForEndpoint,
  isPlotBasedProtocol,
  loadSchema,
} = require('./helpers.js')


module.exports = {
  async run(system, JWT, plotDataToSearch, type) {
    let baseUrl = parseSystemUrl(system)

    let msg = `Requests using base URL '${baseUrl}' with JWT '${JWT}'. Search for plot (type '${type}') '${plotDataToSearch}'`
    console.log(msg)

    const protocolDefs = await getDataForEndpoint({
      endpointPrefix: 'protocols',
      baseUrl,
      JWT,
    })


    const dataDependsOnPlot = []
    for (const prot of protocolDefs.data) {
      const protIsPlotBased = isPlotBasedProtocol({
        protocol: prot.attributes,
      })
      if (!protIsPlotBased) continue

      const visitAttribute = visitAttributeForRelation({
        protocol: prot,
      })

      if (!visitAttribute) {
        console.warn(`Unable to determine the visit field name for relation for protocol ${prot.attributes.name}`)
        continue
      }

      const surveyData = await getDataForEndpoint({
        endpointPrefix: prot.attributes.endpointPrefix.replace('/', ''),
        baseUrl,
        JWT,
      })

      for (const survey of surveyData.data) {
        let surveyMatch = false
        try {
          const currSurveyPlotSelection = survey.attributes.plot_visit.data.attributes.plot_layout.data.attributes.plot_selection
          const currSurveyVisit = survey.attributes.plot_visit

          switch (type) {
            case 'label': {
              // console.log('currSurveyPlotSelection:', JSON.stringify(currSurveyPlotSelection, null, 2))
              if (currSurveyPlotSelection.data.attributes.plot_label === plotDataToSearch) {
                surveyMatch = true
              }
              break
            }
            case 'visit_field_name': {
              if (currSurveyVisit.data.attributes.visit_field_name === plotDataToSearch) {
                surveyMatch = true
              }
              break
            }
            case 'visit_id': {
              if (currSurveyVisit.data.id == plotDataToSearch) {
                surveyMatch = true
              }
              break
            }
            default:
              console.warn(`Unregistered case '${type}'`)
              continue
          }

          if (surveyMatch) {
            dataDependsOnPlot.push({
              protocolName: prot?.attributes.name,
              protocolUuid: prot?.attributes.identifier,
              orgMintedUuid: survey.attributes.survey_metadata.orgMintedUUID,
              plotSelectionId: currSurveyPlotSelection.data.id,
              plotLayoutId: currSurveyVisit.data.attributes.plot_layout.data.id,
              plotVisitId: currSurveyVisit.data.id,
              surveyId: survey.id,
              // rawData: survey,
            })
          }
          
        } catch (err) {
          console.warn(`Unable to parse protocol ${prot.attributes.name} survey with ID=${survey.id}`, err)
          continue
        }
      }

    }

    console.log('dataDependsOnPlot:', JSON.stringify(dataDependsOnPlot, null, 2))
  },
}

function visitAttributeForRelation({ protocol }) {
  const surveySchema = loadSchema({
    path: protocol.attributes.endpointPrefix,
  })

  for (const [attributeName, attributeVal] of Object.entries(surveySchema.attributes)) {
    if (
      attributeVal.type === 'relation' &&
      attributeVal.target === 'api::plot-visit.plot-visit'
    ) {
      return attributeName
    }
  }

  return null
}
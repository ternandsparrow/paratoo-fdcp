const {
  parseSystemUrl,
  getDataForEndpoint,
} = require('./helpers.js')
const {
  cloneDeep,
  isEmpty,
} = require('lodash')
const csvtojsonV2 = require('csvtojson')

//the threshold of free space (in MB) that we will warn - any user with less than this
//amount will be flagged as nearly out of space
const FREE_SPACE_WARNING = 0.5

module.exports = {
  async run(system, JWT, ignoreListFile = null) {
    let baseUrl = parseSystemUrl(system)

    let msg = `Requests using base URL '${baseUrl}' with JWT '${JWT}'`
    if (ignoreListFile) {
      msg += `. Will use file at ${ignoreListFile} to check for users to ignore`
    }
    console.log(msg)

    let ignoreList = []
    if (ignoreListFile) {
      try {
        const ignoreListRaw = await csvtojsonV2().fromFile(ignoreListFile)
        if (!ignoreListRaw.every(o => o?.userId)) {
          throw new Error(`Invalid ignore list - must contain at least one column 'userId'`)
        }
        for (const ignoreCase of ignoreListRaw) {
          console.log('ignoreCase:',ignoreCase)
          ignoreList.push(ignoreCase.userId)
        }
      } catch (err) {
        console.error(`Failed to load ignore list file ${ignoreListFile}`, err)
      }
    }

    const analytics = await getDataForEndpoint({
      endpointPrefix: 'analytics',
      baseUrl,
      JWT,
    })
    
    //find the most recent storage breakdown for each user
    //key will be user ID, value will be metadata (storage breakdown, project(s), etc)
    const userStorageBreakdown = {}
    const usersWithoutStorageBreakdown = new Set()
    for (const analytic of cloneDeep(analytics?.data).reverse() || []) {
      const currAnalyticUser = analytic?.attributes?.info?.authStore?.org_opaque_user_id
      if (ignoreList?.includes(currAnalyticUser)) continue
      if (currAnalyticUser) {
        if (Object.keys(userStorageBreakdown).includes(currAnalyticUser)) {
          //we're iterating over array in reverse, so we've already got the latest
          //analytics from this user
          continue
        } else if (analytic?.attributes?.info?.webApp?.storage_mb) {
          Object.assign(userStorageBreakdown, {
            [currAnalyticUser]: {
              storage_mb: analytic.attributes.info.webApp.storage_mb,
              ping_time: analytic.attributes.info.currentTime.toString(),
              app_version: analytic.attributes.info.webApp.version_app,
            },
          })
          //we've found a storage breakdown for the user, so don't need to track any more
          usersWithoutStorageBreakdown.delete(currAnalyticUser)
        } else {
          usersWithoutStorageBreakdown.add(currAnalyticUser)
        }
      } else {
        console.warn(`No user ID for analytic with ID=${analytic.id}`)
      }
    }

    //will also include those that are nearly full - both overall and localStorage
    const usersWithFullStorage = {}
    for (const [user, storageBreakdown] of Object.entries(userStorageBreakdown)) {
      const assignObForUser = {}
      if (storageBreakdown.storage_mb.free_space.localStorage < FREE_SPACE_WARNING) {
        Object.assign(assignObForUser, {
          localStorageFree: storageBreakdown.storage_mb.free_space.localStorage,
          localStorageBreakdown: storageBreakdown.storage_mb.localStorage,
        })
      }

      if (storageBreakdown.storage_mb.free_space.overall < FREE_SPACE_WARNING) {
        Object.assign(assignObForUser, {
          overall: storageBreakdown.storage_mb.free_space.overall,
        })
      }

      if (!isEmpty(assignObForUser)) {
        Object.assign(usersWithFullStorage, {
          [user]: assignObForUser,
        })
      }
    }

    console.log(`Storage breakdown by user (${Object.keys(userStorageBreakdown).length} total):`, JSON.stringify(userStorageBreakdown, null, 2))
    if (!isEmpty(usersWithoutStorageBreakdown)) {
      console.warn('Some users don\'t have storage breakdowns:', ...usersWithoutStorageBreakdown.keys())
    }
    if (!isEmpty(usersWithFullStorage)) {
      console.warn('Some users are nearly out of space: ', JSON.stringify(usersWithFullStorage, null, 2))
    }

  },
}
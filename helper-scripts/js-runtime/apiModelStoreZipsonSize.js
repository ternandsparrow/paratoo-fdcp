const zipson = require('zipson')
const sizeof = require('object-sizeof')
const fs = require('fs')

module.exports = {
  run() {
    //PUT YOUR COMPRESSED DATA HERE (as string)
    const compressed = null

    if (compressed === null) {
      throw new Error('No compressed data provided')
    }

    const raw = zipson.parse(compressed)
    fs.writeFileSync('rawStore.json', JSON.stringify(raw))

    const totalSizeB = sizeof(raw)
    for (const [model, data] of Object.entries(raw.models)) {
      const sizeInB = sizeof(data)
      console.log(`${sizeInB > 20000 ? '!!!!!!!!' : ''} models size of ${model}: ${sizeInB}kb`)
    }

    for (const [model, data] of Object.entries(raw.modelsIdMap)) {
      const sizeInB = sizeof(data)
      console.log(`${sizeInB > 20000 ? '!!!!!!!!' : ''} models size of ${model}: ${sizeInB}kb`)
    }
    console.log(`Total uncompressed size: ${totalSizeB / 1000}KB`)
    console.log(`Total compressed size: ${sizeof(compressed) / 1000}KB`)

    const keysToRemove = [
      'uri',
      'createdAt',
      'updatedAt',
    ]
    removeKeys(raw, keysToRemove)
    console.log(`Total uncompressed size (after stripping out keys ${keysToRemove.join(', ')}): ${sizeof(raw) / 1000}KB`)
    console.log(`Total compressed size (after stripping out keys ${keysToRemove.join(', ')}): ${sizeof(zipson.stringify(raw)) / 1000}KB`)
    },
}

//https://gist.github.com/aurbano/383e691368780e7f5c98
function removeKeys(obj, keys) {
  var index
  for (var prop in obj) {
    // important check that this is objects own property
    // not from prototype prop inherited
    if (obj.hasOwnProperty(prop)) {
      switch (typeof obj[prop]) {
        case 'string':
          index = keys.indexOf(prop)
          if (index > -1) {
            delete obj[prop]
          }
          break
        case 'object':
          index = keys.indexOf(prop)
          if (index > -1) {
            delete obj[prop]
          } else {
            removeKeys(obj[prop], keys)
          }
          break
      }
    }
  }
}
module.exports = {
  run(identifier) {
    console.log('Decoding identifier: ', identifier)
    console.log('Decoded: ', JSON.parse(Buffer.from(identifier, 'base64')))
  }
}
const pluralize = require('pluralize')
const fs = require('fs')
const fetch = require('node-fetch')
const {
  isEmpty,
  isEqual,
} = require('lodash')

//TODO move these to constants file
const CORE_PATH = '../../paratoo-core/src/api'
const CORE_SRC = '../../paratoo-core/src'
const alreadyPluralModels = [
  'interventions',
  'lut-plot-dimensions',
  'lut-plot-points',
  'lut-soils-evaluation-means',
  'lut-soils-horizon-details',
  'lut-soils-relative-inclination-of-slope-elements',
  'lut-soils-segregation-magnetic-attributes',
  'lut-soils-void-cracks',
  'lut-tier-3-observation-methods',
  'soil-land-surface-phenomena',
]

module.exports = {
  parseSystemUrl(system) {
    let baseUrl = null
    switch(system) {
      case 'localdev':
        baseUrl = 'http://localhost:1337/api'
        break
      case 'monitor-prod':
        baseUrl = 'https://core-api.monitor.tern.org.au/api'
        break
      case 'monitor-beta':
        baseUrl = 'https://staging.core-api.monitor.tern.org.au/api'
        break
      case 'monitor-dev':
        baseUrl = 'https://dev.core-api.monitor.tern.org.au/api'
        break
      case 'monitor-test':
        baseUrl = 'https://test.core-api.monitor.tern.org.au/api'
        break
      case 'paratoo-prod':
        baseUrl = 'https://core-api.paratoo.tern.org.au/api'
        break
      case 'paratoo-beta':
        baseUrl = 'https://staging.core-api.paratoo.tern.org.au/api'
        break
      case 'paratoo-dev':
        baseUrl = 'https://dev.core-api.paratoo.tern.org.au/api'
        break
      default:
        console.log(`Custom system endpoint provided: ${system}`)
        baseUrl = system
    }
    return baseUrl
  },
  getInitData(path) {
    const schema = loadSchema({ path })
    return schema?.initialData || []
  },
  loadSchema: loadSchema,
  loadComponentSchema({ componentName }) {
    const [folder, component] = componentName.split('.')
    const filePath = `${CORE_SRC}/components/${folder}/${component}.json`
    const schema = JSON.parse(fs.readFileSync(filePath))
    return schema
  },
  async getDataForEndpoint({ endpointPrefix, baseUrl, JWT, additionalQueryParams = null, populate = 'deep' }) {
    let url = `${baseUrl}/${endpointPrefix}?populate=${populate}`
    if (Array.isArray(additionalQueryParams)) {
      for (const param of additionalQueryParams) {
        url += `&${param}`
      }
    }
    url += '&use-default=true'
    console.log(`Querying data at URL: ${url}`)
    const dataApiResp = await fetch(
      url,
      {
        headers: {
          'Authorization': `Bearer ${JWT}`,
        },
      },
    )

    // const data = await dataApiResp.json()
    let data = null
    try {
      data = await dataApiResp.json()
    } catch (err) {
      console.warn(`Request failed for URL: ${url}. Got error:`, err)
      return {
        data: [],
      }
    }

    const metadata = findValForKey(data, 'meta')
    if (metadata?.pagination?.pageCount > 1) {
      console.log(`Got paginated response to ${url}. Number of pages: ${metadata.pagination.pageCount}. Expect to have ${metadata.pagination.total} entries in the final response`,)
      //start `currPage` at 1, as we've already fetched page 0
      for (let currPage = 1; currPage <= metadata.pagination.pageCount; currPage++) {
        console.log(`Processing page number/index ${currPage}`)
        let paginationUrl = String(url)
        //the `start` is the record number/index, so need to multiple by the `pageSize`,
        //which is the number of records per page
        let paginationParam = `pagination[start]=${currPage * metadata.pagination.pageSize}`
        if (url.includes('?')) {
          paginationUrl += `&${paginationParam}`
        } else {
          paginationUrl += `?${paginationParam}`
        }
        console.log(`Pagination request URL: ${paginationUrl}`,)
        const paginationResp = await fetch(
          paginationUrl,
          {
            headers: {
              'Authorization': `Bearer ${JWT}`,
            },
          },
        )
        const paginatedData = await paginationResp.json()
        data.data.push(...paginatedData.data)
      }
      
      if (data.data.length != metadata?.pagination?.total) {
        console.warn(`Total data length ${data.data.length} does not match number of records in API response ${metadata?.pagination?.total}`)
      }
    }

    return data
  },
  //based off the helper from core
  isPlotBasedProtocol({ protocol }) {

    let hasLayout = false
    let hasVisit = false
    for (const workflowItem of protocol.workflow) {
      if (workflowItem.modelName === 'plot-layout') {
        hasLayout = true
      }
      if (workflowItem.modelName === 'plot-visit') {
        hasVisit = true
      }
    }

    //this will also include when the protocol is Plot Layout and Visit
    //(plot-definition-survey model)
    return hasLayout && hasVisit
  },
  findValForKey: findValForKey,
  checkSurveyIsInitData({ initData, survey }) {
    let surveyIsInitData = false
    for (const data of initData) {
      //we don't know the structure of the init data, but we do know the structure of
      //the survey metadata, so search for that subset of the object
      const initDataSurveyDetails = findValForKey(
        data,
        'survey_details',
      )
      if (
        initDataSurveyDetails?.uuid === survey?.attributes?.survey_metadata?.survey_details?.uuid
      ) {
        surveyIsInitData = true
        console.log(`Init data found for survey with UUID ${survey?.attributes?.survey_metadata?.survey_details?.uuid}.`)
      }
    }
  
    return surveyIsInitData
  },
  async reverseLookup({ baseUrl, JWT, orgMintedUUID }) {
    let lookupData = null
    try {
      const resp = await fetch(
        `${baseUrl}/protocols/reverse-lookup`,
        {
          method: 'POST',
          body: JSON.stringify({
            org_minted_uuid: orgMintedUUID,
          }),
          headers: {
            'Authorization': `Bearer ${JWT}`,
            'Content-Type': 'application/json',
          },
        },
      )

      if (resp.status !== 200) {
        console.error(`Failed to lookup orgMintedUUID=${orgMintedUUID}`)
      } else {
        lookupData = await resp.json()
      }
    } catch (err) {
      console.log(err)
    }
    return lookupData
  },
  protocolHasMultipleVariants({ protocolDefsData, currProtocol }) {
    //we sometimes have multiple protocols sharing the same endpoint, as they're just
    //different variants of the protocol for the module (e.g., PTV, Cover)
    let endpointHasMultipleProtocolVariants = false
    const foundOtherVariant = protocolDefsData?.data.find(
      p =>
        //not this protocol
        p.attributes.identifier !== currProtocol.attributes.identifier &&
        //share the same endpoint
        p.attributes.endpointPrefix === currProtocol.attributes.endpointPrefix
    )
    if (!isEmpty(foundOtherVariant)) {
      endpointHasMultipleProtocolVariants = true
    }
    return endpointHasMultipleProtocolVariants
  },
  /**
   * Checks a given survey's `survey_metadata` for correct `protocol_id` and
   * `submodule_protocol_id`
   * 
   * @param {Boolean} isPlotBased whether the protocol is plot based
   * @param {Object} protocolDefsData the protocol definitions from GET to /protocols
   * @param {Object} protocol the current protocol definition
   * @param {Object} survey the current survey to check
   * @param {Boolean} isCoverAndFire whether the protocol is Cover+Fire
   * @param {Boolean} isFireProtocol whether the protocol is Fire Survey
   * @param {Object} fireData the fire survey data from GET to /fire-surveys
   * @param {Object} coverData the cover survey data from GET to
   * /cover-point-intercept-surveys
   * 
   * @returns {String | null | Object} 'continue' or `null` to skip check or to indicate it's not a candidate;
   * or an Object containing metadata about a candidate for possible fixing
   */
  checkSurveyProtocolId({
    isPlotBased,
    protocolDefsData,
    protocol,
    survey,
    isCoverAndFire,
    isFireProtocol,
    fireData,
    coverData,
  }) {
    //either not cover, or is cover and isn't part of Cover+Fire
    let coverDataIsFromFireSurvey = false
    if (isPlotBased) {
      const relatedFireSurvey = fireData?.data.find(
        f =>
          //the cover+fire (cover data) and fire survey (fire data) are same visit
          f.attributes.plot_visit.data.id === survey.attributes.plot_visit.data.id &&
          //the two surveys' times are within 1 second of each other
          Math.abs(
            new Date(survey.attributes.survey_metadata.survey_details.time).valueOf() - new Date(f.attributes.survey_metadata.survey_details.time).valueOf()
          ) < 1000
      )
      if (
        !isEmpty(relatedFireSurvey) &&
        //cannot be cover data if we're fire survey
        !isFireProtocol
      ) coverDataIsFromFireSurvey = true
    }
    
    //this survey is not part of cover+fire, but we're searching the cover+fire
    //protocol, so skip checking
    if (isCoverAndFire && !coverDataIsFromFireSurvey) return 'continue'

    if (
      !isFireProtocol &&
      //either we're checking cover+fire protocol and the current survey is the cover
      //data from the cover+fire, or we're not checking cover+fire and so we don't
      //want the data to be cover data from a cover+fire
      (
        (
          isCoverAndFire &&
          coverDataIsFromFireSurvey
        ) ||
        (
          !isCoverAndFire &&
          !coverDataIsFromFireSurvey
        )
      ) &&
      survey?.attributes?.survey_metadata?.survey_details?.protocol_id !== protocol.attributes.identifier
    ) {
      // console.log(`Incorrect protocol ID (using wrong ID=${survey?.attributes?.survey_metadata?.survey_details?.protocol_id}) for survey with ID=${survey.id} and UUID ${survey?.attributes?.survey_metadata?.survey_details?.uuid}.`)
      return {
        id: survey.id,
        survey_uuid: survey?.attributes?.survey_metadata?.survey_details?.uuid,
        survey_org_minted_uuid: survey?.attributes?.survey_metadata?.orgMintedUUID,
        wrong_protocol_id: survey?.attributes?.survey_metadata?.survey_details?.protocol_id,
        wrong_protocol_name: protocolDefsData?.data.find(
          p => p.attributes.identifier === survey?.attributes?.survey_metadata?.survey_details?.protocol_id
        )?.attributes?.name
      }
    } else if (isFireProtocol) {
      const fireUpdateCandidateObj = {
        id: survey.id,
        survey_uuid: survey?.attributes?.survey_metadata?.survey_details?.uuid,
        survey_org_minted_uuid: survey?.attributes?.survey_metadata?.orgMintedUUID,
        fire_survey_missing_cover_survey: false,
      }
      const baseKeys = Object.keys(fireUpdateCandidateObj)

      if (survey?.attributes?.survey_metadata?.survey_details?.submodule_protocol_id !== protocol.attributes.identifier) {
        fireUpdateCandidateObj.wrong_submodule_protocol_id = survey?.attributes?.survey_metadata?.survey_details?.submodule_protocol_id
        fireUpdateCandidateObj.wrong_submodule_protocol_name = protocolDefsData?.data.find(
          p => p.attributes.identifier === survey?.attributes?.survey_metadata?.survey_details?.submodule_protocol_id
        )?.attributes?.name
      }

      const relatedCoverSurvey = coverData?.data.find(
        c =>
          //the cover+fire (cover data) and fire survey (fire data) are same visit
          c.attributes.plot_visit.data.id === survey.attributes.plot_visit.data.id &&
          //the two surveys' times are within 1 second of each other
          Math.abs(
            new Date(survey.attributes.survey_metadata.survey_details.time).valueOf() - new Date(c.attributes.survey_metadata.survey_details.time).valueOf()
          ) < 1000
      )
      // console.log('relatedCoverSurvey:', JSON.stringify(relatedCoverSurvey, null, 2))
      const fireSurveyHasRelatedCoverSurvey = !isEmpty(relatedCoverSurvey)
      if (!fireSurveyHasRelatedCoverSurvey) {
        fireUpdateCandidateObj.fire_survey_missing_cover_survey = true
        console.warn(`Unable to check if Fire Survey with ID=${survey.id} and UUID=${survey?.attributes?.survey_metadata?.survey_details?.uuid} has correct protocol ID as the relevant Cover data cannot be found`)
      } else if (
        survey?.attributes?.survey_metadata?.survey_details?.protocol_id !== relatedCoverSurvey.attributes.survey_metadata.survey_details.protocol_id
      ) {
        fireUpdateCandidateObj.wrong_protocol_id = survey?.attributes?.survey_metadata?.survey_details?.protocol_id
        fireUpdateCandidateObj.wrong_protocol_name = protocolDefsData?.data.find(
          p => p.attributes.identifier === survey?.attributes?.survey_metadata?.survey_details?.protocol_id
        )?.attributes?.name
      }

      if (
        !isEqual(
          Object.keys(fireUpdateCandidateObj).sort(),
          baseKeys.sort(),
        )
      ) {
        if (fireUpdateCandidateObj.fire_survey_missing_cover_survey === false) {
          //if it's not missing, remove the flag as we can treat the lack of this flag as
          //not missing (we're only trying to raise issues, not non-issues)
          delete fireUpdateCandidateObj.fire_survey_missing_cover_survey
        }

        //only push an update candidate if we detected something wrong
        return fireUpdateCandidateObj
      }
    }
  },
}

/**
 * Recursively searches the provided `object` for a `key` and returns the value if found
 *
 * Source: https://stackoverflow.com/a/40604638
 *
 * @param {Object} object the object to search over
 * @param {String} key the key to search for
 *
 * @returns {*} the value of the found `key`
 */
function findValForKey(object, key) {
  let value
  Object.keys(object).some(function (k) {
    if (k === key) {
      value = object[k]
      return true
    }
    if (object[k] && typeof object[k] === 'object') {
      value = findValForKey(object[k], key)
      return value !== undefined
    }
  })
  return value
}

function loadSchema({ path }) {
  let modelName = String(path).replace('/', '')
  
  if (
    alreadyPluralModels.includes(modelName) ||
    (
      alreadyPluralModels.some(
        p => modelName === `${p}s`
      ) &&
      modelName.endsWith('ss')
    )
  ) {
    if (modelName.endsWith('ss')) {
      modelName = modelName.substring(0, modelName.length - 1)
    } else {
      modelName = modelName
    }
    
  } else if (modelName.endsWith('-s')) {
    modelName = modelName.substring(0, modelName.length - 2)
  } else {
    modelName = pluralize.singular(modelName)
  }

  const filePath = `${CORE_PATH}/${modelName}/content-types/${modelName}/schema.json`
  const schema = JSON.parse(fs.readFileSync(filePath))
  return schema
}
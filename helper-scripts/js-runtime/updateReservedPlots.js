const fetch = require('node-fetch')

const {
  parseSystemUrl,
  getDataForEndpoint,
} = require('./helpers.js')

module.exports = {
  async run(system, JWT) {
    let baseUrl = parseSystemUrl(system)

    let msg = `Requests using base URL '${baseUrl}' with JWT '${JWT}'`
    console.log(msg)

    const plotSelectionData = await getDataForEndpoint({
      endpointPrefix: 'plot-selections',
      baseUrl,
      JWT,
    })

    const reservedPlotsData = await getDataForEndpoint({
      endpointPrefix: 'reserved-plot-labels',
      baseUrl,
      JWT,
    })

    for (const plotSelection of plotSelectionData?.data || []) {
      //we assume if a plot was reserve, it was done correctly, i.e., the plot label
      //matches the one in the relation to the plot selection
      const reservedPlotEntry = reservedPlotsData?.data?.find(
        d => d?.attributes?.plot_label === plotSelection?.attributes?.plot_label &&
          d?.attributes?.created_plot_selection?.data?.id === plotSelection?.id
      )
      if (!reservedPlotEntry) {
        const url = `${baseUrl}/reserved-plot-labels`
        console.log(`No reserved plot found for label: ${plotSelection?.attributes?.plot_label}. Will create one at: ${url}.`)
        const body = {
          data: {
            plot_label: plotSelection?.attributes?.plot_label,
            created_plot_selection: plotSelection?.id,
          },
        }
        try {
          const resp = await fetch(
            url,
            {
              method: 'POST',
              body: JSON.stringify(body),
              headers: {
                'Authorization': `Bearer ${JWT}`,
                'Content-Type': 'application/json',
              },
            },
          )

          if (resp.status !== 200) {
            console.error(`Failed to create reserved plot for label: ${plotSelection?.attributes?.plot_label}. Caused by: ${resp?.statusText}`)
          }
        } catch (err) {
          console.log(err)
        }
      }
    }
  }
}
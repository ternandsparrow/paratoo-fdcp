const {
  parseSystemUrl,
  getDataForEndpoint,
} = require('./helpers.js')

module.exports = {
  async run (system, JWT, deleteContextSwitch = false) {
    let baseUrl = parseSystemUrl(system)

    let msg = `Requests using base URL '${baseUrl}' with JWT '${JWT}'`
    if (deleteContextSwitch) {
      msg += '. Will delete plot def surveys when there is more than 1 survey per visit (delete all but the first, which is the \'create new\')'
    }
    console.log(msg)

    const plotDefSurveys = await getDataForEndpoint({
      endpointPrefix: 'plot-definition-surveys',
      baseUrl,
      JWT,
    })

    const plotDefSurveysByVisit = {}
    for (const survey of plotDefSurveys?.data || []) {
      if (
        survey?.attributes?.plot_visit?.data?.id &&
        !plotDefSurveysByVisit[survey?.attributes?.plot_visit?.data?.id]
      ) {
        plotDefSurveysByVisit[survey.attributes.plot_visit.data.id] = []
      }

      plotDefSurveysByVisit[survey.attributes.plot_visit.data.id].push(survey)
    }

    const candidatesForDeletion = []
    for (const [visitId, visitPlotDefSurveys] of Object.entries(plotDefSurveysByVisit)) {
      if (visitPlotDefSurveys.length > 1) {
        //map for better logging (else it takes up a lot of space due to deep-populated
        //relations)
        const mapped = visitPlotDefSurveys.map((s) => {
          return {
            plot_def_survey_id: s.id,
            survey_metadata: s.attributes.survey_metadata,
            plot_def_survey_times: {
              createdAt: s.attributes.createdAt,
              updatedAt: s.attributes.updatedAt,
            },
            plot_visit: {
              id: s.attributes.plot_visit.data.id,
              start_date: s.attributes.plot_visit.data.attributes.start_date,
              end_date: s.attributes.plot_visit.data.attributes.end_date,
              visit_field_name: s.attributes.plot_visit.data.attributes.visit_field_name,
            },
            plot_layout_id: s.attributes.plot_visit.data.attributes.plot_layout.data.id,
            plot_layout_times: {
              createdAt: s.attributes.plot_visit.data.attributes.plot_layout.data.attributes.createdAt,
              updatedAt: s.attributes.plot_visit.data.attributes.plot_layout.data.attributes.updatedAt,
            },
            plot_selection_id: s.attributes.plot_visit.data.attributes.plot_layout.data.attributes.plot_selection.data.id,
            plot_label: s.attributes.plot_visit.data.attributes.plot_layout.data.attributes.plot_selection.data.attributes.plot_label,
          }
        })
        console.log(`Visit with ID=${visitId} has > 1 plot def surveys. All plot def surveys for this visit: ${JSON.stringify(mapped, null, 2)}`)

        //`slice` to keep the 1st but remove the rest, as the 1st will always be when the
        //plot/visit were first defined
        for (const plotDefSurvey of visitPlotDefSurveys.slice(1)) {
          const currPlotDefSurveyLayout = plotDefSurvey?.attributes?.plot_visit?.data?.attributes?.plot_layout

          const currPlotDefSurveyCreatedAt = new Date(plotDefSurvey?.attributes?.createdAt)
          const currPlotDefSurveyLayoutUpdatedAt = new Date(currPlotDefSurveyLayout?.data?.attributes?.updatedAt)

          if (
            //check that a fauna plot was appended at some point
            currPlotDefSurveyLayout?.data?.attributes?.fauna_plot_point &&
            currPlotDefSurveyLayout?.data?.attributes?.createdAt !== currPlotDefSurveyLayout?.data?.attributes?.updatedAt &&
            //check that THIS plot def survey was the one that appended the fauna plot.
            //a fauna plot that was appended to an existing layout/visit will cause the
            //plot-layout's `updatedAt` attribute to the approx the
            //plot-definition-survey's `createdAt` (not the exact same millisecond, but
            //at least within a second of each other)
            Math.abs(currPlotDefSurveyCreatedAt.valueOf() - currPlotDefSurveyLayoutUpdatedAt.valueOf()) < 1000
          ) {
            console.warn(`Skipping plot def survey with ID=${plotDefSurvey.id} as the createdAt and updatedAt differ, indicating a possible creation of a fauna plot for an existing layout/visit`)
          } else if (!deleteContextSwitch) {
            console.log(`Plot def survey with ID=${plotDefSurvey.id} and orgMintedUuid=${plotDefSurvey?.attributes?.survey_metadata?.orgMintedUUID} is candidate for deletion`)
            candidatesForDeletion.push(plotDefSurvey)
          } else if (deleteContextSwitch) {
            console.log(`Deleting plot def survey with ID=${plotDefSurvey.id} and orgMintedUuid=${plotDefSurvey?.attributes?.survey_metadata?.orgMintedUUID}`)
            candidatesForDeletion.push(plotDefSurvey)
            try {
              const resp = await fetch(
                `${baseUrl}/plot-definition-surveys/${plotDefSurvey.id}`,
                {
                  method: 'DELETE',
                  headers: {
                    'Authorization': `Bearer ${JWT}`,
                  },
                },
              )
              if (resp.status !== 200) {
                console.error(`Failed to delete plot def survey with ID=${plotDefSurvey.id}. Caused by: ${resp?.statusText}`)
              }
            } catch (err) {
              console.log(err)
            }
          }
        }
      }
    }

    const mappedCandidatesForDeletion = candidatesForDeletion
      .sort((a, b) => a.id > b.id ? 1 : -1)
      .map(
        o => `ID=${o.id}, orgMintedUUID=${o?.attributes?.survey_metadata?.orgMintedUUID}`
      )
    console.log('All candidates for deletion: ', JSON.stringify(mappedCandidatesForDeletion, null, 2))
  }
}
const fs = require('fs')
const csvtojsonV2 = require('csvtojson')
const _ = require('lodash')

const LUT_TEC_SCHEMA_PATH = '../../paratoo-core/src/api/lut-tec/content-types/lut-tec/schema.json'

module.exports = {
  async run(targetCommunityFilePath) {
    console.log(`Generating target community for lut-tec from file at path: ${targetCommunityFilePath}`)
    const targetCommunityJson = await csvtojsonV2().fromFile(targetCommunityFilePath)
    const targetCommunityJsonSortedAlphabetical = targetCommunityJson.sort(
      (a, b) => (a.Community > b.Community) ? 1 : -1
    )
    console.log('targetCommunityJsonSortedAlphabetical:', JSON.stringify(targetCommunityJsonSortedAlphabetical, null, 2))

    const initData = []
    for (const targetCommunity of targetCommunityJsonSortedAlphabetical) {
      initData.push([
        generateLutSymbol(targetCommunity.Community),   //symbol
        targetCommunity.Community,    //label
        "",   //empty description for now
        generateEbpcStatusId(targetCommunity['EPBC Status']),    //EPBC Status
        generateStateIds(targetCommunity),    //state(s)
        ""    //empty URI for now
      ])
    }

    const duplicates = initData.reduce((acc, cur) => {
      if (acc.includes(cur[0])) {
        acc.push(cur[1])
      }     

      return acc
    }, [])
    if (duplicates.length > 0) {
      console.warn('some communities have duplicate symbols:', JSON.stringify(duplicates, null, 2))
    }

    const lutTecSchema = JSON.parse(fs.readFileSync(LUT_TEC_SCHEMA_PATH, 'utf8'))
    let oldInitData = _.cloneDeep(lutTecSchema.initialData)
    lutTecSchema.initialData = initData
    fs.writeFileSync(LUT_TEC_SCHEMA_PATH, JSON.stringify(lutTecSchema, null, 2))
    
    let msg = `wrote ${initData.length} target communities to ${LUT_TEC_SCHEMA_PATH}`
    if (oldInitData.length > 0) {
      msg += ` (replaced ${oldInitData.length} entries)`
    }
    console.log(msg)
  }
}

function generateLutSymbol(label) {
  const SKIP_WORDS = [
    '-',
    'on',
    'of',
    'for',
    'the',
    'a',
    'an',
    'and',
    'in',
    'to',
    'by',
  ]
  let symbol = '' 
  for (const word of label.replaceAll(/\((.*?)\)/gi, '').split(' ')) {
    if (!SKIP_WORDS.includes(word)) {
      symbol += word.charAt(0).toUpperCase()
    }
  }
  if (symbol.length === 0) {
    console.warn(`failed to generate symbol from label: ${label}`)
  }
  return symbol
}

function generateEbpcStatusId(statusLabel) {
  //TODO should probably grab from `lut-epbc-status`, but hardcoded for now
  const STATUS_MAP = {
    'Endangered': 1,
    'Critically Endangered': 2,
    'Vulnerable': 3,
  }

  if (!STATUS_MAP?.[statusLabel]) {
    console.warn(`failed to generate ebpc status id for status: ${statusLabel}`)
  }

  return STATUS_MAP?.[statusLabel]
}

function generateStateIds(communityObject) {
  const STATES = [
    'ACT',
    'NSW',
    'NT',
    'QLD',
    'SA',
    'TAS',
    'VIC',
    'WA',
  ]
  //TODO should probably grab from `lut-state`, but hardcoded for now
  const STATES_MAP = {
    'ACT': 1,
    'NSW': 2,
    'NT': 3,
    'QLD': 4,
    'SA': 5,
    'TAS': 6,
    'VIC': 7,
    'WA': 8,
  }

  const HYPHEN_UNICODE = '-'.charCodeAt(0)
  const MINUS_UNICODE = '−'.charCodeAt(0)

  const stateIds = []
  for (const [key, value] of Object.entries(communityObject)) {
    
    if (
      //`communityObject` contains all data, but just want columns relating to state
      STATES.includes(key)
    ) {
      const valueIsDash = value.length === 1 &&
        (
          value.charCodeAt(0) === HYPHEN_UNICODE ||
          value.charCodeAt(0) === MINUS_UNICODE
        )
      if (valueIsDash) continue
      if (value.toLowerCase() === 'yes') {
        stateIds.push(STATES_MAP[key])
      } else {
        console.warn(`unhandled condition when generating state IDs for community '${communityObject.Community}' with state '${key}' and value: '${value}'`)
      }
    }
  }
  return stateIds
}
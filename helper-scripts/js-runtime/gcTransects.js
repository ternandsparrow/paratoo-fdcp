const kml = require('gtran-kml')
const {
  parseSystemUrl,
  reverseLookup,
} = require('./helpers.js')
const fs = require('fs')

module.exports = {
  async run (system, JWT, orgMintedUUID) {
    let baseUrl = parseSystemUrl(system)

    let msg = `Requests using base URL '${baseUrl}' with JWT '${JWT}'. Will lookup Ground Counts Transects with orgMintedUUID=${orgMintedUUID}`
    console.log(msg)

    const lookupData = await reverseLookup({
      baseUrl,
      JWT,
      orgMintedUUID,
    })

    if (lookupData?.survey_metadata?.survey_details?.protocol_id !== 'a76dac21-94f4-4851-af91-31f6dd00750f') {
      throw new Error(`Provided an orgMintedUUID (${orgMintedUUID}) for a collection that is not Fauna Ground Counts Transects protocol`)
    }

    const surveyStart = lookupData?.collections?.['fauna-ground-counts-survey']?.start_date_time.toString().replaceAll(':', '-')
    const surveyEnd = lookupData?.collections?.['fauna-ground-counts-survey']?.end_date_time.toString().replaceAll(':', '-')

    const geoJsonForExport = {
      type: 'FeatureCollection',
      features: []
    }

    for (const transect of lookupData?.collections?.['field-reconnaissance-and-transect-set-up'] || []) {
      console.log(`transect ID = ${transect?.new_or_existing?.site_id}`)

      const additionalProperties = {
        transect_id: transect?.new_or_existing?.site_id,
        'transect_start_location.lat': transect.transect_start_location.lat,
        'transect_start_location.lng': transect.transect_start_location.lng,
        transect_length: transect.transect_length,
        transect_spacing: transect.transect_spacing,
        transect_alignment: transect.transect_alignment,
      }

      const transectEndLocation = getDestinationPoint(
        transect.transect_start_location,
        degToRad(transect.transect_alignment),
        transect.transect_length,
      )

      //line that makes the trasnect
      geoJsonForExport.features.push({
        type: 'Feature',
        geometry: {
          type: 'LineString',
          coordinates: [
            //start point
            [
              transect.transect_start_location.lng,
              transect.transect_start_location.lat
            ],
            //end point
            [
              transectEndLocation.lng,
              transectEndLocation.lat,
            ]
          ],
        },
        properties: {
          //the `name` property allows the transect to have a short name, which is useful
          //when trying to get a high-level view in the KML imported to Google Earth
          name: transect?.new_or_existing?.site_id,
          ...additionalProperties,
        },
      })

      const transectSurvey = lookupData?.collections?.['fauna-ground-counts-conduct-survey'].find(
        o => o.transect_ID === transect?.new_or_existing?.site_id
      )

      if (transectSurvey) {
        //this conducted survey's start location
        geoJsonForExport.features.push({
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [
              transectSurvey?.start?.start_location?.lng || 0,
              transectSurvey?.start?.start_location?.lat || 0,
            ],
          },
          properties: {
            name: `${transect?.new_or_existing?.site_id} - Transect Survey Start (${transectSurvey.setup_ID})`,
          },
        })

        //this conducted survey's end location
        const endLocationFeature = {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [
              transectSurvey?.end?.end_location?.lng || 0,
              transectSurvey?.end?.end_location?.lat || 0,
            ],
          },
          properties: {
            name: `${transect?.new_or_existing?.site_id} - Transect Survey End (${transectSurvey.setup_ID})`,
          },
        }
        if (
          !transectSurvey?.end?.end_location ||
          !transectSurvey?.end?.end_location?.lat ||
          !transectSurvey?.end?.end_location?.lng
        ) {
          Object.assign(endLocationFeature.properties, {
            error: 'No recorded end location'
          })
          console.warn(`Survey ${transectSurvey.setup_ID} does not have an end location`)
        }
        geoJsonForExport.features.push(endLocationFeature)

        //this conducted survey's track log
        const currLogLineString = []
        for (const logPoint of transectSurvey?.track_log || []) {
          currLogLineString.push(
            [logPoint.lng, logPoint.lat]
          )
        }

        const trackLogFeature = {
          type: 'Feature',
          geometry: {
            type: 'LineString',
            coordinates: [
              ...currLogLineString,
            ],
          },
          properties: {
            name: `${transect?.new_or_existing?.site_id} - Track Log (${transectSurvey.setup_ID})`,
          },
        }
        if (currLogLineString?.length === 0) {
          console.warn(`Survey ${transectSurvey.setup_ID} does not have a track log`)
          trackLogFeature.geometry.coordinates = [
            [0,0],
            [0,0],
          ]
          Object.assign(trackLogFeature.properties, {
            error: 'No track log'
          })
        }
        geoJsonForExport.features.push(trackLogFeature)
      } else {
        console.warn(`Couldn't find survey for transect ID=${transect?.new_or_existing?.site_id}`)
      }
    }

    console.log('Converting GeoJSON to KML:', JSON.stringify(geoJsonForExport, null, 2))

    const fn = `./gc_transects_${surveyStart}_to_${surveyEnd}_${orgMintedUUID}.kml`
    console.log(`Outputting KML file of GC Transects to: ${fn}`)
    await kml.fromGeoJson(geoJsonForExport, fn, {
      documentName: orgMintedUUID,
    })
  }
}


/**
 * Grabbed from webapp helpers
 * 
 * Get the destination point given a starting point, bearing, and distance.
 *
 * @param {Object} point1 - The starting point coordinates { lat, lng }.
 * @param {number} brng - The bearing in radians.
 * @param {number} d - The distance in meters.
 * @returns {Object} The destination point coordinates { lat, lng }.
 */
function getDestinationPoint(point1, brng, d) {
  // Convert lat and lng to radians
  let { lat: lat1, lng: lng1 } = point1
  lat1 = degToRad(lat1)
  lng1 = degToRad(lng1)

  // Earth's radius in meters
  let R = 6371e3

  // Calculate the latitude of the destination point
  let lat = Math.asin(
    Math.sin(lat1) * Math.cos(d / R) +
      Math.cos(lat1) * Math.sin(d / R) * Math.cos(brng),
  )

  // Calculate the longitude of the destination point
  let lng =
    lng1 +
    Math.atan2(
      Math.sin(brng) * Math.sin(d / R) * Math.cos(lat1),
      Math.cos(d / R) - Math.sin(lat1) * Math.sin(lat),
    )

  // Convert lat and lng back to degrees
  lat = (lat * 180) / Math.PI
  lng = (lng * 180) / Math.PI

  // Return the destination point coordinates
  return { lat, lng }
}

function degToRad(deg) {
  return (deg * Math.PI) / 180
}

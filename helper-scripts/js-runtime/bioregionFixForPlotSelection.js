'use strict'
const args = require('minimist')(process.argv.slice(2))

/** to avoid complexity so just run `export JWT=...; export URL=...` , e.g.
  export JWT=1234
  export URL=http://localhost:1337
 * before running this script
 * we don't really need .env
 */

// v6 -> v7
const lutsToUpdate = {
  VVP: 'SVP',
  FLI: 'FUR',
}

async function doFetch(method, path, body = undefined) {
  const options = {
    method,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${process.env.JWT}`,
    },
  }
  if (body) {
    options.body = JSON.stringify(body)
  }
  const res = await fetch(`${process.env.URL}/api/${path}`, options)
  if (!res.ok) {
    const errorMessage = await res.text()
    throw new Error(
      `HTTP error! status: ${res.status}, statusText: ${errorMessage}`,
    )
  }
  const data = await res.json()
  return data
}

async function findPlotsWithRenamedBioregion() {
  const path = (lut) =>
    `plot-selections?use-default=true&populate=deep&filters[plot_label][$contains]=${lut}`
  let plotData = []
  for (const region in lutsToUpdate) {
    const data = await doFetch('GET', path(region))
    plotData = plotData.concat(data.data)
  }

  console.log(
    `Found ${plotData.length} plots with incorrect bioregion: ${plotData
      .map((p) => p.attributes.plot_label)
      .join(', ')}`,
  )
  return plotData
}

function createPayloadToUpdate(obj) {
  for (const key in obj) {
    if (obj[key] === null) {
      delete obj[key]
    } else if (key === 'plot_selection_survey') {
      obj[key] = obj[key].data.id
    } else if (obj[key].data?.attributes.symbol) {
      obj[key] = obj[key].data.attributes.symbol
    } else if (typeof obj[key] === 'object' && obj[key] !== null) {
      createPayloadToUpdate(obj[key])
    }
  }
  return obj
}

function fixIncorrectData(plotData) {
  const updatedData = []
  for (const data of structuredClone(plotData)) {
    const currData = data.attributes
    const id = data.id
    currData.id = id
    const oldBioregion = currData.plot_label.slice(-7, -4)
    const oldPlotLabel = currData.plot_label

    const newBioregion = lutsToUpdate[oldBioregion]

    if (newBioregion) {
      const newPlotLabel = oldPlotLabel.replace(oldBioregion, newBioregion)
      currData.plot_label = newPlotLabel
      updatedData.push(createPayloadToUpdate(currData))
    } else {
      throw new Error(
        `Failed to update plot selection ${id}, plot name ${oldPlotLabel} has incorrect bioregion`,
      )
    }
  }
  return updatedData
}

async function updateFixedDataToCore(updatedData) {
  const success = []
  const errors = []
  const reservedPlotLabels =
    !updatedData.length || (await getReservedPlotLabels())

  for (const data of updatedData) {
    const id = data.id
    const oldPlotLabel = data.plot_label
    const newPlotLabel = data.plot_label
    try {
      await doFetch('PUT', `plot-selections/${id}`, { data })
      success.push(`id: ${id}, from  ${oldPlotLabel} to  ${newPlotLabel}`)
      const relatedReservedPlotLabelId = reservedPlotLabels[oldPlotLabel]
      await updateReservedPlotLabel(relatedReservedPlotLabelId, newPlotLabel)
    } catch (error) {
      errors.push(
        `Failed to update plot selection ${id}, plot name ${oldPlotLabel}, reason: ${error}`,
      )
    }
  }
  return { success, errors }
}

async function getReservedPlotLabels() {
  const path = 'reserved-plot-labels?use-default=true&populate=deep'
  const data = await doFetch('GET', path)
  const reservedPlotLabels = data.data.reduce(
    (acc, curr) => ({ ...acc, [curr.attributes.plot_label]: curr.id }),
    {},
  )
  return reservedPlotLabels
}

async function updateReservedPlotLabel(id, newPlotLabel) {
  const path = `reserved-plot-labels/${id}`
  const data = { data: { plot_label: newPlotLabel } }
  await doFetch('PUT', path, data)
}

function recursiveDiff(obj1, obj2) {
  const diff = {}
  for (const key of Object.keys(obj1)) {
    if (['createdAt', 'updatedAt'].includes(key) || obj1[key] === null) continue
    if (typeof obj1[key] === 'object' && obj1[key] !== null) {
      if (obj2[key] && typeof obj2[key] === 'object') {
        const subDiff = recursiveDiff(obj1[key], obj2[key])
        if (Object.keys(subDiff).length > 0) diff[key] = subDiff
      } else {
        diff[key] = obj1[key]
      }
    } else if (obj1[key] !== obj2[key]) {
      diff[key] = obj1[key]
    }
  }
  return diff
}

async function compareDataPostUpdated(plots) {
  for (const plot of plots) {
    let path = `plot-selections/${plot.id}?use-default=true&populate=deep`
    const { data } = await doFetch('GET', path)
    const diff = recursiveDiff(data, plot)
    // find differences in data and plots objects
    if (
      diff &&
      Object.keys(diff.attributes).length > 1 &&
      diff.attributes.plot_label !== data.attributes.plot_label
    ) {
      console.log(
        `Plot with id ${plot.id} has incorrect data after update:`,
        JSON.stringify(diff, null, 2),
      )
    }
  }
}

/**
 * return array of plot selections that have bioregion in plot_label
 * that doesn't match with bioregion in plot_name
 * @returns {Promise<Array>} list of plots with incorrect bioregion
 */
async function getAllPlotsUnmatchedBioregionInLabel() {
  const allPlotSelections = await doFetch(
    'GET',
    'plot-selections?use-default=true&populate[plot_name][populate][0]=bioregion',
  )
  const weirdPlotsFromInitData = ['QDASEQ0001 (bird)', 'QDASEQ0003-4 (bird)']
  let plotWithIncorrectBioregion = []
  for (const plot of allPlotSelections.data) {
    if (weirdPlotsFromInitData.includes(plot.attributes.plot_label)) continue
    const bioregion = plot.attributes.plot_name.bioregion.data.attributes.symbol
    const bioRegionInLabel = plot.attributes.plot_label.slice(-7, -4)
    if (bioregion !== bioRegionInLabel) {
      plotWithIncorrectBioregion.push(plot)
    }
  }
  return plotWithIncorrectBioregion
}

function checkDataIntegrity(
  plotsWithRenamedBioregion,
  plotWithIncorrectBioregion,
) {
  if (plotWithIncorrectBioregion.length !== plotsWithRenamedBioregion.length) {
    console.log(
      'plotWithIncorrectBioregion',
      JSON.stringify(plotWithIncorrectBioregion, null, 2),
    )
    console.log(
      'plotsWithRenamedBioregion',
      JSON.stringify(plotsWithRenamedBioregion, null, 2),
    )
    throw new Error(
      'plots and plotWithIncorrectBioregion are not match, something is wrong',
    )
  } else {
    for (const [index, plot] of plotWithIncorrectBioregion.entries()) {
      if (plot.id !== plotsWithRenamedBioregion[index].id) {
        throw new Error(
          'There other plots that have bioregion mismatch that not related to renamed bioregions',
        )
      }
    }
  }
}

async function fix() {
  const plotWithIncorrectBioregion =
    await getAllPlotsUnmatchedBioregionInLabel()
  const plotsWithRenamedBioregion = await findPlotsWithRenamedBioregion()

  // check if the above two are match
  checkDataIntegrity(plotsWithRenamedBioregion, plotWithIncorrectBioregion)

  const updatedData = fixIncorrectData(plotsWithRenamedBioregion)
  const result = await updateFixedDataToCore(updatedData)
  if (result.success.length > 0) {
    console.log(`*****Updated plots:\n${result.success.join(', \n')}`)
  }

  if (result.errors.length > 0) {
    console.log(`*****Errors:\n${result.errors.join(', \n')}`)
  }
  compareDataPostUpdated(plotsWithRenamedBioregion)
}

async function init() {
  if (args.help || args.h) {
    const fileName = __filename.split('/').pop()
    console.log(`
    Usage: node ${fileName} [mode] [options]
    mode: fix, findV6, findInconsistentLabel

    Mode: fix
    - Find and update incorrect bioregion v6 to v7 in plot_label and plot_name

    Mode: findV6
    - Find plots with incorrect bioregion v6 in plot_label and plot_name

    Mode: findInconsistentLabel
    - Find plots with incorrect bioregion in plot_label
    `)
    return
  }
  switch (args['mode']) {
    case 'fix':
      await fix()
      break
    case 'findV6':
    case 'findv6':
      const incorrectData = await findPlotsWithRenamedBioregion()
      console.log('*** incorrect data: ')
      console.log(
        JSON.stringify(
          structuredClone(incorrectData).map((_) => createPayloadToUpdate(_)),
          null,
          2,
        ),
      )
      const data = fixIncorrectData(incorrectData)
      console.log('*** payloads to update: ')
      console.log(JSON.stringify(data, null, 2))
      break
    case 'findInconsistentLabel':
      await getAllPlotsUnmatchedBioregionInLabel()
      break
    default:
      console.error('Unknown mode')
      break
  }
}

init()

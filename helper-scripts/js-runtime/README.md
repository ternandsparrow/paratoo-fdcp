# Simple JS runtime

This is a basic Node.js runtime to be able to run helper scripts that are not bash or Python.

Create your own helper script:

- define your script in a JavaScript (`.js`) file with a `run()` function as a `module.exports`.
- `require` your helper in `index.js`
- create conditional logic based on the command line args and call your `run()` function
- create a sub-section in this README documenting your script

## `createClientInitState`

> TODO automate this process as part of the build process

Creates the app initial data (baked in LUTs) from the intial data in Core's schemas.

Example usage: `node index.js --init_data`

## `generateUUIDs`

Generates a specified number of V4 UUIDs, which are printed to the console.

Example usage: `node index.js --num_uuids_to_generate 50`

## `decodeOrgIdentifier`

Decodes an org minted identifier (from base64).

Example usage: `node index.js --encoded_identifier eyJ1c2VySWQiOjEsImV2ZW50VGltZSI6IjIwMjMtMDktMTJUMDc6MDI6MjUuNTA4WiIsInN1cnZleUlkIjp7InN1cnZleVR5cGUiOiJjb3Zlci1wb2ludC1pbnRlcmNlcHQtc3VydmV5IiwidGltZSI6IjIwMjMtMDktMTJUMDc6MDE6NTcuMTkzWiIsInJhbmROdW0iOjgzMDcyNjU0LCJwcm9qZWN0SWQiOjEsInByb3RvY29sIjp7ImlkIjoiMzdhM2IwMTgtMzc3OS00YzRmLWJmYjMtZDM4ZWI1M2EyNTY4IiwidmVyc2lvbiI6MX19fQ==`

## `findUnusedModels`

Searches for unused models. NOTE: not very robust (will create false-positives), so you will need to manually check the outputs.

Example usage: `node index.js --unused_models`

## `generateTargetCommunity`

Generates the initial data for the target community LUT (`lut-tec`), which is mined from the CSV file path provided as the argument. This file can be found at [data.gov.au]('https://data.gov.au/data/dataset/threatened-species-state-lists/resource/17310982-2a37-4911-89ac-72c80533266a'). The generated init data is written directly to the schema of this LUT

Example usage: `node index.js --generate_target_community /path/to/file.csv`

## `queryAllEndpoints`

Queries all endpoints for a given base URL, and compares to the local file system's init data. Useful if want to check that a system with init data has any additional data that has been submit.

Example usage: `node index.js --query_all_endpoints http://localhost:1337/api`

## `submitCollectionsSummary`

Queries core's org-uuid-survey-metadata table and reduces the data into an easy to read format; number of collections for each protocol, sorted by most collected.

Requires an additional `jwt` parameter, which is the `EXPORTER_JWT` from the system.

Example usage: `node index.js --submit_collections_summary monitor-dev --jwt abc`

## `auditApiData`

Audits API data based on a provided data dump from Org.

Example usage: `node index.js --audit_api_data monitor-prod --jwt abc --org_data_path /path/to/data.json`

## `updateReservedPlots`

Will check if all Plot Selections have an entry in the Reserved Plot Label table. If a given one doesn't, it will create one via a POST request.

Example usage: `node index.js --update_reserved_plots monitor-prod --jwt abc`

## `lookupProtocol`

Queries all data for a given protocol via the `/reverse-lookup` endpoint. Can optionally provide flag to dump data to files.

Note that if you provide the `jwt` argument the `EXPORTER_JWT` all data is retrieved, so proceed with caution. If you're trying to provide data for a specific user, it would be best to use that user's JWT, or a JWT from a different user on the same projects.

Example usage: `node index.js --lookup_protocol paratoo-prod --jwt abc --protocol some_prot_uuid --dump_file true`

## `fixMissingOrgMintedUuids`

Uses a provided text file to update a survey's `survey_metadata` with correct `orgMintedUUID` and creates an entry in `org-uuid-survey-metadata` if needed.

The text file is made up of lines, where each line is formatted: `survey_details.uuid,desired_orgMintedUuid`. The contents of the file are obtained from MERIT team, who are able to query their DB for orgMintedUuids when we provide `survey_details.uuid`.

Example usage: `node index.js --fix_missing_org_minted_uuids monitor-prod --jwt abc --fixed_data_path /path/to/data.txt`

## `removeInitData`

Detects initial data from the provided API using the indexes of the initial data in the schemas and a `createdAt` threshold.

This is not a completely reliable method - won't generate false-positives (i.e., data that is real but is detected as init data), but can generate false-negatives (i.e., data that is init data but is not detected as such). Thus, will likely need to manually check the 'skip' warnings that are printed

Example usage: `node index.js --remove_init_data monitor-prod --jwt abc --created_at 2024-05-01T14:00:00Z --delete_data true`

## `ctDeploymentKml`

Creates a KML file from a CT deployment by using the `orgMintedUUID` to perform a `/reverse-lookup`.

Example usage: `node index.js --ct_deployment_kml monitor-prod --jwt abc --org_minted_uuid abc`


## `compareCoords`

Compares coordinates in two CSV files that have columns 'name', 'latitude', and 'longitude'. Will output the distance between rows in the two files that have the same 'name'.

Example usage: `node index.js --compare_coords --file1 file_one.csv --file2 file_two.csv`

## `gcTransects`

Similar to `ctDeploymentKml`, will generate a KML file of a given Fauna Ground Counts Transects collection (by using the `orgMintedUUID` to perform a `/reverse-lookup`), containing each Transect (from the Transect Setup), and each Conducted Survey's start/end locations and track log.

Example usage: `node index.js --gc_transects monitor-prod --jwt abc --org_minted_uuid abc`

## `findAndRemoveRedundantPlotDefSurveys`

Searches for entries in the Plot Definition Survey table that were submit due to the user swapping plot context ('select existing'). Swapping plot context shouldn't need to POST to core, as there's no new data - see #1800.

If the `delete_context_switch` flag is provided, will also delete these entries. It will keep the first Plot Def Survey for a given visit, as that corresponds with the actual creation of the visit.

Example usage: `node index.js --plot_def_surveys monitor-prod --jwt abc --delete_context_switch true`


## `fixProtocolIds`

Detects (not fixes, yet) incorrect protocol IDs in `survey_metadata` (both in the survey and the entry in org-uuid-collection-identifier).

Example usage: `node index.js --fix_protocol_ids monitor-prod --jwt abc --update_data true`

## `storageSummary`

Outputs a breakdown of storage usage based on analytics in Core.

Also optionally provide an `--ignore_list` (path to a CSV containing list of users to ignore; must contain at least a column with `userId`)

Example usage: `node index.js --storage_summary monitor-prod --jwt abc`

## `apiModelStoreZipsonSize`

Breaks down the size of a compressed (zipson) API Models Store

Example usage: `node index.js --apiModels_zipson_size`

## `resolveCoverRefsFromHistorical`

Resolves queued cover collections that have `temp_offline_id` references from the `historical_submitResponses`. Pass a file containing the `client_state_data_manager` from a data dump.

Example usage: `node index.js --resolve_cover_refs_from_historical /path/to/file`

## `surveyDependsOnPlot`

Searches for surveys that depend on plot data.

Example usage (search by plot label): `node index.js --surveys_depend_plot monitor-prod --jwt abc --plot some_plot_label --type label`

Example usage (search by visit field name): `node index.js --surveys_depend_plot monitor-prod --jwt abc --plot "some visit field name" --type visit_field_name`

Example usage (search by visit ID): `node index.js --surveys_depend_plot monitor-prod --jwt abc --plot some_visit_id --type visit_id`

## `visualiseProjectPlots`

Generates a KML of all project's plots.

Example usage: `node index.js --project_plots monitor-prod --jwt abc --project_uuid some_uuid`

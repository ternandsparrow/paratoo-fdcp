// handle cli args: https://stackoverflow.com/a/24638042
const argv = require('minimist')(process.argv.slice(2))
console.log('args: ', argv)
const arg_keys = Object.keys(argv)

if (arg_keys.includes('init_data')) {
  const createClientInitState = require('./createClientInitState.js')
  createClientInitState.run()
}

if (arg_keys.includes('num_uuids_to_generate')) {
  const generateUUIDs = require('./generateUUIDs.js')
  generateUUIDs.run(argv.num_uuids_to_generate)
}

if (arg_keys.includes('encoded_identifier')) {
  const decodeOrgIdentifier = require('./decodeOrgIdentifier.js')
  decodeOrgIdentifier.run(argv.encoded_identifier)
}

if (arg_keys.includes('unused_models')) {
  const unusedModels = require('./findUnusedModels.js')
  unusedModels.run()
}

if (arg_keys.includes('generate_target_community')) {
  const targetCommunity = require('./generateTargetCommunity.js')
  targetCommunity.run(argv.generate_target_community)
}

if (arg_keys.includes('query_all_endpoints')) {
  const queryAllEndpoints = require('./queryAllEndpoints.js')
  queryAllEndpoints.run(argv.query_all_endpoints)
}

if (arg_keys.includes('submit_collections_summary')) {
  const submitCollectionsSummary = require('./submitCollectionsSummary.js')

  if (!argv.jwt) {
    throw new Error(`Must provide 'jwt' argument`)
  }
  submitCollectionsSummary.run(argv.submit_collections_summary, argv.jwt)
}

if (arg_keys.includes('audit_api_data')) {
  const auditApiData = require('./auditApiData.js')

  if (!argv.jwt) {
    throw new Error(`Must provide 'jwt' argument`)
  }

  auditApiData.run(argv.audit_api_data, argv.jwt, argv?.org_data_path || null)
}

if (arg_keys.includes('update_reserved_plots')) {
  const updateReservedPlots = require('./updateReservedPlots.js')

  if (!argv.jwt) {
    throw new Error(`Must provide 'jwt' argument`)
  }

  updateReservedPlots.run(argv.update_reserved_plots, argv.jwt)
}

if (arg_keys.includes('lookup_protocol')) {
  const lookupProtocol = require('./lookupProtocol.js')

  if (!argv.jwt) {
    throw new Error(`Must provide 'jwt' argument`)
  }

  if (!argv.protocol) {
    throw new Error(`Must provide 'protocol' argument (protocol UUID)`)
  }

  const dumpFile = argv?.dump_file === 'true' || argv?.dump_file === true || false

  lookupProtocol.run(argv.lookup_protocol, argv.jwt, argv.protocol, dumpFile)
}

if (arg_keys.includes('fix_missing_org_minted_uuids')) {
  const fixMissingOrgMintedUuids = require('./fixMissingOrgMintedUuids.js')

  if (!argv.jwt) {
    throw new Error(`Must provide 'jwt' argument`)
  }

  if (!argv.fixed_data_path) {
    throw new Error(`Must provide 'fixed_data_path' argument`)
  }

  fixMissingOrgMintedUuids.run(argv.fix_missing_org_minted_uuids, argv.jwt, argv.fixed_data_path)
}

if (arg_keys.includes('remove_init_data')) {
  const removeInitData = require('./removeInitData.js')

  if (!argv.jwt) {
    throw new Error(`Must provide 'jwt' argument`)
  }
  if (!argv.created_at) {
    throw new Error(`Must provide 'created_at' argument`)
  }

  const deleteData = argv?.delete_data === 'true' || argv?.delete_data === true || false

  removeInitData.run(argv.remove_init_data, argv.jwt, argv.created_at, deleteData)
}

if (arg_keys.includes('ct_deployment_kml')) {
  const ctDeploymentKml = require('./ctDeploymentKml.js')

  if (!argv.jwt) {
    throw new Error(`Must provide 'jwt' argument`)
  }

  if (!argv.org_minted_uuid) {
    throw new Error(`Must provide 'org_minted_uuid' argument`)
  }

  ctDeploymentKml.run(argv.ct_deployment_kml, argv.jwt, argv.org_minted_uuid)
}

if (arg_keys.includes('compare_coords')) {
  const compareCoords = require('./compareCoords.js')

  for (const fileParam of ['file1', 'file2']) {
    if (!argv[fileParam]) {
      throw new Error(`Must provide '${fileParam}' argument`)
    }
  }

  compareCoords.run(argv.file1, argv.file2)
}

if (arg_keys.includes('gc_transects')) {
  const gcTransects = require('./gcTransects.js')

  if (!argv.jwt) {
    throw new Error(`Must provide 'jwt' argument`)
  }

  if (!argv.org_minted_uuid) {
    throw new Error(`Must provide 'org_minted_uuid' argument`)
  }

  gcTransects.run(argv.gc_transects, argv.jwt, argv.org_minted_uuid)
}

if (arg_keys.includes('plot_def_surveys')) {
  const findAndRemoveRedundantPlotDefSurveys = require('./findAndRemoveRedundantPlotDefSurveys.js')

  if (!argv.jwt) {
    throw new Error(`Must provide 'jwt' argument`)
  }
  const deleteContextSwitch = argv?.delete_context_switch === 'true' || argv?.delete_context_switch === true || false

  findAndRemoveRedundantPlotDefSurveys.run(argv.plot_def_surveys, argv.jwt, deleteContextSwitch)
}

if (arg_keys.includes('fix_protocol_ids')) {
  const fixProtocolIds = require('./fixProtocolIds.js')

  if (!argv.jwt) {
    throw new Error(`Must provide 'jwt' argument`)
  }

  const updateData = argv?.update_data === 'true' || argv?.update_data === true || false

  fixProtocolIds.run(argv.fix_protocol_ids, argv.jwt, updateData)
}

if (arg_keys.includes('storage_summary')) {
  const storageSummary = require('./storageSummary')

  if (!argv.jwt) {
    throw new Error(`Must provide 'jwt' argument`)
  }

  storageSummary.run(argv.storage_summary, argv.jwt, argv?.ignore_list)
}

if (arg_keys.includes('apiModels_zipson_size')) {
  const apiModelStoreZipsonSize = require('./apiModelStoreZipsonSize.js')

  apiModelStoreZipsonSize.run()
}

if (arg_keys.includes('resolve_cover_refs_from_historical')) {
  const resolveCoverRefsFromHistorical = require('./resolveCoverRefsFromHistorical.js')

  resolveCoverRefsFromHistorical.run(argv.resolve_cover_refs_from_historical)
}

if (arg_keys.includes('surveys_depend_plot')) {
  const surveyDependsOnPlot = require('./surveyDependsOnPlot')

  if (!argv.jwt) {
    throw new Error(`Must provide 'jwt' argument`)
  }

  if (!argv.plot) {
    throw new Error(`Must provide 'plot' argument`)
  }

  if (!argv.type) {
    throw new Error(`Must provide 'type' argument`)
  }

  surveyDependsOnPlot.run(argv.surveys_depend_plot, argv.jwt, argv.plot, argv.type)
}

if (arg_keys.includes('project_plots')) {
  const visualiseProjectPlots = require('./visualiseProjectPlots')

  if (!argv.jwt) {
    throw new Error(`Must provide 'jwt' argument`)
  }
  
  const cornersOnly = argv?.corners_only === 'true' || argv?.corners_only === true || false

  visualiseProjectPlots.run(argv.project_plots, argv.jwt, argv.project_uuid, cornersOnly)
}

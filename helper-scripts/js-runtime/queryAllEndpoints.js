const fetch = require('node-fetch')
const {
  getInitData,
} = require('./helpers.js')

const excludePathsContaining = [
  //'lut-',
  '{id}',
  '/bulk',
  '/upload',
  '/auth',
  '/users',
  '/connect',
  '/generate-barcode',
  '/many',
  '/protocols',
  'cron',
  '/org-uuid-survey-metadatas',
]

module.exports = {
  async run(urlBase) {
    console.log(`Querying all endpoints from base URL: ${urlBase}`)
    const swaggerResp = await fetch(`${urlBase}/documentation/swagger.json`)
    const swaggerData = await swaggerResp.json()

    const excludedPaths = []
    const dataDiffs = []
    for (const [path, methods] of Object.entries(swaggerData.paths)) {
      
      if (!excludePathsContaining.some(p => path.includes(p))) {
        const apiData = await queryPath(urlBase, path)
        const initData = getInitData(path)

        if (apiData.data.length !== initData.length) {
          dataDiffs.push(path)
        }
      } else {
        excludedPaths.push(path)
      }
    }
    console.log(`Excluded paths: ${JSON.stringify(excludedPaths, null, 2)}`)
    console.log(`Data diffs: ${JSON.stringify(dataDiffs, null, 2)}`)

  }
}

async function queryPath(urlBase, path) {
  const url = `${urlBase}${path}`
  // console.log('url:', url)
  const resp = await fetch(url, {
    headers: {
      'Authorization': 'Bearer 1234',
    }
  })
  if (resp.status === 200) {
    const respData = await resp.json()
    // console.log(`respData (status=${resp.status}):`, respData)
    return respData
  } else {
    console.warn(`failed to query ${url} (status=${resp.status})`)
    return null
  }
}
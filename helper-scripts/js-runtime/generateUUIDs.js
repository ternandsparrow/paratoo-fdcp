const uuid = require('uuid')

module.exports = {
  run(num_uuids_to_generate) {
    console.log(`Generating ${num_uuids_to_generate} UUIDs`)
    for (let i = 0; i < num_uuids_to_generate; i++) {
      console.log(`UUID ${i+1}: ${uuid.v4()}`)
    }
  }
}
const fs = require('fs')

const CORE_PATH = '../../paratoo-core/src/api'
const STATE_INIT_PATH = '../../paratoo-webapp/src/assets/stateInits/apiModels.js'

module.exports = {
  run(writeFile=true, corePath = CORE_PATH) {
    console.log('Creating client state initial data from Core\'s initial data')

    const initState = {}

    fs.readdirSync(corePath).forEach(file => {
      const currLutInitData = makeInitDataForLut(file, corePath)
      if (currLutInitData) {
        initState[file] = currLutInitData
      }
    })
    
    const content = "export const initState = " + JSON.stringify({ models: initState }, null, 2)

    if(writeFile) {
      try {
        fs.writeFileSync(STATE_INIT_PATH, content)
      } catch (err) {
        console.error(err)
        return
      }
    } else {
      return content
    }
  },
}

/**
 * Creates the initial data for the given LUT. Also resolved relations
 * 
 * @param {String} file the LUT's model name
 * 
 * @returns {Array.<Object>} the LUT's initial data, where each object has at minimum the
 * following keys: id, symbol, label, description, uri. Might contain additional keys for relations
 */
function makeInitDataForLut(file, corePath) {
  const path = `${corePath}/${file}/content-types/${file}/schema.json`
  if (file.startsWith('lut-') && fs.existsSync(path)) {
    const schema = JSON.parse(fs.readFileSync(path))
    if (schema.initialData) {
      const currLutInitData = []
      const attributeKeys = Object.keys(schema.attributes)

      for (const [index, initDataRow] of schema.initialData.entries()) {
        const currObj = {
          id: index+1,
        }
        for (const [idx, currInitDataRowAttribute] of initDataRow.entries()) {
          if (schema.attributes[attributeKeys[idx]].type === 'relation') {
            const relationModelName = schema.attributes[attributeKeys[idx]].target.slice(5).split('.')[0]
            const relationInitData = makeInitDataForLut(relationModelName, corePath)
            if (schema.attributes[attributeKeys[idx]].relation === 'oneToOne') {
              currObj[attributeKeys[idx]] = relationInitData.find(o => o.id === currInitDataRowAttribute)
            } else if (schema.attributes[attributeKeys[idx]].relation === 'oneToMany') {
              currObj[attributeKeys[idx]] = relationInitData.filter(o => {
                if (Array.isArray(currInitDataRowAttribute)) {
                  return currInitDataRowAttribute.includes(o.id)
                } else {
                  return o.id === currInitDataRowAttribute
                }
              })
            } else {
              console.warn(`Unknown relation type '${schema.attributes[attributeKeys[idx]].relation}' for relation field name '${schema.attributes[attributeKeys[idx]]}' in schema for '${file}'`)
            }
          } else {
            currObj[attributeKeys[idx]] = currInitDataRowAttribute
          }
          
        }
        currLutInitData.push(currObj)
      }
      
      return currLutInitData
      
    } else {
      console.warn(`Missing init data for LUT ${file}`)
    }
  }

  return null  
}
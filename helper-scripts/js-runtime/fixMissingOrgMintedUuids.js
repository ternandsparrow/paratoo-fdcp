const fs = require('fs')
const {
  parseSystemUrl,
  getDataForEndpoint,
} = require('./helpers.js')
const {
  cloneDeep,
} = require('lodash')

module.exports = {
  //TODO survey endpoint from variable/param - right now it's hardcoded to plot-selection-survey - maybe pass the protocol UUID
  async run(system, JWT, fixedDataPath) {
    let baseUrl = parseSystemUrl(system)
    let msg = `Requests using base URL '${baseUrl}' with JWT '${JWT}'. Using 'fixed' file at path ${fixedDataPath}`
    console.log(msg)

    const formattedFixedData = loadAndFormatFixedDataFile({
      path: fixedDataPath,
    })
    console.log('Will use the following data to fix missing orgMintedUuids and/or missing entries in org-uuid-survey-metadata:', JSON.stringify(formattedFixedData, null, 2))

    const plotSelectionSurveys = await getDataForEndpoint({
      endpointPrefix: 'plot-selection-surveys',
      baseUrl,
      JWT,
    })
    // console.log('plotSelectionSurveys:', JSON.stringify(plotSelectionSurveys, null, 2))
    const orgUuidSurveyMetadatas = await getDataForEndpoint({
      endpointPrefix: 'org-uuid-survey-metadatas',
      baseUrl,
      JWT,
    })
    // console.log('orgUuidSurveyMetadatas:', JSON.stringify(orgUuidSurveyMetadatas, null, 2))

    const plotSelectionSurveyUrl = `${baseUrl}/plot-selection-surveys`
    const orgUuidSurveyMetadataUrl = `${baseUrl}/org-uuid-survey-metadatas`

    const modifiedEntries = {}    //key is orgMintedUUID, value is object with metadata
    for (const surveyToFix of formattedFixedData) {
      const relevantPlotSelectionSurvey = plotSelectionSurveys?.data?.find(
        s => s.attributes.survey_metadata.survey_details.uuid === surveyToFix.surveyMetadataUuid
      )
      // console.log('relevantPlotSelectionSurvey:', JSON.stringify(relevantPlotSelectionSurvey, null, 2))

      //try update the plot selection survey
      if (relevantPlotSelectionSurvey) {
        if (!relevantPlotSelectionSurvey?.attributes?.survey_metadata?.orgMintedUUID) {
          console.log(`Updating Plot Selection Survey's (ID=${relevantPlotSelectionSurvey.id}) metadata to add missing orgMintedUUID: ${surveyToFix.orgMintedUUID}`)
          modifiedEntries[surveyToFix.orgMintedUUID] = {}
          modifiedEntries[surveyToFix.orgMintedUUID].plot_selection_survey_id = relevantPlotSelectionSurvey.id
          modifiedEntries[surveyToFix.orgMintedUUID].survey_metadata_uuid = surveyToFix.surveyMetadataUuid
          //remove all object values with empty (null/undefined) data to prevent
          //validation errors
          const body = removeEmpty(relevantPlotSelectionSurvey?.attributes)
          body.survey_metadata.orgMintedUUID = surveyToFix.orgMintedUUID
          // console.log('body:', JSON.stringify(body, null, 2))
          const baseErrMsg = `Failed to update Plot Selection Survey's (ID=${relevantPlotSelectionSurvey.id}) missing orgMintedUUID`
          try {
            const resp = await fetch(
              `${plotSelectionSurveyUrl}/${relevantPlotSelectionSurvey.id}`,
              {
                method: 'PUT',
                body: JSON.stringify({
                  data: body,
                }),
                headers: {
                  'Authorization': `Bearer ${JWT}`,
                  'Content-Type': 'application/json',
                },
              },
            )
            if (resp.status !== 200) {
              console.error(`${baseErrMsg}. Caused by: ${resp?.statusText}`)
            }
          } catch (err) {
            console.error(`${baseErrMsg}. Caused by: ${err?.message || 'unknown'}`)
          }
        } else if (relevantPlotSelectionSurvey?.attributes?.survey_metadata?.orgMintedUUID) {
          const relevantDataMatchesFixedData = relevantPlotSelectionSurvey?.attributes?.survey_metadata?.orgMintedUUID === surveyToFix.orgMintedUUID
          console.warn(`Want to update Plot Selection Survey's metadata's (uuid=${surveyToFix.surveyMetadataUuid}) orgMintedUUID, but there is already one that ${relevantDataMatchesFixedData ? 'does' : 'does not'} match the desired value: ${relevantPlotSelectionSurvey?.attributes?.survey_metadata?.orgMintedUUID}`)
        }
      } else {
        //TODO test this case
        console.warn(`Failed to query a relevant Plot Selection survey from survey metadata details UUID=${surveyToFix.surveyMetadataUuid}`)
      }
      
      //check if there is also a missing entry in org-uuid-survey-metadata by using the
      //Plot Selection we just updated
      const relevantOrgUuidSurveyMetadata = orgUuidSurveyMetadatas?.data?.find(
        s => s.attributes.survey_details.uuid === surveyToFix.surveyMetadataUuid
      )
      // console.log('relevantOrgUuidSurveyMetadata:', JSON.stringify(relevantOrgUuidSurveyMetadata, null, 2))
      if (relevantOrgUuidSurveyMetadata) {
        //TODO handle this case (do PUT instead of POST)
        console.warn(`Unhandled case: entry already exists in org-uuid-survey-metadata for UUID=${surveyToFix.surveyMetadataUuid}`)
      } else {
        //first retrieve the Plot Selection Survey (that should now be updated/corrected
        //with the desired orgMintedUUID) to duplicate it's survey_metadata
        console.log(`Fetching Plot Selection Survey (survey UUID=${surveyToFix.surveyMetadataUuid}, desired orgMintedUUID=${surveyToFix.orgMintedUUID}) to duplicate it's survey_metadata in org-uuid-survey-metadata`)
        const baseErrMsgSelection = `Failed to find a Plot Selection Survey with survey UUID=${surveyToFix.surveyMetadataUuid} and orgMintedUUID=${surveyToFix.orgMintedUUID}`
        let relevantPlotSelectionSurvey = null
        try {
          const relevantPlotSelectionSurveyData = await getDataForEndpoint({
            endpointPrefix: 'plot-selection-surveys',
            baseUrl,
            JWT,
            additionalQueryParams: [
              `filters[survey_metadata][org_minted_uuid][$eq]=${surveyToFix.orgMintedUUID}`,
              `filters[survey_metadata][survey_details][uuid][$eq]=${surveyToFix.surveyMetadataUuid}`,
            ],
          })
          // console.log('relevantPlotSelectionSurveyData:',relevantPlotSelectionSurveyData)
          if (relevantPlotSelectionSurveyData?.data?.length === 1) {
            //`filters` query param will return 'many', but we've given a filter that
            //should only return a single entry. If more than 1 entry is provided, then
            //something went wrong
            relevantPlotSelectionSurvey = removeEmpty(relevantPlotSelectionSurveyData.data[0])
          } else {
            console.error(`${baseErrMsgSelection}. Caused by: found ${relevantPlotSelectionSurveyData?.data?.length} entry corresponding with the filter, but only expected 1 entry`)
          }
        } catch (err) {
          console.error(`${baseErrMsgSelection}. Caused by: ${err?.message || 'unknown'}`)
        }
        // console.log('relevantPlotSelectionSurvey:', JSON.stringify(relevantPlotSelectionSurvey, null, 2))
        const metadata = cloneDeep(relevantPlotSelectionSurvey.attributes.survey_metadata)

        const baseErrMsgMeta = `Failed to create Org UUID survey metadata for Plot Selection Survey with survey UUID: ${surveyToFix.surveyMetadataUuid}`
        const postBody = {
          org_minted_uuid: cloneDeep(metadata.orgMintedUUID),
          survey_details: cloneDeep(metadata.survey_details),
          provenance: cloneDeep(metadata.provenance),
          org_opaque_user_id: cloneDeep(metadata.org_opaque_user_id),
        }
        // console.log('postBody:', JSON.stringify(postBody, null, 2))
        console.log(`Creating entry in org-uuid-survey-metadata (survey UUID=${surveyToFix.surveyMetadataUuid}, desired orgMintedUUID=${surveyToFix.orgMintedUUID})`)
        try {
          const resp = await fetch(
            orgUuidSurveyMetadataUrl,
            {
              method: 'POST',
              body: JSON.stringify({
                data: postBody,
              }),
              headers: {
                'Authorization': `Bearer ${JWT}`,
                'Content-Type': 'application/json',
              },
            },
          )

          if (resp.status !== 200) {
            console.error(`${baseErrMsgMeta}. Caused by: ${resp?.statusText}`)
          }

          const data = await resp.json()
          modifiedEntries[surveyToFix.orgMintedUUID].new_org_uuid_survey_metadata_id = data.data.id
        } catch (err) {
          console.error(`${baseErrMsgMeta}. Caused by: ${err?.message || 'unknown'}`)
        }
      }
    }

    console.log(`Summary of modified data: ${JSON.stringify(modifiedEntries, null, 2)}`)
  }
}

function loadAndFormatFixedDataFile({ path }) {
  const rawFixedData = fs.readFileSync(path, 'utf8')
  const linesAsArr = rawFixedData.split('\n').filter(l => l !== '')

  return linesAsArr.map(l => {
    const [surveyUuid, expectedOrgMintedUuid] = l.split(',')
    return {
      surveyMetadataUuid: surveyUuid,
      orgMintedUUID: expectedOrgMintedUuid,
    }
  })
}

//https://stackoverflow.com/a/38340730
function removeEmpty(obj) {
  return Object.fromEntries(
    Object.entries(obj)
      .filter(([_, v]) => v != null)
      .map(([k, v]) => [k, v === Object(v) ? removeEmpty(v) : v])
  );
}
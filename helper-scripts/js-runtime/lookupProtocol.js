const fetch = require('node-fetch')
const {
  parseSystemUrl,
  getDataForEndpoint,
  isPlotBasedProtocol,
} = require('./helpers.js')
const fs = require('fs')
const gcTransects = require('./gcTransects.js')

module.exports = {
  async run (system, JWT, protUuid, dumpFile = false) {
    let baseUrl = parseSystemUrl(system)

    let msg = `Requests for protocol with UUID=${protUuid} using base URL '${baseUrl}' with JWT '${JWT}'. Will${dumpFile ? '' : ' not'} dump to file`
    console.log(msg)

    const protocolDefsData = await getDataForEndpoint({
      endpointPrefix: 'protocols',
      baseUrl,
      JWT,
    })
    const protocol = protocolDefsData?.data?.find(p => p?.attributes?.identifier === protUuid)
    console.log(`Querying all data for protocol ${protocol.attributes.name}`)
    const endpointPrefix = protocol.attributes.endpointPrefix
    const isPlotBased = isPlotBasedProtocol({ protocol: protocol.attributes })

    //name of the script to call that generates additional data (e.g., KML files).
    //assumes this script generates a file for a single survey (orgMintedUUID)
    let generateAdditionalDataScript = null
    //fauna ground counts transects
    if (protocol.attributes.identifier === 'a76dac21-94f4-4851-af91-31f6dd00750f') {
      console.log(`Protocol is ${protocol.attributes.name} - will also create KML of data`)
      generateAdditionalDataScript = 'gcTransects'
    }

    const surveys = await getDataForEndpoint({
      endpointPrefix: endpointPrefix.replace('/', ''),
      baseUrl: baseUrl,
      JWT: JWT,
    })

    const allPlots = new Set()
    for (const survey of surveys?.data || []) {
      const currOrgMintedUuid = survey?.attributes?.survey_metadata?.orgMintedUUID
      const surveyTime = survey?.attributes?.survey_metadata?.survey_details?.time?.replaceAll(':', '')

      if (isPlotBased) {
        const currSurveyPlotLabel = survey?.attributes?.plot_visit?.data?.attributes?.plot_layout?.data?.attributes?.plot_selection?.data?.attributes?.plot_label
        if (!currSurveyPlotLabel) {
          console.warn(`Unable to get plot label for survey with orgMintedUuid=${currOrgMintedUuid}`)
        }
        allPlots.add(currSurveyPlotLabel)
      }
      

      let data = null
      try {
        const dataApiResp = await fetch(
          `${baseUrl}/protocols/reverse-lookup`,
          {
            method: 'POST',
            body: JSON.stringify({
              org_minted_uuid: currOrgMintedUuid,
            }),
            headers: {
              'Authorization': `Bearer ${JWT}`,
              'Content-Type': 'application/json',
            },
          },
        )

        if (dataApiResp.status !== 200) {
          console.error(`Failed to lookup data for orgMintedUuid=${currOrgMintedUuid}. Caused by: ${dataApiResp?.statusText}`)
          continue
        }

        data = await dataApiResp.json()
      } catch (err) {
        console.log(err)
        continue
      }
  
      
      // console.log(`data for ${currOrgMintedUuid}: ${JSON.stringify(data)}`)

      if (dumpFile) {
        try {
          if (!fs.existsSync('./protocol_dumps')) {
            fs.mkdirSync('./protocol_dumps')
          }
          fs.writeFileSync(`./protocol_dumps/${surveyTime}_${currOrgMintedUuid}.json`, JSON.stringify(data, null, 2))
        } catch (err) {
          console.error(err)
        }
      }

      if (generateAdditionalDataScript !== null) {
        console.log(`Calling script ${generateAdditionalDataScript} to generate addtional data for survey with orgMintedUUID=${currOrgMintedUuid}`)
        await gcTransects.run(
          system,
          JWT,
          currOrgMintedUuid,
        )
      }
    }

    if (isPlotBased) {
      console.log('All plots:', JSON.stringify(Array.from(allPlots).sort(), null, 2))
    }
  }
}
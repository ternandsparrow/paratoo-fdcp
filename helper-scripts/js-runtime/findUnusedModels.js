const fs = require('fs')

const CORE_PATH_API = '../../paratoo-core/src/api'
const CORE_PATH_COMPONENTS = '../../paratoo-core/src/components'

//NOTE will create false-positives when there are nested components (doesn't follow the targets)
module.exports = {
  run() {
    const allModels = []
    fs.readdirSync(CORE_PATH_API).forEach((file) => {
      allModels.push(file)
    })
    // console.log(`allModels (count=${allModels.length}):`, allModels)
    console.log(`Checking ${allModels.length} models for any that are not used in other models or in a protocol workflow`)

    const modelIsUsed = {}
    for (const model of allModels) {
      modelIsUsed[model] = false
    }

    const errOpening = []
    for (const model of allModels) {
      const schema = getModelSchema(model)
      if (schema === false) {
        if (!errOpening.includes(model)) {
          errOpening.push(model)
        }
        continue
      }

      if (model === 'protocol') {
        for (const prot of schema.initialData) {
          for (const workflowStep of prot[7]) {
            // console.log('workflowStep:', workflowStep)
            for (const modelToCheckFor of allModels) {
              if (workflowStep.modelName === modelToCheckFor) {
                modelIsUsed[modelToCheckFor] = true
              }
            }
          }
        }
      } else {
        for (const modelToCheckFor of allModels) {
          if (JSON.stringify(schema).includes(`"target":"api::${modelToCheckFor}.${modelToCheckFor}`)) {
            //do the `if` check as it might already be set to `true` and don't want to
            //squash it
            modelIsUsed[modelToCheckFor] = true
          }
          const modelToCheckForSchema = getModelSchema(modelToCheckFor)
          if (JSON.stringify(modelToCheckForSchema).includes(`"target":"api::${model}.${model}`)) {
            modelIsUsed[model] = true
          }
        }
      }
    }
    // console.log('modelIsUsed:', modelIsUsed)

    const allComponents = []
    const componentIsUsed = {}
    fs.readdirSync(CORE_PATH_COMPONENTS).forEach(componentFolder => {
      fs.readdirSync(`${CORE_PATH_COMPONENTS}/${componentFolder}`).forEach(componentFile => {
        allComponents.push({
          [componentFolder]: componentFile,
        })
        componentIsUsed[`${componentFolder}.${componentFile}`] = false
      })
    })
    // console.log('allComponents:', allComponents)


    console.log(`Checking ${allComponents.length} components for any that are not used in a model`)
    for (const component of allComponents) {
      // console.log('component:', component)
      const componentFolder = Object.keys(component)[0]
      const componentFile = Object.values(component)[0]
      // console.log(`componentFolder=${componentFolder}. componentFile=${componentFile}`)
      const schema = getComponentSchema(componentFolder, componentFile)
      if (schema === false) {
        if (!errOpening.includes(`${componentFolder}.${componentFile}`)) {
          errOpening.push(`${componentFolder}.${componentFile}`)
        }
      } else {
        for (const modelToCheckIfUsedComponent of allModels) {
          const schema = getModelSchema(modelToCheckIfUsedComponent)
          if (
            schema !== false &&
            JSON.stringify(schema).includes(`"component":"${componentFolder}.${componentFile.replace('.json', '')}"`) &&
            //a component might be used in a model, but that model might not be used
            modelIsUsed[modelToCheckIfUsedComponent]
          ) {
            componentIsUsed[`${componentFolder}.${componentFile}`] = true
          }
        }
      }
    }
    // console.log('componentIsUsed:', componentIsUsed)

    console.log(`Checking for models that are used in components (they're sometimes not used in models)`)
    //check if a model is used in a component - right now it might be marked as not used
    //(as we've only checked models against other models)
    for (const component of allComponents) {
      const componentFolder = Object.keys(component)[0]
      const componentFile = Object.values(component)[0]
      if (componentIsUsed[`${componentFolder}.${componentFile}`]) {
        // console.log('used component ', `${componentFolder}.${componentFile}`)
        const componentSchema = getComponentSchema(componentFolder, componentFile)
        for (const model of allModels) {
          if (JSON.stringify(componentSchema).includes(`"target":"api::${model}.${model}`)) {
            modelIsUsed[model] = true
          }
        }
      }
    }

    console.log('Done...')
    console.log('Could not open:', JSON.stringify(errOpening, null, 2))

    const unusedModels = getUnusedModels(modelIsUsed)
    console.log(`Unused models (count=${unusedModels.length}):`, JSON.stringify(unusedModels, null, 2))

    const unusedComponents = getUnusedModels(componentIsUsed)
    console.log(`Unused components (count=${unusedComponents.length}):`, JSON.stringify(unusedComponents, null, 2))
  },
}

function getModelSchema(model) {
  const path = `${CORE_PATH_API}/${model}/content-types/${model}/schema.json`
  let rawFileContents = null
  try {
    rawFileContents = fs.readFileSync(path)
  } catch {
    return false
  }
  const schema = JSON.parse(rawFileContents)
  return schema
}

function getComponentSchema(componentFolder, componentFile) {
  const path = `${CORE_PATH_COMPONENTS}/${componentFolder}/${componentFile}`
  let rawFileContents = null
  try {
    rawFileContents = fs.readFileSync(path)
  } catch {
    return false
  }
  const schema = JSON.parse(rawFileContents)
  return schema
}

function getUnusedModels(modelIsUsed) {
  //these models aren't 'used' by models or components but serve other purposes
  const exceptions = [
    'protocol',
    'cron-jobs-status',
    'debug-client-state-dump',
    'expose-documentation',
    'paratoo-helper',
    'protocol',
  ]
  const unusedModels = []
  for (const [model, isUsed] of Object.entries(modelIsUsed)) {
    if (!isUsed && !exceptions.includes(model)) {
      unusedModels.push(model)
    }
  }
  return unusedModels.sort()
}

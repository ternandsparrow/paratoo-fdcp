const kml = require('gtran-kml')
const {
  parseSystemUrl,
  reverseLookup,
} = require('./helpers.js')
const fs = require('fs')

//the attributes we want to be added as `properties` in the GeoJSON
const DEPLOYMENT_POINT_PROPERTIES = [
  'camera_trap_point_id',
  'deployment_start_date',
  'deployment_id',
  'camera_trap_number',
  {
    camera_location: [
      'lat',
      'lng',
    ],
  },
]

module.exports = {
  async run (system, JWT, orgMintedUUID) {
    let baseUrl = parseSystemUrl(system)

    let msg = `Requests using base URL '${baseUrl}' with JWT '${JWT}'. Will lookup CT Deployment with orgMintedUUID=${orgMintedUUID}`
    console.log(msg)

    const lookupData = await reverseLookup({
      baseUrl,
      JWT,
      orgMintedUUID,
    })

    if (lookupData?.survey_metadata?.survey_details?.protocol_id !== 'ad088dbe-02b2-472a-901f-bd081e590bcf') {
      throw new Error(`Provided an orgMintedUUID (${orgMintedUUID}) for a collection that is not Camera Trap Deployment protocol`)
    }

    const geoJsonForExport = {
      type: 'FeatureCollection',
      features: []
    }

    let csvStr = 'name,latitude,longitude'

    for (const point of lookupData?.collections?.['camera-trap-deployment-point'] || []) {
      console.log(`deployment ID = ${point.deployment_id}`)

      csvStr += `\n${point.camera_trap_point_id},${point.camera_location.lat},${point.camera_location.lng}`

      const additionalProperties = {}
      for (const field of DEPLOYMENT_POINT_PROPERTIES) {
        if (typeof field === 'object') {
          //essentially flattens an object (with no additional nesting)
          const currField = Object.keys(field)[0]
          for (const nestedField of field[currField]) {
            const nestedFlatKey = `${currField}.${nestedField}`
            if (!additionalProperties?.[nestedFlatKey]) {
              Object.assign(additionalProperties, {
                [nestedFlatKey]: null,
              })
            }
            additionalProperties[nestedFlatKey] = point[currField][nestedField]
          }
        } else {
          if (!additionalProperties?.[field]) {
            Object.assign(additionalProperties, {
              [field]: null,
            })
          }

          additionalProperties[field] = point[field]
        }
      }

      geoJsonForExport.features.push({
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [
            point.camera_location.lng,
            point.camera_location.lat,
          ],
        },
        properties: {
          //the `name` property allows the point to have a short name, which is useful
          //when trying to get a high-level view in the KML imported to Google Earth
          name: point.deployment_id,
          ...additionalProperties,
        },
      })
    }

    console.log('Converting GeoJSON to KML:', JSON.stringify(geoJsonForExport, null, 2))

    const surveyLabel = lookupData?.collections?.['camera-trap-deployment-survey']?.survey_label.replaceAll('/', 'slash')
    const fn = `./ct_deployment_${surveyLabel}.kml`
    console.log(`Outputting KML file of deployments to: ${fn}`)
    await kml.fromGeoJson(geoJsonForExport, fn, {
      documentName: surveyLabel,
    })
    
    fs.writeFileSync(`./ct_deployment_${surveyLabel}.csv`, csvStr)
  }
}
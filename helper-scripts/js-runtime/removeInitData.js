const {
  parseSystemUrl,
  loadSchema,
  getDataForEndpoint,
} = require('./helpers.js')
const fs = require('fs')
const {
  zipObject,
} = require('lodash')

const CORE_PATH_API = '../../paratoo-core/src/api'
const SKIP_MODELS = [
  'protocol',
  'expose-documentation',
  'org-uuid-survey-metadata',
  'paratoo-helper',
  'reserved-plot-label',
]

module.exports = {
  async run(system, JWT, createdAtThresholdStr, deleteData = false) {
    let baseUrl = parseSystemUrl(system)

    //invalid dates return NaN from `Date.parse()`
    if (Number.isNaN(Date.parse(createdAtThresholdStr))) {
      throw new Error(`Provided date string '${createdAtThresholdStr}' is not a valid date`)
    }

    const createdAtThreshold = new Date(createdAtThresholdStr)
    
    let msg = `Requests using base URL '${baseUrl}' with JWT '${JWT}'. Will detect date created before ${createdAtThreshold}`
    if (deleteData) {
      msg += '. Will delete data that match the threshold'
    }
    console.log(msg)

    const allModels = []
    fs.readdirSync(CORE_PATH_API).forEach((file) => {
      if (!file.startsWith('lut')) {
        allModels.push(file)
      }
    })

    const deleteCandidates = {} //key is model, value is array of IDs
    const deleteEndpointsPostfix = {}  //key is model, value is plural name
    for (const model of allModels) {
      if (SKIP_MODELS.includes(model)) continue
      console.log(`Processing model ${model}`)

      if (!deleteCandidates?.[model]) {
        Object.assign(deleteCandidates, {
          [model]: []
        })
      }

      let schema
      try {
        schema = loadSchema({ path: model })
      } catch {
        console.error(`Failed to get schema for model=${model}`)
        continue
      }
      const initData = schema?.initialData || []
      
      if (initData?.length > 1) {
        // console.log('initData:', JSON.stringify(initData, null, 2))

        const existingApiData = await getDataForEndpoint({
          endpointPrefix: `${schema.info.pluralName}`,
          baseUrl,
          JWT,
          populate: '*',
          additionalQueryParams: [
            'sort=id',
          ],
        })
        // console.log('existingApiData:', JSON.stringify(existingApiData?.data, null, 2))

        const colNames = Object.keys(schema.attributes)
        const parsedInitData = initData.map((e) => zipObject(colNames, e))
        // console.log('parsedInitData:', JSON.stringify(parsedInitData, null, 2))

        for (const [index, initD] of parsedInitData.entries()) {
          const currApiData = existingApiData?.data[index]
          if (
            currApiData &&
            currApiData?.id &&
            currApiData.id === (index+1)
          ) {
            //`Date.valueOf` will give time since epoch of the `Date` object, which is
            //easier to compare the dates
            if (new Date(currApiData.attributes.createdAt).valueOf() < createdAtThreshold.valueOf()) {
              deleteCandidates[model].push(currApiData.id)
              deleteEndpointsPostfix[model] = schema.info.pluralName
              console.log(`Data for model ${model} with ID=${currApiData.id} that was created at ${currApiData.attributes.createdAt} is a candidate for deletion`)
            } else {
              //even if we get here, it's possible that it IS init data, but was added
              //after the threshold date, so we need to manually validate if that is the
              //case and then manually delete it
              console.warn(`Skipping adding candidate of init data for API data model ${model} with ID=${currApiData.id} that was created at: ${currApiData.attributes.createdAt}`)
            }
          }
        }

        if (initData.length !== deleteCandidates[model].length) {
          console.warn(`Model ${model} has ${deleteCandidates[model].length} candidates to delete, but ${initData.length} actual init data entries`)
        }
      }

      if (deleteCandidates[model].length === 0) {
        delete deleteCandidates[model]
      }
    }

    console.log(`Candidates for deletion: ${JSON.stringify(deleteCandidates, null, 2)}`)

    if (deleteData) {
      for (const [model, ids] of Object.entries(deleteCandidates)) {
        if (ids.length > 0) {
          console.log(`Processing ${ids.length} deletion candidates for model ${model}`)
          for (const id of ids) {
            console.log(`Deleting init data for API data model ${model} with ID=${id}`)
            const baseErrMsg = `Failed to delete init data for model ${model} with ID=${id}`
            try {
              const resp = await fetch(
                `${baseUrl}/${deleteEndpointsPostfix[model]}/${id}`,
                {
                  method: 'DELETE',
                  headers: {
                    'Authorization': `Bearer ${JWT}`,
                    'Content-Type': 'application/json',
                  },
                },
              )
              if (resp.status !== 200) {
                console.error(`${baseErrMsg}. Caused by: ${resp?.statusText}`)
              }
            } catch (err) {
              console.error(`${baseErrMsg}. Caused by: ${err?.message || 'unknown'}`)
            }
          }
        }
      }
    }
  }
}


/*

const protocolDefsData = await getDataForEndpoint({
      endpointPrefix: 'protocols',
      baseUrl,
      JWT,
    })
    const skippedProtocols = []
    for (const protocol of protocolDefsData?.data || []) {
      console.log(`==================\nProcessing protocol ${protocol.attributes.name}`)
      let endpointPrefix = protocol.attributes.endpointPrefix
      if (protocol.attributes.identifier === 'a9cb9e38-690f-41c9-8151-06108caf539d') {
        //TODO plot selection will be converted to bulk so can remove this special case
        endpointPrefix = '/plot-selection-surveys'
      }

      //get the schema so we can load init data and data attributes
      let schema
      try {
        schema = loadSchema({ path: endpointPrefix })
      } catch {
        console.error(`Failed to get schema. Skipping protocol`)
        skippedProtocols.push({
          protocolName: protocol.attributes.name,
          reason: 'Failed to get init data',
        })
        continue
      }

      const initData = schema?.initialData || []
      const endpoint = endpointPrefix.replace('/', '')
      const surveyData = await getDataForEndpoint({
        endpointPrefix: endpoint,
        baseUrl,
        JWT,
      })

      for (const survey of surveyData?.data || []) {
        const surveyIsInitData = checkSurveyIsInitData({
          initData,
          survey,
        })
        if (surveyIsInitData) {
          console.log(`Survey for ${protocol.attributes.name} with ID=${survey.id} is init data. Will attempt to delete`)

          const baseErrMsg = `Failed to delete survey with ID=${survey.id} for protocol ${protocol.attributes.name}`
          try {
            const resp = await fetch(
              `${baseUrl}/${endpoint}/${survey.id}`,
              {
                method: 'DELETE',
                headers: {
                  'Authorization': `Bearer ${JWT}`,
                  'Content-Type': 'application/json',
                },
              },
            )
            if (resp.status !== 200) {
              console.error(`${baseErrMsg}. Caused by: ${resp?.statusText}`)
            }
          } catch (err) {
            console.error(`${baseErrMsg}. Caused by: ${err?.message || 'unknown'}`)
          }
        }
      }
    }

 */
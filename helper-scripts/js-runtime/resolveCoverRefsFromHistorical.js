const fs = require('fs')
const {
  cloneDeep,
} = require('lodash')

module.exports = {
  run(dataManagerStatePath) {
    const dataManagerState = cloneDeep(JSON.parse(fs.readFileSync(dataManagerStatePath, 'utf8')))

    const fixedDataManagerState = cloneDeep(dataManagerState)
    //don't want the fixed state to include this (can be very large)
    delete fixedDataManagerState.historical_responses
    delete fixedDataManagerState.historical_submitResponses

    const voucherTypes = ['full', 'lite']

    for (const [queueItemIndex, queueItem] of dataManagerState.publicationQueue.entries()) {
      for (const [coverPointIndex, coverPoint] of queueItem.collection['cover-point-intercept-point'].entries()) {
        for (const [interceptIndex, intercept] of coverPoint?.species_intercepts?.entries() || [[],[]]) {
          for (const voucherType of voucherTypes) {
            if (
              intercept?.[`floristics_voucher_${voucherType}`] &&
              intercept[`floristics_voucher_${voucherType}`]?.temp_offline_id
            ) {
              console.log(`point ${coverPoint.point_number} for ${coverPoint.cover_transect_start_point} has voucher with temp ID ref: ${JSON.stringify(intercept[`floristics_voucher_${voucherType}`])}`)

              const resolvedFullVoucherObj = findVoucher({
                historicalSubmitResponses: dataManagerState.historical_submitResponses,
                voucherType,
                voucherTempId: intercept[`floristics_voucher_${voucherType}`].temp_offline_id
              })

              if (resolvedFullVoucherObj) {
                console.log(`resolved to ID (for field name '${resolvedFullVoucherObj.field_name}' and barcode '${resolvedFullVoucherObj.voucher_barcode}'): ${resolvedFullVoucherObj.id}`)

                // console.log(fixedDataManagerState.publicationQueue[queueItemIndex])

                fixedDataManagerState.publicationQueue[queueItemIndex].collection['cover-point-intercept-point'][coverPointIndex].species_intercepts[interceptIndex][`floristics_voucher_${voucherType}`] = resolvedFullVoucherObj.id

                //also apply to the queue backup
                fixedDataManagerState.publicationQueueBackup[queueItemIndex].collection['cover-point-intercept-point'][coverPointIndex].species_intercepts[interceptIndex][`floristics_voucher_${voucherType}`] = resolvedFullVoucherObj.id
              }
            }
          }
        }
      }
    }

    const outputFileName = `${dataManagerStatePath.replaceAll('.json', '')}_fixed.json`
    console.log(`Fixed data will be written to: ${outputFileName}`)
    fs.writeFileSync(
      outputFileName,
      JSON.stringify(fixedDataManagerState),
    )
  }
}

function findVoucher({ historicalSubmitResponses, voucherType, voucherTempId }) {
  for (const submitResponse of historicalSubmitResponses) {
    if (
      submitResponse?.data?.collection?.[`floristics-veg-voucher-${voucherType}`]?.length > 0

    ) {
      for (const submitVoucher of submitResponse?.data?.collection?.[`floristics-veg-voucher-${voucherType}`]) {
        if (submitVoucher?.temp_offline_id === voucherTempId) {
          // console.log(`RESOLVED ${voucherTempId}: ${submitVoucher.id}`)
          return submitVoucher
        }
      }
    }
  }

  return null
}
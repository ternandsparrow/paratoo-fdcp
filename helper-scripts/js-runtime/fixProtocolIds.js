const {
  parseSystemUrl,
  getDataForEndpoint,
  protocolHasMultipleVariants,
  checkSurveyProtocolId,
  isPlotBasedProtocol,
} = require('./helpers.js')
const {
  protocolVariantMapper,
  protocolVariantFieldNameMapper,
} = require('./constants')
const {
  cloneDeep,
} = require('lodash')

const onlyProtocols = [
  // '37a3b018-3779-4c4f-bfb3-d38eb53a2568',
  // '93e65339-4bce-4ca1-a323-78977865ef93',
  
  // '8c47b1f8-fc58-4510-a138-e5592edd2dbc',
  // '58f2b4a6-6ce1-4364-9bae-f96fc3f86958',
  // '36e9d224-a51f-47ea-9442-865e80144311',

  // 'e15db26f-55de-4459-841b-d7ef87dea5cd',
  // 'bbd550c0-04c5-4a8c-ae39-cc748e920fd4',
  // 'b92005b0-f418-4208-8671-58993089f587',
  // 'f01e0673-a29d-48bb-b6ce-cf1c0f0de345',

  'a9cb9e38-690f-41c9-8151-06108caf539d',
]

//excludes the actual Fire Survey protocol
const coverAndFireProtocols = [
  '8c47b1f8-fc58-4510-a138-e5592edd2dbc',
  '58f2b4a6-6ce1-4364-9bae-f96fc3f86958',
]

module.exports = {
  async run(system, JWT, updateData = false) {
    let baseUrl = parseSystemUrl(system)
    let msg = `Requests using base URL '${baseUrl}' with JWT '${JWT}'`
    if (updateData) {
      msg += '. Will update/fix data'
    }
    console.log(msg)

    const protocolDefsData = await getDataForEndpoint({
      endpointPrefix: 'protocols',
      baseUrl,
      JWT,
    })
    const surveyMetadataSummaryData = await getDataForEndpoint({
      endpointPrefix: 'org-uuid-survey-metadatas',
      baseUrl,
      JWT,
    })

    const fireSurveyProtocolEntry = protocolDefsData?.data.find(
      p => p.attributes.identifier === '36e9d224-a51f-47ea-9442-865e80144311'
    )
    //may need this when handling Cover+Fire special case
    const fireData = await getDataForEndpoint({
      endpointPrefix: fireSurveyProtocolEntry.attributes.endpointPrefix.replace('/', ''),
      baseUrl,
      JWT,
    })
    //just get Cover Enhanced (as both variants share same survey model)
    const coverSurveyProtocolEntry = protocolDefsData?.data.find(
      p => p.attributes.identifier === '93e65339-4bce-4ca1-a323-78977865ef93'
    )
    //may need this when handling Cover+Fire special case
    const coverData = await getDataForEndpoint({
      endpointPrefix: coverSurveyProtocolEntry.attributes.endpointPrefix.replace('/', ''),
      baseUrl,
      JWT,
    })

    const updateCandidates = {} //key is expected protocol UUID
    
    for (const protocol of protocolDefsData?.data || []) {
      //TODO remove - for debugging
      // if (!onlyProtocols.includes(protocol.attributes.identifier)) continue


      
      console.log(`Processing protocol ${protocol.attributes.name}`)
      const isPlotBased = isPlotBasedProtocol({ protocol: protocol.attributes })

      //detect if this protocol has special cases (multiple variants, Cover+Fire edge
      //cases, etc.)
      const endpointHasMultipleProtocolVariants = protocolHasMultipleVariants({
        protocolDefsData,
        currProtocol: protocol,
      })
      const isCoverAndFire = coverAndFireProtocols.includes(protocol.attributes.identifier)
      const isFireProtocol = protocol.attributes.identifier === fireSurveyProtocolEntry.attributes.identifier

      if (!updateCandidates?.[protocol.attributes.identifier]) {
        Object.assign(updateCandidates, {
          [protocol.attributes.identifier]: []
        })
      }
      let surveyReqQueryParams = []
      if (endpointHasMultipleProtocolVariants) {
        surveyReqQueryParams.push(
          `filters[${protocolVariantFieldNameMapper[protocol.attributes.identifier]}][$eq]=${protocolVariantMapper[protocol.attributes.identifier]}`
        )
      }
      const surveyData = await getDataForEndpoint({
        endpointPrefix: protocol.attributes.endpointPrefix.replace('/', ''),
        baseUrl,
        JWT,
        additionalQueryParams: surveyReqQueryParams,
      })

      for (const survey of surveyData?.data || []) {
        const surveyCandidateToPush = checkSurveyProtocolId({
          isPlotBased,
          protocolDefsData,
          protocol,
          survey,
          isCoverAndFire,
          isFireProtocol,
          fireData,
          coverData,
        })
        if (surveyCandidateToPush === 'continue') continue

        if (surveyCandidateToPush) {
          updateCandidates[protocol.attributes.identifier].push({
            model_type: 'survey',
            ...surveyCandidateToPush,
          })
        }

        const foundOrgUUIDSurveyMetadata = surveyMetadataSummaryData?.data?.find(
          s => s?.attributes?.survey_details?.uuid === survey?.attributes?.survey_metadata?.survey_details?.uuid
        )
        if(!foundOrgUUIDSurveyMetadata) {
          console.warn(`Unable to check protocol ID in org-uuid-survey-metadata for Survey with ID=${survey.id} and UUID=${survey?.attributes?.survey_metadata?.survey_details?.uuid} due to missing entry`)
        } else {
          //need to parse the org-uuid-survey-metadata to look more like a survey's
          //`survey_metadata`, as we're reusing a helper that expect survey data
          const parsedOrgUuidSurveyMetadata = {
            id: foundOrgUUIDSurveyMetadata.id,
            attributes: {
              survey_metadata: {
                survey_details: foundOrgUUIDSurveyMetadata.attributes.survey_details,
                provenance: foundOrgUUIDSurveyMetadata.attributes.provenance,
                org_opaque_user_id: foundOrgUUIDSurveyMetadata.attributes.org_opaque_user_id,
                //note the different naming (camelCase vs. snake_case)
                orgMintedUUID: foundOrgUUIDSurveyMetadata.attributes.org_minted_uuid,
              },
              plot_visit: survey?.attributes?.plot_visit,
            },
          }

          const orgUuidSurveyMetadataCandidateToPush = checkSurveyProtocolId({
            isPlotBased,
            protocolDefsData,
            protocol,
            survey: parsedOrgUuidSurveyMetadata,
            isCoverAndFire,
            isFireProtocol,
            fireData,
            coverData,
          })

          if (orgUuidSurveyMetadataCandidateToPush) {
            updateCandidates[protocol.attributes.identifier].push({
              model_type: 'org-uuid-survey-metadata',
              ...orgUuidSurveyMetadataCandidateToPush,
            })
          }
        }
      }

      if (updateCandidates[protocol.attributes.identifier].length === 0) {
        //didn't find any issues for this protocol, so remove from summary
        delete updateCandidates[protocol.attributes.identifier]
      }
    }

    const parsedUpdateCandidates = {}
    for (const [protocolUuid, arr] of Object.entries(updateCandidates)) {
      const parsedProtocolName = protocolDefsData?.data.find(
        p => p.attributes.identifier === protocolUuid
      )?.attributes?.name
      parsedUpdateCandidates[`${parsedProtocolName} (${protocolUuid})`] = cloneDeep(arr)
    }
    console.log(`Candidates for update: ${JSON.stringify(parsedUpdateCandidates, null, 2)}`)
  }
}
const csvtojsonV2 = require('csvtojson')
const turf = require('@turf/turf')

module.exports = {
  async run (file1, file2) {
    const parsedFile1 = await parseCsvToJson({
      filePath: file1,
    })

    const parsedFile2 = await parseCsvToJson({
      filePath: file2,
    })

    for (const file1Row of parsedFile1) {
      const file1RowName = file1Row.name
      const relevantFile2Row = parsedFile2.find(o => o.name === file1RowName)

      const file1RowPoint = turf.point([
        file1Row.longitude,
        file1Row.latitude,
      ])
      const file2RowPoint = turf.point([
        relevantFile2Row.longitude,
        relevantFile2Row.latitude,
      ])
      const distance = turf.distance(
        file1RowPoint,
        file2RowPoint,
        {
          units: 'meters',
        },
      )
      console.log(`Points for ${file1RowName} are separated by ${distance}m (Latitude diff=${Math.abs(relevantFile2Row.latitude - file1Row.latitude).toFixed(4)}, longitude diff=${Math.abs(relevantFile2Row.longitude - file1Row.longitude).toFixed(4)})`)
    }
  }
}

const parseCsvToJson = async function({ filePath }) {
  console.log(`Parsing file at path ${filePath}`)
  const currFileAsJson = await csvtojsonV2().fromFile(filePath)
  const sorted = currFileAsJson.sort(
    (a, b) => a.name > b.name ? 1 : -1
  )

  return sorted
}
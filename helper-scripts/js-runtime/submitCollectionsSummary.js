const fetch = require('node-fetch')
const {
  parseSystemUrl,
  getDataForEndpoint,
} = require('./helpers.js')

/**
 * TODO -
 * 
 * - no plot selections (until we convert to bulk)
 * - plot layout & visit protocol doesn't differentiate between 'new' and 'existing'
 * - other systems (base URLs): paratoo, NESP, NSMP, etc.
 */

module.exports = {
  async run(system, JWT) {
    let baseUrl = parseSystemUrl(system)

    let msg = `Requests using base URL '${baseUrl}' with JWT '${JWT}'`
    console.log(msg)

    const plotDefProtocols = [
      //plot selection
      'a9cb9e38-690f-41c9-8151-06108caf539d',
      //plot layout and visit
      'd7179862-1be3-49fc-8ec9-2e219c6f3854',
    ]

    const surveyMetadataSummaryResp = await fetch(
      `${baseUrl}/org-uuid-survey-metadatas?populate=*`,
      {
        headers: {
          'Authorization': `Bearer ${JWT}`,
        },
      },
    )
    const surveyMetadataSummaryData = await surveyMetadataSummaryResp.json()
    console.log(`Raw metadata summary from API: ${JSON.stringify(surveyMetadataSummaryData, null, 2)}`)

    if (surveyMetadataSummaryData?.data?.length === 0) {
      console.log('No submit data')
      return
    }

    const protocolDefsResp = await fetch(
      `${baseUrl}/protocols?populate=*`,
      {
        headers: {
          'Authorization': `Bearer ${JWT}`,
        },
      },
    )
    const protocolDefsData = await protocolDefsResp.json()

    const collectionsSummary = {}

    for (const collectionMeta of surveyMetadataSummaryData.data) {
      const protUuid = collectionMeta.attributes.survey_details.protocol_id

      if (plotDefProtocols.includes(protUuid)) {
        //handle this after
        continue
      }

      if (!collectionsSummary[protUuid]) {
        collectionsSummary[protUuid] = 1
      } else {
        collectionsSummary[protUuid] += 1
      }
    }

    const plotSelectionData = await getDataForEndpoint({
      endpointPrefix: 'plot-selections',
      baseUrl,
      JWT,
    })

    const plotLayoutData = await getDataForEndpoint({
      endpointPrefix: 'plot-layouts',
      baseUrl,
      JWT,
    })

    const plotVisitData = await getDataForEndpoint({
      endpointPrefix: 'plot-visits',
      baseUrl,
      JWT,
    })

    collectionsSummary['Plot Selection and Layout'] = `${plotSelectionData?.data?.length} Planned Plots, ${plotLayoutData?.data?.length}, established across ${plotVisitData?.data?.length} visits`

    //sort object by value (# of collections for protocol): https://stackoverflow.com/a/1069840
    const collectionsSummarySorted = Object.entries(collectionsSummary)
      .sort(([a_,a],[b_,b]) => {
        if (
          a_ === 'Plot Selection and Layout' ||
          b_ === 'Plot Selection and Layout'
        ) {
          //force to the top of the list
          return -1
        }
        return b-a
      })
      .reduce((r, [k, v]) => ({ ...r, [k]: v }), {})

    const collectionsSummaryPretty = {}
    let totalSurveys = 0
    for (const [protocolUuid, count] of Object.entries(collectionsSummarySorted)) {
      if (protocolUuid !== 'Plot Selection and Layout') {
        totalSurveys += count
      }
      let resolvedProtocol = protocolUuid
      const resolved = protocolDefsData.data.find(
        p => p.attributes.identifier === protocolUuid
      )
      if (resolved) {
        resolvedProtocol = resolved
      }
      collectionsSummaryPretty[resolvedProtocol?.attributes?.name || resolvedProtocol] = count
    }

    const str = JSON.stringify(collectionsSummaryPretty, null, 1)
      //strip JSON stuff to get a nice string
      .replaceAll('"', '')
      .replaceAll(',', '')
      .replaceAll('{', '')
      .replaceAll('}', '')
    console.log(`Summary (${totalSurveys} total, minus plots/visits): ${str}`)
  }
}
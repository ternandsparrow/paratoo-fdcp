const {
  parseSystemUrl,
  getDataForEndpoint,
} = require('./helpers.js')
const kml = require('gtran-kml')
const {
  cloneDeep,
} = require('lodash')
const fs = require('fs')

const SORT_ORDER = [
  'SW',
  'W1',
  'W2',
  'W3',
  'W4',
  'W5',
  'NW',
  'N1',
  'N2',
  'N3',
  'N4',
  'N5',
  'NE',
  'E5',
  'E4',
  'E3',
  'E2',
  'E1',
  'SE',
  'S5',
  'S4',
  'S3',
  'S2',
  'S1'
]
const CORNERS = [
  'SW',
  'NW',
  'NE',
  'SE',
]

module.exports = {
  async run (system, JWT, projectUuid = null, cornersOnly = false) {
    let baseUrl = parseSystemUrl(system)

    let msg = `Requests using base URL '${baseUrl}' with JWT '${JWT}'`

    if (projectUuid) {
      msg += `. Will lookup Plot Layouts with projectUuid=${projectUuid}`
    } else {
      msg += '. No project provided, will look-up all plots'
    }
    if (cornersOnly) {
      msg += '. Will only use plot corners in GeoJSON features'
    }
    console.log(msg)

    const projQueryParams = []
    if (projectUuid) {
      projQueryParams.push(
        `filters[survey_metadata][survey_details][project_id][$eqi]=${projectUuid}`
      )
    }
    const plotDefSurveys = await getDataForEndpoint({
      endpointPrefix: 'plot-definition-surveys',
      baseUrl,
      JWT,
      additionalQueryParams: projQueryParams,
    })
    
    // console.log('plotDefSurveys:', JSON.stringify(plotDefSurveys, null, 2))

    const allLayouts = await getDataForEndpoint({
      endpointPrefix: 'plot-layouts',
      baseUrl,
      JWT,
    })
    // console.log('allLayouts:', JSON.stringify(allLayouts, null, 2))

    const layoutsForProject = []
    for (const layout of allLayouts.data) {
      // console.log(`processing layout: ${layout.attributes.plot_selection.data.attributes.plot_label}`)
      if (
        plotDefSurveys.data.some(
          o => o.attributes.plot_visit.data.attributes.plot_layout.data.id === layout.id
        )
      ) {
        // console.log(`match layout: ${layout.attributes.plot_selection.data.attributes.plot_label}`)
        layoutsForProject.push(layout)
      }
    }

    const geoJsonForExport = {
      type: 'FeatureCollection',
      features: []
    }

    for (const layout of layoutsForProject) {
      const currPlotLabel = layout.attributes.plot_selection.data.attributes.plot_label
      const mappedPoints = []
      let startPoint = null
      for (const [index, sortSymbol] of SORT_ORDER.entries()) {
        if (
          cornersOnly &&
          !CORNERS.includes(sortSymbol)
        ) {
          console.log(`skipping ${sortSymbol}`)
          continue
        }
        const relevantPoint = layout.attributes.plot_points.find(
          p => p.name.data.attributes.symbol === sortSymbol
        )
        if (!relevantPoint) {
          console.warn(`Plot point ${sortSymbol} not found for plot ${currPlotLabel}`)
          continue
        }
        if (index === 0) {
          startPoint = cloneDeep(relevantPoint)
        }
        mappedPoints.push([
          relevantPoint.lng,
          relevantPoint.lat,
        ])
      }

      mappedPoints.push([
        startPoint.lng,
        startPoint.lat,
      ])

      //25 points in the plot; minus 1 due to center being excluded; plus 1 for adding
      //back the start point (SW) to complete the polygon/polyline
      if (mappedPoints.length !== 25) {
        console.warn(`Bad number of points (${mappedPoints.length}) for plot ${currPlotLabel}`)
      }

      /* geoJsonForExport.geometries.push({
        type: 'Polygon',
        coordinates: [mappedPoints],
      }) */

      geoJsonForExport.features.push({
        type: 'Feature',
        geometry: {
          type: 'Polygon',
          coordinates: [mappedPoints],
        },
        properties: {
          name: layout.attributes.plot_selection.data.attributes.plot_label
        },
      })

      geoJsonForExport.features.push({
        type: 'Feature',
        geometry: {
          type: 'LineString',
          coordinates: mappedPoints,
        },
        properties: {
          name: layout.attributes.plot_selection.data.attributes.plot_label
        },
      })
    }

    // console.log('geoJsonForExport:', JSON.stringify(geoJsonForExport, null, 2))

    const fn = `project_plots_${projectUuid || 'all'}`
    console.log(`Outputting KML and GeoJSON files of plots to: ${fn}`)
    await kml.fromGeoJson(geoJsonForExport, `${fn}.kml`, {
      documentName: projectUuid,
    })

    fs.writeFileSync(`${fn}.geojson`, JSON.stringify(geoJsonForExport))
  },
}
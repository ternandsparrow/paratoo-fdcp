module.exports = {
  //maps the protocol's UUID to the variant expected in the survey data
  protocolVariantMapper: {
    //Photopoints uses lut-photopoints-protocol-variant
    //Photopoints - Compact Panorama
    '383fa013-c52d-4186-911b-35e9b2375653': 1,
    //Photopoints - DSLR Panorama
    '5fd206b5-25cb-4371-bd90-7b2e8801ea25': 2,
    //Photopoints - Device Panorama
    '2dbb595b-3541-46bd-b200-13db3a823b74': 3,

    //PTV uses lut-protocol-variant
    //Plant Tissue Vouchering - Standard
    'f01e0673-a29d-48bb-b6ce-cf1c0f0de345': 1,
    //Plant Tissue Vouchering - Enhanced
    'b92005b0-f418-4208-8671-58993089f587': 2,

    //Cover uses lut-protocol-variant
    //Cover - Standard
    '37a3b018-3779-4c4f-bfb3-d38eb53a2568': 1,
    //Cover - Enhanced
    '93e65339-4bce-4ca1-a323-78977865ef93': 2,

    //Cover+Fire uses lut-protocol-variant
    //Cover + Fire - Standard
    '58f2b4a6-6ce1-4364-9bae-f96fc3f86958': 1,
    //Cover + Fire - Enhanced
    '8c47b1f8-fc58-4510-a138-e5592edd2dbc': 2,

    //Grassy Weeds uses lut-nesp-weed-survey-type
    //NESP Grassy Weeds Aerial Survey - Survey Setup 
    '9a0aab22-19b8-49e9-b0fa-262b4cf58e8e': 1,
    //NESP Grassy Weeds Aerial Survey - Conduct Survey
    '75600f67-49f2-4c9a-98af-9cde2d020680': 2,
  },
  //maps the protocol to the field name that stores the relation to the variant
  protocolVariantFieldNameMapper: {
    //Photopoints uses lut-photopoints-protocol-variant
    //Photopoints - Compact Panorama
    '383fa013-c52d-4186-911b-35e9b2375653': 'photopoints_protocol_variant',
    //Photopoints - DSLR Panorama
    '5fd206b5-25cb-4371-bd90-7b2e8801ea25': 'photopoints_protocol_variant',
    //Photopoints - Device Panorama
    '2dbb595b-3541-46bd-b200-13db3a823b74': 'photopoints_protocol_variant',

    //PTV uses lut-protocol-variant
    //Plant Tissue Vouchering - Standard
    'f01e0673-a29d-48bb-b6ce-cf1c0f0de345': 'protocol_variant',
    //Plant Tissue Vouchering - Enhanced
    'b92005b0-f418-4208-8671-58993089f587': 'protocol_variant',

    //Cover uses lut-protocol-variant
    //Cover - Standard
    '37a3b018-3779-4c4f-bfb3-d38eb53a2568': 'protocol_variant',
    //Cover - Enhanced
    '93e65339-4bce-4ca1-a323-78977865ef93': 'protocol_variant',

    //Cover+Fire uses lut-protocol-variant
    //Cover + Fire - Standard
    '58f2b4a6-6ce1-4364-9bae-f96fc3f86958': 'protocol_variant',
    //Cover + Fire - Enhanced
    '8c47b1f8-fc58-4510-a138-e5592edd2dbc': 'protocol_variant',

    //Grassy Weeds uses lut-nesp-weed-survey-type
    //NESP Grassy Weeds Aerial Survey - Survey Setup 
    '9a0aab22-19b8-49e9-b0fa-262b4cf58e8e': 'survey_type',
    //NESP Grassy Weeds Aerial Survey - Conduct Survey
    '75600f67-49f2-4c9a-98af-9cde2d020680': 'survey_type',
  },
}
const {
  parseSystemUrl,
  loadSchema,
  getDataForEndpoint,
  isPlotBasedProtocol,
  loadComponentSchema,
  checkSurveyIsInitData,
  protocolHasMultipleVariants,
  checkSurveyProtocolId,
} = require('./helpers.js')
const {
  protocolVariantMapper,
  protocolVariantFieldNameMapper,
} = require('./constants')
const fs = require('fs')
const {
  isEqual,
  cloneDeep,
  difference,
} = require('lodash')
const turf = require('@turf/turf')
const geojsonArea = require('geojson-area')
const kml = require('gtran-kml')

const Ajv = require('ajv')
const ajv = new Ajv({
  strictSchema: true,
  validateFormats: true,
  allErrors: true,
})
const addFormats = require('ajv-formats')
addFormats(ajv)

const uuidRegex = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/i
ajv.addFormat('uuid', {
  type: 'string',
  validate: (p) => uuidRegex.test(p),
})

//turn this off if want provenance info in output
const IGNORE_BAD_PROVENANCE = true

const geoJsonCoordsMapperCb = (p) => [p.lng, p.lat]
const excludeCentrePlotPointFilterCb = (p => p.name.data.attributes.symbol !== 'C')

const onlyProtocols = [
  'a9cb9e38-690f-41c9-8151-06108caf539d',
  'd7179862-1be3-49fc-8ec9-2e219c6f3854',
]

//excludes the actual Fire Survey protocol
const coverAndFireProtocols = [
  '8c47b1f8-fc58-4510-a138-e5592edd2dbc',
  '58f2b4a6-6ce1-4364-9bae-f96fc3f86958',
]

//a successful submission to core should have all these fields
const metadataSchema = {
  type: 'object',
  properties: {
    survey_metadata: {
      type: 'object',
      required: [
        'survey_details',
        'provenance',
        'orgMintedUUID',
      ],
      properties: {
        orgMintedUUID: {
          type: 'string',
          format: 'uuid',
        },
        survey_details: {
          type: 'object',
          required: ['survey_model', 'time', 'uuid', 'project_id', 'protocol_id', 'protocol_version'],
          properties: {
            survey_model: {
              type: 'string',
              minLength: 1,
            },
            time: {
              type: 'string',
              // format: 'date-time'
            },
            uuid: {
              type: 'string',
              // format: 'uuid'
            },
            project_id: {
              type: 'string',
              minLength: 1,
            },
            protocol_id: {
              type: 'string',
              // format: 'uuid'
            },
            protocol_version: {
              type: 'string',
              minLength: 1,
            },
            submodule_protocol_id: {
              type: 'string'
            },
          },
        },
        provenance: {
          type: 'object',
          required: [
            'version_app',
            'version_core_documentation',
            'system_app',
            'version_org',
            'system_org',
            'version_core',
            'system_core',
          ],
          properties: {
            version_app: {
              type: 'string'
            },
            //appended by core
            version_core: {
              type: 'string'
            },
            version_core_documentation: {
              type: 'string',
            },
            //appended by org
            version_org: {
              type: 'string'
            },
            system_app: {
              type: 'string'
            },
            //appended by core
            system_core: {
              type: 'string',
            },
            //appended by org
            system_org: {
              type: 'string',
            },
          },
        },
      },
    },
  }
}

//start SW and move clockwise
const plotPointsSortOrder = [
  'SW',
  'W5',
  'W4',
  'W3',
  'W2',
  'W1',
  'NW',
  'N1',
  'N2',
  'N3',
  'N4',
  'N5',
  'NE',
  'E1',
  'E2',
  'E3',
  'E4',
  'E5',
  'SE',
  'S1',
  'S2',
  'S3',
  'S4',
  'S5',
  'C',
]

module.exports = {
  /** 
   * TODO- 
   * - Hanging data - need to figure out various cases, but as a starting example: we
   *   might have data in the Org dump for a collection that has a survey in core but
   *   nothing in the org-uuid-survey-metadata table, indicating that `/collection` was
   *   not called by core (or it was called and it failed); current build shouldn't have
   *   this, but when we didn't have transactions, this case could occur
   * - Check rest of model data relations using workflow (right now we only check
   *   survey). Has been started, but bunch of TODOs in code to handle various edge cases
   * 
   * For each protocol:
   *  1. Query survey model
   *  2. Find the matching `survey_metadata.survey_details.uuid` in the survey's
   *     `survey_metadata` from the Org data dump containing all projects' survey
   *     metadata. This ensures that `/mint-identifier` and at least part of the data in
   *     Core was created
   *  3. Check the survey metadata from core has correct schema
   *  4. Check the protocol ID in the survey metadata is correct
   *  5. Query `/org-uuid-survey-metadatas` to find a match. This checks that `
   *     collection` was called by Org
   *  6. Check for duplicate Plot Selections and the Layouts that use them
   *  7. Check the reserved plots' relation to the created Plot Selection is correct
   *  8. Check the Plot Layout and Visit data is correct (relations, plot sizes/points)
   * 
   * @param {*} system 
   * @param {*} JWT 
   * @param {*} [orgDataPath]
   */
  async run(system, JWT, orgDataPath=null) {
    let baseUrl = parseSystemUrl(system)

    let msg = `Requests using base URL '${baseUrl}' with JWT '${JWT}'`
    if (orgDataPath !== null) {
      msg += `. Checking against Org data dump at path ${orgDataPath}`
    }
    console.log(msg)

    let orgData = null
    if (orgDataPath !== null) {
      orgData = getAndValidateOrgData({
        orgDataPath,
      })
    } else {
      console.log('Will skip checks for correct project in core\'s survey metadata (no --org_data_path param provided)')
    }

    const protocolDefsData = await getDataForEndpoint({
      endpointPrefix: 'protocols',
      baseUrl,
      JWT,
    })
    // console.log('protocolDefsData:', JSON.stringify(protocolDefsData, null, 2))

    const surveyMetadataSummaryData = await getDataForEndpoint({
      endpointPrefix: 'org-uuid-survey-metadatas',
      baseUrl,
      JWT,
    })
    // console.log('surveyMetadataSummaryData:', JSON.stringify(surveyMetadataSummaryData, null, 2))

    const fireSurveyProtocolEntry = protocolDefsData?.data.find(
      p => p.attributes.identifier === '36e9d224-a51f-47ea-9442-865e80144311'
    )
    //may need this when handling Cover+Fire special case
    const fireData = await getDataForEndpoint({
      endpointPrefix: fireSurveyProtocolEntry.attributes.endpointPrefix.replace('/', ''),
      baseUrl,
      JWT,
    })
    //just get Cover Enhanced (as both variants share same survey model)
    const coverSurveyProtocolEntry = protocolDefsData?.data.find(
      p => p.attributes.identifier === '93e65339-4bce-4ca1-a323-78977865ef93'
    )
    //may need this when handling Cover+Fire special case
    const coverData = await getDataForEndpoint({
      endpointPrefix: coverSurveyProtocolEntry.attributes.endpointPrefix.replace('/', ''),
      baseUrl,
      JWT,
    })
    
    //check that all the survey data in core has expected survey metadata
    const skippedProtocols = []
    let totalSurveys = 0    //includes init data
    for (const protocol of protocolDefsData?.data || []) {
      // if (!onlyProtocols.includes(protocol.attributes.identifier)) continue

      console.log(`==================\nProcessing protocol ${protocol.attributes.name}`)
      let endpointPrefix = protocol.attributes.endpointPrefix
      if (protocol.attributes.identifier === 'a9cb9e38-690f-41c9-8151-06108caf539d') {
        //TODO plot selection will be converted to bulk so can remove this special case
        endpointPrefix = '/plot-selection-surveys'
      }

      //get the schema so we can load init data and data attributes
      let schema
      try {
        schema = loadSchema({ path: endpointPrefix })
      } catch {
        console.error(`Failed to get schema. Skipping protocol`)
        skippedProtocols.push({
          protocolName: protocol.attributes.name,
          reason: 'Failed to get init data',
        })
        continue
      }
      const initData = schema?.initialData || []
      const surveyAttributes = schema?.attributes || {}
      const endpointHasMultipleProtocolVariants = protocolHasMultipleVariants({
        protocolDefsData,
        currProtocol: protocol,
      })

      const isPlotBased = isPlotBasedProtocol({ protocol: protocol.attributes })
      console.log(`Protocol ${isPlotBased ? 'is' : 'is not'} plot based`)
      let surveysVisitFieldName = null
      if (isPlotBased) {
        surveysVisitFieldName = getSurveyVisitFieldName({ surveyAttributes })
        if (!surveysVisitFieldName) {
          skippedProtocols.push({
            protocolName: protocol.attributes.name,
            reason: 'Failed to get survey visit field name',
          })
          continue
        }
      }

      const isCoverAndFire = coverAndFireProtocols.includes(protocol.attributes.identifier)
      const isFireProtocol = protocol.attributes.identifier === fireSurveyProtocolEntry.attributes.identifier

      let surveyReqQueryParams = []
      if (endpointHasMultipleProtocolVariants) {
        surveyReqQueryParams.push(
          `filters[${protocolVariantFieldNameMapper[protocol.attributes.identifier]}][$eq]=${protocolVariantMapper[protocol.attributes.identifier]}`
        )
      }
      const surveyData = await getDataForEndpoint({
        endpointPrefix: endpointPrefix.replace('/', ''),
        baseUrl,
        JWT,
        additionalQueryParams: surveyReqQueryParams,
      })

      totalSurveys += surveyData?.meta?.pagination?.total
      const initDataSurveys = []
      for (const survey of surveyData?.data || []) {
        const surveyIsInitData = checkSurveyIsInitData({
          initData,
          survey,
        })
        if (surveyIsInitData) {
          initDataSurveys.push(survey)
          continue
        }

        if (orgDataPath !== null) {
          const relevantOrgProject = orgData.find(
            p => p.projectId === survey?.attributes?.survey_metadata?.survey_details?.project_id
          )
  
          if (!relevantOrgProject) {
            console.log(`No org project found for survey with UUID ${survey?.attributes?.survey_metadata?.survey_details?.uuid}.`)
          }
        }

        validateSurveyMetadata({
          isPlotBased,
          survey,
          protocol,
          surveyMetadataSummaryData,
          protocolDefsData,
          isCoverAndFire,
          isFireProtocol,
          fireData,
          coverData,
        })

        if (isPlotBased) {
          checkPlotBasedProtocolRelationToVisit({
            survey,
            surveysVisitFieldName,
          })
        }
      }

      //use a switch to handle any edge cases; non-edge-cases will be `default` block
      switch (protocol.attributes.identifier) {
        case 'a9cb9e38-690f-41c9-8151-06108caf539d':
          //plot selection
          //the 'observations' of Plot Selection are a slightly different case
          await checkPlotSelections({
            baseUrl,
            JWT,
            initDataSurveys,
          })
          break
        case 'd7179862-1be3-49fc-8ec9-2e219c6f3854':
          //plot layout and visit
          //use this case to check all layouts/visits are defined correctly
          await checkPlots({
            baseUrl,
            JWT,
          })
          break
        default:
          //TODO check if init data (will be more difficult than survey model as we don't have a known unique field; UUID)
          await checkModelDataRelations({
            baseUrl,
            JWT,
            protocol,
          })
          break
      }
    }
    console.log('Done processing all protocols')
    if (skippedProtocols.length > 0) {
      console.log('Skipped protocols:', JSON.stringify(skippedProtocols, null, 2))
    }
  }
}

function getAndValidateOrgData({ orgDataPath }) {
  const orgData = JSON.parse(fs.readFileSync(orgDataPath))
  // console.log('orgData:', JSON.stringify(orgData, null, 2))
  const topObjKeys = [
    'projectId',
    'surveyMetadata',
  ]
  const invalidProjects = []
  for (const [index, orgProject] of orgData.entries()) {
    //check top-level keys
    if (
      !isEqual(
        Object.keys(orgProject).sort(),
        topObjKeys.sort(),
      )
    ) {
      invalidProjects.push(index)
    }
  }

  let errMsg = ''
  if (invalidProjects.length > 0) {
    errMsg += `Top-level object for each array item must have keys ${topObjKeys.join(', ')}, but the following indexes do not: ${invalidProjects.join(', ')}`
  }
  if (errMsg !== '') {
    throw new Error(errMsg)
  }
  return orgData
}



function validateSurveyMetadata({
  isPlotBased,
  survey,
  protocol,
  surveyMetadataSummaryData,
  protocolDefsData,
  isCoverAndFire,
  isFireProtocol,
  fireData,
  coverData,
}) {
  //check the survey metadata schema
  const validate = ajv.compile(metadataSchema)
  let valid = validate(survey?.attributes)
  if (!valid) {
    const errs = ajv.errorsText(validate.errors)
    const errsArr = []
    if (errs) {
      errsArr.push(...errs.split(', '))
    }
    for (const err of errsArr) {
      if (
        IGNORE_BAD_PROVENANCE &&
        err.includes('data/survey_metadata/provenance')
      ) continue
      console.log(`Invalid schema for survey with UUID ${survey?.attributes?.survey_metadata?.survey_details?.uuid}: ${err}`)
    }
  }

  //check the survey metadata has correct protocol ID
  const incorrectProtocolIdCandidate = checkSurveyProtocolId({
    isPlotBased,
    protocolDefsData,
    protocol,
    survey,
    isCoverAndFire,
    isFireProtocol,
    fireData,
    coverData,
  })
  if (
    incorrectProtocolIdCandidate &&
    incorrectProtocolIdCandidate !== 'continue'
  ) {
    const candidateStr = parseWrongProtocolIdCandidateMsg({
      candidate: incorrectProtocolIdCandidate,
    })
    console.log(`Survey with ID=${incorrectProtocolIdCandidate.id} and UUID=${incorrectProtocolIdCandidate.survey_uuid} has issues with protocol ID(s): ${candidateStr}`)
  }

  //check org-uuid-survey-metadata has record
  const foundOrgUUIDSurveyMetadata = surveyMetadataSummaryData?.data?.find(
    s => s?.attributes?.survey_details?.uuid === survey?.attributes?.survey_metadata?.survey_details?.uuid
  )
  if (!foundOrgUUIDSurveyMetadata) {
    console.warn(`Unable to check protocol ID in org-uuid-survey-metadata for Survey with ID=${survey.id} and UUID=${survey?.attributes?.survey_metadata?.survey_details?.uuid} due to missing entry`)
  } else {
    //need to parse the org-uuid-survey-metadata to look more like a survey's
    //`survey_metadata`, as we're reusing a helper that expect survey data
    const parsedOrgUuidSurveyMetadata = {
      id: foundOrgUUIDSurveyMetadata.id,
      attributes: {
        survey_metadata: {
          survey_details: foundOrgUUIDSurveyMetadata.attributes.survey_details,
          provenance: foundOrgUUIDSurveyMetadata.attributes.provenance,
          org_opaque_user_id: foundOrgUUIDSurveyMetadata.attributes.org_opaque_user_id,
          //note the different naming (camelCase vs. snake_case)
          orgMintedUUID: foundOrgUUIDSurveyMetadata.attributes.org_minted_uuid,
        },
        plot_visit: survey?.attributes?.plot_visit,
      },
    }

    const orgUuidSurveyMetadataCandidate = checkSurveyProtocolId({
      isPlotBased,
      protocolDefsData,
      protocol,
      survey: parsedOrgUuidSurveyMetadata,
      isCoverAndFire,
      isFireProtocol,
      fireData,
      coverData,
    })
    if (
      orgUuidSurveyMetadataCandidate &&
      orgUuidSurveyMetadataCandidate !== 'continue'
    ) {
      const candidateStr = parseWrongProtocolIdCandidateMsg({
        candidate: orgUuidSurveyMetadataCandidate,
      })
      console.log(`org-uuid-survey-metadata entry with ID=${orgUuidSurveyMetadataCandidate.id} and UUID=${orgUuidSurveyMetadataCandidate.survey_uuid} has issues with protocol ID(s): ${candidateStr}`)
    }
  }
}

function parseWrongProtocolIdCandidateMsg({ candidate }) {
  const baseKeys = [
    'model_type',
    'id',
    'survey_uuid',
    'survey_org_minted_uuid',
  ]
  const candidateArrStr = Object.entries(candidate).reduce(
    (accum, [key, val]) => {
      if (!baseKeys.includes(key)) {
        accum.push(`${key.replaceAll('_', ' ')} is ${val}`)
      }

      return accum
    }, []
  )

  return candidateArrStr.join(', ')
}

async function checkPlots({ baseUrl, JWT }) {
  const plotLayoutData = await getDataForEndpoint({
    endpointPrefix: 'plot-layouts',
    baseUrl,
    JWT,
  })
  // console.log('plotLayoutData:', JSON.stringify(plotLayoutData, null, 2))

  //check layouts
  for (const plotPointsKey of ['plot_points', 'fauna_plot_point']) {
    let lutPlotPointsEndpoint = null
    if (plotPointsKey === 'plot_points') {
      lutPlotPointsEndpoint = 'lut-plot-points'
      console.log('Checking core plots')
    } else if (plotPointsKey === 'fauna_plot_point') {
      lutPlotPointsEndpoint = 'lut-plot-corner-and-centre'
      console.log('Checking fauna plots')
    }

    const lutPlotPointsData = await getDataForEndpoint({
      endpointPrefix: lutPlotPointsEndpoint,
      baseUrl,
      JWT,
    })
    // console.log('lutPlotPointsData:', JSON.stringify(lutPlotPointsData, null, 2))
    const lutPlotPointsSymbols = lutPlotPointsData?.data?.map(p => p?.attributes?.symbol)

    for (const plotLayout of plotLayoutData?.data || []) {
      let currLayoutIssues = []
      //check relation to plot selection
      if (
        //don't need to check again on on fauna plot
        plotPointsKey !== 'fauna_plot_point' &&
        !plotLayout?.attributes?.plot_selection?.data?.id
      ) {
        currLayoutIssues.push('no plot selection')
      }

      //check plot points
      if (
        //exclude edge case bird plots from init data
        !plotLayout?.attributes?.plot_selection?.data?.attributes?.plot_label.includes('(bird)')
      ) {
        //TODO check the layout has at least one visit (it shouldn't be possible to define a layout without an initial visit)
        //TODO check if any plots are overlapping - we do a check like this when checking *duplicate* Plot Selections, but also want to check irrespective of being duplicate
        //TODO check fauna plot is near to core plot (there's certain bounds for how far the reference point can be from the edge of the code plot that we set in the app)
        //TODO check fauna plot doesn't overlap with core plot (probs some reusable logic in app since it also does a check like that)
        //TODO dump all core/fauna plots that have issues to KML file
        if (
          plotPointsKey === 'fauna_plot_point' &&
          plotLayout?.attributes?.[plotPointsKey]?.length === 0
        ) {
          //no fauna plot to check (it's optional, unlike the core plot)
          continue
        } else {
          const pointsIssue = checkPlotPoints({
            plotLayout,
            plotPointsKey,
            lutPlotPointsSymbols,
          })
          if (pointsIssue !== '') {
            currLayoutIssues.push(pointsIssue)
          }
        }
      }
  
      if (currLayoutIssues.length > 0) {
        console.log(`Plot layout with ID=${plotLayout?.id} has issues with ${plotPointsKey}: ${currLayoutIssues.join(', ')}`)
      }
    }
  }
  
  //check visits
  console.log('Checking visits')
  const plotVisitData = await getDataForEndpoint({
    endpointPrefix: 'plot-visits',
    baseUrl,
    JWT,
  })
  for (const plotVisit of plotVisitData?.data || []) {
    let currVisitIssues = []

    if (!plotVisit?.attributes?.plot_layout?.data?.id) {
      currVisitIssues.push('no plot layout')
    }
    if (currVisitIssues.length > 0) {
      console.log(`Plot visit with ID=${plotVisit?.id} has issues: ${currVisitIssues.join(', ')}`)
    }
  }
}

function checkPlotPoints({ plotLayout, plotPointsKey, lutPlotPointsSymbols }) {
  let expectedNumOfPoints = null
  if (plotPointsKey === 'fauna_plot_point') {
    expectedNumOfPoints = 5
  } else if (plotPointsKey === 'plot_points') {
    expectedNumOfPoints = 25
  } else {
    throw new Error(`invalid plotPointsKey ${plotPointsKey}`)
  }

  let pointsIssue = ''

  //check if # of points is correct
  if (plotLayout?.attributes?.[plotPointsKey]?.length !== expectedNumOfPoints) {
    pointsIssue += `expected ${expectedNumOfPoints} points but got ${plotLayout?.attributes?.[plotPointsKey]?.length}. `
  }

  //check if all points are used from the lut-plot-points
  const currLayoutPointsSymbols = plotLayout?.attributes?.[plotPointsKey]?.map(
    p => p?.name?.data?.attributes?.symbol
  ).sort()
  // console.log('currLayoutPointsSymbols:', currLayoutPointsSymbols)
  const plotPointsDifference = difference(lutPlotPointsSymbols, currLayoutPointsSymbols)
  if (plotPointsDifference.length > 0) {
    pointsIssue += `missing: ${plotPointsDifference.join(', ')}. `
  }

  //check that all points from lut-plot-points are used once
  const duplicateSymbols = []
  for (const index in currLayoutPointsSymbols) {
    if (currLayoutPointsSymbols[index] === currLayoutPointsSymbols[index-1]) {
      duplicateSymbols.push(currLayoutPointsSymbols[index])
    }
  }
  if (duplicateSymbols.length > 0) {
    pointsIssue += `duplicate symbols: ${duplicateSymbols.join(', ')}. `
  }

  //check plot is roughly 100x100m.
  //very unlikely to hit this, as the app uses the reference point to generate a
  //reference grid, but still worth to check
  try {
    const currLayoutPoly = createPolygonFromLayout({
      layout: plotLayout,
      plotPointsKey,
    })
    const currLayoutArea = geojsonArea.geometry(currLayoutPoly.geometry)
    if (
      //approx +/- 3m in the two axis
      currLayoutArea > 10600 ||
      currLayoutArea < 9400
    ) {
      pointsIssue += `expected area 10000m^2 but got ${currLayoutArea.toFixed(2)}m^2. `
    }
  } catch (err) {
    pointsIssue += `failed to check area (likely due to other issues)`
  }

  return pointsIssue
}

function getSurveyVisitFieldName({ surveyAttributes }) {
  let surveysVisitFieldName = null
  for (const [attributeName, attributeData] of Object.entries(surveyAttributes)) {
    if (
      attributeData.type === 'relation' &&
      attributeData.target === 'api::plot-visit.plot-visit'
    ) {
      surveysVisitFieldName = attributeName
      break
    }
  }
  if (!surveysVisitFieldName) {
    let msg = `Could not get the field name for the relation to the visit for plot-based protocol. Skipping protocol`
    console.log(msg)
  }
  return surveysVisitFieldName
}

//only checking relation to visit and we have other logic to check all the plot data is correct
function checkPlotBasedProtocolRelationToVisit({ survey, surveysVisitFieldName }) {
  if (!survey?.attributes?.[surveysVisitFieldName]?.data?.id) {
    let msg = `Survey with ID=${survey?.id} does not have a relation to the visit`
    console.log(msg)
  }
}

async function checkModelDataRelations({ baseUrl, JWT, protocol }) {
  //iterate over workflow (skipping models survey by checking if has survey_metadata and
  //for plot models plot-layout and plot-visit) and check obs/child obs relations

  let surveyModel = null
  for (const workflowStep of protocol?.attributes?.workflow) {
    //don't need to check stuff for plots and surveys, so skip
    if (
      ['plot-layout', 'plot-visit'].includes(workflowStep?.modelName)
    ) continue
    const workflowStepSchema = loadSchema({
      path: `/${workflowStep.modelName}`
    })
    const workflowStepAttributes = workflowStepSchema.attributes
    if (Object.keys(workflowStepAttributes).includes('survey_metadata')) {
      surveyModel = workflowStep.modelName
      continue
    }

    if (workflowStep?.isSubmoduleStep) {
      //TODO handle this submodule case (will also need to do stuff like check survey metadata, which might be best handled elsewhere, i.e., `validateSurveyMetadata()`)
      console.log(`TODO submodule step found that needs to be handled for model: ${workflowStep.modelName}`)
      continue
    }    

    await checkStepModelData({
      baseUrl,
      JWT,
      attributes: workflowStepAttributes,
      modelName: workflowStep.modelName,
      pluralName: workflowStepSchema?.info?.pluralName,
      childObModels: workflowStep?.relationOnAttributesModelNames,
    })

    if (workflowStep?.relationOnAttributesModelNames?.length > 0) {
      for (const childObModel of workflowStep?.relationOnAttributesModelNames || []) {
        const childObSchema = loadSchema({
          path: `/${childObModel}`
        })
        const childObAttributes = childObSchema.attributes
        await checkStepModelData({
          baseUrl,
          JWT,
          attributes: childObAttributes,
          modelName: childObModel,
          pluralName: childObSchema?.info?.pluralName,
        })
      }
    }
  }
}

async function checkStepModelData({
  baseUrl,
  JWT,
  attributes,
  modelName,
  pluralName,
  childObModels = [],
}) {
  const attributesToCheck = getAttributesToCheckForWorkflowStep({
    attributes: attributes,
  })
  // console.log('attributesToCheck:', JSON.stringify(attributesToCheck, null, 2))

  const workflowStepData = await getDataForEndpoint({
    endpointPrefix: pluralName,
    baseUrl,
    JWT,
  })
  // console.log('workflowStepData:', JSON.stringify(workflowStepData, null, 2))
  for (const modelData of workflowStepData?.data || []) {
    for (const [attributeName, attributeMeta] of Object.entries(attributesToCheck)) {
      // console.log('attributeName:', attributeName)
      // console.log('attributeMeta:', JSON.stringify(attributeMeta, null, 2))
      if (attributeMeta?.type === 'relation') {
        const targetModel = attributeMeta?.target.slice(attributeMeta?.target.lastIndexOf('.') + 1)
        if (!modelData?.attributes?.[attributeName]?.data?.id) {
          let msg = `Data for model ${modelName} with ID=${modelData?.id} does not have a relation to field ${attributeName} (target model ${targetModel})`
          if (
            childObModels?.includes(targetModel)
          ) {
            //child obs are not always required, so we're warning about this
            msg += '. This is a relation to a child observation so it might not be required'
          }

          console.log(msg)
        }
      } else if (
        attributeMeta?.type === 'component' &&
        attributeMeta?.componentAssociations
      ) {
        for (const association of attributeMeta?.componentAssociations || []) {
          // console.log('association:', JSON.stringify(association, null, 2))
          // console.log(modelData?.attributes?.[attributeName])
          if (!modelData?.attributes?.[attributeName]?.[association.attribute]?.data?.id) {
            const isRequired = association?.association?.required || association?.association?.['x-paratoo-required']
            const relation = association?.association?.target.slice(association?.association?.target.lastIndexOf('.') + 1)

            let msg = `Data for model ${modelName} with ID=${modelData?.id} does not have a relation in component field ${attributeName}.${association.attribute} for relation ${relation}. It ${isRequired ? 'IS' : 'is NOT'} a required field`

            if (!isRequired) {
              //the field is not explicity required, but still worth flagging
              msg += ', but it still might be missing'
            }

            console.log(msg)
          }
        }

        //don't need to check for `nestedComponentAssociations` as they'll always exist
        //if the check for `componentAssociations` passes
        for (const nestedAssociation of attributeMeta?.nestedComponentAssociations || []) {
          if (!modelData?.attributes?.[attributeName]?.[nestedAssociation.parentAttribute]?.[nestedAssociation.attribute]?.data?.id) {
            const isRequired = nestedAssociation?.association?.required || nestedAssociation?.association?.['x-paratoo-required']
            const relation = nestedAssociation?.association?.target.slice(nestedAssociation?.association?.target.lastIndexOf('.') + 1)

            let msg = `Data for model ${modelName} with ID=${modelData?.id} does not have a relation in nested component field ${attributeName}.${nestedAssociation.parentAttribute}.${nestedAssociation.attribute} for relation ${relation}. It ${isRequired ? 'IS' : 'is NOT'} a required field`

            if (!isRequired) {
              //the field is not explicity required, but still worth flagging
              msg += ', but it still might be missing'
            }

            console.log(msg)
          }
        }
      }
    }
  }
}

function getAttributesToCheckForWorkflowStep({ attributes }) {
  //key is attribute name, value is type of relation (oneToOne, oneToMany, etc.)
  const attributesToCheck = {}
  for (const [attributeName, attributeData] of Object.entries(attributes)) {
    // console.log('attributeName:', attributeName)
    // console.log('attributeData:', attributeData)
    if (
      attributeData.type === 'relation' &&
      //exclude LUTs
      !attributeData?.target.startsWith('api::lut-')
    ) {
      attributesToCheck[attributeName] = attributeData
    } else if (
      attributeData.type === 'component'
    ) {
      //logic for finding component and nested component associations is based off of
      //lut-interpretation `replaceEnum()` logic
      const componentAttributes = loadComponentSchema({
        componentName: attributeData.component
      })?.attributes
      const componentAssociations = []
      for (const [k, o] of Object.entries(componentAttributes)) {
        if (
          o?.type === 'relation' &&
          //exclude LUTs
          !o?.target.startsWith('api::lut-')
        ) {
          componentAssociations.push({
            attribute: k,
            association: o
          })
        }

        if (o?.type === 'component') {
          const nestedComponentAttributes = loadComponentSchema({
            componentName: o.component
          })?.attributes
          const nestedComponentAssociations = []
          for (const [n_k, n_o] of Object.entries(nestedComponentAttributes)) {
            if (
              n_o?.type === 'relation' &&
              !n_o?.target.startsWith('api::lut-')
            ) {
              nestedComponentAssociations.push({
                attribute: n_k,
                association: n_o
              })
            }
          }
          if (nestedComponentAssociations.length > 0) {
            if (!attributesToCheck?.[attributeName]) {
              attributesToCheck[attributeName] = {}

              if (!attributesToCheck[attributeName]?.nestedComponentAssociations) {
                attributesToCheck[attributeName].nestedComponentAssociations = []
              }
            }
            for (const nestedAssociation of nestedComponentAssociations) {
              attributesToCheck[attributeName].nestedComponentAssociations.push({
                parentAttribute: k,
                component: o.component,
                repeatable: o.repeatable,
                ...nestedAssociation,
              })
            }
          }
        }
      }
      if (componentAssociations.length > 0) {
        attributesToCheck[attributeName] = {
          type: 'component',
          componentAssociations: [],
          //don't want to clobber the nested associations
          ...attributesToCheck[attributeName],
        }
        for (const association of componentAssociations) {
          attributesToCheck[attributeName].componentAssociations.push({
            component: attributeData.component,
            repeatable: attributeData.repeatable,
            ...association,
          })
        }
      }
    }
  }
  return attributesToCheck
}

async function checkPlotSelections({ baseUrl, JWT, initDataSurveys }) {
  const plotSelectionData = await getDataForEndpoint({
    endpointPrefix: 'plot-selections',
    baseUrl,
    JWT,
  })

  const duplicatePlotSelections = []
  for (const [index, plotSelection] of cloneDeep(plotSelectionData?.data).entries()) {
    for (const [index1, plotSelection1] of cloneDeep(plotSelectionData?.data).entries()) {
      if (
        //plot name is the same
        plotSelection.attributes.plot_label === plotSelection1.attributes.plot_label &&
        //it's not checking itself
        index !== index1 &&
        //it's not init data
        !initDataSurveys.some(
          s => s?.attributes?.survey_metadata?.survey_details?.uuid === plotSelection?.attributes?.plot_selection_survey?.data?.attributes.survey_metadata?.survey_details?.uuid
        )
      ) {
        duplicatePlotSelections.push(plotSelection)
        duplicatePlotSelections.push(plotSelection1)
      }
    }
  }

  const reducedDuplicatePlotSelections = duplicatePlotSelections.reduce((accum, curr) => {
    if (!accum[curr.attributes.plot_label]) {
      accum[curr.attributes.plot_label] = []
    }
    
    if (!accum[curr.attributes.plot_label].some(p => p.plotSelectionId === curr.id)) {
      accum[curr.attributes.plot_label].push({
        plotSelectionId: curr.id,
        plotSelectionSurveyId: curr.attributes.plot_selection_survey.data.id,
        projectId: curr.attributes.plot_selection_survey.data.attributes.survey_metadata.survey_details.project_id,
        //very unlikely we have multiple layouts for a selection, but worth
        //checking anyway
        hasLayouts: [],
      })
    }
    
    return accum
  }, {})

  const plotLayoutData = await getDataForEndpoint({
    endpointPrefix: 'plot-layouts',
    baseUrl,
    JWT,
  })

  for (const [plotLabel, metas] of Object.entries(reducedDuplicatePlotSelections)) {
    for (const meta of metas) {
      for (const layout of cloneDeep(plotLayoutData?.data)) {
        if (
          layout?.attributes?.plot_selection?.data?.id === meta.plotSelectionId
        ) {
          meta.hasLayouts.push({
            layoutId: layout.id,
          })
        }
      }
    }
  }

  console.log(`Found ${Object.keys(reducedDuplicatePlotSelections).length} plot labels with duplicates (and JSON output below):`)
  for (const [plotLabel, metas] of Object.entries(reducedDuplicatePlotSelections)) {
    
    for (const [index, meta] of metas.entries()) {
      console.log(`Plot Selection ${plotLabel} #${index+1}`)

      console.log(`\tProject ID: ${meta.projectId}`)
      console.log(`\tPlot Selection Survey ID: ${meta.plotSelectionSurveyId}`)
      console.log(`\tPlot Selection ID: ${meta.plotSelectionId}`)

      const layouts = meta?.hasLayouts?.map(l => l?.layoutId)?.join(', ') || null
      if (layouts) {
        console.log(`\tLayout IDs: ${layouts}`)
      } else {
        console.log(`\tNo Layouts`)
      }
    }
  }
  console.log(JSON.stringify(reducedDuplicatePlotSelections, null, 2))

  //for all the duplicate plot selections that have layouts, find the overlap of
  //the points to determine if the plots are effectively the same
  const duplicatePlotSelectionPlotPointsOverlap = []
  for (const [index, [plotLabel, metas]] of Object.entries(reducedDuplicatePlotSelections).entries()) {
    //create a colour for this plot, so that if many of the KML files are loaded,
    //they're easy to distinguish: https://stackoverflow.com/a/5092872
    const colour = '#000000'.replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);})
    if (
      metas.every(m => m.hasLayouts.length > 0)
    ) {
      

      const layoutIdsForSelection = metas.reduce((accum, curr) => {
        accum.push(...curr.hasLayouts.map(l => l.layoutId))
        
        return accum
      }, [])

      // console.log(`Plot ${plotLabel} has more than one layout: ${layoutIdsForSelection.join(', ')}`)

      //want to get all combinations, as if there's more than 2 layouts, we need to
      //check them all against each other
      //https://stackoverflow.com/a/43241295
      const layoutIdsForSelectionCombinations = []
      for (let i = 0; i < layoutIdsForSelection.length; i++) {
        for (let j = i + 1; j < layoutIdsForSelection.length; j++) {
          layoutIdsForSelectionCombinations.push([layoutIdsForSelection[i], layoutIdsForSelection[j]]);
        }
      }
      for (const layoutCombination of layoutIdsForSelectionCombinations) {
        const layoutId1 = layoutCombination[0]
        const layoutId2 = layoutCombination[1]

        const layout1 = cloneDeep(plotLayoutData?.data).find(l => l.id === layoutId1)
        const layout2 = cloneDeep(plotLayoutData?.data).find(l => l.id === layoutId2)

        const poly1 = createPolygonFromLayout({ layout: layout1 })
        const poly2 = createPolygonFromLayout({ layout: layout2 })

        //based on: https://stackoverflow.com/a/32291453 - it's a bit old so had to
        //also go to docs: https://github.com/Turfjs/turf/tree/master/packages/turf-intersect
        const intersection = turf.intersect(turf.featureCollection([poly1, poly2]))
        if (intersection) {
          const area_intersection = geojsonArea.geometry(intersection.geometry)
          const area_poly1 = geojsonArea.geometry(poly1.geometry)
          const percent_poly1_covered_by_poly2 = (area_intersection / area_poly1)*100
          duplicatePlotSelectionPlotPointsOverlap.push({
            plot: plotLabel,
            layoutId1: layoutId1,
            layoutId2: layoutId2,
            layout1GeoJson: poly1,
            layout2GeoJson: poly2,
            intersection: intersection,
            area_intersection: area_intersection,
            area_layout1: area_poly1,
            percent_layout1_covered_by_layout2: percent_poly1_covered_by_poly2
          })
        }
        const geoJsonForExport = {
          type: 'FeatureCollection',
          features: [
            {
              ...poly1,
              properties: {
                plot_label: plotLabel,
                name: `ID=${layoutId1}`,
              },
            },
            {
              ...poly2,
              properties: {
                plot_label: plotLabel,
                name: `ID=${layoutId2}`,
              },
            },
          ]
        }
        for (const feature of geoJsonForExport.features) {
          feature.geometry.type = 'LineString'
          //flatten array from the polygon format to linestring format
          feature.geometry.coordinates = feature.geometry.coordinates[0]
        }

        const fn = `./${plotLabel}.kml`
        console.log(`Outputting KML file of layouts to: ${fn}`)
        await kml.fromGeoJson(geoJsonForExport, fn, {
          documentName: `Plot ${plotLabel}`,
          symbol: (feature) => {
            return {
              color: colour,
              alpha: 255,
              width: 10,
            }
          },
        })              
      }
    }
  }
  console.log(`Found ${duplicatePlotSelectionPlotPointsOverlap.length} duplicate plot overlapping layouts:`, JSON.stringify(duplicatePlotSelectionPlotPointsOverlap, null, 2))

  const reservedPlotsData = await getDataForEndpoint({
    endpointPrefix: 'reserved-plot-labels',
    baseUrl,
    JWT,
  })

  for (const plotSelection of plotSelectionData?.data || []) {
    const reservedPlotEntryFullMatch = reservedPlotsData?.data?.find(
      d => d?.attributes?.plot_label === plotSelection?.attributes?.plot_label &&
        d?.attributes?.created_plot_selection?.data?.id === plotSelection?.id
    )
    if (reservedPlotEntryFullMatch) {
      console.log(`Reserved plot ${plotSelection?.attributes?.plot_label} is correct`)
      continue
    }

    const reservedPlotEntryLabelMatch = reservedPlotsData?.data?.find(
      d => d?.attributes?.plot_label === plotSelection?.attributes?.plot_label
    )
    const reservedPlotEntryRelationMatch = reservedPlotsData?.data?.find(
      d => d?.attributes?.created_plot_selection?.data?.id === plotSelection?.id
    )

    let msg = `Reserved plot ${plotSelection?.attributes?.plot_label} ${reservedPlotEntryFullMatch ? 'is' : 'is NOT'} correct: `

    if (!reservedPlotEntryLabelMatch && !reservedPlotEntryRelationMatch) {
      msg += `No reservation entry`
    } else {
      msg += `${reservedPlotEntryLabelMatch ? '' : 'NO '}label match. `
      msg += `${reservedPlotEntryRelationMatch ? '' : 'NO '}relation match. `
    }
    console.log(msg)
  }
  
}

function createPolygonFromLayout({ layout, plotPointsKey = 'plot_points' }) {
  const layout_ = cloneDeep(layout)  
  const layoutMapped = [
    layout_.attributes[plotPointsKey]
      .sort(
        (a, b) => plotPointsSortOrder.indexOf(a.name.data.attributes.symbol) - plotPointsSortOrder.indexOf(b.name.data.attributes.symbol)
      )
      .filter(excludeCentrePlotPointFilterCb)
      .map(geoJsonCoordsMapperCb)
  ]
  //need to add the first point to complete the polygon, else`turf.polygon()` will
  //throw an error
  layoutMapped[0].push(layoutMapped[0][0])
  const poly = turf.polygon(layoutMapped)
  
  return poly
}
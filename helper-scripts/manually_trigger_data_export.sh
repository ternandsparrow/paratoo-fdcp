#!/bin/bash

URL="http://localhost:7095/data-export/v1/upload-all"

# START_DATE="2024-05-07T21:17:59.248Z"
read -p "What is start date (format YYYY-MM-DDTHH:MM:SS.sssZ)? " START_DATE

read -p "What is end date (default to current if empty)? " INPUT_END_DATE

END_DATE=$(INPUT_END_DATE || date -u +"%Y-%m-%dT%H:%M:%S.%3NZ")

# Define the payload
PAYLOAD="{\"end_date\":\"$END_DATE\",\"start_date\":\"$START_DATE\"}"

echo "Sending payload: $PAYLOAD to $URL"

# Make the curl request
curl -X POST $URL -H "Content-Type: application/json" -d "$PAYLOAD"
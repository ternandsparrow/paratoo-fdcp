import json
import os

CORE_SOURCE = "paratoo-core/src"
CORE_TEST_DIRECTORY = "paratoo-core/tests"
ORG_SOURCE = "paratoo-org/src"
ORG_TEST_DIRECTORY = "paratoo-org/tests"

API = "api"
COMPONENT = "components"

TEST_VALUES = {
    "datetime": "2022-09-14T01:38:29.237Z",
    "integer": 1,
    "decimal": 0,
    "float": 0.1,
    "string": "testData",
    "text": "testData",
    "boolean": False,
    "json": {
        "surveyType": "survey",
        "time": "2021-10-13T01:16:47.225Z",
        "randNum": 16000000,
    },
    "media": 0,
}

# for some models, we dont need to generate all values
#   e.g. plot-location = {"id": 1} or plot-visit = {"id": 1}
FIXED_API_VALUE = {
    "plot-location": {"id": 1},
    "plot-layout": {"id": 1},
    "plot-visit": {"id": 1},
}


class AutoTestInputs:
    def __init__(self, source_path: str, test_path: str):
        self.source_path = source_path
        self.test_path = test_path
        self.input_data = None

    def generate_all_protocol_input_data(self, store_json=False):
        content = self.get_schema_content("protocol", API)
        protocols = content["initialData"]
        for protocol in protocols:
            self.generate_protocol_payload(
                protocol_endpoint=protocol[2],
                protocols=protocols,
                store_json=store_json,
            )

    def generate_protocol_payload(
        self, protocol_endpoint: str, protocols: list, store_json=False
    ):
        """
        generate and store single protocol.
        """
        if "/" not in protocol_endpoint:
            protocol_endpoint = "/" + protocol_endpoint

        models = self.get_all_models(protocol_endpoint, protocols)

        payload = {}
        all_properties = {}
        for model in models:
            # e.g. model {
            # 'modelName': 'basal-wedge-observation',
            # 'protocol-variant': 'wedge',
            # 'multiple': True
            # }
            child_observations = []
            if "modelName" not in model.keys():
                continue

            model_name = model["modelName"]
            all_properties[model_name] = self.get_all_properties(model_name)
            if model_name in FIXED_API_VALUE.keys() and len(models) > 1:
                payload[model_name] = FIXED_API_VALUE[model_name]
                continue

            if "newInstanceForRelationOnAttributes" in model.keys():
                child_observations = model["newInstanceForRelationOnAttributes"]

            data = self.generate_schema_payload(
                content_name=model_name,
                content_type=API,
                child_observations=child_observations,
            )

            # if multiple
            if self.is_multiple(object=model, key="multiple"):
                data = [data]

            payload[model_name] = data
        self.input_data = payload
        all_routes = self.get_all_routes('/drone-surveys')
        # we store data to test directory
        if store_json:
            self.store_inputs_json(protocol_endpoint[1:], all_properties, all_routes)

    def get_all_routes(self, protocol_endpoint):
        """
        get all available routes.
        """
        # convert api path to model name
        # e.g. '/drone-surveys' to 'drone-survey'
        api_name = protocol_endpoint
        
        
        # remove first / and last 's'
        if '/' in api_name:
            api_name = api_name[1:]
        api_name = api_name[:-1]
        print(api_name)
        route_file = f"./{self.source_path}/api/{api_name}/routes/{api_name}.js"

        all_routes = []
        if not os.path.exists(os.path.dirname(route_file)):
            return all_routes
        
        # if api_name == 'drone-survey':
        #     return[]

        # a simple hack to extract js object as dict 
        #   as route file is a js file not json
        with open(route_file, 'r', encoding='utf-8') as js_file:
            # read file as a text      
            route_js = js_file.read()
            route_js_trim = ''.join(route_js.split())
            routes_splits = route_js_trim.split("'routes'")
            routes_raw = "{'routes'" + routes_splits[1]
            routes_raw = str(routes_raw)
            # routes_raw_1 = routes_raw.replace("'", "\"")
            # routes_str = routes_raw_1.replace('\'', '\"')
            
            # string to dictionary
            print(routes_raw)
            routes_dict = json.loads(routes_raw,  strict=False)
            
            # only methods and paths
            for route in routes_dict['routes']:
                all_routes.append({
                    'method': route['method'],
                    'path': route['path'],
                })
            return all_routes

    def get_all_properties(self, model_name):
        """
        extracts all relations, required fields, unique fields 
        """
        all_properties = {
            "required" : [],
            "unique" : [],
            "relations" : [],
        }

        content = self.get_schema_content(content_name=model_name, content_type=API)

        for key, value in content['attributes'].items():
            target, required, unique = "", False, False

            # if any of the properties exits, we extract values
            if "target" in value.keys():
                target = value["target"]
            if "required" in value.keys():
                required = value["required"]
            if "unique" in value.keys():
                unique = value["unique"]

            # set properties
            if target and target not in all_properties['relations']:
                all_properties['relations'].append(target)
            if required:
                all_properties['required'].append(key)
            if unique:
                all_properties['unique'].append(key)
        
        return all_properties

    def get_all_models(self, protocol_endpoint, protocols):
        """
        loads all model names.
        """
        for protocol in protocols:
            # if endpoint matches
            # /basal-wedge-surveys
            if protocol[2] != protocol_endpoint:
                continue
            return protocol[6]

    def generate_schema_payload(
        self, content_name: str, content_type: str, child_observations: list
    ):
        """
        generate payload using schema.
        e.g plot-visit
        {
            'start_date': '2022-09-14T01:38:29.237Z',
            'end_date': '2022-09-14T01:38:29.237Z',
            'plot_location': {'id': 1},
            'visit_field_name': 'testData',
            'plot_definition_survey': {'id': 1}
        }
        """
        content = self.get_schema_content(content_name, content_type)
        payload = {
            "data": self.generate_input_data(
                content["attributes"], content_type, child_observations
            )
        }
        return payload

    def get_schema_content(self, content_name, content_type):
        """
        loads json schema.
        """
        file_path = f"./{self.source_path}/{content_type}/{content_name}/content-types/{content_name}/schema.json"

        if content_type == COMPONENT:
            file_name = content_name.replace(".", "/") + ".json"
            file_path = f"./{self.source_path}/{content_type}/{file_name}"

        return json.load(open(file_path))

    def store_inputs_json(self, content_name, all_properties, all_routes):
        """
        saves input data in json file to test directory.
        """
        file_path = f"./{self.test_path}/json/inputs/{content_name}.json"
        # create file if doest exist
        if not os.path.exists(os.path.dirname(file_path)):
            try:
                os.makedirs(os.path.dirname(file_path))
            except:
                print("Failed to create json file to save data")

        with open(file_path, "w", encoding="utf-8") as f:
            # print("created " + content_name + ".json")
            json.dump(self.input_data, f, ensure_ascii=False, indent=4)

        self.store_properties_and_routes(content_name, all_properties, all_routes)

    def store_properties_and_routes(self, content_name, all_properties, all_routes):
        """
        saves targets in json file to test directory.
        """
        if not all_properties:
            return
        
        file_path = f"./{self.test_path}/json/properties/{content_name}.json"
        # create file if doest exist
        if not os.path.exists(os.path.dirname(file_path)):
            try:
                os.makedirs(os.path.dirname(file_path))
            except:
                print("Failed to create json file to save data")

        properties = {
            "routes": all_routes,
            "properties": all_properties
        }

        with open(file_path, "w", encoding="utf-8") as f:
            # print("created " + content_name + ".json")
            json.dump(properties, f, ensure_ascii=False, indent=4)

    def generate_input_data(self, attributes, content_type, child_observations=[]):
        """
        assign test values recursively.
        """
        payload = {}
        for key, value in attributes.items():
            target = ""
            if "target" in value.keys():
                # target exists if the object is an api or a lut
                target = value["target"]

            test_value = self.get_test_value(value["type"], target)
            if test_value is not None:
                payload[key] = test_value
                continue

            # if the object is a lut, we set value extracted from initial data
            if self.is_lut(target):
                payload[key] = self.get_lut_value(target, content_type)
                continue

            # if component, we assign values recursively.
            if value["type"] == "component":
                content = self.get_schema_content(value["component"], COMPONENT)
                data = self.generate_input_data(content["attributes"], content_type)

                # if repeatable then data should be in a array
                if self.is_multiple(object=value, key="repeatable"):
                    data = [data]

                payload[key] = data
                continue

            # model exists in newInstanceForRelationOnAttributes
            if key in child_observations:
                model_name = self.target_to_model_name(target)
                content = self.get_schema_content(model_name, API)
                data = {"data": self.generate_input_data(content["attributes"], API)}

                # if oneToMany then data should be in a array
                if value["relation"] == "oneToMany":
                    data = [data]
                if self.is_multiple(
                    object=value, key="relation", expected_text="oneToMany"
                ):
                    data = [data]

                payload[key] = data
                continue

            # bu default we assign 0
            payload[key] = 0

        return payload

    def target_to_model_name(self, target):
        """
        generate api path.
        e.g. "api::coarse-woody-debris-survey.coarse-woody-debris-survey"
            to "coarse-woody-debris-survey"
        """
        if not target or target == "":
            return ""
        return json.dumps(target).split("api::")[1].split(".")[0]

    def is_lut(self, target):
        # checks prefix "lut" exists or not.
        return len(target.split("lut")) > 1

    def get_test_value(self, type, target):
        """
        get test values using types.
        """
        for key, value in TEST_VALUES.items():
            if type != key:
                continue

            return value

        return None

    def get_lut_value(self, target, content_type):
        """
        get first value of initial data.
        """
        api_path = self.target_to_model_name(target)
        content = self.get_schema_content(api_path, content_type)

        if "initialData" not in content.keys():
            return ""
        return content["initialData"][0][0]

    def is_multiple(self, object: dict, key: str, expected_text=None):
        if not object:
            return False
        if key not in object.keys():
            return False
        if not expected_text:
            return object[key]
        return object[key] == expected_text


def main():
    auto_test_input = AutoTestInputs(CORE_SOURCE, CORE_TEST_DIRECTORY)
    auto_test_input.generate_all_protocol_input_data(store_json=True)


if __name__ == "__main__":
    main()

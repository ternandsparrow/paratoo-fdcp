#!/bin/bash
set -euo pipefail
cd `dirname "$0"`
cd ..

echo "WARNING: this will sync the paratoo-core definition into paratoo-org,
which will be clobbered. If you have modified paratoo-org, DO NOT continue as
those changes will be lost."
read -p "Press any key to continue, or Control-c to cancel " -n 1 -r
echo

suffix=src/api/protocol/content-types/protocol/schema.json
rm -fr paratoo-org/$suffix
cp -vr paratoo-core/$suffix paratoo-org/$suffix

echo "Done"

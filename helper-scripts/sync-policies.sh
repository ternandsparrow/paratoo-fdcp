#!/bin/bash
set -euo pipefail
cd `dirname "$0"`
cd ..

echo "WARNING: this will sync the paratoo-core definition into paratoo-org,
which will be clobbered. If you have modified paratoo-org, DO NOT continue as
those changes will be lost."
read -p "Press any key to continue, or Control-c to cancel " -n 1 -r
echo

cp ./paratoo-core/src/policies/is-validated.js ./paratoo-org/src/policies/is-validated.js 

echo "Done"

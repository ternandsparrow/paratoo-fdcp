# Mini Strapi4 migration tool
# Because codemods requires a fresh Strapi v3 install to run, which is inconvenient when we've made
# A lot of changes to a v4 build already



echo ${baseFile}

# API services didn't properly migrate, so find all APIs with default
# controller and make a services file
for d in */ ; do
    # Each folder name is just modelName (ending with '/')
    modelName=${d::-1}
    echo "$modelName"
    cd ${d}

    baseFile="const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::${modelName}.${modelName}')"
    
    echo ${baseFile} > ./services/${modelName}.js

    cd ..
done

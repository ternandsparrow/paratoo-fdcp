import os
import json
import sys
import inflect
p = inflect.engine()

def main():
  """
  Serialises the LUT initial data from a text file, where each line is is a LUT entry. Assumes the LUT's model already exists and has attributes: symbol (string, required), label (string, required), description (string), uri (string)

  Pass the text file path (relative or absolute) with the first argument and the LUT name as the second argument.
  For example, run the script with: `python3 lut_creation.py ~/Desktop/lut_data.txt lut-bioregion`

  Requires `inflect` pip package (pip install inflect)
  """
  args = sys.argv

  try:
    data_path = args[1]
  except:
    print('Expected the data path for the first argument but got none')
    sys.exit()
  
  try:
    lut_name = args[2]
  except:
    print('Expected the LUT name for the second argument but got none')
    sys.exit()

  with open(data_path, 'r') as f:
    lines = f.readlines()
  
  lut_schema_path = f'../paratoo-core/src/api/{lut_name}/content-types/{lut_name}/schema.json'
  # https://stackoverflow.com/a/40718115
  with open(lut_schema_path, 'r') as f:
    model = json.load(f)

    init_data = []
    for l in lines:
      stripped_line = l.replace('\n', '')
      init_data.append([
        stripped_line,
        stripped_line,
        "",
        ""
      ])
    model["initialData"] = init_data

  os.remove(lut_schema_path)
  with open(lut_schema_path, 'w') as f:
    json.dump(model, f, indent=2)
  print(f'Wrote init data to schema at: {lut_schema_path}')

if __name__ == "__main__":
  main()
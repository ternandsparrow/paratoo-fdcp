import dummyValuesBasedOnSchema as dv
import os
import json

CORE_SOURCE = "paratoo-core/src"
ORG_SOURCE = "paratoo-org/src"


class InitDataCreation:
    def __init__(self, source_path: str):
        self.source_path = source_path
        # core or org
        self.auto_values = dv.DummyValuesBasedOnSchema(self.source_path, dv.INIT_DATA)

    def refresh_all_initial_data(self):
        # we should not auto generate init data for luts
        all_models = self.auto_values.get_all_models(including_luts=True)

        print("refreshing all initial values ...................... ")
        for model in all_models:
            content = self.auto_values.get_schema_content(
                content_type=dv.API, model_name=model
            )
            content_keys = content.keys()
            if "initialData" in content_keys or len(content_keys) == 0:
                continue

            # if not initial data found
            attributes = self.auto_values.get_attributes(schema_content=content)
            dummy_data = self.auto_values.generate_input_data(
                attributes=attributes, content_type=dv.API
            )
            content["initialData"] = [
                self.convert_to_initial_data(schema_values=dummy_data)
            ]
            self.add_init_data(model_name=model, schema_content=content)

    def add_init_data(self, model_name: str, schema_content: dict):
        model_path = f"./{self.source_path}/{dv.API}/{model_name}/content-types/{model_name}/schema.json"
        if not os.path.exists(os.path.dirname(model_path)):
            print(model_path + " not found to store init data")
            return

        print("saving init data for " + model_name)
        print(schema_content['initialData'])
        with open(model_path, "w", encoding="utf-8") as f:
            json.dump(schema_content, f, ensure_ascii=False, indent=4)

    def convert_to_initial_data(self, schema_values: dict):
        init_data = []
        for key, value in schema_values.items():
            init_data.append(value)
        return init_data


def main():
    init_data = InitDataCreation(source_path=CORE_SOURCE)
    init_data.refresh_all_initial_data()


if __name__ == "__main__":
    main()

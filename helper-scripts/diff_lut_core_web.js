#! /usr/bin/env node
const {
  run: generateLutInitData,
} = require('./js-runtime/createClientInitState.js')
const { readFileSync } = require('fs')

console.log(__dirname)
const CORE_PATH = '../paratoo-core/src/api'
const WEBAPP_LUT_INIT_PATH =
  '../paratoo-webapp/src/assets/stateInits/apiModels.js'

const currLutInitDateInCore = generateLutInitData(false, CORE_PATH)
const currLutInitDataInWebapp = readFileSync(WEBAPP_LUT_INIT_PATH, 'utf8')

if (currLutInitDateInCore !== currLutInitDataInWebapp) {
  throw new Error(
    'lut init data mismatch between core and webapp, please run the related script in helper to sync the LUT',
  )
}

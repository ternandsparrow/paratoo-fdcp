#!/bin/bash

# handle params and flags: https://stackoverflow.com/a/21128172
recursive_flag=''
remote_host=''
remote_folder_or_file=''
local_folder=''

print_usage() {
  printf "
  Usage:
    > -r: scp recursive flag (to copy a folder/directory, rather than single file)
    > -h: the remote host - e.g., ubuntu@1.1.1.1
    > -f: the folder or file on the remote host to copy - e.g., /home/user/foo/bar (folder), /home/user/foo/bar/file.txt (file)
    > -l: the local folder to copy to - e.g., /home/localuser/foo
  "
}

# don't allow no options: https://unix.stackexchange.com/a/25947
if [ $# -eq 0 ]; then
  >&2 echo "No arguments provided"
  exit 1
fi

while getopts 'rh:f:l:' flag; do
  case "${flag}" in
    r) recursive_flag='true' ;;
    h) remote_host="${OPTARG}" ;;
    f) remote_folder_or_file="${OPTARG}" ;;
    l) local_folder="${OPTARG}" ;;
    *) print_usage
       exit 1 ;;
  esac
done

requiredArgs=("remote_host" "remote_folder_or_file" "local_folder")
missingRequiredArgs=()
for arg in ${requiredArgs[@]}; do
  # access arg from str: https://stackoverflow.com/a/1921337
  if [ "${!arg}" == '' ]; then
    missingRequiredArgs+=("$arg")
  fi
done
if [ "${#missingRequiredArgs[@]}" -gt 0 ]; then
  echo "Missing the following required arguments: ${missingRequiredArgs[@]}"
  exit 1
fi

base_command='scp'

if [ "$recursive_flag" == 'true' ]; then
  base_command="$base_command -r"
fi

base_command="$base_command $remote_host:$remote_folder_or_file $local_folder"
echo "running command: ${base_command}"
$base_command

import dummyValuesBasedOnSchema as dv
import json
import requests
import sys
import hashlib
import os

CORE_SOURCE = "paratoo-core/src"
IMAGES_ROOT = "images_dump/"
COLLECTIONS_ROOT = "collections_dump/"

COLLECTION_TYPES = {
    "VOUCHER": [
        "e15db26f-55de-4459-841b-d7ef87dea5cd", # floristics full
        "a0f57791-e858-4f33-ae8e-7e3e3fffb447" # floristics lite
        ]
}


class SyncCollection:
    def __init__(self, local: str, localToken: str, remote: str, remoteToken: str):
        self.remote = remote
        self.local = local
        self.localToken = localToken
        self.remoteToken = remoteToken

        self.schema_processor = dv.DummyValuesBasedOnSchema(CORE_SOURCE, dv.INIT_DATA)

    def collectionBackUp(self):
        if not os.path.exists(IMAGES_ROOT):
            os.makedirs(IMAGES_ROOT)
        if not os.path.exists(COLLECTIONS_ROOT):
            os.makedirs(COLLECTIONS_ROOT)

        # metadats
        temp = self.existingData(api="org-uuid-survey-metadatas", base=self.remote)
        with open(f"{COLLECTIONS_ROOT}org_uuids_dumb.json", "w", encoding="utf-8") as f:
            json.dump(temp, f, ensure_ascii=False, indent=2)

        print("downloading all collections ")
        if not temp["data"]:
            print(temp)
            return
        # collections
        collections = []
        for d in temp["data"]:
            # print(d["attributes"]["org_minted_uuid"])
            temp2 = requests.post(
                f"{self.remote}/api/protocols/reverse-lookup",
                headers={"Authorization": f"Bearer {self.remoteToken}"},
                data={"org_minted_uuid": d["attributes"]["org_minted_uuid"]},
            )
            collections.append(temp2.json())
        with open(
            f"{COLLECTIONS_ROOT}collections_dumb.json", "w", encoding="utf-8"
        ) as f:
            json.dump(collections, f, ensure_ascii=False, indent=2)

        print("downloading all plot selections ")
        # plot selections
        temp3 = self.existingData(api=self.modelToApiName(modelName="plot-selection"), base=self.remote)
        with open(
            f"{COLLECTIONS_ROOT}plot_selection_dumb.json", "w", encoding="utf-8"
        ) as f:
            json.dump(temp3, f, ensure_ascii=False, indent=2)

        # plot selection surveys
        temp4 = self.existingData(api=self.modelToApiName(modelName="plot-selection-survey"), base=self.remote)
        with open(
            f"{COLLECTIONS_ROOT}plot_selection_surveys_dumb.json", "w", encoding="utf-8"
        ) as f:
            json.dump(temp4, f, ensure_ascii=False, indent=2)

        print("downloading all plot layouts ")
        # plot layouts
        temp5 = self.existingData(api=self.modelToApiName(modelName="plot-layout"), base=self.remote)
        with open(
            f"{COLLECTIONS_ROOT}plot_layouts_dumb.json", "w", encoding="utf-8"
        ) as f:
            json.dump(temp5, f, ensure_ascii=False, indent=2)

        print("downloading all plot visits ")
        # plot visits
        temp6 = self.existingData(api=self.modelToApiName(modelName="plot-visit"), base=self.remote)
        with open(
            f"{COLLECTIONS_ROOT}plot_visits_dumb.json", "w", encoding="utf-8"
        ) as f:
            json.dump(temp6, f, ensure_ascii=False, indent=2)

        print("downloading all files ")
        # download all images
        images = self.existingData(api="upload/files", base=self.remote)
        print(json.dumps(images))
        for image in images:
            path = IMAGES_ROOT + image["name"]
            url = image["url"] 
            if "http" not in image["url"] :
                url = f"{self.remote}{url}"
            img = requests.get(url, stream=True)
            with open(path, "wb") as f:
                for chunk in img:
                    f.write(chunk)

            if image["formats"]:
                for key, value in image["formats"].items():
                    path = IMAGES_ROOT + key + "_" + image["name"]
                    url = value["url"] 
                    if "http" not in value["url"] :
                        url = f"{self.remote}{url}"
                    img = requests.get(url, stream=True)
                    with open(path, "wb") as f:
                        for chunk in img:
                            f.write(chunk)

    def existingData(self, api, base):
        headers = {"Authorization": f"Bearer {self.localToken}"}
        if base == self.remote:
            headers["Authorization"] = f"Bearer {self.remoteToken}"

        url = f"{base}/api/{api}?populate=deep"
        try:
            r = requests.get(url, headers=headers, timeout=20)
            res = r.json()
            return res
        except Exception as e:
            print(e)
            return {"data": []}

    def fixAttribute(self, object, field):
        if not isinstance(object, dict) and not isinstance(object, list):
            return object

        tempObject = object
        if isinstance(tempObject, dict):
            if field in tempObject:
                if tempObject[field] and not isinstance(tempObject[field], list):
                    tempObject = {**tempObject, **tempObject[field]}
                    del tempObject[field]
            obs_keys = list(tempObject.keys())
            for k in obs_keys:
                tempObject[k] = self.fixAttribute(tempObject[k], field)

        if isinstance(tempObject, list):
            temp = tempObject
            for idx, x in enumerate(temp):
                tempObject[idx] = self.fixAttribute(dict(x), field)
        return tempObject

    def removeEmptyFields(self, object, skips=[]):
        if not isinstance(object, dict) and not isinstance(object, list):
            return object
        temp = object
        if isinstance(temp, dict):
            obs_keys = list(temp.keys())
            for k in obs_keys:
                if temp[k] == None:
                    del temp[k]
                    continue
                if temp[k] == {"data": None}:
                    del temp[k]
                    continue
                temp[k] = self.removeEmptyFields(object=temp[k])

        if isinstance(temp, list):
            for idx, x in enumerate(temp):
                temp[idx] = self.removeEmptyFields(object=temp[idx])

        return temp

    def removeFields(self, object, skips=[]):
        fieldsToRemove = ["id"]
        # fieldsToRemove = ["id", "marker"]
        # updateEmptyValue = ["comments", "orientation", "permanently_marked", "end_date"]
        # updateEmptyLutValue = ["recommended_location_point"]
        updateEmptyValue = []
        updateEmptyLutValue = []
        if not isinstance(object, dict) and not isinstance(object, list):
            return object

        tempObject = object
        for field in fieldsToRemove:
            if not isinstance(tempObject, dict):
                continue
            if field in tempObject and "symbol" not in tempObject:
                del tempObject[field]
            obs_keys = list(tempObject.keys())
            for k in obs_keys:
                if k in skips:
                    continue
                tempObject[k] = self.removeFields(tempObject[k])

        for field in updateEmptyValue:
            if not isinstance(tempObject, dict):
                continue
            if field in tempObject:
                if not tempObject[field]:
                    del tempObject[field]
            obs_keys = list(tempObject.keys())
            for k in obs_keys:
                if k in skips:
                    continue
                tempObject[k] = self.removeFields(tempObject[k])

        for field in updateEmptyLutValue:
            if not isinstance(tempObject, dict):
                continue
            if field in tempObject:
                if not tempObject[field]:
                    del tempObject[field]
                if isinstance(tempObject[field], dict):
                    if "data" in tempObject[field]:
                        if not tempObject[field]["data"]:
                            del tempObject[field]
            obs_keys = list(tempObject.keys())
            for k in obs_keys:
                if k in skips:
                    continue
                tempObject[k] = self.removeFields(tempObject[k])

        if isinstance(tempObject, list):
            temp = tempObject
            for idx, x in enumerate(temp):
                tempObject[idx] = self.removeFields(x)
        return object

    def publishData(self, modelName, data, base):
        api = self.modelToApiName(modelName=modelName)
        headers = {"Authorization": f"Bearer {self.localToken}"}
        if base == self.remote:
            headers["Authorization"] = f"Bearer {self.remoteToken}"
        url = f"{base}/api/{api}"
        modelValue = {"data": dict(data)}
        try:
            r = requests.post(url, json=modelValue, headers=headers, timeout=20)
            res = r.json()
            return res
        except Exception as e:
            print(e)
            return None

    def fixFiles(self, object, plotData):
        if not isinstance(object, dict) and not isinstance(object, list):
            return object

        tempObject = object
        if isinstance(object, dict):
            tempObject = dict(object)
            if "name" in tempObject and "hash" in tempObject:
                return plotData["files"][tempObject["name"]]

            obs_keys = list(tempObject.keys())
            for k in obs_keys:
                tempObject[k] = self.fixFiles(object=tempObject[k], plotData=plotData)

        if isinstance(tempObject, list):
            temp = tempObject
            for idx, x in enumerate(temp):
                tempObject[idx] = self.fixFiles(object=x, plotData=plotData)

        return tempObject

    def fixLuts(self, object):
        if not isinstance(object, dict) and not isinstance(object, list):
            return object
        tempObject = object

        if isinstance(object, dict):
            tempObject = dict(object)
            if "symbol" in tempObject:
                return tempObject["symbol"]

            obs_keys = list(tempObject.keys())
            for k in obs_keys:
                # print(k)
                tempObject[k] = self.fixLuts(object=tempObject[k])

        if isinstance(tempObject, list):
            temp = tempObject
            for idx, x in enumerate(temp):
                tempObject[idx] = self.fixLuts(x)
        return tempObject

    def pushPlotCollections(self):
        plot_selection_surveys = {}
        remote = {}
        with open(f"{COLLECTIONS_ROOT}plot_selection_surveys_dumb.json") as fp:
            remote = json.load(fp)
        # push plot selection surveys
        exists = self.existingData(api="plot-selection-surveys", base=self.local)
        exists = self.fixAttribute(exists, "attributes")
        exists = self.fixAttribute(exists, "data")
        for e in exists["data"]:
            uuid = e["survey_metadata"]["survey_details"]["uuid"]
            plot_selection_surveys[uuid] = e["id"]

        remote = self.fixAttribute(remote, "attributes")
        remote = self.fixAttribute(remote, "data")
        remote = remote["data"]

        for r in remote:
            uuid = r["survey_metadata"]["survey_details"]["uuid"]
            if uuid in plot_selection_surveys.keys():
                # print("plot selection survey exists")
                continue

            data = self.removeFields(dict(r), "id")
            print("pushed plot selection survey ")
            res = self.publishData(
                modelName="plot-selection-survey", data=dict(data), base=self.local
            )
            print(res["data"]["id"])
            plot_selection_surveys[uuid] = res["data"]["id"]

        #################################### plot selection ###############################
        plot_selections = {}
        remote = {}
        with open(f"{COLLECTIONS_ROOT}plot_selection_dumb.json") as fp:
            remote = json.load(fp)

        # push plot selections
        exists = self.existingData(api="plot-selections", base=self.local)
        exists = self.fixAttribute(exists, "attributes")
        exists = self.fixAttribute(exists, "data")
        exists = self.fixLuts(exists)
        exists = exists["data"]
        for e in exists:
            plot_selections[e["uuid"]] = e["id"]

        remote = self.fixAttribute(remote, "attributes")
        remote = self.fixAttribute(remote, "data")
        remote = self.fixLuts(remote)
        remote = remote["data"]
        for r in remote:
            uuid = r["uuid"]
            if uuid in plot_selections.keys():
                # print("plot selection exists")
                continue

            p_uuid = r["plot_selection_survey"]["survey_metadata"]["survey_details"][
                "uuid"
            ]
            if p_uuid not in plot_selection_surveys.keys():
                print("invalid plot selections")
                continue
            r["plot_selection_survey"] = plot_selection_surveys[p_uuid]
            r = self.removeFields(dict(r), "id")
            print(json.dumps(r))
            r = self.removeEmptyFields(dict(r))
            print(json.dumps(r))
            res = self.publishData(modelName="plot-selection", data=dict(r), base=self.local)
            print("push plot-selections")
            print(res)
            plot_selections[uuid] = res["data"]["id"]

        print("total plot selections ")
        print(len(plot_selections.keys()))

        ############################ plot layout #################################
        plot_layouts = {}
        remote = {}
        with open(f"{COLLECTIONS_ROOT}plot_layouts_dumb.json") as fp:
            remote = json.load(fp)
        # print(remote)
        # push plot selections
        exists = self.existingData(api="plot-layouts", base=self.local)
        exists = self.fixAttribute(exists, "attributes")
        exists = self.fixAttribute(exists, "data")
        exists = self.fixLuts(exists)
        exists = exists["data"]
        for e in exists:
            uuid = e["plot_selection"]["uuid"]
            plot_layouts[uuid] = e["id"]

        remote = self.fixAttribute(remote, "attributes")
        remote = self.fixAttribute(remote, "data")
        remote = self.fixLuts(remote)
        remote = remote["data"]
        for r in remote:
            uuid = r["plot_selection"]["uuid"]
            if uuid in plot_layouts.keys():
                # print("plot layout exists")
                continue

            if uuid not in plot_selections.keys():
                print("invalid plot layout")
                continue
            r["plot_selection"] = plot_selections[uuid]

            print("plot layout ")
            r = self.removeFields(dict(r), "id")
            r = self.removeEmptyFields(dict(r))
            res = self.publishData(modelName="plot-layout", data=dict(r), base=self.local)
            print(res["data"]["id"])
            plot_layouts[uuid] = res["data"]["id"]

        print("total plot layouts ")
        print(len(plot_layouts.keys()))

        ############################ plot visits #################################
        plot_visits = {}
        remote = {}
        with open(f"{COLLECTIONS_ROOT}plot_visits_dumb.json") as fp:
            remote = json.load(fp)
        # print(remote)
        # push plot selections
        exists = self.existingData(api="plot-visits", base=self.local)
        exists = self.fixAttribute(exists, "attributes")
        exists = self.fixAttribute(exists, "data")
        exists = self.fixLuts(exists)
        exists = exists["data"]
        for e in exists:
            # uuid = e["plot_selection"]["uuid"]
            # plot_layouts[uuid] = e["id"]
            visit_name = e["visit_field_name"]
            start_date = e["start_date"]
            plot_label = e["plot_layout"]["plot_selection"]["plot_label"]
            key = f"{visit_name}{start_date}{plot_label}"
            encrypt = hashlib.md5(key.encode("utf-8")).hexdigest()
            plot_visits[str(encrypt)] = e["id"]

        remote = self.fixAttribute(remote, "attributes")
        remote = self.fixAttribute(remote, "data")
        remote = self.fixLuts(remote)
        remote = remote["data"]
        for r in remote:
            visit_name = r["visit_field_name"]
            start_date = r["start_date"]
            plot_label = r["plot_layout"]["plot_selection"]["plot_label"]
            key = f"{visit_name}{start_date}{plot_label}"
            encrypt = hashlib.md5(key.encode("utf-8")).hexdigest()
            if str(encrypt) in plot_visits.keys():
                # print("plot visit exists")
                continue

            plot_uuid = r["plot_layout"]["plot_selection"]["uuid"]
            if plot_uuid not in plot_layouts.keys():
                print("invalid plot visit")
                continue
            r["plot_layout"] = plot_layouts[plot_uuid]

            print("plot visit ")
            r = self.removeFields(dict(r), "id")
            r = self.removeEmptyFields(dict(r))
            res = self.publishData(modelName="plot-visit", data=dict(r), base=self.local)
            print(res["data"]["id"])
            plot_visits[str(encrypt)] = res["data"]["id"]

        print("total plot visits ")
        print(len(plot_visits.keys()))

        ################################ files and images ############################
        objects = {}
        exists = self.existingData(api="upload/files", base=self.local)
        for e in exists:
            objects[e["name"]] = e["id"]
        images = list(os.listdir(IMAGES_ROOT))
        headers = {"Authorization": f"Bearer {self.localToken}"}
        for img in images:
            if "thumbnail" in img:
                continue
            if "small" in img:
                continue
            if img in objects.keys():
                continue

            ext = img.split(".")
            files = None
            with open(f"{IMAGES_ROOT}{img}", "rb") as file:
                files = {"files": (img, file, str(ext[-1]), {"uri": ""})}
                i = requests.post(
                    url=f"{self.local}/api/upload",
                    files=dict(files),
                    headers=headers,
                    timeout=20,
                )
                res = i.json()
                print(img)
                print(res[0]["id"])
                objects[img] = res[0]["id"]

        print("total files ")
        print(len(objects.keys()))

        return {
            "plot_layouts": dict(plot_layouts),
            "plot_selections": dict(plot_selections),
            "plot_visits": dict(plot_visits),
            "files": dict(objects),
        }

    def findRelationalAttributes(self, modelName):
        content = self.schema_processor.get_schema_content(
            content_type=dv.API, model_name=modelName
        )
        targets = {}
        for attribute, value in content["attributes"].items():
            if "target" in value:
                if "lut" in value["target"]:
                    continue
                model = str(value["target"]).split(".")[-1]
                targetContent = self.schema_processor.get_schema_content(
                    content_type=dv.API, model_name=model
                )
                targets[attribute] = {
                    "model": model,
                    "type": "relation",
                    "path": targetContent["info"]["pluralName"],
                }
            if "component" in value:
                if value["component"] == "vegetation.floristics-voucher":
                    targets[attribute] = {
                       "type": "component",
                       "config": "voucher"
                    }
                    
                
        return dict(targets)
    
    def modelToApiName(self, modelName):
        content = self.schema_processor.get_schema_content(
            content_type=dv.API, model_name=modelName
        )
        return content["info"]["pluralName"]
    
    def pushSurveyData(self, surveyData, plotData, survey_model=None, status={}):
        if not survey_model:
            return None
        if survey_model == "plot-selection-survey":
            print("found ################################")
            return None

        status[survey_model] = {}
        # exists
        exists = []
        exists = self.existingData(api=f"{survey_model}s", base=self.local)
        exists = self.fixAttribute(exists, "attributes")
        exists = self.fixAttribute(exists, "data")
        exists = self.fixLuts(exists)
        exists = exists["data"]

        for e in exists:
            if "survey_metadata" not in e.keys():
                continue
            if not e["survey_metadata"]["survey_details"]:
                continue
            if "uuid" not in e["survey_metadata"]["survey_details"].keys():
                continue

            e_uuid = e["survey_metadata"]["survey_details"]["uuid"]
            if not e_uuid:
                continue
            print(e["survey_metadata"])
            status[survey_model][e_uuid] = e["id"]

        data = dict(surveyData)
        uuid = data["survey_metadata"]["survey_details"]["uuid"]
        if uuid in status[survey_model].keys():
            print("collection exists nothing to do")
            return status
            # return None

        if "plot_visit" in data.keys():
            visit_name = data["plot_visit"]["visit_field_name"]
            start_date = data["plot_visit"]["start_date"]
            plot_label = data["plot_visit"]["plot_layout"]["plot_selection"][
                "plot_label"
            ]
            key = f"{visit_name}{start_date}{plot_label}"
            encrypt = hashlib.md5(key.encode("utf-8")).hexdigest()
            data["plot_visit"] = plotData["plot_visits"][str(encrypt)]

        r = self.removeFields(dict(data), "id")
        r = self.fixFiles(object=dict(r), plotData=dict(plotData))
        r = self.removeEmptyFields(dict(r))
        r = self.fixLuts(r)
        res = self.publishData(
            modelName=survey_model, 
            data=dict(r), 
            base=self.local
        )
        # print(f"response {survey_model} ")
        # print(json.dumps(r))
        # print(res["data"]["id"])
        status[survey_model][uuid] = res["data"]["id"]
        return dict(status)

    def publishCollection(
        self, modelName, modelData, plotData, survey_model=None, status={}
    ):
        skips = ["plot-visit", "plot-layout", "plot-selection"]
        if not modelData:
            return dict(status)
        
        data = modelData
        if isinstance(modelData, dict):
            data = [dict(modelData)]

        status[modelName] = {}
        targets = self.findRelationalAttributes(modelName=modelName)
        print("targets ")
        print(json.dumps(targets))
        # print(data)
        for d in data:
            # fix relations first
            for attribute, value in targets.items():
                print(attribute)
                if value["type"] == "relation":
                    if value["model"] != survey_model:
                        relationalValue = d[attribute]
                        status = self.publishCollection(
                            modelData=relationalValue,
                            modelName=value["model"],
                            plotData=plotData,
                            survey_model=None,
                            status=dict(status),
                        )
                    key = None
                    if isinstance(d[attribute], dict):
                        key = d[attribute]["id"]
                        if value["model"] == survey_model:
                            key = d[attribute]["survey_metadata"]["survey_details"]["uuid"]
                        d[attribute] = status[value["model"]][key]
                        continue

                    if isinstance(d[attribute], list):
                        ids = []
                        for v in d[attribute]:
                            ids.append(status[value["model"]][v["id"]])
                        d[attribute] = ids
                        continue
                    
                if value["type"] == "component":
                    if value["config"] == "voucher":
                        # print(d[attribute])
                        del d[attribute]["id"]
                        if d[attribute]["voucher_full"]:
                            fulls = self.existingData(api=f"floristics-veg-voucher-fulls", base=self.local)
                            barcode = d[attribute]["voucher_full"]["voucher_barcode"]
                            for f in fulls["data"]:
                                if f["attributes"]["voucher_barcode"] != barcode:
                                    continue
                                d[attribute]["voucher_full"] = f["id"]
                                
                        if d[attribute]["voucher_lite"]:
                            fulls = self.existingData(api=f"floristics-veg-voucher-lites", base=self.local)
                            barcode = d[attribute]["voucher_lite"]["voucher_barcode"]
                            for f in fulls["data"]:
                                if f["attributes"]["voucher_barcode"] != barcode:
                                    continue
                                d[attribute]["voucher_lite"] = f["id"]
                            

            existingId = d["id"]
            r = self.removeFields(dict(d), "id")
            r = self.fixFiles(object=dict(r), plotData=dict(plotData))

            # print(json.dumps(r))
            r = self.removeEmptyFields(dict(r))
            r = self.fixLuts(r)
            res = self.publishData(
                modelName=modelName, 
                data=dict(r), 
                base=self.local
            )
            
            # print(f"response {modelName}")
            # print(res)
            # print(json.dumps(r))
            print(res["data"]["id"])
            status[modelName][existingId] = res["data"]["id"]

        return dict(status)

    def pushAllCollections(self, plot_data: dict, collectionTypes: list = []):
        collections = {}
        with open(f"{COLLECTIONS_ROOT}collections_dumb.json") as fp:
            collections = json.load(fp)
            
        # can't handle
        skips = [
            # "4b8b35c7-15ef-4abd-a7b2-2f4e24509b52", # basal
            # "5005b0af-4360-4a8c-a203-b2c9e440547e", # basal
            # "a05f8914-ef4f-4a46-8cf1-d035c9c46d4d", # recruitment
            # "db841be3-dfb7-4860-9474-a131f4de5954", # Recruitment
            
            "f01e0673-a29d-48bb-b6ce-cf1c0f0de345", # Plant Tissue # need handler
            "b92005b0-f418-4208-8671-58993089f587", # Plant Tissue # need handler
            # "bbd550c0-04c5-4a8c-ae39-cc748e920fd4", # Floristics
            # "e15db26f-55de-4459-841b-d7ef87dea5cd", # Floristics
            # "a9cb9e38-690f-41c9-8151-06108caf539d", # plot selection
            "37a3b018-3779-4c4f-bfb3-d38eb53a2568", # Cover # need handler
            "93e65339-4bce-4ca1-a323-78977865ef93", # cover # need handler
            "8c47b1f8-fc58-4510-a138-e5592edd2dbc", # cover fire # need handler
            "58f2b4a6-6ce1-4364-9bae-f96fc3f86958", # cover fire # need handler
            "36e9d224-a51f-47ea-9442-865e80144311", # fire # need handler
            "7f95710a-2003-4119-a2c6-41ce4e34d12a" # Condition # need handler
        ]
        for collection in collections:
            survey_metadata = dict(collection["survey_metadata"])
            allModels = dict(collection["collections"])
            survey_model = survey_metadata["survey_details"]["survey_model"]
            org_minted_uuid = survey_metadata["org_minted_uuid"]
            protocol_id = survey_metadata["survey_details"]["protocol_id"]
            
            if protocol_id in skips: continue
            # if collectionTypes:
            #     if protocol_id not in collectionTypes: 
            #         print("collection not in the list to process >> ")
            #         print(survey_model)
            #         continue
            
            print(f"protocol name: {survey_model} minted uuid: {org_minted_uuid} >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ")
            status = self.pushSurveyData(
                surveyData=allModels[survey_model],
                survey_model=survey_model,
                plotData=dict(plot_data),
            )
            if survey_model == "plot-selection-survey":
                print("found ################################")
                continue
            # print(status)
            # if not status: continue
            for c in allModels.keys():
                if c == survey_model:
                    continue
                status = self.publishCollection(
                    modelName=c,
                    modelData=allModels[c],
                    survey_model=survey_model,
                    plotData=dict(plot_data),
                    status=status,
                )

            data = {
                "org_minted_uuid": org_minted_uuid,
                "survey_details": dict(survey_metadata["survey_details"]),
                "provenance": dict(survey_metadata["provenance"])
            }
            data = self.removeFields(dict(data), "id")
            data = self.removeEmptyFields(dict(data))
            res = self.publishData(modelName="org-uuid-survey-metadata" , data=dict(data), base=self.local)
            print("response org-uuid-survey-metadata ")


def main():
    args = sys.argv
    print(f"local {args[1]}")
    print(f"local token {args[3]}")
    print(f"remote {args[2]}")
    print(f"remote token {args[3]}")
    loader = SyncCollection(
        local=args[1], localToken=args[2], remote=args[3], remoteToken=args[4]
    )
    loader.collectionBackUp()
    plot_data = loader.pushPlotCollections()
    loader.pushAllCollections(plot_data=plot_data, collectionTypes=COLLECTION_TYPES["VOUCHER"])

if __name__ == "__main__":
    main()

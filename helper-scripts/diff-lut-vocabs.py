import dummyValuesBasedOnSchema as dv
from dawe_vocabs import DaweVocabs, LUTSchema
from SparqlQuery import Query
from attributes_all_protocols import AttributesAllProtocol
import inflect
from pattern.en import pluralize
import os
import json
import csv

CORE_SOURCE = "paratoo-core/src"
ORG_SOURCE = "paratoo-org/src"

DCCEEW_RLP_SURVEY_PROTOCOLS = (
    "https://linked.data.gov.au/def/nrm/e8e10807-baea-4c9b-8d1c-d77ced9df1e9"
)

# flag to enable updating uri
UPDATE_PROTOCOLS_URI = False
UPDATE_LUTS_URI = True

OUTPUT_PROTOCOL_ATTRIBUTES = "attributes_all_protocol.csv"
OUTPUT_LUT_DIFF = "lut_diff_vocabs.csv"


class LutVocabs:
    def __init__(self, source_path: str):
        self.source_path = source_path

        # core or org
        self.auto_values = dv.DummyValuesBasedOnSchema(self.source_path, dv.INIT_DATA)
        self.daweVocabs = DaweVocabs()
        self.queryVocabs = Query()
        self.inflect = inflect.engine()

        self.lutConfigs = self.daweVocabs.getLutConfigs()

    def findRelevantProtocols(self, relations: list, model: str):
        protocols = ""
        for r in relations:
            if not r["relation"]:
                continue
            if r["relation"] == "":
                continue
            if r["relation"] != model:
                continue

            if len(protocols) > 1:
                protocols += ", "
            protocols += r["protocol"]
            # relations.append(r["pro"])
        return protocols

    def endpointToConfig(self, endpoint):
        for o in self.lutConfigs:
            if o.endpoint_url != endpoint:
                continue
            return o

    def isMatch(self, label1, label2):
        l1, l2 = label1.lower().replace(" ", ""), label2.lower().replace(" ", "")
        if l1 == l2:
            return True

        # checks label1 1 plural
        if len(label1) <= 10 and self.inflect.plural(label1) == label2:
            return True
        if len(label2) <= 10 and self.inflect.plural(label2) == label1:
            return True
        return False

    def findValue(self, values, symbol, label):
        for v in values:
            if not self.isMatch(label1=label, label2=v["label"]):
                continue
            return dict(v)
        return None

    def nrmConfigs(self, endpoint):
        config = self.endpointToConfig(endpoint=endpoint)
        collectionUri = config.collection_url
        if "linked.data.gov.au" not in collectionUri:
            collectionUri = f"https://linked.data.gov.au/def/nrm/{collectionUri}"
        lutValueConfigs = self.queryVocabs.allValuesUsingSparqlQuery(
            collectionUri=collectionUri
        )
        if not lutValueConfigs:
            return {"values": None, "lut_uri": collectionUri}
        values = []
        for l in lutValueConfigs:
            concept = self.queryVocabs.sparql_query_all(l["concept"]["value"])
            value = {}
            value["label"] = l["label"]["value"]
            value["uri"] = l["concept"]["value"]
            for c in concept:
                if "title" not in c:
                    continue
                if "definition" not in c["title"]["value"]:
                    continue
                value["definition"] = c["value"]["value"]
            values.append(dict(value))
        return {"values": values, "lut_uri": collectionUri}

    def getValueByIndex(self, listObject, index):
        if len(listObject) <= index:
            return "No Record"
        return listObject[index]

    def generateDiff(self):
        # all configs in linked data
        nrms = [o.endpoint_url for o in self.lutConfigs]
        attributesAllProtocol = AttributesAllProtocol(
            source_path=CORE_SOURCE, out_file=OUTPUT_PROTOCOL_ATTRIBUTES
        )
        # all attributes
        attributesAllProtocol.generateRelations()
        relations = []
        with open(OUTPUT_PROTOCOL_ATTRIBUTES) as csvfile:
            reader_obj = csv.DictReader(csvfile)
            for row in reader_obj:
                relations.append(dict(row))

        self.updateLutsURI(nrms=nrms, relations=relations)

    def updateLutsURI(self, nrms, relations: list, update_uri: bool = False):
        # update all luts
        all_models = self.auto_values.get_all_models(including_luts=True)
        all_models.sort()
        luts = []
        with open(OUTPUT_LUT_DIFF, "w", newline="") as csvfile:
            fieldnames = [
                "lut",
                "relevant-protocols",
                "lut-uri",
                "init-data-index",
                "member-uri",
                "monitor-symbol/label",
                "linkeddata-label",
                "monitor-definition",
                "linkeddata-definition",
            ]
            writer = csv.DictWriter(
                csvfile,
                fieldnames=fieldnames,
                extrasaction="ignore",
            )
            writer.writeheader()
            count = 0
            for model in all_models:
                # if model != "lut-floristics-habit": continue
                if "lut" not in model:
                    continue
                content = self.auto_values.get_schema_content(
                    content_type=dv.API, model_name=model
                )

                content_keys = content.keys()
                if "info" not in content_keys or len(content_keys) == 0:
                    continue

                info_keys = content["info"].keys()
                if "pluralName" not in info_keys or len(info_keys) == 0:
                    continue

                pluralName = content["info"]["pluralName"]

                if "lut" not in pluralName.lower():
                    continue
                attributes = list(content["attributes"])
                symbolIndex = attributes.index("symbol")
                labelIndex = attributes.index("label")
                uriIndex = attributes.index("uri")
                dscIndex = attributes.index("description")

                relevantProtocols = self.findRelevantProtocols(
                    relations=relations, model=model
                )

                endpoint = f"https://core.vocabs.paratoo.tern.org.au/api/{pluralName}"
                print(model)
                count += 1
                nrmValues = None
                lutUri = None
                if endpoint in nrms:
                    nrmValues = dict(self.nrmConfigs(endpoint=endpoint))
                    lutUri = nrmValues["lut_uri"]
                    content["info"]["uri"] = lutUri

                initialData = content["initialData"]

                for idx, x in enumerate(initialData):
                    if not nrmValues:
                        writer.writerow(
                            {
                                "lut": model,
                                "relevant-protocols": relevantProtocols,
                                "lut-uri": lutUri if lutUri else "No Record",
                                "init-data-index": idx,
                                "member-uri": "No Record",
                                "monitor-symbol/label": f"{self.getValueByIndex(x, symbolIndex)}/{self.getValueByIndex(x, labelIndex)}",
                                "linkeddata-label": "No Record",
                                "monitor-definition": self.getValueByIndex(x, dscIndex),
                                "linkeddata-definition": "No Record",
                            }
                        )
                        continue
                    if not nrmValues["values"]:
                        writer.writerow(
                            {
                                "lut": model,
                                "relevant-protocols": relevantProtocols,
                                "lut-uri": lutUri if lutUri else "No Record",
                                "init-data-index": idx,
                                "member-uri": "GraphDb returns error",
                                "monitor-symbol/label": f"{self.getValueByIndex(x, symbolIndex)}/{self.getValueByIndex(x, labelIndex)}",
                                "linkeddata-label": "GraphDb returns error",
                                "monitor-definition": self.getValueByIndex(x, dscIndex),
                                "linkeddata-definition": "GraphDb returns error",
                            }
                        )
                        continue

                    nrmValue = None
                    definition = None
                    if nrmValues:
                        nrmValue = self.findValue(
                            values=nrmValues["values"],
                            symbol=x[symbolIndex],
                            label=x[labelIndex],
                        )
                        if nrmValue:
                            nrmValue = dict(nrmValue)
                            content["initialData"][idx][uriIndex] = nrmValue["uri"]
                            if "definition" in nrmValue:
                                definition = nrmValue["definition"]
                                if not content["initialData"][idx][dscIndex]:
                                    content["initialData"][idx][dscIndex] = nrmValue[
                                        "definition"
                                    ]

                    if not nrmValue:
                        nrmValue = {"uri": "No Record", "label": "No Record"}

                    if str(x[dscIndex]) != definition:
                        writer.writerow(
                            {
                                "lut": model,
                                "relevant-protocols": relevantProtocols,
                                "lut-uri": nrmValues["lut_uri"],
                                "init-data-index": idx,
                                "member-uri": nrmValue["uri"],
                                "monitor-symbol/label": f"{self.getValueByIndex(x, symbolIndex)}/{self.getValueByIndex(x, labelIndex)}",
                                "linkeddata-label": nrmValue["label"],
                                "monitor-definition": self.getValueByIndex(x, dscIndex),
                                "linkeddata-definition": (
                                    definition if definition else "No Record"
                                ),
                            }
                        )
                if update_uri:
                    self.updateSchemaContent(model_name=model, schema_content=content)

            print(f"Total luts {count}")
        return luts

    def updateSchemaContent(self, model_name: str, schema_content: dict):
        model_path = f"./{self.source_path}/{dv.API}/{model_name}/content-types/{model_name}/schema.json"
        if not os.path.exists(os.path.dirname(model_path)):
            print(model_path + " not found to store init data")
            return

        print("saving new " + model_name)

        with open(model_path, "w", encoding="utf-8") as f:
            json.dump(schema_content, f, ensure_ascii=False, indent=2)

        print("model path: " + model_path)


def main():
    lutVocabs = LutVocabs(source_path=CORE_SOURCE)
    lutVocabs.generateDiff()


if __name__ == "__main__":
    main()

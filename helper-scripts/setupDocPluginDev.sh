#!/bin/bash

# Pack local version of documentation plugin

cd ~/
git clone git@github.com:ternandsparrow/strapi.git -b michael
cd strapi/packages/strapi-plugin-documentation
npm pack
readlink -f strapi-plugin-documentation*


#!/bin/bash
# checks if the protocol models are in-sync between the core and org projects.
set -euxo pipefail
cd `dirname "$0"`
thisDir=$PWD
cd ..

trap "echo 'see $thisDir/sync-protocol-models.sh to fix'" ERR

# routes can be deffirent as we have api/protocols/collections in core 
#   to reverse-lookup collections(for MERIT) using collections identifier
diff --brief --recursive paratoo-{org,core}/src/api/protocol/content-types
echo "Success! Models are in sync."

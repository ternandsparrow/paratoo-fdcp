#!/bin/bash
set -euo pipefail
cd `dirname "$0"`
cd ..

echo "WARNING: this will sync the paratoo-core definition into paratoo-org,
which will be clobbered. If you have modified paratoo-webapp, DO NOT continue as
those changes will be lost."
read -p "Press any key to continue, or Control-c to cancel " -n 1 -r

rm -fr paratoo-webapp/src/misc/customRules.js
cp -vr paratoo-core/src/customRules.js paratoo-webapp/src/misc/customRules.js

echo "Done"

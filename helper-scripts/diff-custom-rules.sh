#!/bin/bash
# checks if the custom validation rules are consistent between core and web app
set -euxo pipefail

cd `dirname "$0"`
thisDir=$PWD

trap "echo 'diff-custom-rules error'" ERR

diff ../paratoo-webapp/src/misc/customRules.js ../paratoo-core/src/customRules.js
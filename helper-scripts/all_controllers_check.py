import dummyValuesBasedOnSchema as dv
import os
import json
import requests
from typing import Optional, List, Union

CORE_SOURCE = "paratoo-core/src"
ORG_SOURCE = "paratoo-org/src"

SKIPS = [
    "reserved-plot-label",
    "paratoo-helper",
    "generate-barcode",
    "org-uuid-survey-metadata",
    "debug-client-state-dump",
    "grassy-weeds-export",
    "protocol",
    "analytics",
]


class CheckApiControllers:
    def __init__(self, source_path: str):
        self.source_path = source_path

        self.auto_values = dv.DummyValuesBasedOnSchema(self.source_path, dv.INIT_DATA)

    def trimmed_content(self, content: str):
        texts = content.replace("\t", "")
        texts = texts.replace("\n", "")
        texts = texts.replace(" ", "")
        return texts

    def update_controllers(self):
        models = self.auto_values.get_all_models(including_luts=True)
        # process_models = ["sign-based-vehicle-track-log"]
        # skips = ["protocol", "plot-visit", "plot-layout"]
        find_function = f"\n    /* Generic find controller with pdp data filter */\n    async find(ctx) {{\n      const helpers = strapi.service('api::paratoo-helper.paratoo-helper')\n      \n      const authToken = ctx.request.headers['authorization']\n      const modelName = ctx.state.route.info.apiName\n      const tokenStatus = await helpers.apiTokenCheck(authToken)\n      const useDefaultController = ctx.request.query['use-default'] === 'true'\n      \n      // if we need to use default controller (use magic jwt and use-default flag)\n      if (tokenStatus?.type == 'full-access' && useDefaultController) {{\n        helpers.paratooDebugMsg(`Default controller is used to populate model: ${{modelName}}`)\n        return await super.find(ctx)\n      }}\n      return await helpers.pdpDataFilter(\n        modelName,\n        authToken,\n        ctx.request.query,\n        tokenStatus,\n      )\n    }},\n"
        count = 0
        all = 0

        for model in models:

            # if model not in process_models: continue
            if model in SKIPS:
                continue
            if "lut" in model:
                continue
            file_path = f"./{self.source_path}/api/{model}/controllers/{model}.js"

            # if file does'nt exit
            if not file_path or not os.path.exists(os.path.dirname(file_path)):
                continue
            core_controller = "createCoreController"
            content = None
            trimmed = ""
            new_content = None
            file = open(file_path, "r")
            content = file.read()
            trimmed = self.trimmed_content(content=content)

            if core_controller not in content:
                continue
            total = trimmed.count(core_controller)
            all = all + 1
            if total > 2:
                print(
                    f"cant able to string match, found multiple createCoreController, controller: {file_path}"
                )
                continue
            if "asyncfind(ctx)" in trimmed:
                if "Generic find controller with pdp data filter" not in content:
                    print(
                        f"find controller exists but not the generic one with pdp filter, controller: {file_path}"
                    )
                continue
            api = f"'api::{model}.{model}'"

            # 1. default controllers eg. 'createCoreController('api::add-malaise-sample.add-malaise-sample')'
            if f"({api})" in trimmed:
                replace_str = f"{api},\n  ({{ strapi }}) => ({{{find_function}  }}),\n"
                new_content = content.replace(api, replace_str)

            if new_content:
                with open(file_path, "w") as f:
                    count = count + 1
                    print("default controller found ")
                    print(file_path)
                    f.write(new_content)
                continue

            # 2. default controllers with extra comma eg. 'createCoreController('api::add-malaise-sample.add-malaise-sample')'
            if f"({api},)" in trimmed:
                replace_str = f"{api},\n  ({{ strapi }}) => ({{{find_function}  }}),"
                new_content = content.replace(f"{api},", replace_str)

            if new_content:
                with open(file_path, "w") as f:
                    count = count + 1
                    print("default controller with extra comma found ")
                    print(file_path)
                    f.write(new_content)
                continue

            # 3. controllers with other handlers eg. bulk or many
            if "({strapi})=>({" in trimmed:
                # add more if need to
                possible_maches = [
                    "({ strapi }) => ({",
                    "({strapi }) => ({",
                    "({ strapi}) => ({",
                    "({ strapi })=> ({",
                    "({ strapi }) =>({",
                    "({strapi}) => ({",
                ]
                matched = None
                for p in possible_maches:
                    if p in content:
                        matched = p
                        break

                if matched:
                    replace_str = f"{matched}{find_function}"
                    new_content = content.replace(f"{matched}", replace_str)

            if new_content:
                with open(file_path, "w") as f:
                    count = count + 1
                    print("controllers with other handlers")
                    print(file_path)
                    f.write(new_content)
                continue

        print(f"total file changed {str(count)} out of {all}")

    def check_all_get_requests(self):
        models = self.auto_values.get_all_models(including_luts=True)

        for model in models:

            # if model not in process_models: continue
            if "lut" in model:
                continue
            if model in SKIPS:
                continue
            content = self.auto_values.get_schema_content(
                content_type=dv.API, model_name=model
            )
            if not content:
                continue
            plural_name = content["info"]["pluralName"]
            headers = {"Authorization": "Bearer 1234"}
            api = f"http://localhost:1337/api/{plural_name}"
            r = requests.get(api, headers=headers, timeout=10)
            full_doc = r.json()
            value_type = str(type(full_doc))
            if "list" not in value_type:
                print(f"pdp filter is not working in model: {model}")
                print(r)
                print(full_doc)

def main():
    check_api_controllers = CheckApiControllers(source_path=CORE_SOURCE)
    check_api_controllers.update_controllers()
    # check_api_controllers.check_all_get_requests()


if __name__ == "__main__":
    main()

yarn strapi generate:api "LUT Soils Evaluation Means" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Slope Class" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Morphological Type" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Relative Inclination of Slope Elements" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Landform Pattern" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Relief" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Modal Slope" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Landform Element" symbol:string label:string description:text uri:string
# yarn strapi generate:api "LUT Soils Erosion Type" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Wind Erosion Degree" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Scald Erosion Degree" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Sheet Water Erosion Degree" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Rill Water Erosion Degree" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Gully Water Erosion Degree" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Tunnel Water Erosion Degree" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Stream Bank Water Erosion Degree" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Wave Water Erosion Degree" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Mass Movement Erosion Degree" symbol:string label:string description:text uri:string
# yarn strapi generate:api "LUT Soils Erosion State" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Gully Depth" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Microrelief Type" symbol:string label:string description:text uri:string
# yarn strapi generate:api "LUT Soils Gilgal Microreliefs" symbol:string label:string description:text uri:string
# yarn strapi generate:api "LUT Soils Hummocky Microreliefs" symbol:string label:string description:text uri:string
# yarn strapi generate:api "LUT Soils Other Microreliefs" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Gilgal Proportion" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Biotic Relief Agent" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Microrelief Component" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Runoff" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Permeability" symbol:string label:string description:text uri:string
# yarn strapi generate:api "LUT Soils Drainage Likely" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Disturbance" symbol:string label:string description:text uri:string
# yarn strapi generate:api "LUT Soils Coarse Frag Abundance" symbol:string label:string description:text uri:string
# yarn strapi generate:api "LUT Soils Coarse Frag Size" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Coarse Frag Shape" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Coarse Frag Strength" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Coarse Frag Alteration" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Rock Outcrop Abundance" symbol:string label:string description:text uri:string
# LUT Soils Surface Condition
# LUT Soils Observation Type
yarn strapi generate:api "LUT Soils Digging Stopped By" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils O Horizon" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils P Horizon" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils A Horizon" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils B Horizon" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils C Horizon" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils D Horizon" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils R Horizon" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Horizon Suffix" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Horizon Boundary Distinctness" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Horizon Boundary Shape" symbol:string label:string description:text uri:string
# LUT Soils Texture Grade
yarn strapi generate:api "LUT Soils Texture Modifier" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Texture Qualification" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Moisture Status" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Mottle Type" symbol:string label:string description:text uri:string
# LUT Soils Mottle Abundance
# yarn strapi generate:api "LUT Soils Mottle Size" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Mottle Contrast" symbol:string label:string description:text uri:string
# yarn strapi generate:api "LUT Soils Mottle Colour" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Mottle Boundary Distinctness" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Structure Grade" symbol:string label:string description:text uri:string
# yarn strapi generate:api "LUT Soils Structure Size" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Structure Type" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Structure Compound Pedality" symbol:string label:string description:text uri:string
# LUT Soils Segregation Abundance
# LUT Soils Segregation Nature
# LUT Soils Segregation Form
# LUT Soils Segregation Size
yarn strapi generate:api "LUT Soils Segregation Strength" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Segregation Magnetic Attributes" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Void Cracks" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Fine Macropore Abundance" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Medium Macropore Abundance" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Macropore Diameter" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Water Status" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Soil Strength" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Pan Type" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Pan Cementation" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Pan Continuity" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Pan Structure" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Cutan Type" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Cutan Abundance" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Cutan Distinctness" symbol:string label:string description:text uri:string
# yarn strapi generate:api "LUT Soils Effervescence" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Field Dispersion Score" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Field Slaking Score" symbol:string label:string description:text uri:string
yarn strapi generate:api "LUT Soils Confidence Level" symbol:string label:string description:text uri:string
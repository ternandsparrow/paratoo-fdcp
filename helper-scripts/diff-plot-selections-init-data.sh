#!/bin/bash
# checks if the plot-selections initial data in core and org are the same
set -euxo pipefail

cd `dirname "$0"`
thisDir=$PWD

trap "echo 'diff-plot-selections-init-data error'" ERR

diff <(jq '.initialData' ../paratoo-org/src/api/plot-selection/content-types/plot-selection/schema.json) <(jq '.initialData' ../paratoo-core/src/api/plot-selection/content-types/plot-selection/schema.json)
diff <(jq '.initialData' ../paratoo-org/src/api/plot-selection/content-types/plot-selection-survey/schema.json) <(jq '.initialData' ../paratoo-core/src/api/plot-selection/content-types/plot-selection-survey/schema.json)
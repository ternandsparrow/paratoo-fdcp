import dummyValuesBasedOnSchema as dv
from dawe_vocabs import DaweVocabs, LUTSchema
from SparqlQuery import Query
import os
import json
import requests
import re
from typing import Optional, List, Union
import csv
import uuid
import copy
import base64

CORE_SOURCE = "paratoo-core/src"
ORG_SOURCE = "paratoo-org/src"

RESOURCE_PATH = "https://raw.githubusercontent.com/ternaustralia/dawe-rlp-vocabs/main/src/dawe_nrm/api/categorical_values/endpoints.py"

allLutsInfo = {}

vocab_doc = "https://dev.core-api.monitor.tern.org.au/api/documentation/swagger.json"
# vocab_doc = "http://localhost:1337/api/documentation/swagger.json"

third_sets = [
    "90c0f4cc-a22a-4820-9a8b-a01564bc197a",
    "648d545a-cdae-4c19-bc65-0c9f93d9c0eb",
    "2cd7b489-b582-41f6-9dcc-264f6ea7801a",
    "cc826a19-a1e7-4dfe-8d6e-f135d258d7f9",
    "0c5d1d14-c71b-467f-aced-abe1c83c15d3",
    "685b5e9b-20c2-4688-9b04-b6caaf084aad",
    "a76dac21-94f4-4851-af91-31f6dd00750f",
    "d706fd34-2f05-4559-b738-a65615a3d756",
    "80360ceb-bd6d-4ed4-b2ea-9bd45d101d0e",
    "06cd903e-b8b3-40a5-add4-f779739cce35",
    "49d02f5d-b148-4b5b-ad6a-90e48c81b294",
    "228e5e1e-aa9f-47a3-930b-c1468757f81d",
]


class URILoader:
    def __init__(self, source_path: str, resource_path: str):
        self.source_path = source_path
        self.resource_path = resource_path

        self.auto_values = dv.DummyValuesBasedOnSchema(self.source_path, dv.INIT_DATA)
        self.queryVocabs = Query()

    def replaceRefs(self, properties, full_doc):
        fields = list(properties.keys())
        for field in fields:
            keys = list(properties[field].keys())
            if "$ref" in keys:
                m = properties[field]["$ref"].split("/")[-1]
                properties[field] = full_doc["components"]["schemas"][m]
                continue

            if properties[field]["type"] == "array":
                keys = list(properties[field]["items"].keys())
                if "$ref" in keys:
                    m = properties[field]["items"]["$ref"].split("/")[-1]
                    properties[field]["items"] = full_doc["components"]["schemas"][m]

        return properties

    def csvDumpFields(
        self, attributes, protocol, plotBased, model, writer, prefix=None
    ):
        attrs = list(attributes.keys())
        for attr in attrs:
            fieldType = attributes[attr]["type"]
            relation = ""
            component = ""
            if "target" in attributes[attr]:
                relation = attributes[attr]["target"].split(".")[-1]
            if "component" in attributes[attr]:
                component = attributes[attr]["component"]

            if relation and "lut" not in relation:
                fieldType = "integer"
            if "lut" in relation:
                fieldType = "enum"
            if component:
                fieldType = "json"
            if prefix:
                attr = f"{prefix}/{attr}"
            writer.writerow(
                {
                    "protocol": protocol,
                    "plotBased": plotBased,
                    "model": model,
                    "field": attr,
                    "fieldType": fieldType,
                    "relation": relation,
                    "component": component,
                }
            )
            if component:
                content = self.auto_values.get_schema_content(
                    content_type=dv.COMPONENT, component=component
                )
                self.csvDumpFields(
                    attributes=content["attributes"],
                    protocol=protocol,
                    plotBased=plotBased,
                    model=model,
                    writer=writer,
                    prefix=attr,
                )

    def updateTestMock(self):
        path = "paratoo-core/tests/floristics/test-data.json"
        data = json.load(open(path))
        temp = copy.deepcopy(data)
        keys = list(data.keys())
        for key in keys:
            if "data" not in data[key]:
                continue
            if "collections" not in data[key]["data"]:
                continue
            if "orgMintedIdentifier" not in data[key]["data"]["collections"][0]:
                continue
            models = list(data[key]["data"]["collections"][0].keys())

            for model in models:
                if "data" not in data[key]["data"]["collections"][0][model]:
                    continue
                if (
                    "survey_metadata"
                    not in data[key]["data"]["collections"][0][model]["data"]
                ):
                    continue
                survey = copy.deepcopy(
                    data[key]["data"]["collections"][0][model]["data"][
                        "survey_metadata"
                    ]
                )
                s = {
                    "survey_metadata": survey,
                    "userId": 1,
                    "eventTime": "2023-11-16T07:45:46.830Z",
                }
                encoded = base64.urlsafe_b64encode(json.dumps(s).encode()).decode()
                temp[key]["data"]["collections"][0]["orgMintedIdentifier"] = encoded
        with open(path, "w", encoding="utf-8") as f:
            json.dump(temp, f, ensure_ascii=False, indent=2)

    def findProtocolUuid(self, model: str):
        content = self.auto_values.get_schema_content(
            content_type=dv.API, model_name="protocol"
        )
        attributes = list(content["attributes"].keys())
        uuidIndex = attributes.index("identifier")
        workflowIndex = attributes.index("workflow")
        for initialData in content["initialData"]:
            models = []
            relations = []
            for workflow in initialData[workflowIndex]:
                models.append(workflow["modelName"])
                if "relationOnAttributesModelNames" in workflow:
                    models = models + workflow["relationOnAttributesModelNames"]
            if model in models:
                return initialData[uuidIndex]

    def replaceSurveyId(self):
        models = self.auto_values.get_all_models(including_luts=False)
        for model in models:
            content = dict(
                self.auto_values.get_schema_content(
                    content_type=dv.API, model_name=model
                )
            )
            if "lut" in model:
                continue

            if "attributes" not in content:
                continue
            attributes = list(content["attributes"].keys())
            if "surveyId" not in attributes:
                continue
            survey_metadata_attr = {
                "type": "component",
                "repeatable": False,
                "component": "survey-metadata.metadata",
                "x-paratoo-required": True,
            }
            survey_metadata = {
                "survey_details": {
                    "survey_model": model,
                    "time": "2022-09-14T01:39:21.635Z",
                    "uuid": "",
                    "project_id": "1",
                    "protocol_id": self.findProtocolUuid(model),
                    "protocol_version": "1",
                },
                "provenance": {
                    "version_app": "0.0.1-xxxxx",
                    "version_core": "0.0.1-xxxxx",
                    "version_core_documentation": "0.0.1-xxxxx",
                    "version_org": "4.4-SNAPSHOT",
                    "system_app": "monitor",
                    "system_core": "Monitor FDCP API",
                    "system_org": "monitor",
                },
            }

            surveyIdIndex = attributes.index("surveyId")
            print(model)
            del content["attributes"]["surveyId"]
            content["attributes"]["survey_metadata"] = survey_metadata_attr

            if "initialData" in content:
                initialData = content["initialData"].copy()
                for idx, x in enumerate(initialData):
                    survey_id = dict(x[surveyIdIndex])
                    metadata = copy.deepcopy(survey_metadata)
                    if "surveyType" in survey_id:
                        metadata["survey_details"]["survey_model"] = survey_id[
                            "surveyType"
                        ]
                    if "time" in survey_id:
                        metadata["survey_details"]["time"] = survey_id["time"]
                    if "uuid" in survey_id:
                        metadata["survey_details"]["uuid"] = survey_id["uuid"]
                    if "project" in survey_id:
                        metadata["survey_details"]["project_id"] = str(
                            survey_id["project"]
                        )
                    metadata["survey_details"]["uuid"] = str(uuid.uuid4())

                    del content["initialData"][idx][surveyIdIndex]
                    content["initialData"][idx].append(copy.deepcopy(metadata))
                    del metadata

            self.add_update_uri(model_name=model, schema_content=content)

    def generateProtocolRelations(self):
        doc = requests.get(vocab_doc)
        full_doc = doc.json()
        requestsBodies = full_doc["paths"]
        content = self.auto_values.get_schema_content(
            content_type=dv.API, model_name="protocol"
        )
        attributes = list(content["attributes"].keys())
        uuidIndex = attributes.index("identifier")
        workflowIndex = attributes.index("workflow")
        endpointIndex = attributes.index("endpointPrefix")
        nameIndex = attributes.index("name")
        with open("relations.csv", "w", newline="") as csvfile:
            fieldnames = [
                "protocol",
                "plotBased",
                "model",
                "field",
                "fieldType",
                "relation",
                "component",
            ]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            # for protocol in third_sets:
            #     print(protocol)
            skips = [
                "a9cb9e38-690f-41c9-8151-06108caf539d",
                "3cbc5277-45fb-4e7a-8f33-19d9bff4cd78",
                "3d2eaa76-a610-4575-ac30-abf40e57b68a",
                "0628e486-9b33-4d86-98db-c6d3f10f7744",
                "aa64fd4d-2c5a-4f84-a197-9f3ce6409152",
                "7405bd3c-5811-4d9b-aacc-9d279a613b16",
                "aae4dbd8-845a-406e-b682-ef01c3497711",
                "75600f67-49f2-4c9a-98af-9cde2d020680",
                "9a0aab22-19b8-49e9-b0fa-262b4cf58e8e"
            ]
            for initialData in content["initialData"]:
                # if initialData[uuidIndex] != protocol:
                #     continue
                if initialData[uuidIndex] in skips: continue
                
                print(initialData[nameIndex])
                models = []
                relations = []
                for workflow in initialData[workflowIndex]:
                    models.append(workflow["modelName"])
                    if "relationOnAttributesModelNames" in workflow:
                        relations = (
                            relations + workflow["relationOnAttributesModelNames"]
                        )
                
                endpointPostfix = "bulk"
                if initialData[uuidIndex] == "a9cb9e38-690f-41c9-8151-06108caf539d":
                    endpointPostfix = "many"
                    
                body = requestsBodies[f"{initialData[endpointIndex]}/{endpointPostfix}"]["post"][
                    "requestBody"
                ]
                # print(json.dumps(body))
                properties = body["content"]["application/json"]["schema"][
                    "properties"
                ]
                if endpointPostfix == "bulk":
                    properties = properties["data"]["properties"]["collections"]["items"]["properties"]

                properties = self.replaceRefs(properties, full_doc)
                plotBased = False
                if "plot-layout" in models or "plot-visit" in models:
                    plotBased = True
                for m in models:
                    model_content = self.auto_values.get_schema_content(
                        content_type=dv.API, model_name=m
                    )
                    self.csvDumpFields(
                        attributes=model_content["attributes"],
                        protocol=initialData[nameIndex],
                        plotBased=plotBased,
                        model=m,
                        writer=writer,
                    )

                for m in relations:
                    model_content = self.auto_values.get_schema_content(
                        content_type=dv.API, model_name=m
                    )
                    self.csvDumpFields(
                        attributes=model_content["attributes"],
                        protocol=initialData[nameIndex],
                        plotBased=plotBased,
                        model=m,
                        writer=writer,
                    )

    def download_resources(self):
        file = requests.get(self.resource_path)
        lutsSchemaRaw = file.text.split("LUTSchema(")

        for schema in lutsSchemaRaw:
            v = self.findValue(key="endpoint_url", source=schema)
            if not v:
                continue
            lut = v.split("https://core.vocabs.paratoo.tern.org.au/api/")[-1]
            obj = {}
            obj["label"] = self.findValue(key="label", source=schema)
            obj["description"] = self.findValue(key="description", source=schema)
            obj["collection_uuid"] = self.findValue(
                key="collection_uuid", source=schema
            )
            obj["uuid_namespace"] = self.findValue(key="uuid_namespace", source=schema)
            allLutsInfo[lut] = obj

    def findValue(self, key: str, source: str):
        pattern = f"{key}=.*,"
        data = re.findall(pattern, source)
        if len(data) == 0:
            return None

        value = re.findall('"(.*?)"', data[0])
        return value[0]

    def csv_dump(self):
        models = self.auto_values.get_all_models(including_luts=True)
        with open("luts.csv", "w", newline="") as csvfile:
            fieldnames = ["lut", "symbol", "label", "description", "uri"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()

            for model in models:
                content = self.auto_values.get_schema_content(
                    content_type=dv.API, model_name=model
                )
                if "lut" not in model:
                    continue
                content_keys = content.keys()
                # set init data uri
                if "initialData" not in content_keys:
                    continue
                initialData = content["initialData"]
                if not initialData:
                    continue
                attributes = list(content["attributes"].keys())

                symbolIndex = attributes.index("symbol")
                labelIndex = attributes.index("label")
                uriIndex = attributes.index("uri")
                dscIndex = attributes.index("description")

                for idx, x in enumerate(initialData):
                    row = content["initialData"][idx]
                    symbol = row[symbolIndex]
                    label = row[labelIndex]
                    uri = row[uriIndex] if len(row) - 1 >= uriIndex else ""
                    dsc = row[dscIndex] if len(row) - 1 >= dscIndex else ""

                    writer.writerow(
                        {
                            "lut": model,
                            "symbol": symbol,
                            "label": label,
                            "description": dsc,
                            "uri": uri,
                        }
                    )

    def refresh_luts_attributes(self):
        models = self.auto_values.get_all_models(including_luts=True)
        for model in models:
            if "lut" not in model:
                continue
            content = self.auto_values.get_schema_content(
                content_type=dv.API, model_name=model
            )

            if "attributes" not in content:
                continue
            content["attributes"]["symbol"] = {
                "type": "string",
                "required": True,
                "unique": True,
            }
            content["attributes"]["label"] = {
                "type": "string",
                "required": True,
                "unique": True,
            }
            content["attributes"]["description"] = {
                "type": "text",
            }
            content["attributes"]["uri"] = {
                "type": "string",
            }
            self.add_update_uri(model_name=model, schema_content=content)
            
    def remove_luts_duplicate_init_data(self):
        models = self.auto_values.get_all_models(including_luts=True)
        for model in models:
            if "lut" not in model:
                continue
            content = self.auto_values.get_schema_content(
                content_type=dv.API, model_name=model
            )

            if "initialData" not in content:
                continue
            initialData = content["initialData"]
            if not initialData:
                continue
            attributes = list(content["attributes"].keys())

            symbolIndex = attributes.index("symbol")
            labelIndex = attributes.index("label")
            uriIndex = attributes.index("uri")
            dscIndex = attributes.index("description")
            
            symbols = []
            for idx, x in enumerate(initialData):
                if x[symbolIndex] in symbols:
                    print(f"lut >> {model} duplicate symbol  >> {x[symbolIndex]} index {idx}")
                    continue
                symbols.append(x[symbolIndex])
                
            labels = []
            for idx, x in enumerate(initialData):
                if x[labelIndex] in labels:
                    print(f"lut >> {model} duplicate label  >> {x[labelIndex]} index {idx}")
                    continue
                labels.append(x[labelIndex])
            

    def refresh_luts_init_data_uri(self, update_uri: bool, update_desc: bool):
        models = self.auto_values.get_all_models(including_luts=True)
        for model in models:
            content = self.auto_values.get_schema_content(
                content_type=dv.API, model_name=model
            )
            if "lut" not in model:
                continue
            if "info" not in content:
                continue
            pluralName = content["info"]["pluralName"]
            if pluralName not in allLutsInfo:
                continue
            # if model != "lut-soils-tunnel-water-erosion-degree":
            #     continue
            print("processing " + model)
            uuid = allLutsInfo[pluralName]["collection_uuid"]
            uri = f"https://linked.data.gov.au/def/nrm/{uuid}"
            self.loadLutURIS(
                model=model,
                content=content,
                info_uri=uri,
                update_uri=update_uri,
                update_desc=update_desc,
            )

    def refresh_luts_init_data(self):
        models = self.auto_values.get_all_models(including_luts=True)
        for model in models:
            if model != "lut-soil-asc-family-detail":
                continue
            content = self.auto_values.get_schema_content(
                content_type=dv.API, model_name=model
            )
            
            if "initialData" not in content:
                continue
            initialData = content["initialData"]
            if not initialData:
                continue
            attributes = list(content["attributes"].keys())

            # symbolIndex = attributes.index("symbol")
            # labelIndex = attributes.index("label")
            # uriIndex = attributes.index("uri")
            # dscIndex = attributes.index("description")
            
            # print('total')
            # print(len(initialData))
            # file = open('./helper-scripts/sheet1.csv')
            # csv_reader = csv.reader(file)
            # new_init = []
            # for row in csv_reader:
            #     data = []
            #     data.append(row[0])
            #     t = self.remove_special_char(row[1])
            #     if not t: continue
            #     data.append(t)
            #     data.append("")
            #     data.append("")
            #     new_init.append(data)
            #     print(t)
            # # print(len(new_init))
            # content["initialData"] = new_init
            # self.add_update_uri(model_name=model, schema_content=content)
            
    def remove_special_char(self, value):
        if '^' in value: return None
        specials = ['#', '+', '*']
        temp = str(value)
        for s in specials:
            temp = temp.replace(s, '')
            temp = temp.strip()
            # if s in value:
            #     temp = f"{temp}{self.get_label_postfix(s)}"
        return temp
    
    def get_label_postfix(self, symbol):
        if symbol == '#':
            return "(introduced in third edition)"
        if symbol == '*':
            return "(introduced in revised edition)"
        if symbol == '+':
            return "(introduced in second edition)"
        if symbol == '@':
            return "(reintroduced with revised definition in third edition)"
            
    
    def loadLutURIS(self, model, content, info_uri, update_uri=True, update_desc=True):
        content_keys = content.keys()
        if update_uri:
            content["info"]["uri"] = info_uri
        # set init data uri
        if "initialData" not in content_keys:
            self.add_update_uri(model_name=model, schema_content=content)
            return

        initialData = content["initialData"]
        if not initialData:
            return

        attributes = list(content["attributes"].keys())
        uriIndex = attributes.index("uri")
        dscIndex = attributes.index("description")

        lutValueConfigs = self.queryVocabs.allValuesUsingSparqlQuery(info_uri)
        for idx, x in enumerate(initialData):
            row = content["initialData"][idx]
            rowLen = len(row) - 1
            if rowLen < uriIndex:
                while rowLen < uriIndex:
                    content["initialData"][idx].append("")
                    rowLen = rowLen + 1
            if rowLen < dscIndex:
                while rowLen < dscIndex:
                    content["initialData"][idx].append("")
                    rowLen = rowLen + 1

            content["initialData"][idx][uriIndex] = ""
            uri = self.getMatchedLiteral(
                lutSchema=lutValueConfigs,
                symbol=x[0].lower(),
                label=x[1].lower(),
            )
            if not uri:
                continue

            if update_uri:
                content["initialData"][idx][uriIndex] = uri

            if not update_desc:
                continue
            properties = self.queryVocabs.sparql_query_all(uri)
            desc = None
            for property in properties:
                if property["title"]["type"] != "uri":
                    continue
                if (
                    property["title"]["value"]
                    != "http://www.w3.org/2004/02/skos/core#definition"
                ):
                    continue
                if property["value"]["type"] != "literal":
                    continue
                if not property["value"]["value"]:
                    continue
                desc = property["value"]["value"]

            if desc:
                content["initialData"][idx][dscIndex] = desc
        self.add_update_uri(model_name=model, schema_content=content)

    def add_update_uri(self, model_name: str, schema_content: dict):
        model_path = f"./{self.source_path}/{dv.API}/{model_name}/content-types/{model_name}/schema.json"
        if not os.path.exists(os.path.dirname(model_path)):
            print(model_path + " not found to store init data")
            return

        print("saving new " + model_name)

        with open(model_path, "w", encoding="utf-8") as f:
            json.dump(schema_content, f, ensure_ascii=False, indent=2)

        print("model path: " + model_path)

    def getLutSchema(self, lut_name):
        for obj in self.lutConfigs:
            if obj.getDescription(lut_name) is None:
                continue
            return obj

        return None

    def getMatchedLiteral(self, lutSchema, symbol, label):
        for obj in lutSchema:
            obj_keys = obj.keys()
            if "label" not in obj_keys:
                continue

            value = obj["label"]["value"].lower()
            value = value.replace(" ", "")
            lutLabel = label.lower()
            lutLabel = lutLabel.replace(" ", "")

            if value == lutLabel:
                return obj["concept"]["value"]
            # 'tunnel erosion- Not apparent' and 'Not apparent' are the same in lut-soils-tunnel-water-erosion-degree
            if "-" in value and lutLabel in value:
                return obj["concept"]["value"]

        return None

    def getMatchedObject(self, source, key, value):
        for obj in source:
            obj_keys = obj.keys()
            if key not in obj_keys:
                continue

            if obj[key] != value:
                continue

            return obj
        return None

    def update_api_routes(self):
        models = self.auto_values.get_all_models(including_luts=True)
        process_models = ['sign-based-vehicle-track-log']
        # process_models = ['aerial-survey', "basal-wedge-observation"]
        skips = ['protocol', 'plot-visit', 'plot-layout']
        for model in models:
            # if model not in process_models: continue
            if model in skips: continue
            if 'lut' in model: continue
            content = self.auto_values.get_route_content(model_name=model)


def main():
    loader = URILoader(source_path=CORE_SOURCE, resource_path=RESOURCE_PATH)
    # loader.download_resources()
    # loader.csv_dump()
    # loader.refresh_luts_init_data(update_uri=True, update_desc=True)
    # loader.refresh_luts_attributes()
    # print(json.dumps(allLutsInfo))
    # init_data.refresh_URI()
    # loader.generateProtocolRelations()
    # loader.replaceSurveyId()
    # loader.updateTestMock()
    # loader.update_api_routes()
    # loader.remove_luts_duplicate_init_data()
    # loader.refresh_luts_init_data()
    loader.generateProtocolRelations()


if __name__ == "__main__":
    main()

import dummyValuesBasedOnSchema as dv
from dawe_vocabs import DaweVocabs, LUTSchema
from SparqlQuery import Query
import os
import json
import requests
import re
from typing import Optional, List, Union
import csv
import uuid
import copy
import base64

CORE_SOURCE = "paratoo-core/src"
ORG_SOURCE = "paratoo-org/src"

RESOURCE_PATH = "https://raw.githubusercontent.com/ternaustralia/dawe-rlp-vocabs/main/src/dawe_nrm/api/categorical_values/endpoints.py"

allLutsInfo = {}

# vocab_doc = "https://dev.core-api.monitor.tern.org.au/api/documentation/swagger.json"
vocab_doc = "http://localhost:1337/api/documentation/swagger.json"

class AddSchemaAttribute:
    def __init__(self, source_path: str, field: str):
        self.field = field
        self.source_path = source_path
        self.auto_values = dv.DummyValuesBasedOnSchema(self.source_path, dv.INIT_DATA)
        
    def addField(self):
        models = self.auto_values.get_all_models(including_luts=False)
        for model in models:
            content = self.auto_values.get_schema_content(
                    content_type=dv.API, model_name=model
                )
            content_keys = content.keys()
            if len(content_keys) == 0: continue
            attributes = list(content["attributes"].keys())
            if self.field in attributes: continue
            field_object = { "type": "string", "unique": True }
            content["attributes"][self.field] = field_object
            self.save_content(model_name=model, schema_content=content)
    
    def save_content(self, model_name: str, schema_content: dict):
        model_path = f"./{self.source_path}/{dv.API}/{model_name}/content-types/{model_name}/schema.json"
        if not os.path.exists(os.path.dirname(model_path)):
            print(model_path + " not found to store init data")
            return

        print("saving new " + model_name)

        with open(model_path, "w", encoding="utf-8") as f:
            json.dump(schema_content, f, ensure_ascii=False, indent=2)

        print("model path: " + model_path)

def main():
    addSchemaAttribute = AddSchemaAttribute(source_path=CORE_SOURCE, field="abis_export_uuid")
    addSchemaAttribute.addField()
    


if __name__ == "__main__":
    main()

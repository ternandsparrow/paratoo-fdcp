#!/bin/sh
cd $(dirname "$0")
cd ../paratoo-webapp

android_studio_tar=$(find "$HOME/Downloads" -name "android-studio-*-linux.tar.gz" -print -quit)

if [ ! -f "$android_studio_tar" ]; then
  echo "You should download the tar.gz file to from the link below to "$HOME"/Downloads/"
  echo https://developer.android.com/studio
  exit 1
fi

if [ ! -f "./android-studio/bin/studio.sh" ]; then
  mkdir -p ./android-studio
  tar -zxvf "$android_studio_tar" -C ./android-studio --strip-components=1
  echo "Done!"
fi

export ANDROID_HOME="$HOME/Android/Sdk"
export ANDROID_SDK_ROOT="$HOME/Android/Sdk"
PATH=$PATH:$ANDROID_SDK_ROOT/tools
PATH=$PATH:$ANDROID_SDK_ROOT/platform-tools

#!/bin/bash
#checks if the createAdminUser methods in core and org are the same
set -euxo pipefail

cd `dirname "$0"`
thisDir=$PWD

trap "echo 'diff-admin-user error'" ERR

diff ../paratoo-core/src/api/paratoo-helper/functions/createAdminUser.js ../paratoo-org/src/api/paratoo-helper/functions/createAdminUser.js
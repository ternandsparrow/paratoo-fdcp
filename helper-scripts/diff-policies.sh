#!/bin/bash
# checks if the global validation policies are consistent between core and org
set -euxo pipefail

cd `dirname "$0"`
thisDir=$PWD

trap "echo 'diff-policies error'" ERR

diff ../paratoo-org/src/policies/is-validated.js ../paratoo-core/src/policies/is-validated.js
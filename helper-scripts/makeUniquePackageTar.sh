#!/bin/bash
# the purpose of this script is to use the local changes of strapi-plugin-documentation for testing purposes
# this is an alternative to packing, uploading, and publishing a release on github

# to note: simply changing the source of the plugin to a local packed tar will not cause yarn to
# re-grab the new changes, so we break this pseudo-cache by setting a random name for the file.

cd ~/strapiv4/packages/plugins/documentation
rm strapi-plugin-documentation*
npm pack
mv strapi-plugin-documentation* strapi-plugin-documentation-dev$RANDOM.tgz
fname=$(readlink -f strapi-plugin-documentation*)

cd ~/paratoo-fdcp/paratoo-core

yarn remove @strapi/plugin-documentation
yarn add @strapi/plugin-documentation@file:$fname


yarn install

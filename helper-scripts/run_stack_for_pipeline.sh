#!/bin/bash

# script for running the stack in dev mode (for when CI remotes into a test VM to launch
# the stack for cypress to use in a later CI job).
# runs each core/org/app in parallel: https://stackoverflow.com/a/14612605

# pcore and porg scripts and app might not be able to handle tearing down the processes
# properly, so forcefully kill those processes based on their ports
# https://stackoverflow.com/a/9346231
baseurl=$(node -e "require('../paratoo-webapp/getBaseUrl.js')")
echo "baseurl: $baseurl" 
echo "Killing stack if needed"
{
    if lsof -i :1337 | grep LISTEN > /dev/null; then
        kill -9 $(lsof -t -i:1337 | awk 'NR==1{print $1}')
    else
        echo "Port 1337 is not currently in use."
    fi
} 2>/dev/null
{
    if lsof -i :1338 | grep LISTEN > /dev/null; then
        kill -9 $(lsof -t -i:1338 | awk 'NR==1{print $1}')
    else
        echo "Port 1338 is not currently in use."
    fi
} 2>/dev/null

{
    if lsof -i :8080 | grep LISTEN > /dev/null; then
        kill -9 $(lsof -t -i:8080 | awk 'NR==1{print $1}')
    else
        echo "Port 8080 is not currently in use."
    fi
} 2>/dev/null


core_envs () {
  echo -n "" > ../paratoo-core/localdev.env   # reset file contents
  echo "STRAPI_LOG_LEVEL=debug" >> ../paratoo-core/localdev.env
  echo "ORG_URL_PREFIX=http://$baseurl:1338" >> ../paratoo-core/localdev.env
  echo "ENABLE_CACHE=true" >> ../paratoo-core/localdev.env
  echo "ADMIN_PASS=admin" >> ../paratoo-core/localdev.env
  echo "ADMIN_EMAIL=tech.contact@environmentalmonitoringgroup.com.au" >> ../paratoo-core/localdev.env
}

org_envs () {
  echo -n "" > ../paratoo-org/localdev.env   # reset file contents
  echo "STRAPI_LOG_LEVEL=debug" >> ../paratoo-org/localdev.env
  echo "ADMIN_PASS=admin" >> ../paratoo-org/localdev.env
  echo "ADMIN_EMAIL=tech.contact@environmentalmonitoringgroup.com.au" >> ../paratoo-org/localdev.env

  echo -n "" > ../paratoo-org/test_users.env   # reset file contents
  echo "TEST_USER0=TestUser" >> ../paratoo-org/test_users.env
  echo "TEST_USER0_PASS=password" >> ../paratoo-org/test_users.env
  echo "TEST_USER0_EMAIL=testuser@email.com" >> ../paratoo-org/test_users.env
  echo "TEST_USER0_PROJECTS=1,2,3,4,5,6,7,9" >> ../paratoo-org/test_users.env

  echo "TEST_USER1=ProjectAdmin" >> ../paratoo-org/test_users.env
  echo "TEST_USER1_PASS=paratoo" >> ../paratoo-org/test_users.env
  echo "TEST_USER1_EMAIL=tech.contact@environmentalmonitoringgroup.com.au" >> ../paratoo-org/test_users.env
  echo "TEST_USER1_PROJECTS=1,2,3,4,8,9" >> ../paratoo-org/test_users.env
  echo "TEST_USER1_ROLE=project_admin" >> ../paratoo-org/test_users.env

  echo "TEST_USER2=NESPTestUser" >> ../paratoo-org/test_users.env
  echo "TEST_USER2_PASS=paratoo" >> ../paratoo-org/test_users.env
  echo "TEST_USER2_EMAIL=tech.contact@environmentalmonitoringgroup.com.au" >> ../paratoo-org/test_users.env
  echo "TEST_USER2_PROJECTS=16" >> ../paratoo-org/test_users.env
}

app_envs () {
  echo -n "" > ../paratoo-webapp/.env.local   # reset file contents
  echo "VUE_APP_SPOOF_PLOT_LAYOUT=true" >> ../paratoo-webapp/.env.local
  echo "VUE_APP_DEV_BARCODE=true" >> ../paratoo-webapp/.env.local
  echo "VUE_APP_MODE_OF_OPERATION=0" >> ../paratoo-webapp/.env.local
  # set core/org urls to be the VM's IP as the served app is running on that IP and
  # trying to access localhost from an external machine will try access that machine's
  # localhost instead of the remote VM's localhost
  # TODO don't hardcode the IP
  echo "CORE_API_URL_BASE=http://$baseurl:1337" >> ../paratoo-webapp/.env.local
  echo "ORG_API_URL_BASE=http://$baseurl:1338" >> ../paratoo-webapp/.env.local
}

echo "i exist" > foobar.txt
echo "Setting envs"
core_envs
org_envs
app_envs
echo "Starting core"
{ cd ../paratoo-core && ./pcore down --volumes && yarn install && ./pcore & } > core.log

echo "Starting org"
{ cd ../paratoo-org && ./porg down --volumes && yarn install && ./porg & } > org.log

# cypress seems to ignore engines specified in the package (even though we satisfy the
# requirements on node 19, so pass the `--ignore-engines` flag)
# https://stackoverflow.com/a/57748163
echo "Starting app"
{ cd ../paratoo-webapp && yarn install --ignore-engines && yarn start:instrument & } > app.log
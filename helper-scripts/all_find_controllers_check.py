import dummyValuesBasedOnSchema as dv
import os

CORE_SOURCE = "paratoo-core/src"
ORG_SOURCE = "paratoo-org/src"

SKIPS = [
    "reserved-plot-label",
    "paratoo-helper",
    "generate-barcode",
    "org-uuid-survey-metadata",
    "debug-client-state-dump",
    "grassy-weeds-export",
    "protocol",
    "analytics",
]


class CheckApiControllers:
    def __init__(self, source_path: str):
        self.source_path = source_path
        self.auto_values = dv.DummyValuesBasedOnSchema(self.source_path, dv.INIT_DATA)

    def trimmed_content(self, content: str):
        texts = content.replace("\t", "")
        texts = texts.replace("\n", "")
        texts = texts.replace(" ", "")
        return texts

    def check_all_controllers(self):
        models = self.auto_values.get_all_models(including_luts=True)
        for model in models:
            if model in SKIPS:
                continue
            if "lut" in model:
                continue
            file_path = f"./{self.source_path}/api/{model}/controllers/{model}.js"

            # if file does'nt exit
            if not file_path or not os.path.exists(os.path.dirname(file_path)):
                continue
            core_controller = "createCoreController"
            content = None
            trimmed = ""
            file = open(file_path, "r")
            content = file.read()
            trimmed = self.trimmed_content(content=content)

            if core_controller not in content:
                continue
            if "asyncfind(ctx)" in trimmed:
                if "helpers.pdpDataFilter" not in content:
                    raise Exception(f"find controller exists but not the generic one with pdp filter, path: {file_path}")
                continue
            raise Exception(f"find controller with pdp filter does not exist, path: {file_path}")

def main():
    check_api_controllers = CheckApiControllers(source_path=CORE_SOURCE)
    check_api_controllers.check_all_controllers()

if __name__ == "__main__":
    main()

#! /usr/bin/env node

// just run ./bump-version.mjs <version> to update all the version attributes in package.json

import fs from 'fs/promises'
import path from 'path'
import { fileURLToPath } from 'url'
const folders = ['paratoo-core', 'paratoo-webapp', 'paratoo-org']

const __dirname = path.dirname(fileURLToPath(import.meta.url))
const rootFolder = path.join(__dirname, '..')

for (const folder of folders) {
  const packageJsonPath = path.join(rootFolder, folder, 'package.json')
  const packageJson = JSON.parse(await fs.readFile(packageJsonPath))
  packageJson.version = process.argv[2]
  await fs.writeFile(packageJsonPath, JSON.stringify(packageJson, null, 2))
}

# a separate script so that we use this to generate dummy
#   values for both initial data and test inputs
import json
import os
from typing import Any
from random import *
import rstr

CORE_SOURCE = "paratoo-core/src"
CORE_TEST_DIRECTORY = "paratoo-core/tests"
ORG_SOURCE = "paratoo-org/src"
ORG_TEST_DIRECTORY = "paratoo-org/tests"

API = "api"
COMPONENT = "components"

INIT_DATA = "init"
TEST_INPUT = "testing"

TEST_VALUES = {
    "datetime": "2022-09-14T01:38:29.237Z",
    "date": "2022-09-14",
    "integer": 1,
    "decimal": 0,
    "float": 0.1,
    "string": "dummy string",
    "text": "dummy text",
    "boolean": False,
    "json": {
        "surveyType": "dummy type",
        "time": "2021-10-13T01:16:47.225Z",
        "uuid": "363ea133-fc87-4631-9963-545d9c3ecdba",
    },
    "media": 1,
    "enumeration": "dummy string",
}

# for some models, we dont need to generate all values
#   e.g. plot-location = {"id": 1} or plot-visit = {"id": 1}
FIXED_API_VALUE = {
    "plot-location": {"id": 1},
    "plot-layout": {"id": 1},
    "plot-visit": {"id": 1},
}


class DummyValuesBasedOnSchema:
    def __init__(self, source_path: str, source_type: str):
        # core or org
        self.source_path = source_path
        # initial data or test inputs
        self.source_type = source_type

    def generate_input_data(self, attributes, content_type, child_observations=[]):
        """
        assign test values recursively.
        """
        payload = {}
        for key, value in attributes.items():
            target = ""
            if "target" in value.keys():
                # target exists if the object is an api or a lut
                target = value["target"]

            test_value = self.get_test_value(value)
            if test_value is not None:
                payload[key] = test_value
                continue

            # if the object is a lut, we set value extracted from initial data
            if self.is_lut(target):
                payload[key] = self.get_lut_value(target, value["type"], content_type)
                continue

            # if component, we assign values recursively.
            if value["type"] == "component":
                content = self.get_schema_content(
                    content_type=COMPONENT, component=value["component"]
                )
                data = self.generate_input_data(content["attributes"], content_type)

                # if repeatable then data should be in a array
                if self.is_multiple(object=value, key="repeatable"):
                    data = self.generate_multi_component(object=value, data=data)

                payload[key] = data
                continue

            # model exists in newInstanceForRelationOnAttributes
            if key in child_observations:
                model_name = self.target_to_model_name(target, value["type"])
                content = self.get_schema_content(
                    content_type=API, model_name=model_name
                )
                data = {"data": self.generate_input_data(content["attributes"], API)}

                # if oneToMany then data should be in a array
                if value["relation"] == "oneToMany":
                    data = [data]
                if self.is_multiple(
                    object=value, key="relation", expected_text="oneToMany"
                ):
                    data = [data]

                payload[key] = data
                continue

            # by default we assign 1
            payload[key] = 1

        return payload

    def get_schema_content(self, content_type: str, model_name=None, component=None):
        """
        loads json schema for api or component
        """
        file_path = None

        # model name should be provided to get schema
        # e.g. bird-survey
        if content_type == API and model_name:
            file_path = f"./{self.source_path}/{content_type}/{model_name}/content-types/{model_name}/schema.json"

        # component should be provided to get schema
        # e.g. "camera-trap.target-taxa-types"
        if content_type == COMPONENT and component:
            file_name = component.replace(".", "/") + ".json"
            file_path = f"./{self.source_path}/{content_type}/{file_name}"

        # if file does'nt exit
        if not file_path or not os.path.exists(os.path.dirname(file_path)):
            # print(file_path + " does not exist")
            return {}
        return json.load(open(file_path))

    def get_route_content(self, model_name=None, method="find"):
        """
        loads route js for api
        """
        file_path = None

        # model name should be provided to get schema
        # e.g. bird-survey
        file_path = f"./{self.source_path}/api/{model_name}/routes/{model_name}.js"

        # if file does'nt exit
        if not file_path or not os.path.exists(os.path.dirname(file_path)):
            # print(file_path + " does not exist")
            return None

        lines = None
        content = None
        with open(file_path, encoding="utf8") as file:
            lines = file.readlines()
            content = file.read()
            # if f"{model_name}.find" in route:

        for line in lines:
            if "global::pdp-data-filter" in line:
                # print(file_path + " pdp-data-filter exists")
                return
            if "const { createCoreRouter }" in line:
                print(file_path + " default route found")
                default = self.get_default_route_content(model=model_name)
                if default:
                    with open(file_path, "w") as f:
                        f.write(default)
                return

        with open(file_path, "w") as f:
            found = False
            for line in lines:
                if f"{model_name}.find'" in line:
                    found = True
                if found and "}," in line:
                    found = False

                if found:
                    if "config:" in line or "'config':" in line:
                        line = line.replace(
                            "{", "{\n        middlewares: ['global::pdp-data-filter'],"
                        )
                        print(line)

                f.write(line)
        print(file_path)

        # return json.load(open(file_path))

    def get_default_route_content(self, model):
        content = self.get_schema_content(content_type=API, model_name=model)
        if "info" not in content:
            return None
        plural_form = content["info"]["pluralName"]
        with open("./helper-scripts/defaultRouteContent.js", "r") as file:
            temp = (
                file.read()
                .replace("{model}", model)
                .replace("{plural_form}", plural_form)
            )
        return temp

    def get_test_value(self, schema_value):
        """
        get test values using types.
        """
        for key, value in TEST_VALUES.items():
            if schema_value["type"] != key:
                continue

            # checks validation rules
            if "regex" in schema_value.keys():
                value = rstr.xeger(schema_value["regex"])
            if "min" in schema_value.keys():
                return schema_value["min"]
            if "minimum" in schema_value.keys():
                return schema_value["minimum"]
            if "minLength" in schema_value.keys():
                return value[: schema_value["minLength"]]
            # if unique
            if schema_value["type"] == "string" and "unique" in schema_value.keys():
                return value + str(randint(1, 100))
            # if "enumeration"
            if "enum" in schema_value.keys():
                return schema_value["enum"][0]

            return value

        return None

    def get_lut_value(self, target, value_type, content_type):
        """
        get first value of initial data.
        """
        lut_name = self.target_to_model_name(target, value_type)
        content = self.get_schema_content(
            content_type=content_type, model_name=lut_name
        )

        if "initialData" not in content.keys():
            return ""

        # for initial data we usually assign id
        if self.source_type == INIT_DATA:
            return 1

        # for all kind of testings we assign lut symbol or label
        return content["initialData"][0][0]

    def get_attributes(self, schema_content=None):
        # if attributes filed exist
        if "attributes" not in schema_content.keys():
            return {}

        return schema_content["attributes"]

    def is_multiple(self, object: dict, key: str, expected_text=None):
        if not object:
            return False
        if key not in object.keys():
            return False
        if not expected_text:
            return object[key]
        return object[key] == expected_text

    def generate_multi_component(self, object: dict, data: any):
        if "min" in object.keys():
            return [data] * object["min"]

        if "minimum" in object.keys():
            return [data] * object["minimum"]

        return [data]

    def is_lut(self, target):
        # checks if prefix "lut" exists in target.
        return len(target.split("lut")) > 1

    def target_to_model_name(self, target, type):
        """
        get model name from target.
        e.g. "api::coarse-woody-debris-survey.coarse-woody-debris-survey"
            to "coarse-woody-debris-survey"
        """
        if type != "relation":
            return ""
        if not target or target == "":
            return ""
        return json.dumps(target).split("api::")[1].split(".")[0]

    def get_all_protocols(self):
        content = self.get_schema_content(content_type=API, model_name="protocol")
        if "initialData" not in content.keys():
            return []

        # all protocols
        return content["initialData"]

    def get_all_models(self, including_luts=True):
        api_path = self.source_path + "/" + API
        models = os.listdir(api_path)

        model_names = []
        for model in models:
            model_path = api_path + "/" + model

            # we should not assign auto initial
            #   data for luts
            if self.is_lut(model) and not including_luts:
                continue
            # recheck again whether the model exists or not
            if os.path.exists(os.path.dirname(model_path)):
                model_names.append(model)

        return model_names

    def get_all_protocols(self):
        content = self.get_schema_content(content_type=API, model_name="protocol")
        if "initialData" not in content.keys():
            return []

        # all protocols
        return content["initialData"]

    def get_all_models(self, including_luts=True):
        api_path = self.source_path + "/" + API
        models = os.listdir(api_path)

        model_names = []
        for model in models:
            model_path = api_path + "/" + model

            # we should not assign auto initial
            #   data for luts
            if self.is_lut(model) and not including_luts:
                continue
            # recheck again whether the model exists or not
            if os.path.exists(os.path.dirname(model_path)):
                model_names.append(model)

        return model_names

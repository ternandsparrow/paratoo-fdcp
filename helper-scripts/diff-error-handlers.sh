#!/bin/bash
#checks if the paratooErrorHandler methods in core and org are the same
set -euxo pipefail

cd `dirname "$0"`
thisDir=$PWD

trap "echo 'diff-admin-user error'" ERR

diff ../paratoo-core/src/api/paratoo-helper/functions/paratooErrorHandler.js ../paratoo-org/src/api/paratoo-helper/functions/paratooErrorHandler.js
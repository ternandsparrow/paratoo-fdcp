import { defineConfig } from 'cypress'
import { config } from 'dotenv'
import fs from 'fs'

const localEnvExist = fs.existsSync('.env.local')
if (localEnvExist) {
  config({ path: '.env.local' })
}

export default defineConfig({
  userAgent: 'chrome',
  projectId: '6b9ofw',
  fixturesFolder: 'test/cypress/fixtures',
  screenshotsFolder: 'test/cypress/screenshots',
  videosFolder: 'test/cypress/videos',
  video: true,
  experimentalStudio: true,
  chromeWebSecurity: false,
  viewportWidth: 768,
  viewportHeight: 1024,
  e2e: {
    chromeWebSecurity: false,
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./test/cypress/plugins/index.js')(on, config)
    },
    baseUrl: 'http://localhost:8080/',
    specPattern: 'test/cypress/integration/**/*.cy.{js,jsx,ts,tsx}',
    supportFile: 'test/cypress/support/index.js',
    //allows multiple `it`s in `describe` to be executed sequentially, rather than
    //expecting each `it` to start again
    testIsolation: false,
    //should prevent Cypress randomly crashing
    experimentalMemoryManagement: true,
    // numTestsKeptInMemory: 0,
    taskTimeout: 300000,
    pageLoadTimeout: 180000,
    defaultCommandTimeout: 10000,
    // animationDistanceThreshold: 20
  },
  env: {
    /**
     * @description Holds information about different trap types.
     * @property {string} P - 'Pitfall Trap'
     * @property {string} F - 'Funnel Trap'
     * @property {string} E - 'Elliot Trap'
     * @property {string} C - 'Cage Trap'
     */
    VERT_TRAP_TYPE: {
      P: 'Pitfall Trap' /** Pitfall */,
      F: 'Funnel Trap',
      E: 'Box Trap',
      C: 'Cage Trap',
    },
    VERT_TRAP_NUMBERS: [
      'P1',
      'F1',
      'F2',
      'P2',
      'P3',
      'P4',
      'P5',
      'F3',
      'F4',
      'P6',
      'P7',
      'F5',
      'F6',
      'P8',
      'P9',
      'P10',
      'P11',
      'F7',
      'F8',
      'P12',
      'C1',
      'E1',
      'E2',
      'E3',
      'E4',
      'E5',
      'E6',
      'E7',
      'E8',
      'E9',
      'E10',
      'C2',
      'C3',
      'E11',
      'E12',
      'E13',
      'E14',
      'E15',
      'E16',
      'E17',
      'E18',
      'E19',
      'E20',
      'C4',
      'C5',
      'E21',
      'E22',
      'E23',
      'E24',
      'E25',
      'E26',
      'E27',
      'E28',
      'E29',
      'E30',
      'C6',
      'C7',
      'E31',
      'E32',
      'E33',
      'E34',
      'E35',
      'E36',
      'E37',
      'E38',
      'E39',
      'E40',
      'C8',
    ],
    NETWORK_RECHECK_INTERVAL: process.env.NETWORK_RECHECK_INTERVAL || 15,
  },
})

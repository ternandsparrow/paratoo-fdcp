/* eslint-disable no-useless-escape */
const { exec } = require('child_process')
const fs = require('fs')
const fetch = require('node-fetch')
const dotenv = require('dotenv')
const _ = require('lodash')

/**
 * Script for running tests, with helpers for running various stack components
 * (core/org/app) and environment variables.
 * 
 * === Command line params ===
 * 
 * Format: --<param_flag>: [<param_options>] <description>
 * 
 * --sudo: runs core/org/app with `sudo`
 * --core: runs core in separate terminal window (tears down with `--volumes` first)
 * --org: runs org in separate terminal window (tears down with `--volumes` first)
 * --app: runs app in separate terminal window
 * --sso: sets `AUTH_TYPE=oidc` using cross-env
 * --cypress: [run, open]: runs (headless) or opens (headed) Cypress
 * --spec: [spec1,spec2,...]: the spec file names (not full path, only what sub-folder of
 *                           `/test/cypress/integration`; and no extensions)
 * --spec_glob [<spec_glob>]: a glob that Cypress will accept.
 *                           e.g., `test/cypress/integration/priority/` will run all
 *                           specs in `priority` folder
 * --browser [firefox, chromium, safari | chrome]
 * --app_env [env1key=env1,env2key=env2,...]: environment variables to pass to the app.
 *            Creates separate file that overrides any envs in `.env` and `.env.local`
 * --core_env [env1key=env1,env2key=env2,...]: environment variables to pass to core.
 *            Creates separate file that overrides any envs in `.env` and `localdev.env`
 * 
 * === Examples ===
 * 
 * Example usage 1 (opens whole core/org/app stack and runs in headless with all specs):
   
  node run_test \
  --sudo \
  --core \
  --org \
  --app \
  --cypress run \
  --browser firefox
  
  Example usage 2 (opens app and runs in headless with setup and opportune specs, using dev's core/org for API):
  node run_test \
  --sudo \
  --app \
  --cypress run \
  --spec priority/^^setup,run/opportune
  --browser firefox
  --app_env CORE_API_URL_BASE=https://dev.core-api.paratoo.tern.org.au,ORG_API_URL_BASE=https://dev.org-api.paratoo.tern.org.au
  
  Example usage 3 (MERIT):
  node run_test \
  --sudo
  --app \
  --core \
  --org \
  --cypress run \
  --app_env VUE_APP_ENABLE_OIDC=true,VUE_APP_OIDC_CLIENT="Atlas of Living Australia",VUE_APP_OIDC_AUTHORITY="https://cognito-idp.ap-southeast-2.amazonaws.com/ap-southeast-2_OOXU9GW39",VUE_APP_OIDC_CLIENT_ID="4rhkop9tl30lrt98cqjpcu3e1t",VUE_APP_OIDC_LOGOUT="https://auth-secure.auth.ap-southeast-2.amazoncognito.com/logout",ORG_URL_PREFIX=https://ecodata-test.ala.org.au,ORG_API_PREFIX="/ws/paratoo",ORG_CUSTOM_API_PREFIX="/ws/paratoo",ORG_ENABLE_TOKEN_VALIDATION=false,CORE_API_PREFIX="/api",CORE_API_URL_BASE=http://localhost:1337,ORG_API_URL_BASE=https://ecodata-test.ala.org.au,VUE_APP_SPOOF_PLOT_LAYOUT=true \
  --core_env ORG_URL_PREFIX=https://ecodata-test.ala.org.au,ORG_API_PREFIX=/ws/paratoo,ORG_CUSTOM_API_PREFIX=/ws/paratoo
*/

//TODO:
//  - support Gitlab CI/CD (doesn't have access to gnome-terminal) - have a separate
//    script `run_test_pipeline` that is doing some similar stuff with duplicate code, so
//    need to merge these together when have a chance
//  - handle browserstack. i.e., need to handle these:
//      "test:bs:local": "node scripts/createBrowserStackConfig.js --local true && browserstack-cypress run --sync"
//      "test:bs:remote": "node scripts/createBrowserStackConfig.js --local false && browserstack-cypress run --sync"
//  - pass in spec file alias for quickly running certain groups of tests, where those groups are large (e.g., --spec_a camera_trap will run all camera trap tests in their correct order, similar for verts, inverts, soils, sign-based, herbivory; groups such as florisitics won't have alias as there's only two protocols, this is more about large groups of tests)
//  - `-h` `--help` flags (help - usage)
//  - don't allow passing `--core` or `--org` if `app_env` or `core_env` have envs pointing to remote core and/or org
//  - repeat repetition of environment variables in the package.json

async function retryFetch(url, maxRetries, retryDelay) {
try {
  const response = await fetch(url)

    if (response.ok) {
      return response
    } else {
      throw new Error(`Request to ${url} failed with status: ${response.status}`)
    }
  } catch (error) {
    if (maxRetries > 0) {
      console.log(`Request to ${url} failed. Retrying in ${retryDelay} milliseconds...`)
      await new Promise(resolve => setTimeout(resolve, retryDelay))
      return retryFetch(url, maxRetries - 1, retryDelay)
    } else {
      throw new Error(`Max retries to ${url} exceeded: ${error.message}`)
    }
  }
}

// handle cli args: https://stackoverflow.com/a/24638042
const argv = require('minimist')(process.argv.slice(2))
console.log('args: ', argv)
const arg_keys = Object.keys(argv)
const bashUseSudo = arg_keys.includes('sudo') && !!argv.sudo
let browser = 'chrome'    // default that can be overridden by param
const CYPRESS_APP_ENV_FILE_NAME = '.env.cypress.local'
const CYPRESS_CORE_ENV_FILE_NAME = 'localdev.cypress.env'

const runComponent = {
  core: false,
  org: false,
  app: false,
}
const componentLocalUrl = {
  //TODO probably grab from env
  core: 'http://localhost:1337',
  org: 'http://localhost:1338',
  app: 'http://localhost:8080',
}

for (const envName of ['app', 'core']) {
  let envFileName = null
  if (envName === 'app') {
    envFileName = CYPRESS_APP_ENV_FILE_NAME
  } else if (envName === 'core') {
    envFileName = CYPRESS_CORE_ENV_FILE_NAME
  }

  if (arg_keys.includes(`${envName}_env`) && typeof argv[`${envName}_env`] === 'string') {
    let envs = null
    try {
      envs = argv[`${envName}_env`].split(',').join('\n')
      console.log(`Parsed ${envName}_env:\n${envs}`)
    } catch (err) {
      throw new Error(`Failed to parse '${envName}_env' param due to: ${err}`)
    }
  
    if (envName === 'app') {
      console.log(`Creating file '${envFileName}'`)
      exec(`touch ${envFileName}`)
      console.log(`Adding parsed envs to file '${envFileName}'`)
      exec(`echo "${envs}" > ${envFileName}`)
    } else if (envName === 'core') {
      console.log(`Creating file '${envFileName}' in '../paratoo-core/' directory`)
      exec(`touch ../paratoo-core/${envFileName}`)
      console.log(`Adding parsed envs to file '${envFileName}'`)
      exec(`echo "${envs}" > ../paratoo-core/${envFileName}`)
    }
  } else if (!arg_keys.includes(`${envName}_env`)) {
    //need to remove the file so that quasar doesn't try use stale vars in it
    console.log(`'${envName}_env' param not passed so removing file ${envFileName}`)
    exec(`rm ${envFileName}`)
  }
}

//keep open and chain multiple commands: https://unix.stackexchange.com/a/564491
//wait to exit: https://unix.stackexchange.com/a/538607
//capture exit event: https://stackoverflow.com/a/22365830
for (const strapi of ['org', 'core']) {
  if (arg_keys.includes(strapi) && !!argv[strapi]) {
    const downCmdBase = `../paratoo-${strapi}/p${strapi} down --volumes`
    const upCmdBase = `../paratoo-${strapi}/p${strapi}`
    
    let cmd = null
    if (bashUseSudo) {
      cmd = `gnome-terminal --window --wait -- /bin/sh -c "sudo ${downCmdBase};sudo ${upCmdBase};exec bash"`
    } else {
      cmd = `gnome-terminal --window --wait -- /bin/sh -c "${downCmdBase};${upCmdBase};exec bash"`
    }

    runComponent[strapi] = true
    
    console.log(`Running (detached): ${cmd}`)
    const process = exec(cmd)
    process.on('exit', function() {
      console.log(`Exit ${strapi}`)
      if (strapi === 'core') {
        //remove cypress envs so that future non-cypress launching of core won't try use
        //those envs
        console.log(`Removing old Cypress envs for core: ${CYPRESS_CORE_ENV_FILE_NAME}`)
        exec(`rm ../paratoo-core/${CYPRESS_CORE_ENV_FILE_NAME}`)
      }
    })
  }
}

if (arg_keys.includes('app') && !!argv.app) {
  let cmd = null
  //`yarn start` will open in PWA, so don't need to pass that in
  if (bashUseSudo) {
    cmd = `gnome-terminal --window --wait -- /bin/sh -c "yarn cross-env E2E_TEST=true;sudo yarn start:instrument;exec bash"`
  } else {
    cmd = `gnome-terminal --window --wait -- /bin/sh -c "yarn cross-env E2E_TEST=true;yarn start:instrument;exec bash"`
  }
  runComponent.app = true

  console.log(`Running (detached): ${cmd}`)
  const appProcess = exec(cmd)
  appProcess.on('exit', function() {
    console.log('Exit app')
    //remove cypress envs so that future non-cypress launching of core won't try use
    //those envs
    console.log(`Removing old Cypress envs for webapp: ${CYPRESS_APP_ENV_FILE_NAME}`)
    exec(`rm ${CYPRESS_APP_ENV_FILE_NAME}`)
  })
}

let cypressStarted = false
function handleCypressAndDependentParams () {
  if (arg_keys.includes('cypress') && argv.cypress) {
    let cmd = `gnome-terminal --window --wait -- /bin/sh -c`
    if (argv.cypress === 'open' || argv.cypress === 'run') {
      if (arg_keys.includes('browser') && argv.browser) {
        //TODO enforce valid browsers that can be passed
        if (argv.cypress === 'open') {
          throw new Error(`Passed in 'browser' param when calling "cypress ${argv.cypress}", this param is only valid for "cypress run"`)
        }
        console.log(`Cypress using browser ${argv.browser}`)
        browser = argv.browser
      } else {
        console.log(`Cypress using default browser ${browser}`)
      }
  
      let spec = null
      if (arg_keys.includes('spec') && argv.spec) {
        if (argv.cypress === 'open') {
          throw new Error(`Passed in 'spec' param when calling "cypress ${argv.cypress}", this param is only valid for "cypress run"`)
        }
        if (arg_keys.includes('spec_glob') && argv.spec_glob) {
          throw new Error(`Cannot pass in both 'spec' and 'spec_glob'`)
        }
  
        const testBaseDir = 'test/cypress/integration'
        const specs = argv.spec.split(',')
  
        const testDirs = fs.readdirSync(testBaseDir, { withFileTypes: true })
          .filter(o => o.isDirectory())
          .map(o => o.name)
  
        //checks that provided specs have included (`String.startsWith`) one of `testDirs`
        if (specs.some(spec => testDirs.every(dir => !spec.startsWith(dir)))) {
          throw new Error(`A spec you provided did not specify the directory it's contained in. Must be one of: ${testDirs}`)
        }
  
        //didn't throw, so provided specs must be good
        spec = specs.map(s => `${testBaseDir}/${s}.cy.js`)
  
      } else {
        console.log('No `--spec` provided, so letting Cypress handle the order')
      }

      if (arg_keys.includes('spec_glob') && argv.spec_glob) {
        if (argv.cypress === 'open') {
          throw new Error(`Passed in 'spec_glob' param when calling "cypress ${argv.cypress}", this param is only valid for "cypress run"`)
        }
        if (arg_keys.includes('spec') && argv.spec) {
          throw new Error(`Cannot pass in both 'spec' and 'spec_glob'`)
        }

        console.log(`Spec glob '${argv.spec_glob}' passed, assuming it's correct`)
        spec = argv.spec_glob
      }

      const useSSO = arg_keys.includes('sso') && !!argv.sso
      const localEnvs = dotenv.config({ path: '.env.local' }).parsed

      const envsKeys = Object.keys(localEnvs)
      //could use `--app_env` to handle this, but no guarantee we're launching the app
      //and the app's package.json needs to call this script; but to do so would require
      //adding these sensitive vars to version control
      const usernameProvided = envsKeys.includes('E2E_MERIT_USERNAME') && !_.isEmpty(localEnvs.E2E_MERIT_USERNAME)
      const passwordProvided = envsKeys.includes('E2E_MERIT_PASSWORD') && !_.isEmpty(localEnvs.E2E_MERIT_PASSWORD)
      const clientIdProvided = envsKeys.includes('VUE_APP_OIDC_CLIENT_ID') && !_.isEmpty(localEnvs.VUE_APP_OIDC_CLIENT_ID)

      if (
        useSSO &&
        (!usernameProvided || !passwordProvided || !clientIdProvided)
      ) {
        let msg = `Cannot run with --sso without providing the testing username/password credentials in .env.local. Missing: `
        const missingVars = []
        if (!usernameProvided) {
          missingVars.push('username (E2E_MERIT_USERNAME)')
        }
        if (!passwordProvided) {
          missingVars.push('password (E2E_MERIT_PASSWORD)')
        }
        if (!clientIdProvided) {
          //unlike username/password, this is only for the app, so no need to pass using
          //`--env` (which is for Cypress specifically)
          missingVars.push('client ID (VUE_APP_OIDC_CLIENT_ID)')
        }
        msg += missingVars.join(', ')
        throw new Error(msg)
      }
      if(localEnvs.E2E_MERIT_PASSWORD) {
        //need to `replaceAll` password in case it contains quotes
        const pw = localEnvs.E2E_MERIT_PASSWORD
          .replaceAll('\"', '\\"')
          .replaceAll("\'", "\'")
        const ssoEnvsParam = ` --env AUTH_TYPE=\'oidc\',E2E_MERIT_USERNAME=\'${localEnvs.E2E_MERIT_USERNAME}\',E2E_MERIT_PASSWORD=\'${pw}\',OIDC_PROJECT=\'MERI\\ Plan\\ Test\\ Project\\ 11\'`
  
        cmd += ` "yarn cypress ${argv.cypress} --browser ${browser}${spec !== null ? ' --spec ' + spec : ''}${useSSO ? ssoEnvsParam : ''};exec bash"`
      }
    } else {
      throw new Error(`Param 'cypress' got invalid argument '${argv.cypress}'`)
    }

    cypressStarted = true
    //in CI, this gets printed to the console (which is publically-viewable), so don't
    //print sensitive info
    const redactedCmd = cmd.replaceAll(/(E2E_MERIT_PASSWORD=)(')(.*)(')/g, 'E2E_MERIT_PASSWORD=<REDACTED>')
    console.log(`Running (detached): ${redactedCmd}`)
    exec(cmd)
  } else if (arg_keys.includes('browser') && argv.browser) {
    throw new Error(`Passed in 'browser' param but 'cypress' param wasn't also provided`)
  }
}

const componentDone = {
  core: null,
  org: null,
  app: null,
}
//separate loop to init `componentDone` so that we can check if all are null
for (const component of ['core', 'org', 'app']) {
  if (runComponent[component]) {
    componentDone[component] = false
  }
}

if (Object.values(componentDone).every(c => c === null)) {
  //all 3 components are not being started, so don't need to wait for them
  console.log("Core, Org, and App not being started - don't need to wait for them")
  handleCypressAndDependentParams()
} else {
  for (const component of ['core', 'org', 'app']) {
    if (runComponent[component]) {
      console.log(`Waiting for ${component} to start before running tests. Checking at URL: ${componentLocalUrl[component]}`)
      //20 retries of 30 sec each will wait for 10 mins
      retryFetch(componentLocalUrl[component], 20, 30000)
        .then(response => {
          console.log(`Request to check ${component} successful. Status: ${response.status}`)
          componentDone[component] = true
          console.log('Cypress already started?', cypressStarted)
          console.log('All relevant components are started?', Object.values(componentDone).every(c => c !== false))
          if (
            !cypressStarted &&
            //explicitly check `false` as we set null to indicate a given component isn't
            //being started
            Object.values(componentDone).every(c => c !== false)
          ) {
            //it's possible that core/org/webapp are all going to be started, but we need
            //to wait for all to start; rather than chaining nested promises, easier to
            //just check if any of those components have already triggered starting cypress
            handleCypressAndDependentParams()
          }
        })
        .catch(error => {
          console.error(`Failed to make a successful request: ${error.message}`)
        })
    }
  }
}

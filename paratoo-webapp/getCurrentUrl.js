
/*
    Sole point for the pipeline to get the baseUrl for the stack.
    Use this file to add or change any baseurl that is required.
    Can add a variable to a job in the gitlab.yml and check for it here. e.g., IS_OFFLINE_TEST is defined in the webapp-offline-e2e job
    used in run_stack_for_pipeline and package.json to resolve their base url
*/

// TODO extend this to be robust and maybe less hard-coded to the perentie runner
let baseUrl = '172.17.0.1'    // perentie docker
            //`10.12.97.133`  // perentie. Using this one causes cypress to handle camera permissions poorly

if (process.env?.IS_OFFLINE_TEST) {
    baseUrl = 'localhost'
}
else if (process.env?.CYPRESS_BASE_URL) {
    baseUrl = process.env.CYPRESS_BASE_URL
}
module.exports = {
    url: baseUrl   
}
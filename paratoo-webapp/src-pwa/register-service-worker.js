import { register } from 'register-service-worker'
// The ready(), registered(), cached(), updatefound() and updated()
// events passes a ServiceWorkerRegistration instance in their arguments.
// ServiceWorkerRegistration: https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration

import {
  paratooSentryBreadcrumbMessage,
  paratooErrorHandler,
} from 'src/misc/helpers'

import { useEphemeralStore } from '../src/stores/ephemeral'

register(process.env.SERVICE_WORKER_FILE, {
  // The registrationOptions object will be passed as the second argument
  // to ServiceWorkerContainer.register()
  // https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerContainer/register#Parameter

  // registrationOptions: { scope: './' },

  ready(/* registration */) {
    paratooSentryBreadcrumbMessage('Service worker is active.', 'serviceWorker')
    useEphemeralStore().setSWRegistrationForNewContent(false)
  },

  registered(/* registration */) {
    paratooSentryBreadcrumbMessage(
      'Service worker has been registered.',
      'serviceWorker',
    )
  },

  cached(/* registration */) {
    paratooSentryBreadcrumbMessage(
      'Content has been cached for offline use.',
      'serviceWorker',
    )
  },

  updatefound(/* registration */) {
    paratooSentryBreadcrumbMessage(
      'New content is downloading.',
      'serviceWorker',
    )
  },

  updated(/* registration */) {
    paratooSentryBreadcrumbMessage(
      'New content is available; please refresh.....',
      'serviceWorker',
    )
    useEphemeralStore().setSWRegistrationForNewContent(true)
  },

  offline() {
    paratooSentryBreadcrumbMessage(
      'No internet connection found. App is running in offline mode.',
      'serviceWorker',
    )
  },

  error(err) {
    // this happen only in offline, not actually an error
    if (err.message === 'Failed to fetch') {
      return
    }
    paratooErrorHandler('Error during service worker registration:', err)
  },
})

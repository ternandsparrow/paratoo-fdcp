import { clientsClaim } from 'workbox-core'
import { cloneDeep } from 'lodash'
import { cleanupOutdatedCaches, precacheAndRoute } from 'workbox-precaching'
import {
  addPushMessage,
  deletePushMessage,
  getLastPushMessage,
} from './dexieDb/web-push-queue'

const appTitle = process.env.VUE_APP_TITLE

// precached app assets
self.__WB_DISABLE_DEV_LOGS = true
let allAssets = cloneDeep(self.__WB_MANIFEST)
const appIcon = allAssets.find(
  (asset) => asset.url.includes('monitor') && asset.url.includes('.png'),
)

clientsClaim()
precacheAndRoute(allAssets)
cleanupOutdatedCaches()
// Use skipWaiting and clientsClaim
self.addEventListener('install', () => self.skipWaiting())

self.addEventListener('activate', () => clientsClaim())

// ====== push notification logics ======
self.addEventListener('push', async function (event) {
  const data = event.data ? event.data.json() : 'no payload'

  const title = data.title || `Notification`
  const type = data.type
  const message = `${data.message}` || 'Empty message'

  // Check if any clients are open
  const clientList = await self.clients.matchAll({
    type: 'window',
  })

  const isClientActive = clientList?.some((client) => client.visibilityState)
  if (!isClientActive && self.registration.showNotification) {
    // trigger showNotification
    self.registration.showNotification(title, {
      body: message,
      icon: appIcon.url,
      badge: appIcon.url,
      tag: `${appTitle}-${type}`,
      // only work for chrome, and edge (not in ios)
      vibrate: [200, 100, 200],
    })
  }

  // at the moment we only store 1 message
  const lastMessage = await getLastPushMessage()
  if (lastMessage) {
    await deletePushMessage(lastMessage.id)
  }
  const id = await addPushMessage(data)

  const channel = new BroadcastChannel('sw-messages')
  channel.postMessage({ payload: { id, messageData: data } })
})

// open the app at notification click
self.addEventListener('notificationclick', function (event) {
  event.waitUntil(self.clients.openWindow('/'))
})

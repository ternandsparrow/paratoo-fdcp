import Dexie from 'dexie'
import { paratooWarnHandler } from 'src/misc/helpers'
const db = new Dexie('postCache')

db.version(1).stores({
  postCache: '&orgMintedUUID,data,protocolName,order,page,indexInPage', // 'id' is the primary key
})
// Function to save blob data into the cache
export const savePostCache = async (blob) => {
  try {
    await db.open() // Open the database connection
    const currentTime = Date.now()

    for (let key in blob) {
      await db.postCache.put({
        orgMintedUUID: blob[key].orgMintedUUID,
        data: JSON.stringify(blob[key].data),
        protocolName: blob[key].protocolName,
        order: blob[key].order,
        page: blob[key].page,
        indexInPage: blob[key].indexInPage,
        savedAt: currentTime,
      })
    }
    return blob // Return the blob data after successful save
  } catch (error) {
    if (error.message.includes('Key already exists in the object store')) {
      return blob
    } else {
      paratooWarnHandler('Error saving to IndexedDB', error)
    }
    return null // Return null or another default value on error
  }
}

// Function to get all records from the postCache table
export const getAllPostCache = async () => {
  try {
    const expireTime = 30 * 24 * 60 * 60 * 1000 // 30 days
    const currentTime = Date.now()
    await db.open() // Ensure the database is open
    const allRecords = await db.postCache.toArray() // Retrieve all records
    const deleteCache = allRecords.filter(
      (item) => currentTime - item.savedAt > expireTime,
    )
    for (const item of deleteCache) {
      await db.postCache.delete(item.orgMintedUUID)
    }
    const validCache = allRecords.filter(
      (item) => currentTime - item.savedAt <= expireTime,
    )
    return validCache
  } catch (error) {
    paratooWarnHandler('Error getting data from IndexedDB', error)
    return null // Return null or another default value on error
  }
}

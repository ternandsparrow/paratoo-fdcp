import Dexie from 'dexie'
import {
  cloneDeep,
  isEmpty,
} from 'lodash'
import {
  paratooErrorHandler,
  paratooWarnMessage,
} from 'src/misc/helpers'
import { v4 as uuidv4 } from 'uuid'

const historicalDataManagerDexie = new Dexie('historicalDataManagerDexie')

//tables not have to be optimised, so we can just use `data` column for all the
//data from that store state item
const v1StoreObj = {
  responses: '&queuedCollectionIdentifier, data, dateAdded',
  submitResponses: '&queuedCollectionIdentifier, data, dateAdded',
}
historicalDataManagerDexie.version(1).stores(v1StoreObj)

const tables = Object.keys(v1StoreObj)

export const historicalDataManagerDb = {
  tables: tables,
  async init() {
    try {
      historicalDataManagerDexie.open()
    } catch (error) {
      throw paratooErrorHandler('Failed to open historicalDataManagerDexie database', error)
    }
  },
  /**
   * Adds response/historical queue data to IndexDB via Dexie `Table.bulkPut()`
   * 
   * @param {String} table the table name that corresponds to the type of historical
   * data. Table must be defined when initialising Dexie
   * @param {Array.<Object>} dataArr the array of response data, where each object has a
   * `queuedCollectionIdentifier` and other response data
   * 
   * @returns {Boolean} whether the add succeeded
   */
  async bulkAddRespData({ table, dataArr }) {
    const isValidTable = validateTable({ table })
    if (!isValidTable) return false

    const mappedDataArr = []
    for (const [i, d] of cloneDeep(dataArr).entries()) {
      if (isEmpty(d)) {
        paratooWarnMessage(
          `Empty data for item at index=${i} to be inserted into ${table} table`
        )
        continue
      }
      let identifier = null
      if (d?.queuedCollectionIdentifier) {
        identifier = d.queuedCollectionIdentifier
      } else {
        identifier = `unknown-${uuidv4()}`
        paratooWarnMessage(
          `No queued collection identifier for item at index=${i} to be inserted into ${table} table. Will generate a new one: ${identifier}`
        )
      }
      mappedDataArr.push({
        queuedCollectionIdentifier: identifier,
        data: d,
        dateAdded: new Date(),
      })
    }
    //use `Table.bulkPut()` to handle when a given item has already been added (it's
    //possible we had a previous failure to add it before, and now we've synced the queue
    //again and are trying to clear the responses and submitResponses)
    const result = historicalDataManagerDexie
      .transaction(
        'rw',
        historicalDataManagerDexie[table],
        async () => {
          const result = await historicalDataManagerDexie[table]
            .bulkPut(mappedDataArr)
            .then(() => {
              return true
            })
            .catch((err) => {
              paratooErrorHandler(
                `Failed adding ${table} data`,
                err,
              )
              return false
            })
          return result
        },
      )
      .catch((err) => {
        paratooErrorHandler(
          `Transaction failed adding ${table} data`,
          err,
        )
        return false
      })
    return result
  },
  async countHistoricalDmTable({ table }) {
    const isValidTable = validateTable({ table })
    if (!isValidTable) return -1

    return await historicalDataManagerDexie[table].count()
  },
  async getAllTableData({ table }) {
    const isValidTable = validateTable({ table })
    if (!isValidTable) return []

    const result = await historicalDataManagerDexie[table].toArray()
    return result
  },
  async getTableSummary({ table }) {
    const allData = await this.getAllTableData({ table })
    const mappedSummary = allData.map(o => {
      const obj = {
        protocol: o.data.protocolInfo.name,
        dateAdded: new Date(o.dateAdded).toISOString(),
        queuedCollectionIdentifier: o.queuedCollectionIdentifier,
        models: null,
      }

      if (table === 'responses') {
        obj.models = Object.keys(o.data.resp).join(', ')
      } else if (table === 'submitResponses') {
        obj.models = Object.keys(o.data.collection).join(', ')
      }

      return obj
    })

    return mappedSummary
  },
}

function validateTable({ table }) {
  if (!tables.includes(table)) {
    paratooErrorHandler(
      `Programmer error: provided table '${table}' is not valid`,
      new Error(`Table must be one of ${tables.join(', ')}`)
    )
    return false
  }
  return true
}
import Dexie from 'dexie'

// Define the database
const db = new Dexie('WebPushQueueDB')
db.version(1).stores({
  pushMessages: '++id, messageData, timestamp, isRead',
})

// Function to add a push message to the queue
export const addPushMessage = async (messageData) =>
  await db.pushMessages.add({
    messageData,
    timestamp: new Date().toISOString(),
    isRead: false,
  })

// Function to get all push messages from the queue
export const getPushMessages = async () => {
  return await db.pushMessages.toArray()
}

// Function to get last push messages
export const getLastPushMessage = async () => {
  return await db.pushMessages.orderBy('timestamp').last()
}

export const updatePushMessageReadStatus = async (id, isRead = true) => {
  return await db.pushMessages.update(id, { isRead })
}

// Function to delete a push message from the queue by id
export const deletePushMessage = async (id) => {
  await db.pushMessages.delete(id)
}

// Function to clear all push messages from the queue
export const clearPushMessages = async () => {
  await db.pushMessages.clear()
}

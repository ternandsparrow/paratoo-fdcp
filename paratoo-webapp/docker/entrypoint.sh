#!/bin/sh
# write a runner script, that allows env var overrides, and run it
set -euxo pipefail
cd `dirname "$0"`

runnerSh=runner.sh
echo "Writing runner script"
cat <<EOF > $runnerSh
set -euxo pipefail
cd $PWD/..
echo "Setting up merged env vars"
$(sed 's/\(\w*\)=\(.*\)/export \1=${\1:-\2}/' ../.env)

echo "Replacing placeholders"
find dist/ \\
  -type f \\
  -name '*.js' \\
  -exec sed -i \\
$(sed 's/\(\w*\)=.*/    -e "s~%%\1%%~${\1}~g" \\/' ../.env)
    '{}' \;

echo "Starting server"
yarn quasar serve dist/pwa --history --port ${PORT:-3000}
EOF

echo "Running runner script"
sh $runnerSh

module.exports = {
  process(src, filename, config, options) {
    const preprocessor = require('webpack-preprocessor-loader');
    const params = {
      capacitor: process.env.MODE === 'capacitor'
    };

    return preprocessor(src, filename, config, options, { params });
  },
};

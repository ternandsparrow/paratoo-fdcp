/**
 * Left out of `run` folder so this is not run during CI.
 * Main purpose is to test the tests. Right now is just testing that selecting and
 * clearing protocols commands work properly
 */

const selectionAndLayoutModule = 'Plot Selection and Layout'
const layoutVisitProtocol = 'Plot Layout and Visit'
const conditionModule = 'condition'
const conditionProtocol = 'Condition Attributes Protocols'
const plotDescriptionModule = 'Plot Description'
const plotDescriptionEnhancedProtocol = 'Plot Description - Enhanced'
const plotDescriptionStandardProtocol = 'Plot Description - Standard'

const projectCy = 'Kitchen\\ Sink\\ TEST\\ Project'


describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
})

describe('single protocol (layout)', () => {
  it(`navigate to ${layoutVisitProtocol}`, () => {
    cy.selectProtocolIsEnabled(selectionAndLayoutModule)
    cy.dataCy('workflowBack').click()
  })
  it(`navigate to ${layoutVisitProtocol} (and clear first)`, () => {
    cy.selectProtocolIsEnabled(selectionAndLayoutModule)
    cy.dataCy('workflowBack').click()
  })
})

describe('single protocol (condition)', () => {
  it('start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(conditionModule, conditionProtocol)
  })
  it('do plot layout', () => {
    cy.plotLayout()
  })
  it(`navigate to ${conditionProtocol}`, () => {
    cy.selectProtocolIsEnabled(conditionModule)
    cy.dataCy('workflowBack').click()
  })
  it(`navigate to ${conditionProtocol} (and clear first)`, () => {
    cy.selectProtocolIsEnabled(conditionModule)
    cy.dataCy('workflowBack').click()
  })
})

describe('multiple protocols', () => {
  it('start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(plotDescriptionModule, plotDescriptionEnhancedProtocol)
  })
  it(`navigate to ${plotDescriptionEnhancedProtocol}`, () => {
    cy.selectProtocolIsEnabled(plotDescriptionModule, plotDescriptionEnhancedProtocol)
    cy.dataCy('workflowBack').click()
  })
  it(`navigate to ${plotDescriptionEnhancedProtocol} (and clear first)`, () => {
    cy.selectProtocolIsEnabled(plotDescriptionModule, plotDescriptionEnhancedProtocol)
    cy.dataCy('workflowBack').click()
  })
})
const { pub } = require("../../support/command-utils")

const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'vegetation mapping'
const protocol = 'Vegetation Mapping'

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })
})

describe('1 - survey', () => {
  it('do survey step (start)', () => {
    cy.nextStep()
  })
})
describe('2 - [validate-field] - observation 1', () => {
  it('.should() - test field id', () => {
    cy.getAndValidate('field_id').type('VA1')
  })

  it('.should() - record location', () => {
    cy.recordLocation()
  })

  it('.should() - homogeneity measure', () => {
    cy.getAndValidate('homogeneity_measure').type('1')
  })

  it('.should() - add photo', () => {
    // TODO retake photo
    cy.getAndValidate('photo', {
      isComponent: true,
      testEmptyField: ['done', -1],
    })
    cy.get('[data-cy="addImg"]').eq(0).click()
    cy.get('[data-cy="takePhoto"]').click()

    cy.getAndValidate('direction', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('North West')
    cy.getAndValidate('description', { testEmptyField: ['done', -1] }).type(
      'Line1{enter}line2{enter}line3',
    )
  })

  it('.should() - vegetation growth stage', () => {
    cy.getAndValidate('vegetation_growth_stage', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('Uneven Age')
  })

  it('.should() - disturbance', () => {
    cy.getAndValidate('disturbance', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('Limited clearing')
  })

  it('.should() - land use history', () => {
    cy.getAndValidate('land_use_history', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('Landscape')
  })

  it('.should() - fire_history', () => {
    cy.getAndValidate('fire_history', {
      isComponent: true,
      testEmptyField: ['done', -1],
    })

    cy.dataCy('Known').click()

    cy.customSelectDate({
      fieldName: 'known_fire_date',
      date: {
        year: new Date().getFullYear() + 1,
        month: 'current',
        day: 'current',
      },
    })
    cy.dataCy('known_fire_date_dateBtn')
      .nearest()
      .then((btn) => {
        cy.dismissPopups().wait(200)
        cy.get(btn).click()
        cy.get('.q-notification', { timeout: 15000 }).eq(-1).as('popup')
        cy.get('@popup').should(
          'contain.text',
          `Known Fire Date`,
        )
        cy.get('@popup').should(
          'contain.text',
          `Fire date must be in the past`,
        )
        cy.get('@popup').contains('Dismiss').click().wait(500)
      })
      cy.customSelectDate({
        fieldName: 'known_fire_date',
        date: {
          year: 'current',
          month: 'current',
          day: 'current',
        },
      })
  })

  it('.should() - comment', () => {
    cy.getAndValidate('comment', { testEmptyField: ['done', -1] }).type(
      'comment{enter}more comment',
    )
  })

  it('.should - add veg assoc', () => {
    cy.getAndValidate('stratum').selectFromDropdown(
      'Upper Storey',
      'Upper Storey',
    )
    cy.getAndValidate('vegetation_species').selectFromSpeciesList(
      'Plantae [Regnum]',
    )
    cy.getAndValidate('growth_form').selectFromDropdown('Vine', 'Vine')

    cy.getAndValidate('height_range').type(30)
    cy.getAndValidate('cover_percentage').type(80)

    cy.dataCy('done').eq(0).click()
  })
  it('.should() - doingVouchering', () => {
    cy.setCheck('doingVouchering', true)

    cy.getAndValidate('voucher_barcode', {
      isComponent: true,
      testEmptyField: ['saveVoucher'],
    })
    cy.dataCy('recordBarcode').click()

    cy.getAndValidate('voucher_type_tier_1', {
      testEmptyField: ['saveVoucher'],
    }).selectFromDropdown('Plant')

    cy.getAndValidate('voucher_type_tier_2', {
      testEmptyField: ['saveVoucher'],
    }).selectFromDropdown('Complete')

    cy.dataCy('saveVoucher').click()
    cy.get('[data-cy="done"]').eq(-1).click().wait(500)
    cy.get('[data-cy="addEntry"]').eq(-1).click().wait(500)
  })
})
describe('observation 2', () => {
  it('.should() - test field id', () => {
    cy.get('[data-cy="field_id"]').type('VA1')
  })

  it('.should() - record location', () => {
    cy.recordLocation()
  })

  it('.should() - homogeneity measure', () => {
    cy.get('[data-cy="homogeneity_measure"]').type('1')
  })

  it('.should() - add photo', () => {
    // TODO retake photo
    cy.get('[data-cy="addImg"]').eq(0).click()
    cy.get('[data-cy="takePhoto"]').click()

    cy.selectFromDropdown('direction', 'North West')
    cy.get('[data-cy="direction"] > span').should('contain.text', 'North West')
    cy.dataCy('description').type('Line1{enter}line2{enter}line3')
  })

  it('.should() - vegetation growth stage', () => {
    cy.selectFromDropdown('vegetation_growth_stage', 'Uneven Age')
    cy.dataCy('vegetation_growth_stage').should('contain.text', 'Uneven Age')
  })

  it('.should() - disturbance', () => {
    cy.selectFromDropdown('disturbance', 'Limited clearing')
  })

  it('.should() - land use history', () => {
    cy.selectFromDropdown('land_use_history', 'Landscape')
  })

  it('.should() - fire_history', () => {
    cy.dataCy('Known').click()
  })

  it('.should() - comment', () => {
    cy.dataCy('comment').type('comment{enter}more comment')
  })

  it('.should - add veg assoc', () => {
    cy.addVegetationAssociationInformation(2)
  })
  it('.should() - doingVouchering', () => {
    cy.dataCy('doingVouchering').click()
    for (let i = 0; i < 2; i++) {
      cy.dataCy('recordBarcode').click()
      cy.selectFromDropdown('voucher_type_tier_1', 'Plant')
      cy.selectFromDropdown('voucher_type_tier_2', 'Complete')

      cy.dataCy('saveVoucher').click()
    }
    cy.get('[data-cy="done"]').eq(-1).click().wait(500)
  })
})

describe('survey', () => {
  it('do survey step (end)', () => {
    cy.nextStep()
  })
})
describe('submit and publish', () => {
  // it('.should() - submit and publish', () => {
  //   cy.workflowPublish()
  // })
  pub()
})

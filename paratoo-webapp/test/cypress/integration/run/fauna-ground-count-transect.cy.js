const { pub } = require('../../support/command-utils')

const protocol = 'Fauna Ground Counts Transects'
const module = 'Fauna\\ Ground\\ Counts'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

// using different jwt methods returns different ids, unsure it a bug or feature.
// seems to grab the one from the first entry, even if we're trying to get the latest entry. It is only a component id however, so it can safely be ignored?
global.ignorePaths = ['field-reconnaissance-and-transect-set-up.transect_start_location.id']

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})

const species = [
  'Goat',
  'Pig',
  'Camel',
  'Deer',
  'Horse',
  'Wild Dog',
  'Fox',
  'Cat',
  'Rabbit',
]
function pickRandomSpecies() {
  const count = Math.floor(Math.random() * (species.length - 1)) + 1
  const shuffled = species.sort(() => 0.5 - Math.random())
  return shuffled.slice(0, count)
}
let existingTransectSurveyID
const transectIDs = []
const surveyIDs = []

describe('1 - transect setup', () => {
  context('1 - create new point', () => {
    it('survey start', () => {
      cy.nextStep()
    })
    it('1.1[validate-field] - add transect 1', () => {
      cy.window().then(($win) => {
        $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
      })
      cy.get('[aria-label="Transect ID *"]')
        .invoke('val')
        .then((id) => {
          transectIDs.push(id)
        })
      cy.recordLocation()
      cy.getAndValidate('route_type').selectFromDropdown()

      cy.getAndValidate('NVIS_major_vegetation_group').selectFromDropdown()

      cy.dataCy('done').click()
    })

    it('1.2[test-to-pass] - add transect 2', () => {
      cy.dataCy('addEntry').click()
      cy.recordLocation()
      cy.get('[aria-label="Transect ID *"]')
        .invoke('val')
        .then((id) => {
          transectIDs.push(id)
        })
      cy.selectFromDropdown('NVIS_major_vegetation_group', 'Eucalypt Woodlands')
      cy.dataCy('done').click()
      cy.nextStep()
    })
  })
})

describe('2 - Survey setup', () => {
  context(`2.1 test-to-fail`, () => {
    it(`1[validate-field] - fill in general details`, () => {
      cy.selectFromDropdown('select_transect', transectIDs[0])
      // surveyIDs
      cy.dataCy('survey_setup_ID')
        .invoke('val')
        .then((id) => {
          surveyIDs.push(id)
        })
      cy.getAndValidate('survey_intent', {
        testEmptyField: ['done', -1],
      }).selectFromDropdown('Once-off measure')

      cy.getAndValidate('route_type_photo', { isComponent: true })
      cy.dataCy('addImg').eq(0).click()
      cy.dataCy('takePhoto').should('not.be.disabled')
      cy.dataCy('takePhoto').click()
      cy.dataCy('takePhoto').should('not.be.disabled')
      cy.getAndValidate('transect_type', {
        isComponent: true,
        testEmptyField: ['done', -1],
      })
      cy.dataCy('Walked').click()
    })

    it('2[validate-field] - add equipment details', () => {
      cy.getAndValidate('equipment_details', {
        isComponent: true,
        testEmptyField: ['done', -1],
      })
      cy.getAndValidate('equipment_type').selectFromDropdown('Vehicle')
      cy.getAndValidate('make').type('Lamborghini')
      cy.getAndValidate('model').type('Aventador SVJ Coupe')
      cy.getAndValidate('description').type('Aventador SVJ Coupe')

      cy.dataCy('done').eq(0).click()
    })

    it('3[validate-field] - add equipment details', () => {
      cy.getAndValidate('sampling_type', {
        isComponent: true,
        testEmptyField: ['done', -1],
      })
      cy.get(`[data-cy="Fixed width"]`).click()

      cy.getAndValidate('transect_width', {
        testEmptyField: ['done', -1],
      }).type(22)

      cy.getAndValidate('sightability_distance', {
        testEmptyField: ['done', -1],
      }).type(22)

      cy.getAndValidate('visibility_example_photo_1', { isComponent: true })
      cy.dataCy('addImg').eq(1).click()
      cy.dataCy('takePhoto').should('not.be.disabled')
      cy.dataCy('takePhoto').click()
      cy.dataCy('takePhoto').should('not.be.disabled')
      cy.getAndValidate('visibility_example_photo_2', { isComponent: true })
      cy.dataCy('addImg').eq(2).click()
      cy.dataCy('takePhoto').should('not.be.disabled')
      cy.dataCy('takePhoto').click()
      cy.dataCy('takePhoto').should('not.be.disabled')
      const species = ['Fox', 'Cat', 'Rabbit']
      cy.getAndValidate('target_species', {
        testEmptyField: ['done', -1],
      }).selectFromDropdown(species)

      // cy.get('body').then(($body) => {
      //   const otherSpecies = $body.find('[data-cy="other"]').length > 0
      //   if (otherSpecies) {
      //     cy.get('[data-cy="other"]').type('perry the platypus').type('{enter}')
      //   }
      // })

      cy.getAndValidate('survey_time', {
        isComponent: true,
        testEmptyField: ['done', -1],
      })
      cy.get(`[data-cy="Diurnal"]`).click()
    })

    it('4[validate-field] - add observers details', () => {
      cy.getAndValidate('observer_details', {
        isComponent: true,
        testEmptyField: ['done', -1],
      })

      const roles = ['Spotter', 'Data entry', 'Driver']
      roles.forEach((role, index) => {
        const modifier = Math.floor(Date.now())
        cy.addObservers('observerName', [`Anon ${modifier + index}`])
        cy.getAndValidate('role', {
          isChildOf: 'observer_details',
        }).selectFromDropdown(role)

        cy.get('[data-cy="done"]').eq(0).click()
        if (role !== 'Driver') {
          cy.get('[data-cy="addEntry"]').eq(1).click()
        }
      })
    })
    it('5[validate-field] - weather', () => {
      cy.getAndValidate('weather', {
        isComponent: true,
        testEmptyField: ['done', -1],
      })
      // FIXME: need to init the non required single child obs first before validate its fields
      cy.dataCy('temperature').type('1')
      cy.completeWeatherForm(null, ['done', -1])
      cy.dataCy('done').click()
      cy.dataCy('addEntry').click()
    })
  })
  context(`2.2 test-to-pass`, () => {
    it(`1[validate-field] - fill in general details`, () => {
      cy.selectFromDropdown('select_transect', transectIDs[1])
      // surveyIDs
      cy.dataCy('survey_setup_ID')
        .invoke('val')
        .then((id) => {
          surveyIDs.push(id)
        })
      cy.selectFromDropdown('survey_intent', 'Once-off measure')
      cy.dataCy('Walked').click()
      for (let i = 0; i < 3; i++) {
        cy.dataCy('addImg').eq(i).click()
        cy.dataCy('takePhoto').should('not.be.disabled')
        cy.dataCy('takePhoto').click()
        cy.dataCy('takePhoto').should('not.be.disabled')
      }
    })

    it('2[validate-field] - add equipment details', () => {
      cy.selectFromDropdown('equipment_type', 'Vehicle')
      cy.dataCy('make').type('Lamborghini')
      cy.dataCy('model').type('Aventador SVJ Coupe')
      cy.dataCy('description').type('Aventador SVJ Coupe')
      cy.dataCy('done').eq(0).click()
    })

    it('3[validate-field] - add equipment details', () => {
      cy.get(`[data-cy="Distance sample"]`).click()

      cy.dataCy('sightability_distance').type(22)

      const species = ['Fox', 'Cat', 'Rabbit']

      cy.selectFromDropdown('target_species', species)

      // cy.get('body').then(($body) => {
      //   const otherSpecies = $body.find('[data-cy="other"]').length > 0
      //   if (otherSpecies) {
      //     cy.get('[data-cy="other"]').type('perry the platypus').type('{enter}')
      //   }
      // })
      cy.get(`[data-cy="Diurnal"]`).click()
    })

    it('4[validate-field] - add observers details', () => {
      const roles = ['Spotter', 'Data entry', 'Driver']
      roles.forEach((role, index) => {
        const modifier = Math.floor(Date.now())
        cy.addObservers('observerName', [`Anon ${modifier + index}`])
        cy.selectFromDropdown('role', role)
        cy.get('[data-cy="done"]').eq(0).click()
        if (role !== 'Driver') {
          cy.get('[data-cy="addEntry"]').eq(1).click()
        }
      })
    })
    it('5[validate-field] - weather', () => {
      // FIXME: need to init the non required single child obs first before validate its fields
      cy.dataCy('temperature').type('1')
      cy.completeWeatherForm(null, ['done', -1])
      cy.dataCy('done').click()
      cy.dataCy('workflowNextButton').click()
    })
  })
})

describe('3 - Conduct the survey', () => {
  context('3.1 - test-to-fail', () => {
    it('3.1 start survey', () => {
      // in schema the type is string but in vue file it's changed to select so getAndValidate() doesn't work
      cy.testEmptyField('Field: "Transect ID": This field is required', 'done')
      cy.selectFromDropdown('transect_ID', transectIDs[0])
      cy.testEmptyField('Field: "Setup ID": This field is required', 'done')
      cy.selectFromDropdown('setup_ID', surveyIDs[0])
      cy.dataCy('startSurvey', {timeout: 10000}).should('exist')
      cy.dataCy('startSurvey').click()
      cy.dataCy('observation', {timeout: 10000}).should('exist')
    })

    const observationType = ['Seen', 'Heard', 'Seen and heard']
    it('3.2[validate-field] - start transect, add observation 1', () => {
      cy.getAndValidate('observation', {
        isComponent: true,
        testEmptyField: ['done'],
      })
      cy.getAndValidate('attributable_fauna_species').selectFromDropdown('Cat')
      cy.getAndValidate('count').type(10)

      const obsType =
        observationType[
          Math.floor(Math.random() * (observationType.length - 1))
        ]
      cy.getAndValidate('observation_type').selectFromDropdown(obsType)
      cy.getAndValidate('age_class').selectFromDropdown()
      cy.getAndValidate('sex').selectFromDropdown()
      cy.getAndValidate('cat_coat_colour').selectFromDropdown()
      cy.getAndValidate('comments', 'comments')
      cy.dataCy('done').eq(0).click()
    })

    it('3.3[validate-field] - start transect, add observation 2', () => {
      cy.dataCy('addEntry').eq(0).click()
      // cy.getAndValidate('attributable_fauna_species').selectFromDropdown('perry the platypus') //conditional fields
      cy.dataCy('count').type(10)
      cy.dataCy('observation_type').selectFromDropdown()
      cy.dataCy('done').eq(0).click()
    })

    it('3.4[test-to-pass] - end survey', () => {
      cy.dataCy('endSurvey').click()
      cy.dataCy('done').click()
      cy.dataCy('addEntry').last().click()
    })
  })
  context('3.2 - test-to-pass', () => {
    it('1 start survey', () => {
      // in schema the type is string but in vue file it's changed to select so getAndValidate() doesn't work
      cy.testEmptyField('Field: "Transect ID": This field is required', 'done')
      cy.selectFromDropdown('transect_ID', transectIDs[1])
      cy.testEmptyField('Field: "Setup ID": This field is required', 'done')
      cy.selectFromDropdown('setup_ID', surveyIDs[1])
      cy.dataCy('startSurvey').click()
    })

    const observationType = ['Seen', 'Heard', 'Seen and heard']
    it('2[validate-field] - start transect, add observation 1', () => {
      cy.getAndValidate('attributable_fauna_species').selectFromDropdown('Cat')
      cy.getAndValidate('count').type(10)

      const obsType =
        observationType[
          Math.floor(Math.random() * (observationType.length - 1))
        ]
      cy.getAndValidate('observation_type').selectFromDropdown(obsType)
      cy.getAndValidate('distance').type(22)
      cy.getAndValidate('direction').type(22)
      cy.getAndValidate('age_class').selectFromDropdown()
      cy.getAndValidate('sex').selectFromDropdown()
      cy.getAndValidate('cat_coat_colour').selectFromDropdown()
      cy.getAndValidate('comments', 'comments')
      cy.dataCy('done').eq(0).click()
    })

    it('3[validate-field] - start transect, add observation 2', () => {
      cy.dataCy('addEntry').eq(0).click()
      // cy.getAndValidate('attributable_fauna_species').selectFromDropdown('perry the platypus')
      cy.dataCy('count').type(10)
      cy.dataCy('observation_type').selectFromDropdown()
      cy.dataCy('done').eq(0).click()
    })

    it('4[test-to-pass] - end survey', () => {
      cy.dataCy('endSurvey').click()
      cy.dataCy('done').click()
      cy.nextStep()
    })
    pub({ shapeShape: true, shareValues: false, shareValuesWith: false })
    // it('5[test-to-pass] - end survey and submit', () => {
    //   cy.workflowPublish()
    // })
  })
})

describe('4 - transect setup', () => {
  context('1 - select existing', () => {
    it('.should() - assert navigation to ' + protocol + ' is possible', () => {
      cy.selectProtocolIsEnabled(module, protocol)
    })
    it('survey start', () => {
      cy.nextStep()
    })
    it('1.1[validate-field] - check auto-fill', () => {
      cy.dataCy('selectExisting').click()
      cy.selectFromDropdown('site_id', transectIDs[0])

      cy.dataCy('done').click()
      cy.nextStep()
    })
  })

  context(`2 survey setup`, () => {
    it(`1[validate-field] - fill in general details`, () => {
      cy.selectFromDropdown('select_transect', transectIDs[0])
      // surveyIDs
      cy.dataCy('survey_setup_ID')
        .invoke('val')
        .then((id) => {
          existingTransectSurveyID = id
        })
      cy.selectFromDropdown('survey_intent', 'Once-off measure')
      cy.dataCy('Walked').click()
      for (let i = 0; i < 3; i++) {
        cy.dataCy('addImg').eq(i).click()
        cy.dataCy('takePhoto').click()
      }
    })

    it('2[validate-field] - add equipment details', () => {
      cy.selectFromDropdown('equipment_type', 'Vehicle')
      cy.dataCy('make').type('Lamborghini')
      cy.dataCy('model').type('Aventador SVJ Coupe')
      cy.dataCy('description').type('Aventador SVJ Coupe')
      cy.dataCy('done').eq(0).click()
    })

    it('3[validate-field] - fill in general fields', () => {
      cy.get(`[data-cy="Distance sample"]`).click()

      cy.dataCy('sightability_distance').type(22)

      const species = ['Fox', 'Cat', 'Rabbit']

      cy.selectFromDropdown('target_species', species)

      // cy.get('body').then(($body) => {
      //   const otherSpecies = $body.find('[data-cy="other"]').length > 0
      //   if (otherSpecies) {
      //     cy.get('[data-cy="other"]').type('perry the platypus').type('{enter}')
      //   }
      // })
      cy.get(`[data-cy="Diurnal"]`).click()
    })

    it('4[validate-field] - add observers details', () => {
      const roles = ['Spotter', 'Data entry', 'Driver']
      roles.forEach((role, index) => {
        const modifier = Math.floor(Date.now())
        cy.addObservers('observerName', [`Anon ${modifier + index}`])
        cy.selectFromDropdown('role', role)
        cy.get('[data-cy="done"]').eq(0).click()
        if (role !== 'Driver') {
          cy.get('[data-cy="addEntry"]').eq(1).click()
        }
      })
    })
    it('5[validate-field] - weather', () => {
      // FIXME: need to init the non required single child obs first before validate its fields
      cy.dataCy('temperature').type('1')
      cy.completeWeatherForm(null, ['done', -1])
      cy.dataCy('done').click()
      cy.dataCy('workflowNextButton').click()
    })
  })

  context('3 - conduct survey', () => {
    it('1 start survey', () => {
      // in schema the type is string but in vue file it's changed to select so getAndValidate() doesn't work
      cy.testEmptyField('Field: "Transect ID": This field is required', 'done')
      cy.selectFromDropdown('transect_ID', transectIDs[0])
      cy.testEmptyField('Field: "Setup ID": This field is required', 'done')
      cy.selectFromDropdown('setup_ID', existingTransectSurveyID)
      cy.dataCy('startSurvey').click()
    })

    const observationType = ['Seen', 'Heard', 'Seen and heard']
    it('2[validate-field] - add observation', () => {
      cy.getAndValidate('attributable_fauna_species').selectFromDropdown('Cat')
      cy.getAndValidate('count').type(10)

      const obsType =
        observationType[
          Math.floor(Math.random() * (observationType.length - 1))
        ]
      cy.getAndValidate('observation_type').selectFromDropdown(obsType)
      cy.getAndValidate('distance').type(22)
      cy.getAndValidate('direction').type(22)
      cy.getAndValidate('age_class').selectFromDropdown()
      cy.getAndValidate('sex').selectFromDropdown()
      cy.getAndValidate('cat_coat_colour').selectFromDropdown()
      cy.getAndValidate('comments', 'comments')
      cy.dataCy('done').eq(0).click()
    })

    it('4[test-to-pass] - end survey', () => {
      cy.dataCy('endSurvey').click()
      cy.dataCy('done').click()
      cy.nextStep()
    })

    // it('5[test-to-pass] - end survey and submit', () => {
    //   cy.workflowPublish()
    // })
    pub({ shapeShape: true, shareValues: false, shareValuesWith: false })
  })
})


// ==== DATA SHEET MODE ==== // 

describe('data sheet mode', () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })

  it('select data sheet mode in survey step', () => {
    cy.get('.q-checkbox:contains(I\'m doing data entry from data sheet)').as('dataSheetbtn').should('have.attr', 'aria-checked', 'false')
    cy.get('@dataSheetbtn').toggle()
    cy.get('.q-btn:contains(No)').click()
    cy.get('@dataSheetbtn').should('have.attr', 'aria-checked', 'false')
    cy.get('@dataSheetbtn').toggle()
    cy.get('.q-btn:contains(Yes)').click()
    cy.get('@dataSheetbtn').should('have.attr', 'aria-checked', 'true')
    cy.nextStep()
  })

  it('add transect', () => {
    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
    })
    cy.get('[aria-label="Transect ID *"]')
      .invoke('val')
      .then((id) => {
        transectIDs.push(id)
      })
    cy.recordLocation()
    cy.getAndValidate('route_type').selectFromDropdown()
    cy.getAndValidate('NVIS_major_vegetation_group').selectFromDropdown()
    cy.dataCy('done').click()
    cy.nextStep()

  })

  it(`fill in general details`, () => {
    cy.selectFromDropdown('select_transect', null)
    // surveyIDs
    cy.dataCy('survey_setup_ID')
      .invoke('val')
      .then((id) => {
        surveyIDs.push(id)
      })
    cy.selectFromDropdown('survey_intent', 'Once-off measure')
    cy.dataCy('Walked').click()
    for (let i = 0; i < 3; i++) {
      cy.dataCy('addImg').eq(i).click()
      cy.dataCy('takePhoto').should('not.be.disabled')
      cy.dataCy('takePhoto').click()
      cy.dataCy('takePhoto').should('not.be.disabled')
    }
  })

  it('add equipment details', () => {
    cy.selectFromDropdown('equipment_type', 'Vehicle')
    cy.dataCy('make').type('Lamborghini')
    cy.dataCy('model').type('Aventador SVJ Coupe')
    cy.dataCy('description').type('Aventador SVJ Coupe')
    cy.dataCy('done').eq(0).click()

  })

  it('add equipment details', () => {
    cy.get(`[data-cy="Distance sample"]`).click()

    cy.dataCy('sightability_distance').type(22)

    const species = ['Fox', 'Cat', 'Rabbit']

    cy.selectFromDropdown('target_species', species)

    cy.get(`[data-cy="Diurnal"]`).click()
  })

  it('add observers details', () => {
    const roles = ['Spotter', 'Data entry', 'Driver']
    roles.forEach((role, index) => {
      const modifier = Math.floor(Date.now())
      cy.addObservers('observerName', [`Anon ${modifier + index}`])
      cy.selectFromDropdown('role', role)
      cy.get('[data-cy="done"]').eq(0).click()
      if (role !== 'Driver') {
        cy.get('[data-cy="addEntry"]').eq(1).click()
      }
    })
  })
  it('weather', () => {
    cy.dataCy('temperature').type('1')
    cy.completeWeatherForm(null, ['done', -1])
    cy.dataCy('done').click()
    cy.dataCy('workflowNextButton').click()
  })


  it('start survey', () => {
    cy.selectFromDropdown('transect_ID', null)
    cy.selectFromDropdown('setup_ID', null)
    cy.getAndValidate('.q-field:contains(Survey Duration)').type(5)
  })

  it('add details', () => {
    cy.get(':contains(You\'re in datasheet mode)').should('exist')
    cy.recordLocation({
      btnEqIndex: 0,
    })
    cy.getAndValidate('speed').type(20)
    cy.recordLocation({
      btnEqIndex: 1,
    })

  })
  it('add observation', () => {
    cy.dataCy('attributable_fauna_species').selectFromDropdown('Cat')
    cy.dataCy('count').type(10)
    cy.dataCy('observation_type').selectFromDropdown('Heard')
    cy.dataCy('distance').type(22)
    cy.dataCy('direction').type(22)
    cy.dataCy('age_class').selectFromDropdown()
    cy.dataCy('sex').selectFromDropdown()
    cy.dataCy('cat_coat_colour').selectFromDropdown()
    cy.dataCy('comments').type('comments')
    cy.dataCy('done').eq(0).click()
  })

  it('end survey', () => {
    cy.dataCy('end_time').should('be.empty')
    cy.selectDateTime({
      fieldName: 'end_time',
      date: {
        year: 'current',
        month: 'current',
        day: 10,
      },
      time: {
        hour: 10,
        minute: 'current',
      },
    })
    cy.recordLocation({
      btnEqIndex: -1,
    })
    cy.dataCy('done').eq(-1).click()
    cy.nextStep()
  })

  pub({ shapeShape: true, shareValues: false, shareValuesWith: false })
})


const { pub } = require('../../support/command-utils')

const protocol =
  'NESP\\ Grassy\\ Weeds\\ Aerial\\ Survey\\ -\\ Conduct\\ Survey'
const project = 'Grassy\\ Weeds\\ Aerial\\ Survey\\ Project'

// used at the end to create the csv fixture that will be compared against  in the processing tesr
let desktop_setup_id //from summary
let desktop_survey_name
let field_survey_name
let table

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('NESPTestUser', 'paratoo')
  })

  it('start collection button should be disabled', () => {
    cy.selectProtocolIsEnabled(null, protocol, project)
  })
  it('try to finish early', () => {
    cy.get('.q-stepper__title:contains(Summary)').should('not.exist')
  })
  it('get desktop setup', () => {
    cy.loadLocalStore('apiModels', (apiModels) => {
      const surveys = apiModels?.models?.['grassy-weeds-survey']
      console.log('surveys:', surveys)
      //reverse so we're searching from the back (we want the last)
      const latestSetup = surveys.toReversed().find(
        s => s.survey_type.symbol === 'D'
      )
      console.log('latestSetup:', latestSetup)
      desktop_survey_name = Cypress._.cloneDeep(latestSetup?.survey_name)
    })
  })
  it('check desktop setup', () => {
    cy.wrap(desktop_survey_name)
      .should('match', /dev_grassy_survey_(.*)/)
  })
})

describe('survey', () => {
  it('no desktop was completed', () => {
    cy.dataCy('desktop_setup').selectFromDropdown(
      'No desktop setup was completed',
    )
    cy.dataCy('desktop_setup').paratooDialog(
      'button:contains(Change)',
      (dialog) => {
        expect(dialog.text()).to.include('Confirm')
        expect(dialog.text()).to.include(
          'To update the desktop setup you wil need to discard the fields below. Do you want to continue?',
        )
        cy.get('.q-btn').contains('No').click()
      },
    )
    cy.dataCy('desktop_setup')
      .parents('.q-field')
      .should('have.attr', 'aria-disabled', 'true')
    cy.dataCy('desktop_setup').paratooDialog(
      'button:contains(Change)',
      (dialog) => {
        cy.get('.q-btn').contains('Yes').click()
      },
    )
    cy.dataCy('desktop_setup').parents('.q-field')
    // .parents('.q-field').children('button:contains(Change)').click()
    // cy.get('.q-card').within(() => {

    // })
  })

  it('setup', () => {
    cy.dataCy('desktop_setup')
      .click()
      .then(() => {
        cy.withinSelectMenu({
          persistent: true,
          fn: () => {
            cy.get('.q-item[role=option]')
              .its('length')
              .then((length) => {
                if (length === 1) {
                  throw new Error(
                    'Missing previous surveys. Please run the survey spec first.\nIf it has already run, check the network requests and REDIS for incorrect data. ',
                  )
                }
              })
          },
        })
      })
    cy.dataCy('desktop_setup').click().wait(100) // close the dropdown
    cy.dataCy('desktop_setup').selectFromDropdown(desktop_survey_name)
    cy.dataCy('desktop_setup').paratooHelperDialog((dialog) => {
      expect(dialog.text()).to
        .include(`Select a desktop survey (or specify 'none') that has been synced to the cloud and does \
not already have a field survey to autofill the below fields (that you can then append if needed).Once you select a \
desktop survey, you cannot change it unless you discard the existing data using the 'change' button.`)
    })
  })
  it('field survey name', () => {
    field_survey_name = `${desktop_survey_name}_survey_${Math.floor(Math.random() * 10000)}`
    cy.getAndValidate(`field_survey_name`).type(
      field_survey_name,
    )

  })

  it('observer aircraft position', () => {
    cy.getAndValidate('observer_aircraft_position')
      .parents('.q-list')
      .find('[data-cy="Port"]')
      .toggle()
  })

  it('auto-fill is correct', () => {
    cy.dataCy('uploadFile').should('contain.value', 'test.kml')
    cy.dataCy(`target_species_1`).should('contain.text', 'Para grass')
    cy.dataCy(`target_species_2`).should('contain.text', 'Gamba grass')
    cy.dataCy(`target_species_3`).should('contain.text', 'Mimosa')
    cy.dataCy(`target_species_4`).should('contain.text', 'Siam weed')
    cy.dataCy(`target_species_5`).should('contain.text', 'Olive Hymenahcne')
  })

  it('Field Survey Observer', () => {
    cy.getAndValidate('observerName')
    cy.addObservers('observerName', ['Tom', 'Bill', 'Ben'])
  })
  it('finish survey', () => {
    cy.nextStep()
  })
  // .q-notification will tell you its auto filled
  // assert the auto filled info matches the survey previously made
  //  - could fetch it and check, or just hardcode the info from setup spec
})

describe('conduct survey', () => {
  it('survey', () => {
    // TODO end survey before selecting anything to make sure it still lets up go back in instead of locking us out.
    cy.get('.q-btn:contains(Start Survey)').click()

    cy.get('.q-btn:contains(Show summary)').click()
    cy.get('.q-btn:contains(Show map)').click()
    // TODO does map exist and show correct coordinates

    cy.get('.q-btn:contains(Show summary)').click()
    // TODO assert items are being added correctly

    let prev
    cy.get('div:contains(Point number:)')
      .eq(-1)
      .siblings()
      .eq(-1)
      .then((e) => {
        prev = e.text()
      })
    cy.get('.q-btn:contains(Pause)').click()
    cy.wait(500)
    cy.get('div:contains(Point number:)')
      .eq(-1)
      .siblings()
      .eq(-1)
      .then((e) => {
        // TODO maybe compare in a small range to account for possible mistiming on getting the value and clicking pause
        expect(e.text()).to.eq(prev)
      })
    cy.get('.q-btn:contains(Resume)').click()

    // TODO assert timer pauses
    cy.get('.q-btn:contains(Hide Navigator)').click()
    cy.get('.q-btn:contains(Hide Navigator)').should('not.exist')

    cy.get('.q-btn:contains(Show Navigator)').click()
    cy.get('.q-dialog')
      .find('div:contains(Primary species: Para grass)')
      .eq(-1)
      .should('exist')

    cy.get('.q-btn:contains(End Survey)')
    cy.get('.q-btn:contains(Score other species)').click()
    cy.get('.q-btn:contains(Cancel)').should('exist')
    cy.get('.q-btn:contains(Para 6)').should('not.exist')
    cy.get('.q-btn:contains(Gamba grass)').should('exist')
    cy.get('.q-btn:contains(Mimosa)').should('exist')
    cy.get('.q-btn:contains(Siam weed)').should('exist')
    cy.get('.q-btn:contains(Olive Hymenahcne)').should('exist')
    cy.get('.q-btn:contains(Mimosa)').click()
    cy.get('.q-dialog')
      .find('div:contains(species: Mimosa)')
      .eq(-1)
      .should('exist')

    cy.get('.q-btn:contains(Para 6)').click()
    cy.get('.q-btn:contains(Mimosa 6)').click()

    cy.get('table')
      .eq(1)
      .paratooTable()
      .csvFormat()
      .parseCSV()
      .then(dataTable => {
        cy.wrap([
          [
            'Observation Number',
            'Species',
            'Density Category',
            'Speed',
            'Accuracy',
            'Heading',
          ],
          ['1', 'Para grass', 'Present', '0', '0', '0'],
          ['2', 'Mimosa', 'Present', '0', '0', '0'],
        ])
          .csvFormat()
          .parseCSV()
          .then(testTable => {
            const headers = ['Observation Number', 'Species', 'Density Category']
            for (const header of headers) {
              expect(dataTable[header]).to.deep.eq(testTable[header])
            }
          })
      })



    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -28.7, lng: 132.8 }, true)
    })

    cy.get('.q-btn:contains(Mimosa 4)').click()
    cy.wait(2000)
    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -31, lng: 132 }, true)
    })
    cy.get('.q-btn:contains(Para 5)').click()
    cy.wait(2000)
    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -41, lng: 131 }, true)
    })
    cy.get('.q-btn:contains(Para 5)').click()
    cy.get('.q-btn:contains(Change species)').click()
    cy.get('.q-btn:contains(Olive Hymenahcne)').click()
    cy.get('.q-dialog')
      .find('div:contains(species: Olive Hymenahcne)')
      .eq(-1)
      .should('exist')
    cy.get('.q-btn:contains(Olive 3)').click()
    cy.get('.q-btn:contains(Para 4)').click()
    cy.get('.q-btn:contains(Para 3)').click()
    cy.get('.q-btn:contains(Para 2)').click()
    cy.get('.q-btn:contains(No Para)').click()
    cy.get('span.q-table__bottom-item:contains(Records per page:)')
      .siblings(':contains(arrow_drop_down)')
      .eq(-1)
      .click()
    cy.get('.q-item:contains(All)').eq(-1).click()
    cy.get('table')
      .eq(1)
      .paratooTable()
      .csvFormat()
      .parseCSV()
      .then((dataTable) => {
        cy.log(dataTable)
        cy.wrap([
          [
            'Observation Number',
            'Species',
            'Density Category',
            'Speed',
            'Accuracy',
            'Heading',
          ],
          ['1', 'Para grass', 'Present', '0', '0', '0'],
          ['2', 'Mimosa', 'Present', '0', '0', '0'],
          ['3', 'Mimosa', '10-50%', '0', '0', '0'],
          ['4', 'Para grass', '>50%', '0', '0', '0'],
          ['5', 'Para grass', '>50%', '0', '0', '0'],
          ['6', 'Olive Hymenahcne', '1-10%', '0', '0', '0'],
          ['7', 'Para grass', '10-50%', '0', '0', '0'],
          ['8', 'Para grass', '1-10%', '0', '0', '0'],
          ['9', 'Para grass', '<1%', '0', '0', '0'],
          ['10', 'Para grass', 'None', '0', '0', '0'],
        ])
          .csvFormat()
          .parseCSV()
          .then(testTable => {
            const headers = ['Observation Number', 'Species', 'Density Category']
            for (const header of headers) {
              expect(dataTable[header]).to.deep.eq(testTable[header])
            }
          })
      })

    cy.get('.q-btn:contains(End Survey)').click()

    // confirmation yes/no
    cy.get('span:contains(End)').eq(-1).click()
  })

  it('update fixture with observations', () => {
    cy.log(
      desktop_survey_name.split(' ')[0],
      field_survey_name
    )
    cy.get('.q-table__control i.q-icon').eq(0).click()
    cy.get('.q-item__label span:contains(All)').click()
    cy.get('table')
      .paratooTable()
      .then((e) => {
        table = e
        cy.log('table1', Cypress._.cloneDeep(table))
        cy.log('table2', Cypress._.cloneDeep(e))
      })
    cy.nextStep()
    cy.dataCy('summaryTable-desktop_setup').invoke('text').then(txt => {
      desktop_setup_id = txt
      // basically creating a fixture to to test against
      // add header, then the values
      table[0] = table[0].map(header => header.replaceAll(' ', '_').toLowerCase())

      table[0].unshift('field_survey_name')
      for (let [i, row] of table.entries()) {
        if (i > 0) row.unshift(field_survey_name)
      }
      table[0].unshift('desktop_survey_name')
      for (let [i, row] of table.entries()) {
        if (i > 0) row.unshift(desktop_survey_name.split(' ')[0])
      }
    })
    cy.get('[data-cy="Grassy Weeds Observation"]').click()

  })
  pub()
  it('get desktop survey id', () => {
    cy.log('table', Cypress._.cloneDeep(table))
    cy.doRequest('grassy-weeds-surveys')
      .its('body')
      .then((data) => {
        // console.log('aaaaaaaaaa', desktop_setup_id)
        // const current_field_survey = data.find(survey => {
        //   console.log('aaaaa', survey, survey?.desktop_setup)
        //   return survey?.desktop_setup?.id == desktop_setup_id
        // })
        // console.log('aaaaaaaaaa', current_field_survey)
        // table[0].unshift('survey_id')
        // for (let [i, row] of table.entries()) {
        //   if (i > 0) row.unshift(current_field_survey.id)
        // }
      })
    cy.wrap(table)
      .csvFormat()
      .then((csvContent) => {
        table = csvContent
      })
  })
})

let survey_for_collision_test

describe('test survey name collision', () => {
  it('start collection', () => {
    cy.selectProtocolIsEnabled(null, protocol, project)
  })
  it('get desktop setup to test name collision', () => {
    cy.loadLocalStore('apiModels', (apiModels) => {
      const surveys = apiModels?.models?.['grassy-weeds-survey']
      console.log('surveys:', surveys)
      const latestSetup = surveys.find(
        s => s.survey_type.symbol === 'F'
      )
      survey_for_collision_test = Cypress._.cloneDeep(latestSetup?.field_survey_name)
    })
  })
  it('[test-to-fail] survey name', () => {
    cy.dataCy('desktop_setup').selectFromDropdown(
      'No desktop setup was completed',
    )
    cy.dataCy('field_survey_name').type(survey_for_collision_test).blur()
    cy.dataCy('field_survey_name')
      .parents('.q-field')
      .contains('The input name collides with an existing Field Survey Name in a previous Conducted Field Survey. Please ensure the name is unique.')
      .should('exist')
  })
  it('exit protocol', () => {
    cy.dataCy('workflowBack').click()
  })
})


// =========PROCESSING========== //

function checkOptionNumber() {
  return cy.get('.q-virtual-scroll__content .q-item').its('length')
}

describe('Landing', () => {

  it('go to processing', () => {
    cy.get(`[data-cy="${'NESP\\ Grassy\\ Weeds\\ Aerial\\ Survey\\ -\\ Post-survey\\ Processing'}"]`).click({ timeout: 20000 })
  })
})

describe('survey', () => {
  let numberOfSurveys = 0
  it('get toal survey number', () => {
    // cy.dataCy('toggleArchive').toggle(true)
    cy.dataCy('selectSurveyToExport')
      .click()
      .then(() => {
        checkOptionNumber().then((count) => {
          numberOfSurveys = count
        })
      })
  })
  it('select survey', () => {

    cy.dataCy('selectSurveyToExport')
      .click()
    cy.get('.q-field').eq(0).as('survey_dropdown')
    cy.get('.q-field')
      .eq(0)
      .as('survey_dropdown')
      .selectFromDropdown(/dev_grassy_survey/)
    cy.get('.q-field')
      .eq(0)
      .as('survey_dropdown')
      .find('span')
      .invoke('text')
      .should('match', /dev_grassy_survey/)
    cy.get('.q-field').eq(0).as('survey_dropdown').paratooClear()
    cy.get('.q-field')
      .eq(0)
      .as('survey_dropdown')
      .selectFromDropdown(/dev_grassy_survey/)
  })
  it('export survey', () => {
    cy.task('downloads').then((downloads) => {
      cy.get(
        '.q-btn:contains(Export Survey (Download CSV file to your device))',
      ).click()
      cy.wait(5000)
        .then(() => {
          cy.task('downloads').then(downloadsAfter => {
            expect(downloadsAfter).to.have.length.greaterThan(downloads.length)
          })
        }) // just for download to finish before checking
    })
  })

  it('verify the downloaded file', () => {
    cy.task('downloads').then((e) => {
      // FIXME Setup for the pipeline on the assumption that one grassy weeds file is downloaded. Running local requires cleanup (empty cypress/downloads).
      cy.readFile(`cypress/downloads/${e.find(fileName => (new RegExp('dev_grassy_survey')).test(fileName))}`)
        .parseCSV()
        .then(newCSV => {
          cy.wrap(table)
            .parseCSV()
            .then(fixtureCSV => {
              // deeply compares all column values at once. fixtureCSV['location'] would return an array of the location from every row, in order
              cy.log('Using downloaded file to compare to one created from the conduct survey phase to assure data integrity', newCSV, fixtureCSV)
              cy.wrap(null, { log: false }).then(() => {

                // FIXME 'accuracy', 'speed', and 'heading' are unreliable during a test run so they are ignored - they ideally should not be
                const ignoreList = ['utc_date', 'utc_time', 'accuracy', 'speed', 'heading']
                const headers = fixtureCSV.headers.filter((h) => ignoreList.some(e => h !== e))
                for (const header of headers) {
                  // fixtureCSV has a single 'location' header. whereas newCSV has 'lat' and 'lng' headers

                  // fixture's 'location' column is a single column that combines lat and lng as a string e.g., "lat: 33, lng: 50"
                  if (header === 'location') {
                    // split the location string into two arrays of values so that they can be compared against the fixture's 'lat' and 'lng' columns
                    let [lat, lng] = fixtureCSV['location'].reduce((acc, str) => {
                      const [lat, lng] = str.split(',').map(pair => {
                        const [key, value] = pair.trim().split(':')
                        return value.trim()
                      })
                      acc[0].push(lat)
                      acc[1].push(lng)
                      return acc
                    }, [[], []])

                    expect(newCSV['lat']).to.deep.eq(lat)
                    expect(newCSV['lng']).to.deep.eq(lng)
                  } else {
                    expect(
                      newCSV[header]
                    ).to.deep.eq(
                      fixtureCSV[header],
                      `Comparing [${header}] columns for downloaded csv and fixture`)
                  }
                }
              })
            })
        })
    })

  })
  it('archive survey', () => {
    cy.get('.q-btn:contains(back)').click()
    cy.get('[data-cy="NESP Grassy Weeds Aerial Survey - Post-survey Processing"]').click()
    cy.wait(500)
    cy.dataCy('toggleArchive').toggle(false)
    cy.doRequest('grassy-weeds-surveys')
      .its('body')
      .then((data) => {
        // get the archived dropdown - it should match the length of archived field surveys
        cy.dataCy('allSurveysArchived')
          .should('exist')
          .and('contain.text', 'All surveys have been exported and archived')
        cy.dataCy('toggleArchive').toggle(true)
        cy.dataCy('selectSurveyToExport')
          .click()
        cy.get('.q-item:visible').then((items) => {
          expect(items.length).to.eq(data.filter(surveys => surveys?.survey_type?.id == 2 && surveys.archived).length)
        })
      })
    cy.get('.q-btn:contains(back)').click()
  })
})



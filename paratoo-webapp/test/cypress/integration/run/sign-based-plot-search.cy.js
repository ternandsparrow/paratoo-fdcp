const { pub } = require("../../support/command-utils")

const protocol = 'Sign-based Fauna - Plot Sign Search'
const module = 'Sign-based Fauna Surveys'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

// duration isn't in schema for sign-based-nearby-track-plot, so it's assumedly app only. 
global.ignorePaths = ['sign-based-nearby-track-plot.duration']

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  // it('.should() - start collection button should be disabled', () => {
  //   cy.selectProtocolIsDisabled(module, protocolCy)
  // })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
    cy.mockLocation({ lat: -34.971863, lng: 138.637756 })
  })
})

describe(protocol, () => {
  it('complete transect setup', () => {
    cy.mockLocation(-23.7, 132.8)

    cy.getAndValidate('survey_intent', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('Repeated measure')
    cy.dataCy('nearby_track_transect_survey').click()

    cy.getAndValidate('target_species', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown(['Deer', 'Other'])
    cy.dataCy('target_deer').selectFromDropdown('Fallow deer (Dama dama)')
    cy.dataCy('other_species').type('foo{enter}bar{enter}')
    cy.wait(500)
    cy.dataCy('done').first().click()

    cy.getAndValidate('tracking_substrate', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('Gravel')

    cy.getAndValidate('observer_details', {
      isComponent: true,
      testEmptyField: ['workflowNextButton'],
    })
    cy.dataCy('observer_name').type(
      'John Cena ' + (Math.random() + 1).toString(36).substring(7) + '{enter}',
    ) //random characters for uniqueness https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
    cy.selectFromDropdown('role', 'Data entry')
    cy.get('[data-cy="done"] > .q-btn__content > .block').click()

    cy.getAndValidate('weather', {
      isComponent: true,
      testEmptyField: ['workflowNextButton'],
    })
    cy.completeWeatherForm()

    cy.nextStep()
  })
  it('Commence search', () => {
    cy.dataCy('plot_type').selectFromDropdown('Biodiversity plot')
    // cy.getAndValidate('start_location', { isComponent: true })
    cy.recordLocation()

    cy.dataCy('corner_start_point').selectFromDropdown()

    // cy.getAndValidate('start_location_photo', { isComponent: true })
    cy.dataCy('addImg').eq(0).click().wait(50)
    cy.dataCy('takePhoto').eq(0).click().wait(50)
    cy.get('[data-cy="done"]').eq(0).click()
  })
  it('Sign observed', () => {
    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
      cy.recordLocation({
        btnEqIndex: -1,
      })
    })
    cy.getAndValidate('sign_type').selectFromDropdown('Tracks')
    cy.dataCy('attributable_fauna_species')
      .selectFromDropdown('Fallow deer (Dama dama)')
    cy.getAndValidate('sign_age').selectFromDropdown('Fresh 1-2 days')
    cy.getAndValidate('age_class').selectFromDropdown('Juvenile')

    cy.dataCy('count').type(4)

    cy.getAndValidate('photo', { isComponent: true })
    cy.dataCy('addImg').eq(-1).click().wait(500)
    cy.dataCy('takePhoto').eq(-1).click().wait(500)

    cy.dataCy('observation_comments').type(4)

    cy.get('[data-cy="done"] > .q-btn__content > .block')
      .eq(-1)
      .click()
      .wait(100)
  })

  it("Fail when try to go to next step when search hasn't ended ", () => {
    cy.testEmptyField('The search has not finished', 'workflowNextButton')
  })
  it('End search and successfully go to next step', () => {
    cy.dataCy('end_search').click()
    cy.recordLocation({
      btnEqIndex: -1,
    })
    cy.nextStep()
  })

  it('Complete nearby track plot', () => {
    cy.recordLocation()

    cy.dataCy('track_width').type(2)
    cy.selectFromDropdown('track_substrate')
    cy.get('[data-cy="done"]').eq(0).click()

    for (let i = 0; i < 5; i++) {
      cy.selectFromDropdown('attributable_fauna_species', 'foo', null, -1)
      cy.selectFromDropdown('age_class', 'Juvenile')

      cy.dataCy('count').type(4)

      cy.get('[data-cy="done"] > .q-btn__content > .block')
        .eq(-1)
        .click()
        .wait(100)
      cy.dataCy('addEntry').click()
    }
    cy.dataCy('end_search').click()
    cy.recordLocation({
      btnEqIndex: -1,
    })
  })

  it('submit the survey to core', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

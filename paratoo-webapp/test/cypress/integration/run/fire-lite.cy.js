const { pub } = require("../../support/command-utils")

const protocol = 'Cover + Fire - Standard'
const protocolCy = 'Cover\\ +\\ Fire\\ -\\ Standard'
const module = 'Fire Severity'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

global.ignorePaths = ['fire-survey.protocol_variant']

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - check if VUE_APP_MODE_OF_OPERATION env is set to 0', () => {
    cy.checkModeOfOperation(0)
  })
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocolCy)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
})

//TODO test-to-fail
//TODO editing and moving around the various transects & points
//TODO test and assert conditional fields (e.g., `dead` and `in_canopy_sky` are only valid for certain `fractional_cover` selections)
describe(protocol, () => {
  it('start survey', () => {
    cy.nextStep()
  })
  const transects = ['South 2', 'North 4', 'West 2', 'East 4']
  for (const transect of transects) {
    it(`.should() - collect transect ${transect}`, () => {
      if (transect === 'South 2') {
        cy.customSelectDate(
          {
            fieldName: 'fireIgnitionDateCalendar',
            date: {
              year: 'current',
              month: 'current',
              day: 'current',
            },
          },
          false,
        )
        cy.dataCy('doneFireIgnitionSurvey').click()
      }

      cy.get(
        '[data-cy="selectingTransect"] > .multiselect__tags > span',
      ).should('have.text', transect)

      cy.dataCy('startTransect').click()

      //5 points per transect in dev mode (normally 101)
      for (let i = 0; i < 5; i++) {
        cy.completePointInterceptPoint(transect, i, true, true)
      }

      if (transect === 'East 4') {
        //last transect go next step
        // cy.dataCy('workflowNextButton').click()
        cy.nextStep()
      }
    })
  }
  it('.should() - collect fire char observations', () => {
    cy.collectFireCharObservations()
  })
  it('.should() - check the auth context for submodule', () => {
    cy.checkFireSubmoduleAuthContext()
  })
  it('.should() - publish', () => {
    cy.nextStep()
    // cy.workflowPublish(false)
  })
  pub()
})
const { pub } = require("../../support/command-utils")

const protocol = 'Invertebrate Fauna - Active Search'
const protocolCy = 'Invertebrate\\ Fauna\\ -\\ Active\\ Search'
const module = 'invertebrate fauna'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout(true, null, {
      layout: 'QDASEQ0003',
      visit: 'QLD spring survey',
    })
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
    })
  })
})

describe('1 - survey setup', () => {
  it('1.1[validate-field] - General Information', () => {
    cy.getAndValidate('search_method', {
      isComponent: true,
      testEmptyField: ['workflowNextButton'],
    })
    cy.get('[data-cy="Diurnal active search"]').click() //search method field

    cy.getAndValidate('observers', { testEmptyField: ['workflowNextButton'] , isComponent :true})
    cy.addObservers('observerName', ['Observer A', 'Observer B'])
    cy.getAndValidate('search_time', {
      testEmptyField: ['workflowNextButton'],
    }).type(60)
  })

  it('1.2[validate-field] - Weather', () => {
    cy.getAndValidate('weather', { isComponent: true })
    cy.dataCy('temperature').type('1')
    cy.completeWeatherForm(null, ['workflowNextButton'])
  })

  it('1.3[validate-field] - Apparatus', () => {
    cy.getAndValidate('apparatus', {
      isComponent: true,
    })

    cy.getAndValidate('equipment').selectFromDropdown()

    cy.getAndValidate('description').type('apparatus')

    cy.get('[data-cy="done"] > .q-btn__content > .block').click()
  })

  it('1.3[test-to-pass] - Apparatus', () => {
    cy.get('[data-cy="addEntry"] > .q-btn__content').click()

    cy.setExpansionItemState('apparatus_2_expansion', 'open').within(() => {
      cy.dataCy('equipment').type('this is a custom lut')

      cy.dataCy('description').type('apparatus')

      cy.get('[data-cy="done"] > .q-btn__content > .block').click()
    })
  })
})

describe('2 - start search', () => {
  it('2.1[test-to-pass] - Start Search', () => {
    cy.nextStep()
    cy.dataCy('startSearchBtn').click()
    cy.dataCy('stickyTimer', { timeout: 300000 }).should('be.visible')
  })
  it('2.2[test-to-pass] - Take Search Photos', () => {
    for (let i = 0; i < 10; i++) {
      //take 10 photos
      cy.dataCy('takePhoto').click()
    }
  })

  it('2.3[test-to-pass] - Delete Search Photos', () => {
    for (let i = 0; i < 3; i++) {
      // and delete 3 photos
      cy.dataCy('deletePhotoBtn').eq(i).click()
    }
  })

  it('2.4[test-to-pass] - End Search', () => {
    cy.dataCy('endSearchBtn').click()
    cy.wait(50)

    cy.dataCy('confirmEndBtn').should('exist').click()

    cy.get('.q-circular-progress__text').contains('0:00').should('exist')
  })
})

describe('3 - Vouchering', () => {
  it('3.1[validate field] - Adding Sample 1', () => {
    // cy.get('[data-cy="workflowNextButton"]').click()
    cy.nextStep()
    cy.getAndValidate('related_photo_identifier', {isComponent: true}).selectFromDropdown()

    cy.getAndValidate('barcode', { isComponent: true })
    cy.dataCy('recordBarcode').click()

    cy.getAndValidate('preservation_type').selectFromDropdown()

    cy.getAndValidate('preservation_concentration').type(100)

    cy.getAndValidate('sample_photo', { isComponent: true })
    cy.dataCy('addImg').click().wait(50)
    cy.dataCy('takePhoto').click()

    cy.getAndValidate('photo_description').type('photo description')
    cy.getAndValidate('comment').type('comment')

    cy.dataCy('done').click()
  })

  it('3.1[test-to-pass] - Adding Sample 2', () => {
    cy.dataCy('addEntry').click()
    cy.dataCy('related_photo_identifier').selectFromDropdown()

    cy.dataCy('barcode', { isComponent: true })
    cy.dataCy('recordBarcode').click()

    cy.dataCy('preservation_type').selectFromDropdown()

    cy.dataCy('preservation_concentration').type(100)

    cy.dataCy('sample_photo', { isComponent: true })
    cy.dataCy('addImg').click().wait(50)
    cy.dataCy('takePhoto').click()

    cy.dataCy('photo_description').type('photo description')
    cy.dataCy('comment').type('comment')

    cy.dataCy('done').click()
  })
})

describe('4 - submit', () => {
  it('4.1[test-to-pass] - submit the survey', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

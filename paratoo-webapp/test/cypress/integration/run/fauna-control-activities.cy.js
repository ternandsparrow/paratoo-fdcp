const { pub } = require("../../support/command-utils")

const protocol = 'Pest Fauna - Control Activities'
const module = 'Pest\\ Fauna\\ Control\\ Activities'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

// complete intervention with control pest first as it required to do this protocol
const polygonCoords = [
  {
    lat: -23.699147554916962,
    lng: 132.79896855354312,
  },
  {
    lat: -23.69973699706658,
    lng: 132.79779911041263,
  },
  {
    lat: -23.700935521228523,
    lng: 132.7979922294617,
  },
  {
    lat: -23.701210590794854,
    lng: 132.79925823211673,
  },
]
const selectedSpecies = ['Goat', 'Pig', 'Camel', 'Deer']

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
})

describe('0 - Complete intervention with Controlling pest animals', () => {
  it('Intervention - general project info', function () {
    cy.selectProtocolIsEnabled('Interventions')
    // Intervention General Project Information
    cy.recordLocation()
    const modifier = Math.floor(Date.now())
    cy.addObservers('observerName', [`Anon ${modifier}`])
    cy.nextStep()
  })

  it('Interventions', function () {
    // Interventions
    cy.selectFromDropdown('intervention_type', 'Controlling Pest Animals')
    cy.selectFromDropdown('target_species', selectedSpecies)
    cy.dataCy('pest_control_type').selectFromDropdown(null)

    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
    })
    cy.get('[data-cy="startRecordingCorner"]').click()
    // point 1
    cy.get('[data-cy="addPoint"]').click()
    cy.get('[data-cy="addPoint"]').click()

    for (const [idx, coords] of Object.entries(polygonCoords)) {
      // change current position
      cy.window().then(($win) => {
        $win.ephemeralStore.updateUserCoords(coords, true)
      })

      // add points
      if (idx == polygonCoords.length - 1) {
        cy.get('[data-cy="finishSketch"]').click()
        continue
      }
      cy.get('[data-cy="addPoint"]').click()
    }

    cy.get('[data-cy="done"]').eq(-1).click()
  })

  it('do survey step (end)', () => {
    cy.nextStep()
  })

  pub()
  // it('publish interventions', () => {
  //   cy.workflowPublish(false)
  // })
})

describe('1 - Go to Fauna Control Activity', () => {
  it('1.0 - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })

  it('1.1[test-to-pass] - select intervention ID and proceed to next step', () => {
    cy.dataCy('interventions').selectFromDropdown()
    // TODO: test auto fill
    cy.dataCy('target_species').should('exist')
    cy.dataCy('control_activity_type').should('exist')
    cy.nextStep()
  })
})

describe('2 - Start survey and collection observation', () => {
  it('2.0[test-to-pass] - start survey', () => {
    cy.get('[data-cy="startSurvey"]').click()
  })

  it('2.1[validate-field] - add observation 1', () => {
    cy.get('[data-cy="point_unique_id"]')
      // .should('have.value') //TODO: test auto gen ID
      .clear()
      .validateField()
      .clear()
      .type('point_id')

    cy.getAndValidate('target_species', {
      isChildOf: 'observations',
    }).selectFromDropdown()

    cy.getAndValidate('count').type(10)

    cy.getAndValidate('age_class').selectFromDropdown('Adult')
    cy.getAndValidate('number_controlled').type(10)

    cy.getAndValidate('animal_fate').selectFromDropdown('Relocated')

    cy.getAndValidate('photo', { isComponent: true })
    cy.dataCy('addImg').eq(0).click()
    cy.dataCy('takePhoto').click()

    cy.getAndValidate('photo_description', 'photo description balalbal')

    cy.dataCy('comments', 'comment')
    cy.dataCy('done').eq(0).click()
    cy.dataCy('addEntry').click()
  })

  it(`2.2[test-to-pass] - add observation 2`, () => {
    cy.selectFromDropdown('target_species')

    cy.dataCy('count').type(10)

    cy.selectFromDropdown('age_class', 'Adult')
    cy.dataCy('number_controlled').type(10)

    cy.selectFromDropdown('animal_fate', 'Relocated')
    cy.dataCy('addImg').eq(0).click()
    cy.dataCy('takePhoto').click()
    cy.dataCy('photo_description', 'photo_description')

    cy.dataCy('comments', 'comment')
    cy.dataCy('done').eq(0).click()
    cy.dataCy('addEntry').click()
  })

  it('2.3[test-to-pass] - end survey', () => {
    cy.dataCy('endSurvey').click()
  })

  it('2.4[test-to-pass] - complete survey step (end)', () => {
    cy.nextStep()
  })
  pub()
  // it('2.5[test-to-pass] - submit', () => {
  //   cy.workflowPublish()
  // })
})

const { pub } = require("../../support/command-utils")

const protocol = 'Cover - Standard'
const protocolCy = 'Cover\\ -\\ Standard'
const module = 'cover'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - check if VUE_APP_MODE_OF_OPERATION env is set to 0', () => {
    cy.checkModeOfOperation(0)
  })
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocolCy)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
})

//TODO test-to-fail
//TODO editing and moving around the various transects & points
//TODO test and assert conditional fields (e.g., `dead` and `in_canopy_sky` are only valid for certain `fractional_cover` selections)
describe(protocol, () => {
  it('start survey', () => {
    cy.nextStep()
  })
  const transects = ['South 2', 'North 4', 'West 2', 'East 4']
  for (const transect of transects) {
    it(`.should() - collect transect ${transect}`, () => {
      // cy.dataCy('selectingTransect')
      cy.get('.q-banner', {timeout: 15000}).should('not.exist') // it takes some time to get the vouchers
      cy.get('[data-cy="selectingTransect"] > .multiselect__tags > span')
        .should('have.text', transect)

      cy.dataCy('startTransect').click()

      //5 points per transect in dev mode (normally 101)
      for (let i = 0; i < 5; i++) {
        cy.completePointInterceptPoint(transect, i, true)
      }
    })
  }
  it('end survey', () => {
    cy.nextStep()
  })
  pub()
  // it('.should() - publish', () => {
  //   cy.workflowPublish(false)
  // })
})
const bafd = 'Basal Area - DBH'
const module = 'basal\\ area'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const bafdCy = 'Basal\\ Area\\ –\\ DBH\\ Protocols'
import { pub } from "../../support/command-utils"

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
    // cy.getNoAssert('[aria-label="Menup"]', {log: true, timeout: 10000})
    // cy.getNoAssert('[aria-label="Menu"]', {log: true, timeout: 10000})
  })
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + bafd + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, bafd)
  })

})
context('basal 1', () => {
  describe('Basal Area – DBH Protocols (Diameter Tape Measure)', () => {
    it('doing', function () {
      cy.get('[data-cy="40 x 40 m"]').validateField().click().wait(500)

      // cy.testEmptyField(
      //   'Field: "Basal Dbh Instrument": This field is required',
      //   'workflowNextButton',
      // )
      cy.dataCy('basal_dbh_instrument')
        .validateField()
        .selectFromDropdown('Diameter Tape Measure')

      cy.nextStep()

      /* Basal Area DBH Observation 1 */
      // observation 1
      cy.recordLocation()
      cy.getAndValidate('floristics_voucher').selectFromDropdown()
      cy.get('[data-cy="dead"]')
        .validateField(['done', -1])
        .click()
      // by default multi stemmed flag is off
      cy.get('[data-cy="POM_single"]')
        .validateField(['done', -1])
        .clear()
        .type('5.66666667')
      cy.get('[data-cy="DBH"]')
        .validateField(['done', -1])
        .type('123')
      cy.get('[data-cy="done"]')
        .validateField(['done', -1])
        .click()
      // borderline_tree is a required field

      // cy.testEmptyField("POM doesn't allow more than one decimal point")

      cy.get('[data-cy="POM_single"]')
        .validateField(['done', -1])
        .clear()
        .type('1.4')
      // by default Buttresses flag is off
      cy.get('[data-cy="done"]').click().wait(2000)

      // observation 2
      cy.get('[data-cy="addEntry"]').eq(0).click().wait(500)
      cy.recordLocation()
      cy.getAndValidate('floristics_voucher').selectFromDropdown()

      // multi stemmed
      cy.get('[data-cy="multi_stemmed"]').click()
      // stem 1
      cy.get('[data-cy="DBH"]').type('1')
      cy.get('[data-cy="done"]').eq(0).click().wait(500)

      cy.get('[data-cy="addEntry"]').eq(0).click().wait(500)
      // stem 2
      cy.get('[data-cy="DBH"]').type('2')
      cy.get('[data-cy="done"]').eq(0).click().wait(500)


      // Buttressed Tree (not a repeatable component anymore)
      cy.get('[data-cy="buttresses"]').click()
      // tree 1
      cy.get('[data-cy="reach_POM"]').type('1')
      cy.get('[data-cy="reach_DBH"]').type('1')
      cy.get('[data-cy="POM_50_CM_above_buttress"]').type('1')
      cy.get('[data-cy="DBH_50_CM_above_buttress"]').type('1')
      cy.get('[data-cy="done"]').click()
      cy.nextStep()
      // cy.workflowPublish(false, 0)
    })
  })
  pub('basal 1 publish')
})
context('basal 2', () => {
  describe('Basal Area – DBH Protocols (Tape Measure)', () => {
    it('.should() - assert navigation to ' + bafd + ' is possible', () => {
      cy.selectProtocolIsEnabled(module, bafd)
    })
    it('doing', function () {
      cy.get('[data-cy="100 x 100 m"]').click().wait(500)
      // There are two options that contain Tape Measure
      // cy.get().contains() returns the first one(Diameter Tape Measure)
      //   but we need the second one. Regex can be used to find the exact match only
      //   as cy contains supports regex also.
      // source: https://stackoverflow.com/questions/56443963/click-an-exact-match-text-in-cypress
      cy.testEmptyField(
        'Field: "DBH Instrument": This field is required',
        'workflowNextButton',
      )

      cy.selectFromDropdown(
        'basal_dbh_instrument',
        '^Tape Measure$',
        null,
        null,
        false,
        true,
      )
      // cy.get('[data-cy="workflowNextButton"]').click().wait(1000)
      cy.nextStep()

      /* Basal Area DBH Observation 1 */
      // observation 1
      cy.recordLocation()
      cy.selectFromDropdown(
        'floristics_voucher',
        'Voucher Full Example 1 [dev_foo_barcode_987]',
      )
      cy.get('[data-cy="dead"]').click()
      // by default multi stemmed flag is off
      cy.get('[data-cy="POM_single"]').clear().type('5.66666667')
      cy.get('[data-cy="circumference_at_breast_height"]').type('5')
      // dbh should be changed
      cy.get('[data-cy="DBH"]').invoke('val').should('include', '2')
      cy.get('[data-cy="done"]').click()

      // borderline_tree is a required field
      cy.testEmptyField('POM does not allow more than one decimal point')

      cy.get('[data-cy="POM_single"]').clear().type('1.4')
      // by default Buttresses flag is off
      cy.get('[data-cy="done"]').click().wait(5000)

      // observation 2
      cy.get('[data-cy="addEntry"]').eq(0).click().wait(500)
      cy.recordLocation()
      cy.selectFromDropdown(
        'floristics_voucher',
        'Voucher Lite Example 1 [dev_foo_barcode_987]',
      )

      // multi stemmed
      cy.get('[data-cy="multi_stemmed"]').click()
      // stem 1
      cy.get('[data-cy="circumference_at_breast_height"]').type('5')
      // dbh should be changed
      cy.get('[data-cy="DBH"]').invoke('val').should('include', '2')
      cy.get('[data-cy="done"]').eq(0).click().wait(500)

      cy.get('[data-cy="addEntry"]').eq(0).click().wait(500)
      // stem 2
      cy.get('[data-cy="circumference_at_breast_height"]').type('5')
      // dbh should be changed
      cy.get('[data-cy="DBH"]').invoke('val').should('include', '2')
      cy.get('[data-cy="done"]').eq(0).click().wait(500)

      // Buttressed Tree
      cy.get('[data-cy="buttresses"]').click()
      // tree 1
      cy.get('[data-cy="reach_POM"]').type('1')
      cy.get('[data-cy="reach_circumference_at_breast_height"]').type('5')
      // dbh should be changed
      cy.get('[data-cy="reach_DBH"]').invoke('val').should('include', '2')
      cy.get('[data-cy="POM_50_CM_above_buttress"]').type('1')
      cy.get('[data-cy="circumference_50_cm_above_buttress"]').type('5')
      // dbh should be changed
      cy.get('[data-cy="DBH_50_CM_above_buttress"]')
        .invoke('val')
        .should('include', '2')
      cy.get('[data-cy="done"]').click().wait(500)

      // cy.workflowPublish(false, 1)
      cy.nextStep()
    })
  })
  pub('basal 2 publish')
})

context('basal 3', () => {
  describe('Basal Area – DBH Protocols (Tree Caliper)', () => {
    it('.should() - assert navigation to ' + bafd + ' is possible', () => {
      cy.selectProtocolIsEnabled(module, bafd)
    })

    it('doing', function () {
      cy.get('[data-cy="100 x 100 m"]').click().wait(500)

      // Basal Dbh Instrument is required
      cy.testEmptyField(
        'Field: "DBH Instrument": This field is required',
        'workflowNextButton',
      )
      cy.selectFromDropdown('basal_dbh_instrument', 'Tree Caliper')
      // cy.get('[data-cy="workflowNextButton"]').click().wait(1000)
      cy.nextStep()

      /* Basal Area DBH Observation 1 */
      // observation 1
      cy.recordLocation()
      cy.selectFromDropdown(
        'floristics_voucher',
        'Voucher Full Example 1 [dev_foo_barcode_987]',
      )
      cy.get('[data-cy="dead"]').click()
      // enable ellipse
      cy.get('[data-cy="ellipse"]').click()

      // by default multi stemmed flag is off
      cy.get('[data-cy="POM_single"]').clear().type('5.66666667')
      cy.get('[data-cy="DBH"]').clear().type('2')
      cy.get('[data-cy="ellipse_DBH"]').eq(0).type('5')
      // calculated DBH should be changed
      cy.get('[data-cy="calculated_DBH"]')
        .eq(0)
        .invoke('val')
        .should('include', '3')
      cy.get('[data-cy="done"]').eq(0).click()

      // borderline_tree is a required field

      cy.testEmptyField('POM does not allow more than one decimal point')

      cy.get('[data-cy="POM_single"]').clear().type('1.4')
      // by default Buttresses flag is off
      cy.get('[data-cy="done"]').click().wait(2000)

      // observation 2
      cy.get('[data-cy="addEntry"]').eq(0).click().wait(500)
      cy.recordLocation()
      cy.selectFromDropdown(
        'floristics_voucher',
        'Voucher Lite Example 1 [dev_foo_barcode_987]',
      )
      // enable ellipse
      cy.get('[data-cy="ellipse"]').click()

      // multi stemmed
      cy.get('[data-cy="multi_stemmed"]').click()
      // stem 1
      cy.get('[data-cy="DBH"]').clear().type('2')
      cy.get('[data-cy="ellipse_DBH"]').eq(0).type('5')
      // calculated DBH should be changed
      cy.get('[data-cy="calculated_DBH"]')
        .eq(0)
        .invoke('val')
        .should('include', '3')
      cy.get('[data-cy="done"]').eq(0).click().wait(500)

      cy.get('[data-cy="addEntry"]').eq(0).click().wait(500)
      // stem 2
      cy.get('[data-cy="DBH"]').clear().type('2')
      cy.get('[data-cy="ellipse_DBH"]').eq(0).type('5')
      // calculated DBH should be changed
      cy.get('[data-cy="calculated_DBH"]')
        .eq(0)
        .invoke('val')
        .should('include', '3')
      cy.get('[data-cy="done"]').eq(0).click().wait(500)

      // Buttressed Tree
      cy.get('[data-cy="buttresses"]').click()
      // tree 1
      cy.get('[data-cy="reach_POM"]').type('1')
      cy.get('[data-cy="reach_DBH"]').clear().type('2')
      cy.get('[data-cy="ellipse_DBH"]').eq(0).type('5')
      // calculated DBH should be changed
      cy.get('[data-cy="calculated_DBH"]')
        .eq(0)
        .invoke('val')
        .should('include', '3')
      cy.get('[data-cy="POM_50_CM_above_buttress"]').type('1')
      cy.get('[data-cy="DBH_50_CM_above_buttress"]').clear().type('2')
      cy.get('[data-cy="ellipse_DBH"]').eq(1).type('5')
      // calculated DBH should be changed
      cy.get('[data-cy="calculated_DBH"]')
        .eq(1)
        .invoke('val')
        .should('include', '3')
      cy.get('[data-cy="done"]').click().wait(500)

      // cy.workflowPublish(false, 2)
      cy.nextStep()
    })
  })
  pub('basal 3 publish')
})

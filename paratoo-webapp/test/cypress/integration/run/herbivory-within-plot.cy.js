const { pub } = require("../../support/command-utils")

const protocol = 'Herbivory and Physical Damage - Within-plot Belt Transect'
const module = 'Herbivory and Physical Damage'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

global.ignorePaths = ['herbivory-and-physical-damage-transect.transect_line']

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})

const selectedSpecies = ['Deer', 'Rabbit', 'Wild Dog', 'Fox', 'Other']
describe('1 - transect setup', () => {
  it('1.1[validate-field] - validate first three fields', () => {
    cy.getAndValidate('number_of_transects', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('1')
    cy.getAndValidate('survey_intent', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('Once-off measure')
    cy.getAndValidate('target_species', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown(selectedSpecies)

    cy.getAndValidate('target_deer').selectFromDropdown(
      'Fallow deer (Dama dama)',
    )
    cy.getAndValidate('target_wild_dog').selectFromDropdown(
      'Dog (Canis lupus familiaris)',
    )

    cy.getAndValidate('other_species').type('{esc}perry the platypus')
    cy.wait(500)
    cy.dataCy('done').first().click()
  })

  it('1.2[validate-field] - add observers details', () => {
    cy.getAndValidate('observer_details', {
      isComponent: true,
      testEmptyField: ['workflowNextButton'],
    })
    const observers = ['Spotter', 'Data entry']
    observers.forEach((role, index) => {
      const modifier = Math.floor(new Date() / 1000)
      if (index === 0) {
        cy.addObservers('observerName', [`Anon ${modifier + index}`])
        cy.getAndValidate('observer_role').selectFromDropdown(role)
      } else {
        cy.addObservers('observerName', [`Anon ${modifier + index}`])
        cy.dataCy('observer_role').selectFromDropdown(role)
      }
      cy.get('[data-cy="done"]').last().click()
      if (observers !== 'Data entry') {
        cy.get('[data-cy="addEntry"]').last().click()
      }
    })
  })

  it.skip('1.3[validate-field] - complete weather form and go to next step', () => {
    cy.getAndValidate('weather', {
      isComponent: true,
      testEmptyField: ['workflowNextButton'],
    })
    // FIXME: need to init the non required single child obs first before validate its fields
    cy.dataCy('temperature').type('1')
    cy.completeWeatherForm(null, ['workflowNextButton'])
  })
})
// TODO - test conditional fields in quadrat observation
describe('2 - conduct survey', () => {
  it('2.1[validate-field] - start transect 1', () => {
    cy.nextStep()
    cy.selectFromDropdown('transect_number')
    cy.getAndValidate('transect_photo', { isComponent: true })
    cy.dataCy('addImg').eq(0).click().wait(50)
    cy.dataCy('takePhoto').eq(0).click().wait(50)
    cy.get('[data-cy="done"]').eq(0).click()
  })

  it('2.2[validate-field] - collect quadrat 1, no herbivory', () => {
    cy.dataCy('quadrat_number').selectFromDropdown('Quadrat 1')
    cy.setExpansionItemState(`quadrat_observation_1_`, 'open')

    cy.getAndValidate('herbivory_or_damage_encountered').selectFromDropdown(
      'No herbivory or damage encountered',
    )

    cy.getAndValidate('photo', { isComponent: true })
    cy.dataCy('addImg').eq(-1).click()
    cy.dataCy('takePhoto').eq(-1).click()
    cy.getAndValidate('observation_comments').type('comments')

    cy.dataCy('done').eq(-1).click()

    cy.dataCy('nextPointBtnLabel').click()
  })

  it('2.3[validate-field] - collect quadrat 2, Damage encountered', () => {
    cy.setExpansionItemState(`quadrat_observation_1_`, 'open')

    cy.dataCy('herbivory_or_damage_encountered').selectFromDropdown(
      'Physical damage encountered',
    )

    cy.getAndValidate('type_of_physical_damage').selectFromDropdown('Burrow')
    cy.getAndValidate('attributable_fauna_species').selectFromDropdown(
      'perry the platypus',
    )
    cy.getAndValidate('percent_of_quadrat_affected').type(20)

    cy.dataCy('addImg').eq(-1).click()
    cy.dataCy('takePhoto').eq(-1).click()
    cy.getAndValidate('observation_comments').type('comments')

    cy.dataCy('done').eq(-1).click()

    cy.dataCy('nextPointBtnLabel').click()
  })

  it('2.4[validate-field] - collect quadrat 3, Herbivory encountered', () => {
    cy.setExpansionItemState(`quadrat_observation_1_`, 'open')
    cy.dataCy('herbivory_or_damage_encountered').selectFromDropdown(
      'Herbivory encountered',
    )

    cy.getAndValidate('type_of_damage').selectFromDropdown()
    cy.getAndValidate('affected_plant_species').selectFromSpeciesList('Stipa dichelachne')
    cy.getAndValidate('growth_form').selectFromDropdown(
      'Grass-tree',
      'grass-tree',
    )

    cy.getAndValidate('damage_lowest_point').type(22)
    cy.getAndValidate('damage_highest_point').type(22)
    cy.getAndValidate('extent_of_damage').type(22)
    cy.getAndValidate('additional_physical_damage').type('asdasdsadas')
    cy.getAndValidate('grazing_severity').selectFromDropdown()

    cy.getAndValidate('attributable_fauna_species').selectFromDropdown(
      'Fallow deer (Dama dama)',
    )

    cy.dataCy('addImg').eq(-1).click()
    cy.dataCy('takePhoto').eq(-1).click()
    cy.getAndValidate('observation_comments').type('comments')

    cy.dataCy('done').eq(-1).click()
    cy.dataCy('nextPointBtnLabel').click()
  })

  for (let i = 0; i < 3; i++) {
    it(`2.${i + 5}[test-to-pass] - collect quadrat ${i + 3}`, () => {
      cy.setExpansionItemState(`quadrat_observation_1_`, 'open')

      cy.dataCy('herbivory_or_damage_encountered').selectFromDropdown(
        'Herbivory encountered',
      )

      cy.dataCy('attributable_fauna_species').selectFromDropdown('Other')

      cy.dataCy('other_attributable_species').type("it's me DIO!!!!{enter}")

      cy.dataCy('done').eq(-1).click()

      cy.dataCy('nextPointBtnLabel').click()
    })
  }
  it(`2.8[test-to-pass] - end transect`, () => {
    cy.dataCy('done').last().click()
  })
})

describe('3 - submission', () => {
  it('[test-to-pass] - submit the survey to core', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

const { pub } = require("../../support/command-utils")

const protocol = 'Sign-based Fauna - Track Station'
const module = 'Sign-based Fauna Surveys'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
    cy.mockLocation({ lat: -23.7, lng: 132.8 })
  })
})

const stationIDs = []
const setupIds = []
describe('create new', () => {
  describe('1. station setup', () => {
    it('1.1[validate-field] - station 1', () => {
      cy.dataCy('site_id')
        .invoke('val')
        .then((id) => {
          stationIDs.push(id)
        })
      cy.recordLocation()

      cy.getAndValidate('track_station_width').type(33)
      cy.getAndValidate('track_station_length').type(33)
      cy.getAndValidate('track_station_substrate').selectFromDropdown('Sand')

      cy.getAndValidate('track_station_downward', { isComponent: true })

      cy.getAndValidate('track_station_spacing').type(1)

      cy.getAndValidate('micro_habitat').selectFromDropdown()

      cy.getAndValidate('track_station_micro_habitat_photo', {
        isComponent: true,
      })

      cy.getAndValidate('habitat').selectFromDropdown('Forest')

      cy.dataCy('done').eq(0).click()
    })
    it('1.2[test-to-pass] - station 2', () => {
      cy.dataCy('addEntry').click()
      cy.dataCy('site_id')
        .invoke('val')
        .then((id) => {
          stationIDs.push(id)
        })
      cy.recordLocation()

      cy.dataCy('track_station_width').type(33)
      cy.dataCy('track_station_length').type(33)
      cy.dataCy('track_station_substrate').selectFromDropdown('Sand')

      cy.dataCy('habitat').selectFromDropdown('Forest')

      cy.dataCy('done').eq(0).click()
    })
  })
  describe('2. survey setup', () => {
    it(`2.1[validate-field] - survey setup station 1`, () => {
      cy.nextStep()
      cy.getAndValidate('station_setup_ID').selectFromDropdown(stationIDs[0])

      cy.getAndValidate('survey_intent').selectFromDropdown('Once-off measure')
      cy.getAndValidate('target_species').selectFromDropdown(['Deer', 'Other'])
      cy.getAndValidate('target_deer').selectFromDropdown(
        'Fallow deer (Dama dama)',
      )
      cy.dataCy('other_species').type('foo{enter}bar{enter}')
      cy.wait(500)
      cy.dataCy('done').first().click()

      cy.getAndValidate('observer_details', {
        isComponent: true,
        testEmptyField: ['done', -1],
      })
      const modifier = Math.floor(Date.now())
      //TODO test-to-fail observer name (need to get a handle on 'done' button inside observer details component)
      cy.addObservers('observerName', [`Anon ${modifier}`])
      cy.getAndValidate('role').selectFromDropdown()
      cy.get('[data-cy="done"]').eq(0).click()

      cy.getAndValidate('weather', { isComponent: true })
      cy.completeWeatherForm()

      cy.dataCy('survey_setup_ID')
        .invoke('val')
        .then((id) => {
          setupIds.push(id)
        })

      cy.dataCy('done').eq(-1).click()
    })
    it(`2.2[test-to-pass] - survey setup station 2`, () => {
      cy.dataCy('addEntry').eq(-1).click()
      cy.dataCy('station_setup_ID').selectFromDropdown(stationIDs[1])

      cy.dataCy('survey_intent').selectFromDropdown('Once-off measure')
      cy.dataCy('target_species').selectFromDropdown(['Goat', 'Unknown', 'N/A'])

      const modifier = Math.floor(Date.now())
      cy.addObservers('observerName', [`Anon ${modifier}`])
      cy.dataCy('role').selectFromDropdown()
      cy.get('[data-cy="done"]').eq(0).click()

      cy.dataCy('survey_setup_ID')
        .invoke('val')
        .then((id) => {
          setupIds.push(id)
        })

      cy.dataCy('done').eq(-1).click()
      cy.nextStep()
    })
  })
  describe('3. conduct survey', () => {
    it('3.1 - start station 1 ', () => {
      cy.dataCy('station_ID').selectFromDropdown(stationIDs[0])
      cy.dataCy('setup_ID').selectFromDropdown(setupIds[0])
    })
    it('3.2 -  add observation 1', () => {
      cy.dataCy('tracks_observed').click()

      cy.getAndValidate('attributable_fauna_species').selectFromDropdown('foo')
      cy.getAndValidate('age_class').selectFromDropdown()
      cy.getAndValidate('number_of_individuals').type(2)

      cy.getAndValidate('photo', { isComponent: true })
      cy.dataCy('addImg').click().wait(50)
      cy.dataCy('takePhoto').click().wait(50)
      cy.dataCy('done').eq(0).click()
    })
    it('3.3 -  add observation 2', () => {
      cy.dataCy('addEntry').first().click()
      cy.dataCy('attributable_fauna_species').last().selectFromDropdown(
        'Fallow deer (Dama dama)',
      )
      cy.dataCy('age_class').selectFromDropdown()
      cy.dataCy('number_of_individuals').type(2)

      cy.dataCy('done').eq(0).click()

      cy.dataCy('done').click()
      cy.nextStep()
    })
  })
  describe('submit survey', () => {
    // it('submit the survey to core', () => {
    //   cy.workflowPublish()
    // })
    pub()
  })
})
describe('selecting station', () => {
  describe('select protocol', () => {
    it('.should() - assert navigation to ' + protocol + ' is possible', () => {
      cy.selectProtocolIsEnabled(module, protocol)
    })
  })
  describe('4. station setup', () => {
    it('4.1[test-to-pass] - station 1', () => {
      cy.dataCy('selectExisting').click()
      cy.selectFromDropdown('site_id', stationIDs[0])
      cy.selectFromDropdown('site_id', stationIDs[1])

      cy.dataCy('track_station_width')
        .should('be.disabled')
        .should('have.value', '33')

      cy.dataCy('track_station_length')
        .should('be.disabled')
        .should('have.value', '33')

      cy.dataCy('track_station_substrate')
        .should('be.disabled')
        .should('have.value', 'Sand')

      cy.dataCy('habitat')
        .should('be.disabled')
        .prev()
        .should('contain.text', 'Forest')

      cy.dataCy('done').eq(0).click()
    })
  })
  describe('5. survey setup', () => {
    it(`5.1[test-to-pass] - survey setup`, () => {
      cy.nextStep()
      cy.dataCy('station_setup_ID').selectFromDropdown(stationIDs[1])

      cy.dataCy('survey_intent').selectFromDropdown('Once-off measure')
      cy.dataCy('target_species').selectFromDropdown(['Goat', 'Unknown', 'N/A'])

      const modifier = Math.floor(Date.now())
      cy.addObservers('observerName', [`Anon ${modifier}`])
      cy.dataCy('role').selectFromDropdown()
      cy.get('[data-cy="done"]').eq(0).click()

      cy.dataCy('survey_setup_ID')
        .invoke('val')
        .then((id) => {
          setupIds.push(id)
        })

      cy.dataCy('done').eq(-1).click()
      cy.nextStep()
    })
  })
  describe('6. conduct survey', () => {
    it('6.1 - start station 1 ', () => {
      cy.dataCy('station_ID').selectFromDropdown(stationIDs[1])
      cy.dataCy('setup_ID').selectFromDropdown(setupIds[3])
    })
    it('6.2 -  add observation 1', () => {
      cy.dataCy('tracks_observed').click()

      cy.dataCy('attributable_fauna_species').last().selectFromDropdown()
      cy.dataCy('age_class').selectFromDropdown()
      cy.dataCy('number_of_individuals').type(2)

      cy.dataCy('addImg').click().wait(50)
      cy.dataCy('takePhoto').click().wait(50)
      cy.dataCy('done').eq(0).click()
      cy.dataCy('done').eq(0).click()
      cy.nextStep()
    })
  })
  describe('submit survey', () => {
    // it('submit the survey to core', () => {
    //   cy.workflowPublish()
    // })
    pub()
  })
})

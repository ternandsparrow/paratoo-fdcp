const { pub } = require("../../support/command-utils")

const protocol = 'Herbivory and Physical Damage - Active Plot Search'
const module = 'Herbivory and Physical Damage'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

describe('0-Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})

describe('1 - 40x40 subplot - survey setup', () => {
  it('1.1[validate-field] - validate field project code', () => {
    cy.getAndValidate('project_code').type('project code 1')
  })

  it('1.2[validate-field] - observer details', () => {
    const modifier = Math.floor(new Date() / 1000)
    cy.getAndValidate('observer_name', { isComponent: true })
    cy.addObservers('observerName', [`Anon ${modifier}`])
    cy.getAndValidate('observer_role').selectFromDropdown('Spotter')
    cy.get('[data-cy="done"]').eq(0).click()
  })

  it('1.3[validate-field] - other fields in survey setup', () => {
    cy.getAndValidate('survey_intent').selectFromDropdown('Repeated measure')
    cy.getAndValidate('target_species').selectFromDropdown([
      'Wild Dog',
      'Deer',
      'Other',
    ])

    cy.getAndValidate('target_deer').selectFromDropdown(
      'Fallow deer (Dama dama)',
    )
    cy.getAndValidate('target_wild_dog').selectFromDropdown(
      'Dog (Canis lupus familiaris)',
    )

    cy.getAndValidate('other_species').type('{esc}drop bear{esc}')
    cy.wait(500)
    cy.dataCy('done').first().click()
    cy.getAndValidate('plot_size', { isComponent: true })
    cy.get(`[data-cy="40x40m subplot"]`).click()

    cy.recordLocation()

    cy.getAndValidate('start_location_photo', {
      isComponent: true,
      isChildOf: 'herbivory-and-physical-damage-active-search-setup',
    })
    cy.dataCy('addImg').click()
    cy.dataCy('takePhoto').click()

    cy.nextStep()
  })
})

describe('2 - 40x40 subplot - conduct survey', () => {
  it('2.1[validate-field] - start search', () => {
    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
    })
    cy.dataCy('startTransect').click()

    cy.getAndValidate('start_location_photo', {
      isComponent: true,
      testEmptyField: ['workflowNextButton', -1],
    })
    cy.dataCy('addImg').eq(0).click().wait(50)
    cy.dataCy('takePhoto').eq(0).click().wait(50)
  })

  it('2.2[validate-field] - add transect 1 observation 1', () => {
    cy.getAndValidate('herbivory_or_damage_encountered').selectFromDropdown(
      'Herbivory encountered',
    )

    cy.getAndValidate('type_of_damage').selectFromDropdown('Leaf chewing')
    cy.getAndValidate('affected_plant_species').selectFromDropdown()
    cy.getAndValidate('growth_form').selectFromDropdown('Forb', 'forb')

    cy.getAndValidate('attributable_fauna_species').selectFromDropdown(
      'Fallow deer (Dama dama)',
    )

    cy.getAndValidate('photo', { isComponent: true })
    cy.dataCy('addImg').eq(-1).click()
    cy.dataCy('takePhoto').eq(-1).click()

    cy.getAndValidate('observation_comments').type('comments')

    cy.dataCy('done').eq(-1).click()
  })

  it('2.3[test-to-pass] - add transect 1 observation 2', () => {
    cy.get('[data-cy="addEntry"]').click()

    cy.selectFromDropdown(
      'herbivory_or_damage_encountered',
      'Physical damage encountered',
    )

    cy.selectFromDropdown('type_of_physical_damage', 'Burrow')
    cy.selectFromDropdown('attributable_fauna_species', 'drop bear')
    cy.dataCy('addImg').eq(-1).click()
    cy.dataCy('takePhoto').eq(-1).click()
    cy.dataCy('done').eq(-1).click()
  })

  it('2.4-[test-to-pass] - end search', () => {
    //subplot doesn't require full time effort
    cy.get("[data-cy='00:00:20']",{ timeout: 30000 }).should('exist')
    cy.dataCy('endTransect').click()

    cy.get('.q-dialog:contains(Are you sure you want to end the search)')
      .within(() => {
        cy.get('.q-btn:contains(Yes)').click()
      })
  })

  it('2.5[test-to-pass] - submit the survey', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

describe('3 - 100x100 plot - survey setup', () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
  it('3.1 - target species', () => {
    cy.selectFromDropdown('target_species', ['Goat', 'Pig'])
  })
  it('3.2 - observer details', () => {
    const modifier = Math.floor(new Date() / 1000)
    cy.addObservers('observerName', [`Anon ${modifier}`])
    cy.selectFromDropdown('observer_role', 'Spotter')
    cy.dataCy('done').click().wait(200)
  })
  it('3.3 - plot size', () => {
    cy.get(`[data-cy="100x100m plot"]`).click()
  })
  it('3.4 - location', () => {
    cy.recordLocation()
  })
  it('3.5 - finish survey setup step', () => {
    cy.nextStep()
  })
})

describe('4 - 100x100 plot - conduct survey', () => {
  it('4.1 - start search', () => {
    cy.dataCy('startTransect').click()

    //start location photo
    cy.dataCy('addImg').eq(0).click().wait(50)
    cy.dataCy('takePhoto').eq(0).click().wait(50)
  })
  it('4.2[test-to-fail] - reject ending search early', () => {
    cy.dataCy('endTransect').click()
    cy.get('.q-notification')
      .should('exist')
      .invoke('text')
      .then((text) => {
        expect(
          text.match(
            new RegExp(
              `Transect time is (\(.*\)), minimum of (\(.*\)) minutes survey effort is required`,
              'g',
            )
          )
        ).to.have.lengthOf(1)
      })
  })
  it('4.3 - end search', () => {
    cy.get("[data-cy='00:00:30']",{ timeout: 40000 }).should('exist')
    cy.dataCy('endTransect').click()
    cy.get('.q-dialog:contains(Are you sure you want to end the search)')
      .within(() => {
        cy.get('.q-btn:contains(Yes)').click()
      })
  })
  it('4.4 - submit the survey', () => {
    cy.nextStep()
  })
  pub()
})
const protocol = 'Opportunistic Observations'
const module = 'Opportune'

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })
})

describe(`Collect ${protocol}`, () => {
  it('.should() - complete survey step (start)', () => {
    cy.nextStep()
  })
  it('.should() - should warn when user manually enter specific location', () => {
    cy.dataCy('choosePosition', { timeout: 5000 }).click().then(() => {
      cy.dataCy('manualCoords', { timeout: 5000 }).should('be.visible').click()
      cy.dataCy('manualCoordsWarning', { timeout: 5000 }).should('be.visible')
      cy.dataCy('Latitude', { timeout: 5000 })
        .should('be.visible')
        .type('{backspace}10.898042')
      cy.dataCy('Longitude', { timeout: 5000 }).type('{backspace}105.221983')
    })
    cy.dataCy('recordLocation').click()
    cy.get('.q-dialog__message')
      .should('be.visible')
      .and(
        'contain.text',
        'Your selected location is outside of your project area. Are you sure you want to record it?',
      )
      .then(() => {
        cy.get('.q-btn__content:contains(OK)').click()
      })
    cy.get('.q-dialog__message')
      .should('be.visible')
      .and(
        'contain.text',
        'Your selected location is outside of Australia. Are you sure you want to record it?',
      )
      .then(() => {
        cy.get('.q-btn__content:contains(OK)').should('be.visible').eq(-1).click()
      })
    cy.dataCy('recordedLocationOutOfBoundsWarning').should('exist')
  })
})

const { pub } = require("../../support/command-utils")

const protocol = 'Invertebrate Fauna - Wet Pitfall Trapping'
const protocolCy = 'Invertebrate\\ Fauna\\ -\\ Wet\\ Pitfall\\ Trapping'
const module = 'invertebrate fauna'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

global.ignorePaths = []
for (let i = 0; i <= 23; i++) {
  global.ignorePaths.push(`remove-wet-pitfall-trap.${i}.start_date`)
  global.ignorePaths.push(`remove-wet-pitfall-trap.${i}.duration`)
}

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  // only being skipped temporarily as there is an unfinished protocol inside the module
  it.skip('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocolCy)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout(true, null, {
      layout: 'QDASEQ0003',
      visit: 'QLD spring survey',
    })
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
})



describe('1 - trapping information', () => {
  it('1.1[validate-field]', () => {
    const modifier = Math.floor(Date.now())
    cy.get('[data-cy="observers"]').eq(0).click().type(`observer-${modifier}`)
    cy.dataCy('addObserver').click()

    cy.getAndValidate('proposed_duration').type(7)
    cy.getAndValidate('trap_diameter').type(5)
    cy.getAndValidate('trap_depth').type(5)
    cy.getAndValidate('trap_preservative').selectFromDropdown()
    cy.getAndValidate('preservative_concentration').type(5)
    cy.getAndValidate('preservative_volume').type(5)
  })
})

describe('2 - Complete weather', () => {
  it('.should() - Complete weather observation form', () => {
    cy.nextStep()
    cy.completeWeatherForm()
  })
})

describe('3 -add traps', () => {
  it('3.1[test-to-pass] - Place and save grid', () => {
    cy.nextStep()
    cy.dataCy('map').click()
    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords(
        { lat: -27.390413736769265, lng: 152.87625521421432 },
        true,
      )
    })
    cy.dataCy('backToLocation').click()
    cy.dataCy('gridBtn').click()
    cy.dataCy('close').click()
  })

  it('3.2[test-to-pass] - Complete adding traps', () => {
    for (let i = 0; i < 20; i++) {
      cy.dataCy('recordTrapLocationBtn').click()

      cy.dataCy('done').click()
    }
  })
})

describe('4 - trap grid photo', () => {
  it('4.1[validate-tield] - Complete taking Grid Photo', () => {
    cy.nextStep()
    cy.getAndValidate('photo', { isComponent: true })
    cy.dataCy('addImg').click()
    cy.wait(50)
    cy.dataCy('takePhoto').click()
    cy.wait(50)

    cy.getAndValidate('description').type('sdadassad')
  })
})

describe('5 - remove traps', () => {
  it('5.1[test-to-pass] - Complete removing all traps', () => {
    cy.nextStep()
    cy.dataCy('removeAllTrapsBtn').click()
    cy.dataCy('confirmRemoveTrapBtn')
      .should('exist')
      .then(() => {
        cy.dataCy('confirmRemoveTrapBtn').click()
      })
  })
})

describe('6 - vouchering', () => {
  it('6.1[validate-field] - Complete vouchering 1', () => {
    cy.nextStep()

    cy.getAndValidate('barcode', {
      isComponent: true,
      testEmptyField: ['nextTrap'],
    })
    cy.dataCy('recordBarcode').click({ force: true })

    cy.getAndValidate('sample_photo', {
      isComponent: true,
      testEmptyField: ['nextTrap'],
    })

    cy.getAndValidate('single_photo', {
      isComponent: true,
    })
    cy.dataCy('addImg').click({ force: true })
    cy.dataCy('takePhoto').click({ force: true })
    cy.getAndValidate('comment').type('asdadsadsa')
    cy.dataCy('done').click()

    cy.dataCy('nextTrap').click({ force: true })
  })

  it('6.2[test-to-pass] - Complete vouchering 2', () => {
    for (let i = 0; i < 19; i++) {
      cy.setExpansionItemState('voucher_photo_1_expansion', 'open')
      cy.dataCy('recordBarcode').click({ force: true })

      cy.dataCy('addImg').click({ force: true })
      cy.dataCy('takePhoto').click({ force: true })
      cy.dataCy('comment').type('asdadsadsa')
      cy.dataCy('done').click()

      cy.dataCy('nextTrap').click({ force: true })
    }
  })
})

describe('7 - submit', () => {
  it('7.1[test-to-pass] - Submit', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

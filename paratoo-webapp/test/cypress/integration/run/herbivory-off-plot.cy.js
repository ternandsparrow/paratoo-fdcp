const { pub } = require("../../support/command-utils")

const protocol = 'Herbivory and Physical Damage - Off-plot Transect'
const module = 'Herbivory and Physical Damage'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })

  it('mock location', () => {
    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
    })
  })
})

const transectId = []
const setupIds = []
describe(`${protocol} - create new`, () => {
  context('1. transect setup', () => {
    it('1.1[validate-field] - transect 1', () => {
      cy.offPlotTransectSetup1New().then((id) => {
        transectId.push(id)
      })
    })

    it('1.2[test-to-pass] - transect 2', () => {
      cy.offPlotTransectSetup2New().then((id) => {
        transectId.push(id)
      })
    })

    it('1.3[test-to-pass] - transect 3', () => {
      cy.offPlotTransectSetup3New().then((id) => {
        transectId.push(id)
      })
    })

    it('finish transect setup', () => {
      cy.log(`transectId: ${JSON.stringify(transectId)}`)
      cy.nextStep()
    })
  })

  context('2. survey setup', () => {
    it(`2.1[validate-field] - survey setup transect 1`, () => {
      cy.offPlotSurveySetup1(transectId[0], 'herb_off').then((id) => {
        setupIds.push(id)
      })
    })
    it(`2.2[test-to-pass] - survey setup transect 2`, () => {
      cy.offPlotSurveySetup2(transectId[1], 'herb_off').then((id) => {
        setupIds.push(id)
      })
    })
    it('finish survey setup', () => {
      cy.log(`setupIds: ${JSON.stringify(setupIds)}`)
      cy.nextStep()
    })
  })

  //TODO check the notification shows (and has correct message) when we click `nextPointBtnLabel`
  context('3. conduct the survey', () => {
    it('3.1[validate-field] - start transect 1', () => {
      cy.dataCy('transect_number').selectFromDropdown(transectId[0])

      //this gets autofilled
      cy.dataCy('setup_ID').should('contain.text', setupIds[0])

      cy.getAndValidate('transect_photo', { isComponent: true })

      cy.dataCy('addImg').eq(0).click().wait(50)
      cy.dataCy('takePhoto').eq(0).click().wait(50)
      cy.get('[data-cy="done"]').eq(0).click()

      cy.selectFromDropdown('quadrat_number', 'Quadrat 1')
    })

    it('3.2[validate-field] - add quadrat 1 - select attributable deer', () => {
      cy.setExpansionItemState(`quadrat_observation_1_`, 'open')
      cy.getAndValidate('herbivory_or_damage_encountered').selectFromDropdown(
        'Herbivory encountered',
      )

      cy.getAndValidate('type_of_damage').selectFromDropdown('Leaf chewing')
      cy.getAndValidate('affected_plant_species').selectFromSpeciesList('Stipa dichelachne')
      cy.getAndValidate('growth_form').selectFromDropdown('Forb', 'forb')

      cy.getAndValidate('attributable_fauna_species').selectFromDropdown(
        'Fallow deer (Dama dama)',
      )

      cy.getAndValidate('photo', { isComponent: true })
      cy.dataCy('addImg').eq(-1).click()
      cy.dataCy('takePhoto').eq(-1).click()

      cy.getAndValidate('observation_comments').type('comments')
      cy.dataCy('done').eq(-1).click()
      cy.dataCy('nextPointBtnLabel').click()
    })

    it('3.3[test-to-pass] - add quadrat 2 - dog', () => {
      cy.setExpansionItemState(`quadrat_observation_1_`, 'open')
      cy.dataCy('herbivory_or_damage_encountered').selectFromDropdown(
        'Herbivory encountered',
      )

      cy.dataCy('attributable_fauna_species').selectFromDropdown(
        'Dog (Canis lupus familiaris)',
      )

      cy.dataCy('done').eq(-1).click()
      cy.dataCy('nextPointBtnLabel').click()
    })

    it('3.4[test-to-pass] - add quadrat 3', () => {
      cy.setExpansionItemState(`quadrat_observation_1_`, 'open')
      cy.selectFromDropdown(
        'herbivory_or_damage_encountered',
        'Herbivory encountered',
      )

      cy.selectFromDropdown('attributable_fauna_species', 'drop bear')
      cy.dataCy('done').eq(-1).click()
      cy.dataCy('nextPointBtnLabel').click()
    })

    it('3.5[test-to-pass] - add quadrat 4', () => {
      cy.setExpansionItemState(`quadrat_observation_1_`, 'open')
      cy.selectFromDropdown(
        'herbivory_or_damage_encountered',
        'Herbivory encountered',
      )

      cy.dataCy('attributable_fauna_species').selectFromDropdown('Other')

      cy.dataCy('other_attributable_species')
        .selectFromSpeciesList('emu')
        .type('{esc}')

      cy.dataCy('done').eq(-1).click()
      cy.dataCy('nextPointBtnLabel').click()
    })

    it('3.6[test-to-pass] - add rest of quadrats for transect 1', () => {
      for (let i = 0; i < 6; i++) {
        cy.dataCy('nextPointBtnLabel').click()
      }
    })
    it('3.7[test-to-pass] - end transect', () => {
      cy.dataCy('done').eq(-1).click()
    })

    it('3.8[test-to-pass] - start transect 2', () => {
      cy.dataCy('transect_number').selectFromDropdown(transectId[1])
      cy.dataCy('setup_ID').selectFromDropdown(setupIds[1])

      cy.dataCy('addImg').eq(0).click().wait(50)
      cy.dataCy('takePhoto').eq(0).click().wait(50)
      cy.get('[data-cy="done"]').eq(0).click()

      cy.selectFromDropdown('quadrat_number', 'Quadrat 1')
    })

    it('3.8[test-to-pass] - add quadrat 1', () => {
      cy.setExpansionItemState(`quadrat_observation_1_`, 'open')
      cy.dataCy('herbivory_or_damage_encountered').selectFromDropdown(
        'Herbivory encountered',
      )

      cy.dataCy('type_of_damage').selectFromDropdown('Leaf chewing')
      cy.dataCy('affected_plant_species').selectFromSpeciesList('Stipa dichelachne')
      cy.dataCy('growth_form').selectFromDropdown('Forb', 'forb')

      cy.dataCy('attributable_fauna_species').selectFromDropdown()

      cy.dataCy('addImg').eq(-1).click()
      cy.dataCy('takePhoto').eq(-1).click()

      cy.dataCy('observation_comments').type('comments')
      cy.dataCy('done').eq(-1).click()
      cy.dataCy('nextPointBtnLabel').click()
    })
    it('3.9[test-to-fail] - submit when all quadrats are not done', () => {
      cy.testEmptyField(
        `Transect ${transectId[1]} (${setupIds[1]}) is missing 9 quadrat`,
        'workflowNextButton',
      )
    })

    it('3.10[test-to-pass] - add the rest of quadrats', () => {
      for (let i = 0; i < 9; i++) {
        cy.dataCy('nextPointBtnLabel').click()
      }
      cy.dataCy('done').eq(-1).click()
    })

    it('3.11[test-to-pass] - finish conducting the survey', () => {
      cy.nextStep()
    })
  })

  context('publish', () => {
    // it('submit the survey to core', () => {
    //   cy.workflowPublish()
    // })
    pub()
  })
})

const repeatVisitSurveySetupIds = []
describe(`${protocol} - select existing`, () => {
  context('navigate to protocol', () => {
    it('.should() - assert navigation to ' + protocol + ' is possible', () => {
      cy.selectProtocolIsEnabled(module, protocol)
    })
    it('log transect IDs', () => {
      cy.log(`transectId: ${JSON.stringify(transectId)}`)
    })
  })
  //TODO do another transect setup (like sign-based off-plot), and thus another survey setup and conduct the survey
  context('4. transect setup', () => {
    it('4.1[validate-field] - transect 1 - check auto-fill', () => {
      cy.dataCy('selectExisting').click()
      cy.selectFromDropdown('site_id', transectId[0])

      cy.dataCy('transect_route_type')
        .should('contain.value', 'Vehicle road')
        .should('be.disabled')
      cy.dataCy('bearing').should('contain.value', '50').should('be.disabled')
      cy.dataCy('quadrat_width')
        .should('contain.value', '2')
        .should('be.disabled')
      cy.dataCy('quadrat_length')
        .should('contain.value', '2')
        .should('be.disabled')
      cy.dataCy('transect_length')
        .should('contain.value', '20')
        .should('be.disabled')
      cy.dataCy('transect_spacing')
        .should('contain.value', '1')
        .should('be.disabled')
      // TODO: test auto fill for this one
      cy.get('[data-cy="habitat"]')
        .should('be.disabled')
        .prev()
        .should('contain.text', 'Forest')
    })
    it('4.2[test-to-pass] - transect 1 - save and add next step', () => {
      cy.dataCy('done').eq(-1).click()
      cy.log(`transectId: ${JSON.stringify(transectId)}`)
      cy.nextStep()
    })
  })
  context('5. survey setup', () => {
    it(`5.1[validate-field] - survey setup transect 1`, () => {
      cy.offPlotSurveySetup1(transectId[0], 'herb_off').then((id) => {
        repeatVisitSurveySetupIds.push(id)
      })
    })
    it('finish survey setup', () => {
      cy.log(
        `repeatVisitSurveySetupIds: ${JSON.stringify(
          repeatVisitSurveySetupIds,
        )}`,
      )
      cy.nextStep()
    })
  })
  //TODO add test-to-fail and observations to the quadrats
  context('6. conduct the survey', () => {
    it('6.1[test-to-pass] - start transect 1', () => {
      cy.dataCy('transect_number').selectFromDropdown(transectId[0])
      cy.dataCy('setup_ID').should('contain.text', repeatVisitSurveySetupIds[0])

      cy.dataCy('addImg').eq(0).click().wait(50)
      cy.dataCy('takePhoto').eq(0).click().wait(50)
      cy.get('[data-cy="done"]').eq(0).click()

      cy.selectFromDropdown('quadrat_number', 'Quadrat 1')
    })

    it('6.2[test-to-pass] - add quadrat 1', () => {
      cy.setExpansionItemState(`quadrat_observation_1_`, 'open')
      cy.dataCy('herbivory_or_damage_encountered').selectFromDropdown(
        'Herbivory encountered',
      )

      cy.dataCy('type_of_damage').selectFromDropdown('Leaf chewing')
      cy.dataCy('affected_plant_species').selectFromSpeciesList('Stipa dichelachne')
      cy.dataCy('growth_form').selectFromDropdown('Forb', 'forb')

      cy.dataCy('attributable_fauna_species').selectFromDropdown()

      cy.dataCy('addImg').eq(-1).click()
      cy.dataCy('takePhoto').eq(-1).click()

      cy.dataCy('observation_comments').type('comments')
      cy.dataCy('done').eq(-1).click()
      cy.dataCy('nextPointBtnLabel').click()
    })
    it('6.3[test-to-pass] - add rest of quadrats without any obs', () => {
      for (let i = 0; i < 9; i++) {
        cy.dataCy('nextPointBtnLabel').click()
      }
    })
    it('finish conducting the survey', () => {
      cy.dataCy('done').eq(-1).click()
      cy.nextStep()
    })
  })
  context('publish', () => {
    // it('submit the survey to core', () => {
    //   cy.workflowPublish()
    // })
  pub()
  })
})

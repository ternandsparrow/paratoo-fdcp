// trapping setup survey
// each vert trapping test is numbered , so it will be executed in order
//  setup traps -> check traps -> end or remove traps
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'vertebrate fauna'
const protocol = 'Vertebrate Fauna - Identify, Measure and Release'


// barcodes are added app-only so it can fill in data. The schema does not require them.
global.ignorePaths = [
  'vertebrate-check-trap.0.barcode', 
  'vertebrate-check-trap.0.trap_number', 
  'vertebrate-check-trap.1.barcode', 
  'vertebrate-check-trap.1.trap_number',
  'vertebrate-check-trap.2.barcode',
  'vertebrate-check-trap.2.trap_number'
]

import { pub } from '../../support/command-utils'
import {
  vertebrateTrapCheckMeasurement as measurementFields,
  vertebrateTrapCheckReproductive as reproductiveFields,
} from '../../support/vertConditionalFields'

const speciesClass = {
  mammal: 'Mammal',
  amphibian: 'Amphibian',
  reptile: 'Reptile',
}

const speciesLut = {
  mammal: 'MA',
  amphibian: 'AM',
  reptile: 'RE',
}

const capturedPhotos = [
  'head_from_above',
  'head_from_side',
  'whole_body_from_above',
  'whole_body_from_below',
  'hind_foot',
  'front_foot',
  'feature_of_interest',
]

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  // it('.should() - start collection button should be disabled', () => {
  //   cy.selectProtocolIsDisabled(module, protocol)
  // })
  it('.should() - do plot layout', () => {
    cy.plotLayout(
      false,
      null,
      {
        layout: 'QDASEQ0003',
        visit: 'QLD spring survey',
      }
    )
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})

describe('Complete survey setup', () => {
  it('.should - complete survey', () => {
    cy.get('[data-cy="title"]')
      .should('contain.text', 'Trap Check Survey', { timeout: 300000 })
      .then(() => {
        cy.getAndValidate('trap_check_interval').selectFromDropdown('Morning')
        cy.getAndValidate('comment').type('bla bla bla')
        cy.getAndValidate('trap_checked_by', { isComponent: true })
        cy.addObservers('trap_checked_by', ['John', 'Jane', 'Mary'])
        cy.nextStep()
      })
  })
})
describe('Check trap P1 - capture details of trap P1 with species class of mammal', () => {
  it(`Check trap P1 with capture status`, function () {
    cy.vertCheckTrap('P1', 'Capture')
  })
  it(`Record general info of captured animal in P1`, function () {
    cy.vertGeneralInfoOfTrapWithCapture('P1', speciesClass.mammal, 'Platypus')
  })

  it(`Record animal's measurements`, function () {
    cy.getAndValidate('measurement', { isComponent: true })
    cy.vertRecordMeasurements(measurementFields, speciesLut.mammal)
  })

  it(`Record animal's reproductive traits`, function () {
    cy.getAndValidate('reproductive_condition', { isComponent: true })
    cy.vertRecordReproductiveTraits('P1')
  })

  it(`Record animal's sex and age class`, function () {
    cy.vertRecordSexAndAgeClass('P1')
  })

  it(`Record animal's body and skin condition`, function () {
    cy.getAndValidate('body_condition', { isComponent: true })
    cy.vertRecordBodyAndSkinCondition('P1')
  })

  it(`Record animal's Clinical scoring`, function () {
    cy.getAndValidate('clinical_scoring', { isComponent: true })
    cy.vertRecordClinicalScoring('P1')
  })
  it(`save individual record`, function () {
    cy.getAndValidate('captured_photo', { isComponent: true })
    capturedPhotos.forEach((imgField, index) => {
      cy.getAndValidate(imgField, { isComponent: true })
      cy.dataCy('addImg')
        .eq(index + 1)
        .click() // there another addImg above
      cy.wait(50)
      cy.get('[data-cy="takePhoto"]').eq(-1).click()
      cy.wait(50)
    })
  })
  it(`captured photo`, function () {
    cy.dataCy('done').eq(-2).click()
  })
  it(`animal fate`, function () {
    cy.getAndValidate('animal_fate').selectFromDropdown()
  })
  it(`check trap`, function () {
    cy.dataCy('done').eq(-1).click()
  })
})
describe('Check trap F1 - capture details of trap F1 with species class of amphibian', () => {
  it(`Check trap F1  with capture status`, function () {
    cy.vertCheckTrap('F1', 'Capture')
  })
  it(`Record general info of captured animal in F1`, function () {
    cy.vertGeneralInfoOfTrapWithCapture(
      'F1',
      speciesClass.amphibian,
      'Toads [Class]',
    )
  })

  it(`Record animal's measurements`, function () {
    cy.vertRecordMeasurements(measurementFields, speciesLut.amphibian)
  })

  it(`Record animal's reproductive traits`, function () {
    cy.vertRecordReproductiveTraits('F1')
  })

  it(`Record animal's sex and age class`, function () {
    cy.vertRecordSexAndAgeClass('F1')
  })

  it(`Record animal's body and skin condition`, function () {
    cy.vertRecordBodyAndSkinCondition('F1')
  })

  it(`Record animal's Clinical scoring`, function () {
    cy.vertRecordClinicalScoring('F1')
  })
  it(`save individual record`, function () {
    cy.dataCy('done').eq(-1).click()
  })
})
describe('Check trap F2 - capture details of trap F2 with species class of reptile', () => {
  it(`Check trap F2  with capture status`, function () {
    cy.vertCheckTrap('F2', 'Capture')
    // cy.nextStep()
  })
  it(`Record general info of captured animal in F2`, function () {
    cy.vertGeneralInfoOfTrapWithCapture(
      'F2',
      speciesClass.reptile,
      'Crocodylus [Genus]',
    )
  })

  it(`Record animal's measurements`, function () {
    cy.vertRecordMeasurements(measurementFields, speciesLut.reptile)
  })

  it(`Record animal's reproductive traits - should not visible`, function () {
    // cy.dataCy('female_breeding_status').should('not.exist')
    cy.dataCy('male_frog_breeding_status').should('not.exist')
  })

  it(`Record animal's sex and age class`, function () {
    cy.vertRecordSexAndAgeClass()
  })

  it(`Record animal's body and skin condition`, function () {
    cy.vertRecordBodyAndSkinCondition('F2')
  })

  it(`Record animal's Clinical scoring`, function () {
    cy.vertRecordClinicalScoring('F2')
  })
  it(`save individual record`, function () {
    cy.dataCy('done').eq(-2).click()
  })
  it(`check trap`, function () {
    cy.dataCy('done').eq(-1).click()
  })
})

describe('Check trap F2 - empty', () => {
  it(`Check trap F2  with capture status`, function () {
    cy.vertCheckTrap('F2', 'Empty')
  })
})

describe('Publish', () => {
  it('Submit survey', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

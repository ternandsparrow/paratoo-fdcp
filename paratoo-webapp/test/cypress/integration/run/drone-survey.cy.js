const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'Drone Survey'
import { generateCypressTests, pub } from '../../support/command-utils'

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + module + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })
  it('getValidationRules', () => {
    cy.documentation()
      .currentProtocolWorkflow((models) => {
        global.currentProtRules = models
        cy.log('global.currentProtRules', global.currentProtRules)
      })
      .then(() => {
        cy.log(generateCypressTests(global.currentProtRules))
      })
  })
})

//TODO double-check we covered all the fields and the assertions are up-to-date
const sensorsToSelectAndCheck = {
  'P1 + RedEdgeMX-Dual': {
    flight_altitude: { state: 'exist', value: '80' },
    elevation_optimisation: { state: 'exist', value: true },
    frontal_overlap_ratio: { state: 'exist', value: '80' },
    side_overlap_ratio: { state: 'exist', value: '80' },
    margin: { state: 'exist', value: '100' },
    flight_speed: { state: 'exist', value: '8' },
    course_angel: { state: 'exist', value: '0' },
    target_surface_takeoff_point: { state: 'exist', value: '0m', isDropdown: true },
    IMU_calibration: { state: 'not.exist' },

    drone_white_balance: { state: 'exist', value: 'Sunny', isDropdown: true },
    drone_return_mode: { state: 'not.exist' },
    drone_sampling_rate: { state: 'not.exist' },
    drone_scanning_mode: { state: 'not.exist' },
    RGB_colouring: { state: 'not.exist' },

    altitude_tolerance: { state: 'exist', value: '20' },
    drone_capture_mode: { state: 'exist', value: 'Timer', isDropdown: true },
    timer_interval: { state: 'exist', value: '1' },
    target_alt: { state: 'exist', value: '80' },
    drone_micasense_crp: { state: 'exist', value: 'Pre-flight', isDropdown: true },
  },
  'Lidar L2': {
    flight_altitude: { state: 'exist', value: '50' },
    elevation_optimisation: { state: 'exist', value: true },
    frontal_overlap_ratio: { state: 'exist', value: '70' },
    side_overlap_ratio: { state: 'exist', value: '52' },
    margin: { state: 'exist', value: '100' },
    flight_speed: { state: 'exist', value: '5' },
    course_angel: { state: 'exist', value: '0' },
    target_surface_takeoff_point: { state: 'exist', value: '0m', isDropdown: true },
    IMU_calibration: { state: 'exist', value: true },
    
    drone_white_balance: { state: 'exist', value: 'Sunny', isDropdown: true },
    drone_return_mode: { state: 'exist', value: 'Penta-Return', isDropdown: true },
    drone_sampling_rate: { state: 'exist', value: '240 KHz', isDropdown: true },
    drone_scanning_mode: { state: 'exist', value: 'Repetitive', isDropdown: true },
    RGB_colouring: { state: 'exist', value: true },

    altitude_tolerance: { state: 'not.exist' },
    drone_capture_mode: { state: 'not.exist' },
    timer_interval: { state: 'not.exist' },
    target_alt: { state: 'not.exist' },
    drone_micasense_crp: { state: 'not.exist' },
  },
  'Other': null,
}

const sensorLutMapper = {
  // 'P1REMXD': 'P1 + RedEdgeMX-Dual',
  // 'LI2': 'Lidar L2',
  'P1 + RedEdgeMX-Dual': 'P1REMXD',
  'Lidar L2': 'LI2',
}

describe('1. Drone Survey', () => {
  it('1.1 - survey', () => {
    cy.getAndValidate('drone_base_station_location')
      .selectFromDropdown('Other')
    cy.recordLocation()
    cy.addImages('addImg')
    cy.nextStep()
  })
})

for (const [i, [sensorSelection, sensorChecks]] of Object.entries(sensorsToSelectAndCheck).entries()) {
  describe(`2. Drone flight flight ${i + 1} (sensor ${sensorSelection})`, () => {
    it(`initial fields`, () => {
      cy.getAndValidate('flight_number').type(i + 1)
      cy.addObservers('observers', ['John', 'Jane', 'Mary'])
      cy.getAndValidate('UAV_type').selectFromDropdown()
      cy.log(`Will select sensor (and check autofill if it's a relevant case): ${sensorSelection}`)
      cy.getAndValidate('drone_sensor')
        //select a specific sensor, then can handle checking autofill
        .selectFromDropdown(sensorSelection)
    })
    
    if (sensorChecks) {
      let staticInfoTextToMatch = null
      it('get static data to check', () => {
        cy.window()
          .then(($win) => {
            const staticData = $win.droneSensorStaticData
            const relevantStaticData = staticData[sensorLutMapper[sensorSelection]]
            staticInfoTextToMatch = relevantStaticData.join('')
          })
      })
      it('check static info is displayed', () => {
        cy.dataCy('droneSensorStaticInfo')
          .should('contain.text', staticInfoTextToMatch)
      })

      it('check autofill', () => {
        
        for (const [field, check] of Object.entries(sensorChecks)) {
          cy.log(`Field ${field} should ${check.state} and have value ${check.value}`)
          cy.dataCy(field)
            .should(check.state)
          
          if (check.state !== 'not.exist') {
            if (typeof check.value === 'boolean') {
              let assertion = null
              if (check.value === true) {
                assertion = 'be.checked'
              } else {
                assertion = 'not.be.checked'
              }
              cy.dataCy(field)
                .should(assertion)
            } else {
              if (check.isDropdown) {
                cy.dataCy(field)
                  .should('contain.text', check.value)
              } else {
                cy.dataCy(field)
                  .should('have.value', check.value)
              }
            }
          }
          
        }
      })
    }

    it('rest of fields', () => {
      cy.dataCy('flight_altitude').clear().type(100.5)
      cy.dataCy('target_surface_takeoff_point').selectFromDropdown()
      cy.dataCy('flight_speed').clear().type(1.5)
      if (i % 2 !== 0) {
        //only check on odd indexes
        cy.dataCy('elevation_optimisation').check()
      }
      cy.getAndValidate('frontal_overlap_ratio').clear().type(1.5)
      cy.getAndValidate('side_overlap_ratio').clear().type(1.5)
      cy.getAndValidate('margin').clear().type(1.5)
      cy.completeWeatherForm({
        precipitation: 'Showers',
        precipitation_duration: 'Brief',
        wind_description: 'Light winds',
        wind_speed: '10',
        cloud_cover: '1 Haze',
        sun_angle: '10',
      })
      cy.getAndValidate('course_angel').type(1)
      cy.dataCy('issues').type('Some issues')
      cy.dataCy('comments').type('Some comments')

      cy.dataCy('done').click()
      if (i < 3) {
        cy.dataCy('addEntry').click()
      }
    })
  })
}

describe('3. End', () => {
  it('finish survey', () => {
    cy.nextStep()
  })
  pub()
})
const { pub } = require("../../support/command-utils")

const protocol = 'Fauna Aerial Survey'
const module = 'fauna aerial surveys'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

function pickRandomSpecies() {
  const count = Math.floor(Math.random() * (species.length - 1)) + 1
  const shuffled = species.sort(() => 0.5 - Math.random())
  return shuffled.slice(0, count)
}

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })
})

let surveyID = null
let otherSpecies = {}
let map = {}

const species = ['G', 'P', 'CM', 'D', 'H', 'WD', 'F', 'CA', 'R', 'O']
describe('Complete aerial setup -desktop', () => {
  context(`1 test-to-fail`, () => {
    it('.should() - start new survey', () => {
      cy.get(`[data-cy="Create Survey Setup"]`).eq(0).click()
      cy.nextStep()
    })

    it('.should() - Complete aerial setup general fields', () => {
      cy.dataCy('survey_id')
        .invoke('val')
        .then((id) => {
          surveyID = id
        })
      cy.recordLocation()
      cy.getAndValidate('survey_name', {
        testEmptyField: ['workflowNextButton'],
      }).type('<value>')

      cy.getAndValidate('nearest_town_or_landmark', {
        testEmptyField: ['workflowNextButton'],
      }).type('<value>')
      cy.getAndValidate('survey_aim', {
        testEmptyField: ['workflowNextButton'],
      }).type('<value>')
      cy.getAndValidate('target_area_description', {
        testEmptyField: ['workflowNextButton'],
      }).type('<value>')

      cy.addObservers('observer_details', ['Tom', 'Jerry', 'Naruto'])

      cy.getAndValidate('aircraft_type', {
        testEmptyField: ['workflowNextButton'],
      }).selectFromDropdown()
      cy.getAndValidate('pilot_name', {
        testEmptyField: ['workflowNextButton'],
      }).type('<value>')
      cy.getAndValidate('aircraft_model').selectFromDropdown()

      cy.dataCy('aircraft_model')
        .invoke('val')
        .then((id) => {
          if (id === 'Helicopter') {
            cy.getAndValidate('status_of_helicopter_doors', {
              isComponent: true,
            })
            cy.get('.q-list:contains(Status Of Helicopter Doors)')
              .contains('On')
              .click()
          }
        })

      cy.getAndValidate('aircraft_owner_business_name', {
        testEmptyField: ['workflowNextButton'],
      }).type('<value>')
      cy.getAndValidate('aircraft_identification', {
        testEmptyField: ['workflowNextButton'],
      }).type('<value>')

      cy.getAndValidate('average_flying_height', {
        testEmptyField: ['workflowNextButton'],
      }).type(5)
      cy.getAndValidate('average_ground_speed', {
        testEmptyField: ['workflowNextButton'],
      }).type(5)
    })
    it('.should() - Complete aerial setup transects', () => {
      for (let i = 0; i < 2; i++) {
        cy.getAndValidate('transect_length', {
          testEmptyField: ['done', -1],
        }).type(5)
        cy.getAndValidate('transect_spacing', {
          testEmptyField: ['done', -1],
        }).type(5)

        cy.getAndValidate('transect_alignment', {
          testEmptyField: ['done', -1],
        }).type(5)

        cy.getAndValidate('survey_type', {
          testEmptyField: ['done', -1],
        }).selectFromDropdown()

        cy.dataCy('survey_type')
          .invoke('val')
          .then((item) => {
            if (item === 'Strip transect') {
              cy.dataCy('fixed_width').type(2)
            }
            if (item === 'Line transect (distance sampling)') {
              cy.dataCy('category1').clear().type('category1')
            }
          })
        cy.dataCy('done').eq(-1).click()
        if (i === 0) cy.dataCy('addEntry').eq(-1).click()
      }
    })
    it('.should() - Complete aerial setup species', () => {
      const speciesToPick = pickRandomSpecies()
      // const speciesToPick = ['O', 'WD', 'D']
      cy.getAndValidate('target_species', {
        testEmptyField: ['workflowNextButton'],
        isComponent: true,
      })

      for (const species of speciesToPick) {
        cy.get(`[data-cy="${species}_btn"]`).click()
        if (species === 'WD') {
          cy.dataCy('wild_dog').selectFromDropdown()
        }
        if (species === 'D') {
          cy.dataCy('deer').selectFromDropdown()
        }
        if (species === 'O') {
          cy.selectFromSpeciesList(
            'other_species',
            'red kangaroo',
            true,
            null,
            1,
          )
            .invoke('val')
            .then((text) => {
              otherSpecies = { surveyID: text }
            })

          cy.dataCy('done').eq(0).click()
        }
      }
    })

    it('do survey step (end)', () => {
      cy.completeWeatherForm()
      cy.nextStep()
    })
    pub()
    // it('publish setup survey', () => {
    //   cy.workflowPublish()
    // })
  })
})

describe('Complete aerial survey (field?)', () => {
  it('.should() - assert navigation to ' + protocol + ' again', () => {
    cy.selectProtocolIsEnabled(module)
  })

  it('.should() - start new survey', () => {
    cy.setCheck('Conduct Field Survey', true, { index: 0 }) // cy.get(`[data-cy="Survey"]`).click()
    cy.getAndValidate('setup_ID').selectFromDropdown()
    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
    })
    cy.nextStep()
  })

  it('enter observer details', () => {
    cy.getAndValidate('observer_name', {
      testEmptyField: ['workflowNextButton'],
      isComponent: true,
    }).selectFromDropdown()
    cy.dataCy('aerial_survey_experience').selectFromDropdown()
    cy.dataCy('observer_position').selectFromDropdown()
  })

  for (let i = 0; i < 2; i++) {
    it('.should() - Start transect ' + (i + 1), () => {
      cy.getAndValidate('transect_ID', {
        testEmptyField: ['done', -1],
        isComponent: true,
      }).selectFromDropdown()
      cy.dataCy('startSurvey').click()

      if (i === 0) {
        cy.getAndValidate('available_species', {
          testEmptyField: ['done', -1],
          isComponent: true,
        })
      }

      const speciesToPick =
        Object.keys(otherSpecies).length !== 0
          ? [...pickRandomSpecies(), otherSpecies.surveyID]
          : pickRandomSpecies()
      // const speciesToPick = ['O', 'WD', 'D', otherSpecies.surveyID]
      // can't add getAndValidate to this as it a custom component
      for (const species of speciesToPick) {
        if (species === otherSpecies.surveyID) {
          const other =
            species.indexOf(' ') >= 0
              ? species
                  .replace(/\s(.)/g, function (match, group) {
                    return group.toUpperCase()
                  })
                  .replace(/\s/g, '')
              : species
          cy.dataCy(other).click()
        } else {
          cy.dataCy(species).first().click()
        }
        if (species === 'D') {
          cy.dataCy(species).last().selectFromDropdown()
        }
        if (species === 'WD') {
          cy.dataCy(species).last().selectFromDropdown()
        }
        if (species === 'O') {
          cy.dataCy('Other').selectFromDropdown()
        }

        cy.get('body').then(($body) => {
          if ($body.find('[data-cy="distanceCategory"]').length > 0) {
            cy.log('found it!')
            const count = Math.floor(Math.random() * 5) + 1
            cy.dataCy(`category${count}`).click()
          }
        })

        cy.dataCy('comment').type('comment')
        const count = Math.floor(Math.random() * 50)
        let countStr = count.toString()

        for (let i = 0; i < countStr.length; i++) {
          let digit = countStr[i]
          cy.dataCy(digit).click()
        }
        cy.dataCy('enter').click()
      }
      cy.dataCy('endSurvey').click()
      cy.dataCy('done').eq(-1).click()
      if (i === 0) cy.dataCy('addEntry').eq(-1).click()
    })
  }

  it('do survey step (end)', () => {
    cy.getAndValidate('comments').type('comment')
    cy.nextStep()
  })
  pub()
  // it('publish survey', () => {
  //   cy.workflowPublish()
  // })
})

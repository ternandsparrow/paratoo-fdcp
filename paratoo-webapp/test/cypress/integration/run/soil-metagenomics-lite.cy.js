const { pub } = require("../../support/command-utils")

const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'soils'
const protocol = 'Soil Sub-pit and Metagenomics'

describe('landing', () => {
  it('login', () => {
    cy.login('TestUser', 'password')
  })

  it('navigation to ' + protocol + ' is disabled', () => {
    cy.selectProtocolIsDisabled(module, protocol)
    // cy.visit('/')
  })

  it('plot layout', () => {
    cy.plotLayout()
  })

  it('navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})

describe('do field ' + protocol.toLowerCase(), () => {
  describe('survey set-up', () => {
    it('select field survey', () => {
      cy.dataCy('Field').click()
      cy.customSelectDate({
        fieldName: 'start_date_time',
        date: {
          year: '2023',
          month: 'Feb',
          day: '2',
        },
      })
    })
    it('observers', () => {
      cy.getAndValidate('observers')
      cy.addObservers('observerName', ['Tom', 'Luke', 'Walid'])
    })

    it('select standard variant', () => {
      cy.getAndValidate('variant', { isComponent: true })
      cy.setCheck('Standard')

      // cy.dataCy('workflowNextButton').click()
      cy.nextStep()
    })
  })
  describe('soil sub pit', () => {
    it(`[validate-field] pit  body`, () => {
      cy.dataCy('soil_sub_pit_id').should('have.value', 'QDASEQ0001-1')

      cy.getAndValidate('collection_method', {
        testEmptyField: ['done', -1],
      }).selectFromDropdown('Differential GNSS')

      cy.recordLocation()
    })

    // ===================================== microhabitat ==========================
    it(`[validate-field] pit 1 microhabitat 1`, () => {
      cy.setExpansionItemState('microhabitat_*_1_expansion', 'open')

      cy.getAndValidate('photo', { isComponent: true })
      cy.addImages('addImg')

      cy.getAndValidate('microhabitat_description').type(
        'Test comments for expansion ',
      )
      cy.dataCy('done').eq(0).click()
    })

    //   ========================= observation ==========================
    it(`[validate-field] pit observation 1`, () => {
      cy.setExpansionItemState(`soil_sub_pit_observation_1_`, 'open')

      cy.getAndValidate('observation_type', {
        testEmptyField: ['done', 1],
      }).selectFromDropdown('Soil pit')

      cy.getAndValidate('soil_pit_depth', {
        testEmptyField: ['done', 1],
      }).type('5')

      cy.getAndValidate('digging_stopped_by', {
        testEmptyField: ['done', 1],
      }).selectFromDropdown('Rock')

      cy.getAndValidate('soil_pit_photo', { isComponent: true })
      cy.addImages('addImg')
      cy.dataCy('comments').type('Test comments')

      cy.dataCy('done').eq(0).click().wait(100)
    })
  })

  describe('submit', () => {
    it('publish', () => {
      cy.nextStep()
      // cy.workflowPublish()
    })
    pub()
  })
})

describe('lab', () => {
  it('navigation to ' + protocol, () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
  it('survey', () => {
    cy.dataCy('Lab').click()
    cy.testEmptyField('Field Survey', 'workflowNextButton')
    // cy.dataCy('field_survey_id').click().type('02-02-23')
    cy.selectFromDropdown('field_survey_id', '02-02-23')
    cy.nextStep()
  })

  it(`sampling`, () => {
    cy.selectFromDropdown('soil_sub_pit', 'QDASEQ0001-1')
  })

  it('do not collect data for Metagenomics', () => {
    //`collect_metagenomics` wasn't checked during field survey
    cy.dataCy('metagenomics_sample').should('not.exist')
  })
  it('collect data for 0.00 - 0.10m', () => {
    cy.setExpansionItemState('0.00m - 0.10m', 'open').then(() => {
      cy.dataCy('not_collected').click()
      cy.dataCy('done').click()
    })
    //   cy.dataCy('0.00m - 0.10m').click()
  })
  it('collect data for 0.10m - 0.20m', () => {
    cy.setExpansionItemState('0.10m - 0.20m', 'open').then(() => {
      cy.dataCy('not_collected').click()
      cy.dataCy('done').click()
    })
  })
  it('failed to upload if missing samples', () => {
    cy.testEmptyField('Invalid data in: 0.20m - 030m', 'workflowNextButton')
  })
  it('collect data for 0.20m - 0.30m', () => {
    cy.setExpansionItemState('0.20m - 0.30m', 'open').then(() => {
      cy.dataCy('recordBarcode').click()
      cy.dataCy('done').click()
    })
  })

  it('publish', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

import { capitalizeWords } from '../../support/command-utils'

const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const moduleLayout = 'plot selection and layout'
const protocolLayout = 'Plot Layout'
const moduleFloristics = 'floristics'
const protocolFloristicsFull = 'Floristics - Enhanced'
const modulePtv = 'plant tissue vouchering'
const protocolPtvFull = 'Plant Tissue Vouchering - Enhanced'
const moduleOpportune = 'opportune'
const protocolOpportune = 'Opportunistic Observations'

const modulesAndProtocols = {
  layout: {
    module: moduleLayout,
    protocol: protocolLayout,
  },
  floristicsFull: {
    module: moduleFloristics,
    protocol: protocolFloristicsFull,
    speciesName: 'acacia sp.',
  },
  ptvFull: {
    module: modulePtv,
    protocol: protocolPtvFull,
    speciesNameToSelect: 'acacia sp.',
  },
  opportune: {
    module: moduleOpportune,
    protocol: protocolOpportune,
  },
}

const newVisitName = `new test visit ${Math.floor(Math.random() * 100000)}`

function collectLayout({ def }) {
  cy.selectProtocolIsEnabled(def.module)
  cy.dataCy('locationExisting').click()
  cy.selectFromDropdown('id', 'SATFLB0001')
  cy.nextStep()

  cy.dataCy('visitNew').click()
  cy.dataCy('visit_field_name').type(
    newVisitName,
  )
  
  cy.workflowPublishQueue()
}

function collectFloristicsFull({ def }) {
  cy.selectProtocolIsEnabled(def.module, def.protocol)
  cy.nextStep()

  cy.selectFromSpeciesList('field_name', def.speciesName, true)
  cy.setCheck('toggleBarcodeReaderShow', true)
  cy.dataCy('recordBarcode').click()
  cy.selectFromDropdown('growth_form_1', null)
  cy.dataCy('done').eq(-1).click().wait(200)

  cy.nextStep()
  cy.workflowPublishQueue()
}

function collectPtvFull({ def }) {
  cy.selectProtocolIsEnabled(def.module, def.protocol)
  cy.nextStep()

  cy.selectFromDropdown('floristics_voucher', def.speciesNameToSelect)
  cy.setCheck('toggleBarcodeReaderShow', true)
  cy.dataCy('recordBarcode').click()
  cy.dataCy('done').eq(-1).click().wait(200)

  cy.nextStep()
  if (!def?.doNotQueue) {
    cy.workflowPublishQueue()
  }
}

function collectOpportune({ def }) {
  cy.selectProtocolIsEnabled(def.module)
  cy.nextStep()

  cy.recordLocation()
  cy.dataCy('taxa_type').selectFromDropdown('Vascular plant')
  cy.selectFromSpeciesList('species', 'eucalyptus sp.', true)
  cy.dataCy('number_of_individuals').type(2)
  cy.dataCy('Exact').click()
  cy.addObservers('observers', ['John', 'Jane', 'Mary'])
  cy.dataCy('done').eq(-1).click()

  cy.nextStep()
  cy.workflowPublishQueue()
}

function checkHistorical({ isInHistorical, queueData, protocolId, responses, submitResponses }) {
  cy.log(`Checking that historical data ${isInHistorical ? 'is' : 'is not'} in responses and submitResponses`)
  const syncedCollection = queueData.find(
    q => q.protocolId === protocolId
  )
  console.log(`syncedCollection for protocolId=${protocolId}:`, JSON.stringify(syncedCollection, null, ))
  cy.wrap(syncedCollection)
    .should('not.be.undefined')
    .and('not.be.null')
  
  const responsesSurvey = responses.find(
    r => r.queuedCollectionIdentifier === syncedCollection.queuedCollectionIdentifier
  )
  cy.wrap(responsesSurvey)
    .should(`${isInHistorical ? 'not.' : ''}be.undefined`)
  
  const submitResponsesSurvey = submitResponses.find(
    r => r.queuedCollectionIdentifier === syncedCollection.queuedCollectionIdentifier
  )
  cy.wrap(submitResponsesSurvey)
    .should(`${isInHistorical ? 'not.' : ''}be.undefined`)

  return {
    syncedCollection,
  }
}

describe('Landing', () => {
  it('login', () => {
    cy.login('TestUser', 'password')
  })
})

describe('Add multiple collections to queue', () => {
  for (const [collectionProt, collectionDefs] of Object.entries(modulesAndProtocols)) {
    it(`collect ${collectionDefs.protocol}`, () => {
      switch (collectionProt) {
        case 'layout':
          collectLayout({ def: collectionDefs })
          break
        case 'floristicsFull':
          collectFloristicsFull({ def: collectionDefs })
          break
        case 'ptvFull':
          collectPtvFull({ def: collectionDefs })
          break
        case 'opportune':
          collectOpportune({ def: collectionDefs })
          break
        default:
          throw new Error(`Unregistered case for ${collectionProt}`)
      }
    })
  }
})

const dataTracker = {}

describe('Check historical data is moved to IndexDB', () => {
  it('make layout data invalid and sync', () => {
    cy.getStoreReference({
      store: 'dataManager',
      stateKey: 'publicationQueue',
    })
      .then((queueRef) => {
        console.log('queueRef:', queueRef)
        Object.assign(dataTracker, {
          queueRef,
        })
        const layoutQueuedData = queueRef.find(
          q => q.protocolId === 4 && q.collection['plot-visit'].visit_field_name === newVisitName
        )
        console.log('layoutQueuedData:', JSON.stringify(layoutQueuedData, null, 2))
        delete layoutQueuedData.collection['plot-definition-survey'].survey_metadata.survey_details.survey_model
      })
      .then(() => {
        cy.syncAndCheckQueueTable({
          shouldHaveSyncErrors: [
            'Plot Layout and Visit',
            'Floristics - Enhanced',
            'Plant Tissue Vouchering - Enhanced',
          ],
        })
      })
  })
  it('check opportune synced, was removed from responses and submitResponses, and moved to IndexDB', () => {
    cy.getStoreReference({
      store: 'dataManager',
    })
      .then((dataManager) => {
        Object.assign(dataTracker, {
          responsesAfter1: Cypress._.cloneDeep(dataManager.responses),
          submitResponsesAfter1: Cypress._.cloneDeep(dataManager.submitResponses),
        })
      })
      .then(() => {
        const { syncedCollection } = checkHistorical({
          isInHistorical: false,
          queueData: dataTracker.queueRef,
          protocolId: 1,
          responses: dataTracker.responsesAfter1,
          submitResponses: dataTracker.submitResponsesAfter1,
        })
        console.log('syncedCollection:', JSON.stringify(syncedCollection, null, 2))

        cy.checkHistoricalDataInDexie({
          table: 'responses',
          syncedSurveyQueuedCollectionIdentifier: syncedCollection.queuedCollectionIdentifier,
        })
        cy.checkHistoricalDataInDexie({
          table: 'submitResponses',
          syncedSurveyQueuedCollectionIdentifier: syncedCollection.queuedCollectionIdentifier,
        })
      })
  })
  it('make layout data valid and floristics data invalid, and sync', () => {

    cy.getStoreReference({
      store: 'dataManager',
      stateKey: 'publicationQueue',
    })
      .then((queueRef) => {
        console.log('queueRef:', queueRef)
        Object.assign(dataTracker, {
          queueRef,
        })
        const layoutQueuedData = dataTracker.queueRef.find(
          q => q.protocolId === 4 && q.collection['plot-visit'].visit_field_name === newVisitName
        )
        console.log('layoutQueuedData:', JSON.stringify(layoutQueuedData, null, 2))
        layoutQueuedData.collection['plot-definition-survey'].survey_metadata.survey_details.survey_model = 'plot-definition-survey'
    
        const floristicsQueueData = dataTracker.queueRef.find(
          q => q.protocolId === 12 && q.collection['floristics-veg-voucher-full'].some(
            v => v.field_name === modulesAndProtocols.floristicsFull.speciesName
          )
        )
        console.log('floristicsQueueData:', JSON.stringify(floristicsQueueData, null, 2))
        delete floristicsQueueData.collection['floristics-veg-survey-full'].survey_metadata.survey_details.survey_model
      })
      .then(() => {
        cy.syncAndCheckQueueTable({
          shouldHaveSyncErrors: [
            'Floristics - Enhanced',
            'Plant Tissue Vouchering - Enhanced',
          ],
        })
      })
  })
  it('check layout synced but is still in responses and submitResponses due to another collection depending on it, and check it was copied to IndexDB', () => {
    cy.getStoreReference({
      store: 'dataManager',
    })
      .then((dataManager) => {
        Object.assign(dataTracker, {
          responsesAfter2: Cypress._.cloneDeep(dataManager.responses),
          submitResponsesAfter2: Cypress._.cloneDeep(dataManager.submitResponses),
        })
      })
      .then(() => {
        const { syncedCollection } = checkHistorical({
          isInHistorical: true,
          queueData: dataTracker.queueRef,
          protocolId: 4,
          responses: dataTracker.responsesAfter2,
          submitResponses: dataTracker.submitResponsesAfter2,
        })
        console.log('syncedCollection:', JSON.stringify(syncedCollection, null, 2))

        cy.checkHistoricalDataInDexie({
          table: 'responses',
          syncedSurveyQueuedCollectionIdentifier: syncedCollection.queuedCollectionIdentifier,
        })
        cy.checkHistoricalDataInDexie({
          table: 'submitResponses',
          syncedSurveyQueuedCollectionIdentifier: syncedCollection.queuedCollectionIdentifier,
        })
      })
  })
  it('make floristics valid and sync', () => {
    cy.getStoreReference({
      store: 'dataManager',
      stateKey: 'publicationQueue',
    })
      .then((queueRef) => {
        console.log('queueRef:', queueRef)
        Object.assign(dataTracker, {
          queueRef,
        })
        const floristicsQueueData = dataTracker.queueRef.find(
          q => q.protocolId === 12 && q.collection['floristics-veg-voucher-full'].some(
            v => v.field_name === modulesAndProtocols.floristicsFull.speciesName
          )
        )
        console.log('floristicsQueueData:', JSON.stringify(floristicsQueueData, null, 2))
        floristicsQueueData.collection['floristics-veg-survey-full'].survey_metadata.survey_details.survey_model = 'floristics-veg-survey-full'
      })
      .then(() => {
        cy.syncAndCheckQueueTable({
          shouldHaveSyncErrors: [],
        })
      })
  })
  it('check floristics and PTV were removed from responses and were copied to IndexDB', () => {
    cy.wait(1000)
    cy.getStoreReference({
      store: 'dataManager',
    })
      .then((dataManager) => {
        Object.assign(dataTracker, {
          responsesAfter3: Cypress._.cloneDeep(dataManager.responses),
          submitResponsesAfter3: Cypress._.cloneDeep(dataManager.submitResponses),
        })
      })
      //floristics
      .then(() => {
        const { syncedCollection } = checkHistorical({
          isInHistorical: false,
          queueData: dataTracker.queueRef,
          protocolId: 12,
          responses: dataTracker.responsesAfter3,
          submitResponses: dataTracker.submitResponsesAfter3,
        })
        console.log('syncedCollection:', JSON.stringify(syncedCollection, null, 2))

        cy.checkHistoricalDataInDexie({
          table: 'responses',
          syncedSurveyQueuedCollectionIdentifier: syncedCollection.queuedCollectionIdentifier,
        })
        cy.checkHistoricalDataInDexie({
          table: 'submitResponses',
          syncedSurveyQueuedCollectionIdentifier: syncedCollection.queuedCollectionIdentifier,
        })
      })
      //PTV
      .then(() => {
        const { syncedCollection } = checkHistorical({
          isInHistorical: false,
          queueData: dataTracker.queueRef,
          protocolId: 14,
          responses: dataTracker.responsesAfter3,
          submitResponses: dataTracker.submitResponsesAfter3,
        })
        console.log('syncedCollection:', JSON.stringify(syncedCollection, null, 2))

        cy.checkHistoricalDataInDexie({
          table: 'responses',
          syncedSurveyQueuedCollectionIdentifier: syncedCollection.queuedCollectionIdentifier,
        })
        cy.checkHistoricalDataInDexie({
          table: 'submitResponses',
          syncedSurveyQueuedCollectionIdentifier: syncedCollection.queuedCollectionIdentifier,
        })
      })
  })
})

let unresolvedInProgressData = null
let resolvedInProgressData = null
describe('Check data manager queries historical data from IndexDB when syncing queue', () => {
  it('select an existing plot/visit', () => {
    cy.plotLayout()
  })
  it('queue depends-on data (Floristics)', () => {
    collectFloristicsFull({
      def: {
        module: moduleFloristics,
        protocol: protocolFloristicsFull,
        speciesName: 'eucalyptus sp.',
      },
    })
  })
  it('start dependent protocol (PTV) and make reference to data (do not queue)', () => {
    collectPtvFull({
      def: {
        module: modulePtv,
        protocol: protocolPtvFull,
        speciesNameToSelect: 'eucalyptus sp.',
        doNotQueue: true,
      },
    })
  })
  it('get in-progress data that hasn\'t been resolved', () => {
    cy.loadLocalStore('bulk')
      .then((bulkStore) => {
        unresolvedInProgressData = Cypress._.cloneDeep(bulkStore.collections)
      })
  })
  it('sync queue to submit depends-on data', () => {
    cy.dataCy('workflowBack').click().wait(1000)
    cy.syncAndCheckQueueTable({
      shouldHaveSyncErrors: [],
    })
  })
  it('get in-progress data that has been resolved', () => {
    cy.loadLocalStore('bulk')
      .then((bulkStore) => {
        resolvedInProgressData = Cypress._.cloneDeep(bulkStore.collections)
      })
  })
  it('check in-progress data had dependencies resolved when queue is synced', () => {
    console.log('unresolvedInProgressData:', JSON.stringify(unresolvedInProgressData))
    console.log('resolvedInProgressData:', JSON.stringify(resolvedInProgressData))

    cy.checkResolvedData({ unresolvedInProgressData, resolvedInProgressData })
  })
  it('finish dependent protocol (PTV)', () => {
    cy.editProtocol({
      module: modulePtv,
      buttonIndex: 0,
    })

    //don't need to fill in more data, so skip right to the end
    cy.get('[data-cy="Plant\\ Tissue\\ Vouchering\\ Survey\\ (end)"]').click().wait(2000)
    cy.workflowPublishQueue()
  })
  it('sync queue with dependent data', () => {
    cy.syncQueue()
  })
})
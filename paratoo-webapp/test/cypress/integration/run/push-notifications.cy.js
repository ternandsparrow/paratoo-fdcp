describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
})
// FIXME: we create a broadcast channel to mock receiving a push notification from service worker
// as that how the SW send the messages to the app
// the actual process should including create a message in Core, then wait for it to send the message at the scheduled time
// then assert the app to receive the message
describe('Push notifications', () => {
  it('should() - subscribe to push notifications', () => {
    const channel = new BroadcastChannel('sw-messages')
    const messageData = {
      id: 30,
      title: 'Hello from the other side!!!',
      message: `Hello, it's me! Core! I'm sending you a push notification!`,
      type: 'positive',
      isSent: false,
      createdAt: '2025-01-31T03:46:42.445Z',
      updatedAt: '2025-01-31T04:21:32.002Z',
      publishedAt: '2025-01-31T04:21:31.998Z',
      scheduledAt: '2024-12-31T13:30:00.000Z',
    }
    channel.postMessage({
      payload: {
        id: 1,
        messageData,
      },
    })

    cy.dataCy('notification-btn').should('be.visible')

    cy.dataCy('unread-message-count').should('have.text', '1')

    cy.dataCy('notification-btn').click()

    cy.dataCy('notification-title').should('have.text', messageData.title)

    cy.dataCy('notification-message').should('have.text', messageData.message)

    cy.window().then((win) => {
      cy.dataCy('notification-time').should(
        'have.text',
        win.prettyFormatDateTime(messageData.scheduledAt),
      )
    })

    cy.dataCy('dismiss').click()
    cy.dataCy('unread-message-count').should('not.be.visible')
    cy.dataCy('notification-message').should('not.be.visible')

    channel.close()
  })
})

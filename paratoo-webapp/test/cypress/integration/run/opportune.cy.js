
//TODO test conditional logic (that showing/hiding, etc. trigger correctly)
//TODO test opportune quick button
import {
  sample,
  upperFirst,
} from 'lodash'
import { pub, capitalizeWords } from '../../support/command-utils'
import { v4 as uuidv4 } from 'uuid'

const protocol = 'Opportunistic Observations'
const module = 'Opportune'
const uids = []
const barcodes = []
const voucher_tier_1_map = {
  AM: ['Animal tissue', 'Blood', 'Carcass', 'Scat', 'Other (specify)'],
  BI: [
    'Animal tissue',
    'Blood',
    'Carcass',
    'Regurgitant',
    'Scat',
    'Other (specify)',
  ],
  INV: ['Carcass', 'Scat', 'Other (specify)'],
  MA: [
    'Animal tissue',
    'Blood',
    'Carcass',
    'Regurgitant',
    'Scat',
    'Other (specify)',
  ],
  RE: [
    'Animal tissue',
    'Blood',
    'Carcass',
    'Regurgitant',
    'Scat',
    'Other (specify)',
  ],
  VP: ['Plant', 'Plant tissue', 'Other (specify)'],
  NVP: ['Plant', 'Plant tissue', 'Other (specify)'],
}
const mediaTypes = [
  'photo',
  'video',
  'audio',
]
const numMediaToCollect = 3

// select species
function selectFromSpeciesList(taxa) {
  const speciesSelection = {
    VP: ['crossolepis', 'Crossolepis brevifolia [Species]'],

    BI: ['magpie', 'Australian Magpie [sp]'],

    NVP: ['anthoceros', 'Anthoceros capricornii [Species]'],

    AM: ['cane', 'Cane Toad [Species]'],

    INV:
      //use field name as Inverts species list not currently supported
      ['Monitor lizard', null],

    MA: ['echidna', 'Echidna'],

    RE: ['brown', 'King Brown Snake [Species]'],
  }
  const speciesInfo = speciesSelection[taxa]
  cy.selectFromSpeciesList(
    'species',
    speciesInfo[0],
    speciesInfo[1] == null,
    speciesInfo[1],
  )
}

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })
})

describe(`Collect ${protocol}`, () => {
  it('.should() - complete survey step (start)', () => {
    cy.nextStep()
  })
  //TODO add more/all taxas
  const taxasToCollect = {
    VP: 'Vascular plant',
    BI: 'Bird',
    NVP: 'Non-vascular plant',
    AM: 'Amphibian',
    INV: 'Invertebrate',
    MA: 'Mammal',
    RE: 'Reptile',
  }
  for (const [taxaSymbol, taxaLabel] of Object.entries(taxasToCollect)) {
    //collect three of each media type (with comments)
    if (taxaSymbol === 'VP') {
      //don't need to collect media for all taxas as it's very intensive
      for (const mediaType of mediaTypes) {
        for (let mediaNum = 0; mediaNum < numMediaToCollect; mediaNum++) {
          //TODO test deleting and editing of media/comments
          it(`collect media ${mediaType} number ${mediaNum+1}`, () => {
            const parsedMediaType = upperFirst(mediaType)
            if (mediaNum === 0) {
              //first of each media type needs to open the component
              cy.dataCy(`add${parsedMediaType}`).click().wait(1000)
            }
  
            const commentToInput = `${mediaType} comment ${mediaNum+1}`
  
            if (mediaType === 'photo') {
              cy.dataCy(`take${parsedMediaType}`).click().wait(1000)
            } else {
              //2 second video/audio
              //TODO test pausing/resuming
              cy.dataCy('startRecording').click().wait(2000)
              cy.dataCy('stopRecording').click().wait(100)
            }
            cy.dataCy(`comment_${mediaNum}`).click().wait(100)
            cy.dataCy(`addComment_${mediaNum}`).type(commentToInput)
            cy.dataCy(`saveComment_${mediaNum}`).click().wait(100)
            cy.dataCy(`commentPreview_${mediaNum}`).should('contain.text', commentToInput)
  
            if (mediaNum === numMediaToCollect-1) {
              //close the component for this media type when it's the last, so that the
              //indexes between the different media types don't conflict
              cy.dataCy(`add${parsedMediaType}`).click().wait(1000)
            }
          })
        }
      }
      for (const [index, mediaType] of Object.entries(mediaTypes)) {
        //TODO test deleting and editing of media/comments
        it(`collect media ${mediaType} using file picker`, () => {
          const parsedMediaType = upperFirst(mediaType)
          cy.dataCy(`add${parsedMediaType}`).click()
          if (mediaType === 'photo') {
            cy.dataCy(`useFilePicker`).click()
            cy.pickFile(`test_photo.webp`, 0)
          } else {
            //2 second video/audio
            //TODO test pausing/resuming
            cy.dataCy(`useFilePicker`).eq(index).click()
            if(mediaType === 'audio') {
              cy.pickFile(`test_audio.weba`, index)
            } else {
              cy.pickFile(`test_video.mp4`, index)
            }
          }
        })
      }
    }
    it('record location', () => {
      cy.recordLocation()
    })
    if (taxaSymbol === 'MA') {
      it('test general empty fields', () => {
        cy.testEmptyField(
          'Field: "Taxa Type": This field is required',
          'done',
          -1,
        )
        cy.selectFromDropdown('taxa_type', taxaLabel, null)

        cy.testEmptyField(
          'Field: "Species": This field is required',
          'done',
          -1,
        )
      })
    } else {
      it('select ' + taxaLabel, () => {
        //observation ID is auto-generated
        // cy.dataCy('observation_id').isUnique(protocol).should('be.true')
        if (uids.length > 0) {
          cy.dismissPopups()
          cy.dataCy('observation_id').type('{backspace}'.repeat(18)).type(uids[uids.length - 1].substring(3)).blur()
          // cy.dataCy('observation_id').isUnique(protocol).should('be.false')
          cy.dataCy('observation_id').parents('.q-field').contains('Value must be unique').should('exist')
          cy.dataCy('done').eq(-1).click()
          cy.get('.q-notification', { timeout: 15000 })
            .should('contain.text', 'Field: "Observation Id": Value must be unique')
          cy.dismissPopups()

          cy.dataCy('observation_id').clear() // regens barcode
        }
        // cy.dataCy('observation_id').isUnique(protocol).should('be.true')
        cy.dataCy('observation_id').then((e) => {
          uids.push(e.val())
        })

        cy.selectFromDropdown('taxa_type', taxaLabel, null)
      })
    }
    it('.should() - select Species', () => {
      selectFromSpeciesList(taxaSymbol)
      if (
        //no inverts species list so no taxon rank
        taxaSymbol !== 'INV' &&
        //MA species list doesn't have taxon rank
        taxaSymbol !== 'MA' &&
        taxaSymbol !== 'VP' &&
        taxaSymbol !== 'AM' &&
        taxaSymbol !== 'RE'
      ) {
        //selecting a taxon rank of species auto-selects 'confident'
        cy.get('.q-notification').should(
          'contain.text',
          "Field 'confident' auto-populated based on your selected species taxon rank",
          // when species name contains [sp] or [Species] it show display the notification
        )
        cy.dataCy('confident').should('be.checked')
      }
    })
    if (taxaSymbol === 'MA') {
      it('test general required fields', () => {
        cy.testEmptyField(
          'Field: "Number Of Individuals": This field is required',
          'done',
          -1,
        )
        cy.dataCy('number_of_individuals').type(2)
        cy.testEmptyField(
          'Field: "Exact Or Estimate": This field is required',
          'done',
          -1,
        )
        cy.dataCy('Exact').click()
        cy.testEmptyField(
          'Field: "Observation Method Tier 1": This field is required',
          'done',
          -1,
        )
        cy.selectFromDropdown('observation_method_tier_1', 'Sign')
        cy.testEmptyField(
          'Field: "Observation Method Tier 2": This field is required',
          'done',
          -1,
        )
        cy.selectFromDropdown('observation_method_tier_2', 'DNA')
        cy.testEmptyField(
          'Field: "Observers": This field is required',
          'done',
          -1,
        )
      })
    } else {
      it('type number of individuals', () => {
        cy.dataCy('number_of_individuals').type(2)
      })
    }

    it('.should() - complete other fields', () => {
      cy.dataCy('Exact').click()
      cy.addObservers('observers', ['John', 'Jane', 'Mary'])

      if (taxaSymbol !== 'VP') {
        //VP taxa collect bare minimum at first, then come back to edit last
        cy.dataCy('Exact').click()

        cy.selectFromDropdown('habitat_description', null)
        cy.dataCy('comments').type('Some notes\n\nNext line')

        if (taxaSymbol !== 'VP' && taxaSymbol !== 'NVP') {
          //fauna-specific fields
          //each tier depends on the previous
          //TODO add some variation to observation method (so we can test various conditions)
          cy.selectFromDropdown('observation_method_tier_1', 'Sign')
          cy.wait(50)
          cy.get('body').then(($body) => {
            const tier2Hidden =
              !$body.find('[data-cy=observation_method_tier_2]').length > 0
            if (!tier2Hidden) {
              cy.selectFromDropdown('observation_method_tier_2', 'DNA')
              cy.wait(50)
            }
          })
          cy.get('body').then(($body) => {
            const tier3Hidden =
              !$body.find('[data-cy=observation_method_tier_3]').length > 0
            if (!tier3Hidden) {
              //pick any tier 2
              cy.selectFromDropdown('observation_method_tier_3', 'Fresh')
              cy.wait(50)
            }
          })

          cy.dataCy('add_additional_fauna_data').click().wait(50)
          cy.get('body').then(($body) => {
            //FIXME workaround - checking `add_additional_fauna_data` doesn't always show dependent fields in Cypress (works normally)
            const additionalFaunaDataDependenciesHidden =
              !$body.find('[data-cy=age_class]').length > 0
            if (!additionalFaunaDataDependenciesHidden) {
              //collect two `add_additional_fauna_data`
              for (let i = 0; i < 2; i++) {
                cy.selectFromDropdown('sex', null)
                cy.selectFromDropdown('age_class', 'Adult')
                cy.selectFromDropdown('breeding_code', null)
                cy.dataCy('done').eq(0).click()

                if (i < 1) {
                  cy.dataCy('addEntry').eq(0).click()
                }
              }
            } else {
              cy.dataCy('add_additional_fauna_data').click().wait(50)
            }
          })
        } else if (taxaSymbol === 'VP') {
          //collect 3 of growth form and life stage component
          for (let i = 0; i < 3; i++) {
            cy.selectFromDropdown('growth_form', null)
            cy.selectFromDropdown('life_stage', null)

            cy.dataCy('done').eq(0).click().wait(50)
            if (i < 2) {
              cy.dataCy('addEntry').eq(0).click()
            }
          }
        }
      }
    })
    it('.should() - collect 5 vouchers', function () {
      cy.dataCy('doingVouchering').click()
      //collect 5 vouchers
      for (let i = 0; i < 5; i++) {
        if (taxaSymbol === 'VP' || taxaSymbol === 'NVP') {
          cy.get('body').then(($body) => {
            //FIXME workaround - voucher condition doesn't always show when it should (fine outside of Cypress)
            const conditionIsHidden =
              !$body.find('[data-cy=voucher_condition]').length > 0
            if (!conditionIsHidden) {
              cy.selectFromDropdown('voucher_condition', null)
            }
          })
        }

        //different taxa have different types of voucher selections
        const tier1ToSelect = sample(voucher_tier_1_map[taxaSymbol])
        cy.log(`Selecting Voucher Type Tier 1: ${tier1ToSelect}`)
        cy.selectFromDropdown('voucher_type_tier_1', tier1ToSelect)

        //check if field exists: https://stackoverflow.com/a/56151893
        // cy.get('body').then(($body) => {
        //   const tier2Hidden =
        //     !$body.find('[data-cy=voucher_type_tier_2]').length > 0
        //   if (!tier2Hidden) {
        //     //pick any tier 2
        //     cy.selectFromDropdown('voucher_type_tier_2', null)
        //   }
        //   cy.dataCy('voucher_comment').type(
        //     `Comment for ${taxaSymbol} voucher ${i + 1}`,
        //   )
        // })
        if (barcodes.length > 0) {
          cy.dataCy('manualBarcodeEntry')
            .toggle(true)
          cy.dataCy('voucher_barcode').find('input').eq(2).type(barcodes[barcodes.length - 1]) // manual barcode entry
          cy.dataCy('voucher_barcode').find('input').eq(3).type(barcodes[barcodes.length - 1]) // retype
          cy.dataCy('recordBarcode').click()
          cy.get('.q-notification', { timeout: 15000 })
            .should('contain.text', 'Barcode already exists. Please enter a new one')
          // TODO popup //cy.dataCy('saveVoucher').click()
          // cy.dataCy('voucher_barcode')
          //   .isUnique(protocol, function () {
          //     return cy.dataCy('saveVoucher')
          //       .parents('.q-card')
          //       .contains('Saved barcode')
          //       .invoke('text')
          //       .then(e => e.split(':')[1].replaceAll(' ', ''))
          //   }).should('be.false')
          // TODO saveVoucher should fail
          cy.dataCy('saveVoucher').click()
          cy.get('.q-notification', { timeout: 15000 })
            .should('contain.text', 'Barcode already exists. Please enter a new one')
          // cy.get('.q-table').contains('delete').eq(0).click()
          cy.dataCy('voucher_barcode').find('input').eq(2).clear() // manual barcode entry
          cy.dataCy('voucher_barcode').find('input').eq(3).clear() // retype
        }

        cy.dataCy('toggleBarcodeReaderShow')
          .toggle(true)
        cy.dataCy('recordBarcode').click()
        // cy.dataCy('voucher_barcode')
        //   .isUnique(protocol, function () {
        //     return cy.dataCy('saveVoucher')
        //       .parents('.q-card')
        //       .contains('Saved barcode')
        //       .invoke('text')
        //       .then(e => e.split(':')[1].replaceAll(' ', ''))
        //   }).should('be.true')

        cy.dataCy('saveVoucher')
          .parents('.q-card')
          .contains('Saved barcode')
          .invoke('text')
          .then(e => barcodes.push(e.split(':')[1].replaceAll(' ', '')))
        cy.dataCy('saveVoucher').click()
      }

    })
    it('done voucher', () => {
      //TODO come back and edit VP observation with more info (this is just the minimal info)
      cy.get('[data-cy="done"]').eq(-1).click().wait(500)

      if (taxaSymbol !== 'RE') {
        cy.get('[data-cy="addEntry"]').eq(-1).click()
      }
    })
  }
  it('.should() - complete survey step (end)', () => {
    cy.nextStep()
  })
})

describe('submit and publish', () => {
  pub()
  // it('submit and publish', () => {
  //   cy.workflowPublish(false)
  // })
})

// unique id. get one from the list made earlier and try to submit with it
let correctId
describe('Upload with duplicate values in unique fields', () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })

  it('.should() - complete survey step (start)', () => {
    cy.nextStep()
  })
  it('record location', () => {
    cy.recordLocation()
  })
  it('check observation_id uniqueness from previous submission', () => {
    for (const id of uids) {
      cy.dataCy('observation_id').type('{backspace}'.repeat(18)).type(id.substring(3)).blur()
      cy.dataCy('observation_id').parents('.q-field').contains('Value must be unique').should('exist')
      // cy.dataCy('observation_id').isUnique(protocol).should('be.false')
    }
    // TODO upload and test weather the survey submitted without the observation

    // dont clear and upload using a duplicate
    cy.dataCy('observation_id').clear()
    cy.dataCy('observation_id')
      .invoke('val')
      .then((id) => {
        cy.log(id, correctId)
        correctId = id
      }) // regens id
    // cy.dataCy('observation_id').isUnique(protocol).should('be.true')
  })

  it('finish observation', () => {
    cy.selectFromDropdown('taxa_type', 'Bird', null)
    cy.selectFromSpeciesList('species', 'magpie')
    cy.dataCy('number_of_individuals').type(2)
    cy.dataCy('Exact').toggle(true)
    cy.addObservers('observers', ['John', 'Jane', 'Mary'])
    cy.selectFromDropdown('observation_method_tier_1', 'Sign')
    cy.selectFromDropdown('observation_method_tier_2', 'DNA')
    cy.dataCy('doingVouchering').click()
    // cy.dataCy('manualBarcodeEntry')
    //   .toggle(true)
    // cy.dataCy('voucher_barcode').find('input').eq(2).type(barcodes[barcodes.length - 1]) // manual barcode entry
    // cy.dataCy('voucher_barcode').find('input').eq(3).type(barcodes[barcodes.length - 1]) // retype
    cy.dataCy('recordBarcode').click()
    // TODO popup //cy.dataCy('saveVoucher').click()
    // cy.dataCy('voucher_barcode')
    //   .isUnique(protocol, function () {
    //     return cy.dataCy('saveVoucher')
    //       .parents('.q-card')
    //       .contains('Saved barcode')
    //       .invoke('text')
    //       .then(e => e.split(':')[1].replace(' ', ''))
    //   }).should('be.false')
    cy.dataCy('saveVoucher')
    cy.selectFromDropdown('voucher_type_tier_1', 'Animal tissue')
    cy.dataCy('done').eq(-1).click()
  })

  it('bulk should refuse uploaded duplicate field and mint-identifier should call only once on re-submission', () => {
    cy.nextStep()
    cy.dataCy('workflowNextButton').click()

    // look for every call to /mint-identifier and count them so by the end only one should have happened
    cy.intercept('POST', /\/api\/org\/mint-identifier/, cy.spy().as('api-spy')).as('mintIdentifier')


    // get the number of surveys for opportune
    let surveyLength
    cy.doRequest('opportunistic-surveys', {
      failOnStatusCode: true,
      method: 'GET',
      args: {
        'use-default': true,
      },
    })
      .its('body.data')
      .then((surveys) => {
        surveyLength = surveys.length
      })
      .then(() => {
        // force the request fail by altering the request body to have a duplicate id
        // stop modifying on the second upload attempt. A normal user wouldn't be able to do this.
        let hasIntercepted = false
        cy.intercept('POST', /(\/api)((\/.*)(bulk|many))/, (req) => {
          if (!hasIntercepted) {
            hasIntercepted = true
            // alter the request to have a observation_id that we used in a previous submission 
            req.body.data.collections[0]["opportunistic-observation"][0].data.observation_id = uids[uids.length - 1]
          }
        }).as('postInterceptBody')

        
        // submit with the duplicate and it *should* fail
        cy.dataCy('workflowPublish').click({ force: true })
        cy.dataCy('workflowPublishConfirm').click()
        cy.dataCy('submitQueuedCollectionsBtn')
          .should('be.enabled')
          .click()
          .wait(100)
        cy.dataCy('submitQueuedCollectionsConfirm').click()

        cy.wait('@postInterceptBody')
          .then(res => {
            // should have failed with a duplicate observation_id
            expect(res.response.statusCode).to.not.be.eq(200)
          }).then(() => {
            cy.doRequest('opportunistic-surveys', {
              method: 'GET',
              failOnStatusCode: true,
              args: {
                'use-default': true,
              },
            })
              .its('body.data')
              .then((surveys) => {
                // no ghost surveys should have been uploaded
                expect(surveyLength).to.eq(surveys.length)
              })
          })

        // now we have a failed collection that we need to reupload. And since we already intercepted once, this next attempt will not modify anything (see the above intercept). 

        // we should have 1 mint-identifier call right now
        cy.get('@api-spy').its('callCount').should('equal', 1)
        
        // submit again
        cy.dataCy('submitQueuedCollectionsBtn')
          .should('be.enabled')
          .click()
          .wait(100)
        cy.dataCy('submitQueuedCollectionsConfirm').click()
        cy.wait('@postInterceptBody')
          .then(() => {
            // recount the mint-identifier calls. It should NOT have incremented
            cy.get('@api-spy').its('callCount').should('equal', 1)
          })
      })
  })
})

describe('Upload with invalid image', () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })
  it('.should() - complete survey step (start)', () => {
    cy.nextStep()
  })
  it('.should() - add image', () => {
    cy.dataCy('addPhoto').click().wait(500)
    cy.dataCy('takePhoto').click()
    cy.dataCy('comment_0').should('exist')
  })
  it('record rest of observation', () => {
    cy.recordLocation()
    cy.selectFromDropdown('taxa_type', 'Vascular plant')
    selectFromSpeciesList('VP')
    cy.dataCy('number_of_individuals').type(2)
    cy.dataCy('Exact').click()
    cy.addObservers('observers', ['Alice'])
    cy.dataCy('done').eq(-1).click().wait(500)

    cy.nextStep()
  })
  it('fail to publish', () => {
    cy.nextStep()

    //fully stub the request to fail
    //TODO would be nice to actually upload a file that is too large, but `selectFile()` Cypress action can't handle large files, so stick with this approach for now
    cy.intercept('POST', /(\/api)((\/.*)(upload))/, {
      statusCode: 413,
      statusMessage: 'Payload Too Large',
      body: {
        message: 'Payload Too Large'
      }
    })

    cy.dataCy('workflowPublish').click({ force: true })
    cy.dataCy('workflowPublishConfirm').click()
    cy.dataCy('submitQueuedCollectionsBtn')
      .should('be.enabled')
      .click()
      .wait(100)
    cy.dataCy('submitQueuedCollectionsConfirm').click()

    //TODO check all text in 'issue' column, rather than just part of it
    cy.dataCy('failedQueuedCollectionsSummaryTable').within(() => {
      cy.get(`:contains(Request failed with status code 413)`)
        .should('exist')
    })
  })
  it('publish', () => {
    //separate `it` block means we won't intercept/stub the `/upload` request
    cy.intercept('POST', /(\/api)((\/.*)(bulk|many))/).as('postInterceptBody')

    cy.dataCy('submitQueuedCollectionsBtn')
      .should('be.enabled')
      .click()
      .wait(100)
    cy.dataCy('submitQueuedCollectionsConfirm').click()

    //check the queue synced successfully (else will flow-on to next `describe`)
    cy.wait('@postInterceptBody')
      .then(res => {
        console.log('invalid image res.response:', JSON.stringify(res.response, null, 2))
        expect(res.response.statusCode).to.be.eq(200)
      })
  })
})

//TODO this is testing /status, but we need to test /collection (not sure if possible as it has the same fail-case as /status, but /status needs to pass; so we'd need to figure out how to delete the identifier after /status but before /collection)
describe('Handle when core cannot check the collection status', () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })
  it('record observation', () => {
    cy.nextStep()

    cy.recordLocation()
    cy.selectFromDropdown('taxa_type', 'Vascular plant')
    selectFromSpeciesList('VP')
    cy.dataCy('number_of_individuals').type(2)
    cy.dataCy('Exact').click()
    cy.addObservers('observers', ['Alice'])
    cy.dataCy('done').eq(-1).click().wait(500)

    cy.nextStep()
    cy.nextStep()
  })
  it('try submit invalid data so that we mint org identifier', () => {
    let hasIntercepted = false
    cy.intercept('POST', /\/api\/org\/mint-identifier/, cy.spy().as('api-spy')).as('mintIdentifier')
    cy.intercept('POST', /(\/api)((\/.*)(bulk|many))/, (req) => {
      if (!hasIntercepted) {
        hasIntercepted = true
        //remove a required value to cause validator error
        delete req.body.data.collections[0]["opportunistic-observation"][0].data.number_of_individuals
      }
    }).as('postInterceptBody')

    cy.dataCy('workflowPublish').click({ force: true })
    cy.dataCy('workflowPublishConfirm').click()
    cy.dataCy('submitQueuedCollectionsBtn')
      .should('be.enabled')
      .click()
      .wait(100)
    cy.dataCy('submitQueuedCollectionsConfirm').click()

    cy.wait('@postInterceptBody')
      .then(res => {
        console.log('postInterceptBody 1 res.response:', JSON.stringify(res.response, null, 2))
        // should have failed with a duplicate observation_id
        expect(res.response.statusCode).to.not.be.eq(200)
      })
    
    //first /mint-identifier call that we mutate the response
    cy.get('@api-spy').its('callCount').should('equal', 1)
  })
  it('retry upload with failure to due missing org identifier in org', () => {
    let hasInterceptedBulkPost = false
    cy.intercept('POST', /\/api\/org\/mint-identifier/, cy.spy().as('mint-identifier-spy')).as('mintIdentifier')
    cy.intercept('POST', /(\/api)((\/.*)(bulk|many))/, (req) => {
      //only want to intercept and break it once
      if (!hasInterceptedBulkPost) {
        hasInterceptedBulkPost = true
        //modify the `orgMintedIdentifier` to have a different UUID, so that when Core
        //hits Org's /status (when we retry the request later), it won't find the
        //identifier.
        //this is to simulate the identifier being deleted from org - we have no way of
        //doing it here as there's no exposed routes
        console.log('identifier before:',req.body.data.collections[0].orgMintedIdentifier)
        const decodedIdentifier = JSON.parse(Buffer.from(req.body.data.collections[0].orgMintedIdentifier, 'base64'))
        console.log('decodedIdentifier:', JSON.stringify(decodedIdentifier, null, 2))
        const newIdentifier = {...decodedIdentifier}
        newIdentifier.survey_metadata.orgMintedUUID = uuidv4()
        console.log('newIdentifier:', JSON.stringify(newIdentifier, null, 2))
        req.body.data.collections[0].orgMintedIdentifier = Buffer.from(
          JSON.stringify(newIdentifier)
        ).toString('base64')
        console.log('identifier after:',req.body.data.collections[0].orgMintedIdentifier)
      }
    }).as('postInterceptBody')

    //we're no longer intercepting the POST /bulk, so data should be valid
    cy.dataCy('submitQueuedCollectionsBtn')
      .should('be.enabled')
      .click()
      .wait(100)
    cy.dataCy('submitQueuedCollectionsConfirm').click()

    //second /mint-identifier call that is the retry
    cy.get('@mint-identifier-spy').its('callCount').should('equal', 1)

    //check the queue synced successfully (enough to just wait for the request as it's difficult to check response is 200 since there will be two requests)
    cy.wait('@postInterceptBody')
      .then(res => {
        console.log('retry upload with failure to due missing org identifier in org res.response:', JSON.stringify(res.response, null, 2))
        cy.get('.q-notification', { timeout: 360000 })
          .should('contain.text', 'Successfully submitted all queued collections')
      })
    cy.wait(10000)
  })
})

describe('Prevent collecting while syncing', () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })
  it('.should() - complete survey step (start)', () => {
    cy.nextStep()
  })
  it('record observation', () => {
    cy.recordLocation()
    cy.selectFromDropdown('taxa_type', 'Vascular plant')
    selectFromSpeciesList('VP')
    cy.dataCy('number_of_individuals').type(2)
    cy.dataCy('Exact').click()
    cy.addObservers('observers', ['Alice'])
    cy.dataCy('done').eq(-1).click().wait(500)

    cy.nextStep()
  })
  it('publish', () => {
    cy.nextStep()

    cy.dataCy('workflowPublish').click({ force: true })
    cy.dataCy('workflowPublishConfirm').click()
    cy.dataCy('submitQueuedCollectionsBtn')
      .should('be.enabled')
      .click()
      .wait(100)
    
    //slow the queue down so we have time to check blocking entering workflow
    cy.setNetworkConditions({
      latency: 10000,
      downloadThroughput: 100,
      uploadThroughput: 100,
    }).then(() => {
      cy.dataCy('submitQueuedCollectionsConfirm').click()

      //while syncing try to access a protocol and check we're rejected
      cy.get(`[data-cy="${capitalizeWords(module)}"]`)
        .should('be.enabled')
        .click({ timeout: 20000 })
      cy.get('.q-notification', { timeout: 360000 })
        .should('exist')
        .and('contain.text', 'Cannot collect data for Opportune while syncing queue')
        .then(() => {
          //reset network conditions so we can finish submission
          cy.setNetworkConditions({
            latency: -1,
            downloadThroughput: -1,
            uploadThroughput: -1,
          })
            .then(() => {
              cy.get('[data-cy="submissionProgressWrapper"]', { timeout: 20000 })
                .should('not.exist')
                .then(() => {
                  //now check we can access protocol
                  cy.selectProtocolIsEnabled(module)
                })
            })
        })
    })
  })
})

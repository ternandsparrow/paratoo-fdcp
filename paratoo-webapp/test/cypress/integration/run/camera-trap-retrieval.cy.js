const { pub } = require('../../support/command-utils')

// Camera Trap Retrieval
const protocol = 'Camera Trap Retrieval'
const protocolCy = 'Camera\\ Trap\\ Retrieval'
const projectCy = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'camera\\ trapping'

function startRetrievingCameras() {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })

  it('choose survey', () => {
    cy.testEmptyField(
      'Field: "Survey Label": This field is required',
      'workflowNextButton',
    )
    cy.dataCy('survey_label').selectFromDropdown(
      deploymentSurveyToUse?.survey_label,
    )
    cy.nextStep()
  })
}

function finishRetrievingCameras() {
  it('complete survey (end)', () => {
    cy.nextStep()
  })
  pub()
}

before(() => {
  cy.mockLocation(-23.7, 132.8)
})

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
})

let deploymentSurveyToUse = null
const camerasToRetrieve = []
describe('Get cameras created in deployment', { retries: 3 }, () => {
  it('get deployment survey', () => {
    cy.loadLocalStore('apiModels', (apiModels) => {
      cy.wrap(apiModels.models?.['camera-trap-deployment-survey'])
        .should('not.be.undefined')
        .and('not.be.null')
        .then((deploymentSurveys) => {
          //using `findLast`, as the first deployment only has one point, but we need 3
          //FIXME better to do this dynamically by checking that the survey has 3 points
          deploymentSurveyToUse = deploymentSurveys.findLast((o) =>
            o.survey_label.includes('Camera Trap Cypress'),
          )
        })
    })
  })

  it('get deployment points', () => {
    cy.loadLocalStore('apiModels', (apiModels) => {
      cy.wrap(apiModels.models?.['camera-trap-deployment-point'])
        .should('not.be.undefined')
        .and('not.be.null')
        .then((deploymentPoints) => {
          cy.wrap(deploymentPoints).each((deploymentPoint) => {
            if (camerasToRetrieve.length > 3) return

            if (
              deploymentPoint?.camera_trap_survey?.id &&
              deploymentPoint.camera_trap_survey?.id ===
                deploymentSurveyToUse?.id
            ) {
              camerasToRetrieve.push(deploymentPoint)
            }
          })
        })
    })
  })
})

describe('Do retrieval', () => {
  //need to use indexed loop as when Cypress sets up the tests, it iterates over this
  //loop to create the `it`s - if we iterate over `camerasToRetrieve`, it won't be
  //defined at setup time
  for (let index = 0; index < 3; index++) {
    switch (index) {
      case 0:
        startRetrievingCameras()
        it('point 1, missing/theft', () => {
          cy.cameraTrapRetrievalPoint1(camerasToRetrieve[index])
        })
        finishRetrievingCameras()
        break
      case 1:
        startRetrievingCameras()
        it('point 2, unknown failure', () => {
          cy.cameraTrapRetrievalPoint2(camerasToRetrieve[index], camerasToRetrieve[0])
        })
        break
      case 2:
        it('point 3, Operational', () => {
          cy.cameraTrapRetrievalPoint3(camerasToRetrieve[index])
        })
        finishRetrievingCameras()
        break
    }
  }
  it('survey with all camera retrieved should be disabled in retrieving protocol', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
    cy.dataCy('survey_label')
      .click()
      .then(() => {
        cy.withinSelectMenu({
          persistent: true,
          fn: () => {
            cy.get('.q-item[role=option]').as('dropdownOptions')
            cy.get('@dropdownOptions')
              .contains(
                `${deploymentSurveyToUse?.survey_label} (All Cameras Retrieved)`,
              )
              .parents('.q-item')
              .should('have.attr', 'aria-disabled', 'true')
          },
        })
      })
    cy.get('[data-cy="workflowBack"] > .q-btn__content > .block').click()
  })

  it('survey with all camera retrieved should be disabled in reequipping protocol', () => {
    cy.selectProtocolIsEnabled(module, 'Camera Trap Reequipping')
    cy.dataCy('survey_label')
      .click()
      .then(() => {
        cy.withinSelectMenu({
          persistent: true,
          fn: () => {
            cy.get('.q-item[role=option]').as('dropdownOptions')
            cy.get('@dropdownOptions')
              .contains(
                `${deploymentSurveyToUse?.survey_label} (All Cameras Retrieved)`,
              )
              .parents('.q-item')
              .should('have.attr', 'aria-disabled', 'true')
          },
        })
      })
  })
})

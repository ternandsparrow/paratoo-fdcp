const { pub } = require("../../support/command-utils")

const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'soils'
const protocol = 'Soil Sub-pit and Metagenomics'

describe('landing', () => {
  it('login', () => {
    cy.login('TestUser', 'password')
  })

  it('navigation to ' + protocol + ' is disabled', () => {
    cy.selectProtocolIsDisabled(module, protocol)
  })

  it('plot layout', () => {
    cy.plotLayout()
  })

  it('navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})

describe('do field ' + protocol.toLowerCase(), () => {
  describe('survey set-up', () => {
    it('select field survey', () => {
      cy.dataCy('Field').click()
    })
    it('observers', () => {
      cy.addObservers('observerName', [
        'Tom',
        'Luke',
        'Walid'
      ])
    })
    it('metagenomics check to true', () => {
      cy.dataCy('collect_metagenomics').click()   //set true
    })

    it('select Enhanced variant', () => {
      cy.setCheck('Enhanced', true)
      cy.nextStep()
    })
  })
  describe('soil sub pit', () => {
    for (let i = 1; i <= 9; i++) {
      // we like to get a little silly and start at 1
      it(`pit ${i} body`, () => {
        cy.dataCy('soil_sub_pit_id', {log: false}).should('have.value', 'QDASEQ0001-' + i)
        cy.selectFromDropdown('collection_method', 'Differential GNSS')
        cy.recordLocation()
      })

      // ===================================== microhabitat ==========================
      for (let j = 1; j <= 2; j++) {
        it(`pit ${i} microhabitat ${j}`, () => {
          cy.setExpansionItemState(
            'microhabitat_*_' + j + '_expansion',
            'open',
          ).within(() => {
            cy.addImages('addImg')
            cy.dataCy('microhabitat_description', {log: false}).type(
              'Test comments for expansion ' + j, {log: false}
            )
            cy.dataCy('done', {log: false}).click({log: false})
          })
        })
        if (j < 2) {
          it(
            `pit ${i} microhabitat ${j} finished, adding entry`,
            { retries: 2 },
            () => {
              cy.dataCy('addEntry', {log: false})
                .eq(0, {log: false})
                .click({log: false})
                .wait(50, {log: false})
                .then(() => {
                  cy.get('[data-cy*="microhabitat_"]', {log: false}).should(
                    'have.length.greaterThan',
                    j,
                  )
                })
            },
          )
        }
      }

      //   ========================= observation ==========================
      for (let o = 1; o <= 2; o++) {
        it(`pit observation ${o}`, () => {
          cy.setExpansionItemState(
            `soil_sub_pit_observation_${o}_`,
            'open',
          ).within((e) => {
            cy.selectFromDropdown('observation_type', 'Soil pit')
            cy.dataCy('soil_pit_depth').type('5')
            cy.selectFromDropdown('digging_stopped_by', 'Rock')
            cy.addImages('addImg')
            cy.dataCy('comments',  {log: false}).type('Test comments')
            cy.dataCy('done',  {log: false}).click().wait(100)
          })
        })

        if (o < 3) {
          it(`ob ${i} finished, adding entry`, { retries: 2 }, () => {
            const index = i < 9 ? -2 : -1
            cy.dataCy('addEntry', {log: false})
              .eq(index, {log: false})
              .click({log: false})
              .wait(50, {log: false})
              .then(() => {
                cy.get('[data-cy*="expansion"]', {log: false}).should(
                  'have.length.greaterThan',
                  i,
                )
              })
          })
        }
      }

      it(`pit ${i} finished, adding entry`, { retries: 2 }, () => {
        cy.dataCy('done').eq(-1).click()
        if (i < 9) {
          cy.dataCy('addEntry',  {log: false})
            .eq(-1, {log: false})
            .click({log: false})
            .wait(50, {log: false})
            .then(() => {
              cy.get('[data-cy^="expansion-"]', {log: false}).should(
                'have.length.greaterThan',
                i,
              )
            })
        }
      })
    }
  })
})

describe('publish field survey', () => {
  it('publish', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})


// FIXME cypress crashes around the 3rd loop iteration in the pipeline. There are logs in the shared cache that can show the problem. This usually runs in job 18, unless more tests were added
// describe('lab', () => {
//   it('navigation to ' + protocol, () => {
//     cy.selectProtocolIsEnabled(module, protocol)
//   })
//   it('survey', () => {
//     cy.dataCy('Lab').click()

//     cy.dataCy('field_survey_id').selectFromDropdown()
//     cy.nextStep()
//   })

//   for (let i = 1; i <= 9; i++) {
//     it(`sampling ${i} body`, () => {
//       cy.selectFromDropdown('soil_sub_pit', 'QDASEQ0001-' + i)
//     })

//     it('collect data for Metagenomics', () => {
//       cy.dataCy('metagenomics_sample')
//         .should('exist')
//         .within(() => {
//           cy.dataCy('recordBarcode').click()
//         })
//     })
//     it('collect data for 0.00 - 0.10m', () => {
//       cy.setExpansionItemState('0.00m - 0.10m', 'open').within(() => {
//         cy.dataCy('not_collected').click()
//         cy.dataCy('done').click()
//       })
//     })
//     it('collect data for 0.10m - 0.20m', () => {
//       cy.setExpansionItemState('0.10m - 0.20m', 'open').within(() => {
//         cy.dataCy('not_collected').click()
//         cy.dataCy('done').click()
//       })
//     })
//     it('collect data for 0.20m - 0.30m', () => {
//       cy.setExpansionItemState('0.20m - 0.30m', 'open').within(() => {
//         cy.dataCy('recordBarcode').click()
//         cy.dataCy('done').click()
//       })
//     })

//     it(`ob ${i} finished, adding entry`, { retries: 2 }, () => {
//       cy.dataCy('done').eq(-1).click()
//       if (i < 9) {
//         cy.dataCy('addEntry')
//           .eq(-1)
//           .click()
//           .wait(1000)
//           .then(() => {
//             cy.get('[data-cy*="expansion"]').should(
//               'have.length.greaterThan',
//               i,
//             )
//           })
//       }
//     })
//   }
//   it('publish', () => {
//     cy.nextStep()
//     cy.workflowPublish()
//   })
// })

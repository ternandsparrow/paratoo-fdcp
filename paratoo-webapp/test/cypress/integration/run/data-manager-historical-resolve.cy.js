/**
 * Checks that in-progress data has temp relations resolved when those relations were
 * just synced from the queue
 */

import { createVertCollectionMetadata } from '../../support/protocol-commands/vert-commands'
import { vertebrateTrapCheckMeasurement as measurementFields } from '../../support/vertConditionalFields'

const selectionAndLayoutModule = 'Plot Selection and Layout'
const layoutVisitProtocol = 'Plot Layout and Visit'
const cameraTrapModule = 'camera trapping'
const cameraTrapDeploymentProtocol = 'Camera Trap Deployment'
const floristicsModule = 'Floristics'
const floristicsLiteProtocol = 'Floristics - Standard'
const floristicsFullProtocol = 'Floristics - Enhanced'
const ptvModule = 'Plant Tissue Vouchering'
const ptvFullProtocol = 'Plant Tissue Vouchering - Enhanced'
const ptvLiteProtocol = 'Plant Tissue Vouchering - Standard'
const basalWedgeProtocol = 'Basal Area - Basal Wedge'
const basalDbhProtocol = 'Basal Area - DBH'
const basalModule = 'basal area'
const vertModule = 'Vertebrate Fauna'
const vertTrapSetupProtocol = 'Vertebrate Fauna - Trapping Survey Setup'
const vertTrapCheckProtocol = 'Vertebrate Fauna - Identify, Measure and Release'
const vertTrapClosureProtocol = 'Vertebrate Fauna - Trapping Survey Closure'
const soilModule = 'soils'
const protocolCharacterisation = 'Soil Site Observation and Pit Characterisation'
const protocolSamplePit = 'Soil Sample Pit'
const protocolBulkDensity = 'Soils - Bulk Density'

const dataToCollect = {
  floristicsFull: [
    {
      fieldName: `test full voucher ${Math.floor(Math.random() * 10000)}`,
    },
    {
      fieldName: `test full voucher 2 ${Math.floor(Math.random() * 10000)}`,
    },
  ],
  floristicsLite: [
    {
      fieldName: `test lite voucher 1 ${Math.floor(Math.random() * 10000)}`,
    },
    {
      fieldName: `test lite voucher 2 ${Math.floor(Math.random() * 10000)}`,
    },
  ],
  cameraTrapDeployment: [
    {
      faunaPlot: 'SATRAFLB1002',
    },
  ],
}

const { trapNumbers, vertTrapLines } = createVertCollectionMetadata()

const speciesClass = {
  mammal: 'Mammal',
  amphibian: 'Amphibian',
  reptile: 'Reptile',
}

const speciesLut = {
  mammal: 'MA',
  amphibian: 'AM',
  reptile: 'RE',
}

const dataToCollectUse = {
  ptvFull: [
    {
      voucherToUse: dataToCollect.floristicsFull[0].fieldName,
    },
  ],
  ptvLite: [
    {
      voucherToUse: dataToCollect.floristicsLite[0].fieldName,
    },
  ],
  basalDbh: [
    {
      voucherToUse: dataToCollect.floristicsFull[1].fieldName,
    },
    {
      voucherToUse: dataToCollect.floristicsLite[1].fieldName,
    },
  ],
  basalWedge: [
    {
      voucherToUse: dataToCollect.floristicsFull[0].fieldName,
      sweepingPoint: 'Northwest',
    },
    {
      voucherToUse: dataToCollect.floristicsFull[1].fieldName,
      sweepingPoint: 'North',
    },
    {
      voucherToUse: 'Voucher Full Example 1',
      sweepingPoint: 'Northeast',
    },
    {
      voucherToUse: 'Voucher Full Example 2',
      sweepingPoint: 'East',
    },
    {
      voucherToUse: 'Voucher Full Example 3',
      sweepingPoint: 'Southeast',
    },
    {
      voucherToUse: 'Voucher Full Example 4',
      sweepingPoint: 'South',
    },
    {
      voucherToUse: 'Voucher Full Example 5',
      sweepingPoint: 'Southwest',
    },
    {
      voucherToUse: 'Voucher Full Example 6',
      sweepingPoint: 'West',
    },
    {
      voucherToUse: 'Voucher Full Example 7',
      sweepingPoint: 'Centre',
    },
  ],
}

console.log('dataToCollect:', dataToCollect)
console.log('dataToCollectUse:', dataToCollectUse)

function resetState() {
  cy.window()
    .then(($win) => {
      $win.dataManager.$reset()
      $win.bulkStore.$reset()
    })
}

function exitProtocol() {
  cy.dataCy('workflowBack').click().wait(1000)

  //can take a moment to load, so just wait for this button on the projects page
  cy.dataCy('prepareForOfflineVisitBtn', { timeout: 30000 })
    .should('exist', { timeout: 30000 })
}

describe('Landing', () => {
  it('login', () => {
    cy.login('TestUser', 'password')
  })
  it('reset state', () => {
    resetState()
  })
  it('set plot context - new plot/visit in queue', () => {
    //this will submit on first queue sync, as any in-progress collections that use this
    //plot data will resolve the plot after the sync
    cy.selectProtocolIsEnabled(selectionAndLayoutModule)
    cy.dataCy('locationNew').click()
    cy.dataCy('plotAlignedToGridYes').click()
    cy.selectFromDropdown('plot_selection', 'SATRAFLB1002', null)
    cy.selectFromDropdown('plot_type', 'Control', null)
    cy.dataCy('replicate').type(1)
    //use manual reference point so we don't have to manager spoofing location
    cy.dataCy('plot_not_walked').check()
    cy.get('[data-autofocus="true"] > .q-btn__content').click()
    cy.dataCy('choosePosition').click()
    cy.dataCy('manualCoords', { timeout: 5000 }).click().wait(500)
    //near the plot selection's recommended location
    cy.dataCy('Latitude', { timeout: 5000 }).type(
      `{backspace}-34.97315`,
    )
    cy.dataCy('Longitude', { timeout: 5000 }).type(
      `{backspace}138.6392`,
    )
    cy.wait(1000)
    cy.dataCy('recordLocation').click()
    cy.dataCy('startLayout').click().wait(500)
    cy.dataCy('setStartPoint').click().wait(1000)
    cy.dataCy('markClosestPoint').click().wait(1000) //'done' - closes modal
    cy.nextStep()   //moves to fauna plot step

    cy.dataCy('faunaPlotYes').click()
    cy.get('[data-cy="fauna_plot_not_walked"]').check()
    cy.get('[data-autofocus="true"] > .q-btn__content').click()
    cy.selectFromDropdown('startingPoint', 'North West')
    cy.dataCy('startLayout').click().wait(1000)
    cy.dataCy('setStartPoint').click().wait(1000)
    cy.dataCy('markClosestPoint').should('have.text', 'Done').click() //close modal
    cy.nextStep()

    cy.dataCy('visitNew').click()
    cy.dataCy('visit_field_name').type(`test visit ${Math.floor(Math.random() * 100000)}`)

    cy.workflowPublishQueue()
  })
})

let soilPitId   //for characterisation to save for bulk density 1
let soilPitLiteId   //for sample put to save for bulk density 2
describe('Setup depends-on data', () => {
  for (const floristicsType of [floristicsFullProtocol, floristicsLiteProtocol]) {
    let thisDataToCollect = null
    let doPtvSubmodule = false
    if (floristicsType === floristicsFullProtocol) {
      thisDataToCollect = dataToCollect.floristicsFull
      doPtvSubmodule = true
    } else if (floristicsType === floristicsLiteProtocol) {
      thisDataToCollect = dataToCollect.floristicsLite
    }
    it(`do ${floristicsType}${doPtvSubmodule ? ' with ' + ptvFullProtocol + ' submodule' : ''}`, () => {
      cy.selectProtocolIsEnabled(floristicsModule, floristicsType)

      if (doPtvSubmodule) {
        cy.dataCy('collect_ptv_submodule').check()
        cy.dataCy('Enhanced').click()
      }
      cy.nextStep()

      for (const [index, voucher] of thisDataToCollect.entries()) {
        cy.selectFromSpeciesList('field_name', voucher.fieldName, true)
        cy.dataCy('recordBarcode').eq(-1).click().wait(200)
        cy.selectFromDropdown('growth_form_1', null)

        if (doPtvSubmodule) {
          cy.dataCy('openPtvModal').click().wait(100)
          cy.dataCy('recordBarcode').eq(1).click().wait(200)
          cy.dataCy('done').eq(2).click()
          cy.get('.q-notification', { timeout: 10000 }).should(
            'contain.text',
            'Please submit the floristics voucher to save changes associated with plant tissue vouchers',
          )
          cy.dataCy('closePtvModal').click().wait(100)
        }
  
        cy.dataCy('done').eq(-1).click().wait(500)
        if (index < (thisDataToCollect.length - 1)) {
          cy.dataCy('addEntry').eq(-1).click().wait(500)
        }
      }
  
      cy.nextStep()
      cy.workflowPublishQueue()
    })
  }

  //need to split up all these soils commands as it's a lot to do in just one `it` block,
  //which can cause memory issues
  it(`do ${protocolCharacterisation} - select protocol`, () => {
    cy.selectProtocolIsEnabled(soilModule, protocolCharacterisation)
  })
  it(`do ${protocolCharacterisation} - pit location`, () => {
    cy.soilCharacterisationPitLocation()
      .then((soilPitId_) => {
        cy.log(`soilPitId_=${soilPitId_}`)
        soilPitId = soilPitId_
      })
  })
  it(`do ${protocolCharacterisation} - land form element`, () => {
    cy.soilCharacterisationLandformElements()
  })
  it(`do ${protocolCharacterisation} - land surface phenomena & soil development`, () => {
    cy.soilCharacterisationSurfacePhenomenaSoilDevelopment()
  })
  it(`do ${protocolCharacterisation} - microrelief`, () => {
    cy.soilCharacterisationMicrorelief()
  })
  it(`do ${protocolCharacterisation} - erosion observation`, () => {
    cy.soilCharacterisationErosionObservation()
  })
  it(`do ${protocolCharacterisation} - surface coarse fragment observation`, () => {
    cy.soilCharacterisationSurfaceCoarseFragmentObservations()
  })
  it(`do ${protocolCharacterisation} - rock outcrop observation`, () => {
    cy.soilCharacterisationRockOutcropObservation()
  })
  it(`do ${protocolCharacterisation} - soil pit observation`, () => {
    cy.soilCharacterisationSoilPitObservation()
  })
  it(`do ${protocolCharacterisation} - soil horizon observation`, () => {
    cy.soilCharacterisationSoilHorizonObservation()
  })
  it(`do ${protocolCharacterisation} - ASC`, () => {
    cy.soilCharacterisationAustralianSoilClassification()
  })
  it(`do ${protocolCharacterisation} - soil classification`, () => {
    cy.soilCharacterisationSoilClassification()
  })
  it(`do ${protocolCharacterisation} - finish`, () => {
    cy.nextStep()
    cy.workflowPublishQueue({
      afterQueueCollectionCb: () => {
        //don't redirect to bulk density
        cy.get('[data-autofocus="true"] > .q-btn__content').click()
        cy.dataCy('title')
          .eq(0)
          .should('contain.text', 'Soil Bulk Density Survey')
          .then(() => {
            // cy.visit('/projects')
            cy.get('.q-btn').contains('back').eq(0).click()
          })
      },
    })
  })

  it(`do ${vertTrapSetupProtocol} - select protocol`, () => {
    cy.selectProtocolIsEnabled(vertModule, vertTrapSetupProtocol)
    cy.nextStep()
  })
  for (const [trapLine, trapData] of Object.entries(vertTrapLines)) {
    it(`do ${vertTrapSetupProtocol} - collect ${trapData.numOfTraps} traps for trap line '${trapLine}'`, () => {
      cy.setExpansionItemState(`${trapLine}_expansion`, 'open')
      cy.vertStartTrapLine(trapData.isFence)
      cy.vertMarkTrap(trapData.numOfTraps)
      cy.vertEndTrapLine(trapLine)
    })
  }
  it(`do ${vertTrapSetupProtocol} - finish`, () => {
    cy.nextStep()
    cy.workflowPublishQueue()
  })
})

describe('Add dependent data', () => {
  it(`${cameraTrapDeploymentProtocol} (fauna plot type) depends on ${layoutVisitProtocol}`, () => {
    cy.selectProtocolIsEnabled(cameraTrapModule, cameraTrapDeploymentProtocol)

    cy.cameraTrapDeploymentSurvey(null, 'Fauna Plot')
    cy.nextStep()

    cy.dataCy('camera_trap_point_id').type(
      `CTP${String(Math.floor(Math.random() * 10000))}`
    )
    cy.customSelectDate({
      fieldName: 'deployment_start_date',
      date: {
        year: 'current',
        month: 'current',
        day: 'current',
      },
    })
    cy.dataCy('fauna_plot').selectFromDropdown(dataToCollect.cameraTrapDeployment[0].faunaPlot)

    cy.dataCy('taxa_type').selectFromDropdown()
    cy.dataCy('done').eq(0).click().wait(500)

    cy.dataCy('species').selectFromSpeciesList('quokka', false)
    cy.dataCy('done').eq(0).click().wait(500)
    
    cy.dataCy('camera_trap_mount').selectFromDropdown()

    cy.dataCy('feature').selectFromDropdown('Road - sealed')
    cy.dataCy('distance_to_feature').type(1)
    cy.dataCy('addImg').eq(0).click().wait(500)
    cy.dataCy('takePhoto').click().wait(500)
    cy.dataCy('done').eq(0).click().wait(500)

    cy.dataCy('camera_trap_number').type(1)
    cy.dataCy('SD_card_number').type(1)

    cy.dataCy('make').selectFromDropdown()
    cy.dataCy('model').selectFromDropdown()
    cy.dataCy('manufacture_year').type(2000)
    cy.dataCy('illumination').selectFromDropdown()
    cy.dataCy('activation_mechanism').selectFromDropdown()
    cy.dataCy('trigger_speed').type(1)

    cy.dataCy('media_type').selectFromDropdown('Video')
    cy.dataCy('sensor_sensitivity').selectFromDropdown()
    cy.dataCy('quiet_period').type('00:30')
    cy.dataCy('aspect_ratio').selectFromDropdown()
    cy.dataCy('day_night_recording').selectFromDropdown()
    cy.dataCy('time_format').selectFromDropdown()
    cy.selectDateTime({
      fieldName: 'datetime',
      date: {
        year: 'current',
        month: 'current',
        day: 28,
      },
      time: {
        hour: 14,
        minute: 'current',
      },
    })
    cy.dataCy('unit_of_temperature').selectFromDropdown()
    cy.dataCy('battery_type').selectFromDropdown()

    cy.dataCy('camera_trap_height').type(0)
    cy.dataCy('camera_trap_angle').type(0)
    cy.dataCy('camera_trap_direction').type(0)
    
    cy.dataCy('addImg').click().wait(100)
    cy.dataCy('takePhoto').click().wait(100)
    
    cy.dataCy('done').eq(-1).click().wait(1000)
    cy.nextStep()

    exitProtocol()
  })

  for (const ptvType of [ptvFullProtocol, ptvLiteProtocol]) {
    let thisDataToCollect = null
    if (ptvType === ptvFullProtocol) {
      thisDataToCollect = dataToCollectUse.ptvFull
    } else if (ptvType === ptvLiteProtocol) {
      thisDataToCollect = dataToCollectUse.ptvLite
    }
    it(`${ptvType} depends on ${floristicsFullProtocol} - start but don't queue`, () => {
      cy.log(`thisDataToCollect: ${JSON.stringify(thisDataToCollect)}`)
      cy.selectProtocolIsEnabled(ptvModule, ptvType)
      cy.nextStep()   //survey step
      for (const [index, voucher] of thisDataToCollect.entries()) {
        cy.log(`selecting voucher '${voucher.voucherToUse}'`)
        cy.selectFromDropdown('floristics_voucher', voucher.voucherToUse)
        cy.dataCy('recordBarcode').eq(-1).click().wait(200)
  
        cy.dataCy('done').eq(-1).click().wait(500)
        if (index < (thisDataToCollect.length - 1)) {
          cy.dataCy('addEntry').eq(-1).click().wait(500)
        }
      }

      exitProtocol()
    })
  }

  it(`${basalDbhProtocol} depends on both ${floristicsFullProtocol} and ${floristicsLiteProtocol} - start but don't queue`, () => {
    cy.selectProtocolIsEnabled(basalModule, basalDbhProtocol)

    cy.get('[data-cy="100 x 100 m"]').click().wait(100)
    cy.selectFromDropdown('basal_dbh_instrument', 'Diameter Tape Measure')
    cy.nextStep()

    for (const [index, voucher] of dataToCollectUse.basalDbh.entries()) {
      cy.recordLocation()
      cy.log(`selecting voucher '${voucher.voucherToUse}'`)
      cy.selectFromDropdown('floristics_voucher', voucher.voucherToUse)
      cy.dataCy('DBH').type(5)

      cy.dataCy('done').eq(-1).click().wait(500)
      if (index < (dataToCollectUse.basalDbh.length - 1)) {
        cy.dataCy('addEntry').eq(-1).click().wait(500)
      }
    }

    exitProtocol()
  })

  it(`${basalWedgeProtocol} depends on ${floristicsFullProtocol}`, () => {
    cy.selectProtocolIsEnabled(basalModule, basalWedgeProtocol)

    cy.nextStep()

    for (const [index, voucher] of dataToCollectUse.basalWedge.entries()) {
      cy.log(`selecting sweeping point: ${voucher.sweepingPoint}`)
      cy.selectFromDropdown(
        'basal_sweep_sampling_point',
        //some points are similar and will get accidentally matched unless we use a regex
        //that matches whole words
        `^${voucher.sweepingPoint}$`,
        null,
        null,
        false,
        //regex
        true,
      )

      cy.log(`selecting voucher '${voucher.voucherToUse}'`)
      cy.selectFromDropdown('floristics_voucher', voucher.voucherToUse)

      cy.selectFromDropdown('basal_area_factor', '0.1')
      cy.dataCy('in_tree').type('7')
      cy.dataCy('borderline_tree').type('4')

      cy.dataCy('done').eq(-1).click().wait(500)
        if (index < (dataToCollectUse.basalWedge.length - 1)) {
          cy.dataCy('addEntry').eq(-1).click().wait(500)
        }
    }

    cy.nextStep()

    exitProtocol()
  })

  it(`${protocolBulkDensity} depends on ${protocolCharacterisation}`, () => {
    cy.selectProtocolIsEnabled(soilModule, protocolBulkDensity)
    cy.wrap(soilPitId)
      .should('not.be.null')
      .and('not.be.undefined')
    cy.soilBulkDensitySurvey(soilPitId)
    cy.nextStep()
    exitProtocol()
  })

  it(`${vertTrapCheckProtocol} depends on ${vertTrapSetupProtocol}`, () => {
    cy.selectProtocolIsEnabled(vertModule, vertTrapCheckProtocol)
    cy.selectFromDropdown('trap_check_interval', 'Morning')
    cy.addObservers('trap_checked_by', ['John', 'Jane', 'Mary'])
    cy.nextStep()

    cy.vertCheckTrap('P1', 'Capture')
    cy.vertGeneralInfoOfTrapWithCapture(
      'P1',
      speciesClass.mammal,
      'Platypus',
    )
    cy.vertRecordMeasurements(measurementFields, speciesLut.mammal)
    cy.vertRecordReproductiveTraits('P1')
    cy.vertRecordSexAndAgeClass('P1')
    cy.vertRecordBodyAndSkinCondition('P1')
    cy.vertRecordClinicalScoring('P1')
    cy.dataCy('animal_fate').selectFromDropdown(null)
    cy.dataCy('done').eq(-2).click()
    cy.dataCy('done').eq(-1).click()

    cy.vertCheckTrap('F2', 'Capture')
    cy.vertGeneralInfoOfTrapWithCapture(
      'F2',
      speciesClass.reptile,
      'Crocodylus [Genus]',
    )
    cy.vertRecordMeasurements(measurementFields, speciesLut.reptile)
    cy.vertRecordSexAndAgeClass()
    cy.vertRecordBodyAndSkinCondition('F2')
    cy.vertRecordClinicalScoring('F2')
    cy.getAndValidate('animal_fate').selectFromDropdown(null)
    cy.dataCy('done').eq(-2).click()
    cy.dataCy('done').eq(-1).click()

    cy.nextStep()
    exitProtocol()
  })

  it(`${vertTrapClosureProtocol} depends on ${vertTrapSetupProtocol}`, () => {
    cy.selectProtocolIsEnabled(vertModule, vertTrapClosureProtocol)
    cy.nextStep()

    for (const trapNumber of trapNumbers) {
      cy.log(`trapNumber: ${trapNumber}`)
      if (trapNumber === 'P1') {
        cy.dataCy('trap_number').selectFromDropdown(new RegExp(`^${Cypress._.escapeRegExp(trapNumber)}$`), trapNumber, -1)
      }
      // only pitfall traps have 'Pitfall closed' status
      // which have a 'P' prefix
      if (/\bP\w*/.test(trapNumber)) {
        cy.selectFromDropdown('trap_status', 'Pitfall closed - filled in')
        cy.setCheck('toggleBarcodeReaderShow', false)
        cy.dataCy('addImg').click().wait(200)
        cy.dataCy('takePhoto').click().wait(1000)
      } else {
        cy.selectFromDropdown('trap_status', 'Trap closed - permanently')
      }


      cy.dataCy('done').click().wait(100)
    }
    cy.nextStep()

    cy.selectFromDropdown('drift_line', 'B')
    cy.dataCy('takePhoto').click()
    cy.selectFromDropdown('drift_line', 'E')
    cy.dataCy('takePhoto').click()

    cy.nextStep()
    exitProtocol()
  })
})

let queuedPlotLayout = null
let unresolvedInProgressData = null
let resolvedInProgressData = null
describe('Check in-progress dependencies are resolved', () => {
  it(`get queue reference of ${layoutVisitProtocol}`, () => {
    cy.loadLocalStore('dataManager')
      .then((dataManagerStore) => {
        const queuedLayoutIndex = dataManagerStore.publicationQueue.findIndex(
          q => q.protocolId === 4
        )
        return { dataManagerStore, queuedLayoutIndex }
      })
      .then(({ dataManagerStore, queuedLayoutIndex }) => {
        cy.wrap(queuedLayoutIndex).should('eq', 0)

        queuedPlotLayout = dataManagerStore.publicationQueue[queuedLayoutIndex]
      })
  })
  it('get in-progress data that hasn\'t been resolved', () => {
    cy.loadLocalStore('bulk')
      .then((bulkStore) => {
        unresolvedInProgressData = Cypress._.cloneDeep(bulkStore.collections)
      })
  })
  it('sync queue', () => {
    cy.syncAndCheckQueueTable({
      //nothing should skip/fail
      shouldHaveSyncErrors: [],
    })
  })
  
  it(`check ${layoutVisitProtocol} synced (not in queue)`, () => {
    cy.wait(3000)
    cy.dataCy('pulling-api-status', { timeout: 120000 })
      .should('not.exist', { timeout: 120000 })
    console.log(`checking layout synced: ${JSON.stringify(queuedPlotLayout)}`)
    cy.loadLocalStore('dataManager', (dataManagerStore) => {
      console.log('loaded queue:', JSON.stringify(dataManagerStore.publicationQueue))
      const queuedLayout = dataManagerStore.publicationQueue.find(
        q => q.queuedCollectionIdentifier === queuedPlotLayout
      )
      console.log('queuedLayout:', JSON.stringify(queuedLayout))
      //it's synced so shouldn't be in the queue
      cy.wrap(queuedLayout).should('be.undefined')
    })
  })

  it('get in-progress data that has been resolved', () => {
    cy.loadLocalStore('bulk')
      .then((bulkStore) => {
        resolvedInProgressData = Cypress._.cloneDeep(bulkStore.collections)
      })
  })

  it('check in-progress data had dependencies resolved when queue is synced', () => {
    console.log('unresolvedInProgressData:', JSON.stringify(unresolvedInProgressData))
    console.log('resolvedInProgressData:', JSON.stringify(resolvedInProgressData))

    cy.checkResolvedData({ unresolvedInProgressData, resolvedInProgressData })
  })
})

describe('Finish dependent protocols and sync without errors or skips', () => {
  it(`finish ${cameraTrapDeploymentProtocol}`, () => {
    cy.editProtocol({
      module: cameraTrapModule,
      buttonIndex: 0,
    })
    cy.wait(2000)
    cy.get('[data-cy="Camera\\ Trap\\ Deployment\\ Survey\\ (end)"]').click().wait(2000)
    cy.workflowPublishQueue()
  })
  it(`finish ${ptvFullProtocol}`, () => {
    cy.editProtocol({
      module: ptvModule,
      buttonIndex: 0,
    })
    cy.wait(2000)
    cy.get('[data-cy="Plant\\ Tissue\\ Vouchering\\ Survey\\ (end)"]').click().wait(2000)
    cy.workflowPublishQueue()
  })

  it(`finish ${ptvLiteProtocol}`, () => {
    cy.editProtocol({
      module: ptvModule,
      buttonIndex: 0,
    })
    cy.wait(2000)
    cy.get('[data-cy="Plant\\ Tissue\\ Vouchering\\ Survey\\ (end)"]').click().wait(2000)
    cy.workflowPublishQueue()
  })

  it(`finish ${basalDbhProtocol}`, () => {
    cy.editProtocol({
      module: basalModule,
      buttonIndex: 0,
    })
    cy.wait(2000)
    cy.get('[data-cy="Basal\\ Area\\ Dbh\\ Measure\\ Survey\\ (end)"]').click().wait(2000)
    cy.workflowPublishQueue()
  })

  it(`finish ${basalWedgeProtocol}`, () => {
    cy.editProtocol({
      module: basalModule,
      buttonIndex: 0,
    })
    cy.wait(2000)
    cy.get('[data-cy="Basal\\ Wedge\\ Survey\\ (end)"]').click().wait(2000)
    cy.workflowPublishQueue()
  })

  it(`finish ${vertTrapCheckProtocol}`, () => {
    cy.editProtocol({
      module: vertModule,
      buttonIndex: 0,
    })
    cy.wait(2000)
    cy.get('[data-cy="Trap\\ Check\\ Survey\\ (end)"]').click().wait(2000)
    cy.workflowPublishQueue()
  })

  it(`finish ${vertTrapClosureProtocol}`, () => {
    cy.editProtocol({
      module: vertModule,
      buttonIndex: 0,
    })
    cy.wait(2000)
    cy.get('[data-cy="Vertebrate\\ End\\ Trapping\\ Survey\\ (end)"]').click().wait(2000)
    cy.workflowPublishQueue()
  })

  it('sync queue', () => {
    cy.syncAndCheckQueueTable({
      //all should sync now
      shouldHaveSyncErrors: [],
    })
  })
})

//need to do these here as Bulk Density can depend on Characterisation OR Sample Pit -
//this case is for Sample Pit, whereas the above case is for Characterisation
describe('Setup depends-on data', () => {
  it(`do ${protocolSamplePit}  - select protocol and get pit ID reference`, () => {
    cy.selectProtocolIsEnabled(soilModule, protocolSamplePit)

    cy.dataCy('soil_pit_id')
      //append a modifying as Characterisation will auto-gen the same pit ID (it's the
      //plot and today's date, but no time)
      .type(`-${Math.floor(Math.random() * 1000)}`)
      .invoke('val')
      .then((id) => {
        cy.log(`soilPitLiteId to assign: ${id}`)
        soilPitLiteId = id
      })
  })
  it(`do ${protocolSamplePit} - rest`, () => {
    cy.addObservers('observerName', [
      'Bob',
    ])
    cy.dataCy('collection_method').selectFromDropdown('Differential GNSS')
    cy.recordLocation()
    cy.dataCy('observation_type').selectFromDropdown('Soil pit')
    cy.dataCy('soil_pit_depth').type('5')
    cy.dataCy('digging_stopped_by').selectFromDropdown('Rock')
    cy.addImages('addImg')
    cy.dataCy('comments').type('Test comment')
    cy.nextStep()
    cy.workflowPublishQueue()
  })
})

describe('Add dependent data', () => {
  it (`${protocolBulkDensity} depends on ${protocolSamplePit}`, () => {
    cy.selectProtocolIsEnabled(soilModule, protocolBulkDensity)
    cy.wrap(soilPitLiteId)
      .should('not.be.null')
      .and('not.be.undefined')
    cy.soilBulkDensitySurvey(soilPitLiteId)
    cy.nextStep()
    exitProtocol()
  })
})

describe('Check in-progress dependencies are resolved', () => {
  it('get in-progress data that hasn\'t been resolved', () => {
    cy.loadLocalStore('bulk')
      .then((bulkStore) => {
        unresolvedInProgressData = Cypress._.cloneDeep(bulkStore.collections)
      })
  })
  it('sync queue', () => {
    cy.syncAndCheckQueueTable({
      //nothing should skip/fail
      shouldHaveSyncErrors: [],
    })
  })
  it('get in-progress data that has been resolved', () => {
    cy.loadLocalStore('bulk')
      .then((bulkStore) => {
        resolvedInProgressData = Cypress._.cloneDeep(bulkStore.collections)
      })
  })

  it('check in-progress data had dependencies resolved when queue is synced', () => {
    console.log('unresolvedInProgressData:', JSON.stringify(unresolvedInProgressData))
    console.log('resolvedInProgressData:', JSON.stringify(resolvedInProgressData))

    cy.checkResolvedData({ unresolvedInProgressData, resolvedInProgressData })
  })
})

describe('Finish dependent protocols and sync without errors or skips', () => {
  it(`finish ${protocolBulkDensity}`, () => {
    cy.editProtocol({
      module: soilModule,
      buttonIndex: 0,
    })
    cy.get('[data-cy="Soil\\ Bulk\\ Density\\ Survey\\ (end)"]').click().wait(2000)
    cy.workflowPublishQueue()
  })

  it('sync queue', () => {
    cy.syncAndCheckQueueTable({
      //all should sync now
      shouldHaveSyncErrors: [],
    })
  })
})

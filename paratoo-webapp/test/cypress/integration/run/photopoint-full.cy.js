const { pub } = require('../../support/command-utils')

const protocolCy = 'Photopoints\\ -\\ DSLR\\ Panorama'
const protocol = 'Photopoints - DSLR Panorama'
const module = 'photopoints'

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocolCy)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout(true)
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
  it('.should() - fail to go to next step if 3 points are not collected', () => {
    cy.dataCy('centrePostPositionedYes').click()
    cy.recordLocation({
      btnEqIndex: 0,
    })
    cy.dataCy('nextPoint').should('not.exist')
    cy.testEmptyField('Require 3 photopoint positions', 'workflowNextButton')
  })
  it('.should() - collect photopoints full', () => {
    for (let i = 0; i < 3; i++) {
      cy.recordLocation({
        btnEqIndex: 1,
        recordBtnIndex: 1,
      })
      cy.dataCy('nextPoint').should('exist')
      cy.dataCy('qrcode').should('be.visible')
      if (i < 2) {
        cy.get('[data-cy="nextPoint"]').click()
        cy.dataCy('qrcode').should('not.exist')
      }
    }
    cy.nextStep()
  })
  pub()
})

describe('Do again but with a new layout in the queue', () => {
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsEnabled('Plot Selection and Layout')
  })
  it('.should() - collect new Layout (QDASEQ0005) and Fauna Plot, and new Visit', () => {
    cy.newLayoutAndVisit(
      'QDASEQ0005',
      {
        lat: -35.0004723472134,
        lng: 138.66067886352542,
      },
      true,
      'test visit',
    )
    cy.nextStep()
    cy.dataCy('workflowPublish')
      .should('have.text', 'Queue Collection for Submission')
      .click()
    cy.dataCy('workflowPublishConfirm').click()
  })

  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    // cy.get('[data-cy="changeSelection"]').click()
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
  it('.should() - fail to go to next step if 3 points are not collected', () => {
    cy.dataCy('centrePostPositionedYes').click()
    cy.dataCy('nextPoint').should('not.exist')
    cy.recordLocation({
      btnEqIndex: 0,
    })
    // cy.dataCy('nextPoint').should('exist')
    cy.testEmptyField('Require 3 photopoint positions', 'workflowNextButton')
  })

  it('.should() - collect photopoints full', () => {
    for (let i = 0; i < 3; i++) {
      cy.recordLocation({
        btnEqIndex: 1,
        recordBtnIndex: 1,
      })
      cy.dataCy('qrcode').should('be.visible')
      if (i < 2) {
        cy.get('[data-cy="nextPoint"]').click()
        cy.dataCy('qrcode').should('not.exist')
      }
    }
    cy.nextStep()
  })
  pub()
})

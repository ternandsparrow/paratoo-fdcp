const { pub } = require("../../support/command-utils")

const protocol = 'Camera Trap Reequipping'
const protocolCy = 'Camera\\ Trap\\ Reequipping'
const projectCy = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'camera\\ trapping'

before(() => {
  cy.mockLocation(-23.7, 132.8)
})

// afterEach(function () {
//   if (this?.currentTest?.state === 'failed') {
//     Cypress.runner.stop()
//   }
// })
describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
    // cy.prepareForOfflineVisit()
  })
})

let deploymentSurveyToUse = null
const camerasToReequip = []
describe('Get cameras created in deployment', { retries: 3 }, () => {
  it('get deployment survey', () => {
    // waits 10 seconds to assert if undefined or not, which will be used as the dynamic wait for if it fails.
    cy.loadLocalStore('apiModels', (apiModels) => {
      cy.wrap(apiModels.models?.['camera-trap-deployment-survey'])
        .should('not.be.undefined')
        .and('not.be.null')
        .then((deploymentSurveys) => {
          //using `findLast`, as the first deployment only has one point, but we need 3
          //FIXME better to do this dynamically by checking that the survey has 3 points
          deploymentSurveyToUse = deploymentSurveys.findLast((o) => o.survey_label.includes('Camera Trap Cypress'))
        })
    })
  })

  it('get deployment points', () => {
    cy.loadLocalStore('apiModels', (apiModels) => {
      cy.wrap(apiModels.models?.['camera-trap-deployment-point'])
        .should('not.be.undefined')
        .and('not.be.null')
        .then((deploymentPoints) => {
          cy.wrap(deploymentPoints).each((deploymentPoint) => {
            if (camerasToReequip.length > 3) return

            if (
              deploymentPoint?.camera_trap_survey?.id &&
              deploymentPoint.camera_trap_survey.id === deploymentSurveyToUse.id
            ) {
              camerasToReequip.push(deploymentPoint)
            }
          })
        })
    })
  })
})

//do each reequipping twice, as it's possible to reequip the same camera multiple times
for (let i = 0; i < 2; i++) {
  describe(`Do reequipping ${i + 1}`, () => {
    it('.should() - assert navigation to ' + protocol + ' is possible', () => {
      cy.selectProtocolIsEnabled(module, protocolCy)
    })

    it('choose survey', () => {
      cy.testEmptyField(
        'Field: "Survey Label": This field is required',
        'workflowNextButton',
      )
      cy.dataCy('survey_label').selectFromDropdown(deploymentSurveyToUse.survey_label)
      cy.nextStep()
    })

    //need to use indexed loop as when Cypress sets up the tests, it iterates over this
    //loop to create the `it`s - if we iterate over `camerasToReequip`, it won't be
    //defined at setup time
    for (let index = 0; index < 3; index++) {
      switch (index) {
        case 0:
          it('trap 1, vandalised', () => {
            cy.cameraTrapReequippingPoint1(camerasToReequip[index])
          })
          break
        case 1:
          it('trap 2, operational', () => {
            cy.cameraTrapReequippingPoint2(camerasToReequip[index])
          })
          break
        case 2:
          it('trap 3, wildfire damage', () => {
            cy.cameraTrapReequippingPoint3(camerasToReequip[index])
          })
          break
      }
    }

    it('complete survey (end)', () => {
      cy.nextStep()
    })
    pub()
    // it('publish', () => {
    //   cy.workflowPublish()
    // })
  })
}

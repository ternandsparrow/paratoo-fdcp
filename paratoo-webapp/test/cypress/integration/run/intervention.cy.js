const { pub } = require("../../support/command-utils")

const protocol = 'Intervention Project'
const protocolCy = 'Intervention\\ Project'
const module = 'Interventions'
const polygonCoords = [
  {
    lat: -23.699147554916962,
    lng: 132.79896855354312,
  },
  {
    lat: -23.69973699706658,
    lng: 132.79779911041263,
  },
  {
    lat: -23.700935521228523,
    lng: 132.7979922294617,
  },
  {
    lat: -23.701210590794854,
    lng: 132.79925823211673,
  },
]

before(() => {
  cy.mockLocation(-23.7, 132.8)
})
describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })
  it('Intervention', function () {
    // Intervention General Project Information
    cy.recordLocation()
    cy.get('[data-cy="surveyor_names"]').addObservers('observerName', [
      'Tom',
      'Luke',
      'Walid',
    ])
    cy.get('[data-cy="general_note"]').type('testing')
    // cy.get('[data-cy="workflowNextButton"] > .q-btn__content > .block').click().wait(500)
    cy.nextStep()

    // Interventions
    cy.selectFromDropdown('intervention_type', 'Debris Removal')
    cy.get('[data-cy="Initial"]').click()
    cy.selectFromDropdown('debris_removal_type', 'Green Waste')
    cy.get('[data-cy="weight_of_debris_removed"]').type('123')

    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
    })
    cy.get('[data-cy="startRecordingCorner"]').click()
    // point 1
    cy.get('[data-cy="addPoint"]').click()
    cy.get('[data-cy="addPoint"]').click()
    // position of the points should be different
    cy.get('.q-notification').should(
      'contain.text',
      'Position must be new, change your current location or drag point 1 marker to a different place',
    )
    for (const [idx, coords] of Object.entries(polygonCoords)) {
      // change current position
      cy.window().then(($win) => {
        $win.ephemeralStore.updateUserCoords(coords, true)
      })

      // add points
      if (idx == polygonCoords.length - 1) {
        cy.get('[data-cy="finishSketch"]').click()
        continue
      }
      cy.get('[data-cy="addPoint"]').click()
    }
    // area of the polygon should be 2.50 hectares
    cy.get('[data-cy="polygonArea"]').should('contain', 'Area (ha): 2.50')

    // add photos
    cy.get('[data-cy="addImg"]').click()
    cy.get('[data-cy="takePhoto"]').click()

    cy.selectFromDropdown('direction', 'North West')
    cy.get('[data-cy="direction"] > span').should('contain.text', 'North West')
    cy.dataCy('description').type('Line1{enter}line2{enter}line3')
    cy.get('[data-cy="done"]').eq(0).click()

    cy.get('[data-cy="comments"]').type('intervention test')
    cy.get('[data-cy="done"]').click()
  })
  it('.should() - complete survey step (end)', () => {
    cy.nextStep()
  })

  pub()
  // it('.should() - publish', () => {
  //   cy.workflowPublish(false)
  // })
})

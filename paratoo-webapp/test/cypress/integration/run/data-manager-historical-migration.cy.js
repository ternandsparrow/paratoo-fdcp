import { dataManager, bulk } from '../../fixtures/data-manager-historical-migration-state-init.json'

describe('Landing', () => {
  it('login', () => {
    cy.login('TestUser', 'password')
  })
  it('init state', () => {
    //need to re-generate voucher barcode so it's unique regardless of when this test is
    //run w.r.t. other tests
    const parsedDm = Cypress._.cloneDeep(dataManager)
    parsedDm.publicationQueue[0].collection['floristics-veg-voucher-full'][0].voucher_barcode = `dev_foo_barcode_${Math.floor(Date.now())}`

    localStorage.setItem('dataManager', JSON.stringify(parsedDm))
    localStorage.setItem('bulk', JSON.stringify(Cypress._.cloneDeep(bulk)))
  })
  it('hydrate', () => {
    //wrote to localStorage, so need to force pinia to hydrate
    cy.window()
      .then(($win) => {
        $win.dataManager.$hydrate()
        $win.bulkStore.$hydrate()
      })
  })
  it('check init state', () => {
    cy.window()
      .then(($win) => {
        cy.wrap($win.dataManager.publicationQueue.length)
          .should('eq', 1)
        cy.wrap($win.dataManager.responses.length)
          .should('eq', 2)
        cy.wrap($win.dataManager.submitResponses.length)
          .should('eq', 2)
      })
  })
})

describe('Migration', () => {
  it('check dexie is empty', () => {
    cy.countHistoricalDataInDexie({
      table: 'responses',
      expectedCount: 0,
    })
    cy.countHistoricalDataInDexie({
      table: 'submitResponses',
      expectedCount: 0,
    })
  })
  it('trigger migration', () => {
    cy.reload().wait(5000)
  })
  it('check responses and submitResponses moved to Dexie', () => {
    cy.countHistoricalDataInDexie({
      table: 'responses',
      expectedCount: 2,
    })
    cy.countHistoricalDataInDexie({
      table: 'submitResponses',
      expectedCount: 2,
    })
    cy.window()
      .then(($win) => {
        cy.wrap($win.dataManager.publicationQueue.length)
          .should('eq', 1)
        cy.wrap($win.dataManager.responses.length)
          .should('eq', 0)
        cy.wrap($win.dataManager.submitResponses.length)
          .should('eq', 0)
      })
  })
  it('sync remaining queue item', () => {
    cy.dataCy('submitQueuedCollectionsBtn')
      .should('be.enabled')
      .click()
      .wait(100)
    cy.dataCy('submitQueuedCollectionsConfirm').click()
    cy.dataCy('submissionProgressWrapper', { timeout: 60000 })
      .should('not.exist')
    
    cy.window()
      .then(($win) => {
        cy.wrap($win.dataManager.publicationQueue.length)
          .should('eq', 0)
      })
  })
})
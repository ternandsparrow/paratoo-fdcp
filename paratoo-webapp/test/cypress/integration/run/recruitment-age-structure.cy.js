const { pub } = require("../../support/command-utils")

const protocol = 'Recruitment - Age Structure'
const protocolCy = 'Recruitment\\ -\\ Age\\ Structure'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'recruitment'

const speciesToCollect = [
  {
    fieldDataCy: 'floristics_voucher',
    selection: 'Voucher Full Example 2',
    seedlingCount: 5,
    saplingCount: 3,
    juvenileCount: 0,
  },
  {
    fieldDataCy: 'floristics_voucher',
    selection: 'Voucher Lite Example 2',
    seedlingCount: 0,
    saplingCount: 0,
    juvenileCount: 6,
  },
  {
    fieldDataCy: 'floristics_voucher',
    selection: 'Voucher Full Example 7',
    seedlingCount: 0,
    saplingCount: 1,
    juvenileCount: 0,
  },
  {
    fieldDataCy: 'floristics_voucher',
    selection: 'Voucher Lite Example 1',
    seedlingCount: 8,
    saplingCount: 0,
    juvenileCount: 2,
  },
]

const additionalSpeciesToCollect = [
  {
    fieldDataCy: 'floristics_voucher',
    selection: 'Voucher Full Example 1',
    seedlingCount: 1,
    saplingCount: 4,
    juvenileCount: 0,
  },
  {
    fieldDataCy: 'floristics_voucher',
    selection: 'Voucher Full Example 3',
    seedlingCount: 0,
    saplingCount: 10,
    juvenileCount: 2,
  },
]

//we collect one voucher before doing `speciesToCollect`, so need to provide an offset
let initialIndexOffset = 1

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocolCy)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
})

describe(`Collect ${protocol}`, () => {
  it('1.0[test-to-pass] - survey start', () => {
    cy.nextStep()
  })
  it('1.1[test-to-fail] - fail when submit without any data', () => {
    cy.testEmptyField('You have to add species', 'workflowNextButton')
  })
  it('1.2[validate-field] - growth stage', () => {
    //this voucher doesn't get counted
    cy.dataCy('addSpecies').click().wait(500)
    cy.getAndValidate('floristics_voucher', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('Voucher Full Example 6')

    cy.getAndValidate('growth_and_life_stage', {
      isComponent: true,
      testEmptyField: ['done', -1],
    })

    cy.getAndValidate('growth_stage', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown()
    
    cy.getAndValidate('life_stage', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown()

    cy.dataCy('done').first().click()
    cy.dataCy('done').last().click()
  })
  it(`1.3[test-to-pass] - complete growth and life stages step`, () => {
    cy.collectRecruitmentGrowthFormAndLifeStages(speciesToCollect)
    cy.nextStep()
  })

  //TODO test decrement
  it(`1.4[test-to-pass] - collecting sapling and seeding count step`, () => {
    cy.collectSaplingAndSeedlingCount(speciesToCollect, initialIndexOffset)
  })

  it(`1.5[test-to-pass] - add more vouchers`, () => {
    cy.dataCy('workflowBackButton').click().wait(1000)

    cy.collectRecruitmentGrowthFormAndLifeStages(additionalSpeciesToCollect)
    cy.nextStep()
  })

  it(`1.6[test-to-pass] - check initial vouchers' counts are correct`, () => {
    //`collectSaplingAndSeedlingCount()` from before would have asserted counts based on
    //the `speciesToCollect`, but now we need to check those are still correct after
    //adding the `additionalSpeciesToCollect`
    for (const [index, species] of speciesToCollect.entries()) {
      cy.dataCy('seedlingCountValue')
        .eq(index + initialIndexOffset)
        .should('have.text', species.seedlingCount)
      cy.dataCy('saplingCountValue')
        .eq(index + initialIndexOffset)
        .should('have.text', species.saplingCount)
      cy.dataCy('juvenileCountValue')
        .eq(index + initialIndexOffset)
        .should('have.text', species.juvenileCount)
    }
  })

  it(`1.7[test-to-pass] - count additional vouchers`, () => {
    cy.collectSaplingAndSeedlingCount(
      additionalSpeciesToCollect,
      initialIndexOffset + speciesToCollect.length,
    )
  })

  it(`1.8[test-to-pass] - publish collection`, () => {
    cy.nextStep()
  })
  pub()
})

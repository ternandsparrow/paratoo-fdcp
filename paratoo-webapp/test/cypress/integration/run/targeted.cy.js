const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'Targeted Surveys'
const protocol = 'Metadata Collection'
import { generateCypressTests, pub } from '../../support/command-utils'

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })

  it('getValidationRules', () => {
    cy.documentation()
      .currentProtocolWorkflow((models) => {
        global.currentProtRules = models
        cy.log('global.currentProtRules', global.currentProtRules)
      })
      .then(() => {
        cy.log(generateCypressTests(global.currentProtRules))
      })
  })
})

describe('1 - New Targeted Survey (start)', () => {
  // date?
  it('1.1 submit survey', () => {
    cy.nextStep()
  })
})
// FIXME validation is only applied to a zone if  any amount of data has been entered, otherwise its ignored. Unsure if bug or feature. Either way, it's a simple workaround.
describe('2 - Metadata Collection', () => {
  // context() is identical to describe() and specify() is identical to it(), so choose whatever terminology works best for you
  context('2.1 - Data Provider', () => {
    specify('2.1.7 - secondary organisations', () => {
      cy.dataCy('secondary_organisations').type('TERN2')
    })
    specify('2.1.1 submitter name', () => {
      cy.dataCy('submitter_name').type('Tom Ant').clear()
      cy.getAndValidate('submitter_name').type('Tom Ant')
    })
    specify('2.1.2 submitter contact number', () => {
      cy.getAndValidate('submitter_contact_number').type('1234567891')
    })
    specify('2.1.3 submitter contact email', () => {
      cy.getAndValidate('submitter_contact_email').type(
        'a.email@real.email.com',
      )
    })
    specify('2.1.4 provider orginisation', () => {
      cy.getAndValidate('provider_organisation').type('TERN')
    })
    specify('2.1.5 provider email', () => {
      cy.getAndValidate('provider_email').type('provider.email@real.email.com')
    })
    specify('2.1.6 data licensing comment', () => {
      cy.getAndValidate('data_licencing_comments').type(
        'data_licencing_comments',
      )
    })
  })

  context('2.2 - Data Identification', () => {
    specify('2.2.1 - linked dataset', () => {
      cy.getAndValidate('linked_dataset').type('idk what to type here')
    })
    specify('2.2.2 - dataset title', () => {
      cy.dataCy('dataset_title').type('a').clear()
      cy.getAndValidate('dataset_title').type('TERN dataset title')
    })
    specify('2.2.3 - broader monitoring program', () => {
      cy.getAndValidate('broader_monitoring_program').type('more text')
    })
    specify('2.2.4 - dataset version number', () => {
      cy.getAndValidate('dataset_version_number').type('1234567891011121214')
    })
    specify('2.2.5 - dataset creation date', () => {
      cy.dataCy('dataset_creation_date')
      cy.selectDateTime({
        fieldName: 'dataset_creation_date',
        date: {
          year: 'current',
          month: 'current',
          day: 28,
        },
        time: {
          hour: 14,
          minute: 'current',
        },
      })
    })
    specify('2.2.6 - published date', () => {
      cy.getAndValidate('published_status').selectFromDropdown(null)
    })
  })

  context('2.3 - dataset time period', () => {
    specify('2.3.4 - number of sampling periods', () => {
      cy.getAndValidate('number_of_sampling_periods').type('3')
    })
    specify('2.3.1 - dataset start date', () => {
      //cy.getAndValidate('dataset_start_date')
      cy.selectDateTime({
        fieldName: 'dataset_start_date',
        date: {
          year: 'current',
          month: 'current',
          day: 27,
        },
        time: {
          hour: 14,
          minute: 'current',
        },
      })
    })
    specify('2.3.2 - dataset end date', () => {
      //cy.getAndValidate('dataset_end_date')
      cy.selectDateTime({
        fieldName: 'dataset_end_date',
        date: {
          year: 'current',
          month: 'current',
          day: 28,
        },
        time: {
          hour: 14,
          minute: 'current',
        },
      })
    })
    specify('2.3.3 - survey frequency', () => {
      cy.getAndValidate('survey_frequency').selectFromDropdown(null)
    })
  })

  context('2.4 - monitoring summary', () => {
    specify('2.4.1 - broader monitoring program', () => {
      cy.dataCy('broad_monitoring_type').selectFromDropdown(null)
    })
    specify('2.4.2 - monitoring scale', () => {
      cy.getAndValidate('monitoring_scale').selectFromDropdown(null)
    })
    specify('2.4.3 - landscape category', () => {
      cy.getAndValidate('landscape_category').selectFromDropdown(null)
    })
  })

  context('2.5 - survey data', () => {
    specify('2.5.1 - key target taxa', () => {
      cy.dataCy('key_target_taxa').selectFromDropdown(null)
      cy.getAndValidate('key_target_taxa').selectFromDropdown(null)
      //cy.getAndValidate('key_target_taxa')
    })
    specify('2.5.2 - key target species', () => {
      cy.getAndValidate('key_target_species').type('Species name')
      //cy.getAndValidate('key_target_species')
    })
    specify('2.5.3 - key target communities', () => {
      cy.getAndValidate('key_target_communities').selectFromDropdown(null)
      //cy.getAndValidate('key_target_communities')
    })
    specify('2.5.4 - data captured', () => {
      cy.getAndValidate('data_captured').selectFromDropdown(null)
      //cy.getAndValidate('data_captured')
    })
  })

  context('2.6 - methods summary', () => {
    specify('2.6.1 - methods citation name', () => {
      cy.dataCy('methods_citation_name').type('stringf').clear()
      cy.getAndValidate('methods_citation_name').type('stringf')
    })
    specify('2.6.2 - methods citation publisher', () => {
      cy.getAndValidate('methods_citation_publisher').type('stringg')
    })
    specify('2.6.3 - methods citation organisation', () => {
      cy.getAndValidate('methods_citation_organisation').type('stringg')
    })
    specify('2.5.4 - methods citation version', () => {
      cy.getAndValidate('methods_citation_version').type('stringhj')
    })
    specify('2.6.5 - methods citation year', () => {
      cy.getAndValidate('methods_citation_year').type(2020)
    })
    specify('2.6.6 - methods description', () => {
      cy.getAndValidate('methods_description').type('stringtyj')
    })
    specify('2.6.7 - methods employed', () => {
      cy.getAndValidate('methods_employed').selectFromDropdown(null)
    })
    specify('2.6.8 - surveyor type', () => {
      cy.getAndValidate('surveyor_type').selectFromDropdown(null)
    })
    specify('2.6.9 - named personnel', () => {
      // cy.dataCy('named_personnel')
      cy.addObservers('named_personnel', [
        'Tom',
        'tom',
        'tom' + (Math.random() + 1).toString(36).substring(10),
      ])
    })
    specify('2.6.10 - survey period effort', () => {
      // cy.dataCy('survey_period_effort')
      cy.getAndValidate('survey_period_effort').type(1)
    })
    specify('2.6.11 - species determiner', () => {
      cy.getAndValidate('species_determiner').selectFromDropdown(null)
    })
    specify('2.6.12 - lodgement facility', () => {
      cy.getAndValidate('lodgement_facility').type('string')
    })
  })

  context('2.7 - spatial information', () => {
    specify('2.7.1 - geographic extent', () => {
      cy.dataCy('recordLocation').click()
      cy.dataCy('deleteCoordinate').click()
      cy.dataCy('recordLocation').click()
    })
    specify('2.7.2 - temporal resolution', () => {
      cy.dataCy('temporal_resolution').selectFromDropdown(null)
    })
    specify('2.7.3 - horizontal resolution', () => {
      cy.dataCy('horizontal_resolution').selectFromDropdown(null)
    })
    specify('2.7.4 - vertical resolution', () => {
      cy.dataCy('vertical_resolution').selectFromDropdown(null)
    })
    specify('2.7.5 - spatial data type', () => {
      cy.dataCy('spatial_data_type').selectFromDropdown(null)
    })
    specify('2.7.6 - geographic description', () => {
      cy.dataCy('geographic_description').type('string')
    })
  })

  context('2.8 - descriptive keywords', () => {
    specify('2.8.1 - GCMD science keywords', () => {
      cy.dataCy('GCMD_science_keywords').selectFromDropdown(null)
    })
    specify('2.8.2 - ANZSRC fields of research', () => {
      cy.dataCy('ANZSRC_fields_of_research').selectFromDropdown(null)
    })
  })
})

describe('3 - new Targeted Survey (end)', () => {
  it('3,1 - nextStep()', () => {
    cy.nextStep()
  })
})

describe('4 - finish & submit', () => {
  pub()
})

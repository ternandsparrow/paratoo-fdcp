const { pub } = require("../../support/command-utils")

const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'soils'
const protocol = 'Soils - Bulk Density'
//The v in the name is to make sure it goes after the other soils

describe('0 - landing', () => {
  it('login', () => {
    cy.login('TestUser', 'password')
  })

  it('navigation to ' + protocol + ' is disabled', () => {
    cy.selectProtocolIsDisabled(module, protocol)
  })

  it('plot layout', () => {
    cy.plotLayout()
  })

  it('navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})

describe('1 - Soil Bulk Density Survey - field', () => {
  it('1.1[validate-field] - survey', () => {
    cy.dataCy('Field').click()

    cy.getAndValidate('observers', { isComponent: true })
    cy.addObservers('observerName', [
      'Tom',
      'Luke',
      'Walid',
      'Wildcard' + (Math.random() + 1).toString(36).substring(10),
    ])

    // cy.getAndValidate('associated_soil_pit_id')

    cy.getAndValidate('collection_method').selectFromDropdown(
      'Differential GNSS',
    )

    cy.recordLocation()
  })

  it('publish', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})
describe('2 - Soil Bulk Density Survey - lab', () => {
  it('navigation to ' + protocol, () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
  it('2.1[test-to-pass] - survey', () => {
    cy.dataCy('Lab').click()
    cy.dataCy('field_survey_id').selectFromDropdown()
    cy.nextStep()
  })
  it('2.2[test-to-pass] - collect data for 0.00 - 0.10m', () => {
    // cy.dataCy('0.00m - 0.10m').click()
    cy.getAndValidate('barcode', {
      isComponent: true,
      testEmptyField: ['done'],
    })
    cy.dataCy('not_collected').click()
    cy.dataCy('done').click()
  })
  it('2.3[test-to-pass] - collect data for 0.10 - 0.20m', () => {
    cy.get('[data-cy="0.10m - 0.20m"]').click()
    cy.dataCy('not_collected').click()
    cy.dataCy('done').click()
  })
  it('2.3[test-to-fail] - failed to upload if missing samples', () => {
    cy.testEmptyField(
      'Invalid data in: 0.20m - 030m',
      'workflowNextButton',
      null,
      0,
    )
  })
  it('collect data for 0.20m - 0.30m', () => {
    cy.get('[data-cy="0.20m - 0.30m"]').click()
    cy.dataCy('recordBarcode').click()
    cy.dataCy('done').click()
  })
  it('publish', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

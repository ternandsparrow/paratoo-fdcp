const { pub } = require("../../support/command-utils")

const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'vertebrate fauna'
const protocol = 'Vertebrate Fauna - Active and Passive Search'
// TODO needs updated validation, probably

// duration is not in the schema, so its assumedly for app only.
global.ignorePaths = ['vertebrate-active-passive-search.duration']

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it.skip('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocol)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})

describe('1 - Survey set-up', () => {
  it('1.1[validate-field] - survey', () => {
    cy.getAndValidate('survey_intent', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown()

    // cy.getAndValidate('observer_name').selectFromDropdown()
    cy.addObservers('observerName', ['Tom'])
    cy.getAndValidate('role', {
      isChildOf: 'observer_details',
    }).selectFromDropdown()
    cy.dataCy('done').click()

    cy.getAndValidate('weather', {
      testEmptyField: ['workflowNextButton'],
      isComponent: true,
    })
    cy.completeWeatherForm()
    cy.getAndValidate('moon_phase', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown()

    cy.getAndValidate('tracking_surface', {
      testEmptyField: ['workflowNextButton'],
    }).type('Dirt or something')

    cy.getAndValidate('search_type', {
      testEmptyField: ['workflowNextButton'],
      isComponent: true,
    })
    cy.setCheck('Active', true)

    cy.setCheck('call_play_back', true)
    // cy.dataCy('workflowNextButton').click()
    cy.nextStep()
  })
})

describe('Search', { retries: 1 }, () => {
  it('[test-to-pass] - location and time', () => {
    cy.get( '[data-cy="Biodiversity plot"]' ).click()
    cy.recordLocation()
  })

  it('[validate-field] - start location photo', () => {
    cy.addImages('addImg')
  })

  it('[test-to-pass] - start search', { retries: 0 }, () => {
    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
    })
    cy.dataCy('done')
      .as('startSearch')
      .should('contain.text', 'Start active search')
    cy.get('@startSearch').click()
  })

  it('[validate-field] observed signs 1', () => {
    cy.setExpansionItemState('sign_observed_1', 'open').then((e) => {
      // do whatever you need to do inside
      cy.getAndValidate('sign_type').selectFromDropdown()
      cy.getAndValidate('attributable_fauna_species').selectFromDropdown()
      cy.getAndValidate('sign_age').selectFromDropdown()
      cy.getAndValidate('age_class').selectFromDropdown()

      cy.getAndValidate('photo', { isComponent: true })
      cy.addImages('addImg')

      cy.getAndValidate('observation_comments').type('comment comment')

      // will update the expansion making what is currenty being referenced be detached from the dom
      cy.dataCy('done').click()
    })
  })

  it('[test-to-pass] observed signs 2', () => {
    cy.dataCy('addEntry').first().click()
    cy.setExpansionItemState('sign_observed_2', 'open').within((e) => {
      // do whatever you need to do inside
      cy.selectFromDropdown('sign_type')
      cy.selectFromDropdown('attributable_fauna_species')
      cy.selectFromDropdown('sign_age')
      cy.selectFromDropdown('age_class')
      cy.addImages('addImg')
      cy.dataCy('observation_comments').type('comment comment')

      cy.dataCy('done').click()
    })
  })

  it('[validate-field] - observed animals 1', () => {
    cy.setExpansionItemState('animal_observed_1', 'open').then(() => {
      // do whatever you need to do inside
      cy.getAndValidate('species').selectFromSpeciesList('emu').type('{esc}')
      cy.getAndValidate('age_class').selectFromDropdown()
      cy.getAndValidate('reproductive_status').selectFromDropdown()
      cy.getAndValidate('activity_type').selectFromDropdown()

      cy.setCheck('hand_caught', true)
      cy.getAndValidate('head_length').type(Math.floor(Math.random() * 5))
      cy.getAndValidate('body_length').type(Math.floor(Math.random() * 5))
      cy.getAndValidate('tail_length').type(Math.floor(Math.random() * 5))
      cy.getAndValidate('hind_foot_length').type(Math.floor(Math.random() * 5))
      cy.getAndValidate('forearm_length').type(Math.floor(Math.random() * 5))
      cy.getAndValidate('tibia_length').type(Math.floor(Math.random() * 5))
      cy.getAndValidate('ear_width').type(Math.floor(Math.random() * 5))
      cy.getAndValidate('ear_length').type(Math.floor(Math.random() * 5))
      cy.getAndValidate('animal_fate').selectFromDropdown()
      cy.getAndValidate('comments').type('Im a comment yipee')

      // will update the expansion making what is currenty being referenced be detached from the dom
      cy.dataCy('done').click()
    })
  })
  it('[test-to-pass] - observed animals 2', () => {
    cy.dataCy('addEntry').last().click()
    cy.setExpansionItemState('animal_observed_2', 'open').within(() => {
      // do whatever you need to do inside
      cy.selectFromSpeciesList('species', 'emu').type('{esc}')
      cy.selectFromDropdown('age_class')
      cy.selectFromDropdown('reproductive_status')
      cy.selectFromDropdown('activity_type')
      cy.setCheck('hand_caught', true)
      cy.dataCy('head_length').type(Math.floor(Math.random() * 5))
      cy.dataCy('body_length').type(Math.floor(Math.random() * 5))
      cy.dataCy('tail_length').type(Math.floor(Math.random() * 5))
      cy.dataCy('hind_foot_length').type(Math.floor(Math.random() * 5))
      cy.dataCy('forearm_length').type(Math.floor(Math.random() * 5))
      cy.dataCy('tibia_length').type(Math.floor(Math.random() * 5))
      cy.dataCy('ear_width').type(Math.floor(Math.random() * 5))
      cy.dataCy('ear_length').type(Math.floor(Math.random() * 5))
      cy.selectFromDropdown('animal_fate')
      cy.dataCy('comments').type('Im a comment yipee')
      // will update the expansion making what is currenty being referenced be detached from the dom
      cy.dataCy('done').click()
    })
  })

  it('end search', () => {
    cy.testEmptyField('The search has not finished', 'workflowNextButton')

    cy.dataCy('end_search').should('contain.text', 'End search').click()
    cy.dataCy('end_search').should('not.exist')
    // TODO continue button
    // cy.get('.q-btn .q-btn-item .non-selectable .no-outline .q-btn--standard .q-btn--rectangle .q-btn--rounded .bg-secondary .text-white .q-btn--actionable .q-focusable .q-hoverable .q-ma-my') //.should('contain.text', 'Continue').should('be.visible')
  })

  it('record end location', () => {
    cy.recordLocation({
      btnEqIndex: -1,
    })
  })

  it('submit', { retries: 0 }, () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

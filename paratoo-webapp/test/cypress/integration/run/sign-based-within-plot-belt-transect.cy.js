const { pub } = require("../../support/command-utils")

const protocol = 'Sign-based Fauna Surveys - Within-plot Belt Transect'
const module = 'Sign-based Fauna Surveys'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

global.ignorePaths = ['within-plot-belt-transect.transect_line']

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })

  it('mock location', () => {
    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
    })
  })
})

describe(protocol, () => {
  it('complete transect setup', () => {
    cy.getAndValidate('number_of_transects', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('2')
    cy.getAndValidate('survey_intent', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('Repeated measure')
    const selectedSpecies = ['Deer', 'Other']
    cy.getAndValidate('target_species', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown(selectedSpecies)
    
    cy.getAndValidate('tracking_substrate', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('Gravel')
    cy.getAndValidate('scat', {
      isComponent: true,
      testEmptyField: ['workflowNextButton'],
    })
  })

  it('.should() - add observers details', () => {
    cy.getAndValidate('observer_details', {
      isComponent: true,
      testEmptyField: ['workflowNextButton'],
    })
    const observers = ['Spotter', 'Data entry']
    observers.forEach((role, index) => {
      const modifier = Math.floor(new Date() / 1000)
      cy.addObservers('observerName', [`Anon ${modifier + index}`])
      cy.selectFromDropdown('role', role)
      cy.get('[data-cy="done"]').eq(-1).click()
      if (name !== 'Pepe') {
        cy.get('[data-cy="addEntry"]').eq(-1).click()
      }
    })
  })

  it('complete weather form and go to next step', () => {
    cy.completeWeatherForm()
    cy.nextStep()
  })

  //TODO complete transec 2
  it('complete transect 1', () => {
    cy.dataCy('transect_number').selectFromDropdown()
    cy.dataCy('addImg').eq(0).click().wait(50)
    cy.dataCy('takePhoto').eq(0).click().wait(50)
    cy.get('[data-cy="done"]').eq(0).click()
    cy.selectFromDropdown('quadrat_number', 'Quadrat 1')
    cy.dataCy('signs_present').click()
    cy.completeSignObserved()
    
    for (let i = 0; i < 5; i++) {
      cy.dataCy('nextPointBtnLabel').click()
    }
  })
  it(`end transect`, () => {
    cy.dataCy('done').last().click()
  })
  it('submit the survey to core', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

const { pub } = require("../../support/command-utils")

const protocol = 'Condition Attributes Protocols'
const protocolCy = 'Condition\\ Attributes\\ Attributes'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'condition'

describe('0-Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - start collection button should be disabled', () => {
    //test-to-fail
    cy.selectProtocolIsDisabled(module)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
})

//TODO test all `equipment_for_dia`, as it affects some fields (like Basal DBH)
describe('1 - all protocol data', () => {
  it('1.0 - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })

  it('1.1[validate-field] - condition survey', function () {
    cy.getAndValidate('plot_size', { isComponent: true })
    cy.get('[data-cy="40 x 40 m"]').click()
    //TODO check different fields show correctly based on `equipment_for_dia` selection
    cy.selectFromDropdown(
      'equipment_for_dia',
      '^Tape Measure$',
      null,
      null,
      false,
      true,
    )
    cy.getAndValidate('equipment_for_height').selectFromDropdown('Range Finder')
    cy.get('[data-cy="10 cm"]').click()
    cy.nextStep()
  })

  //TODO make multiple obs
  it('1.2[validate-field] - condition tree survey - tree record', function () {
    cy.recordLocation({
      btnEqIndex: 0,
    })

    cy.getAndValidate('tree_record', { isComponent: true })
    cy.get('[data-cy="mallee_or_mulga"]').click()
    cy.dataCy('partially_downed_tree').should('not.exist')    //depends on `dead` being selected
    cy.get('[data-cy="dead"]').click()
    cy.dataCy('partially_downed_tree').should('exist')
    //FIXME check that `growth_stage` is autofilled when `dead` is checked
    // cy.dataCy('growth_stage')
    //   .invoke('val')
    //   .should('include', 'Dead')
    cy.getAndValidate('species').selectFromDropdown()
    cy.dataCy('intensity').should('not.exist')    //depends on `life_stage` selecting 'flowering'
    cy.getAndValidate('life_stage').selectFromDropdown('Flowers')
    cy.dataCy('intensity').should('exist')
    cy.getAndValidate('height', { isChildOf: 'tree_record' }).type('3')
  })

  it('1.3[validate-field] - condition tree survey - record DBH', function () {
    cy.getAndValidate('record_dbh', { isComponent: true })
    // record DBH
    cy.dataCy('stem_1').should('exist')
    cy.dataCy('stem').should('not.exist')
    cy.get('[data-cy="multi_stemmed"]').click()
    cy.dataCy('stem_1').should('not.exist')
    cy.dataCy('stem').should('exist')
    cy.getAndValidate('POM_multi').type('3')
    cy.getAndValidate('DBH').type('11')
    cy.dataCy('done').eq(0).click() //save the stem

    //FIXME checking if `buttressed_tree` is correctly show isn't working - temp workaround is to check a field inside the component instead
    // cy.dataCy('buttressed_tree').should('not.exist')
    cy.dataCy('reach_DBH').should('not.exist')
    cy.dataCy('buttresses').click()
    // cy.dataCy('buttressed_tree').should('exist')
    cy.dataCy('reach_DBH').should('exist')

    cy.dataCy('reach_DBH')
      .invoke('val')
      .should('be.empty')
    cy.getAndValidate('reach_POM').type(1)
    cy.getAndValidate('reach_circumference_at_breast_height').type(5)
    cy.dataCy('reach_DBH')
      .invoke('val')
      .should('include', '2')

    cy.dataCy('DBH_50_CM_above_buttress')
      .invoke('val')
      .should('be.empty')
    cy.getAndValidate('POM_50_CM_above_buttress').type(1)
    cy.getAndValidate('circumference_50_cm_above_buttress').type(5)
    cy.dataCy('DBH_50_CM_above_buttress')
      .invoke('val')
      .should('include', '2')
  })

  it('1.4[validate-field] - condition tree survey - tree health', function () {
    cy.getAndValidate('tree_health', { isComponent: true })
    // add tree health
    cy.getAndValidate('canopy_cover').type('50')
    cy.getAndValidate('crown_damage', { isComponent: true })
    // FIXME: CID is custom component with hard coded field, so we cannot test it with getAndValidate
    // cdi 1
    cy.get('[data-cy="cdiAddItem"]').click().wait(500)
    cy.selectFromDropdown('cdiType', 'Defoliation')
    cy.get('[data-cy="cdiIncidence"]').type('50')
    cy.get('[data-cy="cdiSeverity"]').type('50')
    cy.get('[data-cy="cdiDone"]').click()

    // total CDI should be 25
    cy.get('[data-cy="totalCdi"]').should('contain', 'Total CDI = 25')

    // cdi 2
    cy.get('[data-cy="cdiAddItem"]').click().wait(500)
    // type, Incidence and Severity are required to calculate CDI
    cy.get('[data-cy="cdiDone"]').click()
    cy.testEmptyField('Please insert Type to calculate CDI', 'cdiDone', -1)
    cy.selectFromDropdown('cdiType', 'Necrosis')
    cy.get('[data-cy="cdiIncidence"]').type('10')
    cy.get('[data-cy="cdiDone"]').click()
    cy.testEmptyField('Please insert Severity to calculate CDI', 'cdiDone', -1)
    cy.get('[data-cy="cdiSeverity"]').type('15')
    cy.get('[data-cy="cdiDone"]').click()
    // total CDI should be 26.5
    cy.get('[data-cy="totalCdi"]').should('contain', 'Total CDI = 26.5')

    cy.getAndValidate('mistletoes_alive').type('10')
    cy.getAndValidate('mistletoes_dead').type('10')

    //hollows
    for (let h = 0; h < 3; h++) {
      cy.getAndValidate('size_of_hollow').clear().type(5)
      cy.getAndValidate('direction_of_hollow_opening').clear().type(5)
      cy.getAndValidate('height_of_the_hollow').clear().type(5)

      cy.dataCy('done').eq(0).click()

      if (h < 2) {
        cy.dataCy('addEntry').eq(1).click()
      }
    }

    cy.dataCy('crack_or_crevices').click()
    cy.dataCy('abnormal_growth').click()
    cy.dataCy('loose_bark').click()
    cy.dataCy('insect_bores').click()

    //insect damage
    for (let i = 0; i < 3; i++) {
      cy.getAndValidate('damage_type').selectFromDropdown(
        'Gall',
      )
      cy.getAndValidate('incidence', {isChildOf: 'tree_health'}, null, 0).type('10')
      cy.getAndValidate('severity', {isChildOf: 'tree_health'}, null, 0).type('10')

      cy.dataCy('done').eq(0).click()
      if (i < 2) {
        cy.dataCy('addEntry').eq(2).click()
      }
    }

    cy.getAndValidate('damage').selectFromDropdown('Lopping')
    cy.getAndValidate('abiotic').type('test')
    cy.getAndValidate('biotic').type('test')
    cy.addImages('addImg')
    cy.dataCy('comments').type('comment')
  })

  it('1.5[validate-field] - condition tree survey - leaf litter', function () {
    //TODO check conditional fields before/after clicking on 'leaf litter present'
    cy.dataCy('leaf_litter_present').click()
    cy.getAndValidate('leaf_litter', { isComponent: true })
    // add leaf litter
    // need to init the child component as this child comp is not required so its validation wont trigger if there's no data in it
    cy.dataCy('litter_depth').type('2')
    cy.getAndValidate('starting_point').type('2')
    cy.getAndValidate('point').type('2')
    cy.dataCy('litter_depth').clear()
    cy.getAndValidate('litter_depth').type('2')

    cy.get('[data-cy="done"]').last().click().wait(1000)
  })

  it('1.8[test-to-pass] - Successfully submit collection', function () {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

describe('2 - subset protocol data', () => {
  it('2.0 - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })

  // standard protocol
  it('2.1[test-to-pass] - condition survey', function () {
    cy.get('[data-cy="100 x 100 m"]').click()
    cy.dataCy('equipment_for_dia').selectFromDropdown('Diameter Tape')
    cy.dataCy('equipment_for_height').selectFromDropdown('Range Finder')
    cy.get('[data-cy="10 cm"]').click()
    cy.nextStep()
  })

  //TODO make multiple obs
  // standard protocol
  it('2.2[validate-field] - condition tree survey', function () {
    // add tree record
    // FIXME: sometimes it's got autofilled so cant test with getAndValidate
    cy.recordLocation({
      btnEqIndex: 0,
    })
    cy.get('[data-cy="mallee_or_mulga"]').click()
    cy.get('[data-cy="dead"]').click()
    cy.dataCy('species').selectFromDropdown()
    cy.selectFromDropdown('life_stage', 'Vegetative')
    cy.get('[data-cy="height"]').eq(0).type('3')

    // record DBH
    cy.get('[data-cy="POM_single"]').type('3')
    cy.get('[data-cy="DBH"]').type('11')

    cy.get('[data-cy="done"]').last().click().wait(1000)
  })
  it('Successfully submit collection', function () {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

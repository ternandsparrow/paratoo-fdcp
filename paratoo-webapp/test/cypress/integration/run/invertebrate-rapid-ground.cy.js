// TODO: tests for different cases of offline plots
// we can integrate this test to offline test of the plot layout
// however it's not doable at the moment to make cypress place the SW trap point in a correct location
// within fauna plot due the validation in the app requires all the traps need to be within fauna plot

const { pub } = require('../../support/command-utils')

const protocol = 'Invertebrate Fauna - Rapid Ground Trapping'
const protocolCy = 'Invertebrate\\ Fauna\\ -\\ Rapid\\ Ground\\ Trapping'
const module = 'invertebrate fauna'

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })

  it('.should() - do plot layout', () => {
    cy.plotLayout(true, null, {
      layout: 'QDASEQ0003',
      visit: 'QLD spring survey',
    })
  })

  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
})

describe('1 - survey setup', () => {
  // have to finish 1.1 and 1.2 first as we have a custom logic to enforce finish these first before trigger ApiModelCrud onSubmitFunc
  it('1.1[test-to-pass] - Place and save grid', () => {
    cy.dataCy('map').click().wait(50)
    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords(
        { lat: -27.390413736769265, lng: 152.87625521421432 },
        true,
      )
    })

    
    cy.dataCy('backToLocation').click()
    cy.dataCy('gridBtn').click()
    cy.dataCy('trap-point-1').should('exist')
    cy.dataCy('close').click()
  })

  it('1.2[test-to-pass] - Start and end trapping event', () => {
    for (let i = 0; i < 4; i++) {
      cy.dataCy('trappingEvent').click()
      cy.wait(50)
      cy.dataCy('ok').click()
      cy.wait(50)
    }
  })

  it('1.3[validate-field] - plot type and weather', () => {
    cy.getAndValidate('observers', null, null)
    const modifier = Math.floor(Date.now())
    cy.get('[data-cy="observers"]').eq(0).click().type(`observer-${modifier}`)
    cy.dataCy('addObserver').click()

    cy.getAndValidate('weather', { isComponent: true })
    cy.completeWeatherForm()
  })

  it('1.4[validate-field] - trap specs', () => {
    cy.getAndValidate('trap_diameter').type(2)
    cy.getAndValidate('trap_length').type(2)

    // number_of_traps and bait_type have custom rules
    cy.dataCy('number_of_traps').selectFromDropdown(2)
    cy.dataCy('bait_type')
      .click()
      .then(() => {
        cy.get('.q-item__label ').contains('fish-based cat food').click()
        cy.get('.q-item__label ').contains('honey').click()
      })

    cy.getAndValidate('bait_details').type('bait details')
  })

  it('1.5[validate-field] - Landscape Photo', () => {
    cy.getAndValidate('landscape_photo', { isComponent: true })
    cy.dataCy('addImg').eq(0).click().wait(50)
    cy.dataCy('takePhoto').click()
  })

  it('1.6[test-to-pass] - Complete scanning barcode', () => {
    // cy.get('[data-cy="workflowNextButton"] > .q-btn__content > .block').click()
    cy.nextStep()
    cy.wait(50)
    for (let i = 0; i < 40; i++) {
      cy.dataCy('recordBarcode').click()
      cy.dataCy('nextTrap').click()
    }
  })

  it('1.7[test-to-pass] - Complete Summary', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

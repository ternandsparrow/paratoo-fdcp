const { pub, nukeDatabaseEntriesFor } = require("../../support/command-utils")

const protocol = 'Recruitment - Survivorship '
const protocolCy = 'Recruitment\\ -\\ Survivorship'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'recruitment'
const vouchers = [
  {
    field_name: 'Voucher Full Example 1',
  },
  {
    field_name: 'Voucher Full Example 5',
  },
  {
    field_name: 'Voucher Full Example 7',
  },
  {
    field_name: 'Voucher Lite Example 1',
  },
  {
    field_name: 'Voucher Lite Example 2',
  },
]

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('clean survey and observations relating to protocol', () => {
    // Cypress object will only mount on window object if on '/login' but not '/'?
    cy.window().its('Cypress').should('be.an', 'object')
    cy.window().its('constants').should('be.an', 'object')
      .then(() => {
        nukeDatabaseEntriesFor('recruitment-survivorship-surveys')
        nukeDatabaseEntriesFor('recruitment-survivorship-observations')
      })
      cy.get('button:contains(Refresh Projects)').click()
      
  })
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocolCy)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout(
      false,
      null,
      {
        layout: 'QDASEQ0003',
        visit: 'QLD spring survey',
      }
    )
  })
})

describe(`1 - Collect ${protocol} (new visit)`, () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })

  it('1.1[validate-field] - collect recruitment survivorship survey', () => {


    // FIXME: this keep making the test fail
    cy.dataCy('revisitTable').should('not.exist')
    cy.dataCy('reason_for_revisit').should('not.exist')

    cy.getAndValidate('study_area_type').selectFromDropdown()
    cy.getAndValidate('floristics_voucher')
      .selectFromDropdown(vouchers.map((v) => v.field_name))
      .type('{esc}')
    cy.getAndValidate('comments').type('comment')
    cy.nextStep()
  })

  it(`1.2[validate-field] - collect observation 1`, () => {
    cy.selectFromDropdown('species').selectFromDropdown()
    cy.recordLocation()
    cy.getAndValidate('plant_tag_ID').type(
      `label ${Math.floor(Math.random() * 100000)}`,
    )
    cy.getAndValidate('growth_stage').selectFromDropdown(null)
    cy.getAndValidate('life_stage').selectFromDropdown(null)

    cy.getAndValidate('recruitment_health', { isComponent: true })
    // Health
    cy.getAndValidate('canopy_cover').type(1)

    // Crown Damage Index 1
    cy.dataCy('cdiAddItem').click().wait(500)
    cy.dataCy('cdiType').selectFromDropdown(null)

    cy.dataCy('cdiIncidence').clear().type(9)

    cy.dataCy('cdiSeverity').clear().type(8)
    cy.dataCy('cdiDone').click().wait(500)

    // Crown Damage Index 2
    cy.dataCy('cdiAddItem').click().wait(500)
    cy.dataCy('cdiType').selectFromDropdown(null)

    cy.dataCy('cdiIncidence').clear().type(9)

    cy.dataCy('cdiSeverity').clear().type(8)
    cy.dataCy('cdiDone').click().wait(500)

    cy.getAndValidate('mistletoes_alive').type(2)
    cy.getAndValidate('mistletoes_dead').type(3)

    //hollows
    for (let h = 0; h < 3; h++) {
      cy.getAndValidate('size_of_hollow').clear().type(5)
      cy.getAndValidate('direction_of_hollow_opening').clear().type(5)
      cy.getAndValidate('height_of_the_hollow').clear().type(5)

      cy.dataCy('done').eq(0).click()

      if (h < 2) {
        cy.dataCy('addEntry').eq(0).click()
      }
    }

    cy.getAndValidate('damage_type').selectFromDropdown()
    cy.getAndValidate('incidence').type(2)
    cy.getAndValidate('severity').type(2)
    cy.getAndValidate('damage').selectFromDropdown()
    cy.getAndValidate('abiotic').type(2)
    cy.getAndValidate('biotic').type(2)

    cy.addImages('addImg', 1, {
      btnIndex: 0,
    })
    cy.getAndValidate('comments', {
      isChildOf: 'recruitment_health',
    }).type(2)

    cy.getAndValidate('height').type('10')
    cy.getAndValidate('canopy_width').type('10')
    cy.getAndValidate('dbh').type('10')
    cy.getAndValidate('point_of_measurement').type('10')

    cy.addImages('addImg', 1, {
      btnIndex: 1,
    })
    cy.dataCy('comments').eq(-1).type('comments')
    cy.dataCy('done').eq(-1).click().wait(1000)
  })

  it('.should() - publish', () => {
    cy.nextStep()
    // cy.workflowPublish(false)
  })
  pub()
})

describe(`2 - Collect ${protocol} (revisit)`, () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })

  it('2.1[test-to-pass] - collect recruitment survivorship survey', () => {
    cy.recruitmentSurvivorshipSurvey({
      vouchers,
    })
  })

  for (const [index, voucher] of vouchers.entries()) {
    it(`2.2[test-to-pass] - collect observation ${index + 1}`, () => {
      cy.recruitmentSurvivorshipObservation({
        voucher,
        shouldAddEntry: index < vouchers.length - 1,
      })
    })
  }

  it('2.3[test-to-pass] - publish', () => {
    cy.nextStep()
    // cy.workflowPublish(false)
  })
  pub()
})

describe('cleanup', () => {
  it('clean survey and observations relating to protocol', () => {
    // Cypress object will only mount on window object if on '/login' but not '/'?
    cy.window().its('Cypress').should('be.an', 'object')
    cy.window().its('constants').should('be.an', 'object')
      .then(() => {
        nukeDatabaseEntriesFor('recruitment-survivorship-surveys')
        nukeDatabaseEntriesFor('recruitment-survivorship-observations')
      })
  })
})

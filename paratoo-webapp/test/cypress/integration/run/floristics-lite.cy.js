const { pub } = require('../../support/command-utils')

const protocol = 'Floristics - Standard'
const protocolCy = 'Floristics\\ -\\ Standard'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'floristics'

describe('Landing', () => {
  it('login', () => {
    cy.login('TestUser', 'password')
  })
  it('start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocolCy)
  })
  it('do plot layout', () => {
    cy.plotLayout()
  })
  it('assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
})

describe('Complete survey select', () => {
  it('complete survey', () => {
    // cy.get('[data-cy="workflowNextButton"]').click().wait(100)
    cy.nextStep()
  })
})

describe('[test-to-fail] Do not allow moving past step with empty data', () => {
  it ('Do not allow empty data', () => {
    cy.testEmptyField(
      'Unable to proceed to the next step due to incomplete data. Please ensure you have pressed the "Add" or "Save" button before continuing',
      'workflowNextButton',
    )
  })
})
describe('search for vouchers', () => {
  it('search', () => {
    const searchText = '7637' //'dev_foo_barcode_789'
  
    cy.get(`[aria-label="Search voucher"]`).type(searchText).wait(100)
    // cy.get(`[data-cy*="collectAgainRow"]`).should('have.length', 4)
    cy.get(`[data-cy*="collectAgainRow"]`, {log: false}).each((val) => {
      cy.get(val, {log: false}).within({log: false}, () => {
        cy.get('span.block:contains(Collect Again)').click()
      })
      // need to check manually as element doesn't have a dataCy and setExpansionitemState expects one
      cy.dataCy('expandVouchInfo', {log: false}).then((expansion) => {
        if (expansion.hasClass('q-expansion-item--collapsed')) {
          cy.get(expansion, {log: false}).click({log: false})
        }
      }).wait(50)
      
      // text is very loose. It just needs to contain at least one number number, anywhere, I think.
      // Finds all matches, but I don't know exactly how the app does it
      cy.get('p:contains(Floristics voucher barcode:)', {log: false}).siblings('p', {log: false}).invoke({log: false}, 'text').then(text => {
        // either just expect a length from the returned values like above, or replicate how the app actually does it.
        const findMatches = (searchText, matches = []) => {
          let characters = ''
          for (const char of searchText) {
            characters = characters += char
            let match
            try {
              match = [...text.matchAll(new RegExp(characters, 'g'))]
            } catch (e){
              throw new Error(`${e}\n\n${text}, ${characters}`)
            }
            if (match.length > 0)
              for (const val of match) {
                if (!matches.some(e => val[0] == e[0] && val.index == e.index)) {
                  matches.push(val)
                }
              }
            else 
              findMatches(searchText.substring(characters.length -1, searchText.length-1), matches)
          }
          return matches
        }
        const matches = findMatches(searchText)
        try {
          cy.wrap(matches).should('have.length.greaterThan', 0)
          cy.log(`Voucher barcode [${text}] contains [${matches.map(e => e[0]).join(', ')}] from the search text: [${searchText}]`)
        } catch(e) {
          throw new Error(`Voucher barcode [${text}] does not match any characters from [${searchText}]`)
        }
      })
      // back button will appear in the expansion item
      cy.dataCy('closeTempVoucher').click()
  
  
    })
    cy.get(`[aria-label="Search voucher"]`).paratooClear().wait(100)
  })
})
describe('Existing vouchers', () => {
  it('collect again, once', () => {
    //vue component has async `setup()`, so need to wait for it to load
    //FIXME no arbitrary waits
    cy.wait(2000)
    //set reader to false to enable images to be taken in the dialog
    cy.setCheck('toggleBarcodeReaderShow', false)

    cy.get('[data-cy="collectAgain"]').eq(0).click()

    // TODO: validateFields is not working for within as it only allow DOM select look for element within the component
    // so it can't get the q-notification
    cy.get('#floristics_virtual_voucher').within(() => {
      // growth form 1
      cy.selectFromDropdown('growth_form_1', 'Vine')
      cy.selectFromDropdown('growth_form_2', 'Aquatic')
      cy.selectFromDropdown('habit', 'Climbing')
      cy.selectFromDropdown('phenology', 'Adult')

      cy.get(`[data-cy="addImg"]`).then((imgList) => {
        for (const [index, button] of Array.from(imgList).entries()) {
          if (index === 0)
            cy.testEmptyField(
              'Field: "Plant Photo": This field is required',
              'done',
              -1,
            )
          if (index === 1)
            cy.testEmptyField(
              'Field: "Leaf Photo": This field is required',
              'done',
              -1,
            )
          cy.get(imgList[index]).click()
          cy.dataCy('comment')
            .eq(index)
            .type("I'm a comment about the photo" + (index + 1))
          cy.get('[data-cy="takePhoto"]').eq(index).click()
          cy.wait(100)
        }
      })
      cy.wait(1000)

      cy.dataCy('done').eq(-1).click()
    })
    cy.get('[data-cy="collectAgain"]')
      .eq(0)
      .should('contain.text', 'Edit Repeat Collection')
  })
})

for (let i = 0; i < 3; i++) {
  describe('species' + (i + 1), () => {
    it('not doing submodule for species', () => {
      cy.dataCy('openPtvModal').should('not.exist')
    })
    it('field name ', () => {
      cy.testEmptyField(
        'Field: "Field Name": This field is required',
        'done',
        -1,
      )
      cy.selectFromSpeciesList('field_name', 'Sphenopsida')
    })

    it('barcode ', () => {
      cy.testEmptyField(
        'Field: "Voucher Barcode": This field is required',
        'done',
        -1,
      )
      cy.setCheck('toggleBarcodeReaderShow', true)
      cy.dataCy('recordBarcode').click()
      cy.wait(200)
    })

    it('growth form 1', () => {
      cy.testEmptyField(
        'Field: "Growth Form 1": This field is required',
        'done',
        -1,
      )

      cy.dataCy('growth_form_1').click().type('{esc}')
      cy.dataCy('title').eq(0).click()
      cy.get('.q-field__messages > div').should(
        'contain.text',
        'This field is required',
      )
      cy.selectFromDropdown('growth_form_1', 'Tree')
    })

    it('growth form 2 (optional)', () => {
      cy.selectFromDropdown('growth_form_2', 'Tree-fern')
    })

    it('habit', () => {
      cy.selectFromDropdown('habit', ['Erect', 'Decumbent', 'Creeping'])
    })

    it('phenology', () => {
      cy.selectFromDropdown('phenology', ['Adult', 'Leaf', 'Fruit']).type(
        '{esc}',
      )
    })
    it('add 3 photos', () => {
      for (let j = 0; j < 3; j++) {
        cy.wait(500)
        cy.addImages('addImg', 1)
        cy.dataCy('comment').type('comment' + (i + 1))
        cy.dataCy('done').eq(0).click()
        if (j < 2) cy.dataCy('addEntry').eq(0).click()
      }
    })

    it('save species record', () => {
      cy.dataCy('done').eq(-1).click()
      if (i < 2) cy.dataCy('addEntry').eq(-1).click()
    })
  })
}

describe('Publish', () => {
  it('end survey', () => {
    cy.nextStep()
  })
  pub({ shapeShape: true, shareValues: true, shareValuesWith: false })
})

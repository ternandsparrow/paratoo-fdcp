const { pub } = require('../../support/command-utils')

const protocolCy = 'Photopoints\\ -\\ Compact\\ Panorama'
const protocol = 'Photopoints - Compact Panorama'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'photopoints'

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocolCy)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
  it('.should() - fail to go to next step if 3 points are not collected', () => {
    cy.dataCy('centrePostPositionedYes').click()
    cy.dataCy('nextPoint').should('not.exist')
    cy.recordLocation({
      btnEqIndex: 0,
    })
    cy.testEmptyField('Require 3 photopoint positions', 'workflowNextButton')
  })
  it('.should() - collect photopoints lite compact', () => {
    for (let i = 0; i < 3; i++) {
      cy.recordLocation({
        btnEqIndex: 1,
        recordBtnIndex: 1,
      })
      cy.dataCy('nextPoint').should('exist')
      cy.dataCy('qrcode').should('be.visible')
      if (i < 2) {
        cy.get('[data-cy="nextPoint"]').click()
        cy.dataCy('qrcode').should('not.exist')
      }
    }
    cy.nextStep()
  })
  pub()
  // it('.should() - publish', () => {
  //   cy.workflowPublish(false)
  // })
})

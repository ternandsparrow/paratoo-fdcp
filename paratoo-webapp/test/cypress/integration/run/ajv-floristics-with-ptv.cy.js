const { pub } = require('../../support/command-utils')

const protocolCy = 'Floristics\\ -\\ Enhanced'
const module = 'floristics'

global.ignorePaths = ['floristics-veg-genetic-voucher.has_replicate']

function doFloristics() {
  it('.should() - assert navigation to ' + protocolCy + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
  it('.should() - collection floristics with PTV', () => {
    cy.ptvSubmoduleInFloristicsSurveyStep('full')
    cy.collectPtvAsSubmodule(0, 'full')
    cy.nextStep()
  })
}

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
})
describe('do floristics with PTV with error, edit and submit', () => {
  doFloristics()
  // we don't need to break the data of floristics as it's just normal case
  it('.should() - corrupt collection data of PTV in the store', () => {
    cy.nextStep()

    cy.window().then((win) => {
      // required field
      delete win.bulkStore.collections[14]['floristics-veg-genetic-voucher'][0]
        .replicate[0].genetic_voucher_barcode

      // min value is 1
      win.bulkStore.collections[14][
        'floristics-veg-genetic-voucher'
      ][0].replicate[0].replicate_number = 0
    })
  })
  it('.should() - receive ajv errors when try to queue collection', () => {
    cy.dataCy('workflowPublish').click()

    cy.dataCy('ajv-error-0').should(
      'contain.text',
      'Field "Genetic Voucher Barcode" is missing at "Plant Tissue Vouchering/1/Replicate/1"',
    )

    cy.dataCy('ajv-error-1').should(
      'contain.text',
      'Error at "Plant Tissue Vouchering/1/Replicate/1/Replicate Number": must be >= 1',
    )
  })
  it('.should() - go back to the related step having ajv error if clicking edit button', () => {
    cy.dataCy('ajv-edit-0').click()
    cy.dataCy('title', { timeout: 60000 })
      .eq(0)
      .should('contain.text', 'Floristics voucher - Enhanced')
  })
  it('.should() - the two ajv errors should be visible and formatting error should be an expansion item', () => {
    // should have class q-expansion-item
    cy.dataCy('ajv-error-0')
      .should('be.visible')
      .should('have.class', 'q-expansion-item')
    cy.dataCy('ajv-error-1')
      .should('be.visible')
      .should('have.class', 'q-expansion-item')
  })
  it('.should() - fix the data and queue', () => {
    cy.collectPtvAsSubmodule(0, 'full')
    cy.nextStep()
  })
  pub({ shapeShape: true, shareValues: true, shareValuesWith: false })
})

let showAjvErrorsBtn
describe('do photopoints with error, then stash it', () => {
  doFloristics()
  it('.should() - corrupt collection data in the store', () => {
    cy.nextStep()

    cy.window().then((win) => {
      // required field
      delete win.bulkStore.collections[14]['floristics-veg-genetic-voucher'][0]
        .replicate[0].genetic_voucher_barcode
    })
  })
  it('.should() - receive ajv errors when try to queue collection, then stash the collection', () => {
    cy.dataCy('workflowPublish').click()

    cy.dataCy('ajv-error-0').should(
      'contain.text',
      'Field "Genetic Voucher Barcode" is missing at "Plant Tissue Vouchering/1/Replicate/1"',
    )
    cy.dataCy('workflowPublishConfirm').click()
    cy.dataCy('submitQueuedCollectionsBtn').should('not.exist')
    cy.dataCy('showAjvErrorsBtn').should('have.length.greaterThan', 0)
    cy.dataCy('showAjvErrorsBtn').then((_) => (showAjvErrorsBtn = _.length))
  })
})
describe('collect new floristics and sync, the queued items with ajv errors should be the same', () => {
  doFloristics()
  pub({ shapeShape: true, shareValues: true, shareValuesWith: false })
  it('.should() - the number of queued items should be the same', () => {
    cy.dataCy('showAjvErrorsBtn').should('have.length', showAjvErrorsBtn)
  })
})

const { pub } = require("../../support/command-utils")

const protocol = 'Invertebrate Fauna - Pan Trapping'
const protocolCy = 'Invertebrate\\ Fauna\\ -\\ Pan\\ Trapping'
const module = 'invertebrate fauna'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'


global.ignorePaths = []
for (let i = 0; i <= 23; i++) {
  global.ignorePaths.push(`remove-pan-trap.${i}.start_date`)
  global.ignorePaths.push(`remove-pan-trap.${i}.duration`)
}

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  // only being skipped temporarily as there is an unfinished protocol inside the module
  it.skip('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocolCy)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout(true, null, {
      layout: 'QDASEQ0003',
      visit: 'QLD spring survey',
    })
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
})

describe('1 - survey setup', () => {
  it('1.1[validate-field] - Complete Pan Survey set up', () => {
    cy.getAndValidate('monitoring_duration', { isComponent: true })
    cy.dataCy('Short-term').click()

    const modifier = Math.floor(Date.now())
    cy.get('[data-cy="observers"]').eq(0).click().type(`observer-${modifier}`)
    cy.dataCy('addObserver').click()

    cy.nextStep()
  })
})

describe('2 - add traps', () => {
  it('2.1[test-to-pass] - Open  trap layout and close', () => {
    cy.dataCy('navigate').click()
    cy.dataCy('close').click()
  })
  it('2.1[validate-field] - first trap', () => {
    cy.dataCy('recordTrapLocationBtn').click()
    cy.getAndValidate('proposed_trapping_duration').clear().type(7)
    cy.getAndValidate('inside_pan_colour').selectFromDropdown('White')
    cy.getAndValidate('outside_pan_colour').selectFromDropdown('Blue')
    cy.getAndValidate('pan_diameter').type(5)
    cy.getAndValidate('pan_depth').type(5)
    cy.getAndValidate('pan_capacity').type(5)

    cy.getAndValidate('pan_placement').selectFromDropdown('On ground')

    cy.getAndValidate('pan_height_above_ground').type(5)

    //auto filled field
    // cy.dataCy('liquid_type').clear().getAndValidate().selectFromDropdown(
    //   'Water-Dishwashing Solution',
    // )

    cy.getAndValidate('liquid_amount').type(10)

    cy.dataCy('done').click()
  })

  it('2.1[test-to-pass] - auto fill next traps', () => {
    cy.dataCy('inside_pan_colour').children('span').should('contain', 'White')
    cy.dataCy('outside_pan_colour').children('span').should('contain', 'Blue')

    cy.dataCy('pan_diameter').should('have.value', 5)
    cy.dataCy('pan_depth').should('have.value', 5)
    cy.dataCy('pan_capacity').should('have.value', 5)

    cy.dataCy('pan_placement').children('span').should('contain', 'On ground')

    cy.dataCy('pan_height_above_ground').should('have.value', 5)

    // cy.dataCy('liquid_type').should('contain', 'Water-Dishwashing Solution')

    cy.dataCy('liquid_amount').should('have.value', 10)
  })
  it('2.1[test-to-pass] - complete remaining traps', () => {
    for (let i = 0; i < 23; i++) {
      cy.dataCy('recordTrapLocationBtn').click()

      cy.dataCy('done').click()
    }
  })
})

describe('3 - remove traps', () => {
  it('3[test-to-pass] - Complete removing all traps', () => {
    cy.nextStep()
    cy.dataCy('removeAllTrapsBtn').click()
    cy.dataCy('confirmRemoveTrapBtn')
      .should('exist')
      .then(() => {
        cy.dataCy('confirmRemoveTrapBtn').click()
      })

    cy.nextStep()
  })
})

describe('4 - vouchering', () => {
  it('4.1[validate-field] - Complete vouchering first 12 traps', () => {
    // select traps
    const trapsToSelect = []
    for (let i = 1; i <= 12; i++) {
      trapsToSelect.push(`PT1-${i}`)
    }
    cy.getAndValidate('assigned_traps', {isComponent :true})
      .selectFromDropdown(trapsToSelect)
      .type('{esc}')

    cy.getAndValidate('barcode', { isComponent: true })
    cy.dataCy('recordBarcode').click()

    cy.getAndValidate('preservation_type').selectFromDropdown()

    cy.getAndValidate('preservation_concentration').type(10)

    cy.getAndValidate('sample_photo', { isComponent: true })

    cy.getAndValidate('single_photo', { isComponent: true })
    cy.dataCy('addImg').click()
    cy.dataCy('takePhoto').click()
    cy.dataCy('done').eq(0).click()
    cy.getAndValidate('comment').type('comment')

    cy.dataCy('done').eq(-1).click()
  })

  it('4.2[test-to-pass] - Complete remaining traps', () => {
    cy.dataCy('addEntry').eq(-1).click().wait(1000)
    // select traps
    const trapsToSelect = []
    for (let i = 1; i <= 12; i++) {
      trapsToSelect.push(`PT2-${i}`)
    }
    cy.getAndValidate('assigned_traps', {isComponent :true})
      .selectFromDropdown(trapsToSelect)
      .type('{esc}')

    cy.dataCy('recordBarcode').click()

    cy.dataCy('preservation_concentration').type(10)

    cy.dataCy('preservation_type').selectFromDropdown()

    cy.dataCy('addImg').click()
    cy.dataCy('takePhoto').click()
    cy.dataCy('done').first().click()
    cy.dataCy('comment').type('comment')

    cy.dataCy('done').eq(-1).click()
  })
})

describe('5 - publish', () => {
  it('[test-to-pass] - publish', () => {
    cy.nextStep()
    // cy.workflowPublish(true) // force is true incase a q-notification warning is blocking it
  })
  pub() // force wil be false by default
})

const { pub } = require("../../support/command-utils")

const protocol = 'Invertebrate Fauna - Malaise Trapping'
const protocolCy = 'Invertebrate\\ Fauna\\ -\\ Malaise\\ Trapping'
const module = 'invertebrate fauna'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

// remove-malaise-trap schema only has end date and comment.
// label is just trapId and is app only
global.ignorePaths = [
  'remove-malaise-trap.0.trapId', 
  'remove-malaise-trap.0.start_date', 
  'remove-malaise-trap.0.duration', 
  'remove-malaise-trap.1.trapId', 
  'remove-malaise-trap.1.start_date', 
  'remove-malaise-trap.1.duration',
  'malaise-trap-vouchering.0.label', 
  'malaise-trap-vouchering.1.label'
]

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout(true, null, {
      layout: 'QDASEQ0003',
      visit: 'QLD spring survey',
    })
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
})

describe('1 - Survey setup', () => {
  it('1.1[validate-field] - observers]', () => {
    cy.getAndValidate('observers', { isComponent :true })
    cy.nextStep()
  })
})

describe('2 - add traps', () => {
  it('2.1[validate-field] - trap 1 - direction and photos', () => {
    cy.dataCy('trap', { timeout: 3000 }).click()

    cy.getAndValidate('direction').selectFromDropdown()

    cy.dataCy('moth_excluder').click({ force: true })

    cy.recordLocation()

    cy.getAndValidate('trap_photo', { isComponent: true })
    const directions = ['West', 'North', 'East', 'South']
    directions.forEach((direction, index) => {
      if (index === 0) {
        // cy.selectFromDropdown('direction', direction, null, 1)
        cy.getAndValidate(
          'direction',
          {
            testEmptyField: ['takePhoto'],
            isChildOf: 'trap_photo',
          },
          null,
          1,
        ).selectFromDropdown(direction)

        cy.getAndValidate('description', {
          testEmptyField: ['takePhoto'],
        }).type(`image ${index + 1} description{enter}bla bla`)

        cy.get('[data-cy="takePhoto"]').click()
      } else {
        // cy.selectFromDropdown('direction', direction, null, 1)
        cy.dataCy('direction').eq(1).selectFromDropdown(direction)

        cy.dataCy('description').type(
          `image ${index + 1} description{enter}bla bla`,
        )
        cy.get('[data-cy="takePhoto"]').click()
        cy.wait(50)
      }
    })

    cy.getAndValidate('weather', { isComponent: true })
    cy.completeWeatherForm()

    cy.getAndValidate('comment').type('bla bla')

    cy.dataCy('done').click()
  })

  it('2.2[test-to-pass] - trap 2 - direction and photos', () => {
    cy.dataCy('addEntry').click()

    cy.dataCy('direction').eq(0).selectFromDropdown()

    cy.dataCy('moth_excluder').click({ force: true })

    const directions = ['West', 'North', 'East', 'South']
    directions.forEach((direction, index) => {
      cy.dataCy('direction').eq(1).selectFromDropdown(direction)

      cy.dataCy('description').type(
        `image ${index + 1} description{enter}bla bla`,
      )
      cy.get('[data-cy="takePhoto"]').click()
      cy.wait(50)
    })

    cy.completeWeatherForm()

    cy.dataCy('comment').type('bla bla')
    cy.recordLocation()

    cy.dataCy('done').click()
  })
})

describe('3 - Add samples ', () => {
  it('3.1[validate-field] - Add a sample  for trap 1', () => {
    cy.nextStep()
    cy.dataCy('addSampleBtn').click()

    cy.getAndValidate('voucher_type', { isComponent: true })
    cy.dataCy('Dry').click({ force: true })

    cy.getAndValidate('trap_preservative').selectFromDropdown('Frozen')

    cy.getAndValidate('preservative_concentration').type(22)

    cy.completeWeatherForm()

    cy.dataCy('replace_collection_bottle').click({ force: true })

    cy.get('[data-cy="done"]').click()

    cy.wait(50)
  })

  it('3.1[validate-field] - Add a sample  for trap 2', () => {
    cy.selectFromDropdown('trap_id', 'QDASEQ0003-2')

    cy.dataCy('addSampleBtn').click()

    cy.dataCy('voucher_type', { isComponent: true })
    cy.dataCy('Dry').click({ force: true })

    cy.dataCy('trap_preservative').selectFromDropdown('Frozen')

    cy.dataCy('preservative_concentration').type(22)

    cy.completeWeatherForm()

    cy.dataCy('replace_collection_bottle').click({ force: true })

    cy.get('[data-cy="done"]').click()

    cy.wait(50)
  })
})

describe('4 - remove traps', () => {
  it('4.1[test-to-pass] - Complete removing all traps', () => {
    cy.nextStep()
    cy.dataCy('removeAllTrapsBtn').click({ force: true })
    cy.dataCy('confirmRemoveTrapBtn')
      .should('exist')
      .then(() => {
        cy.dataCy('confirmRemoveTrapBtn').click({ force: true })
      })
  })
})

describe('5 - vouchering', () => {
  it('5.1[validate-field] - Complete vouchering 1', () => {
    cy.nextStep()

    cy.getAndValidate('barcode', {
      isComponent: true,
      testEmptyField: ['nextTrap'],
    })
    cy.dataCy('recordBarcode').click({ force: true })

    cy.getAndValidate('sample_photo', {
      isComponent: true,
      testEmptyField: ['nextTrap'],
    })

    cy.getAndValidate('single_photo', {
      isComponent: true,
    })
    cy.dataCy('addImg').click({ force: true })
    cy.dataCy('takePhoto').click({ force: true })
    cy.getAndValidate('comment').type('asdadsadsa')
    cy.dataCy('done').click()

    cy.dataCy('nextTrap').click({ force: true })
  })

  it('5.1[test-to-pass] - Complete vouchering 2', () => {
    cy.setExpansionItemState('sample_photo_1_expansion', 'open')
    cy.dataCy('recordBarcode').click({ force: true })

    cy.dataCy('addImg').click({ force: true })
    cy.dataCy('takePhoto').click({ force: true })
    cy.dataCy('comment').type('asdadsadsa')
    cy.dataCy('done').click()

    cy.dataCy('nextTrap').click({ force: true })
  })
})

describe('6 - biomass', () => {
  it('6.1[validate-field] - Complete measuring biomass of sample 1', () => {
    cy.nextStep()

    cy.dataCy('trap_number')
      .closest('.q-select')
      .click()
      .then(() => {
        cy.get('.q-virtual-scroll__content > .q-item')
          .eq(0)
          .click({ force: true })
      })

    cy.dataCy('empty_filter_weight').validateField().type(1)
    cy.dataCy('filter_weight_with_sample').validateField().type(2)
    cy.get('[data-cy="done"]').click()
  })

  it('6.2[test-to-pass] - Complete measuring biomass of sample 2', () => {
    cy.dataCy('empty_filter_weight').type(1)
    cy.dataCy('filter_weight_with_sample').type(5)
    // cy.dataCy('sample_biomass').parent('.q-input').should('contain', '4') //TODO: test auto fill
    cy.get('[data-cy="done"]').click()
  })
})

describe('7 - submit', () => {
  it('7.1[test-to-pass] - Submit', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

const { pub } = require("../../support/command-utils")

const protocol = 'Basal Area - Basal Wedge'
const protocolCy = 'Basal\\ Area\\ –\\ Basal\\ Wedge'
const module = 'basal\\ area'

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
    cy.get('[data-cy="workflowNextButton"]').click()
  })

})
describe('Basal wedge', () => {
  const samplingPoints = [
    'Northwest',
    'North',
    'Northeast',
    'East',
    'Southeast',
    'South',
    'Southwest',
    'West',
    'Centre',
  ]
  const vouchers = [
    'Voucher Full Example 1 [dev_foo_barcode_987]',
    'Voucher Full Example 2 [dev_foo_barcode_654]',
    'Voucher Full Example 3 [dev_foo_barcode_321]',
    'Voucher Full Example 4 [dev_foo_barcode_789]',
    'Voucher Full Example 5 [dev_foo_barcode_456]',
    'Voucher Lite Example 1 [dev_foo_barcode_987]',
    'Voucher Lite Example 2 [dev_foo_barcode_982123]',
  ]
  function pickRandomVouchers() {
    const count = Math.floor(Math.random() * (vouchers.length - 1)) + 1
    const shuffled = vouchers.sort(() => 0.5 - Math.random())
    return shuffled.slice(0, count)
  }
  // select sampling point
  // the command selectFromDropdown doesn't work really well for words have matching characters
  // eg: 'North' and 'North West'
  function selectPoint(point) {
    cy.dataCy('basal_sweep_sampling_point').click().wait(50)
    cy.withinSelectMenu({
      persistent: true,
      fn: () => {
        cy.get('.q-item[role=option]').as('dropdownOptions')
        cy.get('@dropdownOptions')
          .filter((index, element) => {
            return Cypress.$(element).text() === point
          })
          .eq(0)
          .scrollIntoView()
          .click()
          .wait(200)
      },
    })
  }
  samplingPoints.forEach((samplingPoint, index) => {
    const label =
      samplingPoint === 'Northwest'
        ? `1.${index + 1}-[validate-field]`
        : `1.${index + 1}-[test-to-pass]`
    it(
      label + 'Basal Area – Basal Wedge (Sampling Point ' + samplingPoint + ')',
      function () {
        switch (samplingPoint) {
          case 'Northwest':
            cy.getAndValidate('basal_sweep_sampling_point').selectFromDropdown(
              samplingPoint,
            )
            cy.getAndValidate('floristics_voucher')
              .selectFromDropdown(pickRandomVouchers(vouchers))
              .type('{esc}')
            cy.getAndValidate('basal_area_factor').selectFromDropdown('0.1')
            cy.getAndValidate('in_tree').type('1')
            cy.getAndValidate('borderline_tree').type('4')
            // custom condition
            cy.testEmptyField(
              'In and Borderline counts together should be greater than or equal to 7',
              'done',
            )
            cy.get('[data-cy="borderline_tree"]').type('6')
            cy.get('[data-cy="done"]').click().wait(50)
            break
          case 'North':
            // in tree is required field
            cy.testEmptyField(
              'Missing sampling point(s): N, NE, E, SE, S, SW, W, C',
              'workflowNextButton',
              null,
              0,
            )
            cy.get('[data-cy="addEntry"]').eq(0).click().wait(50)
            selectPoint(samplingPoint)
            cy.get('[data-cy="group_species"]').click()
            // grouped species 1
            cy.selectFromDropdown(
              'floristics_voucher',
              pickRandomVouchers(vouchers),
            ).type('{esc}')
            cy.selectFromDropdown('basal_area_factor', '0.1')
            cy.get('[data-cy="in_tree"]').type('7')
            cy.get('[data-cy="done"]').eq(0).click()
            cy.get('[data-cy="borderline_tree"]').type('6')
            cy.get('[data-cy="done"]').eq(0).click()
            break
          default:
            cy.get('[data-cy="addEntry"]').eq(0).click().wait(50)
            selectPoint(samplingPoint)
            cy.get('[data-cy="group_species"]').click()
            // grouped species 1
            cy.selectFromDropdown(
              'floristics_voucher',
              pickRandomVouchers(vouchers),
            ).type('{esc}')
            cy.selectFromDropdown('basal_area_factor', '0.1')
            cy.get('[data-cy="in_tree"]').type('7')
            cy.get('[data-cy="done"]').eq(0).click()
            cy.get('[data-cy="borderline_tree"]').type('6')
            cy.get('[data-cy="done"]').eq(0).click()
            break
        }
      },
    )
  })

  it('survey end', () => {
    cy.nextStep()
  })
  pub()
})

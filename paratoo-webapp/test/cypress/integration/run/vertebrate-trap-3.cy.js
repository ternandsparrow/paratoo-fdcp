// trapping setup survey
// each vert trapping test is numbered , so it will be executed in order

const { pub } = require("../../support/command-utils")

//  setup traps -> check traps -> end or remove traps
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'vertebrate fauna'
const protocol = 'Vertebrate Fauna - Trapping Survey Closure'

const trapNumbers = Cypress.env('VERT_TRAP_NUMBERS')
describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  // it('.should() - start collection button should be disabled', () => {
  //   cy.selectProtocolIsDisabled(module, protocol)
  // })
  it('.should() - do plot layout', () => {
    cy.plotLayout(false, null, {
      layout: 'QDASEQ0003',
      visit: 'QLD spring survey',
    })
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})

describe('End traps', () => {
  it('start', function () {
    cy.nextStep()
  })
  for (const trapNumber of trapNumbers) {
    it(`Close trap ${trapNumber}`, function () {
      if (trapNumber === 'P1') {
        cy.dataCy('trap_number').selectFromDropdown(trapNumber, trapNumber, -1)
      }

      // only pitfall traps have 'Pitfall closed' status
      // which have a 'P' prefix
      if (/\bP\w*/.test(trapNumber)) {
        if (trapNumber === 'P1') {
          cy.getAndValidate('trap_status', {
            testEmptyField: ['done', -1],
          }).selectFromDropdown('Pitfall closed - filled in')

          cy.setCheck('toggleBarcodeReaderShow', false)
          cy.getAndValidate('closed_pitfall_photo', {
            testEmptyField: ['done', -1],
            isCoreComponent: true,
          })
          cy.dataCy('addImg').click().wait(500)
          cy.dataCy('takePhoto').click().wait(500)
        } else {
          cy.selectFromDropdown('trap_status', 'Pitfall closed - filled in')
          cy.setCheck('toggleBarcodeReaderShow', false)
          cy.dataCy('addImg').click().wait(500)
          cy.dataCy('takePhoto').click().wait(500)
        }
      } else {
        cy.selectFromDropdown('trap_status', 'Trap closed - permanently')
      }

      cy.dataCy('done').click()
    })
  }
})

describe('Take end drift fences photos and submit survey', () => {
  it(`Take photo of drift B`, function () {
    cy.nextStep()

    cy.dataCy('drift_line').selectFromDropdown('B')
    cy.dataCy('takePhoto').click()
  })
  it(`Take photo of drift E`, function () {
    cy.selectFromDropdown('drift_line', 'E')
    cy.dataCy('takePhoto').click()
  })

  it('Submit survey', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

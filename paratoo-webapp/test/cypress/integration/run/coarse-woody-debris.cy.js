const { pub } = require("../../support/command-utils")

const protocol = 'Coarse Woody Debris'
const protocolCy = 'Coarse\\ Woody\\ Debris'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'coarse\\ woody\\ debris'

describe('0-Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })
})

describe('1 - survey step', () => {
  it('1.1[validate-field] - finish survey step', () => {
    cy.getAndValidate('sampling_survey_method', { isComponent: true })
    cy.get('[data-cy="40 x 40 m"]').click()

    cy.getAndValidate('sampling_survey_cut_off', { isComponent: true })

    cy.get('[data-cy="10 cm"]').click() // selecting Sampling Survey Cut Off (Diameter)

    cy.getAndValidate('sampling_survey_cut_off_length', { isComponent: true })
    cy.get('[data-cy="50 cm"]').click() // selecting Sampling Survey Cut Off (Length)
    cy.nextStep()
  })
})

describe('2 - observation step', () => {
  it('2.0[validate-field] - shound not be able to have cwd and tree stumps at the same time', () => {
    cy.dataCy('CWD').click()
    cy.dataCy('tree_stumps').should('not.be.checked')

    cy.dataCy('tree_stumps').click()
    cy.dataCy('CWD').should('not.be.checked')
    cy.dataCy('tree_stumps').click()
  })
  it('2.1[validate-field] - observation 1', () => {
    // observation 1
    cy.recordLocation()

    cy.dataCy('CWD_section').should('not.exist')
    cy.dataCy('CWD').click()
    cy.dataCy('CWD_section').should('exist')

    // cwd section 1
    // cy.getAndValidate('widest_diameter_cm').clear().type('6')
    cy.getAndValidate('widest_diameter_cm', {
      testEmptyField: ['done', 1],
    }).type(10)
    // minimum is 10 as we selected 10cm Survey Cut Off
    //FIXME: Custom condition
    cy.getAndValidate('narrowest_diameter_cm', {
      testEmptyField: ['done', 1],
    }).type(10)

    cy.getAndValidate('length_cm').clear().type('100')

    cy.getAndValidate('decay_class').selectFromDropdown('Class 1')
    cy.get('[data-cy="done"]').eq(1).click().wait(3000)

    // cwd section 2
    cy.get('[data-cy="addEntry"]').eq(0).click()
    cy.get('[data-cy="widest_diameter_cm"]').clear().type('10')
    cy.get('[data-cy="narrowest_diameter_cm"]').clear().type('11')
    cy.get('[data-cy="length_cm"]').clear().type('50')
    cy.selectFromDropdown('decay_class', 'Class 2')
    cy.get('[data-cy="addImg"]').click().wait(50)
    cy.dataCy('takePhoto').click().wait(50)
    cy.get('[data-cy="done"]').eq(0).click().wait(3000) //save photo
    cy.get('[data-cy="done"]').eq(0).click() //save CWD section

    // TODO: find a way to assert that the tree stumps section is disabled
    // cy.get('[data-cy="tree_stumps"]').should('be.disabled')

    cy.get('[data-cy="done"]').eq(0).click().wait(3000) //saving observation
  })
  it('2.2[test-to-pass] - observation 2', () => {
    // observation 2
    cy.get('[data-cy="addEntry"]').click()
    cy.recordLocation()

    cy.dataCy('CWD_section').should('not.exist')
    cy.dataCy('CWD').click()
    cy.dataCy('CWD_section').should('exist')

    cy.getAndValidate('widest_diameter_cm', {
      testEmptyField: ['done', 1],
    }).type(10)
    // minimum is 10 as we selected 10cm Survey Cut Off
    //FIXME: Custom condition
    cy.getAndValidate('narrowest_diameter_cm', {
      testEmptyField: ['done', 1],
    }).type(10)

    cy.getAndValidate('length_cm').clear().type('100')

    cy.selectFromDropdown('decay_class', 'Class 3')
    cy.get('[data-cy="done"]').eq(0).click().wait(3000)
    // cwd section 2
    cy.get('[data-cy="addEntry"]').eq(0).click()
    cy.get('[data-cy="widest_diameter_cm"]').clear().type('50')
    cy.get('[data-cy="narrowest_diameter_cm"]').clear().type('50')
    cy.get('[data-cy="length_cm"]').clear().type('50')
    cy.selectFromDropdown('decay_class', 'Class 4')
    cy.get('[data-cy="done"]').eq(0).click()
    cy.get('[data-cy="addImg"]').click().wait(50)
    cy.dataCy('takePhoto').click().wait(50)
    cy.get('[data-cy="done"]').eq(0).click().wait(3000)
    cy.get('[data-cy="done"]').eq(0).click().wait(3000) //saving record
  })
  it('2.3[test-to-pass] - observation 3', () => {
    // observation 3
    cy.get('[data-cy="addEntry"]').last().click()
    cy.recordLocation()

    //tree stump (only 1)
    cy.dataCy('tree_stump').should('not.exist')
    cy.dataCy('tree_stumps').click()
    cy.dataCy('tree_stump').should('exist')
    cy.getAndValidate('height').type('10')
    cy.getAndValidate('diameter').type('10')
    cy.selectFromDropdown('decay_class', 'Class 2')
    cy.get('[data-cy="addImg"]').click().wait(50)
    cy.dataCy('takePhoto').click().wait(50)
    cy.get('[data-cy="done"]').eq(0).click().wait(3000) //save photo
    cy.get('[data-cy="done"]').eq(0).click() //save tree stump section
    // cy.dataCy('CWD').should('be.disabled')
  })
})

describe('3 - publish', () => {
  it('3.1[test-to-pass] - publish successfully', function () {
    cy.nextStep()
    // cy.workflowPublish(false)
  })
  pub()
})

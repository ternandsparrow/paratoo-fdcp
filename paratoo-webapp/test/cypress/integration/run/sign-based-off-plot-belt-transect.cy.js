const { pub } = require("../../support/command-utils")

const protocol = 'Sign-based Fauna Surveys - Off-plot Belt Transect'
const module = 'Sign-based Fauna Surveys'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
  it('mock location', () => {
    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
    })
  })
})

const transectId = []
const setupIds = []
describe(`${protocol} - create new`, () => {
  context('1. transect setup', () => {
    it('1.1[validate-field] - transect 1', () => {
      cy.offPlotTransectSetup1New().then((id) => {
        transectId.push(id)
      })
    })

    it('1.2[test-to-pass] - transect 2', () => {
      cy.offPlotTransectSetup2New().then((id) => {
        transectId.push(id)
      })
    })

    it('1.3[test-to-pass] - transect 3', () => {
      cy.offPlotTransectSetup3New().then((id) => {
        transectId.push(id)
      })
    })

    it('finish transect setup', () => {
      cy.log(`transectId: ${JSON.stringify(transectId)}`)
      cy.nextStep()
    })
  })

  context('2. survey setup', () => {
    it(`2.1[validate-field] - survey setup transect 1`, () => {
      cy.offPlotSurveySetup1(transectId[0], 'sign_off').then((id) => {
        setupIds.push(id)
      })
    })
    it(`2.2[test-to-pass] - survey setup transect 2`, () => {
      cy.offPlotSurveySetup2(transectId[1], 'sign_off').then((id) => {
        setupIds.push(id)
      })
    })
    it('finish survey setup', () => {
      cy.log(`setupIds: ${JSON.stringify(setupIds)}`)
      cy.nextStep()
    })
  })

  //TODO check the notification shows (and has correct message) when we click `nextPointBtnLabel`.wait(500)
  context('3. conduct the survey', () => {
    it(`3.1[test-to-fail] - survey for transect 1 (start)`, () => {
      //'done' button ('start transect') shouldn't exist until we select a transect ID
      cy.dataCy('done').should('not.exist')
      cy.selectFromDropdown('transect_number', transectId[0])
      cy.dataCy('done').should('exist')

      //start location
      cy.recordLocation({
        btnEqIndex: 0,
      })

      //this gets autofilled
      cy.dataCy('setup_ID').should('contain.text', setupIds[0])

      cy.getAndValidate('transect_photo', { isComponent: true })
      //FIXME `addImages` doesn't work
      cy.dataCy('addImg').eq(0).click().wait(50)
      cy.dataCy('takePhoto').eq(0).click().wait(50)

      //quadrat number dropdown won't exist until we 'start transect'
      cy.dataCy('quadrat_number').should('not.exist')
      cy.dataCy('done')
        .should('contain.text', 'Start Transect')
        .eq(-1)
        .click()
        //text changes once we start the transect
        .should('contain.text', 'Update Transect Start')
      cy.dataCy('quadrat_number').should('exist')
    })
    it(`3.2[test-to-fail] - survey for transect 1 quadrat 1 (1 observation)`, () => {
      //only need to select first; rest are auto-select as we move quadrats
      cy.selectFromDropdown('quadrat_number', 'Quadrat 1')

      //'signs observed' component doesn't show unless we check the box
      cy.dataCy('sign_observed').should('not.exist')
      cy.dataCy('signs_present').click()
      cy.dataCy('sign_observed').should('exist')

      cy.getAndValidate('sign_type').selectFromDropdown('Warrens')
      cy.getAndValidate('attributable_fauna_species')
        .last()
        .selectFromDropdown('Fallow deer (Dama dama)')
      cy.getAndValidate('number_of_entrance').type(2)
      cy.getAndValidate('sign_age').selectFromDropdown()
      cy.getAndValidate('age_class').selectFromDropdown()
      cy.getAndValidate('count').type(2)
      cy.getAndValidate('photo', { isComponent: true })
      cy.dataCy('addImg').eq(-1).click()
      cy.dataCy('takePhoto').eq(-1).click()
      //FIXME addImages photo breaks when it (accidentally?) opens preview of the photo that was taken
      // cy.addImages('photo', 1, {
      //   btnIndex: -1,
      // })
      cy.dataCy('observation_comments').type('comments')
      cy.dataCy('done').eq(-1).click()
      cy.dataCy('nextPointBtnLabel').click().wait(500)
    })
    it(`3.3[test-to-past] - survey for transect 1 quadrats 2-6 (no data)`, () => {
      for (let i = 0; i < 5; i++) {
        cy.dataCy('nextPointBtnLabel').click().wait(500)
      }
    })
    for (let i = 0; i < 3; i++) {
      //TODO count the 3 expansion items to check all observations were added for this quadrat
      it(`3.${
        4 + i
      }[test-to-pass] - survey for transect 1 quadrat 7 (observation number ${
        i + 1
      })`, () => {
        if (i === 0) cy.dataCy('signs_present').click()
        cy.selectFromDropdown('sign_type', 'Warrens')
        cy.selectFromDropdown(
          'attributable_fauna_species',
          'drop bear',
          null,
          -1,
        )
        cy.dataCy('number_of_entrance').type(1)
        cy.selectFromDropdown('sign_age', null)
        cy.selectFromDropdown('age_class', null)
        cy.dataCy('count').type(1)
        cy.dataCy('done').eq(-1).click()
        if (i < 2) {
          cy.dataCy('addEntry').click()
        } else {
          cy.dataCy('nextPointBtnLabel').click().wait(500)
        }
      })
    }
    it(`3.7[test-to-past] - survey for transect 1 quadrats 8-9 (no data)`, () => {
      for (let i = 0; i < 2; i++) {
        cy.dataCy('nextPointBtnLabel').click().wait(500)
      }
    })
    it(`3.8[test-to-past] - survey for transect 1 quadrat 10 and end the transect`, () => {
      //on the last quadrat, check for the transect end location
      cy.dataCy('transect_end_location').should('not.exist')
      cy.dataCy('nextPointBtnLabel').click().wait(500)
      cy.dataCy('transect_end_location').should('exist')
      //end location
      cy.recordLocation({
        btnEqIndex: 1,
      })

      cy.dataCy('done')
        .eq(-1)
        .should('contain.text', 'End Transect')
        .click()
        //text changes once we end the transect
        .should('contain.text', 'Update Transect End')
    })
    it(`3.9[test-to-pass] - survey for transect 2`, () => {
      //TODO add some observations to the quadrats (like above, but don't need ot test-to-fail all the fields)
      cy.selectFromDropdown('transect_number', transectId[1])
      //start location
      cy.recordLocation({
        btnEqIndex: 0,
      })
      cy.addImages('transect_photo', 1)
      cy.dataCy('done').eq(-1).click() //start transect
      cy.selectFromDropdown('quadrat_number', 'Quadrat 1')
      for (let i = 0; i < 10; i++) {
        cy.dataCy('nextPointBtnLabel').click().wait(500)
      }
      //end location
      cy.recordLocation({
        btnEqIndex: 1,
      })
      cy.dataCy('done').eq(-1).click() //end transect
    })
    it('finish conducting the survey', () => {
      cy.nextStep()
    })
  })

  context('publish', () => {
    // it('submit the survey to core', () => {
    //   cy.workflowPublish()
    // })
    pub()
  })
})

const repeatVisitSurveySetupIds = []
describe(`${protocol} - select existing`, () => {
  context('navigate to protocol', () => {
    it('.should() - assert navigation to ' + protocol + ' is possible', () => {
      cy.selectProtocolIsEnabled(module, protocol)
    })
    it('log transect IDs', () => {
      cy.log(`transectId: ${JSON.stringify(transectId)}`)
    })
  })
  context('4. transect setup', () => {
    it('4.1[validate-field] - transect 1 - check auto-fill', () => {
      //TODO check all other fields are auto-filled and disabled (based on the inputs from 'create new') - similar to herbivory off-plot
      cy.dataCy('selectExisting').click()
      cy.dataCy('transect_route_type')
        .should('not.be.disabled')
        .invoke('val')
        .then((route) => {
          cy.wrap(route).should('be.empty')
        })
      cy.selectFromDropdown('site_id', transectId[0])
      cy.dataCy('transect_route_type')
        .should('be.disabled')
        .invoke('val')
        .then((route) => {
          cy.wrap(route).should('not.be.empty')
        })
    })
    it('4.2[test-to-pass] - transect 1 - save and add next', () => {
      cy.dataCy('done').eq(-1).click()
      cy.dataCy('addEntry').eq(-1).click()
    })
    it('4.3[test-to-pass] - transect 2 - select', () => {
      cy.dataCy('selectExisting').click()
      //this is the transect that a survey setup was not done on
      cy.selectFromDropdown('site_id', transectId[2])
      cy.dataCy('done').eq(-1).click()
    })
    it('finish transect setup', () => {
      cy.log(`transectId: ${JSON.stringify(transectId)}`)
      cy.nextStep()
    })
  })
  context('5. survey setup', () => {
    it(`5.1[validate-field] - survey setup transect 1`, () => {
      cy.offPlotSurveySetup1(transectId[0], 'sign_off').then((id) => {
        repeatVisitSurveySetupIds.push(id)
      })
    })
    it(`2.2[test-to-pass] - survey setup transect 2`, () => {
      cy.offPlotSurveySetup2(transectId[2], 'sign_off').then((id) => {
        repeatVisitSurveySetupIds.push(id)
      })
    })
    it('finish survey setup', () => {
      cy.log(
        `repeatVisitSurveySetupIds: ${JSON.stringify(
          repeatVisitSurveySetupIds,
        )}`,
      )
      cy.nextStep()
    })
  })
  //TODO add test-to-fail and observations to the quadrats
  context('6. conduct the survey', () => {
    it('6.1[test-to-pass] - survey for transect 1', () => {
      cy.selectFromDropdown('transect_number', transectId[0])
      cy.dataCy('setup_ID').should('contain.text', repeatVisitSurveySetupIds[0])

      //start location
      cy.recordLocation({
        btnEqIndex: 0,
      })
      cy.addImages('transect_photo', 1)
      cy.dataCy('done').eq(-1).click() //start transect
      cy.selectFromDropdown('quadrat_number', 'Quadrat 1')
      for (let i = 0; i < 10; i++) {
        cy.dataCy('nextPointBtnLabel').click().wait(500)
      }
      //end location
      cy.recordLocation({
        btnEqIndex: 1,
      })
      cy.dataCy('done').eq(-1).click() //end transect
    })
    it('6.2[test-to-pass] - survey for transect 2', () => {
      cy.selectFromDropdown('transect_number', transectId[2])
      // cy.dataCy('setup_ID').should('contain.text', repeatVisitSurveySetupIds[1])

      //start location
      cy.recordLocation({
        btnEqIndex: 0,
      })
      cy.addImages('transect_photo', 1)
      cy.dataCy('done').eq(-1).click() //start transect
      cy.selectFromDropdown('quadrat_number', 'Quadrat 1')
      for (let i = 0; i < 10; i++) {
        cy.dataCy('nextPointBtnLabel').click().wait(500)
      }
      //end location
      cy.recordLocation({
        btnEqIndex: 1,
      })
      cy.dataCy('done').eq(-1).click() //end transect
    })
    it('finish conducting the survey', () => {
      cy.nextStep()
    })
  })
  context('publish', () => {
    // it('submit the survey to core', () => {
    //   cy.workflowPublish()
    // })
    pub()
  })
})

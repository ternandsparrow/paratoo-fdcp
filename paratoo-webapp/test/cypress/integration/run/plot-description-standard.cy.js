const { pub } = require("../../support/command-utils")

const protocol = 'Plot Description - Standard'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'plot description'

describe('Landing', () => {
  it('login', () => {
    cy.login('TestUser', 'password')
  })
  it('start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocol)
  })
  it('do plot layout', () => {
    cy.plotLayout()
  })
  it('assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})

describe(protocol, () => {
  it('check for past record', () => {
    const noPastRecord = cy.$$('.q-banner__content:contains(Please complete the Enhanced protocol first)')
    cy.log(`noPastRecord (length=${noPastRecord.length}):`, noPastRecord)
    if (noPastRecord.length > 0) {
      throw new Error('No past record for Enhanced to test against. Run the plot-description-enhanced test first.')
    } else {
      cy.dataCy('pastRecordBtn')
        .should('exist')
        .click()
      cy.dataCy('pastRecordTable_initialVisit')
        .should('exist')
      
      cy.dataCy('pastRecordDialogClose').click()
    }
  })
  it('description', () => {
    cy.getAndValidate('revisit_reason', {
      testEmptyField: ['workflowNextButton'],
    }).type('haunted')

    cy.getAndValidate('veg_growth_stage', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('Uneven Age')

    cy.setCheck('Unknown', true).should('have.attr', 'aria-checked', 'true')
    cy.setCheck('Known', true).should('have.attr', 'aria-checked', 'true')
    cy.testToFailValues('year_of_fire', {
      year: { chainer: 'contain.text', value: 'Invalid year format' },
    }).type('2023')

    cy.getAndValidate('structural_formation', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown(null, 'T')

    cy.getAndValidate('homogeneity_measure', {
      testEmptyField: ['workflowNextButton'],
    }).type('6')

    cy.getAndValidate('plant_species_life_stage', {
      testEmptyField: ['workflowNextButton'],
      isComponent: true,
    })
    cy.getAndValidate('species').selectFromDropdown()
    cy.getAndValidate('life_stage').selectFromDropdown()
    cy.dataCy('done').first().click()

    cy.getAndValidate('site_disturbance', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown()

    cy.getAndValidate('introduced_plant_species_impact', {
      testEmptyField: ['workflowNextButton'],
    }).type('¯\\_(ツ)_/¯')

    cy.getAndValidate('comments', {
      testEmptyField: ['workflowNextButton'],
    }).type('comment comment wooo')
  })
})

describe('Publish', () => {
  it('.should() - successful publish', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

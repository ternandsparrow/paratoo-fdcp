const { pub } = require('../../support/command-utils')

const protocol = 'Plot Layout'
const protocolCy = 'Plot\\ Layout\\ and\\ Visit'
const projectCy = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'Plot Selection and Layout'

const moduleFloristics = 'floristics'
const protocolFloristicsEnhanced = 'Floristics - Enhanced'
const protocolFloristicsStandard = 'Floristics - Standard'
const modulePlotDescription = 'plot description'
const protocolPlotDescriptionEnhanced = 'Plot Description - Enhanced'

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
]

function doPlotVisit() {
  cy.dataCy('visitNew').click()
  //need to clear before selecting as the initial autofill include seconds, but the
  //picker only allows precision to the minute
  cy.dataCy('start_date').clear()
  //want to select a specific datetime, as autofill includes the milliseconds, which will
  //make it difficult to compare the start/end datetimes when testing the visit closing
  cy.selectDateTime({
    fieldName: 'start_date',
    date: {
      year: 'current',
      month: 'current',
      day: 'current',
    },
    time: {
      hour: Math.floor(Math.random() * (23 - 0)),
      minute: Math.floor(Math.random() * (59 - 0)),
    },
  })
  cy.dataCy('visit_field_name').type(
    `new test visit ${Math.floor(Math.random() * 100000)}`,
  )
  cy.window().then(($win) => {
    //reset coords
    //since we're in Cypress and `force` is false, it will ignore any coords
    $win.ephemeralStore.updateUserCoords()
  })
}

function checkCloseVisitNotification({ buttonDataCy, messageToCheck }) {
  cy.dismissPopups().wait(200)
  cy.dataCy(buttonDataCy).should('exist').click()
  cy.get('.q-notification', { timeout: 15000 }).eq(-1).as('popup')
  cy.get('@popup')
    .invoke('text')
    .then((text) => {
      cy.log(`text: ${text}`)
      expect(text).to.match(messageToCheck)
    })
  cy.get('@popup').contains('Dismiss').click().wait(500)
}

function doFaunaPlot() {
  cy.dataCy('faunaPlotYes').click()
  cy.selectFromDropdown('startingPoint', 'South East')
  cy.dataCy('startLayout').click().wait(50)
  cy.window().then(() => {
    //fauna plot
    cy.dataCy('setStartPoint').click()
    cy.dataCy('spoofAllPoints').click()
    cy.dataCy('markClosestPoint').click()
  })
}

function mockLocation(coords) {
  return cy.window().then(($win) => {
    //force coords to be near the Selection recommended location
    //headless more can be flaky with location, so need to ensure it is always defined
    $win.ephemeralStore.updateUserCoords(coords, true)
  })
}

describe('landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('reset plot context', () => {
    cy.getStoreReference({
      store: 'bulkStore',
      stateKey: 'staticPlotContext',
    })
      .then((context) => {
        context.plotLayout = null
        context.plotVisit = null
      })
  })
})
describe('create plot layout with fauna plot', () => {
  it('check cannot close visit without context being set', () => {
    cy.dataCy('closeVisitBtn').should('not.exist')
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    //don't provide protocol, as this module only has 1, so it will be auto-selected
    cy.selectProtocolIsEnabled(module)
  })
  it('.should() - start collect new plot layout', () => {
    mockLocation({ lat: -35, lng: 138.66 }).then(() => {
      cy.get('[data-cy="locationNew"]').click()
      cy.selectFromDropdown('plot_selection', 'QDASEQ0009', null)
      cy.getAndValidate('orientation').type(100)
      cy.testEmptyField('Field: "Orientation": Maximum: 45','startLayout')
      cy.dataCy('orientation').clear()
      cy.get('[data-cy="plotAlignedToGridYes"]').click()
      cy.selectFromDropdown('plot_type', 'Control', null)
      cy.get('[data-cy="replicate"]').type('1')
      cy.get('[data-cy="is_the_plot_permanently_marked"]').click()
      cy.selectFromDropdown('marker', 'Star dropper')
      cy.dataCy('All').click()
    })
  })

  it('.should() - fail when submit without finishing plot layout', () => {
    cy.testEmptyField('Missing plot layout points', 'workflowNextButton')
  })

  it('.should() - reject setting reference point when out of project boundary', () => {
    cy.dataCy('startLayout').click().wait(1000)
    mockLocation({ lat: 10, lng: 10 }).then(() => {
      cy.dataCy('setStartPoint').click().wait(1000)
      cy.get('.q-notification').should(
        'contain.text',
        'Reference point is not in the project area! Please try another start point',
      )
      mockLocation({ lat: -35, lng: 138.66 })
    })
  })

  it('.should() -mark all points of plot layout', () => {
    cy.waitForGps(300000).then(() => {
      //start layout of plot points
      cy.dataCy('setStartPoint').click().wait(1000)
      cy.dataCy('spoofAllPoints').click().wait(1000)
      cy.dataCy('markClosestPoint').click().wait(1000) //'done' - closes modal
    })
    //moves to fauna plot step
    cy.nextStep()
  })
  it('.should() - do fauna plot layout', () => {
    doFaunaPlot()
    cy.nextStep()
  })
  it('.should() - do plot visit', () => {
    doPlotVisit()
  })
  // it('.should() -publish plot layout', () => {
  //   cy.workflowPublish(false)
  // })
  pub()
})

describe("select an existing plot and visit to ensure context switches don't get queued", () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module)
  })
  it('.should() - selecting existing layout', () => {
    cy.get('[data-cy="locationExisting"]').click()
    cy.selectFromDropdown('id', 'QDASEQ0001')
    cy.nextStep()
    //skip fauna plot
    cy.nextStep()
  })
  it('.should() - select existing visit', () => {
    cy.get('[data-cy="visitExisting"]').click()
    cy.selectFromDropdown('id', null)
  })
  //TODO also check it didn't go in the queue
  pub({
    successMsg: 'Successfully set plot context of the collection',
    willNotQueue: true,
  })
})

describe('create plot layout without fauna plot', () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    //don't provide protocol, as this module only has 1, so it will be auto-selected
    cy.selectProtocolIsEnabled(module)
  })
  it('.should() - collect new plot layout', () => {
    mockLocation({ lat: -34.971863, lng: 138.637756 }).then(() => {
      cy.get('[data-cy="locationNew"]').click()
      cy.get('[data-cy="plotAlignedToGridYes"]').click()
      cy.selectFromDropdown('plot_selection', 'SATFLB0002', null)
      cy.selectFromDropdown('plot_type', 'Control', null)
      cy.get('[data-cy="replicate"]').type('1')
      cy.get('[data-cy="is_the_plot_permanently_marked"]').click()
      cy.selectFromDropdown('marker', 'Star dropper')
      cy.dataCy('All').click()
      cy.waitForGps(300000).then(() => {
        cy.dataCy('startLayout').click().wait(1000)

        //start layout of plot points
        cy.dataCy('setStartPoint').click().wait(1000)
        cy.dataCy('spoofAllPoints').click().wait(1000)
        cy.dataCy('markClosestPoint').click().wait(1000) //'done' - closes modal
      })
    })
  })
  it('.should() - skip fauna plot', () => {
    cy.nextStep()
  })
  it('.should() - do plot visit', () => {
    cy.nextStep()
    doPlotVisit()
  })

  // it('.should() -publish plot layout', () => {
  //   cy.workflowPublish(false)
  // })
  pub()
})

describe('create fauna plot layout from existing plot and visit', () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    //don't provide protocol, as this module only has 1, so it will be auto-selected
    cy.selectProtocolIsEnabled(module)
  })
  it('.should() - collect existing plot layout', () => {
    cy.get('[data-cy="locationExisting"]').click()
    cy.selectFromDropdown('id', 'SATFLB0002')
    cy.nextStep()
  })
  it('.should() - collect new fauna plot', () => {
    mockLocation({ lat: -34.971863, lng: 138.637756 })
      .then(() => {
        doFaunaPlot()
      })
      .then(() => {
        cy.nextStep()
      })
  })
  it('.should() - collect existing visit', () => {
    cy.get('[data-cy="visitExisting"]').click()
    cy.selectFromDropdown('id', null)
  })
  pub()
})

describe('create fauna plot layout from existing plot and new visit; check that cannot close visit', () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    //don't provide protocol, as this module only has 1, so it will be auto-selected
    cy.selectProtocolIsEnabled(module)
    mockLocation({ lat: -27.388252, lng: 152.88069 })
  })
  it('.should() - collect existing plot layout', () => {
    cy.get('[data-cy="locationExisting"]').click()
    cy.selectFromDropdown('id', 'QDASEQ0001')
    cy.nextStep()
  })
  it('.should() - do fauna plot', () => {
    doFaunaPlot()
    cy.nextStep()
  })
  it('.should() - do plot visit', () => {
    doPlotVisit()
  })
  it('.should() -publish plot layout and check cannot close visit in queue', () => {
    cy.workflowPublish(false, null, false, () => {
      checkCloseVisitNotification({
        buttonDataCy: 'closeVisitBtn',
        messageToCheck:
          /The visit you are trying to close (.*?) is still in the queue and pending sync with the cloud. Please ensure you have synced this visit with the cloud before closing it/,
      })
    })
  })
})

let visitData = null
let visitToClose = null
describe('close visit', () => {
  it('load visit data from store', () => {
    cy.loadLocalStore('apiModels', (apiModels) => {
      visitData = apiModels?.models?.['plot-visit']
      visitToClose = visitData.at(-1)
    })
  })
  it('[test-to-fail]open visit dialog and check empty form disables done button', () => {
    cy.dataCy('closeVisitBtn').click()
    cy.dataCy('doneCloseVisitBtn').should('be.disabled')

    //dialog is now open for subsequent tests
  })
  it('[test-to-fail]try close visit with end datetime before start datetime', () => {
    cy.wrap(new Date(visitToClose?.start_date)).then((startDate) => {
      // cy.get('.q-dialog')
      // .within(dialog => {
      cy.selectDateTime({
        fieldName: 'date',
        date: {
          year: startDate.getFullYear(),
          //`getMonths()` returns months from 0-11, but command expects string
          month: monthNames[startDate.getMonth()],
          day: startDate.getDate() - 1,
        },
        time: {
          hour: startDate.getHours(),
          minute: startDate.getHours(),
        },
      })
      cy.dataCy('closeVisitConfirm').click()
      checkCloseVisitNotification({
        buttonDataCy: 'doneCloseVisitBtn',
        messageToCheck:
          /Cannot close visit (.*?) with an end date that is before the start date/,
      })
      // })
    })
  })
  it('[test-to-fail]try close visit with end datetime that is the same as the start datetime', () => {
    const startDate = new Date(visitToClose?.start_date)
    cy.selectDateTime({
      fieldName: 'date',
      date: {
        year: startDate.getFullYear(),
        month: monthNames[startDate.getMonth()],
        day: startDate.getDate(),
      },
      time: {
        hour: startDate.getHours(),
        minute: startDate.getHours(),
      },
    })
    cy.dataCy('closeVisitConfirm').should('be.checked')
    checkCloseVisitNotification({
      buttonDataCy: 'doneCloseVisitBtn',
      messageToCheck:
        /Cannot close visit (.*?) with an end date that is before the start date/,
    })
  })
  it('[test-to-pass] close visit', () => {
    const startDate = new Date(visitToClose?.start_date)
    cy.selectDateTime({
      fieldName: 'date',
      date: {
        year: 2040,
        month: monthNames[startDate.getMonth()],
        day: startDate.getDate(),
      },
      time: {
        hour: 'current',
        minute: 'current',
      },
    })
    cy.dataCy('closeVisitConfirm').should('be.checked')
    checkCloseVisitNotification({
      buttonDataCy: 'doneCloseVisitBtn',
      messageToCheck: /Successfully closed visit (.*?)/,
    })
  })
  it('[test-to-pass]check that project and plot contexts were reset after closing visit', () => {
    for (const field of ['currentPlot', 'currentVisit']) {
      cy.dataCy(field).should('have.text', 'Not Selected')
    }
    //we clear `currentContext` but not `projectContext`
    cy.loadLocalStore('auth', (auth) => {
      cy.wrap(auth.currentContext).should('be.null')
    })
    cy.loadLocalStore('bulk', (bulk) => {
      const plotContextKeys = ['plotLayout', 'plotVisit']
      cy.wrap(bulk.staticPlotContext).should('have.keys', ...plotContextKeys)
      for (const key of plotContextKeys) {
        cy.wrap(bulk.staticPlotContext[key]).should('be.null')
      }
    })
  })
})

describe('prevent closing visit with in-progress dependencies', () => {
  it('create new visit', () => {
    cy.selectProtocolIsEnabled(module)

    cy.get('[data-cy="locationExisting"]').click()
    cy.selectFromDropdown('id', 'SATFLB0001')
    cy.nextStep()
    
    doPlotVisit()
  })

  pub()

  it('link surveys to visit', () => {
    cy.selectProtocolIsEnabled(moduleFloristics, protocolFloristicsEnhanced, null, {
      count: 1,
    })
    cy.dataCy('workflowBack').click().wait(1000)

    cy.selectProtocolIsEnabled(moduleFloristics, protocolFloristicsStandard, null, {
      count: 1,
    })
    cy.dataCy('workflowBack').click().wait(1000)

    cy.selectProtocolIsEnabled(modulePlotDescription, protocolPlotDescriptionEnhanced, null, {
      count: 1,
    })
    cy.dataCy('workflowBack').click().wait(1000)
  })

  it('check cannot close visit', () => {
    checkCloseVisitNotification({
      buttonDataCy: 'closeVisitBtn',
      messageToCheck: /Unable to close visit (.*?) as there are in-progress protocols that depend on it: Plot Description - Enhanced, Floristics - Enhanced, Floristics - Standard You must finish or discard these surveys before closing the visit/,
    })
  })
})

let manualReferencePointCoords = {
  lat: -34.972959,
  lng: 138.640666,
}

describe('create plot layout with fauna plot using manual reference point', () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    //don't provide protocol, as this module only has 1, so it will be auto-selected
    cy.selectProtocolIsEnabled(module)
  })
  it('.should() - collect new plot layout from manual reference point', () => {
    // as location doesn't matter so make it null
    mockLocation(null)
      .then(() => {
        cy.get('[data-cy="locationNew"]').click()
        cy.get('[data-cy="plotAlignedToGridNo"]').click()
        cy.dataCy('orientation').type(10)
        cy.selectFromDropdown('plot_selection', 'SATFLB0003', null)
        cy.selectFromDropdown('plot_type', 'Control', null)
        cy.get('[data-cy="replicate"]').type('1')
        cy.dataCy('plot_not_walked').check()
        cy.get('[data-autofocus="true"] > .q-btn__content').click()

        cy.dataCy('startingPoint')
        //use non-default starting point
        cy.selectFromDropdown('startingPoint', 'North East')

        //gets disabled when we check the manual reference point box
        cy.dataCy('startLayout').should('be.disabled')
        cy.dataCy('choosePosition').click()
        cy.dataCy('manualCoords', { timeout: 5000 }).click().wait(500)
        //near the plot selection's recommended location
        cy.dataCy('Latitude', { timeout: 5000 }).type(
          `{backspace}${manualReferencePointCoords.lat}`,
        )
        cy.dataCy('Longitude', { timeout: 5000 }).type(
          `{backspace}${manualReferencePointCoords.lng}`,
        )
        cy.wait(1000)
        cy.dataCy('recordLocation').click()

        cy.dataCy('startLayout').should('be.enabled')

        cy.dataCy('startLayout').click().wait(1000)

        cy.dataCy('setStartPoint').click().wait(1000)
        //since we're doing manual reference point, we effectively spoof all the points
        //so the button for marking points changes to 'done' immediately
        cy.dataCy('markClosestPoint').should('have.text', 'Done').click() //close modal
      })
      .then(() => {
        cy.nextStep()
      })
  })
  it('.should() - collect fauna plot from manual reference point', () => {
    cy.dataCy('faunaPlotYes').click()
    //core/veg plot used NE, so SE is effectively the fauna plot starting point (assuming
    //the plot reference pont hasn't changed)
    cy.get('[data-cy="fauna_plot_not_walked"]').check()
    cy.get('[data-autofocus="true"] > .q-btn__content').click()
    cy.selectFromDropdown('startingPoint', 'South West')
    //check the coords were maintained
    cy.dataCy('usingCoords').should(
      'contain.text',
      `Using lat: ${manualReferencePointCoords.lat.toFixed(
        4,
      )}, lng: ${manualReferencePointCoords.lng.toFixed(4)}`,
    )

    cy.dataCy('startLayout').click().wait(1000)
    cy.dataCy('setStartPoint').click().wait(1000)
    //since we're doing manual reference point, we effectively spoof all the points
    //so the button for marking points changes to 'done' immediately
    cy.dataCy('markClosestPoint').should('have.text', 'Done').click() //close modal

    cy.nextStep()
  })
  it('.should() - do plot visit', () => {
    doPlotVisit()
  })
  pub()
})

let faunaManualReferencePointCoords = {
  lat: -34.969945,
  lng: 138.637188,
}
describe('create fauna plot using manual reference point from existing core plot and visit', () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    //don't provide protocol, as this module only has 1, so it will be auto-selected
    cy.selectProtocolIsEnabled(module)
    mockLocation(null)
  })
  it('.should() - collect new plot layout', () => {
    cy.get('[data-cy="locationExisting"]').click()
    cy.selectFromDropdown('id', 'SATFLB0004')
    cy.nextStep()
  })
  it('.should() - collect fauna plot from manual reference point', () => {
    cy.dataCy('faunaPlotYes').click()
    //SW corner of fauna plot is NW of core plot, so we'll set the coords below to NW of
    //core plot
    cy.dataCy('fauna_plot_not_walked').check()
    cy.get('[data-autofocus="true"] > .q-btn__content').click()
    cy.selectFromDropdown('startingPoint', 'North East')
    cy.dataCy('choosePosition').click()
    cy.dataCy('manualCoords', { timeout: 5000 }).click().wait(500)
    //near the plot selection's recommended location
    cy.dataCy('Latitude', { timeout: 5000 }).type(
      `{backspace}${faunaManualReferencePointCoords.lat}`,
    )
    cy.dataCy('Longitude', { timeout: 5000 }).type(
      `{backspace}${faunaManualReferencePointCoords.lng}`,
    )
    cy.wait(1000)
    cy.dataCy('recordLocation').click()

    cy.dataCy('startLayout').click().wait(1000)
    cy.dataCy('setStartPoint').click().wait(1000)
    //since we're doing manual reference point, we effectively spoof all the points
    //so the button for marking points changes to 'done' immediately
    cy.dataCy('markClosestPoint').should('have.text', 'Done').click() //close modal

    cy.nextStep()
  })
  it('.should() - select existing visit', () => {
    cy.get('[data-cy="visitExisting"]').click()
    cy.selectFromDropdown('id', null)
  })
  pub()
})

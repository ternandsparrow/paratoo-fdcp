const { pub } = require("../../support/command-utils")

const protocol = 'Plant Tissue Vouchering - Enhanced'
const protocolCy = 'Plant\\ Tissue\\ Vouchering\\ -\\ Enhanced'
const floristicsProtocol = 'Floristics - Enhanced'
const floristicsProtocolCy = 'Floristics\\ -\\ Enhanced'
const floristicsModule = 'floristics'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'plant\\ tissue\\ vouchering'

global.ignorePaths = [
  'floristics-veg-genetic-voucher.0.has_replicate',
  'floristics-veg-genetic-voucher.1.has_replicate',
  'floristics-veg-genetic-voucher.2.has_replicate'
]

describe(protocol, () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocolCy)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
  it('survey start', () => {
    cy.nextStep()
  })
  it(`.should() - collect 1st Plant Tissue Voucher`, () => {
    cy.selectFromDropdown('floristics_voucher', 'Voucher Full Example 1 [dev_foo_barcode_987]')

    //test-to-fail required fields
    cy.testEmptyField('Field: "Replicate": Barcode required')

    cy.dataCy('recordBarcode').click()
    cy.dataCy('done').click().wait(500)
  })
  it(`.should() - collect 2nd Plant Tissue Voucher`, () => {
    cy.dataCy('addEntry').click()

    //test-to-fail required `floristics_voucher` field
    cy.testEmptyField('Field: "Floristics Voucher": This field is required')

    cy.selectFromDropdown('floristics_voucher', 'Voucher Full Example 7 [dev_foo_barcode_637]')

    //TODO test that `min_distance_between_replicates` field is hidden before this is checked
    cy.get('[data-cy="has_replicate"]').click()
    //TODO won't work as rules are not being applied to fields (only on submission) - expected, as custom rules are only triggered on submission
    // cy.testToFailValues('min_distance_between_replicates', {
    //   '-1': { chainer: 'contain.text', value: 'Minimum: 0' }
    // })

    //test-to-fail required number of replicates
    cy.testEmptyField('Field: "Replicate": Minimum 5 per voucher')

    //record 4
    for (let i = 0; i < 4; i++) {
      cy.dataCy('recordBarcode').click().wait(200)

      if (i >= 1) {
        //after more than 1 voucher, the checkbox is disabled
        cy.dataCy('has_replicate').should('have.attr', 'aria-disabled', 'true')
      }
    }
    cy.testEmptyField('Field: "Replicate": Minimum 5 per voucher')
    //record 1 more to pass
    cy.dataCy('recordBarcode').click().wait(200)

    //test-to-fail require `min_distance_between_replicates` field
    cy.testEmptyField('Field: "Min Distance Between Replicates": This field is required')
    cy.dataCy('min_distance_between_replicates').type('-1')
    cy.testEmptyField('Field: "Min Distance Between Replicates": Minimum 0')

    cy.dataCy('min_distance_between_replicates').type('{backspace}{backspace}{backspace}2')
    cy.dataCy('done').click()
  })
  it(`.should() - publish ${protocol}`, () => {
    cy.nextStep()
    // cy.workflowPublish(false)
  })
  pub()
})

for (const submoduleProtocolVariant of ['full', 'lite']) {
  describe(`${Cypress._.upperFirst(module.replaceAll('/', ''))} (${submoduleProtocolVariant}) as submodule in ${floristicsProtocol}`, () => {
    it('.should() - assert navigation to ' + floristicsProtocol + ' is possible', () => {
      cy.selectProtocolIsEnabled(floristicsModule, floristicsProtocolCy)
    })
    it(`.should() - complete survey step`, () => {
      cy.ptvSubmoduleInFloristicsSurveyStep(submoduleProtocolVariant)
    })
    //don't worry about test-to-fail fields for floristics, as this is done in its tests
    for (let v = 0; v < 3; v++) {
      it(`.should() - collect Floristics voucher ${v + 1} and PTV`, () => {
        cy.collectPtvAsSubmodule(v, 'full')
      })
    }
    it(`.should() - publish ${floristicsProtocol} (and submodule ${protocol})`, () => {
      cy.nextStep()
      // cy.workflowPublish(false)
    })
    pub()
  })
}

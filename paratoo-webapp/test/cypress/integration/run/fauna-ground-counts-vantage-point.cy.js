const { pub } = require("../../support/command-utils")

const protocol = 'Fauna Ground Counts Vantage Point'
const module = 'Fauna\\ Ground\\ Counts'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})
const observationType = ['Seen', 'Heard', 'Seen and heard']

const species = [
  'Goat',
  'Pig',
  'Camel',
  'Deer',
  'Horse',
  'Wild Dog',
  'Fox',
  'Cat',
  'Rabbit',
]
function pickRandomSpecies() {
  const count = Math.floor(Math.random() * (species.length - 1)) + 1
  const shuffled = species.sort(() => 0.5 - Math.random())
  return shuffled.slice(0, count)
}
const transectIDs = []
const surveyIDs = []
let existingSurveyID

describe('1 - point setup', () => {
  it('survey start', () => {
    cy.nextStep()
  })
  // context('1.1 - Create new point', () => {
  it('1[test-to-fail] - vantage point 1', () => {
    cy.get('[aria-label="Vantage Point ID *"]')
      .invoke('val')
      .then((id) => {
        transectIDs.push(id)
      })
    cy.recordLocation()
    cy.getAndValidate('vantage_point_type').type('dsasadasdas')

    for (let index = 0; index < 2; index++) {
      if (index !== 0) {
        cy.dataCy('addEntry').eq(0).click()
      }
      cy.getAndValidate('outlook_zones', {
        isComponent: true,
        testEmptyField: ['done', -1],
      })
      cy.getAndValidate('direction').type(22)
      cy.getAndValidate('right_boundary_direction').type(22)
      cy.getAndValidate('description').type('outlook_zone_description')
      cy.getAndValidate('NVIS_MVG').selectFromDropdown()
      cy.dataCy('done').eq(0).click()
    }
    cy.dataCy('done').click()
  })

  it('2[test-to-pass] - vantage point 2', () => {
    cy.dataCy('addEntry').click()

    cy.get('[aria-label="Vantage Point ID *"]')
      .invoke('val')
      .then((id) => {
        transectIDs.push(id)
      })
    cy.recordLocation()
    cy.dataCy('vantage_point_type').type('dsasadasdas')
    // })

    for (let index = 0; index < 2; index++) {
      if (index !== 0) {
        cy.dataCy('addEntry').eq(0).click()
      }
      cy.dataCy('direction').type(22)
      cy.dataCy('right_boundary_direction').type(22)
      cy.dataCy('description').type('outlook_zone_description')
      cy.dataCy('NVIS_MVG').selectFromDropdown()
      cy.dataCy('done').eq(0).click()
    }
    cy.dataCy('done').click()
    cy.nextStep()
  })
})

describe('2 - Survey setup', () => {
  context(`2.1 test-to-fail`, () => {
    it('1[validate-field] - fill in general details', () => {
      cy.selectFromDropdown('select_vantage_point_ID', transectIDs[0])
      cy.getAndValidate('survey_intent', {
        testEmptyField: ['done', -1],
      }).selectFromDropdown('Once-off measure')
    })

    it('2[validate-field] - add equipment details', () => {
      cy.getAndValidate('equipment_details', {
        isComponent: true,
        testEmptyField: ['done', -1],
      })
      cy.getAndValidate('equipment_type').selectFromDropdown('Vehicle')
      cy.getAndValidate('make').type('Lamborghini')
      cy.getAndValidate('model').type('Aventador SVJ Coupe')
      cy.getAndValidate('description', { isChildOf: 'equipment_details' }).type(
        'Aventador SVJ Coupe',
      )
      cy.dataCy('done').eq(0).click()
    })

    it('3[validate-field] - continue filling in general details', () => {
      cy.getAndValidate('sightability_distance', {
        testEmptyField: ['done', -1],
      }).type(22)

      //TODO test-to-fail that the min/max rule for number of photos per vantage point (based on the point setup) is enforced
      for (let i = 0; i < 2; i++) {
        cy.dataCy('outlook_zone_name').selectFromDropdown(
          `Outlook zone ${i + 1}`,
        )
        cy.getAndValidate('addImg').click()
        cy.dataCy('takePhoto').click().wait(6000)
        cy.dataCy('done').eq(0).click()

        if (i < 1) cy.dataCy('addEntry').eq(1).click()
      }

      const species = pickRandomSpecies()
      cy.getAndValidate('target_species', {
        testEmptyField: ['done', -1],
      }).selectFromDropdown(species)

      // cy.get('body').then(($body) => {
      //   const otherSpecies = $body.find('[data-cy="other"]').length > 0
      //   if (otherSpecies) {
      //     cy.get('[data-cy="other"]').type('perry the platypus').type('{enter}')
      //   }
      // })

      cy.getAndValidate('survey_time', {
        isComponent: true,
        testEmptyField: ['done', -1],
      })
      cy.get(`[data-cy="Diurnal"]`).click()
    })

    it('4[validate-field] - add observers details', () => {
      cy.getAndValidate('observer_details', {
        isComponent: true,
        testEmptyField: ['done', -1],
      })

      cy.getAndValidate('observer_name', { isComponent: true })
      const modifier = Math.floor(Date.now())
      cy.addObservers('observerName', [`Anon ${modifier}`])
      cy.getAndValidate('role', {
        testEmptyField: ['done', -1],
        isChildOf: 'observer_details',
      }).selectFromDropdown()

      cy.getAndValidate('responsible_outlook_zone').selectFromDropdown()
      cy.get('[data-cy="done"]').eq(0).click()
    })

    it('5[validate-field] - weather', () => {
      cy.getAndValidate('weather', {
        isComponent: true,
        testEmptyField: ['done', -1],
      })
      // FIXME: need to init the non required single child obs first before validate its fields
      cy.dataCy('temperature').type('1')
      cy.completeWeatherForm(null, ['done', -1])
      cy.dataCy('done').click()
    })
  })

  context(`2.2 test-to-fail`, () => {
    it('1[validate-field] - fill in general details', () => {
      cy.dataCy('addEntry').click()
      cy.selectFromDropdown('select_vantage_point_ID', transectIDs[0])
      cy.selectFromDropdown('survey_intent', 'Once-off measure')
    })

    it('2[validate-field] - add equipment details', () => {
      cy.dataCy('equipment_type').selectFromDropdown('Vehicle')
      cy.dataCy('make').type('Lamborghini')
      cy.dataCy('model').type('Aventador SVJ Coupe')
      cy.dataCy('description', { isChildOf: 'equipment_details' }).type(
        'Aventador SVJ Coupe',
      )
      cy.dataCy('done').eq(0).click()
    })

    it('3[validate-field] - continue filling in general details', () => {
      cy.dataCy('sightability_distance').type(22)

      for (let i = 0; i < 2; i++) {
        cy.dataCy('outlook_zone_name').selectFromDropdown(
          `Outlook zone ${i + 1}`,
        )
        cy.getAndValidate('addImg').click()
        cy.dataCy('takePhoto').click().wait(6000)
        cy.dataCy('done').eq(0).click()

        if (i < 1) cy.dataCy('addEntry').eq(1).click()
      }

      const species = pickRandomSpecies()
      cy.selectFromDropdown('target_species', species)
      // cy.get('body').then(($body) => {
      //   const otherSpecies = $body.find('[data-cy="other"]').length > 0
      //   if (otherSpecies) {
      //     cy.get('[data-cy="other"]').type('perry the platypus').type('{enter}')
      //   }
      // })
      cy.get(`[data-cy="Diurnal"]`).click()
    })

    it('4[validate-field] - add observers details', () => {
      const modifier = Math.floor(Date.now())
      cy.addObservers('observerName', [`Anon ${modifier}`])
      cy.dataCy('role').selectFromDropdown()
      cy.getAndValidate('responsible_outlook_zone').selectFromDropdown()
      cy.get('[data-cy="done"]').eq(0).click()
    })

    it('5[validate-field] - weather', () => {
      // FIXME: need to init the non required single child obs first before validate its fields
      cy.dataCy('temperature').type('1')
      cy.completeWeatherForm(null, ['done', -1])
      cy.dataCy('done').click()
      cy.nextStep()
    })
  })
})
describe('3 - Conduct the survey', () => {
  context('3.1 - test-to-fail', () => {
    it('1 start survey', () => {
      cy.testEmptyField(
        'Field: "Vantage Point ID": This field is required',
        'done',
      )
      cy.selectFromDropdown('transect_ID', transectIDs[0])
      cy.testEmptyField('Field: "Setup ID": This field is required', 'done')
      cy.selectFromDropdown('setup_ID', surveyIDs[0])
    })
    it('mock the location', () => {
      cy.window().then(($win) => {
        $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
      })
    })
    it('2[validate-field] - start transect, add observation 1', () => {
      cy.dataCy('startSurvey', {timeout: 10000}).should('exist')
      cy.dataCy('startSurvey').click()
      cy.dataCy('observations', {timeout: 10000}).should('exist')
      cy.getAndValidate('observations', {
        isComponent: true,
        testEmptyField: ['done', -1],
      })
      cy.getAndValidate('spotters_name').selectFromDropdown()
      cy.getAndValidate('applicable_outlook_zone').selectFromDropdown()
      cy.getAndValidate('distance').type(10)
      cy.dataCy('compass_bearing').type(10)
      cy.getAndValidate('attributable_fauna_species').selectFromDropdown()
      cy.getAndValidate('count_of_individuals').type(10)
      const obsType =
        observationType[
          Math.floor(Math.random() * (observationType.length - 1))
        ]
      cy.getAndValidate('observation_type').selectFromDropdown(obsType)
      cy.getAndValidate('sex').selectFromDropdown()
      cy.getAndValidate('age_class').selectFromDropdown()
      // cy.getAndValidate('cat_coat_colour').selectFromDropdown()
      cy.getAndValidate('comments', 'comments')
      cy.dataCy('done').eq(0).click()
    })

    it('3[test-to-pass] - end survey', () => {
      cy.dataCy('endSurvey').click()
      cy.dataCy('done').click()
      cy.dataCy('addEntry').last().click()
    })

    it('4[validate-field] - start transect, add observation 2', () => {
      cy.selectFromDropdown('transect_ID', transectIDs[0])
      cy.testEmptyField('Field: "Setup ID": This field is required', 'done')
      cy.selectFromDropdown('setup_ID', surveyIDs[1])
      cy.dataCy('startSurvey').click()
      cy.dataCy('spotters_name').selectFromDropdown()
      cy.dataCy('applicable_outlook_zone').selectFromDropdown()
      cy.dataCy('distance').type(10)
      cy.dataCy('compass_bearing').type(10)
      cy.dataCy('attributable_fauna_species').selectFromDropdown()
      cy.dataCy('count_of_individuals').type(10)
      const obsType =
        observationType[
          Math.floor(Math.random() * (observationType.length - 1))
        ]
      cy.dataCy('observation_type').selectFromDropdown(obsType)
      cy.dataCy('sex').selectFromDropdown()
      cy.dataCy('age_class').selectFromDropdown()
      cy.dataCy('comments', 'comments')
      cy.dataCy('done').eq(0).click()
    })
    it('5[test-to-pass] - end survey', () => {
      cy.dataCy('endSurvey').click()
      cy.dataCy('done').click()
      cy.dataCy('addEntry').last().click()
    })
    it('6[test-to-pass] - end survey and submit', () => {
      cy.nextStep()
      // cy.workflowPublish()
    })
    pub()
  })
})

describe('4 - select existing', () => {
  context('1 - transect setup ', () => {
    it('.should() - assert navigation to ' + protocol + ' is possible', () => {
      cy.selectProtocolIsEnabled(module, protocol)
    })
    it('survey start', () => {
      cy.nextStep()
    })
    it('1.1[validate-field] - check auto-fill', () => {
      cy.dataCy('selectExisting').click()
      cy.selectFromDropdown('site_id', transectIDs[0])
      cy.dataCy('done').eq(0).click()
      cy.get('.q-item__label ')
        .contains('Outlook Zones 2 (Outlook zone 2)')
        .click()
      cy.dataCy('done').eq(0).click()
      cy.dataCy('done').click()
      cy.nextStep()
    })
  })
  context('2 - Survey setup', () => {
    it('1[validate-field] - fill in general details', () => {
      cy.selectFromDropdown('select_vantage_point_ID', transectIDs[0])
      cy.dataCy('survey_setup_ID')
        .invoke('val')
        .then((id) => {
          existingSurveyID = id
        })
    })

    it('2[validate-field] - add equipment details', () => {
      cy.dataCy('equipment_type').selectFromDropdown('Vehicle')
      cy.dataCy('make').type('Lamborghini')
      cy.dataCy('model').type('Aventador SVJ Coupe')
      cy.dataCy('description', { isChildOf: 'equipment_details' }).type(
        'Aventador SVJ Coupe',
      )
      cy.dataCy('done').eq(0).click()
    })

    it('3[validate-field] - continue filling in general details', () => {
      cy.dataCy('sightability_distance').type(22)

      for (let i = 0; i < 2; i++) {
        cy.dataCy('outlook_zone_name').selectFromDropdown(
          `Outlook zone ${i + 1}`,
        )
        cy.getAndValidate('addImg').click()
        cy.dataCy('takePhoto').click().wait(6000)
        cy.dataCy('done').eq(0).click()

        if (i < 1) cy.dataCy('addEntry').eq(1).click()
      }

      const species = pickRandomSpecies()
      cy.selectFromDropdown('target_species', species)
      // cy.get('body').then(($body) => {
      //   const otherSpecies = $body.find('[data-cy="other"]').length > 0
      //   if (otherSpecies) {
      //     cy.get('[data-cy="other"]').type('perry the platypus').type('{enter}')
      //   }
      // })
      cy.get(`[data-cy="Diurnal"]`).click()
    })

    it('4[validate-field] - add observers details', () => {
      const modifier = Math.floor(Date.now())
      cy.addObservers('observerName', [`Anon ${modifier}`])
      cy.dataCy('role').selectFromDropdown()
      cy.getAndValidate('responsible_outlook_zone').selectFromDropdown()
      cy.get('[data-cy="done"]').eq(0).click()
    })

    it('5[validate-field] - weather', () => {
      // FIXME: need to init the non required single child obs first before validate its fields
      cy.dataCy('temperature').type('1')
      cy.completeWeatherForm(null, ['done', -1])
      cy.dataCy('done').click()
      cy.nextStep()
    })
  })

  context('3 - Conduct the survey', () => {
    it('4[validate-field] -  add observation', () => {
      cy.selectFromDropdown('transect_ID', transectIDs[0])
      cy.testEmptyField('Field: "Setup ID": This field is required', 'done')
      cy.selectFromDropdown('setup_ID', surveyIDs[1])
      cy.dataCy('startSurvey').click()
      cy.dataCy('spotters_name').selectFromDropdown()
      cy.dataCy('applicable_outlook_zone').selectFromDropdown()
      cy.dataCy('distance').type(10)
      cy.dataCy('compass_bearing').type(10)
      cy.dataCy('attributable_fauna_species').selectFromDropdown()
      cy.dataCy('count_of_individuals').type(10)
      const obsType =
        observationType[
          Math.floor(Math.random() * (observationType.length - 1))
        ]
      cy.dataCy('observation_type').selectFromDropdown(obsType)
      cy.dataCy('sex').selectFromDropdown()
      cy.dataCy('age_class').selectFromDropdown()
      cy.dataCy('comments', 'comments')
      cy.dataCy('done').eq(0).click()
    })
    it('5[test-to-pass] - end survey', () => {
      cy.dataCy('endSurvey').click()
      cy.dataCy('done').click()
      cy.dataCy('addEntry').last().click()
    })
    it('6[test-to-pass] - end survey and submit', () => {
      cy.nextStep()
      // cy.workflowPublish()
    })
    pub()
  })
})

const { pub } = require('../../support/command-utils')

const project = 'Bird\\ survey\\ TEST\\ Project'
const module = 'vertebrate\\ fauna'
const protocol = 'Vertebrate Fauna - Bird Survey'

describe('0-Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  // it('.should() - start collection button should be disabled', () => {
  //   cy.selectProtocolIsDisabled(module)
  // })
  it('.should() - do plot layout', () => {
    cy.plotLayout(true, null, {
      layout: 'QDASEQ0003',
      visit: 'QLD spring survey',
    })
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})
describe('1-survey step', () => {
  it('1.1[] - add surveyor names', () => {
    //TODO update addObservers function to handle bird survey observers as well
    //bird survey observers are added in different way as compared to addobservers function
    cy.getAndValidate('observerName')
    cy.addObservers('observerName', ['John'])
    cy.getAndValidate('role', {
      isChildOf: 'surveyor_names',
    }).selectFromDropdown('Other')
    cy.get('[data-cy="done"]').click().wait(500)
    cy.get('[data-cy="addEntry"]').click().wait(500)
    cy.addObservers('observerName', ['John'])
    cy.get('[data-cy="role"]').selectFromDropdown('Data entry')
    cy.get('[data-cy="done"]').click().wait(500)
    cy.get('[data-cy="addEntry"]').click().wait(500)
    cy.addObservers('observerName', ['John'])
    cy.get('[data-cy="role"]').selectFromDropdown('Spotter')
    cy.get('[data-cy="done"]').click().wait(500)
  })

  it('1.2[test-to-pass] - choose survey type: 20 minutes, 2ha', () => {
    // cy.getAndValidate('survey_type', { isComponent: true })
    cy.get('[data-cy="20 minute, 2ha"]').click()
    cy.get('[data-cy="playback_used"]').click()
    cy.get('[data-cy="startTimer"]').click()
  })
})
describe('2-bird observation', () => {
  it('2.1[validate-field] - add bird observation 1', () => {
    cy.get('[data-cy="addBirdObservation"]').click()
    cy.getAndValidate('species').selectFromSpeciesList('australian magpie')
    cy.getAndValidate('count').type(4)

    cy.getAndValidate('observation_type', { isComponent: true })
    cy.dataCy('Seen').click()
    cy.getAndValidate('activity_type').selectFromDropdown(
      'Flying over circling',
    )
    cy.getAndValidate('sex').selectFromDropdown('Male')
    cy.getAndValidate('observation_location_type').selectFromDropdown(
      'Within Survey Area',
    )
    cy.getAndValidate('breeding_type').selectFromDropdown('Nest with eggs')
    cy.getAndValidate('age_class').selectFromDropdown('Adult')
    cy.get('[data-cy="done"]').click().wait(500)
  })

  it('2.2[test-to-pass] - add bird observation 2', () => {
    cy.get('[data-cy="addBirdObservation"]').click()
    cy.dataCy('species').selectFromSpeciesList('emu')
    cy.dataCy('count').type(4)
    cy.dataCy('Heard').click()
    cy.get('[data-cy="done"]').click().wait(500)
  })

  it('2.3[test-to-fail] - fail when submit without ending search', () => {
    cy.testEmptyField('The search has not ended', 'workflowNextButton', null, 0)
    cy.get('[data-cy="endSurvey"] > .q-btn__content > .block').click().wait(500)
    cy.get('[data-autofocus="true"] > .q-btn__content').click().wait(500)
  })

  it('2.4[test-to-pass] - edit bird observation', () => {
    cy.get('[data-cy="edit"]').eq(0).click().wait(500)
    cy.get('[data-cy="count"]').type('8')
    cy.selectFromDropdown('breeding_type', 'None')
    cy.get('[data-cy="done"]').click()
  })

  it('2.5[test-to-pass] - copy bird observation', () => {
    cy.get('[data-cy="copy"]').eq(0).click().wait(500)
    cy.selectFromSpeciesList('species', 'crow', false)
    cy.selectFromDropdown('sex', 'Mixed Sexes')
    cy.get('[data-cy="done"]').click()
  })

  it('2.6[test-to-pass] - delete bird observation', () => {
    cy.get('[data-cy="delete"]').eq(1).click()
  })
})

describe('3-complete weather survey', () => {
  it('3.1[test-to-pass] - add weather observation', () => {
    cy.nextStep()
    cy.completeWeatherForm() //no param so it will use pre-defined default values
  })
})
describe('4-submit and publish', () => {
  pub()
})

describe('4- submit a bird survey without any obs', () => {
  it('4.1[test-to-pass] - submit the survey', () => {
    cy.selectProtocolIsEnabled(module, protocol)
    cy.addObservers('observerName', ['John'])
    cy.dataCy('role').selectFromDropdown('Other')
    cy.get('[data-cy="done"]').click().wait(50)
    cy.get('[data-cy="20 minute, 2ha"]').click()
    cy.get('[data-cy="playback_used"]').click()
    cy.get('[data-cy="startTimer"]').click()
    cy.get('[data-cy="endSurvey"] > .q-btn__content > .block').click().wait(500)
    cy.get('[data-autofocus="true"] > .q-btn__content').click().wait(500)
    cy.nextStep()
    cy.completeWeatherForm()
  })
  pub()
})

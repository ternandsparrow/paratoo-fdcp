const { pub } = require("../../support/command-utils")

const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'soils'
const protocol = 'Soil Sample Pit'

describe('landing', () => {
  it('login', () => {
    cy.login('TestUser', 'password')
  })

  it('navigation to ' + protocol + ' is disabled', () => {
    cy.selectProtocolIsDisabled(module, protocol)
    // cy.visit('/')
  })

  it('plot layout', () => {
    cy.plotLayout()
  })

  it('navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})

describe('field', () => {
  it('select field survey', () => {
    // cy.getAndValidate('survey_variant', { isComponent: true })
    cy.dataCy('Field').click()
  })
  it('observers', () => {
    cy.getAndValidate('observers')

    cy.addObservers('observerName', [
      'Tom',
      'Luke',
      'Walid',
      'Wildcard' + (Math.random() + 1).toString(36).substring(10),
    ])
  })

  it('soil pit', () => {
    cy.dataCy('soil_pit_id').should('contain.value', 'QDASEQ0001')

    cy.getAndValidate('collection_method').selectFromDropdown(
      'Differential GNSS',
    )
    cy.recordLocation()

    cy.getAndValidate('observation_type').selectFromDropdown('Soil pit')

    cy.getAndValidate('soil_pit_depth').type('5')

    cy.getAndValidate('digging_stopped_by').selectFromDropdown('Rock')

    cy.getAndValidate('pit_photo', { isComponent: true })
    cy.addImages('addImg')
    cy.getAndValidate('comments').type('Test comment')
  })

  it('publish', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})
describe('lab', () => {
  it('navigation to ' + protocol, () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
  it('survey', () => {
    cy.dataCy('Lab').click()
    cy.dataCy('field_survey_id').click().type('{downarrow}{downarrow}{enter}')
    cy.nextStep()
  })

  it('soil pit sample', () => {
    //   cy.wait(1000) //let the page land before trying to toggle the first barcode reader
    for (let i = 1; i < 3; i++) {
      if (i === 1) {
        cy.getAndValidate('barcode', { isComponent: true })
        cy.setCheck('toggleBarcodeReaderShow')
        cy.dataCy('recordBarcode').click()

        cy.getAndValidate('upper_depth').type('2')

        cy.getAndValidate('lower_depth').type('2')

        cy.dataCy('done').click().wait(500)
      } else {
        cy.setCheck('toggleBarcodeReaderShow')
        cy.dataCy('recordBarcode').click()

        cy.dataCy('upper_depth').type('2')

        cy.dataCy('lower_depth').type('2')
        cy.dataCy('done').click().wait(500)
      }
      if (i < 2) cy.dataCy('addEntry').click()
    }
  })

  it('publish', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

const { pub } = require('../../support/command-utils')

const protocolCy = 'Photopoints\\ -\\ Device\\ Panorama'
const protocol = 'Photopoints - Compact Panorama'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'photopoints'

let photoIds = []

beforeEach(() => {
  cy.intercept('api/upload', (req) => {
    req.on('response', (res) => {
      if (Cypress._.get(res, 'body.0.hasExifIssue'))
        photoIds.push(Cypress._.get(res, 'body.0.id'))
    })
  }).as('PhotoUpload')
})
describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocolCy)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
})
describe('do photopoints', () => {
  // })
  it('.should() - fail to go to next step if 3 points are not collected', () => {
    cy.dataCy('centrePostPositionedYes').click()
    cy.recordLocation({
      btnEqIndex: 0,
    })
    cy.dataCy('nextPoint').should('not.exist')
    cy.recordLocation({
      btnEqIndex: 1,
      recordBtnIndex: 1,
    })
    cy.dataCy('nextPoint').should('exist')
    cy.testEmptyField('Require 3 photopoint positions', 'workflowNextButton')
  })
  it('.should() - collect photopoints lite device', () => {
    // TODO setup a intercept for the response
    for (let i = 0; i < 3; i++) {
      cy.recordLocation({
        btnEqIndex: 1,
        recordBtnIndex: 1,
      })
      if (i < 2) {
        cy.get('[data-cy="addImg"]').click().wait(50)
        cy.dataCy('useFilePicker').toggle(true)
        cy.dataCy('uploadFile').selectFile(
          `test/cypress/fixtures/Site1_Photopoint${i + 2}.jpg`,
        )
        cy.wait(6000)
      }
      if (i < 2) {
        cy.get('[data-cy="nextPoint"]').click()
      }
    }
    cy.testEmptyField('Missing photos for point 3', 'workflowNextButton')
    cy.get('[data-cy="addImg"]').click().wait(50)
    cy.dataCy('useFilePicker').toggle(true)
    cy.dataCy('uploadFile').selectFile(
      'test/cypress/fixtures/Site1_Photopoint1.jpg',
    )
    cy.wait(6000)

    //TODO test multiple photos for each point
    //TODO test photo comments
    cy.nextStep()
  })
  pub()
  it('check hasExifIssue on (bad)samsung image', () => {
    cy.log('Fetching samsung image using its id ', photoIds)
    if (!photoIds.length > 0) {
      throw new Error(
        'At least one of the bad samsung images should have have failed, but none did.',
      )
    }
    for (const id of photoIds) {
      cy.doRequest('upload/files', {
        args: {
          'use-default': true,
          [`filters[id]`]: id,
        },
      })
        .its('body.0.hasExifIssue')
        .should('be.true')
    }
  })
})

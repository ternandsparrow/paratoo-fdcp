const { pub } = require('../../support/command-utils')

const protocol = 'Floristics - Standard'
const protocolCy = 'Floristics\\ -\\ Standard'
const module = 'floristics'

function doVirtualVoucher(protocolOnly = false) {
  if (!protocolOnly) {
    it('assert navigation to ' + protocol + ' is possible', () => {
      cy.selectProtocolIsEnabled(module, protocolCy)
    })
    it('complete survey', () => {
      // cy.get('[data-cy="workflowNextButton"]').click().wait(100)
      cy.nextStep()
    })
  }
  it('collect again, once', () => {
    cy.wait(2000)
    //set reader to false to enable images to be taken in the dialog
    cy.setCheck('toggleBarcodeReaderShow', false)

    cy.get('[data-cy="collectAgain"]').eq(0).click()

    cy.get('#floristics_virtual_voucher').within(() => {
      // growth form 1
      cy.selectFromDropdown('growth_form_1', 'Vine')
      cy.selectFromDropdown('growth_form_2', 'Aquatic')
      cy.selectFromDropdown('habit', 'Climbing')
      cy.selectFromDropdown('phenology', 'Adult')

      cy.get(`[data-cy="addImg"]`).then((imgList) => {
        for (const index of Array.from(imgList).keys()) {
          cy.get(imgList[index]).click()
          cy.dataCy('comment')
            .eq(index)
            .type("I'm a comment about the photo" + (index + 1))
          cy.get('[data-cy="takePhoto"]').eq(index).click()
          cy.wait(100)
        }
      })

      cy.dataCy('done').eq(-1).click()
    })
    cy.get('[data-cy="collectAgain"]')
      .eq(0)
      .should('contain.text', 'Edit Repeat Collection')
    cy.nextStep()
  })
}

describe('Landing', () => {
  it('login', () => {
    cy.login('TestUser', 'password')
  })
  it('do plot layout', () => {
    cy.plotLayout()
  })
})

describe('Existing vouchers with errors, edit and submit', () => {
  doVirtualVoucher()
  it('.should() - corrupt collection data of virtual vouchers in the store', () => {
    cy.nextStep()
    cy.window().then((win) => {
      // required field
      delete win.bulkStore.collections[13]['floristics-veg-virtual-voucher'][0]
        .growth_form_1
    })
  })
  it('.should() - receive ajv errors when try to queue collection', () => {
    cy.dataCy('workflowPublish').click()
    cy.dataCy('ajv-error-0').should(
      'contain.text',
      'Field "Growth Form 1" is missing at "Floristics Veg Virtual Voucher/1".',
    )
  })
  it('.should() - go back to the related step having ajv error if clicking edit button', () => {
    cy.dataCy('ajv-edit-0').click()
    cy.dataCy('title', { timeout: 60000 })
      .eq(0)
      .should('contain.text', 'Floristics voucher - Standard')
  })
  it('.should() - the two ajv errors should be visible and formatting error should be an expansion item', () => {
    // should have class q-expansion-item
    cy.dataCy('ajv-error-0')
      .should('be.visible')
      .should('have.class', 'q-expansion-item')
  })
  doVirtualVoucher(true)
  pub({ shapeShape: true, shareValues: true, shareValuesWith: false })
})

let showAjvErrorsBtn
describe('do photopoints with error, then stash it', () => {
  doVirtualVoucher()
  it('.should() - corrupt collection data in the store', () => {
    cy.nextStep()
    cy.window().then((win) => {
      // required field
      delete win.bulkStore.collections[13]['floristics-veg-virtual-voucher'][0]
        .growth_form_1
    })
  })
  it('.should() - receive ajv errors when try to queue collection, then stash the collection', () => {
    cy.dataCy('workflowPublish').click()

    cy.dataCy('ajv-error-0').should(
      'contain.text',
      'Field "Growth Form 1" is missing at "Floristics Veg Virtual Voucher/1".',
    )
    cy.dataCy('workflowPublishConfirm').click()
    cy.dataCy('submitQueuedCollectionsBtn').should('not.exist')
    cy.dataCy('showAjvErrorsBtn').should('have.length.greaterThan', 0)
    cy.dataCy('showAjvErrorsBtn').then((_) => (showAjvErrorsBtn = _.length))
  })
})
describe('collect new floristics and sync, the queued items with ajv errors should be the same', () => {
  doVirtualVoucher()
  pub()
  it('.should() - the number of queued items should be the same', () => {
    cy.dataCy('showAjvErrorsBtn').should('have.length', showAjvErrorsBtn)
  })
})

const { pub } = require("../../support/command-utils")

const protocol = 'Sign-based Fauna - Vehicle Track'
const module = 'Sign-based Fauna Surveys'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
  it('mock location', () => {
    cy.window().then(($win) => {
      $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
    })
  })
})

const transectIds = []
const setupIds = []
describe('create new transect', () => {
  describe('1. transect setup', () => {
    it('1.1[validate-field] - transect 1', () => {
      cy.dataCy('site_id')
        .invoke('val')
        .then((id) => {
          transectIds.push(id)
        })
      cy.recordLocation()

      cy.getAndValidate('transect_route_type').selectFromDropdown(
        'Vehicle road',
      )
      cy.getAndValidate('bearing').type(50)
      cy.getAndValidate('transect_width').type(2)
      cy.getAndValidate('transect_length').type(20)
      cy.getAndValidate('transect_spacing').type(1)
      cy.getAndValidate('habitat').selectFromDropdown('Forest')

      cy.dataCy('done').eq(0).click()
    })
    it('1.1[test-to-pass] - transect 2', () => {
      cy.dataCy('addEntry').click()
      cy.dataCy('site_id')
        .invoke('val')
        .then((id) => {
          transectIds.push(id)
        })
      cy.recordLocation()

      cy.dataCy('transect_route_type').selectFromDropdown('Vehicle road')
      cy.dataCy('bearing').type(50)
      cy.dataCy('transect_width').type(2)
      cy.dataCy('transect_length').type(20)
      cy.dataCy('transect_spacing').type(1)
      cy.dataCy('habitat').selectFromDropdown('Forest')

      cy.dataCy('done').eq(0).click()
    })
  })
  describe('2. survey setup', () => {
    it(`2.1[validate-field] - survey setup transect 1`, () => {
      cy.nextStep()
      cy.getAndValidate('transect_setup_ID').selectFromDropdown(transectIds[0])

      cy.getAndValidate('survey_intent').selectFromDropdown('Once-off measure')
      cy.getAndValidate('target_species').selectFromDropdown(['Deer', 'Other'])
      cy.getAndValidate('target_deer').selectFromDropdown(
        'Fallow deer (Dama dama)',
      )
      cy.dataCy('other_species').type('foo{enter}bar{enter}')
      cy.wait(500)
      cy.dataCy('done').first().click()

      cy.getAndValidate('tracking_substrate').selectFromDropdown()
      cy.getAndValidate('track_width').type(10)

      cy.getAndValidate('observer_details', {
        isComponent: true,
        testEmptyField: ['done', -1],
      })
      const modifier = Math.floor(Date.now())
      //TODO test-to-fail observer name (need to get a handle on 'done' button inside observer details component)
      cy.addObservers('observerName', [`Anon ${modifier}`])
      cy.getAndValidate('role').selectFromDropdown()
      cy.get('[data-cy="done"]').eq(0).click()

      cy.getAndValidate('weather', { isComponent: true })
      cy.completeWeatherForm()

      cy.dataCy('startTrackDrag').click()
      cy.dataCy('endTrackDrag').click()

      cy.dataCy('survey_setup_ID')
        .invoke('val')
        .then((id) => {
          setupIds.push(id)
        })

      cy.dataCy('done').eq(-1).click()
    })
    it(`2.2[test-to-pass] - survey setup transect 2`, () => {
      cy.dataCy('addEntry').eq(-1).click()
      cy.dataCy('transect_setup_ID').selectFromDropdown(transectIds[1])

      cy.dataCy('survey_intent').selectFromDropdown('Once-off measure')
      cy.dataCy('target_species').selectFromDropdown(['Goat', 'Unknown', 'N/A'])
      // cy.dataCy('other').type('foo{enter}bar{enter}') //TODO test these two values were input correctly

      cy.dataCy('tracking_substrate').selectFromDropdown()
      cy.dataCy('track_width').type(10)

      const modifier = Math.floor(Date.now())
      cy.addObservers('observerName', [`Anon ${modifier}`])
      cy.dataCy('role').selectFromDropdown()
      cy.get('[data-cy="done"]').eq(0).click()

      cy.dataCy('startTrackDrag').click()
      cy.dataCy('endTrackDrag').click()

      cy.dataCy('survey_setup_ID')
        .invoke('val')
        .then((id) => {
          setupIds.push(id)
        })

      cy.dataCy('done').eq(-1).click()
      cy.nextStep()
    })
  })
  describe('3. conduct survey', () => {
    it('3.1 - start transect 1 ', () => {
      cy.dataCy('transect_ID').selectFromDropdown(transectIds[0])
      cy.dataCy('setup_ID').selectFromDropdown(setupIds[0])

      cy.dataCy('startSurvey', {timeout: 10000}).should('exist')
      cy.dataCy('startSurvey').click()
    })
    it('3.2 -  add observation 1', () => {
      cy.dataCy('tracks_observed').click()

      cy.getAndValidate('attributable_fauna_species').last().selectFromDropdown('foo')
      cy.getAndValidate('age_class').selectFromDropdown()
      cy.getAndValidate('number_of_individuals').type(2)

      cy.getAndValidate('photo', { isComponent: true })
      cy.dataCy('addImg').click().wait(50)
      cy.dataCy('takePhoto').click().wait(50)
      cy.dataCy('done').eq(0).click()
    })
    it('3.3 -  add observation 2', () => {
      cy.dataCy('addEntry').first().click()
      cy.dataCy('attributable_fauna_species').last().selectFromDropdown(
        'Fallow deer (Dama dama)',
      )
      cy.dataCy('age_class').selectFromDropdown()
      cy.dataCy('number_of_individuals').type(2)

      cy.dataCy('done').eq(0).click()

      cy.dataCy('endSurvey').click()

      cy.dataCy('done').click()
      cy.nextStep()
    })
  })
  describe('submit survey', () => {
    // it('submit the survey to core', () => {
    //   cy.workflowPublish()
    // })
    pub()
  })
})
describe('select existing', () => {
  describe('4. transect setup', () => {
    it('.should() - assert navigation to ' + protocol + ' is possible', () => {
      cy.selectProtocolIsEnabled(module, protocol)
    })
    it('4.1[validate-field] - transect 1', () => {
      cy.dataCy('selectExisting').click()
      cy.selectFromDropdown('site_id', transectIds[0])
      cy.selectFromDropdown('site_id', transectIds[1])

      cy.dataCy('transect_route_type')
        .should('be.disabled')
        .should('contain.value', 'Vehicle road')
      cy.dataCy('bearing').should('be.disabled').should('contain.value', 50)
      cy.dataCy('transect_width')
        .should('be.disabled')
        .should('contain.value', 2)
      cy.dataCy('transect_length')
        .should('be.disabled')
        .should('contain.value', 20)
      cy.dataCy('transect_spacing')
        .should('be.disabled')
        .should('contain.value', 1)

      cy.dataCy('habitat')
        .should('be.disabled')
        .prev()
        .should('contain.text', 'Forest')

      cy.dataCy('done').eq(0).click()
    })
  })
  describe('5. survey setup', () => {
    it(`5.2[test-to-pass] - survey setup transect 2`, () => {
      cy.nextStep()
      cy.dataCy('transect_setup_ID').selectFromDropdown(transectIds[1])

      cy.dataCy('survey_intent').selectFromDropdown()
      cy.dataCy('target_species').selectFromDropdown(['Goat'])

      cy.dataCy('tracking_substrate').selectFromDropdown()
      cy.dataCy('track_width').type(10)

      const modifier = Math.floor(Date.now())
      cy.addObservers('observerName', [`Anon ${modifier}`])
      cy.dataCy('role').selectFromDropdown()
      cy.get('[data-cy="done"]').eq(0).click()

      cy.dataCy('startTrackDrag').click()
      cy.dataCy('endTrackDrag').click()

      cy.dataCy('survey_setup_ID')
        .invoke('val')
        .then((id) => {
          setupIds.push(id)
        })

      cy.dataCy('done').eq(-1).click()
      cy.nextStep()
    })
  })
  describe('6. conduct survey', () => {
    it('6.1 - start transect 1 ', () => {
      cy.dataCy('transect_ID').selectFromDropdown()
      cy.dataCy('setup_ID').selectFromDropdown()
      cy.dataCy('startSurvey', {timeout: 10000}).should('exist')
      cy.dataCy('startSurvey').click()
    })
    it('6.2 -  add observation 1', () => {
      // cy.dataCy('attributable_fauna_species').last().selectFromDropdown('Cat')
      // cy.dataCy('age_class').selectFromDropdown()
      // cy.dataCy('number_of_individuals').type(2)

      cy.dataCy('done').eq(0).click()

      cy.dataCy('endSurvey').click()

      cy.dataCy('done').click()
      cy.nextStep()
    })
  })
  describe('submit survey', () => {
    // it('submit the survey to core', () => {
    //   cy.workflowPublish()
    // })
    pub()
  })
})

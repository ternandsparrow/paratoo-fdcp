const { pub } = require('../../support/command-utils')

const protocolCy = 'Photopoints\\ -\\ Device\\ Panorama'
const protocol = 'Photopoints - Compact Panorama'
const module = 'photopoints'

function doPhotoPoint() {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
  it('.should() - collect photopoints lite device', () => {
    cy.dataCy('centrePostPositionedYes').click()
    cy.recordLocation({
      btnEqIndex: 0,
    })
    cy.dataCy('nextPoint').should('not.exist')
    cy.recordLocation({
      btnEqIndex: 1,
      recordBtnIndex: 1,
    })
    for (let i = 0; i < 3; i++) {
      cy.recordLocation({
        btnEqIndex: 1,
        recordBtnIndex: 1,
      })
      cy.get('[data-cy="addImg"]').click().wait(50)
      cy.get('[data-cy="takePhoto"]').click().wait(50)
      if (i < 2) {
        cy.get('[data-cy="nextPoint"]').click()
      }
    }

    cy.nextStep()
  })
}

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocolCy)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
})
describe('do photopoints with error, edit and submit', () => {
  it('.should() - collect photopoints lite device', () => {
    cy.dataCy('centrePostPositionedYes').click()
    cy.recordLocation({
      btnEqIndex: 0,
    })
    cy.recordLocation({
      btnEqIndex: 1,
      recordBtnIndex: 1,
    })
    for (let i = 0; i < 3; i++) {
      cy.recordLocation({
        btnEqIndex: 1,
        recordBtnIndex: 1,
      })
      cy.get('[data-cy="addImg"]').click().wait(50)
      cy.get('[data-cy="takePhoto"]').click().wait(50)
      if (i < 2) {
        cy.get('[data-cy="nextPoint"]').click()
      }
    }

    cy.nextStep()
  })
  it('.should() - corrupt collection data in the store', () => {
    cy.window().then((win) => {
      cy.log(
        JSON.stringify(win.bulkStore.collections[11]['photopoints-survey']),
      )
      // need 3 steps
      win.bulkStore.collections[11]['photopoints-survey'].steps.pop()
      // ajv check will fail as this requires uuid format
      win.bulkStore.collections[11][
        'photopoints-survey'
      ].photopoint_3_panorama[0].src = 'bla'
    })
    cy.nextStep()
  })
  it('.should() - receive ajv errors when try to queue collection', () => {
    cy.dataCy('workflowPublish').click()

    cy.dataCy('ajv-error-1').should(
      'contain.text',
      'Invalid Media/File format at "Photopoints Survey/Photopoint 3 Panorama/1/Src"',
    )

    cy.dataCy('ajv-error-0').should(
      'contain.text',
      'Error at "Photopoints Survey/PhotoPoint Position": must NOT have fewer than 3 items',
    )
  })
  it('.should() - go back to the related step having ajv error if clicking edit button', () => {
    cy.dataCy('ajv-edit-0').click()
    cy.dataCy('title', { timeout: 60000 })
      .eq(0)
      .should('contain.text', 'Photopoints Survey (start)')
  })
  it('.should() - the two ajv errors should be visible and formatting error should be an expansion item', () => {
    // should have class q-expansion-item
    cy.dataCy('ajv-error-1')
      .should('be.visible')
      .should('have.class', 'q-expansion-item')
    cy.dataCy('ajv-error-0').should('be.visible')
  })
  it('.should() - fix the data and queue', () => {
    cy.dataCy('nextPoint').click()
    cy.dataCy('nextPoint').click()

    cy.recordLocation({
      btnEqIndex: -1,
      recordBtnIndex: -1,
    })

    cy.get('[data-cy="addImg"]').click().wait(50)
    cy.get('[data-cy="delete"]').click().wait(50)
    cy.get('[data-cy="takePhoto"]').click().wait(50)
    cy.nextStep()
  })
  pub()
  // it('.should() - submit survey', () => {
  //   cy.workflowPublish()
  // })
})

let showAjvErrorsBtn
describe('do photopoints with error, then stash it', () => {
  doPhotoPoint()
  it('.should() - corrupt collection data in the store', () => {
    cy.nextStep()
    cy.window().then((win) => {
      // need 3 steps
      win.bulkStore.collections[11]['photopoints-survey'].steps.pop()
    })
  })
  it('.should() - receive ajv errors when try to queue collection, then stash the collection', () => {
    cy.dataCy('workflowPublish').click()

    cy.dataCy('ajv-error-0').should(
      'contain.text',
      'Error at "Photopoints Survey/PhotoPoint Position": must NOT have fewer than 3 items',
    )
    cy.dataCy('workflowPublishConfirm').click()
    cy.dataCy('submitQueuedCollectionsBtn').should('not.exist')
    cy.dataCy('showAjvErrorsBtn').should('have.length.greaterThan', 0)
    cy.dataCy('showAjvErrorsBtn').then((_) => (showAjvErrorsBtn = _.length))
  })
})
describe('collect new photopoints and sync, the queued items with ajv errors should be the same', () => {
  doPhotoPoint()
  pub()
  it('.should() - the number of queued items should be the same', () => {
    cy.dataCy('showAjvErrorsBtn').should('have.length', showAjvErrorsBtn)
  })
})

const { pub } = require("../../support/command-utils")

// This test will pass when run against a clean Quasar project
const protocol = 'Floristics - Enhanced'
const protocolCy = 'Floristics\\ -\\ Enhanced'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'floristics'
//TODO test PTV submodule (separate test case, like cover/fire)

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocolCy)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
})

describe('Complete survey select', () => {
  it('.should() - complete survey', () => {
    cy.nextStep()
  })
})

describe('1[validate-field] - add species 1', () => {
  it('1.0 - not doing submodule for species', () => {
    cy.dataCy('openPtvModal').should('not.exist')
  })
  it('1.1 -field name ', () => {
    cy.getAndValidate('field_name', {
      testEmptyField: ['done', -1],
    }).selectFromSpeciesList('Sphenopsida ')
  })

  it('1.2 - barcode ', () => {
    cy.getAndValidate('voucher_barcode', {
      isComponent: true,
      testEmptyField: ['done', -1],
    })
    cy.setCheck('toggleBarcodeReaderShow', true)
    cy.dataCy('recordBarcode').click()
  })

  it('1.2 - other fields', () => {
    cy.getAndValidate('growth_form_1', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('Tree')

    cy.getAndValidate('growth_form_2', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('Tree-fern')

    cy.getAndValidate('habit', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown(['Erect', 'Decumbent', 'Creeping'])

    cy.getAndValidate('phenology', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown(['Adult', 'Leaf', 'Fruit'])
  })

  it('1.3 - validate photo', () => {
    cy.getAndValidate('photo', {
      isComponent: true,
      testEmptyField: ['done', -1],
    })
    cy.addImages('addImg')
    cy.dataCy('comment').type('comment')
    cy.dataCy('done').eq(0).click()
    cy.dataCy('done').eq(-1).click()
  })
})

describe('2[test-to-pass] - add species 2', () => {
  it('field name ', () => {
    cy.dataCy('addEntry').click()
    cy.selectFromSpeciesList('field_name', 'Sphenopsida ')
  })
  it('barcode ', () => {
    cy.setCheck('toggleBarcodeReaderShow', true)
    cy.dataCy('recordBarcode').click()
  })

  it('growth form 1', () => {
    cy.selectFromDropdown('growth_form_1', 'Tree')
  })

  it('growth form 2 (optional)', () => {
    cy.selectFromDropdown('growth_form_2', 'Tree-fern')
  })

  it('habit', () => {
    cy.selectFromDropdown('habit', ['Erect', 'Decumbent', 'Creeping'])
  })

  it('phenology', () => {
    cy.selectFromDropdown('phenology', ['Adult', 'Leaf', 'Fruit'])
  })
  it('add 2 photos', () => {
    for (let j = 0; j < 2; j++) {
      cy.wait(50)
      cy.addImages('addImg')
      cy.dataCy('comment').type('comment')
      cy.dataCy('done').eq(0).click()
      if (j < 1) cy.dataCy('addEntry').eq(0).click()
    }
  })

  it('save species record', () => {
    cy.dataCy('done').eq(-1).click()
  })
})

describe('final submit', () => {
  it('publish', () => {
    cy.nextStep()
    // cy.workflowPublish()
  })
  pub()
})

const { cloneDeep } = require('lodash')

const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const selectionAndLayoutModule = 'Plot Selection and Layout'
const layoutVisitProtocol = 'Plot Layout and Visit'
const module = 'soils'
const protocolCharacterisation =
  'Soil Site Observation and Pit Characterisation'
const protocolBulkDensity = 'Soils - Bulk Density'

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
})

let soilPitId = null
//TODO don't test-to-fail fields for this offline test only (but still want non-offline tests to do so) - will need to modify various commands to conditionally test-to-fail (and don't pass ``). Won't be very clean so until we can make calling `validateFields` conditionally easier, just keep test-to-fail of fields
describe(`Collect ${module} protocols`, () => {
  describe('Set Plot Context', () => {
    it('.should() - select plot SATFLB0001', () => {
      cy.selectProtocolIsEnabled(selectionAndLayoutModule)

      cy.dataCy('locationExisting').click()
      cy.selectFromDropdown('id', 'SATFLB0001')
      cy.nextStep()
      cy.dataCy('visitExisting').click()
      cy.selectFromDropdown('id', 'SA autumn survey', null, null, false, true)

      cy.workflowPublishOffline(
        {
          willNotQueue: true,
          models: ['plot-definition-survey', 'plot-fauna-layout', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0001',
          plotVisitName: 'SA autumn survey',
          protocolName: layoutVisitProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'layout',
        },
      )
    })
  })
  let soilPitLiteId = null
  describe('Collection Soil Sample Pit', () => {
    it('select field survey', () => {
      cy.selectProtocolIsEnabled(module, 'Soil Sample Pit')
      // cy.getAndValidate('survey_variant', { isComponent: true })
      cy.dataCy('Field').click()
      cy.get('[aria-label="Soil sample pit ID *"]')
        .invoke('val')
        .then((id) => {
          soilPitLiteId = id
        })
    })
    it('observers', () => {
      cy.getAndValidate('observers')

      cy.addObservers('observerName', ['Dio!!!!!!!!!!'])
    })
    it('soil pit', () => {
      cy.dataCy('collection_method').selectFromDropdown('Differential GNSS')
      cy.recordLocation()

      cy.dataCy('observation_type').selectFromDropdown('Soil pit')

      cy.dataCy('soil_pit_depth').type('5')

      cy.dataCy('digging_stopped_by').selectFromDropdown('Rock')

      cy.addImages('addImg')
      cy.dataCy('comments').type('Test comment')
      cy.nextStep()
    })

    it(`.should() - submit offline collection `, () => {
      cy.workflowPublishOffline({
        models: ['plot-layout', 'plot-visit', 'soil-pit-characterisation-lite'],
        protocolName: 'Soil Sample Pit',
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'soil_sample_pit',
        collectionDependsOn: ['layout'],
      })
    })
  })

  describe(`Collect ${protocolBulkDensity}`, () => {
    it('survey', () => {
      cy.selectProtocolIsEnabled(module, protocolBulkDensity)
      cy.wrap(soilPitLiteId).should('not.be.null').and('not.be.undefined')
      cy.soilBulkDensitySurvey(soilPitLiteId)
      cy.nextStep()
    })

    it(`.should() - submit offline collection for ${protocolBulkDensity} and assert collection was queued correctly`, () => {
      cy.workflowPublishOffline({
        models: ['plot-layout', 'plot-visit', 'soil-bulk-density-survey'],
        protocolName: protocolBulkDensity,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'bulk_density',
        collectionDependsOn: ['layout', 'characterisation'],
      })
      // dismiss the popup
      cy.dismissPopups()
    })
  })
})

describe('Submit queued offline collections', () => {
  it('.should() - failed to submit soil sample due to network dropout', () => {
    cy.dismissPopups()
    //FIXME: Currently, the actual bug occurs when there's a network dropout during the syncing process. We are unable to replicate this in this test 100%, so the approach taken is to override the response to be empty. This simulates the effect of a network dropout, as the app won’t receive a response from the server.
    cy.intercept(
      {
        method: 'POST',
        url: '**/api/soil-pit-characterisation-lites/bulk',
        times: 1,
      },
      (req) => {
        req.continue((res) => {
          // Modify the response after it comes back from the server
          res.send({ forceNetworkError: true })
        })
      },
    ).as('postInterceptBody')
    cy.dataCy('submitQueuedCollectionsBtn')
      .should('be.enabled')
      .click()
      .wait(100)
    cy.dataCy('submitQueuedCollectionsConfirm').click()
    cy.get('.q-notification', { timeout: 360000 })
      .should('exist')
      .and(
        'contain.text',
        "Failed to submit all queued collections.",
      )
    cy.dismissPopups()
  })
  it('.should() - should skip the submitted collection', () => {
    cy.intercept({
      method: 'POST',
      url: '**/api/debug-client-state-dumps',
    }).as('debug-client-state-dumps-spy')
    cy.dataCy('submitQueuedCollectionsBtn')
      .should('be.enabled')
      .click()
      .wait(100)
    cy.dataCy('submitQueuedCollectionsConfirm').click()
    cy.get('.q-notification', { timeout: 360000 })
      .should('exist')
      .and('contain.text', 'Successfully submitted all queued collections')
    cy.wait('@debug-client-state-dumps-spy')
      .its('response.statusCode')
      .should('eq', 200)
  })
})

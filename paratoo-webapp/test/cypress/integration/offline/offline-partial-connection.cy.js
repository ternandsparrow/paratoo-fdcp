const selectionAndLayoutModule = 'Plot Selection and Layout'
const layoutVisitProtocol = 'Plot Layout and Visit'
const plotDescriptionModule = 'Plot Description'
const plotDescriptionEnhancedProtocol = 'Plot Description - Enhanced'
const floristicsModule = 'Floristics'
const floristicsLiteProtocol = 'Floristics - Standard'
const coverModule = 'Cover'
const coverFullProtocol = 'Cover - Enhanced'
const projectCy = 'Kitchen\\ Sink\\ TEST\\ Project'
// beforeEach(goOnline(true))
// afterAll(goOnline(true))
describe('Landing', () => {
  it('.should() - login', () => {
    cy.log(Cypress.env('NETWORK_RECHECK_INTERVAL'))
    cy.login('TestUser', 'password')
  })

  it('.should() - prepare for offline collections', () => {
    cy.prepareForOfflineVisit(projectCy)
  })
})

describe('Layout online, Description offline', () => {
  it(`.should() - navigate to ${layoutVisitProtocol}`, () => {
    cy.selectProtocolIsEnabled(selectionAndLayoutModule)
  })

  it(`.should () - collect new ${layoutVisitProtocol} online`, () => {
    cy.newLayoutAndVisit(
      'QDASEQ0009',
      {
        lat: -35.0004723472134,
        lng: 138.66067886352542,
      },
      false,
      'QLD 9 test visit',
    )
    cy.workflowPublishQueue()
  })

  it('.should() - assert plot context was set correctly', () => {
    cy.assertPlotContext('QDASEQ0009', 'QLD 9 test visit')
  })

  it('.should() - go offline', () => {
    cy.goOffline()
  })

  it(`.should() - navigate to ${plotDescriptionModule}`, () => {
    cy.selectProtocolIsEnabled(
      plotDescriptionModule,
      plotDescriptionEnhancedProtocol,
    )
  })

  it(`.should() - collect ${plotDescriptionModule} offline`, () => {
    cy.fillPlotDescriptionEnhancedForm()
    cy.workflowPublishQueue()
  })

  it('.should() - go online', () => {
    cy.goOnline()
  })

  it(`.should() - submit the queued collections`, () => {
    cy.submitOfflineCollections(null, null)
  })
})

describe('Layout offline, Description online', () => {
  it('.should() - go offline', () => {
    cy.goOffline()
  })

  it(`.should() - navigate to ${layoutVisitProtocol}`, () => {
    cy.selectProtocolIsEnabled(selectionAndLayoutModule)
  })

  it(`.should () - collect new ${layoutVisitProtocol} offline`, () => {
    cy.newLayoutAndVisit(
      'SATFLB0002',
      {
        lat: -34.97258828786832,
        lng: 138.63657495471404,
      },
      false,
      'SA 2 test visit',
    )
    cy.workflowPublishQueue()
  })

  it('.should() - assert plot context was set correctly', () => {
    cy.assertPlotContext('SATFLB0002', 'SA 2 test visit')
  })

  it('.should() - go online', () => {
    cy.goOnline()
  })

  it(`.should() - navigate to ${plotDescriptionModule}`, () => {
    cy.selectProtocolIsEnabled(
      plotDescriptionModule,
      plotDescriptionEnhancedProtocol,
    )
  })

  it(`.should() - collect ${plotDescriptionModule} online`, () => {
    cy.fillPlotDescriptionEnhancedForm()
    cy.workflowPublishQueue()
  })

  it(`.should() - submit the queued collections`, () => {
    cy.submitOfflineCollections(null, null)
  })
})

describe('Floristics online, Cover offline', () => {
  //keep the plot (SATFLB0002) from previous test as context
  it('.should() - assert plot context was set correctly', () => {
    cy.assertPlotContext('SATFLB0002', 'SA 2 test visit')
  })

  it(`.should() - navigate to ${floristicsLiteProtocol}`, () => {
    cy.selectProtocolIsEnabled(floristicsModule, floristicsLiteProtocol)
  })

  //do lite protocol as we can also do voucher re-collections - don't need to do full
  //variant as lite covers it's cases that are relevant for these tests
  it(`.should() - collect ${floristicsLiteProtocol} online`, () => {
    cy.fillInFloristicsLiteForm([
      //vouchers to re-collect
      'Voucher Full Example 1',
      'Voucher Full Example 3',
      'Voucher Lite Example 1',
      'Voucher Lite Example 2',
    ])
    cy.nextStep()
    cy.workflowPublishQueue()
  })

  it('.should() - go offline', () => {
    cy.goOffline()
  })

  it(`.should() - navigate to ${coverFullProtocol}`, () => {
    cy.selectProtocolIsEnabled(coverModule, coverFullProtocol)
  })

  it(`.should() - collect survey step for ${coverFullProtocol}`, () => {
    cy.dataCy('workflowNextButton').click().wait(1000)
  })

  const transects = [
    'South 1',
    'North 2',
    'South 3',
    'North 4',
    'South 5',
    'West 1',
    'East 2',
    'West 3',
    'East 4',
    'West 5',
  ]
  for (const transect of transects) {
    it(`.should() - collect transect ${transect} offline`, () => {
      cy.get(
        '[data-cy="selectingTransect"] > .multiselect__tags > span',
      ).should('have.text', transect)

      cy.dataCy('startTransect').click()

      //5 points per transect in dev mode (normally 101)
      for (let i = 0; i < 5; i++) {
        cy.completePointInterceptPoint(transect, i)
      }
    })
  }

  it(`.should() - finish ${coverFullProtocol}`, () => {
    cy.nextStep()
    cy.workflowPublishQueue()
  })

  it('.should() - go online', () => {
    cy.goOnline()
  })

  it(`.should() - submit the queued collections`, () => {
    cy.submitOfflineCollections(null, null)
  })
})

describe('Floristics offline, Cover online', () => {
  //keep the plot (SATFLB0002) from previous test as context
  it('.should() - assert plot context was set correctly', () => {
    cy.assertPlotContext('SATFLB0002', 'SA 2 test visit')
  })

  it('.should() - go offline', () => {
    cy.goOffline()
  })

  it(`.should() - navigate to ${floristicsLiteProtocol}`, () => {
    cy.selectProtocolIsEnabled(floristicsModule, floristicsLiteProtocol)
  })

  it(`.should() - collect ${floristicsLiteProtocol} offline`, () => {
    cy.fillInFloristicsLiteForm([
      //vouchers to re-collect
      'Voucher Full Example 1',
      'Voucher Full Example 3',
      'Voucher Lite Example 1',
      'Voucher Lite Example 2',
    ])
    cy.nextStep()
    cy.workflowPublishQueue()
  })

  it('.should() - go online', () => {
    cy.goOnline()
  })

  it(`.should() - navigate to ${coverFullProtocol}`, () => {
    cy.selectProtocolIsEnabled(coverModule, coverFullProtocol)
  })

  it(`.should() - collect survey step for ${coverFullProtocol}`, () => {
    cy.dataCy('workflowNextButton').click().wait(1000)
  })

  const transects = [
    'South 1',
    'North 2',
    'South 3',
    'North 4',
    'South 5',
    'West 1',
    'East 2',
    'West 3',
    'East 4',
    'West 5',
  ]
  for (const transect of transects) {
    it(`.should() - collect transect ${transect} offline`, () => {
      cy.get(
        '[data-cy="selectingTransect"] > .multiselect__tags > span',
      ).should('have.text', transect)

      cy.dataCy('startTransect').click()

      //5 points per transect in dev mode (normally 101)
      for (let i = 0; i < 5; i++) {
        cy.completePointInterceptPoint(transect, i)
      }
    })
  }

  it(`.should() - finish ${coverFullProtocol}`, () => {
    cy.nextStep()
    cy.workflowPublishQueue()
  })

  it(`.should() - submit the queued collections`, () => {
    cy.submitOfflineCollections(null, null)
  })
})

import { cloneDeep } from 'lodash'
import { pubOffline } from '../../support/command-utils'

const cameraTrapModule = 'Camera Trapping'
const cameraTrapDeploymentProtocol = 'Camera Trap Deployment'
const cameraTrapReequippingProtocol = 'Camera Trap Reequipping'
const cameraTrapRetrievalProtocol = 'Camera Trap Retrieval'
const projectCy = 'Kitchen\\ Sink\\ TEST\\ Project'

//NOTE: turn this on for debugging - will dump the state to a file before adding a
//collection to queue and before publishing the queue at the end
const DUMP_STATE = false

//will be key=<collection_label>, value=<queuedCollectionIdentifier>
let collectionLabelQueueUuids = {}

function createTodayDate() {
  const todaysDate = new Date()
  const yearStr = todaysDate.getFullYear()
  const monthStr = (() => {
    //quasar abbreviates months to 3-letters, so need to slice
    const monthNames = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ]
    return monthNames[todaysDate.getMonth()].slice(0, 3)
  })()
  const dayStr = String(todaysDate.getDate())
  const dateStr = `${yearStr}-${monthStr}-${dayStr}`
  console.log(`Created date: ${dateStr}`)
  return dateStr
}

let deploymentSurvey = null
//initialise as empty values as the retrieval tests loop over this and will cause Cypress
//to error out when it initialises
let deploymentPoints = [null, null, null]

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })

  it('.should() - prepare for offline collections', () => {
    cy.prepareForOfflineVisit(projectCy)
  })

  it('.should() - go offline', () => {
    cy.window().then(($win) => {
      $win.ephemeralStore.setNetworkOnline(false)
    })
  })
})

describe(`Collect ${cameraTrapModule} protocols`, () => {
  //TODO test array grid/transect & fauna plot survey types
  describe(`Collect ${cameraTrapDeploymentProtocol} ('point' survey type)`, () => {
    //NOTE: most of this logic is directly from the online camera trap tests
    it(`.should() - navigate to ${cameraTrapDeploymentProtocol}`, () => {
      cy.selectProtocolIsEnabled(cameraTrapModule, cameraTrapDeploymentProtocol)
    })

    it('.should() - complete survey step', () => {
      const surveyLabel = `Camera Trap Cypress ${Math.floor(
        Math.random() * 10000,
      )}`
      cy.cameraTrapDeploymentSurvey(surveyLabel)
      deploymentSurvey = {
        survey_label: surveyLabel,
      }
      cy.log(
        `Deployment info saved for use in reequipping and retrieval: ${JSON.stringify(
          deploymentSurvey,
          null,
          2,
        )}`,
      )
      cy.nextStep()
    })

    //point 1
    it('.should() - collect 1st Camera Trap Deployment Point', () => {
      const pointId = `CTP${String(Math.floor(Math.random() * 10000))}`
      const deploymentDate = createTodayDate()
      cy.cameraTrapDeploymentPoint1Pt1(pointId)
      deploymentPoints[0] = {
        camera_trap_point_id: pointId,
        //only really care if year is correct (for reequipping/retrieval), but date still
        //needs to be structured as `YYYY-MM-DD`
        deployment_start_date: deploymentDate,
        deployment_id: `${pointId}-${deploymentDate}`,
      }
    })
    it(".should() - collect 1st Camera Trap Deployment Points' Feature(s)", () => {
      cy.cameraTrapDeploymentPoint1Features()
    })
    it(".should() - collect 1st Camera Trap Deployment Points' Camera Information", () => {
      cy.cameraTrapDeploymentPoint1CameraInfo()
    })
    it(".should() - collect 1st Camera Trap Deployment Points' Camera Settings", () => {
      cy.cameraTrapDeploymentPoint1CameraSettings()
    })
    it('.should() - continue collecting 1st Camera Trap Deployment Point', () => {
      cy.cameraTrapDeploymentPoint1Pt2()
    })

    //point 2
    it('.should() - collect 2nd Camera Trap Deployment Point', () => {
      const pointId = `CTP${String(Math.floor(Math.random() * 10000))}`
      cy.cameraTrapDeploymentPoint2Pt1(pointId)
      const deploymentDate = createTodayDate()
      deploymentPoints[1] = {
        camera_trap_point_id: pointId,
        //only really care if year is correct (for reequipping/retrieval), but date still
        //needs to be structured as `YYYY-MM-DD`
        deployment_start_date: deploymentDate,
        deployment_id: `${pointId}-${deploymentDate}`,
      }
    })
    it(".should() - collect 2nd Camera Trap Deployment Points' Feature(s)", () => {
      cy.cameraTrapDeploymentPoint2Features()
    })
    it(".should() - collect 2nd Camera Trap Deployment Points' Camera Information", () => {
      cy.cameraTrapDeploymentPoint2CameraInfo()
    })
    it(".should() - collect 2nd Camera Trap Deployment Points' Camera Settings", () => {
      cy.cameraTrapDeploymentPoint2CameraSettings()
    })
    it('.should() - continue collecting 2nd Camera Trap Deployment Point', () => {
      cy.cameraTrapDeploymentPoint2Pt2()
    })

    //point 3
    it('.should() - collect 3rd Camera Trap Deployment Point', () => {
      const pointId = `CTP${String(Math.floor(Math.random() * 10000))}`
      const deploymentDate = createTodayDate()
      cy.cameraTrapDeploymentPoint3Pt1()
      deploymentPoints[2] = {
        camera_trap_point_id: pointId,
        //only really care if year is correct (for reequipping/retrieval), but date still
        //needs to be structured as `YYYY-MM-DD`
        deployment_start_date: deploymentDate,
        deployment_id: `${pointId}-${deploymentDate}`,
      }
    })
    it(".should() - collect 3rd Camera Trap Deployment Points' Feature(s)", () => {
      cy.cameraTrapDeploymentPoint3Features()
      console.log(
        'deploymentPoints: ',
        JSON.stringify(deploymentPoints, null, 2),
      )
    })
    it(".should() - collect 3rdCamera Trap Deployment Points' Camera Information", () => {
      cy.cameraTrapDeploymentPoint3CameraInfo()
    })
    it(".should() - collect 3rd Camera Trap Deployment Points' Camera Settings", () => {
      cy.cameraTrapDeploymentPoint3CameraSettings()
    })
    it('.should() - continue collecting 3rd Camera Trap Deployment Point', () => {
      cy.cameraTrapDeploymentPoint3Pt2()
    })

    it('.should() - complete survey step (end)', () => {
      cy.nextStep()
    })

    it(`.should() - submit offline collection for ${cameraTrapDeploymentProtocol} and assert collection was queued correctly`, () => {
      cy.workflowPublishOffline(
        {
          models: [
            'camera-trap-deployment-survey',
            'camera-trap-deployment-point',
          ],
          protocolName: cameraTrapDeploymentProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'deployment',
        },
        assertCtDeploymentCb,
        null,
        DUMP_STATE ? 'offline_ct_deployment' : null,
      )
    })
  })

  //do each reequipping twice, as it's possible to reequip the same camera multiple times
  for (let i = 0; i < 2; i++) {
    describe(`Collect ${cameraTrapReequippingProtocol} ${i + 1}`, () => {
      it(`.should() - navigate to ${cameraTrapReequippingProtocol}`, () => {
        cy.selectProtocolIsEnabled(
          cameraTrapModule,
          cameraTrapReequippingProtocol,
        )
      })

      it('.should() - complete survey step', () => {
        cy.dataCy('survey_label').selectFromDropdown(deploymentSurvey.survey_label)
        cy.log(
          `Reequipping using deployment points: ${JSON.stringify(
            deploymentPoints,
            null,
            2,
          )}`,
        )
        cy.nextStep()
      })

      //need to use indexed loop as when Cypress sets up the tests, it iterates over this
      //loop to create the `it`s - if we iterate over `deploymentPoints`, it won't be
      //defined at setup time
      for (let index = 0; index < 3; index++) {
        switch (index) {
          case 0:
            it('trap 1, vandalised', () => {
              cy.cameraTrapReequippingPoint1(deploymentPoints[index])
            })
            break
          case 1:
            it('trap 2, operational', () => {
              cy.cameraTrapReequippingPoint2(deploymentPoints[index])
            })
            break
          case 2:
            it('trap 3, wildfire damage', () => {
              cy.cameraTrapReequippingPoint3(deploymentPoints[index])
            })
            break
        }
      }

      it('.should() - complete survey step (end)', () => {
        cy.nextStep()
      })

      it(`.should() - submit offline collection for ${cameraTrapReequippingProtocol} #${
        i + 1
      } and assert collection was queued correctly`, () => {
        cy.workflowPublishOffline(
          {
            models: [
              'camera-trap-reequipping-survey',
              'camera-trap-reequipping-point',
            ],
            protocolName: cameraTrapReequippingProtocol,
            projectName: 'Kitchen Sink TEST Project',
            collectionLabel: 'reequipping',
            collectionDependsOn: ['deployment'],
          },
          assertCtReequippingCb,
          null,
          DUMP_STATE ? 'offline_ct_reequipping' : null,
        )
      })
    })
  }

  function startRetrievingCameras() {
    it(`.should() - navigate to ${cameraTrapRetrievalProtocol}`, () => {
      cy.selectProtocolIsEnabled(cameraTrapModule, cameraTrapRetrievalProtocol)
    })

    it('.should() - complete survey step', () => {
      cy.dataCy('survey_label').selectFromDropdown(deploymentSurvey.survey_label)
      cy.nextStep()
    })
  }

  function finishRetrievingCameras() {
    it('.should() - complete survey step (end)', () => {
      cy.nextStep()
    })

    it(`.should() - submit offline collection for ${cameraTrapRetrievalProtocol} and assert collection was queued correctly`, () => {
      cy.workflowPublishOffline(
        {
          models: [
            'camera-trap-retrieval-survey',
            'camera-trap-retrieval-point',
          ],
          protocolName: cameraTrapRetrievalProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'retrieval',
          collectionDependsOn: ['deployment'],
        },
        assertCtRetrievalCb,
        null,
        DUMP_STATE ? 'offline_ct_retrieval' : null,
      )
    })
  }

  describe(`Collect ${cameraTrapRetrievalProtocol}`, () => {
    //need to use indexed loop as when Cypress sets up the tests, it iterates over this
    //loop to create the `it`s - if we iterate over `deploymentPoints`, it won't be
    //defined at setup time
    for (let index = 0; index < 3; index++) {
      switch (index) {
        case 0:
          startRetrievingCameras()
          it('point 1, missing/theft', () => {
            cy.cameraTrapRetrievalPoint1(deploymentPoints[index])
          })
          finishRetrievingCameras()
          break
        case 1:
          startRetrievingCameras()
          it('point 2, unknown failure', () => {
            cy.cameraTrapRetrievalPoint2(deploymentPoints[index], deploymentPoints[0])
          })
          break
        case 2:
          it('point 3, Operational', () => {
            cy.cameraTrapRetrievalPoint3(deploymentPoints[index])
          })
          finishRetrievingCameras()
          break
      }
    }
  })
})
describe('Submit queued offline collections', () => {
  it('.should() - go online', () => {
    cy.window().then(($win) => {
      $win.ephemeralStore.setNetworkOnline(true)
    })
  })

  if (DUMP_STATE) {
    it('.should() - dump state', () => {
      cy.dumpState({
        fileName: 'offline-camera-trapping',
      })
    })
  }

  //we don't make hard relations to any offline data, so no need to hit API and check;
  //the tests themselves will fail if application logic is busted, as the soft links
  //not being created is sufficient to detect this failure
  pubOffline()
})

const assertCtDeploymentCb = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
}) => {
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length).should(
    'eq',
    1,
  )

  const latestQueuedCollection =
    publicationQueueAfter[publicationQueueAfter.length - 1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  cy.log(
    `Setting queued collection identifier for collection label '${
      shouldHaveData.collectionLabel
    }' to: ${
      publicationQueueAfter[publicationQueueAfter.length - 1]
        .queuedCollectionIdentifier
    }`,
  )
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] =
    publicationQueueAfter[
      publicationQueueAfter.length - 1
    ].queuedCollectionIdentifier

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every((o) => collectionModels.includes(o)),
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch (model) {
      case 'camera-trap-deployment-survey':
        //sufficient to just check the survey exists
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        deploymentSurvey = latestQueuedCollection[model]
        break
      case 'camera-trap-deployment-point':
        cy.wrap(Array.isArray(latestQueuedCollection[model])).should('be.true')
        for (const point of latestQueuedCollection[model]) {
          cy.log(
            `Checking deployment point with point ID '${point.camera_trap_point_id}' contains expected data`,
          )
          cy.wrap(point).should(
            'have.any.keys',
            //check subset of keys that contain data we need to use for reequipping and
            //retrieval
            'camera_trap_point_id',
            'camera_trap_information',
            'camera_trap_settings',
            'camera_trap_survey',
            'features',
            'temp_offline_id',
          )

          //these are all child obs
          const fieldsWithTempIds = [
            'camera_trap_information',
            'camera_trap_settings',
            'features',
          ]
          for (const field of fieldsWithTempIds) {
            cy.log(
              `Checking child observation field '${field}' has temp offline ID`,
            )
            if (Array.isArray(point[field])) {
              //multi child ob
              for (const childOb of point[field]) {
                cy.wrap(childOb).should('have.any.keys', 'temp_offline_id')
              }
            } else {
              //single child ob
              cy.wrap(point[field]).should('have.any.keys', 'temp_offline_id')
            }
          }
        }
        deploymentPoints = latestQueuedCollection[model]
        break
    }
  }
}

const assertCtReequippingCb = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
}) => {
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length).should(
    'eq',
    1,
  )

  const latestQueuedCollection =
    publicationQueueAfter[publicationQueueAfter.length - 1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  cy.log(
    `Setting queued collection identifier for collection label '${
      shouldHaveData.collectionLabel
    }' to: ${
      publicationQueueAfter[publicationQueueAfter.length - 1]
        .queuedCollectionIdentifier
    }`,
  )
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] =
    publicationQueueAfter[
      publicationQueueAfter.length - 1
    ].queuedCollectionIdentifier

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      (collectionLabelDep) => collectionLabelQueueUuids[collectionLabelDep],
    )
    cy.log(
      `Checking queued collection tracked collections dependencies that we should have. Comparing superset (actual) / expected: ${JSON.stringify(
        publicationQueueAfter[publicationQueueAfter.length - 1]
          .dependsOnQueuedCollections,
      )} / ${JSON.stringify(shouldHaveDepsUuids)}`,
    )
    cy.wrap(
      shouldHaveDepsUuids.every((dep) =>
        publicationQueueAfter[
          publicationQueueAfter.length - 1
        ].dependsOnQueuedCollections.includes(dep),
      ),
    ).should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every((o) => collectionModels.includes(o)),
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch (model) {
      case 'camera-trap-reequipping-survey':
        //sufficient to just check the survey exists
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        break
      case 'camera-trap-reequipping-point':
        cy.wrap(Array.isArray(latestQueuedCollection[model])).should('be.true')
        for (const [index, point] of latestQueuedCollection[model].entries()) {
          cy.log(
            `Checking reequipping point with deployment ID '${point.deployment_id}' contains expected data`,
          )
          //all points contain these keys
          const baseKeys = [
            'deployment_id',
            'operational_status',
            'replacement_SD_card_number',
            'reequipping_date',
            'camera_trap_mount',
            'bait_station_mount',
            'lure_height',
            'lure_type',
            'lure_variety',
            'reequipping_comments',
          ]
          //each point slightly different data, so only some points contain certain keys
          if (point.camera_needs_replacing) {
            baseKeys.push(
              ...[
                'camera_trap_information',
                'camera_trap_settings',
                'camera_trap_photo',
              ],
            )

            cy.log(
              `Point needs camera to be replaced, comparing with deployment point: ${JSON.stringify(
                deploymentPoints[index],
              )}`,
            )
            cy.log(`Current point to compare with: ${JSON.stringify(point)}`)
            let deploymentSettingToCompare = cloneDeep(
              deploymentPoints[index].camera_trap_settings,
            )
            delete deploymentSettingToCompare.temp_offline_id
            cy.wrap(point.camera_trap_settings).should(
              'deep.equal',
              deploymentSettingToCompare,
            )

            let deploymentInfoToCompare = cloneDeep(
              deploymentPoints[index].camera_trap_information,
            )
            delete deploymentInfoToCompare.temp_offline_id
            cy.wrap(point.camera_trap_information).should(
              'deep.equal',
              deploymentInfoToCompare,
            )
          }
          if (
            point.deployment_id ===
            deploymentPoints[0].deployment_id
            
          ) {
            baseKeys.push(...['carcass_species'])
          }
          cy.wrap(point).should('have.any.keys', ...baseKeys)
        }
        break
    }
  }
}

const assertCtRetrievalCb = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
}) => {
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length).should(
    'eq',
    1,
  )

  const latestQueuedCollection =
    publicationQueueAfter[publicationQueueAfter.length - 1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  cy.log(
    `Setting queued collection identifier for collection label '${
      shouldHaveData.collectionLabel
    }' to: ${
      publicationQueueAfter[publicationQueueAfter.length - 1]
        .queuedCollectionIdentifier
    }`,
  )
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] =
    publicationQueueAfter[
      publicationQueueAfter.length - 1
    ].queuedCollectionIdentifier

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      (collectionLabelDep) => collectionLabelQueueUuids[collectionLabelDep],
    )
    cy.log(
      `Checking queued collection tracked collections dependencies that we should have. Comparing superset (actual) / expected: ${JSON.stringify(
        publicationQueueAfter[publicationQueueAfter.length - 1]
          .dependsOnQueuedCollections,
      )} / ${JSON.stringify(shouldHaveDepsUuids)}`,
    )
    cy.wrap(
      shouldHaveDepsUuids.every((dep) =>
        publicationQueueAfter[
          publicationQueueAfter.length - 1
        ].dependsOnQueuedCollections.includes(dep),
      ),
    ).should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every((o) => collectionModels.includes(o)),
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch (model) {
      case 'camera-trap-retrieval-survey':
        //sufficient to just check the survey exists
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        break
      case 'camera-trap-retrieval-point':
        cy.wrap(Array.isArray(latestQueuedCollection[model])).should('be.true')
        for (const point of latestQueuedCollection[model]) {
          cy.log(
            `Checking retrieval point with point ID '${point.camera_trap_point_id}' contains expected data`,
          )
          //all points contain these keys
          const baseKeys = [
            'camera_trap_point_id',
            'retrieval_date',
            'deployment_period',
            'number_of_images',
            'operational_status',
            'retrieval_comments',
          ]
          cy.wrap(point).should('have.any.keys', ...baseKeys)
        }
        break
    }
  }
}

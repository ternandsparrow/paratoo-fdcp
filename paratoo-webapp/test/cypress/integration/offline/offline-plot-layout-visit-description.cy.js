import { parse } from 'zipson'
import { pubOffline } from '../../support/command-utils'

const selectionAndLayoutModule = 'Plot Selection and Layout'
const layoutVisitProtocol = 'Plot Layout and Visit'
const plotDescriptionModule = 'Plot Description'
const plotDescriptionEnhancedProtocol = 'Plot Description - Enhanced'
const plotDescriptionStandardProtocol = 'Plot Description - Standard'
const projectCy = 'Kitchen\\ Sink\\ TEST\\ Project'

//NOTE: turn this on for debugging - will dump the state to a file before adding a
//collection to queue and before publishing the queue at the end
const DUMP_STATE = false

//will be key=<collection_label>, value=<queuedCollectionIdentifier>
let collectionLabelQueueUuids = {}

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })

  it('.should() - prepare for offline collections', () => {
    cy.prepareForOfflineVisit(projectCy)
  })

  it('.should() - go offline', () => {
    cy.window()
      .then(($win) => {
        $win.ephemeralStore.setNetworkOnline(false)
      })
  })
})

describe('Collection 1 - Existing Layout/Visit, new Fauna Plot, Plot Description (enhanced)', () => {
  it(`.should() - navigate to ${layoutVisitProtocol}`, () => {
    cy.selectProtocolIsEnabled(selectionAndLayoutModule)
  })

  it('.should() - collect existing Layout (QDASEQ0001), new Fauna Plot, and existing Visit', () => {
    cy.dataCy('locationExisting').click()
    cy.selectFromDropdown('id', 'QDASEQ0001')
    cy.dataCy('workflowNextButton').click().wait(3000)
    
    cy.window()
      .then(($win) => {
        //force to out of bounds
        $win.ephemeralStore.updateUserCoords({
          lat: 10,
          lng: 10,
        }, true)
      })
      .then(() => {
        cy.dataCy('faunaPlotYes').click()
        cy.dataCy('startLayout').click().wait(1000)
        cy.dataCy('setStartPoint').click().wait(1000)
        cy.get('.q-notification', { timeout: 15000 })
          .should('contain.text', 'Fauna Plot starting point is out-of-bounds')
        cy.window()
          .then(($win) => {
            //set coords to NW corner of veg/core plot
            $win.ephemeralStore.updateUserCoords({
              lat: -27.388316,
              lng: 152.880797,
            }, true)
          })
          .then(() => {
            //fauna plot
            cy.dataCy('setStartPoint').click().wait(1000)
            cy.dataCy('spoofAllPoints').click().wait(1000)
            cy.dataCy('markClosestPoint').click().wait(1000)  //'done' - closes modal
            cy.dataCy('workflowNextButton').click().wait(3000)
          })
          .then(() => {
            cy.dataCy('visitExisting').click()
            cy.selectFromDropdown('id', 'QLD winter survey', null, null, false, true)
          })
          .then(() => {
            cy.window()
              .then(($win) => {
                //reset coords
                //since we're in Cypress and `force` is false, it will ignore any coords
                $win.ephemeralStore.updateUserCoords()
              })
          })
      })
  })

  it(`.should() - submit offline collection for ${layoutVisitProtocol} and assert collection was queued correctly`, () => {
    // cy.nextStep()
    cy.workflowPublishOffline(
      {
        models: ['plot-definition-survey', 'plot-layout', 'plot-visit'],
        plotLabel: 'QDASEQ0001',
        plotVisitName: 'QLD winter survey',
        protocolName: layoutVisitProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'collection_1_layout',
      },
      assertionsCallback1,
      null,
      DUMP_STATE ? 'offline_layout_collection_1_layout' : null,
    )

    cy.dataCy('plot').filter(':contains("QDASEQ0001 (new fauna plot)")').should('have.length', 1)
  })

  it('.should() - assert plot context was set correctly', () => {
    cy.assertPlotContext('QDASEQ0001', 'QLD winter survey')
  })

  it(`.should() - navigate to ${plotDescriptionEnhancedProtocol}`, () => {
    cy.selectProtocolIsEnabled(plotDescriptionModule, plotDescriptionEnhancedProtocol)
  })

  it(`.should() - collect ${plotDescriptionEnhancedProtocol}`, () => {
    cy.fillPlotDescriptionEnhancedForm()
  })

  it(`.should() - submit offline collection for ${plotDescriptionEnhancedProtocol} and assert collection was queued correctly`, () => {
    cy.workflowPublishOffline(
      {
        models: ['plot-description-enhanced', 'plot-layout', 'plot-visit'],
        plotLabel: 'QDASEQ0001',
        plotVisitName: 'QLD winter survey',
        protocolName: plotDescriptionEnhancedProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'collection_1_description',
      },
      assertionsCallback1,
      null,
      DUMP_STATE ? 'offline_layout_collection_1_description' : null,
    )
  })
})

describe('Collection 2 - collect new Layout/Visit/Fauna & Plot Description (enhanced)', () => {
  it(`.should() - navigate to ${layoutVisitProtocol}`, () => {
    cy.selectProtocolIsEnabled(selectionAndLayoutModule)
  })

  it('.should() - collect new Layout (QDASEQ0009) and Fauna Plot, and new Visit', () => {
    cy.newLayoutAndVisit(
      'QDASEQ0009',
      {
        lat: -35.0004723472134,
        lng: 138.66067886352542,
      },
      true,
      'QLD 9 test visit',
    )
  })

  it(`.should() - submit offline collection for ${layoutVisitProtocol} and assert collection was queued correctly`, () => {
    cy.workflowPublishOffline(
      {
        models: ['plot-definition-survey', 'plot-layout', 'plot-visit'],
        plotLabel: 'QDASEQ0009',
        plotVisitName: 'QLD 9 test visit',
        protocolName: layoutVisitProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'collection_2_layout',
      },
      assertionsCallback2,
      null,
      DUMP_STATE ? 'offline_layout_collection_2_layout' : null,
    )
    cy.dataCy('plot').filter(':contains("QDASEQ0009 (new core and fauna plot)")').should('have.length', 1)
  })

  it('.should() - assert plot context was set correctly', () => {
    cy.assertPlotContext('QDASEQ0009', 'QLD 9 test visit')
  })

  it(`.should() - navigate to ${plotDescriptionStandardProtocol}`, () => {
    cy.selectProtocolIsEnabled(plotDescriptionModule, plotDescriptionStandardProtocol)
  })

  it('.should() - assert Standard cannot be completed without first doing Enhanced', () => {
    cy.get('.q-banner__content')
      .should('contain.text', "The current plot 'QDASEQ0009' has no past record. Please complete the Enhanced protocol first.")
    
    cy.dataCy('workflowBack').click().wait(1000)
  })

  it(`.should() - navigate to ${plotDescriptionEnhancedProtocol}`, () => {
    cy.selectProtocolIsEnabled(plotDescriptionModule, plotDescriptionEnhancedProtocol)
  })

  it(`.should() - collect ${plotDescriptionEnhancedProtocol}`, () => {
    cy.fillPlotDescriptionEnhancedForm()
  })

  it(`.should() - submit offline collection for ${plotDescriptionEnhancedProtocol} and assert collection was queued correctly`, () => {
    cy.workflowPublishOffline(
      {
        models: ['plot-description-enhanced', 'plot-layout', 'plot-visit'],
        plotLabel: 'QDASEQ0009',
        plotVisitName: 'QLD 9 test visit',
        protocolName: plotDescriptionEnhancedProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'collection_2_description',
        collectionDependsOn: [
          'collection_2_layout',
        ],
      },
      assertionsCallback2,
      null,
      DUMP_STATE ? 'offline_layout_collection_2_description' : null,
    )
  })
})

describe('Collection 3 - collect existing Layout w/ new Visit & Plot Description (enhanced), Plot Description (standard)', () => {
  it(`.should() - navigate to ${layoutVisitProtocol}`, () => {
    cy.selectProtocolIsEnabled(selectionAndLayoutModule)
  })

  it('.should - collect existing Layout (QDASEQ0003) and new Visit', () => {
    cy.dataCy('locationExisting').click()
    cy.selectFromDropdown('id', 'QDASEQ0003')
    cy.dataCy('workflowNextButton').click().wait(3000)
    cy.dataCy('workflowNextButton').click().wait(3000)   //skip fauna plot

    cy.dataCy('visitNew').click()
    cy.dataCy('visit_field_name').type('QLD 3 test visit')
  })

  it(`.should() - submit offline collection for ${layoutVisitProtocol} and assert collection was queued correctly`, () => {
    cy.workflowPublishOffline(
      {
        models: ['plot-definition-survey', 'plot-layout', 'plot-visit'],
        plotLabel: 'QDASEQ0003',
        plotVisitName: 'QLD 3 test visit',
        protocolName: layoutVisitProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'collection_3_layout',
      },
      assertionsCallback3,
      null,
      DUMP_STATE ? 'offline_layout_collection_3_layout' : null,
    )
  })

  it(`.should() - navigate to ${plotDescriptionEnhancedProtocol}`, () => {
    cy.selectProtocolIsEnabled(plotDescriptionModule, plotDescriptionEnhancedProtocol)
  })

  it(`.should() - collect ${plotDescriptionEnhancedProtocol}`, () => {
    cy.fillPlotDescriptionEnhancedForm()
  })

  it(`.should() - submit offline collection for ${plotDescriptionEnhancedProtocol} and assert collection was queued correctly`, () => {
    cy.workflowPublishOffline(
      {
        models: ['plot-description-enhanced', 'plot-layout', 'plot-visit'],
        plotLabel: 'QDASEQ0003',
        plotVisitName: 'QLD 3 test visit',
        protocolName: plotDescriptionEnhancedProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: `collection_3_description_1`,
        collectionDependsOn: [
          'collection_3_layout',
        ],
      },
      assertionsCallback3,
      null,
      DUMP_STATE ? 'offline_layout_collection_3_description_1' : null,
    )
  })

  it(`.should() - navigate to ${plotDescriptionEnhancedProtocol}`, () => {
    cy.selectProtocolIsEnabled(plotDescriptionModule, plotDescriptionEnhancedProtocol)
  })

  it(`.should() - assert no more of ${plotDescriptionEnhancedProtocol} can be done for this plot`, () => {
    cy.get('.q-banner__content').should('contain.text', "The current plot 'QDASEQ0003' already has a record for the Enhanced protocol, so this must be a revisit. Only the Standard protocol can be completed for re-visits.")
  })

  it(`.should() - navigate to ${plotDescriptionStandardProtocol}`, () => {
    cy.dataCy('workflowBack').click().wait(1000)
    cy.selectProtocolIsEnabled(plotDescriptionModule, plotDescriptionStandardProtocol)
  })

  it(`.should() - collect ${plotDescriptionStandardProtocol}`, () => {
    cy.fillPlotDescriptionStandardForm()
  })

  it(`.should() - submit offline collection for ${plotDescriptionStandardProtocol} and assert collection was queued correctly`, () => {
    cy.workflowPublishOffline(
      {
        models: ['plot-description-standard', 'plot-layout', 'plot-visit'],
        plotLabel: 'QDASEQ0003',
        plotVisitName: 'QLD 3 test visit',
        protocolName: plotDescriptionStandardProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: `collection_3_description_2`,
        collectionDependsOn: [
          'collection_3_layout',
        ],
      },
      assertionsCallback3,
      null,
      DUMP_STATE ? 'offline_layout_collection_3_description_2' : null,
    )
  })
})

describe('Collection 4 - collect existing Layout (QDASEQ0009, created earlier) and Visit & Plot Description (standard)', () => {
  it(`.should() - navigate to ${layoutVisitProtocol}`, () => {
    cy.selectProtocolIsEnabled(selectionAndLayoutModule)
  })

  it('.should() - collect existing Layout (QDASEQ0009) and Visit', () => {
    cy.dataCy('locationExisting').click()
    cy.selectFromDropdown('id', 'QDASEQ0009')
    cy.dataCy('workflowNextButton').click().wait(3000)
    cy.dataCy('workflowNextButton').click().wait(3000)   //skip fauna plot

    cy.dataCy('visitExisting').click()
    cy.selectFromDropdown('id', 'QLD 9 test visit', null, null, false, true)
  })

  it(`.should() - submit offline collection for ${layoutVisitProtocol} and assert collection was queued correctly`, () => {
    cy.workflowPublishOffline(
      {
        willNotQueue: true,
        models: ['plot-definition-survey', 'plot-layout', 'plot-visit'],
        plotLabel: 'QDASEQ0009',
        plotVisitName: 'QLD 9 test visit',
        protocolName: layoutVisitProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'collection_4_layout',
        collectionDependsOn: [
          'collection_2_layout',
        ],
      },
      assertionsCallback4,
      null,
      DUMP_STATE ? 'offline_layout_collection_4_layout' : null,
    )
  })

  it(`.should() - navigate to ${plotDescriptionEnhancedProtocol}`, () => {
    cy.selectProtocolIsEnabled(plotDescriptionModule,plotDescriptionEnhancedProtocol)
  })

  it(`.should() - assert no more of ${plotDescriptionEnhancedProtocol} can be done for this plot`, () => {
    cy.get('.q-banner__content').should('contain.text', "The current plot 'QDASEQ0009' already has a record for the Enhanced protocol, so this must be a revisit. Only the Standard protocol can be completed for re-visits.")
  })

  it(`.should() - navigate to ${plotDescriptionStandardProtocol}`, () => {
    cy.dataCy('workflowBack').click().wait(1000)
    cy.selectProtocolIsEnabled(plotDescriptionModule, plotDescriptionStandardProtocol)
  })

  it(`.should() - check for past record`, () => {
    cy.dataCy('pastRecordBtn')
      .should('exist')
      .click()

    cy.dataCy('pastRecordTable_initialVisit')
      .should('exist')
    
    cy.dataCy('pastRecordDialogClose').click()
  })

  it(`.should() - collect ${plotDescriptionStandardProtocol}`, () => {
    cy.fillPlotDescriptionStandardForm()
  })

  it(`.should() - submit offline collection for ${plotDescriptionStandardProtocol} and assert collection was queued correctly`, () => {
    cy.workflowPublishOffline(
      {
        models: ['plot-description-standard', 'plot-layout', 'plot-visit'],
        plotLabel: 'QDASEQ0009',
        plotVisitName: 'QLD 9 test visit',
        protocolName: plotDescriptionStandardProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: `collection_4_description`,
        collectionDependsOn: [
          'collection_2_layout',
        ],
      },
      assertionsCallback4,
      null,
      DUMP_STATE ? 'offline_layout_collection_4_description' : null,
    )
  })
})

describe('Collection 5 - collect new Layout/Visit & Plot Description (enhanced)', () => {
  it(`.should() - navigate to ${layoutVisitProtocol}`, () => {
    cy.selectProtocolIsEnabled(selectionAndLayoutModule)
  })

  it('.should() - collect new Layout (SATFLB0002) and Visit', () => {
    cy.newLayoutAndVisit(
      'SATFLB0002',
      {
        lat: -34.97258828786832,
        lng: 138.63657495471404,
      },
      false,
      'SA 2 test visit',
    )
  })

  it(`.should() - submit offline collection for ${layoutVisitProtocol} and assert collection was queued correctly`, () => {
    cy.workflowPublishOffline(
      {
        models: ['plot-definition-survey', 'plot-layout', 'plot-visit'],
        plotLabel: 'SATFLB0002',
        plotVisitName: 'SA 2 test visit',
        protocolName: layoutVisitProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'collection_5_layout',
      },
      assertionsCallback2,
      null,
      DUMP_STATE ? 'offline_layout_collection_5_layout' : null,
    )
    cy.dataCy('plot').filter(':contains("SATFLB0002 (new core plot)")').should('have.length', 1)
  })

  it(`.should() - navigate to ${plotDescriptionEnhancedProtocol}`, () => {
    cy.selectProtocolIsEnabled(plotDescriptionModule, plotDescriptionEnhancedProtocol)
  })

  it(`.should() - collect ${plotDescriptionEnhancedProtocol}`, () => {
    cy.fillPlotDescriptionEnhancedForm()
  })
  
  it(`.should() - submit offline collection for ${plotDescriptionEnhancedProtocol} and assert collection was queued correctly`, () => {
    cy.workflowPublishOffline(
      {
        models: ['plot-description-enhanced', 'plot-layout', 'plot-visit'],
        plotLabel: 'SATFLB0002',
        plotVisitName: 'SA 2 test visit',
        protocolName: plotDescriptionEnhancedProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'collection_5_description',
        collectionDependsOn: [
          'collection_5_layout',
        ],
      },
      assertionsCallback2,
      null,
      DUMP_STATE ? 'offline_layout_collection_5_description' : null,
    )
  })
})

describe('Collection 6 - collect existing Layout (QDASEQ0009, created earlier) and new Visit & Plot Description (standard)', () => {
  it(`.should() - navigate to ${layoutVisitProtocol}`, () => {
    cy.selectProtocolIsEnabled(selectionAndLayoutModule)
  })

  it('.should() - collect existing Layout (QDASEQ0009) and Visit', () => {
    cy.dataCy('locationExisting').click()
    cy.selectFromDropdown('id', 'QDASEQ0009')
    cy.dataCy('workflowNextButton').click().wait(3000)
    cy.dataCy('workflowNextButton').click().wait(3000)   //skip fauna plot

    cy.dataCy('visitNew').click()
    cy.dataCy('visit_field_name').type('QLD 9 test visit 2')
  })

  it(`.should() - submit offline collection for ${layoutVisitProtocol} and assert collection was queued correctly`, () => {
    cy.workflowPublishOffline(
      {
        models: ['plot-definition-survey', 'plot-layout', 'plot-visit'],
        plotLabel: 'QDASEQ0009',
        plotVisitName: 'QLD 9 test visit 2',
        protocolName: layoutVisitProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'collection_6_layout',
        collectionDependsOn: [
          'collection_2_layout',
        ],
      },
      assertionsCallback6,
      null,
      DUMP_STATE ? 'offline_layout_collection_6_layout' : null,
    )
  })

  it(`.should() - navigate to ${plotDescriptionEnhancedProtocol}`, () => {
    cy.selectProtocolIsEnabled(plotDescriptionModule, plotDescriptionEnhancedProtocol)
  })

  it(`.should() - assert no more of ${plotDescriptionEnhancedProtocol} can be done for this plot`, () => {
    cy.get('.q-banner__content').should('contain.text', "The current plot 'QDASEQ0009' already has a record for the Enhanced protocol, so this must be a revisit. Only the Standard protocol can be completed for re-visits.")
  })

  it(`.should() - navigate to ${plotDescriptionStandardProtocol}`, () => {
    cy.dataCy('workflowBack').click().wait(1000)
    cy.selectProtocolIsEnabled(plotDescriptionModule, plotDescriptionStandardProtocol)
  })

  it(`.should() - check for past record`, () => {
    cy.dataCy('pastRecordBtn')
      .should('exist')
      .click()

    cy.dataCy('pastRecordTable_initialVisit')
      .should('exist')
    
    cy.dataCy('pastRecordCarouselNavBtn_revisit_1')
      .should('exist')
      .click()
    
    cy.dataCy('pastRecordTable_revisit_1')
      .should('exist')
    
    cy.dataCy('pastRecordDialogClose').click()
  })

  it(`.should() - collect ${plotDescriptionStandardProtocol}`, () => {
    cy.fillPlotDescriptionStandardForm()
  })

  it(`.should() - submit offline collection for ${plotDescriptionStandardProtocol} and assert collection was queued correctly`, () => {
    cy.workflowPublishOffline(
      {
        models: ['plot-description-standard', 'plot-layout', 'plot-visit'],
        plotLabel: 'QDASEQ0009',
        plotVisitName: 'QLD 9 test visit 2',
        protocolName: plotDescriptionStandardProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'collection_6_description',
        collectionDependsOn: [
          'collection_2_layout',    //plot
          'collection_6_layout',    //visit
        ],
      },
      assertionsCallback6,
      null,
      DUMP_STATE ? 'offline_layout_collection_6_description' : null,
    )
  })
})

describe('Collection 7 - collecting existing Layout (SATFLB0002, created earlier), new Fauna plot, existing Visit', () => {
  it(`.should() - navigate to ${layoutVisitProtocol}`, () => {
    cy.selectProtocolIsEnabled(selectionAndLayoutModule)
  })

  it('.should() - collect existing Layout (SATFLB0002), new Fauna Plot, and existing Visit', () => {
    cy.dataCy('locationExisting').click()
    cy.selectFromDropdown('id', 'SATFLB0002')
    cy.dataCy('workflowNextButton').click().wait(3000)

    //fauna plot
    cy.window()
      .then(($win) => {
        $win.ephemeralStore.updateUserCoords({
          lat: -34.97258828786832,
          lng: 138.63657495471404,
        }, true)
      })
      .then(() => {
        cy.dataCy('faunaPlotYes').click()
        cy.dataCy('startLayout').click().wait(1000)
        cy.dataCy('setStartPoint').click().wait(1000)
        cy.dataCy('spoofAllPoints').click().wait(1000)
        cy.dataCy('markClosestPoint').click().wait(1000)  //'done' - closes modal
        cy.dataCy('workflowNextButton').click().wait(3000)

        cy.dataCy('visitExisting').click()
        cy.selectFromDropdown('id', 'SA 2 test visit', null, null, false, true)
      })
      .then(() => {
        cy.window()
          .then(($win) => {
            //reset coords
            //since we're in Cypress and `force` is false, it will ignore any coords
            $win.ephemeralStore.updateUserCoords()
          })
      })
  })

  it(`.should() - submit offline collection for ${layoutVisitProtocol} and assert collection was queued correctly`, () => {
    // cy.nextStep()
    cy.workflowPublishOffline(
      {
        models: ['plot-definition-survey', 'plot-layout', 'plot-visit'],
        plotLabel: 'SATFLB0002',
        plotVisitName: 'SA 2 test visit',
        protocolName: layoutVisitProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'collection_7_layout',
        collectionDependsOn: [
          'collection_5_layout',    //layout
        ],
      },
      assertionsCallback4,
      null,
      DUMP_STATE ? 'offline_layout_collection_7_layout' : null,
    )
    cy.dataCy('plot').filter(':contains("SATFLB0002 (new fauna plot)")').should('have.length', 1)
  })
})

describe('Submit queued offline collections', () => {
  it('.should() - go online', () => {
    cy.window()
      .then(($win) => {
        $win.ephemeralStore.setNetworkOnline(true)
      })
  })

  if (DUMP_STATE) {
    it('.should() - dump state', () => {
      cy.dumpState({
        fileName: 'offline-plot-layout-visit-description',
      })
    })
  }
  
  const newCollections = [
    //collection 1
    {
      plotLabel: 'QDASEQ0001',
      visitFieldName: 'QLD winter survey',
      plotIsNew: false,
      hasNewFaunaPlot: true,
      visitIsNew: false,
      plotDescriptionVariants: ['enhanced'],
    },
    //collection 2
    {
      plotLabel: 'QDASEQ0009',
      visitFieldName: 'QLD 9 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: true,
      visitIsNew: true,
      plotDescriptionVariants: ['enhanced'],
    },
    //collection 3
    {
      plotLabel: 'QDASEQ0003',
      visitFieldName: 'QLD 3 test visit',
      plotIsNew: false,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      plotDescriptionVariants: ['enhanced', 'standard'],
    },
    //collection 4
    {
      plotLabel: 'QDASEQ0009',
      visitFieldName: 'QLD 9 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: true,
      visitIsNew: true,
      plotDescriptionVariants: ['standard'],
    },
    //collection 5
    {
      plotLabel: 'SATFLB0002',
      visitFieldName: 'SA 2 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      plotDescriptionVariants: ['enhanced'],
    },
    //collection 6
    {
      plotLabel: 'QDASEQ0009',
      visitFieldName: 'QLD 9 test visit 2',
      plotIsNew: false,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      plotDescriptionVariants: ['standard'],
    },
    //collection 7
    {
      plotLabel: 'SATFLB0002',
      visitFieldName: 'SA 2 test visit',
      plotIsNew: false,
      hasNewFaunaPlot: true,
      visitIsNew: false,
    },
  ]
  const cbs = [
    {
      collectionModel: 'plot-layouts',
      cb: (layoutApiResp, collectionsToCheck) => {
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c.plotIsNew || c.hasNewFaunaPlot) {
            const plot = layoutApiResp.body.data.find(
              o => o.attributes.plot_selection.data.attributes.plot_label === c.plotLabel
            )
            console.debug('plot: ', JSON.stringify(plot))
            cy.wrap(plot)
              .should('not.be.undefined')
              .and('not.be.null')
            cy.wrap(plot.attributes)
              .should('not.be.undefined')
              .and('not.be.null')
            
            if (c.plotIsNew) {
              cy.wrap(plot.attributes.plot_points)
                .should('not.be.undefined')
                .and('not.be.null')
            }
            
            if (c.hasNewFaunaPlot) {
              cy.wrap(plot.attributes.fauna_plot_point)
                .should('not.be.undefined')
                .and('not.be.null')
            }
          }
        }
      },
    },
    {
      collectionModel: 'plot-visits',
      cb: (visitApiResp, collectionsToCheck) => {
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c.visitIsNew) {
            const visit = visitApiResp.body.data.find(
              o => o.attributes.visit_field_name === c.visitFieldName
            )
            console.debug('visit: ', JSON.stringify(visit))
            cy.wrap(visit)
              .should('not.be.undefined')
              .and('not.be.null')
            
            const plotLabelForVisit = visit.attributes.plot_layout.data.attributes.plot_selection.data.attributes.plot_label
            cy.wrap(plotLabelForVisit)
              .should('eq', c.plotLabel)
          }
        }
      },
    },
    {
      collectionModel: 'plot-description-enhanceds',
      cb: (descriptionApiResp, collectionsToCheck) => {
        console.debug('descriptionApiResp:', descriptionApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c?.plotDescriptionVariants?.includes('enhanded')) {
            const description = descriptionApiResp.body.data.find(
              o => o.attributes.plot_visit.data.attributes.visit_field_name === c.visitFieldName
            )
            console.debug('description: ', JSON.stringify(description))
            cy.wrap(description)
              .should('not.be.undefined')
              .and('not.be.null')
            
            const plotLabelForDescription = description.attributes.plot_visit.data.attributes.plot_layout.data.attributes.plot_selection.data.attributes.plot_label
            cy.wrap(plotLabelForDescription)
              .should('eq', c.plotLabel)
          }
        }
      },
    },
    {
      collectionModel: 'plot-description-standards',
      cb: (descriptionApiResp, collectionsToCheck) => {
        console.debug('descriptionApiResp:', descriptionApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c?.plotDescriptionVariants?.includes('standard')) {
            const description = descriptionApiResp.body.data.find(
              o => o.attributes.plot_visit.data.attributes.visit_field_name === c.visitFieldName
            )
            console.debug('description: ', JSON.stringify(description))
            cy.wrap(description)
              .should('not.be.undefined')
              .and('not.be.null')
            
            const plotLabelForDescription = description.attributes.plot_visit.data.attributes.plot_layout.data.attributes.plot_selection.data.attributes.plot_label
            cy.wrap(plotLabelForDescription)
              .should('eq', c.plotLabel)
          }
        }
      },
    },
  ]
  pubOffline({
    collectionsToCheck: newCollections,
    collectionIterCbs: cbs,
  })
})

//existing layout and visit
const assertionsCallback1 = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
  apiModelsStore,
}) => {
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
    .should('eq', 1)

  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection

  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      collectionLabelDep => collectionLabelQueueUuids[collectionLabelDep]
    )
    cy.log(`Checking queued collection tracked collections dependencies that we should have. Comparing superset (actual) / expected: ${JSON.stringify(publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections)} / ${JSON.stringify(shouldHaveDepsUuids)}`)
    cy.wrap(
      shouldHaveDepsUuids.every(
        dep => publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.includes(dep)
      )
    ).should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  const resolvedPlotId = apiModelsStore.models['plot-layout'].find(
    o => o.plot_selection.plot_label === shouldHaveData.plotLabel
  )?.id
  const resolvedVisitId = apiModelsStore.models['plot-visit'].find(
    o => o.visit_field_name.includes(shouldHaveData.plotVisitName)
  )?.id
  for (const model of shouldHaveData.models) {
    switch (model) {
      case 'plot-definition-survey':
      case 'plot-description-enhanced':
        cy.wrap(
          Object.keys(latestQueuedCollection[model]).includes('survey_metadata')
        ).should('be.true')
        break
      case 'plot-layout':
        cy.wrap(latestQueuedCollection[model].id)
          .should('eq', resolvedPlotId)
        break
      case 'plot-visit':
        cy.wrap(latestQueuedCollection[model].id)
          .should('eq', resolvedVisitId)
        //visit also has relation to layout
        cy.wrap(latestQueuedCollection[model].plot_layout)
          .should('eq', resolvedPlotId)
        break
    }
  }
}

//new layout and visit
const assertionsCallback2 = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
  apiModelsStore,
}) => {
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
    .should('eq', 1)
  
  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection

  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      collectionLabelDep => collectionLabelQueueUuids[collectionLabelDep]
    )
    cy.log(`Checking queued collection tracked collections dependencies that we should have. Comparing superset (actual) / expected: ${JSON.stringify(publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections)} / ${JSON.stringify(shouldHaveDepsUuids)}`)
    cy.wrap(
      shouldHaveDepsUuids.every(
        dep => publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.includes(dep)
      )
    ).should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  const resolvedPlotSelectionId = apiModelsStore.models['plot-selection'].find(
    o => o.plot_label === shouldHaveData.plotLabel
  )?.id

  for (const model of shouldHaveData.models) {
    switch (model) {
      case 'plot-definition-survey':
      case 'plot-description-enhanced':
        cy.wrap(
          Object.keys(latestQueuedCollection[model]).includes('survey_metadata')
        ).should('be.true')
        break
      case 'plot-layout':
        if (latestQueuedCollection[model].plot_selection) {
          //plot description has this model but not this plot_selection (just temp ID)
          cy.wrap(latestQueuedCollection[model].plot_selection)
            .should('eq', resolvedPlotSelectionId)
        }

        cy.wrap(latestQueuedCollection[model])
          .should('have.any.key', 'temp_offline_id')
        cy.wrap(latestQueuedCollection[model].temp_offline_id)
          .should('not.be.undefined')
          .and('not.be.null')
        break
      case 'plot-visit':
        if (latestQueuedCollection[model].visit_field_name) {
          //plot description has this model but not this visit_field_name (just temp ID)
          cy.wrap(latestQueuedCollection[model].visit_field_name)
            .should('eq', shouldHaveData.plotVisitName)
          
          //visit also has relation to layout
          cy.wrap(latestQueuedCollection[model].plot_layout)
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(latestQueuedCollection[model].plot_layout.temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
            //check the temp ID stored in visit (as relation to layout) is the same as the
            //temp ID in the layout
            .and('eq', latestQueuedCollection['plot-layout'].temp_offline_id)
        }

        cy.wrap(latestQueuedCollection[model])
          .should('have.any.key', 'temp_offline_id')
        cy.wrap(latestQueuedCollection[model].temp_offline_id)
          .should('not.be.undefined')
          .and('not.be.null')
        break
    }
  }
}

//existing layout, new visit
const assertionsCallback3 = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
  apiModelsStore,
}) => {
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
    .should('eq', 1)

  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection

  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      collectionLabelDep => collectionLabelQueueUuids[collectionLabelDep]
    )
    cy.log(`Checking queued collection tracked collections dependencies that we should have. Comparing superset (actual) / expected: ${JSON.stringify(publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections)} / ${JSON.stringify(shouldHaveDepsUuids)}`)
    cy.wrap(
      shouldHaveDepsUuids.every(
        dep => publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.includes(dep)
      )
    ).should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  const resolvedPlotId = apiModelsStore.models['plot-layout'].find(
    o => o.plot_selection.plot_label === shouldHaveData.plotLabel
  )?.id

  for (const model of shouldHaveData.models) {
    switch (model) {
      case 'plot-definition-survey':
      case 'plot-description-enhanced':
      case 'plot-description-standard':
        cy.wrap(
          Object.keys(latestQueuedCollection[model]).includes('survey_metadata')
        ).should('be.true')
        break
      case 'plot-layout':
        cy.wrap(latestQueuedCollection[model].id)
          .should('eq', resolvedPlotId)
        break
      case 'plot-visit':
        if (latestQueuedCollection[model].visit_field_name) {
          //plot description has this model but not this visit_field_name (just temp ID)
          cy.wrap(latestQueuedCollection[model].visit_field_name)
            .should('eq', shouldHaveData.plotVisitName)
          
          //visit also has relation to layout
          cy.wrap(latestQueuedCollection[model])
            .should('have.any.key', 'plot_layout')
          cy.wrap(latestQueuedCollection[model].plot_layout)
            .should('not.be.undefined')
            .and('not.be.null')
            //check the temp ID stored in visit (as relation to layout) is the same as the
            //temp ID in the layout
            .and('eq', latestQueuedCollection['plot-layout'].id)
        }

        cy.wrap(latestQueuedCollection[model])
          .should('have.any.key', 'temp_offline_id')
        cy.wrap(latestQueuedCollection[model].temp_offline_id)
          .should('not.be.undefined')
          .and('not.be.null')
        break
    }
  }
}

//existing layout (QDASEQ0009, created earlier) and visit
const assertionsCallback4 = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
  apiModelsStore,
}) => {
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
    .should('eq', 1)

  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection

  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      collectionLabelDep => collectionLabelQueueUuids[collectionLabelDep]
    )
    cy.log(`Checking queued collection tracked collections dependencies that we should have. Comparing superset (actual) / expected: ${JSON.stringify(publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections)} / ${JSON.stringify(shouldHaveDepsUuids)}`)
    cy.wrap(
      shouldHaveDepsUuids.every(
        dep => publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.includes(dep)
      )
    ).should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    switch (model) {
      case 'plot-definition-survey':
      case 'plot-description-enhanced':
        cy.wrap(
          Object.keys(latestQueuedCollection[model]).includes('survey_metadata')
        ).should('be.true')
        break
      case 'plot-layout':
        //check it's defined
        cy.wrap(latestQueuedCollection[model].id)
          .should('have.any.key', 'temp_offline_id')
        cy.wrap(latestQueuedCollection[model].id.temp_offline_id)
          .should('not.be.undefined')
          .and('not.be.null')
        
        //check the right temp_offline_id is used
        const relevantOfflinePlot = publicationQueueAfter
          .find(o => o.collection['plot-layout']?.temp_offline_id === latestQueuedCollection[model].id.temp_offline_id)
          .collection['plot-layout']
        
        const resolvedPlotSelection = apiModelsStore.models['plot-selection'].find(
          o => o.id === relevantOfflinePlot.plot_selection
        )
        cy.wrap(
          resolvedPlotSelection.plot_label
        ).should('eq', shouldHaveData.plotLabel)
        
        break
      case 'plot-visit':
        //check it's defined
        cy.wrap(latestQueuedCollection[model].id)
          .should('have.any.key', 'temp_offline_id')
        cy.wrap(latestQueuedCollection[model].id.temp_offline_id)
          .should('not.be.undefined')
          .and('not.be.null')
        
        //check the right temp_offline_id is used
        const relevantOfflineVisit = publicationQueueAfter
          .find(o => o.collection['plot-visit']?.temp_offline_id === latestQueuedCollection[model].id.temp_offline_id)
          .collection['plot-visit']
        cy.wrap(
          relevantOfflineVisit.visit_field_name
        ).should('eq', shouldHaveData.plotVisitName)
        break
    }
  }
}

//existing layout (QDASEQ0009, created earlier) and new visit
const assertionsCallback6 = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
  apiModelsStore,
}) => {
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
    .should('eq', 1)

  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection

  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      collectionLabelDep => collectionLabelQueueUuids[collectionLabelDep]
    )
    cy.log(`Checking queued collection tracked collections dependencies that we should have. Comparing superset (actual) / expected: ${JSON.stringify(publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections)} / ${JSON.stringify(shouldHaveDepsUuids)}`)
    cy.wrap(
      shouldHaveDepsUuids.every(
        dep => publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.includes(dep)
      )
    ).should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    switch (model) {
      case 'plot-definition-survey':
      case 'plot-description-enhanced':
        cy.wrap(
          Object.keys(latestQueuedCollection[model]).includes('survey_metadata')
        ).should('be.true')
        break
      case 'plot-layout':
        //check it's defined
        cy.wrap(latestQueuedCollection[model].id)
          .should('have.any.key', 'temp_offline_id')
        cy.wrap(latestQueuedCollection[model].id.temp_offline_id)
          .should('not.be.undefined')
          .and('not.be.null')
        
        //check the right temp_offline_id is used
        const relevantOfflinePlot = publicationQueueAfter
          .find(o => o.collection['plot-layout']?.temp_offline_id === latestQueuedCollection[model].id.temp_offline_id)
          .collection['plot-layout']
        
        const resolvedPlotSelection = apiModelsStore.models['plot-selection'].find(
          o => o.id === relevantOfflinePlot.plot_selection
        )
        cy.wrap(
          resolvedPlotSelection.plot_label
        ).should('eq', shouldHaveData.plotLabel)
        
        break
      case 'plot-visit':
        if (latestQueuedCollection[model].visit_field_name) {
          //plot description has this model but not this visit_field_name (just temp ID)
          cy.wrap(latestQueuedCollection[model].visit_field_name)
            .should('eq', shouldHaveData.plotVisitName)

          //visit also has relation to layout
          cy.wrap(latestQueuedCollection[model].plot_layout)
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(latestQueuedCollection[model].plot_layout.temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
            //check the temp ID stored in visit (as relation to layout) is the same as the
            //temp ID in the layout
            .and('eq', latestQueuedCollection['plot-layout'].id.temp_offline_id)
        }

        cy.wrap(latestQueuedCollection[model])
          .should('have.any.key', 'temp_offline_id')
        cy.wrap(latestQueuedCollection[model].temp_offline_id)
          .should('not.be.undefined')
          .and('not.be.null')
        
        //check the right temp_offline_id is used
        const relevantOfflineVisit = publicationQueueAfter
          .find(o => o.collection['plot-visit']?.temp_offline_id === latestQueuedCollection[model].temp_offline_id)
          .collection['plot-visit']
        cy.wrap(
          relevantOfflineVisit.visit_field_name
        ).should('eq', shouldHaveData.plotVisitName)
        break
    }
  }
}
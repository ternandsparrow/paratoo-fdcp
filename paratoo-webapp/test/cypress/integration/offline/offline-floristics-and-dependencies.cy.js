//TODO refactor usage of `wrap` - doesn't give useful information on failure
//possible solutions:
//  -use a command that still uses `wrap`, but `cy.log`s the expected/actual data
//  -fully refactor to use only built-in assersions (w/o `wrap`) - more time consuming as each will have to be checked it's working as before (in both pass and fail cases)

import {
  isEqual,
  union,
} from 'lodash'
import {
  surveyMetaEqual,
} from '../../support/functions'
import { parse } from 'zipson'
import { pubOffline } from '../../support/command-utils'

global.ignorePaths = ['fire-survey.protocol_variant']

//NOTE: turn this on for debugging - will dump the state to a file before adding a
//collection to queue and before publishing the queue at the end
const DUMP_STATE = false

const selectionAndLayoutModule = 'Plot Selection and Layout'
const layoutVisitProtocol = 'Plot Layout and Visit'
const floristicsModule = 'Floristics'
const floristicsLiteProtocol = 'Floristics - Standard'
const floristicsFullProtocol = 'Floristics - Enhanced'
const ptvModule = 'Plant Tissue Vouchering'
const ptvFullProtocol = 'Plant Tissue Vouchering - Enhanced'
const recruitmentModule = 'recruitment'
const recruitmentAgeStructureProtocol = 'Recruitment - Age Structure'
const recruitmentSurvivorshipProtocol = 'Recruitment - Survivorship'
const conditionProtocol = 'Condition - Attributes'
const conditionModule = 'condition'
const basalWedgeProtocol = 'Basal Area - Basal Wedge'
const basalDbhProtocol = 'Basal Area - DBH'
const basalModule = 'basal area'
const coverModule = 'Cover'
const coverFullProtocol = 'Cover - Enhanced'
const fireFullProtocol = 'Cover + Fire - Enhanced'
const fireModule = 'Fire Severity'
const projectCy = 'Kitchen\\ Sink\\ TEST\\ Project'
const collection1VouchersToRecollect = ['Voucher Full Example 1', 'Voucher Full Example 3', 'Voucher Lite Example 1', 'Voucher Lite Example 2']
const collection3and4VouchersToRecollect = [
  //re-collect sub-set of vouchers collected offline
  'offline voucher lite 1',
  'offline voucher lite 2',
  'offline voucher full 2',
  'offline voucher full 3',
  //also re-collect some vouchers that were collected online
  'Voucher Full Example 4',
  'Voucher Lite Example 2',
]
const basalSamplingPoints = [
  'Northwest',
  'North',
  'Northeast',
  'East',
  'Southeast',
  'South',
  'Southwest',
  'West',
  'Centre',
]

const recruitmentAgeStructureSpeciesToCollect = [
  {
    fieldDataCy: 'floristics_voucher',
    selection: 'Voucher Full Example 2',
    changeSelection: {
      //edit case: online voucher to offline voucher
      selection: 'offline voucher full 2',
    },
    seedlingCount: 5,
    saplingCount: 3,
    juvenileCount: 0,
  },
  {
    fieldDataCy: 'floristics_voucher',
    selection: 'offline voucher full 3',
    changeSelection: {
      //edit case: offline voucher to online voucher
      selection: 'Voucher Full Example 3',
    },
    seedlingCount: 1,
    saplingCount: 5,
    juvenileCount: 2,
  },
  {
    fieldDataCy: 'floristics_voucher',
    selection: 'offline voucher lite 1',
    seedlingCount: 7,
    saplingCount: 0,
    juvenileCount: 0,
  },
  {
    fieldDataCy: 'floristics_voucher',
    selection: 'Voucher Lite Example 2',
    seedlingCount: 0,
    saplingCount: 0,
    juvenileCount: 6,
  },
  {
    fieldDataCy: 'floristics_voucher',
    selection: 'offline voucher full 1',
    seedlingCount: 0,
    saplingCount: 2,
    juvenileCount: 9,
  },
]

const speciesOfflineAndOnlineToCollect = collection3and4VouchersToRecollect.map(o => {
  //NOTE: structured as object to be able to easily add more metadata (like age structure)
  return {
    field_name: o,
  }
})

//will be key=<collection_label>, value=<queuedCollectionIdentifier>
let collectionLabelQueueUuids = {}

//want to track the PI survey so that when we submit to the backend and check it was done
//correctly, we can filter the points to only ones relevant (as the DB might be populated
//with other cover collections, such as from init data)
let coverSurvey = null
let fireSurvey = null
let coverPoints = null
let coverIntercepts = null

let recruitmentSurvivorshipSurveys = {}

let conditionTreeSurveys = {}

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })

  it('.should() - check if VUE_APP_MODE_OF_OPERATION env is set to 0', () => {
    // if already logged it, wills sometimes try checking before the page has loaded properly, which means there is no window object to get
    cy.dataCy('prepareForOfflineVisitBtn').should('exist')
    cy.wait(200)
    //make this check for cover
    cy.checkModeOfOperation(0)
  })

  it('.should() - prepare for offline collections', () => {
    cy.prepareForOfflineVisit(projectCy)
  })

  it('.should() - go offline', () => {
    cy.window()
      .then(($win) => {
        $win.ephemeralStore.setNetworkOnline(false)
      })
  })
})

describe(`Collect ${floristicsLiteProtocol} and ${floristicsFullProtocol}`, () => {
  describe('Collection 1 - vouchers (lite) for existing plot', () => {
    it(`.should() - navigate to ${layoutVisitProtocol}`, () => {
      cy.selectProtocolIsEnabled(selectionAndLayoutModule)
    })

    it('.should() - do Plot Layout (select an existing plot)', () => {
      cy.dataCy('locationExisting').click()
      cy.selectFromDropdown('id', 'QDASEQ0001')
      cy.nextStep()
      cy.nextStep()    //skip fauna plot
      cy.dataCy('visitExisting').click()
      cy.selectFromDropdown('id', 'QLD winter survey', null, null, false, true)
    })

    it(`.should() - submit offline collection for ${layoutVisitProtocol}`, () => {
      //basic callback - can assume it will be good as we already test for this in `offline-plot-layout-visit-description`
      cy.workflowPublishOffline(
        {
          willNotQueue: true,
          models: ['plot-definition-survey', 'plot-fauna-layout', 'plot-layout', 'plot-visit'],
          plotLabel: 'QDASEQ0001',
          plotVisitName: 'QLD winter survey',
          protocolName: layoutVisitProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_1_layout',
        },
        null,
        null,
        DUMP_STATE ? 'offline_floristics_collection_1_layout' : null,
      )
    })

    it('.should() - assert plot context was set correctly', () => {
      cy.assertPlotContext('QDASEQ0001', 'QLD winter survey')
    })

    it(`.should() - navigate to ${floristicsLiteProtocol}`, () => {
      cy.selectProtocolIsEnabled(floristicsModule, floristicsLiteProtocol)
    })

    it(`.should() - collect ${floristicsLiteProtocol}`, () => {
      cy.fillInFloristicsLiteForm(collection1VouchersToRecollect)
    })

    it(`.should() - edit ${floristicsLiteProtocol} and collect host species`, () => {
      for (let i = 0; i < 3; i++) {
        cy.setExpansionItemState(`_${i+1}_`, 'open', 0)
        cy.selectFromDropdown('growth_form_2', 'Epiphyte')
        //selecting 'Epiphyte' should show Host Species field
        cy.dataCy('host_species')
          .should('exist')
        //host species can be from the vouchers we're going to re-collect
        cy.selectFromDropdown('host_species', collection1VouchersToRecollect[i])
        cy.dataCy('done').eq(-1).click()
      }
      cy.nextStep()
    })

    it(`.should() - submit offline collection for ${floristicsLiteProtocol} and assert collection was queued correctly`, () => {
      cy.workflowPublishOffline(
        {
          models: ['floristics-veg-survey-lite', 'floristics-veg-voucher-lite', 'floristics-veg-virtual-voucher', 'plot-layout', 'plot-visit'],
          plotLabel: 'QDASEQ0001',
          plotVisitName: 'QLD winter survey',
          protocolName: floristicsLiteProtocol,
          projectName: 'Kitchen Sink TEST Project',
          newVoucherKeys: ['field_name', 'voucher_barcode', 'growth_form_1', 'growth_form_2', 'habit', 'phenology', 'photo', 'host_species', 'temp_offline_id', 'floristics_veg_survey_lite', 'date_time'],
          existingVoucherKeys: ['growth_form_1', 'habit', 'phenology', 'leaf_photo', 'plant_photo', 'survey', 'date_time'],
          collectionLabel: 'collection_1_floristics',
        },
        assertNewVoucherExistingPlotCb,
        null,
        DUMP_STATE ? 'offline_floristics_collection_1_floristics' : null,
      )
    })
  })

  describe('Collection 2 - vouchers (full) for new plot collected offline', () => {
    it(`.should() - navigate to ${layoutVisitProtocol}`, () => {
      cy.selectProtocolIsEnabled(selectionAndLayoutModule)
    })

    it('.should() - do Plot Layout (create new plot and visit)', () => {
      cy.newLayoutAndVisit(
        'SATFLB0003',
        {
          lat: -34.972959727525875,
          lng: 138.64066633095894
        },
        false,
        'SA 3 test visit',
      )
    })

    it(`.should() - submit offline collection for ${layoutVisitProtocol}`, () => {
      //basic callback - can assume it will be good as we already test for this in `offline-plot-layout-visit-description`
      cy.workflowPublishOffline(
        {
          models: ['plot-definition-survey', 'plot-fauna-layout', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0003',
          plotVisitName: 'SA 3 test visit',
          protocolName: layoutVisitProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_2_layout',
        },
        createCollectionQueueUuidTrackerOnly,
        null,
        DUMP_STATE ? 'offline_floristics_collection_2_layout' : null,
      )
    })

    it('.should() - assert plot context was set correctly', () => {
      cy.assertPlotContext('SATFLB0003', 'SA 3 test visit')
    })

    it(`.should() - navigate to ${floristicsFullProtocol}`, () => {
      cy.selectProtocolIsEnabled(floristicsModule, floristicsFullProtocol)
    })

    it(`.should() - collect ${floristicsFullProtocol}`, () => {
      cy.nextStep()    //survey step

      for (let i = 0; i < 3; i++) {
        cy.setCheck('toggleBarcodeReaderShow', true)
        cy.selectFromSpeciesList('field_name', `offline voucher full ${i+1}`, true)
        cy.dataCy('recordBarcode').eq(-1).click().wait(200)
        cy.selectFromDropdown('growth_form_1', null)
        cy.selectFromDropdown('habit', null)
        cy.selectFromDropdown('phenology', null)
        cy.selectFromDropdown('growth_form_2', null)

        cy.dataCy('done').eq(-1).click()
        if (i < 2) {
          //add new entry except on the last
          cy.dataCy('addEntry').eq(-1).click()
        }
      }
    })

    it(`.should() - edit ${floristicsFullProtocol} and collect host species`, () => {
      for (let i = 0; i < 3; i++) {
        cy.setExpansionItemState(`_${i+1}_`, 'open', 0)
        cy.selectFromDropdown('growth_form_2', 'Epiphyte')
        //selecting 'Epiphyte' should show Host Species field
        cy.dataCy('host_species')
          .should('exist')
        //host species can be from the vouchers we're going to re-collect
        cy.selectFromDropdown('host_species', collection1VouchersToRecollect[i])
        cy.dataCy('done').eq(-1).click()
      }
      cy.nextStep()
    })

    it(`.should() - submit offline collection for ${floristicsFullProtocol} and assert collection was queued correctly`, () => {
      cy.workflowPublishOffline(
        {
          models: ['floristics-veg-survey-full', 'floristics-veg-voucher-full', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0003',
          plotVisitName: 'SA 3 test visit',
          protocolName: floristicsFullProtocol,
          projectName: 'Kitchen Sink TEST Project',
          newVoucherKeys: ['field_name', 'voucher_barcode', 'growth_form_1', 'growth_form_2', 'habit', 'phenology', 'host_species', 'temp_offline_id', 'floristics_veg_survey_full', 'date_time'],
          collectionLabel: 'collection_2_floristics',
          collectionDependsOn: [
            'collection_2_layout'
          ],
        },
        assertNewVoucherNewPlotCb,
        null,
        DUMP_STATE ? 'offline_floristics_collection_2_floristics' : null,
      )
    })
  })

  describe('Collection 3 - re-collect vouchers (lite) collected offline', () => {
    //keep the plot context set in last test
    it('.should() - assert plot context was set correctly', () => {
      cy.assertPlotContext('SATFLB0003', 'SA 3 test visit')
    })

    it(`.should() - navigate to ${floristicsLiteProtocol}`, () => {
      cy.selectProtocolIsEnabled(floristicsModule, floristicsLiteProtocol)
    })

    it(`.should() - collect ${floristicsLiteProtocol}`, () => {
      cy.fillInFloristicsLiteForm(collection3and4VouchersToRecollect, true)
    })

    it('.should() - edit the re-collected vouchers and collect host species', () => {
      //edit the re-collections (add growth form 2)
      for (const fieldName of collection3and4VouchersToRecollect) {
        cy.dataCy(`collectAgainRow_${fieldName.replaceAll(' ', '_')}`).within(() => {
          cy.dataCy('collectAgain').click()
        })
        cy.get('#floristics_virtual_voucher').within(() => {
          cy.selectFromDropdown('growth_form_2', 'Epiphyte')
          //selecting 'Epiphyte' should show Host Species field
          cy.dataCy('host_species')
            .should('exist')
          //select any host species
          cy.selectFromDropdown('host_species', null)
          cy.dataCy('done').eq(-1).click()
        })
      }
      cy.nextStep()
    })

    it(`.should() - submit offline collection for ${floristicsLiteProtocol} and assert collection was queued correctly`, () => {
      cy.workflowPublishOffline(
        {
          models: ['floristics-veg-survey-lite', 'floristics-veg-virtual-voucher', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0003',
          plotVisitName: 'SA 3 test visit',
          protocolName: floristicsLiteProtocol,
          projectName: 'Kitchen Sink TEST Project',
          newVoucherKeys: ['field_name', 'voucher_barcode', 'growth_form_1', 'habit', 'phenology', 'photo', 'temp_offline_id', 'floristics_veg_survey_lite', 'date_time'],
          existingVoucherKeys: ['growth_form_1', 'growth_form_2', 'habit', 'phenology', 'leaf_photo', 'plant_photo', 'host_species', 'survey', 'date_time'],
          collectionLabel: 'collection_3',
          //uses vouchers from collection 1/2, and the new plot from 2
          collectionDependsOn: [
            'collection_1_floristics',
            'collection_2_floristics',
            'collection_2_layout',
          ],
        },
        assertNewVoucherNewPlotCb,
        null,
        DUMP_STATE ? 'offline_floristics_collection_3' : null,
      )
    })
  })
})

describe(`Collect ${ptvFullProtocol} (stand-alone and sub-module)`, () => {
  describe('Collection 4 - Plant Tissue Voucher as stand-alone module', () => {
    it('.should() - assert plot context was set correctly', () => {
      cy.assertPlotContext('SATFLB0003', 'SA 3 test visit')
    })

    it(`.should() - navigate to ${ptvFullProtocol}`, () => {
      //only collect full; lite is same data
      cy.selectProtocolIsEnabled(ptvModule, ptvFullProtocol)
    })

    it(`.should() - collect survey step for ${ptvFullProtocol}`, () => {
      cy.nextStep()
    })

    it(`.should() - collect ${ptvFullProtocol}`, () => {
      for (const [index, floristicsVoucher] of collection3and4VouchersToRecollect.entries()) {
        cy.selectFromDropdown('floristics_voucher', floristicsVoucher)
        cy.dataCy('recordBarcode').click()
        cy.dataCy('done').click().wait(500)

        if (index < (collection3and4VouchersToRecollect.length - 1)) {
          //add new entry except on the last
          cy.dataCy('addEntry').eq(-1).click()
        }
      }
    })

    it(`.should() - edit collected vouchers for ${ptvFullProtocol}`, () => {
      //5th voucher (non-offline) - change the floristics voucher and collect replicates
      cy.setExpansionItemState('_5_', 'open', 0)
      cy.selectFromDropdown('floristics_voucher', 'offline voucher lite 3')
      cy.get('[data-cy="has_replicate"]').click()
      cy.dataCy('min_distance_between_replicates').type('2')
      for (let i = 0; i < 4; i++) {
        cy.dataCy('recordBarcode').click().wait(100)
      }
      cy.dataCy('done').click().wait(500)

      //1st voucher (offline) - change floristics voucher to another offline one
      cy.setExpansionItemState('_1_', 'open', 0)
      cy.selectFromDropdown('floristics_voucher', 'offline voucher full 1')
      cy.dataCy('done').click().wait(500)
      cy.nextStep()
    })

    it(`.should() - submit offline collection for ${ptvFullProtocol} and assert collection was queued correctly`, () => {
      cy.workflowPublishOffline(
        {
          models: ['floristics-veg-genetic-voucher', 'floristics-veg-genetic-voucher-survey', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0003',
          plotVisitName: 'SA 3 test visit',
          protocolName: ptvFullProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_4',
          //uses vouchers from collection 1/2, and the new plot from 2
          collectionDependsOn: [
            'collection_1_floristics',
            'collection_2_floristics',
            'collection_2_layout',
          ],
        },
        assertNewVoucherNewPlotCb,
        null,
        DUMP_STATE ? 'offline_floristics_collection_4' : null,
      )
    })
  })

  describe('Collection 5 - Plant Tissue Voucher as sub-module in Floristics', () => {
    it('.should() - assert plot context was set correctly', () => {
      cy.assertPlotContext('SATFLB0003', 'SA 3 test visit')
    })

    it(`.should() - navigate to ${floristicsFullProtocol}`, () => {
      cy.selectProtocolIsEnabled(floristicsModule, floristicsFullProtocol)
    })

    it(`.should() - complete survey step`, () => {
      cy.ptvSubmoduleInFloristicsSurveyStep('full')
    })

    for (let v = 0; v < 3; v++) {
      it(`.should() - collect Floristics voucher ${v+1} and PTV`, () => {
        cy.collectPtvAsSubmodule(v, 'full', true)
      })
    }

    it(`.should() - edit the 2nd Floristics voucher's PTV`, () => {
      cy.setExpansionItemState('_2_', 'open', 0)

      cy.dataCy('openPtvModal').click().wait(100)
      cy.dataCy('min_distance_between_replicates').type('2')    //will result in val being '12'
      cy.dataCy('done').eq(-1).click()    //saves PTV
      cy.dataCy('closePtvModal').click().wait(100)
      cy.dataCy('done').eq(-1).click()    //saves Floristics Voucher
      cy.nextStep()
    })

    it(`.should() - submit offline collection for ${floristicsFullProtocol} (with submodule ${ptvFullProtocol}) and assert collection was queued correctly`, () => {
      cy.workflowPublishOffline(
        {
          models: {
            plantTissue: ['floristics-veg-genetic-voucher', 'floristics-veg-genetic-voucher-survey', 'plot-layout', 'plot-visit'],
            floristics: ['floristics-veg-survey-full', 'floristics-veg-voucher-full', 'plot-layout', 'plot-visit'],
          },
          plotLabel: 'SATFLB0003',
          plotVisitName: 'SA 3 test visit',
          //use PTV rather than Floristics so that we get correct auth context
          protocolName: ptvFullProtocol,
          projectName: 'Kitchen Sink TEST Project',
          //for the Floristics parent module
          newVoucherKeys: ['field_name', 'growth_form_1', 'temp_offline_id', 'unique_id', 'voucher_barcode', 'floristics_veg_survey_full', 'date_time'],
          collectionLabel: 'collection_5',
          collectionLabelHasSubmodule: true,
          //uses new plot from collection 2
          collectionDependsOn: [
            'collection_2_layout',
          ],
        },
        assertNewVoucherNewPlotCb,
        null,
        DUMP_STATE ? 'offline_floristics_collection_5' : null,
      )
    })
  })
})

describe(`Collection 6 - ${recruitmentAgeStructureProtocol}`, () => {
  it('.should() - assert plot context was set correctly', () => {
    cy.assertPlotContext('SATFLB0003', 'SA 3 test visit')
  })

  it(`.should() - navigate to ${recruitmentAgeStructureProtocol}`, () => {
    cy.selectProtocolIsEnabled(recruitmentModule, recruitmentAgeStructureProtocol)
  })

  it(`.should() - collect survey step for ${recruitmentAgeStructureProtocol}`, () => {
    cy.nextStep()
  })

  it(`.should() - collect growth and life stages step (using online and offline voucher, and new species from field names)`, () => {
    cy.collectRecruitmentGrowthFormAndLifeStages(recruitmentAgeStructureSpeciesToCollect)
  })
  
  it(`.should() - edit collected growth form and life stages`, () => {
    for (const [index, species] of recruitmentAgeStructureSpeciesToCollect.entries()) {
      if (species.changeSelection) {
        const changeSelectionKeys = Object.keys(species.changeSelection)
        if (
          typeof species.selection === 'string' &&
          changeSelectionKeys.includes('selection') &&
          typeof species.changeSelection.selection === 'string'
        ) {
          //edit cases: online voucher to offline voucher; offline voucher to online voucher
          cy.log(`Editing online/offline voucher '${species.selection}' to be offline/online voucher '${species.changeSelection.selection}'`)
          cy.dataCy(`editSpecies_${index}`).click().wait(500)
          cy.selectFromDropdown('floristics_voucher', species.changeSelection.selection)
        }

        //child ob is still open so don't want to select it
        cy.dataCy('done').eq(-1).click()
      }
    }

    cy.nextStep()
  })

  it(`.should() - collect sapling and seedling count step`, () => {
    cy.collectSaplingAndSeedlingCount(recruitmentAgeStructureSpeciesToCollect)
    cy.nextStep()
  })

  it(`.should() - submit offline collection for ${recruitmentAgeStructureProtocol} and assert collection was queued correctly`, () => {
    cy.workflowPublishOffline(
      {
        models: ['recruitment-field-survey', 'recruitment-growth-stage', 'recruitment-sapling-and-seedling-count', 'plot-layout', 'plot-visit'],
        plotLabel: 'SATFLB0003',
        plotVisitName: 'SA 3 test visit',
        protocolName: recruitmentAgeStructureProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'collection_6',
        //uses new plot and vouchers from collection 2, vouchers from collection 1
        collectionDependsOn: [
          'collection_1_floristics',
          'collection_2_floristics',
          'collection_2_layout',
        ],
      },
      assertRecruitmentCb,
      null,
      DUMP_STATE ? 'offline_floristics_collection_6' : null,
    )
  })
})

describe(`Collect ${recruitmentSurvivorshipProtocol}`, () => {
  describe(`Collection 7 - ${recruitmentSurvivorshipProtocol} visit/revisit from online-collected plot visit`, () => {
    it('.should() - select plot SATFLB0001', () => {
      cy.selectProtocolIsEnabled(selectionAndLayoutModule)
      
      cy.dataCy('locationExisting').click()
      cy.selectFromDropdown('id', 'SATFLB0001')
      cy.nextStep()
      cy.dataCy('visitExisting').click()
      cy.selectFromDropdown('id', 'SA autumn survey', null, null, false, true)

      cy.workflowPublishOffline(
        {
          willNotQueue: true,
          models: ['plot-definition-survey', 'plot-fauna-layout', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0001',
          plotVisitName: 'SA autumn survey',
          protocolName: layoutVisitProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_7_layout',
        },
        null,
        null,
        DUMP_STATE ? 'offline_floristics_collection_7_layout' : null,
      )
    })

    it('.should() - assert plot context was set correctly', () => {
      cy.assertPlotContext('SATFLB0001', 'SA autumn survey')
    })

    it(`.should() - navigate to ${recruitmentSurvivorshipProtocol}`, () => {
      cy.selectProtocolIsEnabled(recruitmentModule, recruitmentSurvivorshipProtocol)
    })
    
    it('.should() - collect new visit - survey step', () => {
      cy.recruitmentSurvivorshipSurvey({
        vouchers: speciesOfflineAndOnlineToCollect,
        isNewVisit: true,
      })
    })

    for (const [index, voucher] of speciesOfflineAndOnlineToCollect.entries()) {
      it(`.should() - collect new visit - observation ${index+1}`, () => {
        cy.recruitmentSurvivorshipObservation({
          voucher: voucher,
          shouldAddEntry: index < speciesOfflineAndOnlineToCollect.length-1,
        })
      })
    }
    
    it(`.should() - submit offline collection for ${recruitmentSurvivorshipProtocol} (new visit) and assert collection was queued correctly`, () => {
      cy.nextStep()
      cy.workflowPublishOffline(
        {
          models: ['recruitment-survivorship-survey', 'recruitment-survivorship-observation', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0001',
          layoutIsNew: false,
          plotVisitName: 'SA autumn survey',
          visitIsNew: false,
          protocolName: recruitmentSurvivorshipProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_7_new',
          //uses vouchers from collection 1/2
          collectionDependsOn: [
            'collection_1_floristics',
            'collection_2_floristics',
            //don't actually use this plot, but the vouchers from collection 2 cause this
            //dep so include it
            'collection_2_layout',
          ],
        },
        assertRecruitmentSurvivorshipCb,
        null,
        DUMP_STATE ? 'offline_floristics_collection_7_new' : null,
      )
    })

    it('.should() - assert plot context was set correctly', () => {
      cy.assertPlotContext('SATFLB0001', 'SA autumn survey')
    })

    it(`.should() - navigate to ${recruitmentSurvivorshipProtocol}`, () => {
      cy.selectProtocolIsEnabled(recruitmentModule, recruitmentSurvivorshipProtocol)
    })

    it('.should() - collect revisit - survey step', () => {
      cy.recruitmentSurvivorshipSurvey({
        vouchers: speciesOfflineAndOnlineToCollect,
        isNewVisit: false,
      })
    })

    for (const [index, voucher] of speciesOfflineAndOnlineToCollect.entries()) {
      it(`.should() - collect revisit - observation ${index+1}`, () => {
        cy.recruitmentSurvivorshipObservation({
          voucher: voucher,
          shouldAddEntry: index < speciesOfflineAndOnlineToCollect.length-1,
        })
      })
    }

    it(`.should() - submit offline collection for ${recruitmentSurvivorshipProtocol} (revisit) and assert collection was queued correctly`, () => {
      cy.nextStep()
      cy.workflowPublishOffline(
        {
          models: ['recruitment-survivorship-survey', 'recruitment-survivorship-observation', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0001',
          layoutIsNew: false,
          plotVisitName: 'SA autumn survey',
          visitIsNew: false,
          protocolName: recruitmentSurvivorshipProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_7_revisit',
          //uses vouchers from collection 1/2
          collectionDependsOn: [
            'collection_1_floristics',
            'collection_2_floristics',
            //don't actually use this plot, but the vouchers from collection 2 cause this
            //dep so include it
            'collection_2_layout',
          ],
        },
        assertRecruitmentSurvivorshipCb,
        null,
        DUMP_STATE ? 'offline_floristics_collection_7_revisit' : null,
      )
    })
  })

  describe(`Collection 8 - ${recruitmentSurvivorshipProtocol} revisit from offline-collected plot visit (layout collected online)`, () => {
    it('.should() - select plot SATFLB0001 and create new visit', () => {
      cy.selectProtocolIsEnabled(selectionAndLayoutModule)
      
      cy.dataCy('locationExisting').click()
      cy.selectFromDropdown('id', 'SATFLB0001')
      cy.nextStep()
      cy.dataCy('visitNew').click()
      cy.dataCy('visit_field_name').type('SA 1 test visit')

      cy.workflowPublishOffline(
        {
          models: ['plot-definition-survey', 'plot-fauna-layout', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0001',
          plotVisitName: 'SA 1 test visit',
          visitIsNew: true,
          protocolName: layoutVisitProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_8_layout',
        },
        createCollectionQueueUuidTrackerOnly,
        null,
        DUMP_STATE ? 'offline_floristics_collection_8_layout' : null,
      )
    })

    it('.should() - assert plot context was set correctly', () => {
      cy.assertPlotContext('SATFLB0001', 'SA 1 test visit')
    })

    it(`.should() - navigate to ${recruitmentSurvivorshipProtocol}`, () => {
      cy.selectProtocolIsEnabled(recruitmentModule, recruitmentSurvivorshipProtocol)
    })

    it('.should() - collect revisit - survey step', () => {
      cy.recruitmentSurvivorshipSurvey({
        vouchers: speciesOfflineAndOnlineToCollect,
        isNewVisit: false,
      })
    })

    for (const [index, voucher] of speciesOfflineAndOnlineToCollect.entries()) {
      it(`.should() - collect revisit - observation ${index+1}`, () => {
        cy.recruitmentSurvivorshipObservation({
          voucher: voucher,
          shouldAddEntry: index < speciesOfflineAndOnlineToCollect.length-1,
        })
      })
    }
    
    it(`.should() - submit offline collection for ${recruitmentSurvivorshipProtocol} (revisit) and assert collection was queued correctly`, () => {
      cy.nextStep()
      cy.workflowPublishOffline(
        {
          models: ['recruitment-survivorship-survey', 'recruitment-survivorship-observation', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0001',
          layoutIsNew: false,
          plotVisitName: 'SA 1 test visit',
          visitIsNew: true,
          protocolName: recruitmentSurvivorshipProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_8_revisit',
          //uses vouchers from collection 1/2
          collectionDependsOn: [
            'collection_1_floristics',
            'collection_2_floristics',
            //don't actually use this plot, but the vouchers from collection 2 cause this
            //dep so include it
            'collection_2_layout',
            //we created a visit offline
            'collection_8_layout',
          ],
        },
        assertRecruitmentSurvivorshipCb,
        null,
        DUMP_STATE ? 'offline_floristics_collection_8_revisit' : null,
      )
    })
  })
  
  describe(`Collection 9 - ${recruitmentSurvivorshipProtocol} visit/revisit from same offline-collected plot/visit`, () => {
    it('.should() - select plot SATFLB0003', () => {
      cy.selectProtocolIsEnabled(selectionAndLayoutModule)
      
      cy.dataCy('locationExisting').click()
      cy.selectFromDropdown('id', 'SATFLB0003')
      cy.nextStep()
      cy.nextStep()   //skip fauna plot
      cy.dataCy('visitExisting').click()
      cy.selectFromDropdown('id', 'SA 3 test visit', null, null, false, true)

      cy.workflowPublishOffline(
        {
          willNotQueue: true,
          models: ['plot-definition-survey', 'plot-fauna-layout', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0003',
          plotVisitName: 'SA 3 test visit',
          protocolName: layoutVisitProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_9_layout',
        },
        null,
        null,
        DUMP_STATE ? 'offline_floristics_collection_9_layout' : null,
      )
    })

    it('.should() - assert plot context was set correctly', () => {
      cy.assertPlotContext('SATFLB0003', 'SA 3 test visit')
    })

    it(`.should() - navigate to ${recruitmentSurvivorshipProtocol}`, () => {
      cy.selectProtocolIsEnabled(recruitmentModule, recruitmentSurvivorshipProtocol)
    })
    
    it('.should() - collect new visit - survey step', () => {
      cy.recruitmentSurvivorshipSurvey({
        vouchers: speciesOfflineAndOnlineToCollect,
        isNewVisit: true,
      })
    })

    for (const [index, voucher] of speciesOfflineAndOnlineToCollect.entries()) {
      it(`.should() - collect new visit - observation ${index+1}`, () => {
        cy.recruitmentSurvivorshipObservation({
          voucher: voucher,
          shouldAddEntry: index < speciesOfflineAndOnlineToCollect.length-1,
        })
      })
    }
    
    it(`.should() - submit offline collection for ${recruitmentSurvivorshipProtocol} (new visit) and assert collection was queued correctly`, () => {
      cy.nextStep()
      cy.workflowPublishOffline(
        {
          models: ['recruitment-survivorship-survey', 'recruitment-survivorship-observation', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0003',
          plotIsNew: true,
          plotVisitName: 'SA 3 test visit',
          visitIsNew: true,
          protocolName: recruitmentSurvivorshipProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_9_new',
          //uses vouchers from collection 1/2 and layout from collection 2
          collectionDependsOn: [
            'collection_1_floristics',
            'collection_2_floristics',
            'collection_2_layout',
          ],
        },
        assertRecruitmentSurvivorshipCb,
        null,
        DUMP_STATE ? 'offline_floristics_collection_9_new' : null,
      )
    })

    it('.should() - assert plot context was set correctly', () => {
      cy.assertPlotContext('SATFLB0003', 'SA 3 test visit')
    })

    it(`.should() - navigate to ${recruitmentSurvivorshipProtocol}`, () => {
      cy.selectProtocolIsEnabled(recruitmentModule, recruitmentSurvivorshipProtocol)
    })

    it('.should() - collect revisit - survey step', () => {
      cy.recruitmentSurvivorshipSurvey({
        vouchers: speciesOfflineAndOnlineToCollect,
        isNewVisit: false,
      })
    })

    for (const [index, voucher] of speciesOfflineAndOnlineToCollect.entries()) {
      it(`.should() - collect revisit - observation ${index+1}`, () => {
        cy.recruitmentSurvivorshipObservation({
          voucher: voucher,
          shouldAddEntry: index < speciesOfflineAndOnlineToCollect.length-1,
        })
      })
    }

    it(`.should() - submit offline collection for ${recruitmentSurvivorshipProtocol} (revisit) and assert collection was queued correctly`, () => {
      cy.nextStep()
      cy.workflowPublishOffline(
        {
          models: ['recruitment-survivorship-survey', 'recruitment-survivorship-observation', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0003',
          plotIsNew: true,
          plotVisitName: 'SA 3 test visit',
          visitIsNew: true,
          protocolName: recruitmentSurvivorshipProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_9_revisit',
          //uses vouchers from collection 1/2 and layout from collection 2
          collectionDependsOn: [
            'collection_1_floristics',
            'collection_2_floristics',
            'collection_2_layout',
          ],
        },
        assertRecruitmentSurvivorshipCb,
        null,
        DUMP_STATE ? 'offline_floristics_collection_9_revisit' : null,
      )
    })
  })

  describe(`Collection 10 - ${recruitmentSurvivorshipProtocol} revisit from different offline-collected plot visit (layout from before)`, () => {
    it('.should() - select plot SATFLB0003 and create new visit', () => {
      cy.selectProtocolIsEnabled(selectionAndLayoutModule)
      
      cy.dataCy('locationExisting').click()
      cy.selectFromDropdown('id', 'SATFLB0003')
      cy.nextStep()
      cy.nextStep()   //skip fauna plot
      cy.dataCy('visitNew').click()
      cy.dataCy('visit_field_name').type('SA 3 test visit 2')

      cy.workflowPublishOffline(
        {
          models: ['plot-definition-survey', 'plot-fauna-layout', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0003',
          plotVisitName: 'SA 3 test visit 2',
          protocolName: layoutVisitProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_10_layout',
        },
        createCollectionQueueUuidTrackerOnly,
        null,
        DUMP_STATE ? 'offline_floristics_collection_10_layout' : null,
      )
    })

    it('.should() - assert plot context was set correctly', () => {
      cy.assertPlotContext('SATFLB0003', 'SA 3 test visit 2')
    })

    it(`.should() - navigate to ${recruitmentSurvivorshipProtocol}`, () => {
      cy.selectProtocolIsEnabled(recruitmentModule, recruitmentSurvivorshipProtocol)
    })

    it('.should() - collect revisit - survey step', () => {
      cy.recruitmentSurvivorshipSurvey({
        vouchers: speciesOfflineAndOnlineToCollect,
        isNewVisit: false,
      })
    })

    for (const [index, voucher] of speciesOfflineAndOnlineToCollect.entries()) {
      it(`.should() - collect revisit - observation ${index+1}`, () => {
        cy.recruitmentSurvivorshipObservation({
          voucher: voucher,
          shouldAddEntry: index < speciesOfflineAndOnlineToCollect.length-1,
        })
      })
    }
    
    it(`.should() - submit offline collection for ${recruitmentSurvivorshipProtocol} (revisit) and assert collection was queued correctly`, () => {
      cy.nextStep()
      cy.workflowPublishOffline(
        {
          models: ['recruitment-survivorship-survey', 'recruitment-survivorship-observation', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0003',
          plotIsNew: true,
          plotVisitName: 'SA 3 test visit 2',
          visitIsNew: true,
          protocolName: recruitmentSurvivorshipProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_10_revisit',
          //uses vouchers from collection 1/2 and layout from collection 2
          collectionDependsOn: [
            'collection_1_floristics',
            'collection_2_floristics',
            'collection_2_layout',
            //we reselect a layout from the queue, so have a dep to that collection
            'collection_10_layout',
          ],
        },
        assertRecruitmentSurvivorshipCb,
        null,
        DUMP_STATE ? 'offline_floristics_collection_10_revisit' : null,
      )
    })
  })
})

describe(`Collect ${conditionProtocol}`, () => {
  for (const variant of ['standard', 'enhanced']) {
    const collectionNumber = (collectionVariant) => {
      if (collectionVariant === 'standard') {
        return '11'
      }
      if (collectionVariant === 'enhanced') {
        return '12'
      }
    }
    describe(`Collection ${collectionNumber(variant)} - ${conditionProtocol} specific protocol data (${variant})`, () => {
      //previous test(s) changed plot so change it back
      it('.should() - select plot SATFLB0003', () => {
        cy.selectProtocolIsEnabled(selectionAndLayoutModule)

        cy.dataCy('locationExisting').click()
        cy.selectFromDropdown('id', 'SATFLB0003')
        cy.nextStep()
        cy.nextStep()    //skip fauna plot
        cy.dataCy('visitExisting').click()
        cy.selectFromDropdown('id', 'SA 3 test visit', null, null, false, true)

        cy.workflowPublishOffline(
          {
            willNotQueue: true,
            models: ['plot-definition-survey', 'plot-fauna-layout', 'plot-layout', 'plot-visit'],
            plotLabel: 'SATFLB0003',
            plotVisitName: 'SA 3 test visit',
            protocolName: layoutVisitProtocol,
            projectName: 'Kitchen Sink TEST Project',
            collectionLabel: `collection_${collectionNumber(variant)}_layout`,
          },
          null,
          null,
          DUMP_STATE ? `collection_${collectionNumber(variant)}_layout` : null,
        )
      })
      it('.should() - assert plot context was set correctly', () => {
        cy.assertPlotContext('SATFLB0003', 'SA 3 test visit')
      })
  
      it('.should() - assert navigation to ' + conditionProtocol + ' is possible', () => {
        cy.selectProtocolIsEnabled(conditionModule)
      })
  
      it('.should() - collect protocol', () => {
        cy.condition({
          protocolVariant: variant,
          vouchersToCollect: speciesOfflineAndOnlineToCollect.map(o => o.field_name),
        })
        cy.nextStep()
      })
  
      it(`.should() - submit offline collection for ${conditionProtocol} (${variant}) and assert collection was queued correctly`, () => {
        cy.workflowPublishOffline(
          {
            models: ['condition-survey', 'condition-tree-survey', 'plot-layout', 'plot-visit'],
            plotLabel: 'SATFLB0003',
            plotIsNew: true,
            plotVisitName: 'SA 3 test visit',
            visitIsNew: true,
            protocolName: conditionProtocol,
            projectName: 'Kitchen Sink TEST Project',
            collectionLabel: `collection_${collectionNumber(variant)}`,
            //uses vouchers from collection 1/2 and layout from collection 2
            collectionDependsOn: [
              'collection_1_floristics',
              'collection_2_floristics',
              'collection_2_layout',
            ],
          },
          assertConditionCb,
          null,
          DUMP_STATE ? `collection_${collectionNumber(variant)}` : null,
        )
      })
    })
  }
})

describe(`Collection 13 - ${basalWedgeProtocol}`, () => {
  //keep plot from previous test
  it('.should() - assert plot context was set correctly', () => {
    cy.assertPlotContext('SATFLB0003', 'SA 3 test visit')
  })

  it(`.should() - navigate to ${basalWedgeProtocol}`, () => {
    cy.selectProtocolIsEnabled(basalModule, basalWedgeProtocol)
    cy.nextStep()
  })

  for (const samplingPoint of basalSamplingPoints) {
    it(`Collect sampling point ${samplingPoint}`, () => {
      cy.basalWedgeSamplingPoint(
        samplingPoint,
        speciesOfflineAndOnlineToCollect.map(o => o.field_name),
        false,
      )
    })
  }
  
  it(`.should() - submit offline collection for ${basalWedgeProtocol} and assert collection was queued correctly`, () => {
    cy.nextStep()
    cy.workflowPublishOffline(
      {
        models: ['plot-layout', 'plot-visit', 'basal-wedge-survey', 'basal-wedge-observation'],
        plotLabel: 'SATFLB0003',
        plotIsNew: true,
        plotVisitName: 'SA 3 test visit',
        visitIsNew: true,
        protocolName: basalWedgeProtocol,
        projectName: 'Kitchen Sink TEST Project',
        collectionLabel: 'collection_13',
        //uses vouchers from collection 1/2 and layout from collection 2
        collectionDependsOn: [
          'collection_1_floristics',
          'collection_2_floristics',
          'collection_2_layout',
        ],
      },
      assertBasalWedgeCb,
      null,
      DUMP_STATE ? 'offline_floristics_collection_13' : null,
    )
  })
})

describe(`Collect ${basalDbhProtocol}`, () => {
  const collectionNumber = (dbhInstrument) => {
    if (dbhInstrument === 'Diameter Tape Measure') {
      return '14'
    }
    if (dbhInstrument === 'Tape Measure') {
      return '15'
    }
    if (dbhInstrument === 'Tree Caliper') {
      return '16'
    }
  }
  for (const dbhInstrument of ['Diameter Tape Measure', 'Tape Measure', 'Tree Caliper']) {
    describe(`Collection ${collectionNumber(dbhInstrument)} - ${basalDbhProtocol} (${dbhInstrument})`, () => {
      //keep plot from previous test
      it('.should() - assert plot context was set correctly', () => {
        cy.assertPlotContext('SATFLB0003', 'SA 3 test visit')
      })

      it(`.should() - navigate to ${basalDbhProtocol}`, () => {
        cy.selectProtocolIsEnabled(basalModule, basalDbhProtocol)
      })

      it(`.should() - complete survey step (select DBH Instrument ${dbhInstrument})`, () => {
        cy.get('[data-cy="100 x 100 m"]').click().wait(500)
        cy.selectFromDropdown(
          'basal_dbh_instrument',
          `^${dbhInstrument}$`,
          null,
          null,
          false,
          true,
        )
        cy.nextStep()
      })

      //only really care about testing vouchers, so that's all we'll select (nothing else
      //is required, except location)
      it('.should() - collection DBH observations', () => {
        const vouchers = speciesOfflineAndOnlineToCollect.map(o => o.field_name)
        for (const [index, fieldName] of vouchers.entries()) {
          cy.recordLocation()
          cy.selectFromDropdown('floristics_voucher', fieldName)

          cy.dataCy('done').click()
          
          if (index < (vouchers.length - 1)) {
            cy.dataCy('addEntry').click()
          }
        }
      })
      
      it(`.should() - submit offline collection for ${basalDbhProtocol} and assert collection was queued correctly`, () => {
        cy.nextStep()
        cy.workflowPublishOffline(
          {
            models: ['plot-layout', 'plot-visit', 'basal-area-dbh-measure-survey', 'basal-area-dbh-measure-observation'],
            plotLabel: 'SATFLB0003',
            plotIsNew: true,
            plotVisitName: 'SA 3 test visit',
            visitIsNew: true,
            protocolName: basalDbhProtocol,
            projectName: 'Kitchen Sink TEST Project',
            collectionLabel: `collection_${collectionNumber(dbhInstrument)}`,
            //uses vouchers from collection 1/2 and layout from collection 2
            collectionDependsOn: [
              'collection_1_floristics',
              'collection_2_floristics',
              'collection_2_layout',
            ],
          },
          assertBasalDbhCb,
          null,
          DUMP_STATE ? `collection_${collectionNumber(dbhInstrument)}` : null,
        )
      })
    })
  }
  
})

const coverAndFireProtocolsAndModules = [
  {
    module: coverModule,
    protocol: coverFullProtocol,
    display: 'cover',
  },
  {
    module: fireModule,
    protocol: fireFullProtocol,
    display: 'fire',
  },
]
for (const protocol of coverAndFireProtocolsAndModules) {
  describe(`Collect ${protocol.protocol}`, () => {
    it('.should() - assert plot context was set correctly', () => {
      cy.assertPlotContext('SATFLB0003', 'SA 3 test visit')
    })

    it(`.should() - navigate to ${protocol.protocol}`, () => {
      cy.selectProtocolIsEnabled(protocol.module, protocol.protocol)
      cy.nextStep()
    })

    const transects = ['South 1', 'North 2', 'South 3', 'North 4', 'South 5', 'West 1', 'East 2', 'West 3', 'East 4', 'West 5']
    for (const transect of transects) {
      it(`.should() - collect transect ${transect}`, () => {
        if (
          transect === 'South 1' &&
          protocol.protocol === fireFullProtocol
        ) {
          cy.customSelectDate({
            fieldName: 'fireIgnitionDateCalendar',
            date: {
              year: 'current',
              month: 'current',
              day: 'current',
            },
          }, false)
          cy.dataCy('doneFireIgnitionSurvey').click()
        }

        cy.get('[data-cy="selectingTransect"] > .multiselect__tags > span')
          .should('have.text', transect)

        cy.dataCy('startTransect').click()

        //5 points per transect in dev mode (normally 101)
        for (let i = 0; i < 5; i++) {
          
          cy.completePointInterceptPoint(
            transect,
            i,
            false,
            //specify `isFire`
            protocol.protocol === fireFullProtocol,
            //specify `isOffline` to be true, so that we can force offline vouchers to be
            //collected more often (in addition to normal vouchers)
            true,
          )
        }

        if (protocol.protocol === fireFullProtocol && transect === 'West 5') {
          //last transect go next step (fire char observation)
          cy.nextStep()
        }
      })
    }

    if (protocol.protocol === fireFullProtocol) {
      it('.should() - collect fire char observations', () => {
        cy.collectFireCharObservations()
      })
    }

    it(`.should() - submit offline collection for ${protocol.protocol} and assert collection was queued correctly`, () => {
      const coverModels = ['cover-point-intercept-survey', 'cover-point-intercept-point', 'plot-layout', 'plot-visit']
      const fireModels = ['plot-layout', 'plot-visit', 'fire-survey', 'fire-point-intercept-point', 'fire-char-observation']
      const mightDependOn = [
        'collection_1_floristics',
        'collection_2_floristics',
        'collection_3',
        'collection_5',
        'collection_5_submodule',
      ]
      if (protocol.protocol === fireFullProtocol) {
        mightDependOn.push('collection_cover')
      }
      cy.nextStep()
      cy.workflowPublishOffline(
        {
          models: (() => {
            if (protocol.protocol === coverFullProtocol) {
              return coverModels
            } else if (protocol.protocol === fireFullProtocol) {
              return fireModels
            }
          })(),
          plotLabel: 'SATFLB0003',
          plotVisitName: 'SA 3 test visit',
          protocolName: protocol.protocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: `collection_${protocol.protocol === fireFullProtocol ? 'fire' : 'cover'}`,
          collectionLabelHasSubmodule: protocol.protocol === fireFullProtocol,
          //uses new plot from collection 2
          collectionDependsOn: [
            'collection_2_layout',
          ],
          //MIGHT use any offline/online vouchers
          collectionMightDependsOn: mightDependOn,
        },
        assertCoverCb,
        null,
        DUMP_STATE ? `collection_${protocol.protocol === fireFullProtocol ? 'fire' : 'cover'}` : null,
      )
    })
  })
}

describe('Submit queued offline collections', () => {
  it('.should() - go online', () => {
    cy.window()
      .then(($win) => {
        $win.ephemeralStore.setNetworkOnline(true)
      })
  })

  if (DUMP_STATE) {
    it('.should() - dump state', () => {
      cy.dumpState({
        fileName: 'offline-floristics-and-dependencies',
      })
    })
  }

  const newCollections = [
    //collection 1 (lite vouchers with existing plot)
    {
      plotLabel: 'QDASEQ0001',
      visitFieldName: 'QLD winter survey',
      plotIsNew: false,
      hasNewFaunaPlot: false,
      visitIsNew: false,
      liteVoucherFieldNames: ['offline voucher lite 1', 'offline voucher lite 2', 'offline voucher lite 3'],
      recollectedVoucherFieldNames: collection1VouchersToRecollect,
    },
    //collection 2 (full vouchers with new plot, collected offline)
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      fullVoucherFieldNames: ['offline voucher full 1', 'offline voucher full 2', 'offline voucher full 3'],
    },
    //collection 3 (re-collected and lite vouchers with new plot, collected offline)
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      recollectedVoucherFieldNames: collection3and4VouchersToRecollect,
    },
    //collection 4 (plant tissue vouchering stand-alone)
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      usedFloristicsVouchersFieldNames: ['offline voucher full 1', 'offline voucher lite 2', 'offline voucher full 2', 'offline voucher full 3', 'offline voucher lite 3', 'Voucher Lite Example 2'],
    },
    //collection 5 (plant tissue vouchering as submodule in floristics)
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      //for checking the parent module's (floristics) vouchers (both are the same, but
      //are checked slightly differently)
      usedFloristicsVouchersFieldNames: ['floristics voucher full offline 1', 'floristics voucher full offline 2', 'floristics voucher full offline 3'],
      fullVoucherFieldNames: ['floristics voucher full offline 1', 'floristics voucher full offline 2', 'floristics voucher full offline 3'],
    },
    //collection 6 (recruitment age structure)
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      usedFloristicsVouchers: [
        'offline voucher full 2',
        'Voucher Full Example 3',
        'offline voucher lite 1',
        'Voucher Lite Example 2',
        'offline voucher full 1',
      ],
      usedCounts: [
        {
          species: 'offline voucher full 2',
          seedlingCount: 5,
          saplingCount: 3,
          juvenileCount: 0,
        },
        {
          species: 'Voucher Full Example 3',
          seedlingCount: 1,
          saplingCount: 5,
          juvenileCount: 2,
        },
        {
          species: 'offline voucher lite 1',
          seedlingCount: 7,
          saplingCount: 0,
          juvenileCount: 0,
        },
        {
          species: 'Voucher Lite Example 2',
          seedlingCount: 0,
          saplingCount: 0,
          juvenileCount: 6,
        },
        {
          species: 'offline voucher full 1',
          seedlingCount: 0,
          saplingCount: 2,
          juvenileCount: 9,
        },
      ],
    },
    //collection 7 (recruitment survivorship - visit/revisit from online-collected plot visit)
    {
      plotLabel: 'SATFLB0001',
      visitFieldName: 'SA autumn survey',
      plotIsNew: false,
      hasNewFaunaPlot: false,
      visitIsNew: false,
      survivorshipSurveys: [
        recruitmentSurvivorshipSurveys.collection_7_new,
        recruitmentSurvivorshipSurveys.collection_7_revisit,

      ],
      usedFloristicsVouchersForRecruitmentSurvivorship: speciesOfflineAndOnlineToCollect.map(o => o.field_name),
    },
    //collection 8 (recruitment survivorship - revisit from offline-collected plot visit)
    {
      plotLabel: 'SATFLB0001',
      visitFieldName: 'SA 1 test visit',
      plotIsNew: false,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      survivorshipSurveys: [
        recruitmentSurvivorshipSurveys.collection_8_revisit,
      ],
      usedFloristicsVouchersForRecruitmentSurvivorship: speciesOfflineAndOnlineToCollect.map(o => o.field_name),
    },
    //collection 9 (recruitment survivorship - visit/revisit from same offline-collected plot/visit)
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      survivorshipSurveys: [
        recruitmentSurvivorshipSurveys.collection_9_new,
        recruitmentSurvivorshipSurveys.collection_9_revisit,
      ],
      usedFloristicsVouchersForRecruitmentSurvivorship: speciesOfflineAndOnlineToCollect.map(o => o.field_name),
    },
    //collection 10 (recruitment survivorship - revisit from different offline-collected plot visit)
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit 2',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      survivorshipSurveys: [
        recruitmentSurvivorshipSurveys.collection_10_revisit,
      ],
      usedFloristicsVouchersForRecruitmentSurvivorship: speciesOfflineAndOnlineToCollect.map(o => o.field_name),
    },
    //collection 11/12 (condition standard/enhanced)
    //TODO since we have the same data for both collections, we need to pass the surveys to filter (similar to recruitment survivorshp)
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      usedFloristicsVouchersForCondition: speciesOfflineAndOnlineToCollect.map(o => o.field_name),
      conditionTreeSurveys: [
        conditionTreeSurveys.collection_11,
        conditionTreeSurveys.collection_12,
      ],
      //need to filter each ob ('condition-tree-survey') against the actual survey
      additionalRequest: 'condition-surveys',
    },
    //collection 13 (basal wedge)
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      usedFloristicsVouchersForBasalWedge: speciesOfflineAndOnlineToCollect.map(o => o.field_name),
    },
    // collection 14,15,16 (basal DBH)
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      usedFloristicsVouchersForBasalDbh: speciesOfflineAndOnlineToCollect.map(o => o.field_name),
    },
    //cover collection
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      coverSurvey: coverSurvey,
      //grab the points that will resolve the relation to the intercepts, so that when
      //we validate the intercepts after submission we can filter them to only this
      //that were created for the tests (i.e., exclude init data)
      additionalRequest: 'cover-point-intercept-points',
    },
    //fire collection
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      coverSurvey: coverSurvey,
      coverPoints: coverPoints,
      coverIntercepts: coverIntercepts,
      fireSurvey: fireSurvey,
      additionalRequest: 'fire-point-intercept-points',
    },
  ]
  const cbs = [
    {
      collectionModel: 'plot-layouts',
      cb: (layoutApiResp, collectionsToCheck) => {
        console.log('layoutApiResp: ', layoutApiResp)
        cy.log('layoutApiResp: ', layoutApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c.plotIsNew || c.hasNewFaunaPlot) {
            const plot = layoutApiResp.body.data.find(
              o => o.attributes.plot_selection.data.attributes.plot_label === c.plotLabel
            )
            console.debug('plot: ', JSON.stringify(plot))
            cy.wrap(plot)
              .should('not.be.undefined')
              .and('not.be.null')
            cy.wrap(plot.attributes)
              .should('not.be.undefined')
              .and('not.be.null')
            
            if (c.plotIsNew) {
              cy.wrap(plot.attributes.plot_points)
                .should('not.be.undefined')
                .and('not.be.null')
            }
            
            if (c.hasNewFaunaPlot) {
              cy.wrap(plot.attributes.fauna_plot_point)
                .should('not.be.undefined')
                .and('not.be.null')
            }
          }
        }
      },
    },
    {
      collectionModel: 'plot-visits',
      cb: (visitApiResp, collectionsToCheck) => {
        console.log('visitApiResp: ', visitApiResp)
        cy.log('visitApiResp: ', visitApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c.visitIsNew) {
            const visit = visitApiResp.body.data.find(
              o => o.attributes.visit_field_name === c.visitFieldName
            )
            console.debug('visit: ', JSON.stringify(visit))
            cy.wrap(visit)
              .should('not.be.undefined')
              .and('not.be.null')
            
            const plotLabelForVisit = visit.attributes.plot_layout.data.attributes.plot_selection.data.attributes.plot_label
            cy.wrap(plotLabelForVisit)
              .should('eq', c.plotLabel)
          }
        }
      },
    },
    {
      collectionModel: 'floristics-veg-voucher-lites',
      cb: (liteVoucherApiResp, collectionsToCheck) => {
        console.log('liteVoucherApiResp: ', liteVoucherApiResp)
        cy.log('liteVoucherApiResp: ', liteVoucherApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          for (const fieldName of c.liteVoucherFieldNames || []) {
            const voucher = liteVoucherApiResp.body.data.find(
              o => o.attributes.field_name === fieldName
            )
            console.debug('lite voucher: ', JSON.stringify(voucher))
            cy.wrap(voucher)
              .should('not.be.undefined')
              .and('not.be.null')
          }
        }
      },
    },
    {
      collectionModel: 'floristics-veg-voucher-fulls',
      cb: (fullVoucherApiResp, collectionsToCheck) => {
        console.log('fullVoucherApiResp: ', fullVoucherApiResp)
        cy.log('fullVoucherApiResp: ', fullVoucherApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          for (const fieldName of c.fullVoucherFieldNames || []) {
            const voucher = fullVoucherApiResp.body.data.find(
              o => o.attributes.field_name === fieldName
            )
            console.debug('full voucher: ', JSON.stringify(voucher))
            cy.wrap(voucher)
              .should('not.be.undefined')
              .and('not.be.null')
          }
        }
      },
    },
    {
      collectionModel: 'floristics-veg-virtual-vouchers',
      cb: (recollectedVoucherApiResp, collectionsToCheck) => {
        console.log('recollectedVoucherApiResp: ', recollectedVoucherApiResp)
        cy.log('recollectedVoucherApiResp: ', recollectedVoucherApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          for (const fieldName of c.recollectedVoucherFieldNames || []) {
            const voucher = recollectedVoucherApiResp.body.data.find(
              o =>
                o.attributes.voucher_lite?.data?.attributes?.field_name === fieldName ||
                o.attributes.voucher_full?.data?.attributes?.field_name === fieldName
            )
            console.debug('re-collected voucher: ', JSON.stringify(voucher))
            cy.wrap(voucher)
              .should('not.be.undefined')
              .and('not.be.null')
          }
        }
      },
    },
    {
      collectionModel: 'floristics-veg-genetic-vouchers',
      cb: (ptvApiResp, collectionsToCheck) => {
        console.log('ptvApiResp: ', ptvApiResp)
        cy.log('ptvApiResp: ', ptvApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          for (const fieldName of c.usedFloristicsVouchersFieldNames || []) {
            const floristicsVoucher = ptvApiResp.body.data.find(
              o =>
                o?.attributes?.floristics_voucher?.voucher_lite?.data?.attributes?.field_name === fieldName ||
                o?.attributes?.floristics_voucher?.voucher_full?.data?.attributes?.field_name === fieldName
            )
            console.debug('floristicsVoucher: ', JSON.stringify(floristicsVoucher))
            cy.wrap(floristicsVoucher)
              .should('not.be.undefined')
              .and('not.be.null')
          }
        }
      },
    },
    {
      collectionModel: 'recruitment-growth-stages',
      cb: (growthStageApiResp, collectionsToCheck) => {
        console.log('growthStageApiResp: ', growthStageApiResp)
        cy.log('growthStageApiResp: ', growthStageApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          //check usage of floristics vouchers
          for (const fieldName of c.usedFloristicsVouchers || []) {
            const floristicsVoucher = growthStageApiResp.body.data.find(
              o =>
              o?.attributes?.floristics_voucher?.voucher_lite?.data?.attributes?.field_name === fieldName ||
              o?.attributes?.floristics_voucher?.voucher_full?.data?.attributes?.field_name === fieldName
            )
            console.debug('floristicsVoucher: ', JSON.stringify(floristicsVoucher))
            cy.wrap(floristicsVoucher)
              .should('not.be.undefined')
              .and('not.be.null')
          }
        }
      },
    },
    {
      collectionModel: 'recruitment-sapling-and-seedling-counts',
      cb: (saplingAndSeedlingApiResp, collectionsToCheck) => {
        console.log('saplingAndSeedlingApiResp: ', saplingAndSeedlingApiResp)
        cy.log('saplingAndSeedlingApiResp: ', saplingAndSeedlingApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          for (const countMetaObj of c.usedCounts || []) {
            const count = saplingAndSeedlingApiResp.body.data.find(
              o => {
                //use `String.includes()` as the expected metaObj (when we used a
                //non-voucher, i.e., new species) doesn't contain the full scientific
                //name, but the actual data does
                return o?.attributes.species?.includes(countMetaObj.species) &&
                  o?.attributes.seedling_count === countMetaObj.seedlingCount &&
                  o?.attributes.sapling_count === countMetaObj.saplingCount &&
                  o?.attributes.juvenile_count === countMetaObj.juvenileCount
              }
            )
            console.debug('count: ', count)

            cy.wrap(count)
              .should('not.be.undefined')
              .and('not.be.null')
          }
        }
      },
    },
    {
      collectionModel: 'recruitment-survivorship-surveys',
      cb: (survivorshipSurveyRApiResp, collectionsToCheck) => {
        console.log('survivorshipSurveyRApiResp: ', survivorshipSurveyRApiResp)
        cy.log('survivorshipSurveyRApiResp: ', survivorshipSurveyRApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c.survivorshipSurveys) {
            for (const survey of c.survivorshipSurveys || []) {
              for (const fieldName of c.usedFloristicsVouchersForRecruitmentSurvivorship || []) {
                let floristicsVoucher = null
                for (const respSurvey of survivorshipSurveyRApiResp.body.data) {
                  //we've got a match for the survey in the resp based on stored
                  //surveys, so check the vouchers are all there, which will result in
                  //us checking that these vouchers exist for every survey's visit
                  //and/or revisit
                  if (surveyMetaEqual(survey.survey_metadata, respSurvey?.attributes.survey_metadata)) {
                    for (const voucherComponent of respSurvey?.attributes.floristics_voucher || []) {
                      for (const variant of ['full', 'lite']) {
                        if (
                          voucherComponent?.id &&
                          voucherComponent?.[`voucher_${variant}`]?.data?.attributes?.field_name === fieldName
                        ) {
                          floristicsVoucher = voucherComponent[`voucher_${variant}`]
                        }
                      }
                    }
                  }
                }
                console.debug('floristicsVoucher: ', JSON.stringify(floristicsVoucher))
                cy.wrap(floristicsVoucher)
                  .should('not.be.undefined')
                  .and('not.be.null')
              }
            }
          }
        }
      },
    },
    {
      collectionModel: 'condition-tree-surveys',
      cb: (conditionTreeSurveyApiResp, collectionsToCheck) => {
        console.log('conditionTreeSurveyApiResp:', conditionTreeSurveyApiResp)
        cy.log('conditionTreeSurveyApiResp:', conditionTreeSurveyApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c.additionalRequest && c.conditionTreeSurveys && c.usedFloristicsVouchersForCondition) {
            cy.doRequest(c.additionalRequest, {
              host: 'core',
              args: {
                populate: 'deep',
                'use-default': true,
              },
            })
              .then((conditionSurveys) => {
                console.log('conditionSurveys: ', conditionSurveys)
                for (const fieldName of c.usedFloristicsVouchersForCondition || []) {
                  let floristicsVoucher = null
                  for (const treeSurvey of conditionTreeSurveyApiResp.body.data) {
                    let foundTreeSurveyContainingVoucher = false
                    //this `treeSurvey` that we submit (containing vouchers) is one of
                    //the ones created in this test, so we can check its vouchers
                    if (
                      conditionSurveys.body.data.some(
                        o => surveyMetaEqual(
                          o?.attributes?.survey_metadata,
                          treeSurvey?.attributes.condition_survey?.data?.attributes.survey_metadata
                        )
                      )
                    ) {
                      for (const variant of ['full', 'lite']) {
                        if (treeSurvey?.attributes.tree_record?.species?.[`voucher_${variant}`]?.data?.attributes?.field_name === fieldName) {
                          floristicsVoucher = treeSurvey.attributes.tree_record.species[`voucher_${variant}`].data.attributes.field_name
                          foundTreeSurveyContainingVoucher = true
                          break
                        }
                      }
                    }
                    //want to break out of loop that is iterating over the vouchers
                    //we collected `c.usedFloristicsVouchersForCondition` as once a
                    //given `treeSurvey` finds a match, we need to move onto the next
                    //`treeSurvey` to keep checking vouchers
                    if (foundTreeSurveyContainingVoucher) break
                  }

                  console.debug('floristicsVoucher: ', JSON.stringify(floristicsVoucher))
                  cy.wrap(floristicsVoucher)
                    .should('not.be.undefined')
                    .and('not.be.null')
                }
              })
          }
        }
      },
    },
    {
      collectionModel: 'basal-wedge-observations',
      cb: (basalWedgeObsApiResp, collectionsToCheck) => {
        console.log('basalWedgeObsApiResp:', basalWedgeObsApiResp)
        cy.log('basalWedgeObsApiResp:', basalWedgeObsApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (Array.isArray(c.usedFloristicsVouchersForBasalWedge)) {
            cy.log(`Checking vouchers use field names: ${JSON.stringify(c.usedFloristicsVouchersForBasalWedge)}`)
            for (const ob of basalWedgeObsApiResp.body.data) {
              console.debug('ob:', ob)
              if (Array.isArray(ob?.attributes?.floristics_voucher)) {
                //voucher field not required so we don't have to track if each ob has
                //_a_ voucher, just that each `floristics_voucher` that exists contains
                //a relation to a full or lite voucher
                for (const voucher of ob.attributes.floristics_voucher) {
                  console.debug('voucher:', voucher)
                  cy.log(`Checking specific voucher uses one of the specified field names. voucher: ${JSON.stringify(voucher)}`)

                  cy.wrap(
                    c.usedFloristicsVouchersForBasalWedge.some(
                      fieldName =>
                        fieldName === voucher.voucher_lite?.data?.attributes?.field_name ||
                        fieldName === voucher.voucher_full?.data?.attributes?.field_name
                    )
                  ).should('be.true')
                }
              }
            }
          }
        }
      },
    },
    {
      collectionModel: 'basal-area-dbh-measure-observations',
      cb: (basalDbhObsApiResp, collectionsToCheck) => {
        console.log('basalDbhObsApiResp:', basalDbhObsApiResp)
        cy.log('basalDbhObsApiResp:', basalDbhObsApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          //check usage of floristics vouchers
          for (const fieldName of c.usedFloristicsVouchersForBasalDbh || []) {
            const floristicsVoucher = basalDbhObsApiResp.body.data.find(
              o =>
                o?.attributes?.floristics_voucher?.voucher_lite?.data?.attributes?.field_name === fieldName ||
                o?.attributes?.floristics_voucher?.voucher_full?.data?.attributes?.field_name === fieldName
            )
            console.debug('floristicsVoucher: ', JSON.stringify(floristicsVoucher))
            cy.wrap(floristicsVoucher)
              .should('not.be.undefined')
              .and('not.be.null')
          }
        }
      },
    },
    {
      collectionModel: 'cover-point-intercept-points',
      cb: (interceptPointsApiResp, collectionsToCheck) => {
        console.log('interceptPointsApiResp: ', interceptPointsApiResp)
        cy.log('interceptPointsApiResp: ', interceptPointsApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c.coverSurvey) {
            //we're going to get all points from `interceptPointsApiResp`, so we need to
            //filter those to only the ones we submit for this test
            const points = interceptPointsApiResp.body.data.filter(
              o => surveyMetaEqual(o?.attributes?.cover_point_intercept_survey?.data?.attributes?.survey_metadata, c.coverSurvey.survey_metadata)
            )
            console.debug('points: ', points)

            cy.wrap(Array.isArray(points))
              .should('be.true')
            
            cy.wrap(points.length == 50)
              .should('be.true')

            for (const point of points) {
              cy.log(`Checking point on transect ${point?.attributes?.cover_transect_start_point?.data?.attributes?.label} and point number ${point?.attributes?.point_number}`)
              
              cy.wrap(Array.isArray(point?.attributes?.species_intercepts?.data))
                .should('be.true')
              
              if (point?.attributes?.species_intercepts?.data?.length > 0) {
                for (const intercept of point.attributes.species_intercepts.data) {
                  for (const variant of ['full', 'lite']) {
                    if (intercept?.attributes?.[`floristics_voucher_${variant}`]?.data) {
                      cy.wrap(intercept.attributes[`floristics_voucher_${variant}`].data)
                        .should('not.be.undefined')
                        .and('not.be.null')
                    }
                  }
                }
              }
            }
          }
        }
      },
    },
    {
      collectionModel: 'cover-point-intercept-species-intercepts',
      cb: (speciesInterceptApiResp, collectionsToCheck) => {
        console.log('speciesInterceptApiResp: ', speciesInterceptApiResp)
        cy.log('speciesInterceptApiResp: ', speciesInterceptApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c.additionalRequest && c.coverSurvey) {
            cy.doRequest(c.additionalRequest, {
              host: 'core',
              args: {
                populate: 'deep',
                'use-default': true,
              },
            })
              .then((allPoints) => {
                console.log('allPoints:', allPoints)
                cy.log('allPoints:', allPoints)
                coverIntercepts = speciesInterceptApiResp
                coverPoints = allPoints
                if (c.additionalRequest === 'cover-point-intercept-points') {
                  //we're going to get all intercepts from `speciesInterceptApiResp`, so
                  //we need to filter those to only the ones we submit for this test
                  const intercepts = speciesInterceptApiResp.body.data.filter(
                    o => {
                      return allPoints.body.data.some(
                        p => {
                          if (
                            p?.attributes?.species_intercepts?.data &&
                            Array.isArray(p.attributes.species_intercepts.data)
                          ) {
                            const surveyIdsMatch = surveyMetaEqual(
                              p?.attributes?.cover_point_intercept_survey?.data?.attributes?.survey_metadata,
                              c.coverSurvey.survey_metadata,
                            )
                            const interceptIsMatch = p?.attributes?.species_intercepts?.data?.some(
                              q => q.id === o.id
                            )
                            return surveyIdsMatch && interceptIsMatch
                          }
                          return false
                        }
                      )
                    }
                  )
                  console.debug('intercepts: ', intercepts)

                  cy.wrap(Array.isArray(intercepts))
                    .should('be.true')
                  
                  //check no intercepts have undefined vouchers (need relation to full
                  //or lite, but not both, so is XOR operation)
                  for (const intercept of intercepts) {
                    cy.log(`Checking intercept with ID=${intercept.id}`)
                    cy.log(JSON.stringify(intercept))
                    cy.wrap(
                      xorCompare(
                        intercept?.attributes?.floristics_voucher_full?.data?.attributes,
                        intercept?.attributes?.floristics_voucher_lite?.data?.attributes,
                      )
                    ).should('be.true')
                  }
                }
              })
          }
        }
      },
    },
    {
      collectionModel: 'fire-point-intercept-points',
      cb: (fireInterceptPointsApiResp, collectionsToCheck) => {
        console.log('fireInterceptPointsApiResp: ', fireInterceptPointsApiResp)
        cy.log('fireInterceptPointsApiResp: ', fireInterceptPointsApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c.fireSurvey) {
            //we're going to get all points from `fireInterceptPointsApiResp`, so we need to
            //filter those to only the ones we submit for this test
            const points = fireInterceptPointsApiResp.body.data.filter(
              o => surveyMetaEqual(o?.attributes?.fire_survey?.data?.attributes?.survey_metadata, c.fireSurvey.survey_metadata)
            )
            console.debug('fire points: ', points)

            cy.wrap(Array.isArray(points))
              .should('be.true')
            
            cy.wrap(points.length == 50)
              .should('be.true')

            for (const point of points) {
              cy.log(`Checking point on transect ${point?.attributes.transect_start_point?.data?.attributes.label} and point number ${point?.attributes?.point_number}`)

              cy.wrap(Array.isArray(point?.attributes?.fire_species_intercepts?.data))
                .should('be.true')
              
              if (point?.attributes?.fire_species_intercepts?.data.length > 0) {
                for (const intercept of point.attributes.fire_species_intercepts.data) {
                  cy.wrap(intercept.attributes)
                    .should('have.keys', 'fire_growth_form', 'fire_impact', 'plant_unidentifiable', 'resprouting_type', 'createdAt', 'updatedAt')
                }
              }
            }
          }
        }
      },
    },
    {
      collectionModel: 'fire-species-intercepts',
      cb: (fireSpeciesInterceptApiResp, collectionsToCheck) => {
        console.log('fireSpeciesInterceptApiResp: ', fireSpeciesInterceptApiResp)
        cy.log('fireSpeciesInterceptApiResp: ', fireSpeciesInterceptApiResp)
        console.log('coverPoints: ', coverPoints)
        cy.log('coverPoints: ', coverPoints)
        console.log('coverIntercepts: ', coverIntercepts)
        cy.log('coverIntercepts: ', coverIntercepts)
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c.additionalRequest && c.fireSurvey && c.coverSurvey) {
            cy.doRequest(c.additionalRequest, {
              host: 'core',
              args: {
                populate: 'deep',
                'use-default': true,
              },
            })
              .then((allFirePoints) => {
                if (c.additionalRequest === 'fire-point-intercept-points') {
                  //we're going to get all intercepts from `fireSpeciesInterceptApiResp`, so
                  //we need to filter those to only the ones we submit for this test
                  const intercepts = fireSpeciesInterceptApiResp.body.data.filter(
                    o => {
                      return allFirePoints.body.data.some(
                        p => {
                          if (
                            p?.attributes?.fire_species_intercepts?.data &&
                            Array.isArray(p.attributes.fire_species_intercepts.data)
                          ) {
                            const surveyIdsMatch = surveyMetaEqual(
                              p?.attributes?.fire_survey?.data?.attributes?.survey_metadata,
                              c.fireSurvey.survey_metadata
                            )
                            const interceptIsMatch = p?.attributes?.fire_species_intercepts?.data?.some(
                              q => q.id === o.id
                            )
                            return surveyIdsMatch && interceptIsMatch
                          }
                          return false
                        }
                      )
                    }
                  )
                  console.debug('fire intercepts: ', intercepts)

                  cy.wrap(Array.isArray(intercepts))
                    .should('be.true')
                  
                  //check no intercepts have undefined vouchers (need relation to full
                  //or lite, but not both, so is XOR operation)
                  for (const intercept of intercepts) {
                    cy.log(`Checking intercept with ID=${intercept.id}`)
                    cy.log(JSON.stringify(intercept))
                    cy.wrap(intercept?.attributes)
                      .should('have.keys', 'fire_growth_form', 'fire_impact', 'plant_unidentifiable', 'resprouting_type', 'createdAt', 'updatedAt')
                  }
                }
              })
          }
        }
      },
    },
  ]
  pubOffline({
    collectionsToCheck: newCollections,
    collectionIterCbs: cbs,
  })
})

//new (lite) vouchers for existing plot
const assertNewVoucherExistingPlotCb = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
  apiModelsStore,
}) => {
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
    .should('eq', 1)

  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  const resolvedPlotId = apiModelsStore.models['plot-layout'].find(
    o => o.plot_selection.plot_label === shouldHaveData.plotLabel
  )?.id
  const resolvedVisitId = apiModelsStore.models['plot-visit'].reverse().find(
    o => o.visit_field_name.includes(shouldHaveData.plotVisitName)
  )?.id
  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch (model) {
      case 'floristics-veg-survey-lite':
        //sufficient to just check the survey exists
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        break
      case 'floristics-veg-voucher-lite':
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        cy.wrap(
          isEqual(
            //grab the first voucher and check
            Object.keys(latestQueuedCollection[model][0]).sort(),
            shouldHaveData.newVoucherKeys.sort(),
          )
        ).should('be.true')
        break
      case 'floristics-veg-virtual-voucher':
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        cy.wrap(
          //two `isEqual`, as there can be a relation to either a full or lite voucher
          isEqual(
            //grab the first voucher and check
            Object.keys(latestQueuedCollection[model][0]).sort(),
            shouldHaveData.existingVoucherKeys.concat('voucher_full').sort(),
          ) ||
          isEqual(
            //grab the first voucher and check
            Object.keys(latestQueuedCollection[model][0]).sort(),
            shouldHaveData.existingVoucherKeys.concat('voucher_lite').sort(),
          )
        ).should('be.true')
        break
      case 'plot-fauna-layout':
        //this model is included but is redundant - technical debt see #353
        break
      case 'plot-layout':
        cy.wrap(latestQueuedCollection[model].id)
          .should('eq', resolvedPlotId)
        break
      case 'plot-visit':
        cy.wrap(latestQueuedCollection[model].id)
          .should('eq', resolvedVisitId)
        //visit also has relation to layout
        cy.wrap(latestQueuedCollection[model].plot_layout)
          .should('eq', resolvedPlotId)
        break
    }
  }
}

//new (full/lite) vouchers, re-collected, PTVs (stand-alone and as Floristics submodule): for new plot (collected offline)
const assertNewVoucherNewPlotCb = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
}) => {
  const isPtvSubmoduleCollection = typeof shouldHaveData.models === 'object' &&
    //arrays are also objects so need to check this too
    !Array.isArray(shouldHaveData.models) &&
    //PTV collected as submodule in floristics
    Object.keys(shouldHaveData.models).includes('plantTissue') &&
    Object.keys(shouldHaveData.models).includes('floristics')
  cy.log(`Collection is ${
    isPtvSubmoduleCollection
      ? 'collected as submodule'
      : 'standalone collection'
  }`)

  if (isPtvSubmoduleCollection) {
    //submodules queue two collections
    cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
      .should('eq', 2)
  } else {
    cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
      .should('eq', 1)
  }

  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  if (shouldHaveData.collectionLabelHasSubmodule) {
    cy.log(`Setting submodule's queued collection identifier for collection label '${shouldHaveData.collectionLabel}_submodule' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
    //this is the queued collection identifier for the submodule
    collectionLabelQueueUuids[`${shouldHaveData.collectionLabel}_submodule`] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

    cy.log(`Setting parent module's queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-2].queuedCollectionIdentifier}`)
    //this is the parent module of the submodule
    collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-2].queuedCollectionIdentifier
  } else {
    cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
    //this is the collection, with no additional info for submodule (as there is none)
    collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier
  }

  let parentModuleQueuedCollection = null

  const collectionModels = Object.keys(latestQueuedCollection)
  if (isPtvSubmoduleCollection) {
    parentModuleQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-2].collection
    console.log('parentModuleQueuedCollection: ', parentModuleQueuedCollection)

    cy.wrap(
      shouldHaveData.models.plantTissue.every(o => collectionModels.includes(o))
    ).should('be.true')

    const parentCollectionModels = Object.keys(parentModuleQueuedCollection)
    cy.wrap(
      shouldHaveData.models.floristics.every(o => parentCollectionModels.includes(o))
    ).should('be.true')
  } else {
    cy.wrap(
      shouldHaveData.models.every(o => collectionModels.includes(o))
    ).should('be.true')
  }

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    //take the deps (provided as labels) and resolve to the actual
    //`queuedCollectionIdentifier` from the queue that was assigned during that
    //respective collection's callback and check this collection's
    //`dependsOnQueuedCollections` matches
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      collectionLabelDep => collectionLabelQueueUuids[collectionLabelDep]
    )
    
    if (isPtvSubmoduleCollection && parentModuleQueuedCollection) {
      //submodule means the check below is being done on the submodule's queue entry, but
      //we also need to check the parent module while adding parent module to the
      //submodule's deps (`shouldHaveData.collectionDependsOn`), which is only taking
      //into account the dependent modules; but the queue adds the parent module to the
      //submodule's deps (`dependsOnQueuedCollections`) in addition to parent module's
      cy.log(`Checking queued collection tracked collections dependencies. Comparing expected / actual: ${JSON.stringify(shouldHaveDepsUuids.sort())} / ${JSON.stringify(publicationQueueAfter[publicationQueueAfter.length-2].dependsOnQueuedCollections.sort())}`)
      cy.wrap(isEqual(
        shouldHaveDepsUuids.sort(),
        publicationQueueAfter[publicationQueueAfter.length-2].dependsOnQueuedCollections.sort()
      )).should('be.true')

      shouldHaveDepsUuids.push(publicationQueueAfter[publicationQueueAfter.length-2].queuedCollectionIdentifier)
    }

    //if submodule, this check is on the submodule and the parent is taken care of in
    //the `if` block above
    cy.log(`Checking queued collection tracked collections dependencies. Comparing expected / actual: ${JSON.stringify(shouldHaveDepsUuids.sort())} / ${JSON.stringify(publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.sort())}`)
    cy.wrap(isEqual(
      shouldHaveDepsUuids.sort(),
      publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.sort()
    )).should('be.true')
  }

  let queuedCollectionToCheck = []
  if (isPtvSubmoduleCollection) {
    queuedCollectionToCheck.push(
      //floristics
      {
        collection: parentModuleQueuedCollection,
        models: shouldHaveData.models.floristics,
        newVoucherKeys: shouldHaveData.newVoucherKeys,
        plotVisitName: shouldHaveData.plotVisitName,
      },
      //PTV
      {
        collection: latestQueuedCollection,
        models: shouldHaveData.models.plantTissue,
        plotVisitName: shouldHaveData.plotVisitName,
      },
    )
  } else {
    queuedCollectionToCheck.push({
      collection: latestQueuedCollection,
      models: shouldHaveData.models,
      newVoucherKeys: shouldHaveData.newVoucherKeys,
      existingVoucherKeys: shouldHaveData.existingVoucherKeys,
      plotVisitName: shouldHaveData.plotVisitName,
    })
  }
  console.log('queuedCollectionToCheck: ', queuedCollectionToCheck)

  //if we're just doing floristics, this loop will iterate once; but if we're doing PTV
  //as submodule, it will have 2 iteration, one for the parent module and one for the
  //submodule. This simplified the code and reduces duplication
  for (const collectionToCheck of queuedCollectionToCheck) {
    console.log('collectionToCheck: ', collectionToCheck)
    for (const model of collectionToCheck.models) {
      console.log(`${model}:`, collectionToCheck.collection[model])
      switch (model){
        case 'floristics-veg-survey-full':
        case 'floristics-veg-survey-lite':
        case 'floristics-veg-genetic-voucher-survey':
          //sufficient to just check the survey exists
          cy.wrap(collectionToCheck.collection[model])
            .should('not.be.undefined')
            .and('not.be.null')
          break
        case 'floristics-veg-voucher-full':
        case 'floristics-veg-voucher-lite':
          cy.wrap(collectionToCheck.collection[model])
            .should('not.be.undefined')
            .and('not.be.null')
          cy.wrap(
            isEqual(
              //grab the first voucher and check
              Object.keys(collectionToCheck.collection[model][0]).sort(),
              collectionToCheck.newVoucherKeys.sort(),
            )
          ).should('be.true')
          break
        case 'floristics-veg-virtual-voucher':
          cy.wrap(collectionToCheck.collection[model])
            .should('not.be.undefined')
            .and('not.be.null')
          cy.wrap(
            //two `isEqual`, as there can be a relation to either a full or lite voucher
            isEqual(
              //grab the first voucher and check
              Object.keys(collectionToCheck.collection[model][0]).sort(),
              collectionToCheck.existingVoucherKeys.concat('voucher_full').sort(),
            ) ||
            isEqual(
              //grab the first voucher and check
              Object.keys(collectionToCheck.collection[model][0]).sort(),
              collectionToCheck.existingVoucherKeys.concat('voucher_lite').sort(),
            )
          ).should('be.true')
          break
        case 'floristics-veg-genetic-voucher':
          const baseKeys = ['has_replicate', 'replicate', 'date_time']
          for (const ptv of collectionToCheck.collection[model]) {
            console.log('ptv: ', ptv)
            //make a new copy so we can mutate
            const keysToCheck = [...baseKeys]
            //offline submodule doesn't have this so only check if necessary
            if (!isPtvSubmoduleCollection) keysToCheck.push('floristics_voucher')
  
            for (const voucherVariant of ['full', 'lite']) {
              if (ptv[`floristics_voucher_${voucherVariant}`]) {
                keysToCheck.push(`floristics_voucher_${voucherVariant}`)
  
                //goes inside current `if` block so that it only pushes for the actual
                //voucher key
                if (ptv.has_replicate === true) {
                  keysToCheck.push('min_distance_between_replicates')
    
                  cy.wrap(
                    Array.isArray(ptv.replicate)
                  ).should('be.true')
    
                  cy.wrap(
                    ptv.replicate.length >= 5
                  ).should('be.true')
                }
                cy.wrap(
                  isEqual(Object.keys(ptv).sort(), keysToCheck.sort())
                ).should('be.true')
              }
              
            }
          }
          break
        case 'plot-fauna-layout':
          //this model is included but is redundant - technical debt see #353
          break
        case 'plot-layout':
          cy.wrap(collectionToCheck.collection[model])
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(collectionToCheck.collection[model].temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
          break
        case 'plot-visit':
          if (collectionToCheck.collection[model].visit_field_name) {
            //plot description has this model but not this visit_field_name (just temp ID)
            cy.wrap(collectionToCheck.collection[model].visit_field_name)
              .should('eq', collectionToCheck.plotVisitName)
            
            //visit also has relation to layout
            cy.wrap(collectionToCheck.collection[model].plot_layout)
              .should('have.any.key', 'temp_offline_id')
            cy.wrap(collectionToCheck.collection[model].plot_layout.temp_offline_id)
              .should('not.be.undefined')
              .and('not.be.null')
              //check the temp ID stored in visit (as relation to layout) is the same as the
              //temp ID in the layout
              .and('eq', collectionToCheck['plot-layout'].temp_offline_id)
          }
  
          cy.wrap(collectionToCheck.collection[model])
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(collectionToCheck.collection[model].temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
          break
      }
    }
  }
}

const assertRecruitmentCb = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
}) => {
  console.log('assertRecruitmentCb collectionLabelQueueUuids:', collectionLabelQueueUuids)
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
    .should('eq', 1)

  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      collectionLabelDep => collectionLabelQueueUuids[collectionLabelDep]
    )

    cy.log(`Checking queued collection tracked collections dependencies. Comparing expected / actual: ${JSON.stringify(shouldHaveDepsUuids.sort())} / ${JSON.stringify(publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.sort())}`)
    cy.wrap(isEqual(
      shouldHaveDepsUuids.sort(),
      publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.sort()
    )).should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch(model) {
      case 'recruitment-field-survey':
        //sufficient to just check the survey exists
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        break
      case 'recruitment-growth-stage':
        for (const gs of latestQueuedCollection[model]) {
          cy.log('gs: ', gs)
          const keysToCheck = ['growth_and_life_stage', 'floristics_voucher', 'date_time']
          let voucherVariant = null
          if (gs?.floristics_voucher?.voucher_full) {
            voucherVariant = 'full'
            cy.wrap(
              //existing (online) voucher
              Number.isInteger(gs.floristics_voucher.voucher_full) ||
              //voucher created offline
              (
                typeof gs.floristics_voucher.voucher_full === 'object' &&
                Object.keys(gs.floristics_voucher.voucher_full).includes('temp_offline_id')
              )
            ).should('be.true')
          } else if (gs?.floristics_voucher?.voucher_lite) {
            voucherVariant = 'lite'
            cy.wrap(
              //existing (online) voucher
              Number.isInteger(gs.floristics_voucher.voucher_lite) ||
              //voucher created offline
              (
                typeof gs.floristics_voucher.voucher_lite === 'object' &&
                Object.keys(gs.floristics_voucher.voucher_lite).includes('temp_offline_id')
              )
            ).should('be.true')
          }
          cy.log(`Comparing actual keys vs keysToCheck:\n${JSON.stringify(Object.keys(gs).sort())}\n${JSON.stringify(keysToCheck.sort())}`)
          cy.wrap(
            isEqual(Object.keys(gs).sort(), keysToCheck.sort())
          ).should('be.true')

          cy.log(`Checking nested key 'floristics_voucher.voucher_${voucherVariant}': ${JSON.stringify(gs.floristics_voucher)}`)
          const keys = Object.keys(gs.floristics_voucher)
          cy.wrap(
            keys.includes(`voucher_${voucherVariant}`) &&
            //length of 1 as we only have 1 relation to either full OR lite
            keys.length === 1
          ).should('be.true')

          cy.wrap(Array.isArray(gs.growth_and_life_stage))
            .should('be.true')
          
          cy.wrap(gs.growth_and_life_stage.length > 0)
            .should('be.true')
        }
        break
      case 'recruitment-sapling-and-seedling-count':
        const keysToCheck = ['sapling_count', 'seedling_count', 'juvenile_count', 'species']
        for (const ssc of latestQueuedCollection[model]) {
          cy.log('ssc: ', ssc)
          cy.log(`Comparing actual keys vs keysToCheck:\n${JSON.stringify(Object.keys(ssc).sort())}\n${JSON.stringify(keysToCheck.sort())}`)
          cy.wrap(
            isEqual(Object.keys(ssc).sort(), keysToCheck.sort())
          ).should('be.true')
        }
        break
      case 'plot-layout':
      case 'plot-visit':
        cy.wrap(latestQueuedCollection[model])
          .should('have.any.key', 'temp_offline_id')
        cy.wrap(latestQueuedCollection[model].temp_offline_id)
          .should('not.be.undefined')
          .and('not.be.null')
        break
    }
  }
}

const assertRecruitmentSurvivorshipCb = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
  apiModelsStore,
}) => {
  cy.log(`shouldHaveData: ${JSON.stringify(shouldHaveData, null, 2)}`)
  console.log('assertRecruitmentSurvivorshipCb collectionLabelQueueUuids:', collectionLabelQueueUuids)
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
    .should('eq', 1)

  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      collectionLabelDep => collectionLabelQueueUuids[collectionLabelDep]
    )

    cy.log(`Checking queued collection tracked collections dependencies. Comparing expected / actual: ${JSON.stringify(shouldHaveDepsUuids.sort())} / ${JSON.stringify(publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.sort())}`)
    cy.wrap(isEqual(
      shouldHaveDepsUuids.sort(),
      publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.sort()
    )).should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  const resolvedPlotId = apiModelsStore.models['plot-layout'].find(
    o => o.plot_selection.plot_label === shouldHaveData.plotLabel
  )?.id
  const resolvedVisitId = apiModelsStore.models['plot-visit'].reverse().find(
    o => o.visit_field_name.includes(shouldHaveData.plotVisitName)
  )?.id
  cy.log('resolvedPlotId: ', resolvedPlotId)
  cy.log('resolvedVisitId: ', resolvedVisitId)
  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch(model) {
      case 'recruitment-survivorship-survey':
        cy.log(`Tracking Recruitment Survivorship survey for collection label '${shouldHaveData.collectionLabel}'`)
        recruitmentSurvivorshipSurveys[shouldHaveData.collectionLabel] = latestQueuedCollection[model]
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        
        cy.wrap(latestQueuedCollection[model].floristics_voucher)
          .should('not.be.undefined')
          .and('not.be.null')
        cy.wrap(Array.isArray(latestQueuedCollection[model].floristics_voucher))
          .should('be.true')
        
        for (const voucher of latestQueuedCollection[model].floristics_voucher) {
          cy.log(`Checking voucher: ${JSON.stringify(voucher)}`)
          cy.wrap(voucher)
            .should('have.any.keys', 'voucher_full', 'voucher_lite')
          
          //need to check for object, as might be number (relation to online voucher)
          if (voucher.voucher_full && typeof voucher.voucher_full === 'object') {
            cy.wrap(voucher.voucher_full)
              .should('have.key', 'temp_offline_id')
          } else if (voucher.voucher_lite && typeof voucher.voucher_lite === 'object') {
            cy.wrap(voucher.voucher_lite)
              .should('have.key', 'temp_offline_id')
          }
        }
        break
      case 'recruitment-survivorship-observation':
        for (const ob of latestQueuedCollection[model]) {
          cy.log(`Checking observation (species): ${JSON.stringify(ob.species)}`)
          cy.log(`Comparing with: ${JSON.stringify(speciesOfflineAndOnlineToCollect)}`)
          cy.wrap(
            speciesOfflineAndOnlineToCollect
              .map(o => o.field_name)
              .some(o => o === ob.species)
          ).should('be.true')
        }
        break
      case 'plot-layout':
      case 'plot-visit':
        if (
          (
            model === 'plot-layout' &&
            shouldHaveData.plotIsNew
          ) ||
          (
            model === 'plot-visit' &&
            shouldHaveData.visitIsNew
          )
        ) {
          if (latestQueuedCollection[model]?.id?.temp_offline_id) {
            //we've reselected a plot that was collected offline
            cy.wrap(latestQueuedCollection[model].id)
              .should('have.any.key', 'temp_offline_id')
            cy.wrap(latestQueuedCollection[model].id.temp_offline_id)
              .should('not.be.undefined')
              .and('not.be.null')
          } else {
            cy.wrap(latestQueuedCollection[model])
              .should('have.any.key', 'temp_offline_id')
            cy.wrap(latestQueuedCollection[model].temp_offline_id)
              .should('not.be.undefined')
              .and('not.be.null')
          }
        } else {
          let idToCheck = null
          if (model === 'plot-layout') {
            idToCheck = resolvedPlotId
          } else if (model === 'plot-visit') {
            idToCheck = resolvedVisitId
          }
          cy.wrap(latestQueuedCollection[model].id)
            .should('eq', idToCheck)
        }
        break
    }
  }
}

const assertConditionCb = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
}) => {
  cy.log(`shouldHaveData: ${JSON.stringify(shouldHaveData, null, 2)}`)
  console.log('assertConditionCb collectionLabelQueueUuids:', collectionLabelQueueUuids)
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
    .should('eq', 1)

  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      collectionLabelDep => collectionLabelQueueUuids[collectionLabelDep]
    )

    cy.log(`Checking queued collection tracked collections dependencies. Comparing expected / actual: ${JSON.stringify(shouldHaveDepsUuids.sort())} / ${JSON.stringify(publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.sort())}`)
    cy.wrap(isEqual(
      shouldHaveDepsUuids.sort(),
      publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.sort()
    )).should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch(model) {
      case 'condition-survey':
        cy.log(`Tracking Condition survey for collection label '${shouldHaveData.collectionLabel}'`)
        conditionTreeSurveys[shouldHaveData.collectionLabel] = latestQueuedCollection[model]
        
        //sufficient to just check the survey exists
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        break
      case 'condition-tree-survey':
        for (const ob of latestQueuedCollection[model]) {
          cy.log(`Checking species '${JSON.stringify(ob.tree_record.species)}' has link to voucher`)
          for (const variant of ['full', 'lite']) {
            if (ob.tree_record.species[`voucher_${variant}`]) {
              if (ob.tree_record.species[`voucher_${variant}`].temp_offline_id) {
                cy.wrap(ob.tree_record.species[`voucher_${variant}`].temp_offline_id)
                  .should('not.be.null')
                  .and('not.be.undefined')
              } else {
                cy.wrap(ob.tree_record.species[`voucher_${variant}`])
                  .should('not.be.null')
                  .and('not.be.undefined')
              }
            }
          }
        }
        break
      case 'plot-layout':
      case 'plot-visit':
        if (latestQueuedCollection[model]?.id?.temp_offline_id) {
          //we've reselected a plot that was collected offline
          cy.wrap(latestQueuedCollection[model].id)
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(latestQueuedCollection[model].id.temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
        } else {
          cy.wrap(latestQueuedCollection[model])
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(latestQueuedCollection[model].temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
        }
        break
    }
  }
}

const assertBasalWedgeCb = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
}) => {
  cy.log(`shouldHaveData: ${JSON.stringify(shouldHaveData, null, 2)}`)
  console.log('assertBasalWedgeCb collectionLabelQueueUuids:', collectionLabelQueueUuids)
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
    .should('eq', 1)

  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      collectionLabelDep => collectionLabelQueueUuids[collectionLabelDep]
    )

    cy.log(`Checking queued collection tracked collections dependencies. Comparing expected / actual: ${JSON.stringify(shouldHaveDepsUuids.sort())} / ${JSON.stringify(publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.sort())}`)
    cy.wrap(isEqual(
      shouldHaveDepsUuids.sort(),
      publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.sort()
    )).should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch (model) {
      case 'basal-wedge-survey':
        //sufficient to just check the survey exists
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        break
      case 'basal-wedge-observation':
        for (const ob of latestQueuedCollection[model]) {
          if (Array.isArray(ob.floristics_voucher)) {
            //field isn't required so need to check if exists first
            cy.log(`Checking species '${JSON.stringify(ob.floristics_voucher)}' has link(s) to voucher`)
            for (const voucher of ob.floristics_voucher) {
              cy.log(`Checking voucher: ${JSON.stringify(voucher)}`)
              cy.wrap(voucher)
                .should('have.any.keys', 'voucher_full', 'voucher_lite')
              
              //need to check for object, as might be number (relation to online voucher)
              if (voucher.voucher_full && typeof voucher.voucher_full === 'object') {
                cy.wrap(voucher.voucher_full)
                  .should('have.key', 'temp_offline_id')
              } else if (voucher.voucher_lite && typeof voucher.voucher_lite === 'object') {
                cy.wrap(voucher.voucher_lite)
                  .should('have.key', 'temp_offline_id')
              }
            }
          }
        }
        break
      case 'plot-layout':
      case 'plot-visit':
        if (latestQueuedCollection[model]?.id?.temp_offline_id) {
          //we've reselected a plot that was collected offline
          cy.wrap(latestQueuedCollection[model].id)
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(latestQueuedCollection[model].id.temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
        } else {
          cy.wrap(latestQueuedCollection[model])
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(latestQueuedCollection[model].temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
        }
        break
    }
  }
}

const assertBasalDbhCb = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
}) => {
  cy.log(`shouldHaveData: ${JSON.stringify(shouldHaveData, null, 2)}`)
  console.log('assertBasalWedgeCb collectionLabelQueueUuids:', collectionLabelQueueUuids)
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
    .should('eq', 1)

  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      collectionLabelDep => collectionLabelQueueUuids[collectionLabelDep]
    )

    cy.log(`Checking queued collection tracked collections dependencies. Comparing expected / actual: ${JSON.stringify(shouldHaveDepsUuids.sort())} / ${JSON.stringify(publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.sort())}`)
    cy.wrap(isEqual(
      shouldHaveDepsUuids.sort(),
      publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.sort()
    )).should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch (model) {
      case 'basal-area-dbh-measure-survey':
        //sufficient to just check the survey exists
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        break
      case 'basal-area-dbh-measure-observation':
        for (const ob of latestQueuedCollection[model]) {
          cy.log(`Checking species '${JSON.stringify(ob.floristics_voucher)}' has link to voucher`)
          for (const variant of ['full', 'lite']) {
            if (ob.floristics_voucher[`voucher_${variant}`]) {
              if (ob.floristics_voucher[`voucher_${variant}`].temp_offline_id) {
                cy.wrap(ob.floristics_voucher[`voucher_${variant}`].temp_offline_id)
                  .should('not.be.null')
                  .and('not.be.undefined')
              } else {
                cy.wrap(ob.floristics_voucher[`voucher_${variant}`])
                  .should('not.be.null')
                  .and('not.be.undefined')
              }
            }
          }
          
        }
        break
      case 'plot-layout':
      case 'plot-visit':
        if (latestQueuedCollection[model]?.id?.temp_offline_id) {
          //we've reselected a plot that was collected offline
          cy.wrap(latestQueuedCollection[model].id)
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(latestQueuedCollection[model].id.temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
        } else {
          cy.wrap(latestQueuedCollection[model])
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(latestQueuedCollection[model].temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
        }
        break
    }
  }
}

const assertCoverCb = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
}) => {
  if (shouldHaveData.models.includes('fire-survey')) {
    cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
      .should('eq', 2)
  } else {
    cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
      .should('eq', 1)
  }
  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  if (shouldHaveData.collectionLabelHasSubmodule) {
    cy.log(`Setting submodule's queued collection identifier for collection label '${shouldHaveData.collectionLabel}_submodule' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
    //this is the queued collection identifier for the submodule
    collectionLabelQueueUuids[`${shouldHaveData.collectionLabel}_submodule`] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

    cy.log(`Setting parent module's queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-2].queuedCollectionIdentifier}`)
    //this is the parent module of the submodule
    collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-2].queuedCollectionIdentifier
  } else {
    cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
    //this is the collection, with no additional info for submodule (as there is none)
    collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier
  }

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      collectionLabelDep => collectionLabelQueueUuids[collectionLabelDep]
    )

    cy.log(`Checking queued collection tracked collections dependencies that we should have. Comparing superset (actual) / expected: ${JSON.stringify(publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections)} / ${JSON.stringify(shouldHaveDepsUuids)}`)
    cy.wrap(
      shouldHaveDepsUuids.every(
        dep => publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.includes(dep)
      )
    ).should('be.true')
  }

  if (
    shouldHaveData.collectionMightDependsOn &&
    Array.isArray(shouldHaveData.collectionMightDependsOn) &&
    shouldHaveData.collectionMightDependsOn.length > 0
  ) {
    //`union` to remove potential duplicates between `collectionMightDependsOn` and `collectionDependsOn`
    const shouldMaybeHaveDepsUuids = union(shouldHaveData.collectionMightDependsOn
      //the deps in queue will also store the stuff we do depend on (not just might), so
      //also need to add it to this list (which is also checked above)
      .concat(shouldHaveData.collectionDependsOn)
      .map(
        collectionLabelDep => collectionLabelQueueUuids[collectionLabelDep]
      )
    )
    if (shouldHaveData.collectionLabelHasSubmodule) {
      //fire creates dep to cover (parent module), so need to also add it.
      //use `collection_fire` as this is the parent module (i.e., cover); the submodule
      //is `collection_fire_submodule`, which is the actual Fire Severity collection
      const coverUuid = collectionLabelQueueUuids['collection_fire']
      if (!shouldMaybeHaveDepsUuids.includes(coverUuid)) {
        cy.log(`Is fire, so adding cover (parent module) to list of UUIDs we MIGHT have: ${coverUuid}`)
        shouldMaybeHaveDepsUuids.push(coverUuid)
      }
    }

    cy.log(`Checking queued collection tracked collections dependencies that we MIGHT have. Comparing superset / actual: ${JSON.stringify(shouldMaybeHaveDepsUuids)} / ${JSON.stringify(publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections)}`)
    //check if the actual deps are a subset of the deps we might have
    cy.wrap(
      publicationQueueAfter[publicationQueueAfter.length-1].dependsOnQueuedCollections.every(dep => shouldMaybeHaveDepsUuids.includes(dep))
    ).should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch (model) {
      case 'cover-point-intercept-survey':
        //sufficient to just check the survey exists
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        
        coverSurvey = latestQueuedCollection[model]
        break
      case 'fire-survey':
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
          .and('have.any.keys', 'fire_ignition_date_accurate', 'fire_ignition_date_is_estimate')
        
        fireSurvey = latestQueuedCollection[model]
        break
      case 'cover-point-intercept-point':
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        
        cy.wrap(Array.isArray(latestQueuedCollection[model]))
          .should('be.true')
        
        cy.wrap(latestQueuedCollection[model].length == 50)
          .should('be.true')

        for (const point of latestQueuedCollection[model]) {
          cy.log(`Checking point on transect ${point.cover_transect_start_point} and point number ${point.point_number}`)
          
          cy.wrap(Array.isArray(point.species_intercepts))
            .should('be.true')
          
          if (point.species_intercepts.length > 0) {
            for (const intercept of point.species_intercepts) {
              for (const variant of ['full', 'lite']) {
                if (intercept[`floristics_voucher_${variant}`]) {
                  cy.wrap(
                    //relation to voucher collected online
                    Number.isInteger(intercept[`floristics_voucher_${variant}`]) ||
                    //temp link to voucher collected offline (cast to boolean to resolve in `wrap`)
                    !!intercept[`floristics_voucher_${variant}`].temp_offline_id
                  ).should('be.true')
                }
              }
            }
          }
        }
        break
      case 'fire-point-intercept-point':
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        
        cy.wrap(Array.isArray(latestQueuedCollection[model]))
          .should('be.true')
        
        cy.wrap(latestQueuedCollection[model].length == 50)
          .should('be.true')
        
        const hashTagTransects = [
          'S2',
          'N2',
          'S4',
          'N4',
          'W2',
          'E2',
          'W4',
          'E4',
        ]
        //TODO should probably cross-check the points and intercepts with the main cover collection (as they're collected together, fire doesn't contain the species intercepts)
        for (const point of latestQueuedCollection[model]) {
          cy.log(`Checking point on transect ${point.transect_start_point} and point number ${point.point_number}`)

          cy.wrap(Array.isArray(point.fire_species_intercepts))
            .should('be.true')
          
          const pointIsHashtag = hashTagTransects.some((o) => o === point.transect_start_point)
          if (point.fire_species_intercepts.length > 0) {
            for (const intercept of point.fire_species_intercepts) {
              cy.wrap(intercept).should('have.any.key', 'plant_unidentifiable')

              //TODO test logic doesn't enforce this (neither does the client) - need to extend so leave out for now
              // if (intercept.plant_unidentifiable) {
              //   //unidentifiable plants should have a fire growth form
              //   cy.wrap(intercept).should('have.any.key', 'fire_growth_form')
              // }

              // if (pointIsHashtag) {
              //   //can also have these keys on hashtag transects
              //   cy.wrap(intercept).should('have.any.keys', 'resprouting_type', 'fire_impact')
              // }
            }
          }
        }
        break
      case 'plot-fauna-layout':
        //this model is included but is redundant - technical debt see #353
        break
      case 'plot-layout':
        if (latestQueuedCollection[model]?.id?.temp_offline_id) {
          //we've reselected a plot that was collected offline
          cy.wrap(latestQueuedCollection[model].id)
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(latestQueuedCollection[model].id.temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
        } else {
          cy.wrap(latestQueuedCollection[model])
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(latestQueuedCollection[model].temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
        }
        break
      case 'plot-visit':
        if (latestQueuedCollection[model].visit_field_name) {
          //plot description has this model but not this visit_field_name (just temp ID)
          cy.wrap(latestQueuedCollection[model].visit_field_name)
            .should('eq', shouldHaveData.plotVisitName)
          
          //visit also has relation to layout
          cy.wrap(latestQueuedCollection[model].plot_layout)
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(latestQueuedCollection[model].plot_layout.temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
            //check the temp ID stored in visit (as relation to layout) is the same as the
            //temp ID in the layout
            .and('eq', latestQueuedCollection['plot-layout'].temp_offline_id)
        }

        if (latestQueuedCollection[model]?.id?.temp_offline_id) {
          //we've reselected a plot that was collected offline
          cy.wrap(latestQueuedCollection[model].id)
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(latestQueuedCollection[model].id.temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
        } else {
          cy.wrap(latestQueuedCollection[model])
            .should('have.any.key', 'temp_offline_id')
          cy.wrap(latestQueuedCollection[model].temp_offline_id)
            .should('not.be.undefined')
            .and('not.be.null')
        }
        break
    }
  }
}

const createCollectionQueueUuidTrackerOnly = ({
  shouldHaveData,
  publicationQueueAfter,
}) => {
  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier
}

/**
 * Does a boolean XOR comparison
 * 
 * Source: https://www.howtocreate.co.uk/xor.html
 * 
 * @param {*} a some value that can be truthy or falsey
 * @param {*} b another value that can be truthy or falsey
 * 
 * @returns {Boolean} whether a XOR b is true
 */
const xorCompare = (a,b) => {
  return ( a || b ) && !( a && b ) 
}

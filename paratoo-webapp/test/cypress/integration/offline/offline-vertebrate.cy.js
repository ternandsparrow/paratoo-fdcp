//TODO test when plot layout/visit were collected online

import { isEqual } from 'lodash'
import { vertebrateTrapCheckMeasurement as measurementFields } from '../../support/vertConditionalFields'
import { validate as uuidValidate } from 'uuid'
import {
  surveyMetaEqual,
} from '../../support/functions'
import { pubOffline } from '../../support/command-utils'
import { createVertCollectionMetadata } from '../../support/protocol-commands/vert-commands'

//NOTE: turn this on for debugging - will dump the state to a file before adding a
//collection to queue and before publishing the queue at the end
const DUMP_STATE = false

const speciesClass = {
  mammal: 'Mammal',
  amphibian: 'Amphibian',
  reptile: 'Reptile',
}

const speciesLut = {
  mammal: 'MA',
  amphibian: 'AM',
  reptile: 'RE',
}

const { trapNumbers, vertTrapLines } = createVertCollectionMetadata()

/**
 * A generic helper that can be used in the callback for `submitOfflineCollections`.
 * All surveys' relations to the visit/plot are structured the same
 *
 * @param {Object} apiResp the Strapi API response
 * @param {Array} collectionsToCheck the `collectionsToCheck` passed into each
 * callback for `submitOfflineCollections`
 * @param {String} model the pluralised model (i.e., API endpoint postfix)
 */
const validateSurveyLinksToPlot = (apiResp, collectionsToCheck, model) => {
  cy.log(`Validating survey links to plot for model '${model}'`)
  for (const c of collectionsToCheck) {
    console.debug(c)
    if (c.visitIsNew) {
      //check that the setup survey got linked to the correct visit/plot
      const visit = apiResp.body.data.find(
        (o) =>
          o.attributes.plot_visit.data.attributes.visit_field_name ===
          c.visitFieldName,
      )
      console.debug('visit: ', JSON.stringify(visit))

      cy.wrap(visit).should('not.be.undefined').and('not.be.null')
      const plotLabelForVisit =
        visit.attributes.plot_visit.data.attributes.plot_layout.data
          .attributes.plot_selection.data.attributes.plot_label
      cy.log(
        `correct plot label for visit? ${plotLabelForVisit == c.plotLabel}`,
      )
      cy.wrap(plotLabelForVisit).should('eq', c.plotLabel)
    }
  }
}

/**
 * A generic helper that can be used in the callback for `submitOfflineCollections`.
 * Since many of the 'obs' for the 'check' collection are similarly structured, use
 * this function to handle checking the links to the 'check' and 'setup' surveys
 *
 * @param {Object} apiResp the Strapi API response
 * @param {Array} collectionsToCheck the `collectionsToCheck` passed into each
 * callback for `submitOfflineCollections`
 * @param {String} model the pluralised model (i.e., API endpoint postfix)
 * @param {Number} [obsLength] the number of obs to check (default 2)
 * @param {String} [setupRelationFieldName] the field name used in the relation to
 * the setup survey (typically 'trap_setup')
 */
const validateObsSurveyLinks = (
  apiResp,
  collectionsToCheck,
  model,
  obsLength = 2,
  setupRelationFieldName = 'trap_setup',
) => {
  cy.log(
    `Validating links to 'check' and 'setup' surveys are correctly applied for model '${model}'`,
  )
  for (const c of collectionsToCheck) {
    console.debug(c)
    let filteredObs = null
    let surveyIdToCheckAgainst = null
    let obSurveyFieldName = null //not the field name of the setup

    //both 'check' and 'closure' collections are similar, so do this check/assignment
    //first before filtering the obs
    if (c.modelsLinkedToCheckSurvey?.includes(model) && c.checkSurvey) {
      surveyIdToCheckAgainst = c.checkSurvey.survey_metadata
      obSurveyFieldName = 'trap_check_survey'
    } else if (
      c.modelsLinkedToClosureSurvey?.includes(model) &&
      c.closureSurvey
    ) {
      surveyIdToCheckAgainst = c.closureSurvey.survey_metadata
      obSurveyFieldName = 'end_trapping_survey'
    }

    if (surveyIdToCheckAgainst && obSurveyFieldName) {
      //check this ob (irrespective of setup, which is handled below)
      filteredObs = apiResp.body.data.filter((o) =>
        surveyMetaEqual(
          o.attributes[obSurveyFieldName].data.attributes.survey_metadata,
          surveyIdToCheckAgainst,
        ),
      )
      console.log(`filteredObs (model=${model}): `, filteredObs)

      if (obsLength !== 2) {
        cy.log(
          `Checking for non-default number of observations: ${obsLength}`,
        )
      }

      //the actual data is fairly basic, so just check expected number of obs
      cy.wrap(Array.isArray(filteredObs)).should('be.true')
      cy.wrap(filteredObs.length == obsLength).should('be.true')
    }
  }
}

const selectionAndLayoutModule = 'Plot Selection and Layout'
const layoutVisitProtocol = 'Plot Layout and Visit'
const vertModule = 'Vertebrate Fauna'
const vertTrapSetupProtocol = 'Vertebrate Fauna - Trapping Survey Setup'
const vertTrapCheckProtocol = 'Vertebrate Fauna - Identify, Measure and Release'
const vertTrapClosureProtocol = 'Vertebrate Fauna - Trapping Survey Closure'
const projectCy = 'Kitchen\\ Sink\\ TEST\\ Project'

//will be key=<collection_label>, value=<queuedCollectionIdentifier>
let collectionLabelQueueUuids = {}

let setupSurvey = null
let checkSurvey = null
let closureSurvey = null

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })

  it('.should() - prepare for offline collections', () => {
    cy.prepareForOfflineVisit(projectCy)
  })

  it('.should() - go offline', () => {
    cy.window().then(($win) => {
      $win.ephemeralStore.setNetworkOnline(false)
    })
  })
})

describe(`Vertebrate trapping for offline Layout and Visit`, () => {
  describe(`Collection 1 - ${vertTrapSetupProtocol}`, () => {
    it(`.should() - navigate to ${layoutVisitProtocol}`, () => {
      cy.selectProtocolIsEnabled(selectionAndLayoutModule)
    })

    it('.should() - do Plot Layout (create new plot and visit)', () => {
      cy.newLayoutAndVisit(
        'SATFLB0003',
        {
          lat: -34.972959727525875,
          lng: 138.64066633095894,
        },
        true,
        'SA 3 test visit',
      )
    })

    it(`.should() - submit offline collection for ${layoutVisitProtocol}`, () => {
      //no callback - can assume it will be good as we already test for this in `offline-plot-layout-visit-description`
      cy.workflowPublishOffline(
        {
          models: [
            'plot-definition-survey',
            'plot-fauna-layout',
            'plot-layout',
            'plot-visit',
          ],
          plotLabel: 'SATFLB0003',
          plotVisitName: 'SA 3 test visit',
          protocolName: layoutVisitProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_1_layout',
        },
        createCollectionQueueUuidTrackerOnly,
        null,
        DUMP_STATE ? 'offline_vert_collection_1_layout' : null,
      )
    })

    it('.should() - assert plot context was set correctly', () => {
      cy.assertPlotContext('SATFLB0003', 'SA 3 test visit')
    })

    it(`.should() - navigate to ${vertTrapSetupProtocol}`, () => {
      cy.selectProtocolIsEnabled(vertModule, vertTrapSetupProtocol)
    })

    it(`.should() - collect survey step for ${vertTrapSetupProtocol}`, () => {
      // cy.get('[data-cy="Core monitoring plot"]').click()
      cy.nextStep()
    })

    //TODO test editing
    for (const [trapLine, trapData] of Object.entries(vertTrapLines)) {
      it(`.should() - collect ${trapData.numOfTraps} traps for trap line '${trapLine}'`, () => {
        cy.setExpansionItemState(`${trapLine}_expansion`, 'open')
        cy.vertStartTrapLine(trapData.isFence)
        cy.vertMarkTrap(trapData.numOfTraps)
        cy.vertEndTrapLine(trapLine)
      })
    }

    it(`.should() - submit offline collection for ${vertTrapSetupProtocol} and assert collection was queued correctly`, () => {
      cy.nextStep()
      cy.workflowPublishOffline(
        {
          models: [
            'vertebrate-trapping-setup-survey',
            'vertebrate-trap-line',
            'plot-layout',
            'plot-visit',
          ],
          plotLabel: 'SATFLB0003',
          plotVisitName: 'SA 3 test visit',
          protocolName: vertTrapSetupProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'collection_1_verts',
          collectionDependsOn: ['collection_1_layout'],
        },
        assertTrapSetupCb,
        null,
        DUMP_STATE ? 'offline_vert_collection_1_verts' : null,
      )
    })
  })

  //this `describe` is already nested, so use `context` to wrap `it` (for better
  //organisation of tests)
  //TODO collect multiple checks (e.g., a 'morning' and 'afternoon' one)
  describe(`Collection 2 - ${vertTrapCheckProtocol}`, () => {
    context('Setup/init', () => {
      it('.should() - assert plot context was set correctly', () => {
        cy.assertPlotContext('SATFLB0003', 'SA 3 test visit')
      })

      it(`.should() - navigate to ${vertTrapCheckProtocol}`, () => {
        cy.selectProtocolIsEnabled(vertModule, vertTrapCheckProtocol)
      })

      it(`.should() - collect survey step for ${vertTrapCheckProtocol}`, () => {
        cy.selectFromDropdown('trap_check_interval', 'Morning')
        cy.addObservers('trap_checked_by', ['John', 'Jane', 'Mary'])
        cy.nextStep()
      })
    })

    context('Check traps', () => {
      it(`.should() - Check trap P1 with capture status`, () => {
        cy.vertCheckTrap('P1', 'Capture')
      })
      it(`.should() - Record general info of captured animal in P1`, () => {
        cy.vertGeneralInfoOfTrapWithCapture(
          'P1',
          speciesClass.mammal,
          'Platypus',
        )
      })

      it(`.should() - Record animal's measurements`, () => {
        cy.vertRecordMeasurements(measurementFields, speciesLut.mammal)
      })

      it(`.should() - Record animal's reproductive traits`, () => {
        cy.vertRecordReproductiveTraits('P1')
      })

      it(`.should() - Record animal's sex and age class`, () => {
        cy.vertRecordSexAndAgeClass('P1')
      })

      it(`.should() - Record animal's body and skin condition`, () => {
        cy.vertRecordBodyAndSkinCondition('P1')
      })

      it(`.should() - Record animal's Clinical scoring`, () => {
        cy.vertRecordClinicalScoring('P1')
      })
      it(`animal fate`, function () {
        cy.dataCy('animal_fate').selectFromDropdown(null)
      })
      it(`save individual record`, function () {
        cy.dataCy('done').eq(-2).click()
      })
      it(`check trap`, function () {
        cy.dataCy('done').eq(-1).click()
      })
    })
    context('Check traps', () => {
      it(`.should() - Check trap F2 with capture status`, () => {
        cy.vertCheckTrap('F2', 'Capture')
      })
      it(`.should() - Record general info of captured animal in F2`, () => {
        cy.vertGeneralInfoOfTrapWithCapture(
          'F2',
          speciesClass.reptile,
          'Crocodylus [Genus]',
        )
      })

      it(`.should() - Record animal's measurements`, () => {
        cy.vertRecordMeasurements(measurementFields, speciesLut.reptile)
      })

      // it(`.should() - Record animal's reproductive traits`, () => {
      //   //F2 isn't applicable in this step
      // })

      it(`.should() - Record animal's sex and age class`, () => {
        cy.vertRecordSexAndAgeClass()
      })

      it(`.should() - Record animal's body and skin condition`, () => {
        cy.vertRecordBodyAndSkinCondition('F2')
      })

      it(`.should() - Record animal's Clinical scoring`, () => {
        cy.vertRecordClinicalScoring('F2')
        cy.getAndValidate('animal_fate').selectFromDropdown(null)
      })
      it(`save individual record`, function () {
        cy.dataCy('done').eq(-2).click()
      })
      it(`check trap`, function () {
        cy.dataCy('done').eq(-1).click()
      })
    })

    context('Submission', () => {
      it(`.should() - submit offline collection for ${vertTrapCheckProtocol} and assert collection was queued correctly`, () => {
        cy.nextStep()
        cy.workflowPublishOffline(
          {
            models: [
              'vertebrate-trap-check-survey',
              'vertebrate-check-trap',
              // 'vertebrate-record-capture',
              // 'vertebrate-trap-check-measurement',
              // 'vertebrate-trap-check-reproductive',
              // 'vertebrate-trap-check-sex-age',
              // 'vertebrate-body-skin',
              // 'vertebrate-clinical-scoring',
              // 'vertebrate-trap-check-photo',
              'plot-layout',
              'plot-visit',
            ],
            plotLabel: 'SATFLB0003',
            plotVisitName: 'SA 3 test visit',
            protocolName: vertTrapCheckProtocol,
            projectName: 'Kitchen Sink TEST Project',
            collectionLabel: 'collection_2_verts',
            collectionDependsOn: ['collection_1_layout', 'collection_1_verts'],
          },
          assertTrapCheckCb,
          -1,
          DUMP_STATE ? 'offline_vert_collection_2_verts' : null,
        )
      })
    })
  })

  //this `describe` is already nested, so use `context` to wrap `it` (for better
  //organisation of tests)
  describe(`Collection 3 - ${vertTrapClosureProtocol}`, () => {
    context('Setup/init', () => {
      it('.should() - assert plot context was set correctly', () => {
        cy.assertPlotContext('SATFLB0003', 'SA 3 test visit')
      })

      it(`.should() - navigate to ${vertTrapClosureProtocol}`, () => {
        cy.selectProtocolIsEnabled(vertModule, vertTrapClosureProtocol)
        cy.nextStep()
      })
    })

    context('End traps', () => {
      // trapNumbers.reverse()
      for (const trapNumber of trapNumbers) {
        it(`Close trap ${trapNumber}`, () => {
          if (trapNumber === 'P1') {
            cy.dataCy('trap_number').selectFromDropdown(new RegExp(`^${Cypress._.escapeRegExp(trapNumber)}$`), trapNumber, -1)
          }
          // only pitfall traps have 'Pitfall closed' status
          // which have a 'P' prefix
          if (/\bP\w*/.test(trapNumber)) {
            cy.selectFromDropdown('trap_status', 'Pitfall closed - filled in')
            cy.setCheck('toggleBarcodeReaderShow', false)
            cy.dataCy('addImg').click()
            cy.dataCy('takePhoto').click()
          } else {
            cy.selectFromDropdown('trap_status', 'Trap closed - permanently')
          }


          cy.dataCy('done').click().wait(100)
        })
      }
    })

    context('Take end drift fences photos', () => {
      it(`.should() - take photo of drift B`, () => {
        cy.nextStep()

        cy.selectFromDropdown('drift_line', 'B')
        cy.dataCy('takePhoto').click()
      })
      it(`.should() - take photo of drift E`, () => {
        cy.selectFromDropdown('drift_line', 'E')
        cy.dataCy('takePhoto').click()
      })
    })

    context('Submission', () => {
      it(`.should() - submit offline collection for ${vertTrapClosureProtocol} and assert collection was queued correctly`, () => {
        cy.nextStep()
        cy.workflowPublishOffline(
          {
            models: [
              'vertebrate-end-trapping-survey',
              'vertebrate-end-trap',
              'vertebrate-closed-drift-photo',
              'plot-layout',
              'plot-visit',
            ],
            plotLabel: 'SATFLB0003',
            plotVisitName: 'SA 3 test visit',
            protocolName: vertTrapClosureProtocol,
            projectName: 'Kitchen Sink TEST Project',
            collectionLabel: 'collection_3_verts',
            collectionDependsOn: ['collection_1_layout', 'collection_1_verts'],
          },
          assertTrapClosureCb,
          null,
          DUMP_STATE ? 'offline_vert_collection_3_verts' : null,
        )
      })
    })
  })
})

describe('Submit queued offline collections', () => {
  it('.should() - go online', () => {
    cy.window().then(($win) => {
      $win.ephemeralStore.setNetworkOnline(true)
    })
  })

  if (DUMP_STATE) {
    it('.should() - dump state', () => {
      cy.dumpState({
        fileName: 'offline-vertebrate',
      })
    })
  }

  const newCollections = [
    //collection 1 (vert trap setup)
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      setupSurvey: setupSurvey,
      modelsLinkedToSetupSurvey: ['vertebrate-trap-lines'],
    },
    //collection 2 (vert trap check)
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      setupSurvey: setupSurvey,
      modelsLinkedToSetupSurvey: ['vertebrate-check-traps'],
      checkSurvey: checkSurvey,
      modelsLinkedToCheckSurvey: ['vertebrate-check-traps'],
    },
    //collection 3 (vert trap closure)
    {
      plotLabel: 'SATFLB0003',
      visitFieldName: 'SA 3 test visit',
      plotIsNew: true,
      hasNewFaunaPlot: false,
      visitIsNew: true,
      setupSurvey: setupSurvey,
      modelsLinkedToSetupSurvey: [
        'vertebrate-end-traps',
        'vertebrate-closed-drift-photos',
      ],
      closureSurvey: closureSurvey,
      modelsLinkedToClosureSurvey: [
        'vertebrate-end-traps',
        'vertebrate-closed-drift-photos',
      ],
    },
  ]

  const cbs = [
    {
      collectionModel: 'plot-layouts',
      cb: (layoutApiResp, collectionsToCheck) => {
        console.log('layoutApiResp: ', layoutApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c.plotIsNew || c.hasNewFaunaPlot) {
            const plot = layoutApiResp.body.data.find(
              (o) =>
                o.attributes.plot_selection.data.attributes.plot_label ===
                c.plotLabel,
            )
            console.debug('plot: ', JSON.stringify(plot))
            cy.wrap(plot).should('not.be.undefined').and('not.be.null')
            cy.wrap(plot.attributes)
              .should('not.be.undefined')
              .and('not.be.null')

            if (c.plotIsNew) {
              cy.wrap(plot.attributes.plot_points)
                .should('not.be.undefined')
                .and('not.be.null')
            }

            if (c.hasNewFaunaPlot) {
              cy.wrap(plot.attributes.fauna_plot_point)
                .should('not.be.undefined')
                .and('not.be.null')
            }
          }
        }
      },
    },
    {
      collectionModel: 'plot-visits',
      cb: (visitApiResp, collectionsToCheck) => {
        console.log('visitApiResp: ', visitApiResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c.visitIsNew) {
            const visit = visitApiResp.body.data.find(
              (o) => o.attributes.visit_field_name === c.visitFieldName,
            )
            console.debug('visit: ', JSON.stringify(visit))
            cy.wrap(visit).should('not.be.undefined').and('not.be.null')

            const plotLabelForVisit =
              visit.attributes.plot_layout.data.attributes.plot_selection.data
                .attributes.plot_label
            cy.wrap(plotLabelForVisit).should('eq', c.plotLabel)
          }
        }
      },
    },
    {
      collectionModel: 'vertebrate-trapping-setup-surveys',
      cb: (vertTrappingSurveySetupApiResp, collectionsToCheck) => {
        console.log(
          'vertTrappingSurveySetupApiResp: ',
          vertTrappingSurveySetupApiResp,
        )
        validateSurveyLinksToPlot(
          vertTrappingSurveySetupApiResp,
          collectionsToCheck,
          'vertebrate-trapping-setup-surveys',
        )
      },
    },
    {
      collectionModel: 'vertebrate-trap-lines',
      cb: (vertTrappingSetupTrapLineApiResp, collectionsToCheck) => {
        console.log(
          'vertTrappingSetupTrapLineApiResp: ',
          vertTrappingSetupTrapLineApiResp,
        )
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (
            c.modelsLinkedToSetupSurvey?.includes('vertebrate-trap-lines') &&
            c.setupSurvey
          ) {
            const filteredLines =
              vertTrappingSetupTrapLineApiResp.body.data.filter((o) =>
                surveyMetaEqual(
                  o.attributes.trapping_survey.data.attributes.survey_metadata,
                  c.setupSurvey.survey_metadata,
                ),
              )
            console.log('filteredLines: ', filteredLines)

            //the actual data is fairly basic, so just check expected number of lines
            cy.wrap(Array.isArray(filteredLines)).should('be.true')
            cy.wrap(filteredLines.length == 6).should('be.true')
          }
        }
      },
    },
    {
      collectionModel: 'vertebrate-trap-check-surveys',
      cb: (vertTrappingCheckSurveyApiResp, collectionsToCheck) => {
        console.log(
          'vertTrappingCheckSurveyApiResp: ',
          vertTrappingCheckSurveyApiResp,
        )
        validateSurveyLinksToPlot(
          vertTrappingCheckSurveyApiResp,
          collectionsToCheck,
          'vertebrate-trap-check-surveys',
        )
      },
    },
    {
      collectionModel: 'vertebrate-check-traps',
      cb: (vertTrappingCheckTrapApiResp, collectionsToCheck) => {
        console.log(
          'vertTrappingCheckTrapApiResp: ',
          vertTrappingCheckTrapApiResp,
        )
        validateObsSurveyLinks(
          vertTrappingCheckTrapApiResp,
          collectionsToCheck,
          'vertebrate-check-traps',
        )
      },
    },
    {
      collectionModel: 'vertebrate-end-trapping-surveys',
      cb: (vertTrappingClosureSurveyApiResp, collectionsToCheck) => {
        console.log(
          'vertTrappingClosureSurveyApiResp: ',
          vertTrappingClosureSurveyApiResp,
        )
        validateSurveyLinksToPlot(
          vertTrappingClosureSurveyApiResp,
          collectionsToCheck,
          'vertebrate-end-trapping-surveys',
        )
      },
    },
    {
      collectionModel: 'vertebrate-end-traps',
      cb: (vertTrappingClosureTrapApiResp, collectionsToCheck) => {
        console.log(
          'vertTrappingClosureTrapApiResp: ',
          vertTrappingClosureTrapApiResp,
        )
        validateObsSurveyLinks(
          vertTrappingClosureTrapApiResp,
          collectionsToCheck,
          'vertebrate-end-traps',
          trapNumbers.length,
          'trap_number',
        )
      },
    },
    {
      collectionModel: 'vertebrate-closed-drift-photos',
      cb: (vertTrappingClosureDriftPhotoApiResp, collectionsToCheck) => {
        console.log(
          'vertTrappingClosureDriftPhotoApiResp: ',
          vertTrappingClosureDriftPhotoApiResp,
        )
        validateObsSurveyLinks(
          vertTrappingClosureDriftPhotoApiResp,
          collectionsToCheck,
          'vertebrate-end-traps',
          2, //default
          'drift_line',
        )
      },
    },
  ]

  pubOffline({
    collectionsToCheck: newCollections,
    collectionIterCbs: cbs,
  })
})

const assertTrapSetupCb = ({
  shouldHaveData,
  publicationQueueAfter,
}) => {
  const latestQueuedCollection =
    publicationQueueAfter[publicationQueueAfter.length - 1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  cy.log(
    `Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel
    }' to: ${publicationQueueAfter[publicationQueueAfter.length - 1]
      .queuedCollectionIdentifier
    }`,
  )
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] =
    publicationQueueAfter[
      publicationQueueAfter.length - 1
    ].queuedCollectionIdentifier

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      (collectionLabelDep) => collectionLabelQueueUuids[collectionLabelDep],
    )
    cy.log(
      `Checking queued collection tracked collections dependencies that we should have. Comparing superset (actual) / expected: ${JSON.stringify(
        publicationQueueAfter[publicationQueueAfter.length - 1]
          .dependsOnQueuedCollections,
      )} / ${JSON.stringify(shouldHaveDepsUuids)}`,
    )
    cy.wrap(
      shouldHaveDepsUuids.every((dep) =>
        publicationQueueAfter[
          publicationQueueAfter.length - 1
        ].dependsOnQueuedCollections.includes(dep),
      ),
    ).should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every((o) => collectionModels.includes(o)),
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch (model) {
      case 'vertebrate-trapping-setup-survey':
        //sufficient to just check the survey exists
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        setupSurvey = latestQueuedCollection[model]
        break
      case 'vertebrate-trap-line':
        cy.log('Checking Trap Line is populated with expected number of lines')
        cy.wrap(
          Array.isArray(latestQueuedCollection[model]) &&
          //generally check array is populated
          latestQueuedCollection[model]?.length > 0 &&
          //specifically confirm that the 6 trap lines are there
          latestQueuedCollection[model].length === 6,
        ).should('be.true')

        const lineBaseKeys = [
          'start_location',
          'line_name',
          'end_location',
          //added when it's queued to data manager
          'trapping_survey',
          'temp_offline_id',
          'traps',
          'date_time'
        ]
        for (const line of latestQueuedCollection[model]) {
          const actualKeys = Object.keys(line).sort()

          cy.log(`vertTrapLines[${line.line_name}]: ${JSON.stringify(vertTrapLines[line.line_name])}`)

          const lineShouldHaveKeys = [...lineBaseKeys]
          if (vertTrapLines[line.line_name].isFence) {
            lineShouldHaveKeys.push(...[
              'fence_type',
              'fence_height',
            ])
          }
          lineShouldHaveKeys.sort()   //is in-place

          cy.log(
            `Comparing actual keys / key it should have for trap line '${line.line_name
            }':\n${actualKeys.toString()} / \n${lineShouldHaveKeys.toString()}`,
          )

          cy.wrap(isEqual(lineShouldHaveKeys, actualKeys)).should('be.true')
        }

        cy.log('Checking each line\'s traps are populated with expected number of traps')
        for (const line of latestQueuedCollection[model]) {
          cy.log(`Checking line ${line.line_name}`)
          cy.wrap(
            Array.isArray(line.traps) &&
            line.traps.length > 0 &&
            line.traps.length === vertTrapLines[line.line_name].numOfTraps,
          ).should('be.true')

          const trapShouldHaveKeysBase = [
            'barcode',
            'line_name',
            'trap_number',
            'trap_setup_date',
            'trap_specifics',
            'trap_type',
            'trap_width',
            'number_of_traps',
            'trap_set_status',
            //added when it's queued to data manager
            'temp_offline_id',
          ]
          for (const trap of line.traps) {
            const trapShouldHaveKeys = [...trapShouldHaveKeysBase]
            if (trap.trap_type === 'P') {
              trapShouldHaveKeys.push('trap_depth')
            } else {
              trapShouldHaveKeys.push('trap_height')
            }
            trapShouldHaveKeys.sort()   //is in-place

            const actualKeys = Object.keys(trap).sort()
            cy.log(
              `Comparing actual keys / key it should have for trap '${trap.trap_number
              }' on trap line '${trap.line_name
              }':\n${actualKeys.toString()} / \n${trapShouldHaveKeys.toString()}`,
            )
            cy.wrap(isEqual(trapShouldHaveKeys, actualKeys)).should('be.true')
          }
        }

        break
      case 'plot-layout':
      case 'plot-visit':
        cy.wrap(latestQueuedCollection[model]).should(
          'have.any.key',
          'temp_offline_id',
        )
        cy.wrap(latestQueuedCollection[model].temp_offline_id)
          .should('not.be.undefined')
          .and('not.be.null')
        break
    }
  }
}

const assertTrapCheckCb = ({
  shouldHaveData,
  publicationQueueAfter,
}) => {
  const lastPublication = publicationQueueAfter.at(-1)
  const { collection, queuedCollectionIdentifier, dependsOnQueuedCollections } =
    lastPublication
  console.log('collection:', collection)

  cy.log(
    `Setting queued collection identifier for collection label \
    '${shouldHaveData.collectionLabel}' to: ${queuedCollectionIdentifier}`,
  )

  collectionLabelQueueUuids[shouldHaveData.collectionLabel] =
    queuedCollectionIdentifier

  const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn?.map(
    (collectionLabelDep) => collectionLabelQueueUuids[collectionLabelDep],
  )

  if (shouldHaveDepsUuids?.length) {
    cy.log(
      `Checking queued collection tracked collections dependencies that we should have. Comparing superset (actual) / expected: ${JSON.stringify(
        dependsOnQueuedCollections,
      )} / ${JSON.stringify(shouldHaveDepsUuids)}`,
    )

    cy.wrap(
      shouldHaveDepsUuids.every((dep) =>
        dependsOnQueuedCollections.includes(dep),
      ),
    ).should('be.true')
  }

  const collectionModels = Object.keys(collection)

  cy.wrap(
    shouldHaveData.models.every((model) => collectionModels.includes(model)),
  ).should('be.true')

  const shouldHaveKeys = {
    'vertebrate-check-trap': [
      'barcode',
      'setup_status',
      'trap_check_photo',
      'trap_check_status',
      'trap_number',
      'trap_setup',
      'captured_individual',
    ],
  }

  const shouldHaveLength = {
    'vertebrate-check-trap': 2,
  }

  for (const model of shouldHaveData.models) {
    console.log(`collection[${model}]`, collection[model])
    switch (model) {
      case 'vertebrate-trap-check-survey':
        cy.wrap(collection[model]).should('exist')
        checkSurvey = collection[model]
        break
      case 'vertebrate-check-trap':
        cy.log(
          `Checking observation model '${model}' has expected length=${shouldHaveLength[model]}`,
        )

        cy.wrap(
          Array.isArray(collection[model]) &&
          collection[model].length === shouldHaveLength[model],
        ).should('be.true')

        for (const ob of collection[model]) {
          if (shouldHaveKeys[model]) {
            const expectedKeys = shouldHaveKeys[model]
            const actualKeys = Object.keys(ob)
            const additionalMsg = `trap number ${ob.trap_number}`

            assertKeys(expectedKeys, actualKeys, model, additionalMsg)
            cy.log(
              `Checking observation model '${model}' (${ob.trap_number}) has temporary link to setup`,
            )
            cy.wrap(ob.trap_setup).should('have.keys', 'temp_offline_id')
          }
        }
        break
      case 'plot-layout':
      case 'plot-visit':
        cy.wrap(collection[model]).should('have.any.key', 'temp_offline_id')
        cy.wrap(collection[model].temp_offline_id).should('exist')
        break
    }
  }
  function assertKeys(expectedKeys, actualKeys, model, additionalMsg) {
    cy.log(
      `Comparing actual keys vs key it should have for model '${model}' (${additionalMsg}):\n${actualKeys.toString()}\n${expectedKeys.toString()}`,
    )
    cy.log(actualKeys.sort(), expectedKeys.sort())
    cy.wrap(expectedKeys.every((o) => actualKeys.includes(o))).should('be.true')
  }
}

const assertTrapClosureCb = ({
  shouldHaveData,
  publicationQueueAfter,
}) => {
  const latestQueuedCollection =
    publicationQueueAfter[publicationQueueAfter.length - 1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  cy.log(
    `Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel
    }' to: ${publicationQueueAfter[publicationQueueAfter.length - 1]
      .queuedCollectionIdentifier
    }`,
  )
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] =
    publicationQueueAfter[
      publicationQueueAfter.length - 1
    ].queuedCollectionIdentifier

  if (
    shouldHaveData.collectionDependsOn &&
    Array.isArray(shouldHaveData.collectionDependsOn) &&
    shouldHaveData.collectionDependsOn.length > 0
  ) {
    const shouldHaveDepsUuids = shouldHaveData.collectionDependsOn.map(
      (collectionLabelDep) => collectionLabelQueueUuids[collectionLabelDep],
    )
    cy.log(
      `Checking queued collection tracked collections dependencies that we should have. Comparing superset (actual) / expected: ${JSON.stringify(
        publicationQueueAfter[publicationQueueAfter.length - 1]
          .dependsOnQueuedCollections,
      )} / ${JSON.stringify(shouldHaveDepsUuids)}`,
    )
    //TODO put assertion back in
    cy.wrap(
      shouldHaveDepsUuids.every((dep) =>
        publicationQueueAfter[
          publicationQueueAfter.length - 1
        ].dependsOnQueuedCollections.includes(dep),
      ),
    ) //.should('be.true')
  }

  const collectionModels = Object.keys(latestQueuedCollection)
  cy.wrap(
    shouldHaveData.models.every((o) => collectionModels.includes(o)),
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch (model) {
      case 'vertebrate-end-trapping-survey':
        //sufficient to just check the survey exists
        cy.wrap(latestQueuedCollection[model])
          .should('not.be.undefined')
          .and('not.be.null')
        closureSurvey = latestQueuedCollection[model]
        break
      case 'vertebrate-end-trap':
        cy.log(`Checking observation model '${model}' has expected length`)
        cy.wrap(
          Array.isArray(latestQueuedCollection[model]) &&
          //specifically confirm that the expected number of obs are there
          latestQueuedCollection[model].length === trapNumbers.length,
        ).should('be.true')

        const expectedKeysTrap = ['trap_number', 'trap_status', 'date_time']
        for (const trap of latestQueuedCollection[model]) {
          const expectedKeysCurrTrap = [...expectedKeysTrap]
          let actualKeys = Object.keys(trap)
          if (actualKeys.includes('closed_pitfall_photo')) {
            //photo not always taken
            expectedKeysCurrTrap.push('closed_pitfall_photo')
          }
          cy.log(
            `Comparing actual keys vs key it should have for model '${model}':\n${actualKeys.toString()}\n${expectedKeysCurrTrap.toString()}`,
          )
          cy.wrap(isEqual(expectedKeysCurrTrap.sort(), actualKeys.sort())).should(
            'be.true',
          )

          cy.log(
            `Checking observation model '${model}' has temporary link to trap`,
          )
          cy.wrap(trap.trap_number).should('have.keys', 'temp_offline_id')
        }
        break
      case 'vertebrate-closed-drift-photo':
        cy.log(`Checking observation model '${model}' has expected length`)
        cy.wrap(
          Array.isArray(latestQueuedCollection[model]) &&
          //specifically confirm that the expected number of obs are there
          latestQueuedCollection[model].length === 2,
        ).should('be.true')

        const expectedKeysLine = ['closed_drift_line_photo', 'drift_line']
        for (const line of latestQueuedCollection[model]) {
          let actualKeys = Object.keys(line)
          cy.log(
            `Comparing actual keys vs key it should have for model '${model}':\n${actualKeys.toString()}\n${expectedKeysLine.toString()}`,
          )
          cy.wrap(isEqual(expectedKeysLine.sort(), actualKeys.sort())).should(
            'be.true',
          )

          cy.log(
            `Checking observation model '${model}' has temporary link to trap`,
          )
          cy.wrap(uuidValidate(line.drift_line)).should('be.true')
        }
        break
      case 'plot-layout':
      case 'plot-visit':
        cy.wrap(latestQueuedCollection[model]).should(
          'have.any.key',
          'temp_offline_id',
        )
        cy.wrap(latestQueuedCollection[model].temp_offline_id)
          .should('not.be.undefined')
          .and('not.be.null')
        break
    }
  }
}

const createCollectionQueueUuidTrackerOnly = ({
  shouldHaveData,
  publicationQueueAfter,
}) => {
  cy.log(
    `Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel
    }' to: ${publicationQueueAfter[publicationQueueAfter.length - 1]
      .queuedCollectionIdentifier
    }`,
  )
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] =
    publicationQueueAfter[
      publicationQueueAfter.length - 1
    ].queuedCollectionIdentifier
}

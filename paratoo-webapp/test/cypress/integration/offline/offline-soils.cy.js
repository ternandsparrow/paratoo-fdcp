const { pubOffline } = require("../../support/command-utils")

const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const selectionAndLayoutModule = 'Plot Selection and Layout'
const layoutVisitProtocol = 'Plot Layout and Visit'
const module = 'soils'
const protocolCharacterisation = 'Soil Site Observation and Pit Characterisation'
const protocolBulkDensity = 'Soils - Bulk Density'

//NOTE: turn this on for debugging - will dump the state to a file before adding a
//collection to queue and before publishing the queue at the end
const DUMP_STATE = false

//will be key=<collection_label>, value=<queuedCollectionIdentifier>
let collectionLabelQueueUuids = {}

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })

  it('.should() - prepare for offline collections', () => {
    cy.prepareForOfflineVisit(project)
  })

  it('.should() - go offline', () => {
    cy.window()
      .then(($win) => {
        $win.ephemeralStore.setNetworkOnline(false)
      })
  })
})

let soilPitId = null
describe(`Collect ${module} protocols`, () => {
	describe('Set Plot Context', () => {
		it('.should() - select plot SATFLB0001', () => {
      cy.selectProtocolIsEnabled(selectionAndLayoutModule)
      
      cy.dataCy('locationExisting').click()
      cy.selectFromDropdown('id', 'SATFLB0001')
      cy.nextStep()
      cy.dataCy('visitExisting').click()
      cy.selectFromDropdown('id', 'SA autumn survey', null, null, false, true)

      cy.workflowPublishOffline(
        {
          willNotQueue: true,
          models: ['plot-definition-survey', 'plot-fauna-layout', 'plot-layout', 'plot-visit'],
          plotLabel: 'SATFLB0001',
          plotVisitName: 'SA autumn survey',
          protocolName: layoutVisitProtocol,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'layout',
        },
        createCollectionQueueUuidTrackerOnly,
        null,
        DUMP_STATE ? 'offline_soil_layout' : null,
      )
    })
	})
  describe(`Collect ${protocolCharacterisation}` , () => {
    it(`.should() - navigate to ${protocolCharacterisation}`, () => {
      cy.selectProtocolIsEnabled(module, protocolCharacterisation)
    })

    it('soil pit location', () => {
      cy.soilCharacterisationPitLocation()
        .then((soilPitId_) => {
          cy.log(`soilPitId_=${soilPitId_}`)
          soilPitId = soilPitId_
        })
    })
  
    it('land form element', () => {
      cy.soilCharacterisationLandformElements()
    })
  
    it('land surface phenomena & soil development', () => {
      cy.soilCharacterisationSurfacePhenomenaSoilDevelopment()
    })
  
    it('microrelief', () => {
      cy.soilCharacterisationMicrorelief()
    })
  
    it('erosion observation', () => {
      cy.soilCharacterisationErosionObservation()
    })
  
    it('surface coarse fragment observation', () => {
      cy.soilCharacterisationSurfaceCoarseFragmentObservations()
    })
  
    it('rock outcrop observation', () => {
      cy.soilCharacterisationRockOutcropObservation()
    })
  
    it('soil pit observation', () => {
      cy.soilCharacterisationSoilPitObservation()
    })
  
    it('soil horizon observation', () => {
      cy.soilCharacterisationSoilHorizonObservation()
    })
    it('Australian soil classification', () => {
      cy.soilCharacterisationAustralianSoilClassification()
    })
    it('soil classification', () => {
      cy.soilCharacterisationSoilClassification()
    })

    it(`.should() - submit offline collection for ${protocolCharacterisation} and assert collection was queued correctly`, () => {
      cy.nextStep()
      cy.workflowPublishOffline(
        {
          models: [
            'plot-layout',
            'plot-visit',
            'soil-pit-characterisation-full',
            'soil-landform-element',
            'soil-land-surface-phenomena',
            'microrelief-observation',
            'erosion-observation',
            'surface-coarse-fragments-observation',
            'rock-outcrop-observation',
            'soil-pit-observation',
            'soil-horizon-observation',
            'soil-asc',
            'soil-classification'
          ],
          protocolName: protocolCharacterisation,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'characterisation',
          collectionDependsOn: [
            'layout',
          ],
        },
        assertCharacterisationCb,
        null,
        DUMP_STATE ? 'offline_soil_characterisation' : null,
      )
    })
  })

  describe(`Collect ${protocolBulkDensity}`, () => {
    //don't need to navigate to this protocol as `assertCharacterisationCb` will confirm the re-direct

    it('survey', () => {
      cy.wrap(soilPitId)
        .should('not.be.null')
        .and('not.be.undefined')
      cy.soilBulkDensitySurvey(soilPitId)
    })

    it(`.should() - submit offline collection for ${protocolBulkDensity} and assert collection was queued correctly`, () => {
      cy.nextStep()
      cy.workflowPublishOffline(
        {
          models: [
            'plot-layout',
            'plot-visit',
            'soil-bulk-density-survey'
          ],
          protocolName: protocolBulkDensity,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'bulk_density',
          collectionDependsOn: [
          	'layout',
            'characterisation',
          ],
        },
        assertBulkDensityCb,
        null,
        DUMP_STATE ? 'offline_soil_bulk_density' : null,
      )
    })
  })

  let soilPitLiteId = null
  describe('Collection Soil Sample Pit', () => {
    it('select field survey', () => {
      cy.selectProtocolIsEnabled(module, 'Soil Sample Pit')
      // cy.getAndValidate('survey_variant', { isComponent: true })
      cy.dataCy('Field').click()
      cy.get('[aria-label="Soil sample pit ID *"]')
        .invoke('val')
        .then((id) => {
          soilPitLiteId = id
        })
    })
    it('observers', () => {
      cy.getAndValidate('observers')
  
      cy.addObservers('observerName', [
        'Dio!!!!!!!!!!',
      ])
    })
    it('soil pit', () => {
      cy.dataCy('collection_method').selectFromDropdown(
        'Differential GNSS',
      )
      cy.recordLocation()
  
      cy.dataCy('observation_type').selectFromDropdown('Soil pit')
  
      cy.dataCy('soil_pit_depth').type('5')
  
      cy.dataCy('digging_stopped_by').selectFromDropdown('Rock')
  
      cy.addImages('addImg')
      cy.dataCy('comments').type('Test comment')
      cy.nextStep()
    })
  
    it(`.should() - submit offline collection `, () => {
      cy.workflowPublishOffline(
        {
          models: [
            'plot-layout',
            'plot-visit',
            'soil-pit-characterisation-lite',
          ],
          protocolName: 'Soil Sample Pit',
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'soil_sample_pit',
          collectionDependsOn: [
          	'layout',
          ],
        },
        assertSamplePitCb,
        null,
        DUMP_STATE ? 'offline_soil_sample_pit' : null,
      )
    })
  })

  describe(`Collect ${protocolBulkDensity}`, () => {

    it('survey', () => {
      cy.selectProtocolIsEnabled(module, protocolBulkDensity)
      cy.wrap(soilPitLiteId)
        .should('not.be.null')
        .and('not.be.undefined')
      cy.soilBulkDensitySurvey(soilPitLiteId)
      cy.nextStep()
    })

    it(`.should() - submit offline collection for ${protocolBulkDensity} and assert collection was queued correctly`, () => {
      cy.workflowPublishOffline(
        {
          models: [
            'plot-layout',
            'plot-visit',
            'soil-bulk-density-survey'
          ],
          protocolName: protocolBulkDensity,
          projectName: 'Kitchen Sink TEST Project',
          collectionLabel: 'bulk_density',
          collectionDependsOn: [
          	'layout',
            'characterisation',
          ],
        },
        assertBulkDensityCb,
        null,
        DUMP_STATE ? 'offline_soil_bulk_density' : null,
      )
    })
  })

})

describe('Submit queued offline collections', () => {
  it('.should() - go online', () => {
    cy.window()
      .then(($win) => {
        $win.ephemeralStore.setNetworkOnline(true)
      })
  })

  if (DUMP_STATE) {
    it('.should() - dump state', () => {
      cy.dumpState({
        fileName: 'offline-soils',
      })
    })
  }

  const newCollections = [
    //characterisation
    {
      plotLabel: 'SATFLB0001',
      plotVisitName: 'SA autumn survey',
      plotIsNew: false,
      hasNewFaunaPlot: false,
      visitIsNew: false,
    },
    //bulk density
    {
      plotLabel: 'SATFLB0001',
      plotVisitName: 'SA autumn survey',
      plotIsNew: false,
      hasNewFaunaPlot: false,
      visitIsNew: false,
      soilPitId: soilPitId,
    },
  ]
  const cbs = [
    {
      collectionModel: 'soil-bulk-density-surveys',
      cb: (bulkDensitySurveyResp, collectionsToCheck) => {
        console.log('bulkDensitySurveyResp:', bulkDensitySurveyResp)
        for (const c of collectionsToCheck) {
          console.debug(c)
          if (c.soilPitId) {
            const surveyWithPitId = bulkDensitySurveyResp.body.data.find(
              o => o.attributes?.associated_soil_pit_id?.data?.attributes?.soil_pit_id === c.soilPitId
            )
            console.debug('surveyWithPitId:', surveyWithPitId)
            cy.wrap(surveyWithPitId)
              .should('not.be.undefined')
              .and('not.be.null')
          }
        }
      },
    },
  ]
  pubOffline({
    collectionsToCheck: newCollections,
    collectionIterCbs: cbs,
  })
})

const assertCharacterisationCb = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
}) => {
	// check the button takes you to soil density protocol
  cy.get('[data-autofocus="true"] > .q-btn__content').click()
  cy.dataCy('title')
    .eq(0)
    .should('contain.text', 'Soil Bulk Density Survey')

  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
    .should('eq', 1)

  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

  const collectionModels = Object.keys(latestQueuedCollection)
  console.log('collectionModels:', collectionModels)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch (model) {
      case 'plot-layout':
      case 'plot-visit':
        cy.wrap(latestQueuedCollection[model])
          .should('have.any.keys', 'id')
        if (latestQueuedCollection[model].id) {
          cy.wrap(latestQueuedCollection[model].id)
            .should('not.be.null')
            .and('not.be.undefined')
        }
        break
      //don't need to check rest of models; this one has temp ID and rest are just
      //collection data
      case 'soil-pit-characterisation-full':
        cy.wrap(latestQueuedCollection[model])
          .should('have.any.keys', 'temp_offline_id')
        break
    }
  }
}
const assertSamplePitCb = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
}) => {
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
    .should('eq', 1)

  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection

  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

  const collectionModels = Object.keys(latestQueuedCollection)
  console.log('collectionModels:', collectionModels)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch (model) {
      case 'plot-layout':
      case 'plot-visit':
        cy.wrap(latestQueuedCollection[model])
          .should('have.any.keys', 'id')
        if (latestQueuedCollection[model].id) {
          cy.wrap(latestQueuedCollection[model].id)
            .should('not.be.null')
            .and('not.be.undefined')
        }
        break
      case 'soil-pit-characterisation-lite':
        cy.wrap(latestQueuedCollection[model])
          .should('have.any.keys', 'temp_offline_id')
        break
    }
  }
}

const assertBulkDensityCb = ({
  shouldHaveData,
  publicationQueueBefore,
  publicationQueueAfter,
}) => {
  cy.wrap(publicationQueueAfter.length - publicationQueueBefore.length)
    .should('eq', 1)

  const latestQueuedCollection = publicationQueueAfter[publicationQueueAfter.length-1].collection
  console.log('latestQueuedCollection: ', latestQueuedCollection)

  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier

  const collectionModels = Object.keys(latestQueuedCollection)
  console.log('collectionModels:', collectionModels)
  cy.wrap(
    shouldHaveData.models.every(o => collectionModels.includes(o))
  ).should('be.true')

  for (const model of shouldHaveData.models) {
    console.log(`${model}:`, latestQueuedCollection[model])
    switch(model) {
      case 'plot-layout':
      case 'plot-visit':
        cy.wrap(latestQueuedCollection[model])
          .should('have.any.keys', 'id')
        if (latestQueuedCollection[model].id) {
          cy.wrap(latestQueuedCollection[model].id)
            .should('not.be.null')
            .and('not.be.undefined')
        }
        break
      case 'soil-bulk-density-survey':
        cy.wrap(latestQueuedCollection[model])
          .should('have.any.keys', 'associated_soil_pit_id')
        if (latestQueuedCollection[model].associated_soil_pit_id) {
          cy.wrap(latestQueuedCollection[model].associated_soil_pit_id)
            .should('have.any.keys', 'temp_offline_id')
        }
        break
    }
  }
}

const createCollectionQueueUuidTrackerOnly = ({
  shouldHaveData,
  publicationQueueAfter,
}) => {
  cy.log(`Setting queued collection identifier for collection label '${shouldHaveData.collectionLabel}' to: ${publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier}`)
  collectionLabelQueueUuids[shouldHaveData.collectionLabel] = publicationQueueAfter[publicationQueueAfter.length-1].queuedCollectionIdentifier
}

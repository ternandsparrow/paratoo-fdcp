const { pub } = require('../../support/command-utils')

const protocol = 'Camera Trap Deployment'
const protocolCy = 'Camera\\ Trap\\ Deployment'
const projectCy = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'camera\\ trapping'

global.ignorePaths = ['camera-trap-deployment-point.camera_location.name']


before(() => {
  cy.mockLocation(-23.7, 132.8)
})


describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
  it('getValidationRules', () => {
    cy.documentation().currentProtocolWorkflow((models) => {
      global.currentProtRules = models
    })
  })
})

//`data-manager-historical-resolve` tests fauna plots' ability to be resolved, so no
//point also generally testing that survey type here

describe(`Collect ${protocol} ('Array-grid' survey type)`, () => {
  it('.should() - complete survey step (start)', () => {
    cy.cameraTrapDeploymentSurveyPlannedPointsJSON()
    cy.nextStep()
  })

  it('.should() - collect Camera Trap Deployment Point', () => {
    const earth = 6378.137
    const pi = Math.PI
    const m = 1 / (((2 * pi) / 360) * earth) / 1000

    cy.window()
      .then(($win) => {
        const coords = {
          lat: -33.829352994452101 + 10 * m,
          lng:
            139.095518342932991 +
            (10 * m) / Math.cos(-33.829352994452101 * (pi / 180)),
        }
        cy.log(`Setting coords to: ${JSON.stringify(coords)}`)
        $win.ephemeralStore.updateUserCoords(coords, true)
      })
      .then(() => {
        cy.dataCy('mark_closest_camera_point').click()
      })

    cy.dataCy('camera_trap_point_id').should('have.value', 'CTP001')
    //to prevent future collisions of deployment_id (which is derived from point_id),
    //modify the value from the uploaded file
    const ctPointIdToUse = `CTP${String(Math.floor(Math.random() * 10000))}`
    cy.dataCy('camera_trap_point_id').clear().type(ctPointIdToUse)

    cy.doDeploymentStartDateAndDeploymentId(ctPointIdToUse)

    cy.dataCy('distance_to_closest_point').should('contain.value', 14)

    //target taxa
    // cy.dataCy('target_taxa_types_1_expansion').click()    //opens expansion item
    cy.setExpansionItemState('target_taxa_types_1_expansion', 'open')
    cy.getAndValidate('taxa_type', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('Bird')
    cy.dataCy('done').eq(0).click().wait(1000)
    cy.dataCy('addEntry').eq(0).click().wait(1000)
    cy.getAndValidate('taxa_type', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('Mammal')
    cy.dataCy('done').eq(0).click().wait(1000)

    //target species
    // cy.dataCy('target_species_1_expansion').click()    //opens expansion item
    cy.setExpansionItemState('target_species_1_expansion', 'open')
    //testing species empty field
    cy.getAndValidate('species', { testEmptyField: ['done', 0] })
      .selectFromSpeciesList('emu', false)
      .type('{esc}')
    cy.dataCy('done').eq(0).click().wait(1000)
    cy.dataCy('addEntry').eq(1).click().wait(1000)
    cy.dataCy('species').selectFromSpeciesList('frog', false).type('{esc}')
    cy.dataCy('done').eq(0).click().wait(1000)

    cy.getAndValidate('camera_trap_mount', {
      testEmptyField: ['done', -1],
    }).type('custom CT mount')
  })
  it(".should() - collect Camera Trap Deployment Points' Feature(s)", () => {
    cy.cameraTrapDeploymentPoint1Features()
  })
  it(".should() - collect Camera Trap Deployment Points' Camera Information", () => {
    cy.cameraTrapDeploymentPoint1CameraInfo()
  })
  it(".should() - collect Camera Trap Deployment Points' Camera Settings", () => {
    cy.cameraTrapDeploymentPoint1CameraSettings()
  })
  it('.should() - continue collecting 1st Camera Trap Deployment Point', () => {
    cy.cameraTrapDeploymentPoint1Pt2()
    cy.nextStep()
  })

  // it('.should() - publish the collection', () => {
  //   cy.workflowPublish(false)
  // })
  pub()
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
})

describe(`Collect ${protocol} ('Array-transect' survey type)`, () => {
  it('.should() - complete survey step (start)', () => {
    cy.cameraTrapDeploymentSurveyPlannedPointsCSV()
    cy.nextStep()
  })

  it('.should() - collect Camera Trap Deployment Point', () => {
    const earth = 6378.137
    const pi = Math.PI
    const m = 1 / (((2 * pi) / 360) * earth) / 1000

    cy.window()
      .then(($win) => {
        const coords = {
          lat: -33.829352994452101 + 10 * m,
          lng:
            139.095518342932991 +
            (10 * m) / Math.cos(-33.829352994452101 * (pi / 180)),
        }
        cy.log(`Setting coords to: ${JSON.stringify(coords)}`)
        $win.ephemeralStore.updateUserCoords(coords, true)
      })
      .then(() => {
        cy.dataCy('mark_closest_camera_point').click()
      })
      const ctPointIdToUse = `CTP${String(Math.floor(Math.random() * 10000))}`
      cy.dataCy('camera_trap_point_id').clear().type(ctPointIdToUse)
    cy.customSelectDate({
      fieldName: 'deployment_start_date',
      date: {
        year: 'current',
        month: 'current',
        day: 'current',
      },
    })

    cy.dataCy('distance_to_closest_point').should('contain.value', 14)

    //target taxa
    // cy.dataCy('target_taxa_types_1_expansion').click()    //opens expansion item
    cy.setExpansionItemState('target_taxa_types_1_expansion', 'open')
    cy.getAndValidate('taxa_type', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('Bird')
    cy.dataCy('done').eq(0).click().wait(1000)
    cy.dataCy('addEntry').eq(0).click().wait(1000)
    cy.getAndValidate('taxa_type', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('Mammal')
    cy.dataCy('done').eq(0).click().wait(1000)

    //target species
    // cy.dataCy('target_species_1_expansion').click()    //opens expansion item
    cy.setExpansionItemState('target_species_1_expansion', 'open')
    //testing species empty field
    cy.getAndValidate('species', { testEmptyField: ['done', 0] })
      .selectFromSpeciesList('emu', false)
      .type('{esc}')
    cy.dataCy('done').eq(0).click().wait(1000)
    cy.dataCy('addEntry').eq(1).click().wait(1000)
    cy.dataCy('species').selectFromSpeciesList('frog', false).type('{esc}')
    cy.dataCy('done').eq(0).click().wait(1000)

    cy.getAndValidate('camera_trap_mount', {
      testEmptyField: ['done', -1],
    }).type('custom CT mount')
  })
  it(".should() - collect Camera Trap Deployment Points' Feature(s)", () => {
    cy.cameraTrapDeploymentPoint1Features()
  })
  it(".should() - collect Camera Trap Deployment Points' Camera Information", () => {
    cy.cameraTrapDeploymentPoint1CameraInfo()
  })
  it(".should() - collect Camera Trap Deployment Points' Camera Settings", () => {
    cy.cameraTrapDeploymentPoint1CameraSettings()
  })
  it('.should() - continue collecting 1st Camera Trap Deployment Point', () => {
    cy.cameraTrapDeploymentPoint1Pt2()
    cy.nextStep()
  })

  // it('.should() - publish the collection', () => {
  //   cy.workflowPublish(false)
  // })
  pub()
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocolCy)
  })
})

describe(`Collect ${protocol} ('point' survey type)`, () => {
  it('.should() - complete survey step (start)', () => {
    cy.cameraTrapDeploymentSurvey()
    cy.nextStep()
  })
  //point 1
  it('.should() - collect 1st Camera Trap Deployment Point', () => {
    cy.cameraTrapDeploymentPoint1Pt1()
  })
  it(".should() - collect 1st Camera Trap Deployment Points' Feature(s)", () => {
    cy.cameraTrapDeploymentPoint1Features()
  })
  it(".should() - collect 1st Camera Trap Deployment Points' Camera Information", () => {
    cy.cameraTrapDeploymentPoint1CameraInfo()
  })
  it(".should() - collect 1st Camera Trap Deployment Points' Camera Settings", () => {
    cy.cameraTrapDeploymentPoint1CameraSettings()
  })
  it('.should() - continue collecting 1st Camera Trap Deployment Point', () => {
    cy.cameraTrapDeploymentPoint1Pt2()
  })
  //point 2
  it('.should() - collect 2nd Camera Trap Deployment Point', () => {
    cy.cameraTrapDeploymentPoint2Pt1()
  })
  it(".should() - collect 2nd Camera Trap Deployment Points' Feature(s)", () => {
    cy.cameraTrapDeploymentPoint2Features()
  })
  it(".should() - collect 2nd Camera Trap Deployment Points' Camera Information", () => {
    cy.cameraTrapDeploymentPoint2CameraInfo()
  })
  it(".should() - collect 2nd Camera Trap Deployment Points' Camera Settings", () => {
    cy.cameraTrapDeploymentPoint2CameraSettings()
  })
  it('.should() - continue collecting 2nd Camera Trap Deployment Point', () => {
    cy.cameraTrapDeploymentPoint2Pt2()
  })
  //point 3
  it('.should() - collect 3rd Camera Trap Deployment Point', () => {
    cy.cameraTrapDeploymentPoint3Pt1()
  })
  it(".should() - collect 3rd Camera Trap Deployment Points' Feature(s)", () => {
    cy.cameraTrapDeploymentPoint3Features()
  })
  it(".should() - collect 3rdCamera Trap Deployment Points' Camera Information", () => {
    cy.cameraTrapDeploymentPoint3CameraInfo()
  })
  it(".should() - collect 3rd Camera Trap Deployment Points' Camera Settings", () => {
    cy.cameraTrapDeploymentPoint3CameraSettings()
  })
  it('.should() - continue collecting 3rd Camera Trap Deployment Point', () => {
    cy.cameraTrapDeploymentPoint3Pt2()
  })
  it('.should() - complete survey step (end)', () => {
    cy.nextStep()
  })
  // it('.should() - publish the collection', () => {
  //   cy.workflowPublish(false)
  // })
  pub()
})

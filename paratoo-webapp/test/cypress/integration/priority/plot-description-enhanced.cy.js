const { pub } = require("../../support/command-utils")

const protocol = 'Plot Description - Enhanced'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'plot description'

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - start collection button should be disabled', () => {
    cy.selectProtocolIsDisabled(module, protocol)
  })
  it('.should() - do plot layout', () => {
    cy.plotLayout()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})

describe('Plot Location', () => {
  it('[validate-field] - slope degree', () => {
    cy.getAndValidate('slope', { testEmptyField: ['workflowNextButton'] })
  })

  it('[test-to-pass] - auto fill Slope (class)', () => {
    const degreeToClass = {
      //lower bounds
      0: 'Level',
      0.6: 'Very gently inclined',
      1.77: 'Gently inclined',
      5.77: 'Moderately inclined',
      18: 'Steep',
      30: 'Very steep',
      45: 'Precipitous',
      72: 'Cliffed',
      //upper bounds
      0.59: 'Level',
      1.76: 'Very gently inclined',
      5.76: 'Gently inclined',
      17.99: 'Moderately inclined',
      29.99: 'Steep',
      44.99: 'Very steep',
      71.99: 'Precipitous',
      90: 'Cliffed',    //LUT can go above, but this field max 90
    }
    for (const [degree, slopeClass] of Object.entries(degreeToClass)) {
      cy.dataCy('slope').clear().type(degree)
      cy.get('[data-cy="slope_class"]').should('contain.text', slopeClass)
    }
  })

  it('[validate-field] aspect', () => {
    cy.dataCy('slope_not_uniform').click()
    cy.getAndValidate('aspect')
    cy.dataCy('slope_not_uniform').click()
    cy.getAndValidate('aspect').type('1')
  })

  it('[validate-field] Relief', () => {
    cy.getAndValidate('relief', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('Very high (>300 m)')
  })

  it('[validate-field] modal slope', () => {
    cy.getAndValidate('modal_slope', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('Very steep')
  })

  it('[validate-field] landform pattern', () => {
    cy.getAndValidate('landform_pattern', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('Hills', 'Hills')
  })

  it('[validate-field] landform element', () => {
    cy.getAndValidate('landform_element', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('Cliff', 'Cliff')
  })

  it('[validate-field]select outcrop lithology', () => {
    cy.getAndValidate('outcrop_lithology', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('Andesite', 'Andesite')
  })

  it('[validate-field]collect coarse fragments', () => {
    cy.getAndValidate('coarse_fragments', {
      isComponent: true,
      testEmptyField: ['workflowNextButton'],
    })
    cy.getAndValidate('lithology').selectFromDropdown()
    cy.getAndValidate('abundance').selectFromDropdown()
    cy.getAndValidate('size').selectFromDropdown()
    cy.dataCy('done').first().click()
  })

  it('[validate-field]collect land-use history', () => {
    cy.getAndValidate('land_use_history', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown()
  })

  it('[validate-field] veg growth stage', () => {
    cy.getAndValidate('veg_growth_stage', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('Uneven Age', 'Uneven Age')
  })

  it('[test-to-pass] fire history', () => {
    cy.get('[data-cy="Unknown"]') //fire history is auto-selected
      //https://filiphric.com/cypress-basics-check-attributes-value-and-text#get-attribute
      .invoke('attr', 'aria-checked')
      .should('eq', 'true')
    cy.dataCy('Known')
      .click()
      .invoke('attr', 'aria-checked')
      .should('eq', 'true')
    // cy.testToFailValues('year_of_fire', {
    //   'Thisisnotayear': {chainer: 'contain.text', value: 'Invalid year format'},
    // })
    cy.dataCy('year_of_fire').click().type('2023')
  })

  it('[validate-field] structural formation', () => {
    cy.getAndValidate('structural_formation', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown('Heath Shrubs', 'Heath Shrubs')
  })

  it('[validate-field] hemogenity measure (m)', () => {
    cy.getAndValidate('homogeneity_measure', {
      testEmptyField: ['workflowNextButton'],
    }).type('1')
  })

  it('[validate-field] plant_species_life_stage', () => {
    cy.getAndValidate('plant_species_life_stage', {
      isComponent: true,
      testEmptyField: ['workflowNextButton'],
    })
    cy.getAndValidate('species').selectFromSpeciesList('Plantae')
    cy.getAndValidate('life_stage').selectFromDropdown()
    cy.dataCy('done').last().click()
  })

  it('[validate-field] disturbance', () => {
    cy.getAndValidate('site_disturbance', {
      testEmptyField: ['workflowNextButton'],
    }).selectFromDropdown()
  })

  it('[validate-field] introduced_plant_species_impact', () => {
    cy.getAndValidate('introduced_plant_species_impact', {
      testEmptyField: ['workflowNextButton'],
    })
  })

  it('[validate-field]fill in comment', () => {
    cy.getAndValidate('comments', {
      testEmptyField: ['workflowNextButton'],
    }).type(
      'This is a comment for landform and surface comment.{enter}This is another line.',
    )
  })
})

describe('Publish', () => {
  it('.should() - successful publish', () => {
    cy.nextStep()
  })
  pub()
})

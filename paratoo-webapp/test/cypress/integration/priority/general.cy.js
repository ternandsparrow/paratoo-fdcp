const projectCy = 'Kitchen\\ Sink\\ TEST\\ Project'


// before(() => {
//     // cy.setCookie('loginBgImg', 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Opossum_2.jpg/1280px-Opossum_2.jpg')
// })

describe('Landing', () => {
    it('.should() - login', () => {
        cy.login('TestUser', 'password')
    })
})


describe('loadManyStores', () => {
  it('should loadManyStores', () => {
    cy.loadManyStores(
      [
        {
          storeName: 'auth',
          callback: (auth => {
            return auth.projAndProt
          }) 
        },
        {
          storeName: 'apiModels',
        },
      ],
    ).then(({authStore, apiModelsStore}) => {
      cy.log(authStore)
      cy.log(apiModelsStore)
      // apiModels
    })
  })
})

// test ui buttons, profile
describe('Profile', () => {
    it('navigate', () => {
        cy.get('[aria-label="Menu"]').click() // open sidebar
        cy.dataCy('essentialLink-Profile').click()

        cy.get('h5').contains('Your profile').should('exist')
        cy.get('.q-table').contains('Username').siblings().contains('TestUser').should('exist')

        // cy.get('[aria-label="Toggle Dark Mode"]').toggle(false)
        // cy.get('[aria-label="Toggle Dark Mode"]').toggle()
        // TODO assert color scheme should change

        // cy.get('[role="listitem"]').contains('Show field help').parents('.q-item').find('.q-toggle').as('show_hide_fields')
        // cy.get('@show_hide_fields').toggle(false)
        // cy.get('@show_hide_fields').toggle()
        // TODO assert this by entering a protocol and checking 
    })
})


describe('Preferences', () => {
    it('navigate', () => {
        cy.get('[aria-label="Menu"]').click() // open sidebar
        cy.dataCy('essentialLink-Preferences').click()

        cy.get('[aria-label="Toggle Dark Mode"]').toggle(false)
        cy.get('[aria-label="Toggle Dark Mode"]').toggle()
        // TODO assert color scheme should change

        cy.get('.q-toggle').contains('Show LUTs').parents('.q-toggle').as('show_hide_fields')
        cy.get('@show_hide_fields').toggle(false)
        cy.get('@show_hide_fields').toggle()
        // TODO assert this by entering a protocol and checking 


        cy.get('.q-toggle').contains('Vibrate Mode').parents('.q-toggle').as('toggle_vibrate')
        cy.get('@toggle_vibrate').toggle()
        cy.get('.q-field')
        cy.get('.q-field').should('have.attr', 'aria-disabled', 'true')
        cy.get('@toggle_vibrate').toggle(false)
        cy.get('.q-field').should('not.have.attr', 'aria-disabled')
        //TODO ¯\_(ツ)_/¯ 


        cy.get('.q-field').selectFromDropdown('blip-131856.mp3')
    })
})

describe('Home', () => {
    it('navigate', () => {
        cy.get('[aria-label="Menu"]').click() // open sidebar
        cy.dataCy('essentialLink-Home').click()
        cy.get('p').contains('Welcome to Monitor, the field data collection app for the Ecological Monitoring System Australia.').should('exist')
    })
})

describe('Profile Dropdown', () => {
    it('navigate', () => {
        cy.dataCy('username').click()
        cy.get('[role="menu"]').within(() => {
            cy.get('[role="listitem"]').eq(0).click() //'profile'
        })
        cy.get('h5').contains('Your profile').should('exist')
        cy.dataCy('username').click()
        cy.get('[role="menu"]').within(() => {
            cy.get('[role="listitem"]').eq(0).should('have.class', 'q-router-link--active')
            cy.get('[role="listitem"]').eq(1).find('.q-toggle').toggle(false) // 'toggle dark mode'
            cy.get('.q-list').should('not.have.class', '.q-list--dark')
            cy.get('[role="listitem"]').eq(1).find('.q-toggle').toggle(true)

            cy.get('[role="listitem"]').eq(3).click() // 'logout'
        })



    })
})
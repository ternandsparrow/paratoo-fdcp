const { pub } = require("../../support/command-utils")

import {
  cloneDeep,
} from 'lodash'

const protocol = 'Plot Selection'
const protocolCy = 'Plot\\ Selection'
const projectCy = 'Project\\ Area\\ \\&\\ Plot\\ Selection'
const project = 'Project Area & Plot Selection'
const module = 'plot\\ selection\\ and\\ layout'

// TODO instead of ignoring, handle properly (we send the project areas in the survey, but it's a separate bit of data in the response; same level as the survey)
global.ignorePaths = ['plot-selection-survey.project-areas']

// TODO the remaining 16% of plotSelection.vue coverage is due to merit stuff that cant be run locally

//TODO make this a command (grabbed from `plot-context` test)
function resetState() {
  cy.window()
    .then(($win) => {
      $win.dataManager.$reset()
      $win.bulkStore.$reset()
    })
}

let rules
describe('0 - Landing', () => {
  it('.should() - login', () => {
    //MUST use this user, as they're the only ones who can modify the project schema
    cy.login('ProjectAdmin', 'paratoo')
  })
  it('clean-up project areas', () => {
    let didChange = false
    cy.doRequest('org/user-projects', {
      host: 'org',
    })
      .then((res) => {
        const projIds = res.body.projects
          .filter(
            (project) =>
              [
                'Project Area & Plot Selection',
                'Cypress TEST Project',
              ].includes(project.name) &&
              project.project_area.coordinates.length > 0,
          )
          .map((project) => project.id)
        return projIds
      })
      .then((projIds) => {
        if (projIds.length > 0) {
          projIds.forEach((id) => {
            cy.doRequest('projects', {
              method: 'PUT',
              host: 'org',
              id: id,
              body: {
                data: {
                  project_area_coordinates: [],
                  // project_area_type: ''
                },
              },
              failOnStatusCode: true,
            }).then(() => {
              didChange = true
            })
          })
        }
      })
      .then(() => {
        cy.log('didChange', didChange)
        if (didChange) {
          // cy.visit('/projects')
          cy.get('body').contains('Refresh Projects').click().wait(15000)
          // cy.pullAllApiModels()
          // cy.get('[style="margin-left: -1px;"] > .q-btn__content > .block').click()
        }
      })
  })
  it('clean-up reserved plots', () => {
    cy.doRequest('reserved-plot-labels', {
      method: 'GET'
    })
      .then((reservedPlotLabelsResp) => {
        for (const reservedPlot of reservedPlotLabelsResp?.body?.data || []) {
          if (reservedPlot?.attributes?.plot_label?.includes('SATRAFLB')) {
            cy.doRequest('reserved-plot-labels', {
              method: 'DELETE',
              id: reservedPlot.id
            })
          }
        }
      })
  })
})

describe('Core goes down halfway through collecting protocol', () => {
  it('make sure in kitchen sink project', () => {
    cy.get('body')
      .then(($body) => {
        const projectAlreadySelected = $body.find('[data-cy="changeSelection"]').length > 0
        cy.log(`projectAlreadySelected? ${projectAlreadySelected}`)
        if (!projectAlreadySelected) {
          cy.dataCy('Kitchen\\ Sink\\ TEST\\ Project').click()
        } else {
          const projectIsKitchenSink = $body.find('Kitchen Sink TEST Project').length > 0
          cy.log(`projectIsKitchenSink? ${projectIsKitchenSink}`)
          if (!projectIsKitchenSink) {
            cy.dataCy('changeSelection').click()
            cy.dataCy('Kitchen\\ Sink\\ TEST\\ Project').click()
          }
        }
      })
  })
  it('try to do plot selection with core going down before submit', () => {
    // is disabled when the app thinks core is down. wait for the core ping to know the core is up. 

    cy.dataCy('Plot\\ Selection\\ and\\ Layout').click()
    cy.dataCy('Plot\\ Selection').click()
    cy.dataCy('relevantProjects').selectFromDropdown('Kitchen Sink TEST Project')
    cy.nextStep()


    // cy.get('.q-notification.bg-negative span.block:contains(Dismiss)').eq(0).click()
    
    cy.intercept(/.*:1337.*/, (req) => {
      req.destroy()
    }).as('destroyedReq')
    
    cy.selectFromDropdown('state', 'South Australia')
    cy.selectFromDropdown('program', 'Training')
    cy.selectFromDropdown('bioregion', 'Flinders Lofty Block')
    cy.get('[data-cy="unique_digits"]').click().type('0001').wait(100)
    

    cy.dataCy('projects_for_plot')
      .selectFromDropdown([
        'Kitchen Sink TEST Project',
      ])

    cy.dataCy('recommended_location_point')
      .selectFromDropdown('South West')

    cy.recordLocation({
      coords: {
        lat: -34.95,
        lng: 138.62,
      }
    })

    cy.intercept(/.*:1337\/_health*/, (req) => {
      req.destroy()
    }).as('healthReq')

    // will try to do a plot check and after retrying a few times a dialog should appear and redirect you to the projects screen
    cy.dataCy('done').click()
    cy.log('Blocking core requests. Wait for the app to health check and enter offline state.')
    // Request failed to populate reserved plot labels Core is unavailable
    // cy.wait(500)
    // this healthReq is how the app knows that core is down and will enter an offline mode
    cy.wait('@healthReq', {timeout: 61000})

    cy.get('.q-card', {timeout: 120000})
      .should('be.visible')
      .and('contain.text', 'Monitor\'s server appears to be unavailable, it may be undergoing maintenance')
    cy.get('.q-notification', {timeout: 120000}).should('contain.text', 'Monitor\'s server is currently unavailable')
    cy.wait(100)
    cy.get('.q-card span.block:contains(Exit)').click()
  })

  // it('wait for app to ping core (minute wait)', () => {
  //   cy.get('.q-notification.bg-negative span.block:contains(Dismiss)').eq(0).click()
  //   cy.dataCy('prepareForOfflineVisitBtn', {timeout: 61000}).should('not.have.class', 'disabled')
  //   cy.dataCy('Plot\\ Selection\\ and\\ Layout').click()
  //   cy.dataCy('clearCollection_plot_selection').click()
  //   cy.dataCy('confirmClearCollection').click()
  //   cy.dataCy('closeDialog').click()
  // })
})

describe('Attempting to collect with core down', () => {
  // will kill every single core request the app makes, mimicking core being down as its unable to send anything back
  beforeEach(() => {
    cy.intercept(/.*:1337.*/, (req) => {
      req.destroy()
    }).as('destroyedReq')
  })

  it('set projects selection', () => {
    cy.get('body')
      .then(($body) => {
        const projectAlreadySelected = $body.find('[data-cy="changeSelection"]').length > 0
        cy.log(`projectAlreadySelected? ${projectAlreadySelected}`)
        if (!projectAlreadySelected) {
          cy.dataCy('Kitchen\\ Sink\\ TEST\\ Project').click()
        } else {
          const projectIsKitchenSink = $body.find('Kitchen Sink TEST Project').length > 0
          cy.log(`projectIsKitchenSink? ${projectIsKitchenSink}`)
          if (!projectIsKitchenSink) {
            cy.dataCy('changeSelection').click()
            cy.dataCy('Kitchen\\ Sink\\ TEST\\ Project').click()
          }
        }
      })
  })
  it('try to open plot selection (will have network errors)', () => {
    cy.get('.q-notification.bg-negative span.block:contains(Dismiss)').eq(0).click()
    cy.dataCy('Plot\\ Selection\\ and\\ Layout').click()
    // cy.dataCy('clearCollection_plot_selection').click()
    // cy.dataCy('confirmClearCollection').click() 
    cy.dataCy('edit').click()
    // cy.dataCy('Plot\\ Selection').click()
    // cy.get('.q-card', {timeout: 120000}).should('contain.text', 'Monitor\'s server appears to be unavailable, it may be undergoing maintenance')
    cy.get('.q-notification', {timeout: 120000}).should('contain.text', 'Monitor\'s server is currently unavailable')
    cy.wait(100)
    // cy.get('.q-card span.block:contains(Exit)').click()
    cy.dataCy('closeDialog').click()
  })

  // it('wait for app to ping core and come back online (minute wait)', () => {
  // })
  
  it('clear selection', () => {
    cy.intercept(/.*:1337.*/, (req) => {
      req.continue()
    }).as('continuedReq')

    cy.dataCy('prepareForOfflineVisitBtn', {timeout: 62000}).should('not.have.class', 'disabled')
    // cy.get('.q-notification.bg-negative span.block:contains(Dismiss)').click({multiple: true})
    
    cy.dataCy('Plot\\ Selection\\ and\\ Layout').click()
    cy.dataCy('clearCollection_plot_selection').click()
    cy.dataCy('confirmClearCollection').click()
    cy.dataCy('closeDialog').click()
    // cy.dataCy('Plot\\ Selection').click().wait(100)
    
    // cy.get('.q-notification').should('contain.text', 'Monitor\'s server appears to be unavailable, it may be undergoing maintenance')
    // cy.get('.q-notification.bg-negative span.block:contains(Dismiss)').eq(0).click()
  })

})



describe('Block collecting when no network', () => {
  it('go offline', () => {
    cy.goOffline()
  })
  it('set projects selection', () => {
    cy.get('body')
      .then(($body) => {
        const projectAlreadySelected = $body.find('[data-cy="changeSelection"]').length > 0
        cy.log(`projectAlreadySelected? ${projectAlreadySelected}`)
        if (!projectAlreadySelected) {
          cy.dataCy('Kitchen\\ Sink\\ TEST\\ Project').click()
        } else {
          const projectIsKitchenSink = $body.find('Kitchen Sink TEST Project').length > 0
          cy.log(`projectIsKitchenSink? ${projectIsKitchenSink}`)
          if (!projectIsKitchenSink) {
            cy.dataCy('changeSelection').click()
            cy.dataCy('Kitchen\\ Sink\\ TEST\\ Project').click()
          }
        }
      })
  })
  it('[test-to-fail] block collecting plot selection', () => {
    cy.dataCy('Plot\\ Selection\\ and\\ Layout').click()
    cy.dataCy('Plot\\ Selection').click()
    cy.get('.q-notification').should('contain.text', 'Plot Selection is a desktop protocol that requires a stable internet connection Please check your connection and try again')
      .then(() => {
        cy.dataCy('closeDialog').click()
      })
  })
  it('go online', () => {
    cy.dismissPopups()
    cy.goOnline()
  })
})

describe('Exit protocol when network drops mid-collection', () => {
  it('enter protocol', () => {
    cy.selectProtocolIsEnabled(
      'Plot Selection and Layout',
      'Plot Selection',
      null,
      {
        count: 1,
      },
    )
  })
  it('go offline', () => {
    cy.goOffline()
  })
  it('check dialog appears', () => {
    cy.log('todo')
    cy.get('.q-dialog-plugin')
      .should('exist')
    cy.get('.q-dialog__title')
      .should('contain.text', 'Your device appears to have gone offline')
    //exit button should go back to projects
    cy.get('.q-btn').contains('Exit').click()
    cy.location('pathname').should('eq', '/projects')
  })
  it('go online', () => {
    cy.dismissPopups()
    cy.goOnline()
  })
})

describe('1 - collect plot selection with project area', () => {
  it('reset project selection', () => {
    const projectAlreadySelected = !Cypress._.isEmpty(
      cy.get(`[data-cy="changeSelection"]`)
    )
    cy.log(`projectAlreadySelected? ${projectAlreadySelected}`)
    if (projectAlreadySelected) {
      cy.dataCy('changeSelection').click()
    }
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    //protocol should be assigned to kitchen sink, but test with specific project anyway
    cy.selectProtocolIsEnabled(null, protocolCy, projectCy)
  })
  it('1.1 -[test-to-fail] when submitting without project area', () => {
    //no project selected so it will error
    cy.testEmptyField(
      'Please specify at least one project',
      'workflowNextButton',
    )
  })
  it('1.2 -[test-to-pass] - select a project with a project area', () => {
    cy.window()
      .then(($win) => {
        $win.ephemeralStore.updateUserCoords({ lat: -23.7, lng: 132.8 }, true)
      })
      .then(() => {
        //select a project with a project area - the `confirmProjects` button doesn't
        //show so
        //that the user can click `workflowNextButton` to continue
        cy.selectFromDropdown('relevantProjects', 'Kitchen Sink TEST Project')
        // cy.dataCy('relevantProjects').click() //close the dropdown
        cy.dataCy('relevantProjects').wait(1000).type('{esc}').wait(1000) //close the dropdown
        cy.dataCy('confirmProjects').should('not.exist')
        cy.nextStep()
      })
  })
})

describe('2 - select project without project area', () => {
  it('2.1 -[test-to-fail] - select "Project Area & Plot Selection", failed when going to next step', () => {
    cy.dataCy('workflowBackButton').click() //TODO should probably be a command like `nextStep`

    //select a project w/o a project area
    cy.selectFromDropdown('relevantProjects', 'Project Area & Plot Selection')
    // cy.dataCy('relevantProjects').click() //close the dropdown
    cy.dataCy('relevantProjects').wait(1000).type('{esc}').wait(1000) //close the dropdown
    cy.dataCy('confirmProjects').should(
      'contain.text',
      'Define New project areas for Selected Project(s)',
    )
    cy.testEmptyField(
      'You have missing project areas for at least one of your selected projects',
      'workflowNextButton',
    )
  })
  it('2.2 - [test-to-fail] - select "Cypress TEST Project", failed when going to next step', () => {
    // //now we also select a project without a project area; the button stays the same
    cy.selectFromDropdown('relevantProjects', 'Cypress TEST Project')
    // cy.dataCy('relevantProjects').click() //close the dropdown
    cy.dataCy('relevantProjects').wait(1000).type('{esc}').wait(1000) //close the dropdown
    cy.dataCy('confirmProjects').should(
      'contain.text',
      'Define New project areas for Selected Project(s)',
    )
    cy.testEmptyField(
      'You have missing project areas for at least one of your selected projects',
      'workflowNextButton',
    )
    //also check the model value of the dropdown has all selected projects
    cy.get('[data-cy="relevantProjects"]')
      //selected value(s) are stored in the inner span, which is just a string, so we
      //join the list of options into a string
      //https://stackoverflow.com/a/73205844
      .find('span')
      .should(
        'have.text',
        [
          'Kitchen Sink TEST Project',
          'Project Area & Plot Selection',
          'Cypress TEST Project',
        ].join(', '),
      )
  })
  it('2.3 - [test-to-pass]define project area for "Project Area & Plot Selection"', () => {
    cy.window()
      .then(($win) => {
        //force coords to consistent place so that we don't have non-deterministic
        //failures when location is different during tests
        $win.ephemeralStore.updateUserCoords(
          {
            lat: -34.95,
            lng: 138.62,
          },
          true,
        )
      })
      .then(() => {
        //TODO test deleting points
        //TODO test moving points (when implemented - see TODO in code of `PolygonMap`)
        //define 1st project area
        cy.get('[data-cy="confirmProjects"]').click()
        cy.get(`[data-cy="projectAreasToDefineOptions"]`)
          .children()
          .eq(0)
          .should('have.text', 'Project Area & Plot Selection')
          .click() //opens map interface
        //point 1
        cy.get('[data-cy="polygonMap"]').dragMapFromCenter({
          // Go 1/6 of map container width to the right (negative direction)
          xMoveFactor: -1 / 6,
          // Go 1/3 of map container height to the top (positive direction)
          yMoveFactor: 1 / 3,
        })
        cy.get('[data-cy="addVertex"]').click()
        cy.get('[data-cy="pointsSummaryPointNumberRow0"]').should(
          'have.text',
          '1',
        )
        //need to wait a bit between else going to fast breaks the project area (doesn't
        //create proper polygon; it overlaps the line)
        cy.wait(1000)
        //point 2
        cy.get('[data-cy="polygonMap"]').dragMapFromCenter({
          xMoveFactor: -1 / 10,
          yMoveFactor: -1 / 2,
        })
        cy.get('[data-cy="addVertex"]').click()
        cy.get('[data-cy="pointsSummaryPointNumberRow1"]').should(
          'have.text',
          '2',
        )
        cy.wait(1000)
        //point 3
        cy.get('[data-cy="polygonMap"]').dragMapFromCenter({
          xMoveFactor: 1 / 3,
          yMoveFactor: -1 / 8,
        })
        cy.get('[data-cy="addVertex"]').click()
        cy.get('[data-cy="pointsSummaryPointNumberRow2"]').should(
          'have.text',
          '3',
        )
        cy.wait(1000)
        //point 4
        cy.get('[data-cy="polygonMap"]').dragMapFromCenter({
          xMoveFactor: 1 / 6,
          yMoveFactor: 1 / 2,
        })
        cy.get('[data-cy="addVertex"]').click()
        cy.get('[data-cy="pointsSummaryPointNumberRow3"]').should(
          'have.text',
          '4',
        )
        cy.wait(1000)
      })
  })
  it('2.4 - [test-to-pass]define project area for "Cypress TEST Project"', () => {
    //define 2nd project area
    cy.get(`[data-cy="projectAreasToDefineOptions"]`)
      .children()
      .eq(1)
      .should('have.text', 'Cypress TEST Project')
      .click() //opens map interface
    //point 1
    cy.get('[data-cy="polygonMap"]').dragMapFromCenter({
      xMoveFactor: 1 / 6,
      yMoveFactor: -1 / 3,
    })
    cy.get('[data-cy="addVertex"]').click()
    cy.get('[data-cy="pointsSummaryPointNumberRow0"]').should('have.text', '1')
    cy.wait(1000)
    //point 2
    cy.get('[data-cy="polygonMap"]').dragMapFromCenter({
      xMoveFactor: 1 / 10,
      yMoveFactor: 1 / 2,
    })
    cy.get('[data-cy="addVertex"]').click()
    cy.get('[data-cy="pointsSummaryPointNumberRow1"]').should('have.text', '2')
    cy.wait(1000)
    //point 3
    cy.get('[data-cy="polygonMap"]').dragMapFromCenter({
      xMoveFactor: -1 / 3,
      yMoveFactor: 1 / 8,
    })
    cy.get('[data-cy="addVertex"]').click()
    cy.get('[data-cy="pointsSummaryPointNumberRow2"]').should('have.text', '3')
    cy.wait(1000)
    //point 4
    cy.get('[data-cy="polygonMap"]').dragMapFromCenter({
      xMoveFactor: -1 / 6,
      yMoveFactor: -1 / 2,
    })
    cy.get('[data-cy="addVertex"]').click()
    cy.get('[data-cy="pointsSummaryPointNumberRow3"]').should('have.text', '4')
    cy.wait(1000)

    cy.dataCy('my_location').click()
    // back to center, not sure how to check it worked with complete accuracy, as the map varies by the decimal point sometimes

    //close the project area definition popup
    cy.get('[data-cy="doneProjectArea"]').click()
    cy.get('[data-cy="confirmProjects"]').should(
      'contain.text',
      'Edit New project areas for Selected Project(s)',
    )

    cy.nextStep()
  })
})

describe('3 - complete plot selection', () => {
  it('3.1 - [test-to-pass]complete plot selection for SATRAFLB0001', () => {
    cy.dataCy('plot_name').validateField(['done', -1], true)
    // TODO: validate fields inside plot_name
    cy.selectFromDropdown('state', 'South Australia')
    cy.selectFromDropdown('program', 'Training')
    cy.selectFromDropdown('bioregion', 'Flinders Lofty Block')
    cy.get('[data-cy="unique_digits"]').click().type('0001').wait(100)
    cy.get('[data-cy="plotName"]').click()
    cy.get('[data-cy="plotName"]', { timeout: 3000 }).should(
      'contain.text',
      'SATRAFLB0001',
    )

    //these fields are hidden until we select `projects_for_plot`
    cy.dataCy('recommended_location_point').should('not.exist')
    cy.dataCy('choosePosition').should('not.exist')

    cy.dataCy('projects_for_plot')
      .validateField(['done', -1])
      .selectFromDropdown([
        'Kitchen Sink TEST Project',
        'Project Area & Plot Selection',
      ])

    cy.dataCy('recommended_location_point')
      .validateField(['done', -1])
      .selectFromDropdown('South West')
    cy.recordLocation({
      coords: {
        lat: -34.95,
        lng: 138.62,
      }
    })

    cy.get('[data-cy="comments"]')
      .validateField(['done', -1])
      .type('Plot comment\n\nNext line')

    cy.savePlotAndCheck({
      plotLabel: 'SATRAFLB0001',
      checkMode: 'isReserved',
    })
  })
  it('3.2 - [test-to-fail]try create duplicate plot selection SATRAFLB0001', () => {
    //fill in duplicate data
    cy.get('[data-cy="addEntry"]').click()
    cy.selectFromDropdown('state', 'South Australia')
    cy.selectFromDropdown('program', 'Training')
    cy.selectFromDropdown('bioregion', 'Flinders Lofty Block')
    cy.get('[data-cy="unique_digits"]').click().type('0001').wait(100)
    cy.get('[data-cy="plotName"]').click()
    cy.get('[data-cy="plotName"]', { timeout: 3000 }).should(
      'contain.text',
      'SATRAFLB0001',
    )

    //fill in the rest of the data that we *do* want for SATRAFLB0002
    cy.selectFromDropdown('projects_for_plot', [
      'Project Area & Plot Selection',
      'Cypress TEST Project',
    ])
    cy.recordLocation({
      coords: {
        lat: -34.948569,
        lng: 138.590558,
      }
    })
    cy.selectFromDropdown('recommended_location_point', 'South West')
    cy.get('[data-cy="comments"]').type('Plot comment')
    
    //test that the duplicate plot label is rejected
    cy.dismissPopups().wait(200)
    cy.get('[data-cy="done"]').click()
    cy.get('.q-notification', { timeout: 15000 }).eq(-1).as('popup')
    cy.get('@popup').should(
      'contain.text',
      `Plot with name 'SATRAFLB0001' already exists`,
    )
    cy.get('@popup').contains('Dismiss').click().wait(500)
    cy.checkPlotNameNotRereserved({
      plotLabel: 'SATRAFLB0001',
    })
  })
  it('3.3 - [test-to-pass]complete plot selection for SATRAFLB0002', () => {
    //we filled-in most of the data for this above, so just need to correct the digits to
    //make it pass
    cy.get('[data-cy="unique_digits"]').click().clear().type('0002').wait(100)
    cy.get('[data-cy="plotName"]').click()
    cy.get('[data-cy="plotName"]', { timeout: 3000 }).should(
      'contain.text',
      'SATRAFLB0002',
    )

    cy.savePlotAndCheck({
      plotLabel: 'SATRAFLB0002',
      checkMode: 'isReserved',
    })
  })
  it('3.4 - [test-to-fail]try change SATRAFLB0002 to SATRAFLB0001', () => {
    cy.setExpansionItemState('_(SATRAFLB0002)_'.toLowerCase(), 'open').within(() => {
      cy.get('[data-cy="unique_digits"]').click().clear().type('0001').wait(100)
      cy.get('[data-cy="plotName"]').click()
      cy.get('[data-cy="plotName"]', { timeout: 3000 }).should(
        'contain.text',
        'SATRAFLB0001',
      )
    })
    //test that the duplicate plot label is rejected
    cy.dismissPopups().wait(200)
    cy.get('[data-cy="done"]').click()
    cy.get('.q-notification', { timeout: 15000 }).eq(-1).as('popup')
    cy.get('@popup').should(
      'contain.text',
      `Plot with name 'SATRAFLB0001' already exists`,
    )
    cy.get('@popup').contains('Dismiss').click().wait(500)

    //check that SATRAFLB0001/SATRAFLB0002 were not un-reserved
    cy.checkPlotNameReserved({
      plotLabel: 'SATRAFLB0001',
    })
    cy.checkPlotNameReserved({
      plotLabel: 'SATRAFLB0002',
    })

    //check that SATRAFLB0001 was not re-reserved
    cy.checkPlotNameNotRereserved({
      plotLabel: 'SATRAFLB0001',
    })
  })
  it('3.5 - [test-to-pass]undo attempt to change SATRAFLB0002 to SATRAFLB0001', () => {
    cy.get('[data-cy="unique_digits"]').click().clear().type('0002').wait(100)
    cy.get('[data-cy="plotName"]').click()
    cy.get('[data-cy="plotName"]', { timeout: 3000 }).should(
      'contain.text',
      'SATRAFLB0002',
    )
    cy.get('[data-cy="done"]').click().wait(500)

    //check that SATRAFLB0001 was not un-reserved
    cy.checkPlotNameReserved({
      plotLabel: 'SATRAFLB0001',
    })

    //check that SATRAFLB0002 was not re-reserved
    cy.checkPlotNameNotRereserved({
      plotLabel: 'SATRAFLB0002',
    })
  })
  it('3.6 - [test-to-pass]edit SATRAFLB0002 to SATRAFLB0003', () => {
    cy.setExpansionItemState('_(SATRAFLB0002)_'.toLowerCase(), 'open').within(() => {
      cy.get('[data-cy="unique_digits"]').click().clear().type('0003').wait(100)
      cy.get('[data-cy="plotName"]').click()
      cy.get('[data-cy="plotName"]', { timeout: 3000 }).should(
        'contain.text',
        'SATRAFLB0003',
      )
    })

    //reserve request happens first, then un-reserve request
    cy.savePlotAndCheck({
      plotLabel: 'SATRAFLB0003',
      checkMode: 'isReserved',
    })

    cy.checkPlotNameUnreserved({
      plotLabel: 'SATRAFLB0002',
    })
  })
  it('3.7 - [test-to-pass]complete plot selection for SATRAFLB0004', () => {
    cy.get('[data-cy="addEntry"]').click()
    cy.selectFromDropdown('state', 'South Australia')
    cy.selectFromDropdown('program', 'Training')
    cy.selectFromDropdown('bioregion', 'Flinders Lofty Block')
    cy.get('[data-cy="unique_digits"]').click().type('0004').wait(100)
    cy.get('[data-cy="plotName"]').click()
    cy.get('[data-cy="plotName"]', { timeout: 3000 }).should(
      'contain.text',
      'SATRAFLB0004',
    )

    cy.selectFromDropdown('projects_for_plot', [
      'Cypress TEST Project'
    ])
    cy.recordLocation({
      coords: {
        lat: -34.948569,
        lng: 138.590558,
      }
    })
    cy.selectFromDropdown('recommended_location_point', 'South West')
    cy.get('[data-cy="comments"]').type('Plot comment')

    cy.savePlotAndCheck({
      plotLabel: 'SATRAFLB0004',
      checkMode: 'isReserved',
    })
  })
  it('3.8 - [test-to-pass]delete plot selection SATRAFLB0001', () => {
    cy.savePlotAndCheck({
      plotLabel: 'SATRAFLB0001',
      checkMode: 'isUnreserved',
      interceptRequestType: 'DELETE',
      buttonToPress: 'delete',
    })
  })
  it('3.9 - [test-to-pass]edit value for SATRAFLB0004', () => {
    cy.setExpansionItemState('_(SATRAFLB0004)_'.toLowerCase(), 'open').within(() => {
      cy.selectFromDropdown('recommended_location_point', 'North West')
    })
    cy.get('[data-cy="done"]').click().wait(500)

    cy.checkPlotNameReserved({
      plotLabel: 'SATRAFLB0004',
    })

    cy.checkPlotNameNotRereserved({
      plotLabel: 'SATRAFLB0004',
    })
  })
  it('3.10 - [test-to-pass]re-create plot selection SATRAFLB0001', () => {
    cy.get('[data-cy="addEntry"]').click()
    cy.selectFromDropdown('state', 'South Australia')
    cy.selectFromDropdown('program', 'Training')
    cy.selectFromDropdown('bioregion', 'Flinders Lofty Block')
    cy.get('[data-cy="unique_digits"]').click().type('0001').wait(100)
    cy.get('[data-cy="plotName"]').click()
    cy.get('[data-cy="plotName"]', { timeout: 3000 }).should(
      'contain.text',
      'SATRAFLB0001',
    )

    cy.selectFromDropdown('projects_for_plot', [
      'Kitchen Sink TEST Project',
      'Project Area & Plot Selection',
    ])
    cy.recordLocation({
      coords: {
        lat: -34.948569,
        lng: 138.590558,
      }
    })
    cy.selectFromDropdown('recommended_location_point', 'South West')
    cy.get('[data-cy="comments"]').type('Plot comment')

    cy.savePlotAndCheck({
      plotLabel: 'SATRAFLB0001',
      checkMode: 'isReserved',
    })
  })
  pub()
})

let plotSelectionOrgData, projectOrgData, reservedPlotCoreData
describe('check org data', () => {
  it('fetch plot selection data from org', () => {
    cy.doRequest(
      'plot-selections',
      {
        host: 'org',
        args: {
          populate: 'deep',
        },
      },
    )
      .then((plotSelectionResp) => {
        console.log('plotSelectionResp:', JSON.stringify(plotSelectionResp, null, 2))
        plotSelectionOrgData = cloneDeep(plotSelectionResp.body.data)
      })
  })
  it('fetch project data from org', () => {
    cy.doRequest(
      'org/user-projects',
      {
        host: 'org',
      },
    )
      .then((projectResp) => {
        console.log('projectResp:', JSON.stringify(projectResp, null, 2))
        projectOrgData = cloneDeep(projectResp.body.projects)
      })
  })
  it('fetch reserved plot data from core', () => {
    cy.doRequest(
      'reserved-plot-labels',
      {
        args: {
          populate: 'deep',
        },
      },
    )
      .then((reservedPlotResp) => {
        console.log('reservedPlotResp:', JSON.stringify(reservedPlotResp, null, 2))
        reservedPlotCoreData = cloneDeep(reservedPlotResp.body.data)
      })
  })
  it('check data', () => {
    console.log('plotSelectionOrgData:', JSON.stringify(plotSelectionOrgData, null, 2))
    console.log('projectOrgData:', JSON.stringify(projectOrgData, null, 2))
    console.log('reservedPlotCoreData:', JSON.stringify(reservedPlotCoreData, null, 2))

    for (const proj of projectOrgData) {
      cy.log(`Checking project ${proj.name}`)
      switch (proj.name) {
        case 'Project Area & Plot Selection':
          checkProjectArea({
            projectArea: proj.project_area,
          })
          checkProjectPlots({
            plotSelections: proj.plot_selections,
            expectedNewPlots: [
              'SATRAFLB0001',
              'SATRAFLB0003',
            ],
          })
          break
        case 'Cypress TEST Project':
          checkProjectArea({
            projectArea: proj.project_area,
          })
          checkProjectPlots({
            plotSelections: proj.plot_selections,
            expectedNewPlots: [
              'SATRAFLB0003',
              'SATRAFLB0004',
            ],
          })
          break
        case 'Kitchen Sink TEST Project':
          checkProjectPlots({
            plotSelections: proj.plot_selections,
            expectedNewPlots: [
              'SATRAFLB0001',
            ],
          })
          break
      }
    }

    for (const newPlotName of ['SATRAFLB0001', 'SATRAFLB0003', 'SATRAFLB0004']) {
      cy.log(`Checking that plot ${newPlotName} has been reserved`)
      cy.wrap(
        reservedPlotCoreData.some(
          r => r.attributes.created_plot_selection.data.attributes.plot_label === newPlotName
        )
      ).should('be.true')
    }

    for (const deletedPlotName of ['SATRAFLB0002']) {
      cy.log(`Checking that plot ${deletedPlotName} has NOT been reserved`)
      cy.wrap(
        reservedPlotCoreData.some(
          r => r.attributes.created_plot_selection.data.attributes.plot_label === deletedPlotName
        )
      ).should('be.false')
    }
  })
})

//this is deprecated due to #2067, but leaving in for now in case anything changes
describe.skip('[test-to-fail] Try create plot offline, but that plot has already been reserved and submit', () => {
  it('.should() - go offline', () => {
    //so when we try create the plot, it cannot make the reservation
    cy.goOffline()
  })
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    //protocol should be assigned to kitchen sink, but test with specific project anyway
    cy.selectProtocolIsEnabled(null, protocolCy, projectCy)
  })
  it('.should() - create plot SATRAFLB0001 that already exists', () => {
    cy.selectFromDropdown('relevantProjects', 'Kitchen Sink TEST Project')
    cy.nextStep()

    //try create the same plot we already created above
    cy.selectFromDropdown('state', 'South Australia')
    cy.selectFromDropdown('program', 'Training')
    cy.selectFromDropdown('bioregion', 'Flinders Lofty Block')
    cy.dataCy('unique_digits').type('0001')

    cy.dataCy('projects_for_plot').selectFromDropdown([
      'Kitchen Sink TEST Project',
    ])
    cy.recordLocation({
      coords: {
        lat: -34.95,
        lng: 138.62,
      }
    })
    
    //here it'll attempt to reserve the plot, but fail as it's offline
    cy.dataCy('done').click()

    cy.workflowPublishQueue({
      goWorkflowNext: true,
    })
  })

  it('synch queued collection button should be disabled', () => {
    cy.dataCy('submitQueuedCollectionsBtn')
      .should('be.disabled')
  })
  
  it('.should() - go online', () => {
    cy.goOnline()
  })
  it('[test-to-fail] Try sync queue but get rejected due to duplicate plot', () => {
    cy.dataCy('submitQueuedCollectionsBtn', { timeout: 15000 })
      .should('be.enabled')
      .click()
      .wait(100)
    cy.dataCy('submitQueuedCollectionsConfirm').click()
    cy.get('.q-notification', { timeout: 360000 })
      .should('exist')
      .and('contain.text', 'Failed to submit all queued collections')
    
    cy.dataCy('failedQueuedCollectionsSummaryTable')
      .within(() => {
        cy.get(`:contains(Fields are not unique: plot_label)`)
          .should('exist')
      })
  })
  it('clear queue', () => {
    resetState()
  })
})

//this is deprecated due to #2067, but leaving in for now in case anything changes
describe.skip('[test-to-pass] Create plot offline, preventing reservation, and ensure core reserves this missing entry', () => {
  it('.should() - go offline', () => {
    //so when we try create the plot, it cannot make the reservation
    cy.goOffline()
  // })
  // it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    //protocol should be assigned to kitchen sink, but test with specific project anyway
    cy.selectProtocolIsEnabled(null, protocolCy, projectCy)
  // })
  // it('.should() - create plot SATRAFLB9000 that does not exist', () => {
    cy.selectFromDropdown('relevantProjects', 'Kitchen Sink TEST Project')
    cy.nextStep()

    //try create the same plot we already created above
    cy.selectFromDropdown('state', 'South Australia')
    cy.selectFromDropdown('program', 'Training')
    cy.selectFromDropdown('bioregion', 'Flinders Lofty Block')
    cy.dataCy('unique_digits').type('9000')

    cy.dataCy('projects_for_plot').selectFromDropdown([
      'Kitchen Sink TEST Project',
    ])
    cy.recordLocation({
      coords: {
        lat: -34.95,
        lng: 138.62,
      }
    })
    
    //here it'll attempt to reserve the plot, but fail as it's offline
    cy.dataCy('done').click()

    cy.workflowPublishQueue({
      goWorkflowNext: true,
    })
  })

  it('synch queued collection button should be disabled', () => {
    cy.dataCy('submitQueuedCollectionsBtn')
      .should('be.disabled')
  })

  it('.should() - go online', () => {
    cy.goOnline()
  })
  it('.should() - check that plot SATRAFLB9000 was not reserved', () => {
    //wait for the app to detect we're online
    cy.dataCy('submitQueuedCollectionsBtn', { timeout: 20000 })
      .should('be.enabled')

    cy.checkPlotNameUnreserved({
      plotLabel: 'SATRAFLB9000',
    })
  })
  it('[test-to-pass] Sync queue which will add missing plot reservation', () => {
    //when we sync, core will see that there is a missing reservation entry and attempt
    //to create it
    cy.dataCy('submitQueuedCollectionsBtn')
      .should('be.enabled')
      .click()
      .wait(100)
    cy.dataCy('submitQueuedCollectionsConfirm').click()
    cy.get('.q-notification', { timeout: 360000 })
      .should('exist')
      .and('contain.text', 'Successfully submitted all queued collections')
  })
  it('.should() - check that core reserved plot SATRAFLB9000', () => {
    cy.checkPlotNameReserved({
      plotLabel: 'SATRAFLB9000',
      shouldBeCoreGenerated: true,
    })
  })
})

describe('logout', () => {
  it('logout', () => {
    //need to logout so that subsequent tests use `TestUser`
    cy.logout()
  })
})

const checkProjectArea = ({
  projectArea,
}) => {
  cy.log(`Checking project area: ${JSON.stringify(projectArea)}`)
  cy.wrap(projectArea)
    .should('have.keys', 'type', 'coordinates')
  cy.wrap(projectArea.coordinates)
    .should('be.a', 'array')
    .and('have.length', 4)
  for (const coordObj of projectArea.coordinates) {
    cy.wrap(coordObj)
      .should('have.keys', 'lat', 'lng')
  }
}

const checkProjectPlots = ({
  plotSelections,
  expectedNewPlots,
}) => {
  cy.log(`Checking plot selections (against new plots ${expectedNewPlots.join(', ')}): ${JSON.stringify(plotSelections)}`)
  //all `expectedNewPlots` must be in the project
  cy.wrap(
    expectedNewPlots.every(
      newPlot => plotSelections.some(ps => ps.name === newPlot)
    )
  ).should('be.true')
}
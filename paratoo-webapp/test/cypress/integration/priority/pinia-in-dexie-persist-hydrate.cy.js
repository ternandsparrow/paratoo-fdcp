const { waitForHydration } = require("../../support/command-utils")

const project = 'Kitchen\\ Sink\\ TEST\\ Project'

const states = {
  before: {
    auth: null,
    apiModels: null
  },
  after: {
    auth: null,
    apiModels: null
  },
}

function getStoreStates({ stateKey }) {
  if (!Object.keys(states).includes(stateKey)) {
    throw new Error(`Programmer error: need correct key to assign state to`)
  }
  
  //this command grabs the state from memory, so if there's a hydration issue, we'll
  //detect it due to a mismatch
  cy.loadManyStores(
    [
      {
        storeName: 'auth',
      },
      {
        storeName: 'apiModels',
      },
    ],
  )
    .then((stores) => {
      states[stateKey].auth = Cypress._.cloneDeep(stores.authStore)
      states[stateKey].apiModels = Cypress._.cloneDeep(stores.apiModelsStore)

      delete states[stateKey].auth.lastAccessTime

      console.log(`states ${stateKey}:`, JSON.stringify(states[stateKey]))
    })
}

describe('Landing', () => {
  it('login', () => {
    cy.login('TestUser', 'password')
  })
})

describe('Hydration', () => {
  it('prepare for offline collections', () => {
    cy.prepareForOfflineVisit(project)
  })

  it('get store states before reload', () => {
    getStoreStates({ stateKey: 'before' })
  })

  it('reload page', () => {
    cy.reload()
  })

  it('wait for hydration', () => {
    waitForHydration()
  })

  it('get store states after reload', () => {
    getStoreStates({ stateKey: 'after' })
  })

  it('check states before/after', () => {
    cy.wrap(states.before)
      .should('deep.equal', states.after)
  })
})

const storeCases = [
  {
    storeName: 'auth',
    storeKeyToWrite: 'projectContext',
    storeValueToWrite: 1,
  },
  {
    storeName: 'apiModels',
    storeKeyToWrite: 'testString',
    storeValueToWrite: 'foobar',
  },
  {
    storeName: 'apiModels',
    storeKeyToWrite: 'testNum',
    storeValueToWrite: 123,
  },
  {
    storeName: 'apiModels',
    storeKeyToWrite: 'testObj',
    storeValueToWrite: {
      foo: 'bar',
    },
  },
  {
    storeName: 'apiModels',
    storeKeyToWrite: 'testArrStr',
    storeValueToWrite: [
      'foo',
      'bar',
    ],
  },
  {
    storeName: 'apiModels',
    storeKeyToWrite: 'testArrObj',
    storeValueToWrite: [
      {
        one: 'foo',
      },
      {
        two: 'bar',
      },
    ],
  },
]
for (const storeCase of storeCases) {
  describe(`Persist ${storeCase.storeName}`, () => {
    it('write to pinia', () => {
      //writing to pinia (memory) will then persist to Dexie, where we can read from
      //directly and check the relevant data
      cy.getStoreReference({
        store: `${storeCase.storeName}Store`,
      })
        .then((storeRef) => {
          console.log(`writing value '${storeCase.storeValueToWrite}' to key '${storeCase.storeKeyToWrite}'`)
          storeRef[storeCase.storeKeyToWrite] = storeCase.storeValueToWrite
        })
    })
  
    it('read from Dexie and check store data', () => {
      cy.wait(2000)   //in case persistance is still happening
      cy.window()
        .then(($win) => {
          const storeState = $win.dexiePersistPlugin.getStoreState({ store: storeCase.storeName })
            .then((storeData) => {  
              return Cypress._.cloneDeep(storeData)
            })
          return storeState
        })
        .then((storeState) => {

          //check the data we just wrote is there
          cy.wrap(storeState[storeCase.storeKeyToWrite])
            .should('have.keys', storeCase.storeKeyToWrite)
          //raw JSON data with primitives from Dexie has a redundant key (due to how
          //primitives are handled)
          cy.wrap(storeState[storeCase.storeKeyToWrite][storeCase.storeKeyToWrite])
            //`deep.equal` for non-primitive variables (like objects) - if we use `eq` it
            //will be comparing memory equality, like if we use `===` to check if two
            //objects are equal (which is why we usually use Lodash `isEqual()` for
            //objects and similar)
            .should('deep.equal', storeCase.storeValueToWrite)
        })
    })
  })
}

describe('Persist nulls', () => {
  it('reset variables', () => {
    for (const storeCase of storeCases) {
      cy.getStoreReference({
        store: `${storeCase.storeName}Store`,
      })
        .then((storeRef) => {
          console.log(`resetting key '${storeCase.storeKeyToWrite}' to null`)
          storeRef[storeCase.storeKeyToWrite] = null
        })
    }
  })

  it('check variables were reset', () => {
    cy.wait(2000)   //in case persistance is still happening
    for (const storeCase of storeCases) {
      cy.window()
        .then(($win) => {
          const storeState = $win.dexiePersistPlugin.getStoreState({ store: storeCase.storeName })
            .then((storeData) => {  
              return Cypress._.cloneDeep(storeData)
            })
          return storeState
        })
        .then((storeState) => {
          cy.wrap(storeState[storeCase.storeKeyToWrite][storeCase.storeKeyToWrite])
            .should('be.null')
        })
    }
  })
})
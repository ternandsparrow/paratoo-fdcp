import {
  waitForWindowData,
} from '../../support/command-utils'

// FIXME there is a problem when doing retries as it tries to logout when not logged in.
// I don't think this test even needs to be logged in, actually.

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('init auth and api models with localStorage state', () => {
    cy.fixture('trimmed_api_models.json') // mostly just luts, but works for what we need it to
      .its('apiModels')
      .then((apiModels) => {
        return cy.task('compress', apiModels)
      })
      .as('apiModels')


    cy.window({ log: false })
      .then(($win) => {
        // clear auth and api models dexie tables
        for (const store of ['apiModels', 'auth']) {
          try {
            cy.log('deleting dexie auth table...')
            cy.wrap(
              $win.dexiePersistPlugin.deleteStore({ storeName: store }),
              { log: false, timeout: 60000 }
            ).should('be.true', { timeout: 60000 })
          } catch {
            throw new Error(`${store} failed to delete :(`)
          }
        }
        cy.get('@apiModels')
          .then((apiModels) => {
            return [
              apiModels,
              JSON.stringify({ //this may mess with test retries as login looks for tokens and user profiles in localStorage
                identifiers: [],
                token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZm9yY2VUb2tlbkV4cGlyeSI6eyJmb3JjZUp3dEV4cGlyeSI6bnVsbCwiZm9yY2VSZWZyZXNoRXhwaXJ5IjpudWxsfSwiaWF0IjoxNzMyMjU0OTc0LCJleHAiOjE3MzQ4NDY5NzR9._uSpm2xbPyHJP53d3lqIc1qERzGA1xiUOeGLbhVhZ1M',
                refreshToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNzMyMjU0OTc0LCJleHAiOjE3MzQ4NDY5NzR9.fZXph1eqiNPFMbZmqm00DwOnj_7AVZJQHObfA_8Gypk',
                userProfile: 'testUser',
                cookie: null,
                webauthn: null,
                projAndProt: null,
                currentContext: { plot: 1, visit: 1 },
                projectContext: null,
                oidcTokenResponse: null,
                currentLayoutPlots: [],
                authorisedModels: [
                  'opportunistic-survey',
                  'opportunistic-observation'
                ],
                staticResourceNames: { 'species': null, 'map': null },
                hostInfo: null,
                previousAuthUserProfile: null,
                loginTime: 10,
                lastTokenRefreshTime: null,
                lastRefreshAllApiModelsTime: null,
                lastAccessTime: `Fri Mar 31 2024 12: 40: 37 GMT + 1030(Australian Central Daylight Time)`
              }) // as auth
            ]
          }).then(([apiModels, auth]) => {
            // remove hashydrated flags from localStorage
            $win.localStorage.removeItem(`apiModelsHasHydrated`)
            $win.localStorage.removeItem(`authHasHydrated`)

            expect(Cypress._.get($win, 'apiModelsHasHydrated', undefined), { log: false }).to.be.undefined
            expect(Cypress._.get($win, 'authHasHydrated', undefined), { log: false }).to.be.undefined

            // init api models with compressed state directly to localstorage (compressed above)
            // init auth with uncompressed state directly to local storage 
            $win.localStorage.setItem('auth', auth)
            $win.localStorage.setItem('apiModels', apiModels)

            // FIXME auth store comes with some extra methods that we dont care to compare
            expect(Cypress._.get($win.localStorage, 'auth', {})).to.exist
            expect(Cypress._.get($win.localStorage, 'apiModels', {})).to.exist
          })
      })
  })
  it('reload page to begin migration', () => {
    cy.reload()
    // cy.wait(5000)
  })
  it('wait for hydration', () => {
    waitForWindowData()
      .then(() => {
        cy.window()
          .then($win => {
            expect(Cypress._.get($win.localStorage, 'apiModelsHasHydrated', undefined), { log: false }).to.not.be.undefined
            expect(Cypress._.get($win.localStorage, 'authHasHydrated', undefined), { log: false }).to.not.be.undefined
            expect(Cypress._.get($win.localStorage, 'auth', undefined)).to.not.exist
            expect(Cypress._.get($win.localStorage, 'apiModels', undefined)).to.not.exist
          })
      })

  })
  it('query dexie to double check data', () => {
    cy.window()
      .then(($win) => {
        for (const store of ['apiModels', 'auth']) {
          cy.wrap(
            $win.dexiePersistPlugin.getStoreState({ store: store }),
            { log: false, timeout: 60000 }
          ).then((e) => {
            cy.log(e)
          })

        }
      })
  })

  it('cleaup', () => {
    // clear dexie and localstorage
    // cy.logout(true)
    cy.window()
      .then(($win) => {
        for (const store of ['apiModels', 'auth']) {
          try {
            cy.log('deleting dexie auth table...')
            cy.wrap(
              $win.dexiePersistPlugin.deleteStore({ storeName: store }),
              { log: false, timeout: 60000 }
            ).should('be.true', { timeout: 60000 })
          } catch (e) {
            throw new Error(`${store} failed to delete :(\n${e}`)
          }
        }
      })
    cy.clearAllSessionStorage()
    cy.clearAllLocalStorage() //should log you out
    cy.clearCookies()
  })
})
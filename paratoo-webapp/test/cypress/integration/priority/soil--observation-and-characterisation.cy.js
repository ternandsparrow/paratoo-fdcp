const { pub } = require("../../support/command-utils")

const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'soils'
const protocol = 'Soil Site Observation and Pit Characterisation'
global.ignorePaths = ['soil-horizon-sample.0.horizon', 'soil-horizon-sample.1.horizon']
describe('landing', () => {
  it('login', () => {
    cy.login('TestUser', 'password')
  })

  it('.should() - assert navigation to ' + protocol + ' is disabled', () => {
    cy.selectProtocolIsDisabled(module, protocol)
  })

  it('plot layout', () => {
    cy.plotLayout()
  })

  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})

// TODO split into seperate it()s
let soilPitId
describe('do ' + protocol.toLowerCase(), () => {
  it('soil pit location', () => {
    /* ==== first step === */
    cy.get('[data-cy="Field"]').click()

    cy.testEmptyField('Observers', 'workflowNextButton')
    // TODO use prefilled observerName if available
    cy.get('[data-cy="observerName"]')
      .type('Tom' + (Math.random() + 1).toString(36).substring(7))
      .wait(100)
      .type('{enter}')
    // get auto generated soilPitId for lab step
    cy.get('[data-cy="soil_pit_id"]')
      .invoke('val')
      .then((text) => {
        soilPitId = text
      })

    cy.getAndValidate('collection_method').selectFromDropdown('Tablet')
    cy.get('[data-cy="collection_method"]').click()
    cy.get('[data-cy="choosePosition"]').click()
    cy.get('[data-cy="recordLocation"]').click()

    cy.getAndValidate('location_description').type(
      'This is a location descriptions first line{enter}This is the second',
    )
    // cy.get('[data-cy="workflowNextButton"]').click()
    cy.nextStep()
  })

  it('land form element', () => {
    cy.dataCy('evaluation_means').validateField().selectFromDropdown('Estimate')

    // cy.get('[data-cy="slope_percent"]')
    //   // .validateField()
    //   .type('5')
    // cy.dataCy('slope_class')
    //   // .validateField()
    //   .should('have.text', 'Gently inclined (3-10%)')

    const percentToClass = {
      //lower bounds
      0: 'Level',
      1: 'Very gently inclined',
      3: 'Gently inclined',
      10: 'Moderately inclined',
      32: 'Steep',
      56: 'Very steep',
      100: 'Precipitous',
      300: 'Cliffed',
      //upper bounds
      0.99: 'Level',
      2.99: 'Very gently inclined',
      9.99: 'Gently inclined',
      31.99: 'Moderately inclined',
      55.99: 'Steep',
      99.99: 'Very steep',
      299.99: 'Precipitous',
      1000: 'Cliffed',    //LUT can go above, but this field max 1000
    }
    for (const [percent, slopeClass] of Object.entries(percentToClass)) {
      cy.dataCy('slope_percent').clear().type(percent)
      cy.get('[data-cy="slope_class"]').should('contain.text', slopeClass)
    }

    cy.dataCy('morphological_type').validateField().selectFromDropdown('Flat')
    cy.dataCy('inclination_of_slope')
      .validateField()
      .selectFromDropdown('Minimal')
    cy.dataCy('landform_element')
      .validateField()
      .selectFromDropdown('Dam', 'Dam')
    cy.dataCy('location_within_landform')
      // .validateField()
      .selectFromDropdown('Top third of the height of the landform element')

    cy.nextStep()
  })

  it('land surface phenomena & soil development', () => {
    /* ==== Third step === */
    cy.getAndValidate('surface_soil_when_dry')
      // .validateField()
      .selectFromDropdown('Loose')
    cy.getAndValidate('disturbance').selectFromDropdown(
      'No effective disturbance except high grazing by hoofed animals',
    )

    cy.nextStep()
  })

  it('microrelief', () => {
    // first
    cy.dataCy('microrelief_type')
      .eq(-1)
      .validateField(['done', -1])
      .selectFromDropdown('Normal gilgai')
    cy.dataCy('gilgai_proportion').should('exist')

    cy.dataCy('microrelief_type').eq(-1).selectFromDropdown('Biotic')
    cy.dataCy('gilgai_proportion').should('not.exist')
    cy.dataCy('biotic_relief_agent')
      .eq(-1)
      .validateField(['done', -1])
      .should('exist')

    cy.dataCy('microrelief_type').eq(-1).selectFromDropdown('Mass movement')
    cy.dataCy('gilgai_proportion').should('not.exist')
    cy.dataCy('biotic_relief_agent').should('not.exist')

    cy.dataCy('vertical_interval_distance')
      .validateField(['done', -1])
      .type('2')
    cy.dataCy('horizontal_interval_distance')
      .validateField(['done', -1])
      .type('4')

    cy.dataCy('done').eq(0).click()

    // second
    cy.dataCy('addEntry').click()
    cy.dataCy('microrelief_type')
      .eq(-1)
      .selectFromDropdown('Mound/depression', null, 1)
    cy.dataCy('vertical_interval_distance')
      .eq(-1)
      .validateField(['done', -1])
      .type('5')
    cy.dataCy('horizontal_interval_distance')
      .eq(-1)
      .validateField(['done', -1])
      .type('8')
    cy.dataCy('done').eq(-1).click()

    // third
    cy.dataCy('addEntry').click()
    cy.dataCy('microrelief_type').eq(-1).selectFromDropdown('Biotic', null, 1)
    cy.dataCy('biotic_relief_agent').selectFromDropdown('Animal')
    cy.dataCy('microrelief_component').selectFromDropdown('Mound')
    cy.dataCy('vertical_interval_distance')
      .eq(-1)
      .validateField(['done', -1])
      .type('5')
    cy.dataCy('horizontal_interval_distance')
      .eq(-1)
      .validateField(['done', -1])
      .type('8')
    cy.dataCy('done').eq(-1).click()

    // fourth
    cy.dataCy('addEntry').eq(-1).click()
    cy.dataCy('delete').eq(-1).click()

    // cy.dataCy('workflowNextButton').click()
    cy.nextStep()
  })

  it('erosion observation', () => {
    cy.dataCy('erosion_state')
      .validateField(['done', -1])
      .selectFromDropdown('Absent', 'Absent')
    cy.dataCy('erosion_type').should('not.exist')
    cy.dataCy('done').eq(0).click()
    cy.dataCy('addEntry').click()

    cy.dataCy('erosion_state')
      .validateField(['done', -1])
      .selectFromDropdown('Active', 'Active')
    cy.dataCy('erosion_type')
      .validateField(['done', -1])
      .selectFromDropdown('Mass movement', 'Mass movement')
    cy.dataCy('done').eq(-1).click()

    cy.dataCy('addEntry').click()
    cy.dataCy('delete').eq(-1).click()

    // cy.dataCy('workflowNextButton').click()
    cy.nextStep()
  })

  it('surface coarse fragment observation', () => {
    cy.getAndValidate('coarse_fragments_abundance').selectFromDropdown(
      'Moderately or many (20 - 50%)',
    )
    cy.getAndValidate('coarse_fragments_size').selectFromDropdown(
      'Large Boulders (>2000mm)',
    )
    cy.getAndValidate('coarse_fragments_shape').selectFromDropdown('Subangular')
    cy.getAndValidate('coarse_fragments_lithology').selectFromDropdown(
      'Aplite',
      'Aplite',
    )
    cy.getAndValidate('coarse_fragments_strength').selectFromDropdown(
      'Weak rock (25-50 MPa)',
    )
    cy.getAndValidate('coarse_fragments_alteration').selectFromDropdown(
      'Calcified',
    )

    cy.dataCy('done').eq(0).click(0)
    // cy.dataCy('workflowNextButton').click()
    cy.nextStep()
  })

  it('rock outcrop observation', () => {
    cy.getAndValidate('rock_outcrop_abundance').selectFromDropdown(
      'Very slightly rocky (<2% bedrock exposed)',
    )
    cy.getAndValidate('rock_outcrop_lithology').selectFromDropdown(
      'Halite',
      'Halite',
    )

    // cy.dataCy('workflowNextButton').click()
    cy.nextStep()
  })

  it('soil pit observation', () => {
    /* ==== fourth step === */
    cy.getAndValidate('observation_type').selectFromDropdown('Soil pit')
    cy.getAndValidate('soil_pit_depth').type('6')
    cy.getAndValidate('digging_stopped_by').selectFromDropdown('N/A')

    cy.getAndValidate('pit_photo', { isComponent: true })
    cy.addImages('addImg')
    cy.getAndValidate('comments')
    cy.nextStep()
  })

  it('soil horizon observation 1', () => {
    /* ==== fifth step === */
    cy.getAndValidate('horizon', {
      testEmptyField: ['done', -1],
      isComponent: true,
    })
    // 6M5f
    cy.dataCy('numeric_prefix').selectFromDropdown(6)

    cy.dataCy('deposition_mechanism').should('not.exist')
    cy.getAndValidate('horizon_', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('M')
    cy.dataCy('deposition_mechanism').should('exist')

    cy.getAndValidate('horizon_subdivision', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown(5)
    cy.getAndValidate('horizon_suffix', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('f')

    cy.getAndValidate('upper_depth', { testEmptyField: ['done', -1] }).type('1')
    cy.getAndValidate('lower_depth', { testEmptyField: ['done', -1] }).type('3')

    cy.getAndValidate('boundary_distinctness').selectFromDropdown(
      'Sharp (<5mm)',
    )
    cy.getAndValidate('boundary_shape', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('Wavy')
    cy.getAndValidate('texture_grade', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('Loam')
    cy.getAndValidate('texture_qualification', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('Heavy')
    cy.getAndValidate('texture_modifier', {
      testEmptyField: ['done', -1],
    }).selectFromDropdown('Fine sandy')

    // TODO do 1 or 2 more times
    cy.getAndValidate('colour', { isComponent: true })
    cy.getAndValidate('moisture_status').selectFromDropdown('Dry')
    cy.getAndValidate('colour_hue').selectFromDropdown('5R')
    cy.getAndValidate('colour_value').selectFromDropdown('5')
    cy.getAndValidate('colour_chroma').selectFromDropdown('8')
    cy.dataCy('done').eq(0).click()
    //=================================//
    // TODO do the optional ticks
    cy.dataCy('has_mottles').click()
    cy.dataCy('has_coarse_fragments').click()
    cy.dataCy('has_structure').click()
    cy.dataCy('has_segregation').click()
    cy.dataCy('has_voids').click()
    cy.dataCy('has_pans').click()
    cy.dataCy('has_cutans').click()
    cy.dataCy('has_roots').click()
    // has mottle
    cy.dataCy('mottle_1_expansion').should('exist')
    cy.getAndValidate('mottle_type', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Mottles')

    cy.getAndValidate('mottle_abundance', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Very few (<2%)')

    cy.getAndValidate('mottle_size', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Fine (<5mm)')

    cy.getAndValidate('mottle_contrast', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Distinct')

    cy.getAndValidate('mottle_colour', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Red')

    cy.getAndValidate('mottle_boundary_distinctness', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Sharp')

    cy.dataCy('done').eq(0).click()
    // has coarse frag
    cy.dataCy('coarse_fragment_1_expansion').should('exist')
    cy.selectFromDropdown(
      'coarse_frag_abundance',
      'Very or abundant (50 - 90%)',
    )
    cy.getAndValidate('coarse_frag_size', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Large Boulders (>2000mm)')

    cy.getAndValidate('coarse_frag_shape', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Rounded')

    cy.getAndValidate('coarse_frag_lithology', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Microdiorite', 'Microdiorite')

    cy.getAndValidate('coarse_frag_strength', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Weak rock (25-50 MPa)')

    cy.getAndValidate('coarse_frag_alteration', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Calcified')

    cy.getAndValidate('coarse_frag_distribution', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Stratified')

    cy.dataCy('done').eq(0).click()
    cy.wait(50)
    // has structure
    cy.dataCy('structure_1_expansion').should('exist')
    cy.getAndValidate('structure_grade', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Massive')

    cy.getAndValidate('structure_size', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('<2mm')

    cy.getAndValidate('structure_type', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Columnar')

    cy.getAndValidate('structure_compound_pedality', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown(
      'Largest peds (in the type of soil observation described), parting to',
    )
    cy.dataCy('done').eq(0).click()
    cy.wait(50)

    // has segregation
    cy.dataCy('segregation_1_expansion').should('exist')
    cy.getAndValidate('segregation_abundance', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Many (20-50%)')

    cy.getAndValidate('segregation_nature', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Argillaceous (clayey)')

    cy.getAndValidate('segregation_form', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Tubules')

    cy.getAndValidate('segregation_size', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Extremely coarse (>60mm)')

    cy.getAndValidate('segregation_strength', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Weak')

    cy.getAndValidate('segregation_magnetic_attribute', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Magnetic')

    cy.dataCy('done').eq(0).click()
    cy.wait(50)
    // has voids
    cy.dataCy('void_1_expansion').should('exist')
    cy.getAndValidate('void_crack', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Medium (5-10mm)')

    cy.getAndValidate('fine_macropore_abundance', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Few')

    cy.getAndValidate('medium_macropore_abundance', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Few')

    cy.getAndValidate('macropore_diameter', {
      testEmptyField: ['done', 0],
    }).selectFromDropdown('Fine')
    cy.dataCy('done').eq(0).click()
    cy.wait(50)
    // has pans
    // cy.dataCy('title').should('contain.text', 'Pan')
    cy.selectFromDropdown('pan_cementation', 'Uncemented')
    cy.selectFromDropdown('pan_type', 'Silcrete')
    cy.selectFromDropdown('pan_continuity', 'Broken')
    cy.selectFromDropdown('pan_structure', 'Platy')
    // has cutans
    // cy.dataCy('title').should('contain.text', 'Cutan')
    cy.selectFromDropdown('cutan_type', 'Mangans')
    cy.selectFromDropdown('cutan_abundance', 'Few')
    cy.selectFromDropdown('cutan_distinctness', 'Faint')
    // TODO has roots
    // cy.dataCy('title').should('contain.text', 'Roots')
    cy.selectFromDropdown('root_abundance', 'Few')
    cy.selectFromDropdown('root_size', 'Fine (1-2 mm)')

    //=================================//
    cy.selectFromDropdown('water_status', 'Wet')
    cy.selectFromDropdown('soil_strength', 'Firm')
    cy.selectFromDropdown('plasticity_type', 'Superplastic')
    cy.selectFromDropdown('effervescence', 'Not Collected')

    cy.dataCy('depth_of_field_test').last().type('1')
    cy.getAndValidate('pH').type('2')
    cy.getAndValidate('electrical_conductivity').type('3')
    cy.getAndValidate('slaking_score').selectFromDropdown(
      'Partially slaked within 10 minutes',
    )
    cy.getAndValidate('dispersion_score').selectFromDropdown(
      'Complete dispersion after 10 minutes',
    )
    cy.dataCy('done').eq(0).click()
    cy.dataCy('done').eq(-1).click()
    // cy.get('[data-cy="workflowNextButton"]').click()
  })
  it('soil horizon 2 ', () => {
    // === second horizon ===
    cy.dataCy('addEntry').eq(-1).click()

    cy.dataCy('horizon_').last().selectFromDropdown('AB')

    // cy.get('[data-cy="upper_depth"]').eq(-1).type('1')
    cy.get('[data-cy="lower_depth"]').eq(-1).type('5')
    cy.selectFromDropdown('boundary_distinctness', 'Sharp (<5mm)', null, -1)
    cy.selectFromDropdown('boundary_shape', 'Wavy', null, -1)
    cy.selectFromDropdown('texture_grade', 'Loam', null, -1)
    cy.selectFromDropdown('texture_qualification', 'Heavy', null, -1)
    cy.selectFromDropdown('texture_modifier', 'Fine sandy', null, -1)
    cy.selectFromDropdown('water_status', 'Dry', null, -1)
    cy.selectFromDropdown('soil_strength', 'Loose', null, -1)
    cy.dataCy('done').eq(-1).click()
    cy.nextStep()
  })
  it('Australian soil classification', () => {
    /* ==== sixth step === */
    cy.getAndValidate('confidence_level').selectFromDropdown('2')
    cy.getAndValidate('order').selectFromDropdown('Kandosols', 'kan')
    cy.getAndValidate('suborder').selectFromDropdown('Aeric', 'Aeric')
    cy.getAndValidate('great_group').selectFromDropdown('Basic', 'Basic')
    cy.getAndValidate('subgroup').selectFromDropdown('Black', 'Black')

    cy.getAndValidate('family').selectFromDropdown('Thin', 'Thin')

    // cy.get('[data-cy="workflowNextButton"]').click()
    cy.nextStep()
  })
  it('soil classification', () => {
    /* ==== seventh step === */
    cy.getAndValidate('permeability').selectFromDropdown('Slowly permeable')
    cy.getAndValidate('drainage').selectFromDropdown('Well')
    cy.getAndValidate('substrate_lithology').selectFromDropdown(
      'Granite',
      'Granite',
    )

    cy.getAndValidate('depth_r_horizon').type('5')
    cy.getAndValidate('soil_profile_comments').type(
      'This is a comment{enter}This is another one',
    )
  })

  it('publish', () => {
    cy.nextStep()
  })
  pub({
    callbackFunction: () => {
      // cy.get('[role="dialog"] > .q-dialog__inner > .q-card__actions')
      // check the button takes you to soil density protocol
      cy.get('[data-autofocus="true"] > .q-btn__content').click()
      cy.dataCy('title')
        .eq(0)
        .should('contain.text', 'Soil Bulk Density Survey')
        .then(() => {
          // cy.visit('/projects')
          cy.get('.q-btn').contains('back').eq(0).click()
        })
    }
  })
})
describe('lab protocol', () => {
  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
  it('soil pit location', () => {
    /* ==== first step === */
    cy.get('[data-cy="Lab"]').click()

    cy.selectFromDropdown('field_survey_id', soilPitId, null, null, null, true)

    cy.nextStep()
  })

  it('soil horizon sample 1', () => {
    cy.selectFromDropdown(
      'soil_horizon',
      '6M5f (upper depth: 1m - lower depth: 3m)',
    )
    cy.dataCy('upper_depth').type('1')
    cy.dataCy('lower_depth').type('2')
    cy.dataCy('recordBarcode').click()
    cy.dataCy('comments').type(
      'This is a comment{enter}This is another one{enter}And even another',
    )
    cy.dataCy('done').eq(0).click()
  })
  it('soil horizon sample 2', () => {
    cy.dataCy('addEntry').click()
    cy.selectFromDropdown(
      'soil_horizon',
      'AB (upper depth: 3m - lower depth: 5m)',
    )
    cy.dataCy('upper_depth').type('3')
    cy.dataCy('lower_depth').type('5')
    cy.dataCy('recordBarcode').click()
    cy.dataCy('comments').type(
      'This is a comment{enter}This is another one{enter}And even another',
    )
    cy.dataCy('done').eq(-1).click()
  })
  it('publish', () => {
    cy.nextStep()
  })
  pub({
    callbackFunction: () => {
      // cy.get('[role="dialog"] > .q-dialog__inner > .q-card__actions')
      // check the button takes you to soil density protocol
      cy.get('[data-autofocus="true"] > .q-btn__content').click()
      cy.dataCy('title')
        .eq(0)
        .should('contain.text', 'Soil Bulk Density Survey')
        .then(() => {
          // cy.visit('/projects')
          cy.get('.q-btn').contains('back').eq(0).click()
        })
    }
  })
})

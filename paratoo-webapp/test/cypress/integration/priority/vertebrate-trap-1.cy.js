// trapping setup survey
// each vert trapping test is numbered , so it call executed in order

const { pub } = require("../../support/command-utils")

//  setup traps -> check traps -> end or remove traps
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const module = 'vertebrate fauna'
const protocol = 'Vertebrate Fauna - Trapping Survey Setup'
const VERT_TRAP_TYPE = Cypress.env('VERT_TRAP_TYPE')

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })

  it('.should() - do plot layout', () => {
    //override the selected plot to one that has a fauna plot already defined
    cy.plotLayout(false, null, {
      layout: 'QDASEQ0003',
      visit: 'QLD spring survey',
    })
  })

  it('.should() - assert navigation to ' + protocol + ' is possible', () => {
    cy.selectProtocolIsEnabled(module, protocol)
  })
})

describe('Complete survey', () => {
  it('.should - complete survey', () => {
    cy.get('[data-cy="title"]')
      .should('contain.text', 'Setup trapping survey', { timeout: 300000 })
      .then(() => {
        cy.nextStep()
      })
  })
})
describe('Marking trap lines and traps', () => {
  it(`start drift fence B`, function () {
    // testing end trap line when trap data is not added
    cy.getAndValidate('traps', {
      testEmptyField: ['done', -1],
      isComponent: true,
    })
    // TODO: related to this issue
    // #1340 https://gitlab.com/ternandsparrow/paratoo-fdcp/-/issues/329?work_item_iid=1340
    // cy.getAndValidate('fence_type', {
    //   testEmptyField: ['done', -1],
    // })
    // cy.getAndValidate('fence_height', {
    //   testEmptyField: ['done', -1],
    // })
    cy.setExpansionItemState('B_expansion', 'open')
    cy.vertStartTrapLine(true)
  })

  it('mark pitfall traps and funnel traps within fence B', function () {
    cy.setExpansionItemState('mark_trap', 'open', -1)
    cy.getAndValidate('trap_setup_date', {
      testEmptyField: ['done', -1],
    })
    cy.getAndValidate('barcode', {
      testEmptyField: ['done', -1],
      isComponent: true,
    })
    cy.getAndValidate('trap_type', {
      testEmptyField: ['done', -1],
    })
    cy.getAndValidate('trap_depth', {
      testEmptyField: ['done', -1],
    })
    cy.getAndValidate('trap_width', {
      testEmptyField: ['done', -1],
    })
    cy.getAndValidate('trap_specifics', {
      testEmptyField: ['done', -1],
      isComponent: true,
    })
    cy.getAndValidate('trap_photo', {
      testEmptyField: ['done', -1],
      isComponent: true,
    })
    cy.getAndValidate('comment', {
      testEmptyField: ['done', -1],
    })

    cy.getAndValidate('trap_set_status', {
      testEmptyField: ['done', -1],
    })
    cy.vertMarkTrap(10)
  })

  it('End drift fence B', function () {
    cy.dataCy('done').eq(-1).click()
  })

  it(`start drift fence E`, function () {
    cy.setExpansionItemState('E_expansion', 'open')
    cy.vertStartTrapLine(true)
  })

  it('mark pitfall traps and funnel traps within fence E', function () {
    cy.vertMarkTrap(10)
  })

  it('End drift fence E', function () {
    cy.vertEndTrapLine('E')
  })

  // ==========================================
  it(`start trap line A`, function () {
    cy.setExpansionItemState('A_expansion', 'open')
  })

  it('mark cage traps and elliott traps in line A', function () {
    cy.setExpansionItemState('mark_trap', 'open', -1)
    cy.getAndValidate('trap_height', {
      testEmptyField: ['done', -1],
    })
    cy.vertMarkTrap(12)
  })

  it('End line A', function () {
    cy.vertEndTrapLine('A')
  })

  // =====================================

  it('start trap line C', function () {
    cy.setExpansionItemState('C_expansion', 'open')
  })

  it('mark cage traps and elliott traps in line C', function () {
    cy.vertMarkTrap(12)
  })

  it('End line C', function () {
    cy.vertEndTrapLine('C')
  })

  // ====================================================

  it('start trap line D', function () {
    cy.setExpansionItemState('D_expansion', 'open')
  })

  it('mark cage traps and elliott traps in line D', function () {
    cy.vertMarkTrap(12)
  })

  it('End line D', function () {
    cy.vertEndTrapLine('D')
  })

  // ==========================================

  it('start trap line F', function () {
    cy.setExpansionItemState('F_expansion', 'open')
  })

  it('mark cage traps and elliott traps in line F', function () {
    cy.vertMarkTrap(12)
  })

  it('End line F', function () {
    cy.vertEndTrapLine('F')
  })

  it('Submit survey', () => {
    cy.nextStep()
    // cy.get('[data-cy="Summary"] > .q-stepper__tab > .q-stepper__dot').click()
  })
  pub()
})

const { capitalizeWords, waitForHydration } = require('../../support/command-utils')

const plotDescriptionEnhancedProtocol = 'Plot Description - Enhanced'
const protocolFloristicsFull = 'Floristics - Enhanced'
const project = 'Kitchen\\ Sink\\ TEST\\ Project'
const modulePlotDesc = 'plot description'
const moduleFloristics = 'floristics'
const layoutModule = 'Plot Selection and Layout'
const moduleBasal = 'basal area'
const protocolBasalDbh = 'Basal Area - DBH'
const moduleRecruitment = 'recruitment'
const protocolRecruitmentAge = 'Recruitment - Age Structure'

//TODO refactor (later/future): lots of duplicate logic that could be helpers/commands and can make a lot of the inside a loop
//TODO test when just the visit context differs

function resetState() {
  cy.window()
    .then(($win) => {
      $win.dataManager.$reset()
      $win.bulkStore.$reset()
    })
}

function tryEditPlotDescProtocol() {
  cy.get(`[data-cy="${capitalizeWords(modulePlotDesc)}"]`)
    .should('be.enabled')
    .click({ timeout: 20000 })
  cy.dataCy('edit')
    .should('exist')
    .click()
}

function tryEditFloristicsProtocol() {
  cy.get(`[data-cy="${capitalizeWords(moduleFloristics)}"]`)
    .should('be.enabled')
    .click({ timeout: 20000 })
  cy.dataCy('edit')
    .should('exist')
    .click()
}

function tryEditBasalDbhProtocol() {
  cy.get(`[data-cy="${capitalizeWords(moduleBasal)}"]`)
    .should('be.enabled')
    .click({ timeout: 20000 })
  cy.dataCy('edit')
    .should('exist')
    .click()
}

function tryEditRecruitmentAgeStructureProtocol() {
  cy.get(`[data-cy="${capitalizeWords(moduleRecruitment)}"]`)
    .should('be.enabled')
    .click({ timeout: 20000 })
  cy.dataCy('edit')
    .should('exist')
    .click()
}


function makeNewLayout({
  plotName,
  manualReferencePointCoords = {
    lat: -34.972959,
    lng: 138.640666,
  },
}) {
  cy.dataCy('locationNew').click()
  cy.dataCy('plotAlignedToGridYes').click()
  cy.selectFromDropdown('plot_selection', plotName, null)
  cy.selectFromDropdown('plot_type', 'Control', null)
  cy.dataCy('replicate').type(1)
  cy.dataCy('plot_not_walked').check()
  cy.get('[data-autofocus="true"] > .q-btn__content').click()
  cy.dataCy('choosePosition').click()
  cy.dataCy('manualCoords', { timeout: 5000 }).click().wait(500)
  //near the plot selection's recommended location
  cy.dataCy('Latitude', { timeout: 5000 }).type(
    `{backspace}${manualReferencePointCoords.lat}`,
  )
  cy.dataCy('Longitude', { timeout: 5000 }).type(
    `{backspace}${manualReferencePointCoords.lng}`,
  )
  cy.wait(1000)
  cy.dataCy('recordLocation').click()

  cy.dataCy('startLayout')
    .should('be.enabled')
    .click()
    .wait(1000)
  cy.dataCy('setStartPoint').click().wait(1000)
  cy.dataCy('markClosestPoint').should('have.text', 'Done').click()

  cy.nextStep()
  cy.nextStep()   //skip fauna plot
}

function makeNewVisit({
  visitName,
}) {
  cy.dataCy('visit_field_name').type(visitName)
  cy.nextStep()
}

function selectPlotDescription() {
  cy.selectProtocolIsEnabled(modulePlotDesc, plotDescriptionEnhancedProtocol, null, {
    //setting the count will make it not exit then re-enter the protocol (i.e., don't
    //need to test the edit button)
    count: 1,
  })
}

function selectFloristics() {
  cy.selectProtocolIsEnabled(moduleFloristics, protocolFloristicsFull, null, {
    //setting the count will make it not exit then re-enter the protocol (i.e., don't
    //need to test the edit button)
    count: 1,
  })
}

function selectPlotLayoutProtocol() {
  cy.selectProtocolIsEnabled(layoutModule, null, null, {
    count: 1,
  })
}

//checks the 'shape' of the plot context, not the actual plot/visit (which would require
//resolving all offline data, etc.)
function checkPlotContext({
  //online, offline, offline_existing
  caseType,
  expectedContext = null,   //only if `caseType` is `online`
}) {
  cy.loadLocalStore('bulk', (storeData) => {
    switch(caseType) {
      case 'online':
        cy.log(JSON.stringify(storeData.staticPlotContext))
        cy.wrap(storeData.staticPlotContext)
          .should('deep.equal', expectedContext)
        break
      case 'offline':
        for (const plotModel of ['plotLayout', 'plotVisit']) {
          cy.log(JSON.stringify(storeData.staticPlotContext[plotModel]))
          cy.wrap(storeData.staticPlotContext[plotModel])
            .should('not.have.keys', 'id')
            .and('have.any.keys', 'temp_offline_id', 'flattened_id')
        }
        break
      case 'offline_existing':
        for (const plotModel of ['plotLayout', 'plotVisit']) {
          cy.log(JSON.stringify(storeData.staticPlotContext[plotModel]))
          cy.wrap(storeData.staticPlotContext[plotModel])
            .should('have.any.key', 'id')
            .and('not.have.keys', 'temp_offline_id', 'flattened_id')
          cy.wrap(storeData.staticPlotContext[plotModel].id)
            .should('have.key', 'temp_offline_id')
        }
        break
      default:
        throw new Error(`Cannot call checkPlotContext() with unknown caseType=${caseType}`)
    }
  })
}

//need to provide at least 'actual' and 'collection' for plot and/or visit - e.g.,provide
//just actual plot and collection plot (which will only check the plot context), or can
//provide actual plot and visit and collection plot and visit (which checks both plot and
//visit)
function checkCannotEdit({
  actualPlotContext,
  actualVisitContext,
  collectionPlotContext,
  collectionVisitContext,
  protocol,
}) {
  cy.wait(3000)
  cy.dataCy('pulling-api-status', { timeout: 120000 })
    .should('not.exist', { timeout: 120000 })
  if (protocol === plotDescriptionEnhancedProtocol) {
    tryEditPlotDescProtocol()
  } else if (protocol === protocolFloristicsFull) {
    tryEditFloristicsProtocol()
  } else if (protocol === protocolBasalDbh) {
    tryEditBasalDbhProtocol()
  } else if (protocol === protocolRecruitmentAge) {
    tryEditRecruitmentAgeStructureProtocol()
  } else {
    throw new Error(`Programmer error: protocol ${protocol} not registered case`)
  }

  cy.get('.q-notification').invoke('text')
    .then(text => {
      console.log(`text: ${text}`)

      expect(
        text.match(/Cannot edit protocol '(.*)' due to differing context/g)
      ).to.have.lengthOf(1)
      

      if (collectionPlotContext && actualPlotContext) {
        expect(
          text.match(
            new RegExp(
              `Plot Layout context differs: survey was started with '${collectionPlotContext}' but the context is '${actualPlotContext}'`,
              'g',
            )
          )
        ).to.have.lengthOf(1)
      }

      if (collectionVisitContext && actualVisitContext) {
        expect(
          text.match(
            new RegExp(
              `Plot Visit context differs: survey was started with '${collectionVisitContext} (\(.*\))' but the context is '${actualVisitContext} (\(.*\))'`,
              'g',
            )
          )
        ).to.have.lengthOf(1)
      }
    })
    .then(() => {
      cy.dismissPopups()
      cy.dataCy('closeDialog').click()
    })
}

function checkCanEdit({ protocol }) {
  cy.wait(3000)
  cy.dataCy('pulling-api-status', { timeout: 120000 })
    .should('not.exist', { timeout: 120000 })
  if (protocol === plotDescriptionEnhancedProtocol) {
    tryEditPlotDescProtocol()
  } else if (protocol === protocolFloristicsFull) {
    tryEditFloristicsProtocol()
  } else if (protocol === protocolBasalDbh) {
    tryEditBasalDbhProtocol()
  } else if (protocol === protocolRecruitmentAge) {
    tryEditRecruitmentAgeStructureProtocol()
  } else {
    throw new Error(`Programmer error: protocol ${protocol} not registered case`)
  }
  cy.dataCy('workflowLoaded', { timeout: 120000 })
    .should('exist')
    .then(() => {
      //allow time for model config to be defined
      cy.dataCy('title', { timeout: 60000 }).should('exist')
      cy.wait(2000)
    })
    .then(() => {
      cy.dataCy('workflowBack').click().wait(1000)
    })
}

describe('0 - Landing', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - reset state', () => {
    resetState()
  })
})

describe('check dynamic wording of elements for plot context - set context only', () => {
  it('existing layout and visit', () => {
    cy.selectProtocolIsEnabled(layoutModule)

    cy.dataCy('locationExisting').click()
    cy.dataCy('id').selectFromDropdown('SATFLB0001')
    cy.nextStep()

    cy.dataCy('visitExisting').click()
    cy.dataCy('id').selectFromDropdown()
    cy.nextStep()
  })
  it('check queue button', () => {
    cy.dataCy('workflowPublish')
      .should('contain.text', 'Set Plot Context')
  })
  it('add to queue and check notification', () => {
    cy.dataCy('workflowPublish').click()

    cy.dataCy('confirmMessage')
      .should('contain.text', 'Do you want to change plot context?')
    cy.dataCy('queueConfirmWarning').should('not.exist')

    cy.dataCy('workflowPublishConfirm').click()
    cy.get('.q-notification', { timeout: 360000 })
      .should('exist')
      .and('contain.text', 'Successfully set plot context of the collection')
  })
})

describe('check dynamic wording of elements for plot context - new context data', () => {
  it('existing layout with new visit', () => {
    cy.selectProtocolIsEnabled(layoutModule)

    cy.dataCy('locationExisting').click()
    cy.dataCy('id').selectFromDropdown('SATFLB0001')
    cy.nextStep()

    cy.dataCy('visitNew').click()
    cy.dataCy('visit_field_name').type('new visit for context check')
    cy.nextStep()
  })
  it('check queue button', () => {
    cy.dataCy('workflowPublish')
      .should('contain.text', 'Queue Collection for Submission')
  })
  it('add to queue and check notification', () => {
    cy.dataCy('workflowPublish').click()

    cy.dataCy('confirmMessage')
      .should('contain.text', 'Confirm the collection is finished?')
    cy.dataCy('queueConfirmWarning').should('exist')

    cy.dataCy('workflowPublishConfirm').click()
    cy.get('.q-notification', { timeout: 360000 })
      .should('exist')
      .and('contain.text', 'Successfully queued the collection')
  })
})

/**
 * We test all combinations of (see table below for cases):
 *  - collection context (collection started with a given context) and/or plot context
 *    - is online ('select existing')
 *    - is offline ('create new')
 *    - is offline that has been re-selected ('select existing' for new plot in queue)
 * 
 * `ttf` = test-to-fail
 * `ttp` = test-to-pass
 * 
 * Can only do TTP for when the Plot Context and Survey context cases are the same (e.g.,
 * both 'offline existing')
 * 
 *                         Plot context
 *         |                  |    Online    |    Offline    | Offline existing |
 * Survey  |      Online      | ttp=1,ttf=2  |     ttf=3     |      ttf=4       |
 * context |      Offline     |    ttf=7     |  ttp=5,ttf=6  |      ttf=8       |
 *         | Offline existing |   ttf=12     |    ttf=11     |   ttp=9,ttf=10   |
 */


describe('[test-to-pass] 1 - Can edit protocol when context is online plot and collection was started with online plot', () => {
  it(`Select existing online plot SATFLB0001`, () => {
    cy.plotLayout(
      true,
      null,
      {
        layout: 'SATFLB0001',
        visit: 'SA autumn survey',
      },
    )
  })
  it('Check plot context', () => {
    checkPlotContext({
      caseType: 'online',
      expectedContext: {
        plotLayout: {
          id: 4,
        },
        plotVisit: {
          id: 6,
          plot_layout: 4,
        },
      },
    })
  })
  it(`Start collection for online plot SATFLB0001`, () => {
    selectPlotDescription()
    cy.dataCy('workflowBack').click().wait(1000)
  })
  it(`Check can edit`, () => {
    checkCanEdit({
      protocol: plotDescriptionEnhancedProtocol,
    })
  })
})

describe('[test-to-fail] 2 - Cannot edit protocol when context is online plot and collection was started with online plot', () => {
  it(`Select existing online plot QDASEQ0001`, () => {
    cy.plotLayout(
      true,
      null,
      {
        layout: 'QDASEQ0001',
        visit: 'QLD winter survey',
      },
    )
  })
  it('Check plot context', () => {
    checkPlotContext({
      caseType: 'online',
      expectedContext: {
        plotLayout: {
          id: 1,
        },
        plotVisit: {
          id: 1,
          plot_layout: 1,
        },
      },
    })
  })
  it('Check cannot edit', () => {
    checkCannotEdit({
      actualPlotContext: 'QDASEQ0001',
      actualVisitContext: 'QLD winter survey',
      collectionPlotContext: 'SATFLB0001',
      collectionVisitContext: 'SA autumn survey',
      protocol: plotDescriptionEnhancedProtocol,
    })
  })
})

describe('[test-to-fail] 3 - Cannot edit protocol when context is offline plot and collection was started with online plot', () => {
  it(`Create new plot SATFLB0002`, () => {
    selectPlotLayoutProtocol()
    makeNewLayout({
      plotName: 'SATFLB0002',
    })
    makeNewVisit({
      visitName: 'SATFLB0002 test visit 1'
    })
    cy.workflowPublishQueue({
      goWorkflowNext: false,    //is done by the visit step
    })
  })
  it('Check plot context', () => {
    checkPlotContext({
      caseType: 'offline',
    })
  })
  it('Check cannot edit', () => {
    checkCannotEdit({
      actualPlotContext: 'SATFLB0002',
      actualVisitContext: 'SATFLB0002 test visit 1',
      collectionPlotContext: 'SATFLB0001',
      collectionVisitContext: 'SA autumn survey',
      protocol: plotDescriptionEnhancedProtocol,
    })
  })
})

describe('[test-to-fail] 4 - Cannot edit protocol when context is existing offline plot and collection was started with online plot', () => {
  it('Select existing offline plot SATFLB0002', () => {
    cy.plotLayout(
      true,
      null,
      {
        layout: 'SATFLB0002',
        visit: 'SATFLB0002 test visit 1',
      },
    )
  })
  it('Check plot context', () => {
    checkPlotContext({
      caseType: 'offline_existing',
    })
  })
  it('Check cannot edit', () => {
    checkCannotEdit({
      actualPlotContext: 'SATFLB0002',
      actualVisitContext: 'SATFLB0002 test visit 1',
      collectionPlotContext: 'SATFLB0001',
      collectionVisitContext: 'SA autumn survey',
      protocol: plotDescriptionEnhancedProtocol,
    })
  })
})

describe('[test-to-pass] 5 - Can edit protocol when context is offline plot and collection was started with offline plot', () => {
  //set a context to start the collection with offline plot
  it(`Create new plot SATFLB0003`, () => {
    selectPlotLayoutProtocol()
    makeNewLayout({
      plotName: 'SATFLB0003',
    })
    makeNewVisit({
      visitName: 'SATFLB0003 test visit 1'
    })
    cy.workflowPublishQueue({
      goWorkflowNext: false,    //is done by the visit step
    })
  })
  it('Check plot context', () => {
    checkPlotContext({
      caseType: 'offline',
    })
  })
  it(`Start collection for online plot SATFLB0001`, () => {
    selectPlotDescription()
    cy.dataCy('workflowBack').click().wait(1000)
  })
  it(`Check can edit`, () => {
    checkCanEdit({
      protocol: plotDescriptionEnhancedProtocol,
    })
  })
})

describe('[test-to-fail] 6 - Cannot edit protocol when context is offline plot and collection was started with offline plot', () => {
  it(`Create new plot QDASEQ0005`, () => {
    selectPlotLayoutProtocol()
    makeNewLayout({
      plotName: 'QDASEQ0005',
    })
    makeNewVisit({
      visitName: 'QDASEQ0005 test visit 1'
    })
    cy.workflowPublishQueue({
      goWorkflowNext: false,    //is done by the visit step
    })
  })
  it('Check plot context', () => {
    checkPlotContext({
      caseType: 'offline',
    })
  })
  it('Check cannot edit', () => {
    checkCannotEdit({
      actualPlotContext: 'QDASEQ0005',
      actualVisitContext: 'QDASEQ0005 test visit 1',
      collectionPlotContext: 'SATFLB0003',
      collectionVisitContext: 'SATFLB0003 test visit 1',
      protocol: plotDescriptionEnhancedProtocol,
    })
  })
})

describe('[test-to-fail] 7 - Cannot edit protocol when context is online plot and collection was started with offline plot', () => {
  //set an online plot context
  it(`Select existing online plot QDASEQ0001`, () => {
    cy.plotLayout(
      true,
      null,
      {
        layout: 'QDASEQ0001',
        visit: 'QLD winter survey',
      },
    )
  })
  it('Check plot context for online plot', () => {
    checkPlotContext({
      caseType: 'online',
      expectedContext: {
        plotLayout: {
          id: 1,
        },
        plotVisit: {
          id: 1,
          plot_layout: 1,
        },
      },
    })
  })
  it('Check cannot edit', () => {
    checkCannotEdit({
      actualPlotContext: 'QDASEQ0001',
      actualVisitContext: 'QLD winter survey',
      collectionPlotContext: 'SATFLB0003',
      collectionVisitContext: 'SATFLB0003 test visit 1',
      protocol: plotDescriptionEnhancedProtocol,
    })
  })
})

describe('[test-to-fail] 8 - Cannot edit protocol when context is existing offline plot and collection was started with offline plot', () => {
  it('Select existing offline plot SATFLB0002', () => {
    cy.plotLayout(
      true,
      null,
      {
        layout: 'SATFLB0002',
        visit: 'SATFLB0002 test visit 1',
      },
    )
  })
  it('Check plot context', () => {
    checkPlotContext({
      caseType: 'offline_existing',
    })
  })
  it('Check cannot edit', () => {
    checkCannotEdit({
      actualPlotContext: 'SATFLB0002',
      actualVisitContext: 'SATFLB0002 test visit 1',
      collectionPlotContext: 'SATFLB0003',
      collectionVisitContext: 'SATFLB0003 test visit 1',
      protocol: plotDescriptionEnhancedProtocol,
    })
  })
})

describe('[test-to-pass] 9 - Can edit protocol when context is existing offline plot and collection was started with existing offline plot', () => {
  it('Clear collection', () => {
    //survey was started with offline plot, so need to clear it so we can make it's
    //context and existing offline plot
    cy.get(`[data-cy="${capitalizeWords(modulePlotDesc)}"]`)
        .should('be.enabled')
        .click({ timeout: 20000 })
    cy.dataCy(`clearCollection_${plotDescriptionEnhancedProtocol.toLowerCase().replaceAll(' ', '_')}`, { timeout: 20000 }).click({ timeout: 20000 })
    cy.dataCy('confirmClearCollection', { timeout: 20000 }).click({ timeout: 20000 })
    cy.dataCy('closeDialog').click()
  })
  it('Select existing offline plot SATFLB0002', () => {
    cy.plotLayout(
      true,
      null,
      {
        layout: 'SATFLB0002',
        visit: 'SATFLB0002 test visit 1',
      },
    )
  })
  it('Check plot context', () => {
    checkPlotContext({
      caseType: 'offline_existing',
    })
  })
  it('Start protocol with existing offline plot SATFLB0002', () => {
    selectPlotDescription()
    cy.dataCy('workflowBack').click().wait(1000)
  })
  it(`Check can edit`, () => {
    checkCanEdit({
      protocol: plotDescriptionEnhancedProtocol,
    })
  })
})

describe('[test-to-fail] 10 - Cannot edit protocol when context is existing offline plot and collection was started with existing offline plot', () => {
  it('Select existing offline plot SATFLB0002', () => {
    cy.plotLayout(
      true,
      null,
      {
        layout: 'SATFLB0003',
        visit: 'SATFLB0003 test visit 1',
      },
    )
  })
  it('Check plot context', () => {
    checkPlotContext({
      caseType: 'offline_existing',
    })
  })
  it('Check cannot edit', () => {
    checkCannotEdit({
      actualPlotContext: 'SATFLB0003',
      actualVisitContext: 'SATFLB0003 test visit 1',
      collectionPlotContext: 'SATFLB0002',
      collectionVisitContext: 'SATFLB0002 test visit 1',
      protocol: plotDescriptionEnhancedProtocol,
    })
  })
})

describe('[test-to-fail] 11 - Cannot edit protocol when context is offline plot and collection was started with existing offline plot', () => {
  it(`Create new plot QDASEQ0009`, () => {
    selectPlotLayoutProtocol()
    makeNewLayout({
      plotName: 'QDASEQ0009',
    })
    makeNewVisit({
      visitName: 'QDASEQ0009 test visit 1'
    })
    cy.workflowPublishQueue({
      goWorkflowNext: false,    //is done by the visit step
    })
  })
  it('Check plot context', () => {
    checkPlotContext({
      caseType: 'offline',
    })
  })
  it('Check cannot edit', () => {
    checkCannotEdit({
      actualPlotContext: 'QDASEQ0009',
      actualVisitContext: 'QDASEQ0009 test visit 1',
      collectionPlotContext: 'SATFLB0002',
      collectionVisitContext: 'SATFLB0002 test visit 1',
      protocol: plotDescriptionEnhancedProtocol,
    })
  })
})

describe('[test-to-fail] 12 - Cannot edit protocol when context is online plot and collection was started with existing offline plot', () => {
  it(`Select existing online plot SATFLB0001`, () => {
    cy.plotLayout(
      true,
      null,
      {
        layout: 'SATFLB0001',
        visit: 'SA autumn survey',
      },
    )
  })
  it('Check plot context', () => {
    checkPlotContext({
      caseType: 'online',
      expectedContext: {
        plotLayout: {
          id: 4,
        },
        plotVisit: {
          id: 6,
          plot_layout: 4,
        },
      },
    })
  })
  it('Check cannot edit', () => {
    checkCannotEdit({
      actualPlotContext: 'SATFLB0001',
      actualVisitContext: 'SA autumn survey',
      collectionPlotContext: 'SATFLB0002',
      collectionVisitContext: 'SATFLB0002 test visit 1',
      protocol: plotDescriptionEnhancedProtocol,
    })
  })
})

describe('cleaup', () => {
  it('.should() - reset state', () => {
    resetState()
  })
})

describe('Syncing queue with plot context - case 1 (new plot only)', () => {
  it(`Create new plot SATRAFLB1000`, () => {
    selectPlotLayoutProtocol()
    makeNewLayout({
      plotName: 'SATRAFLB1000',
    })
    makeNewVisit({
      visitName: 'SATRAFLB1000 test visit 1'
    })
    cy.workflowPublishQueue({
      goWorkflowNext: false,    //is done by the visit step
    })
  })
  it(`Start plot-based protocol ${plotDescriptionEnhancedProtocol} to save plot context reference`, () => {
    selectPlotDescription()
    cy.dataCy('workflowBack').click().wait(1000)
  })
  it('Sync queue containing new plot/visit for SATRAFLB1000', () => {
    cy.syncQueue()
  })
  it(`Check in-progress ${plotDescriptionEnhancedProtocol} has updated plot context reference correctly by trying to edit`, () => {
    checkCanEdit({
      protocol: plotDescriptionEnhancedProtocol,
    })
  })
})

describe('Syncing queue with plot context - case 2 (new plot that has had \'select existing\' done)', () => {
  it(`Create new plot SATRAFLB1001`, () => {
    selectPlotLayoutProtocol()
    makeNewLayout({
      plotName: 'SATRAFLB1001',
    })
    makeNewVisit({
      visitName: 'SATRAFLB1001 test visit 1'
    })
    cy.workflowPublishQueue({
      goWorkflowNext: false,    //is done by the visit step
    })
  })
  it(`Start plot-based protocol ${plotDescriptionEnhancedProtocol} to save plot context reference`, () => {
    selectPlotDescription()
    cy.dataCy('workflowBack').click().wait(1000)
  })
  it(`Select existing offline plot SATRAFLB1001`, () => {
    cy.plotLayout(
      true,
      null,
      {
        layout: 'SATRAFLB1001',
        visit: 'SATRAFLB1001 test visit 1',
      },
    )
  })
  it(`Start plot-based protocol ${protocolFloristicsFull} to save plot context reference`, () => {
    selectFloristics()
    cy.dataCy('workflowBack').click().wait(1000)
  })
  it('Sync queue containing new plot/visit for SATRAFLB1001', () => {
    cy.syncQueue()
  })
  it(`Check in-progress ${plotDescriptionEnhancedProtocol} has updated plot context reference correctly by trying to edit`, () => {
    checkCanEdit({
      protocol: plotDescriptionEnhancedProtocol,
    })
  })
  it(`Check in-progress ${protocolFloristicsFull} has updated plot context reference correctly by trying to edit`, () => {
    checkCanEdit({
      protocol: protocolFloristicsFull,
    })
  })
})

let contextToSpoof = null
describe('Update/migrate existing bad plot context references', () => {
  it('Get a historical data manager plot', () => {
    cy.window()
      .then(($win) => {
        const data = $win.historicalDataManagerDb.getAllTableData({ table: 'submitResponses' })
          .then((dexieData) => {
            console.log('dexieData:', dexieData)
            return Cypress._.cloneDeep(dexieData)
          })

        return data
      })
      .then((data) => {
        console.log('data:', JSON.stringify(data, null, 2))
        data.reverse()    //want to search to get most recent
        const plotContextToUse = data.find(
          d =>
            d.data?.protocolId === 4 &&
            //want a new plot/visit
            d.data?.collection?.['plot-layout']?.plot_points &&
            d.data?.collection?.['plot-visit']?.visit_field_name &&
            //want the plot we created before, as that's now the context
            d.data?.collection?.['plot-layout']?.plot_label === 'SATRAFLB1001'
        )
        console.log('plotContextToUse:', plotContextToUse)
        contextToSpoof = plotContextToUse
      })
  })
  it('Spoof an in-progress surveys to have un-migrated/un-updated context references', () => {
    console.log('contextToSpoof:', JSON.stringify(contextToSpoof, null, 2))
    cy.window()
      .then(($win) => {
        Object.assign($win.bulkStore.collections, {
          //basal DBH
          21: {
            'plot-layout': {
              temp_offline_id: contextToSpoof.data.collection['plot-layout'].temp_offline_id,
            },
            'plot-visit': {
              temp_offline_id: contextToSpoof.data.collection['plot-visit'].temp_offline_id,
            },
          },
          //recruitment age structure
          24: {
            'plot-layout': {
              id: {
                temp_offline_id: contextToSpoof.data.collection['plot-layout'].temp_offline_id
              },
            },
            'plot-visit': {
              id: {
                temp_offline_id: contextToSpoof.data.collection['plot-visit'].temp_offline_id
              },
            },
          },
        })
      })
  })
  it('Check editing is broken', () => {
    checkCannotEdit({
      actualPlotContext: 'SATRAFLB1001',
      actualVisitContext: 'SATRAFLB1001 test visit 1',
      collectionPlotContext: 'undefined',
      collectionVisitContext: 'undefined',
      protocol: protocolBasalDbh,
    })

    checkCannotEdit({
      actualPlotContext: 'SATRAFLB1001',
      actualVisitContext: 'SATRAFLB1001 test visit 1',
      collectionPlotContext: 'undefined',
      collectionVisitContext: 'undefined',
      protocol: protocolRecruitmentAge,
    })
  })
  it('Trigger auto update/migrate', () => {
    cy.reload()
  })
  it('Wait for hydration', () => {
    waitForHydration()
  })
  it('Check protocols were updated', () => {
    checkCanEdit({
      protocol: protocolBasalDbh,
    })

    checkCanEdit({
      protocol: protocolRecruitmentAge,
    })
  })
  it('Reset state', () => {
    resetState()
  })
})
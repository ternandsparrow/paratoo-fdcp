import { cloneDeep } from 'lodash'

// checks whether all the luts exist in apiModelStore or not
describe('apiModelStore all luts checking', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })
  it('.should() - all luts exist', () => {
    cy.doRequest('documentation/get-all-luts', {
      host: 'core',
    }).then((res) => {
      const mismatch = []
      const allLuts = res.body

      cy.loadLocalStore('apiModels')
        .then((apiModelsStore) => {
          const modelsNames = Object.keys(apiModelsStore.models)
          allLuts.forEach((lut) => {
            // lut-data should be present in apiModelStore
            // expect(modelsNames).to.include(lut)
            if (!modelsNames.includes(lut)) {
              mismatch.push(lut)
            }
          })

          if (mismatch.length > 0) {
            throw new Error(`Some LUTs in the app and core do not match: ${JSON.stringify(mismatch, null, 2)}`)
          }
        })
    })
  })
})

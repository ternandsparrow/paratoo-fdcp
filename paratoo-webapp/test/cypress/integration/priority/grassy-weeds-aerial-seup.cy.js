const { pub } = require('../../support/command-utils')

const protocol = 'NESP Grassy Weeds Aerial Survey - Survey Setup'
const project = 'Grassy\\ Weeds\\ Aerial\\ Survey\\ Project'

describe('Landing', () => {
  it('.should() - login', () => {
    cy.login('NESPTestUser', 'paratoo')
  })

  it('start collection button should be disabled', () => {
    cy.selectProtocolIsEnabled(null, protocol, project)
  })
})

describe('survey step', () => {
  it('survey_name', () => {
    cy.getAndValidate('survey_name', {
      testEmptyField: ['workflowNextButton'],
    }).type(`dev_grassy_survey_${Math.floor(Math.random() * 10000)}`)
  })

  it('start date/time', () => {
    cy.selectDateTime({
      fieldName: 'visit_start_date_time',
      date: {
        year: 'current',
        month: 'current',
        day: 10,
      },
      time: {
        hour: 10,
        minute: 'current',
      },
    })
  })

  it('end date/time', () => {
    cy.selectDateTime({
      fieldName: 'visit_end_date_time',
      date: {
        year: 'current',
        month: 'current',
        day: 28,
      },
      time: {
        hour: 14,
        minute: 'current',
      },
    })
  })

  it('upload file', () => {
    // TODO test different ones?
    cy.fixture('flight-lines/test.kml').as('flight-lines')
    cy.dataCy('uploadFile').paratooSelectFile('@flight-lines', { log: true })
    cy.waitForGps(300000).then(() => {
      cy.dataCy('polygonMap').wait(1000).should('exist')
    })
  })

  it('observer name', () => {
    cy.addObservers('observerName', ['Ben', 'Big Ben', 'Rather Large Ben'])
  })

  it('target species', () => {
    for (let i = 1; i <= 5; i++) {
      cy.dataCy(`target_species_${i}`).selectFromDropdown(null)
    }
    cy.dataCy(`target_species_2`).paratooClear()
    cy.dataCy(`target_species_3`).should('not.exist')
    cy.dataCy(`target_species_1`).selectFromDropdown('Para grass')
    cy.dataCy(`target_species_2`).selectFromDropdown('Gamba grass')
    cy.dataCy(`target_species_3`).should('not.have.value')
    cy.dataCy(`target_species_3`).selectFromDropdown('Mimosa')
    cy.dataCy(`target_species_4`).selectFromDropdown('Siam weed')
    cy.dataCy(`target_species_5`).selectFromDropdown('Olive Hymenahcne')
  })

  // TODO test the interactive map somehow
  // it('polygon map', () => {
  //     cy.dataCy('polygonMap')
  //         cy.get('q-btn:contains(my_location)').click()
  //         cy.get('q-btn:contains(local_airport)').click()
  // })
  pub()
})

let survey_for_collision_test

describe('test survey name collision', () => {
  it('start collection', () => {
    cy.selectProtocolIsEnabled(null, protocol, project)
  })
  it('get desktop setup to test name collision', () => {
    cy.loadLocalStore('apiModels', (apiModels) => {
      const surveys = apiModels?.models?.['grassy-weeds-survey']
      console.log('surveys:', surveys)
      const latestSetup = surveys.find(
        s => s.survey_type.symbol === 'D'
      )
      survey_for_collision_test = Cypress._.cloneDeep(latestSetup?.survey_name)
    })
  })
  it('[test-to-fail] survey name', () => {
    cy.dataCy('survey_name').type(survey_for_collision_test).blur()
    cy.dataCy('survey_name')
      .parents('.q-field')
      .contains('The input name collides with an existing Survey Name in a previous Desktop Survey Setup. Please ensure the name is unique.')
      .should('exist')
  })
  it('exit protocol', () => {
    cy.dataCy('workflowBack').click()
  })
})
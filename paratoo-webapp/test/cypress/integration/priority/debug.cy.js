// for debug page
describe('Landing', () => {
    it('.should() - login', () => {
        cy.login('TestUser', 'password')
    })
})

describe('debug page', () => {
    it('navigate to debug menu', () => {
        cy.get('[aria-label="Menu"]').click() // open sidebar
        cy.get('[data-cy="essentialLink-Debug Menu"]').click()
        cy.get('h3:contains(Debug tools)').should('exist')
    })

    // FIXME I don't know if cypress can actually interact with the email app popping up
    it.skip('email app state to tern tech', () => {
        cy.get('button:contains(Email App State to TERN Tech)').as('email-app-state-to-TERN-tech-button')
        cy.get('@email-app-state-to-TERN-tech-button').click()
        // cy.get('@email-app-state-to-TERN-tech-button).should('be.disabled')
    })

    // TODO This relies on the email state button to appear
    it.skip('import fixed state', () => {
        cy.intercept('/api/debug-client-state-dumps/1', cy.spy().as('debug-client-state-dumps-spy'))
        cy.get('button:contains(Import Fixed State)')
            .as('import-fixed-state-button')
        cy.get('@import-fixed-state-button').click()
        cy.get('.q-card')
            .should('contain.text', 'Import fixed state')
            .and('contain.text', 'Please confirm that you are aware of what you are doing.')
        cy.get('.q-card').find('button:contains(Confirm)').click()
        // assert popup for state. it has 'No fixed state found from the server' warning

        // add something to the queue and do this step again, hopefully, it should have data

        cy.get('@debug-client-state-dumps-spy').its('callCount').should('be.greaterThan', 0)

        // TODO assert the response has the same state that was previously uploaded with the email state button?
    })

    it('Constants', () => {
        //...
    })

    it('Available local voices', () => {
        //...
    })

    it('Storage status', () => {
        // ...
    })
    it('Project and protocol dump', () => {
        cy.intercept('/api/org/user-projects', cy.spy().as('data_from_org_spy'))
        cy.get('h4:contains(Project and protocol dump)')
            .siblings('button:contains(Get data from paratoo-org)')
            .click()
        cy.get('@data_from_org_spy').its('callCount').should('be.greaterThan', 0)
        // maybe something else?
    })
    it('Available Plots', () => {
        // ... 
    })

    it('Enable dev tools', () => {
        cy.get('button:contains(enable dev tools)')
            .click()
        cy.get('.q-card').find(':contains(Dev tools password)').should('exist')
        cy.get('.q-card').find('input').type('ObviouslyWrongPassword')
        cy.get('.q-card').find('button:contains(Done)').click()
        cy.get('.q-notification.bg-negative')
            .should('contain.text', 'Incorrect password')
            .find('button:contains(Dismiss)')
            .click()
            .wait(500)
        cy.get('.q-card').find('input').clear()
        // assert popup for 'Incorrect password'

        // FIXME should be using the constants.js devToolsPassword
        cy.get('.q-card').find('input').type('perentie')
        cy.get('.q-card').find('button:contains(Done)').click()
        cy.get('.q-notification.bg-positive')
            .should('contain.text', 'Dev tools enabled')
            .find('button:contains(Dismiss)')
            .click()
            .wait(500)
    })
    it('Upload to bulk store `collections`', () => {
        cy.fixture('dev_sandbox.json').then(fixture_data => {
            cy.get('h5:contains(Upload to bulk store `collections`)')
            cy.get('button:contains(Upload to bulk collections)').should('be.disabled')

            const protoId = 8
            cy.get('.q-field:contains(Protocol ID)')
                .type(protoId)
            cy.get('.q-field')
                .find(':contains(Data to upload)')
                .eq(0)
                .parent()
                .find('textarea')
                .type(' ')
                .invoke('val', JSON.stringify(fixture_data))
                .trigger('input')
            cy.get('button:contains(Upload to bulk collections)').click()
            //assert its in collections now
            cy.loadLocalStore('bulk', (bulk) => expect(Cypress._.isEqual(bulk.collections[protoId], fixture_data)).to.be.true)

            // TODO assert a bulk rejection
        })
    })
    it('Set static plot context', () => {
        cy.get('h5:contains(Set static plot context)')
        cy.get('.q-field:contains(Plot Layout)').selectFromDropdown('SATFLB0001')
        cy.get('.q-field:contains(Plot Visit)').selectFromDropdown('2020-03-29T17:05:06.421Z (SA autumn survey)')

        // if you skip the above test bulk won't be created yet
        // cy.loadLocalStore('bulk', (bulk) => {
        //     expect(bulk.staticPlotContext.plotLayout).to.be.null
        //     expect(bulk.staticPlotContext.plotVisit).to.be.null
        // })

        cy.get('button:contains(Set Plot Context)').click()

        cy.loadLocalStore('bulk', (bulk) => {
            expect(bulk.staticPlotContext.plotLayout.id).to.eq(4)
            expect(bulk.staticPlotContext.plotVisit.id).to.eq(6)
            expect(bulk.staticPlotContext.plotVisit.plot_layout).to.eq(4)
        })
        // TODO assert a bulk rejection
    })
    it('Upload (overwrite) to publication queue', () => {
        cy.fixture('dev_sandbox.json').then(fixture_data => {
            cy.get('h5:contains(Upload (overwrite) to publication queue)')
            cy.get('button:contains(Upload to publication queue)').should('be.disabled')
            cy.get('.q-field')
                .find(':contains(Data to upload)')
                .eq(4)
                .parent()
                .find('textarea')
                .type(' ')
                .invoke('val', JSON.stringify(fixture_data))
                .trigger('input')
            cy.get('button:contains(Upload to publication queue)').click()

            cy.loadLocalStore('dataManager', (dataManager) => expect(Cypress._.isEqual(dataManager.publicationQueue, fixture_data)).to.be.true)
        })
    })

    it('Clear publication queue and other data manager meta', () => {
        cy.get('h5:contains(Clear publication queue and other data manager meta)')
        cy.get('button:contains(Clear publication queue)').click()
        cy.loadLocalStore('dataManager', (dataManager) => expect(Cypress._.isEqual(dataManager.publicationQueue, [])).to.be.true)

        // submit again, no need to type it again
        cy.get('button:contains(Upload to publication queue)').click()
        cy.fixture('dev_sandbox.json').then(fixture_data => {
            cy.loadLocalStore('dataManager', (dataManager) => expect(Cypress._.isEqual(dataManager.publicationQueue, fixture_data)).to.be.true)
        })
        // if you input incorrect data for the publication queue the app will break when moving to the projects page 


        // navigate to the queue so it can be submitted
        cy.get('[aria-label="Menu"]').click()
        cy.get('[data-cy="essentialLink-Projects"]').click()



        // cy.get('button:contains(Clear data manager metadata (queue backup, errors, responses, skips, submit responses, submission progress))').click()
        
    })
    //TODO the rest of the page: dexie, helper tool for json conversion, 
    it('cleanup', () => {
        cy.logout()
    })
})

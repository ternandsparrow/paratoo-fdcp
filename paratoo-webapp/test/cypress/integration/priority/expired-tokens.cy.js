const projectCy = 'Kitchen\\ Sink\\ TEST\\ Project'
const expiredJwt =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0ODUxNDA5ODQsImlhdCI6MTQ4NTEzNzM4NCwiaXNzIjoiYWNtZS5jb20iLCJzdWIiOiIyOWFjMGMxOC0wYjRhLTQyY2YtODJmYy0wM2Q1NzAzMThhMWQiLCJhcHBsaWNhdGlvbklkIjoiNzkxMDM3MzQtOTdhYi00ZDFhLWFmMzctZTAwNmQwNWQyOTUyIiwicm9sZXMiOltdfQ.Mp0Pcwsz5VECK11Kf2ZZNF_SMKu5CgBeLN9ZOP04kZo'

describe('Refresh tokens and prepare for offline collections', () => {
  it('.should() - login', () => {
    cy.login('TestUser', 'password')
  })

  /////////////////////////////// STEP-1 ///////////////////////////////
  // Auto refresh token if token is expired but refreshToken is valid
  it('.should() - expired auth token', () => {
    cy.window().then(($win) => {
      // expired jwt
      $win.authStore.token = expiredJwt
      cy.log(`token: ${$win.authStore.token}`)
      expect($win.authStore.token).equal(expiredJwt)
    })
  })
  // token-refresh api will generate new valid jwt
  it('.should() - auto refresh token and prepare for offline collections', () => {
    cy.prepareForOfflineVisit(projectCy)
  })

  it('.should() - new valid auth token', () => {
    cy.window().then(($win) => {
      // should not be the expired one
      expect($win.authStore.token).not.equal(expiredJwt)
    })
  })

  /////////////////////////////// STEP-2 ///////////////////////////////////
  // Fail to prepare for offline collections and refresh tokens if both tokens are expired
  it('.should() - both tokens have expired', () => {
    cy.window().then(($win) => {
      // expired jwt
      $win.authStore.token = expiredJwt
      $win.authStore.refreshToken = expiredJwt
      expect($win.authStore.token).equal(expiredJwt)
      expect($win.authStore.refreshToken).equal(expiredJwt)
    })
  })

  it('.should() - fail to prepare for offline collections', () => {
    cy.prepareForOfflineVisit(projectCy, false)
  })

  it('.should() - unchanged authentication tokens', () => {
    cy.window().then(($win) => {
      // should not be changed as user needs to re-login
      expect($win.authStore.token).equal(expiredJwt)
      expect($win.authStore.refreshToken).equal(expiredJwt)
    })
  })

  ///////////////////////////////////// STEP-3 ////////////////////////////////
  // Login again to refresh both token and refreshToken
  it('.should() - re-login', () => {
    cy.logout(true)
    cy.login('TestUser', 'password')
  })

  it('.should() - new authentication tokens', () => {
    cy.window().then(($win) => {
      // should be valid tokens
      expect($win.authStore.token).not.equal(expiredJwt)
      expect($win.authStore.refreshToken).not.equal(expiredJwt)
    })
  })
})

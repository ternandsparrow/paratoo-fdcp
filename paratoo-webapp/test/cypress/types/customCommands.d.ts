declare namespace Cypress {
    interface Chainable<Subject> {
        publishCollections(force: any, atSummary: any, afterQueueCollectionCb: any): Chainable<any>
        fetchUploadedCollection(res: any, schema: any, documentation: any, documentationStore: any): Chainable<any>
        /**
         * Submits and fetches using the id's returned by a successful strapi post. Rebuilds everything by linking to the survey then checks if 
         * everything present in collections before upload exists in the database. 
         * Does not check direct values, only if a value exists.
         *
         *
         * @param {Boolean} force to force click if there's more then one or it has trouble finding it
         * @param {Number} [index] the index to use for accessing items in the bulkStore->protocol. Default to 0
         * @param {Boolean} [atSummary] whether to skip clicking the workflowNextButton if you are already on the summary page
         * @param {CallableFunction} [afterQueueCollectionCb] a callback function to execute after clicking publish confirm which can handle redirections, or other logic that needs to execute after adding to queue.
         * @param {Object} options - An optional configuration object.
         * @param {boolean} options.log - Whether to log output. Default is true.
         */
        workflowPublish(force?: boolean, submissionIndex: any, atSummary: any, suggestedProtRedirectCb?: any, skip?: boolean): Chainable<any>
        /** 
         * wish.com brand cy.wrap. Just a cy.wrap wrapper that logs the function being called 
         */
        callSync(operation: any, args: any, callback: any, toLog: any): Chainable<any>
        /**
         * Interacts with a popup map component to record coordinates. Will enter manual
         * coordinates if the gps is denied or unavailable.
         * However, this requires mocking coordinates in the ephemeralStore to get it to
         * accept manual coords.
         *
         * @param {Number} [btnEqIndex] the index of the button to interact with. If not
         * provided, the first button will be interacted with.
         * @param {Number} [recordBtnIndex] the index of the 'record' button in the modal/
         * pop-up
         * @param {Object} [coords] the override coordinates to use for manual entry.
         * Must have keys 'lat' and 'lng', both numbers
         * @param {Boolean} [log] whether to log - for passing to child commands
         *
         * @example - Record location from the first button
         * cy.recordLocation()
         *
         * @example - Record location from the last button
         * cy.recordLocation({
         *  btnEqIndex: -1,
         * })
         */
        recordLocation(btnEqIndex: Number, recordBtnIndex: Number, coords: Object, log: Boolean): Chainable<any>
        selectProtocolIsDisabled(module: any, protocol: any, project?: any): Chainable<any>
        selectProtocolIsEnabled(module: any, protocol: any, project?: any, undefined?: object): Chainable<any>
        /*check if start collection is enabled*/
        startCollectionEnabled(project: any, module: any, protocolCy: any): Chainable<any>
        workflowPublishQueue(): Chainable<any>
        workflowPublishOffline(shouldHaveData: any, afterSubmitCb?: any, workflowNextButtonIndex?: any, dumpStateFn?: any): Chainable<any>
        submitOfflineCollections(collectionsToCheck: any, collectionIterCbs: any): Chainable<any>
        /* This command always listens to the exceptions return false and will ignore these errors from failing tests.*/
        catchError(msg: any): Chainable<any>
        /**
         * Simulates a failed login attempt.
         *
         * @param {String} username - The username to use for the login attempt.
         * @param {String} password - The password to use for the login attempt.
         *
         * @example
         * cy.loginFail('invalidUsername', 'invalidPassword');
         */
        loginFail(username: any, password: any): Chainable<any>
        /* oidc Login Test Function*/
        oidcLogin(username: any, password: any, cacheReuslt: any): Chainable<any>
        /**
         * Logs user in after performing test-to-fail, then waits for data to be pulled.
         * Compatable with MERIT auth
         *
         * @param {String} username - The username to use for the login attempt.
         * @param {String} password - The password to use for the login attempt.
         *
         * @example
         * cy.login('myUsername', 'myPassword');
         */
        login(username: any, password: any): Chainable<any>
        plotLayout(forceRedo?: boolean, project?: any, plotAndVisitOverride?: any): Chainable<any>
        /**
         * Completes the weather component form found in protocols such as Bird Surveys
         *
         * @param {Object} selections an object with the keys representing the field names (precipitation, precipitation_duration, wind_description, cloud_cover, temperature) and values
         */
        completeWeatherForm(selections: any, testEmptyFieldOptions: any): Chainable<any>
        /**
         * Iterates through the inputs to test, applying those inputs, making assertions about
         * them, then clearing the field
         *
         * @param {String} field the field name of the field to test
         * @param {Object} inputsToTest an object with key specifying the input and value being another object containing the chainer and value (see: https://docs.cypress.io/api/commands/should#Syntax)
         * @param {Number} nth number for elements index on the dom
         *
         * Example `inputsToTest` (int input):
         * ```json
         * {
            '-1': {
              chainer: 'contain.text',
              value: 'Minimum: 0',
            },
            '1001': {
              chainer: 'contain.text',
              value: 'Maximum: 1000',
            },
            '': { chainer: 'contain.text', value: 'This field is required' },
          }
         * ```
         */
        testToFailValues(field: any, inputsToTest: any, nth?: number, options?: object): Chainable<any>
        /* https://stackoverflow.com/a/62189100*/
        dragMapFromCenter(prevSubject: any, element: any, undefined: any): Chainable<any>
        /**
         * @deprecated use `$win.ephemeralStore.updateUserCoords`
         * 
         * Cypress command wrapper for helper function above
         */
        mockLocation(lat: any, lng: any): Chainable<any>
        /*such as maps, we want to wait for the GPS*/
        waitForGps(timeout: any): Chainable<any>
        /**
         * Selects an item from a dropdown (q-select - `select` or `filteredSelect`)
         *
         * @param {String} fieldName the name of the field
         * @param {String|Array.<String>|null} itemToSelect the label to select, an array of labels if the dropdown can select multiple, or null if we wish to select a random element
         * @param {String} [searchQuery] a query to search when the dropdown is a `filteredSelect`
         * @param {Number} [elemIndex] the index of the element, if there is more than one on the page with the name field name
         * @param {Boolean} [isVueMultiselect] whether the dropdown is a vue `multiselect`, as opposed to Quasar `q-select`
         * @param {Boolean} regex flag to check whether the itemToSelect is a string or a regex
         *
         * Sources: https://stackoverflow.com/a/73233973, https://stackoverflow.com/a/74980973
         *
         * @example
         *  cy.get('element_name').selectFromDropdown('item_label')
         * @example
         *  cy.selectFromDropdown('element_name', 'item_label')
         * @example
         * // can be used with validateField
         *  cy.get('element_name').validateField().selectFromDropdown('item_label')
         */
        selectFromDropdown(prevSubject: any, subject: any, args: any[]): Chainable<any>
        selectFromRadioOptionGroup(fieldName: any, itemToSelect: any): Chainable<any>
        /**
         * Selects an item from the species list, similar to `selectFromDropdown()`
         *
         * @param {String} fieldName the name of the field
         * @param {String} itemToSelect the LUT label to select
         * @param {Boolean} [useFieldName] whether to use the `searchQuery` as a field name, rather than selecting a specific item. Defaults to false; cannot be used with `searchQuery`
         * @param {String} [searchQuery] a query to search when the dropdown is a `filteredSelect`. Defaults to null; cannot be used with `useFieldName`
         * @param {Number} [elemIndex] the index of the element, if there is more than one on the page with the name field name
         */
        selectFromSpeciesList(prevSubject: any, subject: any, args: any[]): Chainable<any>
        /**
         * Selects a date from the 'date' Crud element
         *
         * @param {String} fieldName the name of the field - e.g., `start_date`
         *
         * @param {Object} date an object for the date with keys `year`, `month, `day`
         * @param {String|Number} date.year the 4-digit year, or 'current' to use current year
         * @param {String|Number} date.month the full month string (e.g., 'April'), or 'current' to use current month
         * @param {String|Number} date.day the day number (e.g., '10'), or 'current' to use current day
         * @param {Boolean} [popup] whether the picker is behind a button that triggers a popup (true by default, as this is Crud's default too). Applicable when we are using a `q-date` outside of Crud
         */
        customSelectDate(undefined: any, popup?: boolean): Chainable<any>
        /**
         * Selects a time from the 'time' Crud element
         *
         * @param {String} fieldName the name of the field - e.g., `start_date`
         *
         * @param {Object} time an object for the time with keys `hour`, `minute`
         * @param {String|Number} time.hour the 24hr representation of the hour (e.g., '14' for 2pm), or 'current' to use current hour
         * @param {String|Number} time.minute the minute, or 'current' to use current minute. Rounds to nearest 5
         */
        selectTime(undefined: any): Chainable<any>
        /**
         * Selects a date and time from the 'datetime' Crud element
         *
         * @param {String} fieldName the name of the field - e.g., `start_date`
         *
         * @param {Object} date an object for the date with keys `year`, `month, `day`
         * @param {String|Number} date.year the 4-digit year, or 'current' to use current year
         * @param {String|Number} date.month the full month string (e.g., 'April'), or 'current' to use current month
         * @param {String|Number} date.day the day number (e.g., '10'), or 'current' to use current day
         *
         * @param {Object} time an object for the time with keys `hour`, `minute`
         * @param {String|Number} time.hour the 24hr representation of the hour (e.g., '14' for 2pm), or 'current' to use current hour
         * @param {String|Number} time.minute the minute, or 'current' to use current minute. Rounds to nearest 5
         */
        selectDateTime(undefined: any): Chainable<any>
        /* Logout Test Function*/
        logout(keepLocalData?: boolean): Chainable<any>
        testEmptyField(message: any, btn?: string, index?: any, q_notification_index?: any): Chainable<any>
        assertPlotContext(plotLayoutLabel: any, visitFieldName: any): Chainable<any>
        /**
         * adds images in a multi media component
         *
         * @param {Number} amount how many images to take
         * @param {String} comment an optional comment to add
         * @param {Number} amountToRemove integer of how many images to be deleted
         * @param {Number} timeout timeout in ms to apply to all gets
         */
        addImages(fieldName: any, amount?: number, undefined?: object): Chainable<any>
        /**
         * adds audio in a multi media component
         * @param {String} type name of the multimedia type, e.g., 'Video'
         * @param {Number} amount how many of the media to take
         * @param {Number} duration time in ms to record for
         * @param {String} comment an optional comment to add
         * @param {Number} amountToRemove integer of how many to be deleted
         * @param {Number} timeout timeout in ms to apply to all cy.gets
         * @param {Boolean} testPlayback whether to test playback or not
         * @param {Boolean} dontRunHeadless whether to continue the function if running in headless mode
         */
        addMultiMedia(type: any, amount?: number, duration?: number, undefined?: object): Chainable<any>
        pullAllApiModels(): Chainable<any>
        getAuthToken(undefined: any): Chainable<any>
        /**
         * Make a network request to core or org.
         * Should only be run BEFORE moving to the module/protocol if being used to delete or update items
         *
         * @param {String} protocol name of the protocol. Must be plural
         * @param {String} method REST method for url
         * @param {String} id strapi id
         * @param {String} port url port to swap between strapi or org. Default is core
         * @param {Object} args additional parameters for url. e.g. populate=* -> {'populate': '*'}
         */
        doRequest(protocol: any, undefined?: object): Chainable<any>
        /**
         * Selects all the names given from an array. If a name wasn't found it in the list it
         * will be typed in after, adding it to the list.
         *
         * @param {String} fieldName the data-cy of the field
         * @param {Array} names The names of the observers you want added as strings
         * @param {Number} [elemIndex] the index of the element, if there is more than one on the page with the name field name
         */
        addObservers(fieldName: any, names: any, undefined?: object): Chainable<any>
        /**
         *
         * @deprecated use cy.get() and chain .toggle() instead
         *
         * sets state of an radio or checkbox. Will not change is already desired state.
         *
         * @param {String} dataCy data-cy name to select
         * @param {Boolean} desiredState what boolean state you want the element to be in
         * @param {Number} [index] data-cy index
         *
         * @returns the data-cy element
         */
        setCheck(dataCy: any, desiredState?: boolean, undefined?: object): Chainable<any>
        /**
         * chainable replacement for setCheck. Clicks checkbox and radio to the desired state given
         * Unsafe to chain further as it changes the DOM element, meaning it is not guaranteed subject is the same element now.
         *
         * @param {Boolean} desiredState what boolean state you want the element to be in.
         *
         * @returns the subject given
         */
        toggle(prevSubject: any, subject: any, desiredState?: boolean): Chainable<any>
        /**
         * sets state of an expansion item. Will not change is already desired state.
         *
         * @param {String} fieldName data-cy name to select
         * @param {String} desiredState what state you want the expansion item to be in. "open" or "closed"
         * @param {Number} [index] data-cy index
         *
         * @returns cy.get query with the expansion fieldname
         */
        setExpansionItemState(fieldName: any, desiredState: any, index?: any): Chainable<any>
        prepareForOfflineVisit(project?: any): Chainable<any>
        newLayoutAndVisit(plot: any, coords: any, doFaunaPlot: any, visit: any): Chainable<any>
        /**
         * This command is used to navigate to the next step in a workflow.
         * It first clicks the 'workflowNextButton' at the specified index, then it checks if the next step exists and if the title of the next step is correct.
         * The command can also accept an options object that can include a `force` option to force the click and an `index` option to specify the index of the 'workflowNextButton'.
         *
         * @param {Object} [options] - An options object that can include a `force` option to force the click and an `index` option to specify the index of the 'workflowNextButton'.
         * @param {boolean} [options.force=false] - A boolean indicating whether to force the click.
         * @param {number} [options.index=0] - A number indicating the index of the 'workflowNextButton'.
         * @example
         * // Navigate to the next step in a workflow
         * cy.nextStep({ force: true, index: 1 })
         * @example
         * cy.nextStep()
         */
        nextStep(undefined?: object): Chainable<any>
        getSummaryAsObject(): Chainable<any>
        /**
         * This command is used to dismiss all open popups in the application.
         * It iterates over all the notification states in the window's ephemeralStore and clicks the dismiss button for each open popup.
         * If a popup is not found or has already been dismissed, it logs an error message.
         * Uses the underlying jQuery object of Cypress to avoid built in assertions, as this isnt for testing, just removing popups due to their odd behaviour.
         *
         * @example
         * // Dismiss all open popups
         * cy.dismissPopups()
         */
        dismissPopups(): Chainable<any>
        /**
         * Child command that uses a given documentation model to auto-test fields constraints. Uses the subjects data-cy to match find matches.
         * It is assumed data-cy's are the same, but has yet to be tested 100%.
         * Doesnt do everything, will still need to do some case-by-case testing yourself. e.g., custom rules
         *
         * testEmptyField logic will need to be passed in seperately if inside observations as they need to click 'done' instead. Otherwise, it is assumed to be the last workFlowNextButton
         *
         * @param {object} validationSchema schema that is returned from cy.currentProtocolWorkflow()
         * @param {object} testEmptyField object contains an array that contains the 2nd and/or 3rd arguments for testEmptyField
         * @returns {Cypress.Chainable} the subject
         *
         * @example
         * // schema is usually gotten earlier in the tests
         * let rules
         * cy.documentation().currentProtocolWorkflow().then((model) => rules = model)
         *
         * // then use the rules object to pass into any validateField call
         * cy.get('field_name').validateField()
         * @example
         * // can inject it into any exisiting chains
         * cy.get('field_name').type('blah blah')
         * // into
         * cy.get('field_name').validateField( ['workflowNextButton', -1]).type('blah blah')
         * or
         * cy.get('field_name').validateField().type('blah blah')
         */
        validateField(prevSubject: any, subject: any, testEmptyField?: any, isComponent?: boolean, isChildOf?: any): Chainable<any>
        getAndValidate(prevSubject: any, subject: any, value: any, validateFieldOptions?: object, options: any, index?: number): Chainable<any>
        /**
         * This command is used to load a local store.
         * It first checks if the provided store name is valid.
         * Then it retrieves all local storage and parses the store with the provided name.
         * The command can also accept a callback function that will be called with the store and the subject as arguments.
         * Can be chained off of itself. When chained off itselt, the yielded result will be the 2nd argument of the child.
         *
         * @param {string} storeName - The name of the store to load.
         * @param {Function} [callback] - A callback function that will be called with the store and the subject as arguments.
         * @returns {Cypress.Chainable} The command returns a Cypress Chainable object. If a callback function is provided, the command will return the result of the callback function. Otherwise, it will return the store.
         * @example
         * // Load a local store
         * cy.loadLocalStore('auth').then((store) => {
         * // Your logic here
         * })
         * @example
         * // Load a local store and call a callback function
         * cy.loadLocalStore('auth', (store, subject) => {
         * // Your callback logic here
         * })
         */
        loadLocalStore(prevSubject: any, subject: any, storeName: any, callback: any): Chainable<any>
        /**
         * This command is used to get the current protocol and its associated workflow after getting the documentation.
         * It first loads the 'auth' and 'apiModels' local stores, then it retrieves the protocol id from the 'auth' store and the workflow from the 'apiModels' store.
         * It then maps over the workflow to get the properties of each model in the workflow.
         * The command can also accept a callback function that will be called with the models, context, and workflow as arguments.
         *
         * @param {Function} [callback] - A callback function that will be called with the models, context, and workflow as arguments.
         * @returns {Cypress.Chainable} The command returns a Cypress Chainable object. If a callback function is provided, the command will return the result of the callback function. Otherwise, it will return the models.
         * @example
         * // Get the current protocol and its associated workflow
         * cy.documentation().currentProtocolWorkflow().then((models) => {
         *  // Your logic here
         * })
         * @example
         * // Get the current protocol and its associated workflow and call a callback function. can use one or both.
         * cy.documentation().currentProtocolWorkflow((models, [documentation, docStore]) => {
         *  // Your callback logic here
         * })
         */
        currentProtocolWorkflow(prevSubject: any, subject: any, callback: any): Chainable<any>
        /**
         * Retrieves the documentation store from the window object and returns the documentation and the store as an array.
         *
         * @returns {Promise<Array>} A promise that resolves to an array containing the documentation and the store.
         *
         * @example
         * // Retrieve the documentation store
         * cy.documentation().then(([documentation, store]) => {
         *  // Do something with the documentation and the store
         * })
         * @example
         * cy.documentation((documentation) => {// do something})
         * cy.documentation((documentation, store) => {// do something})
         */
        documentation(callback: any, undefined?: object): Chainable<any>
        /**
         * Checks the VUE_APP_MODE_OF_OPERATION env is set to the desired state. Useful for tests
         * such as cover PI that expect less points per transect during tests.
         *
         * This should not be called at the top of the test, as the app probably is not
         * initialised and thus `modeOfOperation` was probably not set on the `window` object yet
         *
         * @param {Number} [desiredMode] the desired mode of operation - 0 for dev, 1 for prod.
         * Default 0 as we typically test in this mode
         */
        checkModeOfOperation(desiredMode?: number): Chainable<any>
        dumpState(undefined: any): Chainable<any>
        /*TODO unify with non-offline basal test logic (this logic was broken-out while the non-offline test was being extended with field constraints)*/
        basalWedgeSamplingPoint(samplingPoint: any, vouchers: any, testToFail?: boolean): Chainable<any>
        clearDropdown(prevSubject: any, subject: any): Chainable<any>
        checkFireSubmoduleAuthContext(): Chainable<any>
        /**
         * recursively traverses the dom tree to find the first match of a given selector by looking at children of each parent. It will return the last element if multiple are found.
         * If your selector is just a plain name it will be defaulted to [data-cy="your_selector"]. Otherwise it should work with all selectors if you give it one.
         * @param {[string | Array.<string>]} selector - The selector or array of selectors to find the nearest element.
         * 
         * @param {number} [triesForIndex=3] Number of attempts to find the index initiall unless a backup has been supplied.
         * @param {boolean} [log=false] Whether to log the commands at each step. HUGE performance hits if true
         * @returns {Cypress.Chainable<JQuery<HTMLElement>>} Returns the found element.
         * @throws {Error} Will throw an error if the nearest element is not found and it reaches the top of the the dom tree.
         * @example
         * // Get the nearest element with the data-cy attribute of 'my-element'
         * cy.get('body').nearest('[data-cy="my-element"]');
         */
        nearest(prevSubject: any, subject: any, selector?: any[], undefined?: object): Chainable<any>
        /**
         * Wrapper for getNoAssert to handle it returning true instead of an object.
         * @param {string | Array.<string>} selector - The selector or array of selectors to find the nearest element.
         * @param {object} [options] - Options for the command.
         *  
         * @returns {Cypress.Chainable<JQuery<HTMLElement>>} Returns found element or an empty jquery object. 
         */
        $get(selector: any, options?: object): Chainable<any>
        isUnique(prevSubject: any, subject: any, proto: any, valueToCheck?: any): Chainable<any>
        /**
         * Generates a 2D array from a given table element
         * 
         * @return 2D array of data data.
         */
        paratooTable(prevSubject: any, subject: any): Chainable<any>
        /**
         * Cypress selectFile wrapper with added assertions to wait for a complete upload for paratoo-webapp specific uploads fields. 
         * @param files — The file(s) to select or drag onto this element.
         * @see — https://on.cypress.io/selectfile
        */
        paratooSelectFile(prevSubject: any, subject: any, file: any, options?: object): Chainable<any>
        /**
         * Clicks the circular clear button to the right of input fields.
         */
        paratooClear(prevSubject: any, subject: any): Chainable<any>
        /**
         * Opens the helper dialog on the right of a input field, executes the given callback function, then closes the dialog.
         * @param {function} callback commands to execute in within the opened dialog.
         * @param {object} options generic object that is treated the same way Cypress does on its out-of-the-box commands  .
         */
        paratooHelperDialog(prevSubject: any, subject: any, callback: any, options: any): Chainable<any>
        /**
         * Generic dialog opener for input fields. Accepts a query to search for any button within the elements .q-field parent. 
         * @param {string} query query for a cy.get() when looking for the button in the input field. Scoped under the parent .q-field.
         * @param {function} callback commands to execute in within the opened dialog.
         * @param {object} options generic object that is treated the same way Cypress does on its out-of-the-box commands.
         */
        paratooDialog(prevSubject: any, subject: any, query: any, callback: any, options: any): Chainable<any>
        cameraTrapDeploymentSurvey(surveyLabel?: any): Chainable<any>
        cameraTrapDeploymentSurveyPlannedPointsJSON(surveyLabel?: any): Chainable<any>
        cameraTrapDeploymentSurveyPlannedPointsCSV(surveyLabel?: any): Chainable<any>
        cameraTrapDeploymentPoint1Pt1(pointId?: any): Chainable<any>
        cameraTrapDeploymentPoint1Features(rules: any): Chainable<any>
        cameraTrapDeploymentPoint1CameraInfo(rules: any): Chainable<any>
        cameraTrapDeploymentPoint1CameraSettings(rules: any): Chainable<any>
        cameraTrapDeploymentPoint1Pt2(rules: any): Chainable<any>
        cameraTrapDeploymentPoint2Pt1(pointId?: any): Chainable<any>
        cameraTrapDeploymentPoint2Features(): Chainable<any>
        cameraTrapDeploymentPoint2CameraInfo(): Chainable<any>
        cameraTrapDeploymentPoint2CameraSettings(): Chainable<any>
        cameraTrapDeploymentPoint2Pt2(): Chainable<any>
        cameraTrapDeploymentPoint3Pt1(pointId?: any): Chainable<any>
        cameraTrapDeploymentPoint3Features(): Chainable<any>
        cameraTrapDeploymentPoint3CameraInfo(): Chainable<any>
        cameraTrapDeploymentPoint3CameraSettings(): Chainable<any>
        cameraTrapDeploymentPoint3Pt2(): Chainable<any>
        cameraTrapReequippingPoint1(cameraToReequip: any): Chainable<any>
        cameraTrapReequippingPoint2(cameraToReequip: any): Chainable<any>
        cameraTrapReequippingPoint3(cameraToReequip: any): Chainable<any>
        cameraTrapRetrievalPoint1(cameraToRetrieve: any): Chainable<any>
        cameraTrapRetrievalPoint2(cameraToRetrieve: any): Chainable<any>
        cameraTrapRetrievalPoint3(cameraToRetrieve: any): Chainable<any>
        doDeploymentStartDateAndDeploymentId(ctPointIdToUse: any): Chainable<any>
        fillInFloristicsLiteForm(recollections: any, recollectOnly?: boolean): Chainable<any>
        collectFireCharObservations(): Chainable<any>
        /**
         * Collects survey step for Floristics when doing PTV as submodule
         * 
         * @param {'full' | 'lite'} submoduleProtocolVariant the variant of the PTV submodule
         */
        ptvSubmoduleInFloristicsSurveyStep(submoduleProtocolVariant: any): Chainable<any>
        /**
         * Collects a single Plant Tissue Voucher as a submodule in Floristics
         *
         * @param {Number} index the Floristics voucher index
         * @param {String} variant the floristics voucher variant 'full' or 'lite'
         * @param {Boolean} [offline] whether we're collecting offline (to append to field name)
         */
        collectPtvAsSubmodule(index: any, variant: any, offline?: boolean): Chainable<any>
        /**
         * Collects Recruitment Age Structure Growth Form and Life Stage step
         *
         * @param {Array} speciesToCollect an array of objects like (for vouchers):
         * {
         *  fieldDataCy: 'vouchers',
         *  selection: 'Voucher Full Example 2',
         *  seedlingCount: 5,
         *  saplingCount: 3,
         *  juvenileCount: 0,
         * }
         * or for new species (don't specify `itemToSelect` if you want a field name):
         * {
         *  fieldDataCy: 'species',
         *  selection: {
         *    searchQuery: 'acacia',
         *    itemToSelect: 'Acacia abbatiana [Species]',
         *  },
         *  seedlingCount: 2,
         *  saplingCount: 0,
         *  juvenileCount: 8,
         * },
         */
        collectRecruitmentGrowthFormAndLifeStages(speciesToCollect: any): Chainable<any>
        /**
         * Collects Recruitment Age Structure Sapling and Seedling Count step
         *
         * @param {Array} speciesToCollect see `collectRecruitmentGrowthFormAndLifeStages`
         */
        collectSaplingAndSeedlingCount(speciesToCollect: any): Chainable<any>
        /**
         * Collects the condition protocol - wraps the `conditionStandard` and
         * `conditionEnhanced` commands to handle vouchers
         *
         * @param {String} protocolVariant the protocol variant - 'standard' or 'enhanced'
         * @param {Array.<String>} [vouchersToCollect] the list of vouchers to collect where each
         * str in the array is a voucher field name. Defaults to pre-defined list if not provided
         */
        condition(undefined: any): Chainable<any>
        addVegetationAssociationInformation(numberOfTimesToRun?: number): Chainable<any>
        completeSignObserved(): Chainable<any>
        fillPlotDescriptionEnhancedForm(): Chainable<any>
        fillPlotDescriptionStandardForm(): Chainable<any>
        checkPlotNameReserved(undefined: any): Chainable<any>
        /*check if a given plot is not reserved more than once*/
        checkPlotNameNotRereserved(undefined: any): Chainable<any>
        checkPlotNameUnreserved(undefined: any): Chainable<any>
        /**
         * Completes a single point intercept point by pseudo-randomly inputting data
         *
         * @param {String} transect the full LUT label string - e.g., 'South 1'
         * @param {Number} pointIndex the point number index
         * @param {Boolean} [isLite] whether the protocol variant is lite, which determines how
         * species intercepts are made
         * @param {Boolean} [isFire] whether Fire Survey is being completed as a submodule
         * @param {Boolean} [isOffline] whether we are collecting offline - will force vouchers
         * containing 'offline' to be collected more often, as offline Floristics collections use
         * field names in this way
         */
        completePointInterceptPoint(transect: any, pointIndex: any, isLite?: boolean, isFire?: boolean, isOffline?: boolean): Chainable<any>
        /**
         * Collects recruitment survivorship survey
         *
         * @param {Array.<Object>} vouchers the list of vouchers, where each object has at least
         * the `field_name` key
         * @param {Boolean} isNewVisit whether this is a new visit (false assumes revisit)
         *
         * TODO handle when running against dev (or non-clean instance) and we already have collection(s) for the new visit (which will actually just be a revisit)
         * possible approaches:
         *   - cleanup old collections for the visit
         *   - create a new plot (but will require cleanup too most likely)
         *   - detect if we're against a non-clean instance (already have collections) and skip
         *     the new visit test
         */
        recruitmentSurvivorshipSurvey(undefined: any): Chainable<any>
        /**
         * Collects a single recruitment survivorship observation
         *
         * @param {Array.<Object>} vouchers the list of vouchers, where each object has at least
         * the `field_name` key
         * @param {Boolean} shouldAddEntry whether to add a new observation entry
         */
        recruitmentSurvivorshipObservation(undefined: any): Chainable<any>
        soilCharacterisationPitLocation(rules: any): Chainable<any>
        soilCharacterisationLandformElements(): Chainable<any>
        soilCharacterisationSurfacePhenomenaSoilDevelopment(): Chainable<any>
        soilCharacterisationMicrorelief(): Chainable<any>
        soilCharacterisationErosionObservation(rules: any): Chainable<any>
        soilCharacterisationSurfaceCoarseFragmentObservations(rules: any): Chainable<any>
        soilCharacterisationRockOutcropObservation(rules: any): Chainable<any>
        soilCharacterisationSoilPitObservation(rules: any): Chainable<any>
        soilCharacterisationSoilHorizonObservation(rules: any): Chainable<any>
        soilCharacterisationAustralianSoilClassification(rules: any): Chainable<any>
        soilCharacterisationSoilClassification(rules: any): Chainable<any>
        soilBulkDensitySurvey(soilPitId?: any): Chainable<any>
        /**
         * for protocols that follow transect setup / survey setup pattern
         * e.g., sign-based off-plot, herbivory off-plot
         */
        offPlotTransectSetup1New(): Chainable<any>
        offPlotTransectSetup2New(): Chainable<any>
        offPlotTransectSetup3New(): Chainable<any>
        offPlotSurveySetup1(transectSetupId: any, protocol: any): Chainable<any>
        offPlotSurveySetup2(transectSetupId: any, protocol: any): Chainable<any>
        vertStartTrapLine(isFence?: boolean): Chainable<any>
        vertEndTrapLine(): Chainable<any>
        vertMarkTrap(numberOfTraps: any): Chainable<any>
        vertCheckTrap(trapNumber: any, status: any): Chainable<any>
        vertGeneralInfoOfTrapWithCapture(trapNumber: any, speciesClass: any, species: any): Chainable<any>
        vertRecordMeasurements(measurementFields: any, speciesLut: any): Chainable<any>
        vertRecordReproductiveTraits(trapNumber: any): Chainable<any>
        vertRecordSexAndAgeClass(): Chainable<any>
        vertRecordBodyAndSkinCondition(trapNumber: any): Chainable<any>
        vertRecordClinicalScoring(trapNumber: any): Chainable<any>
  }
}
import { registerCommands } from "@quasar/quasar-app-extension-testing-e2e-cypress"

Cypress.Commands.add('checkPlotNameReserved', ({ plotLabel, shouldBeCoreGenerated = false }) => {
  cy.log(`Checking that plot with name '${plotLabel}' was correctly reserved`)
  cy.wait(1000)
  cy.doRequest('reserved-plot-labels', {
    args: {
      'use-default': true,
      'use-cache': false,
    }
  })
    .then((reservedPlotLabelsResp) => {
      console.log('checkPlotNameReserved reservedPlotLabelsResp:', reservedPlotLabelsResp)
      // console.log('reservedPlotLabelsResp:', JSON.stringify(reservedPlotLabelsResp?.body?.data))
      const foundReserved = reservedPlotLabelsResp?.body?.data?.find(
        (reservedPlot) => reservedPlot?.attributes?.plot_label === plotLabel,
      )
      console.log('checkPlotNameReserved foundReserved:', foundReserved)
      cy.wrap(foundReserved)
        .should('not.be.empty')
      cy.wrap(foundReserved?.attributes?.plot_label)
        .should('eq', plotLabel)

      const provenanceKeys = [
        'id',
        'project_id',
        'org_opaque_user_id',
        'version_app',
        'version_core_documentation',
        'is_core_generated',
      ]
      cy.wrap(foundReserved?.attributes?.reservation_provenance)
        .should('have.keys', ...provenanceKeys)
      for (const key of provenanceKeys) {
        cy.wrap(foundReserved?.attributes?.reservation_provenance?.[key])
          .should('not.be.null')
          .and('not.be.undefined')
      }
      cy.wrap(foundReserved?.attributes?.reservation_provenance?.is_core_generated)
        .should(`be.${shouldBeCoreGenerated}`)
    })
})

//check if a given plot is not reserved more than once
Cypress.Commands.add('checkPlotNameNotRereserved', ({ plotLabel }) => {
  cy.wait(1000)
  cy.doRequest('reserved-plot-labels', {
    args: {
      'use-default': true,
      'use-cache': false,
    }
  })
    .then((reservedPlotLabelsResp) => {
      console.log('checkPlotNameNotRereserved reservedPlotLabelsResp:', reservedPlotLabelsResp)
      const foundReserved = reservedPlotLabelsResp?.body?.data?.filter(
        (reservedPlot) => reservedPlot?.attributes?.plot_label === plotLabel,
      )
      console.log('checkPlotNameNotRereserved foundReserved:', foundReserved)
      cy.wrap(foundReserved)
        .should('not.be.empty')
      //we did a `filter` so if the plot was reserved more than once, we'll have length>1
      cy.wrap(foundReserved?.length)
        .should('eq', 1)
    })
})

Cypress.Commands.add('checkPlotNameUnreserved', ({ plotLabel }) => {
  cy.wait(1000)
  cy.doRequest('reserved-plot-labels', {
    args: {
      'use-default': true,
      'use-cache': false,
    }
  })
    .then((reservedPlotLabelsResp) => {
      console.log('checkPlotNameUnreserved reservedPlotLabelsResp:', reservedPlotLabelsResp)
      const foundReserved = reservedPlotLabelsResp?.body?.data?.find(
        (reservedPlot) => reservedPlot?.attributes?.plot_label === plotLabel,
      )
      console.log('checkPlotNameUnreserved foundReserved:', foundReserved)
      expect(foundReserved).to.be.undefined
      // cy.wrap(foundReserved)
      //   .should('be.empty')
    })
})

Cypress.Commands.add('savePlotAndCheck', ({
  plotLabel,
  checkMode,
  interceptRequestType = 'POST',
  buttonToPress = 'done',
}) => {
  cy.log(`savePlotAndCheck for plotLabel=${plotLabel}, checkMode=${checkMode}, interceptRequestType=${interceptRequestType}, buttonToPress=${buttonToPress}`)
  cy.intercept(interceptRequestType, /\/api\/reserved-plot-labels/).as('reserveIntercept')
  switch (buttonToPress) {
    case 'done': {
      cy.dataCy('done').click()//.wait(500)
      break
    }
    case 'delete': {
      cy.setExpansionItemState(`_(${plotLabel})_`.toLowerCase(), 'open').within(() => {
        cy.dataCy('delete').click()
      })
      break
    }
    default:
      throw new Error(`Unreserved 'button' case '${checkMode}' when trying to check plot ${plotLabel}`)
  }
  cy.wait('@reserveIntercept')
    .then((resp) => {
      // console.log(`resp: ${JSON.stringify(resp.response, null, 2)}`)
      expect(resp.response.statusCode).to.be.eq(200)
    })
    .then(() => {
      switch (checkMode) {
        case 'isReserved': {
          cy.checkPlotNameReserved({
            plotLabel: plotLabel,
          })
          break
        }
        case 'isUnreserved': {
          cy.checkPlotNameUnreserved({
            plotLabel: plotLabel,
          })
          break
        }
        default:
          throw new Error(`Unreserved 'check' case '${checkMode}' when trying to check plot ${plotLabel}`)
      }
    })
})

registerCommands()
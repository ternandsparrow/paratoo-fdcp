import { registerCommands } from "@quasar/quasar-app-extension-testing-e2e-cypress"


Cypress.Commands.add(
    'addVegetationAssociationInformation',
    (numberOfTimesToRun = 1) => {
        for (let i = 0; i < numberOfTimesToRun; i++) {
            cy.log('addVegetationAssociationInformation run', i)
            cy.selectFromDropdown('stratum', 'Upper Storey', 'Upper Storey')
            if (i === 0) {
                cy.selectFromDropdown('vegetation_species', 'Plantae [Regnum]')
            }
            cy.selectFromDropdown('growth_form', 'Vine', 'Vine')
            cy.dataCy('height_range').type(30)
            cy.dataCy('cover_percentage').type(80)
            cy.dataCy('done').eq(0).click()
            if (i < numberOfTimesToRun - 1) {
                cy.dataCy('addEntry').eq(0).click().wait(500)
            }
        }
    },
)

Cypress.Commands.add('completeSignObserved', () => {
    cy.selectFromDropdown('sign_type', 'Tracks')
    cy.selectFromDropdown('attributable_fauna_species', 'Cat')
    cy.selectFromDropdown('sign_age', 'Fresh 1-2 days')
    cy.selectFromDropdown('age_class', 'Juvenile')

    cy.dataCy('addImg').eq(-1).click().wait(500)
    cy.dataCy('takePhoto').eq(-1).click().wait(500)

    cy.dataCy('count').type(4)

    cy.get('[data-cy="done"] > .q-btn__content > .block').eq(-1).click().wait(100)
})

Cypress.Commands.add('fillPlotDescriptionEnhancedForm', () => {
    cy.dataCy('slope').type('5')
    cy.dataCy('aspect').type('1')
    cy.selectFromDropdown('relief', 'Low (30–90 m)')
    cy.selectFromDropdown('modal_slope', 'Gently inclined')
    cy.selectFromDropdown('landform_pattern', 'Mountains')
    cy.selectFromDropdown('landform_element', 'Cliff')
    cy.selectFromDropdown('outcrop_lithology', 'Granite')
    cy.selectFromDropdown('lithology', 'Gypsum')
    cy.selectFromDropdown('abundance', 'Slightly or few (2-10%)')
    cy.selectFromDropdown('size', 'Medium gravelly or medium pebbles (6-20mm)')
    cy.selectFromDropdown('veg_growth_stage', 'Uneven Age')
    cy.selectFromDropdown('structural_formation', 'Hummock Grasses')
    cy.dataCy('homogeneity_measure').type('5')
    cy.selectFromDropdown('site_disturbance', null)
    cy.nextStep()
})

Cypress.Commands.add('fillPlotDescriptionStandardForm', () => {
    cy.dataCy('revisit_reason').type('some revisit reason')
    cy.selectFromDropdown('veg_growth_stage', 'Uneven Age')
    cy.selectFromDropdown('structural_formation', 'Hummock Grasses')
    cy.dataCy('homogeneity_measure').type('5')
    cy.selectFromDropdown('site_disturbance', null)
    cy.nextStep()
})

registerCommands()
import { registerCommands } from '@quasar/quasar-app-extension-testing-e2e-cypress'

/**
 * for protocols that follow transect setup / survey setup pattern
 * e.g., sign-based off-plot, herbivory off-plot
 */

Cypress.Commands.add('offPlotTransectSetup1New', () => {
  let transectId = null

  cy.getAndValidate('transect', {
    isComponent: true,
    testEmptyField: ['workflowNextButton'],
  })
  // transect 1
  cy.dataCy('site_id')
    .invoke('val')
    .then((id) => {
      transectId = id
    })
    .then(() => {
      cy.recordLocation()

      cy.getAndValidate('transect_route_type').selectFromDropdown('Vehicle road')
      //FIXME getAndValidate for 'bearing' isn't grabbing notification even though it's there
      // cy.getAndValidate('bearing').type(50)
      cy.dataCy('bearing').type(50)
      cy.getAndValidate('quadrat_width').type(2)
      cy.getAndValidate('quadrat_length').type(2)
      // TODO: test custom rule of this field
      cy.dataCy('transect_length').type(20)
      cy.getAndValidate('transect_spacing').type(1)
      cy.getAndValidate('habitat').selectFromDropdown('Forest')

      cy.dataCy('done').eq(0).click()

      return cy.wrap(transectId)
    })
  
})

Cypress.Commands.add('offPlotTransectSetup2New', () => {
  let transectId = null

  cy.dataCy('addEntry').eq(0).click()
  
  cy.dataCy('site_id')
    .invoke('val')
    .then((id) => {
      transectId = id
    })
    .then(() => {
      cy.recordLocation()
      cy.dataCy('transect_route_type').type('custom route type')
      cy.dataCy('bearing').type(100)
      cy.dataCy('quadrat_width').type(2)
      cy.dataCy('quadrat_length').type(2)
      cy.dataCy('transect_length').type(20)
      cy.dataCy('transect_spacing').type(1)
      cy.selectFromDropdown('habitat', 'Sedgeland')

      cy.dataCy('done').click()

      return cy.wrap(transectId)
    })
})

Cypress.Commands.add('offPlotTransectSetup3New', () => {
  let transectId = null

  cy.dataCy('addEntry').eq(0).click()

  cy.dataCy('site_id')
    .invoke('val')
    .then((id) => {
      transectId = id
    })
    .then(() => {
      cy.recordLocation()
      cy.selectFromDropdown('transect_route_type', null)
      cy.dataCy('bearing').type(200)
      cy.dataCy('quadrat_width').type(2)
      cy.dataCy('quadrat_length').type(2)
      cy.dataCy('transect_length').type(20)
      cy.dataCy('transect_spacing').type(1)

      cy.dataCy('done').click()

      return cy.wrap(transectId)
    })
})

Cypress.Commands.add('offPlotSurveySetup1', (transectSetupId, protocol) => {
  cy.log(`doing survey setup for transect ID: ${transectSetupId}`)
  let setupId = null
  cy.getAndValidate('transect_setup_ID').selectFromDropdown(transectSetupId)

  cy.getAndValidate('survey_intent').selectFromDropdown('Once-off measure')
  cy.getAndValidate('target_species').selectFromDropdown([
    'Wild Dog',
    'Deer',
    'Other',
  ])

  cy.getAndValidate('target_deer').selectFromDropdown('Fallow deer (Dama dama)')
  cy.getAndValidate('target_wild_dog').selectFromDropdown('Dog (Canis lupus familiaris)')

  cy.getAndValidate('other_species').type('drop bear')
  cy.dataCy('done').eq(0).click()
  if (protocol === 'sign_off') {
    cy.getAndValidate('scat', { isComponent: true })
    cy.dataCy('Removed').click()    //FIXME getAndValidate on radio buttons
    cy.getAndValidate('tracking_substrate').selectFromDropdown()
  }
  
  cy.getAndValidate('observer_details', {
    isComponent: true,
    testEmptyField: ['done', -1],
  })
  const observers = ['Spotter', 'Data entry']
  let roleFieldName = null
  if (protocol === 'sign_off') {
    roleFieldName = 'role'
  } else if (protocol === 'herb_off') {
    roleFieldName = 'observer_role'
  }
  observers.forEach((role, index) => {
    const modifier = Math.floor(Date.now())
    //TODO test-to-fail observer name (need to get a handle on 'done' button inside observer details component)
    cy.addObservers('observerName', [`Anon ${modifier + index}`])
    cy.getAndValidate(roleFieldName, {
      isChildOf: 'observer_details',
    }).selectFromDropdown(role)
    cy.get('[data-cy="done"]').eq(0).click()
    if (index < 2) {
      cy.get('[data-cy="addEntry"]').eq(1).click()
    }
  })

  if (protocol === 'sign_off') {
    cy.getAndValidate('weather', { isComponent: true })
    cy.completeWeatherForm()
  }

  //need to grab this here (and not when we initially select the transectID) as all
  //other inputs cause a flow-on effect that re-generates the survey setup ID, so
  //grab it only once we're done with this one
  cy.dataCy('survey_setup_ID')
    .invoke('val')
    .then((id) => {
      //TODO check survey setup ID gets autofilled
      setupId = id
    })
    .then(() => {
      cy.dataCy('done').eq(-1).click()
      cy.dataCy('addEntry').eq(-1).click()

      return cy.wrap(setupId)
    })
})

Cypress.Commands.add('offPlotSurveySetup2', (transectSetupId, protocol) => {
  cy.log(`doing survey setup for transect ID: ${transectSetupId}`)
  let setupId = null

  //TODO check that the transect ID from before (`transectId[0]`) option is disabled (can only do one survey setup for a transect setup per collection)
  cy.selectFromDropdown('transect_setup_ID', transectSetupId)

  cy.selectFromDropdown('survey_intent', 'Once-off measure')
  cy.selectFromDropdown('target_species', 'Cat')
  if (protocol === 'sign_off') {
    cy.dataCy('Removed').click()
    cy.getAndValidate('tracking_substrate').selectFromDropdown()
  }
  
  let roleFieldName = null
  if (protocol === 'sign_off') {
    roleFieldName = 'role'
  } else if (protocol === 'herb_off') {
    roleFieldName = 'observer_role'
  }
  cy.selectFromDropdown('observer_name', null)
  cy.selectFromDropdown(roleFieldName, null)
  cy.dataCy('done').eq(0).click()

  cy.dataCy('survey_setup_ID')
    .invoke('val')
    .then((id) => {
      //TODO check survey setup ID gets autofilled
      setupId = id
    })
    .then(() => {
      cy.dataCy('done').eq(-1).click()

      return cy.wrap(setupId)
    })
})

registerCommands()
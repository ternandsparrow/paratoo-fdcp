import { registerCommands } from "@quasar/quasar-app-extension-testing-e2e-cypress"


/**
 * Collects recruitment survivorship survey
 *
 * @param {Array.<Object>} vouchers the list of vouchers, where each object has at least
 * the `field_name` key
 * @param {Boolean} isNewVisit whether this is a new visit (false assumes revisit)
 *
 * TODO handle when running against dev (or non-clean instance) and we already have collection(s) for the new visit (which will actually just be a revisit)
 * possible approaches:
 *   - cleanup old collections for the visit
 *   - create a new plot (but will require cleanup too most likely)
 *   - detect if we're against a non-clean instance (already have collections) and skip
 *     the new visit test
 */
Cypress.Commands.add(
    'recruitmentSurvivorshipSurvey',
    ({ vouchers, isNewVisit }) => {
        cy.log(
            `Collecting recruitment survivorship survey data for ${isNewVisit ? 'new visit' : 'revisit'
            }`,
        )
        let expectedVisitTypeText = null
        if (isNewVisit) {
            expectedVisitTypeText = 'This is a NEW Visit!'

            cy.dataCy('revisitTable').should('not.exist')
            cy.dataCy('reason_for_revisit').should('not.exist')
        } else {
            expectedVisitTypeText = 'This is a Revisit!'

            cy.dataCy('revisitTable').should('exist')
            cy.dataCy('reason_for_revisit').should('exist')

            for (const voucher of vouchers) {
                cy.dataCy(`revisitTableRow_${voucher.field_name.replaceAll(' ', '_')}`)
                    .should('exist')
                    .and('contain.text', voucher.field_name)
            }
        }

        cy.dataCy('visitTypeText').should('have.text', expectedVisitTypeText)

        //survey step
        cy.selectFromDropdown('study_area_type', 'Plot')
        cy.selectFromDropdown(
            'floristics_voucher',
            vouchers.map((o) => o.field_name),
        ).type('{esc}')
        //workaround - de-focus the dropdown - don't want to modify command due to potential
        //knock-on effects
        // cy.dataCy('floristics_voucher').click()

        cy.dataCy('comments').type('comment\n\nnext line')
        cy.nextStep()
    },
)


/**
 * Collects a single recruitment survivorship observation
 *
 * @param {Array.<Object>} vouchers the list of vouchers, where each object has at least
 * the `field_name` key
 * @param {Boolean} shouldAddEntry whether to add a new observation entry
 */
Cypress.Commands.add(
    'recruitmentSurvivorshipObservation',
    ({ voucher, shouldAddEntry }) => {
        cy.log(
            `Collecting recruitment survivorship observation data for ${voucher.field_name}`,
        )
        cy.selectFromDropdown('species', voucher.field_name)
        cy.recordLocation()
        cy.dataCy('plant_tag_ID').type(
            `${voucher.field_name} label ${Math.floor(Math.random() * 100000)}`,
        )
        cy.selectFromDropdown('growth_stage', null)
        cy.selectFromDropdown('life_stage', null)

        // Health
        cy.dataCy('canopy_cover').type(1)

        // Crown Damage Index 1
        cy.dataCy('cdiAddItem').click().wait(500)
        cy.selectFromDropdown('cdiType', null)

        cy.dataCy('cdiIncidence').clear().type(9)

        cy.dataCy('cdiSeverity').clear().type(8)
        cy.dataCy('cdiDone').click().wait(500)

        // Crown Damage Index 2
        cy.dataCy('cdiAddItem').click().wait(500)
        cy.selectFromDropdown('cdiType', null)

        cy.dataCy('cdiIncidence').clear().type(9)

        cy.dataCy('cdiSeverity').clear().type(8)
        cy.dataCy('cdiDone').click().wait(500)

        cy.dataCy('mistletoes_alive').type(2)
        cy.dataCy('mistletoes_dead').type(3)
        
        
        //hollows
        for (let h = 0; h < 3; h++) {
          cy.getAndValidate('size_of_hollow').clear().type(5)
          cy.getAndValidate('direction_of_hollow_opening').clear().type(5)
          cy.getAndValidate('height_of_the_hollow').clear().type(5)

          cy.dataCy('done').eq(0).click()

          if (h < 2) {
            cy.dataCy('addEntry').eq(0).click()
          }
        }

        cy.dataCy('height').type('10')
        cy.dataCy('canopy_width').type('10')
        cy.dataCy('dbh').type('10')
        cy.dataCy('point_of_measurement').type('10')
        cy.addImages('addImg', 1, {
          btnIndex: 1,
        })
        cy.dataCy('comments').eq(-1).type('comments')
        cy.dataCy('done').eq(-1).click().wait(1000)

        if (shouldAddEntry) {
            cy.log(`Adding new entry for ${voucher.field_name}`)
            cy.dataCy('addEntry').eq(-1).click()
        }
    },
)

registerCommands()
import { registerCommands } from "@quasar/quasar-app-extension-testing-e2e-cypress"

/**
 * Collects the condition protocol - wraps the `conditionStandard` and
 * `conditionEnhanced` commands to handle vouchers
 *
 * @param {String} protocolVariant the protocol variant - 'standard' or 'enhanced'
 * @param {Array.<String>} [vouchersToCollect] the list of vouchers to collect where each
 * str in the array is a voucher field name. Defaults to pre-defined list if not provided
 */
Cypress.Commands.add(
  'condition',
  ({ protocolVariant, vouchersToCollect = [] }) => {
    let vouchers = null
    if (vouchersToCollect.length > 0) {
      cy.log(
        `Collecting Condition ${protocolVariant} with provided vouchers: ${vouchersToCollect}`,
      )
      vouchers = vouchersToCollect
    } else {
      vouchers = [
        'Voucher Full Example 1',
        'Voucher Full Example 6',
        'Voucher Lite Example 2',
      ]
      cy.log(
        `No vouchers provided to collect Condition ${protocolVariant}, using some from init data: ${vouchers}`,
      )
    }

    cy.get('[data-cy="40 x 40 m"]').click()
    cy.selectFromDropdown('equipment_for_dia', 'Diameter Tape')
    cy.selectFromDropdown('equipment_for_height', 'Range Finder')
    cy.get('[data-cy="10 cm"]').click()
    cy.nextStep()

    for (const [index, v] of vouchersToCollect.entries()) {
      // add tree record
      cy.recordLocation() //need to force, as headless might not have default to fallback
      cy.get('[data-cy="mallee_or_mulga"]').click()
      cy.get('[data-cy="dead"]').click()
      cy.selectFromDropdown('species', v)
      cy.selectFromDropdown('growth_stage', 'Recruiting')
      cy.selectFromDropdown('life_stage', 'Vegetative')
      cy.get('[data-cy="height"]').type('3')

      cy.get('[data-cy="done"]').eq(-1).click().wait(1000)

      if (index < vouchersToCollect.length - 1) {
        cy.dataCy('addEntry').click()
      }
    }
  },
)

registerCommands()
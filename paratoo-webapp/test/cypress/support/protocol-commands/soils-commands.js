import { registerCommands } from "@quasar/quasar-app-extension-testing-e2e-cypress"

Cypress.Commands.add('soilCharacterisationPitLocation', () => {
  let soilPitId
  /* ==== first step === */
  cy.get('[data-cy="Field"]').click()
  // TODO use prefilled observerName if available
  cy.get('[data-cy="observerName"]')
    .type('Tom' + (Math.random() + 1).toString(36).substring(7))
    .wait(100)
    .type('{enter}')
  // get auto generated soilPitId for lab step
  cy.get('[data-cy="soil_pit_id"]')
    .invoke('val')
    .then((text) => {
      soilPitId = text
    })
    .then(() => {
      cy.selectFromDropdown('collection_method', 'Tablet')
      cy.get('[data-cy="collection_method"]').click()
      cy.get('[data-cy="choosePosition"]').click()
      cy.get('[data-cy="recordLocation"]').click()
      cy.get('[data-cy="location_description"]').type(
        'This is a location descriptions first line{enter}This is the second',
      )
      cy.nextStep()
    })
    .then(() => {
      return soilPitId
    })
  
})

Cypress.Commands.add('soilCharacterisationLandformElements', () => {
  cy.dataCy('evaluation_means')
    .validateField()
    .selectFromDropdown('Estimate')
  cy.get('[data-cy="slope_percent"]')
    // .validateField()
    .type('5')
  cy.dataCy('slope_class')
    // .validateField()
    .should('have.text', 'Gently inclined')
  cy.dataCy('morphological_type')
    .validateField()
    .selectFromDropdown('Flat')
  cy.dataCy('inclination_of_slope')
    .validateField()
    .selectFromDropdown('Minimal')
  cy.dataCy('landform_element')
    .validateField()
    .selectFromDropdown('Dam', 'Dam')
  cy.dataCy('location_within_landform')
    // .validateField()
    .selectFromDropdown('Top third of the height of the landform element')

  cy.nextStep()
})

Cypress.Commands.add('soilCharacterisationSurfacePhenomenaSoilDevelopment', () => {
  /* ==== Third step === */
  cy.dataCy('surface_soil_when_dry')
    // .validateField()
    .selectFromDropdown('Loose')
  cy.dataCy('disturbance').selectFromDropdown(
    'No effective disturbance except high grazing by hoofed animals',
  )

  cy.nextStep()
})

Cypress.Commands.add('soilCharacterisationMicrorelief', () => {
  // first
  cy.dataCy('microrelief_type')
    .eq(-1)
    .validateField(['done', -1])
    .selectFromDropdown('Normal gilgai')
  cy.dataCy('gilgai_proportion').should('exist')

  cy.dataCy('microrelief_type').eq(-1).selectFromDropdown('Biotic')
  cy.dataCy('gilgai_proportion').should('not.exist')
  cy.dataCy('biotic_relief_agent')
    .eq(-1)
    .validateField(['done', -1])
    .should('exist')

  cy.dataCy('microrelief_type').eq(-1).selectFromDropdown('Mass movement')
  cy.dataCy('gilgai_proportion').should('not.exist')
  cy.dataCy('biotic_relief_agent').should('not.exist')

  cy.dataCy('vertical_interval_distance')
    .validateField(['done', -1])
    .type('2')
  cy.dataCy('horizontal_interval_distance')
    .validateField(['done', -1])
    .type('4')

  cy.dataCy('done').eq(0).click()

  // second
  cy.dataCy('addEntry').click()
  cy.dataCy('microrelief_type')
    .eq(-1)
    .selectFromDropdown('Mound/depression', null, 1)
  cy.dataCy('vertical_interval_distance')
    .eq(-1)
    .validateField( ['done', -1])
    .type('5')
  cy.dataCy('horizontal_interval_distance')
    .eq(-1)
    .validateField( ['done', -1])
    .type('8')
  cy.dataCy('done').eq(-1).click()

  // third
  cy.dataCy('addEntry').click()
  cy.dataCy('microrelief_type').eq(-1).selectFromDropdown('Biotic', null, 1)
  cy.dataCy('biotic_relief_agent').selectFromDropdown('Animal')
  cy.dataCy('microrelief_component').selectFromDropdown('Mound')
  cy.dataCy('vertical_interval_distance')
    .eq(-1)
    .validateField( ['done', -1])
    .type('5')
  cy.dataCy('horizontal_interval_distance')
    .eq(-1)
    .validateField( ['done', -1])
    .type('8')
  cy.dataCy('done').eq(-1).click()

  // fourth
  cy.dataCy('addEntry').eq(-1).click()
  cy.dataCy('delete').eq(-1).click()

  cy.nextStep()
})

Cypress.Commands.add('soilCharacterisationErosionObservation', () => {
  cy.dataCy('erosion_state')
    .validateField( ['done', -1])
    .selectFromDropdown('Absent', 'Absent')
  cy.dataCy('erosion_type').should('not.exist')
  cy.dataCy('done').eq(0).click()
  cy.dataCy('addEntry').click()

  cy.dataCy('erosion_state')
    .validateField( ['done', -1])
    .selectFromDropdown('Active', 'Active')
  cy.dataCy('erosion_type')
    .validateField( ['done', -1])
    .selectFromDropdown('Mass movement', 'Mass movement')
  cy.dataCy('done').eq(-1).click()

  cy.dataCy('addEntry').click()
  cy.dataCy('delete').eq(-1).click()

  // cy.dataCy('workflowNextButton').click()
  cy.nextStep()
})

Cypress.Commands.add('soilCharacterisationSurfaceCoarseFragmentObservations', () => {
  for (let i = 0; i < 3; i++) {
    cy.selectFromDropdown(
      'coarse_fragments_abundance',
      'Moderately or many (20 - 50%)',
    )
    cy.selectFromDropdown('coarse_fragments_size', 'Large Boulders (>2000mm)')
    cy.selectFromDropdown('coarse_fragments_shape', 'Subangular')
    cy.selectFromDropdown('coarse_fragments_lithology', 'Aplite', 'Aplite')
    cy.selectFromDropdown(
      'coarse_fragments_strength',
      'Weak rock (25-50 MPa)',
    )
    cy.selectFromDropdown('coarse_fragments_alteration', 'Calcified')
    cy.wait(500)
    cy.dataCy('done').eq(0).click(0)
    cy.wait(500)
    if (i < 3) cy.dataCy('addEntry').eq(0).click(0)
  }
  cy.dataCy('delete').eq(0).click(0)

  cy.nextStep()
})

Cypress.Commands.add('soilCharacterisationRockOutcropObservation', () => {
  cy.selectFromDropdown(
    'rock_outcrop_abundance',
    'Very slightly rocky (<2% bedrock exposed)',
  )
  cy.selectFromDropdown('rock_outcrop_lithology', 'Halite', 'Halite')

  cy.nextStep()
})

Cypress.Commands.add('soilCharacterisationSoilPitObservation', () => {
  /* ==== fourth step === */
  cy.selectFromDropdown('observation_type', 'Soil pit')
  cy.get('[data-cy="soil_pit_depth"]').type('6')
  cy.selectFromDropdown('digging_stopped_by', null)
  cy.addImages('addImg')
  cy.get('[data-cy="comments"]').click()
  cy.nextStep()
})

Cypress.Commands.add('soilCharacterisationSoilHorizonObservation', () => {
  /* ==== fifth step === */
  cy.get('[data-cy="horizon"]').type('Horizon 1')
  cy.get('[data-cy="upper_depth"]').type('1')
  cy.get('[data-cy="lower_depth"]').type('3')
  cy.selectFromDropdown('boundary_distinctness', 'Sharp (<5mm)')
  cy.selectFromDropdown('boundary_shape', 'Wavy')
  cy.selectFromDropdown('texture_grade', 'Loam')
  cy.selectFromDropdown('texture_qualification', 'Heavy')
  cy.selectFromDropdown('texture_modifier', 'Fine sandy')

  // TODO do 1 or 2 more times
  cy.selectFromDropdown('moisture_status', 'Dry')
  cy.selectFromDropdown('colour_hue', '5R')
  cy.selectFromDropdown('colour_value', '5')
  cy.selectFromDropdown('colour_chroma', '8')
  cy.dataCy('done').eq(0).click()
  //=================================//
  // TODO do the optional ticks
  cy.dataCy('has_mottles').click()
  cy.dataCy('has_coarse_fragments').click()
  cy.dataCy('has_structure').click()
  cy.dataCy('has_segregation').click()
  cy.dataCy('has_voids').click()
  cy.dataCy('has_pans').click()
  cy.dataCy('has_cutans').click()
  cy.dataCy('has_roots').click()
  // has mottle
  cy.dataCy('mottle_1_expansion').should('exist')
  cy.selectFromDropdown('mottle_type', 'Mottles')
  cy.selectFromDropdown('mottle_abundance', 'Very few (<2%)')
  cy.selectFromDropdown('mottle_size', 'Fine (<5mm)')
  cy.selectFromDropdown('mottle_contrast', 'Distinct')
  cy.selectFromDropdown('mottle_colour', 'Red')
  cy.selectFromDropdown('mottle_boundary_distinctness', 'Sharp')
  cy.wait(500)
  cy.dataCy('done').eq(0).click()
  cy.wait(500)
  // has coarse frag
  cy.dataCy('coarse_fragment_1_expansion').should('exist')
  cy.selectFromDropdown(
    'coarse_frag_abundance',
    'Very or abundant (50 - 90%)',
  )
  cy.selectFromDropdown('coarse_frag_size', 'Large Boulders (>2000mm)')
  cy.selectFromDropdown('coarse_frag_shape', 'Rounded')
  cy.selectFromDropdown(
    'coarse_frag_lithology',
    'Microdiorite',
    'Microdiorite',
  )
  cy.selectFromDropdown('coarse_frag_strength', 'Weak rock (25-50 MPa)')
  cy.selectFromDropdown('coarse_frag_alteration', 'Calcified')
  cy.selectFromDropdown('coarse_frag_distribution', 'Stratified')
  cy.wait(500)
  cy.dataCy('done').eq(0).click()
  cy.wait(500)
  // has structure
  cy.dataCy('structure_1_expansion').should('exist')
  cy.selectFromDropdown('structure_grade', 'Massive')
  cy.selectFromDropdown('structure_size', '<2mm')
  cy.selectFromDropdown('structure_type', 'Columnar')
  cy.selectFromDropdown(
    'structure_compound_pedality',
    'Largest peds (in the type of soil observation described), parting to',
  )
  cy.wait(500)
  cy.dataCy('done').eq(0).click()
  cy.wait(500)
  // has segregation
  cy.dataCy('segregation_1_expansion').should('exist')
  cy.selectFromDropdown('segregation_abundance', 'Many (20-50%)')
  cy.selectFromDropdown('segregation_nature', 'Argillaceous (clayey)')
  cy.selectFromDropdown('segregation_form', 'Tubules')
  cy.selectFromDropdown('segregation_size', 'Extremely coarse (>60mm)')
  cy.selectFromDropdown('segregation_strength', 'Weak')
  cy.selectFromDropdown('segregation_magnetic_attribute', 'Magnetic')
  cy.wait(500)
  cy.dataCy('done').eq(0).click()
  cy.wait(500)
  // has voids
  cy.dataCy('void_1_expansion').should('exist')
  cy.selectFromDropdown('void_crack', 'Medium (5-10mm)')
  cy.selectFromDropdown('fine_macropore_abundance', 'Few')
  cy.selectFromDropdown('medium_macropore_abundance', 'Few')
  cy.selectFromDropdown('macropore_diameter', 'Fine')
  cy.wait(500)
  cy.dataCy('done').eq(0).click()
  cy.wait(500)
  // has pans
  // cy.dataCy('title').should('contain.text', 'Pan')
  cy.selectFromDropdown('pan_cementation', 'Uncemented')
  cy.selectFromDropdown('pan_type', 'Silcrete')
  cy.selectFromDropdown('pan_continuity', 'Broken')
  cy.selectFromDropdown('pan_structure', 'Platy')
  // has cutans
  // cy.dataCy('title').should('contain.text', 'Cutan')
  cy.selectFromDropdown('cutan_type', 'Mangans')
  cy.selectFromDropdown('cutan_abundance', 'Few')
  cy.selectFromDropdown('cutan_distinctness', 'Faint')
  // TODO has roots
  // cy.dataCy('title').should('contain.text', 'Roots')
  cy.selectFromDropdown('root_abundance', 'Few')
  cy.selectFromDropdown('root_size', 'Fine (1-2 mm)')

  //=================================//
  cy.selectFromDropdown('water_status', 'Wet')
  cy.selectFromDropdown('soil_strength', 'Firm')
  cy.selectFromDropdown('plasticity_type', 'Superplastic')
  cy.selectFromDropdown('effervescence', 'Not Collected')
  cy.dataCy('depth_of_field_test').last().type('1')
  cy.dataCy('pH').type('2')
  cy.dataCy('electrical_conductivity').type('3')
  cy.selectFromDropdown('slaking_score', 'Partially slaked within 10 minutes')
  cy.selectFromDropdown(
    'dispersion_score',
    'Complete dispersion after 10 minutes',
  )
  cy.dataCy('done').eq(0).click()
  cy.dataCy('done').eq(-1).click()
  // cy.get('[data-cy="workflowNextButton"]').click()
  // === second horizon ===
  cy.dataCy('addEntry').eq(-1).click()
  cy.get('[data-cy="horizon"]').eq(-1).type('Horizon 2')
  // cy.get('[data-cy="upper_depth"]').eq(-1).type('1')
  cy.get('[data-cy="lower_depth"]').eq(-1).type('5')
  cy.selectFromDropdown('boundary_distinctness', 'Sharp (<5mm)', null, -1)
  cy.selectFromDropdown('boundary_shape', 'Wavy', null, -1)
  cy.selectFromDropdown('texture_grade', 'Loam', null, -1)
  cy.selectFromDropdown('texture_qualification', 'Heavy', null, -1)
  cy.selectFromDropdown('texture_modifier', 'Fine sandy', null, -1)
  cy.selectFromDropdown('water_status', 'Dry', null, -1)
  cy.selectFromDropdown('soil_strength', 'Loose', null, -1)
  cy.dataCy('done').eq(-1).click()
  cy.nextStep()
})

Cypress.Commands.add('soilCharacterisationAustralianSoilClassification', () => {
  /* ==== sixth step === */
  cy.selectFromDropdown('confidence_level', '2')
  cy.selectFromDropdown('order', 'Kandosols', 'kan')
  cy.get('[data-cy="order"]').clear()
  cy.selectFromDropdown('confidence_level', '2')
  cy.selectFromDropdown('suborder', 'Aeric', 'Aeric')
  cy.selectFromDropdown('great_group', 'Basic', 'Basic')
  cy.selectFromDropdown('subgroup', 'Black', 'Black')

  cy.selectFromDropdown('family', 'Thin', 'Thin')

  cy.nextStep()
})

Cypress.Commands.add('soilCharacterisationSoilClassification', () => {
  /* ==== seventh step === */
  cy.selectFromDropdown('permeability', 'Slowly permeable')
  cy.selectFromDropdown('drainage', 'Well')
  cy.selectFromDropdown('substrate_lithology', 'Granite', 'Granite')
  cy.dataCy('depth_r_horizon').type('5')
  cy.dataCy('soil_profile_comments').type(
    'This is a comment{enter}This is another one',
  )
})

Cypress.Commands.add('soilBulkDensitySurvey', (soilPitId = null) => {
  cy.dataCy('Field').click()
  //TODO use validateFields, which handles the '*' better
  cy.testEmptyField('Field: "Observers *": This field is required', 'workflowNextButton')
  cy.addObservers('observerName', [
    'Tom',
    'Luke',
    'Walid',
    'Wildcard' + (Math.random() + 1).toString(36).substring(10),
  ])
  cy.testEmptyField(
    'Field: "Collection Method *": This field is required',
    'workflowNextButton',
  )
  cy.selectFromDropdown('collection_method', 'Differential GNSS')
  if (soilPitId !== null) {
    cy.log(`selecting soilPitId=${soilPitId}`)
    cy.selectFromDropdown('associated_soil_pit_id', soilPitId)
  }

  cy.recordLocation()
})

registerCommands()
import { registerCommands } from "@quasar/quasar-app-extension-testing-e2e-cypress"

Cypress.Commands.add('vertStartTrapLine', (isFence = false) => {
    if (isFence) {
        cy.selectFromDropdown('fence_type', 'Plastic sheet', null, -1)
        cy.get('[data-cy="fence_height"]').eq(-1).type('2')
    }
})
Cypress.Commands.add('vertEndTrapLine', () => {
    cy.dataCy('done').eq(-1).click()
})

Cypress.Commands.add('vertMarkTrap', (numberOfTraps) => {
    const VERT_TRAP_TYPE = Cypress.env('VERT_TRAP_TYPE')
    cy.setExpansionItemState('mark_trap', 'open', -1).within(() => {
        const recordedTrapTypes = []
        for (let i = 0; i < numberOfTraps; i++) {
            cy.get('[data-cy="recordBarcode"] > .q-btn__content').click()
            cy.get('[data-cy="trap_type"] > span')
                .invoke('text')
                .then((trapName) => {
                    if (!recordedTrapTypes.includes(trapName)) {
                        cy.log(trapName)
                        recordedTrapTypes.push(trapName)
                        if (trapName === 'Pitfall Trap') {
                            cy.dataCy('trap_depth').eq(-1).type('123')
                        } else {
                            cy.dataCy('trap_height').eq(-1).type('123')
                        }
                        cy.dataCy('trap_width').eq(-1).type('123')
                        const trapSpecifics = {
                            [VERT_TRAP_TYPE.P]: 'PVC pipe',
                            'Funnel Trap': 'Shadecloth',
                            'Box Trap': 'Foldable aluminium or galvanised steel ',
                            'Cage Trap': 'Wire mesh, foot plate mechanism',
                        }
                        cy.get(`[data-cy="${trapSpecifics[trapName]}"]`).eq(-1).click()
                        cy.dataCy('trap_set_status').selectFromDropdown()
                    }
                })
            cy.get('[data-cy="done"]').click()
        }
    })
})

Cypress.Commands.add('vertCheckTrap', (trapNumber, status) => {
    cy.selectFromDropdown('trap_number', trapNumber, trapNumber, -1)

    // should only validate once , there's no reason to it again and again in the same test
    if (trapNumber === 'P1') {
        cy.getAndValidate('trap_check_status', { testEmptyField: ['done', -1] }).selectFromDropdown(status)

        cy.getAndValidate('comment', { testEmptyField: ['done', -1] }).eq(-1).type('bla bla bla')

        cy.getAndValidate('toggleBarcodeReaderShow').eq(-1).click().wait(50)

        cy.getAndValidate('trap_check_photo', { testEmptyField: ['done', -1], isComponent: true })
        cy.dataCy('addImg').eq(0).click().wait(50)
        cy.dataCy('takePhoto').eq(0).click().wait(50)
        // cy.dataCy('setup_status').selectFromDropdown('Trap open')
        cy.getAndValidate('setup_status', { testEmptyField: ['done', -1] }).selectFromDropdown('Trap open')
    } else {
        cy.dataCy('trap_check_status').selectFromDropdown(status)
        cy.dataCy('setup_status').selectFromDropdown('Trap open')

        cy.dataCy('comment').eq(-1).type('bla bla bla')

        cy.dataCy('toggleBarcodeReaderShow').eq(-1).click().wait(50)
        cy.dataCy('addImg').eq(0).click().wait(50)
        cy.dataCy('takePhoto').eq(0).click().wait(50)
        cy.getAndValidate('trap_check_photo', { testEmptyField: ['done', -1], isComponent: true })

        cy.selectFromDropdown('setup_status', 'Trap open')
    }
})

Cypress.Commands.add(
    'vertGeneralInfoOfTrapWithCapture',
    (trapNumber, speciesClass, species) => {
        if (trapNumber === 'P1') {
            cy.getAndValidate('trap_number', { testEmptyField: ['done', 0] }).selectFromDropdown(trapNumber, trapNumber, 0)
            cy.getAndValidate('class', { testEmptyField: ['done', 0] }).selectFromDropdown(speciesClass)

            cy.getAndValidate('species_name', { testEmptyField: ['done', 0] }).selectFromSpeciesList(species)
            // new fields?
            cy.log('OBSERVERNAME P2')
            cy.getAndValidate('observerName', { testEmptyField: ['done', 0] }, {log: true}, 0).selectFromDropdown('Jane')
            cy.getAndValidate('observerName', { testEmptyField: ['done', 0] }, {log: true}, 1).selectFromDropdown('Jane')
            cy.getAndValidate('observerName', { testEmptyField: ['done', 0] }, {log: true}, 2).selectFromDropdown('Jane')

            
            cy.getAndValidate('capture_status', { testEmptyField: ['done', 0] }).selectFromDropdown('New')
            cy.getAndValidate('id_mark_type', { testEmptyField: ['done', 0] }).selectFromDropdown('Microchip')
            // TODO: this field will be replace with a dropdown when the it's vocabs are released
            cy.getAndValidate('ear_tag_type', { testEmptyField: ['done', 0] }).type('ear-tag type')
            cy.getAndValidate('ear_tag_comments', { testEmptyField: ['done', 0] }).type('ear-tag type')
            cy.getAndValidate('temporary_texta_colour', { testEmptyField: ['done', 0] }).selectFromDropdown('Red')
            cy.getAndValidate('id_code', { testEmptyField: ['done', 0] }).type('iddddddddddddddddddddd')

            cy.getAndValidate('total_weight', { testEmptyField: ['done', 0] }).type(10)

            cy.getAndValidate('bag_weight', { testEmptyField: ['done', 0] }).type(5)

            // autofilled field
            cy.get('[data-cy=animal_weight]').should('have.value', '5')
        } else {
            cy.selectFromDropdown('trap_number', trapNumber, trapNumber, 0)
            cy.selectFromDropdown('class', speciesClass)

            cy.log('OBSERVERNAME other')
            cy.getAndValidate('observerName', { testEmptyField: ['done', 0] }, {log: true}, 0).selectFromDropdown('Jane')
            cy.getAndValidate('observerName', { testEmptyField: ['done', 0] }, {log: true}, 1).selectFromDropdown('Jane')
            cy.getAndValidate('observerName', { testEmptyField: ['done', 0] }, {log: true}, 2).selectFromDropdown('Jane')


            cy.selectFromSpeciesList('species_name', species)
            cy.selectFromDropdown('capture_status', 'New')
            cy.selectFromDropdown('id_mark_type', 'Microchip')
            // TODO: this field will be replace with a dropdown when the it's vocabs are released
            cy.dataCy('ear_tag_type').type('ear-tag type')
            cy.dataCy('ear_tag_comments').type('ear-tag type')
            cy.selectFromDropdown('temporary_texta_colour', 'Red')
            cy.dataCy('id_code').type('iddddddddddddddddddddd')

            cy.dataCy('total_weight').type(10)

            cy.dataCy('bag_weight').type(5)

            cy.get('[data-cy=animal_weight]').should('have.value', '5')
        }


        // cy.dataCy('done').click()
        // })
    },
)

Cypress.Commands.add(
    'vertRecordMeasurements',
    (measurementFields, speciesLut) => {
        const fields = measurementFields[speciesLut]
        for (const field of fields) {
            cy.dataCy(field).type(22)
        }
        // cy.dataCy('done').click()
    },
)

Cypress.Commands.add('vertRecordReproductiveTraits', (trapNumber) => {
    switch (trapNumber) {
        case 'P1':
            cy.getAndValidate('female_mammal_teat_condition', {
                testEmptyField: ['done', 0],
            }).selectFromDropdown('Lactating')
            cy.getAndValidate('female_breeding_status', {
                testEmptyField: ['done', 0],
            }).selectFromDropdown('Pregnant')
            cy.getAndValidate('number_of_pouch_young', {
                testEmptyField: ['done', 0],
            }).type(2)
            cy.getAndValidate('female_mammal_young_present', {
                testEmptyField: ['done', 0],
            }).selectFromDropdown('Young on back')
            cy.getAndValidate('pouch_young_length', {
                testEmptyField: ['done', 0],
            }).type(2)
            cy.getAndValidate('young_comments', { testEmptyField: ['done', 0] }).type(
                'this is a comment',
            )
            cy.getAndValidate('male_mammal_testes_position', {
                testEmptyField: ['done', 0],
            }).selectFromDropdown('Abdominal')
            cy.getAndValidate('male_mammal_testes_size_category', {
                testEmptyField: ['done', 0],
            }).selectFromDropdown('Enlarged')
            cy.getAndValidate('male_reproductive_comments', {
                testEmptyField: ['done', 0],
            }).type('tiny')
            break
        case 'F1':
            cy.selectFromDropdown('female_breeding_status', 'Gravid')
            cy.getAndValidate('male_frog_breeding_status', {
                testEmptyField: ['done', 0],
            }).selectFromDropdown('Vocal sac obvious')
            break
    }
})

Cypress.Commands.add('vertRecordSexAndAgeClass', () => {
    cy.getAndValidate('sex', {
        testEmptyField: ['done', 0],
    }).selectFromDropdown('Male')

    cy.getAndValidate('age_class', {
        testEmptyField: ['done', 0],
    }).selectFromDropdown('Juvenile')
    // cy.dataCy('done').click()
})

Cypress.Commands.add('vertRecordBodyAndSkinCondition', (trapNumber) => {
    switch (trapNumber) {
        case 'P1':
            cy.getAndValidate('body_condition_score', {
                testEmptyField: ['done', 0],
            }).selectFromDropdown('1')

            cy.getAndValidate('body_condition_comments', {
                testEmptyField: ['done', 0],
            }).type('body condition comment')

            cy.getAndValidate('skin_condition', {
                testEmptyField: ['done', 0],
            }).selectFromDropdown('Hair loss')

            break
        case 'F1':
        case 'F2':
            cy.dataCy('body_condition_comments').type('body condition comment')
            cy.selectFromDropdown('skin_condition', 'Hair loss')
            break
    }
    // cy.dataCy('done').click()
})

Cypress.Commands.add('vertRecordClinicalScoring', (trapNumber) => {
    switch (trapNumber) {
        case 'P1':
            cy.getAndValidate('chemosis_of_eyes', {
                testEmptyField: ['done', 0],
            }).selectFromDropdown()

            cy.getAndValidate('proliferation_of_eyes', {
                testEmptyField: ['done', 0],
            }).selectFromDropdown()

            cy.getAndValidate('rump', {
                testEmptyField: ['done', 0],
            }).selectFromDropdown()

            cy.getAndValidate('clinical_swab_taken', {
                testEmptyField: ['done', 0],
            }).click()

            cy.getAndValidate('swab_code', {
                testEmptyField: ['done', 0],
            }).type('swab code')

            cy.getAndValidate('swab_comments', {
                testEmptyField: ['done', 0],
            }).type('swab comment')
            break
        case 'F1':
        case 'F2':
            cy.dataCy('clinical_swab_taken').click()
            cy.dataCy('swab_code').type('swab code')
            cy.dataCy('swab_comments').type('swab comment')
            break
    }
    // cy.dataCy('done').click()
})

registerCommands()

export function createVertCollectionMetadata() {
  const allTrapNumbers = Cypress.env('VERT_TRAP_NUMBERS')
  const trapNumbers = allTrapNumbers.filter((o) => ['B', 'P', 'F', 'C'].includes(o[0]))
  for (let i = 1; i < 41; i++) {
    trapNumbers.push(`B${i}`)
  }
  const vertTrapLines = {
    'B': {
      numOfTraps: 10,
      isFence: true,
    },
    'E': {
      numOfTraps: 10,
      isFence: true,
    },
    'A': {
      numOfTraps: 12,
      isFence: false,
    },
    'C': {
      numOfTraps: 12,
      isFence: false,
    },
    'D': {
      numOfTraps: 12,
      isFence: false,
    },
    'F': {
      numOfTraps: 12,
      isFence: false,
    },
  }

  return {
    trapNumbers,
    vertTrapLines,
  }
}

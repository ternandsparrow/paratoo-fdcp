import { registerCommands } from "@quasar/quasar-app-extension-testing-e2e-cypress"


Cypress.Commands.add('cameraTrapDeploymentSurvey', (surveyLabel = null, surveyType = null) => {
    let labelToUse = null
    if (surveyLabel === null) {
        labelToUse = `Camera Trap Cypress ${Math.floor(Math.random() * 10000)}`
        cy.log(`Using generated survey label: ${labelToUse}`)
    } else {
        cy.log(`Using provided survey label: ${surveyLabel}`)
        labelToUse = surveyLabel
    }
    //use semi-random post-/pre-fix numbers to prevent unique value collisions, as during
    //CI we hit dev which may already have this data from previous tests
    // testing survey label empty field
    cy.dataCy('survey_label').validateField().type(labelToUse)
    // testing survey type empty field
    cy.dataCy('survey_type').validateField().selectFromDropdown(surveyType || 'Point')
})

Cypress.Commands.add('cameraTrapDeploymentSurveyPlannedPointsJSON', (surveyLabel = null) => {
    let labelToUse = null
    if (surveyLabel === null) {
        labelToUse = `Camera Trap Cypress ${Math.floor(Math.random() * 10000)}`
        cy.log(`Using generated survey label: ${labelToUse}`)
    } else {
        cy.log(`Using provided survey label: ${surveyLabel}`)
        labelToUse = surveyLabel
    }
    //use semi-random post-/pre-fix numbers to prevent unique value collisions, as during
    //CI we hit dev which may already have this data from previous tests
    // testing survey label empty field
    cy.dataCy('survey_label').validateField().type(labelToUse)
    // testing survey type empty field
    cy.dataCy('survey_type').validateField().selectFromDropdown('Array-grid')
    cy.pickFile('array_planned_points.csv')
})

Cypress.Commands.add('cameraTrapDeploymentSurveyPlannedPointsCSV', (surveyLabel = null) => {
    let labelToUse = null
    if (surveyLabel === null) {
        labelToUse = `Camera Trap Cypress ${Math.floor(Math.random() * 10000)}`
        cy.log(`Using generated survey label: ${labelToUse}`)
    } else {
        cy.log(`Using provided survey label: ${surveyLabel}`)
        labelToUse = surveyLabel
    }
    //use semi-random post-/pre-fix numbers to prevent unique value collisions, as during
    //CI we hit dev which may already have this data from previous tests
    // testing survey label empty field
    cy.dataCy('survey_label').validateField().type(labelToUse)
    // testing survey type empty field
    cy.dataCy('survey_type').validateField().selectFromDropdown('Array-transect')
    cy.dataCy('uploadFile')
    cy.pickFile('array_planned_points.csv')
})


Cypress.Commands.add('cameraTrapDeploymentPoint1Pt1', (pointId = null) => {
    // testing Camera Trap Point Id empty field
    let pointIdToUse = null
    if (pointId === null) {
        pointIdToUse = `CTP${String(Math.floor(Math.random() * 10000))}`
        cy.log(`Using generated point ID: ${pointIdToUse}`)
    } else {
        cy.log(`Using provided point ID: ${pointId}`)
        pointIdToUse = pointId
    }
    cy.getAndValidate('camera_trap_point_id', { testEmptyField: ['done', -1] })
        .type(pointIdToUse)
    cy.doDeploymentStartDateAndDeploymentId(pointIdToUse)

    //target taxa
    // cy.dataCy('target_taxa_types_1_expansion').click()    //opens expansion item
    cy.setExpansionItemState('target_taxa_types_1_expansion', 'open')
    cy.getAndValidate('taxa_type', { testEmptyField: ['done', -1] })
        .selectFromDropdown('Bird')
    cy.dataCy('done').eq(0).click().wait(1000)
    cy.dataCy('addEntry').eq(0).click().wait(1000)
    cy.getAndValidate('taxa_type', { testEmptyField: ['done', -1] })
        .selectFromDropdown('Mammal')
    cy.dataCy('done').eq(0).click().wait(1000)

    //target species
    // cy.dataCy('target_species_1_expansion').click()    //opens expansion item
    cy.setExpansionItemState('target_species_1_expansion', 'open')
    //testing species empty field
    cy.getAndValidate('species', { testEmptyField: ['done', 0] })
        .selectFromSpeciesList('emu', false)
    cy.dataCy('done').eq(0).click().wait(1000)
    cy.dataCy('addEntry').eq(1).click().wait(1000)
    cy.dataCy('species')
        .selectFromSpeciesList('frog', false)
    cy.dataCy('done').eq(0).click().wait(1000)

    cy.getAndValidate('camera_trap_mount', { testEmptyField: ['done', -1] })
        .type('custom CT mount')
    cy.recordLocation()
})

Cypress.Commands.add('cameraTrapDeploymentPoint1Features', (rules) => {
    //test-to-fail that conditional fields are not showing
    //expose the data that determines which fields should be hidden, which is accessible
    //  on the `window` object
    cy.window().then(($win) => {
        for (const f of $win.CameraTrapDeployment.featuresHideDefault) {
            cy.dataCy(f).should('not.exist')
        }
    })

    //feature 1 (custom input)
    cy.getAndValidate('feature', { testEmptyField: ['done', 0] })
        .type('custom feature')
    cy.getAndValidate('feature_comment', { testEmptyField: ['done', 0] })
        .validateField()
        .type('comment\n\nnext line')

    cy.getAndValidate('distance_to_feature', { testEmptyField: ['done', 0] })
        .validateField()
        .type('1.5')
    // testing Feature Photo empty field
    cy.getAndValidate('feature_photo', { testEmptyField: ['done', 0], isComponent: true })
    cy.dataCy('addImg').eq(0).click().wait(500)
    cy.dataCy('takePhoto').click().wait(500)
    cy.dataCy('done').eq(0).click().wait(1000)
    //feature 2 (carcass)
    cy.dataCy('addEntry').eq(2).click().wait(1000)
    cy.dataCy('feature').selectFromDropdown('Carcass - natural')
    cy.dataCy('carcass_species')
        .should('exist')
        .invoke('attr', 'aria-label')
        .should('eq', 'Carcass Species')
    cy.getAndValidate('carcass_species', { testEmptyField: ['done', 0] })
        .selectFromSpeciesList('koala', false)
    cy.dataCy('feature_comment').type('comment')
    cy.dataCy('distance_to_feature').type('0.5')
    cy.dataCy('addImg').eq(0).click().wait(500)
    cy.dataCy('takePhoto').click().wait(500)
    cy.dataCy('done').eq(0).click().wait(1000)
    //feature 3 (plant)
    cy.dataCy('addEntry').eq(2).click().wait(1000)
    cy.selectFromDropdown('feature', 'Palatable plant')
    cy.dataCy('plant_species')
        .should('exist')
        .invoke('attr', 'aria-label')
        .should('eq', 'Plant Species')
    cy.getAndValidate('plant_species', { testEmptyField: ['done', 0] })
        .selectFromSpeciesList('acacia', false)
    cy.dataCy('feature_comment').type('comment')
    cy.dataCy('distance_to_feature').type('1')
    cy.dataCy('addImg').eq(0).click().wait(500)
    cy.dataCy('takePhoto').click().wait(500)
    cy.dataCy('done').eq(0).click().wait(1000)
    //feature 4 (sign)
    cy.dataCy('addEntry').eq(2).click().wait(1000)
    cy.selectFromDropdown('feature', 'Sign - tracks')
    cy.dataCy('sign_species')
        .should('exist')
        .invoke('attr', 'aria-label')
        .should('eq', 'Sign Species')
    cy.getAndValidate('sign_species', { testEmptyField: ['done', 0] })
        .selectFromSpeciesList('emu', false)
    cy.dataCy('feature_comment').type('comment')
    cy.dataCy('distance_to_feature').type('1')
    cy.dataCy('addImg').eq(0).click().wait(500)
    cy.dataCy('takePhoto').click().wait(500)
    cy.dataCy('done').eq(0).click().wait(1000)
    //feature 5 (bait station)
    cy.dataCy('addEntry').eq(2).click().wait(1000)
    cy.selectFromDropdown('feature', 'Bait station')
    cy.dataCy('bait_station_mount').should('exist')
    cy.selectFromDropdown('feature', 'Pitfall trap')
    cy.dataCy('bait_station_mount').should('not.exist')
    cy.selectFromDropdown('feature', 'Bait station - pest control')
    cy.dataCy('bait_station_mount').should('exist')
    cy.getAndValidate('bait_station_mount', { testEmptyField: ['done', 0] })
        .validateField()
        .selectFromDropdown('Star dropper')
    cy.dataCy('lure_height')
        .should('exist')
    cy.getAndValidate('lure_height', { testEmptyField: ['done', 0] })
        .type('20')
    cy.dataCy('lure_type').should('exist')
    cy.getAndValidate('lure_type')
        .selectFromDropdown('Visual')
    cy.getAndValidate('lure_variety')
        .selectFromDropdown('Feathers')
    cy.dataCy('lure_type',)
        .selectFromDropdown('Food')
    cy.dataCy('lure_variety')
        .selectFromDropdown('FOXOFF')
    //selecting a lure type shows the lure variety field
    cy.dataCy('feature_comment').type('comment')
    cy.dataCy('distance_to_feature').type('0.7')
    cy.dataCy('addImg').eq(0).click().wait(500)
    cy.dataCy('takePhoto').click().wait(500)
    cy.dataCy('done').eq(0).click().wait(1000)
})

Cypress.Commands.add('cameraTrapDeploymentPoint1CameraInfo', (rules) => {
    cy.dataCy('model').should('not.exist')
    cy.dataCy('make')
        .validateField(['done', -1])
        .selectFromDropdown('Reconyx')
    cy.dataCy('model').should('exist')
    cy.dataCy('model')
        .validateField(['done', -1])
        .selectFromDropdown('HyperFire HC600')
    cy.dataCy('manufacture_year')
        .validateField(['done', -1])
        .type('2000') //TODO test-to-fail
    cy.dataCy('illumination')
        .validateField(['done', -1])
        .selectFromDropdown('Infrared - low glow')
    cy.dataCy('activation_mechanism')
        .validateField(['done', -1])
        .selectFromDropdown('Passive infrared')
    cy.dataCy('trigger_speed')
        .validateField(['done', -1])
        .type(5)
})

Cypress.Commands.add('cameraTrapDeploymentPoint1CameraSettings', (rules) => {
    //test-to-fail that conditional fields are not showing
    cy.window().then(($win) => {
        for (const f of $win.CameraTrapDeployment.ctSettingsHideDefault) {
            cy.dataCy(f).should('not.exist')
        }
    })
    cy.dataCy('media_type')
        .validateField(['done', -1])
        .selectFromDropdown('Image + Video')
    cy.dataCy('images_per_trigger')
        .validateField(['done', -1])
        .type('10')
    cy.dataCy('image_interval')
        .validateField(['done', -1])
        .type('3')
    cy.dataCy('video_length')
        .validateField(['done', -1])
        .type('01:30')
    cy.dataCy('sensor_sensitivity')
        .validateField(['done', -1])
        .selectFromDropdown('Low')
    cy.dataCy('quiet_period')
        .validateField(['done', -1])
        .type('05:00')
    cy.dataCy('aspect_ratio')
        .validateField(['done', -1])
        .selectFromDropdown('4:3 standard')
    cy.dataCy('image_quality')
        .validateField(['done', -1])
        .type('15')
    cy.dataCy('video_resolution')
        .validateField(['done', -1])
        .selectFromDropdown('1080p (1920x1080)')
    cy.dataCy('day_night_recording')
        .validateField(['done', -1])
        .selectFromDropdown('Day + night')
    cy.dataCy('night_mode_shutter_speed')
        .validateField(['done', -1])
        .type('5')
    cy.dataCy('time_format')
        .validateField(['done', -1])
        .selectFromDropdown('12h')

    cy.dataCy('datetime').validateField(['done', -1])
    cy.selectDateTime({
        fieldName: 'datetime',
        date: {
            year: 'current',
            month: 'current',
            day: 28,
        },
        time: {
            hour: 14,
            minute: 'current',
        },
    })
    cy.dataCy('unit_of_temperature')
        .validateField(['done', -1])
        .selectFromDropdown('Celcius')
    cy.dataCy('battery_type')
        .validateField(['done', -1])
        .selectFromDropdown('NiMH rechargeable')
    cy.dataCy('user_label')
        .validateField(['done', -1])
        .type('camera001')

    cy.dataCy('time_lapse').click().wait(1000)
    cy.dataCy('time_lapse_interval')
        .validateField(['done', -1])
        .type('01:30')
    cy.dataCy('time_lapse_schedule_start_timeBtn').should('exist') //conditional field shown
    cy.selectTime({
        fieldName: 'time_lapse_schedule_start',
        time: {
            hour: 12,
            minute: 0,
        },
    })
    cy.dataCy('time_lapse_schedule_end_timeBtn').should('exist') //conditional field shown
    cy.selectTime({
        fieldName: 'time_lapse_schedule_end',
        time: {
            hour: 18,
            minute: 30,
        },
    })
    cy.dataCy('done').eq(0).click()   //save the time lapse schedule
})

Cypress.Commands.add('cameraTrapDeploymentPoint1Pt2', (rules) => {
    cy.getAndValidate('camera_trap_number', { testEmptyField: ['done', -1] })
        .type('001')

    cy.getAndValidate('SD_card_number', { testEmptyField: ['done', -1] })
        .type('001')

    cy.getAndValidate('camera_trap_height', { testEmptyField: ['done', -1] })
        .type('50')

    cy.getAndValidate('camera_trap_angle', { testEmptyField: ['done', -1] })
        .type('10')

    cy.getAndValidate('camera_trap_direction', { testEmptyField: ['done', -1] })
        .type('90')
    cy.getAndValidate('detection_angle', { testEmptyField: ['done', -1] })
        .type('0')
    cy.getAndValidate('slope', { testEmptyField: ['done', -1] })
        .type('0')
    cy.getAndValidate('aspect', { testEmptyField: ['done', -1] })
        .type('0')

    cy.getAndValidate('camera_trap_photo', { testEmptyField: ['done', -1], isComponent: true })
    cy.dataCy('addImg').click().wait(100)
    cy.dataCy('takePhoto').click().wait(100)

    cy.getAndValidate('habitat', { testEmptyField: ['done', -1] })
        .selectFromDropdown('Chenopod Shrublands, Samphire Shrublands and Forblands', 'Chenopod')
    cy.getAndValidate('deployment_comments', { testEmptyField: ['done', -1] })
        .type('notes\n\nnext line\n\nmore notes')

    cy.dataCy('done').eq(-1).click().wait(1000)
})

Cypress.Commands.add('cameraTrapDeploymentPoint2Pt1', (pointId = null) => {
    cy.dataCy('addEntry').eq(-1).click().wait(2000)

    //camera info and settings are autofilled from previous point
    cy.get('.q-notification').should(
        'contain.text',
        'Some fields have been auto-filled for your convenience: Camera Trap Information, Camera Trap Settings',
    )

    let pointIdToUse = null
    if (pointId === null) {
        pointIdToUse = `CTP${String(Math.floor(Math.random() * 10000))}`
        cy.log(`Using generated point ID: ${pointIdToUse}`)
    } else {
        cy.log(`Using provided point ID: ${pointId}`)
        pointIdToUse = pointId
    }
    cy.dataCy('camera_trap_point_id').type(pointIdToUse)
    cy.doDeploymentStartDateAndDeploymentId(pointIdToUse)

    //target taxa
    // cy.dataCy('target_taxa_types_1_expansion').click()    //opens expansion item
    cy.setExpansionItemState('target_taxa_types_1_expansion', 'open')
    cy.selectFromDropdown('taxa_type', 'Reptile')
    cy.dataCy('done').eq(0).click().wait(1000)
    cy.dataCy('addEntry').eq(0).click().wait(1000)
    cy.selectFromDropdown('taxa_type', 'Invertebrate')
    cy.dataCy('done').eq(0).click().wait(1000)

    cy.selectFromSpeciesList('species', 'emu', false)
    cy.dataCy('done').eq(0).click().wait(1000)

    cy.selectFromDropdown('camera_trap_mount', 'Tree - branch')
    cy.recordLocation()
})

Cypress.Commands.add('cameraTrapDeploymentPoint2Features', () => {
    cy.window().then(($win) => {
        for (const f of $win.CameraTrapDeployment.featuresHideDefault) {
            cy.dataCy(f).should('not.exist')
        }
    })
    //already tested all feature conditional fields in 1st point, so don't need that here
    cy.selectFromDropdown('feature', 'Road - sealed')
    cy.dataCy('feature_comment').type('comment')
    cy.dataCy('distance_to_feature').type('1.5')
    cy.dataCy('addImg').eq(0).click().wait(500)
    cy.dataCy('takePhoto').click().wait(500)
    cy.dataCy('done').eq(0).click().wait(1000)
})

Cypress.Commands.add('cameraTrapDeploymentPoint2CameraInfo', () => {
    //should be auto-filled, but also make a couple changes
    //TODO assert the specific fields that were autofilled (be careful, if we change input data in 1st point, it could bust this)

    //change the make, so model should clear
    cy.selectFromDropdown('make', 'Muddy')
    cy.dataCy('model').should('exist').and('be.empty')
    cy.selectFromDropdown('model', 'Merge')

    //now if we fully clear make, model should not be visible
    cy.dataCy('make').clearDropdown()

    cy.dataCy('model').should('not.exist')

    //now do the final make/model selection
    cy.dataCy('make').type('Some unknown CT make').wait(200).type('{enter}')
    cy.dataCy('model').type('Some unknown CT model').wait(200).type('{enter}')
})

Cypress.Commands.add('cameraTrapDeploymentPoint2CameraSettings', () => {
    //should be auto-filled, but also make a couple changes (specifically this is image
    //only, not image and video like 1st point)
    cy.selectFromDropdown('media_type', 'Image')
    cy.dataCy('video_length').should('not.exist')
    cy.dataCy('video_resolution').should('not.exist')
})

Cypress.Commands.add('cameraTrapDeploymentPoint2Pt2', () => {
    cy.dataCy('camera_trap_number').type('002')
    cy.dataCy('SD_card_number').type('002')

    cy.dataCy('camera_trap_height').type('80')
    cy.dataCy('camera_trap_angle').type('0')
    cy.dataCy('camera_trap_direction').type('0')
    cy.dataCy('detection_angle').type('0')
    cy.dataCy('slope').type('10')
    cy.dataCy('aspect').type('10')
    cy.dataCy('addImg').eq(0).click().wait(500)
    cy.dataCy('takePhoto').eq(0).click().wait(500)
    cy.dataCy('done').eq(0).click().wait(1000)
    // FIXME selecting the same date already entered on the date picker will empty the field 
    // cy.customSelectDate({
    //     fieldName: 'deployment_start_date',
    //     date: {
    //         year: 'current',
    //         month: 'current',
    //         day: 'current',
    //     },
    // })
    cy.selectFromDropdown('habitat', 'Unclassified Forest', 'Unclassified')

    cy.dataCy('done').eq(-1).click().wait(1000)
})

Cypress.Commands.add('cameraTrapDeploymentPoint3Pt1', (pointId = null) => {
    cy.dataCy('addEntry').eq(-1).click().wait(2000)

    //camera info and settings are autofilled from previous point
    cy.get('.q-notification').should(
        'contain.text',
        'Some fields have been auto-filled for your convenience: Camera Trap Information, Camera Trap Settings',
    )

    let pointIdToUse = null
    if (pointId === null) {
        pointIdToUse = `CTP${String(Math.floor(Math.random() * 10000))}`
        cy.log(`Using generated point ID: ${pointIdToUse}`)
    } else {
        cy.log(`Using provided point ID: ${pointId}`)
        pointIdToUse = pointId
    }
    cy.dataCy('camera_trap_point_id').type(pointIdToUse)
    cy.doDeploymentStartDateAndDeploymentId(pointIdToUse)

    //target species
    // cy.dataCy('target_species_1_expansion').click()    //opens expansion item
    cy.setExpansionItemState('target_species_1_expansion', 'open')
    cy.selectFromSpeciesList('species', 'fox', false)
    cy.dataCy('done').eq(1).click().wait(1000)
    cy.dataCy('addEntry').eq(1).click().wait(1000)
    cy.selectFromSpeciesList('species', 'dingo', false)
    cy.dataCy('done').eq(1).click().wait(1000)

    cy.selectFromDropdown('camera_trap_mount', 'Tripod')
    cy.recordLocation()
})

Cypress.Commands.add('cameraTrapDeploymentPoint3Features', () => {
    cy.window().then(($win) => {
        for (const f of $win.CameraTrapDeployment.featuresHideDefault) {
            cy.dataCy(f).should('not.exist')
        }
    })
    //already tested all feature conditional fields in 1st point, so don't need that here
    cy.selectFromDropdown('feature', 'Road - unsealed')
    cy.dataCy('feature_comment').type('comment')
    cy.dataCy('distance_to_feature').type('2.3')
    cy.dataCy('addImg').eq(0).click().wait(500)
    cy.dataCy('takePhoto').click().wait(500)
    cy.dataCy('done').eq(1).click().wait(1000)

    cy.selectFromDropdown('taxa_type', 'Bird')
    cy.dataCy('done').eq(0).click().wait(1000)
})

Cypress.Commands.add('cameraTrapDeploymentPoint3CameraInfo', () => {
    //should be auto-filled, but also make a couple changes
    //TODO assert the specific fields that were autofilled (be careful, if we change input data in 1st point, it could bust this)
    cy.selectFromDropdown('model', 'HyperFire HC500')
})

Cypress.Commands.add('cameraTrapDeploymentPoint3CameraSettings', () => {
    //should be auto-filled, but also make a couple changes (specifically this is video
    //only, not image and video like 1st point)
    cy.selectFromDropdown('media_type', 'Video')
    cy.dataCy('images_per_trigger').should('not.exist')
    cy.dataCy('image_interval').should('not.exist')
    cy.dataCy('image_quality').should('not.exist')
})

Cypress.Commands.add('cameraTrapDeploymentPoint3Pt2', () => {
    cy.dataCy('camera_trap_number').type('003')
    cy.dataCy('SD_card_number').type('003')

    cy.dataCy('camera_trap_height').type('0')
    cy.dataCy('camera_trap_angle').type('0')
    cy.dataCy('camera_trap_direction').type('0')
    cy.dataCy('detection_angle').type('0')
    cy.dataCy('addImg').click().wait(500)
    cy.dataCy('takePhoto').click().wait(500)
    // cy.customSelectDate({
    //     fieldName: 'deployment_start_date',
    //     date: {
    //         year: 'current',
    //         month: 'current',
    //         day: 'current',
    //     },
    // })
    cy.selectFromDropdown('habitat', 'Hummock Grasslands', 'Hummock')

    cy.dataCy('done').eq(-1).click().wait(1000)
})

Cypress.Commands.add('cameraTrapReequippingPoint1', (cameraToReequip) => {
    cy.dataCy('deployment_id', {
        testEmptyField: ['done', -1],
    }).selectFromDropdown(cameraToReequip.deployment_id)

    const currDeploymentDateYear = parseInt(
        cameraToReequip.deployment_start_date.split('-')[0],
    )

    cy.getAndValidate('reequipping_date_dateBtn', { testEmptyField: ['done', -1] })
    cy.customSelectDate({
        fieldName: 'reequipping_date',
        date: {
            //re-equip 1 year after deployment (easier than trying to add months/days)
            year: currDeploymentDateYear + 1,
            month: 'current',
            day: 'current',
        },
    })

    //field only shows when we select a valid operational status
    cy.dataCy('camera_needs_replacing').should('not.exist')

    cy.getAndValidate('operational_status', {
        testEmptyField: ['done', -1],
    }).selectFromDropdown('Vandalism')
    cy.dataCy('camera_needs_replacing').should('exist')

    cy.get('[data-cy="number_of_images"]').should('not.exist')
    cy.getAndValidate('replacement_camera_trap_number', {
        testEmptyField: ['done', -1],
    }).type('987654321')

    cy.getAndValidate('replacement_SD_card_number', {
        testEmptyField: ['done', -1],
    }).type(Math.floor(Math.random() * 10000))
    cy.getAndValidate('replacement_camera_trap_number', {
        testEmptyField: ['done', -1],
    }).type(Math.floor(Math.random() * 10000))
    cy.setCheck('camera_needs_replacing') // TODO assert all auto filled correctly
    cy.get('.q-notification').should(
        'contain.text',
        "Field 'Camera Trap Information' auto-filled for your convenience",
    )
    cy.getAndValidate('camera_trap_height', {
        testEmptyField: ['done', -1],
    }).type('8')
    cy.getAndValidate('camera_trap_angle', { testEmptyField: ['done', -1] }).type(
        '7',
    )
    cy.getAndValidate('camera_trap_direction', {
        testEmptyField: ['done', -1],
    }).type('6')
    cy.getAndValidate('camera_trap_mount', {
        testEmptyField: ['done', -1],
    }).selectFromDropdown('Tripod')
    cy.getAndValidate('bait_station_mount', {
        testEmptyField: ['done', -1],
    }).selectFromDropdown('Tent peg')
    cy.getAndValidate('lure_height', { testEmptyField: ['done', -1] }).type('6')
    cy.addImages('addImg', 1)
    cy.getAndValidate('reequipping_comments', { testEmptyField: ['done', -1] }).type(
        'comment comment comment',
    )
    cy.get('[data-cy="done"]').eq(-1).click()
    cy.wait(100)
    cy.get('[data-cy="addEntry"]')
        .eq(-1)
        .click()
        .wait(1000)
        .then(() => {
            cy.get('[data-cy$="expansion"]').should('have.length', 2)
        })
})

Cypress.Commands.add('cameraTrapReequippingPoint2', (cameraToReequip) => {
    //not using this field
    cy.dataCy('camera_needs_replacing').should('not.exist')
    //field only shows when we select a valid operational status
    cy.dataCy('number_of_images').should('not.exist')

    cy.selectFromDropdown(
        'deployment_id',
        cameraToReequip.deployment_id,
    )

    const currDeploymentDateYear = parseInt(
      cameraToReequip.deployment_start_date.split('-')[0],
    )
    cy.customSelectDate({
        fieldName: 'reequipping_date',
        date: {
            //re-equip 1 year after deployment (easier than trying to add months/days)
            year: currDeploymentDateYear + 1,
            month: 'current',
            day: 'current',
        },
    })

    cy.selectFromDropdown('operational_status', 'Operational')
    cy.getAndValidate('number_of_images', { testEmptyField: ['done', -1] }).type('42')
    cy.dataCy('replacement_SD_card_number').type(
        Math.floor(Math.random() * 10000),
    )
    cy.getAndValidate('replacement_battery_type', { testEmptyField: ['done', -1] }).selectFromDropdown('NiMH rechargeable')
    cy.selectFromDropdown('camera_trap_mount', 'Tripod')

    cy.selectFromDropdown('bait_station_mount', 'Tent peg')
    cy.dataCy('lure_height').type('6')
    cy.getAndValidate('lure_type', { testEmptyField: ['done', -1] }).selectFromDropdown('Audio')
    cy.getAndValidate('lure_variety', { testEmptyField: ['done', -1] }).selectFromDropdown('Ultrasonic')
    cy.dataCy('reequipping_comments').type('comment comment comment')

    cy.get('[data-cy="done"]').eq(-1).click().wait(100)
    cy.get('[data-cy="addEntry"]')
        .eq(-1)
        .click()
        .wait(1000)
        .then(() => {
            cy.get('[data-cy$="expansion"]').should('have.length', 3)
        })
})

Cypress.Commands.add('cameraTrapReequippingPoint3', (cameraToReequip) => {
    //fields only shows when we select a valid operational status (some are also
    //not used)
    cy.dataCy('number_of_images').should('not.exist')
    cy.dataCy('replacement_SD_card_number').should('not.exist')
    cy.dataCy('replacement_camera_trap_number').should('not.exist')
    cy.dataCy('camera_needs_replacing').should('not.exist')
    // broken
    cy.selectFromDropdown(
        'deployment_id',
        cameraToReequip.deployment_id,
    )

    const currDeploymentDateYear = parseInt(
      cameraToReequip.deployment_start_date.split('-')[0],
    )
    cy.customSelectDate({
        fieldName: 'reequipping_date',
        date: {
            //re-equip 1 year after deployment (easier than trying to add months/days)
            year: currDeploymentDateYear + 1,
            month: 'current',
            day: 'current',
        },
    })

    cy.selectFromDropdown(
        'operational_status',
        'Wildlife damage',
        'Wildlife damage',
    )
    cy.dataCy('replacement_SD_card_number')
        .should('exist')
        .type(Math.floor(Math.random() * 10000))
    cy.selectFromDropdown('camera_trap_mount', 'Rock pile')

    cy.selectFromDropdown('bait_station_mount', null)   //any selection is fine
    cy.dataCy('lure_height').type('15')
    cy.selectFromDropdown('lure_type', 'Olfactory')
    cy.selectFromDropdown('lure_variety', 'Dog Urine', 'Dog Urine')
    cy.dataCy('reequipping_comments').type('comment comment comment')

    cy.get('[data-cy="done"]').eq(-1).click().wait(100)
})

Cypress.Commands.add('cameraTrapRetrievalPoint1', (cameraToRetrieve) => {
    cy.dataCy('retrieval_date_dateBtn').should('not.exist')
    cy.testEmptyField(
        'Field: "Deployment ID": This field is required',
        'done',
    )
    cy.dataCy('deployment_id', {
        testEmptyField: ['done', -1],
    }).selectFromDropdown(cameraToRetrieve.deployment_id)

    cy.getAndValidate('retrieval_date_dateBtn', { testEmptyField: ['done', -1] })
    // make sure the date is always ahead of the current date by at least 3 days. That way asserting deployment_period is eas
    const start_date = new Date(cameraToRetrieve.deployment_start_date)
    const end_date = new Date()
    cy.customSelectDate({
        fieldName: 'retrieval_date',
        date: {
            year: 'current',
            month: 'current',
            day: 'current',
        },
    })
    
    cy.getAndValidate('operational_status', {
        testEmptyField: ['done', -1],
    }).selectFromDropdown('Missing/theft')

    cy.getAndValidate('number_of_images', { testEmptyField: ['done', -1] }).type(
        '4',
    )

    // TODO: add validateField to this, not possible atm as it's autofilled
    cy.dataCy('deployment_period').should(
        'have.value',
        Math.round((end_date - start_date) / (1000 * 60 * 60 * 24)),
    )

    cy.getAndValidate('retrieval_comments', { testEmptyField: ['done', -1] }).type(
        'This is a retrieval note',
    )
    cy.dataCy('done').eq(-1).click()
    cy.get('[data-cy="addEntry"]')
        .eq(-1)
        .click()
        .wait(1000)
        .then(() => {
            cy.get('[data-cy$="expansion"]').should('have.length', 2)
        })
})

Cypress.Commands.add('cameraTrapRetrievalPoint2', (cameraToRetrieve, previousRetrievedCamera) => {
    cy.dataCy('deployment_id').click().then(() => {
        cy.withinSelectMenu({
          persistent: true,
          fn: () => {
            cy.get('.q-item[role=option]').as('dropdownOptions')
            cy.get('@dropdownOptions')
              .contains(previousRetrievedCamera.deployment_id)
              .parents('.q-item')
              .should('have.attr', 'aria-disabled', 'true')
              .type('{esc}')
          },
        })
    })
    cy.selectFromDropdown(
        'deployment_id',
        cameraToRetrieve.deployment_id,
    )
    cy.selectFromDropdown('operational_status', 'Unknown failure')
    cy.get('[data-cy="number_of_images"]').type('4')

    // make sure the date is always ahead of the current date by at least 3 days. That way asserting deployment_period is easy
    const start_date = new Date(cameraToRetrieve.deployment_start_date)
    const end_date = new Date()
    cy.customSelectDate({
        fieldName: 'retrieval_date',
        date: {
            year: 'current',
            month: 'current',
            day: 'current',
        },
    })
    cy.dataCy('deployment_period').should(
        'have.value',
        Math.round((end_date - start_date) / (1000 * 60 * 60 * 24)),
    )

    cy.dataCy('retrieval_comments').type('This is a retrieval note')
    cy.dataCy('done').eq(-1).click()
    cy.get('[data-cy="addEntry"]')
        .eq(-1)
        .click()
})

Cypress.Commands.add('cameraTrapRetrievalPoint3', (cameraToRetrieve) => {
    cy.selectFromDropdown(
        'deployment_id',
        cameraToRetrieve.deployment_id,
    )
    cy.selectFromDropdown('operational_status', 'Unknown failure')
    cy.get('[data-cy="number_of_images"]').type('4')

    // make sure the date is always ahead of the current date by at least 3 days. That way asserting deployment_period is easy
    const start_date = new Date(cameraToRetrieve.deployment_start_date)
    const end_date = new Date()
    cy.customSelectDate({
        fieldName: 'retrieval_date',
        date: {
            year: 'current',
            month: 'current',
            day: 'current',
        },
    })
    cy.dataCy('deployment_period').should(
        'have.value',
        Math.round((end_date - start_date) / (1000 * 60 * 60 * 24)),
    )

    cy.dataCy('retrieval_comments').type('This is a retrieval note')
    cy.dataCy('done').eq(-1).click()
})

Cypress.Commands.add('doDeploymentStartDateAndDeploymentId', (ctPointIdToUse) => {
  cy.customSelectDate({
    fieldName: 'deployment_start_date',
    date: {
      year: 'current',
      month: 'current',
      day: 'current',
    },
  })
  const todaysDate = new Date()
  const selectedDate = `${todaysDate.getFullYear()}-${String(todaysDate.getMonth() + 1).padStart(2, '0')}-${String(todaysDate.getDate()).padStart(2, '0')}`
  cy.dataCy('deployment_id')
    .should('be.disabled')
    .invoke('val')
    .then((deploymentId) => {
      cy.log(`checking deployment ID '${deploymentId}' was correctly autofilled`)
      cy.wrap(deploymentId).should('contain', `${ctPointIdToUse}-${selectedDate}`)
    })
})

registerCommands()
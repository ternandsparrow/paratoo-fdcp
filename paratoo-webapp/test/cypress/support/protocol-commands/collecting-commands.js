import { registerCommands } from "@quasar/quasar-app-extension-testing-e2e-cypress"


Cypress.Commands.add(
    'fillInFloristicsLiteForm',
    (recollections, recollectOnly = false) => {
        cy.dataCy('workflowNextButton').click().wait(3000) //survey step

        //set reader to false to enable images to be taken in the dialog
        cy.setCheck('toggleBarcodeReaderShow', false)

        //re-collect 4 existing vouchers
        for (const fieldName of recollections) {
            console.debug(`Re-collecting voucher: ${fieldName}`)
            cy.dataCy(`collectAgainRow_${fieldName.replaceAll(' ', '_')}`).within(
                () => {
                    cy.dataCy('collectAgain').click()
                },
            )
            cy.get('#floristics_virtual_voucher').within(() => {
                cy.selectFromDropdown('growth_form_1', null)
                cy.selectFromDropdown('habit', null)
                cy.selectFromDropdown('phenology', null)

                cy.get(`[data-cy="addImg"]`).then((imgList) => {
                    for (const [index, button] of Array.from(imgList).entries()) {
                        if (index === 0 || index === 1) {
                            //only 'plant' and 'leaf'
                            cy.get(imgList[index]).click()
                            cy.get('[data-cy="takePhoto"]').eq(index).click()
                            cy.wait(100)
                        }
                    }
                })
                cy.wait(100)
                cy.dataCy('done').eq(-1).click()
            })
            cy.wait(100)
            cy.dataCy(`collectAgainRow_${fieldName.replaceAll(' ', '_')}`).within(
                () => {
                    cy.dataCy('collectAgain').should(
                        'contain.text',
                        'Edit Repeat Collection',
                    )
                },
            )
        }

        if (!recollectOnly) {
            //collect 3 new vouchers
            for (let i = 0; i < 3; i++) {
                cy.setCheck('toggleBarcodeReaderShow', true)
                cy.selectFromSpeciesList(
                    'field_name',
                    `offline voucher lite ${i + 1}`,
                    true,
                )
                cy.dataCy('recordBarcode').click().wait(200)
                cy.selectFromDropdown('growth_form_1', null)
                cy.selectFromDropdown('habit', null)
                cy.selectFromDropdown('phenology', null)
                //add two photos for each voucher
                for (let j = 0; j < 2; j++) {
                    cy.wait(500)
                    cy.addImages('addImg', 1)
                    cy.dataCy('comment').type('comment' + (j + 1))
                    cy.dataCy('done').eq(0).click()
                    if (j < 1) cy.dataCy('addEntry').eq(0).click()
                }
                cy.wait(100)
                cy.dataCy('done').eq(-1).click()
                if (i < 2) {
                    //add new entry except on the last
                    cy.wait(100)
                    cy.dataCy('addEntry').eq(-1).click()
                }
            }
        }
    },
)

Cypress.Commands.add('collectFireCharObservations', () => {
  const sampleLocations = ['North West', 'North East', 'South East', 'South West']
    for (const [index, sampleLocation] of sampleLocations.entries()) {
        if (index > 1) {
            //when there are only two sample locations left, dropdown turns into radio
            // cy.dataCy(sampleLocation.replaceAll(' ', '\\')).click()
            cy.get(`[data-cy="${sampleLocation}"]`).click()
        } else {
            cy.selectFromDropdown('fire_char_sample_location', sampleLocation)
        }
        cy.dataCy('char_present').click()
        cy.dataCy('maximum_trunk_char_height').type(
            `${(Math.random() * 10).toFixed(2)}`,
        )
        cy.dataCy('done').click().wait(1000)

        if (index !== 3) {
            //don't add another entry on last sample location
            cy.dataCy('addEntry').click()
        }
    }
})

/**
 * Collects survey step for Floristics when doing PTV as submodule
 * 
 * @param {'full' | 'lite'} submoduleProtocolVariant the variant of the PTV submodule
 */
Cypress.Commands.add('ptvSubmoduleInFloristicsSurveyStep', (submoduleProtocolVariant) => {
    cy.dataCy('Standard').should('not.exist')
    cy.dataCy('Enhanced').should('not.exist')
    cy.dataCy('collect_ptv_submodule').check()
    cy.dataCy('Standard').should('exist')
    cy.dataCy('Enhanced').should('exist')

    if (submoduleProtocolVariant === 'full') {
        cy.dataCy('Enhanced').click()
    } else if (submoduleProtocolVariant === 'lite') {
        cy.dataCy('Enhanced').click()
    }
    cy.nextStep()
})

/**
 * Collects a single Plant Tissue Voucher as a submodule in Floristics
 *
 * @param {Number} index the Floristics voucher index
 * @param {String} variant the floristics voucher variant 'full' or 'lite'
 * @param {Boolean} [offline] whether we're collecting offline (to append to field name)
 */
Cypress.Commands.add(
    'collectPtvAsSubmodule',
    (index, variant, offline = false) => {
        //field name
        cy.selectFromSpeciesList(
            'field_name',
            `floristics voucher ${variant}${offline ? ' offline' : ''} ${index + 1}`,
            true,
        )

        cy.dataCy('toggleBarcodeReaderShow').toggle(true)
        cy.dataCy('recordBarcode').click()
        cy.wait(200)

        //any growth form is fine
        cy.selectFromDropdown('growth_form_1', null)

        cy.dataCy('openPtvModal').click().wait(100)
        //need to wait for a field to appear (i.e., crud is loaded)
        cy.dataCy('has_replicate').should('exist')

        //odd indexes collect replicates (1st index (2nd ob) when calling this command 3
        //times, such as offline-floristics-and-dependencies)
        const collectReplicates = index % 2 !== 0
        cy.log(`PTV ${collectReplicates ? 'is' : 'is not'} collecting replicates`)
        cy.dataCy('toggleBarcodeReaderShow').eq(-1).toggle(true)
        if (collectReplicates) {
            cy.dataCy('has_replicate').click()
            cy.dataCy('min_distance_between_replicates').type('1')
            for (let j = 0; j < 5; j++) {
                cy.dataCy('recordBarcode').eq(1).click().wait(200)
            }
        } else {
            cy.dataCy('recordBarcode').eq(1).click().wait(200)
        }
        cy.dataCy('done').eq(2).click()
        cy.get('.q-notification', { timeout: 10000 }).should(
            'contain.text',
            'Please submit the floristics voucher to save changes associated with plant tissue vouchers',
        )

        cy.dataCy('closePtvModal').click().wait(100)
        //save the floristics voucher
        cy.dataCy('done').eq(-1).click()
        if (index < 2) {
            cy.dataCy('addEntry').eq(-1).click()
        }
    },
)

/**
 * Collects Recruitment Age Structure Growth Form and Life Stage step
 *
 * @param {Array} speciesToCollect an array of objects like (for vouchers):
 * {
 *  fieldDataCy: 'vouchers',
 *  selection: 'Voucher Full Example 2',
 *  seedlingCount: 5,
 *  saplingCount: 3,
 *  juvenileCount: 0,
 * }
 * or for new species (don't specify `itemToSelect` if you want a field name):
 * {
 *  fieldDataCy: 'species',
 *  selection: {
 *    searchQuery: 'acacia',
 *    itemToSelect: 'Acacia abbatiana [Species]',
 *  },
 *  seedlingCount: 2,
 *  saplingCount: 0,
 *  juvenileCount: 8,
 * },
 */
Cypress.Commands.add(
    'collectRecruitmentGrowthFormAndLifeStages',
    (speciesToCollect) => {
        //TODO test editing
        //TODO test indeterminate checkboxes - can't seem to get a handle on the prop to check it
        for (const [index, species] of speciesToCollect.entries()) {
            cy.log(
                `Adding species based on metadata: ${JSON.stringify(species, null, 2)}`,
            )
            cy.dataCy('addSpecies').click().wait(500)

            cy.selectFromDropdown(species.fieldDataCy, species.selection)

            //collection 2x of any growth and life stage - doesn't matter what
            for (let i = 0; i < 2; i++) {
                cy.selectFromDropdown('growth_stage', null)
                cy.selectFromDropdown('life_stage', null)

                cy.dataCy('done').eq(0).click()

                if (i < 1) cy.dataCy('addEntry').eq(0).click().wait(100)
            }

            cy.dataCy('done').click()
        }
    },
)

/**
 * Collects Recruitment Age Structure Sapling and Seedling Count step
 *
 * @param {Array} speciesToCollect see `collectRecruitmentGrowthFormAndLifeStages`
 * @param {Number} [indexOffset] an index offset - useful if this command is called after
 * we've already got some species
 */
Cypress.Commands.add('collectSaplingAndSeedlingCount', (speciesToCollect, indexOffset = 0) => {
    for (const [index, species] of speciesToCollect.entries()) {
        //each count we use to initialise an empty array of zeros to iterate over
        for (const _ of Array(species.seedlingCount).fill(0)) {
            cy.dataCy('seedlingCountIncrement').eq(index + indexOffset).click().wait(100)
        }
        cy.dataCy('seedlingCountValue')
            .eq(index + indexOffset)
            .should('have.text', species.seedlingCount)

        for (const _ of Array(species.saplingCount).fill(0)) {
            cy.dataCy('saplingCountIncrement').eq(index + indexOffset).click().wait(100)
        }
        cy.dataCy('saplingCountValue')
            .eq(index + indexOffset)
            .should('have.text', species.saplingCount)

        for (const _ of Array(species.juvenileCount).fill(0)) {
            cy.dataCy('juvenileCountIncrement').eq(index + indexOffset).click().wait(100)
        }
        cy.dataCy('juvenileCountValue')
            .eq(index + indexOffset)
            .should('have.text', species.juvenileCount)
    }
})

registerCommands()
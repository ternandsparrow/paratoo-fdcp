import { registerCommands } from '@quasar/quasar-app-extension-testing-e2e-cypress'
import { isHashtagTransect } from '../command-utils'
/**
 * Completes a single point intercept point by pseudo-randomly inputting data
 *
 * @param {String} transect the full LUT label string - e.g., 'South 1'
 * @param {Number} pointIndex the point number index
 * @param {Boolean} [isLite] whether the protocol variant is lite, which determines how
 * species intercepts are made
 * @param {Boolean} [isFire] whether Fire Survey is being completed as a submodule
 * @param {Boolean} [isOffline] whether we are collecting offline - will force vouchers
 * containing 'offline' to be collected more often, as offline Floristics collections use
 * field names in this way
 */
Cypress.Commands.add(
  'completePointInterceptPoint',
  (transect, pointIndex, isLite = false, isFire = false, isOffline = false) => {
    const substrates = [
      'Bare',
      'Crypto',
      'Litter',
      'CWD',
      'Gravel',
      'Rock',
      'Outcrop',
      'Lichen\\ on\\ rock',
      'Lichen\\ on\\ outcrop',
      'Water',
      'Black\\ ash',
      'White\\ ash',
      'Unknown',
      'Other',
      'Not\\ Collected',
    ]
    const fractionalCovers = [
      'Photosynthetic vegetation',
      'Non-photosynthetic vegetation',
      'Branch',
    ]

    //randomly decide if we want to keep previous substrate, the select one at random
    //(except the first ever point, which *needs* a selection)
    if (
      (!isLite && transect === 'South 1' && pointIndex === 0) ||
      (isLite && transect === 'South 2' && pointIndex === 0) ||
      Math.random() > 0.5
    ) {
      //TODO charred and scorched for fire
      const subStrateToUse = _.sample(substrates)
      cy.dataCy(subStrateToUse).click()
      cy.dataCy('selectedSubstrateInfoBtn').click()
      cy.dataCy('selectedSubstrateSummaryName')
        .should('contain', subStrateToUse.replaceAll('\\', ''))
        .type('{esc}')
    }

    //randomly decide if we're doing a species intercept (50% of the time)
    if (Math.random() > 0.5) {
      let empty = true
      let newRecord = null
      cy.window()
        .then(($win) => {
          newRecord = $win.PointIntercept.newRecord
        })
        .then(() => {
          if (isLite) {
            //TODO check fire for lite fields too
            if (newRecord.fractional_cover && newRecord.height) {
              empty = false
            }
          } else {
            if (isFire) {
              if (
                //it's good enough checking for growth_form/floristics_voucher and
                //height, but not check `plant_alive_status` or `resprouting_status`
                ((newRecord.plant_unidentifiable &&
                  newRecord.fire_growth_form) ||
                  (!newRecord.plant_unidentifiable &&
                    newRecord.floristics_voucher)) &&
                newRecord.height
              ) {
                empty = false
              }
            } else {
              if (newRecord.floristics_voucher && newRecord.height) {
                empty = false
              }
            }
          }
        })
        .then(() => {
          const formIsEmpty = empty
          if (
            //we want an intercept, but if the form is empty then we need to fill it out regardless
            formIsEmpty ||
            //half the time we clear the point's form and create a new intercept, else just
            //keep the auto-filled intercept from the previous point
            Math.random() > 0.5
          ) {
            // cy.dataCy('clearIntercept').click()

            if (isLite) {
              const fractionalCoverToUse = _.sample(fractionalCovers)
              cy.selectFromRadioOptionGroup(
                'fractional_cover',
                fractionalCoverToUse,
              )
              if (fractionalCoverToUse === 'Branch' && Math.random() > 0.9) {
                //10% of the time mark as 'dead'
                cy.dataCy('dead').click()
              }
              if (
                fractionalCoverToUse === 'Photosynthetic vegetation' &&
                Math.random() > 0.8
              ) {
                //20% of the time mark as 'in canopy sky'
                cy.dataCy('in_canopy_sky').click()
              }

              if (
                //above fields are valid for fire, but there is also some other fields
                isFire &&
                //40% of the time specify the plant as resprouting, but only hashtag
                //transects are valid
                Math.random() > 0.6
              ) {
                cy.dataCy('plantIsResprouting').click()
                cy.selectFromDropdown('resproutingType', null, null, null)
              }
            } else {
              if (isFire) {
                //10% of the time the plant cannot be identified
                const plantShouldBeUnidentifiable = Math.random() > 0.9
                if (plantShouldBeUnidentifiable) {
                  cy.dataCy('plantUnidentifiable').click()
                  cy.selectFromDropdown(
                    'fireGrowthForm',
                    null,
                    null,
                    null,
                    true,
                  )
                } else {
                  // pick random voucher
                  cy.get('.voucher-btn').then((elements) => {
                    const randomIndex = Math.floor(
                      Math.random() * elements.length,
                    )
                    cy.wrap(elements[randomIndex]).click()
                  })
                }

                //if we're on a hashtag transect, we specify the `fire impact`
                const selectFireImpact =
                  isHashtagTransect(transect) && Math.random() > 0.9
                if (selectFireImpact) {
                  const fireImpactCheckboxes = ['charred', 'scorched']
                  cy.dataCy(
                    `fire_impact_${_.sample(fireImpactCheckboxes)}`,
                  ).click()
                }

                if (Math.random() > 0.9) {
                  //10% of the time mark as 'dead'
                  cy.dataCy('dead').click()
                }

                if (Math.random() > 0.8) {
                  //20% of the time mark as 'in canopy sky'
                  cy.dataCy('in_canopy_sky').click()
                }

                if (Math.random > 0.6) {
                  //40% of the time specify the plant as resprouting, but only hashtag
                  //transects are valid
                  cy.dataCy('plantIsResprouting').click()
                  cy.selectFromDropdown('resproutingType', null, null, null)
                }
              } else {
                // pick random voucher
                cy.get('.voucher-btn').then((elements) => {
                  const randomIndex = Math.floor(
                    Math.random() * elements.length,
                  )
                  cy.wrap(elements[randomIndex]).click()
                })
                // cy.dataCy('height').type(`${(Math.random() * 50).toFixed(2)}`)
                if (Math.random() > 0.9) {
                  //10% of the time mark as 'dead'
                  cy.dataCy('dead').click()
                }
                if (Math.random() > 0.8) {
                  //20% of the time mark as 'in canopy sky'
                  cy.dataCy('in_canopy_sky').click()
                }
              }
            }

            // height is required for addIntercept to become enabled now
            cy.dataCy('height')
              .clear()
              .type(`${(Math.random() * 50).toFixed(2)}`)
            cy.dataCy('addIntercept').click()

            if (pointIndex % 2 === 0) {
              cy.log(`Editing intercept on pointIndex=${pointIndex}`)
              cy.dataCy('editBtnLabel').click()

              //TODO edit more fields
              cy.dataCy('heightEdit')
                .clear()
                .type(`${(Math.random() * 50).toFixed(2)}`)
              
              //save the edit
              cy.dataCy('editBtnLabel').click()
            }
          }

          if (!formIsEmpty) {
            //only keep previous intercept if the form in filled in
            cy.dataCy('height')
              .clear()
              .type(`${(Math.random() * 50).toFixed(2)}`)
            cy.dataCy('addIntercept').click()
          }
        })
    }

    cy.dataCy('nextPointBtnLabel').click()
  },
)

registerCommands()

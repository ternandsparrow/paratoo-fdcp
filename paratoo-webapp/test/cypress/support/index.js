// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands';
import './command-utils';
import './commands-publish'
import './functions'
import './protocol-commands/camera-trap-commands'
import './protocol-commands/collecting-commands'
import './protocol-commands/condition-commands'
import './protocol-commands/non-specific-commands'
import './protocol-commands/point-intercept-commands'
import './protocol-commands/recruitment-commands'
import './protocol-commands/soils-commands'
import './protocol-commands/vert-commands'
import './protocol-commands/third-set-commands'
import './protocol-commands/plot-selection'
import './paratoo-commands'
import '@cypress/code-coverage/support'

import {
  waitForHydration,
  waitForWindowData,
} from './command-utils'

Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from failing the test
  return false
})

before(() => {
  waitForHydration()
  waitForWindowData()
})

// if a test is hanging somewhere we force it to error so hopefully it moves on or stops
// https://github.com/cypress-io/cypress/issues/5302
let timeoutId
beforeEach(() => {
  timeoutId = setTimeout(() => {
    throw new Error("likely hanging, a test block shouldn't take this long")
  }, 60000 * 5);
})
afterEach(() => {
  clearTimeout(timeoutId)
})
// https://github.com/quasarframework/quasar-testing/tree/dev/packages/e2e-cypress#upgrade-from-cypress-ae-v3--quasar-v1
// https://github.com/quasarframework/quasar/issues/2233#issuecomment-492975745
// "ResizeObserver loop limit exceeded" fix no longer needed

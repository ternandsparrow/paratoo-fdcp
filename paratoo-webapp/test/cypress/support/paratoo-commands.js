import { registerCommands } from '@quasar/quasar-app-extension-testing-e2e-cypress'


/**
 * Generates a 2D array from a given table element
 * 
 * @return 2D array of data data with [0] being the headers.
 */
Cypress.Commands.add('paratooTable', { prevSubject: 'element' }, (subject) => {
    const table = [subject.find('thead > tr > th').toArray().map(th => th.textContent)]
    subject.find('tbody > tr').each(function (row_index, row_value) {
        let temp = []
        Cypress.$(this).find('td').each(function (column_index, column_value) {
            temp.push(Cypress.$(this).text())
        })
        table.push(temp)
    })
    return table
})

/**
 * Cypress selectFile wrapper with added assertions to wait for a complete upload for paratoo-webapp specific uploads fields. 
 * @param files — The file(s) to select or drag onto this element.
 * @see — https://on.cypress.io/selectfile
*/
Cypress.Commands.add('paratooSelectFile', { prevSubject: 'element' }, (subject, file, options = {}) => {
    cy.get(subject, options).selectFile(file)
    cy.get(subject, options).siblings('.q-file__filler').should('not.exist')
    cy.get(subject, options).parents('.q-field').find('[role="button"]:contains(cancel)').should('exist')
})

/**
 * Clicks the circular clear button to the right of input fields.
 */
Cypress.Commands.add('paratooClear', { prevSubject: 'element' }, (subject) => {
    Cypress.log({
        name: 'paratooClear'
    })
    subject.closest('.q-field').find('[role="button"]:contains(cancel)').trigger('click')
})

/**
 * Opens the helper dialog on the right of a input field, executes the given callback function, then closes the dialog.
 * @param {function} callback commands to execute in within the opened dialog.
 * @param {object} options generic object that is treated the same way Cypress does on its out-of-the-box commands  .
 */
Cypress.Commands.add('paratooHelperDialog', { prevSubject: 'element' }, (subject, callback, options) => {
    cy.get(subject, { log: !!options?.log }).paratooDialog('[role="presentation"]:contains(help)', callback, options)
    cy.get('.q-dialog__backdrop').click(-50, -50, { force: true })
})

/**
 * Generic dialog opener for input fields. Accepts a query to search for any button within the elements .q-field parent. 
 * @param {string} query query for a cy.get() when looking for the button in the input field. Scoped under the parent .q-field.
 * @param {function} callback commands to execute in within the opened dialog.
 * @param {object} options generic object that is treated the same way Cypress does on its out-of-the-box commands.
 */
Cypress.Commands.add('paratooDialog', { prevSubject: 'element' }, (subject, query, callback, options) => {
    cy.get(subject, { log: !!options?.log }).parents('.q-field').find(query).click({ force: true })
    cy.get('.q-card').eq(-1).within(q_card => callback && callback(q_card))
})

Cypress.Commands.add('csvFormat', {prevSubject: true}, (table) => {
  let csvContent = ``
  for (let row of table) {
    const processedRow = row.map(cell => {
      // If cell contains comma or quote, wrap in quotes and escape quotes
      if (cell.includes(',') || cell.includes('"')) {
        return `"${cell.replace(/"/g, '""')}"`
      }
      return cell
    })
    csvContent = csvContent + processedRow.join(',') + '\n'
  }
  return csvContent
})

registerCommands()
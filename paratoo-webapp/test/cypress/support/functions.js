export function surveyMetaEqual(metaA, metaB) {
  console.debug('surveyMetaEqual().\nmetaA:', JSON.stringify(metaA, null, 2), '\nmetaB:', JSON.stringify(metaB, null, 2))
  const surveyMetaDetailsKeys = [
    'survey_model',
    'time',
    'uuid',
    'project_id',
    'protocol_id',
    'protocol_version',
  ]

  let equal = true
  for (const [index, surveyMetaDetailsKey] of surveyMetaDetailsKeys.entries()) {
    if (metaA?.survey_details?.[surveyMetaDetailsKey] != metaB?.survey_details?.[surveyMetaDetailsKey]) {
      equal = false
      break
    }
  }
  console.debug('equal?', equal)

  return equal
}

// here are the maps of conditional fields
// of measurements, sex , age class and other specs, ect
// the object names are the step's model name in camel key

const vertebrateTrapCheckMeasurement = {
  AM: ['body_length', 'tibia_length'],
  MA: [
    'head_length',
    'body_length',
    'tail_length',
    'hind_foot_length',
    'forearm_length',
    'tibia_length',
    'ear_width',
    'ear_length',
  ],
  RE: ['body_length', 'tail_length'],
}

const vertebrateTrapCheckReproductive = {
  MA: [
    'female_mammal_teat_condition',
    'female_breeding_status',
    'number_of_pouch_young',
    'female_mammal_young_present',
    'pouch_young_length',
    'young_comments',
    'female_reproductive_comments',
    'male_mammal_testes_position',
    'male_mammal_testes_size_category',
    'male_reproductive_comments',
  ],
  RE: ['female_breeding_status'],
  AM: ['female_breeding_status', 'male_frog_breeding_status'],
}

const vertebrateBodySkin = {
  MA: ['body_condition_score', 'body_condition_comments', 'skin_condition'],
  RE: ['body_condition_comments', 'skin_condition'],
  AM: ['body_condition_comments', 'skin_condition'],
}

const vertebrateClinicalScoring = {
  MA: [
    'chemosis_of_eyes',
    'proliferation_of_eyes',
    'rump',
    'clinical_swab_taken',
    'swab_code',
    'swab_comments',
  ],
  RE: ['clinical_swab_taken', 'swab_code', 'swab_comments'],
  AM: ['clinical_swab_taken', 'swab_code', 'swab_comments'],
}

export default {
  vertebrateTrapCheckMeasurement,
  vertebrateTrapCheckReproductive,
  vertebrateBodySkin,
  vertebrateClinicalScoring,
}

/*
    __
 >(o )___
  (  ._> /
   \`---'
*/

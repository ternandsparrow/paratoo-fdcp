
class CustomReturn {
    constructor(bool, erroMsg, checkType, names) {
        this.bool = bool
        this.erroMsg = erroMsg
        this.checkType = checkType
        this.names = names
    }
    valueOf() {
        return this.bool
    }
    toString() {
        return this.erroMsg
    }
}


export class ObjectComparitor {
    static isNumericalAscending(arr) {
        return Array.from(Object.keys(arr)).every((val, index) => index == arr[index])
    }

    static flatten(object, options = {}) {
        // TODO would like to have it remove and flatten keys inside here to reduce the amount of times I need to loop
        // but I couldn't get it to work so i'll have to come back to it later.
        // if (options?.['delete']) this.removeKey(object, options.delete)
        // if (options?.['keys']) this.flattenKeys(object, options.keys)
        for (const key in object) {
            if (Array.isArray(object[key]) && object[key].length === 1) {
                // if (typeof object[key][0] === 'object')
                object[key] = object[key][0]
            }
            else if (typeof object[key] === 'object' && object[key] !== null && !Array.isArray(object[key]) && Object.keys(object[key]).length > 1 && ObjectComparitor.isNumericalAscending(Object.keys(object[key]))) {
                object[key] = Object.values(object[key])
            }
            if (typeof object[key] === 'object' && object[key] !== null) {
                ObjectComparitor.flatten(object[key], options)
            }
        }
    }

    static flattenKey(object, key) {
        // console.log('flattenKeys called', object)
        for (let subKey in object[key]) {
            object[subKey] = object[key][subKey]
        }
        delete object[key]
    }

    static flattenKeys(object, keysToFlatten) {
        for (let key in object) {
            if (typeof object[key] === 'object' && object[key] !== null) {
                ObjectComparitor.flattenKeys(object[key], keysToFlatten)
            }
            if (keysToFlatten.some(e => key === e)) {
                ObjectComparitor.flattenKey(object, keysToFlatten.find(e => key === e))
            }
        }
    }

    static allKeys(obj, keys = []) {
        for (let key in obj) {
            keys.push(key)
            if (typeof obj[key] === 'object') {
                ObjectComparitor.allKeys(obj[key], keys)
            }
        }
        return keys
    }

    static allKeysWithPath(obj, keys = [], path = '') {
        for (let key in obj) {
            const newPath = path ? `${path}.${key}` : key
            keys.push({
                key,
                path: newPath
            })
            if (typeof obj[key] === 'object') {
                ObjectComparitor.allKeysWithPath(obj[key], keys, newPath)
            }
        }
        return keys
    }

    static flattenNestedObjects(obj) {
        return Object.entries(obj).reduce((acc, [key, value]) => {
            if (Array.isArray(value)) {
                acc[key] = `[array of (${value.length})]`
            } else if (typeof value === 'object' && value !== null) {
                acc[key] = '[nested Object]'
            } else {
                acc[key] = value
            }
            return acc
        }, {})
    }

    static arrayToStringWithPrefix(arr) {
        return arr.map(e => "\t>\t" + e).join('\n')
    }

    static shareShape(object1, object2) {
        if (!object1 || !object2) throw new Error(`Can\'t compare object when one or both are missing\n   >${object1}\n>${object2}`)
        let obj1Keys = ObjectComparitor.allKeysWithPath(object1).map(k => k.path)
        let obj2Keys = ObjectComparitor.allKeysWithPath(object2).map(k => k.path)
        if (!obj1Keys || !obj2Keys) throw new Error(`Can\'t compare object when one or both are missing\n   >${object1}\n>${object2}`)
        let errorMsg = null
        const isMatch = obj1Keys.every((key) => obj2Keys.includes(key))
        if (!isMatch) {
            const difference = Cypress._.difference(obj1Keys, obj2Keys)
            errorMsg = `Objects do not share the same keys:\n ${ObjectComparitor.arrayToStringWithPrefix(difference)}`
            // throw new Error(`Objects do not share shape\n${difference.join(',')} not in 2nd argument`)
        }
        return new CustomReturn(isMatch, errorMsg, 'all keys are shared', Object.keys(object1))
    }

    // herbivory-off-plot-transect.0.quadrat.1.quadrat_observation.0.herbivory_attributable_fauna_species
    static shareAnyValues(object1, object2) {
        if (!object1 || !object2) throw new Error(`Can\'t compare object when one or both are missing\n   >${object1}\n>${object2}`)
        const obj1Keys = ObjectComparitor.allKeysWithPath(object1)
        const obj2Keys = ObjectComparitor.allKeysWithPath(object2)
        let errorMsg = null
        let unsharedKeys = []
        const isNullOrUndefined = value => value == null
        for (const entry of obj1Keys) {
            let obj1PathValue = Cypress._.get(object1, entry.path)
            let obj2PathValue = Cypress._.get(object2, entry.path)

            // since shareShape checks keys, the other tests should only worry about existing keys.
            if (!Cypress._.has(obj2PathValue, entry.path)) {
                continue
            }

            // both objects must both have values. If one is null or undefined, the other needs to be as well. 
            if (isNullOrUndefined(obj1PathValue) !== isNullOrUndefined(obj2PathValue)) {
                if (obj1PathValue)
                    unsharedKeys.push(entry.path)
            }
        }
        if (Object.keys(unsharedKeys).length > 0) {
            errorMsg = `One or both objects values are null or undefined on the same key: ${ObjectComparitor.arrayToStringWithPrefix(unsharedKeys)}`
        }
        return new CustomReturn(unsharedKeys.length <= 0, errorMsg, 'all shared keys have a value, regardless of what that value is', Object.keys(object1))
    }

    static shareValuesWith(object1, object2, documentation) {
        if (!object1 || !object2) throw new Error(`Can\'t compare object when one or both are missing\n   >${object1}\n>${object2}`)
        const parentKeys = Object.keys(object1)
        let errorMsg = null
        const obj1Keys = ObjectComparitor.allKeysWithPath(object1)
        if (!obj1Keys || !object2) throw new Error(`Can\'t compare object when one or both are missing\n   >${object1}\n>${object2}`)
        const keysWithSchema = parentKeys.map(key => {
            const data = Cypress._.cloneDeep(documentation.components.schemas[`${Cypress._.upperFirst(Cypress._.camelCase(key))}Request`]?.properties?.data?.properties)
            return { key, doc: data }
        })

        let unequalValues = {}
        mainLoop: for (const entry of obj1Keys) {
            let obj1val = Cypress._.get(object1, entry.path)
            let obj2val = Cypress._.get(object2, entry.path)

            const parent = entry.path.split('.')[0]
            const fullDoc = keysWithSchema.find(e => e.key === parent).doc || {}
            const pathWithoutNumbers = entry.path.replace(/\.\d+/g, '').split('.')
            if (pathWithoutNumbers.length > 1) pathWithoutNumbers.shift()

            const entryDoc = Cypress._.get(fullDoc, pathWithoutNumbers.join('.')) || fullDoc
            if (Object.hasOwn(entryDoc, 'x-model-ref') && entryDoc['x-model-ref'] !== 'file') {
                const data = Cypress._.cloneDeep(documentation.components.schemas[
                    `${Cypress._.upperFirst(Cypress._.camelCase(entryDoc['x-model-ref']))}Request`
                ]?.properties?.data?.properties)
                if (data) Cypress._.set(keysWithSchema.find(e => e.key === parent).doc, pathWithoutNumbers, data)
            }
            ObjectComparitor.flattenKey(entryDoc, ['items', 'properties'], { recursive: true })
            // === compare the values now, taking into account the documentation === // 
            if (typeof obj1val !== typeof obj2val) {

                // Missing keys should be found with shareShape. This only cares about keys that both exist
                if ((!obj1val && obj2val) || (obj1val && !obj2val) || ((!obj1val && !obj2val))) {
                    continue
                }
                if (Object.hasOwn(entryDoc, 'x-model-ref') && entryDoc['x-model-ref'] === 'file') {
                    // handle file logic (likely images)
                    continue
                }
                if (Object.hasOwn(entryDoc, 'x-paratoo-component')) {
                    // TODO add a validation check here?
                    continue
                }
                if ((typeof obj1val === 'string' || typeof obj1val === 'number') && typeof obj2val == 'object') {
                    if (obj1val === obj2val?.['symbol'] || obj1val === obj2val?.['id'] || obj1val === obj2val?.['field_survey_id']?.['id']) {
                        continue
                    }
                    if (obj2val?.symbol && obj1val == obj2val?.symbol) {
                        continue
                    }

                    // slightly more complicated submodule shenanigans
                    // get its actual name
                    if (Object.hasOwn(entryDoc, 'x-model-ref')) {
                        // see if its relating to a model we have in the schema
                        if (Object.hasOwn(object1, entryDoc['x-model-ref'])) {
                            // model with temp id 1 is now the schema model
                            const model = object1[entryDoc['x-model-ref']]
                            // matched collections survey should be compared to the new survey
                            const matches = ObjectComparitor.shareValuesWith(model, obj2val, documentation)
                            if (matches != false) {
                                continue mainLoop
                            }
                        }
                    }
                }
            }
            if (typeof obj1val === 'object' && typeof obj2val === 'object') {
                if (Cypress._.isEqual(ObjectComparitor.flattenNestedObjects(obj1val), ObjectComparitor.flattenNestedObjects(obj2val))) {
                    continue
                }
                continue
            }
            if (ObjectComparitor.isPrimitive(obj1val) && ObjectComparitor.isPrimitive(obj2val)) {
                if (Object.hasOwn(entryDoc, 'format') && entryDoc?.format.includes('date')) {
                    if (new Date(obj1val).getTime() === new Date(obj2val).getTime()) {
                        continue
                    }
                }
                if (obj1val === obj2val) {
                    continue
                }
            }
            unequalValues[entry.path] = [obj1val, obj2val]
            // console.log('unequalValues in if at ' + entry.path, Cypress._.cloneDeep(unequalValues))
        }


        if (Object.keys(unequalValues).length > 0) {
            errorMsg = `Values mismatch\n`
            for (const [key, value] of Object.entries(unequalValues)) {
                errorMsg += `[${key}] has value [${JSON.stringify(value[0])}] in the request sent from bulk, while the refetched response has value [${JSON.stringify(value[1])}]\n`

            }
        }
        return new CustomReturn(Object.keys(unequalValues).length <= 0, errorMsg, 'all shared keys have identical values', Object.keys(object1))
    }

    static isPrimitive(value) {
        const type = typeof value
        return type === 'string' || type === 'number' || type === 'boolean' ||
            type === 'undefined' || type === 'symbol' || type === 'bigint'
    }

    static removeKey(obj, keysToRemove) {
        if (!Array.isArray(keysToRemove)) keysToRemove = [keysToRemove]
        for (const key of keysToRemove) {
            if (key in obj) delete obj[key]
        }
    }

    static removePaths(obj, pathsToRemove) {
        // Some keys might be duplicates when looked for individually. E.g., name.
        // So looking for specific paths is a better option for certain keys.
        for (const path of pathsToRemove) {
            let isUnset = Cypress._.unset(obj, path)
            // if (isUnset) {
            //     Cypress.log({
            //         name: 'removePaths',
            //         message: `${path}, ${isUnset}`
            //     })
            // }
        }
    }

    static removeKeys(obj, keyToRemove, depth = 0) {
        // TODO combine removePaths and this together
        if (depth > 4 || obj === null) return
        if (Array.isArray(obj)) {
            for (const element of obj) {
                ObjectComparitor.removeKeys(element, keyToRemove, depth + 1)
            }
        } else if (typeof obj === 'object') {
            ObjectComparitor.removeKey(obj, keyToRemove)
            for (let prop in obj) {
                ObjectComparitor.removeKeys(obj[prop], keyToRemove, depth + 1)
            }
        }
    }

    static validateError(shape) {
        if (shape == false) throw new Error(`Publication Check Failures\n\n${shape}\n`)
        else if (shape != undefined && shape != false) {
            Cypress.log({
                name: `Submit check: ${shape.checkType}`,
                message: `${shape.names} Uploaded successfully ✅`,
            })
        }
    }
}


export function getRandomInt(min, max) {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min + 1)) + min
}

export function capitalizeWords(str) {
    // return str.replace(/\b\w/g, (match) => match.toUpperCase());
    return str
        .split(' ')
        .map((word, index) =>
            ['and'].includes(word) && index != 0
                ? [word]
                : word.charAt(0).toUpperCase() + word.slice(1),
        )
        .join(' ')
}

export function isHashtagTransect(transect) {
    const hashTagTransects = [
        'South 2',
        'North 2',
        'South 4',
        'North 4',
        'West 2',
        'East 2',
        'West 4',
        'East 4',
    ]
    return hashTagTransects.some((o) => o === transect)
}

export function interceptFormIsEmpty(isLite, isFire) {
    //assume true to start, unless we find a
    let empty = true

    let newRecord = null
    cy.window()
        .then(($win) => {
            newRecord = $win.PointIntercept.newRecord
        })
        .then(() => {
            if (isLite) {
                //TODO check fire for lite fields too
                if (newRecord.fractional_cover && newRecord.height) {
                    empty = false
                }
            } else {
                if (isFire) {
                    if (
                        //it's good enough checking for growth_form/floristics_voucher and
                        //height, but not check `plant_alive_status` or `resprouting_status`
                        ((newRecord.plant_unidentifiable && newRecord.fire_growth_form) ||
                            (!newRecord.plant_unidentifiable &&
                                newRecord.floristics_voucher)) &&
                        newRecord.height
                    ) {
                        empty = false
                    }
                } else {
                    if (newRecord.floristics_voucher && newRecord.height) {
                        empty = false
                    }
                }
            }
        })
        .then(() => {
            return empty
        })
}

/**
 * @deprecated use `$win.ephemeralStore.updateUserCoords`
 * 
 * https://www.browserstack.com/guide/cypress-geolocation-testing
 * 
 * @param {*} latitude 
 * @param {*} longitude 
 * @returns 
 */
export function mockLocation(latitude, longitude) {
    return {
        onBeforeLoad(win) {
            const callback = (cb, err) => {
                if (latitude && longitude) {
                    Cypress.log({
                        name: 'mockLocation',
                        message: `mocking location - lat=${latitude}, lng=${longitude}`,
                    })
                    return cb({ coords: { latitude, longitude } })
                }

                throw err({ code: 1 })
            }
            cy.stub(win.navigator.geolocation, 'getCurrentPosition').callsFake(
                callback,
            )
            cy.stub(win.navigator.geolocation, 'watchPosition').callsFake(callback)
        },
    }
}

export function generateCypressTests(jsonData) {
    let cypressCode = ''
    let describeCount = 0
    Object.keys(jsonData).forEach(parent => {
        cypressCode += `describe('${++describeCount} - ${parent.replaceAll('_', ' ')}', () => {\n`
        let contextCount = 0

        Object.keys(jsonData[parent].data.properties).forEach(section => {
            if (section.includes('new_targeted_survey')) return
            let itCount = 1
            cypressCode += `\n\tcontext('${describeCount}.${++contextCount} - ${section.replaceAll('_', ' ')}', () => {\n`

            const sectionProperties = jsonData[parent].data.properties[section]
            if (sectionProperties.type === 'object') {
                Object.keys(sectionProperties.properties).forEach(property => {
                    cypressCode += `\t\tspecify('${describeCount}.${contextCount}.${itCount++} - ${property.replaceAll('_', ' ')}', () => {\n`
                    cypressCode += `\t\t\tcy.getAndValidate('${property}')\n`
                    cypressCode += `\t\t\t//cy.getAndValidate('${property}')\n`
                    cypressCode += `\t\t})\n`
                })
            }

            cypressCode += `\t})\n`
        })
        cypressCode += `})\n`
    })

    return cypressCode
}

/**
 * returns an array of multiple cypress 'if' blocks for a spec file. will wrap the it blocks inside a describe if given a string. Additionally, passing in a 
 * object with the name key will do the same. All it tests are returned by default, but can be excluded.
 * 
 * @param {Object|string} options - The options object OR a string for the describe block name
 * @param {string} [options.name] - The name to be used for the describe block. If not provided, the describe block will be omitted in the return
 * @param {boolean} [options.shareShape=true] - Determines whether to check if both objects share keys. Defaults to true.
 * @param {boolean} [options.shareValues=true] - Determines whether to check if both objects keys share a non undefined or empty value. Defaults to true.
 * @param {boolean} [options.shareValuesWith=true] - Determines whether to perform a deep value comparison. Defaults to true.
 * 
 * @returns {Array} array of 'if' blocks or just the 'describe' block depending on if a title was provided
 * @example
 * // Example usage
 * pub({
 *   name: 'My describe block name',
 *   shareShape: false,
 *   shareValues: true,
 *   shareValuesWith: false
 * })
 * @example
 * // Example usage
 * pub('My describe block name')
 * @example
 * // Example usage
 * pub()
 */
export function pub(options) {
    const describeName = (options?.title || (typeof options === 'string' ? options : undefined))
    const shareShape = options?.shareShape !== undefined ? options?.shareShape : true
    const shareValues = options?.shareValues !== undefined ? options?.shareValues : true
    const shareValuesWith = options?.shareValuesWith !== undefined ? options?.shareValuesWith : true
    const callbackFunction = options?.callbackFunction !== undefined ? options?.callbackFunction : null
    let matchedCollection, schema, documentation


    // const itblocks = function () {
    //     return [
    //         it('submit', function () {
    //             cy.workflowPublish(null, null, null, callbackFunction, true).then(function (result) {
    //                 [matchedCollection, schema, documentation] = result
    //             })
    //         }),
    //         shareShape && it('key equality check', function () {
    //             cy.log(Cypress._.cloneDeep(matchedCollection), Cypress._.cloneDeep(schema), Cypress._.cloneDeep(documentation))
    //             cy.callSync(ObjectComparitor.shareShape, [schema, matchedCollection], ObjectComparitor.validateError)
    //         }),
    //         shareValues && it('partial deep equality check', function () {
    //             cy.log(Cypress._.cloneDeep(matchedCollection), Cypress._.cloneDeep(schema), Cypress._.cloneDeep(documentation))
    //             cy.callSync(ObjectComparitor.shareAnyValues, [schema, matchedCollection], ObjectComparitor.validateError)
    //         }),
    //         shareValuesWith && it('deep equality check', function () {
    //             cy.log(Cypress._.cloneDeep(matchedCollection), Cypress._.cloneDeep(schema), Cypress._.cloneDeep(documentation))
    //             cy.callSync(ObjectComparitor.shareValuesWith, [schema, matchedCollection, documentation], ObjectComparitor.validateError)
    //         })
    //     ]
    // }

    // FIXME this solution is a quick workaround for the problem of not being able to dynamically generate test blocks depending on a value that has an undetermined length until being called.
    // This function runs at compile time so it is impossible to know how many things were uploaded. 
    const itblocks = function () {
        return it('submit', function () {
            cy.workflowPublish(null, null, null, callbackFunction, false, options)
        })
    }
    if (describeName)
        return [
            describe(describeName, () => {
                (itblocks())
            })
        ]
    else
        return (itblocks())

}

export function pubOffline(options) {
    const collectionIterCbs = options?.collectionIterCbs
    const collectionsToCheck = options?.collectionsToCheck
    const itblocks = function () {
        const blocks = [
            it('sync', function () {
                cy.syncQueue()
            }),
        ]

        if (collectionIterCbs?.length > 0) {
            for (const [i, c] of collectionIterCbs.entries()) {
                blocks.push(
                    it(`running checker callback number ${i+1} for model ${c.collectionModel}`, () => {
                        cy.doRequest(c.collectionModel, {
                                host: 'core',
                                args: {
                                populate: 'deep',
                                'use-default': true,
                                },
                            }).then((resp) => {
                                //try/catch so can keep triggering the various callbacks,
                                //else the test will stop on an error
                                try {
                                    c.cb(resp, collectionsToCheck)
                                } catch (err) {
                                    let msg = `Failed to run callback for '${c.collectionModel}'. Got error: ${JSON.stringify(err)}`
                                    console.error(msg)
                                    cy.log(msg)
                                }
                            })
                    })
                )
            }
        } else {
            console.log(`No checker callbacks to run`)
        }
        

        return blocks
    }
    return (itblocks())
}

/**
 * Deletes any existing protocol entries in Strapi and localStorage for the given name so that a test can start with a clean run if necessary.
 * 
 * @param {String} protocolName protocol name formatted for a REST API fetch
 */
export function nukeDatabaseEntriesFor(protocolName) {
    cy.doRequest(protocolName, {args: {'use-default': true}})
        .its('body.data')
        .then(protocolsArray => {
            cy.log(`DELETING ${protocolName.replaceAll('-', ' ')} with ids: ${protocolsArray.map(p => p.id)}`)
            Array.isArray(protocolsArray) && protocolsArray.forEach((el) => {
                cy.doRequest(protocolName, {
                    method: 'DELETE',
                    id: el.id,
                    args: {
                      'use-default': true
                    }
                })
            })
        })
    cy.window().its('apiModelsStore').invoke('setModel', protocolName.endsWith('s') ? protocolName.substring(0, protocolName.length - 1) : protocolName, [])
}

export function waitForHydration() {
  //ensure app has hydrated and exposed necessary data to the window object
  cy.visit('/', { timeout: 120000 })
  cy.waitUntil(
    () => cy.getAllLocalStorage().then(
      storage => {
        const s = storage[Object.keys(storage)[0]]
        //TODO dynamically check this based on which stores have been defined to use Dexie in src/stores/persist (so that when we add new stores we don't have to modify this code)
        return s?.apiModelsHasHydrated == 'true' && s?.authHasHydrated == 'true'
      }
    ),
    {
      timeout: 120000,
      interval: 1000,
    },
  )
  cy.wait(5000)
  cy.dataCy('loadingApp', { timeout: 120000 })
    .should('not.exist')
}

export function waitForWindowData() {
  //when we are starting a test with a non-clean app, sometimes the reference (stores,
  //constants, etc) we expose on the window are not there, so trigger a reload to force
  //the MainLayout to re-mount  
  //TODO build this list dynamically (it comes from MainLayout's `mounted()`) (so that when we add new stores we don't have to modify this code)
  const exposedWindowData = [
    'documentationStore',
    'ephemeralStore',
    'apiModelsStore',
    'dataManager',
    'bulkStore',
    'authStore',
    'constants',
    'historicalDataManagerDb',
  ]

  const windowContainsExposedDataCb = ($win) => {
    const winKeys = Object.keys($win)
    console.log('winKeys:', JSON.stringify(winKeys))
    return exposedWindowData.every(e => winKeys.includes(e))
  }

  return cy.window()
    .then(($win) => {
      const alreadyExposed = windowContainsExposedDataCb($win)
      console.log(`alreadyExposed? ${alreadyExposed}`)
      //don't want to unnecessarily trigger reloads if we don't need to
      if (!alreadyExposed) {
        cy.reload(true, { timeout: 120000 })
          .then(() => {
            cy.waitUntil(
              () => cy.window().then(
                windowContainsExposedDataCb,
              ),
              {
                timeout: 120000,
                interval: 1000,
              },
            )
          })
      }
    })
}
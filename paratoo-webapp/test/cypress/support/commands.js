// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

// **************************************************
// -- Custom types --
// test/cypress/types/customCommands.d.ts will need to be updated as you create commands and/or edit jsdocs
// the `cypress helper` plugin can help automate it via r-click -> 'cypress: generate custom command types'
// will need to go into the cypress-helper function settings to change the location of:
//  - custom commands file to: 'paratoo-webapp/test/cypress/support'
//  - type definition file to: 'paratoo-webapp/test/cypress/types/customCommands.d.ts'
// Also tick 'include annotations' if you want jsdocs to be included
// *************************************************
// -- Updating commands --
// It may help to split this file up into smaller ones if time allows. Just create a new file inside this directory and import it in the index.js here.
// Add types the same way for the new file.
//
// will need to update the custom types manually as you create commands and/or edit jsdocs.
// When updating commands to become children, it may be best to add a '@deprecated' to the command jsdoc and create a new command in order to not break old tests
// OR, you could just follow the example of selectFromDropdown in how it makes itself both a child and parent by rearranging its aguments. Probably shouldnt, but you can.
// *************************************************
// -- Most used custom commands --
// login()
// nextStep()
// selectFromDropdown()
// selectFromSpeciesList()
// recordLocation()
// selectProtocolIsDisabled()
// selectProtocolIsEnabled()
// workflowPublish()
// plotLayout()
// completeWeatherForm()
// testToFailValues()
// selectTime()
// selectDateTime()
// testEmptyField()
// addMultiMedia()
// addImages()
// addObservers()
// setCheck()

// -- Generic utility commands --
// doRequest()
// getAuthToken()
// setExpansionItemState()
// prepareForOfflineVisit()
// dismissPopups()
// loadLocalStore()
// currentProtocolWorkflow()
// documentation()

// Use Mainlayout.vue to expose any needed stores unavailable via getAllLocalStorage() to the cypress window() object.
// Currently exposed:
//   - useEphemeralStore()
//   - useDocumentationStore()
// Example:
//   cy.window().then(($win) => {$win.storeName})

// TODO revise some commands to be child commands that can be chained of of a cy.get(), rather than passing in a name and putting the get logic inside.
//      This will allow a bit more flexibility with what is chosen. Right now most commands only work with a data-cy.
// DO NOT REMOVE
// Imports Quasar Cypress AE predefined commands
import { registerCommands } from '@quasar/quasar-app-extension-testing-e2e-cypress'
import 'cypress-wait-until'
import {
  isEmpty,
  range,
  cloneDeep,
  isEqual,
  upperFirst,
  camelCase,
  forOwn,
  isObject,
  has,
  startCase,
  lowerCase,
  toString,
  isNil
} from 'lodash'
import { ObjectComparitor, capitalizeWords, mockLocation } from './command-utils'
import { parse } from 'zipson'


Cypress.Commands.overwrite('visit', (originalFn, url, options) => {
  if (options) {
    console.warn(
      '`cy.visit()` was called with options, but the command is overwritten so options are not passed',
    )
  }
  return originalFn(url, mockLocation(-23.7, 132.8))
})

/**
 * Interacts with a popup map component to record coordinates. Will enter manual
 * coordinates if the gps is denied or unavailable.
 * However, this requires mocking coordinates in the ephemeralStore to get it to accept
 * manual coords.
 *
 * @param {Number} [btnEqIndex] the index of the button to interact with. If not
 * provided, the first button will be interacted with.
 * @param {Number} [recordBtnIndex] the index of the 'record' button in the modal/pop-up
 * @param {Object} [coords] the override coordinates to use for manual entry. Must have
 * keys 'lat' and 'lng', both numbers
 * @param {Boolean} [log] whether to log - for passing to child commands
 *
 * @example - Record location from the first button
 * cy.recordLocation()
 *
 * @example - Record location from the last button
 * cy.recordLocation({
 *  btnEqIndex: -1,
 * })
 */
Cypress.Commands.add('recordLocation', (
  {
    btnEqIndex = null,
    recordBtnIndex = null,
    coords = {
      lat: -34.95,
      lng: 138.62,
    },
    log = false,
  } = {}
) => {
  cy.dismissPopups()

  // cy.log('btnEqIndex', typeof btnEqIndex)
  if (typeof btnEqIndex === 'number') {
    cy.dataCy('choosePosition', { timeout: 5000, log: log })
      .eq(btnEqIndex, { log: log })
      .click({ log: log })
      .wait(1000)
  } else {
    cy.dataCy('choosePosition', { timeout: 5000, log: log }).click({ log: log }).wait(100)
  }
  cy.document({ log: log }).within(() => {
    const cb = ($button) => {
      let waitingForGps = null
      cy.dataCy('latLngPickerDialogSection', { timeout: 10000, log: log }).then(
        ($picker) => {
          const foundWaitForGps = $picker.find('[data-cy="waitForGPS"]')
          if (foundWaitForGps && foundWaitForGps.length) {
            waitingForGps = true
          } else {
            waitingForGps = false
          }

          //this goes in the `then()` block to execute it synchronously
          if ($button.is(':disabled') || waitingForGps) {
            cy.dataCy('manualCoords', { timeout: 5000 }).click().wait(500)
            if (coords?.lat && coords?.lng) {
              cy.dataCy('Latitude', { timeout: 5000 }).type(`{backspace}${coords.lat}`)
              cy.dataCy('Longitude', { timeout: 5000 }).type(`{backspace}${coords.lng}`)
            } else {
              //approx center of Australia
              cy.dataCy('Latitude', { timeout: 5000 }).type('{backspace}-23.7')
              cy.dataCy('Longitude', { timeout: 5000 }).type('{backspace}132.7999')
            }
          }
        },
      )
    }
    if (typeof recordBtnIndex === 'number') {
      cy.dataCy('recordLocation', { timeout: 5000, log: log }).eq(recordBtnIndex).then(cb)
      cy.wait(1000)
      cy.dataCy('recordLocation', { log: log }).eq(recordBtnIndex).click({ log: log })
    } else {
      cy.dataCy('recordLocation', { timeout: 5000, log: log }).then(cb)
      cy.wait(1000)
      cy.dataCy('recordLocation', { log: log }).click({ log: log })
    }
  })
})


Cypress.Commands.add(
  'selectProtocolIsDisabled',
  (module, protocol, project = null) => {
    cy.wait(200)
    project =
      Cypress.env('AUTH_TYPE') == 'oidc' ? Cypress.env('OIDC_PROJECT') : project

    cy.loadManyStores(
      [
        {
          storeName: 'auth',
        },
        {
          storeName: 'bulk',
        },
      ],
    )
      .then((stores) => {
        const authStore = stores.authStore
        const bulkStore = stores.bulkStore

        if (!authStore?.currentContext?.project) {
          cy.dataCy(project || 'Kitchen\\ Sink\\ TEST\\ Project').click()
        }
        if (
          Cypress.env('AUTH_TYPE') == 'oidc' &&
          !authStore
        ) {
          cy.dataCy(project).click()
        }

        if (
          bulkStore?.staticPlotContext?.plotLayout &&
          bulkStore?.staticPlotContext?.plotVisit
        ) {
          Cypress.log({
            name: 'selectProtocolIsDisabled',
            message:
              'User already completed Plot Layout for a different test, so the plot-based protocol should be enabled, nothing to do',
          })
          cy.visit('/projects')
        } else {
          //can't check for `disabled` prop as we don't pass it - need to manually
          //disable' as to allow the `@click` event to fire that allows us to show
          //the popup, which we check instead
          cy.get(`[data-cy="${capitalizeWords(module)}"]`).click()
          const moduleIsDisabled = cy
            .get('.q-notification')
            .should('contain.text', 'You must specify the plot first')
          cy.log({
            name: 'selectProtocolIsDisabled',
            message: `Module '${module}' is ${moduleIsDisabled ? 'disabled' : 'enabled'
              }`,
          })
          if (!moduleIsDisabled) {
            //if capitalizeWords(module) is enabled
            cy.get(`[data-cy="${capitalizeWords(module)}"]`).click()
            if (protocol) {
              if (!cy.get(`[data-cy="${protocol}"]`).should('be.disabled')) {
                //if protocol number is > 1 and button is enabled
                cy.get(`[data-cy="${protocol}"]`).click()
              } else {
                cy.dataCy('closeDialog').click()
              }
            }
          }
        }
      })
  },
)

//TODO: `selectProtocolIsEnabled` doesn't support editing so have to do this manually - we should roll-up this functionality into `selectProtocolIsEnabled` rather than having it separate (and remove all usages of `editProtocol` command)
Cypress.Commands.add(
  'editProtocol',
  ({ module, buttonIndex }) => {
    cy.dataCy('pulling-api-status', { timeout: 120000 }).should('not.exist', { timeout: 120000 })
    cy.get(`[data-cy="${capitalizeWords(module)}"]`)
      .should('be.enabled')
      .click({ timeout: 20000 })
    //TODO make this dynamic based on a `protocol` param
    cy.dataCy('edit').eq(buttonIndex).click()
  }
)

//TODO allow editing in-progress (when `delPrev` is false)
Cypress.Commands.add(
  'selectProtocolIsEnabled',
  (module, protocol, project = null, { delPrev = true, count = 0 } = {}) => {
    // cy.wait(200)
    // make sure that we have waited for the refresh after uploading
    cy.dataCy('pulling-api-status', { timeout: 120000 }).should('not.exist', { timeout: 120000 })

    project =
      Cypress.env('AUTH_TYPE') == 'oidc' ? Cypress.env('OIDC_PROJECT') : project
    //kitchen sink is fine most of the time, but may also want to use different
    cy.document()
      .find('body')
      .within(($body) => {
        if (!$body.find('[data-cy="changeSelection"]').length) {
          cy.dataCy(project || 'Kitchen\\ Sink\\ TEST\\ Project').click()
        }
      })

    if (module) {
      //only gets here if more than 1 protocol in module, which is fairly typical
      cy.get(`[data-cy="${capitalizeWords(module)}"]`)
        .should('be.enabled')
        .click({ timeout: 20000 })
      // cy.get(`[data-cy="${capitalizeWords(module)}"]`).click()
    }

    cy.log(`delPrev:`, delPrev)
    if (delPrev) {
      cy.get('body').then(($body) => {
        if (
          protocol &&
          $body.find(`[data-cy="clearCollection_${protocol.toLowerCase().replaceAll(' ', '_')}"]`).length > 0
        ) {
          cy.get(`[data-cy="clearCollection_${protocol.toLowerCase().replaceAll(' ', '_')}"]`).click()
          cy.dataCy('confirmClearCollection', { timeout: 20000 }).click({ timeout: 20000 })
        } else if (!protocol) {
          const singleDelBtn = $body.find('.bg-red > .q-btn__content > .q-icon')
          if (singleDelBtn.length > 0) {
            singleDelBtn[0].click({ timeout: 20000 })
            cy.dataCy('confirmClearCollection', { timeout: 20000 }).click({ timeout: 20000 })

            // will need to select again if only a single protocol, since no menu should pop up.
            cy.get(`[data-cy="${capitalizeWords(module)}"]`).click({ timeout: 20000 })
          }
        }
      })
    }

    if (protocol) {
      // cy.get(`[data-cy="${protocol}"]`).should('be.enabled')
      cy.get(`[data-cy="${protocol}"]`).click({ timeout: 20000 })
    }
    // cy.dataCy('workflowLoading', { timeout: 15000 })
    //   .should('exist')
    //   .then(() => {
    //   }).then(() => {
    //     cy.dataCy('workflowLoading', { timeout: 180000 })
    //       .should('not.exist')
    //       .then(() => {
    //       })
    //   })
    //   .then(() => {
    //after we trigger entering `Workflow.vue`, can take a moment to grab docco
    cy.dataCy('workflowLoaded', { timeout: 120000 })
      .should('exist')
      .then(() => {
        //allow time for model config to be defined
        cy.dataCy('title', { timeout: 60000 }).should('exist')
        cy.wait(2000)
      })
    // })
    // TODO test edit button as well
    if (count === 0) {
      cy.dataCy('workflowBack').click()
      cy.selectProtocolIsEnabled(module, protocol, project, { delPrev: delPrev, count: ++count })
    } else {
      cy.log(`getValidationRules`)
      cy.documentation().currentProtocolWorkflow((models) => {
        global.currentProtRules = models
      })
    }
  }
)

//check if start collection is enabled
Cypress.Commands.add(
  'startCollectionEnabled',
  (project, module, protocolCy) => {
    cy.dataCy('closeDialog').click()
    cy.dataCy('changeSelection').click()
    cy.plotDescription()
    cy.dataCy(project).click()
    cy.dataCy(capitalizeWords(module)).click()
    cy.dataCy(protocolCy).should('be.enabled').click()
  },
)

Cypress.Commands.add('workflowPublishQueue', ({ goWorkflowNext = true,afterQueueCollectionCb = null } = {}) => {
  if (goWorkflowNext) {
    cy.dataCy('workflowNextButton').click()
  }
  cy.dataCy('workflowPublish').click()
  cy.dataCy('workflowPublishConfirm').click()
  if (afterQueueCollectionCb) afterQueueCollectionCb()
  cy.get('.q-notification', { timeout: 360000 })
    .should('exist')
    .and('not.contain.text', 'Failed to upload collection')
    .and('contain.text', 'Successfully queued the collection')
    .then(() => {
      cy.get('.q-notification').contains('Dismiss').click().wait(100)
    })
})

Cypress.Commands.add(
  'workflowPublishOffline',
  (shouldHaveData, afterSubmitCb = null, workflowNextButtonIndex = null, dumpStateFn = null) => {
    if (workflowNextButtonIndex !== null) {
      cy.dataCy('workflowNextButton').eq(workflowNextButtonIndex).click()
    } else {
      cy.dataCy('workflowNextButton').click()
    }
    cy.wait(2000)

    if (!shouldHaveData?.willNotQueue) {
      cy.loadManyStores(
        [
          {
            storeName: 'dataManager',
          },
          {
            storeName: 'apiModels',
          },
          {
            storeName: 'auth',
          },
          {
            storeName: 'bulk',
          },
        ],
      )
        .then((stores) => {
          const dataManagerStore = stores.dataManagerStore
          const publicationQueueBefore = dataManagerStore.publicationQueue
          const apiModelsStore = stores.apiModelsStore
          const prot = cloneDeep(
            apiModelsStore.models.protocol.find(
              (o) => o.name === shouldHaveData.protocolName,
            ),
          )
          const authStore = stores.authStore
          const proj = authStore.projAndProt.find(
            (o) => o.name === shouldHaveData.projectName,
          )
          const bulkStore = stores.bulkStore
          let protocolToUse = {
            id: prot.id,
            version: prot.version,
          }
          //workaround - auth context stores the protocol of the parent module, but it
          //should consider the case when there is a submodule and use that instead
          //(survey_metadata and org identifiers are created correctly, as at
          //creation-time we resolve the submodule, which we also need to do here)
          //FIXME doesn't work for cover+fire, which is handled differently to the typical submodule pattern that PTV+Floristics uses
          if (bulkStore.doingSubmoduleMeta.length > 0) {
            // protocolToUse.id = bulkStore.getSubModuleID(prot.id)
            const foundSubmoduleProtocol = bulkStore.doingSubmoduleMeta.find(
              (element) => {
                return element.submoduleProtocolId == protocolToUse.id
              },
            )
            if (foundSubmoduleProtocol?.parentModuleProtocolId) {
              //only override expected protocol if we are actually doing the submodule
              protocolToUse.id = foundSubmoduleProtocol.parentModuleProtocolId
            }
          }
          const expectedAuthContext = {
            project: proj.id,
            protocol: protocolToUse,
          }
          if (authStore?.currentContext?.hasSpecies) delete authStore.currentContext.hasSpecies
          if (authStore?.currentContext?.hasVoucher) delete authStore.currentContext.hasVoucher

          cy.log(
            `Checking auth context in auth store set correctly. Comparing actual / expected: ${JSON.stringify(
              authStore.currentContext,
            )} / ${JSON.stringify(expectedAuthContext)}`,
          )
          cy.wrap(isEqual(authStore.currentContext, expectedAuthContext)).should(
            'be.true',
          )

          if (dumpStateFn !== null) {
            cy.log(`Dumping stage of collection that is about to be queued to fn=${dumpStateFn}`)
            cy.dumpState({
              fileName: dumpStateFn,
            })
          }

          cy.dataCy('workflowPublish')
            .should('have.text', 'Queue Collection for Submission')
            .click()
          cy.wait(2000)
          cy.dataCy('workflowPublishConfirm').click()

          //on success we get a popup, so wait for that
          cy.get('.q-notification', { timeout: 360000 })
            .should('exist')
            .and('not.contain.text', 'Failed to queue the collection')
            .and('contain.text', 'Successfully queued the collection')
            .then(() => {
              cy.loadLocalStore('dataManager')
                .then((dataManagerStore) => {
                  const publicationQueueAfter = dataManagerStore.publicationQueue

                  const latestQueued = publicationQueueAfter[publicationQueueAfter.length - 1]
                  let expectedQueuedContext = {
                    project: proj.id,
                    protocol: {
                      id: prot.id,
                      version: prot.version,
                    },
                  }
                  let parentModuleExpectedQueuedContext = null
                  let latestQueuedIsSubmodule = false
                  //similar to above - as data manager adds submodule protocol to queue,
                  //it overrides the auth context protocol info to be that of the
                  //submodule, else it will just inherit it's parent which is technically
                  //a different collection (i.e., protocol)
                  if (bulkStore.doingSubmoduleMeta.length > 0) {
                    const foundSubmoduleProtocol = bulkStore.doingSubmoduleMeta.find(
                      (element) => {
                        return (
                          element.parentModuleProtocolId ==
                          expectedQueuedContext.protocol.id
                        )
                      },
                    )
                    cy.log(
                      `Detected submodule info: ${JSON.stringify(
                        foundSubmoduleProtocol,
                      )}`,
                    )
                    if (foundSubmoduleProtocol?.submoduleProtocolId) {
                      parentModuleExpectedQueuedContext = cloneDeep(
                        expectedQueuedContext,
                      )
                      latestQueuedIsSubmodule = true
                      expectedQueuedContext.protocol.id =
                        foundSubmoduleProtocol.submoduleProtocolId
                    }
                  }

                  if (latestQueued?.authContext?.hasSpecies) delete latestQueued.authContext.hasSpecies
                  if (latestQueued?.authContext?.hasVoucher) delete latestQueued.authContext.hasVoucher

                  cy.log(
                    `Checking${latestQueuedIsSubmodule ? ' submodule' : ''
                    } auth context publication queue set correctly. Comparing actual / expected: ${JSON.stringify(
                      latestQueued.authContext,
                    )} / ${JSON.stringify(expectedQueuedContext)}`,
                  )
                  cy.wrap(
                    isEqual(latestQueued.authContext, expectedQueuedContext),
                  ).should('be.true')

                  if (latestQueuedIsSubmodule) {
                    const queuedParentModule =
                      publicationQueueAfter[publicationQueueAfter.length - 2]
                    if (queuedParentModule?.authContext?.hasSpecies) delete queuedParentModule.authContext.hasSpecies
                    if (queuedParentModule?.authContext?.hasVoucher) delete queuedParentModule.authContext.hasVoucher
                    cy.log(
                      `Checking parent module auth context publication queue set correctly. Comparing actual / expected: ${JSON.stringify(
                        queuedParentModule.authContext,
                      )} / ${JSON.stringify(parentModuleExpectedQueuedContext)}`,
                    )
                    cy.wrap(
                      isEqual(
                        queuedParentModule.authContext,
                        parentModuleExpectedQueuedContext,
                      ),
                    ).should('be.true')
                  }

                  if (afterSubmitCb) {
                    //the command we're in is generic, but each time it's called we will
                    //want a different callback for checking the data
                    afterSubmitCb({
                      shouldHaveData,
                      publicationQueueBefore,
                      publicationQueueAfter,
                      dataManagerStore,
                      apiModelsStore,
                      authStore,
                      bulkStore,
                    })
                  }
                })
            })
        })

    } else {
      if (dumpStateFn !== null) {
        cy.log(`Dumping stage of collection that is about to be queued to fn=${dumpStateFn}`)
        cy.dumpState({
          fileName: dumpStateFn,
        })
      }

      //TODO check context was set correctly
      cy.dataCy('workflowPublish')
        .should('have.text', 'Set Plot Context')
        .click()
      cy.wait(2000)
      cy.dataCy('workflowPublishConfirm').click()
      cy.get('.q-notification', { timeout: 360000 })
        .should('exist')
        .and('not.contain.text', 'Failed to queue the collection')
        .and('contain.text', 'Successfully set plot context of the collection')
    }
  },
)

Cypress.Commands.add(
  'syncQueue',
  () => {
    cy.dataCy('submitQueuedCollectionsBtn')
      .should('be.enabled')
      .click()
      .wait(100)
    cy.dataCy('submitQueuedCollectionsConfirm').click()
    cy.get('.q-notification', { timeout: 360000 })
      .should('exist')
      .and('contain.text', 'Successfully submitted all queued collections')
  }
)

Cypress.Commands.add('syncAndCheckQueueTable', ({ shouldHaveSyncErrors }) => {
  cy.dataCy('submitQueuedCollectionsBtn')
    .should('be.enabled')
    .click()
    .wait(100)
  cy.dataCy('submitQueuedCollectionsConfirm')
    .click()
    .wait(5000)
  cy.dataCy('submissionProgressWrapper', { timeout: 60000 })
    .should('not.exist')
    .then(() => {
      if (shouldHaveSyncErrors?.length > 0) {
        cy.log(`Checking that we have sync errors for: ${JSON.stringify(shouldHaveSyncErrors)}`)
        //iterating over table rows: https://stackoverflow.com/a/57097316
        cy.get(`[data-cy="failedQueuedCollectionsSummaryTable"] > .q-table__middle > .q-table > tbody`)
          .find('tr')
          .each(($el, $index) => {
            let protocolColTextToMatch = null
            let issueColTextToMatch = null
            if (typeof shouldHaveSyncErrors[$index] === 'object') {
              protocolColTextToMatch = shouldHaveSyncErrors[$index].protocol

              if (shouldHaveSyncErrors[$index]?.issueMessage) {
                issueColTextToMatch = shouldHaveSyncErrors[$index].issueMessage
              }

            } else {
              protocolColTextToMatch = shouldHaveSyncErrors[$index]
            }

            //check each column for this row
            const $cols = $el.children()
            cy.wrap($cols[0])
              .should('contain.text', protocolColTextToMatch)

            if (issueColTextToMatch) {
              cy.wrap($cols[1])
                .should('contain.text', issueColTextToMatch)
            }
          })
      }

      if (
        shouldHaveSyncErrors?.length === 0
      ) {
        cy.log(`Checking that there are no sync errors`)
        cy.dataCy('failedQueuedCollectionsSummaryTable')
          .should('not.exist')
      }
    })
})

Cypress.Commands.add('checkResolvedData', ({ unresolvedInProgressData, resolvedInProgressData }) => {
  //collect all the issues so we can throw them at the end; don't want to throw them as
  //they occur, as we want to know about all errors, not just the first one we encounter
  const issues = []
  for (const [protocolId, unresolvedSurveyData] of Object.entries(unresolvedInProgressData)) {
    for (const [model, data] of Object.entries(unresolvedSurveyData)) {
      switch (model) {
        case 'plot-layout':
        case 'plot-visit':
          if (data?.temp_offline_id) {
            cy.log(`in-progress protocol ${protocolId} model ${model} has temp ID to plot data. checking it's not there for the resolved data: ${JSON.stringify(resolvedInProgressData?.[protocolId]?.[model])}`)
            cy.wrap(resolvedInProgressData?.[protocolId]?.[model])
              .should('have.any.key', 'id')
            cy.wrap(resolvedInProgressData?.[protocolId]?.[model]?.id)
              .should('not.be.NaN')
          } else if (!data?.id) {
            issues.push(`in-progress protocol ${protocolId} for model ${model} doesn't have temp ID or regular ID: ${JSON.stringify(data)}`)
          }
          break
        case 'floristics-veg-genetic-voucher':
        case 'basal-area-dbh-measure-observation':
          //check the resolved data didn't lose anything.
          //the iteration and access data implicitly tests/checks the rest of the data
          //being there
          cy.wrap(data)
            .should('have.length', resolvedInProgressData?.[protocolId]?.[model]?.length)
          for (const [index, ob] of data.entries()) {
            let hasVoucherVariant = null
            if (
              !ob.floristics_voucher?.voucher_full &&
              !ob.floristics_voucher?.voucher_lite
            ) {
              issues.push(`in-progress protocol ${protocolId} model ${model} for ob index ${index} has no voucher: ${JSON.stringify(ob.floristics_voucher)}`)
            } else if (
              ob.floristics_voucher?.voucher_full &&
              ob.floristics_voucher?.voucher_lite
            ) {
              issues.push(`in-progress protocol ${protocolId} model ${model} for ob index ${index} has both full and lite voucher reference (it shouldn't): ${JSON.stringify(ob.floristics_voucher)}`)
            } else {
              if (ob.floristics_voucher?.voucher_full) {
                hasVoucherVariant = 'full'
              } else if (ob.floristics_voucher?.voucher_lite) {
                hasVoucherVariant = 'lite'
              }
              cy.log(`hasVoucherVariant=${hasVoucherVariant}`)

              if (ob.floristics_voucher?.[`voucher_${hasVoucherVariant}`]?.temp_offline_id) {
                cy.log(`in-progress protocol ${protocolId} model ${model} has temp ID to ${hasVoucherVariant} voucher at ob index ${index}. checking it's not there for the resolved data: ${JSON.stringify(resolvedInProgressData?.[protocolId]?.[model]?.[index])}`)

                cy.wrap(resolvedInProgressData?.[protocolId]?.[model]?.[index]?.floristics_voucher?.[`voucher_${hasVoucherVariant}`])
                  .should('not.have.any.keys', 'temp_offline_id')
                  //should have integer PK relation
                  .and('not.be.NaN')
              } else if (!Number.isInteger(ob.floristics_voucher?.[`voucher_${hasVoucherVariant}`])) {
                issues.push(`in-progress protocol ${protocolId} model ${model} variant ${hasVoucherVariant} for ob index ${index} doesn't have either temp ID or regular ID: ${JSON.stringify(ob.floristics_voucher?.[`voucher_${hasVoucherVariant}`])}`)
              }
            }
            
          }
          break
        case 'basal-wedge-observation':
          //check the obs are the same for resolved/unresolved
          cy.wrap(data)
            .should('have.length', resolvedInProgressData?.[protocolId]?.[model]?.length)
          
          //similar to cases for PTV and Basal DBH, but can select many vouchers
          for (const [index, ob] of data.entries()) {
            if (ob.floristics_voucher?.length) {
              for (const [voucherIndex, voucher] of ob.floristics_voucher.entries()) {
                //check the array of vouchers are the same for resolved/unresolved
                cy.wrap(ob.floristics_voucher)
                  .should('have.length', resolvedInProgressData?.[protocolId]?.[model]?.[index]?.floristics_voucher?.length)
                let hasVoucherVariant = null
                if (
                  !voucher?.voucher_full &&
                  !voucher?.voucher_lite
                ) {
                  issues.push(`in-progress protocol ${protocolId} model ${model} for ob index ${index} and voucher index ${voucherIndex} has no voucher: ${JSON.stringify(voucher)}`)
                } else if (
                  voucher?.voucher_full &&
                  voucher?.voucher_lite
                ) {
                  issues.push(`in-progress protocol ${protocolId} model ${model} for ob index ${index} and voucher index ${voucherIndex} has both full and lite voucher reference (it shouldn't): ${JSON.stringify(voucher)}`)
                } else {
                  if (voucher?.voucher_full) {
                    hasVoucherVariant = 'full'
                  } else if (voucher?.voucher_lite) {
                    hasVoucherVariant = 'lite'
                  }

                  cy.log(`hasVoucherVariant=${hasVoucherVariant}`)

                  if (voucher[`voucher_${hasVoucherVariant}`]?.temp_offline_id) {
                    cy.log(`in-progress protocol ${protocolId} model ${model} has temp ID to ${hasVoucherVariant} voucher at ob index ${index} and voucher index ${voucherIndex}. checking it's not there for the resolved data: ${JSON.stringify(resolvedInProgressData?.[protocolId]?.[model]?.[index]?.floristics_voucher?.[voucherIndex]?.[`voucher_${hasVoucherVariant}`])}`)

                    cy.wrap(resolvedInProgressData?.[protocolId]?.[model]?.[index]?.floristics_voucher?.[voucherIndex]?.[`voucher_${hasVoucherVariant}`])
                      .should('not.have.any.keys', 'temp_offline_id')
                      //should have integer PK relation
                      .and('not.be.NaN')
                  } else if (!Number.isInteger(voucher[`voucher_${hasVoucherVariant}`])) {
                    issues.push(`in-progress protocol ${protocolId} model ${model} variant ${hasVoucherVariant} for ob index${index} and voucher index ${voucherIndex} doesn't have temp ID or regular ID: ${JSON.stringify(voucher)}`)
                  }
                }
              }
            } else {
              issues.push(`in-progress protocol ${protocolId} model ${model} ob index ${index} has no vouchers: ${JSON.stringify(ob)}`)
            }
          }
          break
        case 'soil-bulk-density-survey':
          if (data.associated_soil_pit_id?.temp_offline_id) {
            cy.log(`in-progress protocol ${protocolId} model ${model} has associated_soil_pit_id. checking it's resolved: ${JSON.stringify(resolvedInProgressData?.[protocolId]?.[model])}`)
            cy.wrap(resolvedInProgressData?.[protocolId]?.[model]?.associated_soil_pit_id?.id)
              .should('not.be.undefined')
              .and('not.be.null')
              //relation PK
              .and('not.be.NaN')
          } else if (!Number.isInteger(data.associated_soil_pit_id?.id)) {
            issues.push(`in-progress protocol ${protocolId} model ${model} doesn't have temp ID or regular ID: ${JSON.stringify(data.associated_soil_pit_id)}`)
          }
          break
        case 'vertebrate-check-trap':
        case 'vertebrate-end-trap':
          let trapFieldName = null
          if (model === 'vertebrate-check-trap') {
            trapFieldName = 'trap_setup'
          } else if (model === 'vertebrate-end-trap') {
            trapFieldName = 'trap_number'
          }
          cy.wrap(data)
            .should('have.length', resolvedInProgressData?.[protocolId]?.[model]?.length)
          for (const [index, ob] of data.entries()) {
            if (ob?.[trapFieldName]) {
              if (ob[trapFieldName]?.temp_offline_id) {
                cy.log(`in-progress protocol ${protocolId} model ${model} for index ${index} has temp ID. checking it's not there in the resolved data ${JSON.stringify(resolvedInProgressData?.[protocolId]?.[model]?.[index])}`)
                cy.wrap(resolvedInProgressData?.[protocolId]?.[model]?.[index]?.[trapFieldName])
                  .should('not.have.any.keys', 'temp_offline_id')
                  //should have integer PK relation
                  .and('not.be.NaN')
              } else if (!Number.isInteger(ob[trapFieldName])) {
                issues.push(`in-progress protocol ${protocolId} model ${model} doesn't have temp ID or regular ID: ${JSON.stringify(ob)}`)
              }
            } else {
              issues.push(`in-progress protocol ${protocolId} model ${model} for index ${index} doesn't have ${trapFieldName}`)
            }
          }
          break
        case 'camera-trap-deployment-point': {
          cy.wrap(data)
            .should('have.length', resolvedInProgressData?.[protocolId]?.[model]?.length)
          for (const [index, ob] of data.entries()) {
            if (ob.fauna_plot?.temp_offline_id) {
              cy.log(`in-progress protocol ${protocolId} model ${model} has temp ID to fauna plot at ob index ${index}. checking it's not there for the resolved data: ${JSON.stringify(resolvedInProgressData?.[protocolId]?.[model]?.[index])}`)
              cy.wrap(resolvedInProgressData?.[protocolId]?.[model]?.[index]?.fauna_plot)
                .should('not.have.any.keys', 'temp_offline_id')
                //should have integer PK relation
                .and('not.be.NaN')
            } else if (!Number.isInteger(ob?.fauna_plot)) {
              issues.push(`in-progress protocol ${protocolId} model ${model} for ob index ${index} doesn't have either temp ID or regular ID: ${JSON.stringify(ob.fauna_plot)}`)
            }
          }
          break
        }
        default:
          if (
            [
              'floristics-veg-genetic-voucher-survey',
              'basal-area-dbh-measure-survey',
              'basal-wedge-survey',
              'vertebrate-end-trapping-survey',
              'vertebrate-closed-drift-photo',
              'vertebrate-trap-check-survey',
              'camera-trap-deployment-survey',
            ].includes(model)
          ) {
            cy.log(`skipping in-progress protocol ${protocolId} ${model} - no data that needs checking`)
          } else {
            issues.push(`Unregistered model case ${model}`)
          }
      }
    }
  }

  if (issues.length > 0) {
    throw new Error(`There were errors when checking resolved data: ${issues.join('.\n')}`)
  }
})

/**
 * @deprecated use `pubOffline`
 */
Cypress.Commands.add(
  'submitOfflineCollections',
  (collectionsToCheck, collectionIterCbs) => {
    cy.dataCy('submitQueuedCollectionsBtn')
      .should('be.enabled')
      .click()
      .wait(100)
    cy.dataCy('submitQueuedCollectionsConfirm').click()
    cy.get('.q-notification', { timeout: 360000 })
      .should('exist')
      .and('contain.text', 'Successfully submitted all queued collections')
      .then(() => {
        if (collectionsToCheck !== null && collectionIterCbs !== null) {
          for (const c of collectionIterCbs) {
            cy.doRequest(c.collectionModel, {
              host: 'core',
              args: {
                populate: 'deep',
                'use-default': true,
              },
            }).then((resp) => {
              //try/catch so can keep triggering the various callbacks, else the test
              //will stop on an error
              try {
                c.cb(resp, collectionsToCheck)
              } catch (err) {
                let msg = `Failed to run callback for '${c.collectionModel}'. Got error: ${JSON.stringify(err)}`
                console.error(msg)
                cy.log(msg)
              }
            })
          }
        }
      })
  },
)

// This command always listens to the exceptions return false and will ignore these errors from failing tests.
Cypress.Commands.add('catchError', (msg) => {
  cy.on('uncaught:exception', (err, runnable) => {
    console.log('err', err)
    if (err.message.includes(msg)) {
      console.log(msg)
      return false
    }
    return true
  })
})

/**
 * Simulates a failed login attempt.
 *
 * @param {String} username - The username to use for the login attempt.
 * @param {String} password - The password to use for the login attempt.
 *
 * @example
 * cy.loginFail('invalidUsername', 'invalidPassword');
 */
Cypress.Commands.add('loginFail', (username, password) => {
  //Invalid identifier or password. Cypress doesn’t allow you to use the try and catch block to handle the exception.
  // Cypress does not delete IndexedDB by default, only LocalStorage
  // https://github.com/cypress-io/cypress/issues/1383
  indexedDB.deleteDatabase('localforage')
  cy.visit('/login')
  cy.title().should('include', 'Monitor Field Data Collection WebApp', { timeout: 20000 })
  cy.visit('/login')
  // Make sure we reach login page
  // cy.testRoute('login')
  cy.title().should('include', 'Monitor Field Data Collection WebApp')
  // use credentials to login, should fail
  cy.dataCy('loginUsername').type(username)
  cy.dataCy('loginPassword').type(password)
  cy.dataCy('loginSubmit').click()
  cy.get('.q-loading__backdrop', { timeout: 180000 }).should('not.exist')
  cy.catchError('Invalid identifier or password')
})

// oidc Login Test Function
//FIXME don't use the `cacheResult` (is raw output of `cy.getAllLocalStorage()`) - use `cy.loadLocalStore()`
Cypress.Commands.add('oidcLogin', (username, password, cacheReuslt) => {
  if (Cypress.env('AUTH_TYPE') != 'oidc') return
  cy.visit('/login')
  const checkAuth = (cache) => {
    let localCache = null
    if (cache) {
      Object.keys(cache).forEach((key) => {
        if (key.includes('localhost:8080')) {
          localCache = cache[key]
        }
      })
    }
    if (localCache != null) {
      if (Object.keys(localCache).includes('auth')) {
        return (
          localCache.auth.includes('token') && !isEmpty(localCache.auth.token)
        )
      }
    }
    return false
  }
  const authDone = checkAuth(cacheReuslt)
  if (authDone) return

  // TODO: generate form env
  const oidcOrigin = 'https://auth-secure.auth.ap-southeast-2.amazoncognito.com'

  cy.dataCy('loginSubmit').click()
  //we might get automatically logged in (cached our session) once we land on the new
  //origin, so wait a moment to see if it will auto-redirect back to app before actually
  //trying to login with OICD
  cy.wait(10000)
  cy.getAllLocalStorage().then((result) => {
    const authDone = checkAuth(result)
    if (!authDone) {
      cy.origin(
        oidcOrigin,
        { args: { username, password } },
        ({ username, password }) => {
          let existingUser = false
          cy.get('form')
            .eq(0)
            .within(($form) => {
              if ($form.text().includes(`Sign In as ${username}`)) {
                existingUser = true
              }
            })
          if (existingUser) {
            cy.get('[class="btn btn-primary submitButton-customizable"]')
              .eq(1)
              .click()
          } else {
            cy.get('[id="signInFormUsername"]').eq(1).type(username)
            cy.get('[id="signInFormPassword"]').eq(1).type(password)
            cy.get('[name="signInSubmitButton"]').eq(1).click()
          }
        },
      )
      cy.url().should('include', 'http://localhost:8080/')
    }
  })
})

/**
 * Logs user in after performing test-to-fail, then waits for data to be pulled.
 * Compatable with MERIT auth
 *
 * @param {String} username - The username to use for the login attempt.
 * @param {String} password - The password to use for the login attempt.
 *
 * @example
 * cy.login('myUsername', 'myPassword');
 */
Cypress.Commands.add('login', (username, password) => {
  cy.getAllLocalStorage().then((result) => {
    if (Cypress.env('AUTH_TYPE') == 'oidc') {
      const usernameEnv = Cypress.env('E2E_MERIT_USERNAME')
      const passwordEnv = Cypress.env('E2E_MERIT_PASSWORD')
      if (usernameEnv && passwordEnv) {
        cy.oidcLogin(usernameEnv, passwordEnv, result).then(() => {
          cy.get('.q-loading__backdrop', { timeout: 180000 }).should(
            'not.exist',
          )
          cy.dataCy('pulling-api-status', { timeout: 1000000 })
            .should('not.exist')
            .then(() => {
              cy.testRoute('projects')
            })
        })

        return
      } else {
        let msg = `Cannot run with --sso without providing the testing username/password credentials in .env.local.`
        if (!usernameEnv && !passwordEnv) {
          msg +=
            ' Missing username and password - provide them with E2E_MERIT_USERNAME and E2E_MERIT_PASSWORD'
        } else if (!usernameEnv && passwordEnv) {
          msg += ' Missing username - provide it with E2E_MERIT_USERNAME'
        } else if (usernameEnv && !passwordEnv) {
          msg += ' Missing password - provide it with E2E_MERIT_PASSWORD'
        }
        throw new Error(msg)
      }
    }
    cy.loadLocalStore('auth')
      .then((authStore) => {
        if (authStore.token) {
          if (
            username === 'ProjectAdmin' &&
            //if we're already ProjectAdmin then don't need to logout
            authStore.userProfile.username !==
            'ProjectAdmin'
          ) {
            Cypress.log({
              name: 'loginNonProjectAdmin',
              message:
                'Already logged in as non-project-admin, but need their privileges, so logging out',
            })
            cy.visit('/projects')
            cy.logout(true)
            cy.login(username, password)
          } else {
            Cypress.log({
              name: 'loginNormalUser',
              message: 'User already logged in for a different test, nothing to do',
            })
            cy.visit('/projects')
          }
          if (
            authStore.userProfile.username !==
            username
          ) {
            cy.logout(true)
            cy.login(username, password)
          }
        } else {
          cy.loginFail('123', '456') // login fail test
          cy.dataCy('loginUsername', { timeout: 5000 }).clear().type(username)
          cy.dataCy('loginPassword').clear().type(password)
          cy.dataCy('loginSubmit').click()
          cy.get('.q-loading__backdrop', { timeout: 180000 }).should('not.exist')
          // On First Load can take up to 20+ seconds to
          cy.dataCy('login-loading', { timeout: 300000 }).should('not.exist').then(() => {
            cy.dataCy('pulling-api-status', { timeout: 1000000 })
              .should('not.exist')
              .then(() => {
                cy.testRoute('projects')
              })
          })
        }
      })
  })
})

Cypress.Commands.add('plotLayout', (forceRedo = false, project = null, plotAndVisitOverride = null) => {
  cy.wait(300)
  cy.dataCy('currentPlot')
    .invoke('text')
    .then((currentPlot) => {
      let plotToSelect = 'QDASEQ0001'
      if (plotAndVisitOverride?.layout) {
        plotToSelect = plotAndVisitOverride.layout
      }
      project =
        Cypress.env('AUTH_TYPE') == 'oidc' ? Cypress.env('OIDC_PROJECT') : project

      cy.loadLocalStore('bulk')
        .then((bulkStore) => {
          if (
            bulkStore?.staticPlotContext?.plotLayout &&
            bulkStore?.staticPlotContext?.plotVisit &&
            !forceRedo &&
            currentPlot === plotToSelect
          ) {
            Cypress.log({
              name: 'plotLayout',
              message:
                'User already completed Plot Layout for a different test, nothing to do',
            })
            cy.visit('/projects')
          } else {
            // collector(role) will find only one protocol "plot layout" in the plot selection and layout module.
            cy.selectProtocolIsEnabled(
              'Plot Selection and Layout' /*(project === 'Bird\\ survey\\ TEST\\ Project' ? null : 'Plot Layout and Visit')*/,
            ).then(() => {
              cy.wait(50) //allow time for model config to be defined
              cy.get('[data-cy="locationExisting"] > .q-btn__content').click()
              cy.get('[data-cy="id"]').click()
              cy.log(`Selecting plot '${plotToSelect}'`)
              cy.get('.q-item__label ').contains(new RegExp(`^${plotToSelect}$`)).click()
              cy.nextStep()
                .then(() => {
                  const shouldSkipFauna = cy.$$('.q-stepper__step-content:contains(Would you like create a Fauna Plot?)')
                  cy.log(`shouldSkipFauna (length=${shouldSkipFauna.length}): ${shouldSkipFauna}`)
                  if (shouldSkipFauna.length > 0) {
                    cy.log('Skipping fauna plot')
                    cy.nextStep()   //skip fauna plot
                  }
                })
                .then(() => {
                  cy.get('[data-cy="visitExisting"] > .q-btn__content').click()
                  cy.wait(500)
                  // cy.get('[data-cy="id"]').click().
                  let visitToSelect = 'QLD winter survey'
                  if (plotAndVisitOverride?.visit) {
                    visitToSelect = plotAndVisitOverride.visit
                  }
                  cy.log(`Selecting visit '${visitToSelect}'`)
                  cy.selectFromDropdown('id', null, visitToSelect)
                  const successMsg = 'Successfully set plot context of the collection for the Plot Layout and Visit protocol'

                  cy.workflowPublish(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    { successMsg, willNotQueue: true },
                  )
                })
            })
          }
        })
    })
})


/**
 * Completes the weather component form found in protocols such as Bird Surveys
 *
 * @param {Object} selections an object with the keys representing the field names (precipitation, precipitation_duration, wind_description, cloud_cover, temperature) and values
 */
Cypress.Commands.add('completeWeatherForm', (selections, testEmptyFieldOptions) => {
  //TODO would be better to look at schema to determine these fields
  const numberFields = [
    'temperature',
    'wind_speed',
    'sun_angle',
  ]
  let selectionsToUse = null
  if (isEmpty(selections) || !selections) {
    //didn't specify a selection, so set a default
    selectionsToUse = {
      precipitation: 'Showers',
      precipitation_duration: 'Brief',
      wind_description: 'Light winds',
      cloud_cover: 'Overcast',
      temperature: '20',
    }
  } else {
    selectionsToUse = selections
  }
  console.log('selectionsToUse: ', selectionsToUse)

  for (const [fieldName, valueToUse] of Object.entries(selectionsToUse)) {
    if (!numberFields.includes(fieldName)) {
      cy.selectFromDropdown(fieldName, valueToUse)
      cy.wait(500)
    } else {
      cy.dataCy(fieldName).clear().type(valueToUse).wait(500)
    }
  }
})

/**
 * Iterates through the inputs to test, applying those inputs, making assertions about
 * them, then clearing the field
 *
 * @param {String} field the field name of the field to test
 * @param {Object} inputsToTest an object with key specifying the input and value being another object containing the chainer and value (see: https://docs.cypress.io/api/commands/should#Syntax)
 * @param {Number} nth number for elements index on the dom
 *
 * Example `inputsToTest` (int input):
 * ```json
 * {
    '-1': {
      chainer: 'contain.text',
      value: 'Minimum: 0',
    },
    '1001': {
      chainer: 'contain.text',
      value: 'Maximum: 1000',
    },
    '': { chainer: 'contain.text', value: 'This field is required' },
  }
 * ```
 */
Cypress.Commands.add('testToFailValues', (field, inputsToTest, nth = 0, options = {}) => {
  const wait = options?.wait || 1
  const log = options?.log || false
  Cypress.log({
    name: 'testToFailValues',
    message: `[data-cy="${field}"]`
  })
  console.log(`testToFailValues for [data-cy="${field}"]`, inputsToTest)
  cy.get(`[data-cy="${field}"]`)
    .eq(-1)
    .then(e => {
      for (const [input, condition] of Object.entries(inputsToTest)) {
        cy.get(e, { log: log })
          .type(input !== '' ? input : 'e{backspace}')
          .wait(wait)
          .blur()
        cy.wait(wait)
        cy.get(e, { log: log })
          .parents('.q-field', { log: log })
          .contains(condition.value, { log: log })
          .should('exist')
        // cy.wait(wait)
        // cy.get('.q-field__messages > div', { log: log })
        //   .eq(nth, {log: log})
        //   .should(condition.chainer, condition.value)

        if (input !== '') {
          cy.get(e).clear()
        }
      }
    }).then(() => {
      return cy.get(`[data-cy="${field}"]`)
    })


  // cy.get(`[data-cy="${field}"]`).eq(nth).as('fieldToTest')


  // for (const [input, condition] of Object.entries(inputsToTest)) {
  //   console.log('input: ', input)
  //   console.log('condition: ', condition)
  //   if (input !== '') {
  //     // check for This field is required (empty input)
  //     cy.get(`@fieldToTest`).wait(wait).type(input).wait(wait).blur()
  //   } else {
  //     cy.get(`@fieldToTest`).wait(wait).type('e{backspace}').wait(wait).blur()
  //   }
  //   //`.q-field__messages` is the validation message below the field
  //   cy.wait(100)
  //   cy.get('@fieldToTest').parents('.q-field').wait(wait).contains(condition.value).should('exist')
  //   // cy.get('.q-field__messages > div')
  //   //   .eq(nth)
  //   //   .should(condition.chainer, condition.value)
  //   if (input !== '') {
  //     cy.get(`@fieldToTest`).clear()
  //   }
  //   //wait at the end of each loop, as to allow previous ones to finish
  //   // cy.wait(1000)
  // }
  // return cy.get(`@fieldToTest`)
})

// Allows dragging a Leaflet map by the given amounts. A factor of 1 means the map
// will be dragged the whole width of the map canvas in X direction and the whole
// height of the map canvas in Y direction.
// https://stackoverflow.com/a/62189100
Cypress.Commands.add(
  'dragMapFromCenter',
  { prevSubject: 'element' },
  (element, { xMoveFactor, yMoveFactor }) => {
    // Get the raw HTML element from jQuery wrapper
    const canvas = element.get(0)
    const rect = canvas.getBoundingClientRect()
    const center = {
      x: rect.left + rect.width / 2,
      y: rect.top + rect.height / 2,
    }

    // Start dragging from the center of the map
    cy.log('mousedown', {
      clientX: center.x,
      clientY: center.y,
    })
    canvas.dispatchEvent(
      new MouseEvent('mousedown', {
        clientX: center.x,
        clientY: center.y,
      }),
    )

    // Let Leaflet know the mouse has started to move. The diff between
    // mousedown and mousemove event needs to be large enough so that Leaflet
    // will really think the mouse is moving and not that it was a click where
    // the mouse moved just a tiny amount.
    cy.log('mousemove', {
      clientX: center.x,
      clientY: center.y + 5,
    })
    canvas.dispatchEvent(
      new MouseEvent('mousemove', {
        clientX: center.x,
        clientY: center.y + 5,
        bubbles: true,
      }),
    )

    // After Leaflet knows mouse is moving, we move the mouse as depicted by the options.
    cy.log('mousemove', {
      clientX: center.x + rect.width * xMoveFactor,
      clientY: center.y + rect.height * yMoveFactor,
    })
    canvas.dispatchEvent(
      new MouseEvent('mousemove', {
        clientX: center.x + rect.width * xMoveFactor,
        clientY: center.y + rect.height * yMoveFactor,
        bubbles: true,
      }),
    )

    // Now when we "release" the mouse, Leaflet will fire a "dragend" event and
    // the search should register that the drag has stopped and run callbacks.
    cy.log('mouseup', {
      clientX: center.x + rect.width * xMoveFactor,
      clientY: center.y + rect.height * yMoveFactor,
    })
    requestAnimationFrame(() => {
      canvas.dispatchEvent(
        new MouseEvent('mouseup', {
          clientX: center.x + rect.width * xMoveFactor,
          clientY: center.y + rect.height * yMoveFactor,
          bubbles: true,
        }),
      )
    })
  },
)

/**
 * @deprecated use `$win.ephemeralStore.updateUserCoords`
 * 
 * Cypress command wrapper for helper function above
 */
Cypress.Commands.add('mockLocation', (lat, lng) => {
  console.log('mockLocation command')
  mockLocation(lat, lng)
})

// Cypress.Commands.add('testFieldName', (searchQuery, index = null) => {
//   // try to finish without field name and it should give a error
//   cy.testEmptyField('Field: "Field Name": This field is required', 'done', index || 0)

//   // make sure it gets mad if left empty after being focused
//   cy.selectFromSpeciesList('field_name', 'euc')
//   cy.get('[data-cy="field_name"]')
//     .type('{backspace}')
//     .blur()
//   cy.get('.q-field__messages > div', { timeout: 1000 })
//     .eq(0)
//     .should('contain.text', 'This field is required')

//   // make sure it can handle free text
//   cy.selectFromSpeciesList('field_name', 'I\'m a made up taxon', true)
//   cy.get('.q-field__messages > div', { timeout: 1000 })
//     .should('not.exist')

//   // finish by choosing the query
//   cy.selectFromSpeciesList('field_name', searchQuery, true)
// })

//mostly need GPS for latLngPicker (which can fallback to manual entry), but for cases
//such as maps, we want to wait for the GPS
Cypress.Commands.add('waitForGps', (timeout) => {
  Cypress.log({
    name: 'waitForGps',
    message: `Waiting for GPS with timeout: ${timeout}ms`,
  })
  cy.dataCy('coordsValid', { timeout: timeout }).should('exist')
})

/**
 * Selects an item from a dropdown (q-select - `select` or `filteredSelect`)
 *
 * @param {String} fieldName the name of the field
 * @param {String|Array.<String>|null} itemToSelect the label to select, an array of labels if the dropdown can select multiple, or null if we wish to select a random element
 * @param {String} [searchQuery] a query to search when the dropdown is a `filteredSelect`
 * @param {Number} [elemIndex] the index of the element, if there is more than one on the page with the name field name
 * @param {Boolean} [isVueMultiselect] whether the dropdown is a vue `multiselect`, as opposed to Quasar `q-select`
 * @param {Boolean} regex flag to check whether the itemToSelect is a string or a regex
 *
 * Sources: https://stackoverflow.com/a/73233973, https://stackoverflow.com/a/74980973
 *
 * @example
 *  cy.get('element_name').selectFromDropdown('item_label')
 * @example
 *  cy.selectFromDropdown('element_name', 'item_label')
 * @example
 * // can be used with validateField
 *  cy.get('element_name').validateField().selectFromDropdown('item_label')
 */
Cypress.Commands.add(
  'selectFromDropdown',
  { prevSubject: 'optional' },
  (subject, ...args) => {
    Cypress.log({
      name: 'selectFromDropdown'
    })
    const log = false
    //be specific on the check in `elemIndex`, as passing `0` is falsy.
    //force execution to wait for any lingering network requests that have yet to return a response
    // extends command to be used both as a parent and child
    //  if a child, there is no need to pass in a fieldname
    //  so, when a child, all arguments are shifted by 1 so as to not break the exisiting test.
    let fieldName = args[0]
    let itemToSelect = args[1]
    let searchQuery = args[2] || null
    let elemIndex = args[3] || null
    let isVueMultiselect = args[4] || false
    let regex = args[5] || false
    let itemsToExclude = args[6]
    // add mroe arguments here
    if (subject) {
      // args.unshift(subject)
      cy.wrap(subject, { timeout: 20000, log: log }).as('dropdownElem')
      // fieldName = args[0]
      itemToSelect = args[0]
      searchQuery = args[1]
      elemIndex = args[2]
      isVueMultiselect = args[3]
      regex = args[4]
      itemsToExclude = args[5]
    } else if (elemIndex !== null) {
      cy.get(`[data-cy="${fieldName}"]`, { timeout: 20000, log: log })
        .eq(elemIndex)
        .as('dropdownElem')
    } else {
      cy.get(`[data-cy="${fieldName}"]`, { timeout: 20000, log: log }).as('dropdownElem')
    }

    if (searchQuery) {
      cy.get('@dropdownElem', { log: log }).type(searchQuery, { log: log }).wait(500)
    } else {
      cy.get('@dropdownElem', { log: log }).click({ log: log }).wait(800) //opens the dropdown for `withinSelectMenu` to access
    }

    if (isNil(itemToSelect)) {
      let numberOfElements = null
      if (isVueMultiselect) {
        // cy.get('.multiselect__element[role=option]').as('dropdownOptions')
        //dropdown elements that are visible will have an ID like `null-0`, `null-1`, etc.
        cy.get(`.multiselect__element[id^=${fieldName}_select]`, { log: log }).as(
          'dropdownOptions',
        )
        cy.get('@dropdownOptions', { log: log }).eq(0).scrollIntoView()

        cy.get('@dropdownOptions', { log: log })
          .then(($elem) => {
            numberOfElements = $elem.length
          })
          .then(() => {
            cy.get('@dropdownOptions', { log: log })
              .eq(Math.floor(Math.random() * numberOfElements))
              .click()
              .wait(200)
          })
      } else {
        cy.withinSelectMenu({
          persistent: true,
          fn: () => {
            cy.get('.q-item[role=option]', { log: log }).as('dropdownOptions')
            //workaround - sometimes there's a bunch of blank values before/after the actual
            //values, so need to scroll past these blanks
            cy.get('@dropdownOptions', { log: log }).eq(0, { log: log }).scrollIntoView({ log: log })

            cy.get('@dropdownOptions', { log: log })
              .then(($elem) => {
                numberOfElements = $elem.length
              })
              .then(() => {
                cy.get('@dropdownOptions', { log: log })
                  .eq(Math.floor(Math.random() * numberOfElements), { log: log })
                  .as('chosenOption', { log: log })
                  .then(() => {
                    //might be covered by q-notification
                    cy.get('@chosenOption', { log: log }).click({ force: true, log: log })
                  })
              })
          },
        })
      }
    } else {
      let selections = null
      if (Array.isArray(itemToSelect)) {
        selections = itemToSelect
      } else {
        //wrap single in an array to simplify code (reduce duplication)
        selections = [itemToSelect]
      }
      for (const item of selections) {
        const itemToCheck = regex ? new RegExp(item, 'g') : item
        if (isVueMultiselect) {
          cy.get('.multiselect__element[role=option]', { log: log }).eq(0).scrollIntoView()
          cy.get('.multiselect__element[role=option]', { log: log })
            .contains(itemToCheck)
            .click()
            .wait(200)
        } else {
          cy.withinSelectMenu({
            persistent: true,
            fn: () => {
              cy.get('.q-item[role=option]', { log: log }).as('dropdownOptions')
              cy.get('@dropdownOptions', { log: log }).eq(0).scrollIntoView()
              cy.get('@dropdownOptions', { log: log }).contains(itemToCheck).click().wait(200)
            },
          })
        }
      }
    }
    return cy.get('@dropdownElem', { log: log })
      .then((e) => e.css('pointer-events') === 'none' ? e : cy.get(e, { log: log }).type('{esc}').then(() => e)
      )
  }
)


Cypress.Commands.add(
  'selectFromRadioOptionGroup',
  (fieldName, itemToSelect) => {
    // console.log(`selecting '${itemToSelect}' from field '${fieldName}'`)
    cy.get(`[data-cy="${fieldName}"]`)
      .children()
      .contains(itemToSelect)
      .siblings()
      //first sibling is the radio button
      .eq(0)
      .click()
  },
)

/**
 * Selects an item from the species list, similar to `selectFromDropdown()`
 *
 * @param {String} fieldName the name of the field
 * @param {String} itemToSelect the LUT label to select
 * @param {Boolean} [useFieldName] whether to use the `searchQuery` as a field name, rather than selecting a specific item. Defaults to false; cannot be used with `searchQuery`
 * @param {String} [searchQuery] a query to search when the dropdown is a `filteredSelect`. Defaults to null; cannot be used with `useFieldName`
 * @param {Number} [elemIndex] the index of the element, if there is more than one on the page with the name field name
 */
Cypress.Commands.add(
  'selectFromSpeciesList',
  { prevSubject: 'optional' },
  (subject, ...args) => {
    Cypress.log({
      name: 'selectFromSpeciesList'
    })
    const log = false
    let fieldName = args[0]
    let searchQuery = args[1]
    let useFieldName = args[2] || null
    let itemToSelect = args[3] || null
    let elemIndex = args[4] || false
    if (subject) {
      cy.get(subject, { timeout: 20000 }).as('speciesListDropdownElem')
      searchQuery = args[0]
      useFieldName = args[1] || null
      itemToSelect = args[2] || null
      elemIndex = args[3] || false
    } else if (elemIndex !== false) {
      cy.dataCy(fieldName, { log: log }).eq(elemIndex).as('speciesListDropdownElem')
    } else {
      cy.dataCy(fieldName, { log: log }).as('speciesListDropdownElem')
    }

    if (useFieldName && itemToSelect) {
      throw new Error(
        'Cannot use `useFieldName` and `itemToSelect` params together',
      )
    }
    // make sure the species list has loaded initially before trying any actual input 
    // let before
    cy.get('@speciesListDropdownElem')
      .click()
      .then(() => new Date())
      .then((before) => {
        cy.get('@speciesListDropdownElem', { timeout: 120000 })
          .should('have.attr', 'aria-expanded', 'true')
          .then(() => {
            cy.log(`species list took ${(new Date()) - before} ms to load`)
            console.log(`species list took ${(new Date()) - before} ms to load`)
          })

      })


    if (useFieldName) {
      //don't want to `get` twice as it results in weird behaviour with dropdown options
      //TODO arbitrary `wait` not ideal solution
      cy.get('@speciesListDropdownElem', { log: log })
        .type(`${searchQuery}`)
        .wait(500) //this component have a input-debounce of 500 ms
        .type('{enter}')
        .wait(3000)
        .type('{esc}')
        .wait(3000)
    } else {
      cy.get('@speciesListDropdownElem', { log: log }).type(searchQuery).wait(1000)
        // TODO '.q-item[role=option]'.should('exist') is causing vertebrate-active-search, vertebrate-passive-search tests
        // to fail when there is just 1 item in select menu.
        // cy.get('.q-item[role=option]', { timeout: 30000 })
        //   .should('exist')
        .then(() => {
          cy.wait(1000)
          if (itemToSelect) {
            cy.withinSelectMenu({
              persistent: true,
              fn: () => {
                if (cy.$$('.q-virtual-scroll__padding').length > 0) {
                  cy.get('.q-virtual-scroll__padding').invoke('remove')
                }
                //long timeout as the vascular plant species list can take a while to search
                cy.get('.q-item[role=option]', { timeout: 30000, log: log })
                  .eq(0)
                  //workaround - sometimes there's a bunch of blank values before/after the
                  //actual LUT values, so need to scroll past these blanks
                  .scrollIntoView()
                cy.get('.q-item[role=option]', { log: log }).contains(itemToSelect).click().wait(500)
              },
            })
          } else {
            //no specific selection or flag to use field name, so just pick the first element
            cy.get('@speciesListDropdownElem', { log: log }).type('{downarrow}{enter}')
          }
        })
    }
    return cy.get('@speciesListDropdownElem', { log: log })
  })

/**
 * Selects a date from the 'date' Crud element
 *
 * @param {String} fieldName the name of the field - e.g., `start_date`
 *
 * @param {Object} date an object for the date with keys `year`, `month, `day`
 * @param {String|Number} date.year the 4-digit year, or 'current' to use current year
 * @param {String|Number} date.month the full month string (e.g., 'April'), or 'current' to use current month
 * @param {String|Number} date.day the day number (e.g., '10'), or 'current' to use current day
 * @param {Boolean} [popup] whether the picker is behind a button that triggers a popup (true by default, as this is Crud's default too). Applicable when we are using a `q-date` outside of Crud
 */
Cypress.Commands.add(
  'customSelectDate',
  ({ fieldName, date, btnIndex = 0 }, popup = true) => {
    const todaysDate = new Date()
    const yearStr = (() => {
      if (date.year === 'current') {
        return todaysDate.getFullYear()
      }
      return String(date.year)
    })()
    const monthStr = (() => {
      //quasar abbreviates months to 3-letters, so need to slice
      if (date.month === 'current') {
        const monthNames = [
          'January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December',
        ]
        return monthNames[todaysDate.getMonth()].slice(0, 3)
      }
      return String(date.month).slice(0, 3)
    })()
    const dayStr = (() => {
      if (date.day === 'current') {
        return String(todaysDate.getDate())
      }
      return String(date.day)
    })()
    console.log(`Selecting date: ${yearStr}/${monthStr}/${dayStr}`)

    if (popup) {
      cy.dataCy(`${fieldName}_dateBtn`).eq(btnIndex).click().wait(500) //opens date picker popup
    }

    //select year
    cy.get(
      `[data-cy="${fieldName}_dateInput"] > .q-date__main > .q-date__content > .q-date__view > .q-date__navigation > .relative-position.overflow-hidden.flex.flex-center`,
    )
      .eq(1)
      .click()
      .wait(200)
    cy.get('.q-date__years').contains(yearStr).click().wait(200)

    //select month
    cy.get(
      `[data-cy="${fieldName}_dateInput"] > .q-date__main > .q-date__content > .q-date__view > .q-date__navigation > .relative-position.overflow-hidden.flex.flex-center`,
    )
      .eq(0)
      .click()
      .wait(200)
    cy.get('.q-date__months').contains(monthStr).click().wait(200)

    //select day
    cy.get(
      `[data-cy="${fieldName}_dateInput"] > .q-date__main > .q-date__content > .q-date__view > .q-date__calendar-days-container > .q-date__calendar-days > .q-date__calendar-item--in`,
    )
      .contains(dayStr)
      .click()
      .wait(200)

    if (popup) {
      cy.dataCy(`${fieldName}_dateClose`).click().wait(500)
    }
  },
)

/**
 * Selects a time from the 'time' Crud element
 *
 * @param {String} fieldName the name of the field - e.g., `start_date`
 *
 * @param {Object} time an object for the time with keys `hour`, `minute`
 * @param {String|Number} time.hour the 24hr representation of the hour (e.g., '14' for 2pm), or 'current' to use current hour
 * @param {String|Number} time.minute the minute, or 'current' to use current minute. Rounds to nearest 5
 */
Cypress.Commands.add('selectTime', ({ fieldName, time }) => {
  const todaysDate = new Date()
  const hourStr = (() => {
    if (time.hour === 'current') {
      return String(todaysDate.getHours())
    }
    return String(time.hour)
  })()
  const minStr = (() => {
    //need to round to nearest 5, as time picker element only displays minutes that are
    //multiple of 5
    //https://stackoverflow.com/a/18953446
    function round5(x) {
      return Math.floor(x / 5) * 5
    }
    if (time.minute === 'current') {
      return String(round5(todaysDate.getMinutes()))
    }
    return String(round5(time.minute))
  })()
  console.log(`Selecting time: ${hourStr}:${minStr}`)

  cy.dataCy(`${fieldName}_timeBtn`).click().wait(200) //opens time picker popup

  //select hour
  cy.get(
    `[data-cy="${fieldName}_timeInput"] > .q-time__main > .q-time__content`,
  )
    .contains(hourStr)
    .click()
    .wait(200)

  //select minute
  cy.get(
    `[data-cy="${fieldName}_timeInput"] > .q-time__main > .q-time__content`,
  )
    .contains(minStr)
    .click()
    .wait(200)

  cy.dataCy(`${fieldName}_timeClose`).click().wait(200)
})

/**
 * Selects a date and time from the 'datetime' Crud element
 *
 * @param {String} fieldName the name of the field - e.g., `start_date`
 *
 * @param {Object} date an object for the date with keys `year`, `month, `day`
 * @param {String|Number} date.year the 4-digit year, or 'current' to use current year
 * @param {String|Number} date.month the full month string (e.g., 'April'), or 'current' to use current month
 * @param {String|Number} date.day the day number (e.g., '10'), or 'current' to use current day
 *
 * @param {Object} time an object for the time with keys `hour`, `minute`
 * @param {String|Number} time.hour the 24hr representation of the hour (e.g., '14' for 2pm), or 'current' to use current hour
 * @param {String|Number} time.minute the minute, or 'current' to use current minute. Rounds to nearest 5
 */
Cypress.Commands.add('selectDateTime', ({ fieldName, date, time }) => {
  cy.customSelectDate({ fieldName, date })
  cy.selectTime({ fieldName, time })
})

// Logout Test Function
Cypress.Commands.add('logout', (keepLocalData = false) => {
  cy.get('[data-cy="username"]').click()
  cy.get('[data-cy="logout"]').click()
  cy.wait(1000)
  if (!keepLocalData) {
    cy.clearAllCookies()
    cy.clearAllLocalStorage()
  }
})

Cypress.Commands.add(
  'testEmptyField',
  (message, btn = 'done', index = null, q_notification_index = -1) => {
    // close any popups that may exists prior, since they are limited now
    cy.document({ log: false }).within(() => {
      cy.dismissPopups().wait(200)

      if (index !== null) {
        cy.dataCy(btn, { log: false }).eq(index).as('buttonToClick')
      } else {
        cy.dataCy(btn, { log: false }).as('buttonToClick')
      }

      cy.get('@buttonToClick', { log: false }).click()

      cy.get('.q-notification', { timeout: 15000, log: false })
        .eq(q_notification_index)
        .as('popup')
      cy.get('@popup', { log: false }).should('contain.text', message)
      cy.get('@popup', { log: false }).contains('Dismiss').click().wait(500)
    })
  },
)

Cypress.Commands.add('assertPlotContext', (plotLayoutLabel, visitFieldName) => {
  cy.wait(3000)
  cy.dataCy('pulling-api-status', { timeout: 60000 })
    .should('not.exist', { timeout: 60000 })
  cy.dataCy('currentPlot').should('contain.text', plotLayoutLabel)
  cy.dataCy('currentVisit').should('contain.text', visitFieldName)

  cy.loadManyStores(
    [
      {
        storeName: 'bulk',
      },
      {
        storeName: 'apiModels',
      },
    ],
  )
    .then((stores) => {
      const staticPlotContext = stores.bulkStore.staticPlotContext
      console.log('staticPlotContext:', staticPlotContext)
      if (
        staticPlotContext?.plotLayout?.id &&
        staticPlotContext?.plotVisit?.id &&
        !Object.keys(staticPlotContext.plotLayout.id).includes(
          'temp_offline_id',
        ) &&
        !Object.keys(staticPlotContext.plotVisit.id).includes('temp_offline_id')
      ) {
        const resolvedPlotId = stores.apiModelsStore.models['plot-layout'].find(
          (o) => o.plot_selection.plot_label === plotLayoutLabel,
        )?.id
        console.log('resolvedPlotId:', resolvedPlotId)
        const resolvedVisitId = stores.apiModelsStore.models['plot-visit'].find((o) =>
          o.visit_field_name.includes(visitFieldName) && o.id === staticPlotContext.plotVisit.id,
        )?.id
        console.log('resolvedVisitId:', resolvedVisitId)
        cy.wrap(resolvedPlotId).should('eq', staticPlotContext.plotLayout.id)
        cy.wrap(resolvedVisitId).should('eq', staticPlotContext.plotVisit.id)
      }
    })
})

/**
 * adds images in a multi media component
 *
 * @param {Number} amount how many images to take
 * @param {String} comment an optional comment to add
 * @param {Number} amountToRemove integer of how many images to be deleted
 * @param {Number} timeout timeout in ms to apply to all gets
 */
Cypress.Commands.add(
  'addImages',
  (
    fieldName,
    amount = 1,
    { comment, timeout = 8000, amountToRemove, test, btnIndex = 0, log = false } = {},
  ) => {
    cy.get(`[data-cy="${fieldName}"]`).eq(btnIndex).click() // unhide the photo section
    cy.dataCy('cameraIsReady').should('exist')
    cy.wait(100)
    if (amount) {
      for (let i = 0; i < amount; i++) {
        cy.get('[data-cy="takePhoto"]', { timeout: timeout, log: log }).click()
        if (comment) {
          cy.get('[data-cy="comment"]', { timeout: timeout, log: log }).eq(i, { log: log }).click()
          cy.document().within(() => {
            cy.get('[data-cy="addComment"]', { timeout: timeout, log: log })
              .eq(i)
              .type(comment)
            cy.get('[data-cy="saveComment"]', { timeout: timeout, log: log })
              .eq(i)
              .click()
            cy.get('.q-notification', { timeout: timeout, log: log }).should(
              'contain.text',
              'New comment added',
            )
            cy.get(
              '.q-notification__actions > .q-btn > .q-btn__content > .block',
              { timeout: timeout, log: log },
            )
              .eq(0)
              .click()
          })
        }
      }
    }

    // just deletes the first one each time
    if (amountToRemove) {
      for (let i = 0; i < amountToRemove; i++) {
        cy.get('[data-cy="delete"]', { log: log }).eq(0, { log: log }).click()
      }
    }

    if (test) {
      if (
        cy.get('.photo-column > #preview-photo > .galleryImg').should('exist')
      ) {
        cy.log("Yep, that's an image alright")
      }
    }
    // hide photo section
    cy.get(`[data-cy="${fieldName}"]`, { timeout: timeout, log: log })
      .eq(btnIndex, { log: log })
      .click({ log: log })
      .wait(1000)
  },
)

/**
 * adds audio in a multi media component
 * @param {String} type name of the multimedia type, e.g., 'Video'
 * @param {Number} amount how many of the media to take
 * @param {Number} duration time in ms to record for
 * @param {String} comment an optional comment to add
 * @param {Number} amountToRemove integer of how many to be deleted
 * @param {Number} timeout timeout in ms to apply to all cy.gets
 * @param {Boolean} testPlayback whether to test playback or not
 * @param {Boolean} dontRunHeadless whether to continue the function if running in headless mode
 */
Cypress.Commands.add(
  'addMultiMedia',
  (
    type,
    amount = 1,
    duration = 1000,
    {
      comment,
      amountToRemove,
      testPlayback,
      dontRunHeadless,
      btnIndex = 0,
    } = {},
  ) => {
    // if you dont want it to execute in headless mode because it will break, it will return before executing anything
    if (Cypress.browser.isHeadless && dontRunHeadless) return
    cy.get(`[data-cy="add${type}"]`).eq(btnIndex).as('mediaBtn').click()

    // record audio
    range(amount).forEach((i) => {
      cy.get('[data-cy="startRecording"]').click().wait(duration)
      cy.get('[data-cy="stopRecording"]').click()
      if (comment) {
        cy.get('[data-cy="comment"]').eq(i).click()
        // FIXME clicking comment on any video except the very first one that had its comment edited
        // will require an extra step of clicking 'edit' before being able to write a fresh comment.
        // if using within(), will need to go back to global scope to get the dialog
        cy.document().within(() => {
          cy.get('[data-cy="addComment"]')
            .wait(200)
            .then(($e) => {
              if ($e[0].disabled)
                cy.get('.q-card__actions > .q-btn > .q-btn__content > .block')
                  .eq(0)
                  .click({ force: true })
            })
          cy.get('[data-cy="addComment"]').eq(i).type(comment)
          cy.get('[data-cy="saveComment"]').eq(i).click()

          cy.get('.q-notification').should('contain.text', 'New comment added')
        })
      }
    })

    // playback
    if (testPlayback) {
      cy.get(`.preview${type}`).each(($el) => {
        cy.wrap($el)
          .as('mediaPlaying')
          .should('have.prop', 'paused', true)
          .and('have.prop', 'ended', false)
          .then(($media) => {
            $media[0].play()
          })
        cy.get('@mediaPlaying')
          .should('have.prop', 'paused', false)
          .and('have.prop', 'ended', false)
        cy.get('@mediaPlaying', { timeout: duration + 1000 }).should(
          'have.prop',
          'ended',
          true,
        )
      })
    }

    // delete
    if (amountToRemove) {
      cy.document()
        .find('body')
        .within(($body) => {
          range(amountToRemove).forEach(() => {
            if ($body.find('[data-cy="delete"]').length > 0) {
              cy.get('[data-cy="delete"]').eq(0).click()
            } else {
              cy.get('.delete-button').eq(0).click()
            }
          })
        })
    }
    // hide section
    cy.get('@mediaBtn').click().wait(1000)
  },
)

Cypress.Commands.add('pullAllApiModels', () => {
  // It is assumed tests will be running on a small screen size like usual
  cy.get('.q-btn--dense > .q-btn__content > .q-icon').click()
  cy.get('.status.q-px-md > :nth-child(5) > .q-btn__content > .block').click()
  cy.get('.fullscreen').click()
  cy.dataCy('pulling-api-status', { timeout: 1000000 }).should('not.exist')
  cy.reload()
  // cy.dataCy('workflowLoaded', { timeout: 180000 })
  // .should('exist')
})

Cypress.Commands.add('getAuthToken', ({ host }) => {
  cy.loadLocalStore('auth')
    .then((authStore) => {
      if (host === 'org') {
        //requests to org need a legit token
        return authStore?.token
      }
      return '1234' // used to bypass the middleware in pdp-data-filters.js
    })
})
/**
 * Make a network request to core or org.
 * Should only be run BEFORE moving to the module/protocol if being used to delete or update items
 *
 * @param {String} protocol name of the protocol. Must be plural
 * @param {String} method REST method for url
 * @param {String} id strapi id
 * @param {String} port url port to swap between strapi or org. Default is core
 * @param {Object} args additional parameters for url. e.g. populate=* -> {'populate': '*'}
 */
Cypress.Commands.add(
  'doRequest',
  (
    protocol,
    {
      method = 'GET',
      host = 'core',
      id,
      args,
      body = null,
      failOnStatusCode = false,
    } = {},
  ) => {
    console.log(`doRequest called for protocol ${protocol}. method=${method}. host=${host}. id=${id}. failOnStatusCode=${failOnStatusCode}. args=${JSON.stringify(args)}`)
    if (!['core', 'org'].includes(host))
      throw new Error(`Needs to be 'core' or 'org'`)

    cy.window({ log: false })
      .then((win) => {
        // FIXME quick hardcode fix to get the tests to run against dev
        if (Cypress.config('baseUrl').includes('dev.app')) {
          return `https://dev.${host}-api.paratoo.tern.org.au`
        }
        //FIXME this fallback doesn't work when in pipeline (can't use localhost, as need to access stack running in container's host VM)
        let url = win?.constants?.[host]
        if (!url) {
          let hostPort = null
          if (host === 'core') {
            hostPort = '1337'
          } else if (host === 'org') {
            hostPort = '1338'
          }
          url = `http://localhost:${hostPort}`
          console.warn(`WARNING: unable to get URL from constants - defaulting to localhost`)
        }
        return url
      })
      .then((baseUrl) => {
        cy.getAuthToken({ host })
          .then((token) => {
            let options = {
              method: method,
              url: `${baseUrl}/api`,
              failOnStatusCode: failOnStatusCode,
              auth: { bearer: token },
              timeout: 120000,   //2 min
            }

            if (body) {
              options['body'] = body
            }

            // make sure the protocol name is pluralised
            options['url'] = `${options['url']}/${protocol}`
            // if (protocol.slice(-1) !== 's')
            //   options['url'] = `${options['url']}s`
            // if given an id, put it at the end of
            if (id) options['url'] = `${options['url']}/${id}`
            // append all the arguments to the url as paramers
            if (args) {
              Object.entries(args).forEach((arg) => {
                options['url'] = `${options['url']}${options['url'].includes('?') ? '&' : '?'
                  }${arg[0]}=${arg[1]}`
              })
            }
            // org requires the jwt in the header, as opposed to core, which uses a url param
            options['auth'] = { bearer: token }
            // if (host === 'org') {
            // } else if (host === 'core') {
            //   options['url'] = `${options['url']}${options['url'].includes('?') ? '&' : '?'}auth=${token}`
            // }
            return options
          })
          .then((options) => {
            // cy.log('options', JSON.stringify(options))
            return cy.request(options)
          })
      })
  },
)

/**
 * Selects all the names given from an array. If a name wasn't found it in the list it
 * will be typed in after, adding it to the list.
 *
 * @param {String} fieldName the data-cy of the field
 * @param {Array} names The names of the observers you want added as strings
 * @param {Number} [elemIndex] the index of the element, if there is more than one on the page with the name field name
 */
Cypress.Commands.add('addObservers', (fieldName, names, { elemIndex } = {}) => {
  elemIndex
    ? cy.get(`[data-cy="${fieldName}"]`).eq(elemIndex).as('observerDropdown')
    : cy.get(`[data-cy="${fieldName}"]`).as('observerDropdown')

  let usedNames = []
  cy.get('@observerDropdown')
    .click()
    .wait(800)
    .then(() => {
      cy.withinSelectMenu({
        persistent: true,
        fn: () => {
          if (cy.$$('.q-item[role=option]').length > 0) {
            cy.get('.q-item[role=option]').as('observerOptions')
            cy.get('@observerOptions').then((options) => {
              for (const [index, option] of Array.from(options).entries()) {
                if (option.textContent.includes('clear'))
                  option.textContent = option.textContent.split('clear')[0]
                if (names.includes(option.textContent)) {
                  usedNames.push(option.textContent)
                  cy.get(options[index]).click().wait(500)
                  if (usedNames.length == names.length) break
                }
              }
            })
          }
        },
      })
    })
    .then(() => {
      let difference = names.filter((name) => !usedNames.includes(name))
      for (const leftoverName of difference) {
        cy.get('@observerDropdown')
          .type(leftoverName + '{enter}')
          .wait(900)
      }
      cy.get('@observerDropdown').type('{enter}')
    })
    .type('{esc}')
})

/**
 *
 * @deprecated use cy.get() and chain .toggle() instead
 *
 * sets state of an radio or checkbox. Will not change is already desired state.
 *
 * @param {String} dataCy data-cy name to select
 * @param {Boolean} desiredState what boolean state you want the element to be in
 * @param {Number} [index] data-cy index
 *
 * @returns the data-cy element
 */
Cypress.Commands.add(
  'setCheck',
  (dataCy, desiredState = true, { index } = {}) => {
    // TODO make this a child command, works better than way
    if (typeof desiredState !== 'boolean')
      throw new Error(`setCheck() desiredState must be boolean`)
    cy.get(`[data-cy="${dataCy}"]`).then(() => {
      const $data = index
        ? cy.$$(`[data-cy="${dataCy}"]`).eq(index)
        : cy.$$(`[data-cy="${dataCy}"]`)
      if ($data[0].ariaChecked !== desiredState.toString()) {
        cy.get(`[data-cy="${dataCy}"]`).click().wait(500)
        cy.log(`changed state to ${desiredState}`)
      } else {
        cy.log(`state already ${desiredState}. No need to change.`)
      }
      return cy.get(`[data-cy="${dataCy}"]`)
    })
  },
)

/**
 * chainable replacement for setCheck. Clicks checkbox and radio to the desired state given. If the subject is q-radio it will assert the cancel button works.
 * Unsafe to chain further as it changes the DOM element, meaning it is not guaranteed subject is the same element now.
 *
 * @param {Boolean} desiredState what boolean state you want the element to be in.
 *
 * @returns the subject given
 */
Cypress.Commands.add(
  'toggle',
  { prevSubject: 'element' },
  (subject, state = true, { log = false } = {}) => {
    if (typeof state !== 'boolean')
      throw new Error(`toggle() state argument must be boolean`)
    cy.get(subject, { log: log })
      .then(($data) => {
        cy.log(`state is currently ${$data.attr('aria-checked')} and is being set to ${state}`)
        // test the cancel button works if we're on a radio, THEN do the normal check.
        // if not true, click it so it is so the cancel button appears.
        if ($data.parents('.q-list .q-item').find('.q-radio').length > 0) {
          cy.log('This is a q-radio')

          if ($data[0].ariaChecked !== 'true') {
            cy.get(subject).click().wait(500)
            cy.get(subject).should(
              'have.attr',
              'aria-checked',
              'true',
            )
          }
          // cancel the current selected item
          cy.get(subject)
            .parents('.q-item')
            .within(() => {
              cy.get('i.q-icon:contains(cancel)').as('cancelBtn').click()
            })
          cy.get('@cancelBtn').should('not.exist')
        }
        // Subject might have updated, so to be safe it should be grabbed again.
        cy.get(subject, { log: log })
          .then($data => {
            if (state.toString() !== $data[0].ariaChecked) {
              cy.log(`changing state to ${state}`)
              cy.get(subject).click().wait(500)
            } else {
              cy.log(`state already ${state}. No need to change.`)
            }
            cy.get(subject).should(
              'have.attr',
              'aria-checked',
              toString(state),
            )
          })
          .then(() => {
            return subject
          })
      })
  })

/**
 * sets state of an expansion item. Will not change is already desired state.
 *
 * @param {String} fieldName data-cy name to select
 * @param {String} desiredState what state you want the expansion item to be in. "open" or "closed"
 * @param {Number} [index] data-cy index
 *
 * @returns cy.get query with the expansion fieldname
 */
Cypress.Commands.add(
  'setExpansionItemState',
  (fieldName, desiredState, index = -1) => {
    Cypress.log({
      name: `setExpansionItemState`,
      message: `${fieldName}[${index}] setting to '${desiredState}'`
    })

    const states = {
      closed: 'q-expansion-item--collapsed',
      open: 'q-expansion-item--expanded',
    }

    cy.get(`[data-cy*="${fieldName}"] `, { log: false })
      .eq(index)
      .then(expansion => {
        const hasClass = expansion.hasClass(states[desiredState])
        if (!hasClass) {
          cy.get(expansion, { log: false })
            .find('.q-expansion-item__container > .q-item')
            .click()
            .wait(1000)
          cy.get(expansion)
            .then(expansionChild => {
              if (!expansionChild.attr('aria-expanded')) {
                return expansionChild.find('.q-expansion-item__container > .q-item')
              } else {
                return expansionChild
              }
            }).should('have.attr', 'aria-expanded', (!hasClass).toString().toLowerCase())
        }
        cy.log(`state set as ${desiredState}`, expansion.hasClass(states[desiredState]))
          .then(() => {
            return expansion
          })
      })
  })

Cypress.Commands.add('prepareForOfflineVisit', (project = null, success=true) => {
  console.log('IM INSIDE PREPAREFOROFFLINEVISIT')
  let projectToUse = project
  if (projectToUse === null) {
    //no project provided so fallback to kitchen sink
    projectToUse = 'Kitchen\\ Sink\\ TEST\\ Project'
  }

  cy.dataCy('prepareForOfflineVisitBtn').click()
  cy.dataCy(`prepareOfflineProject_${projectToUse}`).check()
  cy.dataCy('donePrepareVisitBtn').click()
  
  if (!success) {
    cy.get('.q-notification', { timeout: 120000 }).should(
      'contain.text',
      'Failed to refresh models for protocols in project',
    )
    return
  }

  //wait for models to finish refreshing before continuing test
  cy.get('.q-notification', { timeout: 120000 }).should(
    'contain.text',
    'Successfully refreshed models for protocols in project',
  )
})

Cypress.Commands.add(
  'newLayoutAndVisit',
  (plot, coords, doFaunaPlot, visit) => {
    cy.window()
      .then(($win) => {
        //set coords to recommended location from the Plot Selection
        $win.ephemeralStore.updateUserCoords(coords, true)
      })
      .then(() => {
        cy.dataCy('locationNew').click()
        cy.dataCy('plotAlignedToGridYes').click()
        cy.selectFromDropdown('plot_selection', plot)
        cy.dataCy('replicate').type('1')
        cy.dataCy('plot_type').selectFromDropdown()
        cy.get('[data-cy="100 x 100"]').click()
        cy.dataCy('startingPoint').selectFromDropdown('North East').wait(1000)
        cy.dataCy('startLayout').click().wait(1000)
        cy.dataCy('setStartPoint').click().wait(1000)
        cy.dataCy('spoofAllPoints').click().wait(1000)
        cy.dataCy('markClosestPoint').click()
        cy.dataCy('workflowNextButton').click().wait(3000)
      })
      .then(() => {
        if (doFaunaPlot) {
          //fauna plot
          cy.dataCy('faunaPlotYes').click()
          cy.dataCy('startLayout').click().wait(1000)
          cy.dataCy('setStartPoint').click().wait(1000)
          cy.dataCy('spoofAllPoints').click().wait(1000)
          cy.dataCy('markClosestPoint').click().wait(1000) //'done' - closes modal
          cy.dataCy('workflowNextButton').click().wait(3000)
        } else {
          cy.dataCy('workflowNextButton').click().wait(3000)
        }
      })
      .then(() => {
        cy.dataCy('visit_field_name').type(visit)
      })
      .then(() => {
        cy.window().then(($win) => {
          //reset coords
          //since we're in Cypress and `force` is false, it will ignore any coords
          $win.ephemeralStore.updateUserCoords()
        })
      })
  },
)

/**
 * This command is used to navigate to the next step in a workflow.
 * It first clicks the 'workflowNextButton' at the specified index, then it checks if the next step exists and if the title of the next step is correct.
 * The command can also accept an options object that can include a `force` option to force the click and an `index` option to specify the index of the 'workflowNextButton'.
 *
 * @param {Object} [options] - An options object that can include a `force` option to force the click and an `index` option to specify the index of the 'workflowNextButton'.
 * @param {boolean} [options.force=false] - A boolean indicating whether to force the click.
 * @param {number} [options.index=0] - A number indicating the index of the 'workflowNextButton'.
 * @example
 * // Navigate to the next step in a workflow
 * cy.nextStep({ force: true, index: 1 })
 * @example
 * cy.nextStep()
 */
Cypress.Commands.add('nextStep', ({ force = false, index = 0, log = false } = {}) => {
  cy.dataCy('workflowNextButton', { log: log })
    .eq(index, { log: log })
    .parents('.q-stepper__step', { log: log })
    .invoke({ log: log }, 'attr', 'data-cy')
    .then((stepDataCY) => {
      // cy.log(stepDataCY)
      cy.get(`[data-cy="${stepDataCY}"]`, { log: log })
        .find('.q-stepper__step-content', { log: log })
        .should('exist')
      cy.dataCy('workflowNextButton', { log: log }).eq(index, { log: log }).click({ force: force, log: log })
      cy.get(`[data-cy="${stepDataCY}"]`, { log: log })
        .find('.q-stepper__step-content', { log: log })
        .should('not.exist')
      cy.get('.q-stepper__step', { log: log }).then((steps) => {
        const stepArray = Array.from(steps).map((s) =>
          s.getAttribute('data-cy'),
        )
        cy.get(`[data-cy="${stepArray[stepArray.indexOf(stepDataCY) + 1]}"]`, { log: log })
          .find('.q-stepper__step-content', { log: log })
          .should('exist')
        // log && cy.log(stepArray[stepArray.indexOf(stepDataCY) + 1])
        if (stepArray[stepArray.indexOf(stepDataCY) + 1] !== 'Summary') {
          // ignore summary since there are no 'title' data-cys
          cy.dataCy('title', { log: log })
            .eq(0, { log: log })
            .should(
              'contain.text',
              stepArray[stepArray.indexOf(stepDataCY) + 1],
            )
        }
      })
    })
  cy.wait(400) // just for good measure, i guess
})

Cypress.Commands.add('getSummaryAsObject', () => {
  let summary = {}
  cy.get('table > tbody')
    .each((table) => {
      cy.wrap(table)
        .find('tr')
        .each((row) => {
          cy.wrap(row)
            .find('td')
            .then((cells) => {
              summary[cells.eq(0).text()] = cells.eq(1).text()
            })
        })
    })
    .then(() => {
      return summary
    })
})

Cypress.Commands.overwrite('visit', (originalFn, url, options) => {
  cy.url().then((currentUrl) => {
    cy.log('currentUrl', currentUrl)
    cy.log('url', url)
    if (currentUrl.includes(url)) {
      cy.log(`Already on the ${url} page`)
      // Add your assertions or actions for the current page here.
      return
    }

    cy.log('cy.visit: ', url)
    originalFn(url, options)
  })
})

/**
 * This command is used to dismiss all open popups in the application.
 * It iterates over all the notification states in the window's ephemeralStore and clicks the dismiss button for each open popup.
 * If a popup is not found or has already been dismissed, it logs an error message.
 * Uses the underlying jQuery object of Cypress to avoid built in assertions, as this isnt for testing, just removing popups due to their odd behaviour.
 *
 * @example
 * // Dismiss all open popups
 * cy.dismissPopups()
 */
Cypress.Commands.add('dismissPopups', () => {
  cy.window({ log: false }).then(($win) => {
    const states = $win.ephemeralStore.notificationState
    for (const key in states) {
      if (states[key] === true) {
        try {
          cy.$$(`.q-notifications .bg-${key}`).each((index, el) => {
            cy.$$(el).find(`.q-btn:contains(Dismiss)`).trigger('click')
          })
        } catch (error) {
          console.log(
            'Notification is gone, unable to trigger click event on moving, hidden, or removed elements.',
            error,
          )
        }
      }
    }
  })
})

/**
 * Child command that uses a given documentation model to auto-test fields constraints. Uses the subjects data-cy to match find matches.
 * It is assumed data-cy's are the same, but has yet to be tested 100%.
 * Doesnt do everything, will still need to do some case-by-case testing yourself. e.g., custom rules
 *
 * testEmptyField logic will need to be passed in seperately if inside observations as they need to click 'done' instead. Otherwise, it is assumed to be the last workFlowNextButton
 *
 * @param {object} validationSchema schema that is returned from cy.currentProtocolWorkflow()
 * @param {object} testEmptyField object contains an array that contains the 2nd and/or 3rd arguments for testEmptyField
 * @returns {Cypress.Chainable} the subject
 *
 * @example
 * // schema is usually gotten earlier in the tests
 * let rules
 * cy.documentation().currentProtocolWorkflow().then((model) => rules = model)
 *
 * // then use the rules object to pass into any validateField call
 * cy.get('field_name').validateField()
 * @example
 * // can inject it into any exisiting chains
 * cy.get('field_name').type('blah blah')
 * // into
 * cy.get('field_name').validateField( ['workflowNextButton', -1]).type('blah blah')
 * or
 * cy.get('field_name').validateField().type('blah blah')
 */
Cypress.Commands.add(
  'validateField',
  { prevSubject: true },
  (
    subject,
    testEmptyField = null,
    isComponent = false,
    isChildOf = null,
  ) => {
    const log = false
    const validationSchema = global.currentProtRules
    const subjectDataCy = subject.attr('data-cy')
    const subjectAriaLabel = subject.attr('aria-label')
    Cypress.log({
      name: 'validateField',
      message: log ? 'with logging' : 'without logging (set log to true for debugging)'
    })
    // TODO get custom rules from webapp and apply it to its respective field? or perhaps that should remain seperate for the spec file to handle
    //      Maybe need to change source from the docs to modelConfig?

    // TODO pattern(for Dates?), x-paratoo-regex-message?, x-paratoo-regex-description?, maxItems?, format: date-time
    //      plot-description-standard is a good starting point for some of these things. Although, depending on how
    //      unique the cases are, it may just be worth ignoring

    // TODO optimise the recursion to that it has some kind of exit conditions. Could get very slow on some systems or just on big objects.
    //      It also assumes that any duplicate keys are the same. However, it hasnt been double checked to see if there are different fields.
    function getFields(obj, targetKey, isChildOf = null) {
      let results = []
      if (isChildOf) {
        obj = obj[isChildOf]
      }
      forOwn(obj, (value, key) => {
        if (key == targetKey) {
          if (Array.isArray(value)) {
            results.push(...value)
          } else {
            results.push(value)
          }
        }
        if (isObject(value)) {
          results = results.concat(getFields(value, targetKey))
        }
      })
      return results
    }
    // cy.wraps to put the recusion into a Cypress.Promise to properly enable .then() for synchronous code that doesnt break cyrpess' asynchronous behaviour
    cy.wrap(getFields(validationSchema, 'required', isChildOf), { log: log })
      .then((requiredFields) => {
        cy.wrap(getFields(validationSchema, subjectDataCy, isChildOf), {
          log: log,
        }).then((validationFields) => [requiredFields, validationFields[0]])
      })
      .then(([requiredFields, validationFields]) => {
        log && cy.log('fields for ' + subjectDataCy, requiredFields, validationFields)
        // -- here's where we get a little silly --

        const toValidateWith = {} // object for testToFailValues

        // TODO dropdown test and compatability
        //      - selectFromDropDown may need to become an optional child, using the jquery subject for its cy.get
        //      - lists are sometimes very long, so im not sure how checking will go.
        //      - test length? initials from the enum?

        if (
          requiredFields.includes(subjectDataCy) ||
          requiredFields.includes(lowerCase(subjectAriaLabel))
        ) {
          Cypress.log({
            name: '-required',
            message: true
          })
          // TODO handle pre-filled forms properly. Just empties then currently, which may cause problems in some protocols
          // needs to know if it uses workflowNext or done
          let fieldLabel
          if ('x-paratoo-rename' in validationFields) {
            fieldLabel = validationFields['x-paratoo-rename']
          } else if (subjectAriaLabel) {
            fieldLabel = subjectAriaLabel.split('(')[0].replace('*', '').trim()
          } else {
            fieldLabel = startCase(subjectDataCy).trim()
          }
          log && cy.log(fieldLabel)

          if (testEmptyField) {
            cy.testEmptyField(
              `This field is required`,
              ...testEmptyField,
            )
            cy.testEmptyField(
              `${fieldLabel}`,
              ...testEmptyField,
            )
          } else {
            cy.wrap(subject, { log: log })
              .nearest()
              .then((btn) => {
                cy.dismissPopups().wait(200, { log: log })
                cy.get(btn, { log: log }).click({ log: log })
                cy.get('.q-notification', { timeout: 15000, log: log }).eq(-1, { log: false }).as('popup')
                cy.get('@popup', { log: log }).should(
                  'contain.text',
                  `${fieldLabel}`,
                )
                cy.get('@popup', { log: log }).should(
                  'contain.text',
                  `This field is required`,
                )
                cy.get('@popup', { log: log }).contains('Dismiss', { log: log }).click({ log: log })
              })
          }

          // TODO This will need to be customised to account for a few cases, such as dropdowns
          // if q-select or role="combobox"

          // if q-input has a child element input

          if (
            (subject.find('[role="combobox"] ').length > 0 ||
              subject.attr('role') == 'combobox') &&
            !isComponent
          ) {
            // TODO clicks the circular X button if it exist. Don't know if this works
            if (subject.closest('.q-field')) {
              if (
                subject.closest('.q-field').find('.q-btn:contains(cancel)')
                  .length > 0
              ) {
                cy.get(subject, { log: log }).parents('.q-field', { log: log }).contains('cancel', { log: log }).click({ log: log })
              }
            }
            cy.get(subject, { log: log })
              .click({ log: log })
              // .wait(100)
              .then((e) => {
                // if e is not an input, that means the data-cy is not on the input element and it is likely a child, so we will need
                // find the input in order to call .blur() successfully. Otherwise, calling type('') will focus
                // the child input, so blur() will have the wrong subject
                if (e.is('input')) {
                  // is focused
                  cy.get(e, { log: log }).type('{esc}', { log: log }).blur({ log: log })
                } else if (e.find('input').length > 0) {
                  // e will not have focus, as focus is on the elements child input element
                  cy.get(e, { log: log }).type('{esc}', { log: log }).find('input', { log: log }).blur({ log: log })
                }
                cy.wait(500) // long time so the runner can catch up to the fact we have left the input 

                // goofy ahh species list
                cy.get(subject).parents('.q-field').then(qField => {
                  if (qField.find(':contains(Label format: Canonical Name [taxon rank])').length > 0) {
                    cy.log('this is a species list')
                    cy.get(subject).then(el => {
                      if (el.attr('aria-expanded')) {
                        // close it just to be sure. Hopefully pressing esc doesn't mess with anything.
                        cy.log('is expanded')
                        cy.get(subject).type('{esc}').blur()
                      }
                    })
                  }
                })
                cy.get(subject, { log: log }).parents('.q-field', { log: log }).contains('This field is required',).should('exist')
                // .should('contain.text', 'This field is required')
              })
          } else if (!isComponent) {
            cy.get(subject, { log: log })
              .click({ log: log })
              .blur({ log: log })
              .then((e) => {
                cy.get(e, { log: log }).parents('.q-field', { log: log }).contains('This field is required').should('exist')
                cy.get(e, { log: log }).click({ log: log }).clear({ log: log })
              })
          }
        }

        if (has(validationFields, 'x-paratoo-unit')) {
          Cypress.log({
            name: '-x-paratoo-unit',
            message: validationFields['x-paratoo-unit']
          })

        }
        // if this is a component, we don't need to check its type
        if (isComponent) return

        // type

        if (has(validationFields, 'type')) {
          cy.dismissPopups().wait(100)
          Cypress.log({
            name: '-field expects',
            message: validationFields.type
          })
          if (validationFields.type === 'string') {
            if (typeof subject.val() === 'string') {
              // is a number
              // can type anything???
              // will be string if using enums (luts)
              if (!has(validationFields, 'enum')) {
                cy.get(subject, { log: log })
                  .type('427829. This should work. Hurray for us', { log: true })
                  .should(
                    'contain.value',
                    '427829. This should work. Hurray for us',
                  )
                  .clear({ log: log })
              }
              // TODO check the luts?
              //      - just chekcing if required should do
            }
          } else if (validationFields.type === 'number') {
            if (!isNaN(subject.val())) {
              // is a number
              // shouldnt be able to type words, only numbers
              cy.get(subject, { log: log })
                .clear({ log: log })
                .type('This shouldnt work', { log: log })
                .should('not.have.value', 'This shouldnt work')
              cy.get(subject, { log: log }).clear({ log: log }).type('5', { log: log }).should('have.value', '5')
            }
          } else if (validationFields.type === 'boolean') {
            if (
              subject.find('[class$="--truthy"]').length > 0 ||
              subject.find('[class$="--falsy"]').length > 0 ||
              subject.is('[class$="--truthy"]').length > 0 ||
              subject.is('[class$="--falsy"]').length > 0
            ) {
              // just ticking should prove it s boolean, i guess? toggle has an assertion after tick.
              cy.dataCy(subjectDataCy, { log: log }).toggle(true)
              cy.dataCy(subjectDataCy, { log: log }).toggle(false)
            }
          }
        }

        if (has(validationFields, 'enum')) {
          // these are luts that should be in a dropdown
          // TODO check the number is right? Not sure how to test this
          // cy.log(subjectDataCy + ' must use enum', validationFields.enum)
        }

        //format
        if (has(validationFields, 'format')) {
          Cypress.log({
            name: subjectDataCy + ' must have format',
            message: validationFields.format
          })
          if (validationFields.format === 'integer') {
            cy.get(subject, { log: log }).clear({ log: log }).type('3.69', { log: log })
            cy.wrap(Number(subject.val()) % 1 === 0).should('be.true')
          }
          if (validationFields.format === 'float') {
            cy.get(subject, { log: log })
              .clear({ log: log })
              .type('4.333', { log: log })
              .then((e) => {
                const value =
                  Number(e.val()) == 0 ? Number(e.text()) : Number(e.val())
                cy.wrap(Number.isInteger(value) || value % 1 !== 0, { log: log }).should(
                  'be.true',
                )
              })
          }
          // TODO check other types that appear
          cy.get(subject, { log: log }).clear({ log: log })
        }

        // min and max
        if (has(validationFields, 'minimum')) {
          toValidateWith[validationFields.minimum - 1] = {
            chainer: 'contain.text',
            value: `Minimum: ${validationFields.minimum}`,
          }
        }
        if (has(validationFields, 'maximum')) {
          toValidateWith[validationFields.maximum + 1] = {
            chainer: 'contain.text',
            value: `Maximum: ${validationFields.maximum}`,
          }
        }

        if (Object.keys(toValidateWith).length > 0) {
          cy.testToFailValues(subjectDataCy, { ...toValidateWith }, 0, { log: log })
          // breaks if not using lazy loading on any empty fields
        }
      })
      // yields the cy.get() subject to a child, if needed. Allows the you to call validate, then continue with existing commands
      .then(() => {
        return subject
      })
  },
)


const defaultValidateFieldOptions = {
  testEmptyField: null,
  isComponent: false,
  isChildOf: null
}
Cypress.Commands.add(
  'getAndValidate',
  { prevSubject: 'optional' },
  (subject, value, validateFieldOptions = {}, options, index = 0) => {
    const mergedOptions = {
      ...defaultValidateFieldOptions,
      ...validateFieldOptions,
    }
    if (global.currentProtRules) {
      return cy
        .get(
          // assumes anything that doesnt start with . or is enclosed in brackets is a data-cy 
          /^\.+|\[.*?\]$/.test(value) ? value : `[data-cy="${value}"]`,
          Object.assign({ withinSubject: subject }, options),
        )
        .eq(index)
        .validateField(
          mergedOptions.testEmptyField,
          mergedOptions.isComponent,
          mergedOptions.isChildOf,
        )
    }
    return cy.get(
      `[data-cy=${value}]`,
      Object.assign({ withinSubject: subject }, options),
    )
  },
)

/**
 * This command is used to load a local store.
 * It first checks if the provided store name is valid.
 * Then it retrieves all local storage and parses the store with the provided name.
 * The command can also accept a callback function that will be called with the store and the subject as arguments.
 * Can be chained off of itself. When chained off itselt, the yielded result will be the 2nd argument of the child.
 *
 * @param {string} storeName - The name of the store to load.
 * @param {Function} [callback] - A callback function that will be called with the store and the subject as arguments.
 * @returns {Cypress.Chainable} The command returns a Cypress Chainable object. If a callback function is provided, the command will return the result of the callback function. Otherwise, it will return the store.
 * @example
 * // Load a local store
 * cy.loadLocalStore('auth').then((store) => {
 * // Your logic here
 * })
 * @example
 * // Load a local store and call a callback function
 * cy.loadLocalStore('auth', (store, subject) => {
 * // Your callback logic here
 * })
 */
Cypress.Commands.add(
  'loadLocalStore',
  { prevSubject: 'optional' },
  (subject, storeName, callback) => {
    console.log(`Loading store ${storeName}`)
    //TODO refactor this command (or make new command that wraps this one) to allow returning multiple stores (else we end up in messy .then() chaining while we load each store)

    const stores = ['bulk', 'auth', 'apiModels', 'ephemeral', 'dataManager']

    const nonPersistStores = ['ephemeral']

    //TODO dynamically build this based on which stores have been defined to use Dexie in src/stores/persist (so that when we add new stores we don't have to modify this code)
    const storesIndexDb = ['apiModels', 'auth']
    if (
      !stores.some((e) =>
        storeName.includes(e),
      )
    )
      throw new Error(storeName + ' is not a store')

    cy.window()
      .then(($win) => {
        cy.getAllLocalStorage({ log: false }).then((localStorage) => {
          let store = null

          if (
            storesIndexDb.includes(storeName) ||
            nonPersistStores.includes(storeName)
          ) {
            store = cloneDeep($win?.[`${storeName}Store`]?.$state || {})
          } else {
            //don't need to clone as localStorage doesn't store references
            store = JSON.parse(
              localStorage[Object.keys(localStorage)[0]]?.[storeName],
            )
          }

          if (callback) {
            return callback(store, subject)
          } else {
            return store
          }
        })
      })
  },
)

/**
 * Recursively loads multiple stores and returns an object mapping store names to loaded stores.
 *
 * @param {Array} stores - Array of objects containing storeName and callback properties
 * @param {Object} [allStores={}] - Optional parameter to accumulate loaded stores across recursive calls
 * @returns {Object} An object mapping store names to loaded stores
 * 
 * @example
 *     cy.loadManyStores(
      [
        {
          storeName: 'auth',
          callback: (auth => {
            // optional return if only needing a certain value 
            return auth.projAndProt
          }) 
        },
        {
          storeName: 'apiModels',
        },
      ],
    ).then((storesStore) => {
      stores.authStore
      stores.apiModelsStore
    })
    // or 
    // They will be returned in the order you list them in the original argument 
    .then(({authStore, apiModelsStore}) => {
      authStore
      apiModelsStore
    })
 */
Cypress.Commands.add(
  'loadManyStores',
  { prevSubject: 'optional' },
  //`stores` will be array of objects, where each object has a `storeName` and `callback`
  (subject, stores, allStores = {}) => {
    const currentStore = stores.pop()

    // recursion is an easy way to do this a dynamic amount of times, while maintaining clear subject chaining
    cy.loadLocalStore(currentStore.storeName, currentStore.callback ?? null)
      .then((store) => {
        allStores[`${currentStore.storeName}Store`] = store
        if (stores.length > 0) {
          cy.loadManyStores(stores, allStores)
        } else {
          return allStores
        }
      })
  },
)

/**
 * This command is used to get the current protocol and its associated workflow after getting the documentation.
 * It first loads the 'auth' and 'apiModels' local stores, then it retrieves the protocol id from the 'auth' store and the workflow from the 'apiModels' store.
 * It then maps over the workflow to get the properties of each model in the workflow.
 * The command can also accept a callback function that will be called with the models, context, and workflow as arguments.
 *
 * @param {Function} [callback] - A callback function that will be called with the models, context, and workflow as arguments.
 * @returns {Cypress.Chainable} The command returns a Cypress Chainable object. If a callback function is provided, the command will return the result of the callback function. Otherwise, it will return the models.
 * @example
 * // Get the current protocol and its associated workflow
 * cy.documentation().currentProtocolWorkflow().then((models) => {
 *  // Your logic here
 * })
 * @example
 * // Get the current protocol and its associated workflow and call a callback function. can use one or both.
 * cy.documentation().currentProtocolWorkflow((models, [documentation, docStore]) => {
 *  // Your callback logic here
 * })
 */
Cypress.Commands.add(
  'currentProtocolWorkflow',
  { prevSubject: 'optional' },
  (subject, callback) => {
    cy.loadLocalStore(
      'auth',
      (authStore) => authStore.currentContext.protocol.id,
    )
      .loadLocalStore('apiModels', (apiModels, context) => {
        const currProtocol = apiModels.models.protocol.find(
          (protocol) => protocol.id == context,
        )
        // cy.log(currProtocol)
        return [currProtocol?.['workflow'], context]
        //
      })
      .then(([workflow, context]) => {
        const models = {}
        workflow.forEach((e) => {
          // [0] because subject is an array
          models[e.modelName] = cloneDeep(
            subject[0].components.schemas[
              `${upperFirst(camelCase(e.modelName))}Request`
            ].properties,
          )
        })
        if (callback) {
          return callback(models, subject)
        } else {
          return models
        }
      })
  },
)

/**
 * Retrieves the documentation store from the window object and returns the documentation and the store as an array.
 *
 * @returns {Promise<Array>} A promise that resolves to an array containing the documentation and the store.
 *
 * @example
 * // Retrieve the documentation store
 * cy.documentation().then(([documentation, store]) => {
 *  // Do something with the documentation and the store
 * })
 * @example
 * cy.documentation((documentation) => {// do something})
 * cy.documentation((documentation, store) => {// do something})
 */
Cypress.Commands.add('documentation', (callback, { log = false } = {}) => {
  cy.window({ log: log })
    .its('documentationStore')
    .then((documentationStore) => {
      return documentationStore.getDocumentation().then((documentation) => {

        if (callback) {
          console.log('documentation callback')
          return callback(documentation, documentationStore)
        } else {
          console.log('documentation no callback')
          return [documentation, documentationStore]
        }
      })
    })
})

/**
 * Checks the VUE_APP_MODE_OF_OPERATION env is set to the desired state. Useful for tests
 * such as cover PI that expect less points per transect during tests.
 *
 * This should not be called at the top of the test, as the app probably is not
 * initialised and thus `modeOfOperation` was probably not set on the `window` object yet
 *
 * @param {Number} [desiredMode] the desired mode of operation - 0 for dev, 1 for prod.
 * Default 0 as we typically test in this mode
 */
Cypress.Commands.add('checkModeOfOperation', (desiredMode = 0) => {
  cy.window().then(($win) => {
    cy.log(`modeOfOperation: ${$win.constants.modeOfOperation}`)
    //logic grabbed from src/misc/constants - cannot import the const itself so parse
    //the env the same as the app does
    let parsed = null
    //each subsequent case squashed previous, which is fine because the order of squashing prioritieses most important
    let errMsg = null
    try {
      parsed = Number.parseInt($win.constants.modeOfOperation)
    } catch (err) {
      errMsg =
        'Tried to parse the VUE_APP_MODE_OF_OPERATION env but was not provided number'
    }
    if (![0, 1].includes(parsed)) {
      errMsg = `Value provided to VUE_APP_MODE_OF_OPERATION is invalid. Provided '${$win.constants.modeOfOperation}', but expected '0' or '1'`
    }
    if (parsed !== desiredMode) {
      errMsg = `Value provided to VUE_APP_MODE_OF_OPERATION (${$win.constants.modeOfOperation}) not the desired mode (${desiredMode})`
    }
    if (errMsg !== null) {
      let msg = `Issue with VUE_APP_MODE_OF_OPERATION not set properly for tests - tests might not work as expected. Issue caused by: ${errMsg}`
      throw new Error(msg)
    }
  })
})


Cypress.Commands.add('dumpState', ({ fileName = 'state.json' }) => {
  cy.loadManyStores(
    [
      {
        storeName: 'bulk',
      },
      {
        storeName: 'dataManager',
      },
    ],
  )
    .then((stores) => {
      const path = `test/cypress/stateDumps/${fileName}${fileName.includes('.json') ? '' : '.json'
        }`
      cy.log(`Dumping store state to file path: ${path}`)
      cy.writeFile(path, {
        dataManager: stores.dataManagerStore,
        bulk: stores.bulkStore,
      })
    })
})

//TODO unify with non-offline basal test logic (this logic was broken-out while the non-offline test was being extended with field constraints)
Cypress.Commands.add('basalWedgeSamplingPoint', (
  samplingPoint,
  vouchers,
  testToFail = true,
) => {
  function pickRandomVouchers() {
    const count = Math.floor(Math.random() * (vouchers.length - 1)) + 1
    const shuffled = vouchers.sort(() => 0.5 - Math.random())
    return shuffled.slice(0, count)
  }
  // select sampling point
  // the command selectFromDropdown doesn't work really well words have matching characters
  // eg: 'North' and 'North West'
  function selectPoint(point) {
    if (testToFail) cy.testEmptyField('Field: "Basal Sweep Sampling Point": This field is required', 'done', -1)
    cy.dataCy('basal_sweep_sampling_point').click().wait(50)
    cy.withinSelectMenu({
      persistent: true,
      fn: () => {
        cy.get('.q-item[role=option]').as('dropdownOptions')
        cy.get('@dropdownOptions')
          .filter((index, element) => {
            return Cypress.$(element).text() === point
          })
          .eq(0)
          .scrollIntoView()
          .click()
          .wait(200)
      },
    })
  }

  switch (samplingPoint) {
    case 'Northwest':
      selectPoint(samplingPoint)
      cy.selectFromDropdown('floristics_voucher', pickRandomVouchers(vouchers))
        .type('{esc}')    //de-select dropdown
      if (testToFail) cy.testEmptyField('Field: "Basal Area Factor": This field is required', 'done')
      cy.selectFromDropdown('basal_area_factor', '0.1')
      if (testToFail) cy.testEmptyField('Field: "In Tree": This field is required', 'done')
      cy.get('[data-cy="in_tree"]').type('1')
      if (testToFail) cy.testEmptyField('Field: "Borderline Tree": This field is required', 'done')
      cy.get('[data-cy="borderline_tree"]').type('4')
      if (testToFail) cy.testEmptyField('In and Borderline counts together should be greater than or equal to 7', 'done')
      cy.get('[data-cy="borderline_tree"]').type('6')
      cy.get('[data-cy="done"]').click().wait(50)
      break
    case 'North':
      // in tree is required field
      if (testToFail) cy.testEmptyField('Missing sampling point(s): N, NE, E, SE, S, SW, W, C', 'workflowNextButton', null, 0)
      cy.get('[data-cy="addEntry"]').eq(0).click().wait(50)
      selectPoint(samplingPoint)
      cy.get('[data-cy="group_species"]').click()
      // grouped species 1
      cy.selectFromDropdown('floristics_voucher', pickRandomVouchers(vouchers))
        .type('{esc}')    //de-select dropdown
      cy.selectFromDropdown('basal_area_factor', '0.1')
      cy.get('[data-cy="in_tree"]').type('7')
      cy.get('[data-cy="done"]').eq(0).click()
      cy.get('[data-cy="borderline_tree"]').type('6')
      cy.get('[data-cy="done"]').eq(0).click()
      break
    default:
      cy.get('[data-cy="addEntry"]').eq(0).click().wait(50)
      selectPoint(samplingPoint)
      cy.get('[data-cy="group_species"]').click()
      // grouped species 1
      cy.selectFromDropdown('floristics_voucher', pickRandomVouchers(vouchers))
        .type('{esc}')    //de-select dropdown
      cy.selectFromDropdown('basal_area_factor', '0.1')
      cy.get('[data-cy="in_tree"]').type('7')
      cy.get('[data-cy="done"]').eq(0).click()
      cy.get('[data-cy="borderline_tree"]').type('6')
      cy.get('[data-cy="done"]').eq(0).click()
      break
  }
})

Cypress.Commands.add('clearDropdown', { prevSubject: 'element' }, (subject) => {
  return cy.get(subject).parents('.q-field').contains('cancel').click().then(() => subject)
})

Cypress.Commands.add('checkFireSubmoduleAuthContext', () => {
  cy.loadLocalStore('auth')
    .then((authStore) => {
      cy.loadLocalStore('bulk')
        .then((bulkStore) => {
          cy.loadLocalStore('apiModels')
            .then((apiModelsStore) => {
              const foundSubmoduleProtocol = bulkStore.doingSubmoduleMeta.find(
                (element) => {
                  return element.parentModuleProtocolId == authStore.currentContext.protocol.id
                },
              )
              console.log('foundSubmoduleProtocol:', foundSubmoduleProtocol)

              const fireCollection = bulkStore.collections[foundSubmoduleProtocol.submoduleProtocolId]

              cy.wrap(fireCollection['fire-survey'].survey_metadata.survey_details)
                .should('have.any.key', 'submodule_protocol_id')

              const fireSurveyProtocolEntry = apiModelsStore.models.protocol.find(
                p => p.id == foundSubmoduleProtocol.submoduleProtocolId
              )
              console.log('fireSurveyProtocolEntry:', fireSurveyProtocolEntry)

              cy.wrap(
                fireCollection['fire-survey'].survey_metadata.survey_details.submodule_protocol_id
              ).should('eq', fireSurveyProtocolEntry.identifier)
            })
        })
    })
})

/**
 * recursively traverses the dom tree to find the first match of a given selector by looking at children of each parent. It will return the last element if multiple are found.
 * If your selector is just a plain name it will be defaulted to [data-cy="your_selector"]. Otherwise it should work with all selectors if you give it one.
 * @param {[string | Array.<string>]} selector - The selector or array of selectors to find the nearest element.
 * 
 * @param {number} [triesForIndex=3] Number of attempts to find the index initiall unless a backup has been supplied.
 * @param {boolean} [log=false] Whether to log the commands at each step. HUGE performance hits if true
 * @returns {Cypress.Chainable<JQuery<HTMLElement>>} Returns the found element.
 * @throws {Error} Will throw an error if the nearest element is not found and it reaches the top of the the dom tree.
 * @example
 * // Get the nearest element with the data-cy attribute of 'my-element'
 * cy.get('body').nearest('[data-cy="my-element"]');
 */
Cypress.Commands.add('nearest', { prevSubject: 'element' }, function (subject, selector = ['done', 'workflowNextButton'], { triesForIndex = 3, log = false } = {}) {
  // just incase an non-array string is added we just put it in one
  if (!Array.isArray(selector)) selector = [selector]

  // TODO I imagine this could use some optimisation
  // im not married to the name but it is not super important
  const recurse = function (subject, count = 0) {
    cy.get(subject, { log: log })
      .parent({ log: log })
      .then(parent => {
        const newCount = count + 1

        // if we go up far enough that we can even see the workflownextbutton, we should probably click it now
        const index = parent.hasClass('q-stepper__step-inner') ? 1 : 0

        const transformedSelector = /^\[.*?=".*"\]$/.test(selector[index]) ? selector[index] : `[data-cy="${selector[index]}"]`

        cy.get(parent, { log: log })
          .children({ log: log })
          .then(children => {
            const matching = children.find(transformedSelector)

            if (matching.length == 0 || matching.length == undefined || matching.length == null) {
              recurse(cy.$$(subject).parent(), newCount)
            }
            else {
              log && Cypress.log({
                name: 'nearest',
                message: [transformedSelector]
              })
              // its position in the dom is very important to where it is in the array. -1 is safe... probably
              return cy.get(matching[matching.length - 1], { log: log })
            }
          })
      })
  }
  try {
    recurse(subject)
  }
  catch (e) {
    throw new Error('probably reached the top top most DOM element, nothing else to see', e)
  }
})


Cypress.Commands.addQuery('getNoAssert', function (selector, options = {}) {
  const log = options.log !== false && Cypress.log({ timeout: options.timeout })
  this.set('timeout', options?.timeout || Cypress.config('defaultCommandTimeout'))
  let date = new Date()

  return () => {
    let $el = cy.$$(selector)
    log &&
      cy.state('current') === this &&
      log.set({
        $el,
        consoleProps: () => {
          return {
            Yielded: $el?.length ? $el[0] : '--nothing--',
            Elements: $el != null ? $el.length : 0,
          }
        },
      })
    // console.log(new Date() - date, this.get('timeout'))
    if (new Date() - date > (this.get('timeout') - 400)) { // arbitrary reduction to account for ms differences when calculating the time
      return true
    }
    // console.log($el)
    return $el
  }
})

/**
 * Wrapper for getNoAssert to handle it returning true instead of an object.
 * @param {string | Array.<string>} selector - The selector or array of selectors to find the nearest element.
 * @param {object} [options] - Options for the command.
 *  
 * @returns {Cypress.Chainable<JQuery<HTMLElement>>} Returns found element or an empty jquery object. 
 */
Cypress.Commands.add('$get', (selector, options = {}) => {
  return cy.getNoAssert(selector, options).then((e) => {
    // if it returns true
    if (e === true && !Object.hasOwn(e, 'length')) {
      return cy.$$()
    }
    return e

  })
})

Cypress.Commands.add('isUnique', { prevSubject: 'optional' }, function (subject, proto, valueToCheck = undefined) {
  // TODO needs to be extended to handle nested paths for child observations when looking for the field. 
  // Lodash may have something, and recursion is always an option
  Cypress.log({
    name: 'isUnique'
  })
  const log = false
  cy.wrap(null, { log: log })
    .then(function () {
      if (typeof valueToCheck === 'function') {
        valueToCheck() // needs to be a single cypress chain
          .as('value')
      }
      else if (typeof valueToCheck === 'string') {
        cy.wrap(valueToCheck, { log: log })
          .as('value')
      } else {
        cy.wrap(subject?.val(), { log: log })
          .as('value')
      }
    })
    .then(function () {
      cy.wrap(subject.attr('data-cy')).as('path')
      cy.wrap((proto.endsWith('s') ? proto.substring(0, proto.length - 1) : proto).toLowerCase().replaceAll(' ', '-')).as('protocol')
    })
    .then(() => {
      log && cy.log(this.protocol, this.path, this.value)
    })
    .then(() => {
      let isUnique = true
      cy.loadLocalStore('auth')
        .its('currentContext.protocol.id')
        .then(function (projectContext) {
          // TODO less duplication
          // compare others in bulk
          cy.loadLocalStore('bulk')
            .then(function (bulk) {
              let keys = ObjectComparitor.allKeysWithPath(bulk.collections[projectContext]?.[this.protocol])
                .filter(e => e.key.includes(this.path))
                .map(e => Cypress._.get(bulk.collections[projectContext]?.[this.protocol], e.path))
              log && console.log('eeeeeeea-bulk', keys, this.value)
              if (keys?.find((e) => e === this.value)) {
                log && console.log('is dupe in bulk', keys, this.value)
                isUnique = false
              }
            })
          // check apiModelstore (historical data) for the id
          cy.loadLocalStore('apiModels')
            .then(function (apiModels) {
              let keys = ObjectComparitor.allKeysWithPath(apiModels.models?.[this.protocol])
                .filter(e => e.key.includes(this.path))
                .map(e => Cypress._.get(apiModels.models?.[this.protocol], e.path))
              log && console.log('eeeeeeea-apiModels', keys, this.value)
              if (keys?.find((e) => e === this.value)) {
                log && console.log('is dupe in apiModels', keys, this.value)
                isUnique = false
              }
            })
          // check the pending queue
          cy.loadLocalStore('dataManager')
            .then(function (dataManager) {
              let keys = ObjectComparitor.allKeysWithPath(dataManager.publicationQueue?.find((e) => e.collection[this.protocol]))
                .filter(e => e.key.includes(this.path))
                .map(e => Cypress._.get(dataManager.publicationQueue?.find((e) => e.collection[this.protocol], e.path)))
              log && console.log('eeeeeeea-dataManager', keys, this.value)
              if (keys?.find((e) => e === this.value)) {
                log && console.log('is dupe in queue', keys, this.value)
                isUnique = false
              }
            })
        })
        .then(() => {
          return isUnique
        })
    })
})

Cypress.Commands.add('pickFile', function (fileName, index = 0) {
  cy.dataCy('uploadFile').eq(index).selectFile(`test/cypress/fixtures/${fileName}`)
  cy.dataCy('uploadFile').eq(index).siblings(`:contains(${fileName})`).should('exist')
  // cy.dataCy('uploadFile').parents('.q-field').find('[role="button"]:contains(cancel)').should('exist')
  cy.dataCy('file-loaded', { timeout: 20000 }).should('exist')
})

Cypress.Commands.add('goOffline', function () {
  cy.log('**go offline**')
    .then(() => {
      return Cypress.automation('remote:debugger:protocol', {
        command: 'Network.enable',
      })
    })
    .then(() => {
      return Cypress.automation('remote:debugger:protocol', {
        command: 'Network.emulateNetworkConditions',
        params: {
          offline: true,
          latency: -1,
          downloadThroughput: -1,
          uploadThroughput: -1,
        },
      }).then(() => {
        cy.window().then(($win) => {
          cy.wrap(null, { timeout: 60000 }).should(() => {
            expect($win.ephemeralStore.networkOnline).to.equal(false)
          })
        })
      })
    })
})

Cypress.Commands.add('goOnline', function () {
  // disable offline mode, otherwise we will break our tests :)
  cy.log('**go online**')
    .then(() => {
      // https://chromedevtools.github.io/devtools-protocol/1-3/Network/#method-emulateNetworkConditions
      return Cypress.automation('remote:debugger:protocol', {
        command: 'Network.emulateNetworkConditions',
        params: {
          offline: false,
          latency: -1,
          downloadThroughput: -1,
          uploadThroughput: -1,
        },
      })
    })
    .then(() => {
      return Cypress.automation('remote:debugger:protocol', {
        command: 'Network.disable',
      }).then(() => {
        const waitTime = Number(Cypress.env('NETWORK_RECHECK_INTERVAL'))
        cy.wait(waitTime)
        cy.window().then(($win) => {
          //network change result in immediate detection, but the check for Core
          //being up is only every 1 minute, so even if we're online, and can take
          //a moment for full functionality to return
          cy.wrap(null, { timeout: 120000 }).should(() => {
            expect($win.ephemeralStore.networkOnline).to.equal(true)
            expect($win.ephemeralStore.coreIsAvailable).to.equal(true)
          })
        })
      })
    })
})

Cypress.Commands.add('setNetworkConditions', ({
  latency = -1,
  downloadThroughput = -1,    //bytes/sec
  uploadThroughput = -1,    //bytes/sec
}) => {
  cy.log(`**slow network** - latency=${latency}. downloadThroughput=${downloadThroughput}. uploadThroughput=${uploadThroughput}`)
    .then(() => {
      return Cypress.automation('remote:debugger:protocol', {
        command: 'Network.emulateNetworkConditions',
        params: {
          offline: false,
          latency: latency,
          downloadThroughput: downloadThroughput,
          uploadThroughput: uploadThroughput,
        },
      })
    })
})

Cypress.Commands.add(
  'getStoreReference',
  ({ store, stateKey = null }) => {
    let returnRef = null
    cy.window()
      .then(($win) => {
        if (!stateKey) {
          returnRef = $win?.[store]
        } else {
          returnRef = $win?.[store]?.[stateKey]
        }

        return returnRef
      })
  }
)

Cypress.Commands.add(
  'checkHistoricalDataInDexie',
  ({ table, syncedSurveyQueuedCollectionIdentifier, callbackForCustomAssertions }) => {
    cy.log(`Checking historical data is in Dexie for table=${table} and syncedSurveyQueuedCollectionIdentifier=${syncedSurveyQueuedCollectionIdentifier}`)
    cy.window()
      .then(($win) => {
        cy.wrap($win.historicalDataManagerDb.getAllTableData({ table: table })
          .then((dexieData) => {
            console.log('dexieData:', dexieData)
            const historicalSurvey = dexieData.find(
              rd => rd.queuedCollectionIdentifier === syncedSurveyQueuedCollectionIdentifier
            )
            return historicalSurvey
          })).should(hs => {
            // TODO Make these assertions be more robust
            expect((typeof hs === 'object' && !Array.isArray(hs))).to.be.true
            expect(Object.keys(hs)).to.have.length.greaterThan(0, 'Checking returned object has keys and values')
          })
          .then((hs) => {
            return hs
          })
      })
      .then((historicalSurvey) => {
        if (historicalSurvey?.window?.window) {
          throw new Error(`historicalSurvey value is the window object :(\n\nExpected __historicalSurvey__ to be from dexie table: __${table}__`)

        } else {
          // FIXME historicalSurvey sometimes evaluates to the window object. Possible fix could be leveraging Cypress.Promise.
          cy.log(`historicalSurvey: ${JSON.stringify(historicalSurvey)}`)
          // incase a more detailed assertion is needed
          if (callbackForCustomAssertions)
            callbackForCustomAssertions(historicalSurvey)
          else
            cy.wrap(historicalSurvey)
              .should('not.be.undefined')
              .and('not.be.null')
        }
      })
  }
)

Cypress.Commands.add(
  'countHistoricalDataInDexie',
  ({ table, expectedCount }) => {
    cy.log(`Counting historical data for table=${table}. Expect count of ${expectedCount}`)
    cy.getHistoricalDataInDexieCount({ table: table })
      .then((count) => {
        try {
          expect(count).to.eq(expectedCount)
        }
        catch {
          throw new Error(`Dexie ${table} count should equal ${expectedCount}, but instead is ${count}`)
        }
      })
  }
)

Cypress.Commands.add(
  'getHistoricalDataInDexieCount',
  ({ table }) => {
    cy.window({ log: false })
      .then(($win) => {
        let count = $win.historicalDataManagerDb.countHistoricalDmTable({ table: table })
          .then((count) => {
            return count
          })
        return count
      })
  }
)


Cypress.Commands.add('parseCSV', { prevSubject: 'optional' }, (csvContent) => {
  // Split the content into lines
  
  const lines = csvContent.split('\n')

  // Extract headers from first line
  const headers = lines[0].split(',').map(header => header.trim())

  const rows = lines.slice(1).filter(line => line.trim() !== '')
    .map(row => {
      console.log('bbbb1', row)
      // Split the row while preserving quoted values
      const values = row.match(/(".*?"|[^",\s]+)(?=\s*,|\s*$)/g)
        .map(value => {
          // Remove quotes if present and trim
          return value.replace(/^"(.*)"$/, '$1').trim()
        })
      console.log('bbbb2', values)
      // Create object mapping headers to values
      return values.reduce((obj, value, index) => {
        obj[headers[index]] = value
        return obj
      }, {})
  })
  console.log('bbbb3', rows)

  // Create the base object with parsed data
  const baseObject = {
    headers,
    rows,
  }

  headers.forEach(header => {
    Object.defineProperty(baseObject, header, {
      get: () => baseObject.rows.map(row => row[header]),
      configurable: true,
      enumerable: true
    })
  })

  // Add properties for numeric indices
  rows.forEach((row, index) => {
    Object.defineProperty(baseObject, index.toString(), {
      get: () => row,
      configurable: true,
      enumerable: false
    })
  })

  return baseObject
})

registerCommands()

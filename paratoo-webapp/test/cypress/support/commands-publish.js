import { registerCommands } from '@quasar/quasar-app-extension-testing-e2e-cypress'
import { ObjectComparitor as oc, checkHistorical } from './command-utils'

const log = false

Cypress.Commands.add('publishCollections', (force, atSummary, afterQueueCollectionCb, options) => {
  cy.intercept('POST', /(\/api)((\/.*)(bulk|many))/).as('postInterceptBody')

  // Count dexie prior to submission for later in this command.
  cy.getHistoricalDataInDexieCount({ table: 'responses' })
    .then((c) => {
      cy.wrap(c).as('dexieResponsesCount')
    })
  cy.getHistoricalDataInDexieCount({ table: 'submitResponses' })
    .then((c) => {
      cy.wrap(c).as('dexieSubmitResponsesCount')
    })


  // === navigate the menu buttons to eventually publish === //
  cy.dismissPopups()
  if (atSummary !== true) cy.nextStep()
  cy.dataCy('workflowPublish').click({ force: force })
  cy.dataCy('ajv-error-0').should('not.exist')
  cy.dataCy('workflowPublishConfirm').click()

  let successMsg = 'Successfully queued the collection'
  if (options?.successMsg) {
    successMsg = options.successMsg
  }
  cy.get('.q-notification', { timeout: 360000 })
    .should('exist')
    .and('contain.text', successMsg)
  if (afterQueueCollectionCb !== null) afterQueueCollectionCb()

  // No queue to submit so no data to check
  if (options?.willNotQueue) {
    cy.log('Collection will not queue, skipping checks for response data')
    return cy.wrap([])
  }

  cy.dataCy('submitQueuedCollectionsBtn')
    .should('be.enabled')
    .click()
    .wait(100)
  cy.dataCy('submitQueuedCollectionsConfirm').click()

  // Uses the text "Upload progress (completed): 0 of 3" on the DOM to know when the app is done uploading.
  cy.get('.q-page-container').contains('Upload progress (completed):').invoke('text')
    .then(function (text) { return text.slice(-1) })
    .then(function (totalUploads) {
      cy.log('Uploads to intercept', totalUploads)

      cy.wrap([], { log: log }).as('interceptedRequests')

      // ignore swagger-build-info requests
      cy.intercept('GET', /^(?!.*swagger).*\/api(\/.*)$/, cy.spy().as('post-fetch-spy')).as('post-fetch')


      for (let i = 0; i < totalUploads; i++) {
        cy.wait('@postInterceptBody', { timeout: 300000 })
          .then(function (req) {
            if (req.response.statusCode !== 200) {
              throw new Error('Failed to sync queue', req.response.statusCode)
            }
            this.interceptedRequests.push(req)
          })
      }
    })
    .then(function () {
      // Once everything has uploaded and has had a success response from core, there should be a popup indicating
      // the protocol(s) that were uploaded. This popup contains the queued collection identifier that is used when 
      // migrating the data to dexie. It's also used for the summary page redirect.
      cy.get('.q-notification:contains(Successfully submitted all queued collections):visible')
        .find('span.block:contains(Open details)')
        .click()
        .then(() => {
          // sometimes a protocol might take slightly longer to be migrated, so wait a little longer
          cy.get('.q-linear-progress.text-teal-8.rounded-borders').should('not.exist')
          cy.wait(1000)
          cy.get('.q-card .q-pr-sm', { log: false })
            .then(elements => {
              const failedToGetIdentifiers = []
              let identifiers = []
              for (const element of elements) {
                const text = Cypress.$(element).text()
                const identifier = text.substring(
                  text.indexOf(": ") + 1,
                  text.lastIndexOf(")")
                ).replaceAll(' ', '')
                
                if ((!identifier || !identifier.length > 0) && text) {
                  failedToGetIdentifiers.push(text)
                }
                
                identifiers.push(identifier)
                
              }
              if (elements.length != identifiers.length && failedToGetIdentifiers.length) {
                throw new Error(`Missing ${Math.abs(elements.length - identifiers.length)} identifier(s) from:\n\n${failedToGetIdentifiers.join('\n')}`)
              }
              return identifiers
            })
        })
        .each((identifier) => {
          // Check that each table (responses & submitResponses) in dexie has been populated
          // *should* work with multiple uploaded protocols.
          for (const table of ['responses', 'submitResponses']) {
            // There should be a entry where the key is the queued collection identifier.
            // Its value doesn't matter right now — just that it does exist.
            cy.checkHistoricalDataInDexie({ table: table, syncedSurveyQueuedCollectionIdentifier: identifier })

            // Next we simply check if the number of entries actually went up in the table by 
            // comparing the tables before and after.
            cy.get(`@dexie${Cypress._.upperFirst(table)}Count`)
              .then((oldCount) => {
                cy.countHistoricalDataInDexie({ table: table, expectedCount: (this.interceptedRequests.length + oldCount) })
              })
          }
          // If the dexie data was populated correctly then localStorage.dataManager should have removed the data after migration.
          cy.wait(500)
          cy.getStoreReference({ store: 'dataManager' })
            .then(dataManager => {
              let failedAssertions = []
              for (const table of ['responses', 'submitResponses']) {
                console.log('checking', table, Cypress._.cloneDeep(dataManager[table]))
                try {
                  expect(dataManager[table].length).to.eq(0)
                } catch (e) {
                  failedAssertions.push(`__${table}__`)
                }
              }
              if (failedAssertions.length > 0) {
                throw new Error(failedAssertions.join(' & ') + ` ${failedAssertions.length > 1 ? 'are' : 'is'} populated when ${failedAssertions.length > 1 ? 'they' : 'it'} should have been migrated to Dexie`)
              }
            })
        })
    })
    .then(() => {
      cy.get('.q-dialog span.block:contains(Dismiss)').click()
      // After uploading, the app does some fetches to repopulate apiModels. It is necessary to wait for
      // this to finish before moving on as some tests immediately move to a protocol that depends on the 
      // one that was just uploaded.
      cy.get('@post-fetch-spy').its('callCount').then(callCount => {
        cy.log('callCount', callCount)
        for (let i = 0; i < callCount; i++) {
          cy.wait('@post-fetch')
        }
      })
    })
    .then(function () {
      return this.interceptedRequests
    })
})


Cypress.Commands.add('fetchUploadedCollection', (res, schema, documentation, documentationStore) => {
  let matchedCollection = {}
  let keysThatWereIgnored = []
  cy.wrap(null, { log: false }).then(function () {
    let surveyKey = Object.keys(res).find((key) => !Array.isArray(res[key]) && res[key]?.['survey_metadata'])

    // strips trailing s incase it is already pluralised (plot-selections)
    schema = Cypress._.mapKeys(schema, (val, key) => {
      if (key === 'plot-selections') {
        return key.slice(0, -1)
      }
      return key
    })

    //perform a fetch for each top level key of the schema
    Object.entries(schema).forEach(([model, value]) => {
      // ignore rules
      if (['plot-layout', 'plot-visit', 'project-area'].some(e => model.includes(e))) {
        keysThatWereIgnored.push({ [`${model}`]: value })
        return
      }

      // get the documentation schema for the model
      // stolen from schemafrommodelname in helpers.js
      const newModelName = `${Cypress._.upperFirst(Cypress._.camelCase(model))}Request`
      let data
      try {
        data = documentation.components.schemas[newModelName].properties.data.properties
      } catch (e) {
        throw new Error(`${e}\nFor '${newModelName}'`)
      }

      // checks if the survey goes by a different name for the model and will swap it if so
      let surveyName = surveyKey

      Object.keys(data).forEach(key => {
        if (data[key]?.['x-model-ref']) {
          if (surveyKey && data[key]['x-model-ref'] === surveyKey) surveyName = key
        }
      })

      // survey stuff done, just confirm if it does get changed
      if (surveyName !== surveyKey) cy.log(`surveyName changed from ${surveyKey} to ${surveyName} based on its x-model-ref`)

      // builds the filters
      // gets the ids via filters. Just makes the logic easier in my head. Unsure about notable performance impact, but im sure its fine.
      let filters = {}
      if (model === surveyKey || (surveyKey !== undefined && model !== surveyKey)) {
        const key = model === surveyKey ? `filters[id]` : `filters[${surveyName}][id]`
        filters[key] = res[surveyKey].id
      } else if (Array.isArray(res[model])) {
        res[model].forEach((el, index) => filters[`filters[$or][${index}][id]`] = el.id)
      }

      // generate endpoint and fetch request
      cy.wrap(documentationStore.findEndpointFromModel(model), { log: log })
        .then(endpoint => {
          endpoint = endpoint.startsWith('/') ? endpoint.slice(1) : endpoint
          cy.doRequest(endpoint, {
            method: 'GET',
            // failOnStatusCode: true,
            args: {
              'use-default': true,
              populate: 'deep', // '*'
              ...filters
            }
          }).then((response) => {
            // if it cant find it, get its relation from schema, and if it is one of our other models, get via that instead
            if (response.status !== 200) {
              throw new Error(`Failed fetching [${endpoint}] on [${surveyName}]. `)
            }
            matchedCollection[model] = Cypress._.cloneDeep(response.body.data)
          })
        })
    })
  }).then(function () {
    return [matchedCollection, keysThatWereIgnored]
  })
})

/**
 * Submits and fetches using the id's returned by a successful strapi post. Rebuilds everything by linking to the survey then checks if 
 * everything present in collections before upload exists in the database. 
 * Does not check direct values, only if a value exists.
 *
 *
 * @param {Boolean} force to force click if there's more then one or it has trouble finding it
 * @param {Number} [index] the index to use for accessing items in the bulkStore->protocol. Default to 0
 * @param {Boolean} [atSummary] whether to skip clicking the workflowNextButton if you are already on the summary page
 * @param {CallableFunction} [afterQueueCollectionCb] a callback function to execute after clicking publish confirm which can handle redirections, or other logic that needs to execute after adding to queue.
 * @param {Object} options - An optional configuration object.
 * @param {boolean} options.log - Whether to log output. Default is true.
 */
Cypress.Commands.add('workflowPublish', function (force = true, submissionIndex, atSummary, suggestedProtRedirectCb = null, skip = false, options = {}) {
  // TODO refactor to use a single object for the arguments to make it easier to manage the growing arguments.
  // grab docco, we'll need it a lot here
  let all = []
  cy.wrap(null, { log: false })
    .then(function () {
      cy.documentation(function (documentation, documentationStore) {
        cy.wrap(Cypress._.cloneDeep(documentation), { log: log }).as('documentation')
        cy.wrap(documentationStore, { log: log }).as('documentationStore')
      })
        .publishCollections(force, atSummary, suggestedProtRedirectCb, options)
        .each(function (request) {
          if (options?.willNotQueue) {
            return []
          }

          // getting the data 
          // if (this?.res && request.response.body[0] === this.res) {
          //     throw new Error('An error has occurred\n\t> Using the same res as previous request')
          // }

          cy.wrap(Cypress._.cloneDeep(request.response.body[0]), { log: log }).as('res')
          cy.wrap(Cypress._.cloneDeep(request.request.body.data?.collections?.[0] || request.request.body.data), { log: log }).as('schema')

          // slight cleaning
          cy.get('@schema', { log: log }).then(schema => delete schema.orgMintedIdentifier)
            .then(function () {
              // we need all of this, so make sure they're there
              cy.get('@res', { log: log }).should('not.be.undefined')
              cy.get('@schema', { log: log }).should('not.be.undefined')
              cy.get('@schema', { log: log }).its('orgMintedIdentifier', { log: log }).should('not.exist')
              cy.get('@documentation', { log: log }).should('not.be.undefined')
              cy.get('@documentationStore', { log: log }).should('not.be.undefined')
              log && cy.log('docco', this.documentation, this.documentationStore)
              log && cy.log('schema & res - unedited', Cypress._.cloneDeep(this.schema), Cypress._.cloneDeep(this.res))
            }).then(function () {
              cy.fetchUploadedCollection(this.res, this.schema, this.documentation, this.documentationStore)
                .then(function (res) {
                  cy.wrap(res[0], { log: log }).as('matchedCollection')
                  cy.wrap(res[1], { log: log }).as('keysThatWereIgnored')
                })
                .then(function () {
                  log && cy.log('res', Cypress._.cloneDeep(this.res))
                  log && cy.log('schema', Cypress._.cloneDeep(this.schema))
                  log && cy.log('matchedCollection', Cypress._.cloneDeep(this.matchedCollection))
                  log && cy.log('keysThatWereIgnored', Cypress._.cloneDeep(this.keysThatWereIgnored))
                })
            })
            .then(function () {
              // cy.log('matchedCollection & schema - unedited', Cypress._.cloneDeep(matchedCollection), Cypress._.cloneDeep(schema))
              // === process the matchedCollections to remove the fluff that strapi likes. e.g., {data: {id: 1}} will become just {id: 1} === //
              cy.wrap(null, { log: false })
                .then(function () {
                  this.keysThatWereIgnored.forEach((obj) => {
                    const [model, value] = Object.entries(obj)[0]
                    this.matchedCollection[model] = value
                  })
                  this.schema = Cypress._.mapKeys(this.schema, (val, key) => {
                    if (key === 'plot-selections') {
                      return key.slice(0, -1)
                    }
                    return key
                  })
                })
                .then(() => {
                  // using globals is a quick fix
                  if (global?.ignorePaths) {
                    return global.ignorePaths
                  }
                  return []
                })
                .then(function (ignorePaths) {
                  cy.callSync(oc.flatten, [this.schema], null, log)
                  cy.callSync(oc.flatten, [this.matchedCollection], null, log)
                  cy.callSync(oc.flattenKeys, [this.schema, ['data']], null, log)
                  cy.callSync(oc.flattenKeys, [this.matchedCollection, ['attributes', 'data']], null, log)
                  cy.callSync(oc.removePaths, [this.schema, ignorePaths], null, log)
                  cy.callSync(oc.removeKeys, [this.schema, ['projects_for_plot', 'temp_offline_id']], null, log)
                  cy.callSync(oc.removeKeys, [this.matchedCollection, ['projects_for_plot']], null, log)
                })
                .then(function () {
                  log && cy.log('res - after', Cypress._.cloneDeep(this.res))
                  log && cy.log('schema - after', Cypress._.cloneDeep(this.schema))
                  log && cy.log('matchedCollection - after', Cypress._.cloneDeep(this.matchedCollection))
                  log && cy.log('keysThatWereIgnored - after', Cypress._.cloneDeep(this.keysThatWereIgnored))
                })
                .then(function () {
                  all.push([Cypress._.cloneDeep(this.matchedCollection), Cypress._.cloneDeep(this.schema), this.documentation])
                })
            })
        })
    })
    .then(function () {
      // until the FIXME in command-utils:pub is dealt with, skip *should* never be called.
      if (skip || options?.willNotQueue)
        return all
      else {
        cy.wrap(all, { log: false })
          .each(function (data) {
            cy.callSync(oc.shareShape, [data[1], data[0]], oc.validateError)
            cy.callSync(oc.shareAnyValues, [data[1], data[0]], oc.validateError)
            cy.callSync(oc.shareValuesWith, [data[1], data[0], data[2]], oc.validateError)
          })
      }
    })
})

/** 
 * wish.com brand cy.wrap. Just a cy.wrap wrapper that logs the function being called 
 */
Cypress.Commands.add('callSync', function (operation, args, callback, toLog) {
  // context might become an issue but it'll probably be fine.
  toLog && cy.log('toLog', toLog)
  toLog && Cypress.log({
    name: 'callSync',
    message: operation.name
  })
  cy.wrap({ [operation.name]: function () { return operation(...args) } }, { log: false })
    .invoke({ log: false }, operation.name)
    .then(function (output) {
      // Could just use a .then after the command normally for the same effect?
      if (callback) callback(output)
      else return output
    })
})

registerCommands()
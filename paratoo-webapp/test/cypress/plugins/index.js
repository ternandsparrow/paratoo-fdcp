/* eslint-env node */
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

// cypress/plugins/index.js
const { cypressBrowserPermissionsPlugin, onBeforeBrowserLaunch, modifyAndTransformPluginEnv } = require('cypress-browser-permissions/plugin')
const { readdirSync } = require('fs')
const zipson = require('zipson')
module.exports = (on, config) => {
  if (config?.env?.AUTH_TYPE !== 'oicd') {
    //TODO want this back in but will have issues with login - need to somehow ignore coverage on redirect
    require('@cypress/code-coverage/task')(on, config)
    config.env.coverageDir = '../coverage'
  } else {
    console.log('Code coverage disabled')
  }

  // FIXME if still crashing comment out initialiseData in MainLayout (specifically, the map tiles section).
  if (config.isTextTerminal) {
    // cyrpess run
    config.numTestsKeptInMemory = 0
  } else {
    // cyrpess open
    config.numTestsKeptInMemory = 5
  }
  console.log('===========numTestsKeptInMemory========================', config.numTestsKeptInMemory)

  on('task', {
    log(args) {
      console.log(...args)
      return config
    },
    downloads: (downloadsPath = 'cypress/downloads') => {
      return readdirSync(downloadsPath)
    },
    compress: (uncompressedData) => {
      return zipson.stringify(uncompressedData)
    },
    uncompress: (compressedData) => {
      return zipson.parse(compressedData)
    }
  })
  config.env.browserPermissions = {
    notifications: "allow",
    geolocation: "allow",
    camera: "allow",
    microphone: "allow",
    images: "allow",
  }

  // interweaves the exported browser-permission-plugin functions with a normal call of on:before:browser launch to attach flags without being overriden by the plugin
  modifyAndTransformPluginEnv(config)
  on("before:browser:launch", (browser = {}, launchOptions) => {
    beforeLaunchFunc = onBeforeBrowserLaunch(config)
    launchOptions = beforeLaunchFunc(browser, launchOptions)
    if (browser.family === "chromium") {
      // don't think this actually works anymore but lets see if it has an effect 
      launchOptions.args.push("--js-flags=--max-old-space-size=3500")
      launchOptions.args.push(`--unsafely-treat-insecure-origin-as-secure=${config['baseUrl']}`)
      launchOptions.args.push('--ignore-certificate-errors')
      config.isInteractive && launchOptions.args.push('--auto-open-devtools-for-tabs')
    }
    return launchOptions
  })
  return config
}

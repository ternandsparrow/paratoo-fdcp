const fetch = require('node-fetch')
const baseUrl = require('./getCurrentUrl').url

async function retryFetch(url, maxRetries, retryDelay) {
  try {
    const response = await fetch(url)

    if (response.ok) {
      return response
    } else {
      throw new Error(`Request to ${url} failed with status: ${response.status}`)
    }
  } catch (error) {
    if (maxRetries > 0) {
      console.log(`Request to ${url} failed. Retrying in ${retryDelay} milliseconds...`)
      await new Promise(resolve => setTimeout(resolve, retryDelay))
      return retryFetch(url, maxRetries - 1, retryDelay)
    } else {
      throw new Error(`Max retries to ${url} exceeded: ${error.message}`)
    }
  }
}


const runComponent = {
  core: false,
  org: false,
  app: false,
}
//tests run in the runner's container so accessing localhost won't work, instead need to
//access the process running outside the runner's container
const componentLocalUrl = {
  core: `http://${baseUrl}:1337`,
  org: `http://${baseUrl}:1338`,
  app: `http://${baseUrl}:8080`,
}
const componentDone = {
  core: null,
  org: null,
  app: null,
}
//separate loop to init `componentDone` so that we can check if all are null
for (const component of ['core', 'org', 'app']) {
  if (runComponent[component]) {
    componentDone[component] = false
  }
}

for (const component of ['core', 'org', 'app']) {
  retryFetch(componentLocalUrl[component], 100, 30000)
    .then(response => {
      console.log(`Request to check ${component} successful. Status: ${response.status}`)
      componentDone[component] = true
      console.log('All relevant components are started?', Object.values(componentDone).every(c => c === true))
      if (
        Object.values(componentDone).every(c => c === true)
      ) {
        console.log('stack ready... you can start cypress now')
      }
    })
    .catch(error => {
      console.error(`Failed to make a successful request: ${error.message}`)
    })
}

#!/bin/bash
# if we're running in dev, beta or demo, we'll change the PWA manifest and icon to
# make it obvious
set -euxo pipefail
cd `dirname "$0"`

logPrefix='[DevMarking]'
deployedEnvName=${2-develop}
commitShortSHA=${3-DEADBEEF}

pre_build() {
  [ "$deployedEnvName" = "develop" ] && {
    echo "$logPrefix Building in the develop branch, updating pre-built PWA icons"
    cd ../public/icons/
    mv icon-512x512.png icon-512x512.png.disabled-for-dev
    mv dev-icon-512x512.png icon-512x512.png
  } || ([ "$deployedEnvName" = "beta" ] && {
    echo "$logPrefix Building in the beta branch, updating pre-built PWA icons"
    cd ../public/icons/
    mv icon-512x512.png icon-512x512.png.disabled-for-beta
    mv beta-icon-512x512.png icon-512x512.png
  }) || ([ "$deployedEnvName" = "demo" ] && {
    echo "$logPrefix Building in the in the demo branch, updating pre-built PWA icons"
    cd ../public/icons/
    mv icon-512x512.png icon-512x512.png.disabled-for-demo
    mv demo-icon-512x512.png icon-512x512.png
  })  || {
    echo "$logPrefix Running in prod, nothing to do"
  }
}

post_build() {
  [ "$deployedEnvName" = "develop" ] && {
    echo "$logPrefix Building in the develop branch, updating post-build PWA manifest"
    cd ../dist/pwa
    cat <<EOJS | node > manifest.json.dev
      const mf = require('./manifest.json')
      mf.name += ' Dev'
      mf.short_name += 'Dev'
      console.log(JSON.stringify(mf, null, 2))
EOJS
    mv manifest.json manifest.json.disabled-for-dev
    mv manifest.json.dev manifest.json
  } || ([ "$deployedEnvName" = "beta" ] && {
    echo "$logPrefix Building in the beta branch, updating post-build PWA manifest"
    cd ../dist/pwa
    cat <<EOJS | node > manifest.json.beta
      const mf = require('./manifest.json')
      mf.name += ' Beta'
      mf.short_name += 'Beta'
      console.log(JSON.stringify(mf, null, 2))
EOJS
    mv manifest.json manifest.json.disabled-for-beta
    mv manifest.json.beta manifest.json
  }) || ([ "$deployedEnvName" = "demo" ] && {
    echo "$logPrefix Building in the in the demo branch, updating post-build PWA manifest"
    cd ../dist/pwa
    cat <<EOJS | node > manifest.json.demo
      const mf = require('./manifest.json')
      mf.name += ' Demo'
      mf.short_name += 'Demo'
      console.log(JSON.stringify(mf, null, 2))
EOJS
    mv manifest.json manifest.json.disabled-for-demo
    mv manifest.json.demo manifest.json
  }) || {
    echo "$logPrefix Running in prod, no post-build work to do"
  }
}

undo() {
  # for easier testing :D
  cd ../public/icons/
  [ -f icon-512x512.png.disabled-for-dev ] && {
    echo "$logPrefix undoing dev icon changes"
    mv icon-512x512.png dev-icon-512x512.png
    mv icon-512x512.png.disabled-for-dev icon-512x512.png
  }
  [ -f icon-512x512.png.disabled-for-beta ] && {
    echo "$logPrefix undoing beta icon changes"
    mv icon-512x512.png beta-icon-512x512.png
    mv icon-512x512.png.disabled-for-beta icon-512x512.png
  }
  [ -f icon-512x512.png.disabled-for-demo ] && {
    echo "$logPrefix undoing demo icon changes"
    mv icon-512x512.png demo-icon-512x512.png
    mv icon-512x512.png.disabled-for-demo icon-512x512.png
  }
  [ -f manifest.json.disabled-for-dev ] && {
    echo "$logPrefix undoing dev manifest changes"
    rm manifest.json
    mv manifest.json.disabled-for-dev manifest.json
  }
  [ -f manifest.json.disabled-for-beta ] && {
    echo "$logPrefix undoing beta manifest changes"
    rm manifest.json
    mv manifest.json.disabled-for-beta manifest.json
  }
  [ -f manifest.json.disabled-for-demo ] && {
    echo "$logPrefix undoing demo manifest changes"
    rm manifest.json
    mv manifest.json.disabled-for-demo manifest.json
  }
}

nothing() {
  echo "usage: $0 <stage>"
  echo "      stage = pre_build|post_build"
  echo "   eg: $0 pre_build"
  exit 1
}

eval ${1:-nothing}

const dotenv = require('dotenv')
const fs = require('fs')
const cypress_parallel = require('./cypress-parallel')

console.log('Making BrowserStack config')

// handle cli args: https://stackoverflow.com/a/24638042
const argv = require('minimist')(process.argv.slice(2))
//get local envs like `quasar.conf.js`
const localEnvs = dotenv.config({ path: '.env.local' }).parsed

const useLocal = (() => {
  if (argv.local == 'true') {
    return true
  } else if (argv.local == 'false') {
    return false
  } else {
    console.warn("Couldn't parse `--local` param, defaulting to false")
    return false    
  }
})()

let errMsg = ''
let usernameToUse = null
let accessKeyToUse = null

if (process.env.BROWSER_STACK_USERNAME) {
  usernameToUse =  process.env.BROWSER_STACK_USERNAME
} else if (localEnvs.BROWSER_STACK_USERNAME) {
  usernameToUse = localEnvs.BROWSER_STACK_USERNAME
} else {
  errMsg += 'Missing BrowserStack username. '
}

if (process.env.BROWSER_STACK_ACCESS_KEY) {
  accessKeyToUse =  process.env.BROWSER_STACK_ACCESS_KEY
} else if (localEnvs.BROWSER_STACK_ACCESS_KEY) {
  accessKeyToUse = localEnvs.BROWSER_STACK_ACCESS_KEY
} else {
  errMsg += 'Missing BrowserStack username. '
}

if (errMsg !== '') {
  errMsg += 'You can define these in the version-controlled `.env` file, or the local `.env.local` file. '
  throw new Error(errMsg)
}

//for now (at least) we use less browsers on remote as to not use-up all of our trial minutes
const localBrowsers = [
  {
    "browser": "chrome",
    "os": "Windows 10",
    "versions": ["latest"]
  },
  {
    "browser": "firefox",
    "os": "Windows 10",
    "versions": ["latest"]
  },
  {
    "browser": "edge",
    "os": "Windows 10",
    "versions": ["latest"]
  },
  {
    "browser": "chrome",
    "os": "OS X Ventura",
    "versions": ["latest"]
  },
]
const remoteBrowsers = [
  {
    "browser": "chrome",
    "os": "Windows 10",
    "versions": ["latest"]
  },
  {
    "browser": "firefox",
    "os": "Windows 10",
    "versions": ["latest"]
  },
]
const browsersToUse = (() => {
  if (useLocal) {
    console.log('Running locally with browsers:', localBrowsers)
    return localBrowsers
  } else {
    console.log('Running remotely with browsers:', remoteBrowsers)
    return remoteBrowsers
  }
})()

//TODO figure out how to make `npm_dependencies` more dynamic - cannot just use all as some are not needed or can cause build errors (e.g., TypeScript-related modules). Also need to determine which packages we actually need
//TODO dynamic project_name (use postfix of branch/build - e.g., 'develop')
//TODO `run_settings.build_name` from Git hash (like app's versioning)
//TODO consider passing browsers as arg (will need a fairly terse method of doing so)
let baseConfig = {
  "auth": {
    "username": usernameToUse,
    "access_key": accessKeyToUse,
  },
  "browsers": browsersToUse,
  "run_settings": {
    "project_name": "Monitor BrowserStack Cypress Tests",
    "cypress_config_file": "./cypress.config.ts",
    "specs": cypress_parallel.getSpecFiles,
    "exclude": ["node_modules/*", "test/cypress/videos/*", "test/cypress/screenshots/*"],
    "npm_dependencies": {
      "@babel/core": "^7.18.2",
      "@babel/eslint-parser": "^7.0.0",
      "@babel/plugin-syntax-dynamic-import": "^7.8.3",
      "@babel/preset-env": "^7.0.0",
      "@quasar/app-webpack": "^3.9.2",
      "@quasar/babel-preset-app": "^2.0.1",
      "@quasar/quasar-app-extension-qpdfviewer": "^2.0.0-alpha.6",
      "@quasar/quasar-app-extension-testing": "^2.1.0",
      "@quasar/quasar-app-extension-testing-e2e-cypress": "^4.2.2",
      "@quasar/quasar-app-extension-testing-unit-jest": "^3.0.0-beta.5",
      "@sentry/webpack-plugin": "^2.2.0",
      "@vue/compiler-dom": "^3.2.0",
      "@vue/compiler-sfc": "^3.2.0",
      "@vue/server-renderer": "^3.2.0",
      "@vue/test-utils": "^2.2.0",
      "@vue/vue3-jest": "27.0.0-alpha.3",
      "autoprefixer": "^10.4.5",
      "babel-jest": "^27.3.1",
      "babel-loader": "^8.2.2",
      "browserslist-useragent-regexp": "^3.0.2",
      "cors": "^2.8.5",
      "cypress": "^12.12.0",
      "eslint": "^7.32.0",
      "eslint-config-prettier": "^6.9.0",
      "eslint-config-standard": "^16.0.2",
      "eslint-loader": "4.0.2",
      "eslint-plugin-cypress": "^2.11.3",
      "eslint-plugin-import": "^2.19.1",
      "eslint-plugin-jest": "^24.3.6",
      "eslint-plugin-node": "^11.0.0",
      "eslint-plugin-promise": "^5.1.0",
      "eslint-plugin-quasar": "^1.0.0",
      "eslint-plugin-unused-imports": "1.1.5",
      "eslint-plugin-vue": "^7.19.1",
      "eslint-webpack-plugin": "3.2.0",
      "html-webpack-plugin": "^5.5.0",
      "jest": "^27.3.1",
      "node-polyfill-webpack-plugin": "^1.1.4",
      "process": "^0.11.10",
      "typescript": "^4.5.1-rc",
      "vite": "^2.9.1",
      "vue-loader": "^17.1.0",
      "webpack": "^5.84.1",
      "webpack-bundle-analyzer": "4.8.0",
      "webpack-dev-server": "^4.11.0",
      "webpack-preprocessor-loader": "^1.3.0",
      "workbox-webpack-plugin": "^7.0.0",
      "quasar": "2.12.0",
      "uuid": "^8.3.2",
      "vue": "^3.2.0"
    }
  },
  "connection_settings": {
    "local": useLocal
  }
}

const configPath = './browserstack.json'
try {
  fs.writeFileSync(configPath, JSON.stringify(baseConfig, null, 2))
} catch (err) {
  console.error(err)
}

console.log('Successfully made BrowserStack config at:', configPath)
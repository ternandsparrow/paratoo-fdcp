// hardcoding the jobs for offline

// handle cli args: https://stackoverflow.com/a/24638042
const argv = require('minimist')(process.argv.slice(2))

const NODE_INDEX = Number(process.env.CI_NODE_INDEX || 1)
const TEST_FOLDER = './test/cypress/integration/offline'

if (argv?.stdout) {
    console.log(sortOfflineSpecs().join(','))
}

function sortOfflineSpecs() {
    switch (NODE_INDEX) {
        case 1:
            return [
                TEST_FOLDER + '/offline-floristics-and-dependencies.cy.js'
            ]
        case 2:
            // plot-layout
            return [
                TEST_FOLDER + '/offline-plot-layout-visit-description.cy.js'
            ]
        case 3:
            return [
                TEST_FOLDER + '/offline-partial-connection.cy.js.js',
                TEST_FOLDER + '/offline-camera-trapping.cy.js',
                TEST_FOLDER + '/offline-soils.cy.js',
                TEST_FOLDER + '/offline-vertebrate.cy.js',
            ]
    }
}
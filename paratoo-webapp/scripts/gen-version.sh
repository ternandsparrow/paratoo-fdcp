## thanks Tom! https://github.com/ternandsparrow/wild-orchid-watch-pwa/blob/master/scripts/gen-version.sh
#!/bin/bash
# generates a version that can be used for a deployed app
#
# we have to use the version in a few places so the trick is to generate it
# once and make sure we can read it everywhere
set -euo pipefail
cd `dirname "$0"`

commitHash=$CI_COMMIT_SHORT_SHA
timestamp=`TZ='Australia/Adelaide' date +%Y%m%d-%H%M%S`
version=$(grep '"version"' ../package.json | cut -d '"' -f 4 | head -n 11)

cat << EOF > ../src/misc/build_version_info.js
module.exports = {
    git_hash: '${commitHash}',
    timestamp: '$timestamp',
    paratoo_webapp_version: '$version'
  };
EOF

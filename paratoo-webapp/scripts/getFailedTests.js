
// reads allSpecsStatus.json defined in cypress-parallel.js for failed cypress tests
// and sets their status to true if they passed
// example usage: cat test/cypress/logs/current_terminal.log | grep ✖ | node scripts/getFailedTests.js
const fs = require('fs')
const { allSpecStatusPath } = require('./cypress-parallel')

process.stdin.setEncoding('utf8')

let done = false
let passedTests = []
process.stdin.on('readable', () => {
  let chunk
  while ((chunk = process.stdin.read()) !== null) {
    // strip ansi escape sequences
    // Stole the regex from: https://stackoverflow.com/questions/25245716/remove-all-ansi-colors-styles-from-strings
    // sure hope ignoring this has no far reaching consequences
    // eslint-disable-next-line no-control-regex
    let parsedChunk = chunk.split('  ').map(e => e.replace(/[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g, ''))
      // eslint-disable-next-line no-control-regex
    passedTests = parsedChunk
      .filter(e => /^[a-zA-Z\d.-]+(?=[a-zA-Z])/.test(e) && e.length > 1)
      .filter(e => !e.includes('All specs passed!')) 
    console.log('debug for found tests that passed from regex: ', passedTests)
  
  }
})

process.stdin.on('end', () => {
  // needs to be in sync with the json file its using. If it all works this shouldnt be an issue
  // but if it is finding failed tests and not saying it will be rerun, just double check the json.
  let allSpecs = JSON.parse(fs.readFileSync(allSpecStatusPath))
  for (let [spec, passed] of Object.entries(allSpecs)) {
    if (passedTests.some(e => spec.includes(e))) {
      allSpecs[spec] = true 
    } else {
      console.log(spec, 'failed, will be rerun')
    }
  }
  fs.writeFileSync(allSpecStatusPath, JSON.stringify(allSpecs))
  console.log('Writing to', allSpecStatusPath)
})

const fs = require('fs')
const path = require('path')
// handle cli args: https://stackoverflow.com/a/24638042
const argv = require('minimist')(process.argv.slice(2))
const NODE_INDEX = Number(process.env.CI_NODE_INDEX || 1)
const NODE_TOTAL = Number(process.env.CI_NODE_TOTAL || 1)

const SPEC_STATUS_PATH = `test/cypress/allSpecsStatus.json`
// deprecated --priority 
// REPLACED --priority bool with --folder [folderName]
// if omitted defaults to run
// example: 
//    --folder offline
//    --folder priority 
// lets you specify if we want to use /[folderName] specs instead of the default /run
let TEST_FOLDER = './test/cypress/integration/' + (argv?.folder && typeof argv?.folder !== 'boolean' ? `${argv?.folder}/` : 'run/')
// console.log(TEST_FOLDER)
// --ignore <spec>,<spec>
// any specs you want to omit from the selection as a string separated by commas
let ignore_list = argv.ignore ? argv.ignore.split(',') : []

// This log will be printed out to the console
// so that cypress will know which files will be run.
// Also, since getSpecFiles returns an array, the paths are
// joined with comma
if (argv?.stdout) {
  // gets them based on if they have passed or not using allSpecStatus.json
  // when a job finishes, it will update its allSpecStatus.json with the passed specs by assigning them true as a value
  // the json will be unique to each job and passed to any job of the same name (e.g., webapp-e2e 3/8) via gitlab cache using the commit hash + node_index as its key
  console.log(Object.entries(allSpecStatus(getSpecFiles())).filter((e) => e[1] === false).map(spec => spec[0]).join(','))
  // console.log(getSpecFiles().join(','))
}
// for the gitlab pipeline specifically
function getSpecFiles() {
  const allSpecFiles = walk(TEST_FOLDER)

  const sorted = allSpecFiles
    .sort()
    //splits up the specs for the parallel GitLab jobs
    .filter((_, index) => (index % NODE_TOTAL) === (NODE_INDEX - 1))
  return sorted
}

function walk(dir) {
  let files = fs.readdirSync(dir, { recursive: true })
  files = files.map(file => {
    const filePath = path.join(dir, file)
    if (ignore_list.some(e => filePath.includes(e))) return
    const stats = fs.statSync(filePath)
    if (stats.isDirectory()) return walk(filePath)
    else if (stats.isFile()) return filePath
  }).filter(e => e)
  return files
    .reduce((all, folderContents) => all.concat(folderContents), [])
}


function allSpecStatus(filteredSpecsArray) {
  // console.log(filteredSpecsArray)
  // shouldnt need to worry about the object having too many keys as it should be created with each job
  let data = {}
  // console.log('data', data)
  if (!fs.existsSync(SPEC_STATUS_PATH)) {
    for (const spec of filteredSpecsArray) {
      data[spec] = false
      // console.log('data1', data)
    }
    fs.writeFileSync(SPEC_STATUS_PATH, JSON.stringify(data))
    // console.log('data', data)
  } else {
    data = JSON.parse(fs.readFileSync(SPEC_STATUS_PATH))
  }
  return data
}



module.exports = {
  getSpecFiles: getSpecFiles(),
  allSpecStatusPath: SPEC_STATUS_PATH
}
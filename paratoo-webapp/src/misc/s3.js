import { doManagedFetch } from './api'
export default class S3 {
  constructor() {
    this.url = `https://${process.env.VUE_APP_AWS_BUCKET}.s3.${process.env.VUE_APP_AWS_REGION}.amazonaws.com`
  }
  async list(prefix, callback) {
    const url = this.url + `/?list-type=2&prefix=${prefix}`
    const options = {
      headers: {
        methods: 'GET',
      },
      'x-static-resource': true
    }
    try {
      const res = await doManagedFetch(
        url, 
        options, 
        [],
        true,
        false,
        false,
        false
      )
      if (res) {
        return callback(null, { Contents: res })
      } else {
        return callback(res)
      }
    } catch (error) {
      return callback(error)
    }
  }

  getObjectUrl(key) {
    const url = this.url + `/${key}`
    return url
  }
}

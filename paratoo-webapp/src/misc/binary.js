import { paratooErrorHandler, paratooSentryBreadcrumbMessage } from './helpers'
import Dexie from 'dexie'
import { useDataManagerStore } from 'src/stores/dataManager'
import { v4 as uuidv4 } from 'uuid'
const db = new Dexie('binary')

db.version(1).stores({
  binary: '&id, surveyId',
})

db.version(2)
  .stores({
    binary: '&id, surveyId, protocolId',
  })
  // data migration
  .upgrade((tx) => {
    return tx
      .table('binary')
      .toCollection()
      .modify((file) => {
        const surveyId = file.surveyId
        const inQueue = useDataManagerStore().publicationQueue.some(
          (queuedItem) => {
            const collection = queuedItem.collection
            const hasRelatedSurveyId = Object.values(collection).some(
              (item) => item.survey_metadata?.survey_details.uuid === surveyId,
            )
            return hasRelatedSurveyId
          },
        )
        file.inQueue = inQueue
      })
  })

export const binary = {
  async init() {
    try {
      await db.open()
    } catch (error) {
      throw paratooErrorHandler('Failed to open binary database', error)
    }
  },

  /**
   * save binary object as an attachment to
   * @param {Blob} blob binary object
   * @param {Number} protocolId
   * @param {String} modelName to distinguish between steps of a protocol, so we can cleanup after emitting to bulk store
   */
  async saveBinary(blob, protocolId, modelName) {
    // for testing only
    if (blob.name === 'this_image_should_fail_to_upload.jpg') {
      throw new Error('this_image_should_fail_to_upload')
    }
    if (!modelName) {
      throw new Error('modelName is required')
    }
    const isBlob = blob instanceof Blob
    if (!isBlob) {
      throw new Error('Must be a Blob object')
    }

    if (!blob.size) {
      throw new Error('0 bytes file detected, not allowed')
    }
    // somehow crud trigger twice when submitting, so this is a work around to avoid race condition
    try {
      const fileIdentifier = uuidv4()
      await db.binary.add({
        id: fileIdentifier,
        protocolId: parseInt(protocolId),
        data: blob,
        modelName,
        inQueue: false,
      })
      return fileIdentifier
    } catch (error) {
      throw paratooErrorHandler('Failed to save binary file', error)
    }
  },
  async saveApiModelsBinary(fileIdentifier, blob, originalFileName) {
    const count = await db.binary.where({ id: fileIdentifier }).count()
    if (count === 0) {
      //don't add if we've already fetched
      await db.binary.add({
        id: fileIdentifier,
        originalFileName: originalFileName,
        data: blob,
      })
    }
  },
  async getRawBinary(id, silent = false) {
    // Get the attachment data
    try {
      const res = await db.binary.get({ id })
      if (!res) {
        const error = new Error('Attachment not found')
        if (!silent) {
          throw paratooErrorHandler(
            'Failed to retrieve attachment with id ' + id,
            error,
          )
        } else {
          throw error
        }
      }
      paratooSentryBreadcrumbMessage(
        `Retrieved attachment with id ${id}, name: ${res.data.name}, type: ${res.data.type}, size: ${res.data.size}`,
        'getRawBinary',
      )
      const isBlob = res?.data instanceof Blob
      if (!isBlob) {
        throw paratooErrorHandler(
          'failed to retrieve attachment',
          new Error('Must be a Blob object'),
        )
      }
      return res?.data
    } catch (error) {
      if(!silent) {
        throw paratooErrorHandler('failed to retrieve attachment', error)
      } else {
        throw error
      }
    }
  },
  async getBinaryInUrl(id) {
    const blob = await this.getRawBinary(id)
    return URL.createObjectURL(blob)
  },
  async removeBinaryFile(id) {
    try {
      await db.binary.where('id').equals(id).delete()
    } catch (error) {
      throw paratooErrorHandler('Failed to remove binary file', error)
    }
  },
  /**
   * remove all binary files for a given protocol the not in the queue
   * */
  async removeBinaryFileOfCurrentProt(protocolId) {
    try {
      await db.binary
        .where('protocolId')
        .equals(protocolId)
        .filter((file) => !file.inQueue)
        .delete()
    } catch (error) {
      throw paratooErrorHandler(
        'Failed to remove all binary files of current protocol',
        error,
      )
    }
  },
  async updateStateOfBinaryOfQueuedProtocol(protocolId, collection) {
    try {
      const surveyId = this.getSurveyUUIDFromCollection(collection)
      await db.binary
        .where('protocolId')
        .equals(protocolId)
        .filter((file) => !file.inQueue)
        .modify({ inQueue: true, surveyId })
    } catch (error) {
      throw paratooErrorHandler(
        'Failed to update state of binary files of current protocol',
        error,
      )
    }
  },
  async cleanupBinaryFileAfterProtSubmission(submissionRes) {
    try {
      const surveyId = this.getSurveyUUIDFromCollection(submissionRes)
      if (!surveyId) return
      await db.binary.where('surveyId').equals(surveyId).delete()
    } catch (error) {
      throw paratooErrorHandler('Failed to clean up binary files', error)
    }
  },
  getSurveyUUIDFromCollection(data) {
    if (!data) return
    for (const collection of Object.values(data)) {
      if ('survey_metadata' in collection) {
        return collection.survey_metadata.survey_details.uuid
      }
    }
  },
  async cleanupUnusedBlobsOfCurrentProtocol(IDs, protocolId, modelName) {
    try {
      await db.binary
        .where('protocolId')
        .equals(protocolId)
        .filter(
          (file) =>
            !IDs.includes(file.id) &&
            !file.inQueue &&
            file.modelName === modelName,
        )
        .delete()
    } catch (error) {
      throw paratooErrorHandler('Failed to delete unused files', error)
    }
  },
  async findAndSyncBlobInCollectionWithDexie(
    collection,
    protocolId,
    modelName,
  ) {
    const binaryFileIds = await this.findFileIdentifiersInCollection(collection)
    await this.cleanupUnusedBlobsOfCurrentProtocol(
      binaryFileIds,
      protocolId,
      modelName,
    )
  },
  async findFileIdentifiersInCollection(data, binaryFileIds = []) {
    // we traverse the collection and look for object with file_identifier, which is binary file(photo, video, ect)
    if (Array.isArray(data)) {
      for (const obj of data) {
        await this.findFileIdentifiersInCollection(obj, binaryFileIds)
      }
    } else if (typeof data === 'object') {
      for (let key in data) {
        if (key === 'file_identifier') {
          //the obj that has the file identifier is a binary
          const fileIdentifier = data[key]
          const mediaType = data.src ? 'src' : 'media'
          //we save it to the store if it's not there yet , and replace with the file_identifier
          if (data[mediaType] instanceof Blob) {
            data.mimeType = data[mediaType].type
          }
          binaryFileIds.push(fileIdentifier)
        } else if (typeof data[key] === 'object') {
          await this.findFileIdentifiersInCollection(data[key], binaryFileIds)
        }
      }
    }
    return binaryFileIds
  },
  // get all the binary files in a collection
  async getAllBinaryFilesInCollection() {
    return await db.binary.toArray()
  },
}

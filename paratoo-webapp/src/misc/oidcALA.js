import axios from 'axios'
import { enableOIDC } from './constants'
import OIDCManager from './oidcManager'
import { useAuthStore } from '../stores/auth'
import { useEphemeralStore } from '../stores/ephemeral'
import * as Sentry from '@sentry/vue'
const { isEmpty } = require('lodash')
import {
  generateOIDCTokenParams,
  clearOIDCFromLocalStorage,
  notifyHandler,
  openIdConfiguration,
  paratooWarnMessage,
  paratooErrorHandler,
  paratooSentryBreadcrumbMessage,
  isUrlReadyToContinue,
} from 'src/misc/helpers'

// 30 days in seconds
const REFRESH_TOKEN_EXPIRES_IN = 25920000

/**
 * trigger a redirect of the current window to the authorization endpoint.
 */
export async function alaLoginRedirect() {
  if (!enableOIDC) {
    paratooWarnMessage('oidc is disabled.')
    return {
      oidc: 'SSO is disabled',
    }
  }

  // clear existing configs
  clearOIDCFromLocalStorage()
  const meta_config = await openIdConfiguration(
    process.env.VUE_APP_OIDC_DISCOVERY_URI,
  )
  const oidcManager = new OIDCManager(meta_config)
  if (!oidcManager) {
    paratooWarnMessage('SSO variables are not valid.')
    return {
      oidc: 'Please make sure all the SSO variables are valid',
    }
  }

  await oidcManager.loginRedirect()
  paratooSentryBreadcrumbMessage(
    `(login)redirecting to: ${process.env.VUE_APP_OIDC_CLIENT}`,
    'oidcALA',
  )
  return {
    oidc: 'redirecting to ' + process.env.VUE_APP_OIDC_CLIENT,
  }
}

/**
 * sso logout.
 */
export async function alaLogout(keepPreviousProfile = false) {
  if (!enableOIDC) return null
  const isOnline = useEphemeralStore().networkOnline
  if (!isOnline) {
    paratooSentryBreadcrumbMessage(
      'Cannot logout as network is not online',
    )
    return null
  }
  
  let intervalId = setInterval(() => {
    if (
      localStorage.getItem('apiModelsHasHydrated') == 'true' &&
      localStorage.getItem('authHasHydrated') == 'true'
    ) {
      paratooSentryBreadcrumbMessage(
        `Stores are hydrated, proceeding with ALA Logout`,
      )
      if (keepPreviousProfile) {
        useAuthStore().keepPreviousProfile()
      }

      // clear existing configs
      clearOIDCFromLocalStorage()

      const authStore = useAuthStore()
      const ephemeralStore = useEphemeralStore()
      authStore.setToken(null, null)
      authStore.setLoginTime(null)
      authStore.setLastTokenRefreshTime(null)
      authStore.setUserProfile(null)
      authStore.setProjAndProt([])
      ephemeralStore.tokenHasExpired = false
      ephemeralStore.refreshTokenHasExpired = false

      // current session is already destroyed, don't need to hit logout url.
      if (!authStore?.oidcTokenResponse?.id_token) {
        paratooSentryBreadcrumbMessage(
          'skip trying logging out, reason: auth token is expired',
        )
        return
      }
      // logout
      let logoutEndpoint = process.env.VUE_APP_OIDC_LOGOUT_REDIRECT_URI
      // use [equal] and [and] instead of '=' and '&'.
      // FIXME: sed command in dockerfile
      logoutEndpoint = logoutEndpoint
        .replaceAll('[equal]', '=')
        .replaceAll('[and]', '&')
      logoutEndpoint = logoutEndpoint.replaceAll(
        '[id_token_hint]',
        authStore.oidcTokenResponse.id_token,
      )

      authStore.oidcTokenResponse = null

      paratooSentryBreadcrumbMessage(
        `(logout)redirecting to: ${logoutEndpoint}`,
        process.env.VUE_APP_OIDC_AUTH_TYPE,
      )

      clearInterval(intervalId)
      intervalId = null

      window.location.href = logoutEndpoint
    } else {
      const storesNotHydrated = []
      for (const store of ['apiModels', 'auth']) {
        if (localStorage.getItem(`${store}HasHydrated`) != 'true') {
          storesNotHydrated.push(store)
        }
      }
      paratooSentryBreadcrumbMessage(
        `Stores are not hydrated (${storesNotHydrated.join(', ')}), waiting...`,
      )
    }
  }, 1000)
}

/**
 * refresh access token.
 */
export async function refreshAccessToken() {
  if (!enableOIDC) {
    paratooWarnMessage('oidc is disabled')
    return
  }

  const authStore = useAuthStore()

  // checks expiry
  if (!authStore.oidcTokenResponse) {
    throw paratooErrorHandler('oidcTokenResponse not found in authstore')
  }
  const token_expire_in = authStore.oidcTokenResponse['created_at']
  const token_diff = (Date.now() - token_expire_in) / 1000
  if (token_diff > REFRESH_TOKEN_EXPIRES_IN) {
    throw paratooErrorHandler(
      'oidc auth token is expired, last refreshed 30 days ago',
    )
  }

  // getting token api endpoint from oidc config
  const meta_config = await openIdConfiguration(
    process.env.VUE_APP_OIDC_DISCOVERY_URI,
  )
  if (!meta_config.token_endpoint) {
    throw paratooErrorHandler(
      `token endpoint is missing in openId configuration, source url: ${process.env.VUE_APP_OIDC_DISCOVERY_URI}`,
    )
  }

  const requestOptions = {
    method: 'POST',
    url: meta_config.token_endpoint,
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
    data: new URLSearchParams({
      grant_type: 'refresh_token',
      client_id: process.env.VUE_APP_OIDC_CLIENT_ID,
      refresh_token: authStore.oidcTokenResponse.refresh_token,
    }),
  }

  paratooSentryBreadcrumbMessage(
    'Attempting to refresh token',
  )

  const response = await sendRequestWithOptions(
    requestOptions,
    authStore,
    false,
  )
  await updateTokenAndUserDetails(response)
  paratooSentryBreadcrumbMessage('Successfully refreshed oidc token')

  return {
    jwt: response.data.access_token,
    refreshToken: authStore.oidcTokenResponse.refresh_token,
  }
}

/**
 * sends requests to get access token and refresh token if param `code` found in the url
 */
export async function getAccessTokenFromUrlCode() {
  if (!enableOIDC) {
    paratooWarnMessage('oidc is disabled')
    return
  }

  // creates api payload using `code`
  let tokenParams = await generateOIDCTokenParams(
    process.env.VUE_APP_OIDC_DISCOVERY_URI,
  )
  if (!tokenParams) return

  paratooSentryBreadcrumbMessage(
    'Valid code found in the url, sending request to get access token and refresh token',
  )
  const requestOptions = {
    method: 'POST',
    url: tokenParams.meta_config.token_endpoint,
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
    data: tokenParams.params,
  }

  const response = await sendRequestWithOptions(
    requestOptions,
    null,
    false,
    true,
  )
  return await updateTokenAndUserDetails(
    response,
    true,
    tokenParams.meta_config.userinfo_endpoint,
  )
}

/**
 * update or set token and user details
 */
async function updateTokenAndUserDetails(
  response,
  newToken = false,
  userinfoEndpoint = null,
) {
  if (!response?.data) {
    throw paratooErrorHandler('Token not found in response body')
  }

  // update tokens
  const authStore = useAuthStore()
  const currentTime = Date.now()

  authStore.oidcTokenResponse = response.data
  authStore.oidcTokenResponse['updated_at'] = currentTime
  authStore.setToken(response.data.access_token, response.data.refresh_token)
  authStore.setLastTokenRefreshTime(new Date(currentTime))

  // if new token
  if (newToken) {
    authStore.oidcTokenResponse['created_at'] = currentTime
    authStore.setLoginTime(new Date(currentTime))

    // login complete
    const ephemeralStore = useEphemeralStore()
    ephemeralStore.isLogin(true)
    notifyHandler('positive', 'Success. Login complete')

    // update login status
    ephemeralStore.tokenHasExpired = false
    ephemeralStore.refreshTokenHasExpired = false
  }

  // update user details
  if (userinfoEndpoint) {
    // set user details
    const userDetails = await generateUserProfileFromToken(
      authStore,
      userinfoEndpoint,
    )
    authStore.setUserProfile(userDetails)
    Sentry.setUser({ username: authStore.userProfile.email })
    paratooSentryBreadcrumbMessage('successfully set user details')

    // clear temporary oidc variables
    clearOIDCFromLocalStorage()
    await refreshAllProtocols()
  }

  return authStore.token
}

/**
 * Determine user profile from base64 encoded token
 *
 * @param {String} authstore auth store
 * @param {String} url user info token endpoint
 *
 * @returns {Object} userProfile user info
 */
export async function generateUserProfileFromToken(authstore, url) {
  try {
    const requestOptions = {
      method: 'GET',
      url: url,
      headers: { Authorization: `Bearer ${authstore.token}` },
    }

    const userInfo = await sendRequestWithOptions(
      requestOptions,
      authstore,
      true,
      true,
    )
    return {
      id:
        userInfo.data.userid /* cas */ || userInfo.data.username /* cognito */,
      username: userInfo.data.email,
      email: userInfo.data.email,
      provider: userInfo.data['custom:organisation'] || 'TERN Adelaide',
      confirmed: userInfo.data['custom:activated'] || true,
      blocked: userInfo.data['custom:disabled'] || false,
      createdAt: userInfo.data['custom:created'],
      updatedAt:
        userInfo.data['custom:lastUpdated'] /* cognito */ ||
        userInfo.data['updated_at'] /* cas */,
    }
  } catch (error) {
    paratooErrorHandler('Failed to generate user profile from token: ', error)
  }
}

async function refreshAllProtocols() {
  const authStore = useAuthStore()

  //Getting users' projects and protocols
  await authStore.refreshProjectsAndProtocols().catch((err) => {
    const msg = `Field to refresh projects and protocols, reason is: ${err}`
    paratooWarnMessage(msg)
  })
  // populate documentation during app startup
  authStore.populateApiModelsAndDocs().catch((reason) => {
    const msg = `Field to load api and documentation, reason is: ${reason}`
    paratooWarnMessage(msg)
  })
}

/**
 * Send axios request
 *
 * @param {Object} options request option with url and method type
 * @param {Object} authStore auth store
 * @param {Bool} includeBearerToken token required or not
 * @param {Bool} force force to send request
 *
 * @returns {Object} response
 */
async function sendRequestWithOptions(
  options,
  authStore,
  includeBearerToken,
  force = false,
) {
  if (!options || isEmpty(options)) {
    throw paratooErrorHandler('Trying to send request without request options')
  }

  if (!authStore) authStore = useAuthStore()

  const urlStatus = isUrlReadyToContinue(
    options.url,
    includeBearerToken,
    authStore,
    options,
    true,
  )
  if (!urlStatus.isReady && !force) {
    paratooWarnMessage(`${urlStatus.msg}, url: ${options.url}`)
  }

  try {
    // so that we can abort all requests using signal when device goes offline
    const ephemeralStore = useEphemeralStore()
    const last = ephemeralStore.fetchAbortControllers.length - 1
    options['signal'] = ephemeralStore.fetchAbortControllers[last].signal
    paratooSentryBreadcrumbMessage(
      `sendRequestWithOptions called with options: ${JSON.stringify(options)}`,
    )
    const response = await axios.request(options)
    paratooSentryBreadcrumbMessage(
      JSON.stringify(response?.data),
      'sendRequestWithOptionsResponseData',
    )
    paratooSentryBreadcrumbMessage(
      `${response?.status}: $${response?.statusText}`,
      'sendRequestWithOptionsResponseStatus',
    )
    return response
  } catch (error) {
    throw paratooErrorHandler(
      `Failed to send request with options (see above trace)`,
      error,
    )
  }
}

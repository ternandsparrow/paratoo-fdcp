//TODO revise JSDoc and comments
import {
  at,
  upperFirst,
  camelCase,
  cloneDeep,
  isEmpty,
  isEqual,
  findIndex,
  isNil,
  union,
  kebabCase,
  clone,
} from 'lodash'
import { Notify, Loading, Dialog } from 'quasar'
import * as Sentry from '@sentry/vue'
import { defineAsyncComponent } from 'vue'
import { dexieDB } from '../misc/dexieDB'
import { initState } from '../assets/stateInits/apiModels'
import { useApiModelsStore } from 'src/stores/apiModels'
import { useAuthStore } from 'src/stores/auth'
import { useBulkStore } from 'src/stores/bulk'
import { useDataManagerStore } from 'src/stores/dataManager'
import {
  webAppCookiePrefix,
  coreApiPrefix,
  australiaOutline,
  coreApiUrlBase,
  orgApiUrlBase,
  reverseGeocodingUrl,
  modeOfOperation,
  potentialOtherValues,
  enableOIDC,
  OIDCClient,
  resourceTypes,
  orgApiPrefix,
  orgCustomApiPrefix,
  paratooWebAppBuildVersion,
  buildGitHash,
  layoutAndVisitUuid,
  sendAppAnalyticsInterval,
  dumpOrigins,
  cameraTrapDeploymentUuid,
  floristicsLiteUuid,
  locationRequiredMsg,
  permissionTypes,
  storageTypes,
} from './constants'
import { customRules } from './customRules'
import supportedBrowsers from 'app/supportedBrowsers'
import { useEphemeralStore } from '../stores/ephemeral'
import {
  doCoreApiGet,
  doCoreApiPost,
  doCoreApiDelete,
  doCoreApiPut,
} from './api'
import { NVIS_RULES } from './NVISRules'
import { Capacitor } from '@capacitor/core'
import GeoJSON from 'geojson'
import axios from 'axios'
import * as turf from '@turf/turf'
import { useDocumentationStore } from 'src/stores/documentation'
import { v4 as uuidv4, validate as isUuid } from 'uuid'
import {
  modelsToPopulate,
  login,
  validateToken,
  tokenRefresh,
  mintIdentifier,
  docBuildInfo,
  heartbeat,
} from './apiLists'
import { binary } from './binary'
import build from './build_version_info'
import * as mime from 'mime-types'
import { date } from 'quasar'
import { snakeCase } from 'lodash'
import { conditionalFieldLabels } from './conditionalFieldLabels'
import { parse } from 'zipson'
import {
  historicalDataManagerDb,
} from '../../src-pwa/dexieDb/historical-data-manager'
import { useRouter, useRoute } from 'vue-router'





/**
 * this mapper is to deal with weird cases when converting camel case schema names to
 * kebab case model names. For example LutSoils30CmSamplingIntervalRequest should be
 * lut-soils-30cm-sampling-interval but the default handler (using lodash) will get
 * lut-soils-30-cm-sampling-interval
 * 
 * also mirrored in core's expose-documentation
 * 
 * TODO this is not the preferred way, ideally we want to use a single generic handler, or apply a policy in how LUTs containing numbers are named
 */
const schemaToModelNameExceptions = {
  'LutSoils30CmSamplingIntervalRequest': 'lut-soils-30cm-sampling-interval',
}

export function getPath(obj, path) {
  return at(obj, path)[0]
}

// set to 'production', 'beta' or 'development' so we can adapt to where we're deployed
export const deployedEnvName = process.env.VUE_APP_DEPLOYED_ENV_NAME

// Flag for when we are in PROD
export const isProduction = process.env.NODE_ENV === 'production'

/*
 * Due to circular references, we need to load ApiModelCrudUI asynchronously
 */
export const asyncCrud = defineAsyncComponent(() => {
  return import('../components/ApiModelCrudUi.vue')
})

export async function updateBgImage() {
  const currentBgImage = getCookie('loginBgImg')
  // if current state exists,
  //   we won't hit geocode api find current state again
  if (currentBgImage) return

  const success = (position) => {
    var pos = position.coords
    const url = `${reverseGeocodingUrl}&lat=${pos.latitude}&lon=${pos.longitude}`
    fetch(url)
      .then((result) =>
        result.json().then((featureCollection) => {
          const foundAddress = featureCollection.features[0]
          const loginBgImg = foundAddress.properties.state
            ? foundAddress.properties.state.replace(' ', '')
            : null
          // store current state
          saveCookie('loginBgImg', loginBgImg)
        }),
      )
      .catch((error) => {
        console.log("Failed to resolve user's current state", error)
      })
  }

  // lazy load geocode and find current coords
  import('@capacitor/geolocation')
    .then(({ Geolocation }) => {
      Geolocation.getCurrentPosition()
        .then((position) => {
          success(position)
        })
        .catch((error) => {
          console.log('Failed to get current position', error)
        })
    })
    .catch((error) => console.log('Failed to load geolocation package', error))
}

/**
 * Handles Quasar notifications
 * We use Quasar's predefined types to indicate the type of message. See:
 * https://quasar.dev/quasar-plugins/notify#predefined-types
 * It's also possible to define our own types, but this isn't needed at the moment
 * //TODO add proper support for 'ongoing' type
 * //TODO allow passing of a specific icon which overrides the pre-defined types
 * //TODO refactor error so we can get better messages out of it
 *
 * @param {('positive' | 'negative' | 'warning' | 'info' | 'ongoing')} type - the type of notification
 * @param {String} msg - the message to notify
 * @param {Error} [err] - an error, if type is 'negative'
 */
export function notifyWithActions(
  type,
  icon = 'check_circle',
  timeout = 5,
  html = false,
  notifyMsg = "Data is saved successfully.",
  actions = [],
  onDismissCb = null,
) {
  const warning = type === "warning"
  const notifyObj = {
    icon,
    message: notifyMsg,
    color: type,
    textColor: warning ? "black" : "white",
    position: "bottom",
    timeout: timeout * 1000,
    actions: [
      {
        label: "Dismiss",
        color: "white",
      },
      ...actions
    ],
    html: html,
  }
  if (onDismissCb !== null) {
    Object.assign(notifyObj, {
      onDismiss: onDismissCb,
    })
  }
  Notify.create(notifyObj)
}

export function notifyWithHelpDeskInfo(type, icon, msg, timeout) {
  const actions = [
    {
      label: 'Open help-desk',
      color: 'white',
      handler: () => {
        //TODO test on tablet PWA - must open new tab/window, not override current tab/window (have tested on tablet browser and desktop browser and PWA, as local-built Docker image cannot have PWA installed)
        window.open('https://emsa.tern.org.au/helpdesk', '_blank')
      },
      noDismiss: true,
    },
  ]
  notifyWithActions(
    type,
    icon,
    timeout,
    true,
    `${msg} | 
    <a target="_blank" href="https://emsa.tern.org.au/helpdesk" style="font-weight: bold;">
      HELP-DESK
    </a>`,
    actions,
  )
}

export function degToRad(deg) {
  return (deg * Math.PI) / 180
}

/**
 * Calculates the orientation between two points on a map.
 *
 * @param {Object} point1 - The first point on the map containing latitude and longitude.
 * @param {Object} point2 - The second point on the map containing latitude and longitude.
 * @return {number} The orientation between the two points in radians.
 */
export function getMapOrientation(point1, point2) { 
  let { lat: lat1, lng: lng1 } = point1
  let { lat: lat2, lng: lng2 } = point2
  lat1 = degToRad(lat1)
  lat2 = degToRad(lat2)
  lng1 = degToRad(lng1)
  lng2 = degToRad(lng2)

  const dLng = lng2 - lng1

  const y = Math.sin(dLng) * Math.cos(lat2)
  const x =
    Math.cos(lat1) * Math.sin(lat2) -
    Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLng)

  const brng = Math.atan2(y, x)
  return brng
}

/**
 * Get the destination point given a starting point, bearing, and distance.
 *
 * @param {Object} point1 - The starting point coordinates { lat, lng }.
 * @param {number} brng - The bearing in radians.
 * @param {number} d - The distance in meters.
 * @returns {Object} The destination point coordinates { lat, lng }.
 */
export function getDestinationPoint(point1, brng, d) {
  // Convert lat and lng to radians
  let { lat: lat1, lng: lng1 } = point1
  lat1 = degToRad(lat1)
  lng1 = degToRad(lng1)

  // Earth's radius in meters
  let R = 6371e3

  // Calculate the latitude of the destination point
  let lat = Math.asin(
    Math.sin(lat1) * Math.cos(d / R) +
      Math.cos(lat1) * Math.sin(d / R) * Math.cos(brng),
  )

  // Calculate the longitude of the destination point
  let lng =
    lng1 +
    Math.atan2(
      Math.sin(brng) * Math.sin(d / R) * Math.cos(lat1),
      Math.cos(d / R) - Math.sin(lat1) * Math.sin(lat),
    )

  // Convert lat and lng back to degrees
  lat = (lat * 180) / Math.PI
  lng = (lng * 180) / Math.PI

  // Return the destination point coordinates
  return { lat, lng }
}

/**
 *
 * @param {*} point1 [lat,lng] point 1 and point 2 is a line which called line A
 * @param {*} point2 [lat,lng]
 * @param {*} point3 [lat,lng]
 * @param {*} point4 [lat,lng] point 3 and point 4 is a line which called line B
 * @returns the point coordinate of A and B
 */

export function getSubPlot(vegPlot, ratio1, ratio2) {
  const resultPoints = []
  vegPlot.forEach((point, index) => {
    const nextIndex = (index + 1) % vegPlot.length
    const latDiff = vegPlot[nextIndex][0] - point[0]
    const lngDiff = vegPlot[nextIndex][1] - point[1]

    const point1 = {
      lat: point[0] + latDiff * ratio1,
      lng: point[1] + lngDiff * ratio1,
    }

    const point2 = {
      lat: point[0] + latDiff * (ratio1 + ratio2),
      lng: point[1] + lngDiff * (ratio1 + ratio2),
    }

    resultPoints.push(point1, point2)
  })
  return resultPoints
}

export function getIntersectionPoint(point1, point2, point3, point4) {
  const slope1 = (point2.lat - point1.lat) / (point2.lng - point1.lng)
  const slope2 = (point4.lat - point3.lat) / (point4.lng - point3.lng)
  
  // Check if the slope is 0 or infinity (vertical to the x-axis
  if (
    isNaN(slope1) ||
    isNaN(slope2) ||
    Math.abs(slope1) === Infinity ||
    Math.abs(slope2) === Infinity
  ) {
    // When the two line segments are parallel or vertical, use different calculation methods
    if (isNaN(slope1) || Math.abs(slope1) === Infinity) {
      // When the line segment formed by point1 and point2 is vertical, use point1's longitude as the reference.
      const lng = point1.lng
      const lat =
        ((point4.lat - point3.lat) * (lng - point3.lng)) /
          (point4.lng - point3.lng) +
        point3.lat
      return { lat, lng }
    } else {
      // When the line segment formed by point3 and point4 is vertical, use point3's longitude as the reference.
      const lng = point3.lng
      const lat =
        ((point2.lat - point1.lat) * (lng - point1.lng)) /
          (point2.lng - point1.lng) +
        point1.lat
      return { lat, lng }
    }
  }

  const lng =
    ((point2.lng - point1.lng) *
      (point4.lng * point3.lat - point3.lng * point4.lat) -
      (point4.lng - point3.lng) *
        (point2.lng * point1.lat - point1.lng * point2.lat)) /
    ((point2.lat - point1.lat) * (point4.lng - point3.lng) -
      (point4.lat - point3.lat) * (point2.lng - point1.lng))

  const lat =
    ((point4.lat - point3.lat) * lng +
      point4.lng * point3.lat -
      point3.lng * point4.lat) /
    (point4.lng - point3.lng)

  return { lat, lng }
}

export function findMidpoint(point1, point2) {
  const {geometry} = turf.midpoint([point1.lng, point1.lat], [point2.lng, point2.lat])
  return {lat : geometry.coordinates[1], lng : geometry.coordinates[0]}
}

export function warnImplementation(msg = null) {
  let message = 'Not Yet Implemented'
  if (msg !== null) {
    message += `. ${msg}`
  }
  notifyHandler('warning', message)
  paratooWarnMessage(message)
}

/**
 * Refresh assets, speciesLists etc.
 *
 * @param {String} resourceType - type of the resource e.g. SPECIES_LIST or DOCUMENTATION.
 * @param {Boolean} isReload - flag to reload resource.
 */
export async function refreshLocalResources(resourceType, isReload = false) {
  const isOnline = useEphemeralStore().networkOnline
  if (!isOnline) return

  let result = null
  const documentationStore = useDocumentationStore()
  switch (resourceType) {
    case resourceTypes.ANZSRC_FIELDS_OF_RESEARCH:
      // Clear map tiles
      result = indexedDB.deleteDatabase('dexieDatabase')
      result.onerror = () => {
        console.error('Error deleting dexie database.')
      }

      result.onsuccess = () => {
        console.log('anzsrc fields of research deleted successfully')
        // clear map-tiles
        for (const c of document.cookie.split(';')) {
          if (c.includes('anzsrc-fields-of-research')) {
            document.cookie =
              c.trim().split('=')[0] +
              '=;' +
              'expires=Thu, 01 Jan 1970 00:00:00 UTC;'
          }
        }
        if (isReload) location.reload(true)
      }
      break

    case resourceTypes.GCMD_EARTH_SCIENCE_KEYWORDS:
      // Clear map tiles
      result = indexedDB.deleteDatabase('dexieDatabase')
      result.onerror = () => {
        console.error('Error deleting dexie database.')
      }

      result.onsuccess = () => {
        console.log('gcmd earth science keywords deleted successfully')
        // clear map-tiles
        for (const c of document.cookie.split(';')) {
          if (c.includes('gcmd-earth-science-keywords')) {
            document.cookie =
              c.trim().split('=')[0] +
              '=;' +
              'expires=Thu, 01 Jan 1970 00:00:00 UTC;'
          }
        }
        if (isReload) location.reload(true)
      }
      break
    case resourceTypes.SPECIES_LIST:
      // Clear dexieDatabase
      result = indexedDB.deleteDatabase('dexieDatabase')
      result.onerror = () => {
        console.error('Error deleting dexie database.')
      }

      result.onsuccess = () => {
        console.log('dexieDatabase deleted successfully')
        // clear species-lists
        for (const c of document.cookie.split(';')) {
          if (c.includes('species-lists')) {
            document.cookie =
              c.trim().split('=')[0] +
              '=;' +
              'expires=Thu, 01 Jan 1970 00:00:00 UTC;'
          }
        }
        if (isReload) location.reload(true)
      }

      break
    case resourceTypes.MAP_TILES:
      // Clear map tiles
      result = indexedDB.deleteDatabase('_pouch_offline-tiles')
      result.onerror = () => {
        console.error('Error deleting pouch database.')
      }

      result.onsuccess = () => {
        console.log('map tiles deleted successfully')
        // clear map-tiles
        for (const c of document.cookie.split(';')) {
          if (c.includes('map-tiles')) {
            document.cookie =
              c.trim().split('=')[0] +
              '=;' +
              'expires=Thu, 01 Jan 1970 00:00:00 UTC;'
          }
        }
        if (isReload) location.reload(true)
      }
      break
    case resourceTypes.METHOD_PROGRESS_FLAGS: {
        // Clear method progress flags
        for (const c of document.cookie.split(';')) {
          if (c.includes('current_status') || c.includes('last_processed')) {
            document.cookie =
              c.trim().split('=')[0] +
              '=;' +
              'expires=Thu, 01 Jan 1970 00:00:00 UTC;'
          }
        }
        
        // clear localstorage
        let keys = []
        // Iterate over localStorage and insert the keys
        for (let i = 0; i < localStorage.length; i++){
          const key = localStorage.key(i)
          if (key.includes('current_status') || key.includes('last_processed')) {
            keys.push(key)
          }
        }
        // as removing element while iterating is unsafe
        keys.forEach((key) => {
          localStorage.removeItem(key)
        })
        paratooSentryBreadcrumbMessage('deleted all method progress flags successfully')
        break
      }
    case resourceTypes.DOCUMENTATION:
      // Clear documentation
      await documentationStore.clearDocumentation()
      if (isReload) {
        const doc = await documentationStore.downloadDocumentation()
        if (doc) notifyHandler('positive', 'Documentation is up to date')
      }
      break
    case resourceTypes.LOCAL_STORAGE:
      // Clear local storage
      localStorage.clear()
      if (isReload) location.reload(true)
      break
    case resourceTypes.SESSION_STORAGE:
      // Clear local storage
      sessionStorage.clear()
      if (isReload) location.reload(true)
      break
    case resourceTypes.PUBLIC_ASSETS:
      // service can detect any new changes(if it finds any new manifest.json).
      // so just reloading the page is enough.
      if (isReload) location.reload(true)
      break
    case resourceTypes.ALL:
      for (const type of Object.keys(resourceTypes)) {
        if (type == resourceTypes.ALL) continue
        if (type == resourceTypes.PUBLIC_ASSETS) continue
        await refreshLocalResources(type)
      }
      // delete all cookies
      for (const c of document.cookie.split(';')) {
        document.cookie =
          c.trim().split('=')[0] +
          '=;' +
          'expires=Thu, 01 Jan 1970 00:00:00 UTC;'
      }
      if (isReload) location.replace('/login')
      break
    default:
      break
  }
  return
}

// extract current doc version
export function currentDocVersion(documentation) {
  let docFulVersion = '000-XXX'
  if (!documentation?.info?.description) {
    paratooWarnMessage('Description not found in documentation info')
    return docFulVersion
  }

  // format: Build: development-Version: 0.0.0-GitHash: xxxx
  let description = documentation.info.description

  let docBuildParams
  switch (true) {
    // new format: Build: development-Version: 0.0.0-GitHash: xxxx
    case description.includes('-Version'):
      docBuildParams = description.split('-')
      docFulVersion = `${docBuildParams[1].split(' ')[1]}-${
        docBuildParams[2].split(' ')[1]
      }`
      break
    // old format: Build: development Version: 0.0.0 GitHash: xxxx (deprecated)
    // Note: if old doc exists
    case description.includes(' Version'):
      docBuildParams = description.split(' ')
      docFulVersion = `${docBuildParams[3]}-${docBuildParams[5]}`
      break
    default:
      paratooWarnMessage(
        `Invalid swagger documentation, Info: ${JSON.stringify(
          documentation.info,
        )}`,
      )
      break
  }
  return docFulVersion
}

export async function checkDocVersion(documentation) {
  const isOnline = useEphemeralStore().networkOnline
  if (!isOnline) return documentation

  paratooSentryBreadcrumbMessage('Checking documentation version')

  const dataManager = useDataManagerStore()
  const authStore = useAuthStore()
  let docFulVersion = currentDocVersion(documentation)

  const content = await doCoreApiGet(
    { urlSuffix: `${coreApiPrefix}${docBuildInfo}` },
    { root: true },
  )
  if (!content && !documentation) return null
  if (!content) return documentation

  const coreVersion = `${content['Version']}-${content['GitHash']}`
  if (docFulVersion == coreVersion) {
    return documentation
  }
  paratooSentryBreadcrumbMessage(`Swagger documentation outdated! found: ${docFulVersion}, current-core: ${coreVersion}`)

  // if in-progress collections exist in publicationQueue we wont refresh docco
  if (dataManager.publicationQueue.length != 0) {
    paratooSentryBreadcrumbMessage(
      'skip refreshing swagger documentation due to in-progress collections exist in publicationQueue',
    )
    return documentation
  }
  return null
}

// we are not going to hit network for all urls if users offline
export function approvedUrlsForOffline(url) {
  if (useEphemeralStore().networkOnline) return true

  // in offline, we ignore all the requests to core and org except swagger
  if (url.includes('swagger')) return true
  if (url.includes('amazonaws.com')) return false
  if (url.includes(coreApiUrlBase)) return false
  if (url.includes(orgApiUrlBase)) return false
}

/**
 *
 * @param {*} type positive | negative | warning | info | ongoing
 * @param {*} msg
 * @param {*} err
 * @param {*} timeout default 5s
 * @param {*} position "center" | "left" | "right" | "top" | "top-left" | "top-right" | "bottom-left" | "bottom-right" | "bottom"
 * @param {*} html whether the notifications `msg` contains HTML to render
 * @param {Boolean} force whether to ignore the `notificationState` and force showing
 */
export function notifyHandler(
  type,
  msg,
  err,
  timeout = 5,
  position = 'bottom',
  html = false,
  force = false,
) {
  const ephemeralStore = useEphemeralStore()
  const notificationTypes = [
    'positive',
    'negative',
    'warning',
    'info',
    'ongoing',
  ]
  if (!notificationTypes.includes(type)) {
    let msg = `notifyHandler was provided with invalid 'type': ${type}`
    type = 'info' //no valid type provided, default to info
    return paratooWarnMessage(msg)
  }
  if (type === 'ongoing') {
    let msg =
      '`ongoing` `type` provided to notifyHandler, which is not yet supported'
    return paratooWarnMessage(msg)
  }
  //replace the last fullstop - if there's more than one, just using `String.replace()`
  //will replace the first (i.e., middle of the whole string between two sentences)
  let trimmedMsg = msg
    //reverse the string
    .split('')
    .reverse()
    .join('')
    //replace last occurrence of fullstop
    .replace('.', '')
    //reverse the string again
    .split('')
    .reverse()
    .join('')
  let message
  if (err) {
    if (err?.message) {
      message = trimmedMsg + '. ' + err.message
    } else {
      message = trimmedMsg + '. ' + err
    }
  } else {
    message = trimmedMsg
  }
  // we only want one type of notification active at a time
  // so the UI won't be flooded with too many
  let timeoutId

  if (
    !force &&
    (type === 'negative' && ephemeralStore.notificationState['negative'])
  ) {
    return
  } else {
    ephemeralStore.notificationState[type] = true
    timeoutId = setTimeout(() => {
      ephemeralStore.notificationState[type] = false
    }, timeout * 1000)
  }
  Notify.create({
    type: type,
    message: message,
    position: position,
    actions: [
      {
        label: 'Dismiss',
        color: 'white',
      },
    ],
    timeout: timeout * 1000,
    onDismiss: () => {
      ephemeralStore.notificationState[type] = false
      clearTimeout(timeoutId)
    },
    html: html,
  })
}

/**
 * Plays a sound based on the give sound param
 *
 * @param {} sound a sound clip, link or mp3/wav
 */
export function playSound(sound) {
  if (!sound) return

  try {
    const audio = new Audio(sound)
    audio.play()
  } catch (error) {
    paratooWarnHandler(
      'Warning - detected an exception when playing out a sound',
      error,
    )
  }
}

//simple method to save cookie
export function saveCookie(key, value) {
  //generate expires
  const d = new Date()
  d.setTime(d.getTime() + 3650 * 24 * 60 * 60 * 1000)
  let expires = 'expires=' + d.toUTCString()

  // save cookie
  const cookie =
    webAppCookiePrefix + key + '=' + value + '; ' + expires + '; path=/'
  document.cookie = cookie

  return cookie
}

//simple method to retrive cookie
export function getCookie(key) {
  var match = document.cookie.match(
    new RegExp('(^| )' + webAppCookiePrefix + key + '=([^;]+)'),
  )
  if (match) {
    return match[2]
  }

  return ''
}

//simple method to delete cookie
export function deleteCookie(key) {
  // simple hack to delete cookie
  const cookie =
    webAppCookiePrefix + key + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/'
  document.cookie = cookie
}

export function paratooErrorHandler(msg, err) {
  console.error(msg, err || '(warn - no error object passed to the handler)')
  const processedError = chainedError(msg, err)

  Sentry.withScope(function (scope) {
    if (err && err.httpStatus) {
      scope.setTag('http-status', err.httpStatus)
    }
    Sentry.captureException(processedError)
  })

  return processedError
}

// for errors that are only warnings
export function paratooWarnHandler(msg, err) {
  console.warn(msg, err || '(no error object passed to the handler)')
  Sentry.withScope((scope) => {
    scope.setLevel('warning')
    Sentry.captureException(chainedError(msg, err))
  })
}

/**
 * Helper to change the order of the fields following the provided pattern
 *
 * @param {object} modelConfig - schema object
 * @param {Array} pattern - string array with fields
 *
 * @returns {object} reordered schema object
 */
export function changeModelConfigFieldOrder(
  modelConfig,
  pattern,
  suppressWarnings = false,
) {
  const sortedConfig = Object.fromEntries(
    Object.entries(modelConfig.fields).sort(
      ([keyA], [keyB]) => pattern.indexOf(keyA) - pattern.indexOf(keyB),
    ),
  )
  if (
    Object.keys(sortedConfig).length !== pattern.length &&
    !suppressWarnings
  ) {
    paratooWarnMessage(
      `The fields in the model config are different from the pattern sortedConfig: ${JSON.stringify(
        sortedConfig,
      )} pattern: ${pattern}`,
    )
  }
  return sortedConfig
}

/**
 * Helper function to find the max value of array of objects and
 * generate barcode replicate number
 *
 * @param {Array} arrObject - an array of object
 * @param {string} key - key to find max value
 *
 * @returns {number} - max value
 */
export function generateReplicateFromArrMax(arrObject, key) {
  if (!arrObject || arrObject.length == 0) {
    return 1
  }
  try {
    // existing replicate number for this floristics voucher, so find highest + 1
    // https://stackoverflow.com/a/4020842
    let maxNumber = Math.max.apply(
      Math,
      arrObject.map((o) => {
        return o[key]
      }),
    )

    // if maxReplicateNumber is greater than zero
    if (maxNumber > 0) {
      return maxNumber + 1
    }

    // or any other cases it should be one
    return 1
  } catch (error) {
    return 1
  }
}

// for warn messages, send errors to paratooWarnHandler
export function paratooWarnMessage(msg) {
  console.warn(msg)
  Sentry.withScope(function (scope) {
    scope.setLevel('warning')
    Sentry.captureMessage(msg)
  })
}

// Generic message handler
// for info messages - for dev/test, console is enough and for prod, we only want to report warns/errors
export function paratooInfoMessage(msg) {
  if (!isProduction) {
    console.info(msg)
  }
}

// Sentry breadcrumb message handler for in production testing @beta
export function paratooSentryBreadcrumbMessage(msg, category='breadcrumb') {
  if (!isProduction) {
    //use `debug` as the `msg` can often be very long, crowding the console
    console.debug(msg)
  } else {
    Sentry.addBreadcrumb({
      category: category,
      message: msg,
      level: 'debug',
    })
  }
}

// Generic message handler
// for debug messages - for dev/test, console is enough and for prod, we only want to report warns/errors
export function paratooDebugMessage(msg) {
  if (!isProduction) {
    console.info(msg)
  }
}

// a function to checks the browser is white-listed or not at runtime
// notify user and also report the event to sentry
export function supportedBrowserCheck() {
  //hard refreshes in dev mode will cause this to constantly trigger, so skip
  if (process.env.DEV || Capacitor.isNativePlatform()) return

  // dont need to show every time
  const alreadyShown = getCookie('browser_status')
  if (alreadyShown) return

  saveCookie('browser_status', 'shown')

  if (supportedBrowsers.test(navigator.userAgent)) {
    notifyHandler(
      'positive',
      'Nice.. Your browser is known to work with this Application',
    )
    return
  }
  paratooWarnMessage('The browser is not white-listed')
}

// According to issue 163
// If schemaTitle is 'Plot Location' then title will be 'Plot Description'
export function createTitle(schemaTitle) {
  switch (schemaTitle) {
    case 'Floristics Veg Genetic Voucher Survey':
      return 'Plant Tissue Vouchering Survey'
    default:
      return schemaTitle
  }
}

export function chainedError(msg, err) {
  if (!err) {
    return new Error(
      `${msg}\nWARNING: chainedError was called without an error to chain`,
    )
  }
  if (typeof err === 'object') {
    // TODO can we detect and handle a ProgressEvent and get details from
    // err.target.error.code (and lookup name of error by getting key for the
    // code on .error)
    const newMsg = `${msg}\nCaused by: ${err.message}`
    if (isImmutableError(err)) {
      // we can't construct a new DOMException because support for the
      // constructor isn't great.
      return new Error(
        newMsg +
          `\nOriginal stack (immutable original error forced ` +
          `creation of a new Error with new stack):\n${err.stack}`,
      )
    }
    try {
      err.message = newMsg
      return err
    } catch (err2) {
      // We get here by trying to modify an immutable Error. DOMException seems
      // to always be but there may be others.
      console.warn(
        `While handling the first error:\n` +
          `  [name=${err.name}, type=${(err.constructor || {}).name}] ${
            err.message || '(no message)'
          }\n` +
          `encountered this error:\n` +
          `  ${err2.message}\n` +
          `but we're working around it! Bubbling original error now.`,
      )
      return new Error(
        newMsg +
          `\nOriginal stack (readonly Error.message forced ` +
          `creation of a new Error with new stack):\n${err.stack}`,
      )
    }
  }
  return new Error(`${msg}\nCaused by: ${err}`)
}

function isImmutableError(err) {
  return self.DOMException && err instanceof DOMException
}

export function capitalise(s) {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}

export function pluralise(s) {
  if (typeof s !== 'string') return ''
  return `${s}s`
}

// get values based on storage types e.g. cookie or localstorage
export function getFlagValue(storageTag, storageType=storageTypes.COOKIE) {
  switch (storageType) {
    case storageTypes.COOKIE:
      return getCookie(storageTag)
    case storageTypes.LOCALSTORAGE:
      return localStorage.getItem(storageTag)
    default:
      return localStorage.getItem(storageTag)
  }
}

// store values based on storage types e.g. cookie or localstorage
export function storeFlagValue(storageTag, value, storageType=storageTypes.COOKIE) {
  switch (storageType) {
    case storageTypes.COOKIE:
      return saveCookie(storageTag, value)
    case storageTypes.LOCALSTORAGE:
      return localStorage.setItem(storageTag, value)
    default:
      return localStorage.setItem(storageTag, value)
  }
}

// update method progress flags
export function setMethodProgressFlagsDone(methodName, storageType=storageTypes.COOKIE) {
  paratooSentryBreadcrumbMessage(`${methodName} is completed, updating all status cookies`)
  const currentTime = Date.now()
  const currentStatusTag = `current_status_${methodName}`
  const lastProcessedTimeTag = `last_processed_time_${methodName}`

  storeFlagValue(currentStatusTag, 'done', storageType)
  storeFlagValue(lastProcessedTimeTag, currentTime, storageType)
}

/**
 * Checks if the method is ready to process further based on flags
 * 
 * @param {Bool} concurrentProcessing method is allowed to process concurrently e.g. authRefreshToken 
 * @param {Int} waitTime wait certain amount of time to process again e.g. refreshTokenAndApiListsModels 
 * @param {String} storageType where to store values e.g. storageTypes.COOKIE or storageTypes.LOCALSTORAGE
 *
 * @returns {Object} status with isReady flag
 */
export function isMethodReadyToContinue(
  methodName, 
  concurrentProcessing=false, 
  waitTime=0, 
  storageType=storageTypes.COOKIE
) {

  const status = {
    method: methodName,
    isReady: true,
    msg: 'Method is ready to continue',
    err: null,
  }
  const currentTime = Date.now()

  const skipMsg = `Skipping processing method: ${methodName}`
  const skipReasons = []

  const currentStatusTag = `current_status_${methodName}`
  const lastProcessedTimeTag = `last_processed_time_${methodName}`

  try {
    // stops processing if the method is busy 
    if (!concurrentProcessing) {
      const currentStatus = getFlagValue(currentStatusTag, storageType)
      switch (currentStatus) {
        case 'processing':
          skipReasons.push('currently processing another request')
          break
        default:
          break
      }
    }

    // wait for some time to process heavy request again 
    if (waitTime > 0) {
      const lastProcessed = getFlagValue(lastProcessedTimeTag, storageType)
      if (lastProcessed) {
        // in minutes
        const diff = (currentTime - lastProcessed) / (1000 * 60) 
        status.msg += `, last processed ${parseInt(diff)} minutes ago` 

        if (diff < waitTime) {
          skipReasons.push(`waiting ${parseInt(waitTime)} minutes to process again`)
        }
      }
    }

    // method is not ready to process further if one or multiple reasons found
    if (skipReasons.length > 0) {
      status.isReady = false
      status.msg = `${skipMsg}, reasons: ${skipReasons.join(', ')}`
    }

    // update current flags if method is ready to continue
    // NOTE: current status flag 'done' needs to be set once processing is done use `setMethodProgressFlagsDone()'
    if (status.isReady) {
      storeFlagValue(currentStatusTag, 'processing', storageType)
      storeFlagValue(lastProcessedTimeTag, currentTime, storageType)
    }
  }
  catch(err) {
    status.err = err
    status.msg = skipMsg
    status.isReady = false
  }

  return status
}

// refresh and store ip address
export async function refreshAndStoreHostInfo() {
  const isOnline = useEphemeralStore().networkOnline
  if (!isOnline) return

  const authStore = useAuthStore()
  // one of ways to get all host info
  // see https://ipdata.co/blog/how-to-get-the-ip-address-in-javascript/
  const resp = await axios.get('https://www.cloudflare.com/cdn-cgi/trace')
  if (!resp.data) return
  const fields = resp.data.split('\n')
  const data = {}
  fields.forEach((field) => {
    // ip address
    if (field.includes('ip')) {
      // ip=129.127.180.3
      data['ip'] = field.split('=')[1]
    }
    // agent info
    if (field.includes('uag')) {
      // uag=Mozilla/5.0 (X11; Linux x86_64)
      data['agent'] = field.split('=')[1]
    }
  })
  authStore.setHostInfo(data)
}

//generate survey ID - uniquely identifies a collection (a survey and its observations)
export function generateSurveyId(surveyType, documentation) {
  const authStore = useAuthStore()
  const hostInfo = authStore.getHostInfo
  const ipAddress = hostInfo ? hostInfo.ip : null
  const agent = hostInfo ? hostInfo.agent : null
  const appVersion = `${build.paratoo_webapp_version}-${build.git_hash}`
  const coreDocVersion = currentDocVersion(documentation)

  const systemInfo = `${
    enableOIDC ? OIDCClient.replace('-', ' ') : 'monitor webApp'
  }-${process.env.VUE_APP_DEPLOYED_ENV_NAME.replace('-', ' ')}-${
    ipAddress ? ipAddress : '0.0.0.0'
  }-${agent ? agent.replace('-', ' ') : ''}`

  const currContextProt = resolveProtocolAuthContext({
    authProtocolContext: authStore.currentContext.protocol,
    surveyModel: surveyType,
  })
  return {
    survey_details: {
      survey_model: surveyType,
      time: new Date(Date.now()),
      uuid: uuidv4(),
      project_id: authStore.currentContext.project.toString(),
      protocol_id: currContextProt.id,
      protocol_version: currContextProt.version.toString(),
      submodule_protocol_id: currContextProt.submoduleProtocolId || '',
    },
    provenance: {
      version_app: appVersion,
      version_core_documentation: coreDocVersion,
      system_app: systemInfo,
    },
    org_opaque_user_id: authStore.userProfile?.id.toString(),
  }
}

/**
 * Creates a protocol auth context from the auth store's stored context. Takes into
 * consideration submodules and resolves protocol IDs to UUIDs
 *
 * @param {Object} authProtocolContext the protocol context from the auth store
 * @param {String} surveyModel the model name of the survey
 *
 * @returns {Object} the new protocol context with resolved UUID
 */
export function resolveProtocolAuthContext({
  authProtocolContext,
  surveyModel,
}) {
  const apiModelsStore = useApiModelsStore()
  const bulkStore = useBulkStore()

  const submoduleInfo = bulkStore.doingSubmoduleMeta.find(
    //`authProtocolContext` will always be the parent module if we're doing the a
    //submodule collection
    (o) =>
      o.parentModuleProtocolId === authProtocolContext.id &&
      //need to check against the survey model, as if we're the parent survey we'll end up
      //setting it's protocol ID to the submodule's protocol ID, so only want a match when
      //the model name matches the submodule
      o.submoduleProtocolInfo.workflow.some((o) => o.modelName === surveyModel),
  )

  let resolvedUuid = null
  let isPlaceholderSubmodule = null
  if (submoduleInfo) {
    const submoduleProtocolEntry = apiModelsStore.protocolInformation({
      protId: submoduleInfo.submoduleProtocolId,
    })

    if (submoduleProtocolEntry.isHidden && !submoduleProtocolEntry.isWritable) {
      //special case (currently just fire+cover, but can be extended).
      //this submodule's protocol entry is a placeholder (is hidden), so we need to rely
      //on parent module
      resolvedUuid = apiModelsStore.protUuidFromId(authProtocolContext.id)
      isPlaceholderSubmodule = true
    } else {
      //we're resolving for a submodule
      resolvedUuid = apiModelsStore.protUuidFromId(
        submoduleInfo.submoduleProtocolId,
      )
    }
  } else {
    resolvedUuid = apiModelsStore.protUuidFromId(authProtocolContext.id)
  }

  let currContextProt = cloneDeep(authProtocolContext)
  currContextProt.id = resolvedUuid.uuid
  currContextProt.version = resolvedUuid.version
  if (isPlaceholderSubmodule) {
    //will allow us to get a handle on the placeholder submodule (e.g., the 'fire survey'
    //for cover+fire). Useful, for example, when doing `/reverse-lookup` and need to grab
    //the fire survey data from the 'cover+fire' submodule
    currContextProt.submoduleProtocolId = apiModelsStore.protUuidFromId(
      submoduleInfo.submoduleProtocolId,
    )?.uuid
  }

  return currContextProt
}

/**
 * Generates an observation ID in the format <prefix><digits><postfixDateTime>
 *
 * @param {Number} length the length of the digits
 * @param {String} prefix the prefix string to use
 * @param {Boolean} postfixDate whether to postfix the datetime
 *
 * @returns {String} the genrated ID
 */
export function generateObservationId(length, prefix, postfixDate = false) {
  // prepend leading zeroes
  const zeroPad = (num, places) => String(num).padStart(places, '0')
  let ID = prefix + zeroPad(length, 3)

  if (postfixDate) {
    //append a datetime formatted as YYYYMMDDHHMMSS
    const dateTimeNow = new Date()
    const year = zeroPad(dateTimeNow.getFullYear(), 4)
    const month = zeroPad(dateTimeNow.getMonth() + 1, 2)
    const day = zeroPad(dateTimeNow.getDate(), 2)
    const hour = zeroPad(dateTimeNow.getHours(), 2)
    const minute = zeroPad(dateTimeNow.getMinutes(), 2)
    const second = zeroPad(dateTimeNow.getSeconds(), 2)
    ID += '-' + year + month + day + hour + minute + second
  }

  return ID
}

export function isUrlReadyToContinue(
  url,
  includeBearerToken,
  authStore,
  options,
  publicApi=false
) {
  const result = {}
  result['isReady'] = true
  const api = url.replace(coreApiUrlBase, '').replace(orgApiUrlBase, '')
  const isPublic = isPublicApi(url) ? !publicApi : publicApi
  result['apiType'] =  isPublic ? 'public' : 'private'
  result['tokenExpired'] = isTokenExpired(authStore?.token)
  result['refreshTokenExpired'] = isTokenExpired(authStore?.refreshToken)

  if (!url || url.includes('undefined')) {
    result['isReady'] = false
    result[
      'msg'
    ] = `Supplied URL=${api} does NOT look valid, refusing to continue`
  }
  // if offline, we don't need to hit the network all the time.
  if (!useEphemeralStore().networkOnline && !approvedUrlsForOffline(url)) {
    result['isReady'] = false
    result['msg'] = `skip hitting network as user is offline url: ${api}`
  }
  // if token not found
  if (includeBearerToken && !authStore.token) {
    result['isReady'] = false
    result['msg'] = `No auth token found url: ${api}`
  }
  // if both token got expired
  if (!isPublic && isSessionDestroyed()) {
    result['isReady'] = false
    result['msg'] = 'skip hitting network, reason: both token & refresh token are expired'
  }

  // skip hitting private api if no token exists
  if (!isPublic && !authStore?.token) {
    result['isReady'] = false
    result['msg'] = `No user exists, skip calling api url: ${api}`
  }

  if (options['x-static-resource']) {
    result['isReady'] = true
    delete result['msg']
  }

  // paratooSentryBreadcrumbMessage(`url: ${url}, status: ${JSON.stringify(result, null, 2)}`)
  return result
}

// checks public or private
export function isPublicApi(url) {
  const publicApis = [
    login,
    heartbeat,
    validateToken,
    tokenRefresh,
    'amazonaws.com', // static resources
  ]
  for (const p of publicApis) {
    if (!url.includes(p)) continue
    return true
  }

  return false
}

export function getRequestInfo(url, init) {
  const requestInfo = {}
  // body
  requestInfo['body'] = init.body ? JSON.parse(init.body) : null
  // action
  requestInfo['action'] = url
    .replace(orgApiUrlBase, '')
    .replace(orgCustomApiPrefix, '')
    .replace(orgApiPrefix, '')
    .replace(coreApiUrlBase, '')
    .replace(coreApiPrefix, '')
    .replaceAll('/', ' ')
    .replaceAll('-', ' ')
    .replace(' bulk', '')
    .replace(' many', '')
    .replace(' user.json', '')
  if (url.includes('?'))
    requestInfo['action'] = requestInfo['action'].split('?')[0]

  // method
  requestInfo['method'] = 'get'
  if (url.includes(coreApiUrlBase)) {
    requestInfo['source'] = 'Core'
    if (!url.includes('swagger')) requestInfo['method'] = 'populate'
    if (init.method == 'POST') requestInfo['method'] = 'publish'
  }
  if (url.includes(orgApiUrlBase)) {
    requestInfo['source'] = 'Org'
    if (init.method == 'POST') requestInfo['method'] = 'post'
  }
  return requestInfo
}

export async function customErrorHandler(url, init, response,offlinePublicationQueueIndex = null) {
  let result = {}
  let apiResult = null
  if (!response.status) return result

  // handle errors generated in org
  if (url.includes(orgApiUrlBase)) {
    apiResult = await customErrorHandlerOrg(url, init, response)
    result = {
      status: response.status,
      ...apiResult,
    }
  }
  // handle errors generated in core
  if (url.includes(coreApiUrlBase)) {
    apiResult = await customErrorHandlerCore(url, init, response,offlinePublicationQueueIndex)
    result = {
      status: response.status,
      ...apiResult,
    }
  }

  return result
}

export async function customErrorHandlerCore(url, init, response,offlinePublicationQueueIndex = null) {
  let msg = ''
  let data = null
  const ephemeralStore = useEphemeralStore()
  const requestInfo = getRequestInfo(url, init)
  const result = {}

  result['handleJsonResp'] = true
  result['throwError'] = false
  result['requestInfo'] = requestInfo
  result['status'] = response.status

  const overrideMapper = {
    'reverse-lookup': {
      method: 'fetch',
    },
  }
  for (const [endpoint, overrideData] of Object.entries(overrideMapper)) {
    if (url.includes(endpoint)) {
      for (const [requestInfoKey, requestInfoOverride] of Object.entries(overrideData)) {
        result.requestInfo[requestInfoKey] = requestInfoOverride
      }
    }
  }

  switch (true) {
    // validation error
    case response.status == 400:
      data = await response.json()
      console.log('data:', data)
      msg = `Failed to ${requestInfo.method} ${requestInfo.action}. ${
        data.error ? data.error.message : ''
      }`
      if(!offlinePublicationQueueIndex){
        notifyHandler('negative', msg)
      }
      result['msg'] = msg
      result['throwError'] = true
      result['error'] = data.error ? data.error.message : {}
      
      if (data?.error?.details?.errorCode) {
        result['errorCode'] = data.error.details.errorCode
      }
      break

    // forbidden
    case response.status == 403:
      data = await response.json()
      msg = `Failed to ${requestInfo.method} ${requestInfo.action}. ${
        data.error ? data.error.message : ''
      }`
      notifyHandler('negative', msg)
      result['msg'] = msg
      result['throwError'] = true
      result['error'] = data.error ? data.error.message : {}
      break

    // invalid token
    case response.status == 401:
      data = await response.json()
      msg = `Failed to ${requestInfo.method} ${requestInfo.action}. ${
        data.error ? data.error.message : ''
      } status: ${response.status}`
      //notifyHandler('warning', msg)
      paratooWarnMessage(msg)

      ephemeralStore.tokenHasExpired = true
      result['refreshAuthToken'] = true
      result['msg'] = msg
      result['throwError'] = false
      result['error'] = data.error ? data.error.message : {}
      break

    // if org is unavailable
    case response.status == 502:
    case response.status == 503:
      msg = `Failed to ${requestInfo.method} ${requestInfo.action}. Org is unavailable. status: ${response.status}`
      notifyHandler('negative', msg)
      result['msg'] = msg
      result['throwError'] = true
      result['handleJsonResp'] = false
      break

    case response.status == 500:
      // unknown error
      msg = `Failed to ${requestInfo.method} ${requestInfo.action}. An unknown error occurred`
      notifyHandler('negative', msg)
      result['msg'] = msg
      result['throwError'] = true
      break
    default:
      break
  }
  return result
}

export async function customErrorHandlerOrg(url, init, response) {
  const requestInfo = getRequestInfo(url, init)
  const ephemeralStore = useEphemeralStore()
  let msg = ''
  let data = null
  let survey_model = 'survey'
  const result = {}

  result['handleJsonResp'] = true
  result['throwError'] = false
  result['requestInfo'] = requestInfo
  result['status'] = response.status

  switch (true) {
    // auth/login
    case url.includes(login):
      // wrong user/password
      if (response.status == 400) {
        msg = 'Invalid user or password'
        notifyHandler('warning', msg)

        result['msg'] = msg
        result['handleJsonResp'] = false
        result['throwError'] = false
      }
      return result

    // api/validate-token
    case url.includes(validateToken):
      // wrong user/password
      if (response.status != 200) {
        notifyHandler('warning', 'Failed to validate token')
        result['msg'] = msg
      }
      return result

    // token/refresh
    case url.includes(tokenRefresh):
      if (response.status != 200) {
        notifyHandler(
          'warning',
          'Tried to automatically refresh token but failed',
        )
        result['msg'] = msg
      }
      return result

    // /mint-identifier
    case url.includes(mintIdentifier):
      if (requestInfo.body) {
        survey_model =
          requestInfo.body.survey_metadata.survey_details.survey_model
        survey_model = survey_model.replaceAll('-', ' ')
      }
      if (response.status == 403) {
        msg = `User is not authorised to mint ${survey_model} metadata`
        notifyHandler('negative', msg)
        result['msg'] = msg
      }
      if (response.status != 200) {
        msg = `Failed to mint ${survey_model} metadata`
        notifyHandler('negative', msg)
        result['msg'] = msg
      }
      return result

    default:
      if (response.status == 403) {
        msg = `User is not authorised to ${requestInfo.method} ${requestInfo.action}`
        notifyHandler('negative', msg)
        result['msg'] = msg
      }
      if (response.status == 401) {
        data = await response.json()
        msg = `Failed to ${requestInfo.method} ${requestInfo.action}. ${
          data.error ? data.error.message : ''
        } status: ${response.status}`
        // notifyHandler('warning', msg)
        paratooWarnMessage(msg)

        ephemeralStore.tokenHasExpired = true
        result['refreshAuthToken'] = true
        result['msg'] = msg
        result['throwError'] = true
        result['error'] = data.error ? data.error.message : {}
      }
      return result
  }
}

// checks valid resource or not
export function isValidResource(value) {
  if (!value) return false
  try {
    // JSON parse doest work as the file size is so huge
    const keys = Object.keys(value)
    return keys.length > 0
  } catch (error) {
    return false
  }
}

// if unique fields exist we update or store values in bulkStore
export async function updateAndStoreUniqueModelValues(
  modelNames,
) {
  if (!modelNames?.length) return
  paratooSentryBreadcrumbMessage(
    `Updating unique values for models: ${modelNames.join(', ')}`
  )
  const documentation = await useDocumentationStore().getDocumentation()
  if (!documentation?.info) return
  const selectedProject = useAuthStore().selectedProject.id.toString()

  const bulkStore = useBulkStore()
  const apiModelsStore = useApiModelsStore()
  const authStore = useAuthStore()

  // delete all existing
  setExistingBarcodes([])
  bulkStore.setTemporaryIndexId(null)

  const allUniqueFieldsForModels = {}
  for (const model of modelNames) {
    const allUniqueFields = allUniqueFieldsFromSchema(
      schemaFromModelName(model, documentation),
    )
    if (allUniqueFields?.root || allUniqueFields?.components) {
      allUniqueFieldsForModels[model] = allUniqueFields
    }
  }

  if (!isEmpty(allUniqueFieldsForModels)) {
    apiModelsStore
      .populateVerifiedModels({
        modelNames: Object.keys(allUniqueFieldsForModels).filter((m) =>
          authStore.getAllAuthorisedModels().includes(m),
        ),
        projectIds: [selectedProject],
        useCache: false,
      })
      .then(() => {
        for (const [uniqueFieldModel, uniqueFieldsForModel] of Object.entries(allUniqueFieldsForModels)) {
          storeUniqueFields(uniqueFieldModel, uniqueFieldsForModel)
        }
      })
  }
}

// store values in bulkStore
function storeUniqueFields(modelName, allUniqueFields) {
  if (!modelName) return
  const bulkStore = useBulkStore()
  const dataManager = useDataManagerStore()

  bulkStore.uniqueCollections[modelName] = {}
  bulkStore.uniqueCollections[modelName]['allFields'] = allUniqueFields
  bulkStore.uniqueCollections[modelName]['values'] = {}

  const data = dataManager.cachedModel({ modelName })
  const uniqueData = {}

  for (const value of data) {
    const fieldValues = getUniqueFieldValues(value, allUniqueFields)
    for (const field of Object.keys(fieldValues)) {
      if (!uniqueData[field]) uniqueData[field] = {}
      uniqueData[field] = {
        ...uniqueData[field],
        ...fieldValues[field],
      }
    }
  }

  bulkStore.uniqueCollections[modelName]['values'] = cloneDeep(uniqueData)
}

function getUniqueFieldValues(modelData, allUniqueFields) {
  let result = {}
  // can be repeatable component
  const data = Array.isArray(modelData) ? modelData : [modelData]
  for (const d of data) {
    const fields = Object.keys(d)
    if (allUniqueFields.root) {
      for (const field of allUniqueFields.root) {
        if (!fields.includes(field)) continue
        if (d[field] == null) continue
        if (!result[field]) result[field] = {}
        // we use map instead of an array
        //  as map is faster than an array
        //  also, keys cant be duplicated
        // if (field.includes('barcode')) {
        //   result[field]['123456'] = true
        //   continue
        // }
        result[field][d[field]] = true
      }
    }
    if (allUniqueFields.components) {
      for (const field of Object.keys(allUniqueFields.components)) {
        if (!fields.includes(field)) continue
        if (d[field] == null) continue
        const tempResult = getUniqueFieldValues(
          modelData[field],
          allUniqueFields.components[field],
        )
        result = {
          ...result,
          ...tempResult,
        }
      }
    }
  }
  return result
}

// get list of all unique keys
function allUniqueFieldsFromSchema(schema) {
  if (!schema) return null
  const result = {}
  let uniqueFields = []
  const skips = ['survey_metadata', 'abis_export_uuid']

  if (!schema.properties) return result
  for (const [key, property] of Object.entries(schema.properties)) {
    if (skips.includes(key)) continue
    // for components
    if (property['type'] == 'object' || property['type'] == 'array') {
      const componentSchema =
        property['type'] == 'array'
          ? schema.properties[key].items // if repeatable component
          : schema.properties[key]
      const values = allUniqueFieldsFromSchema(componentSchema)
      if (!values.root && !values.components) continue

      result['components'] = {}
      result['components'][key] = {}
      if (values.root) result['components'][key]['root'] = values.root
      if (values.components)
        result['components'][key]['components'] = values.components
      continue
    }

    const propertyTypes = Object.keys(property)
    if (!propertyTypes.includes('unique')) continue
    if (!property.unique) continue
    uniqueFields.push(key)
  }

  if (uniqueFields.length > 0) result['root'] = uniqueFields
  return result
}

// get stored unique values from bulkStore using modelName and key
export function getValuesFromUniqueCollections(bulkStore, modelName, key) {
  if (!bulkStore.uniqueCollections) return {}
  if (!bulkStore.uniqueCollections[modelName]) return {}
  if (!bulkStore.uniqueCollections[modelName].values) return {}
  if (!bulkStore.uniqueCollections[modelName].values[key]) return {}
  // all unique fields associated with the modelName exist
  return bulkStore.uniqueCollections[modelName].values[key]
}

/**
 * generate a copy of the records
 *
 * @param {object} records e.g., `species filter`, `observation details`, etc.
 *
 * @returns {Object} updatedNewRecord a copy of the records
 */
export function safeObjectCopy(records) {
  let updatedNewRecord = {}
  for (const rec in records) {
    if (rec && records[rec]) {
      Object.assign(updatedNewRecord, { [rec]: records[rec] })
    }
  }
  return updatedNewRecord
}

// checks empty or null field
export function isNullorEmpty(records, field) {
  if (!records || !field) {
    return true
  }
  const recordKeys = Object.keys(records)
  if (!recordKeys.includes(field)) {
    // Added a check in case field is passed in as a string,
    // TODO: maybe convert the string into an int above
    // for now this is fine
    if (recordKeys.includes(field.toString())) {
      return false
    }
    return true
  }
  if (!records[field]) {
    return true
  }

  if (records[field].length == 0) {
    return true
  }

  return false
}

export function binToStr(bin) {
  return btoa(
    new Uint8Array(bin).reduce((s, byte) => s + String.fromCharCode(byte), ''),
  )
}

/**
 * Parses the documentation and store in order to dynamically create field types, which
 * is passed to ApiModelCrudUi, to generate form display.
 *
 * @param {String} modelName e.g., `plot-location`, `bird-survey`, etc.
 * @param {Object} documentation OpenAPI schema e.g. this.documentationStore.documentation
 * @param {Boolean} addReferenceFields Whether to add references to other fields (i.e. we generally don't want this on bulk)
 *
 * @returns {Object} modelConfigToAdd
 */
export function makeModelConfig(
  modelName,
  documentation,
  currentProtId,
  addReferenceFields = false,
  title = null,
  splitSurveyStep = false,
) {
  // resolve name to schema
  const schema = schemaFromModelName(modelName, documentation)
  if (!schema) return null

  let modelConf = modelConfigFromSchema(
    schema,
    documentation,
    currentProtId,
    title || prettyFormatFieldName(modelName),
    addReferenceFields,
  )

  if (splitSurveyStep) {
    //this is to handle custom components, so assume this is the 'start', as Workflow
    //adds the 'end' step that squashes custom components
    removeFieldFromModelConfig(modelConf, 'end_date_time')
  }

  // The rowDataKey is only valid/relevant when making model config with a real modelName
  // so any recursive calls within to makeModelConfig will still have it, but using schema directly doesn't,
  // and in fact has no valid "model" reference

  // Therefore it's only possible to add afterwards
  modelConf.rowDataKey = modelName
  // rename plot location
  modelConf.title = createTitle(modelConf.title)
  return modelConf
}

/**
 *
 * @param {*} schema
 * @param {*} documentation Used so that we can resolve children with outside schema
 * @param {Number} currentProtId Used because we make calls to makeModelConfig
 * @param {String} title Informative display name to draw a heading above
 */
export function modelConfigFromSchema(
  schema,
  documentation,
  currentProtId,
  title,
  addReferenceFields = false,
  parentModel = null,
) {
  if (!schema) return null
  // TODO: should probably handle deciding if filteredSelect or not within ApiModelCrudUI
  const FILTERED_SELECT_MINIMUM = 10

  let mainSurveyFieldFlag = false
  let accField = []

  // sometimes we need to update schema rules
  // e.g. fields in cwd observation depend on the cwd survey sample method
  schema = updateSchemaRules(schema, title)
  // we reset schema rules for cwd based on survey inputs
  if (title == 'Coarse Woody Debris Observation') {
    schema = resetCWDSchemaRules(schema, title)
  }

  const bulkStore = useBulkStore()
  const authStore = useAuthStore()
  let deps = []
  for (const [key, property] of Object.entries(schema.properties)) {
    // these specific field types do not need to be entered by the user
    switch (key) {
      // skip any non-user-submitted attributes
      // http://localhost:1337/content-type-builder/reserved-names
      case 'created_at':
      case 'createdAt':
      case 'updated_at':
      case 'updatedAt':
      case 'created_by':
      case 'createdBy':
      case 'updated_by':
      case 'updatedBy':
      case 'published_at':
      case 'publishedAt':
      case 'abis_export_uuid':
        continue

      default:
        break
    }

    // a collection with a survey ID is the main survey
    // therefore it should be displayed first. (but still don't let the user enter this field)
    if (key === 'survey_metadata') {
      mainSurveyFieldFlag = true
      continue
    }
    let fieldData = {}
    fieldData.description = property['x-paratoo-description']
    fieldData.parentModel = parentModel
    if (property['x-paratoo-rename']) {
      fieldData.label = property['x-paratoo-rename'] // label for fields
    } else {
      fieldData.label = prettyFormatFieldName(key)
    }

    fieldData.originalLabel = fieldData.label

    if (property['x-paratoo-unit']) {
      fieldData.label += ` (${property['x-paratoo-unit']})`
    }

    const modelName = title.toLowerCase().replaceAll(' ', '-')
    const hasRequiredFields = customRules[modelName]?.some((item) =>
      item?.conditions?.some(
        (condition) => condition.requiredFields.includes(key)
      ),
    )

    const hasRequiredFields2 = customRules[modelName]?.some((o) =>
      o?.rule?.toString().includes('required'),
    )

    if (
      property['x-paratoo-required'] ||
      schema?.required?.some((o) => o == key) ||
      hasRequiredFields ||
      hasRequiredFields2
    ) {
      fieldData.label += ' *'
    }

    //soft targets need to have dependencies managed for offline
    if (property['x-paratoo-soft-relation-target']) {
      fieldData.label += ` (${property['x-paratoo-soft-relation-target']})`
    }

    // Add validation rules based on the constraints
    let requiredArray = schema.required

    if (property['x-paratoo-add-minted-identifier']) {
      requiredArray.splice(requiredArray.indexOf(key), 1)
    }

    // get all the values to make validation rules so that
    //   we can enforce user to insert only unique value
    //   if unique field found in schema properties
    const savedValues = getValuesFromUniqueCollections(
      bulkStore,
      modelName,
      key,
    )
    fieldData.rules = makeValidationRules(
      key,
      property,
      requiredArray,
      savedValues,
    )
    if (property['$ref'] && property['$ref'].match(/^#\/components\/schemas/)) {
      // we are some generic child that should be embedded directly in the parent observation
      // i.e. a single Vegetation_mapping_species_cover from "Vegetation Mapping Observation"
      fieldData.type = 'child_observation_single'
      const childFieldModelName = schemaNameToModelName(
        property['$ref'].replace('#/components/schemas/', ''),
      )
      fieldData.modelConfig = makeModelConfig(
        childFieldModelName,
        documentation,
        currentProtId,
        false,
        //pass the overridden title to the child observation
        property['x-paratoo-rename'] ? property['x-paratoo-rename'] : null,
      )
    } else if (
      property.type === 'array' &&
      property.items.type === 'integer' &&
      property.items['x-model-ref'] === 'file'
    ) {
      //check for files.
      //if we want a camera, we give it a type containing `photo` or `video`. Cameras
      //  will always render with a file chooser, with a toggle to allow the use of one
      //if we want a file chooser, we give it type `file`.
      if (property.items['x-paratoo-file-type'].length === 1) {
        switch (property.items['x-paratoo-file-type'][0]) {
          case 'images':
            fieldData.type = 'multi_photo'
            break
          case 'videos':
            fieldData.type = 'multi_video'
            break
          case 'files':
            //'files' are any Strapi-supported files that aren't images or videos
            fieldData.type = 'multi_file'
            break
          case 'audio':
            //audio files are grouped with the Strapi media type `files`. However, we
            //want to differentiate audio on the client to know when to render the
            //audio recorder component
            fieldData.type = 'multi_audio'
            break
          default:
            //any file will allow images, videos, or other files
            // paratooWarnMessage(
            //   `Unknown multi-file type: ${property.items['x-paratoo-file-type'][0]}`,
            // )
            fieldData.type = 'multi_any_file'
            break
        }
      } else {
        // unless it's one single file type, just treat it all the same
        fieldData.type = 'multi_any_file'
      }
    } else if (
      property.type === 'array' &&
      property.items &&
      (property.items['$ref'] || property.items['x-model-ref'])
    ) {
      // Need "childObservations" here
      fieldData.type = 'child_observation'
      let param
      if (property.items['$ref']) {
        param = property.items['$ref'].replace('#/components/schemas/', '')
      } else {
        param = property.items['x-model-ref'].replace(
          '#/components/schemas/',
          '',
        )
      }

      const childFieldModelName = schemaNameToModelName(param)
      fieldData.modelConfig = makeModelConfig(
        childFieldModelName,
        documentation,
        currentProtId,
        false,
        //pass the overridden title to the child observation
        property['x-paratoo-rename'] ? property['x-paratoo-rename'] : null,
      )
      if (property['x-paratoo-rename']) {
        fieldData.modelConfig.title = property['x-paratoo-rename']
      }
    } else if (
      property.type === 'integer' &&
      property['x-model-ref'] === 'grassy-weeds-survey' &&
      modelName === 'grassy-weeds-survey'
    ) {
      fieldData.type = 'filteredSelect'
      fieldData.options = getGrassyWeedsDesktopSurveys(
        property['x-model-ref'],
        'D',
      )
      fieldData.optionsKey = property['x-model-ref']
    } else if (
      key === 'voucher_barcode' ||
      key === 'genetic_voucher_barcode' ||
      key === 'barcode'
    ) {
      fieldData.type = 'barcode'
    } else if (key === 'observer_name') {
      fieldData.type = 'observer'
    } else if (
      key === 'x_using_datasheet'
    ) {
      fieldData.type = 'using_datasheet_confirmation'
    }
    // check if it's a lut-ref
    // we also handle floristics vouchers as the process is very similar
    else if (property['x-lut-ref']) {
      // Do common functions for LUTs
      try {
        fieldData.optionsKey = property['x-lut-ref']
        fieldData.optionsMapper = (e) => ({
          value: e.symbol,
          label: e.label,
        })
        fieldData.optionsFilter = undefined
        fieldData.lutFlag = true

        if (property.sort_order) {
          fieldData.optionsSorter = (a, b) => {
            return a.sort_order - b.sort_order
          }
        }

        fieldData.type =
          property.enum.length > FILTERED_SELECT_MINIMUM
            ? 'filteredSelect'
            : 'select'
      } catch (error) {
        paratooWarnMessage(`Error making LUT ref field, ${error}`)
      }
      const { plotHasFaunaPlot } = useDataManagerStore()
      switch (property['x-lut-ref']) {
        //don't need to see protocol variant field
        case 'lut-protocol-variant':
          fieldData = {}
          continue
        // some protocol required to select plot type "Core monitor(veg)" or "Fauna"
        // we not always have Fauna plot attached to the Core monitor
        case 'lut-plot-area':
          // // for cases of non plot based
          if (plotHasFaunaPlot === undefined) break
          if (!plotHasFaunaPlot) {
            fieldData.optionsFilter = (option) => option.symbol === 'C'
            fieldData.default = 'C'
            fieldData.hint = property['x-paratoo-hint']
          }
          break
        default:
          break
      }
    }

    // This checks if the current model references a previous model
    else if (property['x-model-ref']) {
      let childFieldModelName = null
      //check for files.
      //if we want a camera, we give it a type containing `photo` or `video`. Cameras
      //  will always render with a file chooser, with a toggle to allow the use of one
      //if we want a file chooser, we give it type `file`.

      // Specifically, we know these are all single inputs, rather than allowing multiple images etc.
      switch (property['x-model-ref']) {
        case 'file':
          if (property['x-paratoo-file-type'].length === 1) {
            switch (property['x-paratoo-file-type'][0]) {
              case 'images':
                fieldData.type = 'photo'
                break
              case 'videos':
                fieldData.type = 'video'
                break
              case 'files':
                //'files' are any Strapi-supported files that aren't images or videos
                fieldData.type = 'file'
                break
              case 'audio':
                //audio files are grouped with the Strapi media type `files`. However, we
                //want to differentiate audio on the client to know when to render the
                //audio recorder component
                fieldData.type = 'audio'
                break
              default:
                //any file will allow images, videos, or other files
                // paratooWarnMessage(
                //   `Unknown single-file type: ${property['x-paratoo-file-type'][0]}`,
                // )
                fieldData.type = 'any_file'
                break
            }
          } else {
            // unless it's one single file type, just treat it all the same
            fieldData.type = 'any_file'
          }

          break
        case 'plot-layout':
          fieldData.optionsKey = property['x-model-ref']
          fieldData.optionsMapper = makePlotLayoutOptionsMapper()
          fieldData.type = 'filteredSelect'
          break
        case 'camera-trap-setting':
        case 'camera-trap-information':
        case 'soil-horizon-pan':
        case 'soil-horizon-cutan':
        case 'soil-horizon-root':
        case 'soil-horizon-consistence':
        case 'soil-asc':
          //FIXME this should be handled with the rest of the single child ob case, but
          //for some reason it is missing `$ref` and uses `x-model-ref` instead. Is this
          //intended behaviour by core's `amendDoc`? Is the use of `$ref` in schemas no
          //longer used?
          fieldData.type = 'child_observation_single'
          childFieldModelName = schemaNameToModelName(property['x-model-ref'])
          fieldData.modelConfig = makeModelConfig(
            childFieldModelName,
            documentation,
            currentProtId,
            false,
            //pass the overridden title to the child observation
            property['x-paratoo-rename'] ? property['x-paratoo-rename'] : null,
          )
          break
        default:
          // it's some generic reference, this is only needed if it's not handled server-side in a bulk etc.
          // for now, just put in all references if not bulk
          if (addReferenceFields) {
            fieldData.optionsKey = property['x-model-ref']
            fieldData.optionsMapper = (e) => ({
              value: e.id,
              label: e.label,
            })
            fieldData.optionsFilter = undefined
            fieldData.type = 'filteredSelect'

            if (
              key === 'floristics_voucher_full' ||
              key === 'floristics_voucher_lite'
            ) {
              // current protocol has voucher
              const context = authStore.currentContext
              if(context){
                context['hasVoucher'] = true
              }
              authStore.setCurrentContext(context)
              fieldData.optionsFilter = function (
                selectedVisitId,
                suffix,
                surveys,
              ) {
                return (v) => {
                  //get the current voucher's survey
                  const survey = surveys.find((survey) => {
                    //in this case, `suffix` is the floristics voucher variant - 'lite' or 'full
                    return v[`floristics_veg_survey_${suffix}`].id === survey.id
                  })

                  return survey.plot_visit.id === selectedVisitId
                }
              }
            }
          } else {
            continue
          }
          break
      }
    } else if (property['x-paratoo-csv-list-taxa']) {
      // current protocol has species field
      const context = authStore.currentContext
      if (context) {
        context['hasSpecies'] = true
      }
      authStore.setCurrentContext(context)
      fieldData.type = 'filteredSelectWithCustomInput'
      fieldData.csv = property['x-paratoo-csv-list-taxa']
    } else if (property['x-paratoo-multi-select-json']) {
      // current protocol has species field
      fieldData.type = 'multiSelectJson'
    }
    // Check if this field is a reference to a single component
    else if (property['x-paratoo-component']) {
      // If it's known, set the type accordingly
      const xParatooComponent = property['x-paratoo-component']
      fieldData.parentModel = title.toLowerCase().replaceAll(' ', '-')
      if (xParatooComponent === 'basal-area.tree-caliper-ellipse') {
        fieldData.type = 'object'
        fieldData.modelConfig = modelConfigFromSchema(
          property,
          documentation,
          currentProtId,
          prettyFormatFieldName(key),
          false,
          `${title.toLowerCase().replaceAll(' ', '-')}.${key}`,
        )
      } else if (xParatooComponent === 'weather.weather') {
        fieldData.type = 'weather'
        fieldData.modelConfig = modelConfigFromSchema(
          property,
          documentation,
          currentProtId,
          prettyFormatFieldName(key),
          false,
          `${title.toLowerCase().replaceAll(' ', '-')}.${key}`,
        )
      } else if (
        xParatooComponent === 'location.location' ||
        xParatooComponent === 'location.trunk-char-height-location'
      ) {
        fieldData.type = 'location'

        if (Object.keys(schema.properties).includes('new_or_existing')) {
          //need user to be explicit, else when autofilling existing site the start
          //location will get squashed
          fieldData.initCurrent = false
        }
      } else if (xParatooComponent.startsWith('custom-lut.')) {
        fieldData = makeCustomLutConfig(
          fieldData,
          key,
          property,
          documentation,
          currentProtId,
        )
      } else if (
        xParatooComponent === 'plot-location-name.plot-location-name'
      ) {
        fieldData.type = 'locationNamePicker'
        fieldData.modelConfig = modelConfigFromSchema(
          property,
          documentation,
          currentProtId,
          prettyFormatFieldName(key),
          false,
          `${title.toLowerCase().replaceAll(' ', '-')}.${key}`,
        )
      } else if (xParatooComponent === 'vegetation.floristics-voucher') {
        // current protocol has voucher
        const context = authStore.currentContext
        if(context) {
          context['hasVoucher'] = true
        }
        authStore.setCurrentContext(context)
        fieldData.type = 'FloristicsVoucherPicker'
        fieldData.modelConfig = modelConfigFromSchema(
          property,
          documentation,
          currentProtId,
          prettyFormatFieldName(key),
          false,
          `${title.toLowerCase().replaceAll(' ', '-')}.${key}`,
        )
        fieldData.multiple = false
      } else if (xParatooComponent === 'general.new-or-existing') {
        let siteIdSiteFieldName = null
        if (property['x-paratoo-new-or-existing-site-name']) {
          siteIdSiteFieldName = property['x-paratoo-new-or-existing-site-name']
        } else {
          paratooWarnMessage(
            `No 'x-paratoo-new-or-existing-site-name' in ${key}. Defaulting to '${siteIdSiteFieldName}'`,
          )
          siteIdSiteFieldName = 'Site ID'
        }

        fieldData.type = 'NewOrExistingPicker'
        fieldData.modelConfig = modelConfigFromSchema(
          property,
          documentation,
          currentProtId,
          prettyFormatFieldName(key),
        )
        fieldData.modelConfig.fields.site_id.label = `${siteIdSiteFieldName} *`
        fieldData.multiple = false
      } else if (xParatooComponent === 'pest.attributable-fauna-species') {
        fieldData.type = 'attributableFaunaSpecies'
        fieldData.modelConfig = modelConfigFromSchema(
          property,
          documentation,
          currentProtId,
          prettyFormatFieldName(key),
        )
      } else {
        fieldData.type = 'child_observation_single'
        fieldData.modelConfig = modelConfigFromSchema(
          property,
          documentation,
          currentProtId,
          prettyFormatFieldName(key),
          false,
          `${title.toLowerCase().replaceAll(' ', '-')}.${key}`,
        )
      }
    }

    // Check if this field is a reference to a collections of repeatable components
    else if (property.items && property.items['x-paratoo-component']) {
      const componentName = property.items['x-paratoo-component']
      fieldData.parentModel = title.toLowerCase().replaceAll(' ', '-')
      if (componentName === 'observers.observers') {
        // custom component for observers drop down names
        fieldData.type = 'observers'
      } else if (
        ['location.fauna-plot-points', 'location.plot-location-point'].includes(
          componentName,
        )
      ) {
        // edge case for plot layout
        fieldData.type = 'plot-layout-gps'
      } else if (componentName === 'pest.transect-detail') {
        fieldData.type = 'child_observation'
        fieldData.modelConfig = modelConfigFromSchema(
          property.items,
          documentation,
          currentProtId,
          prettyFormatFieldName(key),
          false,
          `${title.toLowerCase().replaceAll(' ', '-')}.${key}`,
        )
        //FIXME workaround as nested components don't work
        fieldData.modelConfig.fields.transect_start_point.type = 'location'
      } else if (componentName === 'camera-trap.target-species') {
        fieldData.type = 'child_observation'
        fieldData.modelConfig = modelConfigFromSchema(
          property.items,
          documentation,
          currentProtId,
          prettyFormatFieldName(key),
          false,
          `${title.toLowerCase().replaceAll(' ', '-')}.${key}`,
        )
      } else if (/^multi-lut/.test(componentName)) {
        fieldData.type = 'multiLutSelect'
        fieldData.modelConfig = modelConfigFromSchema(
          property.items,
          documentation,
          currentProtId,
          prettyFormatFieldName(key),
          false,
          `${title.toLowerCase().replaceAll(' ', '-')}.${key}`,
        )
        // at rule if this field is required, as it return an array so the validation is different
        const targetFunction = fieldData.rules.some(
          (func) => func.name === 'requiredRule',
        )
        if (targetFunction) {
          const rule = (val) => val.length > 0 || 'This field is required'
          fieldData.rules.push(rule)
        }
      } else if (componentName === 'vegetation.floristics-voucher') {
        // current protocol has voucher
        const context = authStore.currentContext
        if(context) {
          context['hasVoucher'] = true
        }
          authStore.setCurrentContext(context)
        fieldData.type = 'FloristicsVoucherPicker'
        fieldData.modelConfig = modelConfigFromSchema(
          property.items,
          documentation,
          currentProtId,
          prettyFormatFieldName(key),
          false,
          `${title.toLowerCase().replaceAll(' ', '-')}.${key}`,
        )
        fieldData.multiple = true
      } else {
        // Generic child component functionality
        fieldData.type = 'child_observation'
        fieldData.modelConfig = modelConfigFromSchema(
          property.items,
          documentation,
          currentProtId,
          fieldData.label || prettyFormatFieldName(key),
          false,
          `${title.toLowerCase().replaceAll(' ', '-')}.${key}`,
        )
      }
    }

    // check if it's a generic Enum
    else if (property.enum && property.enum.length > 0) {
      fieldData.type = 'select'
      fieldData.options = property.enum
    }
    // grab the format if detected
    else if (property.format) {
      // format detected
      switch (property.format) {
        case 'date-time':
          fieldData.type = 'datetime'
          break
        case 'date':
          fieldData.type = 'date'
          break
        case 'time':
        case 'iso-time':
          fieldData.type = 'time'
          break
        case 'float':
          fieldData.type = 'number'
          break
        default:
          break
      }
    } else {
      fieldData.type = property.type
    }

    if (property['x-paratoo-has-dependencies']) {
      deps = property['x-paratoo-has-dependencies']
      fieldData.dependencies = deps
      fieldData.callback = (curr, inputDefs) => {
        for (const def of inputDefs) {
          if (curr.dependencies.some((o) => o === def.model)) {
            def.hidden = !def.hidden
          }
        }
      }
    }

    //TODO add this to fields in crud as needed (initially implemented on basic string inputs).
    //should just need to use `hint` prop or `bottom-slots` prop with `hint` slot; also
    //make sure to use `lazy-rules` prop else validation will occur immediately and thus
    //not show the hint
    if (property['x-paratoo-hint']) {
      fieldData.hint = property['x-paratoo-hint']
    }
    accField[key] = fieldData
  }

  if (Array.isArray(deps) && deps.length > 0) {
    for (const d of deps) {
      accField[d].hidden = true
    }
  }
  //handle componments
  const modelConfigToAdd = {
    title: schema['x-paratoo-rename'] ? schema['x-paratoo-rename'] : title,
    fields: {
      ...accField,
    },
    maximum: schema['maximum'] || 9999,
    mainSurveyFlag: mainSurveyFieldFlag,
    currentProtId: currentProtId,
  }
  return modelConfigToAdd
}

/**
 * remove specific fields from modelConfig
 *
 * @param {object} modelConfig model config
 * @param {string} field specific key to remove
 *
 * @returns {Array} modelConfigfields
 */
export function removeFieldFromModelConfig(modelConfig, field) {
  if (!modelConfig || !field) return

  // every valid modelConfig has field property
  if (isNullorEmpty(modelConfig, 'fields')) return null

  for (let key in modelConfig.fields) {
    if (!key.includes(field)) continue

    // if specific field exists
    delete modelConfig.fields[key]
  }

  return modelConfig.fields
}

/**
 *
 * @param {Object} fieldData the accumulator object `fieldData` from `modelConfigFromSchema()`
 * @param {String} key the key (i.e., field name) of the schema
 * @param {Object} property the value (i.e., schema)
 * @param {Object} documentation swagger doc
 * @param {Number} currentProtId the protocol ID
 * @returns
 */
function makeCustomLutConfig(
  fieldData,
  key,
  property,
  documentation,
  currentProtId,
) {
  fieldData.type = 'filteredSelectWithCustomInput'
  fieldData.lutFlag = false
  const customLutConfig = modelConfigFromSchema(
    property,
    documentation,
    currentProtId,
    prettyFormatFieldName(key),
  )
  const customLutKeys = Object.keys(property.properties)
  const lutFieldName = customLutKeys.find((o) => o.endsWith('lut'))
  const textFieldName = customLutKeys.find((o) => o.endsWith('text'))
  fieldData.select = {
    ...customLutConfig.fields[lutFieldName],
    fieldName: lutFieldName,
  }

  fieldData.select.optionsFilter = (o) => {
    return !potentialOtherValues.some((p) => p === o.label.toLowerCase())
  }

  fieldData.input = {
    ...customLutConfig.fields[textFieldName],
    fieldName: textFieldName,
  }

  return fieldData
}

// update schema rules
function updateSchemaRules(schema, title) {
  if (title == 'Coarse Woody Debris Observation') {
    return resetCWDSchemaRules(schema, title)
  }

  if (title == 'Fauna Ground Counts Observation') {
    return updateSurveyType(schema, title)
  }

  return schema
}

/**
 * Store selected fauna protocol type
 *
 * @param {object} schema
 * @param {string} title
 *
 * @returns {object} schema
 */
function updateSurveyType(schema, title) {
  if (title != 'Fauna Ground Counts Observation') return schema

  const authStore = useAuthStore()
  const bulkStore = useBulkStore()

  // if current context doesn't exist
  if (!authStore.currentContext) return schema
  if (isNullorEmpty(authStore.currentContext, 'protocol')) return schema

  const currentProtocol = authStore.currentContext.protocol
  const currentProtID = currentProtocol.id ? currentProtocol.id : null
  const currCollection = bulkStore.collectionGetForProt({
    protId: currentProtID,
  })

  // if fauna survey survey doesn't exist
  if (!currCollection['fauna-ground-counts-survey']) return schema

  const faunaSurvey = currCollection['fauna-ground-counts-survey']
  const ephemeralStore = useEphemeralStore()

  // store selected protocol type
  ephemeralStore.setFaunaGroundCountProtocolType(
    faunaSurvey.ground_count_protocol_type,
  )

  return schema
}

/**
 * In some cases we need to customize some rules
 * ex. customization of required fields, max or min values
 *
 * @param {object} schema
 * @param {string} title
 *
 * @returns {object} schema
 */
function resetCWDSchemaRules(schema, title) {
  if (title != 'Coarse Woody Debris Observation') return schema

  const authStore = useAuthStore()
  const bulkStore = useBulkStore()

  // if current context doesn't exist
  if (!authStore.currentContext) return schema
  if (isNullorEmpty(authStore.currentContext, 'protocol')) return schema

  const currentProtocol = authStore.currentContext.protocol
  const currentProtID = currentProtocol.id ? currentProtocol.id : null
  const currCollection = bulkStore.collectionGetForProt({
    protId: currentProtID,
  })

  // if cwd survey doesn't exist
  if (!currCollection) return schema
  if (isNullorEmpty(currCollection, 'coarse-woody-debris-survey')) return schema

  const cwdSurvey = currCollection['coarse-woody-debris-survey']

  // customized minimum narrowest diameter based on cut off
  schema.properties.CWD_section.items.properties.narrowest_diameter_cm.minimum =
    cwdSurvey.sampling_survey_cut_off == 'C10' ? 10 : 5
  return schema
}

/**
 * Generate random coordinates to draw circle in dev mode
 *
 * @param {Object} currentCoord
 * @param {number} totalNumberOfPoints
 * @param {number} radius
 *
 * @returns {Array} all the coordinates to draw circle
 */
export function generateRandomCoordinates(
  currentCoord,
  totalNumberOfPoints,
  radius,
) {
  let newCirclePoints = []
  for (let i = 0; i < totalNumberOfPoints; i++) {
    const xOffet = radius * Math.cos((2 * Math.PI * i) / totalNumberOfPoints)
    const yOffet = radius * Math.sin((2 * Math.PI * i) / totalNumberOfPoints)
    newCirclePoints.push({
      lat: currentCoord.lat + xOffet,
      lng: currentCoord.lng + yOffet,
    })
  }
  return newCirclePoints
}

/**
 * checks whether any null field exists or not
 *
 * @param {Object} object source to find null fields
 * @param {Boolean} getAllNullFields if on, it returns all null fields
 *
 * @returns {String/Array} result if getAllNullFields is on, sends string array
 * or only the first null field if any field exists
 */
export function findNullValuesInObject(object, getAllNullFields = false) {
  let nullKeys = []
  Object.keys(object).forEach((key) => {
    // push all null values
    if (object[key] == null || typeof object[key] == 'undefined') {
      nullKeys.push(key)
    }
  })

  // if no null values found
  if (nullKeys.length == 0) {
    return null
  }

  // if getAllNullFields is true, returns array
  //   or returns only first one
  return getAllNullFields ? nullKeys : nullKeys[0]
}

/**
 * determine equality for two objects.
 * see https://stackoverflow.com/questions/1068834/object-comparison-in-javascript
 *
 * @param {object} object1
 * @param {object} object2
 *
 * @returns {boolean} result
 */
export function isEqualObjects(object1, object2) {
  // if both objects are null or undefined and exactly the same
  if (object1 === object2) return true

  // if they are not strictly equal, they both need to be objects
  if (!(object1 instanceof Object) || !(object2 instanceof Object)) return false

  // they must have the exact same prototype chain, the closest we can do is
  //   test there constructor
  if (object1.constructor !== object2.constructor) return false

  for (let p in object1) {
    // other properties were tested using constructors
    if (!Object.prototype.hasOwnProperty.call(object1, p)) continue

    // allows to compare object1[ p ] and object2[ p ] when set to undefined
    if (!Object.prototype.hasOwnProperty.call(object2, p)) return false

    // if they have the same strict value or identity then they are equal
    if (object1[p] === object2[p]) continue

    // numbers, strings, functions, booleans must be strictly equal
    if (typeof object1[p] !== 'object') return false

    // objects and arrays must be tested recursively
    if (!isEqualObjects(object1[p], object2[p])) return false
  }

  for (let p in object2) {
    // allows object2[p] to be set to undefined
    if (
      Object.prototype.hasOwnProperty.call(object2, p) &&
      !Object.prototype.hasOwnProperty.call(object1, p)
    )
      return false
  }

  return true
}

/**
 * get protocol name using protocol id
 *
 * @param {object} allProtocols
 * @param {int} protocolID
 *
 * @returns {string} protocolName
 */
export function protocolName(allProtocols, protocolID) {
  try {
    return `${allProtocols.find(({ id }) => id === protocolID).name}`
  } catch (error) {
    return null
  }
}

/**
 * find the differences in records
 *
 * @param {object} newRecord record with latest changes
 * @param {object} oldRecord old record
 *
 * @returns {object} newChanges only differences
 */
export function findChangesInNewRecord(newRecord, oldRecord) {
  const newRecordKeys = Object.keys(newRecord)
  const oldRecordKeys = Object.keys(oldRecord)

  // if page/component is loaded for first time
  //    oldRecord will be null
  // we don't need to save values
  if (oldRecordKeys.length == 0) return null

  let newChanges = {}
  newRecordKeys.forEach((key) => {
    // we can skip
    if (isEqualObjects(newRecord[key], oldRecord[key])) return

    // push only new changes
    newChanges[key] = newRecord[key]
  })

  return newChanges
}

/**
 * new changes with validity status so that
 *   we can store valid values instantly
 * also, if cacheEnable flag is on
 *   we store new changes in cache
 *
 * @param {Object} newRecord record with latest changes
 * @param {Object} oldRecord old record
 * @param {Object} modelConfig model config
 * @param {Int} index if multiple component
 * @param {Boolean} cacheEnable cache enable flag
 *
 * @returns {object} newChanges only differences
 */
export function findValidChangesInNewRecord(
  newRecord,
  oldRecord,
  modelConfig,
  index = null,
  cacheEnable = true,
) {
  // differences
  const newChanges = findChangesInNewRecord(newRecord, oldRecord)
  const newChangesKeys = newChanges ? Object.keys(newChanges) : []

  // if no changes found
  if (newChangesKeys.length == 0) return null

  // const modelConfigFields = modelConfig.fields

  let validChanges = {}
  newChangesKeys.forEach((key) => {
    // we can skip
    if (!checkValidity(newChanges[key], modelConfig.fields[key])) return

    // add valid change
    validChanges[key] = newChanges[key]

    // if cache enable
    //   we store values
    if (!cacheEnable) return
    temporaryStoreValue(newChanges[key], key, modelConfig.rowDataKey, index)
  })
  return validChanges
}

/**
 * checks whether the data is valid or not
 *
 * @param {Object} data data
 * @param {Object} modelConfigField model config field
 *
 * @returns {Boolean} status
 */
export function checkValidity(data, modelConfigField) {
  let allStatus = []
  for (const [key, rules] of Object.entries(modelConfigField)) {
    if (key != 'rules') continue
    if (!rules) continue

    // checks every rules
    rules.forEach((rule) => {
      allStatus.push(rule(data))
    })
  }

  // if any false/undefined found
  return !allStatus.includes(false)
}

/**
 * temporary store value in cache
 *
 * @param {Object} value new change
 * @param {String} field field to change
 * @param {String} modelName model to change
 * @param {int} index if multiple component
 *
 */
export function temporaryStoreValue(value, field, modelName, index = null) {
  // temporary saved collection
  const autoSavedCollections = useBulkStore().autoSavedCollections

  // initialize collections if doest exist
  if (isNullorEmpty(autoSavedCollections, modelName)) {
    // collection should be an array if index exists
    autoSavedCollections[modelName] = index == null ? {} : []
  }

  // initialize if collection is an array
  if (index != null && !autoSavedCollections[modelName][index]) {
    autoSavedCollections[modelName][index] = {}
  }

  // push new value
  if (index == null) {
    autoSavedCollections[modelName][field] = value
  } else {
    autoSavedCollections[modelName][index][field] = value
  }
}

/**
 * Removes one auto saved collection from the store,
 *   specified with a model name
 *
 * @param {String} modelName The protocol name of the auto
 *   saved collection to remove
 */
export function resetOneAutoSavedCollection(modelName) {
  // temporary saved collection
  const autoSavedCollections = useBulkStore().autoSavedCollections

  // if model exists in autoSavedCollections we reset
  let keys = Object.keys(autoSavedCollections)
  if (!keys.includes(modelName)) return
  delete autoSavedCollections[modelName]
}

/**
 * Removes unique collections from the store,
 *   specified with a model name
 *
 * @param {String} modelName The model name of unique
 *   collection to remove
 */
export function resetUniqueCollections(modelName) {
  // temporary saved collection
  const uniqueCollections = useBulkStore().uniqueCollections

  // if model exists in uniqueCollections we reset
  let keys = Object.keys(uniqueCollections)
  if (!keys.includes(modelName)) return
  delete uniqueCollections[modelName]
}

/**
 * Resolves a modelName to the associated schema for the "new" entry
 *
 * @param {String} modelName
 * @param {*} documentation OpenAPI schema e.g. this.documentationStore.documentation
 * @returns
 */
export function schemaFromModelName(modelName, documentation) {
  if (!documentation.components) return null
  const newModelName = `${upperFirst(camelCase(modelName))}Request`
  const data = documentation.components.schemas[newModelName].properties.data
  // previously all schemas have component properties included
  //    but now(starpi 4.10.6) we have only ref object of the components
  //    so, we are going to use component models using refs to extract
  //    properties of all components recursively
  return data
}

/**
 * update or create species list using field name assigned in floristics.
 *
 * @param {Object} floristics e.g., `floristics-veg-voucher-lite`, `floristics-veg-voucher-full`, etc.
 */
export function refreshConditionSpeciesList(floristics) {
  if (!floristics) return

  let apiModelsStore = useApiModelsStore()
  let species_list = apiModelsStore.cachedModel({
    modelName: 'lut-condition-species',
  })

  if (!species_list) species_list = []

  // update/push species list
  for (const value of floristics) {
    // if species already exists
    const checkSpeciesName = (obj) => obj.label === value['field_name']
    if (species_list.some(checkSpeciesName)) continue

    const id = !species_list ? value['id'] : species_list.length + 1
    // initials of field name
    const symbol = value['field_name']
      .match(/(\b\S)?/g)
      .join('')
      .toUpperCase()

    // push new species
    species_list.push({
      id: id,
      symbol: symbol,
      label: value['field_name'],
      description: '',
      uri: '',
      createdAt: value['createdAt'],
      updatedAt: value['updatedAt'],
    })
  }

  apiModelsStore.models['lut-condition-species'] = species_list
}

/**
 * Determine the slope class from degree of slope
 *
 * @param {int} degree degree of slope
 *
 * @returns {object | null} slope class object
 */
export function generateSlopeClassFromSlopeDegree(degree) {
  if (degree == null || degree == undefined) return null
  const plotSlope = initState.models['lut-slope-class']

  //TODO less hard-code - can be derived from LUT which contains extra metadata
  let symbol
  if (degree >= 0 && degree < 0.6) {
    symbol = 'LE'
  } else if (degree >= 0.6 && degree < 1.77) {
    symbol = 'VG'
  } else if (degree >= 1.77 && degree < 5.77) {
    symbol = 'GE'
  } else if (degree >= 5.77 && degree < 18) {
    symbol = 'MO'
  } else if (degree >= 18 && degree < 30) {
    symbol = 'ST'
  } else if (degree >= 30 && degree < 45) {
    symbol = 'VS'
  } else if (degree >= 45 && degree < 72) {
    symbol = 'PR'
  } else if (degree >= 72) {
    symbol = 'CL'
  }

  for (let slopeClass of plotSlope) {
    if (slopeClass['symbol'] === symbol) {
      return { value: slopeClass['symbol'], label: slopeClass['label'] }
    }
  }
  return null
}

/**
 * Determine the slope class from percentage of slope
 *
 * @param {int} percent percentage of slope
 *
 * @returns {object | null} slope class object
 */
export function generateSlopeClassFromSlopePercent(percent) {
  let symbol

  //TODO less hard-code - can be derived from LUT which contains extra metadata
  if (percent >= 0 && percent < 1) {
    symbol = 'LE'
  } else if (percent >= 1 && percent < 3) {
    symbol = 'VG'
  } else if (percent >= 3 && percent < 10) {
    symbol = 'GE'
  } else if (percent >= 10 && percent < 32) {
    symbol = 'MO'
  } else if (percent >= 32 && percent < 56) {
    symbol = 'ST'
  } else if (percent >= 56 && percent < 100) {
    symbol = 'VS'
  } else if (percent >= 100 && percent < 300) {
    symbol = 'PR'
  } else if (percent >= 300) {
    symbol = 'CL'
  }

  for (let slopeClass of initState.models['lut-slope-class']) {
    if (slopeClass['symbol'] === symbol) {
      return { value: slopeClass['symbol'], label: slopeClass['label'] }
    }
  }

  return null
}

/**
 * Determine diameter using circumference
 *
 * @param {int} circumference circumference at breast hight
 *
 * @returns {int} diameter diameter
 */
export function generateDiameterFromCircumference(circumference) {
  // as circumference = π × diameter
  // see https://www.mometrix.com/academy/the-diameter-radius-and-circumference-of-circles
  const diameter = parseInt(circumference) / Math.PI
  return diameter ? Math.round(diameter) : 0
}

/**
 * create NVIS level 5 summary
 *
 * @param {object} vegetationAssociations vegetation associations
 *
 * @returns {string} NVIS summary
 */
export function generateNVISLevel5(vegetationAssociations) {
  if (!vegetationAssociations) return null

  let NVISSummary = ''

  for (const veg_association of vegetationAssociations) {
    NVISSummary += NVISSummary.length == 0 ? '+' : ' / '

    // if field name exists
    if (!isNullorEmpty(veg_association, 'field_name')) {
      NVISSummary += veg_association.field_name
      continue
    }
    // if species name exists
    const veg_species = veg_association.vegetation_species
    // default to full name if we can't extract scientific name
    let scientificName = veg_species
    if (veg_species && typeof veg_species === 'string') {
      // extract scientific name in veg_species
      const regex = /\(scientific: ([^)]+)\)/
      const match = veg_species.match(regex) || []
      scientificName = match[1]
    }
    NVISSummary += scientificName ? scientificName : ''
  }
  return NVISSummary
}

/**
 * create NVIS structural formation usign veg growths and veg heights
 *
 * @param {Object} newVal new record
 *
 * @returns {string} matched formation
 */
export function generateVegStructuralFormation(newVal) {
  // rules to generate structural formation
  const veg_structural_rules = NVIS_RULES.StructuralFormation
  const cover_codes = NVIS_RULES.CoverCodes

  // new vlaues
  const veg_growths = newVal.growth_form
  const veg_heights = newVal.height_range
  const cover_percentages = newVal.cover_percentage

  const size = generateVegStructuralFormationSize(newVal)
  let final_formation = size ? size + ' ' : ''

  // check which rule satisfies the new record
  for (let rule of veg_structural_rules) {
    const is_growth_match = isValueExistInArray(
      rule.veg_growths,
      veg_growths.label,
    )
    const is_height_match = isValueExistInArray(
      rule.veg_heights,
      veg_heights.label,
    )

    if (!is_growth_match || !is_height_match) continue

    const cover_index = cover_codes.findIndex(
      (x) => x == cover_percentages.symbol,
    )

    // if growth and height match we return structural
    //   formation using the index of cover_codes.
    final_formation +=
      cover_index != -1 ? rule.veg_structural_formation[cover_index] : ''
    return final_formation
  }

  return final_formation
}

/**
 * create NVIS structural formation size usign veg growths and veg heights
 *
 * @param {Object} newVal new record
 *
 * @returns {string} matched formation
 */
export function generateVegStructuralFormationSize(newVal) {
  // rules to generate formation size
  const formation_size_rules = NVIS_RULES.FormationSize

  // new vlaues
  const veg_growths = newVal.growth_form
  const veg_heights = newVal.height_range

  // check which rule satisfies the new record
  for (let rule of formation_size_rules) {
    const is_growth_match = isValueExistInArray(
      rule.veg_growths,
      veg_growths.label,
    )

    if (!is_growth_match) continue

    const height_index = rule.veg_heights.findIndex(
      (x) => x == veg_heights.symbol,
    )

    // if growth match we return formation size
    //   using the index of veg_heights.
    return height_index != -1 ? rule.formation_size[height_index] : ''
  }
}

/**
 * retrieve objects in array if property matches another array
 *
 * @param {Object} allValues original array contains all values
 * @param {String} key field to match
 * @param {Array} labels array of string
 *
 * @returns {Object} matched_values all the filterd values
 */
export function retrieveMatchedObjectsFromNVISData(allValues, key, labels) {
  let matched_values = []
  labels.forEach((label) => {
    // we push new object if matched label exists
    //    in the source array
    let new_value = allValues.find((c) => c[key] == label)
    if (new_value) {
      matched_values.push(new_value)
    }
  })

  return cloneDeep(matched_values)
}

/**
 * Checks whether value exists in array or not
 *
 * @param {Array} sourcArray source array
 * @param {String} value value to check
 *
 * @returns {Boolean} status
 */
export function isValueExistInArray(sourcArray, value) {
  try {
    return sourcArray.some((label) => {
      return label == value
    })
  } catch (error) {
    return false
  }
}

/**
 * Checks whether object exists in array of objects
 *
 * @param {Object} sourcArray array of objects
 * @param {Object} value value to check
 * @param {String} key which field in source array
 *
 * @returns {Boolean} status
 */
export function isObjectExistInObjectArray(sourcArray, value, key = null) {
  try {
    return sourcArray.some((element) => {
      return key
        ? JSON.stringify(element[key]) == JSON.stringify(value)
        : JSON.stringify(element) == JSON.stringify(value)
    })
  } catch (error) {
    return false
  }
}

/**
 * find matched object from an array
 *
 * @param {Array} sourceArray source array
 * @param {String} field field in source array
 * @param {String/Integer} value field to check
 *
 * @returns {Object} Object
 */
export function getMatchedObject(sourceArray, field, value) {
  const index = sourceArray.findIndex((x) => x[field] == value)

  return index != -1 ? sourceArray[index] : null
}

// get species full object
export function SpeciesFullNameFromScientificName(ScientificName, speciesList) {
  if (!ScientificName) return null
  const results = speciesList.filter(function (entry) {
    return entry.label === ScientificName
  })
  return results.length > 0 ? results[0] : null
}

/**
 * generate NVIS Species object from species list
 * e.g. convert [species1, species2, species3] to
 *   a json object {first: species1, second: species2,
 *   third: species3}
 *
 * @param {Array} species_list array of species object
 *
 * @returns {Object} NVISSpeciesModel an object
 */
export function generateNVISSpeciesApiModel(speciesList) {
  return {
    first: speciesList[0] ? speciesList[0] : null,
    second: speciesList[1] ? speciesList[1] : null,
    third: speciesList[2] ? speciesList[2] : null,
  }
}

/**
 * generate species list from NVIS Species object
 * e.g. convert {first: species1, second: species2,
 *   third: species3} to [species1, species2, species3]
 *
 * @param {Object} NVISSpeciesModel NVIS Species object
 *
 * @returns {Array} array of species object
 */
export function NVISSpeciesApiModelToArray(NVISSpeciesModel) {
  let speciesList = []
  if (!NVISSpeciesModel) return speciesList

  if (NVISSpeciesModel.first) speciesList.push(NVISSpeciesModel.first)
  if (NVISSpeciesModel.second) speciesList.push(NVISSpeciesModel.second)
  if (NVISSpeciesModel.third) speciesList.push(NVISSpeciesModel.third)

  return speciesList
}

/**
 * generate a combined string to display mulitple speices
 *   for vegetation association table
 * e.g. convert {first: species1, second: species2, third: species3}
 *   to a string "species1, species2, species3"
 *
 * @param {Object} NVISSpeciesModel species object
 *
 * @returns {String} allSpecies containg all the species names
 */
export function NVISSpeciesToDisplayData(NVISSpeciesModel) {
  if (!NVISSpeciesModel) return ''

  // if NVISSpeciesModel is not an array
  if (!Array.isArray(NVISSpeciesModel)) {
    return NVISSpeciesModel.data ? NVISSpeciesModel.data.scientificName : ''
  }

  NVISSpeciesModel = generateNVISSpeciesApiModel(NVISSpeciesModel)

  const frist = NVISSpeciesModel.first
    ? NVISSpeciesModel.first.data.scientificName
    : ''

  const second = NVISSpeciesModel.second
    ? ', ' + NVISSpeciesModel.second.data.scientificName
    : ''
  const third = NVISSpeciesModel.third
    ? ', ' + NVISSpeciesModel.third.data.scientificName
    : ''

  return frist + second + third
}

/**
 * Determine the measure of trunk or stem diameter
 *
 * @param {float} dbh first diameter
 * @param {float} ellipseDbh second diameter
 *
 * @returns {float} square root of the product
 */
export function generateCalculatedDBH(dbh, ellipseDbh) {
  if (!dbh || !ellipseDbh) return

  const _dbh = parseInt(dbh)
  const _ellipseDbh = parseInt(ellipseDbh)

  if (isNaN(_dbh) || isNaN(_ellipseDbh)) return null
  // as the square root of the product of the two diameters
  // is the measure of trunk or stem diameter
  return Math.round(Math.sqrt(_dbh * _ellipseDbh))
}

/**
 * Plot-location-name component takes in references to some LUTs
 * and some unique digits, but it's meant to be viewed as a string
 *
 * @param {Object} newRecord attributes to serialise over
 *
 * @returns {string} serialized name (used on some barcodes)
 */
export function serializePlotLocationName(newRecord) {
  // ensure order and relevancy (sometimes an ID is thrown in when grabbing existing locations)
  const relevantAttributes = ['state', 'program', 'bioregion', 'unique_digits']
  let accPlotName = ''
  for (const attribute of relevantAttributes) {
    if (!newRecord[attribute]) return
    // handle even if in the newRecord format of {label, value}
    if (newRecord[attribute].value) {
      accPlotName += newRecord[attribute].value
      continue
    }
    if (newRecord[attribute].symbol) {
      accPlotName += newRecord[attribute].symbol
      continue
    }
    accPlotName += newRecord[attribute].toString()
  }
  return accPlotName
}

export function assignValidUserCoords(userCoords) {
  return isValidUserCoords(userCoords)
    ? cloneDeep(userCoords)
    : {
        lat: -34.936075,
        lng: 138.617835,
      }
}
export function isValidUserCoords(userCoords) {
  // if invalid userCoords
  if (!userCoords) return false
  const keys = Object.keys(userCoords)
  if (!keys.includes('lat') || !keys.includes('lng')) return false
  if (!userCoords.lat || !userCoords.lng) return false

  return true
}

export function generateUUIDAndPlotLabel(values) {
  if (!Array.isArray(values) && values?.plot_name) {
    values['uuid'] = uuidv4()
    values['plot_label'] = Object.values(values.plot_name).join('')
    return cloneDeep(values)
  }

  for (const value of values) {
    if (!value || !value?.plot_name) continue
    // plot label and uuid need to be updated every time
    value['uuid'] = uuidv4()
    value['plot_label'] = Object.values(value.plot_name).join('')
  }

  return cloneDeep(values)
}
/**
 * Takes in an object referring to a single field within a documentation schema field.
 * 
 * TODO export the rules from a single place to import here, so that we can reuse these rules in other places
 * 
 * @param {String} fieldName Name of the target documentation field object
 * @param {Object} property schema property of the target field
 * @param {Array} requiredArray Documentation-sourced array of the required fields
 *
 * @returns {Array} rules conforming to Quasar validation rule format.
 */
export function makeValidationRules(
  fieldName,
  property,
  requiredArray,
  savedValues,
) {
  let rules = []

  // TODO add more cases to this, relating to all the different possible constraints
  if (requiredArray !== undefined && requiredArray.includes(fieldName)) {
    // TODO, confirm this works with Select and FilteredSelect
    // We can't check if the value itself is true or false because it might be 0(int), false(bool)
    const requiredRule = (value) =>
      (value !== undefined && value !== null && value !== '') ||
      `This field is required`

    rules.push(requiredRule)
  }

  if ('minimum' in property) {
    rules.push(
      (value) =>
        value == null ||
        value >= property.minimum ||
        value?.length >= property.minimum ||
        `Minimum: ${property.minimum}`,
    )
  }

  if ('maximum' in property) {
    rules.push(
      (value) =>
        value <= property.maximum ||
        value?.length <= property.maximum ||
        `Maximum: ${property.maximum}`,
    )
  }

  if ('minLength' in property) {
    rules.push(
      (value) =>
        !value ||
        value.length >= property.minLength ||
        `Must be longer than ${property.minLength} characters`,
    )
  }

  if ('maxLength' in property) {
    rules.push((value) => {
      if (!value || value.length <= property.maxLength || value?.value?.length <= property.maxLength) {
        return true
      } else {
        if (value.length > 255) {
          paratooWarnMessage(
            `Whoops! Maximum length of ${property.maxLength} exceeded for ${fieldName}. We may need to change this one to 'text'`,
          )
          return `Must be shorter than ${property.maxLength} characters. If you think this field should allow more than 255 characters, please raise a ticket so we can fix this in the next release. Sorry for the inconvenience.`
        }
        return `Must be shorter than ${property.maxLength} characters`
      }
    })
  }

  if ('minItems' in property) {
    rules.push(
      (value) =>
        !value ||
        (Array.isArray(value) && value.length >= property.minItems) ||
        `Must have at least ${property.minItems} entries`,
    )
  }

  if ('maxItems' in property) {
    rules.push(
      (value) =>
        !value ||
        (Array.isArray(value) && value.length <= property.maxItems) ||
        `Must have less than ${property.maxItems} entries`,
    )
  }

  if ('pattern' in property) {
    let mismatchMsg = `Must match ${property.pattern}`
    if (property['x-paratoo-regex-message']) {
      mismatchMsg = property['x-paratoo-regex-message']
    }

    rules.push((value) => {
      return (
        !value ||
        Array.isArray(value.toString().match(property.pattern)) ||
        mismatchMsg
      )
    })
    // TODO, match works but not user error message
  }

  // if integer we will reject float values
  if (property.type == 'integer') {
    rules.push(
      (value) =>
        !value || !value.toString().includes('.') || `Value must be an integer`,
    )
  }

  if ('unique' in property) {
    if (property.unique) {
      rules.push(
        (value) =>
          !value ||
          // checks whether the value is already saved or not
          !savedValues[value] ||
          `Value must be unique`,
      )
    }
  }

  // if integer we will reject float values
  if (property.type == 'integer') {
    rules.push(
      (value) =>
        !value || !value.toString().includes('.') || `Value must be an integer`,
    )
  }

  return rules
}

export function updateUniqueRule(modelName, inputDefs, collectionIdex) {
  try {
    if (!inputDefs) return
    const bulkStore = useBulkStore()
    const authStore = useAuthStore()

    // fields in api crud is an object whereas inputdef is an array
    const isField = !Array.isArray(inputDefs)

    if (!bulkStore.uniqueCollections) return inputDefs
    if (!bulkStore.uniqueCollections[modelName]) return inputDefs

    const currentContext = authStore.currentContext
    

    // all pending data excluding the current index so that we can edit modelValue.
    const pendingCollections = bulkStore.collections?.[currentContext.protocol.id]?.[modelName]
    let collectionData = []
    const isMultiple = Array.isArray(pendingCollections)
    if (pendingCollections) {
      if (isMultiple) {
        //likely observation
        collectionData = pendingCollections
      } else {
        //likely survey or another type of single entity.
        //easier to treat everything as arrays
        collectionData = [pendingCollections]
      }
    }

    let index = collectionIdex
    if (index === null) {
      if (isMultiple) {
        index = bulkStore.getTemporaryIndexId
      } else {
        //likely survey or similar, so it'll be 1 element
        index = 0
      }
      
    }
    
    let data = collectionData 
      ? collectionData.filter((_, i) => i != index) 
      : []

    let allFields = bulkStore.uniqueCollections[modelName].allFields

    // should be a component
    let parentModel = null
    if (!isField) parentModel = inputDefs[0]?.parentModel
    if (parentModel) {
      if (parentModel.includes('.')){
        const relations = inputDefs[0].parentModel.split('.')
        for (const r of relations) {
          if (r==modelName) continue
          if (!allFields.components) return inputDefs

          const components = Object.keys(allFields.components)
          if (!components.includes(r)) return inputDefs

          allFields = cloneDeep(allFields.components[r])
          let newData = []
          for (const d of data) {
            if (!d[r]) continue
            if (Array.isArray(d[r])) {
              newData = newData.concat(d[r])
              continue
            }
            newData.push(d[r])
          }
          data = newData
        }
      }
    }
    const uniqueValues = bulkStore.uniqueCollections[modelName].values
    applyUniqueRule(
      inputDefs, 
      cloneDeep(allFields), 
      data, 
      cloneDeep(uniqueValues),
      false,
      isField
    )
  } catch (error) {
    paratooWarnHandler('An error occurred while updating unique rule',error)
  }
}
// eslint-disable-next-line
export function applyUniqueRule(inputDefs, allUniqueFields, data, uniqueValues, isComponent=false, isField=false) {
  try {
    if (allUniqueFields.root) {
      for (const field of allUniqueFields.root) {
        if (!inputDefs) continue
        // modelConfig is an object if component 
        let key = field
        if (Array.isArray(inputDefs)) {
          // modelConfig is an array if component 
          key = inputDefs.findIndex(x => x['model'] === field)
          if (key < 0) continue
        }
        // update unique values if any pending collection found
        let pendingValues = []
        for (const d of data) {
          if (!d?.[field]) continue
          if (!Array.isArray(d[field])) {
            pendingValues.push(d[field])
            continue
          }
          pendingValues = pendingValues.concat(d[field])
        }
  
        let savedValues = uniqueValues[field]
        if (!savedValues) savedValues = {}
        if (pendingValues) {
          for (const p of pendingValues) {
            if (!p) continue
            savedValues[p] = true
          }
        }
        
        // dont need add rule if no saved data found
        if (isEmpty(savedValues)) continue
        if (!inputDefs?.[key]?.rules) continue
        // checks whether the rule is already existed or not
        let existUniqueRuleIndex = -1
        for (const [idx, rule] of inputDefs[key].rules.entries()) {
          if (!rule.toString().includes('Value must be unique')) continue
          existUniqueRuleIndex = idx
        }
        const updatedRule = (value) => !value ||
          !savedValues[value] ||
          `Value must be unique`
        if (existUniqueRuleIndex < 0) {
          // new rule
          inputDefs[key].rules.push(updatedRule)
          continue
        }
        // update existing rule
        inputDefs[key].rules[existUniqueRuleIndex] = updatedRule
        continue
      }
    }
    // if component then apply rule recursively.
    if (allUniqueFields.components) {
      for (const field of Object.keys(allUniqueFields.components)) {
        let key = field
        if (Array.isArray(inputDefs)) {
          // modelConfig is an array if component 
          key = inputDefs.findIndex(x => x['model'] === field)
          if (key < 0) continue
        }

        if (!inputDefs?.[key]?.['modelConfig']) continue
        let pendingData = []
        for (const d of data) {
          if (!d[field]) continue
          if (!Array.isArray(d[field])) {
            pendingData.push(d[field])
            continue
          }
          pendingData = pendingData.concat(d[field])
        }
        applyUniqueRule(
          inputDefs[key]['modelConfig']['fields'], 
          allUniqueFields.components[field],
          pendingData,
          uniqueValues,
          true
        )
      }
    }
  } catch (error) {
    paratooWarnHandler('An error occurred while applying unique rule',error)
  }
}

export function setExistingBarcodes(barcodes) {
  const bulkStore = useBulkStore()
  bulkStore.setCurrentBarcodes(barcodes)
}

export function strToBin(str) {
  return Uint8Array.from(atob(str), (c) => c.charCodeAt(0))
}
/**
 * Formats a field name to a nicer looking format. Ideally this wouldn't be used
 * and instead there'd be a display name field in the documentation
 */
export function prettyFormatFieldName(fieldName) {
  // the regex replaces all underlines and hyphens with spaces
  // The loop capitalises the first letter of every word
  let out = ''
  for (var word of fieldName.replace(/(-|_)/g, ' ').split(' ')) {
    if (word) {
      if (word == 'metres') {
        word = '(metres)'
      }
      out = `${out} ${word[0].toUpperCase()}${word.substring(1)}`
    }
  }
  // remove leading space
  return out.trim()
}

// generate network info
export function networkConnectionStatus() {
  // if navigator is undefined
  if (!navigator?.connection) {
    return { msg: 'Failed to generate network info, reason: navigator is undefined' }
  }
  return {
    bandwidth: navigator.connection.downlink,
    effectiveType: navigator.connection.effectiveType,
    type: navigator.connection.type,
    rtt: navigator.connection.rtt,
    saveData: navigator.connection.saveData
  }
}
/**
 * converts base64 to blob
 * @param {String} b64Encoded encoded string
 * @param {String} blobType blob type e.g. image/png
 * @returns {Object} Blob object
 */
export function b64toBlob(b64Encoded, blobType) {
  const byteCharacters = atob(b64Encoded)
  const byteArrays = []
  const sliceSize = 512

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize)

    const byteNumbers = new Array(slice.length)
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i)
    }

    const byteArray = new Uint8Array(byteNumbers)
    byteArrays.push(byteArray)
  }

  return new Blob(byteArrays, { type: blobType })
}
/**
 * Formats a datetime to a nicer looking format.
 * https://stackoverflow.com/questions/35893462/how-can-i-format-an-iso-8601-date-to-a-more-readable-format-using-javascript?noredirect=1&lq=1
 */
export function prettyFormatDateTime(dateTime) {
  try {
    const dateObj = new Date(dateTime)
    const dateLocal = dateObj.toLocaleString()
    const timezoneOffsetMins = dateObj.getTimezoneOffset()
    const timezoneOffsetHrsFormatted = `${Math.floor(
      (timezoneOffsetMins * -1) / 60,
    )
      .toString()
      .replace('-', '')
      .padStart(2, '0')}`
    const timezoneOffsetMinsFormatted = `${((timezoneOffsetMins % 60) * -1)
      .toString()
      .padStart(2, '0')}`
    return `${dateLocal} (local time GMT${
      timezoneOffsetMins > 0 ? '-' : '+'
    }${timezoneOffsetHrsFormatted}${timezoneOffsetMinsFormatted})`
  } catch (err) {
    paratooWarnHandler(
      `Failed to pretty-format datetime '${dateTime}', keeping unformatted instead`,
      err,
    )
    return dateTime
  }
}
export function inputDefsToModelConfig(inputDefs) {
  const fields = {}
  for (const i of inputDefs){
    fields[i["model"]] = cloneDeep(i)
  }
  return cloneDeep(fields)
 }
/**
 * Checks the currently inputted form data to determine whether it is valid, based on
 * the rules provided by each this.fields instance.
 *
 * fulfills "custom validator" function mentioned in:
 * https://quasar.dev/vue-components/input#validation
 *
 * Displays error messages on failure
 * @param {*} formData An object of keys relating to field names, with values being the form data. (e.g. newRecord)
 * @param {*} modelConfigFields An object of keys relating to field names, with a value of each being an array with name: 'rules'
 * @returns {Boolean} Validity of the form
 */
export function validateForm(
  formData,
  modelConfigFields,
  silent = false,
  modelName = null,
) {
  // Check if there are any fields, form should not be valid if empty
  // (such as when the user has selected neither (SELECT EXISTING LOCATION) or (CREATE NEW))
  const mCKeys = Object.keys(modelConfigFields)
  if (
    // Specifically when the only inputdef is a component (making plot-visit etc.)
    mCKeys.length === 1 &&
    mCKeys.includes('component')
  ) {
    // notifyHandler('negative', 'No input fields detected')
    paratooWarnMessage(
      'This should only appear when trying to validate a form that has no fields',
    )
    return true
  }

  for (const [key, value] of Object.entries(modelConfigFields)) {
    if (!value || !value.rules) continue

    for (const rule of value.rules) {
      let ruleReturn
      try {
        ruleReturn = rule(formData[key])
      } catch {
        continue
      }
      // const ruleReturn = rule(formData[key])

      if (ruleReturn !== true) {
        if (!silent) {
          // TODO improve readability of error message
          let fieldLabel = value?.originalLabel || prettyFormatFieldName(key)
          // change field labels. e.g. 'Id' can be model name
          fieldLabel = checkConditionalFieldLabels(fieldLabel, modelName)
          notifyHandler('negative', `Field: "${fieldLabel}": ${ruleReturn}`)
        } else {
          paratooWarnMessage(
            `(Silent validation error). Field: "${prettyFormatFieldName(
              key,
            )}": ${ruleReturn}`,
          )
        }
        return false
      }
    }
    if (value?.type === 'child_observation_single') {
      const childFields = value.modelConfig.fields
      const childObsValidation = validateForm(
        formData?.[key],
        childFields,
        silent,
      )
      if (!childObsValidation) {
        return false
      }
    }
  }
  // if custom rule exists we validate
  if (!isNullorEmpty(customRules, modelName)) {
    let outputs = null
    let result = true
    let status = false
    const assignedRules = customRules[modelName]
    for (const assignedRule of assignedRules) {
      switch (assignedRule.ruleType) {
        // checks the total of multiple values together
        // e.g in Basal Wedge Observation, in_tree And borderline_tree counts
        //    together should be greater than 7
        case 'multipleValuesMinimumCheck':
          outputs = 0
          assignedRule.dependentKeys.forEach((dependentKey) => {
            outputs += formData[dependentKey] ? formData[dependentKey] : 0
          })
          // if no species to record
          if (formData.no_species_to_record) continue

          result = assignedRule.rule(outputs)
          // if fails to validate
          if (result !== true) {
            notifyHandler('negative', `${result}`)
            return false
          }
          break
        // validates the difference between two fields cannot be greater than a certain value
        // e.g. upper depth and lower depth of soil pit samples cannot be larger than 30cm
        case 'maximumDifferenceCheck': {
          if (assignedRule.dependentKeys.length !== 2) {
            console.warn(
              'Required exactly two dependent keys to apply maximumDifferenceCheck rule',
            )
            break
          }
          const field1Val = formData[assignedRule.dependentKeys[0]]
          const field2Val = formData[assignedRule.dependentKeys[1]]
          const diff = Math.abs(field1Val - field2Val)
          result = assignedRule.rule(diff)

          if (result !== true) {
            notifyHandler('negative', `${result}`)
            return false
          }
          break
        }
        // checks whether all the values are null/false or not
        // e.g basal wedge must have at least one record or
        //   have the 'No species to record' checkbox checked
        case 'OneOfTheFieldsMustHaveValue':
          outputs = 0
          // count total values
          // e.g adds one for true boolean
          //  or string non null value
          assignedRule.dependentKeys.forEach((dependentKey) => {
            outputs += formData[dependentKey] ? 1 : 0
          })
          status = formData[assignedRule.dependentKeysWithFlags.flag]
            ? isNullorEmpty(
                formData,
                assignedRule.dependentKeysWithFlags.fields[0],
              )
            : isNullorEmpty(
                formData,
                assignedRule.dependentKeysWithFlags.fields[1],
              )

          outputs += status ? 0 : 1
          // if total is greater than 1
          result = assignedRule.rule(outputs)
          // if fails to validate
          if (result !== true) {
            notifyHandler('negative', `${result}`)
            return false
          }
          break
        // NOTE: latest strapi fixed that already by using regex with integer
        case 'OneDecimalPlaceCheck':
          outputs = []
          assignedRule.dependentKeys.forEach((dependentKey) => {
            const output = getValueFromData(formData, dependentKey)
            if (output.fieldExist) {
              if (!Array.isArray(output.value)) {
                outputs.push({ value: output.value, field: dependentKey })
                return
              }
              output.value.forEach((v) => {
                outputs.push({ value: v, field: dependentKey })
              })
            }
          })
          if (outputs.length == 0) return true
          for (const output of outputs) {
            let fieldName = output.field.replace('_single', '')
            fieldName = fieldName.replace('_multi', '')
            result = assignedRule.rule(output.value, fieldName)
            // if fails to validate
            if (result !== true) {
              notifyHandler('negative', `${result}`)
              return false
            }
          }
          break
        case 'minNumOfReplicates':
          result = assignedRule.rule({
            replicateBarcodeLength: formData.replicate?.length,
            hasReplicate: formData.has_replicate,
          })
          if (result !== true) {
            notifyHandler('negative', `${result}`)
            return false
          }
          break
        case 'distanceBetweenReplicates':
          if (formData.has_replicate) {
            result = assignedRule.rule(formData.min_distance_between_replicates)
          } else {
            break
          }

          if (result !== true) {
            notifyHandler('negative', `${result}`)
            return false
          }
          break
        case 'ConditionalFieldsCheck':
          for (const condition of assignedRule.conditions) {
            if (!condition.dependOn) continue
            if (!condition.requiredFields) continue

            // finds dependOn.field into formData
            let output = getValueFromData(formData, condition.dependOn.field)
            let dependOnValues = condition.dependOn.value
            if (!Array.isArray(condition.dependOn.value)) {
              dependOnValues = [dependOnValues]
            }
            const outPutValue = output?.value?.value || output.value
            if (!output.fieldExist) continue
            if (
              (!dependOnValues.includes('$defined') &&
                !dependOnValues.includes(outPutValue)) ||
              (dependOnValues.includes('$defined') && isNil(outPutValue))
            )
              continue
            // check all required fields
            for (const field of condition.requiredFields) {
              output = getValueFromData(formData, field)
              if (output.fieldExist && output.value) continue
              const fieldLabel =
                modelConfigFields[field]?.label || prettyFormatFieldName(field)
              notifyHandler(
                'negative',
                `Field: "${fieldLabel}": This field is required`,
              )
              return false
            }
          }
          break
        default:
          break
      }
    }
  }

  return true
}

// change conditional field labels. e.g. 'Id' to 'Plot Visit' or 'Plot Layout'
function checkConditionalFieldLabels(fieldLabel, modelName) {
  let label = fieldLabel

  // all conditions related to field labels
  for (const condition of conditionalFieldLabels) {
    if (!condition) continue
    switch (condition.label) {
      case 'modelName':
        if (condition.fields.includes(fieldLabel.toLocaleLowerCase())) {
          label = prettyFormatFieldName(modelName)
        }
        break
      // extend if necessary
      default:
        break
    }
  }

  return label
}

// get values from object
function getValueFromData(data, field, replace = null, remove = false) {
  const result = {
    data: null,
    fieldExist: false,
    value: null,
    changedValue: null,
  }
  if (!data) return result
  const keys = Object.keys(data)
  const payload = cloneDeep(data)
  // if field exists
  if (keys.includes(field)) {
    result.fieldExist = true
    result.value = payload[field]

    // if needs to replace or delete
    if (replace) {
      payload[field] = replace
      result.changedValue = replace
    }
    if (remove) delete payload[field]
    result.data = cloneDeep(payload)
    return cloneDeep(result)
  }

  // checks all the objects recursively
  for (const key of keys) {
    if (typeof data[key] != 'object') continue
    // if not an array
    if (!Array.isArray(data[key])) {
      const objectValue = getValueFromData(data[key], field, replace, remove)
      if (!objectValue.data) continue

      // if found
      payload[key] = objectValue.data
      result.fieldExist = objectValue.fieldExist
      result.value = objectValue.value
      result.changedValue = objectValue.changedValue
      result.data = cloneDeep(payload)
      return cloneDeep(result)
    }

    // if an array
    let values = []
    let changedValues = []
    for (const [index, obj] of data[key].entries()) {
      const objectValue = getValueFromData(obj, field, replace, remove)
      if (!objectValue.data) continue
      result.fieldExist = objectValue.fieldExist
      values.push(objectValue.value)
      payload[key][index] = objectValue.data
      if (objectValue.changedValue) changedValues.push(objectValue.changedValue)
    }

    if (!result.fieldExist) continue

    // if field found
    result.data = cloneDeep(payload)
    result.value = values
    result.changedValue = changedValues
    return cloneDeep(result)
  }
  return result
}
export async function updateDexieFieldNames(modelName, protocolID) {
  if (!modelName || !protocolID) return

  // if invalid data or model names
  const bulkStore = useBulkStore()
  const currCollection = bulkStore.collectionGetForProt({ protId: protocolID })
  // if (!modelNamesWithFieldName.includes(modelName)) return
  if (!currCollection) return
  if (!currCollection[modelName]) return

  const data = currCollection[modelName]
  let fieldNames = []

  // append all field_names using modelNames
  //   e.g. in veg observation filed_names are in the NVIS association
  let allVegAssociation = []
  switch (modelName) {
    case 'vegetation-mapping-observation':
      data.forEach((value) => {
        value.NVIS_level_5_vegetation_association_information.forEach((v) => {
          if (v.field_name) allVegAssociation.push(v)
        })
      })
      break
    default:
      data.forEach((value) => {
        if (value.field_name) allVegAssociation.push(value)
      })
      break
  }

  // TODO: checks if field names are unique or not
  for (const value of allVegAssociation) {
    if (!value.field_name) continue
    fieldNames.push({
      canonicalName: value.field_name,
      scientificName: '',
      taxonRank: '',
    })
  }
  if (fieldNames.length == 0) return
  // update newly created filed names
  const apiModelsStore = useApiModelsStore()
  await apiModelsStore.dexieTransaction('fieldNames', fieldNames, true)
}

/**
 * Queries Dexie for the species list(s)
 *
 * @param {Array} selectedTaxa a list of the taxa to search over. Dexie tables are
 * segmented by these taxa
 * @param {Number} [limit] maximum number of entries to return
 * @param {String} [searchString] a string to filter the search by (case insensitive)
 *
 * @returns {Array} the list of items matching the search criteria (or empty array if
 * no matches)
 */
export async function queryDexieFromTaxa(
  selectedTaxa,
  limit = undefined,
  searchString = undefined,
) {
  // if dexieDB is still loading
  // we return an empty array
  if (useEphemeralStore().dexieDbIsLoading) {
    notifyHandler('warning', 'Species lists are still loading')
    return []
  }

  //no matter the `limit` provided, we always want a hard limit to ensure decent
  //performance. The user can provide more precise queries if they need more results
  const hardLimit = 200
  //extra check just in case passed non-array
  if (!Array.isArray(selectedTaxa)) {
    selectedTaxa = [selectedTaxa]
  }
  let list = []
  let tablesToSearch = []

  //translate taxa type symbol to a Dexie table name (some taxa have multiple tables)
  for (const taxa of selectedTaxa) {
    switch (taxa) {
      // All the unknown species or manual entries
      // e.g. field names/unknown species assigned in vegetation mapping should be available in floristics
      //   in case same unknown species is encountered in a plot - needs to use consistent naming
      case 'FN': {
        tablesToSearch.push('fieldNames')
        break
      }
      case 'VP': {
        tablesToSearch.push('vascularFlora')
        break
      }
      case 'NVP': {
        //TODO get algae, fungi, protists, etc.
        tablesToSearch.push(...['mosses', 'lichens'])
        break
      }
      case 'AM': {
        tablesToSearch.push('amphibia')
        break
      }
      case 'BI': {
        tablesToSearch.push('birds')
        break
      }
      //TODO:
      case 'INV': {
        let msg =
          'Invertebrate species list not yet supported, but you can enter your own field name'
        notifyHandler('warning', msg)
        tablesToSearch.push('invertebrates')
        break
      }
      case 'MA': {
        tablesToSearch.push('mammalia')
        break
      }
      case 'RE': {
        tablesToSearch.push('reptiles')
        break
      }
      default: {
        paratooWarnMessage(`Unhandled taxa type: ${taxa}`)
        break
      }
    }
  }

  if (tablesToSearch.length === 1 && tablesToSearch[0] === 'invertebrates') {
    //only inverts list has been specified, but it is not yet supported so just return
    //empty. sometimes inverts can be specified along-side others, so we want to still
    //load up these other lists
    return []
  } else {
    //remove inverts as we can't search a non-existant list
    tablesToSearch = tablesToSearch.filter((o) => o !== 'invertebrates')
  }

  let searchStrSplit = null
  if (searchString) {
    searchStrSplit = searchString.split(' ')
  }

  for (const tableName of tablesToSearch) {
    //older versions of the dexie DB don't have indexed words, so we need to check for
    //the index that corresponds to the words so that we don't access undefined
    let canonicalName = 'canonicalName'
    let scientificName = 'scientificName'
    const indexes = await dexieDB[tableName].schema.indexes
    const tableHasIndexedWords =
      indexes.some((o) => o.name === `${canonicalName}_`) &&
      indexes.some((o) => o.name === `${scientificName}_`)
    if (tableHasIndexedWords) {
      canonicalName = `${canonicalName}_`
      scientificName = `${scientificName}_`
    } else {
      console.debug(
        `Table '${tableName}' doesn't have indexed words (species names) so search will be slow`,
      )
    }

    if (limit && searchString) {
      list = list.concat(
        await dexieDB[tableName]
          .where(canonicalName)
          .startsWithAnyOfIgnoreCase(searchStrSplit)
          .or(scientificName)
          .startsWithAnyOfIgnoreCase(searchStrSplit)
          .limit(limit < hardLimit ? limit : hardLimit)
          .toArray(),
      )
    } else if (limit) {
      list = list.concat(
        await dexieDB[tableName]
          .limit(limit < hardLimit ? limit : hardLimit)
          .toArray(),
      )
    } else if (searchString) {
      list = list.concat(
        await dexieDB[tableName]
          .where(canonicalName)
          .startsWithAnyOfIgnoreCase(searchStrSplit)
          .or(scientificName)
          .startsWithAnyOfIgnoreCase(searchStrSplit)
          .limit(hardLimit)
          .toArray(),
      )
    } else {
      list = list.concat(await dexieDB[tableName].limit(hardLimit).toArray())
    }
  }


  return list
}

/**
 * CamelCase to kebab-case + remove postfix
 *
 * The schemaNames in our modified version of StrapiV4
 * plugin-documentation (which we use an unreleased PR) use a different schema
 * naming convention than StrapiV3, which requires a more in-depth conversion
 *
 * @param {String} schemaName name from documentation.components.schemas
 * @returns
 */
export function schemaNameToModelName(schemaName) {
  if (Object.keys(schemaToModelNameExceptions).includes(schemaName)) {
    return schemaToModelNameExceptions[schemaName]
  }

  return kebabCase(
    clone(schemaName)
    .replace(/Request$/, '')
  )
}

/**
 * kebab-case to CamelCase + add postfix
 *
 * @param {String} modelName
 */
export function modelNameToSchemaName(modelName) {
  return `${upperFirst(camelCase(modelName))}Request`
}

/**
 * query to get only field names.
 *
 * @param {String} modelName
 *
 * @return query string
 */
export function queryParamsForFieldNames(modelName) {
  // generate query for non null field names only
  if (modelName == 'vegetation-mapping-observation') {
    return '?populate[NVIS_level_5_vegetation_association_information][filters][field_name][$ne]='
  }
  return '?[filters][field_name][$ne]='
}

/**
 * Promise-based function that ensures workflow is defined, such that we don't
 * access undefined, which can prevent the page from loading
 * source: https://codepen.io/eanbowman/pen/jxqKjJ?editors=0010
 *
 * @param {*} variable the variable we're checking
 * @param {Function} comparator anonymous function that does some check on the `variable`
 * and returns a boolean
 * @param {Number} timeout timeout in milliseconds
 *
 * @returns resolved or rejected promise
 */
export function ensureVariableDefined(variable, comparator, timeout) {
  const start = Date.now()
  return new Promise(waitForVariable)

  function waitForVariable(resolve, reject) {
    let variableIsDefined = false
    try {
      variableIsDefined = comparator(variable)
    } catch {
      variableIsDefined = false
    }

    if (variableIsDefined) {
      resolve(variable)
    } else if (timeout && Date.now() - start >= timeout) {
      reject(new Error('timeout'))
    } else {
      setTimeout(waitForVariable.bind(this, resolve, reject), 30)
    }
  }
}

/**
 * Waits for the callback to return `true` before continuing.
 * 
 * Thanks ChatGPT
 * 
 * @param {Function} conditionFn the function to run for the check. Must return truthy
 * @param {Number} [interval] how often to trigger the checker function in milliseconds
 * @param {Number} [timeout] how much total time to check the condition before giving up
 * 
 * @returns {Promise} a promise that wraps the wait condition
 */
export function waitForCondition({ conditionFn, interval = 1000, timeout = 5000 }) {
  return new Promise((resolve, reject) => {
    const startTime = Date.now()

    function checkCondition() {
      if (conditionFn()) {
        resolve(true)
      } else if (Date.now() - startTime >= timeout) {
        reject(new Error('Timeout waiting for condition'))
      } else {
        setTimeout(checkCondition, interval)
      }
    }

    checkCondition()
})
}

/**
 * Retrives the list of all projects and protocols linked to the modelname
 *
 * @param {String} modelName name of the model
 *
 * @returns {Array} array of objects. e.g. [project_id]: protocol_uuid
 */
export function findProjectsAndProtocolsFromModelName(modelName) {
  const authStore = useAuthStore()
  const apiModelsStore = useApiModelsStore()
  const userProjects = authStore.projAndProt ? authStore.projAndProt : []
  const result = {}

  for (const project of userProjects) {
    for (const protocol of project.protocols) {
      const workFlow = apiModelsStore.findModelsAndChildObsFromProtocolUUID({
        protUuid: protocol.identifier,
      })
      if (
        !workFlow.allModels.includes(modelName) &&
        !workFlow.plotModels.includes(modelName)
      )
        continue
      if (!result[project.id]) result[project.id] = []
      result[project.id] = union(result[project.id], [protocol.identifier])
    }
  }

  return result
}

/**
 * Retrieves all the name of models, child observation, and other relational models
 * (e.g., vouchers) linked to the protocol
 * 
 *
 * @param {String} protUuid protocol uuid
 *
 * @returns {Object} workflow with all the models, child obs and plot models.
 */
export function findModelsAndChildObsFromProtocolUUID(protUuid) {
  paratooWarnMessage(`findModelsAndChildObsFromProtocolUUID() is deprecated. Use apiModels.findModelsAndChildObsFromProtocolUUID`)
  const apiModelsStore = useApiModelsStore()
  const documentationStore = useDocumentationStore()
  const plots = ['plot-visit', 'plot-layout']
  let allModels = []
  let plotModels = []

  const workFlow = {}
  const modelsAndRelations = {}

  const resolved = apiModelsStore.protIdFromUuid(protUuid)
  const protocol = apiModelsStore.protocolInformation({
    protId: resolved?.id,
  })

  if (!protocol?.workflow) {
    paratooWarnMessage(`Failed to get workflow for protocol with UUID=${protUuid}`)
  }

  for (const w of protocol?.workflow || []) {
    if (!w.newInstanceForRelationOnAttributes) {
      modelsAndRelations[w.modelName] = null
      continue
    }
    modelsAndRelations[w.modelName] = {}
    for (let i = 0; i < w.newInstanceForRelationOnAttributes.length; i++) {
      modelsAndRelations[w.modelName][w.newInstanceForRelationOnAttributes[i]] =
        w.relationOnAttributesModelNames[i]
    }
  }

  workFlow['raw'] = modelsAndRelations
  let modelNames = Object.keys(modelsAndRelations)
  

  // delete plot models
  plots.forEach((p) => {
    if (!modelNames.includes(p)) return
    plotModels.push(p)
    modelNames = modelNames.filter((item) => item !== p)
  })
  workFlow['surveyModel'] = modelNames[0]
  workFlow['modelNames'] = modelNames
  workFlow['plotModels'] = plotModels
  if (plotModels.length > 0 && !plotModels.includes('plot-selection')) {
    workFlow['plotModels'] = plotModels.concat(['plot-selection'])
  }

  modelNames.forEach((m) => {
    allModels.push(m)
    if (!modelsAndRelations[m]) return
    for (const f of Object.keys(modelsAndRelations[m])) {
      //follow child ob relations
      allModels.push(modelsAndRelations[m][f])
    }
  })
  
  const apiListModels = allApiListModels()

  const relationalModels = new Set()
  for (const model of allModels) {
    const modelDeps = documentationStore.modelDependencies({
      modelName: model,
      modelsAcc: [],
      includeSoftLinks: true,
      includeLuts: false,
    })
    for (const dep of modelDeps) {
      if (
        //don't care about LUTs
        !dep.startsWith('lut-') &&
        //must be refreshable
        apiListModels.includes(dep)
      ) {
        relationalModels.add(dep)
      }
    }
  }
  
  const relationalModelsArr = Array.from(relationalModels)
  if (relationalModelsArr?.length) {
    for (const relationalModel of relationalModelsArr) {
      if (
        //don't push duplicates
        !allModels.includes(relationalModel)
      ) {
        allModels.push(relationalModel)
      }
    }
  }

  workFlow['allModels'] = allModels

  return workFlow
}

// checks whether the protocol is ready or not
export function isProtocolReady(protId) {
  paratooSentryBreadcrumbMessage(`Checking if protocol with ID=${protId} is ready`)
  const apiModelsStore = useApiModelsStore()
  const ephemeralStore = useEphemeralStore()
  const authStore = useAuthStore()
  const apiListModels = allApiListModels()
  const result = {
    status: true,
    msg: '',
  }

  if (!ephemeralStore?.networkOnline) {
    paratooSentryBreadcrumbMessage(`No network, so skipping checks for protocol being ready`)
    return result
  }

  const resolved = apiModelsStore.protUuidFromId(protId)
  let models = apiModelsStore.findModelsAndChildObsFromProtocolUUID({
    protUuid: resolved?.uuid,
  })?.allModels || []

  //need to check if authorised, as we don't want to set a model as needed if
  //they're not authorised. For example, if they're assigned Floristics Enhanced
  //but not Standard, the relation to floristics-veg-voucher-lite will be kept
  //even though they're not authorised to access it.
  //but we don't want this check inside `findModelsAndChildObsFromProtocolUUID` as
  //`getAllAuthorisedModels` also calls that function, and can result in a max callstack
  //size error
  const allAuthorisedModels = authStore.getAllAuthorisedModels()
  models = models.filter(m => allAuthorisedModels.includes(m))

  // if any model is still loading
  paratooSentryBreadcrumbMessage(`Checking if models are still populating: ${models.join(', ')}`)
  for (const m of models) {
    if (!apiListModels.includes(m)) continue
    if (isSessionDestroyed()) {
      paratooWarnMessage(`Could not refresh data for ${m}, using existing data`)
      return result
    }
    if (!ephemeralStore.populatedModels.includes(m)) {
      result.status = false
      result.msg = `Data for ${m} is still loading`
      paratooSentryBreadcrumbMessage(result.msg)
      return result
    }
  }
  return result
}

/**
 * Retrieves all vouchers for the project area, which is derived from the selected
 * project's plot data
 *
 * @returns {Array.<Object>} An array of vouchers, where each voucher is an object
 * containing info such as the field name, growth forms, survey, etc.
 */
export async function getVouchersForProjectArea() {
  const authStore = useAuthStore()
  const dataManager = useDataManagerStore()
  const apiModelsStore = useApiModelsStore()
  const allAuthorisedModels = authStore.getAllAuthorisedModels()

  const cachedSelections = apiModelsStore
    .cachedModel({ modelName: 'plot-selection' })

  //check this first, as there's no point continuing if there's no plots defined for the project
  let plotsForProject
  try {
    plotsForProject = authStore.projAndProt
      .find((o) => o.id === authStore.selectedProject.id)
      .plot_selections.map((o) => o.name)
    let offlinePlotsForProject = []
    dataManager.publicationQueue.forEach((queuedItem) => {
      if (
        queuedItem.authContext.project === authStore.selectedProject.id &&
        Object.keys(queuedItem.collection).includes('plot-definition-survey') &&
        queuedItem.collection['plot-layout']?.plot_points &&
        queuedItem.collection['plot-layout']?.plot_selection &&
        Number.isInteger(queuedItem.collection['plot-layout'].plot_selection)
      ) {
        const relevantPlotSelection = cachedSelections
          .find(
            (p) => p.id === queuedItem.collection['plot-layout'].plot_selection,
          )
        if (relevantPlotSelection) {
          offlinePlotsForProject.push(relevantPlotSelection.plot_label)
        }
      }
    })
    if (
      offlinePlotsForProject &&
      Array.isArray(offlinePlotsForProject) &&
      offlinePlotsForProject.length > 0
    ) {
      plotsForProject = plotsForProject.concat(offlinePlotsForProject)
    }
  } catch {
    const msg = `Failed to resolve the plots for the selected project '${authStore.selectedProject.name}'. Has the project been defined with the plots in the project area?`
    throw new Error(msg)
  }

  //check if we got the vouchers from Core - if not, get them again (won't work if
  //user is offline)
  const modelsMightRefresh = [
    'floristics-veg-survey-full',
    'floristics-veg-survey-lite',
    'floristics-veg-voucher-full',
    'floristics-veg-voucher-lite',
  ]
  const modelsToRefresh = []
  for (const model of modelsMightRefresh) {
    const currModel = dataManager.cachedModel({ modelName: model })
    if (!currModel) {
      //entire model doesn't exist
      modelsToRefresh.push(model)
      continue
    }

    //if we get here then entry must exist in the store, but we need to check visits in survey
    if (model.includes('survey') && Array.isArray(currModel)) {
      for (const entry of currModel) {
        if (!Object.keys(entry).includes('plot_visit')) {
          //visit doesn't exist for one of the surveys
          modelsToRefresh.push(model)
          break //don't need to keep searching
        }
      }
    }
  }
  if (modelsToRefresh.length > 0) {
    const isOnline = useEphemeralStore().networkOnline
    if (isOnline) {
      await apiModelsStore.populateVerifiedModels({
        modelNames: modelsToRefresh.filter((m) =>
          allAuthorisedModels.includes(m),
        ),
        projectIds: [authStore.selectedProject.id.toString()],
      })
    } else {
      paratooWarnMessage(
        `The following models needed to be refreshed but we're offline, so skipping: ${modelsToRefresh}`,
      )
    }
  }
  
  const filteredLayouts = dataManager
    .cachedModel({ modelName: 'plot-layout' })
    .filter((layout) => {
      return plotsForProject.some((plot_label) => {
        if (
          !layout?.plot_selection?.plot_label &&
          layout.temp_offline_id &&
          Number.isInteger(layout.plot_selection)
        ) {
          //layout collected offline so resolve plot label from the Plot Selection
          const relevantPlotSelection = cachedSelections
            .find((o) => {
              return o.id === layout.plot_selection
            })
          return relevantPlotSelection.plot_label === plot_label
        }
        return layout.plot_selection.plot_label === plot_label
      })
    })

  const allSurveys = cloneDeep(
    dataManager.cachedModel({ modelName: 'floristics-veg-survey-full' }),
  ).concat(
    cloneDeep(
      dataManager.cachedModel({ modelName: 'floristics-veg-survey-lite' }),
    ),
  )

  const filteredSurveys = (() => {
    const surveys = []
    for (const survey of allSurveys) {
      let relevantCollection = null
      if (!survey?.plot_visit?.id) {
        //survey/vouchers probably collected offline and have not been published yet
        relevantCollection = dataManager.publicationQueue.find(
          (o) =>
            isEqual(o.collection['floristics-veg-survey-full'], survey) ||
            isEqual(o.collection['floristics-veg-survey-lite'], survey),
        )?.collection
      }

      const currSurveyVisit = (() => {
        let visitId = null
        if (relevantCollection) {
          //the survey doesn't have the relation to the visit yet, so need to grab the
          //visit from the publication queue
          if (relevantCollection['plot-visit']?.id) {
            visitId = relevantCollection['plot-visit'].id
          } else if (relevantCollection['plot-visit']?.temp_offline_id) {
            visitId = relevantCollection['plot-visit'].temp_offline_id
          }
        } else {
          visitId = survey.plot_visit.id
        }
        return dataManager
          .cachedModel({ modelName: 'plot-visit' })
          .find((o) => o.id === visitId || o.temp_offline_id === visitId)
      })()
      if (!currSurveyVisit) continue

      const currVisitLayout = (() => {
        let layoutId = null
        if (relevantCollection) {
          if (relevantCollection['plot-layout']?.id) {
            layoutId = relevantCollection['plot-layout'].id
          } else if (relevantCollection['plot-layout']?.temp_offline_id) {
            layoutId = relevantCollection['plot-layout'].temp_offline_id
          }
        } else {
          layoutId = currSurveyVisit.plot_layout.id
        }

        return filteredLayouts.find(
          (l) => l.id === layoutId || l.temp_offline_id === layoutId,
        )
      })()
      if (!currVisitLayout) continue

      const currVisitLayoutPlotLabel = (() => {
        if (currVisitLayout?.plot_selection?.plot_label) {
          return currVisitLayout.plot_selection.plot_label
        } else if (
          !currVisitLayout?.plot_selection?.plot_label &&
          currVisitLayout.temp_offline_id &&
          Number.isInteger(currVisitLayout.plot_selection)
        ) {
          const relevantPlotSelection = apiModelsStore
            .cachedModel({ modelName: 'plot-selection' })
            .find((o) => o.id === currVisitLayout.plot_selection)
          if (relevantPlotSelection) {
            return relevantPlotSelection.plot_label
          }
        }
        return null
      })()
      if (
        !plotsForProject.includes(currVisitLayoutPlotLabel) ||
        !currVisitLayoutPlotLabel
      ) {
        continue
      }
      surveys.push(survey)
    }
    return surveys
  })()

  const vouchers = []
  const decideVoucher = (voucher, variant) => {
    if (
      isNullorEmpty(voucher, `floristics_veg_survey_${variant}`) &&
      !voucher.temp_offline_id
    ) {
      notifyHandler(
        'warning',
        `Something went wrong showing floristics-veg-voucher-${variant}`,
      )
      return false
    }
    return filteredSurveys.some((s) => {
      if (
        !s.id &&
        s.temp_offline_id &&
        voucher.temp_offline_id &&
        voucher[`floristics_veg_survey_${variant}`]?.temp_offline_id
      ) {
        //survey was collected offline; voucher won't have hard link to it so need to
        //find the unlinked survey in the publication queue
        return (
          voucher[`floristics_veg_survey_${variant}`].temp_offline_id ===
          s.temp_offline_id
        )
      } else if (s.id && voucher?.[`floristics_veg_survey_${variant}`]?.id) {
        return s.id === voucher[`floristics_veg_survey_${variant}`].id
      }
      return false
    })
  }
  for (const fullVoucher of dataManager.cachedModel({
    modelName: 'floristics-veg-voucher-full',
  })) {
    if (decideVoucher(fullVoucher, 'full')) {
      vouchers.push(fullVoucher)
    }
  }
  for (const liteVoucher of dataManager.cachedModel({
    modelName: 'floristics-veg-voucher-lite',
  })) {
    if (decideVoucher(liteVoucher, 'lite')) {
      vouchers.push(liteVoucher)
    }
  }
  return vouchers.sort((a, b) => a.label - b.label)
}

export function getLayoutForLocation(location) {
  const apiModelsStore = useApiModelsStore()
  for (const layout of apiModelsStore.cachedModel({
    modelName: 'plot-layout',
  })) {
    if (layout.plot_location.id === location.id) return layout
  }
}

export function getProjectFromProtID(protUUID, getAllProjects = false) {
  const authStore = useAuthStore()
  const allProjects = authStore.projAndProt.map((pp) => {
    const id = pp.id
    const name = pp.name
    const prots = pp.protocols.map((p) => {
      return p.identifier
    })
    return {
      id: id,
      name: name,
      prot: prots.includes(protUUID) ? protUUID : null,
    }
  })
  const projects = allProjects.filter(function (o) {
    if (o.prot) {
      return o
    }
  })
  if (getAllProjects) return projects
  // if getAllProjects is false we are going to process the first one
  return projects.length > 0 ? projects[0] : null
}

// get key of stored oidc config
export function getOIDCKeyFromLocalStorage() {
  if (localStorage.length == 0) return null

  // if localStorage exists
  for (let i = 0; i < localStorage.length; i++) {
    if (localStorage.key(i).substring(0, 4) != 'oidc') continue

    return localStorage.key(i)
  }

  return null
}
// convert coordinates to geojson object
export function coordinatesToGeoJSON(points) {
  const allPoints = [
    {
      polygon: [points.map((o) => [o.lng, o.lat])],
    },
  ]
  const geoCollections = GeoJSON.parse(allPoints, { Polygon: 'polygon' })

  // we only need polygon type feature
  //   so we ignore other types if exists
  return geoCollections.features.find((o) => o.geometry.type === 'Polygon')
}

export function GeoJSONToCoordinates(geoJSON) {
  if (isNullorEmpty(geoJSON, 'geometry')) return []
  return geoJSON.geometry.coordinates.map((p) => {
    return {
      lat: p[0],
      lng: p[1],
    }
  })
}

// calculates area in hectare
export function calculateAreaInHectare(points) {
  let coordinates = [points.map((o) => [o.lat, o.lng])]
  // in polygon, the first and last points are the same
  let firstPoint = coordinates[0][0]
  coordinates[0].push(firstPoint)

  let turfPolygon = turf.polygon(coordinates)
  let areaInSquareMeters = turf.area(turfPolygon)
  return areaInSquareMeters / 10000
}

export function updatedPlotType(newRecord) {
  if (!newRecord) return newRecord
  if (isNullorEmpty(newRecord, 'plot_type')) return newRecord

  // if 'Control' or 'Impact' is checked
  //    we set null/false to permanent
  if (newRecord.plot_type.control || newRecord.plot_type.impact) {
    newRecord.plot_type.permanent = 'N/A'
  }

  return cloneDeep(newRecord)
}

/**
 * Gets the project area based on the current selected project
 *
 * @param {Function} [mapper] a callback function for `Array.map`. By default will map so
 * that we have array of arrays in format [lng, lat], which is what GeoJSON uses
 *
 * @returns {Array.<Array>} an array of arrays, where the inner array is the lng/lat
 * (unless overridden by the `mapper`)
 */
export function getProjectAreaFromContext(mapper = (o) => [o.lng, o.lat], fallback = true) {
  const authStore = useAuthStore()

  const relevantProject = authStore.projAndProt.find(
    (o) => o.id === authStore.currentContext.project,
  )

  let projectBoundary = null
  if (
    relevantProject?.project_area?.coordinates &&
    Array.isArray(relevantProject.project_area.coordinates) &&
    //need at least 3 to make a polygon
    relevantProject.project_area.coordinates.length > 2
  ) {
    // GeoJSON coordinates are in longitude, latitude order - not vice-versa
    projectBoundary = relevantProject.project_area.coordinates.map(mapper)
  } else if (fallback) {
    paratooWarnMessage(
      `Falling back to all-Australia project area for project '${relevantProject.name}'`,
    )
    projectBoundary = australiaOutline.map(mapper)
  }
  return projectBoundary
}

// checks if all the points inside the project boundary or not
// see https://stackoverflow.com/questions/22521982/check-if-point-is-inside-a-polygon
export function insideProjectBoundary(points, recommendLocation = null) {
  const projectBoundary =
    recommendLocation != null ? recommendLocation : getProjectAreaFromContext()
  if (!projectBoundary || projectBoundary.length == 0) {
    paratooWarnMessage(
      'Tried to check if plot points were in the project area but no project boundary could be found, so assuming it is within.',
    )
    return true
  }

  const allPoints = points.map((o) => [o.lng, o.lat])

  for (const point of allPoints) {
    const x = point[0]
    const y = point[1]
    let inside = false
    for (
      let i = 0, j = projectBoundary.length - 1;
      i < projectBoundary.length;
      j = i++
    ) {
      const xi = projectBoundary[i][0],
        yi = projectBoundary[i][1]
      const xj = projectBoundary[j][0],
        yj = projectBoundary[j][1]

      const intersect =
        yi > y != yj > y && x < ((xj - xi) * (y - yi)) / (yj - yi) + xi
      if (intersect) inside = !inside
    }

    // if no inside
    if (!inside) return inside
  }
  return true
}

// loads plots assigned in the selected project
export function loadPlotNames() {
  const apiModelsStore = useApiModelsStore()

  if (isNullorEmpty(apiModelsStore.models, 'plot-selection')) return []

  const plotSelections = apiModelsStore.cachedModel({
    modelName: 'plot-selection',
  })
  if (!plotSelections) return []

  let updatedPlots = []
  plotSelections.forEach((plotSelection) => {
    const plotName = serializePlotLocationName(plotSelection.plot_name)
    updatedPlots.push({
      label: plotName,
      value: plotName,
      description: plotSelection.uuid,
      category: 1,
    })
  })

  return plotSelections.map((p) => {
    const plotName = serializePlotLocationName(p.plot_name)
    return {
      value: p.id,
      label: plotName,
    }
  })
}

// loads unselected plot selections
export function loadUnselectedPlotSelection() {
  const dataManager = useDataManagerStore()
  const authStore = useAuthStore()
  const plotSelections = dataManager.cachedModel({
    modelName: 'plot-selection',
  })
  const plotLayouts = dataManager.cachedModel({ modelName: 'plot-layout' })

  //don't include plots selections that have a layout collected
  const allSelectedPlotSelections = []
  plotLayouts.forEach((plotLayout) => {
    let currPlotLayoutLabel = plotLayout?.plot_selection?.plot_label
    if (!currPlotLayoutLabel && Number.isInteger(plotLayout.plot_selection)) {
      //layout was likely collected offline so the full relation to the selection is not
      //resolved (it's stored only the ID of the Selection)
      currPlotLayoutLabel = plotSelections.find(
        (o) => o.id === plotLayout.plot_selection,
      )?.plot_label
    }

    allSelectedPlotSelections.push(currPlotLayoutLabel)
  })

  let updatedPlots = []
  plotSelections.forEach((plotSelection) => {
    if (!allSelectedPlotSelections.includes(plotSelection.plot_label))
      updatedPlots.push(plotSelection)
  })

  const currentProject = authStore.selectedProject
  const projectPlotsUuids = currentProject.plot_selections.reduce(
    (accum, curr) => {
      accum.push(curr.uuid)
      return accum
    },
    [],
  )

  return (
    updatedPlots
      //only include plots that are assigned to the current project
      .filter((plot) => projectPlotsUuids.includes(plot.uuid))
      .map((p) => ({
        value: p.id,
        label: p.plot_label,
      }))
  )
}

// refresh plot selection model
export async function refreshPlotSelection() {
  const apiModelsStore = useApiModelsStore()
  try {
    await apiModelsStore.populateVerifiedModels({
      modelNames: ['plot-selection']
    })
  } catch (err) {
    paratooWarnMessage('failed to refresh plot-selection model')
  }
}

// refresh plot selection model
export async function refreshInterventions() {
  const apiModelsStore = useApiModelsStore()
  try {
    await apiModelsStore.populateVerifiedModels({
      modelNames: ['interventions']
    })
  } catch (err) {
    paratooWarnMessage('failed to refresh intervention model')
  }
}

// clear all oidc configs
export function clearOIDCFromLocalStorage() {
  if (localStorage.length == 0) return
  try {
    let keys = []
    // get all oidc keys
    for (let i = 0; i < localStorage.length; i++) {
      if (localStorage.key(i).substring(0, 4) != 'oidc') {
        continue
      }
      keys.push(localStorage.key(i))
    }

    // remove all values
    if (keys.length == 0) return
    for (let i = 0; i < keys.length; i++) {
      localStorage.removeItem(keys[i])
    }
  } catch (error) {
    paratooErrorHandler('Failed to clear OIDC from localStorage: ',error)
  }
}

// generate URLSearchParams to send token request
// see https://docs.ala.org.au/?shell#authentication-code-flow
export async function generateOIDCTokenParams(metaData) {
  const currentUrl = new URL(window.location.href)
  const code = currentUrl.searchParams.get('code')
  if (!code) return null
 
  let oidcKey = getOIDCKeyFromLocalStorage()
  if (!oidcKey) return null

  const storedConfig = JSON.parse(localStorage[oidcKey])
  if (!storedConfig) return null

  try {
    return {
      meta_config: await openIdConfiguration(metaData),
      params: new URLSearchParams({
        grant_type: 'authorization_code',
        client_id: process.env.VUE_APP_OIDC_CLIENT_ID,
        code_verifier: storedConfig['code_verifier'],
        code: code,
        redirect_uri: process.env.VUE_APP_OIDC_LOGIN_REDIRECT_URI,
      }),
    }
  } catch (error) {
    paratooErrorHandler('Failed to generate OIDC token params: ',error)
  }
}

// get openId configuration
export async function openIdConfiguration(metaDataUrl) {
  const options = {
    method: 'GET',
    url: metaDataUrl,
  }

  try {
    // so that we can abort all requests using signal when device goes offline
    const ephemeralStore = useEphemeralStore()
    const last = ephemeralStore.fetchAbortControllers.length - 1
    options['signal'] = ephemeralStore.fetchAbortControllers[last].signal
    const response = await axios.request(options)
    const config = response ? response.data : {}
    config['client_id'] = process.env.VUE_APP_OIDC_CLIENT_ID
    config['login_redirect_uri'] = process.env.VUE_APP_OIDC_LOGIN_REDIRECT_URI
    config['logout_redirect_uri'] = process.env.VUE_APP_OIDC_LOGIN_REDIRECT_URI
    config['scopes'] = process.env.VUE_APP_OIDC_SCOPES
    return config
  } catch (error) {
    const isOnline = useEphemeralStore().networkOnline
    if (isOnline) paratooErrorHandler('Failed to set OIDC config: ', error)
  }
}

/**
 * Handles errors that have been caught. Differs from `paratooErrorHandler()` as it
 * doesn't integrate with Sentry's error handling (but does warn) but is for handling the
 * result of a caught error
 *
 * @param {Error} err the error object - if generated by `handleJsonResp()` it will have
 * some useful keys:
 *  - `httpStatus` - the HTTP status code
 *  - `body` - the error object returned by the Strapi API. `body.error.message`
 *     contains the error message
 *  - `message` - the messaged created and formatted by `handleJsonResp()`
 */
export function handleError(err, routerInstance) {
  const authStore = useAuthStore()
  if (
    err.httpStatus &&
    err.httpStatus === 401 &&
    err.body.error.message === 'Auth token is not valid'
  ) {
    notifyHandler(
      'negative',
      'Your credentials have expired, please login again.',
    )
    authStore.doLogout()
    routerInstance.replace('/login')
  } else {
    const msg = (() => {
      let baseMsg = 'An error occurred.'
      if (err.httpStatus) {
        baseMsg += ` HTTP status=${err.httpStatus}.`
      }
      if (err.body) {
        // this is nested and not part of the parent `if` as we might want to check
        //different aspects of the error body in the future
        if (err.body.error && err.body.error.message) {
          baseMsg += ` Server error message: ${err.body.error.message}`
        }
      }

      //lastly, append full error object
      baseMsg += ` ${err}`

      return baseMsg
    })()
    //TODO in this catch-all case, do we want to notify the user too? else the error will
    //be silent
    paratooWarnMessage(msg)
  }
}

/**
 * Gets a particular fire intercept IF it cannot be identified (i.e.,
 * `plant_unidentifiable` is true)
 *
 * @param {Object} fireCollection
 * @param {Number} pointIndex
 * @param {Number} interceptIndex
 * @returns {Object | null} the intercept or null
 */
export function interceptFireFields(
  fireCollection,
  pointIndex,
  interceptIndex,
) {
  let firePoints = null
  if (
    fireCollection &&
    Object.keys(fireCollection).includes('fire-point-intercept-point') &&
    Array.isArray(fireCollection['fire-point-intercept-point'])
  ) {
    firePoints = fireCollection['fire-point-intercept-point']
  }
  if (firePoints) {
    const fireInterceptAtIndex =
      firePoints[pointIndex]['fire_species_intercepts'][interceptIndex]
    if (
      fireInterceptAtIndex &&
      //if the plant cannot be identified then we cannot use the voucher
      fireInterceptAtIndex.plant_unidentifiable === true
    ) {
      return fireInterceptAtIndex
    }
  }
  return null //fallback
}

export function isHashtagTransect(transectSymbol) {
  const hashTagTransects = ['S2', 'N2', 'S4', 'N4', 'W2', 'E2', 'W4', 'E4']
  return hashTagTransects.some((o) => o === transectSymbol)
}
//https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
export function deg2rad(deg) {
  return deg * (Math.PI / 180)
}

/**
 * Find distance between two points based on their coordinates
 *
 * @param {point1} point1 object contain coordinate :{lat:number, lng:number}
 * @param {point2} point2 object contain coordinate :{lat:number, lng:number}
 *
 */
export function getDistanceFromLatLonInM(point1, point2) {
  if (!point1 || !point2) return null
  const lat1 = point1.lat
  const lon1 = point1.lng
  const lat2 = point2.lat
  const lon2 = point2.lng
  const R = 6371000 // Radius of the earth in m
  const dLat = deg2rad(lat2 - lat1)
  const dLon = deg2rad(lon2 - lon1)
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2)
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
  const d = R * c // Distance in m
  return Math.abs(d)
}

export function getCameraTrapVariant(protocolId) {
  const apiModelsStore = useApiModelsStore()
  const variants = ['deployment', 'reequipping', 'retrieval']
  const currProtocol = apiModelsStore.protocolInformation({
    protId: protocolId,
  })
  let currVariant = null
  for (const workflowItem of currProtocol.workflow) {
    variants.forEach((o) => {
      if (`camera-trap-${o}-point` === workflowItem.modelName) {
        currVariant = o
      }
    })
  }
  return currVariant
}

/**
 * Gets the relevant deployments for the given protocol. Useful when doing camera
 * reequipping or retrieval and need the deployments for the selected survey label
 *
 * @param {Number} protocolId the protocol ID to use for store look-up
 *
 * @returns {Array.<Object>} an array of deployment objects obtained from the store
 */
export function getCameraTrapRelevantDeployments(protocolId) {
  const bulkStore = useBulkStore()
  const dataManager = useDataManagerStore()

  const selectedSurveyLabel = bulkStore.collectionGetForProt({
    protId: protocolId,
  })[`camera-trap-${getCameraTrapVariant(protocolId)}-survey`].survey_label
  //`selectedSurveyLabel` is the value of the dropdown where they selected the survey
  //label provided during deployment. This label is unique so reverse look-up to store.
  //We could have the `value` be set to this survey's object, but that would require
  //stringifying it to be compatible with the schema that expects a string.
  //We also check that `survey_type` is defined, as deployment is the only (out of
  //  deployment, reequipping, and retrieval) one that sets it
  const selectedSurvey = dataManager
    .cachedModel({ modelName: 'camera-trap-deployment-survey' })
    .find(
      (o) => o.survey_label === selectedSurveyLabel && !isEmpty(o.survey_type),
    )
  let selectedSurveyId = null
  if (selectedSurvey?.id) {
    selectedSurveyId = selectedSurvey.id
  } else if (selectedSurvey.temp_offline_id) {
    selectedSurveyId = selectedSurvey.temp_offline_id
  }

  //since deployments' schema points to the survey, we need to search all deployments
  //for the selected survey
  const deployments = dataManager.cachedModel({
    modelName: 'camera-trap-deployment-point',
  })
  const retrievals = dataManager.cachedModel({
    modelName: 'camera-trap-retrieval-point',
  })
  const relevantDeployments = deployments.reduce((accum, currDeployment) => {
    let currDeploymentId = null
    if (currDeployment?.camera_trap_survey?.id) {
      currDeploymentId = currDeployment.camera_trap_survey.id
    } else if (currDeployment?.camera_trap_survey?.temp_offline_id) {
      currDeploymentId = currDeployment.camera_trap_survey.temp_offline_id
    }
    const isRetrieved = retrievals.some(
      (o) => o.deployment_id === currDeployment.deployment_id,
    )
    currDeployment.isRetrieved = isRetrieved
    
    if (selectedSurveyId === currDeploymentId) {
      accum.push(currDeployment)
    }

    return accum
  }, [])

  return relevantDeployments
}

export function createCameraTrapRelevantDeploymentOptions(
  modelConfig,
  relevantDeployments
) {
  //rather than inputting manually, use dropdown
  modelConfig.fields.deployment_id.type = 'select'
  // modelConfig.fields.deployment_id.prefer_dropdown = true
  modelConfig.fields.deployment_id.options = relevantDeployments.map((o) => {
    const option = {
      label: o.deployment_id,
      value: o.deployment_id,
      inactive: o.isRetrieved,
    }
    if (o.isRetrieved) {
      option.label = option.label + ' (Retrieved)'
    }
    return option
  })
  return modelConfig
}

/**
 * Modifies the passed `dependentModelConfig` to filter based on a LUT relational dependency.
 *
 * If a selection in LUT A (depends on, e.g., Lure Type) should require some filtering constraints in LUT B (dependent, e.g., Lure Variety), then use this method. LUT B should have a relation to LUT A.
 *
 * This differs from a similar case, Observation Method, which represent it's relation in the other direction due to multiple filtering constrains on the various tiers
 *
 * TODO test when `dependentIsCustomLut` is false
 *
 * @param {Object} dependentModelConfig the model config of the dependent field, passed as a reference
 * @param {String} dependsOnOptionsKey the options key from the model config of the depends on field
 * @param {String} dependsOnFieldName the field name used to save the depends on field to the new record
 * @param {String} dependentRelationFieldName the field name used in the relation of the dependent field, can sometimes be the same as the `dependsOnFieldName`. For example, if LUT B (depends on) has a relation to LUT A (dependent), the field name used in LUT B to point to LUT A is the value of this parameter
 * @param {Boolean} dependentIsCustomLut if the dependent field is a custom LUT, so that we can access the specific `select` attribute of the model config that also wraps the `input` attribute
 * @param {Object} apiModelsStore a reference to the API models store, for looking up the depends on's selected option
 */
export function createDependentFieldConfig(
  dependentModelConfig,
  dependsOnOptionsKey,
  dependsOnFieldName,
  dependentRelationFieldName,
  dependentIsCustomLut,
  apiModelsStore,
) {
  let dependentModelConfig_ = dependentModelConfig
  if (dependentIsCustomLut) {
    dependentModelConfig_ = dependentModelConfig.select
  }
  dependentModelConfig.hidden = true

  dependentModelConfig_.optionsFilter = (newRecord) => {
    //do this outside of the anonymous function below as to not trigger it on every loop
    //(as it is an `Array.filter()` callback)
    const selectedDependsOn = apiModelsStore
      .cachedModel({ modelName: dependsOnOptionsKey })
      .find(
        (p) =>
          p.symbol === newRecord[dependsOnFieldName].value ||
          //when the dependent is also a custom LUT it stores just the string
          p.symbol === newRecord[dependsOnFieldName],
      )

    return (o) => {
      if (
        typeof o[dependentRelationFieldName] === 'object' &&
        selectedDependsOn &&
        o[dependentRelationFieldName].id === selectedDependsOn.id
      ) {
        return o
      }
      if (
        Array.isArray(o[dependentRelationFieldName]) &&
        selectedDependsOn &&
        o[dependentRelationFieldName]
          .map((o) => o.id)
          .some((o) => o === selectedDependsOn.id)
      ) {
        return o
      }
    }
  }
  dependentModelConfig_.optionsFilterNeedsNewRecord = true
}

export async function barcodeCb(newVal, oldVal, inputDefs) {
  const ephemeralStore = useEphemeralStore()
  if (isEqual(newVal, oldVal)) return
  inputDefs.forEach(async (item) => {
    if (
      item.type === 'voucher_barcode' ||
      item.type === 'barcode' ||
      item.type === 'multi_barcode' ||
      item.type === 'multi_barcode_with_custom_fields'
    ) {
      try {
        if (!ephemeralStore.userMedia.camera) {
          ephemeralStore.userMedia.camera =
            await navigator.mediaDevices.getUserMedia({
              video: {
                facingMode: 'environment',
              },
            })
        }
      } catch (err) {
        let msg = 'Failed to start the camera'
        if (ephemeralStore.checkMediaNotificationTimer()) {
          notifyHandler('warning', msg, err)
        }
        paratooWarnHandler(msg, err)
        return
      }
    }
  })
}

/**
 * Wrapper that creates the callback function to handle dependent/condition fields.
 *
 * For example, if you have a LUT whose selection may cause a filter to trigger in
 * another LUT, this callback handles hiding/showing the dependent field and clearing
 * it's data when it's `dependsOn` changes. This works for LUTs that are explicity linked
 * via relations, and LUTs/fields that are not linked, but the dependencies can be
 * specified as code (`dependsOnSelection` or `dependsOnCb`)
 *
 * @returns {Function} the callback function
 */
export function createDependentFieldCallback() {
  /**
   * Callback function to handle conditional fields
   *
   * @param {Object} newVal the `newVal` value of Crud's newRecordWatcher
   * @param {Object} oldVal  the `newVal` value of Crud's newRecordWatcher
   * @param {String} conditionalFields.field the dependency field
   * @param {String} conditionalFields.dependsOn the field that `conditionalFields.field` depends on
   * @param {Array.<String>} conditionalFields.dependsOnSelection an array of LUT symbols that are considered 'valid' selection for the `field` from LUT `dependsOn`
   * @param {Function} conditionalFields.dependsOnCb a callback function to pass to the `dependsOn` field (which must be an array) - useful when the `dependsOn` field is nested deeply. Specify the `dependsOn` as the top-most field and have this callback access it's nested children
   * @param {Object} inputDefs Crud's input definitions (fields)
   *
   * @returns {Object} object with keys `updatedNewRecord` (default null value)
   */
  const cb = function ({ newVal, oldVal, conditionalFields, inputDefs }) {
    /**
     * Handles clearing fields; special case booleans should be set to `false` not `null`
     *
     * @param {*} field the field to check
     * @returns the value to set the field to
     */
    function clearField(field) {
      if (field === true || field === false) {
        return false
      } else {
        return null
      }
    }
    let updatedNewRecord = cloneDeep(newVal)
    let newRecordUpdated = false
    for (const f of conditionalFields) {
      const def = inputDefs.find((o) => o.model === f.field)
      let suppressDependencyHandler = false

      if (f.fieldCb) {
        const fieldCbReturn = f.fieldCb({
          newVal: updatedNewRecord,
          oldVal,
          field: f,
          inputDef: def,
          inputDefs,
        })
        if (fieldCbReturn) {
          //returning boolean means we want to just refresh the fields, not the record
          if (typeof fieldCbReturn != 'boolean') {
            const fieldCbReturnKeys = Object.keys(fieldCbReturn)
            if (
              //rather than just returning the `updatedNewRecord` we've wrapped it in an
              //object with an additional `suppressDependencyHandler` flag
              fieldCbReturnKeys.includes('record') &&
              fieldCbReturnKeys.includes('suppressDependencyHandler')
            ) {
              suppressDependencyHandler =
                fieldCbReturn.suppressDependencyHandler
              if (fieldCbReturn.record !== null)
                updatedNewRecord = fieldCbReturn.record
            } else {
              updatedNewRecord = fieldCbReturn
            }
            if (fieldCbReturn.record !== null) newRecordUpdated = true
          }
        }
      }
      if (
        suppressDependencyHandler !== true &&
        //field was empty then populated - show dependent field
        !oldVal[f.dependsOn] &&
        newVal[f.dependsOn]
      ) {
        if (
          (f.dependsOnSelection === undefined && f.dependsOnCb === undefined) ||
          (f.dependsOnSelection &&
            newVal[f.dependsOn].value &&
            f.dependsOnSelection.some(
              (o) => o === newVal[f.dependsOn].value,
            )) ||
          (f.dependsOnSelection &&
            !newVal[f.dependsOn].value &&
            f.dependsOnSelection.some((o) => o === newVal[f.dependsOn])) ||
          // (f.dependsOnCb && newVal[f.dependsOn].some(f.dependsOnCb))
          //NOTE: two cases: depends on can be many (e.g., multiple camera trap features) or one (single LUT selection)
          (f.dependsOnCb &&
            Array.isArray(newVal[f.dependsOn]) &&
            newVal[f.dependsOn].some(f.dependsOnCb)) ||
          (f.dependsOnCb &&
            !Array.isArray(newVal[f.dependsOn]) &&
            f.dependsOnCb(newVal[f.dependsOn]))
        ) {
          def.hidden = false
        }
      } else if (
        suppressDependencyHandler !== true &&
        //check existence before key access (use lodash as we may be provided empty object)
        //field changed (but not cleared) - clear dependent field's data
        oldVal[f.dependsOn] &&
        newVal[f.dependsOn] &&
        !isEqual(newVal[f.dependsOn], oldVal[f.dependsOn])
      ) {
        if (f.dependsOnSelection === undefined && f.dependsOnCb === undefined) {
          updatedNewRecord[f.field] = clearField(updatedNewRecord[f.field])
          newRecordUpdated = true
        } else {
          if (
            (f.dependsOnSelection &&
              newVal[f.dependsOn].value &&
              !f.dependsOnSelection.some(
                (o) => o === newVal[f.dependsOn].value,
              ) &&
              (newVal[f.dependsOn].value !== f.dependsOnSelection ||
                !newVal[f.dependsOn].includes(f.dependsOnSelection))) ||
            (f.dependsOnSelection &&
              !newVal[f.dependsOn].value &&
              !f.dependsOnSelection.some((o) => o === newVal[f.dependsOn]) &&
              (newVal[f.dependsOn].value !== f.dependsOnSelection ||
                !newVal[f.dependsOn].includes(f.dependsOnSelection))) ||
            // (f.dependsOnCb && !newVal[f.dependsOn].some(f.dependsOnCb))
            //NOTE: two cases (see above)
            (f.dependsOnCb &&
              Array.isArray(newVal[f.dependsOn]) &&
              !newVal[f.dependsOn].some(f.dependsOnCb) &&
              !newVal[f.dependsOn].includes(f.dependsOnSelection)) ||
            (f.dependsOnCb &&
              !Array.isArray(newVal[f.dependsOn]) &&
              !f.dependsOnCb(newVal[f.dependsOn]) &&
              !newVal[f.dependsOn].includes(f.dependsOnSelection))
          ) {
            //they changed the `dependsOn` selection to an invalid value, so clear the field and hide it
            updatedNewRecord[f.field] = clearField(updatedNewRecord[f.field])
            newRecordUpdated = true
            def.hidden = true
          } else {
            //they changed the `dependsOn` selection to something (either from an invalid
            //value to a valid one, or a valid to valid), but it is still valid
            def.hidden = false
          }
        }
      } else if (
        suppressDependencyHandler !== true &&
        //check existence before key access (use lodash as we may be provided empty object)
        //field was populated then cleared - clear dependent field's data (TODO), hide dependent field
        oldVal[f.dependsOn] &&
        !newVal[f.dependsOn]
      ) {
        updatedNewRecord[f.field] = clearField(updatedNewRecord[f.field])
        newRecordUpdated = true
        def.hidden = true
      }
    }
    let returnObj = {
      updatedNewRecord: null,
    }
    if (newRecordUpdated) {
      returnObj.updatedNewRecord = updatedNewRecord
    }
    return returnObj
  }

  return cb
}

/**
 * Wrapper that creates callback function for determining the mask to apply for
 * 'duration'-type fields that can be specified by the seconds, minutes, or minutes and
 * seconds
 *
 * @returns {Function} the callback function
 */
export function createMinSecMaskCallback() {
  const cb = function maskCallback({ newVal, field, inputDef }) {
    switch (newVal[field.dependsOn]) {
      case 'seconds':
        inputDef.mask = '##'
        inputDef.label = `${prettyFormatFieldName(
          inputDef.model,
        )} (sec in format 'ss')`
        break
      case 'minutes':
        inputDef.mask = '##'
        inputDef.label = `${prettyFormatFieldName(
          inputDef.model,
        )} (min in format 'mm')`
        break
      case 'minutes and seconds':
        inputDef.mask = '##:##'
        inputDef.label = `${prettyFormatFieldName(
          inputDef.model,
        )} (min and sec in format 'mm:ss')`
        break
    }
  }

  return cb
}

/**
 * Converts base64 string to `File`
 *
 * Source: https://stackoverflow.com/a/38935990/6476994
 *
 * @param {String} dataurl the base64 string
 * @param {String} filename the file name
 *
 * @returns {File} the converted file
 */
export function dataURLtoFile(dataurl, filename) {
  const Buffer = require('buffer/').Buffer
  var arr = dataurl.split(','),
    mime = arr[0].match(/:(.*?);/)[1],
    // bstr = atob(arr[1]),
    bstr = Buffer.from(arr[1], 'base64').toString('utf8'),
    n = bstr.length,
    u8arr = new Uint8Array(n)

  while (n--) {
    u8arr[n] = bstr.charCodeAt(n)
  }

  return new File([u8arr], filename, { type: mime })
}

/**
 * Determines the most eligible planned point based on user's current location
 *
 * @param {Array.<Object>} plannedPointList a list of planned points, with keys `lat`, `lng`, and `name`
 * @param {Array.<Object>} existingPoints a list of points already saved, with keys `lat`, `lng`, and `name`
 * @param {Number} meterMinimum the minimum number of meters between the eligible point and planned point
 * @param {Object} currentLocation the user's current location, with keys `lat`, `lng`
 *
 * @returns {Object} the eligible point, with keys `lat`, `lng`, `name`, `key`, and `distanceToCurrent`
 */
export function checkEligibleLocationPoint(
  plannedPointList,
  existingPoints,
  meterMinimum,
  currentLocation,
) {
  let eligiblePoint
  for (const [key, point] of Object.entries(plannedPointList)) {
    // Ensure there is not already a point associated by searching for name
    if (
      findIndex(existingPoints, (o) => {
        return o.name === point.name
      }) !== -1
    )
      continue
    // Ensure it is within given number of metres
    const planLoc = { lat: point.lat, lng: point.lng }
    const distanceToCurrent = getDistanceFromLatLonInM(currentLocation, planLoc)
    if (distanceToCurrent > meterMinimum) continue

    // this point is eligible, check if it's closer than the previous
    if (!eligiblePoint || eligiblePoint.distanceToCurrent > distanceToCurrent)
      eligiblePoint = { key, ...point, distanceToCurrent }
  }
  return eligiblePoint
}

/**
 * Adds extra fields or modifies existing body. e.g. plot-selection with project areas
 *
 * @param {String} modelName name of the model
 * @param {Object} body payload to send
 * @param {Object} fullDocumentation documentation
 * @param {Boolean} isMany use many endpoint
 * @param {Boolean} addNonPlotSurveyID adds survey id if true
 *
 * @returns {Object} updated body
 */
export async function updateBody(
  modelName,
  body,
  fullDocumentation,
  isMany = false,
  addNonPlotSurveyID = false,
  protocolId
) {
  paratooSentryBreadcrumbMessage(`updateBody() called with model ${modelName}, isMany=${isMany}, addNonPlotSurveyID=${addNonPlotSurveyID}, protocolId=${protocolId}`)
  paratooSentryBreadcrumbMessage(`body: ${JSON.stringify(body, null, 2)}`)
  if (!body || !modelName) return

  const bulkStore = useBulkStore()
  let savedIDs = []

  //needs to be a function as cannot declare stuff in a switch case
  const handleUpdateOfUniqueIds = () => {
    //assumes that body is an array of collections (but usually it's just one), and that
    //the body's data at the relevantModel is an array of observations
    const modelMapper = {
      //map the `modelName` (of the survey) to the relevant model we're trying to
      //generate the ID for
      'interventions-general-project-information': {
        relevantModel: 'interventions',
        relevantFieldName: 'intervention_id',
        prefix: 'INT',
      },
      'opportunistic-survey': {
        relevantModel: 'opportunistic-observation',
        relevantFieldName: 'observation_id',
        prefix: 'OPP',
      },
      'vegetation-mapping-survey': {
        relevantModel: 'vegetation-mapping-observation',
        relevantFieldName: 'vegetation_mapping_point',
        prefix: 'VMP',
      },
    }
    if (!modelMapper[modelName]) {
      throw paratooErrorHandler(
        `Model mapper for '${modelName}' not found`,
        new Error('Model mapper not found'),
      )
    }
    const relevantModel = modelMapper[modelName].relevantModel
    const relevantFieldName = modelMapper[modelName].relevantFieldName
    savedIDs = getValuesFromUniqueCollections(
      bulkStore,
      relevantModel,
      relevantFieldName,
    )
    for (const index in body) {
      for (const i in body[index][relevantModel]) {
        if (isEmpty(body[index][relevantModel][i][relevantFieldName])) {
          const recordLength = Object.keys(savedIDs).length + 1 + parseInt(i)
          const newId = generateObservationId(
            recordLength,
            modelMapper[modelName].prefix,
            true,
          )
          paratooDebugMessage(
            `Model '${relevantModel}' has empty ID at observation index=${i}, so creating one: ${newId}`,
          )
          body[index][relevantModel][i][relevantFieldName] = newId
        }
      }
    }
  }

  switch (modelName) {
    case 'interventions-general-project-information':
    case 'opportunistic-survey':
    case 'vegetation-mapping-survey':
      handleUpdateOfUniqueIds()
      break
  }
  if (!addNonPlotSurveyID) return body

  // we add additional survey id for non plot based protocols
  const surveyDetails = await generateSurveyIdFromSchema(
    modelName,
    body,
    fullDocumentation,
  )
  if (!surveyDetails) {
    return body
  }

  // submit new survey and extract id
  const submittedSurvey = await submitNonPlotSurvey(
    surveyDetails.survey_model,
    surveyDetails.survey_body,
    protocolId
  )
  if (!submittedSurvey) {
    return body
  }

  // if many
  if (isMany) {
    // update survey id and add minted identifier
    for (const index in body[`${modelName}s`]) {
      body[`${modelName}s`][index]['data'][surveyDetails.obs_survey_field] =
        submittedSurvey.id
    }
    body['orgMintedIdentifier'] = submittedSurvey.orgMintedIdentifier
    return body
  }

  // if non "many"
  for (const index in body) {
    body[index][surveyDetails.obs_survey_field] = submittedSurvey.id
    body[index].orgMintedIdentifier = submittedSurvey.orgMintedIdentifier
    // we call "org/api/status/" to check whether the collection is submitted or not
    //   as we have only one collection identifier for bulk.
    // for multiple non bulk request, the first one should be true and the rest should be false
    //   as we call "org/api/status/" every time we send request.
    body[index].collectionIdSubmitted = index == 0 ? false : true
  }
  return body
}

/**
 * submits survey and creates minted identifier
 *
 * @param {String} survey_model name of the survey model
 * @param {Object} survey_body payload to send
 *
 * @returns {Object} submitted id and minted identifier
 */
export async function submitNonPlotSurvey(survey_model, survey_body, protocolId) {
  // we need mint identifier as well
  const bulkStore = useBulkStore()
  const orgMintedIdentifier = await bulkStore.createOrgMintedIdentifier(
    survey_body.survey_metadata,
    null,
    protocolId
  )
  // for pdp we sending minted identifier as well
  survey_body['orgMintedIdentifier'] = orgMintedIdentifier
  if (!orgMintedIdentifier) return null

  const documentationStore = useDocumentationStore()
  const endpoint = await documentationStore.findEndpointFromModel(
    survey_model,
    { root: true },
  )
  const survey_end_point = `${coreApiPrefix}${endpoint}`
  let resp = await doCoreApiPost(
    {
      urlSuffix: survey_end_point,
      body: {
        data: survey_body,
      },
    },
    { root: true },
  )
  if (!resp) return null

  return {
    id: resp.data.id,
    orgMintedIdentifier: orgMintedIdentifier,
  }
}

/**
 * generate survey id from the field and model extracted from schema
 *
 * @param {String} modelName name of the model
 * @param {Object} body payload to send
 * @param {Object} fullDocumentation documentation
 *
 * @returns {Object} updated body
 */
export async function generateSurveyIdFromSchema(
  modelName,
  body,
  fullDocumentation,
) {
  if (!body || !modelName) return null

  const currSchema =
    fullDocumentation.components.schemas[modelNameToSchemaName(modelName)]
  if (!currSchema.properties.data) return null

  // we assume survey field has survey prefix
  const survey_data = findMatchedField(currSchema.properties.data.properties, [
    'survey',
  ])
  if (!survey_data) {
    return null
  }
  // flags both x-paratoo-add-minted-identifier and x-model-ref
  // have to be in a valid non plot survey field
  if (isNullorEmpty(survey_data.value, 'x-model-ref')) {
    return null
  }
  if (survey_data.value['x-paratoo-add-minted-identifier'] == false) {
    return null
  }

  // generate survey payload to upload new survey
  const survey_model = survey_data.value['x-model-ref']
  const survey_schema =
    fullDocumentation.components.schemas[modelNameToSchemaName(survey_model)]
  // we assume surveyID field has survey/id prefix
  const survey_id = findMatchedField(survey_schema.properties.data.properties, [
    'survey',
    'metadata',
  ])
  const start_date = findMatchedField(
    survey_schema.properties.data.properties,
    ['start', 'date'],
  )

  // survey body to send
  const id = generateSurveyId(survey_model, fullDocumentation)
  return {
    obs_survey_field: survey_data.key,
    survey_model: survey_model,
    survey_body: {
      [survey_id.key]: id,
      [start_date.key]: id.time,
    },
  }
}

/**
 * Determines the matched value using a list of full/partial texts
 * NOTE: all strings in patterns array have to fully or partially match
 *
 * @param {Object} source source object
 * @param {Array} patterns a list of strings to find matched or partially matched key from the source
 *
 * @returns {Object} matched key and the value
 */
export function findMatchedField(source, patterns) {
  if (!source || !patterns) {
    return null
  }

  for (const field of Object.keys(source)) {
    // checks all the patterns
    const status = []
    for (const pattern of patterns) {
      // partially or fully match
      if (!field.toLocaleLowerCase().includes(pattern.toLocaleLowerCase())) {
        continue
      }
      status.push(pattern)
    }
    if (!isEqual(status, patterns)) {
      continue
    }

    return {
      key: field,
      value: source[field],
    }
  }

  return null
}

/**
 * Determines if the given project has a project area
 *
 * @param {Object} projectObj the options object used for plot selection, in the form:
 *
 * ```json
 * {
 *    project: {
 *      project_area: {
 *        coordinates: []
 *      }
 *    }
 * }
 * ```
 * @returns {Boolean} whether there is no project area
 */
export function noProjectAreaCb(projectObj) {
  if (isNullorEmpty(projectObj.project.project_area, 'coordinates')) return true

  const projArea = projectObj.project.project_area.coordinates
  return Array.isArray(projArea) ? projArea.length === 0 : true
}

/**
 * Gets all the selected projects for Plot Selection that have no project area
 *
 * @returns {Array.<Object>} project metadata where each object has the project `label`
 * (name), `project` object (from auth store) and `value` (the ID of the project used for
 * quasar dropdowns)
 */
export function emptyProjectAreas() {
  const bulkStore = useBulkStore()
  const empties = []
  for (const proj of bulkStore.getPlotSelectionRelevantProjects) {
    if (noProjectAreaCb(proj) && !bulkStore.getNewProjectArea(proj.value)) {
      empties.push(proj)
    }
  }

  return empties
}

/**
 * Submits the collection through and redirects user to protocols page
 *
 * @param {Object} workflowItem the current workflow steps
 * @param {Number} protocolId the ID of the protocol to submit
 * @param {Object} [submoduleInfo] the submodule info, if relevant. Pass both protocol
 * entry and workflow item (even though workflow item is inside protocol entry) so that
 * we don't need to call `makeWorkflow()` from here
 * @param {Object} [submoduleInfo.protocolEntry] the submodule's protocol entry
 * @param {Object} [submoduleInfo.workflow] the submodule's workflow item
 * @param {Number} [offlinePublicationQueueIndex] the index in the data manager's
 * publication queue (if it was collected offline)
 * @param {Object} [nextProtInfo] the protocol info for the next protocol to redirect to
 * (if said protocol has a redirect)
 * @param {Array} [ajvValidationErrors] the ajv validation errors
 *
 * @returns {Object | null} the response from Core, or null if no response (unsuccessful)
 */
export async function workflowSubmission(
  workflowItem,
  protocolId,
  routerInstance,
  submoduleInfo = null,
  offlinePublicationQueueIndex = null,
  nextProtInfo = null,
  ajvValidationErrors = [],
) {
  const authStore = useAuthStore()
  const dataManager = useDataManagerStore()
  let resp = null
  if (offlinePublicationQueueIndex !== null) {
    //this function was called by the dataManager, so we must be submitting queue to core
    await submitCollection(
      workflowItem,
      protocolId,
      null,
      submoduleInfo,
      offlinePublicationQueueIndex,
    )
      .then((res) => {
        resp = res
      })
      .catch((err) => {
        throw err
      })
  } else {
    //this function was called by Workflow.vue, so must be queuing for later submission
    try {
      await dataManager.queueSubmission({
        protocolId: protocolId,
        workflowItem: workflowItem,
        submoduleInfo: submoduleInfo,
        ajvValidationErrors
      })
    } catch (err) {
      let msg = 'Failed to queue the collection'
      notifyHandler('negative', msg, err)
      paratooErrorHandler(msg, err)
    }
  }

  if (nextProtInfo && authStore.protocols.includes(nextProtInfo.id)) {
    confirmRedirectToOtherProt(nextProtInfo, routerInstance)
  } else {
    redirectProjectScreen(true, routerInstance)
  }

  return resp
}

/**
 * Submits the collection to bulk while handling submodules
 *
 * @param {Object} workflowItem Current workflow set of steps
 * @param {Number} protocolId the ID of the protocol to submit
 * @param {String} [protocolRowDataKey] The model name (e.g. `bird-survey`), refers to the step
 * @param {Object} [submoduleInfo] the submodule info, if relevant. Pass both protocol
 * entry and workflow item (even though workflow item is inside protocol entry) so that
 * we don't need to call `makeWorkflow()` from here
 * @param {Object} [submoduleInfo.protocolEntry] the submodule's protocol entry
 * @param {Object} [submoduleInfo.workflow] the submodule's workflow item
 * @param {Number} [offlinePublicationQueueIndex] the index in the data manager's
 * publication queue (if it was collected offline)
 *
 * @returns {Object | null} the response from Core, or null if no response (unsuccessful)
 */
export async function submitCollection(
  workflowItem,
  protocolId,
  protocolRowDataKey = null,
  submoduleInfo = null,
  offlinePublicationQueueIndex = null,
) {
  const bulkStore = useBulkStore()
  const apiModelsStore = useApiModelsStore()
  let mainProtocolRes = null
  //offline queue might submit many collections so let it handle loading spinners
  if (offlinePublicationQueueIndex === null) Loading.show()
  try {
    // We can do a direct post if there's only one model to submit
    const isBulkPost =
      workflowItem.length !== 1 &&
      !(
        //this case is when we have 2 steps but one of them is a non-model step (i.e., it
        //isn't submittable to core but is still used for data collection), which is
        //effectively non-bulk
        (
          workflowItem.some((o) => o.isNonModelStep) &&
          workflowItem.length === 2
        )
      )
    // main protocol which is submitted first
    mainProtocolRes = await handleSubmission(
      workflowItem,
      protocolId,
      isBulkPost,
      protocolRowDataKey,
      // TODO: for multiple workflowItem
      workflowItem[0].useManyEndpoint,
      workflowItem[0].addSurveyID,
      offlinePublicationQueueIndex,
    )

    // submit submodule
    if (
      submoduleInfo &&
      //if we're submitting an offline collection, we let the dataManager's
      //`submitOfflineQueue()` handle this, else we will attempt to publish it twice
      !offlinePublicationQueueIndex
    ) {
      if (submoduleInfo.protocolEntry.module === 'Plant Tissue Vouchering') {
        // Edge case for floristics with plant tissue vouchering, where floristics is
        // submitted first to get its id for relation with plant tissue vouchering.
        // Offline Floristics that collect PTV as submodule are handled by the data manager
        handleFloristicsWithPTV(
          mainProtocolRes,
          submoduleInfo.protocolEntry,
          protocolId,
        )
      }

      //if submit from the offline queue, any relation dependencies (e.g., vouchers) are
      //already resolved, so can just pass submission on
      const submoduleIsBulkPost = submoduleInfo.workflow.length !== 1
      await handleSubmission(
        submoduleInfo.workflow,
        submoduleInfo.protocolEntry.id,
        submoduleIsBulkPost,
      )

      bulkStore.removeSubmodule(protocolId)
    }

    if (offlinePublicationQueueIndex === null) {
      Loading.hide()
      //like spinner, let offline collections be handled in one place
      const prot = apiModelsStore.protocolInformation({ protId: protocolId })
      notifyHandler(
        'positive',
        `Successfully queued the collection for the ${prot.name} protocol`,
      )
    }

    // clean up binary files of this protocol in the indexedDB
    binary.cleanupBinaryFileAfterProtSubmission(mainProtocolRes)
  } catch (error) {
    //only catch this error to hide spinner (if relevant), so need to re-throw
    if (offlinePublicationQueueIndex === null) Loading.hide()
    throw error
  }
  //since default null, this will return that on unsuccessful submission
  return mainProtocolRes
}

/**
 * Handles submission of a single collection
 *
 * @param {Object} workflowItem Current workflow set of steps
 * @param {Number} protocolId the ID of the protocol to submit
 * @param {Boolean} [isBulkPost] whether the collection has a bulk endpoint. Default `true`
 * @param {String} [protocolRowDataKey] The model name (e.g. `bird-survey`), refers to the step
 * @param {Boolean} [isMany] Whether the protocol has a `many` endpoint. Defaults `false`
 * @param {Number} [offlinePublicationQueueIndex] the index in the data manager's
 * publication queue (if it was collected offline)
 * @param {Boolean} [retryMintOrgIdentifier] whether we need to redo /mint-identifier
 *
 * @returns {Object} response from Core
 */
export async function handleSubmission(
  workflowItem,
  protocolId,
  isBulkPost = true,
  protocolRowDataKey = null,
  isMany = false,
  addSurveyID = false,
  offlinePublicationQueueIndex = null,
  retryMintOrgIdentifier = false,
) {
  const bulkStore = useBulkStore()
  const authStore = useAuthStore()
  const apiModelsStore = useApiModelsStore()
  try {
    const modelName = await getModelName(
      workflowItem,
      protocolRowDataKey,
      isBulkPost,
      protocolId,
    )

    const res = await bulkStore.upload({
      modelName: modelName,
      isBulk: isBulkPost,
      protocolId: protocolId,
      isMany: isMany,
      addSurveyID: addSurveyID,
      offlinePublicationQueueIndex: offlinePublicationQueueIndex,
      retryMintOrgIdentifier: retryMintOrgIdentifier,
    })

    await bulkStore.clearCollections({
      protocolId: protocolId,
      offlinePublicationQueueIndex: offlinePublicationQueueIndex,
    })

    // reset auto saved values connected to the model name
    resetOneAutoSavedCollection(modelName)

    if (modelName === 'plot-selection') {
      bulkStore.clearPlotSelectionTmpData(protocolId)
    }

    if (Array.isArray(res) && res.length === 1) {
      return res[0]
    } else {
      return res
    }
  } catch (error) {
    if (
      error?.cause === 'ERR_MISSING_MINTED_ORG_IDENTIFIER' &&
      //don't want to keep retrying - if this call is a retry, we don't want to endlessly
      //recursive call
      retryMintOrgIdentifier !== true
    ) {
      paratooDebugMessage(`Will retry submission and re-request the org minted identifier`)
      //core indicated it got a 404 from org w.r.t. the org minted identifier, so we'll
      //need to do the /mint-identifier request again
      return await handleSubmission(
        workflowItem,
        protocolId,
        isBulkPost,
        protocolRowDataKey,
        isMany,
        addSurveyID,
        offlinePublicationQueueIndex,
        true,   //`retryMintOrgIdentifier`
      )
    }

    const currentUser = authStore?.userProfile?.username || 'unknown'

    let httpStatus = null
    if (!isNullorEmpty(error, 'httpStatus')) {
      httpStatus = error.httpStatus
    }

    const prot = apiModelsStore.protocolInformation({ protId: protocolId })
    // if status code is 403 forbidden then user should
    //   know whether he is allowed to create or update collection
    const errorMsg =
      httpStatus == '403'
        ? currentUser +
          ` is not authorized to upload the collection for the ${prot.name} protocol`
        : `Failed to upload the collection for the ${prot.name} protocol`

    //if offline, handle all errors/messages in one place (dataManager)
    if (offlinePublicationQueueIndex === null)
      notifyHandler('negative', errorMsg)
    paratooErrorHandler('Failed to upload to core', error)
    throw error
  }
}

/**
 * Updates the submodule's store entry with the floristics voucher IDs, so that
 * submission can create relations in the backend
 *
 * @param {Object} mainProtocolRes the API response of the parent module, which contains
 * the key `floristics-veg-voucher-<variant>`
 * @param {Object} subModuleInfo the submodule info
 * @param {Number} parentModuleProtocolId the protocol ID of the parent module
 */
export function handleFloristicsWithPTV(
  mainProtocolRes,
  subModuleInfo,
  parentModuleProtocolId,
) {
  const bulkStore = useBulkStore()

  const PTVProtocolId = subModuleInfo.id
  const protocolVariant = bulkStore.getSubModuleVariant(parentModuleProtocolId)

  const floristicsVouchers =
    mainProtocolRes[`floristics-veg-voucher-${protocolVariant}`]
  const floristicsIdMap = floristicsVouchers.map((voucher) => {
    const result = {
      id: voucher['id'],
      unique_id: voucher['unique_id'],
    }
    return result
  })

  //clone so we don't modify the store (yet)
  let storeEntry = cloneDeep(
    bulkStore.collectionGetForProt({ protId: PTVProtocolId }),
  )['floristics-veg-genetic-voucher']
  for (let ptv of storeEntry) {
    const relevantFloristicsVoucher = floristicsIdMap.find(
      (o) => o.unique_id === ptv[`floristics_voucher_${protocolVariant}`],
    )
    ptv[`floristics_voucher_${protocolVariant}`] = relevantFloristicsVoucher.id
  }
  bulkStore.recvGeneral({
    modelName: 'floristics-veg-genetic-voucher',
    content: storeEntry,
    protocolId: PTVProtocolId,
  })
}

/**
 * Get model name using workflowItem
 *
 * @param {Object} workflowItem Current workflow set of steps
 * @param {String} protocolRowDataKey Model name (e.g. 'bird-survey'), refers to the step
 * @param {Boolean} isBulkPost whether the collection has a bulk endpoint
 * @param {Number} protocolId the ID of the protocol to submit
 *
 * @returns {String} Model name (e.g. 'bird-survey')
 */
export async function getModelName(
  workflowItem,
  protocolRowDataKey,
  isBulkPost,
  protocolId,
) {
  const dataManager = useDataManagerStore()
  let modelName
  for (let item in workflowItem) {
    if (workflowItem[item].title === 'Plot Description') {
      isBulkPost = false
      modelName = 'plot-location'
    }
    if (workflowItem[item].title === 'Plot Selection Project Information') {
      isBulkPost = false
    }
  }
  if (isBulkPost) {
    // If it's a bulk, we search for the model with the SurveyID
    modelName =
      workflowItem[
        findIndex(workflowItem, function (o) {
          return o.crud.mainSurveyFlag == true
        })
      ].crud.rowDataKey
  }
  // if it only the plot visit we want to send that is within another protocol,
  // use this to send of the visit across to strapi
  else if (protocolRowDataKey === 'plot-visit') {
    modelName = protocolRowDataKey
  } else {
    // For non bulks, we know it should be the only key in the collection
    modelName = Object.keys(
      dataManager.collectionGetForProt({ protId: protocolId }),
    )[0]
  }
  for (let item in workflowItem) {
    if (workflowItem[item].title === 'Plot Description') {
      isBulkPost = false
      modelName = 'plot-location'
    }
  }
  return modelName
}

//TODO JSDoc
export function redirectProjectScreen(isSubmitted, routerInstance) {
  const ephemeralStore = useEphemeralStore()
  if (
    isSubmitted &&
    routerInstance &&
    //don't bother redirecting if already on projects screen
    !routerInstance.currentRoute._value.path.includes('project')
  ) {
    routerInstance.replace('/')
    const prevProtocol = ephemeralStore.prevProtocol
    ephemeralStore.prevProtocol = null
    if (!prevProtocol) {
      return routerInstance.replace('/')
    }
    // redirect to prev protocol if user used quick access to opportune
    // keep current context
    routerInstance.replace(prevProtocol)
  }
}

//TODO JSDoc
export function confirmRedirectToOtherProt(nextProtInfo, routerInstance) {
  const authStore = useAuthStore()
  Dialog.create({
    title: 'Confirm',
    // TODO: make this dynamic
    message: `Do you want to do ${nextProtInfo.name} protocol?`,
    ok: 'Yes',
    cancel: 'No (go back to project screen)',
  })
    .onOk(() => {
      const currentProjectId = authStore.currentContext.project
      authStore.doCurrentContext({
        protocol: {
          id: nextProtInfo.id,
          version: nextProtInfo.version,
        },
        project: currentProjectId,
      })
      routerInstance.replace(`/workflow/${nextProtInfo.id}`)
    })
    .onCancel(() => {
      redirectProjectScreen(true, routerInstance)
    })
}

export function makePlotLayoutOptions() {
  const dataManager = useDataManagerStore()
  const authStore = useAuthStore()

  const currentProject = authStore.selectedProject
  const projectPlotsUuids = currentProject.plot_selections.reduce(
    (accum, curr) => {
      accum.push(curr.uuid)
      return accum
    },
    [],
  )

  const layouts = dataManager.cachedModel({ modelName: 'plot-layout' })
  const selections = dataManager.cachedModel({ modelName: 'plot-selection' })
  return layouts
    .filter((layout) => {
      if (
        //layout collected offline (temp ID and the Selection isn't fully resolved from
        //the API)
        layout.temp_offline_id &&
        Number.isInteger(layout.plot_selection)
      ) {
        const currLayoutSelection = selections.find(
          (s) => s.id === layout.plot_selection,
        )
        return projectPlotsUuids.includes(currLayoutSelection.uuid)
      }
      return projectPlotsUuids.includes(layout.plot_selection.uuid)
    })
    .map(makePlotLayoutOptionsMapper())
}

export function makePlotLayoutOptionsMapper() {
  const apiModelsStore = useApiModelsStore()
  return (e) => {
    if (e.id) {
      return {
        value: e.id,
        label: e.plot_selection.plot_label,
      }
    } else if (e.temp_offline_id) {
      return {
        //value needs to be an object as we look for the `temp_offline_id`
        //key during submission when we resolve these temp IDs
        value: {
          temp_offline_id: e.temp_offline_id,
        },
        label: (() => {
          if (e.plot_selection?.plot_label) {
            return e.plot_selection.plot_label
          } else if (Number.isInteger(e.plot_selection)) {
            //likely an ID of the Plot Selection, so need to resolve (we can
            //assume it was collected online, so don't use
            //`dataManager.cachedModel()`)
            const relevantPlotSelection = apiModelsStore
              .cachedModel({ modelName: 'plot-selection' })
              .find((o) => o.id === e.plot_selection)
            return relevantPlotSelection.plot_label
          }
        })(),
      }
    }
  }
}

/**
 * Determines the variant (full or lite) of the provided voucher, which must contain
 * a relation to it's respective survey
 *
 * @param {Object} voucher the voucher object
 *
 * @returns {'Full'|'Lite'|'Error'} the voucher variant in human-readable format
 */
export function voucherVariantFromVoucher(voucher) {
  const dataManager = useDataManagerStore()

  const voucherKeys = Object.keys(voucher)
  if (
    voucherKeys.includes('floristics_veg_survey_full') ||
    voucherKeys.includes('voucher_full')
  ) {
    return 'Full'
  } else if (
    voucherKeys.includes('floristics_veg_survey_lite') ||
    voucherKeys.includes('voucher_lite')
  ) {
    return 'Lite'
  } else if (voucherKeys.includes('temp_offline_id')) {
    let variant = 'Error'
    //this voucher was collected offline
    dataManager.publicationQueue.forEach((o) => {
      for (const v of ['full', 'lite']) {
        if (o.collection[`floristics-veg-voucher-${v}`]) {
          const foundOfflineVoucher = o.collection[
            `floristics-veg-voucher-${v}`
          ].find((v) => v.temp_offline_id === voucher.temp_offline_id)
          if (foundOfflineVoucher) {
            variant = v
          }
        }
      }
    })
    return variant
  } else return 'Error'
}

/**
 * Makes the host species options for Floristics by grabbing all vouchers for the
 * project area
 *
 * @returns {Object} `voucherOptions`, which are the mapped options, and
 * `vouchersFullObjs`, which are the unmapped options
 */
export async function makeHostSpeciesOptions() {
  const vouchersFullObjs = await getVouchersForProjectArea()
  const vouchers = vouchersFullObjs.map(makeHostSpeciesOptionsMapper())

  return {
    voucherOptions: vouchers,
    vouchersFullObjs: vouchersFullObjs,
  }
}

/**
 * Creates the options mapper for the host species for Floristics. Can also handle when
 * the host species is a re-collected voucher
 *
 * @returns {Object} `value` and `label` with the field name and voucher barcode
 */
export function makeHostSpeciesOptionsMapper() {
  const dataManager = useDataManagerStore()
  return (e) => {
    if (!e.field_name) {
      //virtual vouchers have reference to the actual voucher
      const currVoucherVariant = voucherVariantFromVoucher(e).toLowerCase()
      const voucherVariantToUse = `floristics-veg-voucher-${currVoucherVariant}`
      const resolvedVoucher = dataManager
        .cachedModel({ modelName: voucherVariantToUse })
        .find((o) => {
          if (
            !o.id &&
            o.temp_offline_id &&
            e[`voucher_${currVoucherVariant}`]?.temp_offline_id
          ) {
            return (
              o.temp_offline_id ===
              e[`voucher_${currVoucherVariant}`].temp_offline_id
            )
          }
          return o.id === e[`voucher_${currVoucherVariant}`]
        })
      return {
        value: `${resolvedVoucher.field_name} (re-collected) (${resolvedVoucher.voucher_barcode})`,
        label: `${resolvedVoucher.field_name} (re-collected) (${resolvedVoucher.voucher_barcode})`,
      }
    }

    return {
      value: `${e.field_name} (${e.voucher_barcode})`,
      label: `${e.field_name} (${e.voucher_barcode})`,
    }
  }
}

//TODO JSDoc
export function makeSelectableVoucherOptionsMapper(valueIsObject = false) {
  const dataManager = useDataManagerStore()
  return (o) => {
    const voucherKeys = Object.keys(o)
    const currVoucherCollectedOffline =
      o.temp_offline_id &&
      (!o.floristics_veg_survey_full || !o.floristics_veg_survey_lite)
    let variant = null
    let relatedPlotLabel = null
    if (currVoucherCollectedOffline) {
      //voucher was likely collected offline
      let breakLoops = false //don't want too much UI-blocking logic
      for (const v of ['full', 'lite']) {
        if (breakLoops) break
        for (const queuedItem of dataManager.publicationQueue) {
          if (breakLoops) break
          if (
            Array.isArray(
              queuedItem.collection[`floristics-veg-voucher-${v}`],
            ) &&
            queuedItem.collection[`floristics-veg-voucher-${v}`].length > 0
          ) {
            for (const offlineVoucher of queuedItem.collection[
              `floristics-veg-voucher-${v}`
            ]) {
              if (offlineVoucher.temp_offline_id === o.temp_offline_id) {
                //don't need to check survey, as looking-up this voucher based on
                //the variant (`v`) is sufficient
                variant = v
                breakLoops = true
                console.log(queuedItem.collection['plot-layout'])
                const relatedPlot = queuedItem.collection['plot-layout']
                const realtedPlotId = relatedPlot?.id || relatedPlot?.temp_offline_id
                relatedPlotLabel = dataManager.getPlotSelectionData({ field: 'plot_label', plotLayoutId : realtedPlotId })
                break
              }
            }
          }
        }
      }
    } else {
      for (const v of ['full', 'lite']) {
        if (voucherKeys.includes(`floristics_veg_survey_${v}`)) {
          variant = v
        }
      }
    }
    let idToUse = o.id
    if (currVoucherCollectedOffline) {
      idToUse = {
        temp_offline_id: o.temp_offline_id,
      }
    } else {
      relatedPlotLabel = o[`floristics_veg_survey_${variant}`]?.plot_visit?.plot_layout?.plot_selection?.plot_label
    }
    
    let valueToUse = idToUse
    if (valueIsObject) {
      valueToUse = {
        id: idToUse,
        variant: variant,
      }
    }
    let label = `${o.field_name} [${o.voucher_barcode}]`
    if(relatedPlotLabel) {
      label += ` (${relatedPlotLabel})`
    } else {
      paratooWarnMessage(`no plot found for voucher ${o.field_name} [${o.voucher_barcode}]`)
    }

    return {
      value: valueToUse,
      label,
      voucherVariant: variant,
      relatedPlotLabel,
      fieldName: o.field_name,
      barcode: o.voucher_barcode,
    }
  }
}

//TODO JSDoc
export function makeVertTrapsInitOptions(selectedSetupSurvey) {
  if (!selectedSetupSurvey) return []
  const { cachedModel } = useDataManagerStore()
  const trapsLineData = cachedModel({ modelName: 'vertebrate-trap-line' })

  let selectedSetupSurvey_ = null
  if (typeof selectedSetupSurvey === 'object' && selectedSetupSurvey.id) {
    //setup survey collected offline
    selectedSetupSurvey_ = selectedSetupSurvey.id
  } else {
    selectedSetupSurvey_ = selectedSetupSurvey
  }

  const associatedTrapsLineData = trapsLineData.filter((line) => {
    if (line?.trapping_survey?.id) {
      return line.trapping_survey.id === selectedSetupSurvey_
    }
    if (line?.trapping_survey?.temp_offline_id) {
      return line.trapping_survey.temp_offline_id === selectedSetupSurvey_
    }
  })

  const dropdownOptions = associatedTrapsLineData.flatMap((line) => {
    const options = line.traps.map((trap) => {
      return {
        ...trap,
        label: trap.trap_number,
      }
    })
    return options
  })
  return dropdownOptions
}

/* Determines if the given plot is plot-based
 *
 * @param {Number} id the protocol ID
 *
 * @returns {Boolean} whether the protocol is plot-based
 */
export function isPlotBasedProtocol(id) {
  const apiModelsStore = useApiModelsStore()
  let info = apiModelsStore.protocolInformation({
    protId: id,
  })
  try {
    //if plot-layout is the only model, then it's the plot layout protocol and
    //shouldn't be disabled
    return (
      info.workflow[0].modelName === 'plot-layout' && info.workflow.length !== 1
    )
  } catch {
    //likely no workflow, so we can assume it's probably not plot-based
    return false
  }
}

/**
 * Determines the mode of operation for the given `protocolCase`. Each case must be
 * manually 'registered' by the developer before being consumed by the app
 *
 * @param {String} protocolCase the case registered in the `switch`
 *
 * @returns {*} the conditional value the consumer needs determined based on the desired
 * mode of operation
 */
export function determineModeOfOperation(protocolCase) {
  let invalidModeMsg = `Tried to determine the mode of operation for case '${protocolCase}' but the mode of operation '${modeOfOperation}' is invalid`
  //NOTE: extend cases as-needed
  switch (protocolCase) {
    case 'coverPiTransectMaxPoint':
      if (modeOfOperation === 0) {
        return 4
      } else if (modeOfOperation === 1) {
        return 100
      } else {
        paratooWarnMessage(invalidModeMsg)
        return 100
      }
    case 'herbivoryNumberOfQuadratsRequired':
      if (modeOfOperation === 0) {
        return 5
      } else if (modeOfOperation === 1) {
        return 100
      } else {
        paratooWarnMessage(invalidModeMsg)
        return 100
      }
    case 'herbivoryActive':
      if (modeOfOperation === 0) {
        return 30000
      } else if (modeOfOperation === 1) {
        return 3600000
      } else {
        paratooWarnMessage(invalidModeMsg)
        return 100
      }
    default:
      paratooWarnMessage(
        `determineModeOfOperation() provided with unhandled 'protocolCase'=${protocolCase}. Please register/handle this case if desired`,
      )
  }
}

export function checkFloristicsVoucherInfo(protocolID) {
    switch (protocolID) {
      case floristicsLiteUuid:
        return {
          uuid:floristicsLiteUuid,
          barcode: true,
          plotName: true,
        }
      default:
        paratooWarnMessage(`Programmer error: ${protocolID} is an unregistered case for checkFloristicsVoucherInfo()`)
        return {
          uuid:protocolID,
          barcode: false,
          plotName: false,
        }
    }
}

/**
 * Auto-selects 'other' from the custom LUT component when free text is entered.
 *
 * Custom LUTs are components structured with two attributes:
 *  - <name>_text: the custom input
 *  - <name>_lut: the LUT relation where we auto-fill 'other'
 *
 * @param {*} newRecord a reference to the record containing the custom LUT
 * @param {String} fieldName the field name of the custom LUT (<name>)
 * @param {Object} record the model config
 * @param {Number} [index] the index of the custom LUT in the new record if it's
 * repeatable. Defaults to null
 *
 * @returns {*} the `newRecord` modified with the auto-filled LUT
 */
export function autofillCustomLutOtherIfRequired(
  newRecord,
  fieldName,
  record,
  index = null,
) {
  const apiModelsStore = useApiModelsStore()
  //easier to treat everything as arrays for less code duplication
  let newRec = null
  let idx = index
  if (index === null) {
    newRec = [newRecord]
    idx = 0
  } else {
    newRec = newRecord
  }

  const resolvedLutSymbol = apiModelsStore.resolvedLutSymbol({
    symbol: newRec[idx][fieldName],
    modelName: record.select.optionsKey,
    suppressWarnings: true,
  })

  if (resolvedLutSymbol === null) {
    let otherLutSymbol = null
    try {
      //use `cachedModel` rather than doing `find` on the `inputDefs`
      //as the latter will remove 'other' from the output
      const options = apiModelsStore.cachedModel({
        modelName: record.select.optionsKey,
      })

      const otherLutEntry = options.find((o) =>
        potentialOtherValues.some((p) => o.label.toLowerCase() === p),
      )
      otherLutSymbol = otherLutEntry.symbol
    } catch (err) {
      //probably got here because the 'other' entry of the LUT was missing
      //or named in an unexpected way (i.e., the 'label' is not 'other')
      paratooErrorHandler(
        `Something went wrong auto-selecting 'other' for the LUT with custom input '${record.select.optionsKey}' with field name '${fieldName}' and custom text '${newRec[idx][fieldName]}'. Continuing without auto-selecting, which might result in dirty data.`,
        err,
      )
    }
    //no LUT to resolve and thus is probably custom text
    return {
      [record.input.fieldName]: newRec[idx][fieldName],
      [record.select.fieldName]: otherLutSymbol,
    }
  } else {
    return {
      [record.select.fieldName]: newRec[idx][fieldName],
    }
  }
}

/**
 * Assembles the emit payloads to update the PTV submodule
 *
 * @param {String} uniqueId that both floristics voucher and plant tissue voucher uses
 * @param {Number} floristicsProtocolId the protocol ID of the parent module (floristics)
 * @param {Number} plantVoucheringId the protocol ID of the sub-module (PTV)
 * @param {String} ptvVariant the protocol variant of the sub-module (PTV)
 * @param {Object} [removedVoucher] the removed floristics voucher (if applicable).
 * Defaults to null when no voucher was removed
 *
 * @returns {Array.<Object> | null} a list of objects for the caller of this function to
 * emit 'updateSubmodule' (as we can only emit in vue files) or `null` if nothing to
 * change
 */
export function savePtvSubmodule(
  uniqueId,
  floristicsProtocolId,
  plantVoucheringId,
  ptvVariant,
  removedVoucher = null,
) {
  const bulkStore = useBulkStore()

  const PTVdata = bulkStore.getTmpPTVData({ uniqueId: uniqueId })
  let existingGeneticVoucherData = null
  if (bulkStore.doingSubmodule(floristicsProtocolId)) {
    existingGeneticVoucherData = bulkStore.collectionGetForProt({
      protId: plantVoucheringId,
    })
  }
  const existingFloristicsData = bulkStore.collectionGetForProt({
    protId: floristicsProtocolId,
  })

  const objectsToEmit = []
  if (!PTVdata && removedVoucher !== null) {
    let existingData = cloneDeep(
      existingGeneticVoucherData['floristics-veg-genetic-voucher'],
    )
    existingData = existingData?.filter(
      (item) =>
        item.floristics_voucher?.['voucher_full'] !==
          removedVoucher.unique_id &&
        item.floristics_voucher?.['voucher_lite'] !== removedVoucher.unique_id,
    )

    objectsToEmit.push({
      data: existingData,
      submoduleProtocolId: plantVoucheringId,
      rowDataKey: 'floristics-veg-genetic-voucher',
      mainSurveyFlag: false,
      protocolVariant: ptvVariant,
    })
  } else if (!PTVdata) {
    return null
  } else if (isEmpty(existingGeneticVoucherData)) {
    // create survey if not exist - use `isEmpty` as it can be empty object or
    //something empty-like

    const existingSurveyDateTimes = {
      start_date_time: null,
      end_date_time: null,
    }

    for (const floristicsVariant of ['full', 'lite']) {
      const floristicsSurveyData = existingFloristicsData?.[`floristics-veg-survey-${floristicsVariant}`]
      if (floristicsSurveyData) {
        for (const dateTime of ['start_date_time', 'end_date_time']) {
          if (floristicsSurveyData?.[dateTime]) {
            existingSurveyDateTimes[dateTime] = floristicsSurveyData[dateTime]
          }
        }
      }
    }

    const current = new Date()

    //survey_metadata and protocol_variant handled by store
    const surveyData = {
      start_date_time: existingSurveyDateTimes?.start_date_time
        ? existingSurveyDateTimes.start_date_time
        : current,
      end_date_time: existingSurveyDateTimes?.end_date_time
      ? existingSurveyDateTimes.end_date_time
      : current,
    }

    //update layout and visit ref for the submodule
    const plotData = {}
    ;['plot-layout', 'plot-visit'].forEach((field) => {
      plotData[field] = bulkStore.collectionGetForProt({
        protId: floristicsProtocolId,
      })[field]
    })
    for (const [modelName, pData] of Object.entries(plotData)) {
      objectsToEmit.push({
        data: pData,
        submoduleProtocolId: plantVoucheringId,
        rowDataKey: modelName,
        mainSurveyFlag: false,
        protocolVariant: ptvVariant,
      })
    }
    //update submodule survey
    objectsToEmit.push({
      data: surveyData,
      submoduleProtocolId: plantVoucheringId,
      rowDataKey: 'floristics-veg-genetic-voucher-survey',
      mainSurveyFlag: true,
      protocolVariant: ptvVariant,
    })
    //update submodule obs
    objectsToEmit.push({
      data: PTVdata,
      submoduleProtocolId: plantVoucheringId,
      rowDataKey: 'floristics-veg-genetic-voucher',
      mainSurveyFlag: false,
      protocolVariant: ptvVariant,
    })
  } else {
    let existingData = cloneDeep(
      existingGeneticVoucherData['floristics-veg-genetic-voucher'],
    ) || []
    existingData = existingData?.filter(
      (item) =>
        item.floristics_voucher?.['voucher_full'] !== uniqueId &&
        item.floristics_voucher?.['voucher_lite'] !== uniqueId,
    )
    console.log('existingData:', existingData)

    objectsToEmit.push({
      data: existingData.concat(PTVdata),
      submoduleProtocolId: plantVoucheringId,
      rowDataKey: 'floristics-veg-genetic-voucher',
      mainSurveyFlag: false,
      protocolVariant: ptvVariant,
    })
  }

  bulkStore.clearTmpPTVData(uniqueId)
  return objectsToEmit
}

/**
 * Generates dropdown options for Grassy Weeds Aerial Survey's desktop setup for when
 * doing the field survey and want to select a desktop setup to auto-fill fields from
 *
 * Appends an extra 'none' value that the vue component will need to handle
 *
 * @param {String} surveyModelName the desktop setup's model name
 * @param {String} desktopSurveyToGet the survey type to get. Must be 'D' or 'F'
 * @param {Boolean} [appendFullData] whether to append the full data object (i.e., the
 * entire survey object from the store). Defaults to false
 *
 * @returns {Array.<Object>} the dropdown options with keys `label` and `value`
 */
export function getGrassyWeedsDesktopSurveys(
  surveyModelName,
  desktopSurveyToGet,
  appendFullData = false,
  options = null
) {
  const apiModelsStore = useApiModelsStore()
  //using apiModelsStore's `cachedModel` instead of dataManager's as we don't want
  //surveys in the queue (no offline case as it's not relevant)
  const allDesktopSurveys = options ? options : apiModelsStore.cachedModel({
    modelName: surveyModelName,
  })

  let surveyNameKey = 'survey_name'
  if (desktopSurveyToGet === 'F') {
    surveyNameKey = 'field_survey_name'
  }

  const returnSurveys = []
  for (const survey of allDesktopSurveys) {
    if (
      survey.survey_type.symbol === desktopSurveyToGet &&
      //don't want desktop setups that already have a field survey
      !allDesktopSurveys.some((ds) => ds?.desktop_setup?.id === survey.id)
    ) {
      let label = `${survey[surveyNameKey]}`

      if (desktopSurveyToGet === 'F') {
        label += ` by ${survey.field_survey_observer}`
      }
      if (desktopSurveyToGet === 'D') {
        label +=
          ' ' + survey.planned_observers?.map((o) => o.observer)?.join('-')
      }
      const originalTime = prettyFormatDateTime(survey.visit_start_date_time)
      const parts = originalTime.split(', ')
      const datePart = parts[0]
      const timePart = parts[1]?.split(' ')[0]
      const adjustedTime = `${datePart} ${timePart}`

      label += ' ' + adjustedTime
      const obj = {
        value: survey.id,
        label: label,
      }
      if (appendFullData) {
        obj.full_data = survey
      }
      returnSurveys.push(obj)
    }
  }

  if (desktopSurveyToGet === 'D') {
    returnSurveys.push({
      value: 'none',
      label: 'No desktop setup was completed',
    })
  }

  return returnSurveys
}

/**
 * Resolves the file metadata object from Strapi to the full URL location
 *
 * @param {Object} fileMetaObj the file metadata object, which has at least the `url` key
 *
 * @returns {String} the fully resolved url to the image
 */
export function resolveFileFromUrl(fileMetaObj) {
  //try/catch as there's not always a file
  try {
    if (process.env.DEV && fileMetaObj.url.includes('http')) {
      //accessing the minio bucket url over https results in SSL protocol error
      return fileMetaObj.url.replace('https', 'http')
    } else if (process.env.DEV && !fileMetaObj.url.includes('http')) {
      //it's possible we've fallen back to local provider (thus no full url encoded)
      return `${coreApiUrlBase}${fileMetaObj.url}`
    }
    return fileMetaObj.url
  } catch {
    return null
  }
}

/**
 * Retrieves all flight lines for Grassy Weeds Surveys (Desktop) that do not already have
 * an associated Field Survey, then stores in the binary dexieDB store
 */
export async function fetchGrassyWeedsFlightLines() {
  //all desktop surveys without an existing field survey
  const surveys = getGrassyWeedsDesktopSurveys('grassy-weeds-survey', 'D', true)

  for (const survey of surveys) {
    if (survey?.full_data?.flight_lines?.url) {
      const res = await fetch(
        resolveFileFromUrl(survey.full_data.flight_lines),
        {
          headers: {
            Accept: survey.full_data.flight_lines.mime,
          },
        }
      )
      const blobData = await res.blob()
      let originalFileName = null
      if (
        survey.full_data.flight_lines?.alternativeText &&
        survey.full_data.flight_lines.alternativeText.includes(
          'file_original_name=',
        )
      ) {
        //this is the file name that was stored on the device; once we upload the file we
        //name it with a UUID to ensure files are not uploaded more than once
        originalFileName = survey.full_data.flight_lines.alternativeText.split(
          'file_original_name=',
        )[1]

        Object.assign(survey.full_data.flight_lines, {
          originalFileName,
        })
      } else {
        //fall-back if we don't have it for some reason
        originalFileName = survey.full_data.flight_lines.name
      }

      const extension = mime.extension(survey.full_data.flight_lines.mime)
      let parsedApiData = null
      const msg = `Unknown file extension '${extension}' for survey '${survey.label}'`
      const supportedExtensions = ['kml', 'kmz']
      if (supportedExtensions.includes(extension)) {
        parsedApiData = [blobData]
      } else {
        paratooWarnMessage(msg)
        notifyHandler('negative', msg)
        continue
      }

      //TODO might want to clean these up, as the clean-up done on `bulk.uploadFile` isn't really relevant as these are pre-fetched API model data
      await binary.saveApiModelsBinary(
        //by giving the file name that was stored in the API (UUID), we can ensure that
        //if we auto-fill this file in a new record, this new record will use the same
        //reference to the file
        survey.full_data.flight_lines.name.replace(
          `${survey.full_data.flight_lines.ext}`,
          '',
        ),
        //convert the file bytes to a `File` that the file picker can accept
        //https://stackoverflow.com/a/70525054
        new File(
          parsedApiData,
          //keep the human-readable file name (not UUID), so that the file picker it
          originalFileName,
          {
            type: survey.full_data.flight_lines.mime,
          },
        ),
        originalFileName,
      )
    }
  }
}
// checks the url is valid or not
// see https://stackoverflow.com/questions/5717093/check-if-a-javascript-string-is-a-url
export function isValidUrl(value, queryObject) {
  let url = value
  const queryParams = Object.keys(queryObject).sort()
  
  try {
    // if url doesn't have domain e.g. `/api/plot-visit&use-cache=true`
    if (!url.includes('http')) url = `http://localhost:1337${value}`
    
    const urlSerachParams = new URL(url).searchParams
    const paramsObject = Object.fromEntries(urlSerachParams.entries())
    const params = Object.keys(paramsObject).sort()

    // params shold be same
    return isEqual(params, queryParams)
  } catch (_) {
    return false  
  }
}

/**
 * Resolves a given file metadata object from Strapi to a base64 encoded string wrapped
 * in an object that the file picker can handle (keys: src, fileName, fileType='file', id)
 *
 * @param {Object} fileMetaObj the file metadata object, which has at least the `url` key
 *
 * @returns {Object} an object that the file picker can handle (keys: src, fileName,
 * fileType='file', id)
 */
export async function resolveFileFromUrlAsBase64(fileMetaObj) {
  const authStore = useAuthStore()

  const blobData = await axios.get(resolveFileFromUrl(fileMetaObj), {
    headers: {
      Authorization: 'Bearer ' + authStore.token,
      Accept: 'application/vnd.google-earth.kmz',
    },
  })

  const Buffer = require('buffer/').Buffer
  //https://stackoverflow.com/a/60186623
  const base64Data = Buffer.from(blobData.data).toString('base64')

  let originalFileName = null

  if (
    fileMetaObj?.alternativeText &&
    fileMetaObj.alternativeText.includes('file_original_name=')
  ) {
    originalFileName = fileMetaObj.alternativeText.split(
      'file_original_name=',
    )[1]
  }

  return {
    id: fileMetaObj.id,
    src: 'data:application/vnd.google-earth.kmz;base64,' + base64Data,
    fileType: 'file',
    fileName: fileMetaObj.name,
    originalFileName: originalFileName,
  }
}

/**
 * Recursively searches the provided `object` for a `key` and returns the value if found
 *
 * Source: https://stackoverflow.com/a/40604638
 *
 * @param {Object} object the object to search over
 * @param {String} key the key to search for
 *
 * @returns {*} the value of the found `key`
 */
export function findValForKey(object, key) {
  let value
  Object.keys(object).some(function (k) {
    if (k === key) {
      value = object[k]
      return true
    }
    if (object[k] && typeof object[k] === 'object') {
      value = findValForKey(object[k], key)
      return value !== undefined
    }
  })
  return value
}

export function projAreaTermMap(enableOIDC) {
  if (enableOIDC) {
    return 'project extent area'
  }
  return 'project area'
}

/**
 * Generates submit label for the workflow
 */
export function submitLabel(workflow) {
  let title = createTitle(workflow.title)
  return `Complete ${title} Component`
}

export function makeCustomComponentConfig({
  stepIndex,
  protocolId,
  modelName,
  protocolWorkflow,
  modelConfig,
  collectMultiple,
}) {
  // TODO: remove this in some future release, this is for backward compatiblity
  // somehow the app doesn't fetch the protocol schema properly
  if (modelName === 'floristics-veg-voucher-lite') {
    protocolWorkflow.minimum = 0
  }
  return {
    name: stepIndex,
    title:
      protocolWorkflow.overrideDisplayName || prettyFormatFieldName(modelName),
    excludeSubmitBtnFlag: true,
    description: protocolWorkflow['x-paratoo-description'],
    hint: protocolWorkflow['x-paratoo-hint'],
    currentProtId: protocolId,
    protocolVariant: protocolWorkflow['protocol-variant'],
    required: protocolWorkflow['required'] === undefined ? true : false,
    crud: {
      fields: {
        component: {
          type: `${modelName}-component`,
          //NOTE: each component will need to handle this by passing this param
          //to `makeModelConfig`
          splitSurveyStep: protocolWorkflow.splitSurveyStep || false,
        },
      },
      title:
        protocolWorkflow.overrideDisplayName ||
        prettyFormatFieldName(modelName),
      rowDataKey: modelName,
      mainSurveyFlag: modelConfig.mainSurveyFlag,
      splitSurveyStep: protocolWorkflow.splitSurveyStep || false,
    },
    hidden: protocolWorkflow.defaultHidden || false,
    wrapperStepOnly: protocolWorkflow.wrapperStepOnly || false,
    multiple: collectMultiple,
    useManyEndpoint: protocolWorkflow.useManyEndpoint || false,
    // adds one additional survey id in non plot based protocol
    addSurveyID: protocolWorkflow.addSurveyID || false,
    splitSurveyStep: protocolWorkflow.splitSurveyStep || false,
    minimum: protocolWorkflow.minimum || 0,
    isCustomComponent: true,
  }
}

export function makePlotSelectionProjInfoWorkflow(customComponentConfig) {
  const plotSelectionProjInfoTitle = 'Plot Selection Project Information'
  const plotSelectionProjInfoConfig = structuredClone(customComponentConfig)
  plotSelectionProjInfoConfig.title = plotSelectionProjInfoTitle
  plotSelectionProjInfoConfig.crud.title = plotSelectionProjInfoTitle
  plotSelectionProjInfoConfig.name = 1
  plotSelectionProjInfoConfig.crud.fields.component.type =
    'plot-selection-survey-component'
  plotSelectionProjInfoConfig.crud.rowDataKey = 'plot-selection-survey'
  plotSelectionProjInfoConfig.isNonModelStep = true
  plotSelectionProjInfoConfig.crud.mainSurveyFlag = true

  return plotSelectionProjInfoConfig
}

/**
 * Disabled fields by modifying a reference to the model config
 *
 * @param {Array.<String> | null} fieldsToDisable the fields to disable, or null to
 * re-enable all fields
 * @param {Object} modelConfigFieldsRef a reference to the modelConfig's `field`
 * property, as the appropriate object depth
 */
export function disableFields(fieldsToDisable, modelConfigFieldsRef) {
  for (const [field, def] of Object.entries(modelConfigFieldsRef)) {
    if (fieldsToDisable === null) {
      def.disable = false
    } else if (fieldsToDisable.includes(field)) {
      def.disable = true
    }
  }
}

/**
 * Modifies the field def (passed as reference) for a site setup field to create a
 * filtered select of options from the site setup(s) done in the current collection
 *
 * @param {Number} protocolId the current protocol ID
 * @param {Object} fields a reference to the model config's `fields` property
 * @param {String} siteSetupModelName the model name of the site setup
 * @param {String} siteSetupOptionsKey the options key of the site setup. Can be the same
 * as the `siteSetupModelName`, but if the site setups are nested, then this key will
 * need to be a string that can be parsed by `Object.byString` in crud's `getOptions()`
 * @param {String} surveySetupFieldName the field name of the survey setup that is used
 * to store the site setup ID
 */
export function createSiteSetupFieldDef(
  protocolId,
  fields,
  siteSetupModelName,
  siteSetupOptionsKey,
  surveySetupFieldName,
) {
  const bulkStore = useBulkStore()
  const currCollection = bulkStore.collectionGetForProt({
    protId: protocolId,
  })
  fields[surveySetupFieldName].type = 'filteredSelect'
  fields[surveySetupFieldName].optionsKey = siteSetupOptionsKey
  fields[surveySetupFieldName].storeOverride = 'bulk'
  //NOTE: can't use callback function with a `return` as crud's getOptions has a custom
  //`Object.byString` method that parses the callback function wrong due to having a
  //composite optionsKey
  fields[surveySetupFieldName].optionsMapper = (t) => ({
    value: t.new_or_existing.site_id,
    label: t.new_or_existing.site_id,
    inactive: currCollection?.[siteSetupModelName]?.some(
      (setup) => setup[surveySetupFieldName] === t.new_or_existing.site_id,
    ),
  })
  fields[surveySetupFieldName].hint = `Select a ${prettyFormatFieldName(
    surveySetupFieldName,
  )} without a survey setup for this collection`
}

export function createSurveySetupIdCb(newVal, oldVal, transect_setup_ID) {
  let updatedModelValue = null
  if (
    !isEqual(newVal, oldVal) &&
    newVal[transect_setup_ID] &&
    !newVal.survey_setup_ID
  ) {
    const transectSetupId =
      newVal[transect_setup_ID]?.value || newVal[transect_setup_ID]
    updatedModelValue = structuredClone(newVal)
    const dateCode = date.formatDate(Date.now(), 'YYYYMMDDHHmmss')
    updatedModelValue.survey_setup_ID = `${transectSetupId}-${dateCode}`
  }
  return updatedModelValue
}

const conditionalTargetSpeciesMap = {
  WD: 'target_wild_dog',
  D: 'target_deer',
  O: 'other_target_species',
}

export function makeConditionFieldForTargetSpecies(
  fields,
  conditionalFields = [],
) {
  if (fields['target_species']) {
    for (const value of Object.values(conditionalTargetSpeciesMap)) {
      fields[value].hidden = true
    }
    conditionalFields.push({
      field: 'target_species',
      fieldCb: conditionalTargetSpeciesCb,
    })
  }
  return conditionalFields
}

export function conditionalTargetSpeciesCb({ newVal, oldVal, inputDefs }) {
  if (isEqual(newVal, oldVal)) return
  const targetSpecies = newVal?.target_species

  for (const [lut, field] of Object.entries(conditionalTargetSpeciesMap)) {
    const conditionalLut = targetSpecies?.find((species) => species.lut === lut)
    const relevantInputDefIndex = inputDefs.findIndex((i) => i.model === field)
    if (relevantInputDefIndex === -1) continue
    inputDefs[relevantInputDefIndex].hidden = !conditionalLut
    if(inputDefs[relevantInputDefIndex].hidden) {
      newVal[inputDefs[relevantInputDefIndex].model] = null
    }
  }
  return newVal
}

/**
 * Sets a given field's `required` attribute and updates rules/label accordingly
 * 
 * FIXME rules get updated (e.g., field goes when defocus with empty data) but when actually try save record it doesn't enforce
 * 
 * TODO use this helper in other places where we tend to change the `required` rule (e.g., when we want to make something conditionally-required on another field)
 * 
 * TODO reset rules on UI (e.g., if trigger `required` rule then make not required, the field will still be red)
 * 
 * @param {Boolean} isRequired whether the field is required or not
 * @param {Object} inputDef the inputDef of the field containing the rules array, etc.
 * Passed by reference so this function doesn't need to return it
 */
export function setRequiredRule(isRequired, inputDef) {
  const requiredRule = (value) =>
      (value !== undefined && value !== null && value !== '') ||
      `This field is required`

  if (inputDef?.rules) {
    if (isRequired) {
      inputDef.rules.push(requiredRule)
      inputDef.label = `${inputDef.originalLabel} *`
    } else {
      //TODO instead of completely squashing other rules, call `makeValidationRules`
      inputDef.rules = []
      inputDef.label = inputDef.originalLabel
    }
  }
}

/**
 * Checks if a given plot name is already reserved or not. If not reserved, will reserve
 * it. Also handles un-reserving plot names when we've changed a given plot
 * 
 * @param {Object} newRecord the local new record containing the `plot_label`
 * @param {Number} observationIndex the index of the 'observation' (plot)
 * @param {Number} protocolId the Plot Selection protocol ID
 * 
 * @returns {Boolean} false if the plot name is already reserved, else true
 */
export async function checkAndReservePlotName({
  newRecord,
  observationIndex,
  protocolId,
}) {
  const bulkStore = useBulkStore()
  const authStore = useAuthStore()

  //generate plot label so it's easy to check.
  //helper expects array of plots, but we only have 1
  const valueWithUUID = generateUUIDAndPlotLabel(cloneDeep([newRecord]))?.[0]

  if (!valueWithUUID?.projects_for_plot && enableOIDC) {
    //when we add a plot (not edit) and we're in MERIT, the `projects_for_plot` are not
    //defined as the field is hidden, so it's not on the local record. Thus, we need to
    //append this data so that when we get the `projects_for_plot` further down, it has
    //something to access
    valueWithUUID.projects_for_plot = [{
      project: cloneDeep(authStore.selectedProject),
    }]
  }

  const savedPlots = bulkStore.collectionGetForProt({
    protId: protocolId,
  })

  let plotMustBeUnreserved = false
  const savedPlotLabel = savedPlots?.['plot-selection']?.[observationIndex]?.plot_label
  if (
    savedPlotLabel &&
    savedPlotLabel === valueWithUUID.plot_label
  ) {
    //we're likely editing, so don't need to check if reserved as we already have and will
    //end up returning false-positive
    paratooSentryBreadcrumbMessage(
      `Plot ${valueWithUUID.plot_label} is being edited`,
      'checkAndReservePlotName',
    )
    return true
  } else if (
    savedPlotLabel &&
    savedPlotLabel !== valueWithUUID.plot_label
  ) {
    //we likely changed the plot, so need to un-reserve it
    paratooSentryBreadcrumbMessage(
      `Plot ${savedPlotLabel} must be un-reserved`,
      'checkAndReservePlotName',
    )
    plotMustBeUnreserved = true
  }

  const reservedPlots = await getReservedPlotNames()
  if (valueWithUUID.plot_label) {
    if (Array.isArray(reservedPlots)) {
      const plotLabelIsReserved = reservedPlots?.some(
        o => o.attributes.plot_label === valueWithUUID.plot_label
      )

      if (plotLabelIsReserved) {
        paratooSentryBreadcrumbMessage(
          `Plot ${valueWithUUID.plot_label} is already reserved`,
          'checkAndReservePlotName',
        )
        notifyHandler('negative', `Plot with name '${valueWithUUID.plot_label}' already exists`)
        return false
      } else {
        paratooSentryBreadcrumbMessage(
          `Reserving plot ${valueWithUUID.plot_label}`,
          'checkAndReservePlotName',
        )
        await reservePlotName({
          plotLabel: valueWithUUID.plot_label,
          projects: valueWithUUID.projects_for_plot.map(
            p => p.project.id,
          ),
        })
      }
    }
  }

  //do this last as if we try edit a plot to be one that is already reserved, it will
  //incorrectly unreserved the plot we tried to change from. Thus, if it is reserved,
  //this function will return false and won't get to this block
  if (plotMustBeUnreserved) {
    await unreservePlotName({
      plotLabel: savedPlotLabel,
      projects: valueWithUUID.projects_for_plot.map(
        p => p.project.id,
      ),
      reservedPlots: reservedPlots,
    })
  }

  //we haven't returned `false` so valid
  return true
}

/**
 * Retrieves the list of all reserved plots
 * 
 * @returns {Array.<Object>} list of reserved plots
 */
export async function getReservedPlotNames() {
  const reservedPlotLabelsResp = await doCoreApiGet({
    urlSuffix: '/api/reserved-plot-labels',
  })
    .catch((err) => {
      let msg = 'Failed to get reserved plot labels. Continuing without check'
      paratooErrorHandler(msg, err)
      notifyHandler('negative', msg)
    })
  return reservedPlotLabelsResp?.data || null
}

/**
 * Reserves a given plot name
 * 
 * @param {String} plotLabel the plot name to reserve
 * @param {Array.<String | Number>} array of project IDs or UUIDs
 */
export async function reservePlotName({ plotLabel, projects }) {
  const authStore = useAuthStore()
  const resp = await doCoreApiPost({
    urlSuffix: '/api/reserved-plot-labels',
    body: {
      data: {
        plot_label: plotLabel,
        reservation_provenance: {
          project_id: authStore?.getCurrentContext?.project?.toString(),
          org_opaque_user_id: authStore?.userId?.toString(),
          version_app: `${build.paratoo_webapp_version}-${build.git_hash}`,
          version_core_documentation: currentDocVersion(
            await useDocumentationStore().getDocumentation()
          ),
        },
        //provide projects for authorisation check by PME
        projects,
      },
    },
    handleUnauthorised: false,
  })
    .catch((err) => {
      //TODO if unauthorised, should say so
      let msg = `Failed to reserve plot with name '${plotLabel}'. Continuing without check`
      paratooErrorHandler(msg, err)
      notifyHandler('negative', msg)
    })
}
// remove fields from array of objects
export function filterValuesFromArrayOfObjects(data, fields=[]) {
  if (!data) return data
  const result = []
  for (const d of data) {
    const values = {}
    for(const[key, value] of Object.entries(d)) {
      if (fields.includes(key)) continue
      values[key] = cloneDeep(value)
    }
    if (isEmpty(values)) continue
    result.push(values)
  }
  return cloneDeep(result)
}

/**
 * filter object by conditions
 * @param {Object} data object to filter
 * @param {Bool} nonEmpty flag to remove empty element
 * @param {String} filterKeyPrefix remove element if key contains the prefix
 * @returns {Object} filterd data
 */
export function filterObjectByCondition(data, nonEmpty, filterKeyPrefix=null) {
  if (!data) return data
  const result = {}
  for(const[key, value] of Object.entries(data)) {
    if (nonEmpty) {
      if (isEmpty(value)) continue
    }
    if (filterKeyPrefix) {
      if (key.includes(filterKeyPrefix)) continue
    }
    result[key] = cloneDeep(value)
  }
  return cloneDeep(result)
}

/**
 * start timer to send analytics
 */
export function startTimerToSendAppAnalytics() {
  paratooSentryBreadcrumbMessage(
    `Starting analytics timer that will fire every ${sendAppAnalyticsInterval} seconds`,
  )

  const ephemeralStore = useEphemeralStore()

  if (!ephemeralStore.isExistingInterval({ intervalName: 'analytics' })) {
    let intervalId = setInterval(() => {
      sendAnalyticsToCore()
    }, sendAppAnalyticsInterval * 1000)
  
    ephemeralStore.trackInterval({
      intervalName: 'analytics',
      intervalId,
    })
  } else {
    paratooSentryBreadcrumbMessage(
      `Skipping adding analytics interval as one already exists`,
    )
  }

  
}

/**
 * send current app analytics to core if user is logged in and network connection detected
 */
export async function sendAnalyticsToCore() {
  const authStore = useAuthStore()
  const ephemeral = useEphemeralStore()

  // skip if not logged in
  if (!authStore.token) {
    paratooSentryBreadcrumbMessage(
      'Skip sending analytics to core, reason: user is not logged in',
    )
    return
  }
  // skip if offline
  if (!ephemeral.networkOnline) {
    paratooSentryBreadcrumbMessage(
      'Skip sending analytics to core, reason: no network activity',
    )
    return
  }

  const apiModelsStore = useApiModelsStore()
  const bulkStore = useBulkStore()
  const dataManager = useDataManagerStore()
  const documentationStore = useDocumentationStore()

  const documentation = await documentationStore.getDocumentation()
  const currentTime = new Date(Date.now())

  // Build/version info and provenance
  const appVersion = `${build.paratoo_webapp_version}-${build.git_hash}`
  const hostInfo = authStore.getHostInfo
  const coreDocVersion = currentDocVersion(documentation)
  const storageEstimate = await showEstimatedQuota()

  const parsedIndexDbStorageEstimate = parseEstimatedQuota(storageEstimate)
  const localStorageEstimate = getLocalStorageUsage()
  const storageObj = {}
  const freeSpaceObj = {}
  if (!isEmpty(parsedIndexDbStorageEstimate)) {
    Object.assign(storageObj, parsedIndexDbStorageEstimate)
    Object.assign(freeSpaceObj, {
      overall: parsedIndexDbStorageEstimate.totalQuota - parsedIndexDbStorageEstimate.totalUsed,
    })
  }
  if (!isEmpty(localStorageEstimate)) {
    Object.assign(storageObj, {
      localStorage: localStorageEstimate,
    })
    Object.assign(freeSpaceObj, {
      localStorage: 5 - localStorageEstimate.total
    })
  }
  if (!isEmpty(freeSpaceObj)) {
    Object.assign(storageObj, {
      free_space: freeSpaceObj,
    })
  }

  const _webApp = {
    version_app: appVersion,
    version_core_documentation: coreDocVersion,
    host_info: hostInfo,
  }
  if (!isEmpty(storageObj)) {
    Object.assign(_webApp, {
      //will look something like:
      /**
        {
          "swUsed": "5.300",
          "totalUsed": "450.460",
          "cachesUsed": "160.525",
          "free_space": {
            "overall": 575111.819,
            "localStorage": 3.9616661071777344
          },
          "totalQuota": "575562.279",
          "indexDbUsed": "284.636",
          "localStorage": {
            "total": 1.0383338928222656,
            "stores": {
              "auth": 0.0589599609375,
              "apiModels": 0.9765167236328125,
              "dataManager": 0.0025653839111328125
            }
          }
        }
      */
      storage_mb: storageObj,
    })
  }

  // auth store information
  const _authStore = {
    org_opaque_user_id: authStore.userProfile?.id.toString(),
    authContext: authStore.currentContext,
    loginTime: authStore.loginTime,
    lastRefreshTime: authStore.lastTokenRefreshTime,
  }
  
  // api model store info
  const plot_selecitons = apiModelsStore.cachedModel({
    modelName: 'plot-selection',
  })
  const plot_layouts = apiModelsStore.cachedModel({
    modelName: 'plot-layout',
  })
  const plot_visits = apiModelsStore.cachedModel({
    modelName: 'plot-visit',
  })
  const _apiModelStore = {
    plot_selecitons: plot_selecitons.map((e)=> e.plot_label),
    plot_layouts: plot_layouts.map((e)=> e.plot_selection.plot_label),
    plot_visits: filterValuesFromArrayOfObjects(plot_visits, ['plot_layout']),
    modelsIdMap: filterObjectByCondition(cloneDeep(apiModelsStore.modelsIdMap), true, 'lut')
  }

  // bulk store information e.g. collections that are in progress
  const _bulkStore = {
    plot_context: bulkStore.staticPlotContext,
    collections: allSurveyMetdataFromCollections(cloneDeep(bulkStore.collections))
  }
  
  // data manager information e.g. collections in publicationQueue
  const _dataManager = {
    publicationQueue: allSurveyMetadataFromList(dataManager.publicationQueue),
    submitResponses: allSurveyMetadataFromList(dataManager.submitResponses),
    errors: allSurveyMetadataFromList(dataManager.errors),
    skips: allSurveyMetadataFromList(dataManager.skips),
  }

  const { subscription } = await getWebPushSubscription()
  const status = await doCoreApiPost({
    urlSuffix: coreApiPrefix + '/analytics/heartbeat',
    body: {
      info: {
        currentTime: currentTime,
        webApp: _webApp,
        authStore: _authStore,
        apiModelStore: _apiModelStore,
        bulkStore: _bulkStore,
        dataManager: _dataManager,
        ...(subscription && { webPushEndpoint: subscription.endpoint })
      },
    },
  }).catch((reason) => {
    const msg = 'Failed to send app analytics'
    paratooWarnHandler(msg, reason)
    return null
  })

  // send the endpoint to core again if it's not there for some reason
  if (subscription && status && !status?.webPushSubscriptionStatus) {
    paratooWarnHandler(
      `Push endpoint does not exist in Core, this shouldn't happen, send it to Core`,
    )
    sendPushSubscriptionToCore(subscription)
  }

  return status
}

/**
 * generate a list of survey metadata from publication queue
 * 
 * @param {Array} publicationQueue datamanager publication queue
 * @returns {Array} list of survey metadata
 */
export function allSurveyMetadataFromList(data) {
  const skips = ['workflowItem', 'collection' , 'protocolInfo']
  let result = []
  for (const value of Object.values(data)) {
    const d = cloneDeep(value)
    for (const s of skips) {
      if (!d[s]) continue
      delete d[s]
    }
    
    if (value.collection) {
      d.collection = collectionToMetadata(cloneDeep(value.collection))
    }
    result.push(cloneDeep(d))
  }
  return result
}

/**
 * generate a list of survey metadata from collections
 * 
 * @param {Array} collections bulk store collections
 * @returns {Array} list of survey metadata
 */
export function allSurveyMetdataFromCollections(collections) {
  let metadata = []
  for (const collection of Object.values(collections)) {
    metadata.push(collectionToMetadata(cloneDeep(collection)))
  }
  return metadata
}

/**
 * generate metadata with plot data from a collection
 * 
 * @param {object} collection bulk store collections
 * @returns {object} filtered metadata
 */
export function collectionToMetadata(collection) {
  const plotModels = ['plot-layout', 'plot-visit']
  const skips = ['orgMintedIdentifier']
  let object = {}
  let models = []
  for(const[model, value] of Object.entries(collection)) {
    if (skips.includes(model)) continue
    models.push(model)
    if (plotModels.includes(model)) {
      object = {
        ...object,
        [model]: value
      }
    }
    if (!value.survey_metadata) continue
    object = {
      ...object,
      ...value.survey_metadata
    }
  }
  object.models = models
  return cloneDeep(object)
}

/**
 * Un-reserves a given plot name
 * 
 * @param {String} plotLabel the plot name ro un-reserve
 * @param {Array.<String | Number>} array of project IDs or UUIDs
 * @param {Array.<Object>} [reservedPlots] list of reserved plots
 */
export async function unreservePlotName({ plotLabel, projects, reservedPlots = null }) {
  paratooSentryBreadcrumbMessage(
    `Unreserving plot with name '${plotLabel}' and projects ${projects?.join(', ')}`,
    'unreservePlotName',
  )

  let reservedPlots_ = null
  if (reservedPlots !== null) {
    reservedPlots_ = reservedPlots
  } else {
    paratooSentryBreadcrumbMessage(
      'List of already-reserved plots not provided, so fetching them',
      'unreservePlotName',
    )
    reservedPlots_ = await getReservedPlotNames()
  }

  //need to get a handle on the plot that we want to un-reserve
  const reservedPlotLabel = reservedPlots_.find(
    o => o?.attributes?.plot_label === plotLabel
  )
  paratooSentryBreadcrumbMessage(
    `Reserved plot label entry: ${JSON.stringify(reservedPlotLabel, null, 2)}`,
    'unreservePlotName',
  )
  if (!reservedPlotLabel) {
    let msg = `Unable to unreserve plot with name '${plotLabel}'. No reservation record in backend`
    //this is a warning - we don't have anything to unreserve, so won't break anything,
    //but still need to log something in case it's a knock-on effect of another error
    paratooWarnMessage(msg)
    notifyHandler('warning', msg)
    return
  }

  //provide projects for authorisation check my PME
  let url = `/api/reserved-plot-labels/${reservedPlotLabel?.id}?project_ids=${projects.join(',')}`

  await doCoreApiDelete({
    urlSuffix: url,
    handleUnauthorised: false,
  })
    .catch((err) => {
      //TODO if unauthorised, should say so
      let msg = `Failed to un-reserve plot with name '${plotLabel}'.`
      paratooErrorHandler(msg, err)
      notifyHandler('negative', msg)
    })
}
export async function showEstimatedQuota() {
  if (navigator.storage && navigator.storage.estimate) {
    const estimation = await navigator.storage.estimate()
    return estimation
  } else {
    paratooWarnMessage(`Browser does not support navigator.storage.estimate - cannot check storage usage`)
  }
}

export async function checkStorageAvailability (blobSize) {
  const estimation = await showEstimatedQuota()
  if(estimation) {
    const availableQuota = estimation.quota - estimation.usage
    return availableQuota > blobSize
  } else {
    return false
  }
}

export function parseEstimatedQuota(indexDbStorageEstimate) {
  console.log('indexDbStorageEstimate:', JSON.stringify(indexDbStorageEstimate, null, 2))
  if (!indexDbStorageEstimate) return null
  try {
    const totalUsed = (indexDbStorageEstimate?.usage / 1024 / 1024).toFixed(3)
    const totalQuota = (indexDbStorageEstimate?.quota / 1024 / 1024).toFixed(3)
    const indexDbUsed = (indexDbStorageEstimate?.usageDetails?.indexedDB  / 1024 / 1024).toFixed(3)
    const cachesUsed = (indexDbStorageEstimate?.usageDetails?.caches  / 1024 / 1024).toFixed(3)
    const swUsed = (indexDbStorageEstimate?.usageDetails?.serviceWorkerRegistrations / 1024 / 1024).toFixed(3)
    return {
      totalUsed,
      totalQuota,
      indexDbUsed,
      cachesUsed,
      swUsed,
    }
  } catch (err) {
    paratooErrorHandler(
      `Unable to parse estimated quota`,
      err,
    )
    return null
  }
}

export async function logEstimatedQuota() {
  const indexDbStorageEstimate = await showEstimatedQuota()
  const localStorageEstimate = getLocalStorageUsage()
  if (indexDbStorageEstimate && localStorageEstimate) {
    const parsedObj = parseEstimatedQuota(indexDbStorageEstimate)
    Object.assign(parsedObj, {
      localStorage: localStorageEstimate.total.toFixed(3),
    })
    if (parsedObj) {
      paratooSentryBreadcrumbMessage(
        `Browser has used ${parsedObj.totalUsed}MB of ${parsedObj.totalQuota}MB (Local Storage has used ${parsedObj.localStorage}MB, IndexDB has used ${parsedObj.indexDbUsed}MB, Caches have used ${parsedObj.cachesUsed}MB, Service Workers have used ${parsedObj.swUsed}MB)`,
        'dataManagerClearHistorical',
      )
      if (parsedObj.totalQuota - parsedObj.totalUsed < 1000) {
        paratooWarnMessage(
          `Browser is nearly out of space! Less that 1GB (${(parsedObj.totalQuota - parsedObj.totalUsed).toFixed(3)}MB) of space remains`
        )
      }
    } else {
      //we've got the estimate (so it's supported), but failed to parse
      paratooSentryBreadcrumbMessage(
        `Unable to parse/format storage usage - raw estimate: ${JSON.stringify(indexDbStorageEstimate)}`,
        'dataManagerClearHistorical',
      )
    }
  } else {
    paratooSentryBreadcrumbMessage(
      `Unable to parse/format storage usage no estimate provided`,
      'dataManagerClearHistorical',
    )
  }
}

export async function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}

export async function checkIsOnline() {
  try {
    await fetch('https://www.google.com', {
      method: 'HEAD',
      mode: 'no-cors',
    })
    return true
  } catch {
    return false
  }
}

/**
 * Sends the current state of the stores to the server by sending each store separately.
 *
 * Sends the first store via a POST request, then sends each of the rest via PUT.
 *
 * Sending all stores at once might result in a payload that is too large.
*/
export async function postStoreState(origin = dumpOrigins.USER, storeData = {}, silent = false) {
  if (!origin) {
    paratooWarnMessage(
      `Programmer error: postStoreState provided with bad origin: ${origin}`,
    )
  } else {
    paratooSentryBreadcrumbMessage(
      `Posting store state with origin '${origin}'`,
      'postStoreState',
    )
  }
  const storesToDump = ['bulk', 'dataManager', 'auth', 'apiModels']
  if(storeData?.auth?.token && storeData?.auth?.refreshToken) {
    paratooSentryBreadcrumbMessage(
      'overwriting auth token',
      'postStoreState'
    )
    // update auth store using storeData.auth
    const authStore = useAuthStore()
    authStore.token = storeData.auth.token
    authStore.refreshToken = storeData.auth.refreshToken
  }
      
  const allStates = getStoresStateFromPinia()
  let initialPOSTId = null
  const clientStateUuid = useBulkStore().clientStateUuid || uuidv4()
  const errs = []
  for (const [storeName, state] of Object.entries(allStates)) {
    let dataToUse = storeData[storeName] ?? state
    paratooSentryBreadcrumbMessage(
      `Sending state for ${storeName} to server (buildVersion=${paratooWebAppBuildVersion}, gitHash=${buildGitHash})`,
    )
    try {
      if (storeName === storesToDump[0]) {
        const resp = await doCoreApiPost({
          urlSuffix: coreApiPrefix + '/debug-client-state-dumps',
          body: {
            data: {
              [`client_state_${snakeCase(storeName)}`]: JSON.parse(JSON.stringify(dataToUse, getCircularReplacer())),
              client_state_uuid: clientStateUuid,
              origin
            },
          },
        })
        if (!resp) {
          //will be caught by catch and handled
          throw new Error(`No response from server for store ${storeName}`)
        } else {
          initialPOSTId = resp.data.id
        }
      } else {
        if (!initialPOSTId) {
          throw new Error(
            `Cannot append for store ${storeName} as the first store didn't succeed`,
          )
        }
        if (storeName === 'dataManager') {
          //TODO what happens when this is too large to send?
          for (const historicalDataType of ['responses', 'submitResponses']) {
            const keyToUse = `historical_${historicalDataType}`
            let data = null
            try {
              data = await historicalDataManagerDb.getAllTableData({
                table: historicalDataType,
              })
            } catch (err) {
              errs.push({
                error: err,
                storeName: keyToUse,
              })
            }
        
            if (data?.length > 0) {
              Object.assign(dataToUse, {
                [keyToUse]: cloneDeep(data),
              })
            } else {
              Object.assign(dataToUse, {
                [keyToUse]: 'none',
              })
            }
          }
        }
        const resp = await doCoreApiPut({
          urlSuffix: `${coreApiPrefix}/debug-client-state-dumps/${initialPOSTId}`,
          body: {
            data: {
              [`client_state_${snakeCase(storeName)}`]: JSON.parse(JSON.stringify(dataToUse, getCircularReplacer())),
            },
          },
        })
        if (!resp) {
          //will be caught by catch and handled
          throw new Error(`No response from server for store ${storeName}`)
        }
      }
    } catch (err) {
      errs.push({
        error: err,
        storeName: storeName,
      })
    }
  }

  if (errs.length > 0) {
    const reducedMessage = errs.reduce((accum, curr) => {
      const currMsg = `Failed to upload state for store ${curr.storeName}`
      accum.push(currMsg)
      //want a sentry exception for each
      const errorToPass = curr.error instanceof Error ? curr.error : new Error(curr.error)
      paratooErrorHandler(currMsg, errorToPass)
      return accum
    }, [])

    //notify user only once for all failures
    if(!silent) {
      notifyHandler('negative', reducedMessage.join(', '))
    }
  } else {
    if(!silent) {
      notifyHandler('positive', 'State uploaded successfully')
    }
    if(origin === dumpOrigins.USER) {
      useBulkStore().debugClientStateDumpUrl = `${coreApiPrefix}/debug-client-state-dumps/${initialPOSTId}`
    }
    if(!useBulkStore().clientStateUuid) {
      useBulkStore().clientStateUuid = clientStateUuid
    }
    // keep last 10 of the sent files, maybe useful for debugging
    useBulkStore().sentStateUids.push(initialPOSTId)
    if(useBulkStore().sentStateUids.length > 10) {
      useBulkStore().sentStateUids.shift()
    }
    return initialPOSTId
  }
}

function getStoresStateFromPinia() {
  const apiModelsStore = useApiModelsStore()
  const authStore = useAuthStore()
  const bulkStore = useBulkStore()
  const ephemeralStore = useEphemeralStore()
  const dataManagerStore = useDataManagerStore()
  const allStates = {
    bulk: cloneDeep(bulkStore?.$state) || 'error',
    dataManager: cloneDeep(dataManagerStore?.$state) || 'error',
    auth: cloneDeep(authStore?.$state) || 'error',
    ephemeral: cloneDeep(ephemeralStore?.$state) || 'error',
  }
  const apiModels = cloneDeep(apiModelsStore?.$state) || 'error'
  if(apiModels !== 'error') {
    allStates.apiModels = {
      models: {
        protocol: apiModels.models?.protocol,
      },
      modelsIdMap: apiModels.modelsIdMap
    }
  } else {
    allStates.apiModels = 'error'
  }
 
  return allStates
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cyclic_object_value#circular_references
export function getCircularReplacer() {
  const ancestors = [];
  //eslint-disable-next-line no-unused-vars
  return function (key, value) {
    if (typeof value !== "object" || value === null) {
      return value;
    }
    // `this` is the object that value is contained in,
    // i.e., its direct parent.
    while (ancestors.length > 0 && ancestors.at(-1) !== this) {
      ancestors.pop();
    }
    if (ancestors.includes(value)) {
      return "[Circular]";
    }
    ancestors.push(value);
    return value;
  };
}

/**
 * checks if token is expired or not.
 * NOTE: Only checks expiry, does'nt check valid JWT or not.
 */
export function isTokenExpired(token) {
  if (!token) return false
  try {
    const decoded = JSON.parse(window.atob(token.split('.')[1]))
    if (!decoded?.exp) {
      return false
    }

    const currentTime = Date.now()
    const diff = (decoded.exp * 1000) - currentTime
    if (diff > 0) {
      const minsUntilExpire = diff/(1000*60)
      if (minsUntilExpire < 10) {
        //don't want to spam trace, so log only if we're nearing expiry
        paratooSentryBreadcrumbMessage(`Token is going to expire in: ${minsUntilExpire}minutes`)
      }
    }
    return currentTime >= decoded.exp * 1000
  } catch (error) {
    return false
  }
}

/**
 * checks if both token and refresh token are expired or not.
 */
export function isSessionDestroyed() {
  const authStore = useAuthStore()
  if (!authStore.token) return
  
  const ephemeral = useEphemeralStore()

  // checks both tokens are valid or not 
  ephemeral.tokenHasExpired = isTokenExpired(authStore.token)
  ephemeral.refreshTokenHasExpired = isTokenExpired(authStore.refreshToken)

  if (ephemeral.tokenHasExpired && ephemeral.refreshTokenHasExpired) {
    paratooWarnMessage('Both token and refresh token are expired')
    return true
  }
  return false
}
/**
 * @param initialPOSTId the id of the state dump record in Strapi
 */
export function emailDumpFileUrl(initialPOSTId) {
  const email = 'tech.contact@environmentalmonitoringgroup.com.au'
  const subject = 'Store dump'
  const restUrl = `${coreApiUrlBase}${coreApiPrefix}/debug-client-state-dumps/${initialPOSTId}`
  // save to the localStorage to pull the fixed later
  const adminDashboard = `${coreApiUrlBase}/admin/content-manager/collection-types/api::debug-client-state-dump.debug-client-state-dump/${initialPOSTId}`
  const body = `
      Debug ID: ${initialPOSTId}. %0D%0A
      REST_URL: ${restUrl} %0D%0A
      ADMIN_DASHBOARD_URL(need to login first): ${adminDashboard} %0D%0A
      Version: ${paratooWebAppBuildVersion} %0D%0A
      Build: ${buildGitHash}
      `
  window.open(`mailto:${email}?subject=${subject}&body=${body}`, '_blank')
}
/**
 * 
 * @param {Blob} blob 
 * @param {String} extension
 * @returns {String} filename as string
 */
export function downloadFile(blob, extension, filename) {
  // Create a URL for the Blob
  const url = URL.createObjectURL(blob)

  // Create a link element
  const link = document.createElement('a')
  link.href = url
  const date = new Date()
  if(!filename) {
    filename = `${date.getTime()}.${extension}`
  }
  link.download = filename

  // Programmatically trigger the download
  link.click()

  // Revoke the object URL to free up memory
  URL.revokeObjectURL(url)
  return filename
}
/**
 * promisifed quasar create dialog, so it can be used inline
 * @param {*} options 
 * @returns 
 */
export async function promiseDialog(options) {
  return new Promise(resolve => {
    Dialog.create(options).onOk(function () {
      resolve(true)
    }).onCancel(function () {
      resolve(false)
    })
  })
}

/**
 * https://stackoverflow.com/a/15720835
 * get localStorage usage in mb
 */
export function getLocalStorageUsage() {
  const validStores = [
    'bulk',
    'dataManager',
  ]

  let _lsTotal = 0
  let storeBreakdown = {}
  for (const _x in localStorage) {
    if (!Object.keys(localStorage).includes(_x)) {
      continue
    }

    const _xLen = (localStorage[_x].length + _x.length) * 2

    if (validStores.includes(_x)) {
      storeBreakdown[_x] = _xLen / 1024**2
    }

    _lsTotal += _xLen
  }
  return {
    total: _lsTotal / 1024**2,
    stores: storeBreakdown,
  }
}

export function checkLocalStorageAvailability() {
  const currLocalStorageUsage = getLocalStorageUsage()
  const warningUsageThreshold = 5 * 0.85 // MB

  let mappedStoreUsage = null
  try {
    mappedStoreUsage = Object.entries(currLocalStorageUsage.stores).map(
      (([key, storeUsageMb]) => `${key}=${storeUsageMb} MB`)
    )
  } catch (err) {
    paratooWarnHandler(
      `Unable to format breakdown of local storage usage by store. Raw data: ${JSON.stringify(currLocalStorageUsage.stores)}`,
      err,
    )
  }
  
  if (currLocalStorageUsage.total >= warningUsageThreshold) {
    paratooWarnMessage(
      `Local storage usage reached above ${currLocalStorageUsage.total} MB (${mappedStoreUsage}), if we receive this warining, we might need to push a fix for clearning up the apiModelStore or consider moving to to indexedDb. Things will silently break if localStorage usage exceed 5 MB.`,
    )
  }
}
/**
 * Checks if a protocol is Plot Layout and Visit, and whether it contains no new data.
 * 
 * 'New data' means it has one of:
 *  - new core plot
 *  - new fauna plot
 *  - new visit
 * 
 * @param {Number} protocolId the protocol to check
 * 
 * @returns {Boolean} whether the survey is Layout and Visit protocol, and if it has no
 * new data
 */
export function isExistingLayoutAndVisit({
  protocolId,
}) {
  const apiModelsStore = useApiModelsStore()
  const bulkStore = useBulkStore()

  const protocolUUID = apiModelsStore.protUuidFromId(protocolId)
  const isLayoutProtocol = protocolUUID?.uuid === layoutAndVisitUuid
  if (!isLayoutProtocol) return false

  const currCollection = bulkStore.collectionGetForProt({
    protId: protocolId,
  })

  const hasNewCorePlot = currCollection?.['plot-layout']?.plot_points
  const hasNewFaunaPlot = currCollection?.['plot-layout']?.fauna_plot_point
  const hasNewVisit = currCollection?.['plot-visit']?.visit_field_name || currCollection?.['plot-visit']?.start_date

  return isLayoutProtocol && !(hasNewCorePlot || hasNewFaunaPlot || hasNewVisit)
}

export function tryZipsonParse({ store }) {
  let zipParsedSuccess
  if (store in localStorage) {
    try {
      parse(localStorage.getItem(store))
      zipParsedSuccess = true
    } catch (err) {
      paratooSentryBreadcrumbMessage(`Could not zipson parse ${store}`)
      zipParsedSuccess = false
    }
    
  } else {
    zipParsedSuccess = false
    paratooWarnMessage(`Unable to zipson parse ${store} as it's not in localStorage`)
  }

  return zipParsedSuccess
}

export function tryJsonParse({ store }) {
  let jsonParseSuccess
  if (store in localStorage) {
    try {
      JSON.parse(localStorage.getItem(store))
      jsonParseSuccess = true
    } catch (err) {
      paratooSentryBreadcrumbMessage(`Could not json parse ${store}`)
      jsonParseSuccess = false
    }
  } else {
    jsonParseSuccess = false
    paratooWarnMessage(`Unable to json parse ${store} as it's not in localStorage`)
  }
  return jsonParseSuccess
}

//flattens data from the context so we've either got an integer ID or a string
//(temp_offline_id) UUID, or similar variations
export function flattenPlotDataId(rawContext, tempIdOnly = false) {
  if (!tempIdOnly && Number.isInteger(rawContext)) {
    return rawContext
  }

  if (!tempIdOnly && Number.isInteger(rawContext?.id)) {
    return rawContext.id
  }

  if (isUuid(rawContext)) {
    return rawContext
  }

  if (
    rawContext?.temp_offline_id &&
    isUuid(rawContext.temp_offline_id)
  ) {
    return rawContext.temp_offline_id
  }

  if (
    rawContext?.id?.temp_offline_id &&
    isUuid(rawContext.id.temp_offline_id)
  ) {
    return rawContext.id.temp_offline_id
  }
}

/**
 * Checks if the current plot context is referencing a plot/visit we just synced,
 * specifically, if the user did 'select existing' for a plot/visit that was still in the
 * queue, we do not track/map this context change in the queue (see #1800), so we need to
 * check if the context needs to be re-mapped to the data that came back from Core
 * 
 * @param {Object} submitResponses the submit responses state object
 * @param {Object} historicalSubmitResponses submit responses from the historical data
 * manager (Dexie)
 * @param {Boolean} [warnWhenUpdate] whether we should generate a warning during an
 * update of data - useful when we're triggering a migration
 */
export function checkAndUpdatePlotContext({
  submitResponses,
  historicalSubmitResponses,
  warnWhenUpdate = false,
}) {
  const bulkStore = useBulkStore()
  const dataManager = useDataManagerStore()
  const apiModelsStore = useApiModelsStore()
  let plotLayoutContext = cloneDeep(bulkStore.getPlotLayoutId)
  const flattenedLayoutId = flattenPlotDataId(plotLayoutContext)

  let plotVisitContext = cloneDeep(bulkStore.getPlotVisitId)
  const flattenedVisitId = flattenPlotDataId(plotVisitContext)

  paratooSentryBreadcrumbMessage(
    `plotLayoutContext=${JSON.stringify(plotLayoutContext)}. flattenedLayoutId=${JSON.stringify(flattenedLayoutId)}. plotVisitContext=${JSON.stringify(plotVisitContext)}. flattenedVisitId=${JSON.stringify(flattenedVisitId)}`,
  )

  if (typeof plotLayoutContext !== 'object') {
    if (Number.isInteger(plotLayoutContext)) {
      plotLayoutContext = { id: plotLayoutContext }
    } else if (isUuid(plotLayoutContext)) {
      plotLayoutContext = { temp_offline_id: plotLayoutContext }
    }
  }
  if (typeof plotVisitContext !== 'object') {
    if (Number.isInteger(plotVisitContext)) {
      plotVisitContext = { id: plotVisitContext }
    } else if (isUuid(plotVisitContext)) {
      plotVisitContext = { temp_offline_id: plotVisitContext }
    }
  }

  const allResponseData = []
    .concat(cloneDeep(submitResponses))
    .concat(cloneDeep(historicalSubmitResponses))

  const plotContext = {
    layout: {
      ...plotLayoutContext,
      flattened_id: flattenedLayoutId,
    },
    visit: {
      ...plotVisitContext,
      flattened_id: flattenedVisitId,
    }
  }
  paratooSentryBreadcrumbMessage(
    `plotContext: ${JSON.stringify(plotContext)}`,
  )
  const plotTypes = ['layout', 'visit']

  //update static plot context
  const plotContextUpdates = []
  paratooSentryBreadcrumbMessage(
    'Checking if static plot context needs update',
  )
  for (const plotType of plotTypes) {
    if (
      !plotContext?.[plotType]?.id?.temp_offline_id &&
      !plotContext?.[plotType]?.id &&
      plotContext?.[plotType]?.flattened_id &&
      isUuid(plotContext?.[plotType]?.flattened_id) &&
      !dataManager.plotDataIsQueued({
        plotType,
        tempOfflineIdToSearchFor: plotContext?.[plotType]?.flattened_id,
      })
    ) {
      paratooSentryBreadcrumbMessage(
        `Might need to update ${plotType} context: ${JSON.stringify(plotContext?.[plotType])}`,
      )
      const submitResponseForContext = allResponseData.find(
        s => s.collection?.[`plot-${plotType}`]?.temp_offline_id === plotContext[plotType].flattened_id
      )
      paratooSentryBreadcrumbMessage(
        `submitResponseForContext: ${JSON.stringify(submitResponseForContext)}`,
      )

      if (submitResponseForContext?.collection?.[`plot-${plotType}`]?.id) {
        const contextDataForUpdate = submitResponseForContext.collection[`plot-${plotType}`]
        paratooSentryBreadcrumbMessage(
          `Updating ${plotType} context to: ${JSON.stringify(contextDataForUpdate)}`,
        )
        plotContextUpdates.push(`plotType=${plotType}. contextDataForUpdate=${JSON.stringify(contextDataForUpdate)}`)
        switch(plotType) {
          case 'layout':
            bulkStore.setStaticPlotLayout(contextDataForUpdate)
            break
          case 'visit':
            bulkStore.setStaticPlotVisit(contextDataForUpdate)
            break
          default:
            paratooWarnMessage(`Programmer error: plot type '${plotType}' not a registered case`)
            break
        }
      } else {
        paratooWarnMessage(
          `${plotType} data needs to be updated but couldn't find the relevant submit response data`,
        )
      }
    } else {
      paratooSentryBreadcrumbMessage(
        `Don't need to update ${plotType} context data`,
      )
    }
  }

  if (warnWhenUpdate && plotContextUpdates.length > 0) {
    paratooWarnMessage(
      `Updated static plot context: ${plotContextUpdates}`,
    )
  }

  //update in-progress (bulk store) plot-based protocols
  const protocolUpdates = []
  paratooSentryBreadcrumbMessage(
    'Checking if in-progress plot-based protocol(s) need updates',
  )
  for (const [protocolId, collection] of Object.entries(cloneDeep(bulkStore.collectionGet) || [[],[]])) {
    for (const plotType of plotTypes) {
      if (collection?.[`plot-${plotType}`]) {
        const flattenedCollectionId = flattenPlotDataId(
          collection?.[`plot-${plotType}`],
        )
        paratooSentryBreadcrumbMessage(
          `flattenedCollectionId for plotType=${plotType}: ${JSON.stringify(flattenedCollectionId)}`,
        )
        if (
          flattenedCollectionId &&
          !Number.isInteger(collection?.[`plot-${plotType}`]?.id) &&
          //can't resolve from queued data
          !dataManager.plotDataIsQueued({
            plotType,
            tempOfflineIdToSearchFor: flattenedCollectionId,
          })
        ) {
          const submitResponseForContext = allResponseData
            .find(
              (s) => {
                const currSubmitResponseFlattenedId = flattenPlotDataId(
                  s.collection?.[`plot-${plotType}`],
                  //force only searching for temp_offline_id as we expect that
                  //`flattenedCollectionId` is also a temp_offline_id
                  true,
                )
                return flattenedCollectionId === currSubmitResponseFlattenedId
              }
            )
          paratooSentryBreadcrumbMessage(
            `submitResponseForContext: ${JSON.stringify(submitResponseForContext)}`,
          )
  
          if (submitResponseForContext?.collection?.[`plot-${plotType}`]?.id) {
            const contextDataForUpdate = submitResponseForContext.collection[`plot-${plotType}`]
            paratooSentryBreadcrumbMessage(
              `Updating collection for protocol ID=${protocolId} ${plotType} context to: ${JSON.stringify(contextDataForUpdate)}`,
            )
            protocolUpdates.push(`protocolId=${protocolId}. plotType=${plotType}. context: ${JSON.stringify(contextDataForUpdate)}`)
            switch(plotType) {
              case 'layout':
                bulkStore.updateCollectionPlotContext({
                  protocolId,
                  updatedContext: {
                    'plot-layout': contextDataForUpdate,
                  },
                })
                break
              case 'visit':
                bulkStore.updateCollectionPlotContext({
                  protocolId,
                  updatedContext: {
                    'plot-visit': contextDataForUpdate,
                  },
                })
                break
              default:
                paratooWarnMessage(`Programmer error: plot type '${plotType}' not a registered case`)
                break
            }
          } else {
            paratooWarnMessage(
              `${plotType} data needs to be updated for protocol ID=${protocolId} but couldn't find the relevant submit response data`,
            )
          }
        } else {
          paratooSentryBreadcrumbMessage(
            `Don't need to update ${plotType} context for collection with protocol ID=${protocolId}`,
          )
        }
      } else {
        //non-plot-based edge case(s) - register on a case-by-case basis
        const resolvedProtocol = apiModelsStore.protUuidFromId(
          Number.parseInt(protocolId)
        )
        switch(resolvedProtocol.uuid) {
          case (cameraTrapDeploymentUuid): {
            if (
              plotType === 'layout' &&
              collection?.['camera-trap-deployment-point']?.length
            ) {
              //only care about resolving the layout
              paratooSentryBreadcrumbMessage(`Camera Trap Deployment might have fauna plot to resolve`)

              for (const [index, ob] of collection['camera-trap-deployment-point'].entries()) {
                if (!isEmpty(ob?.fauna_plot)) {
                  const flattenedCollectionId = flattenPlotDataId(
                    ob?.fauna_plot,
                  )
                  paratooSentryBreadcrumbMessage(
                    `flattenedCollectionId for plotType=${plotType}: ${JSON.stringify(flattenedCollectionId)}`,
                  )
                  if (
                    flattenedCollectionId &&
                    !Number.isInteger(ob?.fauna_plot) &&
                    //can't resolve from queued data
                    !dataManager.plotDataIsQueued({
                      plotType,
                      tempOfflineIdToSearchFor: flattenedCollectionId,
                    })
                  ) {
                    paratooSentryBreadcrumbMessage(`Camera Trap Deployment ob at index=${index} has temp ID to layout: ${ob.fauna_plot.temp_offline_id}`)
                    const submitResponseForContext = allResponseData
                      .find(
                        (s) => {
                          const currSubmitResponseFlattenedId = flattenPlotDataId(
                            s.collection?.[`plot-${plotType}`],
                            //force only searching for temp_offline_id as we expect that
                            //`flattenedCollectionId` is also a temp_offline_id
                            true,
                          )
                          return flattenedCollectionId === currSubmitResponseFlattenedId
                        }
                      )
                    paratooSentryBreadcrumbMessage(
                      `submitResponseForContext: ${JSON.stringify(submitResponseForContext)}`,
                    )
                    if (submitResponseForContext?.collection?.[`plot-${plotType}`]?.id) {
                      const contextDataForUpdate = submitResponseForContext.collection[`plot-${plotType}`]
                      paratooSentryBreadcrumbMessage(
                        `Updating collection for protocol ID=${protocolId} ${plotType} relation: ${JSON.stringify(contextDataForUpdate)}`,
                      )
                      protocolUpdates.push(`protocolId=${protocolId}. plotType=${plotType}. relation: ${JSON.stringify(contextDataForUpdate)}`)
  
                      bulkStore.updateCollectionPlotReference({
                        modelName: 'camera-trap-deployment-point',
                        protocolId,
                        index,
                        fieldNameForRelation: 'fauna_plot',
                        resolvedLayout: contextDataForUpdate,
                      })
                    }
                  }
                } else {
                  paratooSentryBreadcrumbMessage(
                    `No plot reference for index=${index} to resolve`,
                  )
                }
              }
            }
            break
          }
          //don't need a default block as we register cases as-needed
        }
      }
    }
  }

  if (warnWhenUpdate && protocolUpdates.length > 0) {
    paratooWarnMessage(
      `Updated in-progress collection(s) context: ${protocolUpdates}`,
    )
  }
}


export async function getWebPushSubscription() {
  if (!('Notification' in window) || Notification.permission === 'denied')
    return { subscription: null }

  const serviceWorkerRegistration =
    await navigator.serviceWorker.getRegistration()

  const subscription =
    await serviceWorkerRegistration.pushManager.getSubscription()
  return {
    subscription
  }
}

export async function sendPushSubscriptionToCore(subscription) {
  const { userId, username, email } = useAuthStore()
  const data = {
    endpoint: subscription.endpoint,
    subscription,
    subscribe_by_user_id: userId?.toString(),
    ...(username && { subscribe_by_username: username }),
    ...(email && { subscribe_by_email: email }),
  }
  await doCoreApiPost({
    urlSuffix: `${coreApiPrefix}/push-notification-subscriptions`,
    body: { data },
  })
}

export function getPermissionsEnableInstructions(
  permissionType,
  customMessage = null,
) {
  if (!permissionTypes.includes(permissionType)) {
    paratooWarnMessage(
      `Programmer error: '${permissionType}' is not a valid permission type`,
    )
  }

  const isPWA = window.matchMedia('(display-mode: standalone)').matches
  const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent)
  const isAndroidPWA = isPWA && /android/i.test(navigator.userAgent)
  const isIOSPWA = isPWA && /iphone|ipad|ipod/i.test(navigator.userAgent)
  const isDesktopPWA = isPWA && !isAndroidPWA && !isIOSPWA

  let message = `${customMessage || ''}To enable ${permissionType}, please follow these steps:\n\n`

  if (isDesktopPWA) {
    message +=
      `Desktop App Mode:\n1. Open your system Settings.\n2. Go to the App settings or App Info in the top-right corner.\n3. Find the ${permissionType} settings in permission section for this app.\n4. Enable ${permissionType} for this app.\n\n`
  } else if (isAndroidPWA) {
    message += `Android App Mode:\n1. Open your phone settings.\n2. Select the app or browser.\n3. Go to the app ${permissionType} settings.\n4. Enable ${permissionType} for this app.\n\n`
  } else if (isIOSPWA) {
    message += `iOS App Mode:\n1. Open the iPhone/iPad Settings.\n2. Go to ${permissionType}.\n3. Find the app in the list (note that PWAs may have limited ${permissionType} options).\n4. Enable ${permissionType} for this app.\n\n`
  } else if (isSafari) {
    message += `Safari:\n1. Open Safari Preferences.\n2. Go to the Websites tab.\n3. Find ${permissionType} in the sidebar.\n4. Allow ${permissionType} for this site.\n\n`
  } else {
    message += `Browser Mode:\n`
    message += `Chrome:\n1. Click on the lock icon next to the URL in the address bar.\n2. Go to the Permissions section.\n3. Enable ${permissionType} for this site.\n\n`
  }

  return message
}

export function getPermissionReasonMessage(permissionType) {
  let msg = null
  switch (permissionType) {
    case 'geolocation': {
      msg = 'Most EMSA modules require collecting location-based data, which is often based on your current location. It\'s possible to \'choose on map\' or \'enter specific coordinates\', but it can be cumbersome'
      break
    }
    case 'camera':
    case 'microphone': {
      msg = 'Some EMSA modules require media to be uploaded. It\'s possible to use the app\'s file picker, but can be cumbersome'
      break
    }
    case 'notifications': {
      msg = 'We can inform you about important updates, scheduled maintenance, etc.'
      break
    }
    default: {
      msg = 'Something went wrong'
      paratooWarnMessage(
        `Programmer error: unable to determine permission reason message for type '${permissionType}' due to unregistered case`,
      )
      break
    }
  }
  return msg
}

/**
 * Checks that static assets' cookies flags so we can prevent duplicates of the same
 * requests, or re-send requests that we interupted while processing
 */
export function checkStaticAssets() {
  const ephemeralStore = useEphemeralStore()
  const apiModelsStore = useApiModelsStore()
  const resourceNames = useAuthStore().staticResourceNames
  for (
    const [assetType, assetLocations] of Object.entries(resourceNames) || [[], []]
  ) {
    if (!isEmpty(assetLocations)) {
      for (const location of assetLocations || []) {
        const resourceStatus = getCookie(location.Key)
        const isPending = ephemeralStore.isPendingTrackedFetchRequest({
          url: apiModelsStore.createAssetUrl({
            assetKey: location.Key,
          }),
        })
        if (
          resourceStatus !== 'done' &&
          !isPending
        ) {
          paratooSentryBreadcrumbMessage(`Asset location ${location.Key} was either processed last time, the flag isn't set, or there was a previous error, but we just mounted so resetting the flag (previous value: ${resourceStatus})`)
          saveCookie(location.Key, '')
        } else if (isPending) {
          paratooSentryBreadcrumbMessage(`Asset location ${location.Key} has pending request`)
        }
      }
    } else {
      //can be valid to get here, as we might still be initialising, but eventually we'll
      //get to the correct case and process everything
      paratooSentryBreadcrumbMessage(`Skipping checking static assets for type ${assetType} as they're not defined yet`)
    }
    
  }
}

export function allApiListModels() {
  let apiListModels = []
  Object.keys(modelsToPopulate).forEach((key) => {
    apiListModels = apiListModels.concat(modelsToPopulate[key])
  })

  return apiListModels
}

export function createLocationPermissionRejectionMessage({ routerInstance }) {
  const ephemeralStore = useEphemeralStore()
  const actions = []
  let msg = locationRequiredMsg
  if (!routerInstance.currentRoute?._value?.fullPath?.includes('preferences')) {
    //only want this action if we're not already on the preferences page
    actions.push({
      label: 'Set permissions',
      color: 'white',
      handler: () => {
        routerInstance.replace('/preferences')
      },
    })
    msg += '. You must grant permission on the Preferences page'
  }
  if (!ephemeralStore.locationRequiredNotificationState) {
    ephemeralStore.locationRequiredNotificationState = true
    //TODO auto-dismiss this when the user sets location permission - quasar docs seem to indicate it's possible, but can't get it working: https://quasar.dev/quasar-plugins/notify#programmatically-closing
    notifyWithActions(
      'negative',
      'location_off',
      0,    //no timeout
      false,
      msg,
      actions,
      () => {
        //when they dismiss the notification
        ephemeralStore.locationRequiredNotificationState = false
      },
    )
  } else {
    paratooSentryBreadcrumbMessage(
      `Location required notification already shown`,
    )
    ephemeralStore.locationRequiredNotificationState = false
  }
}

export async function getPermissionStatus({ permissionType }) {
  let permissionStatus = null
    
  try {
    const result = await navigator.permissions.query({ name: permissionType })
    permissionStatus = result?.state === 'granted'
  } catch (err) {
    permissionStatus = 'error'
    paratooWarnHandler(
      `Unable to determine permission status for ${permissionType}. It's likely not supported`,
      err,
    )
  }
  
  return permissionStatus
}

/**
 * Sorts a project list by name and each project's protocols by ID (or name as fallback)
 * 
 * Based on `Projects.vue`'s `sortedProjProt`, but not using that directly as it has
 * other logic we don't need here (creating v-model and filtering hidden protocols)
 * 
 * @param {Array.<Object>} projectList list of projects from org
 * 
 * @returns {Array.<Object>} sorted list of projects
 */
export function sortProjAndProt({ projectList }) {
  let sortedProjects = cloneDeep(projectList).sort((a, b) => {
    //https://stackoverflow.com/a/34779158
    return a.name.localeCompare(b.name)
  })

  //sort protocols within the projects
  const sortedProjProt = (() => {
    let sortedProt = []
    for (let proj of sortedProjects) {
      proj.protocols = proj.protocols
        .sort((a, b) => {
          return a.id - b.id || a.name.localeCompare(b.name) //sort by id
        })
      sortedProt.push(proj)
    }

    return sortedProt
  })()
  return sortedProjProt
}

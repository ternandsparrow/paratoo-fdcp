export default {
  herbivory_observation: [
    //TODO we shouldn't need to use conditional fields for these sorts of LUTs (`createDependentFieldCallback()` should be sufficient if we set the relations up correctly). Don't want to mess with this now though, as other pest modules also use these LUTs
    //attributable fauna species
    {
      field: 'percent_of_quadrat_affected',
      dependsOn: 'herbivory_or_damage_encountered',
      dependsOnSelection: ['D'],
    },
    {
      field: 'herbivory_attributable_fauna_species',
      dependsOn: 'herbivory_or_damage_encountered',
      dependsOnSelection: ['H'],
    },
    {
      field: 'physical_damage_attributable_fauna_species',
      dependsOn: 'herbivory_or_damage_encountered',
      dependsOnSelection: ['D'],
    },
    {
      field: 'type_of_physical_damage',
      dependsOn: 'herbivory_or_damage_encountered',
      dependsOnSelection: ['D'],
    },
    {
      field: 'grazing_severity',
      dependsOn: 'seedling_or_sapling',
    },
    // TODO: confirm these `dependsOnSelection`s are correct
    {
      field: 'seedling_or_sapling',
      dependsOn: 'growth_form',
      dependsOnSelection: [
        'Z',
        'Q',
        'T',
        'D',
        'A',
        'O',
        'NC',
        'F',
        'B',
        'C',
        'K',
        'E',
        'S',
        'Y',
        'M',
        'L',
        'U',
        'UK',
        'N',
        'P',
      ],
    },
    {
      field: 'type_of_damage',
      dependsOn: 'herbivory_or_damage_encountered',
      dependsOnSelection: ['H'],
    },
    {
      field: 'affected_plant_species',
      dependsOn: 'herbivory_or_damage_encountered',
      dependsOnSelection: ['H'],
    },
    {
      field: 'growth_form',
      dependsOn: 'herbivory_or_damage_encountered',
      dependsOnSelection: ['H'],
    },
    //growth form
    //TODO confirm these `dependsOnSelection`s are correct
    {
      field: 'damage_lowest_point',
      dependsOn: 'growth_form',
      //shrubs and trees
      dependsOnSelection: ['X', 'Z', 'T', 'D', 'C', 'S', 'Y', 'M', 'U'],
    },
    {
      field: 'damage_highest_point',
      dependsOn: 'growth_form',
      //shrubs and trees
      dependsOnSelection: ['X', 'Z', 'T', 'D', 'C', 'S', 'Y', 'M', 'U'],
    },
    {
      field: 'extent_of_damage',
      dependsOn: 'growth_form',
      //shrubs and trees
      dependsOnSelection: ['X', 'Z', 'T', 'D', 'C', 'S', 'Y', 'M', 'U'],
    },
    {
      field: 'additional_physical_damage',
      dependsOn: 'growth_form',
      //shrubs and trees
      dependsOnSelection: ['X', 'Z', 'T', 'D', 'C', 'S', 'Y', 'M', 'U'],
    },
    {
      field: 'grazing_severity',
      dependsOn: 'growth_form',
      //seedlings, saplings, sedges, rushes, and grasses
      dependsOnSelection: ['V', 'X', 'R', 'H', 'G', 'O', 'W', 'J'],
    },
  ],

  sign_based_sign: [
    {
      field: 'active',
      dependsOn: 'sign_type',
      dependsOnSelection: ['DEN', 'WAR', 'BUR'],
    },
    {
      field: 'number_of_entrance',
      dependsOn: 'sign_type',
      dependsOnSelection: ['DEN', 'WAR', 'BUR'],
    },
    {
      field: 'sign_observed',
      dependsOn: 'signs_present',
    },
  ],
}

import { modelsToPopulate } from './apiLists'

function envBoolCheck(envFlag) {
  if (!envFlag) return false

  const value = envFlag.toString()
  if (value == 'true') return true
  return false
}

export const adminRoleType = process.env.ADMIN_ROLE_TYPE
export const appTitle = process.env.VUE_APP_TITLE

export const coreApiUrlBase = process.env.CORE_API_URL_BASE
export const coreApiPrefix = process.env.CORE_API_PREFIX
  ? process.env.CORE_API_PREFIX
  : '/api'
export const orgApiUrlBase = process.env.ORG_API_URL_BASE
export const orgApiPrefix = process.env.ORG_API_PREFIX
  ? process.env.ORG_API_PREFIX
  : '/api'
export const orgCustomApiPrefix = process.env.ORG_CUSTOM_API_PREFIX
  ? process.env.ORG_CUSTOM_API_PREFIX
  : '/api/org'

export const enableTokenValidation = envBoolCheck(
  process.env.ORG_ENABLE_TOKEN_VALIDATION,
)
export const enableOIDC = envBoolCheck(process.env.VUE_APP_ENABLE_OIDC)
export const OIDCClient = process.env.VUE_APP_OIDC_CLIENT

export const deployment = process.env.VUE_APP_DEPLOYED_ENV_NAME

export const webAppCookiePrefix = 'Paratoo-WebApp-'
export const persistedStateLocalStorageKey = 'paratoo-pinia'
export const requiresLoggedIn = 'requiresLoggedIn'
export const requiresAdmin = 'requiresAdmin'

//NOTE: do not use helpers that rely on reading flags such as `isProduction` (e.g.,
//paratooSentryBreadcrumbMessage) as it results in error relating to accessing variables
//before begin defined - likely related to hydration lifecyle
import {
  paratooWarnHandler,
  paratooWarnMessage,
} from '../misc/helpers'
// import params from '@/misc/build_version_info';
// export const buildGitHash = params.git_hash;
// export const paratooWebAppBuildVersion = params.paratoo_webapp_version;
let buildGitHash_, paratooWebAppBuildVersion_
try {
  var params = require('@/misc/build_version_info')
  buildGitHash_ = params.git_hash
  paratooWebAppBuildVersion_ = params.paratoo_webapp_version
} catch (err) {
  paratooWarnHandler(
    'Unable to read the baked-in WebApp version info - using defaults',
    err,
  )

  buildGitHash_ = 'DEV BUILD'
  paratooWebAppBuildVersion_ = 'DEV BUILD'
}
export const buildGitHash = buildGitHash_
export const paratooWebAppBuildVersion = paratooWebAppBuildVersion_

//TODO make sure passed to runner correctly
let modeOfOperation_ = null
let parsed = null
try {
  parsed = Number.parseInt(process.env.VUE_APP_MODE_OF_OPERATION)
} catch (err) {
  parsed = 1
  paratooWarnHandler(
    "Tried to parse the VUE_APP_MODE_OF_OPERATION env but was not provided number, defaulting to '1' (prod mode)",
    err,
  )
}
if ([0, 1].includes(parsed)) {
  modeOfOperation_ = parsed
} else {
  modeOfOperation_ = 1
  paratooWarnMessage(
    `Value provided to VUE_APP_MODE_OF_OPERATION is invalid. Provided '${process.env.VUE_APP_MODE_OF_OPERATION}', but expected '0' or '1'. Defaulting to '1' (prod mode)`,
  )
}
export const modeOfOperation = modeOfOperation_

//disable WebAuthn as backend logic has not been updated for Strapi V4 (and the
//implementation is not very secure at the moment)
export const webAuthnIsAllowed = false

export const spoofPlotLayout = envBoolCheck(
  process.env.VUE_APP_SPOOF_PLOT_LAYOUT,
)

// simple http server on nectar to download all the resources
export const monitorResourcesUrl = process.env.VUE_APP_RESOURCE_URL

export const spoofBarcode = envBoolCheck(process.env.VUE_APP_DEV_BARCODE)

export const reverseGeocodingUrl = `${process.env.VUE_APP_GEOCODING_API_URL_BASE}/geocode/reverse?apiKey=${process.env.VUE_APP_GEOCODING_MAP_KEY}`

export const allAustraliaProjectArea = [
  {
    lat: -21.12549763660628,
    lng: 113.15917968750001,
  },
  {
    lat: -10.40137755454354,
    lng: 129.50683593750003,
  },
  {
    lat: -10.919617760254685,
    lng: 137.94433593750003,
  },
  {
    lat: -10.055402736564236,
    lng: 142.50366210937503,
  },
  {
    lat: -13.47510594433495,
    lng: 145.36010742187503,
  },
  {
    lat: -20.262197124246534,
    lng: 150.19409179687503,
  },
  {
    lat: -25.34402602913432,
    lng: 153.99536132812503,
  },
  {
    lat: -31.222197032103185,
    lng: 153.99536132812503,
  },
  {
    lat: -35.371135022801006,
    lng: 151.622314453125,
  },
  {
    lat: -38.151837403006766,
    lng: 150.10620117187503,
  },
  {
    lat: -44.5748174046703,
    lng: 148.01879882812503,
  },
  {
    lat: -44.60611274517391,
    lng: 145.53588867187503,
  },
  {
    lat: -39.11301365149974,
    lng: 142.02026367187503,
  },
  {
    lat: -34.198173096277245,
    lng: 133.16528320312503,
  },
  {
    lat: -32.73184089686568,
    lng: 130.68237304687503,
  },
  {
    lat: -33.797408767572485,
    lng: 125.211181640625,
  },
  {
    lat: -35.22767235493584,
    lng: 123.03588867187501,
  },
  {
    lat: -35.532226227703376,
    lng: 114.92797851562501,
  },
  {
    lat: -25.839449402063185,
    lng: 112.357177734375,
  },
]

export const offlineRefreshableModels = {
  //vouchers used in various protocols (cover, PTV, recruitment, etc.)
  plotsForProjectQueryParams: modelsToPopulate.vouchers,
  plotBased: modelsToPopulate.plotModels.concat(modelsToPopulate.plotBased),
  nonPlotBased: modelsToPopulate.nonPlotBased,
}

//LUT values that might match 'other'.
//don't want to do partial string matching as we might get
//false-positives, so make this list instead to be more precise
export const potentialOtherValues = ['other', 'other (specify)']

// local resources type
export const resourceTypes = {
  SPECIES_LIST: 'SPECIES_LIST',
  MAP_TILES: 'MAP_TILES',
  GCMD_EARTH_SCIENCE_KEYWORDS: 'GCMD_EARTH_SCIENCE_KEYWORDS',
  ANZSRC_FIELDS_OF_RESEARCH: 'ANZSRC_FIELDS_OF_RESEARCH',
  PUBLIC_ASSETS: 'PUBLIC_ASSETS',
  DOCUMENTATION: 'DOCUMENTATION',
  LOCAL_STORAGE: 'LOCAL_STORAGE',
  SESSION_STORAGE: 'SESSION_STORAGE',
  METHOD_PROGRESS_FLAGS: 'METHOD_PROGRESS_FLAGS',
  ALL: 'ALL',
}

// local resources type
export const awsAssetType = {
  SPECIES_LIST: 'species-lists',
  MAP_TILES: 'map-tiles',
  ANZSRC_FIELDS_OF_RESEARCH: 'anzsrc-fields-of-research',
  GCMD_EARTH_SCIENCE_KEYWORDS: 'gcmd-earth-science-keywords',
}

// as vascularFlora has multiple chunks such as vascularFlora1 and vascularFlora2
export const vascularFloraTotalChunks = 19

// NOTE: the species lists have been reduced to only include relevant data, for full
//      lists, visit the respective links
//      FIXME lists obtained from Biodiversity's AFD contain author names embedded in the
//      species names

// vascular flora list from: https://biodiversity.org.au/nsl/services/export/index
// mosses list from: https://moss.biodiversity.org.au/nsl/services/export/index
// amphibia list from: https://biodiversity.org.au/afd/taxa/AMPHIBIA/names/csv
// lichens list from: https://lichen.biodiversity.org.au/nsl/services/export/index
// mammalia list from: https://biodiversity.org.au/afd/taxa/MAMMALIA
// birds list from: https://birdlife.org.au/documents/BWL-BirdLife_Australia_Working_List_v3.xlsx
// reptiles list from: https://biodiversity.org.au/afd/taxa/REPTILIA

export const dexieTableNames = [
  'vascularFlora',
  'mosses',
  'amphibia',
  'lichens',
  'mammalia',
  'birds',
  'reptiles',
  'GCMDScienceKeywords',
  'ANZSRCFieldsOfResearch',
]

//TODO want to be env, but not critical right now
export const devToolsPassword = 'perentie'

// custom service worker actions
export const customServiceWorkerAction = {
  REFRESH_ASSETS: 'REFRESH_ASSETS',
  LOAD_ASSETS: 'LOAD_ASSETS',
  CLEAR_ASSETS: 'CLEAR_ASSETS',
}

// types of storage
export const storageTypes = {
  LOCALSTORAGE: 'LOCALSTORAGE',
  COOKIE: 'COOKIE',
}

export const allModelsToRefresh = [].concat(
  modelsToPopulate.plotModels,
  modelsToPopulate.vouchers,
  modelsToPopulate.plotBased,
  modelsToPopulate.nonPlotBased,
  modelsToPopulate.modelsWithFieldNames
)

export const floristicsLiteUuid = 'bbd550c0-04c5-4a8c-ae39-cc748e920fd4'
export const floristicsFullUuid = 'e15db26f-55de-4459-841b-d7ef87dea5cd'
export const layoutAndVisitUuid = 'd7179862-1be3-49fc-8ec9-2e219c6f3854'
export const cameraTrapDeploymentUuid = 'ad088dbe-02b2-472a-901f-bd081e590bcf'
export const plotSelectionUuid = 'a9cb9e38-690f-41c9-8151-06108caf539d'

export const networkRecheckInterval = process.env.NETWORK_RECHECK_INTERVAL || 15 //seconds
export const sendAppAnalyticsInterval = process.env.SEND_ANALYTICS_INTERVAL || 1200 // 20 minutes
export const heavyMethodWaitTime = process.env.HEAVY_METHOD_WAIT_TIME || 10 // 10 minutes
export const documentationVersionCheck = process.env.DOC_VERSION_CHECK_DIFF || 60 // 60 minutes
// thanks chatGPT
export const australiaOutline = [
  { lat: -5.0, lng: 130.0 },  
  { lat: -5.0, lng: 155.0 },  
  { lat: -25.0, lng: 160.0 },  
  { lat: -45.0, lng: 150.0 },  
  { lat: -45.0, lng: 110.0 }, 
  { lat: -25.0, lng: 105.0 },  
  { lat: -5.0, lng: 105.0 },   
  { lat: -5.0, lng: 130.0 }
]
export const rcNumber=process.env.VUE_APP_RC

export const deploymentToWeeklyWipedData = process.env.VUE_APP_DEPLOYMENT_WEEKLY_WIPED || 'staging'

export const dumpOrigins = {
  //these values must be part of the `debug-client-state-dump` attribute `origin` enum
  USER: 'user',
  AUTOHEAL: 'autoheal',
  PERSIST_FAILURE: 'persist_failure',
  HYDRATE_FAILURE: 'hydrate_failure',
  MIGRATE_FAILURE: 'migrate_failure',
  SYNC_FAILURE: 'sync_failure',
}

export const serverUnavailableMsg = `${appTitle}'s server is currently unavailable. It might be undergoing maintenance.`

export const locationRequiredMsg = `${appTitle} needs to know your location so the App can record accurate locations as part of its Core data collection function. It won't be able to work properly, otherwise.`

export const permissionTypes = ['geolocation', 'camera', 'microphone', 'notifications']
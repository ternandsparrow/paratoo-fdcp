// custom rules to validate both frontend and backend
module.exports = {
  customRules: {
    'basal-wedge-observation': [
      {
        ruleType: 'ConditionalFieldsCheck',
        conditions: [
          {
            requiredFields: ['basal_area_factor', 'in_tree', 'borderline_tree'],
            dependOn: {
              field: 'no_species_to_record',
              value: false,
            },
          },
        ],
      },
      {
        ruleType: 'multipleValuesMinimumCheck',
        rule: (value) =>
          value >= 7 ||
          'In and Borderline counts together should be greater than or equal to 7',
        description:
          'In and Borderline counts together should be greater than or equal to 7',
        dependentKeys: ['in_tree', 'borderline_tree'],
      },
    ],
    'basal-area-dbh-measure-observation': [
      {
        ruleType: 'OneDecimalPlaceCheck',
        rule: (value, filed) =>
          !value ||
          Array.isArray(value.toString().match('^(\\d+)?([.]?\\d{0,2})?$')) ||
          `${filed} does not allow more than one decimal point`,
        description: 'POM does not allow more than one decimal point',
        dependentKeys: [
          'POM_single',
          'POM_multi',
          'reach_POM',
          'POM_50_CM_above_buttress',
        ],
      },
    ],
    'floristics-veg-genetic-voucher': [
      {
        ruleType: 'minNumOfReplicates',
        rule: (value) =>
          value.hasReplicate
            ? value.replicateBarcodeLength > 4 ||
              'Field: "Replicate": Minimum 5 per voucher'
            : value.replicateBarcodeLength > 0 ||
              'Field: "Replicate": Barcode required',
      },
      {
        ruleType: 'distanceBetweenReplicates',
        rule: (value) =>
          value === null || value === '' //v-model.number turn empty string when type something then clear
            ? 'Field: "Min Distance Between Replicates": This field is required'
            : value >= 0 ||
              'Field: "Min Distance Between Replicates": Minimum 0',
      },
    ],
    'soil-lite-sample': [
      {
        ruleType: 'maximumDifferenceCheck',
        rule: (value) =>
          value <= 0.3 ||
          '"upper depth" and "lower depth" fields should not have greater than 30cm difference between their values',
        description:
          '"upper depth" and "lower depth" fields should not have greater than 30cm difference between their values',
        dependentKeys: ['upper_depth', 'lower_depth'],
      },
    ],
    'soil-pit-characterisation-full': [
      {
        ruleType: 'ConditionalFieldsCheck',
        stepName: 'Soil Pit Location', //optional
        conditions: [
          {
            requiredFields: [
              'observers',
              'start_date_time',
              'soil_pit_id',
              'collection_method',
              'location',
              'location_description',
            ],
            dependOn: {
              field: 'survey_variant',
              value: 'F',
            },
          },
          {
            requiredFields: ['field_survey_id'],
            dependOn: {
              field: 'survey_variant',
              value: 'L',
            },
          },
        ],
      },
    ],
    'soil-bulk-density-survey': [
      {
        ruleType: 'ConditionalFieldsCheck',
        conditions: [
          {
            requiredFields: [
              'observers',
              'start_date_time',
              'collection_method',
              'location',
            ],
            dependOn: {
              field: 'survey_variant',
              value: 'F',
            },
          },
          {
            requiredFields: ['field_survey_id'],
            dependOn: {
              field: 'survey_variant',
              value: 'L',
            },
          },
        ],
      },
    ],
    'soil-bulk-density-sample': [
      {
        ruleType: 'ConditionalFieldsCheck',
        conditions: [
          {
            requiredFields: ['barcode'],
            dependOn: {
              field: 'not_collected',
              value: false,
            },
          },
        ],
      },
    ],
    'soil-sub-pit-and-metagenomics-survey': [
      {
        ruleType: 'ConditionalFieldsCheck',
        stepName: 'Survey set-up', //optional
        conditions: [
          {
            requiredFields: ['observers', 'start_date_time', 'variant'],
            dependOn: {
              field: 'survey_variant',
              value: 'F',
            },
          },
          {
            requiredFields: ['field_survey_id'],
            dependOn: {
              field: 'survey_variant',
              value: 'L',
            },
          },
        ],
      },
    ],
    'soil-pit-characterisation-lite': [
      {
        ruleType: 'ConditionalFieldsCheck',
        conditions: [
          {
            requiredFields: [
              'observers',
              'soil_pit_id',
              'collection_method',
              'location',
              'soil_pit_depth',
              'digging_stopped_by',
              'pit_photo',
              'observation_type',
              'start_date_time',
            ],
            dependOn: {
              field: 'survey_variant',
              value: 'F',
            },
          },
          {
            requiredFields: ['field_survey_id'],
            dependOn: {
              field: 'survey_variant',
              value: 'L',
            },
          },
        ],
      },
    ],
    'soil-sub-pit-sampling': [
      {
        ruleType: 'ConditionalFieldsCheck',
        conditions: [
          {
            requiredFields: ['barcode'],
            dependOn: {
              field: 'not_collected',
              value: false,
            },
          },
        ],
      },
    ],
    'vertebrate-end-trap': [
      {
        ruleType: 'ConditionalFieldsCheck',
        conditions: [
          {
            requiredFields: ['closed_pitfall_photo'],
            dependOn: {
              field: 'trap_status',
              value: ['PCF', 'PCL'],
            },
          },
        ],
      },
    ],
    'plot-layout': [
      {
        ruleType: 'ConditionalFieldsCheck',
        conditions: [
          {
            requiredFields: [
              'plot_selection',
              'plot_type',
              'replicate',
              'plot_dimensions',
            ],
            dependOn: {
              field: 'plot_points',
              value: '$defined',
            },
          },
        ],
      },
    ],
  },
}

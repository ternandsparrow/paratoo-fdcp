// list of apis
module.exports = {
  modelsToPopulate: {
    // required for UI and we don't need to check PDP read permission for these models.
    appInitialization: ['protocol'],
    /* PDF read permission required to populate all the models except the models required for app initialization */
    plotModels: ['plot-layout', 'plot-selection', 'plot-visit'],
    // required for vouchers
    vouchers: [
      'floristics-veg-survey-full',
      'floristics-veg-survey-lite',
      'floristics-veg-voucher-full',
      'floristics-veg-voucher-lite',
    ],
    // all the other models (e.g, non-plot-based protocol)
    plotBased: [
      // plot-based models that don't have handler for `project_ids`
      'plot-description-enhanced',
      'plot-description-standard',
      // need these for closing traps
      'vertebrate-trapping-setup-survey',
      'vertebrate-trap',
      'vertebrate-check-trap',
      'vertebrate-trap-line',
      // bulk density - requires characterisation
      'soil-pit-characterisation-full',
      'fauna-ground-counts-survey',
      'field-reconnaissance-and-transect-set-up',
      'ground-counts-vantage-point',
      'ground-counts-vantage-point-survey',
      // for lab phase
      'soil-pit-characterisation-full',
      'soil-horizon-observation',
      'soil-horizon-sample',
      'soil-sub-pit-and-metagenomics-survey',
      'soil-sub-pit',
      'soil-sub-pit-sampling',
      'soil-pit-characterisation-lite',
      'soil-lite-sample',
      'soil-bulk-density-survey',
      'soil-bulk-density-sample',
      //recruitment survivorship revisits
      'recruitment-survivorship-survey',
      'sign-based-track-station-survey-setup',
      'off-plot-belt-transect-survey',
      'ground-counts-vantage-point',
      'sign-based-track-station-setup',
      'sign-based-vehicle-track-survey',
    ],
    nonPlotBased: [
      // for when we don't have `project_ids`
      'camera-trap-feature',
      'camera-trap-information',
      'camera-trap-setting',
      'camera-trap-deployment-survey',
      'camera-trap-deployment-point',
      'camera-trap-reequipping-survey',
      'camera-trap-reequipping-point',
      'camera-trap-retrieval-survey',
      'camera-trap-retrieval-point',
      'interventions',
      'grassy-weeds-survey',
      'aerial-setup-desktop',
      'herbivory-off-plot-belt-transect-setup',
    ],
    // an array of model names which have field_names to sync unknown species of DexieDB
    // TODO should derive from models that have `x-paratoo-csv-list-taxa`
    modelsWithFieldNames: ['vegetation-mapping-observation']
  },
  userProjects: '/user-projects',
  login: '/auth/local',
  register: '/auth/local/register',
  validateToken: '/validate-token',
  tokenRefresh: '/token/refresh',
  mintIdentifier: '/mint-identifier',
  docBuildInfo: '/documentation/swagger-build-info',
  heartbeat: '/api/analytics/heartbeat'
}

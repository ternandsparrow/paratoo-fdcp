// api call functions

import * as constants from 'src/misc/constants'
import {
  chainedError,
  paratooWarnHandler,
  paratooDebugMessage,
  paratooSentryBreadcrumbMessage,
  customErrorHandler,
  notifyHandler,
  getRequestInfo,
  paratooErrorHandler,
  isUrlReadyToContinue,
  paratooWarnMessage,
  networkConnectionStatus,
} from 'src/misc/helpers'
import { useAuthStore } from '../stores/auth'
import { useEphemeralStore } from 'src/stores/ephemeral'
import { login } from './apiLists'
import { sleep } from './helpers'

export function doCoreApiGet({ urlSuffix }) {
  let url = urlSuffix
  return _doApiGet({
    apiUrlBase: constants.coreApiUrlBase,
    urlSuffix: url,
  })
}

export function doCoreApiPost({ urlSuffix, body, handleUnauthorised=true,offlinePublicationQueueIndex=null }) {
  return _doApiPost({
    apiUrlBase: constants.coreApiUrlBase,
    urlSuffix,
    body,
    handleUnauthorised,
    offlinePublicationQueueIndex
  })
}

export function doCoreApiDelete({ urlSuffix, body, handleUnauthorised=true }) {
  return _doApiDelete({
    apiUrlBase: constants.coreApiUrlBase,
    urlSuffix,
    handleUnauthorised,
  })
}

export function doCoreApiPut({ urlSuffix, body }) {
  return _doApiPut({
    apiUrlBase: constants.coreApiUrlBase,
    urlSuffix,
    body,
  })
}


export function doOrgApiGet({ urlSuffix }) {
  return _doApiGet({
    apiUrlBase: constants.orgApiUrlBase,
    urlSuffix,
  })
}

export function doOrgApiPost({
  urlSuffix,
  suppressAuthTokenHeader = false,
  body,
}) {

  return _doApiPost({
    apiUrlBase: constants.orgApiUrlBase,
    urlSuffix,
    body,
    suppressAuthTokenHeader,
  })
}

function _doApiGet({ apiUrlBase, urlSuffix, providedHeaders }) {
  if (providedHeaders) {
    return doManagedFetch(`${apiUrlBase}${urlSuffix}`, {
      headers: {
        ...providedHeaders,
      },
    })
  }
  return doManagedFetch(`${apiUrlBase}${urlSuffix}`, {
    headers: {},
  })
}

function _doApiPost({ apiUrlBase, urlSuffix, body, extraHeaders, suppressAuthTokenHeader, handleUnauthorised=true,offlinePublicationQueueIndex=null }) {
  return doManagedFetch(
    `${apiUrlBase}${urlSuffix}`,
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        ...extraHeaders,
      },
      body: JSON.stringify({ ...body }),
    },
    [201],
    //NOTE: was false, revisit if there is any issues.
    //need to be true for dispatches to doCoreApiPost return a defined response
    true,
    !suppressAuthTokenHeader,
    handleUnauthorised,
    1,
    offlinePublicationQueueIndex
  )
}

function _doApiDelete({ apiUrlBase, urlSuffix, body, extraHeaders, handleUnauthorised }) {
  return doManagedFetch(
    `${apiUrlBase}${urlSuffix}`,
    {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        ...extraHeaders,
      },
    },
    [],
    true,
    true,
    handleUnauthorised,
  )
}

function _doApiPut({ apiUrlBase, urlSuffix, body, extraHeaders }) {
  return doManagedFetch(
    `${apiUrlBase}${urlSuffix}`,
    {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        ...extraHeaders,
      },
      body: JSON.stringify({ ...body }),
    },
    [],
    true,
    true,
  )
}

export async function doManagedFetch(
  url,
  init,
  alsoOkHttpStatuses,
  isRespBodyRequired,
  includeBearerToken=true,
  handleUnauthorised=true,
  tries = 1,
  offlinePublicationQueueIndex = null
) {
  const ephemeralStore = useEphemeralStore()
  const authStore = useAuthStore()
  const MAX_TRIES = 5

  const urlStatus = isUrlReadyToContinue(url, includeBearerToken, authStore, init)
  if (!urlStatus.isReady) {
    paratooWarnHandler(`${urlStatus.msg}, url: ${url}`)
    return null
  }
  
  let resp = null
  let responseInfo = null
  try {
    if (includeBearerToken && urlStatus.tokenExpired && !urlStatus.refreshTokenExpired) {
      paratooWarnMessage('Auth token is expired, trying to refresh token automatically')
      await authStore.refreshAuthToken()
    }
    // if token exists and swagger not present in the url
    if (includeBearerToken) {
      init['headers']['Authorization'] = `Bearer ${authStore.token}`
    }

    // so that we can abort all requests using signal when device goes offline
    const last = ephemeralStore.fetchAbortControllers.length - 1
    init['signal'] = ephemeralStore.fetchAbortControllers[last].signal
    
    resp = await fetch(url, init)

    
    if (handleUnauthorised) {
      // some errors need custom handler
      responseInfo = await customErrorHandler(url, init, resp,offlinePublicationQueueIndex)
    } else {
      paratooSentryBreadcrumbMessage(
        'doManagedFetch will NOT handle unauthorised automatically',
        'doManagedFetch',
      )
      // FIXME: doesn't this happen in catch?
      if(!resp.ok) {
        const dataResp = await resp.text()
        responseInfo = {
          throwError: resp.ok,
          msg: dataResp.error ? dataResp.error.message : ''
        }
      } else {
        responseInfo = {
          throwError: resp.ok
        }
      }
    }
    // if response is 401 and token exists
    if (responseInfo.refreshAuthToken && handleUnauthorised && authStore.token && tries < MAX_TRIES) {
      // if fails, will throw for catch block
      await authStore.refreshAuthToken(responseInfo.error)
      if (authStore.token && includeBearerToken) {
        // need to re-apply new token, as the one passed has expired
        init['headers']['Authorization'] = `Bearer ${authStore.token}`
      }
      return await doManagedFetch(
        url, 
        init, 
        alsoOkHttpStatuses,
        isRespBodyRequired,
        includeBearerToken,
        handleUnauthorised,
        tries + 1
      )
    }

    let errCause = null
    if (responseInfo?.errorCode) {
      switch (responseInfo.errorCode) {
        case 'ERR_MISSING_MINTED_ORG_IDENTIFIER':
          errCause = responseInfo.errorCode
          break
        default:
          paratooWarnMessage(`Programmer error: unregistered handling for error code ${responseInfo.errorCode}`)
          break
      }
    }

    if (responseInfo.throwError && responseInfo.msg) {
      const errObj = new Error(responseInfo.msg)

      if (errCause) {
        errObj.cause = errCause
      }

      throw paratooErrorHandler('An error occurred while making request', errObj)
    }

  } catch (err) {
    if (isDowngradable(err.message)) {
      const result = chainedError(
        '[Downgraded error] something went wrong during fetch that we cannot control',
        err,
      )
      result.isDowngradable = true
      throw result
    }
    if (
      err.httpStatus === 401 &&
      err.body.error.message === 'Auth token is not valid'
    ) {
      const result = chainedError(
        'Your credentials have expired, please login again.',
        err,
      )
      throw result
    }
    const isNetworkErrorParatoo = err.message === 'Failed to fetch' || err.message === 'The user aborted a request.'
    const isOnline = useEphemeralStore().networkOnline
    if (isNetworkErrorParatoo && !isOnline) {
      notifyHandler('warning', 'Request failed due to no network connection')
      paratooDebugMessage('Request failed but we\'re offline so ignoring')
      return null
    }

    // if there's a network error, we will retry up to MAX_TRIES
    if(isNetworkErrorParatoo && tries < MAX_TRIES) {
      paratooDebugMessage(`Request failed ${tries}. Retrying...`)   
      await sleep(5000) //retry after 5 seconds 
      return await doManagedFetch(
        url,
        init,
        alsoOkHttpStatuses,
        isRespBodyRequired,
        includeBearerToken,
        handleUnauthorised,
        tries + 1,
      )
    }

    let networkErrorParatooMsg = null
    // if one of the servers is down and we reach max tries, we will notify user.
    if (isNetworkErrorParatoo) {
        const requestInfo = getRequestInfo(url, init)
        networkErrorParatooMsg = `Request failed to ${requestInfo.method} ${requestInfo.action}. ${requestInfo.source} is unavailable`
        if (url.includes(login)) {
          networkErrorParatooMsg = `Request failed. ${requestInfo.source} is unavailable`
        }
        if (url.includes('swagger')) {
          paratooDebugMessage(networkErrorParatooMsg)
          return null
        }
        const connectionStatus = networkConnectionStatus()
        notifyHandler('negative', networkErrorParatooMsg)
        const networkInfo = `\nstatus code ${err.httpStatus}, \nnetwork info: ${JSON.stringify(connectionStatus, null, 2)}`
        paratooWarnHandler(networkErrorParatooMsg+ networkInfo, err)
    }
    
    let msg = `Failed while doing fetch() with\n`
    msg += `  Method='${(init || {}).method || 'GET'}'\n`
    msg += `  URL='${url}'\n`
    msg += `  Req body='${JSON.stringify(init, null, 2)}'`
    const result = chainedError(msg, err)
    try {
      
      result.isNetworkErrorParatoo = isNetworkErrorParatoo
      if (networkErrorParatooMsg) result.msg = networkErrorParatooMsg
      if (responseInfo.msg) result.msg = responseInfo.msg
    } catch (err2) {
      paratooWarnHandler(
        `Could not set property on error object. It's only for a nicer UX, ` +
          `so continuing without it`,
        err2,
      )
    }
    throw result
  }

  //  handle response go here
  const contentType = resp.headers.get('Content-Type').split(';')[0]
  let result
  switch (contentType) {
    case 'application/json': {
      if (!responseInfo.handleJsonResp) {
        result = null
      } else {
        result = await handleJsonResp(
          resp,
          alsoOkHttpStatuses,
          isRespBodyRequired,
        )
      }
      break
    }
    case 'application/xml':
      result = await handleXmlResp(resp)
      break
    case 'text/plain':
      result = resp
      break
  
    default:
      throw paratooErrorHandler('Unknown content type: ' + contentType, new Error('Unknown content type'))
      // break
  }


  return result
}

async function handleJsonResp(
  resp,
  alsoOkHttpStatuses = [],
  isRespBodyRequired = true,
) {
  const isJson = isRespJson(resp)
  const isRespStatusOk = resp.ok || alsoOkHttpStatuses.includes(resp.status)
  if (isRespStatusOk && !isRespBodyRequired) {
    return
  }
  if (isRespStatusOk && isJson) {
    try {
      const result = await resp.json()
      return result
    } catch (err) {
      throw chainedError('Failed while parsing JSON response', err)
    }
  }
  // resp either NOT ok or NOT JSON, prep nice error msg
  const bodyAccessor = isJson ? 'json' : 'text'
  const bodyPromise = resp.bodyUsed
    ? Promise.resolve('(body already used)')
    : resp[bodyAccessor]()
  const body = await bodyPromise
  const trimmedBody =
    typeof body === 'string'
      ? body.substr(0, 300)
      : JSON.stringify(body).substr(0, 300)
  let msg = `\nResponse is either not OK or not JSON\nResp details:\n`
  msg += `  is status ok=${isRespStatusOk},\n`
  msg += `  is JSON=${isJson}\n`
  msg += `  status=${resp.status}\n`
  msg += `  statusText='${resp.statusText}'\n`
  msg += `  headers=${JSON.stringify(resp.headers)}\n`
  msg += `  url=${resp.url}\n`
  msg += `  body first 300 chars='${trimmedBody}'\n`
  let err = new Error(msg)
  err.httpStatus = resp.status
  err.body = body
  throw err
}

async function handleXmlResp(resp) {
  try {
    const xmlText = await resp.text()
    const parser = new DOMParser()
    const xmlDoc = parser.parseFromString(xmlText, 'application/xml')
    const contentsEl = xmlDoc.getElementsByTagName('Contents')
    const contents = []
    for (let i = 0; i < contentsEl.length; i++) {
      const Key = contentsEl[i].getElementsByTagName('Key')[0].textContent
      contents.push({ Key })
    }
    return contents
  } catch (error) {
    throw paratooErrorHandler('Failed while parsing XML response', error)    
  }
}

function isDowngradable(msg) {
  // there are number of error related to fetching that aren't good but also
  // there's nothing we can do about it. Things like network dropping out. Here
  // we build a list of error messages that indicate those situations and
  // downgrade the errors to warnings. The system operators still should know
  // they're happening but we don't want them to panic.
  const downgradableMessages = ['The network connection was lost']
  return downgradableMessages.some((e) => msg.includes(e))
}

function isRespJson(resp) {
  const mimeStr = resp.headers.get('Content-Type') || ''
  return /application\/(\w+(\.\w+)*\+)?json/.test(mimeStr)
}

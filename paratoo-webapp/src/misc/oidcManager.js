import { UserManager, WebStorageStateStore, Log } from 'oidc-client-ts'
let userManager = null
export default class OIDCManager {
  constructor(configuration) {
    Log.setLogger(console)
    Log.setLevel(Log.DEBUG)

    const settings = {
      userStore: new WebStorageStateStore({
        store: window.localStorage,
      }),
      authority: configuration.issuer,
      client_id: configuration.client_id,
      redirect_uri: configuration.login_redirect_uri,
      response_type: 'code',
      scope: configuration.scopes,
      code_challenge_method: 'S256',
      loadUserInfo: true,
      automaticSilentRenew: true,
    }
    userManager = new UserManager(settings)

    // events
    userManager.events.addUserLoaded(() => {
      console.debug('USER LOADED EVENT')
    })
    userManager.events.addUserSignedIn(() => {
      console.debug('USER SIGNED IN EVENT')
    })
  }

  async loginRedirect() {
    if (!userManager) return null

    userManager.signinRedirect()
  }
}

// import Dexie from 'dexie'
import Dexie  from "dexie"

export const dexieDB = new Dexie('dexieDatabase')

// Define the V1 table schema
dexieDB.version(1).stores({
  vascularFlora: '++id, canonicalName, scientificName', //non-id values are indexed
  mosses: '++id, canonicalName, scientificName',
  amphibia: '++id, canonicalName, scientificName',
  lichens: '++id, canonicalName, scientificName',
  mammalia: '++id, canonicalName, scientificName',
  birds: '++id, canonicalName, scientificName',
  reptiles: '++id, canonicalName, scientificName',
})


dexieDB.version(2).stores({
  vascularFlora: '++id, canonicalName, scientificName', //non-id values are indexed
  mosses: '++id, canonicalName, scientificName',
  amphibia: '++id, canonicalName, scientificName',
  lichens: '++id, canonicalName, scientificName',
  mammalia: '++id, canonicalName, scientificName',
  birds: '++id, canonicalName, scientificName',
  reptiles: '++id, canonicalName, scientificName',
  fieldNames: '++id, canonicalName, scientificName',
})

dexieDB.version(3).stores({
  vascularFlora: '++id, canonicalName, scientificName, *canonicalName_, *scientificName_', //non-id values are indexed
  mosses: '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
  amphibia: '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
  lichens: '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
  mammalia: '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
  birds: '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
  reptiles: '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
  fieldNames: '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
})

dexieDB.version(4).stores({
  vascularFlora:
    '++id, canonicalName, scientificName, *canonicalName_, *scientificName_', //non-id values are indexed
  mosses:
    '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
  amphibia:
    '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
  lichens:
    '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
  mammalia:
    '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
  birds:
    '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
  reptiles:
    '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
  fieldNames:
    '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
  ANZSRCFieldsOfResearch: '++id, anzsrcid, option, *anzsrcid_, *option_',
  GCMDScienceKeywords: '++id, keyword, *keyword_',
})

//Add hooks that will index for full-text search:
//https://github.com/dexie/Dexie.js/blob/master/samples/full-text-search/FullTextSearch.js
//TODO can cause over-matching when using `startsWithAnyOfIgnoreCase` (e.g., searching 'australian magpie' matches everything that has words starting with 'australian' and 'magpie') - may want to index the 'words' in a more trie-like structure
function getAllWords(text) {
  let allWordsIncludingDups = text.split(' ')
  let wordSet = allWordsIncludingDups.reduce(function (prev, current) {
    prev[current] = true
    return prev
  }, {})
  return Object.keys(wordSet)
}

for (const table of [
  'vascularFlora',
  'mosses',
  'amphibia',
  'lichens',
  'mammalia',
  'birds',
  'reptiles',
  'fieldNames',
]) {
  // eslint-disable-next-line
  dexieDB[table].hook('creating', function (primKey, obj, trans) {
    if (typeof obj.canonicalName == 'string')
      obj.canonicalName_ = getAllWords(obj.canonicalName)
    if (typeof obj.scientificName == 'string')
      obj.scientificName_ = getAllWords(obj.scientificName)
  })
  // eslint-disable-next-line
  dexieDB[table].hook('updating', function (mods, primKey, obj, trans) {
    if (Object.keys(mods).includes('canonicalName')) {
      if (typeof mods.canonicalName == 'string')
        return { canonicalName_: getAllWords(mods.canonicalName) }
      else return { canonicalName_: [] }
    }

    if (Object.keys(mods).includes('scientificName')) {
      if (typeof mods.scientificName == 'string')
        return { scientificName_: getAllWords(mods.canonicalName) }
      else return { scientificName_: [] }
    }
  })
}

for (const table of ['GCMDScienceKeywords', 'ANZSRCFieldsOfResearch']) {
  dexieDB[table].hook('creating', function (primKey, obj, trans) {
    if (typeof obj.anzsrcid == 'string')
      obj.anzsrcid_ = getAllWords(obj.anzsrcid)
    if (typeof obj.option == 'string') obj.option_ = getAllWords(obj.option)
    if (typeof obj.keyword == 'string') obj.keyword_ = getAllWords(obj.keyword)
  })
  // eslint-disable-next-line
  dexieDB[table].hook('updating', function (mods, primKey, obj, trans) {
    if (Object.keys(mods).includes('anzsrcid')) {
      if (typeof mods.anzsrcid == 'string')
        return { anzsrcid_: getAllWords(mods.anzsrcid) }
      else return { anzsrcid_: [] }
    }
    if (Object.keys(mods).includes('option')) {
      if (typeof mods.option == 'string')
        return { option_: getAllWords(mods.option) }
      else return { option_: [] }
    }
    if (Object.keys(mods).includes('keyword')) {
      if (typeof mods.keyword == 'string')
        return { keyword_: getAllWords(mods.keyword) }
      else return { keyword_: [] }
    }
  })
}
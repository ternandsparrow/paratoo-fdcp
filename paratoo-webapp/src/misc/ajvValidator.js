import Ajv from 'ajv'
import cloneDeep from 'lodash/cloneDeep'
import { validate as isUuid } from 'uuid'
import addFormats from 'ajv-formats'
import {
  prettyFormatFieldName,
  findValForKey,
  paratooSentryBreadcrumbMessage,
  paratooErrorHandler,
} from './helpers'
import { useBulkStore } from 'src/stores/bulk'
const ajv = new Ajv({ strictSchema: false, allErrors: true })
addFormats(ajv)
ajv.addFormat('uuid', {
  type: 'string',
  validate: (val) => isUuid(val),
})

function createWebappVersionSchema(schema, newSchema = {}, parentKey) {
  // flat out the "data", as we don't have it in the webapp, it's only added when make a 'bulk' request
  if (schema?.properties?.data) {
    newSchema = createWebappVersionSchema(schema.properties.data, newSchema)
  } else if (schema['x-model-ref']) {
    newSchema = handleXModelRef(schema, newSchema, parentKey)
  } else newSchema = processSchemaProperties(schema, newSchema)

  return newSchema
}

function handleXModelRef(schema, newSchema, parentKey) {
  // for files, we store them in indexedDb with uuid as reference, so we need to update the schema for them
  if (schema['x-model-ref'] === 'file') {
    if (parentKey === 'media') {
      newSchema.type = 'string'
      newSchema.format = 'uuid'
    } else {
      newSchema.type = 'object'
      newSchema.required = ['src']
      newSchema.properties = {
        src: { type: 'string', format: 'uuid' },
      }
    }
  } else if (schema.type === 'integer') {
    // for offline cases
    newSchema = {
      anyOf: [
        { type: 'integer' }, // usually ID
        {
          type: 'object',
          required: ['temp_offline_id'],
          properties: { temp_offline_id: { type: 'string', format: 'uuid' } },
        },
      ],
    }
  }
  return newSchema
}

// plot-layout and plot-visit are the two only cases atm
function createAnyOfSchema(anyOfValues) {
  return [
    ...anyOfValues.map((value) => createWebappVersionSchema(value)),
    {
      type: 'object',
      properties: {
        id: {
          type: 'object',
          properties: {
            temp_offline_id: { type: 'string', format: 'uuid' },
          },
          required: ['temp_offline_id'],
        },
      },
    },
  ]
}

function processSchemaProperties(schema, newSchema) {
  for (const [key, value] of Object.entries(schema)) {
    if (key === 'orgMintedIdentifier') continue

    if (isObject(value)) {
      newSchema[key] = createWebappVersionSchema(value, newSchema[key], key)
    } else if (key === 'anyOf') {
      newSchema[key] = createAnyOfSchema(value)
    } else {
      newSchema[key] = value
    }
  }
  return newSchema
}

function isObject(value) {
  return value && typeof value === 'object' && !Array.isArray(value)
}

/**
 * similar to prettyFormatFieldName, but we get the x-paratoo-rename flag and overrideDisplayName if they are available
 * @param {*} field
 * @param {*} schema
 * @returns
 */
function beautifyFieldName(field, schema) {
  const schemaProperty = findValForKey(schema, field)
  const customName =
    schemaProperty['x-paratoo-rename'] || schemaProperty['overrideDisplayName']
  if (customName) return customName
  else return prettyFormatFieldName(field)
}

/**
 * format the error path in a user friendly way
 * @param {*} fullPath
 * @param {*} schema
 * @returns
 */
function formatPath(fullPath = '', schema) {
  fullPath = fullPath.split('/')
  const message = fullPath.reduce((accum, path, index) => {
    if (!path) return accum
    let formattedName = `${beautifyFieldName(path, schema)}`
    if (/^\d+$/.test(path)) {
      formattedName = parseInt(path) + 1
    }
    accum += formattedName
    if (index !== fullPath.length - 1) {
      accum += '/'
    }
    return accum
  }, '')
  return `"${message}"`
}

function beautifyAjvErrors(
  errors,
  schema,
  collectionData,
  isSubmodule = false,
) {
  const relatedAnyOfPaths = []
  const data = cloneDeep(collectionData)
  let formattedErrors = []
  for (const error of errors) {
    // this keyword will come with other errors related to it, so we only need these errors instead of this one
    if (error.keyword === 'anyOf') {
      relatedAnyOfPaths.push(error.instancePath)
      continue
    }

    const path = formatPath(error.instancePath, schema)
    let message
    let relatedData
    // Customize messages based on keyword
    switch (error.keyword) {
      case 'required': {
        const field = beautifyFieldName(error.params.missingProperty, schema)
        message = `Field "${field}" is missing at ${path}.`
        relatedData = getDataOfErrorField(error.instancePath, path, data, true)
        break
      }
      case 'type':
        message = `Field at ${path} should be of type "${error.params.type}".`
        relatedData = getDataOfErrorField(error.instancePath, path, data)
        break
      case 'minLength':
        message = `Field at ${path} should have at least ${error.params.limit} characters.`
        relatedData = getDataOfErrorField(error.instancePath, path, data)
        break
      case 'maxLength':
        message = `Field at ${path} should not exceed ${error.params.limit} characters.`
        relatedData = getDataOfErrorField(error.instancePath, path, data)
        break
      // TODO: lut case, not sure should we show the allowed values as some lut may have more then 20 items
      // case 'enum':
      //   message = `Error at ${path}: ${error.message}: ${error.params.allowedValues.toString()}`
      //   break
      case 'format':
        if (error.params.format === 'uuid') {
          message = `Invalid Media/File format at ${path}`
        } else {
          message = `Field at ${path} ${error.message} `
        }
        relatedData = getDataOfErrorField(error.instancePath, path, data)
        break
      default:
        paratooSentryBreadcrumbMessage(error, 'ajv')
        message = `Error at ${path}: ${error.message}.`
        // for now, the only known error in this case are minItems and maxItems, which don't need to show the related fields
        // as it don't have related data
        if (error.keyword !== 'minItems' && error.keyword !== 'maxItems') {
          relatedData = getDataOfErrorField(error.instancePath, path, data)
        }
        break
    }
    formattedErrors.push({
      path: error.instancePath,
      message,
      relatedData,
      isSubmodule,
    })
  }

  // reduce anyOf errors, usually when a schema have anyOf keyword and the data does not match any of them,
  // ajv will show 3 errors but we only need 1 , so we need to reduce them
  formattedErrors = reduceAnyOfErrors(relatedAnyOfPaths, formattedErrors)

  return formattedErrors
}

function reduceAnyOfErrors(relatedAnyOfPaths, formattedErrors) {
  relatedAnyOfPaths.forEach((path) => {
    let newReducedError
    const relatedIndices = []
    formattedErrors.forEach((error, index) => {
      if (error.path === path) {
        if (!newReducedError) {
          newReducedError = structuredClone(error)
          newReducedError.message = `Data is invalid due to one of the following reasons: ${newReducedError.message}`
        } else {
          // remove the dot and merge the messages
          newReducedError.message = newReducedError.message.replace(/\.$/, '')
          newReducedError.message += `, or ${error.message}`
          if (!newReducedError.relatedData)
            newReducedError.relatedData = error.relatedData
        }
        relatedIndices.push(index)
      }
    })
    formattedErrors = formattedErrors.filter(
      (_, index) => relatedIndices.indexOf(index) === -1,
    )
    formattedErrors.push(newReducedError)
  })
  return formattedErrors
}

/**
 * Create a data structure of the component contain the field, to show in the summary component
 * with the error field highlighted in red
 * @param {*} path
 * @param {*} beautifiedPath
 * @param {*} collectionData
 * @returns
 */
function getDataOfErrorField(
  path,
  beautifiedPath,
  collectionData,
  missingField = false,
) {
  path = path.split('/')
  let curr = collectionData
  // get the parent of field error, so we might have an better view
  let fieldHavingTheIssue //as we might have src or media, so last node might not be the field having the issue
  //exclude the error field in the path unless it missing
  const lastNode = missingField ? path.length : path.length - 1
  for (let i = 0; i < lastNode; i++) {
    const node = path[i]
    //as we '/' at the beginning of path so we have empty first item as empty string after splitting
    if (node === '') continue
    if (node === 'src' || node === 'media') break
    curr = curr[node]
    fieldHavingTheIssue = path[i + 1]
  }
  let relatedData = { [beautifiedPath]: [] }
  Object.entries(curr).forEach(([field, value]) => {
    // don't need to display nested value as we only need to show the data of the related level
    if (typeof value === 'object') return
    const row = { field, value }
    if (fieldHavingTheIssue === field) {
      row.style = 'text-red text-bold'
    }
    relatedData[beautifiedPath].push(row)
  })
  return [relatedData]
}

/**
 * validate protocol data in bulk store based on it's bulk's schema
 * @param {*} schema
 * @param {*} protId
 * @returns
 */
export function schemaValidate(schema, protId, isSubmodule = false) {
  try {
    const newSchema = createWebappVersionSchema(schema)
    // we only need to remove this field in required array once so there's not need to put it in the recursive function
    newSchema.required = newSchema.required.filter(
      (f) => f !== 'orgMintedIdentifier',
    )
    // edge case for floristics with plant tissue as submodule
    if (newSchema.properties['floristics-veg-genetic-voucher-survey']) {
      const variant = ['lite', 'full']
      variant.forEach((v) => {
        newSchema.properties[
          'floristics-veg-genetic-voucher'
        ].items.properties.floristics_voucher.properties[
          `voucher_${v}`
        ].anyOf.push({
          type: 'string',
        })
      })
    }

    // edge case for soil bulk density, as the relation is resolved in dataManager.resolveSoilCharacterisationRelation
    if (newSchema.properties['soil-bulk-density-survey']) {
      newSchema.properties[
        'soil-bulk-density-survey'
      ].properties.associated_soil_pit_id.anyOf.push({
        type: 'object',
        required: ['id'],
        properties: {
          id: {
            type: 'number',
          },
        },
      })
    }

    // for plot visit and layout
    // const protocolUuid = useApiModelsStore().protUuidFromId(protId)
    // if (protocolUuid.uuid === layoutAndVisitUuid) {
    //   // remove the offline id as this is a separate protocol, to avoid confusion for the user
    //   newSchema.properties['plot-visit'].anyOf.splice(2, 1)

    // }

    const validate = ajv.compile(newSchema)
    const currentData = useBulkStore().collectionGetForProt({ protId })
    validate(JSON.parse(JSON.stringify(currentData)))
    if (!validate.errors) return []
    paratooSentryBreadcrumbMessage(validate.errors, 'schemaValidate')
    console.log(newSchema)
    const errors = beautifyAjvErrors(
      validate.errors,
      newSchema,
      currentData,
      isSubmodule,
    )
    return structuredClone(errors)
  } catch (error) {
    // as this is a relatively new feature with a lot of nested logics,
    // so we don't want the app hang if there's any unexpected errors
    // and core will catch the actual errors when user sync the collection
    paratooErrorHandler('ajv programmatically failed', error)
    return []
  }
}

export const conditionalFieldLabels = [
  {
    // e.g. message should be `Field: "Plot Visit" is required` instead of `Field: "Id" is required`.
    fields: ['id'],
    label: 'modelName',
  },
  // can be extended for other cases
]

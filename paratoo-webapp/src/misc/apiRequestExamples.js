export const bird_survey_example = {
  "data": {
    "collections": [
      {
        "plot-layout": {
          "id": 4
        },
        "plot-location": {
          "id": 4
        },
        "plot-visit": {
          "id": 5
        },
        "weather-observation": {
          "data": {
            "precipitation": "NO",
            "wind_description": "LW",
            "cloud_cover": "SU",
            "temperature": 31
          }
        },
        "bird-survey-observation": [
          {
            "data": {
              "species": "Blue-winged Kookaburra [sp] (scientific: Dacelo leachii)",
              "count": 2,
              "observation_type": "H",
              "activity_type": "FOG",
              "sex": "F",
              "observation_location_type": "WS",
              "breeding_type": "YON",
              "fauna_maturity": "A",
              "location": {
                "lat": -34.9765632,
                "lng": 138.6315776
              }
            }
          },
          {
            "data": {
              "species": "Noisy Miner [sp] (scientific: Manorina melanocephala)",
              "count": 2,
              "observation_type": "S",
              "activity_type": "ROS",
              "sex": "M",
              "observation_location_type": "OSH",
              "breeding_type": "YON",
              "fauna_maturity": "J",
              "location": {
                "lat": -34.9765632,
                "lng": 138.6315776
              }
            }
          }
        ],
        "bird-survey": {
          "data": {
            "start_date": "2022-09-14T01:38:29.237Z",
            "end_date": "2022-09-14T01:58:32.077Z",
            "survey_type": "202",
            "playback_used": true,
            "survey_metadata": {
              "survey_details": {
                "survey_model": "bird-survey",
                "time": "2024-03-04T05:52:28.193Z",
                "uuid": "ea344b36-859a-42a7-bc09-61cc2783e3e2",
                "project_id": "1",
                "protocol_id": "f0e75400-491f-44b3-9034-271f3a858f03",
                "protocol_version": "1"
              },
              "user_details": {
                "role": "collector",
                "email": "testuser@email.com",
                "username": "TestUser"
              },
              "provenance": {
                "version_app": "0.0.1-xxxxx",
                "version_core_documentation": "0.0.0-xxxx",
                "system_app": "Monitor FDCP API--localdev",
                "version_core": "0.0.0-xxxx",
                "system_core": "Monitor FDCP API-development",
                "version_org": "0.0.0-xxxx",
                "system_org": "Monitor FDCP API-development"
              }
            },
            "protocol_variant": null
          }
        },
        "orgMintedIdentifier": "{\"iv\":\"yl9P/3ZnJgN61Iizt8usuw==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"t3tYF1dWmL8=\",\"ct\":\"g+GFSQqYDtb3PHroQ1VUP8WITMD8/AyuOUt25OOimCad8MTk8t4GbPtZMVMTdFBSgzkLmuacUBXluxfbmh96VqOcMB3yiu7N1D03UkS+dtE3K5bgRFspDs7/xsmJ2zt3NxlyN995NVq1JoRPYxmMc5Y2sIKNeY2G+zTOG60R+Qz3ioQspTzPbN/Fl0wuJcH4KG4AytBKC3iMnSRlKr3JKWOnPBU8Eel9/2DcAN++NIszQbNwn33dJ5N1GdZKaITTLlhb6gZmcOZ7\"}"
      },
      {
        "plot-layout": {
          "id": 4
        },
        "plot-location": {
          "id": 4
        },
        "plot-visit": {
          "id": 5
        },
        "weather-observation": {
          "data": {
            "precipitation": "SH",
            "precipitation_duration": "O",
            "wind_description": "MW",
            "cloud_cover": "PC",
            "temperature": 15
          }
        },
        "bird-survey-observation": [
          {
            "data": {
              "species": "Australian Brush-turkey [sp] (scientific: Alectura lathami)",
              "count": 4,
              "observation_type": "S",
              "activity_type": "ROS",
              "sex": "A",
              "observation_location_type": "ODH",
              "breeding_type": "RFY",
              "fauna_maturity": "I",
              "location": {
                "lat": -34.9765632,
                "lng": 138.6315776
              }
            }
          },
          {
            "data": {
              "species": "Australian Eurasian Coot [ssp] (scientific: Fulica atra australis)",
              "count": 6,
              "observation_type": "H",
              "activity_type": "ROT",
              "sex": "X",
              "observation_location_type": "OSH",
              "breeding_type": "RFY",
              "fauna_maturity": "J",
              "location": {
                "lat": -34.9765632,
                "lng": 138.6315776
              }
            }
          }
        ],
        "bird-survey": {
          "data": {
            "start_date": "2022-09-14T01:40:17.830Z",
            "end_date": "2022-09-14T02:40:21.285Z",
            "survey_type": "500",
            "playback_used": true,
            "survey_metadata": {
              "survey_details": {
                "survey_model": "bird-survey",
                "time": "2024-03-04T05:52:28.193Z",
                "uuid": "ea344b36-859a-42a7-bc09-61cc2783e3e2",
                "project_id": "1",
                "protocol_id": "f0e75400-491f-44b3-9034-271f3a858f03",
                "protocol_version": "1"
              },
              "user_details": {
                "role": "collector",
                "email": "testuser@email.com",
                "username": "TestUser"
              },
              "provenance": {
                "version_app": "0.0.1-xxxxx",
                "version_core_documentation": "0.0.0-xxxx",
                "system_app": "Monitor FDCP API--localdev",
                "version_core": "0.0.0-xxxx",
                "system_core": "Monitor FDCP API-development",
                "version_org": "0.0.0-xxxx",
                "system_org": "Monitor FDCP API-development"
              }
            },
            "protocol_variant": null
          }
        },
        "orgMintedIdentifier": "{\"iv\":\"qrs+2P9O0B/Ssw35AJHKWg==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"t3tYF1dWmL8=\",\"ct\":\"bWteye78BRq97iD+o6KJ7Xi3BYg/udpkadWn0NeUQqNfp+QJQ5VsDx7Q0OpDIlXmZ5jHK+uqQUFJJyHqRGCL+8IidShFbN4EUSgm4qLujyYd60ienWknVljKGisCmDaq+SCmq8WIEH0E0Ac5pKaFAchB9ztBh6uOQCqi3QVibTcBlbq+oAS/RqXNpO0p3XitsZA/yQ378XPZ1H1Gi7PaEznt2e7hInDmWF4eIuqMr61XCSvT6XUTp5PLH7/dCi0Z/EiXxC7bqTw5\"}"
      }
    ]
  }
}

export const cover_pi_example = {
  data: {
    collections: [
      {
        'plot-layout': {
          id: 1,
        },
        'plot-location': {
          id: 1,
        },
        'plot-visit': {
          id: 1,
        },
        'cover-point-intercept-survey': {
          data: {
            start_date_time: '2022-09-14T05:03:07.115Z',
            end_date_time: '2022-09-14T05:03:07.115Z',
            survey_metadata: {
              survey_details: {
                survey_model: "cover-point-intercept-survey",
                time: "2024-03-04T05:52:28.193Z",
                uuid: "ea344b36-859a-42a7-bc09-61cc2783e3e2",
                project_id: "1",
                protocol_id: "f0e75400-491f-44b3-9034-271f3a858f03",
                protocol_version: "1"
              },
              user_details: {
                role: "collector",
                email: "testuser@email.com",
                username: "TestUser"
              },
              provenance: {
                version_app: "0.0.1-xxxxx",
                version_core_documentation: "0.0.0-xxxx",
                system_app: "Monitor FDCP API--localdev",
                version_core: "0.0.0-xxxx",
                system_core: "Monitor FDCP API-development",
                version_org: "0.0.0-xxxx",
                system_org: "Monitor FDCP API-development"
              }
            },
            protocol_variant: 'full',
          },
        },
        'cover-point-intercept-point': [
          {
            data: {
              point_number: 0,
              soils_substrate: 'BR',
              cover_transect_start_point: 'S1',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 1,
              soils_substrate: 'CR',
              cover_transect_start_point: 'S1',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 2,
              soils_substrate: 'OC',
              cover_transect_start_point: 'S1',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 3,
              soils_substrate: 'BR',
              cover_transect_start_point: 'S1',
              species_intercepts: [
                {
                  data: {
                    floristics_voucher_full: 4,
                    height: 4,
                    dead: true,
                    in_canopy_sky: false,
                  },
                },
                {
                  data: {
                    floristics_voucher_full: 5,
                    height: 1,
                    dead: false,
                    in_canopy_sky: true,
                  },
                },
              ],
            },
          },
          {
            data: {
              point_number: 4,
              soils_substrate: 'LT',
              cover_transect_start_point: 'S1',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 0,
              soils_substrate: 'LT',
              cover_transect_start_point: 'N2',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 1,
              soils_substrate: 'BR',
              cover_transect_start_point: 'N2',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 2,
              soils_substrate: 'WR',
              cover_transect_start_point: 'N2',
              species_intercepts: [
                {
                  data: {
                    floristics_voucher_full: 6,
                    height: 5,
                    dead: true,
                    in_canopy_sky: true,
                  },
                },
              ],
            },
          },
          {
            data: {
              point_number: 3,
              soils_substrate: 'RK',
              cover_transect_start_point: 'N2',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 4,
              soils_substrate: 'GR',
              cover_transect_start_point: 'N2',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 0,
              soils_substrate: 'CWD',
              cover_transect_start_point: 'S3',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 1,
              soils_substrate: 'LT',
              cover_transect_start_point: 'S3',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 2,
              soils_substrate: 'CWD',
              cover_transect_start_point: 'S3',
              species_intercepts: [
                {
                  data: {
                    floristics_voucher_full: 5,
                    height: 10,
                    dead: false,
                    in_canopy_sky: false,
                  },
                },
                {
                  data: {
                    floristics_voucher_full: 6,
                    height: 2,
                    dead: true,
                    in_canopy_sky: false,
                  },
                },
              ],
            },
          },
          {
            data: {
              point_number: 3,
              soils_substrate: 'UK',
              cover_transect_start_point: 'S3',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 4,
              soils_substrate: 'LT',
              cover_transect_start_point: 'S3',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 0,
              soils_substrate: 'BR',
              cover_transect_start_point: 'N4',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 1,
              soils_substrate: 'BR',
              cover_transect_start_point: 'N4',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 2,
              soils_substrate: 'OC',
              cover_transect_start_point: 'N4',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 3,
              soils_substrate: 'CR',
              cover_transect_start_point: 'N4',
              species_intercepts: [
                {
                  data: {
                    floristics_voucher_full: 5,
                    height: 13,
                    dead: true,
                    in_canopy_sky: true,
                  },
                },
              ],
            },
          },
          {
            data: {
              point_number: 4,
              soils_substrate: 'CWD',
              cover_transect_start_point: 'N4',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 0,
              soils_substrate: 'BR',
              cover_transect_start_point: 'S5',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 1,
              soils_substrate: 'GR',
              cover_transect_start_point: 'S5',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 2,
              soils_substrate: 'LT',
              cover_transect_start_point: 'S5',
              species_intercepts: [
                {
                  data: {
                    floristics_voucher_full: 4,
                    height: 3,
                    dead: true,
                    in_canopy_sky: false,
                  },
                },
                {
                  data: {
                    floristics_voucher_full: 5,
                    height: 1,
                    dead: false,
                    in_canopy_sky: false,
                  },
                },
                {
                  data: {
                    floristics_voucher_full: 6,
                    height: 8,
                    dead: false,
                    in_canopy_sky: false,
                  },
                },
              ],
            },
          },
          {
            data: {
              point_number: 3,
              soils_substrate: 'NC',
              cover_transect_start_point: 'S5',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 4,
              soils_substrate: 'UK',
              cover_transect_start_point: 'S5',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 0,
              soils_substrate: 'RK',
              cover_transect_start_point: 'W1',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 1,
              soils_substrate: 'GR',
              cover_transect_start_point: 'W1',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 2,
              soils_substrate: 'WR',
              cover_transect_start_point: 'W1',
              species_intercepts: [
                {
                  data: {
                    floristics_voucher_full: 5,
                    height: 2,
                    dead: false,
                    in_canopy_sky: true,
                  },
                },
              ],
            },
          },
          {
            data: {
              point_number: 3,
              soils_substrate: 'GR',
              cover_transect_start_point: 'W1',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 4,
              soils_substrate: 'CWD',
              cover_transect_start_point: 'W1',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 0,
              soils_substrate: 'CWD',
              cover_transect_start_point: 'E2',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 1,
              soils_substrate: 'CWD',
              cover_transect_start_point: 'E2',
              species_intercepts: [
                {
                  data: {
                    floristics_voucher_full: 4,
                    height: 9,
                    dead: false,
                    in_canopy_sky: true,
                  },
                },
                {
                  data: {
                    floristics_voucher_full: 4,
                    height: 2,
                    dead: true,
                    in_canopy_sky: false,
                  },
                },
              ],
            },
          },
          {
            data: {
              point_number: 2,
              soils_substrate: 'WR',
              cover_transect_start_point: 'E2',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 3,
              soils_substrate: 'WR',
              cover_transect_start_point: 'E2',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 4,
              soils_substrate: 'CWD',
              cover_transect_start_point: 'E2',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 0,
              soils_substrate: 'CR',
              cover_transect_start_point: 'W3',
              species_intercepts: [
                {
                  data: {
                    floristics_voucher_full: 5,
                    height: 3,
                    dead: true,
                    in_canopy_sky: true,
                  },
                },
                {
                  data: {
                    floristics_voucher_full: 6,
                    height: 1,
                    dead: false,
                    in_canopy_sky: false,
                  },
                },
              ],
            },
          },
          {
            data: {
              point_number: 1,
              soils_substrate: 'WR',
              cover_transect_start_point: 'W3',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 2,
              soils_substrate: 'LOO',
              cover_transect_start_point: 'W3',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 3,
              soils_substrate: 'LT',
              cover_transect_start_point: 'W3',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 4,
              soils_substrate: 'LT',
              cover_transect_start_point: 'W3',
              species_intercepts: [
                {
                  data: {
                    floristics_voucher_full: 5,
                    height: 2,
                    dead: false,
                    in_canopy_sky: true,
                  },
                },
              ],
            },
          },
          {
            data: {
              point_number: 0,
              soils_substrate: 'GR',
              cover_transect_start_point: 'E4',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 1,
              soils_substrate: 'UK',
              cover_transect_start_point: 'E4',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 2,
              soils_substrate: 'LOR',
              cover_transect_start_point: 'E4',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 3,
              soils_substrate: 'LOO',
              cover_transect_start_point: 'E4',
              species_intercepts: [
                {
                  data: {
                    floristics_voucher_full: 4,
                    height: 12,
                    dead: false,
                    in_canopy_sky: false,
                  },
                },
                {
                  data: {
                    floristics_voucher_full: 5,
                    height: 4,
                    dead: true,
                    in_canopy_sky: false,
                  },
                },
              ],
            },
          },
          {
            data: {
              point_number: 4,
              soils_substrate: 'LOO',
              cover_transect_start_point: 'E4',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 0,
              soils_substrate: 'CWD',
              cover_transect_start_point: 'W5',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 1,
              soils_substrate: 'CWD',
              cover_transect_start_point: 'W5',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 2,
              soils_substrate: 'LT',
              cover_transect_start_point: 'W5',
              species_intercepts: [],
            },
          },
          {
            data: {
              point_number: 3,
              soils_substrate: 'LOO',
              cover_transect_start_point: 'W5',
              species_intercepts: [
                {
                  data: {
                    floristics_voucher_full: 6,
                    height: 1,
                    dead: false,
                    in_canopy_sky: true,
                  },
                },
              ],
            },
          },
          {
            data: {
              point_number: 4,
              soils_substrate: 'LOO',
              cover_transect_start_point: 'W5',
              species_intercepts: [],
            },
          },
        ],
        orgMintedIdentifier:
          '{"iv":"gRKlFKyKSOnKcxZviD45fw==","v":1,"iter":10000,"ks":128,"ts":64,"mode":"ccm","adata":"","cipher":"aes","salt":"2FdC+EHYz70=","ct":"oa5iDqx/g6yxH5sPmCrA7w/98m9FyW6X5+BqbbwoJAChYC/cH416vvf0g1tgwMnByAx+3Nhf86WSZ0vK/Y8Ic+R5ZnCPMDmzO5czR1KdZRjNovVUS8L69rdirkr1sN3FqT6bGxWJIZoJmcrWyuEqal3UO1htoCPkDje4Td3Wqa60QL0yVHKSSPncrdNHPNyjef0cy28fh/AVTGFclgrGo4co3aokWf0DxWIp6RhlxLGpUPqHZ9sQr6XdE7iIy/NpLFg5WvI9+AHhtiv5QiuYPBoiRJLeOecwkhQI"}',
      },
    ],
  }
}

export const veg_mapping_example = {
  "data": {
    "collections": [
      {
        "vegetation-mapping-survey": {
          "data": {
            "start_date_time": "2022-07-25T01:56:11.549Z",
            "location": {
              "lat": -34.9765632,
              "lng": 138.6315776
            },
            "survey_metadata": {
              "survey_details": {
                "survey_model": "vegetation-mapping-survey",
                "time": "2024-03-04T05:52:28.193Z",
                "uuid": "ea344b36-859a-42a7-bc09-61cc2783e3e2",
                "project_id": "1",
                "protocol_id": "f0e75400-491f-44b3-9034-271f3a858f03",
                "protocol_version": "1"
              },
              "user_details": {
                "role": "collector",
                "email": "testuser@email.com",
                "username": "TestUser"
              },
              "provenance": {
                "version_app": "0.0.1-xxxxx",
                "version_core_documentation": "0.0.0-xxxx",
                "system_app": "Monitor FDCP API--localdev",
                "version_core": "0.0.0-xxxx",
                "system_core": "Monitor FDCP API-development",
                "version_org": "0.0.0-xxxx",
                "system_org": "Monitor FDCP API-development"
              }
            },
            "protocol_variant": null
          }
        },
        "vegetation-mapping-observation": [
          {
            "data": {
              "observation_id": "123",
              "position": {
                "lat": -34.9765632,
                "lng": 138.6315776
              },
              "photos": [
                1
              ],
              "veg_growth_stage": "AR",
              "disturbance": "1M",
              "fire_history": "RB",
              "vegetation_mapping_substrate_cover": {
                "data": {
                  "bare_cover_percent": 1,
                  "cryptogam_cover_percent": 1,
                  "outcrop_cover_percent": 1,
                  "litter_cover_percent": 1,
                  "rock_cover_percent": 1,
                  "coarse_woody_debris_cover_percent": 1,
                  "gravel_cover_percent": 1,
                  "unknown_cover_percent": 1
                }
              },
              "vegetation_mapping_species_covers": [
                {
                  "data": {
                    "species_name": "Characeae [Division] (scientific: Characeae Sachs)",
                    "percentage_cover": 1,
                    "growth_form": "FU",
                    "height_metres": 2
                  }
                },
                {
                  "data": {
                    "species_name": "Anthoceros capricornii [Species] (scientific: Anthoceros capricornii Cargill & G.A.M.Scott)",
                    "percentage_cover": 2,
                    "growth_form": "FU",
                    "height_metres": 3
                  }
                }
              ]
            }
          },
          {
            "data": {
              "observation_id": "321",
              "position": {
                "lat": -34.9765632,
                "lng": 138.6315776
              },
              "photos": [
                2
              ],
              "veg_growth_stage": "MA",
              "fire_history": "UB",
              "vegetation_mapping_substrate_cover": {
                "data": {
                  "bare_cover_percent": 2,
                  "cryptogam_cover_percent": 2,
                  "outcrop_cover_percent": 2,
                  "litter_cover_percent": 2,
                  "rock_cover_percent": 2,
                  "coarse_woody_debris_cover_percent": 2,
                  "gravel_cover_percent": 2,
                  "unknown_cover_percent": 2
                }
              },
              "vegetation_mapping_species_covers": [
                {
                  "data": {
                    "species_name": "Anthoceros adscendens [Species] (scientific: Anthoceros adscendens Lehm. & Lindenb.)",
                    "percentage_cover": 1,
                    "growth_form": "CY",
                    "height_metres": 1
                  }
                },
                {
                  "data": {
                    "species_name": "Illosporium [Genus] (scientific: Illosporium Mart.)",
                    "percentage_cover": 3,
                    "growth_form": "X",
                    "height_metres": 5
                  }
                }
              ]
            }
          }
        ],
        "orgMintedIdentifier": "{\"iv\":\"V9Q2IcmBQeUymNZaWDFcmA==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"aXjXSN6qC0U=\",\"ct\":\"cf+q9j0JFc4x8/hIdkuex9g9D4Uv29VQ3Gn9Z34jApUrROjUB+bY9eK9V2LWJywqYaja258WN1gtZ+72nBU0rZzqfc3u4Vm9M849n1MARY5/X5CpNTqH7MbVBRuHZhf9WD1UBIjsnEQ5n/uNpYEumH2USeqoTSPdXIzQFX+Kk3oA/K3Ip2UHZgpwlb/aEhQWh/uxQmG9sZglR6GEiBPQoO9hU1mY2Ua9WpI4D/YUhOofx5iQQuVHIIZ9g/zjR3K6RdakjGne3fkn9YXgxRhxApN3q3F0Ae+V\"}"
      }
    ]
  }
}

export const floristics_lite_example = {
  "data": {
    "collections": [
      {
        "plot-layout": {
          "id": 1
        },
        "plot-location": {
          "id": 1
        },
        "plot-visit": {
          "id": 1
        },
        "floristics-veg-survey-lite": {
          "data": {
            "start_date_time": "2022-09-21T01:55:44.186Z",
            "end_date_time": "2022-09-21T01:55:44.186Z",
            "survey_metadata": {
              "survey_details": {
                "survey_model": "floristics-veg-survey-lite",
                "time": "2024-03-04T05:52:28.193Z",
                "uuid": "ea344b36-859a-42a7-bc09-61cc2783e3e2",
                "project_id": "1",
                "protocol_id": "f0e75400-491f-44b3-9034-271f3a858f03",
                "protocol_version": "1"
              },
              "user_details": {
                "role": "collector",
                "email": "testuser@email.com",
                "username": "TestUser"
              },
              "provenance": {
                "version_app": "0.0.1-xxxxx",
                "version_core_documentation": "0.0.0-xxxx",
                "system_app": "Monitor FDCP API--localdev",
                "version_core": "0.0.0-xxxx",
                "system_core": "Monitor FDCP API-development",
                "version_org": "0.0.0-xxxx",
                "system_org": "Monitor FDCP API-development"
              }
            },
            "protocol_variant": "lite"
          }
        },
        "floristics-veg-voucher-lite": [
          {
            "data": {
              "field_name": "Voucher Lite Example 1",
              "voucher_barcode": "dev_foo_barcode_484",
              "growth_form_1": "V",
              "growth_form_2": "X",
              "plant": 1,
              "leaf": 2
            }
          },
          {
            "data": {
              "field_name": "Voucher Lite Example 2",
              "voucher_barcode": "dev_foo_barcode_430",
              "growth_form_1": "Z",
              "growth_form_2": "A",
              "plant": 3,
              "leaf": 4
            }
          },
          {
            "data": {
              "field_name": "Voucher Lite Example 3",
              "voucher_barcode": "dev_foo_barcode_914",
              "growth_form_1": "TP",
              "growth_form_2": "D",
              "plant": 5,
              "leaf": 6
            }
          }
        ],
        "orgMintedIdentifier": "{\"iv\":\"640XaP0yrheZ1aLqvFdsJA==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"DUaexpoBTmU=\",\"ct\":\"auNv0Hk4u5fsAZUwKU3BsxtK1giW1Sr3jdp/3fs0FOhQ+uDA+WKVD4nfLT5I2sDx99LaSBAYIgQe3WvN+3QeUbf7BrKbDFmaB2jgPYylz9UEIRUxRe8tRo7eoPWexCseFzgf3SF7MphKJeRw7XdMkPbdgS/xuceuunxWvRH/sGpQGDIow9t+QsQk6ozNUQQGkFNF0Qd/C4KcpUqAUAeXymXc+MiI7SZNwnIWcC2+itFg1p/+MXVAi6z9yn3JqU+Y56GAF6aL2lBGdMXhkrsO+9D2xhA+xkn5\"}"
      }
    ]
  }
}

export const plant_tissue_vouchering_example = {
  "data": {
    "collections": [
      {
        "plot-layout": {
          "id": 2
        },
        "plot-location": {
          "id": 2
        },
        "plot-visit": {
          "id": 3
        },
        "floristics-veg-genetic-voucher-survey": {
          "data": {
            "start_date_time": "2022-09-14T06:50:09.673Z",
            "end_date_time": "2022-09-14T06:50:09.673Z",
            "survey_metadata": {
              "survey_details": {
                "survey_model": "floristics-veg-genetic-voucher-survey",
                "time": "2024-03-04T05:52:28.193Z",
                "uuid": "ea344b36-859a-42a7-bc09-61cc2783e3e2",
                "project_id": "1",
                "protocol_id": "f0e75400-491f-44b3-9034-271f3a858f03",
                "protocol_version": "1"
              },
              "user_details": {
                "role": "collector",
                "email": "testuser@email.com",
                "username": "TestUser"
              },
              "provenance": {
                "version_app": "0.0.1-xxxxx",
                "version_core_documentation": "0.0.0-xxxx",
                "system_app": "Monitor FDCP API--localdev",
                "version_core": "0.0.0-xxxx",
                "system_core": "Monitor FDCP API-development",
                "version_org": "0.0.0-xxxx",
                "system_org": "Monitor FDCP API-development"
              }
            },
            "protocol_variant": "full"
          }
        },
        "floristics-veg-genetic-voucher": [
          {
            "data": {
              "floristics_voucher_full": 1,
              "min_distance_between_replicates": 3,
              "replicate": [
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_711",
                  "replicate_number": 1
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_746",
                  "replicate_number": 2
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_636",
                  "replicate_number": 3
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_183",
                  "replicate_number": 4
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_592",
                  "replicate_number": 5
                }
              ]
            }
          },
          {
            "data": {
              "floristics_voucher_full": 2,
              "min_distance_between_replicates": 1,
              "replicate": [
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_19",
                  "replicate_number": 1
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_83",
                  "replicate_number": 2
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_98",
                  "replicate_number": 3
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_696",
                  "replicate_number": 4
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_591",
                  "replicate_number": 5
                }
              ]
            }
          },
          {
            "data": {
              "floristics_voucher_full": 3,
              "min_distance_between_replicates": 8,
              "replicate": [
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_258",
                  "replicate_number": 1
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_814",
                  "replicate_number": 2
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_33",
                  "replicate_number": 3
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_570",
                  "replicate_number": 4
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_189",
                  "replicate_number": 5
                }
              ]
            }
          }
        ],
        "orgMintedIdentifier": "{\"iv\":\"5+MnoGhJF4F+dP9/2xlaqA==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"ggj0yWUFq6Q=\",\"ct\":\"LBqNSCplZ69Rd6jRkwAiCCJh1Z8CmDrvjwgFNPq5CW3R3agnhzHO4qODzJv7GdOr1mY37aOngnDcDaVtxEGbExrLvR4+aPBsfQHDx6zeYpevLe8jV7INnfs/iN8iixPqtKKCfg/UDQakNzTig7UF/W3sTKnANrUf7VMtI6zWyLfsXtDwOEymKIysENJQ/Irsa/LYzPE3lGfQtUZIz/FfzbMrX0j6M0rqOBtTmoQETMf6+Ch0USoJl2+ab7teaC9xqnqlae37s0cuacLvmRbmY+klBh/5I4YXJrzw9UB2X/nkbyk=\"}"
      },
      {
        "plot-layout": {
          "id": 1
        },
        "plot-location": {
          "id": 1
        },
        "plot-visit": {
          "id": 1
        },
        "floristics-veg-genetic-voucher-survey": {
          "data": {
            "start_date_time": "2022-09-14T07:08:35.448Z",
            "end_date_time": "2022-09-14T07:08:35.448Z",
            "survey_metadata": {
              "survey_details": {
                "survey_model": "floristics-veg-genetic-voucher-survey",
                "time": "2024-03-04T05:52:28.193Z",
                "uuid": "ea344b36-859a-42a7-bc09-61cc2783e3e2",
                "project_id": "1",
                "protocol_id": "f0e75400-491f-44b3-9034-271f3a858f03",
                "protocol_version": "1"
              },
              "user_details": {
                "role": "collector",
                "email": "testuser@email.com",
                "username": "TestUser"
              },
              "provenance": {
                "version_app": "0.0.1-xxxxx",
                "version_core_documentation": "0.0.0-xxxx",
                "system_app": "Monitor FDCP API--localdev",
                "version_core": "0.0.0-xxxx",
                "system_core": "Monitor FDCP API-development",
                "version_org": "0.0.0-xxxx",
                "system_org": "Monitor FDCP API-development"
              }
            },
            "protocol_variant": "full"
          }
        },
        "floristics-veg-genetic-voucher": [
          {
            "data": {
              "floristics_voucher_full": 4,
              "min_distance_between_replicates": 3,
              "replicate": [
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_598",
                  "replicate_number": 1
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_368",
                  "replicate_number": 2
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_441",
                  "replicate_number": 3
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_286",
                  "replicate_number": 4
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_856",
                  "replicate_number": 5
                }
              ]
            }
          },
          {
            "data": {
              "floristics_voucher_full": 5,
              "min_distance_between_replicates": 1,
              "replicate": [
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_519",
                  "replicate_number": 1
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_686",
                  "replicate_number": 2
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_527",
                  "replicate_number": 3
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_81",
                  "replicate_number": 4
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_520",
                  "replicate_number": 5
                }
              ]
            }
          },
          {
            "data": {
              "floristics_voucher_full": 6,
              "min_distance_between_replicates": 2,
              "replicate": [
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_543",
                  "replicate_number": 1
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_359",
                  "replicate_number": 2
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_418",
                  "replicate_number": 3
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_300",
                  "replicate_number": 4
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_231",
                  "replicate_number": 5
                }
              ]
            }
          }
        ],
        "orgMintedIdentifier": "{\"iv\":\"AMTC2LnrBUg+sNpaJEBmew==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"jgyh+PIqoUQ=\",\"ct\":\"L4Vtd1z9c9g8kt2zcqzqkTRfTSzsxnZRjcYdkLf4dwQQTrIzZlO0/tvA9Lgvv17BtuFIqAcCj9x4Z8pFFgRI8ATp2MixblB21bDFJ+m8MT8GNZ68UDNH2C+b709ZXv1SqxEFR1iv9PuVpGb7a1wG5pOlCE1VKMiiz6hBW/I8q9NN7njbNa23AVUDcebw/7DK74nZeF0aqvwdsxXgRK27b6YSZHg3yPr/FzZ8dFW+KNwRCBCqEafjR9HS2nIdVabh8xH/eA9uHi0B9Xr2TZUIABDOpzeK8k0Cjp7FezmblXMfQ1U=\"}"
      },
      {
        "plot-layout": {
          "id": 2
        },
        "plot-location": {
          "id": 2
        },
        "plot-visit": {
          "id": 3
        },
        "floristics-veg-genetic-voucher-survey": {
          "data": {
            "start_date_time": "2022-09-14T07:16:03.380Z",
            "end_date_time": "2022-09-14T07:16:03.380Z",
            "survey_metadata": {
              "survey_details": {
                "survey_model": "floristics-veg-genetic-voucher-survey",
                "time": "2024-03-04T05:52:28.193Z",
                "uuid": "ea344b36-859a-42a7-bc09-61cc2783e3e2",
                "project_id": "1",
                "protocol_id": "f0e75400-491f-44b3-9034-271f3a858f03",
                "protocol_version": "1"
              },
              "user_details": {
                "role": "collector",
                "email": "testuser@email.com",
                "username": "TestUser"
              },
              "provenance": {
                "version_app": "0.0.1-xxxxx",
                "version_core_documentation": "0.0.0-xxxx",
                "system_app": "Monitor FDCP API--localdev",
                "version_core": "0.0.0-xxxx",
                "system_core": "Monitor FDCP API-development",
                "version_org": "0.0.0-xxxx",
                "system_org": "Monitor FDCP API-development"
              }
            },
            "protocol_variant": "lite"
          }
        },
        "floristics-veg-genetic-voucher": [
          {
            "data": {
              "floristics_voucher_full": 1,
              "min_distance_between_replicates": 5,
              "replicate": [
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_969",
                  "replicate_number": 1
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_285",
                  "replicate_number": 2
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_924",
                  "replicate_number": 3
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_939",
                  "replicate_number": 4
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_349",
                  "replicate_number": 5
                }
              ]
            }
          },
          {
            "data": {
              "floristics_voucher_full": 2,
              "min_distance_between_replicates": 1,
              "replicate": [
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_586",
                  "replicate_number": 1
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_203",
                  "replicate_number": 2
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_705",
                  "replicate_number": 3
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_215",
                  "replicate_number": 4
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_144",
                  "replicate_number": 5
                }
              ]
            }
          },
          {
            "data": {
              "floristics_voucher_full": 3,
              "min_distance_between_replicates": 4,
              "replicate": [
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_449",
                  "replicate_number": 1
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_630",
                  "replicate_number": 2
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_526",
                  "replicate_number": 3
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_653",
                  "replicate_number": 4
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_257",
                  "replicate_number": 5
                }
              ]
            }
          }
        ],
        "orgMintedIdentifier": "{\"iv\":\"HYuore41qOpS0rdR6Sbo8w==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"jgyh+PIqoUQ=\",\"ct\":\"1SqS12Zh6YJlmJKLjoxpC6vGIEdvg/eRC9hkF3iY4kRpuhTZv7/KsOUXZzVgquPyjpfULfCuR3Zxfhx5oiQZi6vDh5o5IQ0wYy5EX7kdrUl8F9t1EXqIbVUqceGKawk21n/pw5NcWy7KztQRljAA70jsDAz3VL3F7WbA9SY0BU04D0b6GsQlryUwfPsnaOAj5bGGswNSdJHnXtLSPZkTo6A4GoINNnqK4hNNfXl8HDW7l9TnHHkOrc2+Oe+9AxIEEJbAKY5tvzRi5qOo/UbHS+jZ6fRzIF2NOlJsDHTKdjz8xGk=\"}"
      },
      {
        "plot-layout": {
          "id": 1
        },
        "plot-location": {
          "id": 1
        },
        "plot-visit": {
          "id": 1
        },
        "floristics-veg-genetic-voucher-survey": {
          "data": {
            "start_date_time": "2022-09-14T07:19:24.078Z",
            "end_date_time": "2022-09-14T07:19:24.078Z",
            "survey_metadata": {
              "survey_details": {
                "survey_model": "floristics-veg-genetic-voucher-survey",
                "time": "2024-03-04T05:52:28.193Z",
                "uuid": "ea344b36-859a-42a7-bc09-61cc2783e3e2",
                "project_id": "1",
                "protocol_id": "f0e75400-491f-44b3-9034-271f3a858f03",
                "protocol_version": "1"
              },
              "user_details": {
                "role": "collector",
                "email": "testuser@email.com",
                "username": "TestUser"
              },
              "provenance": {
                "version_app": "0.0.1-xxxxx",
                "version_core_documentation": "0.0.0-xxxx",
                "system_app": "Monitor FDCP API--localdev",
                "version_core": "0.0.0-xxxx",
                "system_core": "Monitor FDCP API-development",
                "version_org": "0.0.0-xxxx",
                "system_org": "Monitor FDCP API-development"
              }
            },
            "protocol_variant": "lite"
          }
        },
        "floristics-veg-genetic-voucher": [
          {
            "data": {
              "floristics_voucher_full": 4,
              "min_distance_between_replicates": 6,
              "replicate": [
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_973",
                  "replicate_number": 1
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_550",
                  "replicate_number": 2
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_574",
                  "replicate_number": 3
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_121",
                  "replicate_number": 4
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_880",
                  "replicate_number": 5
                }
              ]
            }
          },
          {
            "data": {
              "floristics_voucher_full": 5,
              "min_distance_between_replicates": 4,
              "replicate": [
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_182",
                  "replicate_number": 1
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_93",
                  "replicate_number": 2
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_822",
                  "replicate_number": 3
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_128",
                  "replicate_number": 4
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_38",
                  "replicate_number": 5
                }
              ]
            }
          },
          {
            "data": {
              "floristics_voucher_full": 6,
              "min_distance_between_replicates": 9,
              "replicate": [
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_998",
                  "replicate_number": 1
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_761",
                  "replicate_number": 2
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_92",
                  "replicate_number": 3
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_40",
                  "replicate_number": 4
                },
                {
                  "genetic_voucher_barcode": "dev_foo_barcode_166",
                  "replicate_number": 5
                }
              ]
            }
          }
        ],
        "orgMintedIdentifier": "{\"iv\":\"jpUTbvv3bx08iW5PLBFBgw==\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"jgyh+PIqoUQ=\",\"ct\":\"L7L1Kmf55o7uEVMQZWV6r0GSMsT/KC1QvYVeFTm5FyxqABoKgPBDXN/scVXUps5BfTxcZJwbhC7tEJGXEnBL6zVIvMJZYk77DlKk6RsgGe7UuUczWpNxs6n+OdfMQyu5RtsescONKnZ8PTVO+FNZysVK3MiwS3trosnDylTLeoD1+5SVa2F32hl8UQzKMt9c+ZCf8vo+XBtW7Btdgx7lpv0EtTZjGUjU4Rv/iaayhB/PyHJFrYFk2R2APSZSTXSoidVIWVmhcYu+ut0OGmK+npCCN2IJ35XKqvi+cmOVVUyHNq0=\"}"
      }
    ]
  }
}
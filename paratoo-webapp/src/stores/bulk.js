import { defineStore } from 'pinia'
import { useApiModelsStore } from './apiModels'
import { useAuthStore } from './auth'
import { useDocumentationStore } from './documentation'
import { useDataManagerStore } from 'src/stores/dataManager'
import { binary } from 'src/misc/binary'

import {
  generateSurveyId,
  modelNameToSchemaName,
  notifyHandler,
  paratooErrorHandler,
  paratooWarnMessage,
  paratooWarnHandler,
  paratooSentryBreadcrumbMessage,
  prettyFormatFieldName,
  prettyFormatDateTime,
  schemaNameToModelName,
  interceptFireFields,
  resetUniqueCollections,
  updateDexieFieldNames,
  updateBody,
  paratooDebugMessage,
  makeModelConfig,
  findValForKey,
  unreservePlotName,
  flattenPlotDataId,
} from 'src/misc/helpers'
import { doCoreApiPost } from 'src/misc/api'
import * as cc from 'src/misc/constants'
import axios from 'axios'
import { cloneDeep, isEmpty } from 'lodash'
import * as mime from 'mime-types'
import { modelsToPopulate } from '../misc/apiLists'
import {
  v4 as uuidv4,
} from 'uuid'


function initialState() {
  return {
    collections: {},
  }
}

export const useBulkStore = defineStore('bulk', {
  state: () => ({
    //if we don't write to the store at some point early in app startup, it is not added
    //to local storage and might result in weird behaviour when trying to access the
    //store (particularly in Cypress that directly grabs from local storage)
    init: false,
    pointNumber: 1,
    time: 0,
    intervalId: null,
    timerInterval: null,
    grassyEndObs: false,
    collections: {},
    staticPlotContext: {
      plotLayout: null,
      plotVisit: null,
    },
    autoSavedCollections: {},
    doingSubmoduleMeta: [],
    tmpPTVData: {},
    uniqueCollections: {},
    plotSelectionRelevantProjects: [],
    newProjectAreas: [],
    currentBarcodes: [],
    temporaryIndexId: null,
    layoutReferencePoint: null,
    debugClientStateDumpUrl: null,
    clientStateUuid: null,
    ajvErrors: {},
    sentStateUids: []
  }),

  persist: true,

  actions: {
    clearClientStateDump() {
      this.debugClientStateDumpUrl= null
      this.clientStateUuid= null
    },
    setCurrentBarcodes(barcodes) {
      if (!barcodes) {
        this.currentBarcodes = []
        return
      }
      if (Array.isArray(barcodes)) {
        this.currentBarcodes = barcodes
        return
      }
      this.currentBarcodes.push(barcodes)
      console.log(this.currentBarcodes)
      return barcodes
    },
    setTemporaryIndexId(index) {
      this.temporaryIndexId = index
    },
    //should set at least the following keys: parentModuleProtocolId, submoduleProtocolId
    /**
     *
     * @param {Number} parentModuleProtocolId
     * @param {Number} submoduleProtocolId
     * @param {String} protocolVariant protocol variant of both floristics and plant tissue voucher
     * @param {Array} submoduleWorkFlow workflows of the submodule that will be displayed in summary
     */
    setDoingSubmodule(
      parentModuleProtocolId,
      submoduleProtocolId,
      protocolVariant = null,
      submoduleWorkFlow = [],
    ) {
      const authStore = useAuthStore()
      const apiModelsStore = useApiModelsStore()

      const protocolInformation = apiModelsStore.protocolInformation({ protId: submoduleProtocolId })
      if (
        //edge case for cover+fire - the 'submodule' is hidden (as it's just metadata),
        //and the need to enforce assignment is not needed as the hidden 'cover+fire'
        //protocol is effectively a superset of the parent and submodule protocols
        
        protocolInformation?.isHidden ||
        //generally check if submodule not part of selected project
        authStore.selectedProject?.protocols.some(
          (p) => p.id === submoduleProtocolId,
        )
      ) {
        const isExist = this.doingSubmoduleMeta?.some(
          (element) =>
            element.parentModuleProtocolId === parentModuleProtocolId,
        )
        if (!isExist) {
          const data = {
            parentModuleProtocolId,
            submoduleProtocolId,
            protocolVariant,
            submoduleWorkFlow,
            submoduleProtocolInfo: protocolInformation,
          }
          this.doingSubmoduleMeta.push(data)
        }
      } else {
        const submoduleAssignedToAnyProject =
          authStore.protocols.includes(submoduleProtocolId)
        paratooWarnMessage(
          `Tried to set submodule metadata for parent module ID=${parentModuleProtocolId} and submodule ID=${submoduleProtocolId}. The submodule isn't assigned to this project ID=${
            authStore.currentContext.project
          }${
            submoduleAssignedToAnyProject
              ? ' (but it is assigned to a different project)'
              : ''
          } so ignoring`,
        )
      }
    },
    getSubModuleID(parentId) {
      return this.doingSubmoduleMeta.find(
        (element) => element.parentModuleProtocolId === parentId,
      )?.submoduleProtocolId
    },
    getSubModuleVariant(parentId) {
      return this.doingSubmoduleMeta.find(
        (element) => element.parentModuleProtocolId === parentId,
      )?.protocolVariant
    },
    getSubModuleWorkFlow(parentId) {
      return this.doingSubmoduleMeta.find(
        (element) => element.parentModuleProtocolId === parentId,
      ).submoduleWorkFlow
    },
    getSubModuleProtocolInfo(parentId) {
      return this.doingSubmoduleMeta.find(
        (element) => element.parentModuleProtocolId === parentId,
      )?.submoduleProtocolInfo
    },
    removeSubmodule(parentModuleProtocolId) {
      this.doingSubmoduleMeta = this.doingSubmoduleMeta.filter(
        (element) => element.parentModuleProtocolId !== parentModuleProtocolId,
      )
    },
    /**
     * Adds a project area to the list of new (in-progress) project areas. If it's
     * already been added, overwrite the array entry
     *
     * @param {Object} projectArea the project area object with key `project` with object
     * as value also containing a key `value` and value of the project ID
     */
    addNewProjectArea(projectArea) {
      const authStore = useAuthStore()
      let returnFlag = false
      for (const [index, value] of this.newProjectAreas.entries()) {
        if (value && value.project.value === projectArea.project.value) {
          //editing one of the new project areas
          this.newProjectAreas[index] = projectArea
          returnFlag = true
        }
      }
      //update the auth store so that everything gets access to the new area
      authStore.updateProjArea({
        projectAreaAndInfo: projectArea,
      })
      if (returnFlag) return

      //didn't return, so a completely new project area to add
      this.newProjectAreas.push(projectArea)
    },
    removeProjectArea(projectId) {
      this.newProjectAreas = this.newProjectAreas.filter(
        (o) => o.project.project.id !== projectId,
      )

      //refreshing projects will reset to what we have in the backend; the auth store
      //will then update these projects with any project areas in `newProjectAreas` and
      //in the queue
      useAuthStore().refreshProjectsAndProtocols()
    },
    /**
     * Gets all (in-progress) project areas
     *
     * @returns {Array.<Object>} list of project areas, or empty array
     */
    getNewProjectAreas() {
      return this.newProjectAreas
    },
    /**
     * Gets a particular (in-progress) project area object
     *
     * @param {Number} projectId the project ID
     *
     * @returns {Object} the project area object
     */
    getNewProjectArea(projectId) {
      return this.newProjectAreas.find((o) => o.project.value === projectId)
    },
    /**
     * Checks if the (in-progress) project areas contain at least 3 points
     *
     * @returns {Array.<Object> | Boolean} the list of invalid projects, or `true`
     */
    projectAreasAreValid() {
      const invalidProjects = []
      for (const projectArea of this.getNewProjectAreas()) {
        if (projectArea.points.length < 3) {
          invalidProjects.push(projectArea.project.label)
        }
      }

      if (invalidProjects.length > 0) {
        return invalidProjects
      }
      return true
    },
    /**
     * Clear related project areas and selected projects of plot selection
     */
    clearPlotSelectionTmpData() {
      this.plotSelectionRelevantProjects = []
      this.newProjectAreas = []
    },
    setPlotSelectionRelevantProjects(projects) {
      this.plotSelectionRelevantProjects = projects
    },
    /**
     *
     * @param {*} uniqueId of the floristics voucher
     * @param {*} clearAll
     */
    clearTmpPTVData(uniqueId, clearAll = false) {
      if (clearAll) {
        this.tmpPTVData = []
        return
      }
      delete this.tmpPTVData[uniqueId]
    },
    /**
     *  check if the current protocol are collected with a submodule
     * @param {*} parentId protocolId of the main one
     * @returns
     */
    doingSubmodule(parentId) {
      if (!parentId || !this.doingSubmoduleMeta) return false
      return this.doingSubmoduleMeta.some(
        (element) => element.parentModuleProtocolId === parentId,
      )
    },
    /**
     * Set the Static Context
     * @param {*} value
     */
    setStaticPlotLocation(location) {
      this.staticPlotContext.plotLocation = location
    },
    setStaticPlotLayout(layout) {
      this.staticPlotContext.plotLayout = layout
    },
    setStaticPlotVisit(visit) {
      this.staticPlotContext.plotVisit = visit
    },
    /**
     * Delete the specified observaton and shift the array.
     * @param {*} protocolId
     * @param {string} modelName
     * @param {int} observationIndex
     */
    deleteObservation({ protocolId, modelName, observationIndex }) {
      this.collections[protocolId][modelName].splice(observationIndex, 1)
    },
    /**
     * Delete specific model from a protocol.
     * @param {*} protocolId
     * @param {string} modelName
     */
    deleteCollectionModel({ protocolId, modelName }) {
      delete this.collections[protocolId][modelName]
    },
    /**
     * Clears unfinished collection for a specific protocol only
     * @param {Number} protocolId
     * @param {Boolean} [unreservePlotSelections] whether to unreserve plots
     */
    async clearCollection({ protocolId , unreservePlotSelections = false}) {
      const protocolInfo = useApiModelsStore().protocolInformation({
        protId: protocolId,
      })
      if (protocolInfo?.identifier === 'a9cb9e38-690f-41c9-8151-06108caf539d') {
        if (unreservePlotSelections) {
          //clearing plot selection, so need to un-reserve plot names
          for (const plot of this.collections?.[protocolId]?.['plot-selection'] || []) {
            await unreservePlotName({
              plotLabel: plot.plot_label,
              projects: plot.projects_for_plot.map(
                p => p.project.id,
              ),
            })
          }
        }
        
        this.clearPlotSelectionTmpData()
      }

      delete this.collections[protocolId]
      delete this.autoSavedCollections[protocolId]
      delete this.ajvErrors[protocolId]

      if (protocolInfo?.identifier === cc.layoutAndVisitUuid) {
        this.layoutReferencePoint = null
      }
      if (protocolInfo?.identifier === 'a9cb9e38-690f-41c9-8151-06108caf539d') {
        this.clearPlotSelectionTmpData()
        //want to refresh projects to 'reset' any updated project areas
        //(`addNewProjectArea()` does an update, but when we clear a collection we need
        //to reverse this)
        useAuthStore().refreshProjectsAndProtocols()
      }
    },
    /**
     * Clears current  staticPlotContext collection
     */
    clearStaticPlotContext() {
      for (let item in this.staticPlotContext) {
        this.staticPlotContext[item] = null
      }
    },
    /**
     * Adds a new model entry e.g. weather-survey, to the most recent collection
     * @param {Object}
     */
    addGeneralModelEntry({ modelName, content, protocolId }) {
      let newState = { ...this.collections[protocolId] }

      newState[modelName] = content
      this.collections[protocolId] = newState
    },
    /**
     * Use to add main observation that have multiple entries
     * @param {*} modelName
     * @param {*} protocolId
     * @param {*} content
     */
    addObservation(modelName, protocolId, content) {
      if (!this.collections[protocolId][modelName]) {
        this.collections[protocolId][modelName] = []
      }
      this.collections[protocolId][modelName].push(content)
    },
    /**
     * Updates the existing model entry e.g. bird-survey-observations, adds a
     * new array element if specified.
     * @param {*} param1
     */
    addGeneralCollectionInstance({
      modelName,
      content,
      protocolId,
      collectionSurveyIndex = null,
    }) {
      // if there are no previous instances, ensure the collection array exists
      if (!this.collections[protocolId]) this.collections[protocolId] = {}
      if (!Array.isArray(this.collections[protocolId][modelName])) {
        this.collections[protocolId][modelName] = [content]
      } else {
        const useIndex = Number.isInteger(collectionSurveyIndex)
          ? collectionSurveyIndex
          : this.collections[protocolId][modelName].length

        // add to current
        this.collections[protocolId][modelName][useIndex] = content
      }
    },

    updateStoreProtocol({ protocolId, value }) {
      this.collections[protocolId] = value
    },

    addWeatherSurvey(body) {
      //FIXME we now wrap collections in their protocol ID - workflow doesn't use this
      //but anything that does will be busted
      this.collections[this.collections.length - 1]['weather-survey'] = body
    },
    /**
     * Removes all collections from the store
     */
    resetCollections() {
      this.collections = {}
    },
    /**
     * Removes one collection from the store, specified with a protocolId
     *
     * @param {*} options
     * @param {Number} options.protocolId The protocol number of the collection to remove
     */
    resetOneCollection({ protocolId }) {
      delete this.collections[protocolId]
    },
    reset() {
      const s = initialState()
      Object.keys(s).forEach((key) => {
        this[key] = s[key]
      })
    },
    /**
     * Dev helper
     *
     * @param {Object} example object containing collections array for the example, which
     * is obtained from the documentation
     * @param {Number} currentProtocol the ID of the protocol to add the example to
     */
    addExampleToStore({ example, currentProtocol }) {
      this.collections[currentProtocol] = example.collections[0]
    },

    /**
     * Receives the main bulk survey which specifically requires a SurveyId*
     *
     * @param {String} modelName the model of the protocol to update
     * @param {*} content the new content
     * @param {Number} protocolId the ID of the protocol to update
     * @param {Object} documentation the documentation object
     * @param {'full' | 'lite'} [protocolVariant] the protocol variant if applicable
     * @param {Boolean} [splitSurveyStep] if the survey step will be split if applicable
     */
    recvMainSurvey({
      modelName,
      content,
      protocolId,
      documentation,
      protocolVariant,
      splitSurveyStep,
    }) {
      // pass a generated surveyID because it's not included with user input
      const existingContent = this.collectionGetForProt({ protId: protocolId })[
        modelName
      ]

      //always regenerate the survey metadata, as we might need to update certain
      //information such as the project ID, but keep the UUID as the binary store uses
      //this and if it changes we'll pollute the binary store with stale files
      content.survey_metadata = generateSurveyId(modelName, documentation)
      if (existingContent?.survey_metadata?.survey_details?.uuid) {
        paratooDebugMessage(`Existing survey uuid in the survey metadata: ${existingContent.survey_metadata.survey_details.uuid}. Will maintain for binary store logic`)
        content.survey_metadata.survey_details.uuid = existingContent.survey_metadata.survey_details.uuid
      }

      //FIXME if we've already added an end_date_time it will get removed when we save the 'start' step - maybe doesn't need to be done as if we save the 'start' step again, it might mean the parameters of the survey has changed and therefore we can force the 'end' step to be done again
      if (splitSurveyStep) {
        //if splitting survey step, default behaviour will squash start datetime when
        //adding end datetime, so merge the records together to preserve both
        if (content.date_time && existingContent) {
          // for start survey, have do this otherwise deleted data will be overwritten by existing record
          content.end_date_time = existingContent.end_date_time
        } else if (content.end_date_time) {
          // for end survey, as it only emit end date time so we want to keep existing record
          content = {
            ...existingContent,
            ...content,
          }
        }
      }

      if (protocolVariant) {
        content.protocol_variant = protocolVariant
      }

      this.addGeneralModelEntry({
        modelName,
        content,
        protocolId,
      })
      const submoduleId = this.getSubModuleID(protocolId)
      if (submoduleId) {
        const existingSubmoduleContent = this.collectionGetForProt({ protId: submoduleId })
        const submoduleProtocolInfo = this.getSubModuleProtocolInfo(protocolId)

        let submoduleSurveyModel =  null
        if(submoduleProtocolInfo){
          for (const workflowItem of submoduleProtocolInfo?.workflow) {
            const schema = makeModelConfig(
              workflowItem.modelName,
              documentation,
              submoduleId,
            )

            if (schema.mainSurveyFlag) {
              submoduleSurveyModel = schema.rowDataKey
            }
          }
        }

        const submoduleVariant = this.getSubModuleVariant(protocolId)

        if (submoduleSurveyModel && submoduleVariant) {
          const updatedContent = existingSubmoduleContent?.[submoduleSurveyModel] || {}
        
          for (const dateTime of ['start_date_time', 'end_date_time']) {
            //squash the submodule start/end datetime for the parent module's
            if (content?.[dateTime]) {
              updatedContent[dateTime] = content[dateTime]
            }
          }

          this.recvMainSurvey({
            modelName: submoduleSurveyModel, 
            content: updatedContent,
            protocolId: submoduleId,
            documentation,
            protocolVariant: submoduleVariant,
            splitSurveyStep,
          })
        } else {
          paratooWarnMessage(`Something went wrong updating the submodule survey data for parent module model '${modelName}'. submoduleSurveyModel=${submoduleSurveyModel}. submoduleVariant=${submoduleVariant}`)
        }

        
      }
    },
    /**
     * Updates the plot context for a specific protocol, if that protocol is in-progress
     * 
     * @param {Number} protocolId the protocol ID to update context
     * @param {Object} updatedContext the context to update contain one or both of keys
     * `plot-layout` and `plot-visit`
     */
    updateCollectionPlotContext({
      protocolId,
      updatedContext,
    }) {
      if (
        !updatedContext?.['plot-layout'] &&
        !updatedContext?.['plot-visit']
      ) {
        paratooWarnMessage(
          `Cannot update plot context for protocol with ID=${protocolId} as no context to update was provided`
        )
        return
      }
      if (this.collections[protocolId]) {
        for (const plotType of ['layout', 'visit']) {
          if (updatedContext?.[`plot-${plotType}`]) {
            paratooSentryBreadcrumbMessage(
              `Updating ${plotType} context to: ${JSON.stringify(updatedContext[`plot-${plotType}`])}`,
              'updateCollectionPlotContext',
            )
            if (!this.collections[protocolId]?.[`plot-${plotType}`]) {
              Object.assign(this.collections[protocolId], {
                [`plot-${plotType}`]: null,
              })
            }
            this.collections[protocolId][`plot-${plotType}`] = updatedContext[`plot-${plotType}`]
          }
        }
      } else {
        paratooWarnMessage(`Unable to update plot context for protocol with ID=${protocolId} as there is no in-progress collection`)
        return
      }
    },
    updateCollectionPlotReference({
      protocolId,
      modelName,
      index,
      fieldNameForRelation,
      resolvedLayout,
    }) {
      paratooSentryBreadcrumbMessage(
        `Updating protocol ${protocolId} model ${modelName} at index=${index} and field name=${fieldNameForRelation} with resolved layout: ${JSON.stringify(resolvedLayout)}`,
      )

      if (this.collections[protocolId]) {
        if (this.collections[protocolId]?.[modelName]?.[index]) {
          Object.assign(this.collections[protocolId][modelName][index], {
            [fieldNameForRelation]: resolvedLayout.id,
          })
        } else {
          paratooWarnMessage(`Unable to update protocol ${protocolId} model ${modelName} at index=${index} as there is no data at that model and/or index`)
        }
      } else {
        paratooWarnMessage(`Unable to update protocol ${protocolId} layout relation as that protocol is not in-progress`)
      }
    },
    /**
     *
     * @param {*} currentProtocol is protocolId
     * @param {*} documentation
     * @returns
     */
    pendingCollectionSummary({ currentProtocol, documentation, overrideData = null, plotDefinition = false }) {      
      let collectionForProtocol = overrideData ? overrideData : this.collections[currentProtocol]

      // add fire into cover in profile
      if (collectionForProtocol?.['cover-point-intercept-point'] && collectionForProtocol?.['fire-point-intercept-point']) {
        collectionForProtocol['cover-point-intercept-point']['fire-point-intercept-point'] =
          collectionForProtocol['fire-point-intercept-point'];
      }
      
      let tablesArrays = []
      //loop over each step of collection workflow
      if (!collectionForProtocol) return tablesArrays
      for (const [workflowStep, workflowStepData] of Object.entries(
        collectionForProtocol,
      )) {
        if (workflowStep === 'orgMintedIdentifier') continue //user doesn't need to see
        // dont want to show this to the user
        if (workflowStep === 'plot-definition-survey' && !plotDefinition) continue //skipped data
        if (workflowStep === 'plot-fauna-layout') continue
        if (workflowStep === 'cover-point-intercept-point' 
        ) {
          //point intercept has a lot of data associated with it, so we ignore it.
          //if we let this proceed for PI, we would have at least 1000 tables (one for
          //each point), so we make a more compacted summary
          tablesArrays.push({
            'Cover Point Intercept': this.calculateSubsVegPercentage({
              currentProtocol: currentProtocol,
              workflowStepData,
              // ...(overrideData && { overrideData }) // only add overrideData if it is passed in
            }),
          })

          const doingSubmodule = this.doingSubmodule(currentProtocol)
          if (doingSubmodule || overrideData) {
            let fireCharData = null
            if (doingSubmodule) {
              fireCharData = this.collections?.[this.getSubModuleID(currentProtocol)]?.['fire-char-observation']
            } else if (overrideData) {
              fireCharData = overrideData?.['fire-char-observation']
            } else {
              paratooWarnMessage('Got to catch-all block when trying to make summary for submodule')
              fireCharData = []
            }

            if(fireCharData){
              const newTables = this.makeSummary({
                modelName: 'fire-char-observation',
                value: fireCharData,
                documentation,
                overrideDataPassed: overrideData !== null
              })
              for (const table of newTables) {
                tablesArrays.push(table)
              } 
            }
          }
        } else {
          // Re-added the wait, as we need to wait for the promise made by `this.makeSummary`
          // Reason why it was removed to begin with was 'solar sonar' trigger, implying it was bad code
          // Alternative solution can be looked into.
          const newTables = this.makeSummary({
            modelName: workflowStep,
            value: workflowStepData,
            documentation,
            overrideDataPassed: overrideData !== null
          })

          for (const table of newTables) {
            tablesArrays.push(table)
          }
        }
      }
      // add submodule to summary
      if (this.doingSubmodule(currentProtocol)) {
        const apiModelsStore = useApiModelsStore()

        const submoduleId = this.getSubModuleID(currentProtocol)
        const submoduleProtocolEntry = apiModelsStore.protocolInformation({
          protId: submoduleId,
        })
        const submoduleWorkFlow = this.getSubModuleWorkFlow(currentProtocol)

        //don't want to generate dummy/duplicate data for hidden (placeholder) submodules
        if (
          !submoduleProtocolEntry.isHidden &&
          submoduleProtocolEntry.isWritable
        ) {
          for (const workflow of submoduleWorkFlow) {
            if (
              this.collections[submoduleId] &&
              this.collections[submoduleId][workflow.modelName]
            ) {
              const submoduleValue =
                this.collections[submoduleId][workflow.modelName]
              const submoduleTable = this.makeSummary({
                modelName: workflow.modelName,
                value: submoduleValue,
                documentation,
                overrideDataPassed: overrideData !== null
              })
              tablesArrays = tablesArrays.concat(submoduleTable)
            }
          }
        }
      }

      return tablesArrays
    },

    /**
     * Makes data required for summary table(s)
     *
     * @param {*} modelName Associated in the documentation under components/schemas/[modelName]
     * @param {*} value Currently stored value
     * @param {*} documentation
     *
     * @returns {Array} Each element is intended to be in a separate table
     */
    makeSummary({ modelName, value, documentation, overrideDataPassed = false }) {
      const documentationStore = useDocumentationStore()
      const apiModelsStore = useApiModelsStore()
      const dataManager = useDataManagerStore()
      const newSchemaName = documentationStore.formattedNewSchema(
        { modelName },
        { root: true },
      )
      const documentationProperties = documentation.components.schemas[newSchemaName]
          .properties.data.properties
      let tablesArrays = []
      let instances = Array.isArray(value) ? value : [value]
      //for each item in the workflow step, create an object that will be readable by
      //a q-table
      for (const [key, ob] of Object.entries(instances)) {
        var stepArr = []

        if (!ob) continue
        // make new table for each observation
        for (const [attributeName, attributeData] of Object.entries(ob)) {
          let counter = 1
          for (const iterator of addSummaryAttribute(
            documentationProperties,
            attributeName,
            attributeData,
            documentation,
            overrideDataPassed,
          )) {
            const ext = ['png','jpg','jpeg','gif','svg','webp', 'mp4', 'mp3','mpeg','mpga']            
            if (
              Object.values(iterator).includes('id') &&
              (modelName === 'plot-visit' ||
                modelName === 'plot-location' ||
                modelName === 'plot-layout')
            ) {              
              const cachedModel = dataManager.cachedModel({
                modelName: modelName,
              })
              const expanded = cachedModel.find((o) => {
                if (
                  !o?.id &&
                  o?.value?.temp_offline_id &&
                  iterator?.value?.temp_offline_id
                ) {
                  return (
                    o.value.temp_offline_id === iterator.value.temp_offline_id
                  )
                } else if (
                  !o?.id &&
                  o?.temp_offline_id &&
                  iterator?.value?.temp_offline_id
                ) {
                  return o.temp_offline_id === iterator.value.temp_offline_id
                } else {
                  return o.id === iterator.value
                }
              })
              switch (modelName) {
                case 'plot-visit':
                  stepArr.push({
                    field: modelName,
                    value: prettyFormatDateTime(expanded?.start_date),
                  })
                  break
                case 'plot-layout':
                  stepArr.push({
                    field: 'Plot Label',
                    value: (() => {
                      if (expanded?.plot_selection?.plot_label) {
                        return expanded.plot_selection.plot_label
                      } else if (Number.isInteger(expanded?.plot_selection)) {
                        //likely an ID of the Plot Selection, so need to resolve (we can
                        //assume it was collected online, so don't use
                        //`dataManager.cachedModel()`)
                        const relevantPlotSelection = apiModelsStore
                          .cachedModel({ modelName: 'plot-selection' })
                          .find((o) => o.id === expanded.plot_selection)
                        return relevantPlotSelection.plot_label
                      }
                    })(),
                  })

                  break
              }
            }
            // edge case for projects for plot selection
            // as they are not in the schema
            else if (iterator.field === 'projects_for_plot') {
              const row = {
                field: `Selected Projects ${counter}`,
                value: iterator.value.label,
              }
              stepArr.push(row)
            }
            // they are not in the schema
            else if (iterator.field === 'crown_damage_values') {
              const row = {
                field: `Crown Damage Values`,
                value: null,
              }
              stepArr.push(row)

              Object.keys(iterator.value).forEach((key) => {
                stepArr.push({
                  field: key,
                  value: iterator.value[key],
                })
              })
            }
            // edge case for floristics lite, we want to display voucher field name
            // instead of ID
            //FIXME PTV submodule is hitting this case so the below todo will prob fix
            // TODO: should be derived from backend, maybe a flag for masking this field
            else if (
              iterator.field === 'voucher_full' ||
              iterator.field === 'voucher_lite'
            ) {
              const variant = iterator.field.split('_').at(-1)
              const modelName = `floristics-veg-voucher-${variant}`
              const relatedVoucher = apiModelsStore
                .cachedModel({ modelName })
                .find(({ id }) => {
                  let idForCompare = null
                  if (Number.isInteger(iterator.value)) {
                    idForCompare = iterator.value
                  }
                  return id === idForCompare
                })

              stepArr.push({
                field: iterator.field,
                value: relatedVoucher?.field_name,
              })
            } else if (
              typeof iterator.value === 'object' &&
              iterator.value !== null
            ) {              
              if (Object.values(iterator.value).length === 1){
                iterator.value = Object.values(iterator.value)[0]
              } 
              if (!iterator.value.mimeType && !iterator.value.ext ) {
                // do not format photos
                let value = iterator.value
                // do not format date: start/end_date_time otherwise will break normal summary display
                if (!(value instanceof Date)) {
                  iterator.value = this.makeContentHumanReadable(value)
                }
              }   
              if (iterator.value && iterator.value.ext && !ext.some(extension => iterator.value?.ext?.includes(extension))) {
                iterator.value = `File with name '${
                  iterator.value?.name
                }' (type ${iterator.value?.mime })`
              }
              
              stepArr.push(iterator)

            } else {
              if(typeof iterator.value === 'string' && iterator.value.includes('{')){
                // handle photopoints barcode content in profile
                const obj = JSON.parse(iterator.value);
                const formatData = (obj) => {
                  return Object.entries(obj)
                    .map(([key, value]) => {
                      // Replace underscores with spaces and format the key
                      const formattedKey = key
                        .replace(/_/g, ' ') // Replace underscores with spaces
                        .replace(/\b(\w)/g, match => match.toUpperCase()); // Capitalize the first letter of each word

                      // Return the formatted key and value
                      return `${formattedKey}: ${value}`;
                    })
                    .join('\n'); // Join the formatted key-value pairs into a single string
                };
                iterator.value = formatData(obj)
              }
              stepArr.push(iterator)
            }
            counter++
          }
        }

        tablesArrays.push({
          // If there are multiple instances, include an id in the header
          [prettyFormatFieldName(
            value.length > 1
              ? `${modelName} ${Number.parseInt(key) + 1}`
              : modelName,
          )]: stepArr,
        })
      }
      return tablesArrays
    },
    
    /**
     * handle nested objects to human readable content in Summary
     * @param {*} value 
     * @param {*} iterator iterator.value
     */
    makeContentHumanReadable(value) { 
      const textKey = Object.keys(value).find(key => key.endsWith('_text'))
      const lutKey = Object.keys(value).find(key => key.endsWith('_lut'))
      
      if(value[textKey]){
        return `${value[lutKey]?.symbol} (${value[textKey]})`
      }
      if(lutKey && !value[lutKey]){
        return `${value[lutKey]?.symbol} (${value[lutKey]?.label})`
      }
      for (const key in value) {
        if(Object.prototype.hasOwnProperty.call(value, key)) {
          const res = value[key]
          if(res && typeof res === 'object'){
            const result = this.makeContentHumanReadable(res)
            if(result){
              return result
            }
          } else if(key==='symbol' || key === 'label'){
            return `${value.symbol} (${value.label})`
          } else {
            
            if(key != 'id' && res!= null){
              const string = Object.entries(value)
                .filter(([k, v]) => k != 'id' && v != null)
                .map(([k, v]) =>
                  typeof v === 'object'
                    ? this.makeContentHumanReadable(v)
                    : `${k}: ${v}`,
                )
                .join('\n')
                return string.includes('_')? string.replace(/_/g, ' ') : string
            }
          }
        }
      }
    },
    /**
     * Appends a collection field to the most recent collection in the state
     * @param {*} modelName
     * @param {*} content
     * @param {*} protocolId
     */
    async recvGeneral({ modelName, content, protocolId }) {
      await this.addGeneralModelEntry({ modelName, content, protocolId })
    },
    async recvGeneralCollection({
      modelName,
      content,
      protocolId,
      collectionSurveyIndex,
    }) {
      collectionSurveyIndex = parseInt(collectionSurveyIndex)

      let newValue = cloneDeep(this.collections[protocolId]) || {}

      // ensure each layer exists before trying to reach next
      newValue[modelName] = newValue[modelName] || []
      newValue[modelName][collectionSurveyIndex] =
        newValue[modelName][collectionSurveyIndex] || {}

      // Normal observation insert
      newValue[modelName][collectionSurveyIndex] = content

      this.updateStoreProtocol({
        protocolId,
        value: newValue,
      })
    },
    async createOrgMintedIdentifier(
      survey_metadata,
      offlinePublicationQueueIndex = null,
      protocolId
    ) {
      const authStore = useAuthStore()

      const protUUID = useApiModelsStore().protUuidFromId(protocolId).uuid
      const {
        protocol_id: protIdFromSurveyMetaData,
        submodule_protocol_id: subProtIdFromSurveyMetaData,
      } = survey_metadata.survey_details

      const checkUUIDMismatch = (uuid, expectedId, errorMsg) => {
        if (uuid !== expectedId) {
          throw paratooErrorHandler(errorMsg, new Error(errorMsg))
        }
      }

      if (subProtIdFromSurveyMetaData) {
        checkUUIDMismatch(
          protUUID,
          subProtIdFromSurveyMetaData,
          `Protocol UUID ${protUUID} does not match sub protocol ID in survey metadata ${subProtIdFromSurveyMetaData}`,
        )
      } else if (protUUID !== protIdFromSurveyMetaData) {
        checkUUIDMismatch(
          protUUID,
          protIdFromSurveyMetaData,
          `Protocol UUID ${protUUID} does not match Protocol ID in survey metadata ${protIdFromSurveyMetaData}`,
        )
      }
      const resp = await authStore
        .mintIdentifier(
          {
            survey_metadata: survey_metadata,
            offlinePublicationQueueIndex: offlinePublicationQueueIndex,
          },
          { root: true },
        )
        .catch((err) => {
          let msg = 'Failed to mint Org Identifier'
          notifyHandler('negative', msg, err)
          throw paratooErrorHandler(msg, err)
        })
      if (!resp) return null

      return resp.orgMintedIdentifier
    },
    /**
     * TODO update JSDoc
     *
     * Uploads the first collection from the Vuex store's `collection` array.
     * Decide if it is bulk or not by the number of fields within (>1 = bulk)
     *
     * Note: collections are stored in an array as we will potentially be adding
     * functionality to upload multiple collections at once (a feature that Core supports)
     *
     * @typedef {Object} ActionParams
     * @property {String} modelName
     * @property {Boolean} isBulk (true by default)
     * @property {Number} protocolId
     *
     * @param {VuexContext} param0 object with the Vuex context
     * @param {ActionParams} param1 object with the params needed to perform an upload:
     * modelName, isBulk, protocolId
     */
    async upload({
      modelName,
      isBulk = true,
      protocolId,
      isMany = false,
      addSurveyID = false,
      offlinePublicationQueueIndex = null,
      retryMintOrgIdentifier = false,
    }) {
      paratooSentryBreadcrumbMessage(
        `bulk upload() called with modelName=${modelName}, isBulk=${isBulk}, protocolId=${protocolId}, isMany=${isMany}, addSurveyID=${addSurveyID}, offlinePublicationQueueIndex=${offlinePublicationQueueIndex}`,
        'bulkUpload',
      )
      if (modelName === 'plot-location') {
        isBulk = false
      }
      //the `isBulk` is passed to this function as it set by checking if the workflow has
      //multiple steps, but sometimes we still use bulk when only having one step
      //TODO should probably have this in `submitCollection` helper where we actually 'detect' if it's bulkable
      const bulkable = ['metadata-collection', 'grassy-weeds-survey']
      if (bulkable.includes(modelName)) {
        isBulk = true
      }
      let collection = this.collections[protocolId]
      const dataManager = useDataManagerStore()
      if (offlinePublicationQueueIndex) {
        collection = dataManager.queuedCollectionFromIndex({
          index: offlinePublicationQueueIndex,
        })
      }

      paratooSentryBreadcrumbMessage(
        `upload will proceed with collection data: ${JSON.stringify(collection, null, 2)}`,
        'bulkUpload',
      )

      const apiModelsStore = useApiModelsStore()
      const protocolInfo = apiModelsStore.protocolInformation({
        protId: protocolId,
      })

      if (isEmpty(protocolInfo)) {
        paratooWarnMessage(`Could not get protocol information for protocol with ID=${protocolId}. There may be issues handing edge cases for Plot Selection 'compatibility mode'.`)
      }

      //track which apiModels we'll refresh at the end, so that any submit data can
      //be immediately reused, e.g., new plot locations
      const modelsToRefresh = []
      let corePostRes
      if (
        isBulk ||
        (
          //we're in Plot Selection /many, which is the old approach and thus we're in
          //compatibly mode and will need to mint an identifier here
          protocolInfo?.identifier === 'a9cb9e38-690f-41c9-8151-06108caf539d' &&
          isMany
        )
      ) {
        if (
          !collection?.orgMintedIdentifier ||
          (
            collection?.orgMintedIdentifier &&
            retryMintOrgIdentifier
          )
        ) {
          if (
            protocolInfo?.identifier === 'a9cb9e38-690f-41c9-8151-06108caf539d' &&
            isMany
          ) {
            paratooWarnMessage(
              `Plot Selection is in compatibly mode, and no orgMintedIdentifier found. Will force minting identifier.`,
            )

            if (!collection[modelName]?.survey_metadata) {
              paratooWarnMessage(
                `Plot Selection is in compatibility mode but no survey_metadata to create orgMintedIdentifier, so will create it`,
              )
              const documentationStore = useDocumentationStore()
              const doc = await documentationStore.getDocumentation()
              Object.assign(collection[modelName], {
                survey_metadata: generateSurveyId(modelName, doc),
              })
            }
          }
          
          const orgMintedIdentifier = await this.createOrgMintedIdentifier(
            collection[modelName].survey_metadata,
            offlinePublicationQueueIndex,
            protocolId
          )
          if (!orgMintedIdentifier) {
            let err = new Error('Org returned ok status but no org minted identifier')
            let msg = `Failed to create survey metadata. ${err.message}`
            
            notifyHandler(
              'negative',
              msg,
            )
            //throw the error rather than calling `paratooErrorHandler` as
            //`handleSubmission` catches and handles, but won't call notifyHandler, as it
            //expects that to be done in the data manager
            throw err
          }

          //since `collection` is a reference to the queue, this will be assigned to the
          //item in the queue
          Object.assign(collection, {
            orgMintedIdentifier: orgMintedIdentifier,
          })

          if (
            offlinePublicationQueueIndex &&
            !collection?.orgMintedIdentifier
          ) {
            //if we still somehow don't have an orgMintedIdentifier in the queue, force
            //it in anyway
            paratooDebugMessage(`Adding orgMintedIdentifier to collection in queue index=${offlinePublicationQueueIndex}: ${orgMintedIdentifier}`)
            dataManager.updateCollectionOrgMintedIdentifier({
              collectionIndex: offlinePublicationQueueIndex,
              orgMintedIdentifier: orgMintedIdentifier,
            })
          }
        } else {
          paratooDebugMessage('Collection already has orgMintedIdentifier. Will keep that one instead of requesting a new one.')
        }
      }

      const documentationStore = useDocumentationStore()
      // finds the endpoint name based on the model (from the documentation), if it's a bulk, append bulk to the end
      const endpoint = await documentationStore.findEndpointFromModel(modelName, { root: true })
      let endpointName = `${
        cc.coreApiPrefix
      }${endpoint}${
        isBulk ? '/bulk' : ''
      }${isMany ? '/many' : ''}`

      const fullDocumentation = await documentationStore.getDocumentation(
        null,
        { root: true },
      )

      //looks for Strapi components, using the results to convert fields to objects
      //(single) or array of objects (repeatable)
      const strapiComponentProperties =
        documentationStore.strapiComponentProperties({ protocolId: protocolId })

      /*
       * For non-bulk submissions
       */
      if (Object.keys(collection).length > 0 && !isBulk) {
        let body = collection[modelName]

        const authStore = useAuthStore()
        body = !Array.isArray(body) ? [body] : body
        // if /many is enabled
        if (isMany) {
          body = {
            [`${modelName}s`]: body.map((value) => ({
              data: value,
            })),
          }
          if (
            protocolInfo?.identifier === 'a9cb9e38-690f-41c9-8151-06108caf539d'
          ) {
            if (collection?.['project-areas']) {
              paratooWarnMessage(
                `Compatibility mode for plot selection enabled. Will keep project areas in collection data: ${JSON.stringify(collection['project-areas'])}`,
              )
              Object.assign(body, {
                'project-areas': collection['project-areas'],
              })
            }
            if (endpointName.endsWith('/plot-selection-surveys/many')) {
              const endpointPostfixOverride = '/plot-selections/many'
              paratooWarnMessage(
                `Plot Selection is in compatibility mode but is trying to submit to the wrong endpoint '${endpointName}'. Will update to '${endpointPostfixOverride}'`,
              )
              const manyEndpoint = await documentationStore.findEndpointFromModel('plot-selection', { root: true })
              endpointName = `${
                cc.coreApiPrefix
              }${manyEndpoint}/many`
              paratooSentryBreadcrumbMessage(
                `Updated endpoint to: ${endpointName}`,
                'bulkUpload',
              )
            }

            if (
              endpointName.endsWith('/many') &&
              collection?.['plot-selection'] &&
              !collection?.['plot-selections']
            ) {
              paratooWarnMessage(
                `Plot Selection is in compatibility mode and has put the data in 'plot-selection' rather than 'plot-selections'. Will move data to 'plot-selections' and wrap each in a 'data' object so that /many call will no be rejected by validator`,
              )

              Object.assign(body, {
                'plot-selections': cloneDeep(
                  collection['plot-selection'].map(p => ({ data: p })),
                ),
              })
              delete body['plot-selection']
            }

            if (
              !body?.orgMintedIdentifier &&
              collection?.orgMintedIdentifier
            ) {
              body.orgMintedIdentifier = collection.orgMintedIdentifier
            }
          }
        }

        // sometimes we send some extra info or modify existing body
        // e.g. plot-selection with project related info
        body = await updateBody(
          modelName,
          body,
          fullDocumentation,
          isMany,
          addSurveyID,
          protocolId
        )

        paratooSentryBreadcrumbMessage(
          `non-bulk body: ${JSON.stringify(body, null, 2)}`,
          'bulkUpload',
        )

        corePostRes = await this.uploadMultipleBody(
          modelName,
          body,
          authStore.token,
          protocolId,
          endpointName,
        )
      } else if (isBulk) {
        /*
         * For bulk submissions
         */
        let bodyToSend = {}
        bodyToSend.collections = [collection]
        // sometimes we need to update bulk body as well
        // e.g. before submission we should update interventions(bulk) ids again
        bodyToSend.collections = await updateBody(
          modelName,
          bodyToSend.collections,
          fullDocumentation,
          addSurveyID,
        )
        // upload and resolve files separately, since they are too big for a single POST
        for (const [key, value] of Object.entries(bodyToSend.collections)) {
          //don't want to catch the error that this method throws, as we want `upload` to
          //fail and its caller to catch/handle the error instead - i.e., the dataManager
          //will end up with the error to then format/display to the user
          bodyToSend.collections[key] = await removeFilesFromBulkRequest(
            value,
          )
        }
        //look for collection fields with files for protocols such as floristics and opportune

        //resolve any floristics vouchers to their IDs before submission
        // TODO, Can we handle this in the custom component, before it adds to the store?
        try {
          for (const key in bodyToSend.collections[0]) {
            var floristicsVoucherVariant
            if (
              Object.keys(bodyToSend.collections[0][key]).includes(
                `floristics_voucher_${floristicsVoucherVariant}`,
              )
            ) {
              floristicsVoucherVariant = apiModelsStore.voucherVariant({
                protId: protocolId,
              })
            }
            if (Array.isArray(bodyToSend.collections[0][key])) {
              for (
                var itemIndex = 0;
                itemIndex < bodyToSend.collections[0][key].length;
                itemIndex++
              ) {
                const itemAtCurrIndex =
                  bodyToSend.collections[0][key][itemIndex]
                floristicsVoucherVariant = apiModelsStore.voucherVariant({
                  protId: protocolId,
                })
                if (
                  Object.keys(itemAtCurrIndex).includes(
                    `floristics_voucher_${floristicsVoucherVariant}`,
                  )
                ) {
                  itemAtCurrIndex[
                    `floristics_voucher_${floristicsVoucherVariant}`
                  ] = apiModelsStore.voucherFromFieldName({
                    fieldName:
                      itemAtCurrIndex[
                        `floristics_voucher_${floristicsVoucherVariant}`
                      ],
                    protocolVariant: floristicsVoucherVariant,
                  }).id
                }
              }
            }
          }
        } catch {
          paratooWarnMessage(
            "Tried to resolve floristics vouchers but didn't find any",
          )
        }
        if (strapiComponentProperties !== undefined) {
          bodyToSend.collections[0] = handleStrapiComponent(
            bodyToSend.collections[0],
            strapiComponentProperties,
          )
        }
        // convert body to Strapi4 format
        bodyToSend = { data: bodyToSend }
        for (const [modelName, payload] of Object.entries(
          bodyToSend.data.collections[0],
        )) {
          switch (modelName) {
            case 'orgMintedIdentifier':
              continue
            case 'plot-location':
            case 'plot-layout':
            case 'plot-visit':
              if (payload.id) continue
              break
            default:
              break
          }

          bodyToSend.data.collections[0][modelName] = strapi4Wrap(
            bodyToSend.data.collections[0][modelName],
            modelNameToSchemaName(modelName),
            fullDocumentation.components.schemas,
          )
        }
        // add user role for authentication in some cases
        const { roleType } = useAuthStore()
        bodyToSend.role = roleType
        paratooSentryBreadcrumbMessage(
          `bulk body: ${JSON.stringify(bodyToSend, null, 2)}`,
          'bulkUpload',
        )
        corePostRes = await doCoreApiPost(
          {
            urlSuffix: endpointName,
            body: bodyToSend,
            offlinePublicationQueueIndex:offlinePublicationQueueIndex
          },
          { root: true },
        )
      }
      //track the apiModels to refresh - all relevant workflow models and child obs
      const currProtWorkflow = apiModelsStore.protocolInformation({
        protId: protocolId,
      }).workflow
      for (const workflowItem of currProtWorkflow) {
        modelsToRefresh.push(workflowItem.modelName)

        const currSchema =
          fullDocumentation.components.schemas[
            modelNameToSchemaName(workflowItem.modelName)
          ]
        if (workflowItem.newInstanceForRelationOnAttributes) {
          //these are relation field names, so need to resolve to model names
          for (const relationFieldName of workflowItem.newInstanceForRelationOnAttributes) {
            const relationSchema =
              currSchema.properties.data.properties[relationFieldName]
            if (relationSchema.type === 'integer') {
              //single child ob
              modelsToRefresh.push(relationSchema['x-model-ref'])
            } else if (relationSchema.type === 'array') {
              //multi child ob
              modelsToRefresh.push(relationSchema.items['x-model-ref'])
            }
          }
        }
      }
      // reset unique collections after submission
      modelsToRefresh.forEach(async (modelName) => {
        resetUniqueCollections(modelName)
        // refresh filed names of dexieDB if modelNames have field name
        // e.g. newly created field names assigned in vegetation mapping
        //   should be available in floristics
        if (modelsToPopulate.modelsWithFieldNames.includes(modelName))
          await updateDexieFieldNames(modelName, protocolId)
      })
      //indicate to the User that the populated item is 'pending upload'
      return corePostRes
    },
    /**
     * upload multiple body ex. opportunistics observation
     */
    async uploadMultipleBody(
      modelName,
      body,
      authToken,
      protocolId,
      endpointName,
    ) {
      if (!body) {
        notifyHandler('negative', 'Faild to upload, body is empty')
        return
      }

      let responses = []
      body = !Array.isArray(body) ? [body] : body

      const modelsToRefresh = []
      const documentationStore = useDocumentationStore()
      const fullDocumentation = await documentationStore.getDocumentation(
        null,
        { root: true },
      )
      const strapiComponentProperties =
        documentationStore.strapiComponentProperties({ protocolId: protocolId })

      // handle multiple entries
      for (const [index, entry] of Object.entries(body)) {
        // replace files with IDs of uploaded files
        body[index] = await removeFilesFromModel(
          modelName,
          entry,
          fullDocumentation,
          authToken,
        )
        if (strapiComponentProperties !== undefined) {
          body[index] = handleStrapiComponent(
            { [modelName]: body[index] },
            strapiComponentProperties,
          )[modelName]
        }
        const { roleType } = useAuthStore()
        let resp = await doCoreApiPost(
          {
            urlSuffix: endpointName,
            body: {
              data: body[index],
              role: roleType,
            },
          },
          { root: true },
        )
        responses.push(resp)
      }

      try {
        //track the apiModels to refresh
        //the keys of a non-bulk body are its fields and are not models. But if we're
        //  submitting opportune, then it will contain plot-location which we should
        //  track
        for (const key of Object.keys(body[0])) {
          //field name is snake case but model name is kebab case
          if (key === 'plot_location') modelsToRefresh.push('plot-location')
        }
        modelsToRefresh.push(modelName)
        // TODO: see if you can push modelName generically
        if (modelName === 'plot-visit') modelsToRefresh.push(modelName)
      } catch (error) {
        paratooErrorHandler('Failed to track apiModels',error)
      }

      // reset unique collections after submission
      modelsToRefresh.forEach((modelName) => {
        resetUniqueCollections(modelName)
      })

      return responses
    },
    //TODO update JSDoc
    clearCollections({ protocolId, offlinePublicationQueueIndex }) {
      if (!offlinePublicationQueueIndex) {
        this.resetOneCollection({ protocolId })
      }
    },
    /**
     * Removes everything in store/bulk/collections and other collection metadata
     */
    deleteUnsentSubmissions() {
      this.resetCollections()
      this.doingSubmoduleMeta = []
      this.plotSelectionRelevantProjects = []
      this.newProjectAreas = []
    },
    updateLayoutReferencePoint(locationObj) {
      this.layoutReferencePoint = locationObj
    },
  },
  getters: {
    /**
     * Creates a list of protocols whose visit's depend on the visit context
     * 
     * @returns {Array.<Number>} a list of protocol IDs
     */
    inProgressProtocolsReferVisitContext() {
      const contextVisitData = useDataManagerStore().getPlotDataFromContext({
        modelName: 'plot-visit',
      })
      const flattenedContextVisitId = flattenPlotDataId(contextVisitData)
      const protocolsUsingVisit = []
      
      for (const [protocolId, collectionData] of Object.entries(this.collectionGet)) {

        if (collectionData?.['plot-visit']) {
          const flattenedCollectionVisitId = flattenPlotDataId(collectionData?.['plot-visit'])

          if (flattenedCollectionVisitId === flattenedContextVisitId) {
            protocolsUsingVisit.push(Number.parseInt(protocolId))
          }
        }
      }
      return protocolsUsingVisit
    },
    getTemporaryIndexId(state) {
      return state.temporaryIndexId
    },
    getCurrentBarcodes(state) {
      return state.currentBarcodes
    },
    getPlotLayoutId(state) {
      if (state.staticPlotContext.plotLayout?.id) {
        return state.staticPlotContext.plotLayout.id
      }
      return state.staticPlotContext.plotLayout?.temp_offline_id
    },
    getPlotVisitId(state) {
      if (state.staticPlotContext.plotVisit?.id) {
        return state.staticPlotContext.plotVisit.id
      }
      return state.staticPlotContext.plotVisit?.temp_offline_id
    },
    getStaticPlotContext(state) {
      return state.staticPlotContext
    },
    collectionGet(state) {
      return state.collections
    },
    collectionGetForProt: (state) => {
      return ({ protId }) => {
        return state.collections[protId] || {}
      }
    },
    getPlotSelectionRelevantProjects: (state) => {
      return state.plotSelectionRelevantProjects
    },
    geneticVouchersForFloristicsVoucher:
      (state) =>
      ({ protId, protocolVariant, floristicsVoucher }) => {
        try {
          return state.collections[protId][
            'floristics-veg-genetic-voucher'
          ].find(
            (v) =>
              v[`floristics_voucher_${protocolVariant}`] === floristicsVoucher,
          )
        } catch {
          return null
        }
      },
    /**
     * Gets all in-progress collections have overlap with the provided project that might
     * have been collected with another project
     * 
     * @param {Number} projectId the project to check for overlaps against it
     * @param {Boolean} [tableData] whether to return the data for use in a q-table
     * 
     * @returns {Object | Array.<Object>} an object where the key is the project ID and
     * the value is an array of protocol IDs; or, if tableData is true, an array of
     * objects
     */
    openCollectionsForProject: (state) => {
      return ({ projectId, tableData = false }) => {
        const authStore = useAuthStore()
        const apiModelsStore = useApiModelsStore()
        const protocolForProject = authStore.projects.find(
          p => p.id == projectId
        )?.protocols
        if (protocolForProject && protocolForProject.length > 0) {
          const collectionsForProject = Object.entries(
            state.collections,
          ).reduce((accum, [currProtId, val]) => {
            const currProtSurveyMetadata = findValForKey(val, 'survey_metadata')

            const currCollectionProject =
              currProtSurveyMetadata?.survey_details?.project_id
            if (
              protocolForProject.some((p) => p.id == currProtId) &&
              currCollectionProject &&
              //don't care if the provided project, only other projects
              currCollectionProject != projectId
            ) {
              if (!accum[currCollectionProject]) {
                accum[currCollectionProject] = []
              }
              accum[currCollectionProject].push(currProtId)
            }

            return accum
          }, {})

          if (tableData) {
            const protocols = apiModelsStore.cachedModel({
              modelName: 'protocol',
            })
            return Object.entries(collectionsForProject).map(
              ([projId, protocolIds]) => {
                const resolvedProject = authStore.projects.find(
                  (p) => p.id == projId,
                )
                const resolvedProtocols = protocolIds.map(
                  (p) => protocols.find((prot) => prot.id == p)?.name,
                )
                return {
                  project: resolvedProject.name,
                  protocols: resolvedProtocols.join(', '),
                }
              },
            )
          }
          return collectionsForProject
        } else {
          notifyHandler(
            'warning',
            `Detected no protocols assigned to the project ${
              authStore.getProjectNameFromId(projectId)?.name
            }.`,
          )
          return
        }
      }
    },
    /**
     * Creates the data for a summary table of completed transects
     *
     * TODO reduce code duplication, particularly between the 3 main cases
     *
     * @param {Number} currentProtocol the protocol ID
     * @param {Array} completedTransects list of completed transects
     * @returns
     */
    completeTransects(state) {
      return ({ currentProtocol, completedTransects }) => {
        const apiModelsStore = useApiModelsStore()
        const protocolVariant = apiModelsStore.protocolVariant({
          protocolId: currentProtocol,
        })

        const fireProtocolId = this.getSubModuleID(currentProtocol)
        const fireCollection = state.collections[fireProtocolId]

        function voucherVariant(point) {
          const pointKeys = Object.keys(point)
          if (pointKeys.includes('floristics_voucher_full') && point.floristics_voucher_full !== null) return 'full'
          if (pointKeys.includes('floristics_voucher_lite') && point.floristics_voucher_lite !== null) return 'lite'
        }

        //get all existing points and filter the array to only include points from
        //transects that are completed
        let points =
          state.collections[currentProtocol]['cover-point-intercept-point']
        if (completedTransects) {
          points = points.filter((p) =>
            completedTransects.some(
              (o) => o.value === p.cover_transect_start_point,
            ),
          )
        }
        var pointsTableData = []
        for (const [pointIndex, point] of points.entries()) {
          //look over points we've already pushed to see if the current point's transect
          //is already tracked
          var searchedPointForTransect = pointsTableData.find(
            (p) => p['Transect'] === point.cover_transect_start_point,
          )
          var currSubstrates = []
          var currSpecies = []

          //CASE: there's no entries for any transect, so add the current one
          if (pointsTableData.length === 0) {
            currSubstrates.push(point.substrate)
            point.species_intercepts.forEach((s, index) => {
              const currInterceptFireFields = interceptFireFields(
                fireCollection,
                pointIndex,
                index,
              )
              //check with `includes` so we don't push duplicates
              if (protocolVariant !== 'lite') {
                if (currInterceptFireFields !== null) {
                  //use the fire growth form instead of a species (as the species
                  //cannot be identified, the growth form is all that can be collected)
                  const fireGrowthForm = `Unidentifiable plant with growth form: ${apiModelsStore.resolvedLutSymbol(
                    {
                      symbol: currInterceptFireFields.fire_growth_form,
                      modelName: 'lut-veg-growth-form',
                    },
                  )}`
                  if (!currSpecies.includes(fireGrowthForm)) {
                    currSpecies.push(fireGrowthForm)
                  }

                  //return here (i.e., `continue` the `forEach` loop) as attempting to
                  //resolve voucher stuff will cause errors when there area fire fields
                  //(and thus vouchers may not be present) - we could make the voucher
                  //case `else if` but then there will be multiple unnecessary calls to
                  //`voucherVariant`
                  return
                }

                const currVoucherVariant = voucherVariant(s)
                if (
                  !currSpecies.includes(
                    //we add voucher ID's to the store, so we need to look up that ID to get
                    //the voucher field name
                    voucherIdToFieldName(
                      s[`floristics_voucher_${currVoucherVariant}`],
                      currVoucherVariant,
                    ),
                  )
                ) {
                  currSpecies.push(
                    voucherIdToFieldName(
                      s[`floristics_voucher_${currVoucherVariant}`],
                      currVoucherVariant,
                    ),
                  )
                }
              }
              //cover lite case
              if (
                protocolVariant === 'lite' &&
                !currSpecies.includes(s.fractional_cover)
              ) {
                currSpecies.push(s.fractional_cover)
              }
            })
            pointsTableData.push({
              Transect: point.cover_transect_start_point,
              Substrates: currSubstrates,
              Species: currSpecies,
            })
          }
          //CASE: there's already an entry for this transect, so add to it
          else if (
            searchedPointForTransect !== undefined &&
            searchedPointForTransect['Transect'] ===
              point.cover_transect_start_point
          ) {
            currSubstrates.push(point.substrate)
            //search for index to append to
            const indexToAddTo = pointsTableData.findIndex((p) => {
              return p['Transect'] == point.cover_transect_start_point
            })
            //handle substrates. don't push duplicates
            if (
              !pointsTableData[indexToAddTo]['Substrates'].includes(
                point.substrate,
              )
            ) {
              pointsTableData[indexToAddTo]['Substrates'].push(point.substrate)
            }
            //handle species
            point.species_intercepts.forEach((s, index) => {
              const currInterceptFireFields = interceptFireFields(
                fireCollection,
                pointIndex,
                index,
              )
              //check with `includes` so we don't push duplicates
              if (protocolVariant !== 'lite') {
                if (currInterceptFireFields !== null) {
                  //use the fire growth form instead of a species (as the species
                  //cannot be identified, the growth form is all that can be collected)
                  const fireGrowthForm = `Unidentifiable plant with growth form: ${apiModelsStore.resolvedLutSymbol(
                    {
                      symbol: currInterceptFireFields.fire_growth_form,
                      modelName: 'lut-veg-growth-form',
                    },
                  )}`
                  if (
                    !pointsTableData[indexToAddTo]['Species'].includes(
                      fireGrowthForm,
                    )
                  ) {
                    pointsTableData[indexToAddTo]['Species'].push(
                      fireGrowthForm,
                    )
                  }

                  return
                }

                const currVoucherVariant = voucherVariant(s)
                if (
                  !pointsTableData[indexToAddTo]['Species'].includes(
                    //we add voucher ID's to the store, so we need to look up that ID to get
                    //the voucher field name
                    voucherIdToFieldName(
                      s[`floristics_voucher_${currVoucherVariant}`],
                      currVoucherVariant,
                    ),
                  )
                ) {
                  pointsTableData[indexToAddTo]['Species'].push(
                    voucherIdToFieldName(
                      s[`floristics_voucher_${currVoucherVariant}`],
                      currVoucherVariant,
                    ),
                  )
                }
              }
              //cover lite case
              if (
                protocolVariant === 'lite' &&
                !pointsTableData[indexToAddTo]['Species'].includes(
                  s.fractional_cover,
                )
              ) {
                pointsTableData[indexToAddTo]['Species'].push(
                  s.fractional_cover,
                )
              }
            })
          }
          //CASE: there's no entry for this transect, so add it
          else {
            currSubstrates.push(point.substrate)
            point.species_intercepts.forEach((s, index) => {
              const currInterceptFireFields = interceptFireFields(
                fireCollection,
                pointIndex,
                index,
              )
              //check with `includes` so we don't push duplicates
              if (protocolVariant !== 'lite') {
                if (currInterceptFireFields !== null) {
                  //use the fire growth form instead of a species (as the species
                  //cannot be identified, the growth form is all that can be collected)
                  const fireGrowthForm = `Unidentifiable plant with growth form: ${apiModelsStore.resolvedLutSymbol(
                    {
                      symbol: currInterceptFireFields.fire_growth_form,
                      modelName: 'lut-veg-growth-form',
                    },
                  )}`
                  if (!currSpecies.includes(fireGrowthForm)) {
                    currSpecies.push(fireGrowthForm)
                  }

                  return
                }

                const currVoucherVariant = voucherVariant(s)
                if (
                  !currSpecies.includes(
                    //we add voucher ID's to the store, so we need to look up that ID to get
                    //the voucher field name
                    voucherIdToFieldName(
                      s[`floristics_voucher_${currVoucherVariant}`],
                      currVoucherVariant,
                    ),
                  )
                ) {
                  currSpecies.push(
                    voucherIdToFieldName(
                      s[`floristics_voucher_${currVoucherVariant}`],
                      currVoucherVariant,
                    ),
                  )
                }
              }
              //cover lite case
              if (
                protocolVariant === 'lite' &&
                !currSpecies.includes(s.fractional_cover)
              ) {
                currSpecies.push(s.fractional_cover)
              }
            })
            pointsTableData.push({
              Transect: point.cover_transect_start_point,
              Substrates: currSubstrates,
              Species: currSpecies,
            })
          }
        }

        //format substrate and species arrays to be strings, else the table will display
        //each item in array with no spaces
        for (var point of pointsTableData) {
          for (var pointData in point) {
            let newArr = []
            //convert LUT symbols to labels (not field names, as those are created
            //by the User during vouchering and can be somewhat arbitrary)
            if (pointData === 'Substrates' && point[pointData].length > 0) {
              point[pointData].forEach((lutSymbol) => {
                newArr.push(
                  apiModelsStore.resolvedLutSymbol({
                    symbol: lutSymbol,
                    modelName: 'lut-substrate',
                  }),
                )
              })
            } else if (pointData === 'Transect') {
              newArr.push(
                apiModelsStore.resolvedLutSymbol({
                  symbol: point[pointData],
                  modelName: 'lut-cover-transect-start-point',
                }),
              )
            } else {
              //no LUT symbol to resolve
              newArr.push(point[pointData])
            }
            //make array string
            point[pointData] = newArr.toString()
          }
        }
        return pointsTableData
      }
    },
    calculateSubsVegPercentage(state) {
      return ({ currentProtocol, workflowStepData } ) => {
        const apiModelsStore = useApiModelsStore()
        const protocolVariant = apiModelsStore.protocolVariant({
          protocolId: currentProtocol,
        })
        //get all existing points and filter the array to only include points from
        //transects that are completed
        let points = workflowStepData ? workflowStepData : state.collections[currentProtocol]['cover-point-intercept-point']
        
        // let points =  state.collections[currentProtocol]['cover-point-intercept-point']
        let substrateOccurence = {}
        let speciesOccurence = {}
  
        function voucherVariant(point) {
          const pointKeys = Object.keys(point)
          const typeMap = {
            'floristics_voucher_full': 'full',
            'floristics_voucher_lite': 'lite'
          };
          
          for (const key in typeMap) {
            if (pointKeys.includes(key) &&  point[key] !== null) {
              return typeMap[key];
            }
          }
        }
        if (Array.isArray(points)) {
          for (const point of points) {
            const fireProtocolId = this.getSubModuleID(currentProtocol)
            // TODO: need to get fire points of cover + fire in profile summary, but now they are divided into two expansion items
            const firePoints = state.collections?.[fireProtocolId]?.['fire-point-intercept-point'] || workflowStepData['fire-point-intercept-point']
            //convert LUT symbols to labels (not field names, as those are created
            //by the User during vouchering and can be somewhat arbitrary
            const substrateSymboltoName = (lutSymbol) => {
              if ((typeof lutSymbol === 'object' && lutSymbol !== null && 'symbol' in lutSymbol && 'label' in lutSymbol) ||(lutSymbol.includes('symbol') && lutSymbol.includes('label')) ) {
                // useAttributeValue = `${attrVal.symbol} (${attrVal.label})`
                return apiModelsStore.resolvedLutSymbol({
                  symbol: lutSymbol.symbol,
                  modelName: 'lut-substrate',
                })
              }
             
              return apiModelsStore.resolvedLutSymbol({
                symbol: lutSymbol,
                modelName: 'lut-substrate',
              })
            }
  
            // count number of substrate
            let substrateName = substrateSymboltoName(point.substrate)
            if (!(substrateName in substrateOccurence)) {
              substrateOccurence[`${substrateName}`] = 1
            } else substrateOccurence[`${substrateName}`]++
            // count number of species    
            if(point.species_intercepts){
              for (const [index, species] of Object.entries(
                point?.species_intercepts,
              )) {
                const currVoucherVariant = voucherVariant(species)
                let objProp
                let currSpeciesUnidentifiable = false
                if (firePoints?.length) {
                  const currFirePoint = firePoints.find(
                    (p) => state.collections?.[fireProtocolId]?.['fire-point-intercept-point'] ? 
                      p.point_number === point.point_number &&
                      p.transect_start_point === point.cover_transect_start_point : p.point_number === point.point_number && p.transect_start_point.symbol === point.cover_transect_start_point.symbol
                  )

                  const currPointFireIntercept =
                    //the indexes for the cover species intercepts and fire species
                    //intercepts should line up with each other
                    currFirePoint?.fire_species_intercepts?.[index] || currFirePoint?.species_intercepts?.[index]
                    
                  if (currPointFireIntercept?.plant_unidentifiable) {
                    const fireGrowthFormLabel = apiModelsStore.resolvedLutSymbol({
                      symbol:  currPointFireIntercept.fire_growth_form?.symbol || currPointFireIntercept.fire_growth_form,
                      modelName: 'lut-veg-growth-form',
                    })
                    objProp = `Plant Unidentifiable - ${fireGrowthFormLabel}`
                    //a species is not identifiable, so there will be no voucher selected
                    currSpeciesUnidentifiable = true
                  }
                }
                // for full protocol (no fire)
                if (protocolVariant === 'full' && !currSpeciesUnidentifiable) {
                  objProp = voucherIdToFieldName(
                    species[`floristics_voucher_${currVoucherVariant}`],
                    currVoucherVariant,
                  )
                  
                }
    
                // for lite protocol (no fire)
                else if (protocolVariant === 'lite' && !currSpeciesUnidentifiable) {                  
                  objProp = typeof species.fractional_cover === 'object' ? `${species.fractional_cover.symbol}` : `${species.fractional_cover}`
                }
                if(objProp){ // temporary fix for undefined species (due to cover located at the bottom on the page and fire is in the next page)
                  // count number of species
                  if (!(objProp in speciesOccurence)) {
                    speciesOccurence[objProp] = {
                      count: 1,
                      totalHeight: species.height
                    }
                  } else {
                     speciesOccurence[objProp].count++
                     speciesOccurence[objProp].totalHeight += species.height
                    }
                }
              }
            }
          }
        }
        // turn to count to percentage and calculate avg height
        function toTableData(object, isSpecies) {
          let result = {}
          if (!isSpecies) {
            const sumValue = Object.values(object).reduce((a, b) => a + b, 0)
            for (let i in object) {
              result[i] = (object[i] * 100) / sumValue
            }
          } else {
            let sumValue = 0
            for (let i in object) {
              sumValue += object[i].count
            }
            for (let i in object) {
              result[i] = {}
              result[i].percentage = (object[i].count * 100) / sumValue
              result[i].avgHeight = object[i].totalHeight / object[i].count
              result[i].count = object[i].count
            }
          }
          return result
        }
        const Substrate = toTableData(substrateOccurence, false)
        const Species = toTableData(speciesOccurence, true)
        return { Substrate, Species }
      }
    },
    getTmpPTVData: (state) => {
      return ({ uniqueId }) => {
        return state.tmpPTVData[uniqueId]
      }
    },
  },
})

/*
 * Helpers for bulk.js
 */

/**
 *
 *
 * @param {*} documentationProperties Associated Schema properties for this section of the collection
 * @param {*} attributeName
 * @param {*} attributeValue
 * @param {*} documentation
 *
 * @returns An array containing the summary row(s) to add
 */
function addSummaryAttribute(
  documentationProperties,
  attributeName,
  attributeValue,
  documentation,
  overrideDataPassed = false
) {
  const apiModelsStore = useApiModelsStore()
  const documentationStore = useDocumentationStore()
  let stepRows = []

  const attributeValues = Array.isArray(attributeValue)
    ? attributeValue
    : [attributeValue]
    
  const fieldsToSkip = [
    'survey_metadata', 
    'orgMintedIdentifier', 
    'protocol_variant', 
    'trapsLocations', 
    'markedPoints', 
    'survey',
    'surveyId'
  ]
  for (const attrVal of attributeValues) {
    if(fieldsToSkip.includes(attributeName)) continue
    if (!attrVal) continue // sometimes no attrVal is undefined that creates exception
    
    let useAttributeValue = attrVal
    let schemaForAttribute = documentationProperties[attributeName]?.items
      ? documentationProperties[attributeName].items
      : documentationProperties[attributeName]    
    if (!schemaForAttribute) {
      // for nested strapi component
      if (documentationProperties?.data?.properties[attributeName]?.items) {
        schemaForAttribute =
          documentationProperties?.data?.properties[attributeName]?.items
          console.log(attributeName, schemaForAttribute)
      } else {
        // for nested attribute
        schemaForAttribute = documentationProperties?.data?.properties[attributeName]
      }
      if(!schemaForAttribute) {
        stepRows.push({ field: attributeName, value: attrVal })
        continue
      }
    }
    // Lookup documentation for LUT reference
    if (schemaForAttribute?.['x-lut-ref']) {
      if (typeof attrVal === 'string') {
        useAttributeValue = `${attrVal} (${apiModelsStore.resolvedLutSymbol({
          symbol: attrVal,
          modelName: schemaForAttribute?.['x-lut-ref'],
        })})`
      } else if (
        typeof attrVal === 'object'
      ) {
        const keys = Object.keys(attrVal)
        if (keys.includes('symbol') && keys.includes('label')) {
          useAttributeValue = `${attrVal.symbol} (${attrVal.label})`
        }
      }
    }

    // resolve collection relation in summary
    if (schemaForAttribute.type === 'integer' && schemaForAttribute['x-model-ref']) {
      const resolvedAttributeValue = resolveCollectionRelationInSummary(
        attributeName,
        attributeValue,
        schemaForAttribute['x-model-ref'])       
        
        if (resolvedAttributeValue) {
        stepRows.push({ field: attributeName, value: resolvedAttributeValue })
        continue
      } else {
        if( overrideDataPassed && attributeValue.id && !attributeValue.mime){
          if(attributeName === 'plot_visit' && attributeValue.visit_field_name){
            stepRows.push({ field: attributeName, value: attributeValue.start_date + ' ' + attributeValue.visit_field_name })
          }else{
            stepRows.push({ field: attributeName, value: attributeValue.id })
          }
          continue
        }
      }
    }
    

    // Add any sub-models (non strapi component objects)
    // these would all be referenced with a x-model-ref (since $ref would only be
    // in bulk requestBody)
    let subSchemaName
    subSchemaName = schemaForAttribute['x-model-ref']
    if (schemaForAttribute.items) {
      subSchemaName = schemaForAttribute.items['x-model-ref']
    }

    if (
      subSchemaName &&
      subSchemaName !== 'file' &&
      //don't attempt to keep formatting primitive types - fix was applied for plant
      //tissue vouchering displaying the respective floristics voucher's field name
      //incorrectly (Object.entries() on a string)
      !(typeof attrVal === 'string' || typeof attrVal === 'number')
    ) {
      stepRows.push({ field: prettyFormatFieldName(subSchemaName) })

      const newSchemaName = documentationStore.formattedNewSchema(
        { modelName: subSchemaName },
        { root: true },
      )
      const documentationSection = documentation.components.schemas[
        newSchemaName
      ].items
        ? documentation.components.schemas[newSchemaName].items.properties
        : documentation.components.schemas[newSchemaName].properties

      for (const [subAttrName, subAttrVal] of Object.entries(attrVal)) {
        for (const iterator of addSummaryAttribute(
          documentationSection,
          subAttrName,
          subAttrVal,
          documentation,
          overrideDataPassed,
        )) {
          stepRows.push(iterator)
        }
      }
      // add empty separating row since we have a big header above
      stepRows.push({ field: '', value: '' })
      continue
    }

    // Check for repeatable Strapi components (schema inside of property)
    if (schemaForAttribute.type === 'object' && typeof attrVal === 'object') {
      // iterate through the object inside collection if present
      stepRows.push({ field: prettyFormatFieldName(attributeName) })

      for (const [subAttrName, subAttrVal] of Object.entries(attrVal)) {
        for (const iterator of addSummaryAttribute(
          schemaForAttribute.properties,
          subAttrName,
          subAttrVal,
          documentation,
          overrideDataPassed,
        )) {
          stepRows.push(iterator)
        }
      }
      // add empty separating row since we have a big header above
      stepRows.push({ field: '', value: '' })
      continue
    }

    // format date time to local time
    if( schemaForAttribute.type === 'string' && schemaForAttribute.format === 'date-time' ){
      useAttributeValue = prettyFormatDateTime(attrVal)
    }

    stepRows.push({ field: attributeName, value: useAttributeValue })
  }
  return stepRows
}

/**
 * Resolves the collection relation in the summary.
 *
 * @param {string} attributeName - The name of the attribute.
 * @param {number|object} useAttributeValue - The value of the attribute.
 * @param {string} modelName - The name of the model.
 * @return {string} The resolved attribute value.
 */
function resolveCollectionRelationInSummary(attributeName, useAttributeValue, modelName) {
  const { getDataFromModelAndId, cachedModel } = useDataManagerStore()
  let resolvedAttributeValue
  // edge case for offline vertebrate remove trap
  if (useAttributeValue.temp_offline_id) {
    switch (modelName) {
      case 'vertebrate-trap': {
        const queueSetupData = cachedModel({
          modelName: 'vertebrate-trap-line',
        })
        const relatedTrapNumber = queueSetupData.flatMap(({ traps }) => traps).find(
          (trap) => trap.temp_offline_id === useAttributeValue.temp_offline_id,
        )
        resolvedAttributeValue = relatedTrapNumber.trap_number
        break
      }

      default:
        break
    }
  }
  // online cases
  else {
    const relatedData = getDataFromModelAndId(modelName, useAttributeValue)
    if (relatedData) {
      // related attribute map for custom cases
      const relatedAttributeMap = {
        // 'modelName': 'fieldName',
      }
      if (relatedAttributeMap[modelName]) {
        resolvedAttributeValue = relatedData[relatedAttributeMap[modelName]]
      } else {
        // or if the relation attribute has the same as the one we want to display
        resolvedAttributeValue = relatedData[attributeName]
      }
    }
  }
  return resolvedAttributeValue
}

/**
 * Given a payload body (can refer to either a Bulk portion or the whole body), and replace all
 * base64 files with a relation ID to a file on the server
 *
 * @param {String} modelName The model associated with the attached body
 * @param {*} body The payload to replace files with IDs
 * @param {*} documentation The full documentation object saved in the store
 * @param {*} authToken The authorization token used for uploading
 * @param {*} schemaOverride The subset of the documentation to be directly passed instead of using modelName + documentation
 * @returns Amended body with IDs
 */
async function removeFilesFromModel(
  modelName,
  body,
  documentation,
  authToken,
  schemaOverride = null,
) {
  const documentationStore = useDocumentationStore()
  let newBody = cloneDeep(body)
  // check the newSchema for each field
  let attributes
  if (schemaOverride) {
    attributes = schemaOverride.properties
  } else {
    const schemaName = await documentationStore.formattedNewSchema(
      { modelName },
      { root: true },
    )
    attributes =
      documentation.components.schemas[schemaName].properties.data.properties
  }

  for (const [attributeName, schema] of Object.entries(attributes)) {
    // for single files, upload and put the ID in
    if (schema['x-model-ref'] && newBody[attributeName]) {
      if (schema['x-model-ref'] === 'file') {
        newBody[attributeName] = await uploadFile(
          // for multiple media component
          attributeName === `media`
            ? body[attributeName]
            : // for single file
              body[attributeName].src,
          authToken,
          attributeName === `media`
            ? body.fileName
            : body[attributeName].fileName,
          attributeName === `media`
            ? body?.mimeType
            : body[attributeName]?.mimeType,
        )
      } else if (schema.type === 'object' && schema['$ref']) {
        // this is a single child observation (has a $ref)
        // so we should resolve it
        newBody[attributeName] = await removeFilesFromModel(
          schemaNameToModelName(
            schema['$ref'].replace('#/components/schemas/', ''),
          ),
          newBody[attributeName],
          documentation,
          authToken,
        )
      }
    }

    // check for single nested components
    // We know their schema is directly passed (no $ref)
    if (schema['x-paratoo-component'] && newBody[attributeName]) {
      newBody[attributeName] = await removeFilesFromModel(
        null,
        newBody[attributeName],
        documentation,
        authToken,
        schema,
      )
    }

    // For multiple files, put array of IDs in
    if (schema.type === 'array' && Array.isArray(body[attributeName])) {
      // resolve files for each

      if (schema.items['x-model-ref'] === 'file') {
        for (const [key, value] of Object.entries(body[attributeName])) {
          newBody[attributeName][key] = await uploadFile(
            value.src,
            authToken,
            value.fileName,
            value?.mimeType,
          )
        }
      }
      // resolve newChildObservations
      else if (schema.items.type === 'object' && schema.items['$ref']) {
        for (const [key, value] of Object.entries(body[attributeName])) {
          newBody[attributeName][key] = await removeFilesFromModel(
            schemaNameToModelName(
              schema.items['$ref'].replace('#/components/schemas/', ''),
            ),
            value,
            documentation,
            authToken,
          )
        }
      } else if (
        //handle multi files (eg. photos in fauna passive observation)
        schema.items.type === 'integer' &&
        schema.items['x-model-ref']
      ) {
        for (let index = 0; index < newBody[attributeName].length; index++) {
          newBody[attributeName][index] = await removeFilesFromModel(
            schema.items['x-model-ref'],
            newBody[attributeName][index],
            documentation,
            authToken,
          )
        }
      }
      // resolve nested components for each
      if (schema.items['x-paratoo-component']) {
        for (const [key, value] of Object.entries(body[attributeName])) {
          newBody[attributeName][key] = await removeFilesFromModel(
            null,
            value,
            documentation,
            authToken,
            schema.items,
          )

          //remove redundant keys (Strapi typically ignores these, but in cases like with
          //media, there will be an `id` field that Strapi doesn't seem to ignore)
          const propertiesKeys = Object.keys(schema.items.properties)
          for (const k in newBody[attributeName][key]) {
            if (!propertiesKeys.includes(k)) {
              delete newBody[attributeName][key][k]
            }
          }
        }
      }
    }
  }
  return newBody
}

/**
 * Removes files from the provided collection and adds them to FormData, uploads, and replaces
 * them in the main collection body with an ID reference to them
 *
 * @param {Object} collection  a singular collection object
 *
 * @returns {Object} The amended collection, with files replaced with IDs of uploaded files
 */
async function removeFilesFromBulkRequest(collection) {
  const documentationStore = useDocumentationStore()
  const authStore = useAuthStore()
  let newCollection = cloneDeep(collection)
  //most collection's observations are arrays, but in some cases they are not. to keep
  //the loop simple, we just assign this singular collection to a 1-element array
  const fullDocumentation = await documentationStore.getDocumentation(null, {
    root: true,
  })
  //loop over array of items, look for files matching listOfFileFieldNames
  for (const [modelName, value] of Object.entries(collection)) {
    // We know to skip orgMintedIdentifier because it's not a model (see core-bulk.md)
    if (modelName === 'orgMintedIdentifier') continue

    if (modelName === 'plot-location') continue
    // iterate through if this is an observation
    if (Array.isArray(value)) {
      for (const [index, entry] of Object.entries(collection[modelName])) {
        try {
          newCollection[modelName][index] = await removeFilesFromModel(
            modelName,
            entry,
            fullDocumentation,
            authStore.token,
          )
        } catch (err) {
          //we catch so that we can append which specific area there was a failure
          throw new Error(`Failed removing files for model '${modelName}' from observation number ${Number.parseInt(index)+1}: ${err}`)
        }
      }
    }
    // not an observation, just treat it as a single model
    else {
      try {
        newCollection[modelName] = await removeFilesFromModel(
          modelName,
          newCollection[modelName],
          fullDocumentation,
          authStore.token,
        )
      } catch (err) {
        //we catch so that we can append which specific area there was a failure
        throw new Error(`Failed removing files for model '${modelName}': ${err}`)
      }
    }
  }
  return newCollection
}

/**
 * POSTs a new file to the backend and returns the file ID to be added as a relation to another POST
 * @param {String} file A base64 string of any valid file type (saved to the store when submitting forms)
 * @param {*} authToken valid bearer token to upload with
 * @param {String} fileIdentifier the file UUID
 * @param {String} [originalFileName] the original file name of a file that was added via
 * the file picker that has been previously uploaded. Useful if we want to explicity
 * upload the same file that might have been autofilled from a previous collection.
 * Defaults to null
 * @returns {Number} The backend ID of the newly created file
 */
async function uploadFile(fileIdentifier, authToken, originalFileName = null, overrideMimeType = null) {
  let debugMsg = `Attempting to upload file with name/identifier '${fileIdentifier}'`
  if (originalFileName) {
    debugMsg += `. Original file name (from file picker): ${originalFileName}`
  }
  if (overrideMimeType) {
    debugMsg += `. Overridden mime type (might not be defined): ${overrideMimeType}`
  }
  paratooSentryBreadcrumbMessage(debugMsg, 'uploadFile')
  const formData = new FormData()

  let fileExtension = null
  let blob = null
  try {
    // get related file from indexedDB
    blob = await binary.getRawBinary(fileIdentifier)
    //https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
    const mimeType = blob?.type
    fileExtension = mime.extension(mimeType)
  } catch (err) {
    paratooWarnHandler(
      `Could not get Blob from store for file identifier=${fileIdentifier}. This upload might have been modified from a data dump and the file has already been manually uploaded. Will continue by trying to check if this is the case.`,
      err,
    )
    if (overrideMimeType) {
      fileExtension = mime.extension(overrideMimeType)
    }
  }

  if (!fileIdentifier) {
    paratooWarnMessage(`No file identifier provided, so re-generating it instead. This might result in unexpected behaviour`)
    fileIdentifier = uuidv4()
  } else if (typeof fileIdentifier !== 'string') {
    throw new Error(`Unable to upload file because file identifier '${fileIdentifier}' is not a string`)
  }

  const fileName = `${fileIdentifier}.${fileExtension}`

  const existingFile = await axios.get(
    `${cc.coreApiUrlBase}${cc.coreApiPrefix}/upload/files?filters[name]=${fileName}`,
    {
      headers: {
        Authorization: 'Bearer ' + authToken,
      },
    },
  )

  if (
    existingFile?.data &&
    Array.isArray(existingFile.data) &&
    existingFile.data.length > 0
  ) {
    paratooSentryBreadcrumbMessage(`File with name '${fileName}' already exists, so not sending again`, 'uploadFile')
    if (existingFile.data.length > 1) {
      paratooWarnMessage(`GET request for existing files with file name '${fileName}' returned ${existingFile.data.length} results, but file names should be unique, so just using the first entries' ID`)
    }
    return existingFile.data[0].id
  }
  const blobSize = blob?.size
  paratooSentryBreadcrumbMessage(`Attempting to send file ${fileName}, type ${fileExtension}, size ${blobSize}`, 'uploadFile')
  formData.append(`files`, blob, fileName)
  if (originalFileName) {
    formData.append('fileInfo', JSON.stringify({
      alternativeText: `file_original_name=${originalFileName}`,
    }))
  }

  const postAddress = `${cc.coreApiUrlBase}${cc.coreApiPrefix}/upload`
  // we can't use doCoreApiPost because it is hardcoded to be application/json
  // when we require formData
  try {
    const resp = await axios.post(postAddress, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + authToken,
      },
    })
    if (resp.status !== 200)
      throw new Error(`Failed to upload file.\nresponse: ${JSON.stringify(resp)}`)

    return resp.data[0].id
  } catch (err) {
    //catch to append extra information to specific error.
    //the specific error really shouldn't happen due to the check for existing file
    //above, but need to handle regardless
    if (
      err?.response?.data?.error?.status === 400 &&
      err.response.data.error.message.match(/(File with name)(.*)(already exists)/gi)
    ) {
      err.message += `. ${err.response.data.error.message}`
    }
    throw new Error('Failed to upload file.\n response: ' + JSON.stringify(err.message))
    
  }
  
}

/**
 * Strapi components with exclusive-OR fields need to be put into an object for
 * submission.
 *
 * @param {Object} collection the collection containing Strapi component fields to be mutated
 * @param {Object} componentProperties an object containing the properties of the Strapi
 * component, obtained from strapiComponentProperties() getter
 */
function handleStrapiComponent(collection, componentProperties) {
  const apiModelsStore = useApiModelsStore()

  //we clone as to not directly mutate the store
  var clonedCollection = cloneDeep(collection)
  for (const collectionItem in clonedCollection) {
    //ignore these as they can be new with various fields, or existing with object: `id: <id>`
    if (
      collectionItem === 'plot-layout' ||
      collectionItem === 'plot-location' ||
      collectionItem === 'plot-visit'
    ) {
      continue
    }

    //likely an array of observations
    //FIXME won't work if collectionItem has more than one Strapi component
    //FIXME remove code duplication from this if/else block
    if (Array.isArray(clonedCollection[collectionItem])) {
      for (const arrayItem of clonedCollection[collectionItem]) {
        //check if the component fields found in the documentation match with the fields
        //of the currently collectionItem. If this is true and it is not an object,
        //mark it as the field we need to mutate into an object
        for (const componentProperty of componentProperties) {
          const compPropKey = (() => {
            let key
            Object.keys(componentProperty).forEach((k) => {
              if (k !== 'modelName') key = k
            })
            return key
          })()
          if (
            Object.keys(arrayItem).includes(compPropKey) &&
            typeof arrayItem[compPropKey] !== 'object'
          ) {
            const objKey = (() => {
              const resolvedLutSymbol = apiModelsStore.resolvedLutSymbol({
                symbol: arrayItem[compPropKey],
                modelName: componentProperty.modelName,
              })
              //didn't resolve a LUT symbol, so it's a custom input
              if (resolvedLutSymbol === null) {
                return `${compPropKey}_text`
              }
              return `${compPropKey}_lut`
            })()
            arrayItem[compPropKey] = {
              [objKey]: arrayItem[compPropKey],
            }
          }
        }
      }
    }
    //most other non-observations
    else if (typeof clonedCollection[collectionItem] === 'object') {
      //check if the component fields found in the documentation match with the fields
      //of the currently collectionItem. If this is true and it is not an object,
      //mark it as the field we need to mutate into an object
      for (const componentProperty of componentProperties) {
        const compPropKey = (() => {
          let key
          Object.keys(componentProperty).forEach((k) => {
            if (k !== 'modelName') key = k
          })
          return key
        })()
        if (
          Object.keys(clonedCollection[collectionItem]).includes(compPropKey) &&
          typeof clonedCollection[collectionItem][compPropKey] !== 'object'
        ) {
          const objKey = (() => {
            const resolvedLutSymbol = apiModelsStore.resolvedLutSymbol({
              symbol: clonedCollection[collectionItem][compPropKey],
              modelName: componentProperty.modelName,
            })
            //didn't resolve a LUT symbol, so it's a custom input
            if (resolvedLutSymbol === null) {
              return `${compPropKey}_text`
            }
            return `${compPropKey}_lut`
          })()
          clonedCollection[collectionItem][compPropKey] = {
            [objKey]: clonedCollection[collectionItem][compPropKey],
          }
        }
      }
    } else {
      paratooWarnMessage(
        'handleStrapiComponent() got to case that is not yet implemented',
      )
    }
  }
  return clonedCollection
}

/**
 * Resolved the voucher ID to it's field name.
 * 
 * @param {Number | Object} voucher the voucher ID, or an object with key
 * `temp_offline_id` if the voucher was collected offline
 * @param {'full' | 'lite'} voucherVariant the voucher variant
 * 
 * @returns {Object} the entire voucher, either retrieved from the apiModelsStore, or
 * the publication queue
 */
function voucherIdToFieldName(voucher, voucherVariant) {
  const dataManager = useDataManagerStore()
  const cachedVouchers = dataManager.cachedModel({ modelName: `floristics-veg-voucher-${voucherVariant}` })
  const relevantVoucher = cachedVouchers.find(o => {
    if (
      voucher?.temp_offline_id &&
      !o.id &&
      o.temp_offline_id
    ) {
      return o.temp_offline_id === voucher.temp_offline_id
    } else {
      return o.id === voucher || o.id === voucher?.id
    }
  })
  return relevantVoucher?.field_name
}

/**
 * Converts the webapp's StrapiV3 structured data into StrapiV4's
 * structure. i.e. with {data:'foo'} for every schema
 *
 * @param {Object} body payload to send
 * @param {String} schemaName Where to look for in allComponentSchemas
 * @param {Object} allComponentSchemas documentation.components.schemas
 * @returns
 */
function strapi4Wrap(body, schemaName, allComponentSchemas) {
  let accBody = body
  if (Array.isArray(accBody)) {
    for (const index in accBody) {
      // use same modelName because this is just itself but in
      // multiple instances
      accBody[index] = strapi4Wrap(
        accBody[index],
        schemaName,
        allComponentSchemas,
      )
    }
    return accBody
  } else {
    // find $ref relations (so has new body data HERE)
    // and do another wrap
    for (const [attributeName, attribute] of Object.entries(accBody)) {
      // if attributeName a relation (int), and payload attribute is an object
      // then assume it's referencing a components.schemas
      try {
        const properties =
          allComponentSchemas[schemaName].properties.data.properties

        const modelNameRef =
          (properties[attributeName] || {})['x-model-ref'] ||
          ((properties[attributeName] || {}).items || {})['x-model-ref']
        if (
          typeof attribute === 'object' &&
          modelNameRef &&
          modelNameRef !== 'file'
        )
          accBody[attributeName] = strapi4Wrap(
            accBody[attributeName],
            modelNameToSchemaName(modelNameRef),
            allComponentSchemas,
          )
      } catch (error) {
        // it's either not array or not present
        paratooWarnHandler(
          `Failed to wrap in Strapi 4 format for attribute name '${attributeName}'`,
          error,
        )
        continue
      }
    }

    // wrap top-level object in data key
    accBody = { data: body }
  }

  return accBody
}

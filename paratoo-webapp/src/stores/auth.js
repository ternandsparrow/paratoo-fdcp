import { defineStore } from 'pinia'
import * as Sentry from '@sentry/vue'
import { useDocumentationStore } from './documentation'
import { useApiModelsStore } from './apiModels'
import { useEphemeralStore } from './ephemeral'
import * as apiLists from '../misc/apiLists'
import axios from 'axios'
import * as constants from 'src/misc/constants'
import {
  getPath,
  binToStr,
  strToBin,
  paratooErrorHandler,
  paratooWarnMessage,
  paratooWarnHandler,
  prettyFormatFieldName,
  chainedError,
  paratooSentryBreadcrumbMessage,
  isSessionDestroyed,
  sortProjAndProt,
  refreshAndStoreHostInfo,
  isMethodReadyToContinue,
  setMethodProgressFlagsDone,
} from 'src/misc/helpers'
import { doOrgApiGet, doOrgApiPost } from 'src/misc/api'
import { alaLoginRedirect, alaLogout, refreshAccessToken } from 'src/misc/oidcALA'
import { v4 as uuidv4 } from 'uuid'
import { Loading } from 'quasar'
import { isEmpty, isEqual, union } from 'lodash'
import { useDataManagerStore } from './dataManager'
import { useBulkStore } from './bulk'

export const useAuthStore = defineStore('auth', {
  state: () => ({
    init: false,
    identifiers: [],
    token: null,
    refreshToken: null,
    userProfile: null,
    cookie: null,
    webauthn: null,
    projAndProt: null,
    currentContext: null,
    //differs from `currentContext` as this is set when a user 'selects' the project from
    //the Projects screen, but `currentContext` is only set when a user enters a protocol
    //(as that context is both the project and protocol)
    projectContext: null,
    oidcTokenResponse: null,
    currentLayoutPlots: [],
    authorisedModels: [],
    staticResourceNames: {
      species: null,
      map: null,
      GCMDScienceKeywords: null,
      ANZSRCFieldsOfResearch: null,
    },
    hostInfo: null,
    previousAuthUserProfile: null,
    loginTime: null,
    lastTokenRefreshTime: null,
    lastRefreshAllApiModelsTime: null,
    lastAccessTime: null,
    permissionStatuses: {},
  }),

  actions: {
    initStore() {
      this.init = true
      if(this.userProfile?.email) {
        Sentry.setUser({username: this.userProfile.email})
      }
    },
    setPermissionStatus({ permissionType, status }) {
      this.permissionStatuses[permissionType] = status
    },
    getProjectNameFromId(id) {
      const name = this.projAndProt?.find((o) => o.id === id)
      if (name) return name
      return null
    },
    setToken(token, refreshToken) {
      this.token = token
      this.refreshToken = refreshToken
    },
    setLoginTime(timeStr) {
      this.loginTime = timeStr
    },
    setLastTokenRefreshTime(timeStr) {
      this.lastTokenRefreshTime = timeStr
    },
    setLastRefreshAllApiModelsTime(timeStr) {
      this.lastRefreshAllApiModelsTime = timeStr
    },
    setHostInfo(hostInfo) {
      this.hostInfo = hostInfo
    },
    setUserProfile(userProfile) {
      this.userProfile = userProfile
    },
    setProjAndProt(projAndProt) {
      const isExistingProjects = !isEmpty(this.projAndProt)
      const existingProjectsSorted = isExistingProjects
        ? sortProjAndProt({
            projectList: this.projAndProt,
          })
        : []
      const newProjectsSorted = sortProjAndProt({
        projectList: projAndProt,
      })
      const projectsChanged = !isExistingProjects || !isEqual(
        existingProjectsSorted,
        newProjectsSorted,
      )
      if (projAndProt) {
        //set the base data first, then make mods to it (project area stuff below)
        this.projAndProt = projAndProt
      } else {
        paratooWarnMessage(
          `setProjAndProt provided will null/undefined data to set. Will skip setting of the data, but will continue with rest of action`,
        )
      }

      const dataManager = useDataManagerStore()
      const bulkStore = useBulkStore()
      const apiModelsStore = useApiModelsStore()
      const documentationStore = useDocumentationStore()
      
      for (const proj of this.projAndProt) {
        //handle project area updates if any are queued
        const foundQueuedProjectArea = dataManager.queuedProjectAreas.find(
          (p) => p?.project?.project?.id === proj?.id,
        )
        if (foundQueuedProjectArea) {
          paratooSentryBreadcrumbMessage(
            `Found queued project area for project '${proj.name}' (ID=${proj.id})`,
            'setProjAndProt',
          )
          if (!proj?.project_area?.coordinates) {
            paratooSentryBreadcrumbMessage(
              `Project '${proj.name}' (ID=${
                proj.id
              }) has empty object path for project_area.coordinates. Will assign it to the object. Full project object: ${JSON.stringify(
                proj,
              )}`,
              'setProjAndProt',
            )
            Object.assign(proj, {
              project_area: {
                coordinates: [],
                type: '',
              },
            })
          }
          proj.project_area.coordinates = foundQueuedProjectArea.points
          proj.project_area.type = 'Polygon'
        }

        const newProjectArea = bulkStore.getNewProjectArea(proj.id)
        if (newProjectArea) {
          paratooSentryBreadcrumbMessage(
            `Found new project area in bulk store for project '${proj.name}' (ID=${proj.id})`,
            'setProjAndProt',
          )
          this.updateProjArea({ projectAreaAndInfo: newProjectArea })
        }
      }

      const protocolDefsDefined = !isEmpty(apiModelsStore?.models?.protocol)
      const doccoIsDefined = !isEmpty(documentationStore?.documentation)
      paratooSentryBreadcrumbMessage(
        `protocolDefsDefined=${protocolDefsDefined}, doccoIsDefined=${doccoIsDefined}`,
        'setProjAndProt',
      )
      if (
        projectsChanged &&
        //if we *just* logged-in, we probably won't have protocol defs and docco yet
        protocolDefsDefined &&
        doccoIsDefined
      ) {
        //force update of protocol models' cache (e.g., if they were added/removed from a
        //protocol the cache may no longer be valid)
        paratooSentryBreadcrumbMessage(
          `Updating protocol models' cache due to project(s) change`,
        )
        for (const protId of this.protocols) {
          const resolved = apiModelsStore.protUuidFromId(protId)
          apiModelsStore.findModelsAndChildObsFromProtocolUUID({
            protUuid: resolved.uuid,
            forceCacheRevalidation: true,
          })
        }
      } else if (!protocolDefsDefined || !doccoIsDefined) {
        paratooSentryBreadcrumbMessage(
          `Skip updating protocol models' cache as there are no protocol defs and/or docco`,
        )
      } else {
        paratooSentryBreadcrumbMessage(
          `Skip updating protocol models' cache as projects haven't changed`,
        )
      }
      
    },
    updateProjArea({ projectAreaAndInfo }) {
      paratooSentryBreadcrumbMessage(
        `Updating project area. ${JSON.stringify(projectAreaAndInfo)}`,
        'projectArea',
      )
      //find but don't clone as we need to update the entry in the store by reference
      const projectToUpdate = this.projAndProt.find(
        (p) => p.id === projectAreaAndInfo.project?.project?.id,
      )

      paratooSentryBreadcrumbMessage(
        `Project to update. ${JSON.stringify(projectToUpdate)}`,
        'projectArea',
      )

      if (projectToUpdate && !projectToUpdate?.project_area) {
        projectToUpdate.project_area = {
          coordinates: [],
          type: '',
        }
      }

      projectToUpdate.project_area.coordinates = projectAreaAndInfo.points
      projectToUpdate.project_area.type = 'Polygon'
    },
    setAuthorisedModels(allAuthModels) {
      this.authorisedModels = allAuthModels
    },
    setCookie(cookie) {
      this.cookie = cookie
    },
    setWebAuthn(webauthn) {
      this.webauthn = webauthn
    },
    addIdentifier(identifier) {
      this.identifiers.push(identifier)
    },
    setCurrentContext(context) {
      this.currentContext = context
    },
    clearAuthContext() {
      this.currentContext = null
    },
    setProjectContext(projectId) {
      this.projectContext = projectId
    },
    setStaticResourceNames(contents, field) {
      this.staticResourceNames[field] = contents
    },

    //TODO reduce code duplication between this function and `doLogin()`
    async doRegister({ username, email, password }) {
      Loading.show()
      const resp = await doOrgApiPost({
        urlSuffix: constants.orgApiPrefix + apiLists.register,
        body: {
          username,
          email,
          password,
        },
      }).catch((err) => {
        Loading.hide()
        throw err
      })
      this.setToken(resp.jwt)
      this.setLoginTime(new Date(Date.now()))
      this.setUserProfile(resp.user)

      //Getting users' projects and protocols
      await this.refreshProjectsAndProtocols().catch((err) => {
        Loading.hide()
        throw err
      })
      // populate documentation during app startup
      this.populateApiModelsAndDocs().catch((reason) => {
        Loading.hide()
        console.log(`Field to load api and documentation, reason is: ${reason}`)
      })
      return resp
    },
    async doLogin({ usernameOrEmail, password }) {
      this.currentContext = null
      // for SSO
      if (constants.enableOIDC) {
        return await alaLoginRedirect()
      }
      Loading.show({
        message:
          "This may take a moment if you're logging in for the first time on this device",
        // cssClass: 'loading',
      })
      const resp = await doOrgApiPost({
        urlSuffix: constants.orgApiPrefix + apiLists.login,
        body: {
          identifier: usernameOrEmail,
          password,
        },
        suppressAuthTokenHeader: true,
      }).catch(() => {
        Loading.hide()
        return null
      })
      if (!resp) {
        Loading.hide()
        return null
      }

      this.setToken(resp.jwt, resp.refreshToken)
      this.setLoginTime(new Date(Date.now()))
      this.setUserProfile(resp.user)

      const documentationStore = useDocumentationStore()
      const doc = await documentationStore.getDocumentation()
      if (!doc) {
        paratooErrorHandler('Failed to load Documentation.')
      }

      // Getting users' projects and protocols
      await this.refreshProjectsAndProtocols().catch((err) => {
        throw err
      })
      // populate documentation during app startup
      this.populateApiModelsAndDocs().catch((reason) => {
        Loading.hide()
        console.log(`Field to load api and documentation, reason is: ${reason}`)
      })

      return resp
    },
    async populateApiModelsAndDocs() {
      /* const status = isMethodReadyToContinue(
        'populateApiModelsAndDocs',
        false,
        0.5,    //30 seconds
      )
      console.log('status:', status)
      paratooSentryBreadcrumbMessage(JSON.stringify(status))
      if (status.err) {
        throw paratooErrorHandler(status.msg, status.err)
      }
      if (!status.isReady) {
        paratooWarnMessage(status.msg)
        return
      } */
      // populate and wait for essential ApiModels to return
      const apiModelsStore = useApiModelsStore()
      const ephemeralStore = useEphemeralStore()

      ephemeralStore.setApiModelsArePopulating(true, { root: true })
      apiModelsStore
        .populateApiModels({
          forceRefresh: true,
          modelsToRefresh: apiLists.modelsToPopulate.appInitialization,
        })
        .then(() => {
          // setMethodProgressFlagsDone('populateApiModelsAndDocs')
          //now we have protocol defs, so can build this cache.
          //since `populateApiModelsAndDocs` is called on login, it will allow us to
          //refresh this cache each login time
          for (const protId of this.protocols) {
            const resolved = apiModelsStore.protUuidFromId(protId)
            apiModelsStore.findModelsAndChildObsFromProtocolUUID({
              protUuid: resolved.uuid,
              // forceCacheRevalidation: true,
            })
          }

          Loading.hide()
          ephemeralStore.setApiModelsArePopulating(true, { root: true })

          // populate authorised model in background mode.
          const allModelsToPopulate = constants.allModelsToRefresh
          ephemeralStore.setApiModelsArePopulating(true, { root: true })
          const allAuthorisedModels = this.getAllAuthorisedModels()
          apiModelsStore
            .populateVerifiedModels({
              modelNames: allModelsToPopulate.filter((m) =>
                allAuthorisedModels.includes(m),
              ),
            })
            .then(() => {
              this.setLastRefreshAllApiModelsTime(Date.now())
              // load field names to DexieDb in background mode
              apiModelsStore
                .populateFieldNamesFromDB()
                .then(() => {})
                .catch((reason) => {
                  paratooErrorHandler(
                    'Something is wrong adding field names to dexieDB',
                    reason,
                  )
                })
            })
            .catch((reason) => {
              this.setLastRefreshAllApiModelsTime(null)
              Loading.hide()
              paratooErrorHandler(
                'Failed to populate authorised models',
                reason,
              )
            })
            .finally(() => {
              ephemeralStore.setApiModelsArePopulating(false, { root: true })
            })
        })
        .catch((reason) => {
          this.setLastRefreshAllApiModelsTime(null)
          Loading.hide()
          console.error(
            `Failed to populate authorised models reason: ${reason}`,
          )
          // setMethodProgressFlagsDone('populateApiModelsAndDocs')
        })
    },

    // usually gets called if network changed
    async refreshApiListsModels() {
      // populate and wait for essential ApiModels to return
      const apiModelsStore = useApiModelsStore()
      useEphemeralStore().setApiModelsArePopulating(true, { root: true })
      this.setLastRefreshAllApiModelsTime(Date.now())
      // populate authorised model in background mode.
      const allModelsToPopulate = constants.allModelsToRefresh
      const allAuthorisedModels = this.getAllAuthorisedModels()
      useEphemeralStore().setApiModelsArePopulating(true, { root: true })
      apiModelsStore
        .populateVerifiedModels({
          modelNames: allModelsToPopulate.filter((m) =>
            allAuthorisedModels.includes(m),
          ),
        })
        .catch((reason) => {
          this.setLastRefreshAllApiModelsTime(null)
          paratooErrorHandler(
            'Failed to populate authorised models reason',
            reason,
          )
        })
        .finally(() => {
          this.setLastRefreshAllApiModelsTime(Date.now())
          useEphemeralStore().setApiModelsArePopulating(false, { root: true })
        })
    },

    /**
     * Finds and performs network fetches on all content-types associated with
     * protocols in currently assigned projects
     */
    async refreshAssignedProtocols() {
      const isOnline = useEphemeralStore().networkOnline
      if (!isOnline) return
      const assignedProtocols = this.allAssignedProtocols

      const apiModelsStore = useApiModelsStore()

      // find all models associated with assigned protocols
      let protocolModels = []
      for (const prot of assignedProtocols) {
        protocolModels.push(
          ...(await apiModelsStore.modelsOfProtocol({ protocolId: prot })),
        )
      }

      // remove duplicates
      protocolModels = union(protocolModels)

      const documentationStore = useDocumentationStore()
      // find all LUTs/references THEY reference
      let allRelevantModels = []
      for (const model of protocolModels) {
        //push the model itself, then it's deps
        allRelevantModels.push(model)
        //FIXME see if we can optimise this; inefficient recursion and many calls to
        //`_.union()`. Specifically, see how the call `_.union(allRelevantModels)` has
        //a big size difference before and after the call. `modelDependencies()` also
        //makes many `_.union()` calls that can make this issue worse
        const deps = documentationStore.modelDependencies(
          { modelName: model, modelsAcc: allRelevantModels },
          { root: true },
        )
        allRelevantModels.push(...deps)
      }

      // remove duplicates
      allRelevantModels = union(allRelevantModels)

      //this needs to be awaited so that we can correctly rethrow (done automatically)
      //rejected promise - should still happen in the background though
      await apiModelsStore.populateApiModels(
        { modelsToRefresh: allRelevantModels, forceRefresh: true },
        { root: true },
      )
    },
    async doLogout(keepPreviousProfile = false) {
      if (!keepPreviousProfile) {
        this.keepPreviousProfile()
      }
      this.setToken(null, null)
      this.setLoginTime(null)
      this.setLastTokenRefreshTime(null)
      this.setUserProfile(null)
      this.setAuthorisedModels([])
      this.setLastRefreshAllApiModelsTime(null)
      if (constants.enableOIDC) {
        await alaLogout()
      }
      useBulkStore().clearStaticPlotContext()
    },
    keepPreviousProfile() {
      if (!this.userProfile) return
      const hasPendingCollectionInQueue =
        useDataManagerStore().pendingQueuedCollections
      const hasPendingCollectionInBulk =
        Object.keys(useBulkStore().collections).length > 0
      this.previousAuthUserProfile = this.userProfile
      this.previousAuthUserProfile.hasPendingCollection = {
        queue: hasPendingCollectionInQueue,
        bulk: hasPendingCollectionInBulk,
      }
    },
    async refreshProjectsAndProtocols() {
      const result = await doOrgApiGet(
        {
          urlSuffix: constants.orgCustomApiPrefix + apiLists.userProjects,
        },
        { root: true },
      ).then((resp) => {
        const projAndProt = resp ? resp.projects : null
        this.setProjAndProt(projAndProt)
        return projAndProt
      })
      /* .catch((err) => {
          this.setProjAndProt([])
          this.setAuthorisedModels([])
          return null
        }) */
      return result
    },
    doCurrentContext({ protocol, project }) {
      this.setCurrentContext({
        protocol: protocol,
        project: project,
      })
    },
    async validateToken() {
      const ephemeralStore = useEphemeralStore()
      const isOnline = useEphemeralStore().networkOnline
      if (constants.enableTokenValidation && isOnline && this.token) {
        let isValid = false
        try {
          // checks both tokens are valid or not 
          if (isSessionDestroyed()) {
            throw paratooErrorHandler('Failed to validate token, reason: your credentials have expired, please login again.')
          }

          const resp = await doOrgApiPost({
            urlSuffix: constants.orgCustomApiPrefix + apiLists.validateToken,
            suppressAuthTokenHeader: true,
            body: {
              //as per org interface spec, we don't want to send the token in the header as
              //Strapi will attempt to validate, but we want finer control over this so we
              //attach in the body
              token: `Bearer ${this.token}`,
            },
          })

          //old version of org interface returns boolean, so need to add that check for
          //backwards compatibility
          if (typeof resp === 'boolean') {
            isValid = resp
          } else if (typeof resp === 'object' && resp?.valid) {
            isValid = resp.valid
          }
        } catch (err) {
          paratooWarnHandler('Failed to validate token', err)
          ephemeralStore.tokenHasExpired = true
        }

        if (!isValid) {
          if (!this.token) {
            ephemeralStore.tokenHasExpired = true
            return
          }
          //since invalid returns 401, doManagedFetch will likely refresh the token
          //before we get here, but worth keeping in in-case something goes wrong
          await this.refreshAuthToken()
        } else {
          ephemeralStore.tokenHasExpired = false
        }
      }
    },
    async doRefreshToken() {
      if (constants.enableOIDC) {
        if (this.token && this.oidcTokenResponse) {
          paratooSentryBreadcrumbMessage('Refreshing oidc token')
          return await refreshAccessToken()
        }
      }
      paratooSentryBreadcrumbMessage('Refreshing org token')
      return await doOrgApiPost({
        urlSuffix: constants.orgApiPrefix + apiLists.tokenRefresh,
        suppressAuthTokenHeader: true,
        body: {
          refreshToken: this.refreshToken,
        },
      })

    },
    /**
     * Refreshes the JWT (i.e., mints a new one) using the `refreshToken`
     *
     * @param {Error} [originalErr] the error that caused the need to refresh the token.
     * Optional as we might be explicity refreshing the token, not as the result of an
     * auth error
     */
    async refreshAuthToken(originalErr = null, force=false) {
      // don't need to refresh if user logs out.
      if (!this.token) return
      if (isSessionDestroyed()) {
        throw paratooErrorHandler('Failed to refresh token, reason: your credentials have expired, please login again.')
      }

      if (!force) {
        // stops precessing if there is already a pending request
        const status = isMethodReadyToContinue('refreshAuthToken', false)
        paratooSentryBreadcrumbMessage(JSON.stringify(status))
        if (status.err) {
          throw paratooErrorHandler(status.msg, status.err)
        }
        if (!status.isReady) {
          paratooWarnMessage(status.msg)
          return
        }
      }
      
      const ephemeralStore = useEphemeralStore()
      try {

        const resp = await this.doRefreshToken()
        setMethodProgressFlagsDone('refreshAuthToken')
        if (resp?.jwt && resp?.refreshToken) {
          this.setToken(resp.jwt, resp.refreshToken)
          this.setLastTokenRefreshTime(new Date(Date.now()))
          ephemeralStore.tokenHasExpired = false
          ephemeralStore.refreshTokenHasExpired = false
        } else {
          throw new Error('Tried to automatically refresh token but failed')
        }
      } catch (error) {
        setMethodProgressFlagsDone('refreshAuthToken')
        paratooWarnHandler('Failed to automatically refresh token', error)
        ephemeralStore.tokenHasExpired = true
        ephemeralStore.refreshTokenHasExpired = true
        //just fall-back to expiry and keep original error
        if (originalErr !== null) {
          const result = chainedError(
            'Your credentials have expired, please login again.',
            originalErr,
          )
          throw result
        }
        throw new Error('Your credentials have expired, please login again.')
      }
    },
    /**
     * Refreshes apiListModels if token is vlaid
     */
    async refreshTokenAndApiListsModels(force = false) {
      if (!force) {
        // stop if processed less than 10 minutes ago
        const status = isMethodReadyToContinue('refreshTokenAndApiListsModels', false, constants.heavyMethodWaitTime)
        paratooSentryBreadcrumbMessage(JSON.stringify(status))
        if (status.err) {
          throw paratooErrorHandler(status.msg, status.err)
        }
        if (!status.isReady) {
          paratooWarnMessage(status.msg)
          return
        }
      }
      
      // checks token is valid or not
      this.validateToken()
        .then(() => {
          // only refresh if token is valid
          this.refreshApiListsModels().then(()=> {
            setMethodProgressFlagsDone('refreshTokenAndApiListsModels')
            // refresh host info
            refreshAndStoreHostInfo()
          }).catch((err) => {
            setMethodProgressFlagsDone('refreshTokenAndApiListsModels')
            throw paratooErrorHandler('Failed to refresh models', err)
          })
          
        })
        .catch((err) => {
          setMethodProgressFlagsDone('refreshTokenAndApiListsModels')
          throw paratooErrorHandler('Failed to validate token', err)
        })
    },
    createCookie({ uuid }) {
      //generate and store longLivedCookie
      const d = new Date()
      d.setTime(d.getTime() + 3650 * 24 * 60 * 60 * 1000)
      let expires = 'expires=' + d.toUTCString()
      const cookie =
        'Paratoo-WebAuthn-longLivedCookie=' + uuid + '; ' + expires + '; path=/'
      document.cookie = cookie
      this.setCookie(cookie)
    },
    async registerAltAuth() {
      /**
       * Docs and useful links:
       *  -https://w3c.github.io/webauthn/#sctn-registering-a-new-credential
       *  -https://webauthn.guide/
       *  -https://developer.mozilla.org/en-US/docs/Web/API/Web_Authentication_API
       *  -https://itnext.io/biometrics-fingerprint-auth-in-your-web-apps-d5599522d0b3
       */

      Loading.show()

      //check if device is compatible
      if (typeof PublicKeyCredential == 'undefined') {
        Loading.hide()
        throw 'This authentication method is not supported by your device'
      }

      var uuid
      if (this.webAuthnStatus) {
        //decideWebAuthn() shouldn't allow us to get here, but we check just in case
        Loading.hide()
        throw 'This device is already registered for WebAuthn'
      } else {
        //generate a UUID, unique identifier for this device
        //NOTE: if using WebAuthn devtool, it generates its own UUID
        //      but we seem to ignore it
        console.log(
          'This device has not been registered for WebAuthn. ' +
            'Beginning registration process...',
        )
        uuid = uuidv4()
      }

      //call org generateChallenge()
      const resp_challenge = await axios
        .post(
          constants.orgApiUrlBase + constants.orgCustomApiPrefix + '/challenge',
          {
            //data
            userInfo: {
              userId: this.userId,
              username: this.username,
              userEmail: this.email,
            },
          },
          {
            headers: {
              Authorization: 'Bearer ' + this.token,
            },
          },
        )
        .catch((err) => {
          Loading.hide()
          let msg =
            'Could not receive challenge from server. ' +
            err.response.data.message
          throw paratooErrorHandler(msg, err)
        })

      //check if mobile device
      //NOTE this had limited support and is experimental. For compatibility see:
      //https://caniuse.com/mdn-api_navigator_useragentdata
      let authenticator
      if (navigator.userAgentData.mobile) {
        authenticator = 'platform'
      } else {
        authenticator = 'cross-platform'
      }
      //FIXME remove from prod (is overriding to force fingerprint for mobile/tablet)
      authenticator = 'platform'

      //spec step 1: construct object for creation of credential
      const publicKeyCredentialCreationOptions = {
        extensions: {
          uvm: true, //records which verification method was used (fingerprint, PIN, etc.)
        },
        //buffer of cryptographically random bytes generated on the server,
        //and is needed to prevent "replay attacks"
        challenge: Uint8Array.from(resp_challenge.data.challenge, (c) =>
          c.charCodeAt(0),
        ),

        //relying party, describes the organization responsible for registering and
        //authenticating the user
        rp: resp_challenge.data.rp,

        //information about the user currently registering
        user: {
          id: Uint8Array.from(resp_challenge.data.user.id, (c) =>
            c.charCodeAt(0),
          ),
          name: resp_challenge.data.user.name, //username
          displayName: resp_challenge.data.user.displayName, //email
        },

        //array of objects describing what public key types are acceptable to a server
        //we use elliptic curve public keys using a SHA-256
        pubKeyCredParams: resp_challenge.data.pubKeyCredParams,
        // pubKeyCredParams: [{alg: -7, type: 'public-key'}],

        //(optional) helps relying parties make further restrictions on the type of
        //authenticators allowed for registration
        authenticatorSelection: {
          authenticatorAttachment: authenticator,
        },

        //time (in milliseconds) that the user has to respond to a prompt for
        //registration before an error is returned
        timeout: 60000,

        //attestation data that is returned from the authenticator has information that
        //could be used to track users. Direct means that the server wishes to receive
        //the attestation data from the authenticator
        attestation: 'direct',
      }

      //spec step 2: create credential
      const credential = await navigator.credentials
        .create({
          publicKey: publicKeyCredentialCreationOptions,
        })
        .catch((err) => {
          Loading.hide()
          let msg = 'Could not create the credential'
          throw paratooErrorHandler(msg, err)
        })

      //spec step 3: if response is not an instance of AuthenticatorAttestationResponse,
      //abort the ceremony
      //https://stackoverflow.com/a/7894320
      if (
        Object.prototype.toString.call(credential.response) !=
        '[object AuthenticatorAttestationResponse]'
      ) {
        Loading.hide()
        throw 'credential.response is not of type AuthenticatorAttestationResponse'
      }

      //spec step 4
      //we don't define extensions but they should be handled
      //FIXME this is returning an empty object even when provided with extensions
      //it's supposed to return an ArrayBuffer. only certain extensions are supported
      //by chrome. Are we supplying the right one(s)?
      const clientExtensionResult = credential.getClientExtensionResults()

      //create encoded credential object
      const encodedCredential = {
        id: credential.id,
        rawId: binToStr(credential.rawId),
        response: {
          clientDataJSON: binToStr(credential.response.clientDataJSON),
          attestationObject: binToStr(credential.response.attestationObject),
        },
        type: credential.type,
      }

      //send encoded credential to server for verification
      await axios
        .post(
          constants.orgApiUrlBase +
            constants.orgCustomApiPrefix +
            '/verify-register',
          {
            encodedCredential,
            clientExtensionResult,
          },
          {
            headers: {
              Authorization: 'Bearer ' + this.token,
            },
          },
        )
        .catch((err) => {
          Loading.hide()
          let msg =
            'Could not verify the credential. ' + err.response.data.message
          throw paratooErrorHandler(msg, err)
        })

      //construct params for notifying server
      const params = {
        longLivedCookie: uuid,
        challenge: resp_challenge.data.challenge,
        user: this.userId,
        credential: encodedCredential,
      }

      //tell server about event and store credential
      //FIXME double-check that this is following spec step 22
      const resp_device = await axios
        .post(
          constants.orgApiUrlBase + constants.orgApiPrefix + '/devices',
          {
            params,
          },
          {
            headers: {
              Authorization: 'Bearer ' + this.token,
            },
          },
        )
        .catch((err) => {
          Loading.hide()
          let msg =
            'Could not send credential to server for storage. ' +
            err.response.data.message
          throw paratooErrorHandler(msg, err)
        })

      //tell user success/failure
      if (resp_device.status == 200) {
        //generate and store longLivedCookie
        this.createCookie(uuid)
        this.setWebAuthn(true)
        Loading.hide()

        //send success to webapp
        return 'Alternate authentication registration complete'
      } else {
        //send failure to webapp
        Loading.hide()
        throw 'Could not complete alternate authentication registration'
      }
    },
    async loginAltAuth({ usernameOrEmail }) {
      Loading.show()
      const resp_challenge = await axios
        .post(
          constants.orgApiUrlBase +
            constants.orgCustomApiPrefix +
            '/login-challenge',
          {
            //data
            uuid: this.uuid,
          },
        )
        .catch((err) => {
          Loading.hide()
          let msg =
            'Could not receive challenge from server. ' +
            err.response.data.message
          throw paratooErrorHandler(msg, err)
        })

      //spec step 1: construct object for retrieval of credential
      //TODO handle when we supply allowCredentials
      //transports member of each item SHOULD be set to the value that was returned by
      //credential.response.getTransports() when the corresponding credential was
      //registered (i.e., during create())
      //FIXME JSON.stringify(credential.response.getTransports()) is returning an empty
      //array. It should return the transport(s) used when registering
      const publicKeyCredentialRequestOptions = {
        //buffer of cryptographically random bytes generated on the server,
        //and is needed to prevent "replay attacks"
        challenge: Uint8Array.from(resp_challenge.data.challenge, (c) =>
          c.charCodeAt(0),
        ),

        //tells the browser which credentials the server would like the user to
        //authenticate with. credentialId saved during registration is passed in here
        //TODO transports should be specified based on the value(s) obtained when doing
        //credential.response.getTransports() after the credential was created
        allowCredentials: [
          {
            id: Uint8Array.from(strToBin(resp_challenge.data.credentialId)),
            type: 'public-key',
            transports: ['internal'],
          },
        ],

        //time (in milliseconds) that the user has to respond to a prompt for
        //registration before an error is returned
        timeout: 60000,

        userVerification: 'preferred',
      }

      //spec step 2: authenticate user
      const assertion = await navigator.credentials
        .get({
          publicKey: publicKeyCredentialRequestOptions,
        })
        .catch((err) => {
          Loading.hide()
          let msg = 'Could not get the credential'
          throw paratooErrorHandler(msg, err)
        })

      //spec step 3: if assertion.response is not an instance of
      //AuthenticatorAssertionResponse, abort the ceremony
      if (
        Object.prototype.toString.call(assertion.response) !=
        '[object AuthenticatorAssertionResponse]'
      ) {
        Loading.hide()
        throw 'credential.response is not of type AuthenticatorAssertionResponse'
      }

      //spec step 4
      //we don't define extensions but they should be handled
      //FIXME this is returning an empty object even when provided with extensions
      //it's supposed to return an ArrayBuffer. only certain extensions are supported
      //by chrome. Are we supplying the right one(s)?
      assertion.getClientExtensionResults()

      //spec step 5: if options.allowCredentials is not empty, verify that assertion.id
      //identifies one of the public key credentials listed in options.allowCredentials
      if (publicKeyCredentialRequestOptions.allowCredentials) {
        const assertionRawIdStr = binToStr(assertion.rawId)
        const optsAllowCred = publicKeyCredentialRequestOptions.allowCredentials
        var foundAllowCred = false
        for (var i = 0; i < optsAllowCred.length; i++) {
          if (assertionRawIdStr == binToStr(optsAllowCred[i].id)) {
            foundAllowCred = true
          }
        }
        if (!foundAllowCred) {
          Loading.hide()
          throw 'The created assertion does not contain any of the specified allowed credentials.'
        }
      }

      //create encoded assertion object, as directly sending the assertion (raw bytes)
      //to the server corrupts the data
      const PublicKeyCredential = {
        id: assertion.id,
        rawId: binToStr(assertion.rawId),
        response: {
          authenticatorData: binToStr(assertion.response.authenticatorData),
          clientDataJSON: binToStr(assertion.response.clientDataJSON),
          signature: binToStr(assertion.response.signature),
          userHandle: usernameOrEmail,
        },
        type: assertion.type,
      }

      //send assertion to server for verification (spec steps 6-21)
      //also mints and returns the JWT and user data
      const resp_verify = await axios
        .post(
          constants.orgApiUrlBase +
            constants.orgCustomApiPrefix +
            '/verify-login',
          {
            //data
            PublicKeyCredential,
          },
        )
        .catch((err) => {
          Loading.hide()
          let msg =
            'Could not verify the generated credential. ' +
            err.response.data.message
          throw paratooErrorHandler(msg, err)
        })

      //login
      this.setToken(resp_verify.data.jwt)
      this.setLoginTime(new Date(Date.now()))
      this.setUserProfile(resp_verify.data.user)
      Loading.hide()

      return 'Login complete'
    },
    async disableAltAuth() {
      Loading.show()
      //TODO if there's issues with any of the steps, revert changes from previous steps
      //and return error
      let cookieToDelete = this.uuid

      //delete cookie
      const d = new Date()
      d.setTime(d.getTime() - 3650 * 24 * 60 * 60 * 1000) //10 years ago
      let expires = 'expires=' + d.toUTCString()
      const cookie = 'Paratoo-WebAuthn-longLivedCookie=;' + expires + '; path=/'
      document.cookie = cookie

      //check cookie was deleted
      let cookieName = decodeURIComponent(document.cookie).split('=')[0]
      if (cookieName == 'Paratoo-WebAuthn-longLivedCookie') {
        Loading.hide()
        throw 'Failed to delete stored cookie for registered device'
      }

      //remove from store
      this.setCookie(null)

      //check cookie was removed from vuex store
      if (this.webAuthnStatus) {
        Loading.hide()
        throw 'There was an error removing the cookie from local storage'
      }

      //remove from DB
      await axios
        .delete(
          constants.orgApiUrlBase +
            constants.orgApiPrefix +
            '/devices/' +
            cookieToDelete,
          {
            headers: {
              Authorization: 'Bearer ' + this.token,
            },
          },
        )
        .catch((err) => {
          Loading.hide()
          let msg =
            'Could not delete the device record from the database. ' +
            err.response.data.message
          throw paratooErrorHandler(msg, err)
        })

      //check if removed from DB
      const resp_device_check = await axios
        .get(
          constants.orgApiUrlBase +
            constants.orgApiPrefix +
            '/devices/' +
            cookieToDelete,
          {
            headers: {
              Authorization: 'Bearer ' + this.token,
            },
          },
        )
        .catch((err) => {
          Loading.hide()
          let msg =
            'Could not check if the device was removed correctly. ' +
            err.response.data.message
          throw paratooErrorHandler(msg, err)
        })
      if (resp_device_check.data.found) {
        Loading.hide()
        throw 'Failed to delete registered device from database'
      }

      Loading.hide()
      return 'WebAuthn has been disabled on this device' //success
    },
    async mintIdentifier({
      survey_metadata,
      offlinePublicationQueueIndex = null,
    }) {
      const dataManager = useDataManagerStore()
      let storedAuthContext = null
      if (offlinePublicationQueueIndex !== null) {
        storedAuthContext = dataManager.authContextForQueuedCollection({
          index: offlinePublicationQueueIndex,
        })
      }

      if (
        this.currentContext === null &&
        offlinePublicationQueueIndex !== null &&
        !storedAuthContext
      ) {
        throw 'No context defined for the project and protocol'
      }

      ;(() => {
        if (offlinePublicationQueueIndex) {
          return {
            project: storedAuthContext.project,
            protocol: storedAuthContext.protocol,
          }
        }
        return {
          project: this.currentContext.project,
          protocol: this.currentContext.protocol,
        }
      })()
      return await doOrgApiPost({
        urlSuffix: constants.orgCustomApiPrefix + apiLists.mintIdentifier,
        body: {
          survey_metadata: survey_metadata,
        },
      })
    },
  },

  getters: {
    getAllAuthorisedModels: (state) => {
      return ({ projectsIds = null } = {}) => {
        const apiModelsStore = useApiModelsStore()
        if (
          Array.isArray(projectsIds) &&
          isEqual(
            state.projAndProt.map(p => p.id).sort(),
            projectsIds.sort(),
          )
        ) {
          //passing all projects as a param to this function is redundant, as when we
          //don't pass them, we are looking through all projects anyway
          paratooWarnMessage(`getAllAuthorisedModels() passed redundant list of projects`)
        }

        if (state.authorisedModels.length > 0 && !projectsIds) {
          return state.authorisedModels
        }
      
        let models = []
        for (const proj of state.projAndProt) {
          if (projectsIds && !projectsIds.includes(proj.id)) continue
          for (const p of proj.protocols) {
            const workFlow = apiModelsStore.findModelsAndChildObsFromProtocolUUID({
              protUuid: p.identifier,
            })
            models = union(models, workFlow.allModels, workFlow.plotModels)
          }
        }
        if (!projectsIds) state.setAuthorisedModels(models)
        return models
      }
    },
    isParatooAdmin(state) {
      if (!state.roleType || !constants.adminRoleType) {
        return false
      }
      return state.roleType === constants.adminRoleType
    },
    isProjectAdmin(state) {
      return state.roleType === 'project_admin'
    },
    username(state) {
      return getPath(state.userProfile, 'username')
    },
    email(state) {
      return getPath(state.userProfile, 'email')
    },
    projects(state) {
      return state.projAndProt
    },
    getHostInfo(state) {
      return state.hostInfo
    },
    /**
     * Gets the list of User's protocol's IDs
     *
     * @param {Object} state
     * @returns {Array} list of protocol IDs assigned to the User
     */
    protocols(state) {
      const listOfProt = []
      //loop over User's projects
      for (const proj of state.projAndProt || []) {
        //push the protocol ID of the project if it's not already in the array
        proj.protocols.forEach((prot) => {
          if (!listOfProt.includes(prot.id)) {
            listOfProt.push(prot.id)
          }
        })
      }
      return listOfProt
    },
    userId(state) {
      return getPath(state.userProfile, 'id')
    },
    webAuthnStatus(state) {
      return state.cookie ? true : false
    },
    uuid(state) {
      return state.cookie
        ?.split('Paratoo-WebAuthn-longLivedCookie=')[1]
        .split(';')[0]
    },
    isConfirmed(state) {
      return getPath(state.userProfile, 'confirmed')
    },
    role(state) {
      if (state.roleType) {
        //human-readable role (e.g., `Project Admin`)
        return prettyFormatFieldName(state.roleType)
      }
      return null
    },
    roleType(state) {
      if (state?.projAndProt && state?.currentContext?.project) {
        const currProjectFromContext = state.projAndProt.find(
          (o) => o.id === state.currentContext?.project,
        )
        if (!currProjectFromContext?.role) {
          paratooWarnMessage(
            `No role defined for user's project ID='${state.currentContext?.project}' - things might break`,
          )
          return null
        }
        //raw role string (e.g., `project_admin`)
        return currProjectFromContext.role
      }
      return null
    },
    /**
     * Used to grab all models (and subsequent LUTs) for conditional api-model population
     *
     * @returns Array of protocols assigned to logged in user
     */
    allAssignedProtocols(state) {
      let protocols = []
      for (const project of state?.projAndProt || []) {
        const assignedProtocols = project.protocols.map((o) => o.id)

        protocols.push(...assignedProtocols)
      }
      // remove duplicates
      // if the same protocol is assigned to multiple projects
      return union(protocols)
    },
    selectedProject(state) {
      const proj = state?.projAndProt?.find(
        (o) => o.id === state?.currentContext?.project,
      )
      if (proj) return proj
      return null
    },
    getCurrentContext(state) {
      return state?.currentContext
    },
  },
})

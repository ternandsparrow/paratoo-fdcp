import { store } from 'quasar/wrappers'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import {
  dexiePersistPlugin,
} from './persist'
export default store((/* { ssrContext } */) => {
  const pinia = createPinia()

  pinia.use(dexiePersistPlugin.dexiePersistPlugin)
  pinia.use(piniaPluginPersistedstate)

  return pinia
})

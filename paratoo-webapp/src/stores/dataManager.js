import { defineStore } from 'pinia'
import { useApiModelsStore as apiModelsStore } from './apiModels'
import { useBulkStore as bulkStore } from './bulk'
import { useAuthStore as authStore } from './auth'
import { useDocumentationStore as documentationStore } from './documentation'
import {
  v4 as uuidv4,
  validate as uuidValidate,
} from 'uuid'
import {
  workflowSubmission,
  notifyHandler,
  paratooWarnMessage,
  paratooErrorHandler,
  schemaFromModelName,
  paratooSentryBreadcrumbMessage,
  findValForKey,
  notifyWithActions,
  postStoreState,
  isExistingLayoutAndVisit,
  logEstimatedQuota,
  flattenPlotDataId,
  checkAndUpdatePlotContext,
} from '../misc/helpers'
import { useRouter } from 'vue-router'
import {
  cloneDeep,
  isEmpty,
  isEqual,
} from 'lodash'
import * as cc from 'src/misc/constants'
import { useEphemeralStore } from './ephemeral'
import { doCoreApiGet, doCoreApiPost } from 'src/misc/api'
import * as apiLists from '../misc/apiLists'
import { binary } from '../misc/binary'
import ProfileCollectionDetails from '../components/ProfileCollectionDetails.vue'
import { Dialog } from 'quasar'
import { intersection } from 'lodash'
import {
  historicalDataManagerDb,
} from '../../src-pwa/dexieDb/historical-data-manager'

export const useDataManagerStore = defineStore('dataManager', {
  state: () => ({
    //TODO might be able to have Workbox handle this
    publicationQueue: [],
    //TODO once we integrate workbox, probably don't need to have this (we can have more trust in Workbox's stability)
    //fail-safe in-case something catastrophic happens during queue submission
    publicationQueueBackup: [],
    //models that any given protocol might depend on
    DEPENDS_ON_MODELS: Object.keys(cc.offlineRefreshableModels).reduce((accum, currKey) => {
      accum.push(
        ...cc.offlineRefreshableModels[currKey]
      )
      
      return accum
    }, []),
    //differs from `responses` as it maps the collection to combine the responses with
    //temp offline IDs
    submitResponses: [],
    errors: [],
    responses: [],
    skips: [],
    //if we don't write to the store at some point early in app startup, it is not added
    //to local storage and might result in weird behaviour when trying to access the
    //store (particularly in Cypress that directly grabs from local storage)
    init: false,
    queueSubmissionProgress: null,
    queuedCollectionUuidWithAjvErrors: {},
    //the timestamp when a queue sync was last finished (successful or not)
    lastSyncTime: null,
  }),
  persist: true,
  actions: {
    /**
     * Adds a collection and metadata to the publication queue, removing it from bulk
     * 
     * @param {Number} protocolId the collection's protocolId
     * @param {Object} workflowItem the workflow item for the protocol
     * @param {Object} [submoduleInfo] submodule metadata, if relevant
     * @param {Array} [ajvValidationErrors] ajv validation errors
     */
    async queueSubmission({
      protocolId,
      workflowItem,
      submoduleInfo = null,
      ajvValidationErrors = []
    }) {
      const relevantBulkCollection = bulkStore().collectionGetForProt({ protId: protocolId })
      const isExistingLayoutAndVisit_ = isExistingLayoutAndVisit({
        protocolId,
      })
      const prot = apiModelsStore().protocolInformation({ protId: protocolId })
      paratooSentryBreadcrumbMessage(`${isExistingLayoutAndVisit_ ? 'Setting context of' : 'Queuing'} submission with protocol ID=${protocolId}: ${JSON.stringify(relevantBulkCollection)}`, 'dataManagerQueueSubmission')

      //keep track of which other collections (already in queue) that this collection
      //depends on, so that we can handle re-tries if an error occurs. e.g., if
      //collection B depends on collection A, and A fails, we don't want to submit B.
      //iterate over this collection and check schema for relations that are populated
      //with temp offline IDs; those relations are what we depend on
      const dependsOnQueuedCollections = await resolveDependsOnCollections(
        relevantBulkCollection,
        this.publicationQueue,
        this.DEPENDS_ON_MODELS
      )

      const queuedCollectionIdentifier = uuidv4()
      if (!isExistingLayoutAndVisit_) {
        ajvValidationErrors = this.resolveDependencyAjvErrors( queuedCollectionIdentifier, ajvValidationErrors, dependsOnQueuedCollections, prot)
        this.publicationQueue.push({
          protocolId: protocolId,
          collection: relevantBulkCollection,
          workflowItem: workflowItem,
          submoduleInfo: submoduleInfo,
          authContext: authStore().currentContext,
          queuedCollectionIdentifier,
          dependsOnQueuedCollections,
          ajvValidationErrors
        })
      }
      
      // purposely not await, we want let it run in the background
      // as this is just for cleaning up after a submission
      binary.updateStateOfBinaryOfQueuedProtocol(protocolId, relevantBulkCollection)

      if (!isExistingLayoutAndVisit_ && submoduleInfo) {
        const relevantSubmoduleBulkCollection = bulkStore().collectionGetForProt({ protId: submoduleInfo.protocolEntry.id })
        paratooSentryBreadcrumbMessage(`Queuing submodule submission with protocol ID=${submoduleInfo.protocolEntry.id}: ${JSON.stringify(relevantSubmoduleBulkCollection)}`, 'dataManagerQueueSubmission')
        const queueItem = {
          protocolId: submoduleInfo.protocolEntry.id,
          collection: relevantSubmoduleBulkCollection,
          workflowItem: submoduleInfo.workflow,
          //collection is queued as if it wasn't a submodule, so don't include the info
          //(unless in the future we have sub-sub-modules)
          submoduleInfo: null,
          authContext: {
            project: authStore().currentContext.project,
            //submodule can't use the protocol context from the store as that's the
            //parent module, so need to store submodule info
            protocol: {
              id: submoduleInfo.protocolEntry.id,
              version: submoduleInfo.protocolEntry.version,
            },
          },
          queuedCollectionIdentifier: uuidv4(),
          dependsOnQueuedCollections: [queuedCollectionIdentifier, ...dependsOnQueuedCollections],
        }
        queueItem.ajvValidationErrors = ajvValidationErrors.filter(error => error.isSubmodule)
        this.publicationQueue.push(queueItem)

        bulkStore().removeSubmodule(protocolId)
        binary.updateStateOfBinaryOfQueuedProtocol(protocolId, relevantBulkCollection)
      }

      if (prot?.identifier === 'a9cb9e38-690f-41c9-8151-06108caf539d') {
        paratooSentryBreadcrumbMessage(`Queued plot selection submission with protocol ID=${protocolId}`, 'dataManagerQueueSubmission')

        if (
          relevantBulkCollection?.['plot-selection-survey'] &&
          //could have an empty survey object, which is not valid and thus needs to be in
          //backwards compatibility mode
          !isEmpty(relevantBulkCollection['plot-selection-survey'])
        ) {
          Object.assign(relevantBulkCollection['plot-selection-survey'], {
            'project-areas': bulkStore().getNewProjectAreas(),
          })
        } else {
          paratooWarnMessage(`Compatibility mode for plot selection enabled. Will assign project area to root of collection data, rather than to the plot-selection-survey`)
          Object.assign(relevantBulkCollection, {
            'project-areas': bulkStore().getNewProjectAreas(),
          })
        }
      }

      const collectionKeys = Object.keys(relevantBulkCollection)
      if (collectionKeys.includes('plot-definition-survey')) {
        let layout = relevantBulkCollection['plot-layout']
        if (!layout.id) {
          //we've created a new layout (rather than select existing), so need to store a
          //temporary ID to link other protocols to
          Object.assign(layout, {
            temp_offline_id: uuidv4()
          })
        }

        let visit = relevantBulkCollection['plot-visit']
        if (!visit.id) {
          Object.assign(visit, {
            temp_offline_id: uuidv4()
          })
          if (!layout.id && layout.temp_offline_id) {
            Object.assign(visit, {
              plot_layout: {
                temp_offline_id: layout.temp_offline_id,
              }
            })
          }
        }

        bulkStore().setStaticPlotLayout(layout)
        bulkStore().setStaticPlotVisit(visit)
      }

      for (const variant of ['full', 'lite']) {
        if (collectionKeys.includes(`floristics-veg-survey-${variant}`)) {
          const survey = relevantBulkCollection[`floristics-veg-survey-${variant}`]
          if (!survey.id) {
            //survey was collected offline, so assign temp ID for other modules to use
            Object.assign(survey, {
              temp_offline_id: uuidv4()
            })
          }
        }
        if (collectionKeys.includes(`floristics-veg-voucher-${variant}`)) {
          for (let voucher of relevantBulkCollection[`floristics-veg-voucher-${variant}`]) {
            if (!voucher.id) {
              //voucher was collected offline, so assign temp ID for other modules to use
              Object.assign(voucher, {
                temp_offline_id: uuidv4(),
                //temp link to survey so we can easily resolve a given voucher's survey
                [`floristics_veg_survey_${variant}`]: {
                  temp_offline_id: relevantBulkCollection[`floristics-veg-survey-${variant}`].temp_offline_id,
                }
              })
            }
          }
        }
      }

      if (collectionKeys.includes('vertebrate-trapping-setup-survey')) {
        const survey = relevantBulkCollection['vertebrate-trapping-setup-survey']
        if (!survey.id) {
          Object.assign(survey, {
            temp_offline_id: uuidv4()
          })
        }
      }

      const vertCollectionsToHandle = [
        // 'vertebrate-trap',
        'vertebrate-trap-line'
      ]
      for (const vertCollectionModel of vertCollectionsToHandle) {
        if (
          collectionKeys.some(o => o.includes(vertCollectionModel)) &&
          //also need to check this as we'll retrieve the survey to store in the trap
          relevantBulkCollection['vertebrate-trapping-setup-survey']
        ) {
          const relevantCollection = relevantBulkCollection[vertCollectionModel]

          for (let c of relevantCollection) {
            if (!c?.trapping_survey?.id) {
              /*
              `vertebrate-trap-[line]` and `vertebrate-trapping-setup-survey` are part of
              the same collection, but we need to add a link of the latter to the former
              so that when we do Identify, Measure and Release or Trapping Survey
              Closure, we can quickly get the survey data (which is usually already in
              `vertebrate-trap-[line]` that has been collected online; the relation to
              the survey is populated when we hit the API and store in apiModelsStore).
              however, since core expects this survey to be a relation, but core itself
              creates the relation from the workflow, we need to remove this on
              submission (i.e., `resolveVertTrapRelation()`)
              */
              Object.assign(c, {
                trapping_survey: relevantBulkCollection['vertebrate-trapping-setup-survey']
              })
            }
            if (!c?.id) {
              Object.assign(c, {
                temp_offline_id: uuidv4()
              })
            }
            for(const trap of c.traps ) {
              if (!trap?.id) {
                Object.assign(trap, {
                  temp_offline_id: uuidv4()
                })
              }
            }
          }
        }
      }

      if (collectionKeys.includes('camera-trap-deployment-survey')) {
        let deploymentSurvey = relevantBulkCollection['camera-trap-deployment-survey']
        if (!deploymentSurvey.id) {
          //reequipping/retrieval need to reference (not a hard relational link) survey
          Object.assign(deploymentSurvey, {
            temp_offline_id: uuidv4()
          })
        }
      }

      if (collectionKeys.includes('camera-trap-deployment-point')) {
        //all these temporary IDs DO NOT need to be resolved on submission, as we don't
        //make hard relational links, but typically copies based on auto-filled data
        let deploymentPoints = relevantBulkCollection['camera-trap-deployment-point']
        let deploymentSurvey = relevantBulkCollection['camera-trap-deployment-survey']
        for (let point of deploymentPoints) {
          if (!point?.id) {
            //reequipping/retrieval need to reference (not a hard relational link) points
            Object.assign(point, {
              temp_offline_id: uuidv4()
            })
          }
          
          if (!point?.camera_trap_survey) {
            //we will need to filter points by their survey, so add this temporary copy
            //of the survey that's later removed before submission (so that core's
            //validator doesn't complain)
            Object.assign(point, {
              camera_trap_survey: deploymentSurvey
            })
          }

          //both info and settings are used for auto-fill in reequipping, so need temp ID
          //to resolve them
          if (!point?.camera_trap_information?.id) {
            Object.assign(point.camera_trap_information, {
              temp_offline_id: uuidv4()
            })
          }
          if (!point?.camera_trap_settings?.id) {
            Object.assign(point.camera_trap_settings, {
              temp_offline_id: uuidv4()
            })
          }

          //features are multi child ob, so assign temp ID to each (for autofill in
          //reequipping)
          if (Array.isArray(point.features) && point.features.length > 0) {
            for (let feature of point.features) {
              if (!feature.id) {
                Object.assign(feature, {
                  temp_offline_id: uuidv4()
                })
              }
            }
          }
        }
      }

      
      for (const variant of ['full', 'lite']) {
        const key = `soil-pit-characterisation-${variant}`
        if (collectionKeys.includes(key)) {
          let characterisation = relevantBulkCollection[key]
          if (!characterisation.id) {
            Object.assign(characterisation, {
              temp_offline_id: uuidv4(),
            })
          }
        }
      }

      if (prot?.identifier === 'a9cb9e38-690f-41c9-8151-06108caf539d') {
        //might have new project areas in queue, so need to reflect in auth store, where
        //we typically access project areas
        await authStore().refreshProjectsAndProtocols()
      }

      let successMsg = `Successfully ${isExistingLayoutAndVisit_ ? 'set plot context of' : 'queued'} the collection for the ${prot.name} protocol`

      bulkStore().clearCollection({ protocolId: protocolId })
      if (submoduleInfo) {
        bulkStore().clearCollection({ protocolId: submoduleInfo.protocolEntry.id })

        const protSubmodule = apiModelsStore().protocolInformation({ protId: submoduleInfo.protocolEntry.id })
        successMsg += ` and the submodule ${protSubmodule.name}`
      }
      
      notifyHandler('positive', `${successMsg}.`)
    },
    resolveDependencyAjvErrors(queuedCollectionIdentifier,ajvValidationErrors, dependsOnQueuedCollections, protocolInfo) {
      const isDependentOnOtherCollectionWithAjv = intersection(
        dependsOnQueuedCollections, Object.keys(this.queuedCollectionUuidWithAjvErrors))
      if(isDependentOnOtherCollectionWithAjv.length > 0) {
        const relatedProtocolNames = isDependentOnOtherCollectionWithAjv.map((currUuid) => this.queuedCollectionUuidWithAjvErrors[currUuid].protocolInfo.name)
        ajvValidationErrors.push({
          message: 'Depends on other collections that was queued with errors, related collections: ' + relatedProtocolNames.join(', ')
        })
      }
      if(ajvValidationErrors.length > 0) {
        this.queuedCollectionUuidWithAjvErrors[queuedCollectionIdentifier] = {
          protocolInfo
        }
      }
      return ajvValidationErrors
    },
    /**
     * Submits all collections in the publication queue, resolving steps' and fields'
     * temporary relations
     */
    async submitPublicationQueue() {
      notifyHandler(
        'info',
        'Syncing publication in progress. Please keep the app open until the loading bar completes.',
        null,
        10,
      )
      this.publicationQueueBackup = cloneDeep(this.publicationQueue)
      this.errors = []
      this.responses = []
      this.skips = []
      let dumpIsSent = false
      let submittedModelsToRefresh = []
      let queueHasPlotSelection = false

      const inProgressCollectionsDependOnQueueIdentifiers = await inProgressCollectionsDependOnQueue({
        publicationQueue: this.publicationQueue,
        DEPENDS_ON_MODELS: this.DEPENDS_ON_MODELS,
      })
      paratooSentryBreadcrumbMessage(
        `Queue has in-progress dependencies: ${JSON.stringify(inProgressCollectionsDependOnQueueIdentifiers)}`,
        'dataManagerSubmitPublicationQueue',
      )

      for (let [index, queuedCollection] of Object.entries(this.publicationQueue)) {
        if(queuedCollection.ajvValidationErrors?.length) {
          paratooSentryBreadcrumbMessage(`Skipping item at index '${index}' due to validation errors: ${JSON.stringify(queuedCollection)}`, 'dataManagerSubmitPublicationQueue')
          continue
        }
        this.queueSubmissionProgress = Number.parseInt(index)
        const currCollectionProtocol = apiModelsStore().protocolInformation({
          protId: queuedCollection.protocolId,
        })
        paratooSentryBreadcrumbMessage(
          `Processing item at index '${index}' for protocol '${
            currCollectionProtocol.name
          }' from publication queue: ${JSON.stringify(queuedCollection)}`,
          'dataManagerSubmitPublicationQueue',
        )
        paratooSentryBreadcrumbMessage(
          `submitResponses: ${JSON.stringify(this.submitResponses)}`,
          'dataManagerSubmitPublicationQueue',
        )
        paratooSentryBreadcrumbMessage(
          `responses: ${JSON.stringify(this.responses)}`,
          'dataManagerSubmitPublicationQueue',
        )
        paratooSentryBreadcrumbMessage(
          `errors: ${JSON.stringify(this.errors)}`,
          'dataManagerSubmitPublicationQueue',
        )
        paratooSentryBreadcrumbMessage(
          `skips: ${JSON.stringify(this.skips)}`,
          'dataManagerSubmitPublicationQueue',
        )

        const currQueuedCollectionFailedDeps = []
        for (const collectionDepUuid of queuedCollection.dependsOnQueuedCollections) {
          for (const err of this.errors) {
            if (err.queuedCollectionIdentifier === collectionDepUuid) {
              currQueuedCollectionFailedDeps.push({
                queuedCollectionIdentifier: err.queuedCollectionIdentifier,
                protocolInfo: err.protocolInfo,
                type: 'error',
              })
            }
          }

          for (const skip of this.skips) {
            if (skip.queuedCollectionIdentifier === collectionDepUuid) {
              currQueuedCollectionFailedDeps.push({
                queuedCollectionIdentifier: skip.queuedCollectionIdentifier,
                protocolInfo: skip.protocolInfo,
                type: 'skip',
              })
            }
          }
        }
        paratooSentryBreadcrumbMessage(
          `currQueuedCollectionFailedDeps: ${JSON.stringify(
            currQueuedCollectionFailedDeps,
          )}`,
          'dataManagerSubmitPublicationQueue',
        )
        if (currQueuedCollectionFailedDeps.length > 0) {
          const failedDepCollectionMsgs = []
          for (const failedDep of currQueuedCollectionFailedDeps) {
            //failures are also skipped, so add if not already
            if (
              !this.skips.some(
                (s) =>
                  s.queuedCollectionIdentifier ===
                  failedDep.queuedCollectionIdentifier,
              ) &&
              //also check errors - no point saying this collection is skipped if it was
              //the reason for a failure/error
              !this.errors.some(
                (e) =>
                  e.queuedCollectionIdentifier ===
                  failedDep.queuedCollectionIdentifier,
              )
            ) {
              this.skips.push({
                queuedCollectionIdentifier:
                  failedDep.queuedCollectionIdentifier,
                protocolInfo: failedDep.protocolInfo,
              })
            }

            failedDepCollectionMsgs.push(
              `${failedDep.protocolInfo.name} (queued collection ID: ${failedDep.queuedCollectionIdentifier})`,
            )
            const msg = `Item at index '${index}' for protocol '${
              currCollectionProtocol.name
            }' has collection(s) it depends on that failed to submit, so we're skipping submitting this item too. Failed dependencies: ${failedDepCollectionMsgs.join(
              ', ',
            )}`
            paratooSentryBreadcrumbMessage(
              msg,
              'dataManagerSubmitPublicationQueue',
            )
          }

          //this collection is skipped
          if (
            !this.skips.some(
              (s) =>
                s.queuedCollectionIdentifier ===
                queuedCollection.queuedCollectionIdentifier,
            )
          ) {
            this.skips.push({
              queuedCollectionIdentifier:
                queuedCollection.queuedCollectionIdentifier,
              protocolInfo: currCollectionProtocol,
              dependencies: failedDepCollectionMsgs,
            })
          }
        }

        //let all the resolving of relations/deps happen even if this collection has
        //failed deps, so that all deps of this collection DID succeed, we can still
        //resolve those now. We do this to avoid errors if we try access any data that
        //should have been resolved but wasn't
        const submitResponsesCollections = await combineCurrentAndHistoricalSubmitResponses({
          currentSubmitResponses: this.submitResponses,
        })
        // console.log('submitResponsesCollections:', JSON.stringify(submitResponsesCollections, null, 2))

        //resolve when layout/visit are in collection steps (separate models)
        //pass `queuedCollection` as a reference, so this function mutates it's values
        const queuedLayoutTempOfflineId = resolveLayoutCollectionStep(
          queuedCollection,
          submitResponsesCollections,
        )
        const queuedVisitTempOfflineId = resolveVisitCollectionStep(
          queuedCollection,
          submitResponsesCollections,
        )

        //resolve when layout/visit/voucher are stored as relations in model's fields
        for (let [modelName, value] of Object.entries(
          queuedCollection.collection,
        )) {
          const removedLayoutRelation = removeLayoutRelation(
            modelName,
            value,
            queuedCollection,
          )
          if (removedLayoutRelation) continue

          const modelFields = Object.keys(value)
          resolveLayoutRelation(
            modelName,
            value,
            modelFields,
            submitResponsesCollections,
            queuedLayoutTempOfflineId,
          )
          resolveVisitRelation(
            modelName,
            value,
            modelFields,
            submitResponsesCollections,
            queuedVisitTempOfflineId,
          )
          removeVoucherSurveyRelation(modelName, value)
          resolveVoucherRelation(modelName, value, submitResponsesCollections)
          resolveVertTrapRelation(modelName, value, submitResponsesCollections)
          removeTempCameraTrapSurvey(modelName, value)
          resolveSoilCharacterisationRelation(
            modelName,
            value,
            submitResponsesCollections,
          )

          removeGrassyWeedsSetupRelation(modelName, value)
        }

        //only try to submit if this collection doesn't have any deps that failed and no
        //in-progress dependencies
        if (
          currQueuedCollectionFailedDeps.length === 0
        ) {
          try {
            let resp = await this.checkIfCollectionSubmitted(index, dumpIsSent)
            if(!resp) {
              resp = await workflowSubmission(
                queuedCollection.workflowItem,
                queuedCollection.protocolId,
                useRouter(),
                queuedCollection.submoduleInfo,
                index,
              )
            } else {
              dumpIsSent = true
            }
            //TODO workaround - child obs are included as a separate model in the response, so we need to skip. Solution should dynamically map child ob field names to the collection names to skip
            const skipMap = [
              'cover-point-intercept-species-intercept',
              'fire-species-intercept',
              'camera-trap-feature',
              'camera-trap-information',
              'camera-trap-setting',
              'ground-counts-vantage-point-observation',
              'coarse-woody-debris-observation'
            ]
            if (Array.isArray(resp)) {
              //TODO is this case used any more?
              //array is response likely originates from /many call
              //TODO handle mapping if required (not needed at the moment)
              this.responses.push({
                protocolInfo: currCollectionProtocol,
                resp: resp,
                queuedCollectionIdentifier:
                  queuedCollection.queuedCollectionIdentifier,
              })
            } else if (resp) {
              //keep track of data from this collection that we may need to resolve in other
              //collections (that are not yet submit)
              let mappedQueuedCollection = { ...queuedCollection.collection }
              for (const [modelName, value] of Object.entries(resp)) {
                if (skipMap.includes(modelName)) continue
                if (
                  Array.isArray(value) &&
                  //also need to check the local-stored model data is array
                  Array.isArray(mappedQueuedCollection[modelName])
                ) {
                  //likely array of observations
                  for (const [index, ob] of Object.entries(value)) {
                    if (ob.id) {
                      Object.assign(mappedQueuedCollection[modelName][index], {
                        id: ob.id,
                      })
                    }
                    // resolve child obs
                    for (const [obAtr, atrValue] of Object.entries(ob)) {
                      if (!Array.isArray(atrValue)) continue
                      for (const [childObsIndex, childOb] of Object.entries(
                        atrValue,
                      )) {
                        if (
                          childOb.id &&
                          mappedQueuedCollection[modelName][index]?.[obAtr]
                        ) {
                          Object.assign(
                            mappedQueuedCollection[modelName][index]?.[obAtr][
                              childObsIndex
                            ],
                            {
                              id: childOb.id,
                            },
                          )
                        }
                      }
                    }
                  }
                } else {
                  if (value.id) {
                    Object.assign(mappedQueuedCollection[modelName], {
                      id: value.id,
                    })
                  }
                }
              }

              this.responses.push({
                protocolInfo: currCollectionProtocol,
                resp: resp,
                queuedCollectionIdentifier:
                  queuedCollection.queuedCollectionIdentifier,
              })

              this.submitResponses.push({
                protocolId: queuedCollection.protocolId,
                collection: mappedQueuedCollection,
                authContext: queuedCollection.authContext,
                protocolInfo: currCollectionProtocol,
                queuedCollectionIdentifier:
                  queuedCollection.queuedCollectionIdentifier,
              })
              submittedModelsToRefresh.push(queuedCollection.protocolId)
              delete this.queuedCollectionUuidWithAjvErrors[
                queuedCollection.queuedCollectionIdentifier
              ]
            }
          } catch (err) {
            let errStr = null
            try {
              errStr = err?.toString()
            } catch {
              errStr = null
            }
            this.errors.push({
              protocolInfo: currCollectionProtocol,
              queuedCollectionIdentifier:
                queuedCollection.queuedCollectionIdentifier,
              error: err,
              errorStr: errStr,
            })

            this.restoreCollectionFromBackupQueue(index)

            if (
              currQueuedCollectionFailedDeps.length > 0 &&
              index == this.publicationQueue.length - 1
            ) {
              console.debug('last queued item failed')
              //last item in queue failed, so skipped and needs to be tracked
              if (
                !this.skips.some(
                  (s) =>
                    s.queuedCollectionIdentifier ===
                    queuedCollection.queuedCollectionIdentifier,
                )
              ) {
                this.skips.push({
                  queuedCollectionIdentifier:
                    queuedCollection.queuedCollectionIdentifier,
                  protocolInfo: currCollectionProtocol,
                })
              }
            }
          }

          if (
            !queueHasPlotSelection &&
            currCollectionProtocol.name === 'Plot Selection'
          ) {
            queueHasPlotSelection = true
          }
        }
        
        if (inProgressCollectionsDependOnQueueIdentifiers?.[queuedCollection.queuedCollectionIdentifier]) {
          const submitResponsesCollections_ = await combineCurrentAndHistoricalSubmitResponses({
            currentSubmitResponses: this.submitResponses,
          })

          for (const dependentProtocol of inProgressCollectionsDependOnQueueIdentifiers[queuedCollection.queuedCollectionIdentifier]) {
            const resolvedName = apiModelsStore().protocolInformation({
              protId: Number.parseInt(dependentProtocol),
            })?.name
            if (!resolvedName) {
              paratooWarnMessage(
                `Unable to resolve protocol name with ID=${dependentProtocol} when checking if queuedCollectionIdentifier=${queuedCollection.queuedCollectionIdentifier} has in-progress dependencies`,
              )
            }
            paratooSentryBreadcrumbMessage(
              `Checking in-progress protocol ${resolvedName || ('ID=' + dependentProtocol)} for temp links to queue data`,
            )
            const inProgressData = bulkStore().collectionGetForProt({
              protId: Number.parseInt(dependentProtocol)
            })
            for (const [model, data] of Object.entries(inProgressData)) {
              //don't need to call any plot resolve functions, as
              // `checkAndUpdatePlotContext()` will take care of it
              resolveVoucherRelation(
                model,
                data,
                submitResponsesCollections_,
              )
              resolveVertTrapRelation(
                model,
                data,
                submitResponsesCollections_,
              )
              resolveSoilCharacterisationRelation(
                model,
                data,
                submitResponsesCollections_,
                true,
              )
            }
          }
        }
      }   //end of main loop over queue

      paratooSentryBreadcrumbMessage(`responses: ${JSON.stringify(this.responses)}`, 'dataManagerSubmitPublicationQueue')
      paratooSentryBreadcrumbMessage(`submitResponses: ${JSON.stringify(this.submitResponses)}`, 'dataManagerSubmitPublicationQueue')
      paratooSentryBreadcrumbMessage(`errors: ${JSON.stringify(this.errors)}`, 'dataManagerSubmitPublicationQueue')
      paratooSentryBreadcrumbMessage(`skips: ${JSON.stringify(this.skips)}`, 'dataManagerSubmitPublicationQueue')
      
      this.lastSyncTime = Date.now()
      paratooSentryBreadcrumbMessage(
        `Finished syncing: ${this.lastSyncTime.toString()}`,
      )

      //we do this right at the end of *all* collections so that any actions performed by
      //the bulk store don't get interrupted by data not being where we expect
      const failures = this.skips.map(s => s.queuedCollectionIdentifier).concat(
        this.errors.map(s => s.queuedCollectionIdentifier)
      )
      this.sendDumpOnFailures(failures)
      this.resetPublicationQueue(failures)
      const reducedMessage = (messages) => {
        return messages.map(s => {
          let base = `${s.protocolInfo.name} (queued collection ID: ${s.queuedCollectionIdentifier})`
          if (s.error) {
            base += `: ${s.error.message}`
          }
          return base
        }).join(', ')
      }

      let notificationMsg = ''
      let notificationType = null
      let icon
      let msg = '' 
      let canClearHistorical = false
      const skipMsg = `<br><b>Some collections were also skipped because another collection it they depended on failed. See output on Projects page for more details`
      if (this.responses.length > 0 && this.errors.length === 0 && this.skips.length === 0) {
        msg = 'Successfully submitted all queued collections.'
        notificationMsg = `${msg}: ${reducedMessage(this.responses)}`
        notificationType = 'positive'
        icon = 'check_circle'
        bulkStore().clearClientStateDump()

        //set this flag to manage further down
        canClearHistorical = true
      } else if (this.responses.length === 0 && this.errors.length > 0) {
        msg = 'Failed to submit all queued collections. Please see the failed to sync table.'
        notificationMsg = `${msg}: ${reducedMessage(this.errors)}`
        if (this.skips.length > 0) {
          notificationMsg += skipMsg
        }
        notificationType = 'negative'
        icon = 'warning'
      } else if (this.responses.length > 0 && this.errors.length > 0) {
        const warningMsg = `Queued collections didn't all succeed on submission. `
        msg = warningMsg + `See the table for failed to sync collections and click 'Open details' for successful collections.`
        notificationMsg = `${warningMsg}<br><b>Failures</b>: ${reducedMessage(this.errors)}.<br><b>Successes</b>: ${reducedMessage(this.responses)}`
        if (this.skips.length > 0) {
          notificationMsg += skipMsg
        }
        notificationType = 'warning'
        icon = 'priority_high'

        //set this flag to manage further down (we will only move `responses` and
        //`submitResponses` that the queue doesn't need)
        canClearHistorical = true
      } else if (this.skips.length > 0) {
        notificationType = 'negative'
        icon = 'warning'
        msg = `${this.responses.length > 0 ? 'Some' : 'All'} queued collections were skipped`
      } else {
        paratooWarnMessage(
          `Unhandled case when generating post-sync message. lengths of various data: responses=${this.responses.length}, errors=${this.errors.length}. skips=${this.skips.length}`
        )
      }
      paratooSentryBreadcrumbMessage(
        `notificationMsg=${JSON.stringify(notificationMsg)}. msg=${JSON.stringify(msg)}`,
        'dataManagerSubmitPublicationQueue'
      )

      const orgMintedUUIDs = [];
      // get org minted uuids from responses
      for (const response of this.responses) {
        const uuid = findValForKey(response, 'orgMintedUUID');
        if (uuid !== undefined) {
          orgMintedUUIDs.push(uuid);
        }
      }
      
      const collections = reducedMessage(this.responses)
      notifyWithActions(
        notificationType,
        icon,
        // reducedMessage(this.responses),
        notificationType === 'positive'
          ? 5 //success not as long of a message
          : 10, //longer timeout for longer error/warning message
        true, //msg contains HTML
        msg,
        (notificationType === 'warning' || notificationType === 'positive') ?[{
          label: "Open details",
          color: notificationType === 'warning' ? "black" : "yellow",
          handler: () =>
            Dialog.create({
              component: ProfileCollectionDetails,
              componentProps: {
                orgMintedUUIDs: orgMintedUUIDs,
                msg:collections,
              },
            }),
        }]:[],
      )
      if (canClearHistorical) {
        const historicalSubmitResponses = await historicalDataManagerDb.getAllTableData({
          table: 'submitResponses'
        })
        checkAndUpdatePlotContext({
          submitResponses: this.submitResponses,
          //`map` to remove extra metadata we don't need here and append flag
          historicalSubmitResponses: historicalSubmitResponses.map(h => {
            return {
              ...h.data,
              isHistorical: true,
            }
          }),
        })

        await this.clearHistorical()
      }

      if (queueHasPlotSelection) {
        //Plot Selection may assign new plots to projects, so we need to refresh the
        //projects so the app has this information
        await authStore().refreshProjectsAndProtocols()
      }

      this.refreshCollectionsPostSync(submittedModelsToRefresh)
    },
    async sendDumpOnFailures(failures) {
      if(failures?.length === 0 || !Array.isArray(failures)) return
      
      const { clientStateUuid, debugClientStateDumpUrl, sentStateUids } = bulkStore()
      const sentDumpId = `${cc.coreApiPrefix}/debug-client-state-dumps/${sentStateUids.at(-1)}` || debugClientStateDumpUrl
      const urlSuffix = `${sentDumpId}?client_state_uuid=${clientStateUuid}`
      if(clientStateUuid) {
        const sentStateDump = await doCoreApiGet({ urlSuffix })
        const sentDataManagerState = sentStateDump.data.attributes.client_state_data_manager
        const sentFailures = sentDataManagerState.skips.map(s => s.queuedCollectionIdentifier).concat(
          sentDataManagerState.errors.map(s => s.queuedCollectionIdentifier)
        )
        const hasNoChanges = isEqual(failures, sentFailures)
        if(hasNoChanges) {
          paratooWarnMessage(`Sync error, same error as before, not sending a new file. Dump file ID: ${sentDumpId}`)
          return
        }
      }
      const dumpId =  await postStoreState(cc.dumpOrigins.SYNC_FAILURE, {}, true)
      this.sentStateDump
      paratooWarnMessage(`Sync error. Dump file ID: ${dumpId}`)

    },
    async checkIfCollectionSubmitted(queuedCollectionIndex, dumpIsSent) {
      const collection = this.publicationQueue[queuedCollectionIndex].collection
      let orgMintedIdentifier = collection.orgMintedIdentifier
      // if there's no orgMintedIdentifier, the collection hasn't been submitted, no need to make any requests
      if(!orgMintedIdentifier) return false

      // check core api for submitted collection
      orgMintedIdentifier = JSON.parse(atob(orgMintedIdentifier)) 
      const orgMintedUuid = orgMintedIdentifier.survey_metadata?.orgMintedUUID
      if(!orgMintedUuid) {
        paratooErrorHandler(
          `Something went wrong at index ${queuedCollectionIndex} in dataManager publicationQueue, we have orgMintedIdentifier, but no orgMintedUUID can't be found after decoded`,
          new Error('orgMintedUUID is undefined')
        )
        // still let the app make the request as the submit function have better error handling
        return
      }

      const orgUuidPrefix = `/api/org-uuid-survey-metadatas?filters[org_minted_uuid][$eq]=${orgMintedUuid}`
      const orgUuidRes = await doCoreApiGet({ urlSuffix: orgUuidPrefix })
      if(orgUuidRes?.data?.length === 1) {
        // the collection has been submitted, 
        // want to send the dump first just in case, and we only want to do it once per transaction
        if(!dumpIsSent) {
          try {
            const dumpFileId = await postStoreState(cc.dumpOrigins.AUTOHEAL)
            paratooWarnMessage(`submitted collection result at index ${queuedCollectionIndex} already exists. Dump file ID: ${dumpFileId}, skipping...`)
          } catch (error) {
            paratooErrorHandler(`Failed to send dump at index ${queuedCollectionIndex}, abort skipping`, error)
            return false
          }
        }

        try {
          const submittedBulkCollection = await doCoreApiPost({
            urlSuffix: '/api/protocols/reverse-lookup',
            body: {
              org_minted_uuid: orgMintedUuid
            }
          })
          if(!submittedBulkCollection?.collections) return false
          // return the submitted collections data similar to bulk response
          return [submittedBulkCollection.collections]
        } catch (error) {
          paratooErrorHandler(`Failed to get submitted data at index ${queuedCollectionIndex}, abort skipping`, error)
          return false
        }
      }
    },
    /**
     * 
     * @param {Array} modelsToRefresh Array of successfully submitted models to refresh
     */
    async refreshCollectionsPostSync(protocolIds) {
      const { populateApiModels, populateVerifiedModels, modelsOfProtocol } = apiModelsStore()
      const { setApiModelsArePopulating } = useEphemeralStore()

      let submittedModelsToRefresh = []
      const promises = protocolIds.map(async protocolId => {
        const models = await modelsOfProtocol({protocolId})
        models.forEach(model => {
          if(cc.allModelsToRefresh.includes(model) && !submittedModelsToRefresh.includes(model)) {
            submittedModelsToRefresh.push(model)
          }
        })
      })
      await Promise.all(promises)

      console.log('submittedModelsToRefresh:', JSON.stringify(submittedModelsToRefresh, null, 2))

      //populate `protocol` model as it contains workflow defs that might need refreshing
      await populateApiModels({
        forceRefresh: true,
        modelsToRefresh: apiLists.modelsToPopulate.appInitialization,
      })

      try {
        setApiModelsArePopulating(true)
        await populateVerifiedModels({
          modelNames: submittedModelsToRefresh,
          useCache: false
        })
      }       
      finally {
        setApiModelsArePopulating(false)
      }
      
    },
    /**
     * Resets the publication queue and keeps collections that might have failed
     * 
     * @param {Array.<String>} failedCollections the list of collection UUIDs that failed
     */
    resetPublicationQueue(failedCollections = []) {
      if (
        !failedCollections ||
        (
          Array.isArray(failedCollections) &&
          failedCollections.length === 0
        )
      ) {
        //no collections failed so can completely reset
        paratooSentryBreadcrumbMessage('Hard resetting the publication queue', 'dataManagerResetPublicationQueue')
        this.publicationQueue = this.publicationQueue.filter(queueItem => queueItem.ajvValidationErrors?.length)
        this.publicationQueueBackup = this.publicationQueueBackup.filter(queuedItem => queuedItem.ajvValidationErrors?.length)
      } else {
        paratooSentryBreadcrumbMessage(`Resetting the publication queue but keeping failures and skips: ${failedCollections}`, 'dataManagerResetPublicationQueue')
        //any collections that failed need to remain in the queue
        const newQueue = []
        for (const queuedCollection of this.publicationQueue) {
          if (failedCollections.includes(queuedCollection.queuedCollectionIdentifier) || queuedCollection.ajvValidationErrors?.length) {
            newQueue.push(queuedCollection)
          }
        }
        this.publicationQueue = newQueue
        this.publicationQueueBackup = newQueue
      }
      this.queueSubmissionProgress = null
    },
    /**
     * Restore a given collection from the backup queue
     * 
     * We need to restore items as a given collection might have already been wrapped in
     * the `data` object for submission, or had some (or all) photos submit. So we
     * 'reset' this item to try again.
     * 
     * @param {Number} collectionIndex the index of the queue item to restore
     */
    restoreCollectionFromBackupQueue(collectionIndex) {
      paratooSentryBreadcrumbMessage(`Restoring collection at index=${collectionIndex} with queued collection identifier '${this.publicationQueueBackup[collectionIndex]?.queuedCollectionIdentifier}'`, 'dataManagerRestoreCollectionFromBackupQueue')
      const orgMintedIdentifierToRestore = cloneDeep(this.publicationQueue?.[collectionIndex]?.collection?.orgMintedIdentifier)
      if(this.publicationQueueBackup[collectionIndex]) {
        this.publicationQueue[collectionIndex] = cloneDeep(this.publicationQueueBackup[collectionIndex])
      } else {
        paratooErrorHandler(`Cannot restore collection at index=${collectionIndex}`, new Error('No collection at index to restore'))
      }
      
      if (
        orgMintedIdentifierToRestore &&
        !this.publicationQueue?.[collectionIndex]?.collection?.orgMintedIdentifier
      ) {
        paratooSentryBreadcrumbMessage(`Restoring orgMintedIdentifier for collection at index=${collectionIndex}`, 'dataManagerRestoreCollectionFromBackupQueue')
        this.publicationQueue[collectionIndex].collection.orgMintedIdentifier = orgMintedIdentifierToRestore
      }
    },
    getDataFromModelAndId(modelName, id) {
      const model = this.cachedModel({ modelName })
      if(!model.length) return null
      return model.find(m => m?.id === id)
    },
    updateCollectionOrgMintedIdentifier({ collectionIndex, orgMintedIdentifier }) {
      if (!this.publicationQueue?.[collectionIndex]?.collection) {
        return paratooErrorHandler(`Cannot update orgMintedIdentifier for collection at index=${collectionIndex}`, new Error('No collection at index to update'))
      }
      if (this.publicationQueue?.[collectionIndex]?.collection?.orgMintedIdentifier) {
        return paratooWarnMessage(`Collection at index=${collectionIndex} already has orgMintedIdentifier, so no need to add again`)
      }
      this.publicationQueue[collectionIndex].collection.orgMintedIdentifier = orgMintedIdentifier
    },
    /**
     * This function is used get get the data of plot layout that contain only fauna plot in the queue
     * This happen when user submit a core plot first without fauna then submit the fauna plot after
     * but both of these collections have not been synced yet
     * @param {*} collectionIdObject { temp_offline_id: String }
     */
    getQueuedAmendedFaunaPlotCollection(collectionIdObject) {
      const faunaQueuedItem = this.publicationQueue.find(({collection}) => {
        if(!collection['plot-layout']) return
        const plotLayoutId = collection['plot-layout'].id
        return isEqual(plotLayoutId, collectionIdObject)
      })
      if(!faunaQueuedItem) {
        const plotLayoutCollection = this.publicationQueue.filter(({collection}) => collection?.['plot-definition-survey'])
        paratooSentryBreadcrumbMessage(`Current queue: ${JSON.stringify(plotLayoutCollection)}`, 'dataManagerGetQueuedAmendedFaunaPlotCollection')
        paratooSentryBreadcrumbMessage(`Fauna plot with id ${JSON.stringify(collectionIdObject)} not found in queue`)
        return null
      }
      return faunaQueuedItem.collection
    },
    /**
     * Wrapped that calls helpers that move all `responses` and `submitResponses` to
     * IndexDB (Dexie)
     */
    async clearHistorical() {
      await logEstimatedQuota()

      try {
        const responsesSuccess = await historicalDataManagerDb.bulkAddRespData({
          table: 'responses',
          dataArr: this.responses,
        })
        if (responsesSuccess) {
          const newResponsesArr = []
          for (const response of this.responses) {
            if (
              this.publicationQueue.some(
                q => q.dependsOnQueuedCollections.includes(
                  response.queuedCollectionIdentifier
                )
              )
            ) {
              paratooSentryBreadcrumbMessage(
                `Skipping moving response with queued collection ID ${response.queuedCollectionIdentifier} to IndexDB - item(s) in queue still depends on it`,
                'dataManagerClearHistorical',
              )
              newResponsesArr.push(response)
            } else {
              paratooSentryBreadcrumbMessage(
                `Response with queued collection ID ${response.queuedCollectionIdentifier} will be moved to IndexDB`,
                'dataManagerClearHistorical',
              )
            }
          }
          this.responses = cloneDeep(newResponsesArr)
        } else {
          paratooWarnMessage(
            `Failed when adding responses (${this.responses?.length} number of items) data to IndexDB`,
          )
        }
      } catch (err) {
        paratooErrorHandler(
          'Failed to clear responses data',
          err,
        )
      }
      
      try {
        const submitResponsesSuccess = await historicalDataManagerDb.bulkAddRespData({
          table: 'submitResponses',
          dataArr: this.submitResponses,
        })
        if (submitResponsesSuccess) {
          const newSubmitResponsesArr = []
          for (const submitResponse of this.submitResponses) {
            if (
              this.publicationQueue.some(
                q => q.dependsOnQueuedCollections.includes(
                  submitResponse.queuedCollectionIdentifier
                )
              )
            ) {
              paratooSentryBreadcrumbMessage(
                `Skipping moving submit response with queued collection ID ${submitResponse.queuedCollectionIdentifier} to IndexDB - item(s) in queue still depends on it`,
                'dataManagerClearHistorical',
              )
              newSubmitResponsesArr.push(submitResponse)
            } else {
              paratooSentryBreadcrumbMessage(
                `Submit response with queued collection ID ${submitResponse.queuedCollectionIdentifier} will be moved to IndexDB`,
                'dataManagerClearHistorical',
              )
            }
          }
          this.submitResponses = cloneDeep(newSubmitResponsesArr)
        } else {
          paratooWarnMessage(
            `Failed when adding submit responses (${this.submitResponses?.length} number of items) data to IndexDB`,
          )
        }
      } catch (err) {
        paratooErrorHandler(
          'Failed to clear submit responses data',
          err,
        )
      }

      await logEstimatedQuota()
    },
  },
  getters: {
    queuedCollectionsWithoutAjvErrors: state => {
      if(!state.publicationQueue.length) return []
      else return state.publicationQueue.filter(item => !item.ajvValidationErrors?.length)
    },
    /**
     * Wraps the API Models Store `cachedModel` getter and appends data for the same
     * model name that is stored in the publication queue
     * 
     * @param {String} modelName the model name of the data to grab
     * @param {Boolean} [publicationQueueOnly] whether to only grab from the queue
     * @param {Object} [childObData] an object containing child observation information
     * @param {String} [childObData.parentModel] the child ob's parent model name
     * @param {String} [childObData.childObFieldName] the relational field name used in
     * the parent model that refers to the child ob
     * 
     * @returns {*} the cached data for the model name
     */
    cachedModel: (state) => {
      return ({ modelName, publicationQueueOnly = false, childObData = null }) => {
        const pendingOffline = state.publicationQueue.reduce((accum, curr) => {
          if (curr.collection[modelName]) {
            if (Array.isArray(curr.collection[modelName])) {
              //likely array of observations
              for (const collectionItem of curr.collection[modelName]) {
                if (!collectionItem.id) {
                  //only include observations without IDs - they should all not have, but
                  //do the check anyway
                  accum.push(collectionItem)
                }
              }
            } else if (!curr.collection[modelName].id) {
              const collectionKeys = Object.keys(curr.collection[modelName])
              if (
                //don't include if the only key is `temp_offline_id`; if this is the only
                //key, then it's just a reference to a model collected offline and thus
                //isn't included
                (
                  collectionKeys.includes('temp_offline_id') &&
                  collectionKeys.length > 1
                ) ||
                (
                  !collectionKeys.includes('temp_offline_id')
                )
              ) {
                accum.push(curr.collection[modelName])
              }
            }
          }
          if (
            childObData !== null &&
            curr.collection[childObData.parentModel]
          ) {
            let cachedChildObData = null
            if (Array.isArray(curr.collection[childObData.parentModel])) {
              //likely parent model is array of observations, so look in each for the
              //child ob
              for (const collectionItem of curr.collection[childObData.parentModel]) {
                if (!collectionItem[childObData.childObFieldName].id) {
                  if (cachedChildObData === null) {
                    //this is the first match we've found so set an array to push to
                    cachedChildObData = []
                  }
                  if (Array.isArray(collectionItem[childObData.childObFieldName])) {
                    //multi child ob
                    cachedChildObData.push(...collectionItem[childObData.childObFieldName])
                  } else {
                    //single child ob
                    cachedChildObData.push(collectionItem[childObData.childObFieldName])
                  }
                }
              }
            } else if (
              curr.collection[childObData.parentModel][childObData.childObFieldName] &&
              !curr.collection[childObData.parentModel][childObData.childObFieldName].id
            ) {
              cachedChildObData = curr.collection[childObData.parentModel][childObData.childObFieldName]
            }

            if (
              //should be sufficient to just check not null, as we set the array only if
              //we've also going to push to it
              cachedChildObData !== null
            ) {
              if (Array.isArray(cachedChildObData)) {
                accum.push(...cachedChildObData)
              } else {
                accum.push(cachedChildObData)
              }
            }
          }
          return accum
        }, [])

        if (publicationQueueOnly) {
          return pendingOffline
        }

        const apiModelsResp = apiModelsStore().cachedModel({ modelName: modelName }) || []
        return apiModelsResp.concat(pendingOffline)
        
      }
    },
    /**
     * Wrapper for Bulk store's `collectionGetForProt`
     * 
     * @param {Number} protId the protocol ID to retrieve the collection for
     * 
     * @returns {Object} the collection
     */
    collectionGetForProt: (state) => {
      return ({ protId }) => {
        const bulkStoreResp = bulkStore().collectionGetForProt({ protId: protId })
        if (!bulkStoreResp || isEmpty(bulkStoreResp)) {
          const collections = state.publicationQueue.reduce((accum, curr) => {
            if (curr.protocolId === protId) {
              accum.push(curr.collection)
            }
            return accum
          }, [])
          if (collections.length > 1) {
            paratooWarnMessage(`Data manager attempted to retrieve collection for protocol with ID=${protId}, but found multiple, so returning the latest-queued one`)
            return collections.at(-1)
          }
          return collections[0]
        }
        return bulkStoreResp
      }
    },
    /**
     * Retrieves a specific collection from the publication queue by index
     * 
     * @param {Number} index the index to grab from
     * 
     * @returns {Object | null} the collection that was retrieved, if it's at the index
     */
    queuedCollectionFromIndex: (state) => {
      return ({ index }) => {
        return state.publicationQueue[index].collection || null
      }
    },
    /**
     * Retrieves a specific collections' auth context from the publication queue by index
     * 
     * @param {Number} index the index to grab from
     * 
     * @returns {Object | null} the auth context that was retrieved, if it's at the index
     */
    authContextForQueuedCollection: (state) => {
      return ({ index }) => {
        return state.publicationQueue[index].authContext || null
      }
    },
    /**
     * Checks if there are collections in the publication queue
     * 
     * @returns {Boolean} whether the publication queue is populated
     */
    pendingQueuedCollections: state => {
      return state.publicationQueue.length > 0
    },
    /**
     * Creates summary data for a q-table to consume - displays the protocol, plot,
     * visit, and project
     * 
     * @returns {Array.<Object>} a list of the table data
     */
    pendingQueuedCollectionsSummary: state => {
      let cachedLayouts = state.cachedModel({ modelName: 'plot-layout' })
      let cachedVisits = state.cachedModel({ modelName: 'plot-visit' })
      const cachedSelections = state.cachedModel({ modelName: 'plot-selection' })

      //if there's submit responses, there's likely to be items in the queue that depend
      //on these. This is typically fine, but we need to resolve plots from these submit
      //responses so add them to the existing cached list
      if (state.submitResponses.length > 0) {
        const submitLayouts = []
        const submitVisits = []
        for (const submitResponse of state.submitResponses) {
          if (submitResponse.collection['plot-layout']) {
            submitLayouts.push(submitResponse.collection['plot-layout'])
          }
          
          if (submitResponse.collection['plot-visit']) {
            submitVisits.push(submitResponse.collection['plot-visit'])
          }
        }

        cachedLayouts = cachedLayouts.concat(submitLayouts)
        cachedVisits = cachedVisits.concat(submitVisits)
      }

      const summary = state.publicationQueue
        .filter(queuedCollection => {
          const currCollectionProtocol = apiModelsStore().protocolInformation({ protId: queuedCollection.protocolId})
          //hidden protocols should not show in summary - they are submitable, but
          //typically the parent module takes care of handling everything
          //e.g., if doing Cover+Fire, don't need to also show Fire Survey as we're
          //already indicating we're doing fire by the parent module
          return !currCollectionProtocol.isHidden
        })
        .map((queuedCollection, index) => {
          const currCollectionProtocol = apiModelsStore().protocolInformation({ protId: queuedCollection.protocolId})

          let plot = 'n/a'
          let plotIsNew = false
          let visit = 'n/a'
          let visitIsNew = false
          let newFaunaPlot = false

          if (queuedCollection.collection['plot-layout']) {
            if (
              queuedCollection.collection['plot-layout']?.id &&
              Number.isInteger(queuedCollection.collection['plot-layout'].id)
            ) {
              //selected existing plot that has done round-trip
              plot = cachedLayouts
                .find(o => o.id === queuedCollection.collection['plot-layout'].id)?.plot_selection?.plot_label
            } else if (
              queuedCollection.collection['plot-layout']?.plot_selection &&
              Number.isInteger(queuedCollection.collection['plot-layout'].plot_selection)
            ) {
              //brand-new plot layout
              plot = cachedSelections
                .find(o => o.id === queuedCollection.collection['plot-layout'].plot_selection)?.plot_label
              plotIsNew = true
            } else if (queuedCollection.collection['plot-layout']?.id?.temp_offline_id) {
              //selected existing plot that has not done round trip
              const relevantSelectionId = cachedLayouts
                .find(o => o.temp_offline_id === queuedCollection.collection['plot-layout'].id.temp_offline_id)?.plot_selection
              plot = cachedSelections
                .find(o => o.id === relevantSelectionId)?.plot_label
            } else if (queuedCollection.collection['plot-layout']?.temp_offline_id) {
              //selected existing plot that has not done round trip (duplicate but similar
              //case as above)
              const relevantSelectionId = cachedLayouts
                .find(o => o.temp_offline_id === queuedCollection.collection['plot-layout'].temp_offline_id)?.plot_selection
              plot = cachedSelections
                .find(o => o.id === relevantSelectionId)?.plot_label
            } else {
              console.warn(`pendingQueuedCollectionsSummary() tried to resolve a Plot Layout for queue index=${index} but couldn't`)
            }
            if(queuedCollection.collection['plot-layout']?.fauna_plot_point?.length > 0) {
              newFaunaPlot = true
            }
          }

          if (queuedCollection.collection['plot-visit']) {
            if (
              queuedCollection.collection['plot-visit']?.id &&
              Number.isInteger(queuedCollection.collection['plot-visit'].id)
            ) {
              //selected existing visit that has done round-trip
              visit = cachedVisits
                .find(o => o.id === queuedCollection.collection['plot-visit'].id)?.visit_field_name
            } else if (queuedCollection.collection['plot-visit']?.visit_field_name) {
              //brand-new visit
              visit = queuedCollection.collection['plot-visit'].visit_field_name
              visitIsNew = true
            } else if (queuedCollection.collection['plot-visit']?.id?.temp_offline_id) {
              //selected existing visit that has not done round trip
              visit = cachedVisits
                .find(o => o.temp_offline_id === queuedCollection.collection['plot-visit'].id.temp_offline_id)?.visit_field_name
            } else if (queuedCollection.collection['plot-visit']?.temp_offline_id) {
              //selected existing visit that has not done round trip (duplicate but similar
              //case as above)
              visit = cachedVisits
                .find(o => o.temp_offline_id === queuedCollection.collection['plot-visit'].temp_offline_id)?.visit_field_name
            } else {
              console.warn(`pendingQueuedCollectionsSummary() tried to resolve a Plot Visit for queue index=${index} but couldn't`)
            }
          }

          const resolvedProject = authStore().projAndProt.find(
            o => o.id === queuedCollection.authContext.project
          )

          let newPlotLabel = ''
          if (plotIsNew && newFaunaPlot) {
            newPlotLabel = ' (new core and fauna plot)'
          } else if (newFaunaPlot) {
            newPlotLabel = ' (new fauna plot)'
          } else if (plotIsNew) {
            newPlotLabel = ' (new core plot)'
          } else {
            paratooSentryBreadcrumbMessage(
              'plots and visit are already exist',
              'pendingQueuedCollectionsSummary',
            )
          }

          return {
            protocol: currCollectionProtocol.name,
            plot: `${plot}${newPlotLabel}`,
            visit: `${visit}${visitIsNew ? ' (new visit)' : ''}`,
            project: resolvedProject?.name,
            ajvErrors: queuedCollection.ajvValidationErrors
          }
        })
      return summary
    },
    /**
     * Checks if there are failed collections pending upload
     * 
     * @returns {Boolean} if there is a failed pending collection
     */
    pendingFailedCollections: state => {
      return state.errors.length > 0 || state.skips.length > 0
    },
    /**
     * Creates a summary of failed collections
     * 
     * @returns {Array.<Object>} the list of failures format for a q-table
     */
    failedQueuedCollectionsSummary: state => {
      const failureOrSkipSummary = []
      
      for (const e of state.errors) {
        let issue = null
        if (e?.error?.msg) {
          issue = e.error.msg
        } else if (e?.error?.message) {
          issue = e.error.message
        } else if (typeof e === 'string') {
          issue = e
        } else if (e?.errorStr) {
          issue = e.errorStr
        } else {
          issue = 'Error with submission'
        }

        failureOrSkipSummary.push({
          protocol: `${e.protocolInfo.name} (queued collection ID: ${e.queuedCollectionIdentifier})`,
          issue: issue,
        })
      }

      for (const s of state.skips) {
        const baseMsg = s.skipReason ? s.skipReason : 'Skipped due to failure of dependent protocol(s)'
        failureOrSkipSummary.push({
          protocol: `${s.protocolInfo.name} (queued collection ID: ${s.queuedCollectionIdentifier})`,
          issue: `${baseMsg}: ${s.dependencies.join(', ')}`,
        })
      }

      return failureOrSkipSummary
    },
    /**
     * Retrieves the floristics voucher object based on it's ID
     * 
     * @param {Number | Object} ID the voucher ID or the object w/ key `temp_offline_id`
     * @param {String} protocolVariant the voucher's variant - i.e., 'full' or 'lite'
     * 
     * @returns {Object} the full voucher object
     */
    voucherFromID: (state) => {
      return ({ ID, protocolVariant }) => {
        if (typeof ID === 'object' && ID.temp_offline_id) {
          const vouchers = state.cachedModel({
            modelName: `floristics-veg-voucher-${protocolVariant}`
          })
          return vouchers.find(
            (v) => {
              return v.temp_offline_id === ID.temp_offline_id
            }
          )
        } else {
          return apiModelsStore().voucherFromID({
            ID: ID,
            protocolVariant: protocolVariant,
          })
        }
      }
    },
    /**
     * Retrieves all surveys part of a given plot by following the survey->visit->plot relation
     * 
     * @param {String} modelName the survey model name
     * @param {Number | String} plotLayoutId the Number (relation) ID if the plot was
     * collected online, or a string of the temp offline UUID if the survey was collected
     * offline
     * 
     * @returns {Array} an array of survey(s) for the plot
     */
    surveyDataFromPlot: (state) => {
      return ({ modelName, plotLayoutId }) => {
        const surveys = state.cachedModel({ modelName: modelName })
        const cachedLayouts = state.cachedModel({ modelName: 'plot-layout' })
        return surveys.filter((s) => {
          let layoutForVisit = null
          // already submitted
          if (s.plot_visit?.id) {
            layoutForVisit = s.plot_visit?.plot_layout?.id === plotLayoutId
          } else {
            //offline stuffs
            //visit and/or layout collected offline - need to resolve
            for (const queuedCollection of state.publicationQueue) {
              if (
                //ensure grab only collections with relevant model
                queuedCollection.collection[modelName] &&
                //now look in that collection's visit for a temp relation to the layout
                (
                  queuedCollection.collection['plot-visit']?.plot_layout === plotLayoutId ||
                  queuedCollection.collection['plot-layout']?.temp_offline_id === plotLayoutId ||
                  queuedCollection.collection['plot-visit']?.plot_layout?.temp_offline_id === plotLayoutId ||
                  queuedCollection.collection['plot-layout']?.id === plotLayoutId
                )
              ) {
                layoutForVisit = cachedLayouts.some(o => {
                  if (o.temp_offline_id) {
                    return o.temp_offline_id === plotLayoutId
                  } else {
                    return o.id === plotLayoutId
                  }
                })

              }
            }
          }

          //if this gets set by any case, we found, else it's null/undefined (falsy), so
          //it's not been found and `find` will keep searching
          return layoutForVisit
        })
      }
    },
    /**
     * Retrieves all surveys part of a given plot by following the survey->visit->plot relation
     * 
     * @param {String} obsModelName the observation model name
     * @param {Number | Object} survey_metadata the Number (relation) ID if the plot was
     * collected online, or an object with key `id` and value containing a temp offline
     * UUID if the plot was collected offline
     * 
     * @returns {Array} an array of observation(s) for the survey
     */
    obsDataFromSurvey: (state) => {
      return ({ obsModelName, survey_metadata }) => {
        const obs = state.cachedModel({ modelName: obsModelName })

        let idToLookFor = survey_metadata
        if (survey_metadata.id) {
          //want the raw ID (relation int or temp offline UUID), not whole object
          idToLookFor = survey_metadata.id
        }

        const associatedObs = obs.filter((entry) => {
          // look for survey id in entry
          // where the props has survey_metadata object
          const id = Object.values(entry)?.reduce((acc, prop) => {
            if (prop?.survey_metadata) {
              if (prop.id) {
                return prop.id
              }
              if (prop.temp_offline_id) {
                return prop.temp_offline_id
              }
            }
            return acc
          }, null)
          return id === idToLookFor
        })
        return associatedObs
      }
    },
    /**
     * get a value from a plot layout record based on the key
     * plotLayoutId is default to current plot if not provided
     * @param {*} {field:string, plotLayoutId = null} 
     * @returns 
     */
    getPlotSelectionData(state) {
      return ({ field, plotLayoutId = null }) => {
        if (!plotLayoutId) {
          plotLayoutId = bulkStore().getPlotLayoutId
          if (!plotLayoutId) return
        }

        const found = state.cachedModel({ modelName:'plot-layout' }).find(
          (layout) =>
            //need to check value exists before checking against `plotLayoutId`, else we
            //might be comparing nulls/undefined
            (
              layout?.id &&
              layout.id === plotLayoutId
            ) ||
            (
              layout?.temp_offline_id &&
              layout.temp_offline_id === plotLayoutId
            ) ||
            (
              layout?.temp_offline_id &&
              plotLayoutId?.temp_offline_id &&
              layout.temp_offline_id === plotLayoutId.temp_offline_id
            )
        )

        if (Number.isInteger(found?.plot_selection)) {
          //layout is storing only integer relation to selection, which means the layout
          //was collected offline and thus has not round-trip, which resolves the full Plot
          //Selection relation
          const resolvedSelection = state.cachedModel({ modelName: 'plot-selection' }).find(
            p => p.id === found.plot_selection
          )
          return resolvedSelection[field]
        }
        return found?.plot_selection[field]
      }
    },
    /**
     * Retrieves the current plot context's Plot Selection's UUID
     * 
     * @returns {String} the Plot's UUID
     */
    currentPlotSelectionUuid(state) {
      return state.getPlotSelectionData({ field: 'uuid' })
    },
    /**
     * Retrieves the current plot context's Plot Selection's plot label
     * 
     * @returns {String} the Plot's label
     */
    currentPlotLabel(state) {
      return state.getPlotSelectionData({ field: 'plot_label' })
    },
    plotHasFaunaPlot: (state) => {
      const { staticPlotContext } = bulkStore()

      // non plot based protocol or no plot has been selected
      if (!staticPlotContext) return undefined

      // for plot data still in the queue
      if(staticPlotContext?.plotLayout?.fauna_plot_point) return true
      const layout =  state.cachedModel({ modelName: 'plot-layout' })
      const selectedPlot = layout.find((layout) => layout?.id === staticPlotContext?.plotLayout?.id)
      if (!selectedPlot) return undefined
      const faunaPlotData = selectedPlot['fauna_plot_point']
      const hasFauna = faunaPlotData?.length > 0

      return hasFauna
    },
    getPlotDataFromContext: (state) => {
      const ephemeralStore = useEphemeralStore()
      return ({ modelName }) => {
        //check if a single model entry (e.g., a visit or layout) matches the ID (or
        //temp_offline_id) of the context
        const modelDatumMatchesContext = (o, flattenedPlotId) => {
          if (o?.id) {
            return o.id === flattenedPlotId
          }

          if (!o.id && o?.temp_offline_id) {
            return o.temp_offline_id === flattenedPlotId
          }
        }

        const modelData = state.cachedModel({ modelName: modelName })
        let expanded
        switch (modelName) {
          case 'plot-layout': {
            const plotLayoutContext = cloneDeep(bulkStore().getPlotLayoutId)
            const flattenedPlotLayoutId = flattenPlotDataId(plotLayoutContext)
            expanded = modelData.find((o) => {
              return modelDatumMatchesContext(o, flattenedPlotLayoutId)
            })
            
            if (expanded) {
              //try resolve the plot label
              const collectionPlotLabel = state.getPlotSelectionData({
                field: 'plot_label',
                plotLayoutId: flattenedPlotLayoutId,
              })
              if (collectionPlotLabel) {
                Object.assign(expanded, {
                  plot_label: collectionPlotLabel,
                })
              } else {
                const plotSelectionIsPopulating = ephemeralStore.modelIsPopulating({ modelName: 'plot-selection' })
                const plotLayoutIsPopulating = ephemeralStore.modelIsPopulating({ modelName: 'plot-layout' })
                if (
                  !plotSelectionIsPopulating &&
                  !plotLayoutIsPopulating
                ) {
                  paratooWarnMessage(`Unable to get plot label for layout data with flattened ID=${flattenedPlotLayoutId}`)
                } else {
                  paratooSentryBreadcrumbMessage(`Skipping warning about missing plot label with flattened ID=${flattenedPlotLayoutId} as data is still populated. plotSelectionIsPopulating=${plotSelectionIsPopulating}, plotLayoutIsPopulating=${plotLayoutIsPopulating}`)
                }
              }

              //append flattened ID for comparison in some situations
              Object.assign(expanded, {
                flattened_id: flattenedPlotLayoutId,
              })
            } else if (!expanded && !isEmpty(plotLayoutContext)) {
              if (
                !ephemeralStore.modelIsPopulating({ modelName: 'plot-layout' }) &&
                !state.queueRecentlySynced
              ) {
                paratooWarnMessage('Unable to get context layout data')
              } else {
                paratooSentryBreadcrumbMessage('Skipping warning about missing context layout data as the model is still loading')
              }
            }

            break
          }
          case 'plot-visit': {
            const plotVisitContext = cloneDeep(bulkStore().getPlotVisitId)
            const flattenedPlotVisitId = flattenPlotDataId(plotVisitContext)
            expanded = modelData.find((o) => {
              return modelDatumMatchesContext(o, flattenedPlotVisitId)
            })

            if (expanded) {
              //append flattened ID for comparison in some situations
              Object.assign(expanded, {
                flattened_id: flattenedPlotVisitId,
              })
            } else if (!expanded && !isEmpty(plotVisitContext)) {
              if (
                !ephemeralStore.modelIsPopulating({ modelName: 'plot-visit' }) &&
                !state.queueRecentlySynced
              ) {
                paratooWarnMessage('Unable to get context visit data')
              } else {
                paratooSentryBreadcrumbMessage('Skipping warning about missing context visit data as the model is still loading')
              }
            }

            break
          }
        }
        return cloneDeep(expanded)
      }
    },
    queuedProjectAreas: (state) => {
      const projectAreas = []
      for (const queuedCollection of state?.publicationQueue || []) {
        const queuedProjAreas = queuedCollection?.collection?.['plot-selection-survey']?.['project-areas']
        if (queuedProjAreas?.length) {
          projectAreas.push(...queuedProjAreas)
        }
      }
      return projectAreas
    },
    plotDataIsQueued: (state) => {
      return ({ plotType, tempOfflineIdToSearchFor }) => {
        paratooSentryBreadcrumbMessage(
          `Checking if ${plotType} data with Temp ID ${tempOfflineIdToSearchFor} is still in the queue`
        )
        const plotTypes = ['layout', 'visit']
        const plotLayoutAndVisitId = apiModelsStore().protIdFromUuid(cc.layoutAndVisitUuid)?.id
        let dataIsQueued = false
        if (!plotTypes.includes(plotType)) {
          paratooWarnMessage(
            `Programmer error: plot type '${plotType}' is not a registered case`,
          )
          return dataIsQueued
        }

        if (state.publicationQueue.length === 0) {
          paratooSentryBreadcrumbMessage('Queue is empty, nothing to check')
          return dataIsQueued
        }

        for (const [index, queueItem] of state.publicationQueue.entries()) {
          if (queueItem?.protocolId !== plotLayoutAndVisitId) continue
          const flattenedCollectionId = flattenPlotDataId(queueItem?.collection?.[`plot-${plotType}`])
          paratooSentryBreadcrumbMessage(
            `flattenedCollectionId: ${JSON.stringify(flattenedCollectionId)}`,
          )
          if (
            !Number.isInteger(queueItem?.collection?.[`plot-${plotType}`]?.id) &&
            flattenedCollectionId &&
            flattenedCollectionId === tempOfflineIdToSearchFor
          ) {
            dataIsQueued = true
            paratooSentryBreadcrumbMessage(
              `Queued item at index=${index} with queuedCollectionIdentifier=${queueItem.queuedCollectionIdentifier} contains a new ${plotType}`
            )
            break
          }
        }

        return dataIsQueued
      }
    },
    queueRecentlySynced: (state) => {
      const threshold = 5000    //5 seconds

      return state.queueSubmissionProgress === null &&
        (Date.now() - state.lastSyncTime < threshold)
    },
  },
})

/**
 * Finds all collections that this collection is dependent on
 * 
 * @param {Object} relevantBulkCollection the collection from the queue (not the whole
 * object with additional meta data)
 * @param {Array} publicationQueue a reference to the publication queue
 * @param {Array.<String>} DEPENDS_ON_MODELS a reference to the list of models that may
 * be depended on
 * 
 * @returns {Array.<String>} all the collection UUIDs that this collection depends on
 */
async function resolveDependsOnCollections(relevantBulkCollection, publicationQueue, DEPENDS_ON_MODELS) {
  const dependsOnQueuedCollections = []
  for (const collectionModel of Object.keys(relevantBulkCollection)) {
    const schemaForDep = schemaFromModelName(collectionModel, await documentationStore().getDocumentation())

    const modelDeps = documentationStore().modelDependencies({
      modelName: collectionModel,
      modelsAcc: [],
      includeSoftLinks: true,
    })
   
    for (const dep of modelDeps) {
      if (DEPENDS_ON_MODELS.includes(dep)) {
        if (
          ['plot-layout', 'plot-visit'].includes(dep)
        ) {
          //edge case for layout/visit - the plot context is set here so need to handle
          //it differently to other related models, as there's no schema or field name we
          //can follow for these due to how /bulk accepts them
          const plotFoundTempId = findValForKey(relevantBulkCollection[collectionModel], 'temp_offline_id')
          if (plotFoundTempId) {
            if (!Number.isInteger(relevantBulkCollection[collectionModel]?.id)) {
              findQueuedCollectionFromDependentField(
                dependsOnQueuedCollections,
                plotFoundTempId,
                publicationQueue,
                collectionModel,
              )
            } else {
              paratooSentryBreadcrumbMessage(
                `Collection already has non-temp link to ${collectionModel}`,
              )
            }
            
          }
        } else {
          //use this field name to find a given model's relation to the `dep`'s model
          let currDepFieldName = null
          let depFieldIsSoftLink = false
          let depFieldIsComponent = {
            type: null,   //will be set to 'single' or 'multi'
            field: null,    //the field in the component
          }
          for (const [fieldName, schema] of Object.entries(schemaForDep.properties)) {
            if (schema['x-model-ref']?.match(dep)) {
              currDepFieldName = fieldName
              break
            }
            if (schema['x-paratoo-soft-relation-target']?.match(dep)) {
              currDepFieldName = fieldName
              depFieldIsSoftLink = true
              break
            }
            //multi child observations
            if (schema?.items && schema?.items['x-model-ref']?.match(dep)) {
              currDepFieldName = fieldName
              break
            }
            //multi component that contains dep (e.g., `vegetation.floristics-voucher`)
            if (schema?.items?.['x-paratoo-component']) {
              for (const [field, prop] of Object.entries(schema.items.properties)) {
                if (prop['x-model-ref']?.match(dep)) {
                  currDepFieldName = fieldName
                  depFieldIsComponent.type = 'multi'
                  depFieldIsComponent.field = field
                  break
                }
              }
            }
            //single component that contains dep (e.g., `vegetation.floristics-voucher`)
            if (schema['x-paratoo-component']) {
              for (const [field, prop] of Object.entries(schema.properties)) {
                if (prop['x-model-ref']?.match(dep)) {
                  currDepFieldName = fieldName
                  depFieldIsComponent.type = 'single'
                  depFieldIsComponent.field = field
                  break
                }
                //single nested component
                if (prop?.['x-paratoo-component']) {
                  for (const [nestedField, nestedProp] of Object.entries(prop.properties)) {
                    console.log(`nestedField=${nestedField}. nestedProp: `, nestedProp)
                    if (nestedProp['x-model-ref']?.match(dep)) {
                      currDepFieldName = fieldName
                      depFieldIsComponent.type = 'single'
                      depFieldIsComponent.field = field
                      break
                    }
                  }
                }
              }
            }
          }

          let collectionModelData = null
          if (!Array.isArray(relevantBulkCollection[collectionModel])) {
            //treat non-arrays as arrays to make the below iteration easier to handle
            collectionModelData = [relevantBulkCollection[collectionModel]]
          } else {
            collectionModelData = relevantBulkCollection[collectionModel]
          }

          for (const modelData of collectionModelData) {
            // console.log(`modelData (for collectionModel=${collectionModel}):`, modelData)
            // console.log(`modelData[${currDepFieldName}] - is object? ${typeof modelData[currDepFieldName] === 'object'}; is array? ${Array.isArray(modelData[currDepFieldName])}`)
            // if (depFieldIsComponent?.type === 'multi') {
            //   console.log(`modelData[${currDepFieldName}] 'some' of ${depFieldIsComponent.field} not empty?`, modelData[currDepFieldName].some(
            //     o => !isEmpty(o[depFieldIsComponent.field])
            //   ))
            // }
            // if (depFieldIsComponent?.type === 'single') {
            //   console.log(`modelData[${currDepFieldName}][${[depFieldIsComponent.field]}] not empty?`, !isEmpty(modelData[currDepFieldName][depFieldIsComponent.field]))
            // }
            //could be empty string, object, array, etc., so use lodash
            if (
              !isEmpty(modelData[currDepFieldName]) ||
              (
                depFieldIsComponent?.type === 'multi' &&
                modelData[currDepFieldName]?.some(
                  o => !isEmpty(o[depFieldIsComponent.field])
                )
              ) ||
              (
                depFieldIsComponent?.type === 'single' &&
                !isEmpty(modelData[currDepFieldName]?.[depFieldIsComponent.field])
              )
            ) {
              //might be multi child ob, so need to iterate. Treat non-multi-child-ob as
              //array too, to make iteration simpler
              let currModelData = null
              if (!Array.isArray(modelData[currDepFieldName])) {
                currModelData = [modelData[currDepFieldName]]
              } else {
                currModelData = modelData[currDepFieldName]
              }

              for (const data of currModelData) {
                if (depFieldIsSoftLink) {
                  findQueuedCollectionFromDependentField(
                    dependsOnQueuedCollections,
                    data,
                    publicationQueue,
                    collectionModel,
                    dep,
                    currDepFieldName,
                  )
                }

                //current collection has a temp ID used (i.e., it's referencing some other
                //data), so need to resolve
                const foundTempId = data.temp_offline_id ||
                (
                  //need to check for object before calling `findValForKey`, as that's what
                  //it expects
                  typeof data === 'object' &&
                  findValForKey(data, 'temp_offline_id')
                )
                if (foundTempId) {
                  findQueuedCollectionFromDependentField(
                    dependsOnQueuedCollections,
                    foundTempId,
                    publicationQueue,
                    collectionModel,
                  )
                }
              }
            }
          }
        }
        
      }
    }
  }


  return dependsOnQueuedCollections
}

/**
 * Searches the publication queue for the provided temp offline ID
 * 
 * @param {Array.<String>} dependsOnQueuedCollections the running list of queued
 * collection decencies. Passed as a reference so we can push to it without returning
 * @param {String} dependentField the dependent field value (usually temp offline ID, but
 * can be another field for 'soft' links) that exists somewhere in the queue. This value
 * is used as a reference somewhere in the collection we're queuing
 * @param {Array} publicationQueue a reference to the publication queue
 * @param {String} collectionModel the model from the bulk collection - used for trace
 * @param {String} [dependencyModel] the model of the dependency we're looking for. Used
 * to match soft links with the dependency without getting false-positives on models that
 * also use similar data (`dependentField`) we're looking for
 * @param {String} [queueFieldToMatch] the field name we're trying to match against in
 * the queued data. Defaults to 'temp_offline_id'
 */
function findQueuedCollectionFromDependentField(
  dependsOnQueuedCollections,
  dependentField,
  publicationQueue,
  collectionModel,
  dependencyModel = null,
  queueFieldToMatch = 'temp_offline_id',
) {
  
  //search all previous collections with the `collectionModel` to find the
  //collection that THIS collection relies on
  for (const [index, queuedCollection] of Object.entries(publicationQueue)) {
    for (const queuedCollectionModel in queuedCollection.collection) {
      let queuedCollectionModelData = null
      if (!Array.isArray(queuedCollection.collection[queuedCollectionModel])) {
        //treat non-arrays as arrays to make the below iteration easier to handle
        queuedCollectionModelData = [queuedCollection.collection[queuedCollectionModel]]
      } else {
        queuedCollectionModelData = queuedCollection.collection[queuedCollectionModel]
      }

      for (const queuedModelData of queuedCollectionModelData) {
        if (
          dependencyModel &&
          queueFieldToMatch !== 'temp_offline_id' &&
          queuedCollectionModel !== dependencyModel
        ) {
          //since we have no hard links, we don't want to get false-positive matches in
          //other models that may use the field value (`dependentField`) we're checking
          continue
        }
        if (
          queuedModelData &&
          //want to exclude models (steps) that only have 1 key, as this is
          //likely a dependent model referencing it's dependency
          //(specifically likely plot layout/visit)
          Object.keys(queuedModelData).length > 1 &&
          queuedModelData[queueFieldToMatch] &&
          queuedModelData[queueFieldToMatch] === dependentField
        ) {
          let msg = `Collection's model '${collectionModel}' found the initial dependent field '${dependentField}' in queued collection model '${queuedCollectionModel}' (at index=${index} for queued collection identifier '${queuedCollection.queuedCollectionIdentifier}')`
          if (!dependsOnQueuedCollections.includes(queuedCollection.queuedCollectionIdentifier)) {
            //only push if not already
            dependsOnQueuedCollections.push(queuedCollection.queuedCollectionIdentifier)
          } else {
            msg += '. Already added so not adding again'
          }
          console.debug(msg)

          //TODO might need to recursively follow these deps
          if (
            Array.isArray(queuedCollection.dependsOnQueuedCollections) &&
            queuedCollection.dependsOnQueuedCollections.length > 0
          ) {
            let msg = `Also found this dependent collection has dependencies: ${queuedCollection.dependsOnQueuedCollections.join(', ')}`
            const addedNonDuplicates = []
            for (const dep of queuedCollection.dependsOnQueuedCollections) {
              if (!dependsOnQueuedCollections.includes(dep)) {
                //only push if not already
                dependsOnQueuedCollections.push(dep)
                addedNonDuplicates.push(dep)
              }
            }
            
            if (addedNonDuplicates.length > 0) {
              msg += `. Adding non-duplicates: ${addedNonDuplicates.join(', ')}.`
            } else {
              msg += '. All are duplicates so nothing more to add'
            }
            console.debug(msg)
          }
        }
      }

      // edge case for vertebrate trap check and vertebrate end trap
      // they depends on vertebrate-trap which is a child obs of vertebrate-trap-line
      if (
        ['vertebrate-check-trap', 'vertebrate-end-trap'].includes(
          collectionModel,
        ) &&
        queuedCollectionModel === 'vertebrate-trap-line'
      ) {
        const hasTrapsWithTempOfflineId = queuedCollection.collection[
          'vertebrate-trap-line'
        ].some((line) => line.traps.some((trap) => trap.temp_offline_id))

        if (hasTrapsWithTempOfflineId) {
          const queuedCollectionIdentifier =
            queuedCollection?.queuedCollectionIdentifier
          if (
            !dependsOnQueuedCollections.includes(queuedCollectionIdentifier)
          ) {
            dependsOnQueuedCollections.push(queuedCollectionIdentifier)
          }
        }
      }

    }
  }
}

/**
 * Resolves temporary offline IDs from a layout step. Most plot-based modules will have
 * this step
 * 
 * @param {Object} queuedCollection the queued collection object
 * @param {Array} submitResponses the list of submit responses, which should contain the
 * already-submit layout to resolve (as the queue submits in-order, layouts go first)
 * 
 * @returns {String} the temporary offline UUID of the layout, for later use in resolving
 * field relations to this layout
 */
function resolveLayoutCollectionStep(queuedCollection, submitResponses) {
  console.debug(`attempting to resolve layout step for collection: `, queuedCollection)
  let queuedLayoutTempOfflineId = null
  if (
    queuedCollection.collection['plot-layout']?.temp_offline_id ||
    queuedCollection.collection['plot-layout']?.id?.temp_offline_id 
  ) {
    queuedLayoutTempOfflineId = (() => {
      if (queuedCollection.collection['plot-layout']?.temp_offline_id) {
        return queuedCollection.collection['plot-layout'].temp_offline_id
      } else if (queuedCollection.collection['plot-layout']?.id?.temp_offline_id) {
        return queuedCollection.collection['plot-layout'].id.temp_offline_id
      }
      return null
    })()
    const collectionToResolveLayout = submitResponses.find(
      o => o['plot-layout']?.temp_offline_id === queuedLayoutTempOfflineId
    )
    if (collectionToResolveLayout) {
      Object.assign(queuedCollection.collection['plot-layout'], {
        id: collectionToResolveLayout['plot-layout'].id
      })
    }
  }

  return queuedLayoutTempOfflineId
}

/**
 * Resolves temporary offline IDs from a visit step. Most plot-based modules will have
 * this step
 * 
 * @param {Object} queuedCollection the queued collection object
 * @param {Array} submitResponses the list of submit responses, which should contain the
 * already-submit visit to resolve (as the queue submits in-order, visits go first)
 * 
 * @returns {String} the temporary offline UUID of the visit, for later use in resolving
 * field relations to this visit
 */
function resolveVisitCollectionStep(queuedCollection, submitResponses) {
  console.debug(`attempting to resolve visit step for collection: `, queuedCollection)
  let queuedVisitTempOfflineId = null
  if (
    queuedCollection.collection['plot-visit']?.temp_offline_id ||
    queuedCollection.collection['plot-visit']?.id?.temp_offline_id
  ) {
    queuedVisitTempOfflineId = (() => {
      if (queuedCollection.collection['plot-visit']?.temp_offline_id) {
        return queuedCollection.collection['plot-visit'].temp_offline_id
      } else if (queuedCollection.collection['plot-visit']?.id?.temp_offline_id) {
        return queuedCollection.collection['plot-visit'].id.temp_offline_id
      }
      return null
    })()
    const collectionToResolveVisit = submitResponses.find(
      o => o['plot-visit']?.temp_offline_id === queuedVisitTempOfflineId
    )
    if (collectionToResolveVisit) {
      Object.assign(queuedCollection.collection['plot-visit'], {
        id: collectionToResolveVisit['plot-visit'].id
      })
    }
  }

  return queuedVisitTempOfflineId
}

/**
 * Removes a field's relation to a layout, to prevent the backend validator from
 * complaining
 * 
 * Handles `modelName` `plot-visit` - can extend further as-needed
 * 
 * @param {String} modelName the model, i.e., the step
 * @param {Object} value the value for the model, i.e., the collection data body. Passed
 * as a reference, so this function does not return the mutated data
 * @param {Object} queuedCollection the queued collection object
 * 
 * @returns {Boolean} whether the resolution succeeded
 */
function removeLayoutRelation(modelName, value, queuedCollection) {
  console.debug(`attempting to remove layout relation for '${modelName}': `, value)
  if (
    modelName === 'plot-visit' &&
    value?.temp_offline_id &&
    value?.plot_layout?.temp_offline_id &&
    //not just a new visit, but also a new layout
    queuedCollection.collection['plot-layout'].temp_offline_id
  ) {
    console.debug('removing layout from visit')
    //if we've got new layout and visit, the visit doesn't need to and cannot
    //resolve the layout (as the new layout is getting submit with this new
    //visit, where they will both be resolved in the backend)
    delete value.plot_layout    //so that backend validator doesn't complain
    return true
  }
  return false
}

/**
 * Resolves a field's relation to a layout that has been stored as a temporary offline ID
 * 
 * @param {String} modelName the model, i.e., the step. Only used for debug trace
 * @param {Object} value the value for the model, i.e., the collection data body. Passed
 * as a reference, so this function does not return the mutated data
 * @param {Array.<String>} modelFields the fields of the model, i.e., the keys of the `value`
 * @param {Array} submitResponses the list of submit responses, which should contain the
 * already-submit layout to resolve
 * @param {String} queuedLayoutTempOfflineId the temporary offline UUID of the layout
 */
function resolveLayoutRelation(modelName, value, modelFields, submitResponses, queuedLayoutTempOfflineId) {
  console.debug(`attempting to resolve layout relation for '${modelName}' (ID=${queuedLayoutTempOfflineId}): `, value)
  if (
    modelFields.includes('plot_layout') &&
    value.plot_layout?.temp_offline_id
  ) {
    const collectionToResolveLayout = submitResponses.find(
      o => o['plot-layout']?.temp_offline_id === queuedLayoutTempOfflineId
    )
    if (collectionToResolveLayout) {
      value.plot_layout = collectionToResolveLayout['plot-layout'].id
    }
  } else if (Array.isArray(value)) {
    for (const val of value) {
      if (val?.fauna_plot?.temp_offline_id) {
        if (queuedLayoutTempOfflineId) {
          //case for when an 'observation' relates to a plot, and the survey is
          //plot-based (has `plot-layout` model in the collection)
          const collectionToResolveLayout = submitResponses.find(
            o => o['plot-layout']?.temp_offline_id === queuedLayoutTempOfflineId
          )
          console.debug('collectionToResolveLayout:', JSON.stringify(collectionToResolveLayout))
          val.fauna_plot = collectionToResolveLayout['plot-layout'].id
        } else {
          //case for when an 'observation' relates to a plot, but is not plot-based
          const relevantLayout = submitResponses.find(
            o => o['plot-layout']?.temp_offline_id === val.fauna_plot.temp_offline_id
          )
          console.debug('relevantLayout:', JSON.stringify(relevantLayout))
          if (relevantLayout) {
            val.fauna_plot = relevantLayout['plot-layout'].id
          }
        }
      }
    }
  }
}

/**
 * Resolves a field's relation to a visit that has been stored as a temporary offline ID
 * 
 * @param {String} modelName the model, i.e., the step. Only used for debug trace
 * @param {Object} value the value for the model, i.e., the collection data body. Passed
 * as a reference, so this function does not return the mutated data
 * @param {Array.<String>} modelFields the fields of the model, i.e., the keys of the `value`
 * @param {Array} submitResponses the list of submit responses, which should contain the
 * already-submit visit to resolve
 * @param {String} queuedVisitTempOfflineId the temporary offline UUID of the visit
 */
function resolveVisitRelation(modelName, value, modelFields, submitResponses, queuedVisitTempOfflineId) {
  console.debug(`attempting to resolve visit relation for '${modelName}' (ID=${queuedVisitTempOfflineId}): `, value)
  if (
    modelFields.includes('plot_visit') &&
    value.plot_visit?.temp_offline_id
  ) {
    const collectionToResolveVisit = submitResponses.find(
      o => o['plot-visit']?.temp_offline_id === queuedVisitTempOfflineId
    )
    if (collectionToResolveVisit) {
      value.plot_visit = collectionToResolveVisit['plot-visit'].id
    }
  }
}

/**
 * Searched for a submit voucher with a given temporary offline ID
 * 
 * @param {Array} submitResponses the list of submit responses, which should contain the
 * already-submit visit to resolve
 * @param {String} temp_offline_id the temporary offline UUID of the voucher to search
 * @param {'full' | 'lite'} variant the variant of the voucher we're searching for
 * 
 * @returns {Object} the voucher that came from the API response during submission
 */
function findVoucherFromSubmitted(submitResponses, temp_offline_id, variant) {
  let relevantVoucher = null
  for (const r of submitResponses) {
    if (r[`floristics-veg-voucher-${variant}`]) {
      //used to use `Array.find()`, but for some reason it won't return when it should,
      //so manually implement the search
      for (const currSubmitVoucher of r[`floristics-veg-voucher-${variant}`]) {
        if (currSubmitVoucher.temp_offline_id === temp_offline_id) {
          relevantVoucher = currSubmitVoucher
        }
      }
    }
  }
  return relevantVoucher
}

/**
 * Resolves a field's relation to a floristics voucher (and sometimes the survey) that
 * has been stored as a temporary offline ID
 * 
 * Handles `modelName` `floristics-veg-virtual-voucher`, `cover-point-intercept-point` - 
 * can be extended as-needed
 * 
 * @param {String} modelName the model, i.e., the step
 * @param {Object} value the value for the model, i.e., the collection data body. Passed
 * as a reference, so this function does not return the mutated data
 * @param {Array} submitResponses the list of submit responses, which should contain the
 * already-submit voucher to resolve
 */
function resolveVoucherRelation(modelName, value, submitResponses) {
  console.debug(`attempting to resolve voucher relation for '${modelName}': `, value)
  console.log(JSON.stringify(value))
  switch(modelName) {
    case 'floristics-veg-virtual-voucher':
      for (let recollectedVoucher of value) {
        //can relate to full or lite, so check both
        for (const variant of ['full', 'lite']) {
          //resolve relation to survey
          if (recollectedVoucher?.survey?.temp_offline_id) {
            const relevantSurvey = submitResponses.find(
              o => o[`floristics-veg-survey-${variant}`]?.temp_offline_id === recollectedVoucher.survey.temp_offline_id
            )
            if (relevantSurvey) {
              recollectedVoucher.survey = relevantSurvey[`floristics-veg-survey-${variant}`].id
            }
          }

          //resolve relation to voucher
          if (recollectedVoucher[`voucher_${variant}`]?.temp_offline_id) {
            const relevantVoucher = findVoucherFromSubmitted(
              submitResponses,
              recollectedVoucher[`voucher_${variant}`].temp_offline_id,
              variant,
            )
            if (relevantVoucher) {
              recollectedVoucher[`voucher_${variant}`] = relevantVoucher.id
            }
          }
        }
      }
      break
    case 'cover-point-intercept-point':
      for (let point of value) {
        for (let intercept of point.species_intercepts) {
          for (const voucherVariant of ['full', 'lite']) {
            if (intercept[`floristics_voucher_${voucherVariant}`]?.temp_offline_id) {
              const relevantVoucher = findVoucherFromSubmitted(
                submitResponses,
                intercept[`floristics_voucher_${voucherVariant}`].temp_offline_id,
                voucherVariant,
              )
              if (relevantVoucher) {
                intercept[`floristics_voucher_${voucherVariant}`] = relevantVoucher.id
              }
            }
          }
        }
      }
      break
    case 'floristics-veg-genetic-voucher':
      for (let val of value) {
        //can relate to full or lite, so check both
        for (const voucherVariant of ['full', 'lite']) {
          if (val?.floristics_voucher?.[`voucher_${voucherVariant}`]?.temp_offline_id) {
            const relevantVoucher = findVoucherFromSubmitted(
              submitResponses,
              val?.floristics_voucher?.[`voucher_${voucherVariant}`].temp_offline_id,
              voucherVariant,
            )
            console.debug('relevantVoucher:', relevantVoucher)
            if (relevantVoucher) {
              val.floristics_voucher[`voucher_${voucherVariant}`] = relevantVoucher.id
            }
          } else if (val?.floristics_voucher?.[`voucher_${voucherVariant}`]) {
            //possible PTV was collected as submodule, so have a look for a matching unique_id
            let relevantVoucher = null
            for (const r of submitResponses) {
              if (r[`floristics-veg-voucher-${voucherVariant}`]) {
                /* relevantVoucher = r[`floristics-veg-voucher-${voucherVariant}`].find(
                  currSubmitVoucher => currSubmitVoucher.unique_id === val.floristics_voucher[`voucher_${voucherVariant}`]
                ) */
                for (const currSubmitVoucher of r[`floristics-veg-voucher-${voucherVariant}`]) {
                  if (currSubmitVoucher.unique_id === val.floristics_voucher[`voucher_${voucherVariant}`]) {
                    relevantVoucher = currSubmitVoucher
                    //don't need to keep searching
                    break
                  }
                }
              }
            }
            console.debug('relevantVoucher:', relevantVoucher)
            if (relevantVoucher) {
              val.floristics_voucher[`voucher_${voucherVariant}`] = relevantVoucher.id
            }
          }
        }
      }
      break
    case 'recruitment-growth-stage':
    case 'basal-area-dbh-measure-observation':
      for (let val of value) {
        //can relate to full or lite, so check both
        for (const voucherVariant of ['full', 'lite']) {
          if (val?.floristics_voucher?.[`voucher_${voucherVariant}`]?.temp_offline_id) {
            const relevantVoucher = findVoucherFromSubmitted(
              submitResponses,
              val.floristics_voucher[`voucher_${voucherVariant}`].temp_offline_id,
              voucherVariant,
            )
            console.debug('relevantVoucher: ', relevantVoucher)
            if (relevantVoucher) {
              val.floristics_voucher[`voucher_${voucherVariant}`] = relevantVoucher.id
            }
          }
        }
      }
      break
    case 'recruitment-survivorship-survey':
      for (let voucher of value.floristics_voucher || []) {
        //can relate to full or lite, so check both
        for (const voucherVariant of ['full', 'lite']) {
          if (voucher[`voucher_${voucherVariant}`]?.temp_offline_id) {
            const relevantVoucher = findVoucherFromSubmitted(
              submitResponses,
              voucher[`voucher_${voucherVariant}`].temp_offline_id,
              voucherVariant,
            )
            console.debug('relevantVoucher: ', relevantVoucher)
            if (relevantVoucher) {
              voucher[`voucher_${voucherVariant}`] = relevantVoucher.id
            }
          }
        }
      }
      break
    case 'condition-tree-survey':
      for (let val of value) {
        for (const voucherVariant of ['full', 'lite']) {
          if (val?.tree_record?.species?.[`voucher_${voucherVariant}`]?.temp_offline_id) {
            const relevantVoucher = findVoucherFromSubmitted(
              submitResponses,
              val.tree_record.species[`voucher_${voucherVariant}`].temp_offline_id,
              voucherVariant,
            )
            console.debug('relevantVoucher: ', relevantVoucher)
            if (relevantVoucher) {
              val.tree_record.species[`voucher_${voucherVariant}`] = relevantVoucher.id
            }
          }
        }
      }
      break
    case 'basal-wedge-observation':
      for (let val of value) {
        //can relate to full or lite, so check both
        for (const voucherVariant of ['full', 'lite']) {
          //can select zero to many species (stored as array, else field
          //`floristics_voucher` won't exist if none are selected)
          for (let voucher of val?.floristics_voucher || []) {
            if (voucher?.[`voucher_${voucherVariant}`]?.temp_offline_id) {
              const relevantVoucher = findVoucherFromSubmitted(
                submitResponses,
                voucher[`voucher_${voucherVariant}`].temp_offline_id,
                voucherVariant,
              )
              console.debug('relevantVoucher: ', relevantVoucher)
              if (relevantVoucher) {
                voucher[`voucher_${voucherVariant}`] = relevantVoucher.id
              }
            }
          }
        }
      }
      break
  }
}

/**
 * Resolves the various relations for the models pertaining to vertebrate trapping
 * collections (setup, check, closure). Also removes the temporary copies of the setup
 * survey from relevant models
 * 
 * @param {String} modelName the model, i.e., the step
 * @param {Object} value the value for the model, i.e., the collection data body. Passed
 * as a reference, so this function does not return the mutated data
 * @param {Array} submitResponses the list of submit responses, which should contain the
 * already-submit voucher to resolve
 */
function resolveVertTrapRelation(modelName, value, submitResponses) {
  console.debug(`attempting to resolve vert trap relation for '${modelName}': `, value)
  const modelsUseTraps = [
    'vertebrate-check-trap',
    'vertebrate-end-trap',
  ]
  if (modelsUseTraps.includes(modelName)) {
    //all store trap's temp_offline_id in subset of known field names, so make a single
    //generic loop
    const potentialFieldNames = [
      'trap_setup',
      'trap_number',
    ]
    for (let record of value) {
      for (const fieldName of potentialFieldNames) {
        if(!record[fieldName]?.temp_offline_id) continue
        const relevantTrapData = findRelevantTrap(record[fieldName].temp_offline_id)
        if(relevantTrapData) {
          record[fieldName] = relevantTrapData.id
        }
      }
    }
  }
  function findRelevantTrap(tmp_offline_id) {
    for (const response of submitResponses) {
      if (!response['vertebrate-trap-line']) continue
      for (const line of response['vertebrate-trap-line']) {
        const relevantTrapData = line.traps.find(
          (trap) => trap.temp_offline_id === tmp_offline_id,
        )
        if (relevantTrapData) return relevantTrapData
      }
    }
  }

  const modelsUseTrapLine = [
    'vertebrate-closed-drift-photo',
  ]
  if (modelsUseTrapLine.includes(modelName)) {
    for (let record of value) {
      if (!Number.isInteger(record.drift_line) && uuidValidate(record.drift_line?.temp_offline_id)) {
        let relevantTrapLine = null
        for (const r of submitResponses) {
          if (r['vertebrate-trap-line'])  {
            for (const trapLine of r['vertebrate-trap-line']) {
              if (trapLine.temp_offline_id === record.drift_line.temp_offline_id) {
                relevantTrapLine = trapLine
              }
            }
          }
        }
        if (relevantTrapLine) {
          record.drift_line = relevantTrapLine.id
        }
      }
    }
  }

  const modelsToRemoveTempSurveys = [
    'vertebrate-trap-line',
  ]
  if (modelsToRemoveTempSurveys.includes(modelName)) {
    for (let record of value) {
      if (
        record.trapping_survey &&
        !record.trapping_survey.id
      ) {
        //need to remove this survey as core resolves the relation (see explanation in
        //`queueSubmission()` case for vertebrate-trap)
        console.debug(`removing temporary relation to 'vertebrate-trapping-setup-survey' from '${modelName}'`)
        delete record.trapping_survey
      }
    }
  }
}

/**
 * Removes the temporary copy of the camera trap survey
 * 
 * @param {String} modelName the model to remove the survey from
 * @param {Object} value the value for the model, i.e., the collection data body. Passed
 * as a reference, so this function does not return the mutated data
 */
function removeTempCameraTrapSurvey(modelName, value) {
  if (modelName === 'camera-trap-deployment-point') {
    for (let record of value) {
      if (
        record.camera_trap_survey &&
        !record.camera_trap_survey.id
      ) {
        console.debug(`removing temporary relation to 'camera-trap-deployment-survey' from '${modelName}'`)
        delete record.camera_trap_survey
      }
    }
  }
}

/**
 * Resolves the relation to soil characterisation (specifically
 * `soil-pit-characterisation-full` model)
 * 
 * @param {String} modelName the model to remove the survey from
 * @param {Object} value the value for the model, i.e., the collection data body. Passed
 * as a reference, so this function does not return the mutated data
 * @param {Array} submitResponses the list of submit responses, which should contain the
 * already-submit voucher to resolve
 * @param {Boolean} [isInProgress] whether the `value` is in-progress (bulk store) data
 */
function resolveSoilCharacterisationRelation(
  modelName,
  value,
  submitResponses,
  isInProgress = false,
) {
  console.debug(
    `attempting to resolve soil characterisation for '${modelName}'${isInProgress ? ' (is in-progress)' : ''}: `,
    value,
  )
  console.log('value:', JSON.stringify(value))
  console.log('submitResponses:', JSON.stringify(submitResponses))
  if (!value.associated_soil_pit_id) return

  const variant = value.associated_soil_pit_id.variant
  let associatedModelName = `soil-pit-characterisation-full`
  let fieldNameToReplaceValue = `associated_soil_pit_id`
  if(variant === 'lite') {
    associatedModelName = `soil-pit-characterisation-lite`
    fieldNameToReplaceValue = `associated_soil_pit_id_lite`
  }

  if (!isInProgress) {
    if (value?.associated_soil_pit_id.temp_offline_id) {
      for (const response of submitResponses) {
        if (!response[associatedModelName]) continue

        if (
          response[associatedModelName]?.temp_offline_id ===
          value.associated_soil_pit_id.temp_offline_id
        ) {
          value[fieldNameToReplaceValue] = response[associatedModelName].id
          break
        }
      }
    } else {
      value[fieldNameToReplaceValue] = value.associated_soil_pit_id.id
    }
    // remove temporary relation if we are not using the full variant
    if(variant === 'lite') {
      delete value.associated_soil_pit_id
    }
  } else {
    if (value.associated_soil_pit_id?.temp_offline_id) {
      for (const response of submitResponses) {
        if (!response[associatedModelName]) continue

        if (
          response[associatedModelName]?.temp_offline_id ===
          value.associated_soil_pit_id.temp_offline_id
        ) {
          Object.assign(value.associated_soil_pit_id, {
            id: response[associatedModelName].id
          })
          delete value.associated_soil_pit_id.temp_offline_id
        }
      }
    }
  }
}

function removeVoucherSurveyRelation(modelName, value) {
  for (const variant of ['full', 'lite']) {
    if (modelName === `floristics-veg-voucher-${variant}`) {
      for (let record of value) {
        if (
          record?.[`floristics_veg_survey_${variant}`] &&
          !record?.[`floristics_veg_survey_${variant}`]?.id
        ) {
          console.debug(`removing temporary relation to 'floristics-veg-survey-${variant}' from '${modelName}'`)
          delete record[`floristics_veg_survey_${variant}`]
        }
      }
    }
  }
}

function removeGrassyWeedsSetupRelation(modelName, value) {
  if (
    modelName === 'grassy-weeds-survey' &&
    value?.survey_type === 'F' &&
    value.desktop_setup === 'none'
  ) {
    console.debug(`removing desktop setup from field grassy weeds survey`)
    delete value.desktop_setup
  }
}

/**
 * Checks all in-progress (bulk store) collections that depend-on any items in the queue
 * 
 * @param {Array} publicationQueue a reference to the queue
 * @param {Array} DEPENDS_ON_MODELS a reference to DEPENDS_ON_MODELS
 * 
 * @returns {Object} keys corresponding to a queuedCollectionIdentifier and an array
 * value of all protocol ID's of in-progress data that depend on the queued item
 */
async function inProgressCollectionsDependOnQueue({
  publicationQueue,
  DEPENDS_ON_MODELS,
}) {
  //key will be queuedCollectionIdentifier and value will be all the protocol IDs that
  //depend on that queued item
  const dependencies = {}
  for (const [protocolId, collection] of Object.entries(bulkStore().collectionGet)) {
    const protocolDeps = await resolveDependsOnCollections(
      collection,
      publicationQueue,
      DEPENDS_ON_MODELS,
    )
    paratooSentryBreadcrumbMessage(`protocolDeps for protocolId=${protocolId}: ${JSON.stringify(protocolDeps)}`)

    for (const queuedIdentifierDep of protocolDeps) {
      const queuedItem = publicationQueue.find(
        q => q.queuedCollectionIdentifier === queuedIdentifierDep
      )
      if (queuedItem) {
        const queuedItemProtocol = apiModelsStore().protUuidFromId(queuedItem.protocolId)
        paratooSentryBreadcrumbMessage(`queued collection with identifier=${queuedIdentifierDep} has protocol UUID=${queuedItemProtocol?.uuid}`)

        if (queuedItemProtocol?.uuid === cc.layoutAndVisitUuid) {
          paratooSentryBreadcrumbMessage(`skipping tracking in-progress protocols depending on ${queuedItemProtocol?.name}`)
          continue
        }
      }

      if (!dependencies?.[queuedIdentifierDep]) {
        Object.assign(dependencies, {
          [queuedIdentifierDep]: []
        })
      }

      dependencies[queuedIdentifierDep].push(protocolId)
    }
  }
  return dependencies
}

/**
 * Combines the current submit responses (data manager) with those from the historical
 * data manager
 * 
 * @param {Object} currentSubmitResponses the data manger submit responses
 * 
 * @returns {Array.<Object>} the combined historical data
 */
async function combineCurrentAndHistoricalSubmitResponses({ currentSubmitResponses }) {
  const historicalSubmitResponses = await historicalDataManagerDb.getAllTableData({
    table: 'submitResponses',
  })
  const submitResponsesCollections = currentSubmitResponses
    .map(
      (o) => o.collection,
    )
    //TODO this data could be massive, so we probably only want to search it if the other `this.submitResponses` doesn't have the data we need
    .concat(
      historicalSubmitResponses.map(
        h => h?.data?.collection
      ) || []
    )
  return submitResponsesCollections
}
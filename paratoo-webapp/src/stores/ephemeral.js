/**
 * A pinia module for anything that should NOT be saved to local storage.
 *
 * Put anything here that meets any of the following:
 *  - doesn't serialise well, like functions or references
 *  - shouldn't be saved between sessions, like "are we online" flag
 */
import { defineStore } from 'pinia'
import AbortController from 'abort-controller'
import { notifyHandler, paratooSentryBreadcrumbMessage } from '../misc/helpers'
import {
  isNil,
} from 'lodash'
export const useEphemeralStore = defineStore('ephemeral', {
  state: () => ({
    voucherLabelOptions: null,
    mediaDetectionTimePeriod: 600000, // 10 mins
    firstClickTimestamp: null,
    timeDifference: null,
    stopTimer: false,
    startSurveyFlag: false,
    projectArea: null,
    transectLines: null,
    plotLayoutRecommendedLocation: {},
    floraSpeciesCluster: [],
    availableSpecies: [],
    trap_or_equipment_name: null,
    aerialSearchArea: { center: null, radius: null },
    aerialPolyline: [],
    aerialTrackLog: [],
    selectCommunity: null,
    selectedValue: null,
    startTransect: false,
    transectPoints: [],
    aerialPoints: [],
    speed: 0,
    currentTime: 0,
    transectTimer: null,
    transectTime: 0,
    trackLogInterval: null, //TODO roll-up with `trackedIntervals`
    networkOnline: null,
    lastOnline: Math.round(Date.now() / 1000),
    locationOnline: false,
    // i think valueOf is a function not property
    // if we use false.valueOf() instead of false.valueOf
    // it seems working otherwise it returns dodgy value like "function valueOf() { [native code] }"
    // false.valueOf() returns false
    // it can be revisited
    apiModelsArePopulating: false.valueOf(),
    apiModelsPopulatingStartTime: null,
    apiModelsPopulatingProcesses: {},
    SWRegistrationForNewContent: false,
    useExistingLocation: undefined,
    useExistingVisit: undefined,
    setLoginFlag: false,
    imgName: null,
    protocolImg: null,
    loginBgImg: '',
    beepAudio: null,
    beepAudioRoute: null,
    prevProtocol: null,
    dexieDbIsLoading: false,
    userMedia: { camera: null, barcodeReader: null },
    treeEllipseOn: false,
    basalDbhInstrumentType: null,
    currentOpportuneInfo: {},
    plotPoints: [],
    barcodeOpen: false,
    faunaGroundCountProtocolType: null,
    //FIXME hacky solution to ensure coords are at least defined during tests
    userCoords: null,
    gpsAccuracy: null,
    heading: 0,
    collectionSurveyLength: 1,
    pouchDbIsLoading: false,
    notificationState: {
      positive: false,
      negative: false,
      warning: false,
      info: false,
      ongoing: false,
    },
    persistFailureNotificationState: false,
    locationRequiredNotificationState: false,
    populatedDexieTables: [],
    tokenHasExpired: false,
    refreshTokenHasExpired: false,
    showLUTDesc: true,
    vibrate: false,
    // controller to cancel pending requests
    fetchAbortControllers: [new AbortController()],
    pendingFetchRequests: new Set(),
    grassyWeedsSurveyIsAutofilled: false,
    // list of populated models
    populatedModels: [],
    selectedTransect: null,
    // only want persist during a session
    isAutoHealTriggered: false,
    coreIsAvailable: true,
    broadcastChanel: null,
    trackedIntervals: {},
  }),
  actions: {
    trackFetchRequest({ url }) {
      paratooSentryBreadcrumbMessage(`tracking fetch request to: ${url}`)
      this.pendingFetchRequests.add(url)
    },
    untrackFetchRequest({ url }) {
      paratooSentryBreadcrumbMessage(`untracking fetch request to: ${url}`)
      this.pendingFetchRequests.delete(url)
    },
    isPendingTrackedFetchRequest({ url }) {
      const isPending = [...this.pendingFetchRequests].some(u => u.includes(url))
      paratooSentryBreadcrumbMessage(`${url} is pending? ${isPending}`)
      return isPending
    },
    trackInterval({ intervalName, intervalId }) {
      if (isNil(this.trackedIntervals?.[intervalName])) {
        paratooSentryBreadcrumbMessage(
          `Tracking interval ${intervalName}`,
        )
        Object.assign(this.trackedIntervals, {
          [intervalName]: intervalId,
        })
      } else {
        paratooSentryBreadcrumbMessage(
          `Already tracked interval for ${intervalName}`,
        )
      }
    },
    checkMediaNotificationTimer() {
      const now = Date.now()
      const { firstClickTimestamp, mediaDetectionTimePeriod } = this
      // If it's the first click or the timestamp needs to be reset
      if (
        firstClickTimestamp === null ||
        (firstClickTimestamp !== 0 &&
          now - firstClickTimestamp > mediaDetectionTimePeriod)
      ) {
        this.firstClickTimestamp = now
        return true
      }

      return false
    },
    setTrackLog(lat, lng) {
      if (window.Cypress) {
        this.userCoords = { lat: lat, lng: lng }
      }
    },
    updateUserCoords(newCoords, force = false) {
      // in prod/dev we update immediately
      if (!window.Cypress) {
        this.userCoords = newCoords
        return
      }
      if (window.Cypress) {
        // in test, enable force flag to update userCoords.
        // e.g. current position needs to be updated
        //   to add new points in intervention
        this.userCoords = force ? newCoords : { lat: -23.7, lng: 132.8 }
        return
      }

      this.userCoords = newCoords
    },
    setPrevProtocol(value) {
      this.prevProtocol = value
    },
    setCollectionSurveyLength(value) {
      this.collectionSurveyLength = value
    },
    isLogin(value) {
      this.setLoginFlag = value
    },
    setSWRegistrationForNewContent(value) {
      this.SWRegistrationForNewContent = value
    },

    setNetworkOnline(value, silent=false) {
      let msg = 'Device is online'
      let type = 'positive'
      let msgDuration = 5
      if(!value) {
        msg = 'Device is offline. All ongoing downloads/uploads have been aborted'
        type = 'negative'
        msgDuration = 10
      }
      if(!silent) {
        notifyHandler(type, msg, null, msgDuration)
      }
      paratooSentryBreadcrumbMessage(msg)
      this.networkOnline = value
    },
    setLocationOnline(value) {
      this.locationOnline = value
    },
    setApiModelsArePopulating(value) {
      this.apiModelsArePopulating = value
      if (value === true) {
        this.apiModelsPopulatingStartTime = Date.now()
      } else {
        this.apiModelsPopulatingStartTime = null
      }
    },
    setUseExistingLocation(value) {
      this.useExistingLocation = value
    },
    setUseExistingVisit(value) {
      this.useExistingVisit = value
    },
    setDexieDbIsLoading(value) {
      this.dexieDbIsLoading = value
    },
    setTreeEllipseOn(value) {
      this.treeEllipseOn = value
    },
    setBasalDbhInstrumentType(value) {
      this.basalDbhInstrumentType = value
    },
    setCurrentOpportuneInfo(value) {
      this.currentOpportuneInfo = value
    },
    setPlotPoints(value) {
      this.plotPoints.push(value)
    },
    // clearPlotPoints() {
    //   this.plotPoints = []
    // },
    setBarcodeOpen(value) {
      this.barcodeOpen = value
    },
    setFaunaGroundCountProtocolType(value) {
      this.faunaGroundCountProtocolType = value
    },
    setPouchDbIsLoading(value) {
      this.pouchDbIsLoading = value
    },
    setPopulatedDexieTables(value) {
      this.populatedDexieTables.push(value)
    },
    setNewFetchAbortController() {
      this.fetchAbortControllers.push(new AbortController())
    },
    setPopulatedModels(value) {
      this.populatedModels.push(value)
    },
    deletePopulatedModels(value) {
      this.populatedModels = this.populatedModels.filter(
        (item) => item !== value,
      )
    },
    modelIsPopulating({ modelName }) {
      return this.apiModelsArePopulating && !this.populatedModels.includes(modelName)
    },
  },
  getters: {
    isExistingInterval: (state) => {
      return ({ intervalName }) => {
        return !isNil(state.trackedIntervals?.[intervalName])
      }
    },
  },
})

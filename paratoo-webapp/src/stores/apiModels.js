import { defineStore } from 'pinia'
import { useDocumentationStore } from './documentation'
import { useEphemeralStore } from './ephemeral'
import { useBulkStore } from './bulk'
import { initState } from '../assets/stateInits/apiModels'
import { union, findIndex, find, cloneDeep, isEmpty } from 'lodash'
import {
  paratooErrorHandler,
  paratooWarnHandler,
  paratooWarnMessage,
  notifyHandler,
  paratooSentryBreadcrumbMessage,
  b64toBlob,
  schemaFromModelName,
  saveCookie,
  getCookie,
  fetchGrassyWeedsFlightLines,
  findProjectsAndProtocolsFromModelName,
  isValidUrl,
  checkLocalStorageAvailability,
  networkConnectionStatus,
  isUrlReadyToContinue,
  isValidResource,
  isSessionDestroyed,
  allApiListModels,
} from 'src/misc/helpers'
import { doCoreApiGet } from 'src/misc/api'
import { dexieDB } from '../misc/dexieDB'
import * as cc from 'src/misc/constants'
import { useDataManagerStore } from './dataManager'
import PouchDB from 'pouchdb'
import { modelsToPopulate } from 'src/misc/apiLists'

const offlineMapDB = new PouchDB('offline-tiles')

import { useAuthStore } from './auth'
import S3 from 'src/misc/s3'

import AWS from 'aws-sdk'
let s3
if(process.env.VUE_APP_AWS_SECRET_ACCESS_KEY) {
  AWS.config.update({
    region: process.env.VUE_APP_AWS_REGION,
    accessKeyId: process.env.VUE_APP_AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.VUE_APP_AWS_SECRET_ACCESS_KEY
  })
  s3 = new AWS.S3()
} else {
  s3 = new S3()
}
const querystring = require('querystring')
initState.modelsIdMap = {}
initState.protocolModels = {},
initState.init = false

if (window?.Cypress) {
  initState.testString = null
  initState.testNum = null
  initState.testObj = null
  initState.testArrStr = null
  initState.testArrObj = null
  initState.testFn = null
}

export const useApiModelsStore = defineStore('apiModels', {
  state: () => (
    initState
      ? structuredClone(initState)
      : {
        models: {},
        modelsIdMap: {},
        protocolModels: {},
        init: false,
      }
  ),

  actions: {
    setModel(modelName, content=[]) {
      if(Array.isArray(content)) {
        this.models[modelName] = content
        const modelsWithNull = new Set()
        const mappedId = content.reduce((accum, curr, index) => {
          if(curr?.id) {
            accum[curr.id] = index
          } else {
            modelsWithNull.add(modelName)
          }
          return accum
        }, {})
        // create a collection id - index map for concatModel function
        //  for faster lookup
        this.modelsIdMap[modelName] = mappedId
        if (modelsWithNull.size > 0) {
          paratooWarnMessage(
            `The following models response array have last element as null issue #1944: ${[
              ...modelsWithNull,
            ].join(',')}`,
          )
        }
        checkLocalStorageAvailability()
      } else {
        paratooWarnMessage(`Model ${modelName} data is not an array, skipping setModel`)
      }
    },
    concatModel(modelName, content=[]) {
      if(!Array.isArray(content)) {
        paratooWarnMessage(`Model ${modelName} data is not an array, skipping concatModel`)
        return
      }

      if (
        !Array.isArray(this.models[modelName]) ||
        this.models[modelName].length === 0
      ) {
        this.setModel(modelName, content)
      } else {
        content.forEach((newContent) => {
          const index = this.modelsIdMap[modelName][newContent.id]
          if (index === undefined) {
            this.models[modelName].push(newContent)
            // add new content to id map
            this.modelsIdMap[modelName][newContent.id] =
              this.models[modelName].length - 1
          } else {
            this.models[modelName][index] = newContent
          }
        })
      }
      checkLocalStorageAvailability()
    },
    /**
     * More like a data migration, we only need this for the old version that doesn't have modelsIdMap 
     * as modelsIdMap is created when setModel is called
     */
    initModelsIdMap() {
      // if the map is not created yet, it won't have any keys
      let modelsWithNull = new Set()
      let nonArrayModels = new Set()
      if( Object.keys(this.models).length === Object.keys(this.modelsIdMap).length ) return
      for (const modelName in this.models) {
        if(!this.models[modelName] || !Array.isArray(this.models[modelName])) {
          paratooSentryBreadcrumbMessage(`Found model with non array value, modelName: ${modelName}, value: ${this.models[modelName]}`)
          nonArrayModels.add(modelName)
          continue
        }
        const mappedId = this.models[modelName].reduce((accum, curr, index) => { 
          if(curr?.id) {
            accum[curr.id] = index
          } else {
            modelsWithNull.add(modelName)
          }
          return accum
        }, {})
        // create a collection id - index map for concatModel function
        //  for faster lookup
        this.modelsIdMap[modelName] = mappedId
      }
      if(modelsWithNull.size > 0) {
        paratooWarnMessage(`The following models response array have last element as null issue #1944: ${[...modelsWithNull].join(',')}`)
      }
      if(nonArrayModels.size > 0) {
        paratooWarnMessage(`The following models response are non array: ${[...nonArrayModels].join(',')}`)
      }
    },
    /**
     * Initializes the LUT, ensure we alway have the latest lut
     */
    updateLuts() {
      paratooSentryBreadcrumbMessage(`Updating LUT`)
      for (const [modelName, value] in Object.entries(initState.models)) {
        this.models[modelName] = structuredClone(value)
      }
    },
    /**
     * Populates verified models.
     * @param {Array.<String>} modelNames list of model names (in singular forms) to
     * populate
     * @param {Array.<String>} [projectIds] an array of string-represented project IDs.
     * If provided, will `concatModel` rather than `setModel`
     * @param {Boolean} [useCache] whether to pass `use-cache` query param to Core
     */
    async populateVerifiedModels({
      modelNames,
      projectIds = [],
      useCache = true,
    }) {
      if (isEmpty(modelNames)) {
        paratooWarnMessage(
          `Programmer error: populateVerifiedModels() called with no model names`
        )
        return
      }

      const ephemeralStore = useEphemeralStore()
      const authStore = useAuthStore()
      if (!ephemeralStore.networkOnline) return

      // as token is needed for authorisation
      if (!authStore.token) return
      
      paratooSentryBreadcrumbMessage(
        `The following models have been specified for refresh with pdp verification (will${useCache ? '' : ' not'} use cache) (for project(s): ${projectIds}): ${JSON.stringify(modelNames)}`,
        'populateVerifiedModels',
      )
      if (
        projectIds?.length > 0 &&
        projectIds.some(p => typeof p !== 'string')
      ) {
        paratooWarnMessage(
          `Programmer error: populateVerifiedModels() provided list of projectIds but some/all are not cast to string. All provided projects: ${projectIds}. Projects with non-string: ${projectIds.filter(p => typeof p !== 'string')}`,
        )
      }
      const allAuthorisedModels = authStore.getAllAuthorisedModels()
      for (const modelName of modelNames) {
        if (!allAuthorisedModels.includes(modelName)) continue
        ephemeralStore.deletePopulatedModels(modelName)
      }
      if (isSessionDestroyed()) {
        throw paratooErrorHandler(`Failed to populate models, reason: your credentials have expired, please login again, \nmodels: [${modelNames}]`)
      }
      //for trace keep track of the models we actually refresh, as this method might get
      //called with models not assigned to the user, so can be worth understanding if
      //any: were skipped when they should have been; skipped when they shouldn't have;
      //or not skipped when they should have been
      const pid = Math.floor(Math.random() * 100)
      useEphemeralStore().apiModelsPopulatingProcesses[pid] = 0
      for (const [ index, modelName ] of Object.entries(modelNames)) {
        // if model is not authorised
        if (!allAuthorisedModels.includes(modelName)) {
          paratooWarnMessage(
            `User is not authorised to populate model '${modelName}'`,
          )
          continue
        }

        // extract endpoint from documentation
        const documentationStore = useDocumentationStore()
        let endPoint = null
        try {
          endPoint = await documentationStore.findEndpointFromModel(modelName)
        } catch (err) {
          paratooWarnHandler(
            `Tried to find endpoint for model '${modelName}'`,
            err,
          )
          continue
        }
        if (!endPoint) continue
        const base = `/api${endPoint}`
        const queryObject = { "use-cache": useCache } 

        // if we don't pass project_id and prot_uuid, core will use userProjects to filter outgoing data
        // we will hit api multiple times with different project ids
        if (projectIds.length > 0) {
          let uuids = []
          let projIds = []

          // model can be existed in multiple protocols
          const projectsAndProtocols =
            findProjectsAndProtocolsFromModelName(modelName)
          // project id params
          for (const projectId of Object.keys(projectsAndProtocols)) {
            if (!projectIds.includes(projectId)) continue
            uuids = uuids.concat(projectsAndProtocols[projectId])
            projIds.push(projectId)
          }
          uuids = union(uuids)
          projIds = union(projIds)
          for (const [i, p] of projIds.entries()) {
            queryObject[`project_ids[${i}]`] = p
          }
          // protocol uuid params
          for (const [i, p] of uuids.entries()) {
            // all prot uuids
            queryObject[`prot_uuids[${i}]`] = p
          }
        }
        // need to decode string as querystring makes it encoded
        const queryString = decodeURIComponent(querystring.stringify(queryObject))
        const url = `${base}?${queryString}`
        
        if (!isValidUrl(url, queryObject)) {
          const msg = `Failed to populate model: ${modelName} url: ${url}, reason: URL is not built correctly`
          paratooWarnMessage(msg)
          continue
        }

        const content = await doCoreApiGet(
          { urlSuffix: url },
          { root: true },
        ).catch((reason) => {
          const msg = `Failed to populate model: ${modelName}`
          paratooWarnHandler(msg, reason)
          return null
        })
        useEphemeralStore().apiModelsPopulatingProcesses[pid] = (parseInt(index) +1) / modelNames.length
        if (!content) continue

        // refactor data back into pre-Strapi4 format
        // so that we don't have to refactor even more functionality
        const mappedData = content.data ? mapData(content) : content
        // we want to concat model when projectIds is empty or undefined
        if(!projectIds?.length) {
          this.setModel(modelName, mappedData)
        } else {
          this.concatModel(modelName, mappedData)
        }

        //do this after we set the data in the store (`setModel` or `concatModel`)
        if (modelName === 'grassy-weeds-survey') {
          await fetchGrassyWeedsFlightLines()
        }
        ephemeralStore.setPopulatedModels(modelName)
      }
      delete useEphemeralStore().apiModelsPopulatingProcesses[pid]
    },

    /**
     * Populates or ensures population (e.g. for LUTs), for core API endpoint returned data.
     * @param {Object} param the parameter object
     * @param {String} param.modelName model name to refresh
     * @param {Boolean} [param.forceRefresh] optional flag to force a refresh (e.g. for
     * volatile data)
     * @param {Array.<Object>} [param.queryParams] list of optional query params - each
     * object has a property `key` that is the query param string key and another
     * property `value` that is the string value of the param. For example, an element in
     * the array might look like: { key: 'foo', value: 'bar' } for simple string params,
     * or { key: 'foo', value: { bar: 'foobar' } } for when the param value is an object
     */
    async refreshModel({ modelName, forceRefresh = false, queryParams = [] }) {
      const authStore = useAuthStore()
      if (!modelsToPopulate.appInitialization.includes(modelName)) {
        // if not authorised we return
        const allAuthorisedModels = authStore.getAllAuthorisedModels()
        if (!allAuthorisedModels.includes(modelName)) return
      }

      if (modelName === undefined) {
        throw new Error('Tried to refresh undefined modelName')
      }

      // check if we even need to GET the data from the endpoints again
      const anyExists =
        this.models[modelName] !== undefined &&
        this.models[modelName].length > 0 &&
        this.models[modelName][0] !== undefined

      // in dev or test mode, we don't need to force refresh luts.
      // need to make sure the initial data in api model store are valid and updated
      if (window.Cypress || process.env.DEV) {
        if (anyExists && modelName.includes('lut')) {
          return
        }
      }

      if (!forceRefresh && anyExists) {
        // for any model, if it's already in the store and we are not forcing the
        //refresh, then skip it
        return
      }

      useEphemeralStore().deletePopulatedModels(modelName)
      // TODO Check to see if the timestamp of refresh is old enough - if so, then hit the network
      const documentationStore = useDocumentationStore()
      // get endpoint based on documentation
      let urlSuffix = null
      //use try/catch so we can return from this function inside catch (which we can't do using the promise `.catch`)
      try {
        urlSuffix = await documentationStore.findEndpointFromModel(modelName, {
          root: true,
        })
      } catch (err) {
        const msg = `Failed to refresh model '${modelName}' when looking for it's endpoint, continuing...`
        paratooWarnHandler(msg, err)
        // notifyHandler('warning', msg)
        return
      }
      // populate everything we can (Strapi3 populated everything by default but now we need to specify)
      let populateSymbol = 'deep'
      // some protocols need to be deep populated as they have multi-level nested components inside
      // we don't apply to all models as it gonna extremely heavy
      if (urlSuffix !== '/protocols') {
        // find the protocol that have this modelName
        const protocol = this.models.protocol.find((protocol) => {
          const found = protocol.workflow.some(
            (workflow) => workflow.modelName === modelName,
          )
          return found
        })

        // get the config of this modelName
        const modelConfig = protocol?.workflow.find(
          (o) => o.modelName === modelName,
        )
        // check the deepPopulate flag
        if (modelConfig?.deepPopulate) {
          // change parameter to deep
          populateSymbol = 'deep'
        }
      }
      //append url params if they're provided
      const suffix = (() => {
        let base = `/api${urlSuffix}?populate=${populateSymbol}`
        if (
          queryParams &&
          Array.isArray(queryParams) &&
          queryParams.length > 0
        ) {
          for (const param of queryParams) {
            if (typeof param.value === 'object') {
              //can't send raw objects so need to stringify the; API should parse this
              base += `&${param.key}=${JSON.stringify(param.value)}`
            } else {
              base += `&${param.key}=${param.value}`
            }
          }
        }
        return base
      })()

      const content = await doCoreApiGet({ urlSuffix: suffix }, { root: true })
      const modelsToSentryBreadcrumb = [
        'plot-location',
        'plot-layout',
        'plot-visit',
        'weather-observation',
        'bird-survey',
        'bird-survey-observation',
      ]
      if (modelsToSentryBreadcrumb.includes(modelName)) {
        paratooSentryBreadcrumbMessage(
          `Response from doCoreApiGet for model name '${modelName}': ` +
            JSON.stringify(content, null, 2),
          'BirdsDebug',
        )
      }

      if (!content) return
      useEphemeralStore().setPopulatedModels(modelName)
      // refactor data back into pre-Strapi4 format
      // so that we don't have to refactor even more functionality
      if (content.data) {
        let mappedData = mapData(content)
        this.setModel(modelName, mappedData)
      } else {
        this.setModel(modelName, content)
      }
    },

    /**
     * Uses the form's modelConfig to find all the needed fields to populate drop-downs, ensure they are in state.apiModels
     */
    async doRefresh({ modelConfig }) {
      let modelsToRefresh = []
      for (const field of Object.values(modelConfig.fields)) {
        if (
          typeof field.optionsKey === 'string' &&
          (field.type === 'select' || field.type === 'filteredSelect')
        ) {
          modelsToRefresh.push(field.optionsKey)
        }
      }
      if (modelsToRefresh.length > 0) {
        await this.populateApiModels({
          forceRefresh: true,
          modelsToRefresh,
        })
      }
    },

    /**
     * update PopulatedModelsList so that workflow can check whether the protocol is ready or not
     */
    async refreshPopulatedModelsList() {
      if (!useAuthStore().token || isSessionDestroyed()) {
        paratooSentryBreadcrumbMessage(`Cannot refresh ephemeral's populated model list as we're not logged in`)
        return
      }
      // if token exists that means all the models have already been populated
      const ephemeralStore = useEphemeralStore()
      Object.keys(modelsToPopulate).forEach((key) => {
        // as workflow uses populatedModels to check whether the protocol is ready or not
        modelsToPopulate[key].forEach((model) => {
          if (!ephemeralStore.populatedModels.includes(model)) {
            ephemeralStore.setPopulatedModels(model)
          }
        })
      })
    },

    /**
     * Iterates over all keys in apiModels and refreshes them, unless provided with
     * specific models, which it refreshes those only
     *
     * TODO doc (params, etc.)
     */
    async populateApiModels({
      forceRefresh = false,
      modelsToRefresh = [],
      queryParams = [],
    }) {
      const startTime = Date.now()
      const documentationStore = useDocumentationStore()
      const isOnline = useEphemeralStore().networkOnline
      if (!isOnline) return
      console.info(
        `Populating apiModels with ${forceRefresh ? '' : 'no '}force refresh`,
      )

      var itemsToIterateOver

      if (modelsToRefresh.length > 0) {
        console.info(
          `The following models have been specified for refresh:`,
          modelsToRefresh,
        )
        itemsToIterateOver = modelsToRefresh
        itemsToIterateOver.push['plot-selection']
      } else {
        itemsToIterateOver = await documentationStore.refreshableModels(null, {
          root: true,
        })
        console.info(
          'No specific models have been provided, so refreshing all of apiModels:',
          itemsToIterateOver,
        )
      }

      // remove any duplicates because never needed
      itemsToIterateOver = union(itemsToIterateOver)

      // Some items not relevant/don't have endpoint
      // (but nonetheless appear in documentation)
      // - can't refresh user list, so remove
      // - Files not at standard endpoint, and shouldn't pre-cache all
      for (const i of ['user', 'file']) {
        const index = itemsToIterateOver.indexOf(i)
        if (index > -1) {
          itemsToIterateOver.splice(index, 1)
        }
      }

      for (var modelName of itemsToIterateOver) {
        try {
          await this.refreshModel({
            modelName: modelName,
            forceRefresh: forceRefresh,
            queryParams: queryParams,
          })
        } catch (err) {
          const error = paratooErrorHandler(
            `Couldn't refresh the model for ${modelName}.\nError:`,
            err,
          )
          throw error
        }
      }
      console.info(
        `Finished populating apiModels, took ${Date.now() - startTime}ms`,
      )
    },

    async processStaticAssets(assetType) {
      const authStore = useAuthStore()
      let contents = null

      switch (assetType) {
        // download and process species lists
        case cc.resourceTypes.SPECIES_LIST:
          // temporary stores all the names of the files, so that we don't need to call api
          contents = authStore.staticResourceNames.species
          if (contents) {
            this.processSpeciesData(null, contents, false)
            return
          }
          // call api to get the list of the names of the files.
          if (process.env.VUE_APP_AWS_SECRET_ACCESS_KEY) {
            s3.listObjectsV2(
              {
                Bucket: process.env.VUE_APP_AWS_BUCKET,
                Prefix: cc.awsAssetType.SPECIES_LIST,
              },
              this.processSpeciesData,
            )
          } else {
            s3.list(cc.awsAssetType.SPECIES_LIST, this.processSpeciesData)
          }
          break

        case cc.resourceTypes.MAP_TILES:
          // temporary stores all the names of the files, so that we don't need to call api
          contents = authStore.staticResourceNames.map
          if (contents) {
            this.processMapData(null, contents, false)
            return
          }
          if (process.env.VUE_APP_AWS_SECRET_ACCESS_KEY) {
            // call api to get the list of the names of the files.
            s3.listObjectsV2(
              {
                Bucket: process.env.VUE_APP_AWS_BUCKET,
                Prefix: cc.awsAssetType.MAP_TILES,
              },
              this.processMapData,
            )
          } else {
            s3.list(cc.awsAssetType.MAP_TILES, this.processMapData)
          }
          break

        case cc.resourceTypes.ANZSRC_FIELDS_OF_RESEARCH:
          // temporary stores all the names of the files, so that we don't need to call api
          contents = authStore.staticResourceNames.ANZSRCFieldsOfResearch
          if (contents) {
            this.processANZSRCFieldsOfResearchData(null, contents, false)
            return
          }
          if (process.env.VUE_APP_AWS_SECRET_ACCESS_KEY) {
            // call api to get the list of the names of the files.
            s3.listObjectsV2(
              {
                Bucket: process.env.VUE_APP_AWS_BUCKET,
                Prefix: cc.awsAssetType.ANZSRC_FIELDS_OF_RESEARCH,
              },
              this.processANZSRCFieldsOfResearchData,
            )
          } else {
            s3.list(
              cc.awsAssetType.ANZSRC_FIELDS_OF_RESEARCH,
              this.processANZSRCFieldsOfResearchData,
            )
          }
          break

        case cc.resourceTypes.GCMD_EARTH_SCIENCE_KEYWORDS:
          // temporary stores all the names of the files, so that we don't need to call api
          contents = authStore.staticResourceNames.GCMDScienceKeywords
          if (contents) {
            this.processGCMDScienceKeywordsData(null, contents, false)
            return
          }
          if (process.env.VUE_APP_AWS_SECRET_ACCESS_KEY) {
            // call api to get the list of the names of the files.
            s3.listObjectsV2(
              {
                Bucket: process.env.VUE_APP_AWS_BUCKET,
                Prefix: cc.awsAssetType.GCMD_EARTH_SCIENCE_KEYWORDS,
              },
              this.processGCMDScienceKeywordsData,
            )
          } else {
            s3.list(
              cc.awsAssetType.GCMD_EARTH_SCIENCE_KEYWORDS,
              this.processGCMDScienceKeywordsData,
            )
          }
          break
      }
    },

    async processGCMDScienceKeywordsData(error, data, storeData = true) {
      const authStore = useAuthStore()
      if (error) {
        const msg = 'Failed fetch data from S3 bucket'
        paratooWarnHandler(msg, error)
        return error
      }
      if (storeData)
        authStore.setStaticResourceNames(data.Contents, 'GCMDScienceKeywords')
      const contents = storeData ? data.Contents : data
      contents.forEach((content) => {
        this.populateDexie(content.Key, 'GCMDScienceKeywords')
      })
    },

    async processANZSRCFieldsOfResearchData(error, data, storeData = true) {
      const authStore = useAuthStore()
      if (error) {
        const msg = 'Failed fetch data from S3 bucket'
        paratooWarnHandler(msg, error)
        return error
      }
      if (storeData)
        authStore.setStaticResourceNames(
          data.Contents,
          'ANZSRCFieldsOfResearch',
        )
      const contents = storeData ? data.Contents : data
      contents.forEach((content) => {
        this.populateDexie(content.Key, 'ANZSRCFieldsOfResearch')
      })
    },

    async processSpeciesData(error, data, storeData = true) {
      const authStore = useAuthStore()
      if (error) {
        const msg = `Failed fetch data from S3 bucket. \nnetwork info: ${JSON.stringify(networkConnectionStatus(), null, 2)}`
        paratooWarnHandler(msg, error)
        return error
      }
      if (storeData) authStore.setStaticResourceNames(data.Contents, 'species')
      const contents = storeData ? data.Contents : data
      contents.forEach((content) => {
        // dexie table name
        const dexieTable = content.Key.replace('species-lists/', '')
          .replace('.json', '')
          .replace(/[0-9]/g, '')
          .replaceAll('vascular-flora', 'vascularFlora')
        this.populateDexie(content.Key, dexieTable)
      })
    },

    async processMapData(error, data, storeData = true) {
      const authStore = useAuthStore()
      if (error) {
        const msg = `Failed fetch map data from S3 bucket. \nnetwork info: ${JSON.stringify(networkConnectionStatus(), null, 2)}`
        paratooWarnHandler(msg, error)
        return error
      }
      if (storeData) authStore.setStaticResourceNames(data.Contents, 'map')
      const contents = storeData ? data.Contents : data
      contents.forEach((content) => {
        this.downloadAndProcessOfflineTiles(content.Key)
      })
    },
    /**
     * Read in the species list(s) and populate Dexie with them
     *
     * TODO detect changes rather that checking count
     * // if you need to nuke the dexieDB, use this:
     * https://gist.github.com/rmehner/b9a41d9f659c9b1c3340?permalink_comment_id=3747431#gistcomment-3747431
     */
    async populateDexie(assetKey, dexieTable) {
      const ephemeralStore = useEphemeralStore()
      if (!assetKey.includes('json')) {
        paratooWarnMessage(
          `Unable to populateDexie() without a valid asset key. Provided with ${assetKey}`
        )
        return 
      }

      if (!cc.dexieTableNames.includes(dexieTable)) {
        return `Skipping dexieDB operation asset key: ${assetKey}`
      }
      
      const cookieVal = getCookie(assetKey)
      if (cookieVal === 'done') {
        const msg = `Skipping dexieDB operation for asset key ${assetKey}: ${cookieVal}`
        paratooSentryBreadcrumbMessage(msg)
        return msg
      }

      // generate aws signed download url
      const url = this.createAssetUrl({ assetKey })
      const isPending = ephemeralStore.isPendingTrackedFetchRequest({
        url,
      })
      if (
        cookieVal === 'processing' ||
        isPending
      ) {
        paratooSentryBreadcrumbMessage(`Asset ${assetKey} is${cookieVal === 'processing' ? '' : ' not'} processing and ${isPending ? 'has' : 'does not have'} pending request`)
        return
      }

      // set dexie db loading flag true
      ephemeralStore.setDexieDbIsLoading(true)

      
      // adds abort controller
      const signal = ephemeralStore.fetchAbortControllers[0].signal
      const init = { method: 'GET', signal: signal }
      const urlStatus = isUrlReadyToContinue(url, false, null, init)
      if (!urlStatus.isReady) {
        // paratooWarnMessage(urlStatus.msg)
        return null
      }

      saveCookie(assetKey, 'processing')
      ephemeralStore.trackFetchRequest({ url: url })

      const status = fetch(url, init).then(
        (data) => {
          const result = data
            .json()
            .then((speciesData) => {
              // if unstable network connection
              if (!isValidResource(speciesData)) {
                let msg = 'Failed to download species data'
                msg += `\nasset-key: ${assetKey}`
                msg += `\nnetwork info: ${JSON.stringify(networkConnectionStatus(), null, 2)}`
                ephemeralStore.untrackFetchRequest({ url: url })
                return msg
              }
              const result = this.dexieTransaction(dexieTable, speciesData)
                .then(() => {
                  ephemeralStore.setDexieDbIsLoading(false)
                  saveCookie(assetKey, 'done')
                  ephemeralStore.untrackFetchRequest({ url: url })
                  return `dexieDB operations done asset key: ${assetKey}`
                })
                .catch((reason) => {
                  ephemeralStore.setDexieDbIsLoading(false)

                  const msg = `dexieDB operations failed, asset key: ${assetKey} \nnetwork info: ${JSON.stringify(networkConnectionStatus(), null, 2)}`
                  paratooWarnHandler(msg, reason)
                  saveCookie(assetKey, msg)
                  ephemeralStore.untrackFetchRequest({ url: url })
                  return msg
                })
              return result
            })
            .catch((reason) => {
              const msg = `dexieDB operations failed, asset key: ${assetKey} \nnetwork info: ${JSON.stringify(networkConnectionStatus(), null, 2)}`
              paratooWarnHandler(msg, reason)
              saveCookie(assetKey, msg)
              ephemeralStore.untrackFetchRequest({ url: url })
              return msg
            })
          return result
        },
      ).catch((reason) => {
        const msg = `dexieDB operations failed, asset key: ${assetKey} \nnetwork info: ${JSON.stringify(networkConnectionStatus(), null, 2)}`
        paratooWarnHandler(msg, reason)
        saveCookie(assetKey, msg)
        ephemeralStore.untrackFetchRequest({ url: url })
        return msg
      })
      return status
    },

    /**
     * Load unknown species inserted manually and populate Dexie with them.
     * NOTE: after logging in we should call this function, as we load data from Database
     *
     */
    async populateFieldNamesFromDB() {
      let fieldNames = []
      for (const modelName of modelsToPopulate.modelsWithFieldNames) {
        // append all field_names using modelNames
        //   e.g. in veg observation filed_names are in the NVIS association
        let allVegAssociation = []
        switch (modelName) {
          case 'vegetation-mapping-observation':
            this.cachedModel({ modelName }).forEach((value) => {
              // TODO: field that have `x-paratoo-csv-list-taxa`
              if (value.NVIS_level_5_vegetation_association_information) {
                value.NVIS_level_5_vegetation_association_information.forEach(
                  (v) => {
                    if (v.field_name) allVegAssociation.push(v)
                  },
                )
              }
            })
            break
          default:
            this.cachedModel({ modelName }).forEach((value) => {
              if (value.field_name) allVegAssociation.push(value)
            })
            break
        }
        fieldNames = fieldNames.concat(
          allVegAssociation.map((value) => ({
            canonicalName: value.field_name,
            scientificName: '',
            taxonRank: '',
          })),
        )
      }

      if (fieldNames.length == 0) return
      // load/update filed names
      await this.dexieTransaction('fieldNames', fieldNames)
    },

    /**
     * populate Dexie with field name and mode.
     *
     * @param {String} tableName name of the field
     * @param {Array.<Object>} data array of names
     * @param {Boolean} updateContent if true then update existing data
     *
     */
    async dexieTransaction(tableName, data) {
      // if the datatype is csvText, we will transform text into an array of objects
      if (!data) return false

      // update dexieDB
      const result = dexieDB
        .transaction('rw', dexieDB[tableName], () => {
          const result = dexieDB[tableName]
            .bulkAdd(data)
            .then(() => {
              return true
            })
            .catch((err) => {
              let msg = `Failed to initialise the ${tableName} error: ${err}`
              notifyHandler('negative', msg)
              return false
            })
          return result
        })
        .catch((err) => {
          let msg = `Transaction failed when adding ${tableName}`
          notifyHandler('negative', msg)
          if (err?.message.includes('QuotaExceededError')) {
            msg += ' reason: Storage Quota exceeded'
            navigator.storage.estimate().then(({ usage, quota }) => {
              const storageQuota = (quota / 1024 ** 2).toFixed(0)
              const storageUsage = (usage / 1024 ** 2).toFixed(0)
              msg += `,  Usage: ${storageUsage} MB, StorageQuota: ${storageQuota} MB, hostInfo: ${
                useAuthStore().getHostInfo
              }`
              paratooWarnHandler(msg,err)
            })
          }
          return false
        })
      // make an indication in the console about whether the validation allowed something for debug purposes
      return result
    },
    // parse all the directories and sub directories
    async getFileLists(baseUrl) {
      // adds abort controller
      const ephemeralStore = useEphemeralStore()
      const signal = ephemeralStore.fetchAbortControllers[0].signal
      const result = fetch(baseUrl, { method: 'GET', signal: signal })
        .then((resp) => {
          const urls = resp
            .text()
            .then((data) => {
              const exp = /"([^"]*json")/g
              const links = data.match(exp)
              return links
            })
            .catch((reason) => {
              console.log('Failed to parse all links ', reason)
              return []
            })
          return urls
        })
        .catch((reason) => {
          console.log('Failed to parse all links ', reason)
          return []
        })
      return result
    },
    // checks whether offline tiles need refreshing
    async needOfflineTilesUpdate() {
      const result = offlineMapDB
        .info()
        .then((info) => {
          // 1900+ tiles exist into pouchDB we don't need to update
          return !(info.doc_count > 1900)
        })
        .catch((error) => {
          paratooWarnHandler('PouchDB operation failed reason: ', error)
          return true
        })
      return result
    },
    async downloadAndProcessOfflineTiles(assetKey) {
      const ephemeralStore = useEphemeralStore()
      const cookieVal = getCookie(assetKey)
      if (cookieVal === 'done') {
        paratooSentryBreadcrumbMessage(`Asset ${assetKey} already processed`)
        return
      }

      const url = this.createAssetUrl({ assetKey })
      const isPending = ephemeralStore.isPendingTrackedFetchRequest({
        url,
      })
      if (
        cookieVal === 'processing' ||
        isPending
      ) {
        paratooSentryBreadcrumbMessage(`Asset ${assetKey} is${cookieVal === 'processing' ? '' : ' not'} processing and ${isPending ? 'has' : 'does not have'} pending request`)
        return
      }

      // adds abort controller
      const signal = ephemeralStore.fetchAbortControllers[0].signal
      const init = { method: 'GET', signal: signal }
      const urlStatus = isUrlReadyToContinue(url, false, null, init)
      if (!urlStatus.isReady) {
        // paratooWarnMessage(urlStatus.msg)
        return null
      }

      saveCookie(assetKey, 'processing')
      ephemeralStore.trackFetchRequest({ url: url })

      fetch(url, init).then((data) => {
        data
          .json()
          .then((mapOfflineData) => {
            // if unstable network connection
            if (!isValidResource(mapOfflineData)) {
              let msg = 'failed to download offline tiles'
              msg += `\nasset-key: ${assetKey}`
              msg += `\nnetwork info: ${JSON.stringify(networkConnectionStatus(), null, 2)}`
              paratooWarnMessage(msg)
              ephemeralStore.untrackFetchRequest({ url: url })
              return 
            }
            // downloaded tiles successfully
            this.populateOfflineTiles(mapOfflineData, assetKey)
              .catch(
                (reason) => {
                  const msg = `Failed to populate tile image, \nnetwork info: ${JSON.stringify(networkConnectionStatus(), null, 2)}`
                  paratooWarnHandler(msg, reason)
                  ephemeralStore.untrackFetchRequest({ url: url })
                },
              )
              .then(() => {
                //only untrack AFTER processing, as this processing can take a moment
                ephemeralStore.untrackFetchRequest({ url: url })
              })
          })
          .catch((reason) => {
            const msg = `Failed to parse downloaded offline tiles, \nnetwork info: ${JSON.stringify(networkConnectionStatus(), null, 2)}`
            paratooWarnHandler(msg, reason)
            ephemeralStore.untrackFetchRequest({ url: url })
          })
      })
        .catch((reason) => {
          const msg = `Failed to download offline tiles, \nnetwork info: ${JSON.stringify(networkConnectionStatus(), null, 2)}`
          paratooWarnHandler(msg, reason)
          ephemeralStore.untrackFetchRequest({ url: url })
        })
    },
    // populate tiles for offline use
    async populateOfflineTiles(mapOfflineData, assetKey) {
      if (!mapOfflineData) return

      const timestamp = Date.now()
      let count = mapOfflineData['rows'].length
      for (const raw of mapOfflineData['rows']) {
        // push data to pouchDB
        offlineMapDB
          .put({
            _id: raw['id'],
            _rev: undefined,
            timestamp: timestamp,
          })
          .then((result) => {
            const encoded = raw['data']
            const imageType = raw['content_type']
            const imageBlob = b64toBlob(encoded, imageType)
            offlineMapDB
              .putAttachment(
                raw['id'],
                'tile',
                result.rev,
                imageBlob,
                imageType,
              )
              .catch(function () {})
            count = count - 1
            if (count == 0) {
              saveCookie(assetKey, 'done')
            }
          })
          .catch(function () {
            count = count - 1
            if (count == 0) {
              saveCookie(assetKey, 'done')
            }
          })
      }
    },
    createAssetUrl({ assetKey }) {
      let url
      if (process.env.VUE_APP_AWS_SECRET_ACCESS_KEY) {
        url = s3.getSignedUrl('getObject', {
          Bucket: process.env.VUE_APP_AWS_BUCKET,
          Key: assetKey,
          Expires: 100,
        })
      } else {
        url = s3.getObjectUrl(assetKey)
      }
      return url
    },
    /**
     *TODO remove - redundant
     *
     * @param {Number} plotVisitId
     * @returns plotId
     */
    plotIdFromVisit(plotVisitId) {
      const plotVisitData = this.models['plot-visit'].find(
        ({ id }) => id === plotVisitId,
      )
      if (!plotVisitData) return null
      return plotVisitData['plot_layout'].id
    },
    /**
     * Filter survey's data related to the current plot.
     * Only applicable for survey with plot visit id and plot layout id.
     * Otherwise unexpected bugs may occur
     *
     * TODO delete - replaced by `dataManager.surveyDataFromPlot`
     *
     * @param {*} state
     * @returns
     */
    filterSurveyDataFromPlot(modelName, plotLayoutId) {
      const modelData = this.cachedModel({ modelName: modelName })
      if (!modelData) {
        paratooWarnMessage('invalid model name')
        return null
      }
      // filter setup survey related to current plot
      const associatedModelData = modelData
        .filter(({ plot_visit }) => {
          const plotId = this.plotIdFromVisit(plot_visit.id)
          return plotLayoutId === plotId
        })
        .sort((a, b) => a.id - b.id)
      return associatedModelData
    },
    /**
     * Filters survey data from plot context based on the given model name.
     *
     * @param {string} modelName - The name of the model to filter the data for.
     * @return {Array} The filtered survey data associated with the given model name.
     */
    filterSurveyDataFromPlotContext(modelName) {
      const { staticPlotContext } = useBulkStore()
      if (!staticPlotContext.plotVisit) {
        return []
      }
      const modelData = this.cachedModel({ modelName })
      if (!modelData) {
        paratooWarnMessage('invalid model name')
        return []
      }

      // filter setup survey related to current plot
      const associatedModelData = modelData
        .filter((survey) => {
          const surveyVisitId = survey.plot_visit?.id
          const currentVisitId = staticPlotContext.plotVisit?.id
          return surveyVisitId === currentVisitId
        })
        .sort((a, b) => a.id - b.id)
      return associatedModelData
    },
    filterObsFromSurvey(obsModelName, survey_metadata) {
      const obsData = this.cachedModel({ modelName: obsModelName })
      const associatedObs = obsData.filter((entry) => {
        // look for survey id in entry
        // where the props has survey_metadata object
        const id = Object.values(entry)?.reduce((acc, prop) => {
          if (prop?.survey_metadata) {
            return prop.id
          }
          return acc
        }, null)
        return id === survey_metadata
      })
      return associatedObs
    },
    protIdFromUuid(uuid) {
      if (!uuid) return null

      const relatedProt = this.models.protocol.find(
        (prot) => prot.identifier === uuid,
      )

      return {
        name: relatedProt.name,
        id: relatedProt?.id,
        version: relatedProt.version,
      }
    },
    protUuidFromId(id) {
      const inputType = typeof id
      if (inputType !== 'number') {
        paratooWarnMessage(
          `Programmer error: protUuidFromId() passed a string input but expected int. If the string is a number, weak equality is used, but the passed value should be an int. Passed value (type=${inputType}): ${id}`,
        )
      }

      if (!id) return null

      const relatedProt = this.models.protocol.find(
        //use weak equality in case we passed the ID as a string
        (prot) => prot.id == id
      )

      return {
        name: relatedProt.name,
        id: id,
        uuid: relatedProt.identifier,
        version: relatedProt.version,
      }
    },
    /**
     *
     * @param {*} type 'vegetation' or 'fauna'
     * @returns
     */
    plotLayoutPoints(
      type,
      id = null,
      points = ['SW', 'NW', 'NE', 'SE'],
      fromCollectionData = null,
    ) {
      const types = [
        'C',
        'vegetation',
        'F',
        'fauna',
      ]
      if(!types.includes(type)) {
        const error = new Error(`invalid type ${type} for plot layout points`)
        paratooWarnMessage(error.message)
        throw error
      }

      const dataManager = useDataManagerStore()
      const layout = dataManager.cachedModel({ modelName: 'plot-layout' })

      let selectedPlot = null
      if (fromCollectionData) {
        selectedPlot = fromCollectionData
      } else {
        if (id === null) {
          const { getPlotLayoutId } = useBulkStore()

          if (!getPlotLayoutId) return {}

          id = getPlotLayoutId
        }

        selectedPlot = layout.find((layout) => {
          if (layout?.id) {
            return layout.id === id
          }
          if (layout.temp_offline_id && id.temp_offline_id) {
            return layout.temp_offline_id === id.temp_offline_id
          }
          if (layout.temp_offline_id && !id.temp_offline_id && id) {
            return layout.temp_offline_id === id
          }
        })
      }

      const attrToFind =
        type === 'vegetation' || type === 'C'
          ? 'plot_points'
          : 'fauna_plot_point'
      let plotPoints = selectedPlot?.[attrToFind]

      // if hit this case likely fauna and core are in the queue or apiModel separately
      if (!plotPoints?.length) {
        // look to the queue for the fauna collection
        const faunaCollection = dataManager.getQueuedAmendedFaunaPlotCollection(id)
        plotPoints = faunaCollection?.['plot-layout']?.fauna_plot_point
      }

      if (!plotPoints?.length) {
        paratooWarnMessage('plot layout points not found')
        return {}
      }

      const sortedPoints = points.map((point) =>
        plotPoints.find((plotPoint) => {
          if (plotPoint.name?.symbol) {
            return point === plotPoint.name.symbol
          }
          //plot was probably collected offline (still pending upload), so the full LUT
          //object (with `symbol` key) is not defined. Instead, the `name` is the symbol
          return point === plotPoint.name
        }),
      )

      const centerCoords = plotPoints.find((plotPoint) => {
        if (plotPoint.name?.symbol) {
          return plotPoint.name.symbol === 'C'
        }
        return plotPoint.name === 'C'
      })
      return { latLngs: sortedPoints, color: 'blue', centerCoords, plotPoints }
    },
    protIdFromModelName(modelName, returnTypeUUID = false) {
      const protocol = this.models.protocol.find((protocol) => {
        const found = protocol.workflow.some((w) => {
          if (w.modelName === modelName) return true
          if (w.relationOnAttributesModelNames)
            return w.relationOnAttributesModelNames.includes(modelName)
          return false
        })
        return found
      })
      return returnTypeUUID ? protocol?.identifier : protocol?.id
    },
    /**
     * Check if the plot have fauna plot
     * @param {*} id plot layout id
     * @returns Boolean
     */
    checkPlotHaveFauna(plotId) {
      const allPlots = useDataManagerStore().cachedModel({
        modelName: 'plot-layout',
      })
      const selectedPlot = allPlots.find(
        ({ id, temp_offline_id }) => {
          if(temp_offline_id) {
            return temp_offline_id === plotId.temp_offline_id
          }
          return id === plotId
        }
      )
      if (!selectedPlot) {
        console.error('Plot not found')
        return false
      }
      if(plotId?.temp_offline_id) {
        const faunaPlotCollection = useDataManagerStore().getQueuedAmendedFaunaPlotCollection(plotId)
        //This happen when user submit a core plot first without fauna then submit the fauna plot after
        //but both of these collections have not been synced yet
        if(faunaPlotCollection) {
          return faunaPlotCollection['plot-layout'].fauna_plot_point?.length > 0
        }
      } 
      console.log(selectedPlot)
      return selectedPlot?.fauna_plot_point?.length > 0
    },
    async modelsOfProtocol({ protocolId }) {
      // grab associated protocol workflow
      const workflow = this.protocolInformation({
        protId: protocolId,
      }).workflow

      if (!workflow) return []

      const documentationStore = useDocumentationStore()

      const models = []
      for (const workflowItem of workflow) {
        models.push(workflowItem.modelName)

        if (
          workflowItem.newInstanceForRelationOnAttributes &&
          Array.isArray(workflowItem.newInstanceForRelationOnAttributes)
        ) {
          const documentation = await documentationStore.getDocumentation()
          const schemaForWorkflowItem = schemaFromModelName(
            workflowItem.modelName,
            documentation,
          )
          for (const childObFieldName of workflowItem.newInstanceForRelationOnAttributes) {
            if (
              schemaForWorkflowItem?.properties?.[childObFieldName]?.[
                'x-model-ref'
              ]
            ) {
              models.push(
                schemaForWorkflowItem.properties[childObFieldName][
                  'x-model-ref'
                ],
              )
            }
          }
        }
      }

      return models
    },
    checkFaunaPlotDependency(protocolId) {
      const associatedProtocol = this.models.protocol.find(
        (o) => o.id === protocolId,
      )
      if (!associatedProtocol)
        throw paratooErrorHandler('Associated protocol not found', new Error('Associated protocol not found'))
      return associatedProtocol.faunaPlotDependent
    }
  },
  getters: {
    findModelsAndChildObsFromProtocolUUID: (state) => {
      return ({ protUuid, forceCacheRevalidation = false }) => {
        if (
          !forceCacheRevalidation &&
          !isEmpty(state.protocolModels?.[protUuid]) &&
          //want to make sure we don't have extremely old data, for example, if the user
          //hasn't had project updates in a while. 1 day seems appropriate, but we can
          //adjust this or create a better cache expiry condition if this approach is an
          //issue
          (
            !state.protocolModels[protUuid]?.cacheTime ||
            Date.now() - state.protocolModels[protUuid].cacheTime <= 86400000   //1 day
          )
        ) {
          /* paratooSentryBreadcrumbMessage(
            `Protocol models for ${protUuid} already cached, using that instead`,
          ) */
          return cloneDeep(state.protocolModels[protUuid])
        }

        const documentationStore = useDocumentationStore()
        const plots = ['plot-visit', 'plot-layout']
        let allModels = []
        let plotModels = []
      
        const workFlow = {}
        const modelsAndRelations = {}
      
        const resolved = state.protIdFromUuid(protUuid)
        const protocol = state.protocolInformation({
          protId: resolved?.id,
        })
      
        if (!protocol?.workflow) {
          paratooWarnMessage(`Failed to get workflow for protocol with UUID=${protUuid}`)
        }
      
        for (const w of protocol?.workflow || []) {
          if (!w.newInstanceForRelationOnAttributes) {
            modelsAndRelations[w.modelName] = null
            continue
          }
          modelsAndRelations[w.modelName] = {}
          for (let i = 0; i < w.newInstanceForRelationOnAttributes.length; i++) {
            modelsAndRelations[w.modelName][w.newInstanceForRelationOnAttributes[i]] =
              w.relationOnAttributesModelNames[i]
          }
        }
      
        workFlow['raw'] = modelsAndRelations
        let modelNames = Object.keys(modelsAndRelations)
        
      
        // delete plot models
        plots.forEach((p) => {
          if (!modelNames.includes(p)) return
          plotModels.push(p)
          modelNames = modelNames.filter((item) => item !== p)
        })
        workFlow['surveyModel'] = modelNames[0]
        workFlow['modelNames'] = modelNames
        workFlow['plotModels'] = plotModels
        if (plotModels.length > 0 && !plotModels.includes('plot-selection')) {
          workFlow['plotModels'] = plotModels.concat(['plot-selection'])
        }
      
        modelNames.forEach((m) => {
          allModels.push(m)
          if (!modelsAndRelations[m]) return
          for (const f of Object.keys(modelsAndRelations[m])) {
            //follow child ob relations
            allModels.push(modelsAndRelations[m][f])
          }
        })
        
        const apiListModels = allApiListModels()
      
        const relationalModels = new Set()
        for (const model of allModels) {
          const modelDeps = documentationStore.modelDependencies({
            modelName: model,
            modelsAcc: [],
            includeSoftLinks: true,
            includeLuts: false,
          })
          for (const dep of modelDeps) {
            if (
              //don't care about LUTs
              !dep.startsWith('lut-') &&
              //must be refreshable
              apiListModels.includes(dep)
            ) {
              relationalModels.add(dep)
            }
          }
        }
        
        const relationalModelsArr = Array.from(relationalModels)
        if (relationalModelsArr?.length) {
          for (const relationalModel of relationalModelsArr) {
            if (
              //don't push duplicates
              !allModels.includes(relationalModel)
            ) {
              allModels.push(relationalModel)
            }
          }
        }
      
        workFlow['allModels'] = allModels

        Object.assign(state.protocolModels, {
          [protUuid]: {
            ...cloneDeep(workFlow),
            cacheTime: Date.now(),
          },
        })
      
        return workFlow
      }
    },
    /**
     * Returns a reduced version of `models` based on provided models to include
     *
     * @param {Array.<String>} modelsToInclude an array of model names
     * @returns {Object} a reduced store state based on the provided models to include
     */
    reducedModels: (state) => {
      return ({ modelsToInclude }) => {
        let reducedModels = {}
        for (const [key, value] of Object.entries(state.models)) {
          if (modelsToInclude.includes(key)) {
            reducedModels[key] = value
          }
        }
        return reducedModels
      }
    },
    /**
     * Retrieves the information of a specified ProtocolId.
     * @param {Number} protId - Protocol id to retreive from the store
     * @returns
     */
    protocolInformation: (state) => {
      return ({ protId }) => {
        let temp =
          state.models.protocol[
            findIndex(state.models.protocol, (o) => {
              return o.id === protId
            })
          ]
        if (
          //empty object
          isEmpty(temp) ||
          //empty/falsy primitive
          !temp
        ) {
          paratooWarnMessage(`Tried to get protocol information for protocol with ID=${protId} but got: ${JSON.stringify(temp)}.`)
        } else if (!Array.isArray(temp?.workflow) || temp?.workflow?.length === 0) {
          //we have protocol info, but empty workflow
          if (temp?.isIncomplete) {
            paratooSentryBreadcrumbMessage(`Skipping warning about missing workflow for protocol with ID=${protId}`)
          } else {
            paratooWarnMessage(`Got protocol information for protocol with ID=${protId} but the protocol has an empty workflow`)
          }
        }
        return temp
      }
    },
    lutDescription: (state) => {
      return ({ symbol, modelName }) => {
        var found
        try {
          found = find(state.models[modelName], function (o) {
            return o.symbol === symbol
          })
        } catch {
          paratooWarnMessage(
            `Couldn't find description for symbol=${symbol} and modelName=${modelName}`,
          )
          return ''
        }
        if (found && found.description) return found.description
        else return ''
      }
    },
    /**
     * Gets the corresponding label for the provided symbol
     *
     * @param {String} symbol the symbol to look up
     * @param {String} modelName the model name of the LUT to search over
     * @param {Boolean} [suppressWarnings] whether to suppress the warning message when
     * the LUT label cannot be resolved. Useful if we're only checking if it exists, such
     * as when determining if a custom LUT is from a vocab or is custom text. Defaults to
     * false
     *
     * @returns {(String|null)} the resolved label, or null if not found
     */
    resolvedLutSymbol: (state) => {
      return ({ symbol, modelName, suppressWarnings }) => {
        var label
        let errMsg = `Couldn't find label for symbol=${symbol} and modelName=${modelName}`
        try {
          label = find(state.models[modelName], {
            symbol: symbol,
          }).label
        } catch (err) {
          if (!suppressWarnings) paratooWarnMessage(errMsg)
          return null
        }
        if (label) {
          return label
        } else {
          if (!suppressWarnings) {
            paratooWarnMessage(
              errMsg +
                '. LUT symbol exists but no corresponding label was found.',
            )
          }
          return null
        }
      }
    },
    /**
     * Gets the voucher variant for floristics based on the selected visit. Some
     * protocols use floristics (visit-based) but we need to know what the variant is
     *
     * @param {*} state
     * @returns 'full' or 'lite'
     */
    voucherVariant: (state) => {
      return ({ protId }) => {
        var selectedVisit
        const bulkStore = useBulkStore()
        try {
          selectedVisit = bulkStore.collectionGetForProt({
            protId: protId,
          })['plot-visit']
        } catch {
          //we shouldn't get here as we enforce doing the location/visit before continuing
          let msg = "Tried to access plot visits but couldn't"
          paratooWarnMessage(msg)
          notifyHandler('warning', msg)
        }
        return (() => {
          //search over full and lite for a voucher with survey containing the selected visit
          const full = state.models['floristics-veg-voucher-full'].find(
            (voucher) => {
              //get the current voucher's survey
              const survey = state.models['floristics-veg-survey-full'].find(
                (survey) => {
                  return voucher.floristics_veg_survey_full.id === survey.id
                },
              )

              //try/catch as plot visit ID might be undefined
              try {
                return survey.plot_visit.id === selectedVisit.id
              } catch {
                return
              }
            },
          )
          if (full) return 'full'

          const lite = state.models['floristics-veg-voucher-lite'].find(
            (voucher) => {
              //get the current voucher's survey
              const survey = state.models['floristics-veg-survey-lite'].find(
                (survey) => {
                  return voucher.floristics_veg_survey_lite.id === survey.id
                },
              )

              //try/catch as plot visit ID might be undefined
              try {
                return survey.plot_visit.id === selectedVisit.id
              } catch {
                return
              }
            },
          )
          if (lite) return 'lite'

          return undefined
        })()
      }
    },
    layoutForLocation: (state) => {
      return ({ locationId }) => {
        return state.models['plot-layout'].find(
          (o) => o.plot_location.id === locationId,
        ).id
      }
    },
    voucherFromFieldName: (state) => {
      return ({ fieldName, protocolVariant }) => {
        return state.models[`floristics-veg-voucher-${protocolVariant}`].find(
          (v) => {
            return v.field_name === fieldName
          },
        )
      }
    },

    voucherFromID: (state) => {
      return ({ ID, protocolVariant }) => {
        return state.models[`floristics-veg-voucher-${protocolVariant}`].find(
          (v) => {
            return v.id === ID
          },
        )
      }
    },

    /**
     * Generic getter for grabbing all results from a single apiModel
     * Should eventually do some logic to ensrue availability
     *
     * @param {*} Options containing modelName
     * @returns
     */
    cachedModel: (state) => {
      return ({ modelName }) => {
        // TODO ensure something is saved here/warn when not available/request
        if (!Object.keys(state.models).includes(modelName)) {
          state.models[modelName] = []
        }
        return state.models[modelName]
      }
    },
    /**
     * Looks in the protocol's workflow for a variant
     *
     * @param {Number} protocolId protocol ID to search over
     * @returns {String} the protocol variant
     */
    protocolVariant: (state) => {
      return ({ protocolId }) => {
        const relevantProtocol = state.models.protocol.find(
          (o) => o.id === protocolId,
        )
        for (const workflowItem of relevantProtocol.workflow) {
          if (workflowItem['protocol-variant'])
            return workflowItem['protocol-variant']
        }
      }
    },
    soilPitInCurrentPlot() {
      const dataManager = useDataManagerStore()
      const plotLabel = dataManager.currentPlotLabel
      if (!plotLabel) return []
      const modelName = (variant) => `soil-pit-characterisation-${variant}`

      let associatedSoilPitSurvey = []
      ;['lite', 'full'].forEach((variant) => {
        const variantModelName = modelName(variant)
        const relatedSoilPitSurvey = dataManager.cachedModel({
          modelName: variantModelName,
        })
        let survey = relatedSoilPitSurvey?.filter((pit) => {
          // soil pit full have lab phase which doesn't have soil_pit_id
          if (!pit.soil_pit_id) return false
          if (!pit.plot_visit) return true
          // find pit that is in the same plot
          const plotLayout =
            pit.plot_visit.plot_layout.plot_selection.plot_label
          return plotLayout === plotLabel
        })
        survey = cloneDeep(survey).map(i => {
          i.variant = variant
          return i
        })
        associatedSoilPitSurvey = associatedSoilPitSurvey.concat(survey)
      })

      return associatedSoilPitSurvey || []
    },
    nextSoilPitId() {
      const dataManager = useDataManagerStore()
      const plotLabel = dataManager.currentPlotLabel

      const date = new Date()
      const day = ('0' + date.getDate()).slice(-2)
      const month = ('0' + (date.getMonth() + 1)).slice(-2)
      const year = date.getFullYear().toString()
      const soilPitOptions = this.soilPitInCurrentPlot.map(
        (survey) => survey.soil_pit_id,
      )
      let nextAvailableLabel = `${plotLabel}-${day + month + year}`
      // check if it clashes with an existing soil pit
      for (let i = 0; i < soilPitOptions.length; i++) {
        if (soilPitOptions[i] === nextAvailableLabel) {
          // don't want to break as we might more more clashes
          nextAvailableLabel = `${plotLabel}-${day + month + year}-${i + 1}`
        }
      }
      return nextAvailableLabel
    },
  },
})

/**
 * Recursive function to reformat strapi4 data to the way the app expects it.
 * Removes the 'data' and 'attributes' wrapper objects
 */
function mapData(data) {
  if (data) {
    if (Array.isArray(data)) {
      let newArray = []
      for (let item of data) {
        newArray.push(mapData(item))
      }
      return newArray
    } else if (data.attributes) {
      let newData = { id: data.id }
      for (let attr of Object.keys(data.attributes)) {
        newData[attr] = mapData(data.attributes[attr])
      }
      return newData
    } else if (data.data) {
      return mapData(data.data)
    } else if (typeof data === 'object') {
      let newData = {}
      for (let attr of Object.keys(data)) {
        newData[attr] = mapData(data[attr])
      }
      return newData
    }
  }
  return data
}
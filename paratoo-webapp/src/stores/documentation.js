import { defineStore } from 'pinia'

import {
  union,
  remove,
  find,
  cloneDeep,
  isEmpty,
} from 'lodash'
import * as constants from 'src/misc/constants'
import {
  paratooErrorHandler, 
  paratooWarnMessage, 
  schemaNameToModelName, 
  modelNameToSchemaName ,
  checkDocVersion,
  paratooSentryBreadcrumbMessage,
  isSessionDestroyed,
  waitForCondition,
} from 'src/misc/helpers'
import { doCoreApiGet } from 'src/misc/api'
import { useApiModelsStore } from './apiModels'
import Dexie from 'dexie'
import { useAuthStore } from './auth'
import { useEphemeralStore } from './ephemeral'

const db = new Dexie('core-api')
db.version(1).stores({
  documentation: 'url,version,data,user,savedAt',
})

export const useDocumentationStore = defineStore('documentation', {
  state: () => ({
    documentation: {},
  }),

  actions: {
    setDocumentation(content) {
      if (content) this.documentation = cloneDeep(content)
    },

    async clearDocumentation() {
      const ephemeralStore = useEphemeralStore()
      // should not delete if token expired as we can not download again without valid token
      if (isSessionDestroyed()) return

      paratooSentryBreadcrumbMessage('Deleting existing documentation.')
      const url = `${constants.coreApiPrefix}/documentation/swagger-user.json`
      await db.open()
      await db.documentation.delete(url)
      this.setDocumentation(null)
    },
    /**
     * Ensures that documentation exists in the local state, and returns the whole documentation object
     * @param {*} param0
     */
    async getDocumentation() {
      try {
        const url = `${constants.coreApiPrefix}/documentation/swagger-user.json`
        const content = await db.documentation.get({ url })
        const authStore = useAuthStore()

        if (!authStore.token) {
          paratooSentryBreadcrumbMessage(`Cannot get documentation as user is not logged in`)
          return this.documentation
        }

        if (isSessionDestroyed()) {
          throw paratooErrorHandler('Failed to download documentation, reason: your credentials have expired, please login again.')
        }
        // if not exist
        if (!content?.data) {
          paratooSentryBreadcrumbMessage('Documentation not found.')
          await this.downloadDocumentation()
          return cloneDeep(this.documentation) 
        }

        // checks when it was saved
        const currentTime = Date.now()
        const diff = (currentTime - content.savedAt) / (1000 * 60)
        if (
          diff < constants.documentationVersionCheck 
          && content.user == authStore.userProfile.id 
        ) {
          this.setDocumentation(content.data)
          return cloneDeep(this.documentation)
        }
        paratooSentryBreadcrumbMessage(`Documentation was created ${diff} minutes ago`)
        const doc = await checkDocVersion(content.data)
        if (doc) {
          if (content?.data) {
            // update time
            content.savedAt = Date.now()
            await db.documentation.put(cloneDeep(content))
            this.setDocumentation(content.data)
            return cloneDeep(this.documentation)
          }
          
        } 

        // doc is outdated
        paratooSentryBreadcrumbMessage('Trying to refresh documentation, doc is outdated')
        await this.downloadDocumentation()
        
        // although lut is baked in the app, we still have to update the state in apiModels store
        useApiModelsStore().updateLuts()
        return cloneDeep(this.documentation)

      } catch (error) {
        const msg = `An error occurred while loading documentation, reason: ${JSON.stringify(error)}`
        paratooWarnMessage(msg)
        await this.downloadDocumentation()
        return cloneDeep(this.documentation)
      }
    },

    /**
     * load documentation in background which doesn't block UI
     */
    async downloadDocumentation() {
      const ephemeralStore = useEphemeralStore()
      const errMsg = 'Failed to download workflow definitions'
      const url = `${constants.coreApiPrefix}/documentation/swagger-user.json`

      if (ephemeralStore.isPendingTrackedFetchRequest({ url })) {
        await waitForCondition({
          conditionFn: () => {
            const isPending = ephemeralStore.isPendingTrackedFetchRequest({ url })
            const hasDocco = !isEmpty(this.documentation)
            console.log(`isPending? ${isPending}, hasDocco? ${hasDocco}`)
            /* if (!isPending) {
              ephemeralStore.untrackFetchRequest({ url })
            } */
            return !isPending || hasDocco
          },
        })
      }
      
      if (!isEmpty(this.documentation)) {
        paratooSentryBreadcrumbMessage(
          `Went to download documentation but already found it there, so returning that instead. Likely this was an extra redundant call that waited for the previous one to resolve.`
        )
        return cloneDeep(this.documentation)
      }

      try {
        paratooSentryBreadcrumbMessage('Downloading new documentation')
        ephemeralStore.trackFetchRequest({
          url,
        })
        // download documentation
        const content = await doCoreApiGet(
            { urlSuffix: url },
            { root: true }
        ).catch((error) => {
            paratooErrorHandler(errMsg, error)
            ephemeralStore.untrackFetchRequest({ url })
            return null
        })
        // checks whether info exists or not
        if (!content?.info) {
          paratooWarnMessage(`${errMsg} reason: content is empty`)
          ephemeralStore.untrackFetchRequest({ url })
          return null
        }
        
        // stores content
        const info = content.info
        this.setDocumentation(content)
        paratooSentryBreadcrumbMessage(`Successfully downloaded documentation, version: ${info.description}`)

        await db.open() // Open the database connection
        const currentTime = Date.now()
        await db.documentation.put({
          url: url,
          version: info.description,
          data: content,
          savedAt: currentTime,
          user: useAuthStore().userProfile.id
        })
        ephemeralStore.untrackFetchRequest({ url })
        return content
      } catch (error) {
        paratooWarnMessage(`${errMsg} reason: ${JSON.stringify(error)}`)
        ephemeralStore.untrackFetchRequest({ url })
        return null
      }
    },

    /**
     * @returns A list of modelsNames that would have data that needs refreshing
     */
    async refreshableModels() {
      await this.getDocumentation()

      let modelNames = []
      // use schemas as model names and remove duplicates (e.g. NewBird-surveys)
      for (const [key] of Object.entries(
        this.documentation.components.schemas,
      )) {
        // only add non 'New<name>' ones first
        if (!key.match(/Request$/)) continue
        const modelName = schemaNameToModelName(key)
        modelNames.push(modelName)
      }
      return modelNames
    },

    async findEndpointFromModel(modelName) {
      if (!this.documentation.info) {
        console.debug('Documentation not found!, refreshing')
        await this.getDocumentation()
        return `/${modelName}s`
      }
      
      const formattedName = modelNameToSchemaName(modelName)
      const schema = this.documentation.components.schemas[formattedName]
      if (!schema) {
        console.debug('schema not found in docco!, refreshing')
        await this.getDocumentation()
        return `/${modelName}s`
      }
      const pluralForm = schema['x-paratoo-endpoint-prefix']
      return `/${pluralForm}`
    },

    /**
     * Formats, and verifies existence of, and returns a newSchema $ref name
     * @param {*} state
     * @returns The valid NewSchema name, or null on failure
     */
    formattedNewSchema({ modelName }) {
      {
        const schemas = Object.keys(this.documentation.components.schemas)
        const formattedName = modelNameToSchemaName(modelName)
        // new swagger does'nt have any lut info to reduce the size of the documentation 
        if (modelName.includes('lut')) return null
        if (schemas.includes(formattedName)) return formattedName
        
        paratooWarnMessage(
          `created name: ${formattedName} from ${modelName}, but not found in documentation`,
        )
        return null
      }
    },

    /**
     * Finds all modelNames of content-types that "modelName" requires to
     * display forms
     *
     * e.g. an observation allows a LUT entry, so the lut entry modelName will be returned
     */
    modelDependencies({
      modelName,
      modelsAcc,
      prevModel = null,
      includeSoftLinks = false,
    }) {
      // Retrieve schema section
      let schemaPath = this.formattedNewSchema({ modelName: modelName })
      if (!schemaPath) return []

      const schema = this.documentation.components.schemas[schemaPath]

      // resolve all $refs
      const derefSchema = dereferenceSection(schema, this.documentation)

      // ultra lazy deep search (lodash flatten/flatmap only work on arrays/aren't actually deep)
      // https://stackoverflow.com/a/52984049
      let regex = /"x-lut-ref":"(.*?)"|"x-model-ref":"(.*?)"/g
      if (includeSoftLinks) {
        regex = /"x-lut-ref":"(.*?)"|"x-model-ref":"(.*?)"|"x-paratoo-soft-relation-target":"(.*?)"/g
      }
      const matchedList = JSON.stringify(derefSchema).match(
        regex,
      )

      if (!matchedList || matchedList.length < 1) return []

      // reduce to just the modelname, and remove duplicates
      let models = union(
        matchedList.map((o) => {
          return o
            .replace('"x-lut-ref":"', '')
            .replace('"x-model-ref":"', '')
            .replace('"x-paratoo-soft-relation-target":"', '')
            .slice(0, -1)
        }),
      )

      //don't want admin content type models (mostly for removing `admin::user`)
      remove(models, o => o.startsWith('admin'))

      // don't want file content type models (mostly for removing `file`)
      remove(models, o => o.startsWith('file'))

      //recursion base case
      //need to use `union` here else the return will be too large and can cause call
      //stack overflow
      if (models.length === 0) return union(modelsAcc)

      //find recursive dependencies
      for (const model of models) {
        if (
          modelsAcc.includes(model)
        ) continue
        // remove self referencing or
        // circular references (e.g., two models both pointing to each other)
        if(model === modelName || prevModel === model) {
          modelsAcc.push(modelName)
        } else {
          const recursiveModelDeps = this.modelDependencies({
            modelName: model,
            modelsAcc,
            prevModel: modelName,
          })
          modelsAcc = union(modelsAcc, recursiveModelDeps)
        }
      }

      models = union(models, modelsAcc)
      return models
    },

    /**
     * Determines if the current protocol contains Strapi components with a particular
     * pattern - the field name using the component matches the field names within the
     * component. e.g., `species` is the field name used in dev sandbox as the field
     * names for the component are `species_lut` and `species_text`
     *
     * @param {Number} protocolId the protocol ID of the protocol to check for components
     * @returns {Array.<Object>} array of objects, each a component's field name &
     * properties and model name of the LUT
     */
    strapiComponentProperties({ protocolId }) {
      const apiModelsStore = useApiModelsStore()
      const protocolWorkflow = find(apiModelsStore.models.protocol, {
        id: protocolId,
      }).workflow
      let components = []
      //search schema of each workflow item to see if there is a Strapi component
      for (const workflowItem of protocolWorkflow) {
        const newModelName = modelNameToSchemaName(workflowItem.modelName)
        const schema =
          this.documentation.components.schemas[newModelName].properties
        for (const schemaProperty in schema) {
          //skip plot layout's plot points, as it is unique handled
          //FIXME it might be useful to have it generically handled, but this will
          //  require refactoring plot layout
          if (schemaProperty === 'plot_points') continue

          let itemToPush = undefined
          if (
            schema[schemaProperty].type === 'object' &&
            Object.keys(schema[schemaProperty]).includes('x-paratoo-component')
          ) {
            const schemaPropertyItemKeys = Object.keys(
              schema[schemaProperty].properties,
            )
            const modelName = (() => {
              let name = undefined
              schemaPropertyItemKeys.forEach((o) => {
                if (
                  Object.keys(schema[schemaProperty].properties[o]).includes(
                    'x-lut-ref',
                  )
                ) {
                  name = schema[schemaProperty].properties[o]['x-lut-ref']
                }
              })
              return name
            })()
            itemToPush = {
              [schemaProperty]: schemaPropertyItemKeys,
            }
            if (modelName !== undefined) {
              Object.assign(itemToPush, { modelName: modelName })
            }
          } else if (
            schema[schemaProperty].type === 'array' &&
            Object.keys(schema[schemaProperty].items).includes(
              'x-paratoo-component',
            )
          ) {
            const schemaPropertyItemKeys = Object.keys(
              schema[schemaProperty].items.properties,
            )
            const modelName = (() => {
              let name = undefined
              schemaPropertyItemKeys.forEach((o) => {
                if (
                  Object.keys(
                    schema[schemaProperty].items.properties[o],
                  ).includes('x-lut-ref')
                ) {
                  name = schema[schemaProperty].items.properties[o]['x-lut-ref']
                }
              })
              return name
            })()
            itemToPush = {
              [schemaProperty]: schemaPropertyItemKeys,
            }
            if (modelName !== undefined) {
              Object.assign(itemToPush, { modelName: modelName })
            }
          }

          if (itemToPush !== undefined) {
            components.push(itemToPush)
          }
        }
      }
      if (components.length > 0) {
        return components
      }
      return undefined
    },
  },

  getters: {
    /**
     * Gets a list of recusively searched x-lut/x-model refs from a requested protocol
     * or component NewModel name
     *
     * @param {Object} options - the context of the get
     * @param {Number} options.protId - The protocolId that should have it's workflow searched
     * @param {Boolean} options.modelFlag
     * @param {Boolean} options.lutFlag
     * @param {Boolean} options.componentFlag
     *
     * @returns {Array.<Object>} key/value pair for field name and model/lut/component
     */
    xRefs:
      (state) =>
      ({
        protId,
        modelFlag = false,
        lutFlag = false,
        componentFlag = false,
      }) => {
        const apiModelsStore = useApiModelsStore()
        const models = apiModelsStore
          .protocolInformation({
            protId: protId,
          })
          .workflow.map((x) => {
            return x.modelName
          })
        // For each model, search through it's fields and find x-model-ref and x-lut-ref
        let returns = []
        for (const model of models) {
          let schema
          try {
            // Generate the request model name
            let newModelName = model[0].toUpperCase()
            for (let i = 1; i < model.length; i++) {
              if (model[i] === '-') {
                newModelName += model[i+1].toUpperCase()
                i++
              }
              else {
                newModelName += model[i]
              }
            }
            newModelName += 'Request'

            schema =
              state.documentation.components.schemas[newModelName].properties
          } catch (error) {
            paratooErrorHandler('Failed to generate model name',error)
            continue
          }
          for (const [fieldName, property] of Object.entries(schema)) {
            if ('x-model-ref' in property && modelFlag) {
              if (
                // check if returns doesn't already contain a duplicate
                !returns.some((e) => e[fieldName] === property['x-model-ref'])
              ) {
                returns.push({
                  [fieldName]: property['x-model-ref'],
                })
              }
            }
            if ('x-lut-ref' in property && lutFlag) {
              if (
                !returns.some((e) => e[fieldName] === property['x-lut-ref'])
              ) {
                returns.push({
                  [fieldName]: property['x-lut-ref'],
                })
              }
            }
            if ('x-paratoo-component' in property && componentFlag) {
              if (
                !returns.some(
                  (e) => e[fieldName] === property['x-paratoo-component'],
                )
              ) {
                returns.push({
                  [fieldName]: property['x-paratoo-component'],
                })
              }
            }
            try {
              if ('x-paratoo-component' in property.items && componentFlag) {
                if (
                  !returns.some(
                    (e) => e[fieldName] === property['x-paratoo-component'],
                  )
                ) {
                  returns.push({
                    [fieldName]: property.items['x-paratoo-component'],
                  })
                }
              }
            } catch {
              continue
            }
          }
        }
        return returns
      },
  },
})

// Taken from is-validated.js
/**
 * Dereferences an object's $ref's based on relative paths in 'source'. Returns the dereferenced object.
 * @param {*} section
 * @param {*} source
 */
export function dereferenceSection(section, source) {
  let newSection = cloneDeep(section)

  if (!(section && typeof section === 'object')) {
    // section is not valid, ignore
    return section
  }

  // Iterating through object
  for (const [key, property] of Object.entries(section)) {
    if (typeof property === 'object') {
      newSection[key] = dereferenceSection(property, source)
    }

    if (key === '$ref') {
      let ref = property
      ref = ref.split('/')
      let accObj = source
      // find the part of the documentation the ref refers to (by iterating through)
      for (const value of ref) {
        if (value === '#') continue
        accObj = accObj[value]
      }

      // Dereference nested references
      accObj = dereferenceSection(accObj, source)

      // we replace the whole object because a ref will always be the only value of an object
      newSection = accObj
    }
  }
  return newSection
}
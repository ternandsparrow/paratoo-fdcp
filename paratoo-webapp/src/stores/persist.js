import Dexie from 'dexie'
import {
  cloneDeep,
  isEmpty,
  isEqual,
  upperCase,
} from 'lodash'
import {
  paratooErrorHandler,
  paratooSentryBreadcrumbMessage,
  tryZipsonParse,
  tryJsonParse,
  paratooWarnMessage,
  notifyWithActions,
  showEstimatedQuota,
  postStoreState,
  downloadFile,
  getCircularReplacer,
  logEstimatedQuota,
} from 'src/misc/helpers'
import * as cc from 'src/misc/constants'
import { parse } from 'zipson'
import { useEphemeralStore } from './ephemeral'

const apiModelsDb = new Dexie('apiModels')
const authDb = new Dexie('auth')

//this is setting up a schema that results in a 'store' (table) for each key of the
//store's `state` (e.g., for API Models `models`, `modelsIdMap`; from `stateKeys`), then
//each table contains rows, where each row is a single model and it's data (or mapper).
//we explicity define these (rather than dynamically grabbing from base store state) as
//we need to ensure proper version management - so when adding new fields to a store,
//they need to be registered here as a new version
const tableSchemas = {
  //use functions as it gives us the ability to manage schemas and versions more easily
  apiModels: () => {
    let columnsStr = '&key, value'
    const storesObj = {
      //each item of the state becomes a store, and each store has key/value columns
      models: columnsStr,
      modelsIdMap: columnsStr,
      protocolModels: columnsStr,
      init: columnsStr,
    }
    if (window?.Cypress) {
      Object.assign(storesObj, {
        testString: columnsStr,
        testNum: columnsStr,
        testObj: columnsStr,
        testArrStr: columnsStr,
        testArrObj: columnsStr,
        testFn: columnsStr,
      })
    }
    return {
      version: 2,
      storesObj,
    }
  },
  auth: () => {
    let columnsStr = '&key, value'
    const storesObj = {
      init: columnsStr,
      identifiers: columnsStr,
      token: columnsStr,
      refreshToken: columnsStr,
      userProfile: columnsStr,
      cookie: columnsStr,
      webauthn: columnsStr,
      projAndProt: columnsStr,
      currentContext: columnsStr,
      projectContext: columnsStr,
      oidcTokenResponse: columnsStr,
      currentLayoutPlots: columnsStr,
      authorisedModels: columnsStr,
      staticResourceNames: columnsStr,
      hostInfo: columnsStr,
      previousAuthUserProfile: columnsStr,
      loginTime: columnsStr,
      lastTokenRefreshTime: columnsStr,
      lastRefreshAllApiModelsTime: columnsStr,
      lastAccessTime: columnsStr,
      permissionStatuses: columnsStr,
    }
    return {
      version: 1,
      storesObj,
    }
  },
}
const tables = {
  apiModels: [],
  auth: [],
}

export const dexiePersistPlugin = {
  stores: Object.keys(tables),
  async dexiePersistPlugin({ store }) {
    if (store.$id in tables) {
      paratooSentryBreadcrumbMessage(
        `Processing store ${store.$id}`,
        'dexiePersist',
      )
      localStorage.setItem(`${store.$id}HasHydrated`, false)
      const baseState = cloneDeep(store.$state)
      const currDb = createStoreDbRef({ store: store.$id })
      const stateKeys = cloneDeep(Object.keys(store.$state))
      //dynamically call the function based on the current store
      const storeDefMeta = tableSchemas?.[store.$id]()
      if (isEmpty(storeDefMeta)) {
        throw paratooErrorHandler(
          `Missing store definition for store ${store.$id}`,
          new Error(`You must create table definition(s) for all stores' databases`),
        )
      }
      const storesObj = storeDefMeta.storesObj
  
      const storeObjKeys = Object.keys(storesObj)
      tables[store.$id].push(...storeObjKeys)
      if (!isEqual(storeObjKeys, stateKeys)) {
        //if this error is thrown, a new key was added to the store's `state` but not
        //registered as a new table in the `storesObj`. Will need to define a new store
        //version with the new key(s)
        throw paratooErrorHandler(
          `Programmer error: Invalid store definitions for ${store.$id}`,
          new Error (`Pinia state has keys=${stateKeys} but the store definitions have keys=${storeObjKeys} - both must be the same`),
        )
      }
  
      paratooSentryBreadcrumbMessage(
        `Initialising ${store.$id} Store with Dexie store config: ${JSON.stringify(storesObj)}`,
        'dexiePersist',
      )
      currDb.version(storeDefMeta.version).stores(storesObj)
  
      try {
        await currDb.open()
      } catch (error) {
        paratooErrorHandler(
          `Unable to open Dexie when creating ${store.$id} Store`,
          error,
        )
      }

      //log before we migrate/hydrate (for each store)
      await logEstimatedQuota()
  
      //handle hydration of pinia and migration from old approach if required
      const wholeStateToInit = {}   //state for current store (not all stores)
      let storeIsMigration = false
      if (
        localStorage.getItem(store.$id)
      ) {
        //this helper logs the migration
        const [ dataToInit, issues ] = tryGettingStoreDataFromLocalStorage(
          store.$id,
        )
  
        if (issues.length > 0) {
          paratooWarnMessage(`Unable to migrate ${store.$id} Store due to: ${issues.join('; ')}`)
        } else {
          const migrationIssues = []
          for (const [key, value] of Object.entries(dataToInit)) {
            if (stateKeys.includes(key)) {
              wholeStateToInit[key] = cloneDeep(value)
            } else {
              migrationIssues.push(`Skipping adding key=${key} to data for migration as the key is not part of the current state ${stateKeys}`)
              
            }
          }

          if (migrationIssues.length > 0) {
            paratooWarnMessage(`Migration has issues: ${migrationIssues.join('; ')}`)
          }

          storeIsMigration = true
        }
      } else {
        paratooSentryBreadcrumbMessage(`Initting ${store.$id} from existing IndexDB state`)

        const hydrationIssues = []
        const hydratePromises = []
        for (const stateKey of stateKeys) {
          wholeStateToInit[stateKey] = baseState[stateKey]
  
          let hydrationError = null
          const hydratePromise = currDb
            .transaction('r', currDb[stateKey], async () => {
              await currDb[stateKey]
                .each((d) => {
                  wholeStateToInit[stateKey] = d.value
                })
                .catch((err) => {
                  hydrationError = err

                  hydrationIssues.push({
                    msg: `Failed to retrieve ${store.$id} data to hydrate for stateKey=${stateKey}`,
                    error: err,
                  })
                })
            })
            .catch((err) => {
              hydrationError = err

              hydrationIssues.push({
                msg: `Transaction failed to retrieve ${store.$id} data to hydrate for stateKey=${stateKey}`,
                error: err,
              })
            })
            .finally(() => {
              if (hydrationError !== null) {
                handleFailure({
                  failureAction: 'hydrate',
                  error: hydrationError,
                  storeName: store.$id,
                  stateKey,
                })
              }
            })
          
          hydratePromises.push(hydratePromise)
        }

        //need to resolve the promises together outside the loop so we can easily log
        //errors in one go
        await Promise.all(hydratePromises)
          .then(() => {
            if (hydrationIssues.length > 0) {
              const hydrationIssueMessages = hydrationIssues.map(h => h.msg)
              const hydrationIssueErrors = hydrationIssues.map(h => h.error)
    
              paratooErrorHandler(
                `Hydration has issues: ${hydrationIssueMessages.join('; ')}`,
                new AggregateError(hydrationIssueErrors),
              )
            }
          })
          .catch((err) => {
            paratooSentryBreadcrumbMessage(`One of the hydration promises was rejected during Promise.all()`)
            handleFailure({
              failureAction: 'hydrate',
              error: err,
              storeName: store.$id,
            })
          })
      }
      // console.log('wholeStateToInit:', JSON.stringify(wholeStateToInit, null, 2))
      let migrationHasError = false
      let patchType = 'hydrate'
      if (storeIsMigration) {
        patchType = 'migrate'
      }
      try {
        paratooSentryBreadcrumbMessage(
          `Patching store ${store.$id} for ${patchType}`,
          'dexiePersist',
        )
        store.$patch(wholeStateToInit)
      } catch (err) {
        paratooErrorHandler(
          `Failed to patch store ${store.$id} (${patchType})`,
          err,
        )
        migrationHasError = true
        handleFailure({
          failureAction: patchType,
          error: err,
          storeName: store.$id,
        })
      }

      if (!migrationHasError && storeIsMigration) {
        //only want to remove old store if we succeeded in migration (and hydrating with
        //the `$patch` above)
        paratooSentryBreadcrumbMessage(
          `Cleaning up old localStorage for store ${store.$id}`,
          'dexiePersist',
        )
        //now remove the whole item from localStorage to free up the space
        localStorage.removeItem(store.$id)
        //various flags for the old store approach (should only apply to API Models,
        //but leave generic for simplicity)
        localStorage.removeItem(`${store.$id}StoreSerializer`)
        localStorage.removeItem(`${store.$id}StoreNeedsMigration`)
        localStorage.removeItem(`${store.$id}StoreMigrationFailed`)
      }
      //log after we migrate/hydrate (for each store)
      await logEstimatedQuota()

      paratooSentryBreadcrumbMessage(
        `${store.$id} Store has been hydrated`,
        'dexiePersist',
      )
      localStorage.setItem(`${store.$id}HasHydrated`, true)
      //we can listen for this event to trigger a DOM mount.
      //based on: https://stackoverflow.com/a/61178486.
      //we keep the variable in localstorage so that reloads will say persisted
      window.dispatchEvent(
        new CustomEvent(`${store.$id}HasHydrated-localstorage-changed`,
        {
          detail: {
            storage: localStorage.getItem(`${store.$id}HasHydrated`)
          }
        },
      ))
  
      //handle persisting pinia in Dexie
      store.$subscribe(
        (mutation, state) => {
          const clonedState = cloneDeep(state)
          //`stateKey` is the top-level key of the store `state`, and the `stateVal` is 
          //whatever is assigned to it
          const persistIssues = []
          const transactionPromises = []
          for (const [stateKey, stateVal] of Object.entries(clonedState || {})) {
            // console.log(`stateKey=${stateKey}. stateVal:`, JSON.stringify(stateVal))

            let persistError = null
            const transactionPromise = currDb
              .transaction('rw', currDb[stateKey], () => {
                currDb[stateKey]
                  .bulkPut([
                    {
                      key: stateKey,
                      value: stateVal,
                    }
                  ])
                  .catch((err) => {
                    persistError = err
                    persistIssues.push({
                      msg: `Failed to persist ${store.$id} data for stateKey=${stateKey}`,
                      error: err,
                    })
                  })
              })
              .catch((err) => {
                persistError = err
                persistIssues.push({
                  msg: `Transaction failed to persist ${store.$id} data for stateKey=${stateKey}`,
                  error: err,
                })
              })
              .finally(() => {
                if (persistError !== null) {
                  handleFailure({
                    failureAction: 'persist',
                    error: persistError,
                    storeName: store.$id,
                    stateKey,
                  })
                }
              })
            transactionPromises.push(transactionPromise)
          }

          //need to resolve the promises together outside the loop so we can easily log
          //errors in one go
          Promise.all(transactionPromises)
            .then(() => {
              if (persistIssues.length > 0) {
                const persistIssueMessages = persistIssues.map(h => h.msg)
                const persistIssueErrors = persistIssues.map(h => h.error)
    
                paratooErrorHandler(
                  `Persist has issues: ${persistIssueMessages.join('; ')}`,
                  new AggregateError(persistIssueErrors),
                )
              }
            })
            .catch((err) => {
              paratooSentryBreadcrumbMessage(`One of the persist promises was rejected during Promise.all()`)
              handleFailure({
                failureAction: 'persist',
                error: err,
                storeName: store.$id,
              })
            })
        },
        // { detached: true }
      )
    }
  },
  async getStoreState({ store }) {
    const currDb = createStoreDbRef({ store })
    const state = {}
    for (const table of tables[store]) {
      const result = await currDb[table]
        .toArray()
        
      const mappedResult = {}
      for (const row of result) {
        mappedResult[row.key] = row.value
      }
      Object.assign(state, {
        [table]: mappedResult,
      })
    }
    
    return state
  },
  /**
   * delete's a table and returns if the table is open or not to indicate success.
   */
  async deleteStore({storeName}) {
    if (window?.Cypress) {
      paratooSentryBreadcrumbMessage(
        `Deleting ${storeName} in Dexie`,
        'dexiePersist',
      )
      const currDb = createStoreDbRef({ store: storeName })
      await currDb.delete()
      // returns true if operation succeeded
      return !currDb.isOpen()
    } else {
      paratooWarnMessage(`Unable to delete ${storeName} as we're not in the Cypress context`)
    }
  },
}

async function handleFailure({ failureAction, error, storeName, stateKey = null }) {
  const ephemeralStore = useEphemeralStore()
  if (!ephemeralStore?.persistFailureNotificationState) {
    ephemeralStore.persistFailureNotificationState = true
    //don't want to trigger the notification and data dump every time
    //we fail to persist, so when we get here, we suppress for 30s
    const timeoutId = setTimeout(() => {
      ephemeralStore.persistFailureNotificationState = false
    }, 30000)
    
    let authAndApiModelsStoreState = {}
    for (const store of dexiePersistPlugin.stores) {
      let [ storeDataInLocalStorage ] = tryGettingStoreDataFromLocalStorage(store)
      if (!storeDataInLocalStorage) {
        const dataFromDexie = await dexiePersistPlugin.getStoreState({ store })
        storeDataInLocalStorage = Object.keys(dataFromDexie).reduce(
          (acc, key) => ({ ...acc, [key]: dataFromDexie[key][key] }), {})
      }
      if(storeDataInLocalStorage) {
        authAndApiModelsStoreState[store] = storeDataInLocalStorage
      }
    }

    const stateId = await postStoreState(
      cc.dumpOrigins?.[`${upperCase(failureAction)}_FAILURE`],
      authAndApiModelsStoreState,
      true,
    )
    const postDumpSuccess = stateId !== undefined 
    createFailureNotification({
      failureAction,
      error,
      timeoutId,
      postDumpSuccess,
    })
  } else {
    paratooSentryBreadcrumbMessage(`Skip notification of failure to persist of ${storeName}${stateKey ? ' for stateKey=' + stateKey : ''}`)
  }
}

async function createFailureNotification({ failureAction, error, timeoutId, postDumpSuccess }) {
  let baseMsg = 'Unknown error'   //set to something in case switch below falls through
  switch (failureAction) {
    case 'persist':
      baseMsg = 'Failed to save data'
      break
    case 'hydrate':
      baseMsg = 'Failed to restore saved data'
      break
    case 'migrate':
      baseMsg = 'Failed to migrate data'
      break
    default:
      paratooWarnMessage(`Programmer error: cannot create failure notification for unregistered action=${failureAction}`)
  }

  let additionalInfo = null
  if (
    error?.message?.includes('QuotaExceededError') ||
    error?.inner?.includes('QuotaExceededError')
  ) {
    additionalInfo = 'due to low storage space'
    const estimatedQuota = await showEstimatedQuota()
    if (estimatedQuota?.quota && estimatedQuota?.usage) {
      additionalInfo += ` (used ${(estimatedQuota.usage / 1024 / 1024).toFixed(1)}MB of ${(estimatedQuota.quota  / 1024 / 1024).toFixed(1)}MB)`
    }
  }
  const actions = [
    {
      label: 'Open help-desk',
      color: 'white',
      handler: () => {
        //TODO test on tablet PWA - must open new tab/window, not override current tab/window (have tested on tablet browser and desktop browser and PWA, as local-built Docker image cannot have PWA installed)
        window.open('https://emsa.tern.org.au/helpdesk', '_blank')
      },
      noDismiss: true
    },
  ]
  if(!postDumpSuccess) {
    notifyWithActions(
      'negative',
      'error',
      0,  //notification must be manually dismissed
      true,
      `<b>${baseMsg}</b>${additionalInfo ? ` ${additionalInfo}` : ''}. Diagnostic data is sent to tech support, <b>please contact the help-desk</b> to correct this issue.`,
      actions,
      () => {
        useEphemeralStore().persistFailureNotificationState = false
        clearTimeout(timeoutId)
      },
    )
  } else {
    actions.unshift({
        label: 'Download diagnostic data',
        color: 'white',
        handler: async () => {
          const authStoreData = await dexiePersistPlugin.getStoreState({ store: 'auth' })
          const apiModelsStoreData = await dexiePersistPlugin.getStoreState({ store: 'apiModels' })
          // create a json file to download
          const data = JSON.stringify({ auth: authStoreData, apiModels: apiModelsStoreData }, getCircularReplacer())
          const blob = new Blob([data], { type: 'text/plain' })
          downloadFile(blob, 'json', 'paratoo_diagnostics.json')
        },
        noDismiss: true
      })
    // fallback to ask user download the states if failed to post dump file
    notifyWithActions(
      'negative',
      'error',
      0,  //notification must be manually dismissed
      true,
      `<b>${baseMsg}</b>${additionalInfo ? ` ${additionalInfo}` : ''}. 
      Please download the diagnostic data and send it to
      <a target="_blank" href="https://emsa.tern.org.au/helpdesk" style="font-weight: bold;">
        help-desk
      </a> 
      to correct this issue.`,
      actions,
      () => {
        useEphemeralStore().persistFailureNotificationState = false
        clearTimeout(timeoutId)
      },
    )
  }
}

function createStoreDbRef({ store }) {
  let currDb = null
  switch (store) {
    case 'apiModels':
      currDb = apiModelsDb
      break
    case 'auth':
      currDb = authDb
      break
    default:
      throw paratooErrorHandler(
        `Programmer error: store ${store} does not have a Dexie table defined`,
        new Error('You must register all stores as separate Dexie databases'),
      )
  }

  return currDb
}

function tryGettingStoreDataFromLocalStorage(storeName) {
  const zipParsedSuccess = tryZipsonParse({ store: storeName })
  const jsonParseSuccess = tryJsonParse({ store: storeName })
  paratooSentryBreadcrumbMessage(
    `zipParsedSuccess=${zipParsedSuccess}, jsonParseSuccess=${jsonParseSuccess}`,
    'dexiePersist',
  )

  let dataToInit = null
  if (zipParsedSuccess && !jsonParseSuccess) {
    paratooWarnMessage(
      `${storeName} Store requires migration to IndexDB from zipson. build=${cc.deployment}. version=${cc.paratooWebAppBuildVersion}. git hash=${cc.buildGitHash}`,
    )
    dataToInit = parse(localStorage.getItem(storeName))
  } else if (!zipParsedSuccess && jsonParseSuccess) {
    paratooWarnMessage(
      `${storeName} Store requires migration to IndexDB from uncompressed (no zipson). build=${cc.deployment}. version=${cc.paratooWebAppBuildVersion}. git hash=${cc.buildGitHash}`,
    )
    dataToInit = JSON.parse(localStorage.getItem(storeName))
  } else {
    paratooWarnMessage(
      `Tried to check if ${storeName} Store requires migration to IndexDB, but didn't match any cases`,
    )
  }

  //even though there's not a lot of cases, this is implemented like this to make
  //extending easier in the future
  const issues = []
  if (typeof dataToInit !== 'object') {
    issues.push('data is not an object')
  }
  return [ dataToInit, issues ]
}
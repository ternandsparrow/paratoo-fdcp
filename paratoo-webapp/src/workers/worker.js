import Dexie from './dexieDB.js'

const WORKER_TYPE = {
  Dexie: 'dexie',
  Documentation: 'documentation',
}
const dexieTables = [
  'vascularFlora',
  'mosses',
  'amphibia',
  'lichens',
  'mammalia',
  'birds',
  'reptiles',
  'GCMDScienceKeywords',
  'ANZSRCFieldsOfResearch',
]

async function initDexie() {
  const dexieDB = new Dexie('dexieDatabase')

  // Define the V1 table schema
  dexieDB.version(1).stores({
    vascularFlora: '++id, canonicalName, scientificName', //non-id values are indexed
    mosses: '++id, canonicalName, scientificName',
    amphibia: '++id, canonicalName, scientificName',
    lichens: '++id, canonicalName, scientificName',
    mammalia: '++id, canonicalName, scientificName',
    birds: '++id, canonicalName, scientificName',
    reptiles: '++id, canonicalName, scientificName',
  })

  dexieDB.version(2).stores({
    vascularFlora: '++id, canonicalName, scientificName', //non-id values are indexed
    mosses: '++id, canonicalName, scientificName',
    amphibia: '++id, canonicalName, scientificName',
    lichens: '++id, canonicalName, scientificName',
    mammalia: '++id, canonicalName, scientificName',
    birds: '++id, canonicalName, scientificName',
    reptiles: '++id, canonicalName, scientificName',
    fieldNames: '++id, canonicalName, scientificName',
  })

  dexieDB.version(3).stores({
    vascularFlora:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_', //non-id values are indexed
    mosses:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
    amphibia:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
    lichens:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
    mammalia:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
    birds:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
    reptiles:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
    fieldNames:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
  })

  dexieDB.version(4).stores({
    vascularFlora:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_', //non-id values are indexed
    mosses:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
    amphibia:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
    lichens:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
    mammalia:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
    birds:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
    reptiles:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
    fieldNames:
      '++id, canonicalName, scientificName, *canonicalName_, *scientificName_',
    GCMDScienceKeywords: '++id, anzsrcid, option, *anzsrcid_, *option_',
    ANZSRCFieldsOfResearch: '++id, option, *option_',
  })

  //Add hooks that will index for full-text search:
  //https://github.com/dexie/Dexie.js/blob/master/samples/full-text-search/FullTextSearch.js
  //TODO can cause over-matching when using `startsWithAnyOfIgnoreCase` (e.g., searching 'australian magpie' matches everything that has words starting with 'australian' and 'magpie') - may want to index the 'words' in a more trie-like structure
  function getAllWords(text) {
    let allWordsIncludingDups = text.split(' ')
    let wordSet = allWordsIncludingDups.reduce(function (prev, current) {
      prev[current] = true
      return prev
    }, {})
    return Object.keys(wordSet)
  }

  for (const table of [
    'vascularFlora',
    'mosses',
    'amphibia',
    'lichens',
    'mammalia',
    'birds',
    'reptiles',
    'fieldNames',
  ]) {
    dexieDB[table].hook('creating', function (primKey, obj, trans) {
      if (typeof obj.canonicalName == 'string')
        obj.canonicalName_ = getAllWords(obj.canonicalName)
      if (typeof obj.scientificName == 'string')
        obj.scientificName_ = getAllWords(obj.scientificName)
    })

    dexieDB[table].hook('updating', function (mods, primKey, obj, trans) {
      if (Object.keys(mods).includes('canonicalName')) {
        if (typeof mods.canonicalName == 'string')
          return { canonicalName_: getAllWords(mods.canonicalName) }
        else return { canonicalName_: [] }
      }

      if (Object.keys(mods).includes('scientificName')) {
        if (typeof mods.scientificName == 'string')
          return { scientificName_: getAllWords(mods.canonicalName) }
        else return { scientificName_: [] }
      }
    })
  }

  for (const table of ['GCMDScienceKeywords', 'ANZSRCFieldsOfResearch']) {
    dexieDB[table].hook('creating', function (primKey, obj, trans) {
      if (typeof obj.anzsrcid == 'string')
        obj.anzsrcid_ = getAllWords(obj.anzsrcid)
      if (typeof obj.option == 'string') obj.option_ = getAllWords(obj.option)
      if (typeof obj.keyword == 'string')
        obj.keyword_ = getAllWords(obj.keyword)
    })
    // eslint-disable-next-line
    dexieDB[table].hook('updating', function (mods, primKey, obj, trans) {
      if (Object.keys(mods).includes('anzsrcid')) {
        if (typeof mods.anzsrcid == 'string')
          return { anzsrcid_: getAllWords(mods.anzsrcid) }
        else return { anzsrcid_: [] }
      }
      if (Object.keys(mods).includes('option')) {
        if (typeof mods.option == 'string')
          return { option_: getAllWords(mods.option) }
        else return { option_: [] }
      }
      if (Object.keys(mods).includes('keyword')) {
        if (typeof mods.keyword == 'string')
          return { keyword_: getAllWords(mods.keyword) }
        else return { keyword_: [] }
      }
    })
  }

  return dexieDB
}

async function populateDexie(dexieDB, csvSources, worker, workerType) {
  // NOTE: the species lists have been reduced to only include relevant data, for full
  //      lists, visit the respective links
  //      FIXME lists obtained from Biodiversity's AFD contain author names embedded in the
  //      species names

  // vascular flora list from: https://biodiversity.org.au/nsl/services/export/index
  // mosses list from: https://moss.biodiversity.org.au/nsl/services/export/index
  // amphibia list from: https://biodiversity.org.au/afd/taxa/AMPHIBIA/names/csv
  // lichens list from: https://lichen.biodiversity.org.au/nsl/services/export/index
  // mammalia list from: https://biodiversity.org.au/afd/taxa/MAMMALIA
  // birds list from: https://birdlife.org.au/documents/BWL-BirdLife_Australia_Working_List_v3.xlsx
  // reptiles list from: https://biodiversity.org.au/afd/taxa/REPTILIA
  const promises = dexieTables.map(async (tableName) => {
    if (!dexieDB[tableName]) return
    if ((await dexieDB[tableName].count()) != 0) {
      worker.postMessage({
        type: workerType,
        tableName: tableName,
        tableExist: true,
        timeTaken: 0,
        tableNames: dexieTables,
      })
      return
    }

    if (tableName == 'vascularFlora') {
      // as vascularFlora has two sections vascularFlora1 and vascularFlora2
      await dexieTransaction(
        dexieDB,
        tableName,
        csvSources[`${tableName}1`],
        worker,
        workerType,
      )
      await dexieTransaction(
        dexieDB,
        tableName,
        csvSources[`${tableName}2`],
        worker,
        workerType,
      )
      return
    }

    await dexieTransaction(
      dexieDB,
      tableName,
      csvSources[tableName],
      worker,
      workerType,
    )
  })
  await Promise.all(promises)
}

/**
 * populate Dexie with field name and mode.
 *
 * @param {String} tableName name of the field
 * @param {Array.<Object>} data array of names
 * @param {Boolean} updateContent if true then update existing data
 *
 */
async function dexieTransaction(dexieDB, tableName, data, worker, workerType) {
  let t0 = new Date().getTime()
  if (!data) return
  // update dexieDB
  await dexieDB
    .transaction('rw', dexieDB[tableName], async () => {
      await dexieDB[tableName]
        .bulkAdd(data)
        .then(() => {
          let t1 = new Date().getTime()
          worker.postMessage({
            type: workerType,
            tableName: tableName,
            timeTaken: Math.round(t1 - t0),
            tableNames: dexieTables,
          })
        })
        .catch((err) => {
          let msg =
            'Failed to initialise the ' +
            tableName +
            ' list(s) from CSV sources'
          worker.postMessage({ type: workerType, errorMsg: msg, error: err })
        })
    })
    .catch((err) => {
      let msg = 'Transaction failed when adding ' + tableName + ' to dexieDB'
      worker.postMessage({ type: workerType, errorMsg: msg, error: err })
    })
}

/**
 * Get/Post requests.
 *
 * @param {String} url endpoint
 * @param {Object} init header object
 */
async function sendRequest(url, init, worker, workerType) {
  fetch(url, init)
    .then((resp) => {
      if (resp.status != 200) {
        worker.postMessage({
          type: workerType,
          error: `response code ${resp.status}`,
        })
        return
      }
      resp
        .json()
        .then((result) => {
          worker.postMessage({
            type: workerType,
            response: result.info ? result : null,
            error: null,
          })
        })
        .catch((reason) => {
          worker.postMessage({
            type: workerType,
            error: reason,
          })
        })
    })
    .catch((reason) => {
      worker.postMessage({
        type: workerType,
        error: reason,
      })
    })
}

self.onmessage = async (event) => {
  const data = event.data
  // dexie operation
  if (data.type == WORKER_TYPE.Dexie) {
    self.postMessage({ msg: 'DexieDB operation started' })
    const csvSources = data.csvSources
    const dexieDB = await initDexie()
    await populateDexie(dexieDB, csvSources, self, data.type)
    return
  }
  // swagger documentation
  if (data.type == WORKER_TYPE.Documentation) {
    self.postMessage({ msg: 'Loading documentation' })
    sendRequest(data.url, data.init, self, data.type)
    return
  }
}
export {}

<template>
  <!-- only allow closing visit from current context -->
  <div v-if="visitData && config">
    <q-btn 
      :label="'Close Current Visit'"
      data-cy="closeVisitBtn"
      outline
      style="height: 100%"
      :disable="!ephemeralStore.networkOnline || isSessionDestroyed_() || !ephemeralStore.coreIsAvailable"
      @click="onCloseVisit()"
    >
      <slot/>
    </q-btn>
    <!-- even if `visitData` is defined, it might just return 'offline' so the
      `onCloseVisit` method will not open this dialog -->
    <q-dialog v-model="closeVisitPopup" persistent>
      <q-card>
        <q-card-section>
          <h6 class="q-ma-none">
            Close visit '{{ visitData?.visit_field_name }} ({{ visitData?.start_date_local_string }})'
          </h6>
        </q-card-section>
        <q-card-section>
          <DateTimeInput
            data-cy="visitEndDate"
            v-model="visitEndDate"
            filled
            :label="config.label"
            :model="config.model"
            :rules="config.rules"
            :disabled="config.disable"
            :hint="config.hint"
            lazy-rules
          />
        </q-card-section>
        <q-card-section v-if="visitEndDate">
          <p class="text-weight-bold text-subtitle1">You're about to close visit '{{ visitData?.visit_field_name }} ({{ visitData?.start_date_local_string }})'. Please ensure you have submit ('sync queued collections with cloud') all protocol collections, if applicable. Once this visit is closed, it will no longer be possible to submit additional collections for this visit.</p>
          <q-checkbox
            v-model="closeVisitConfirm"
            label="I understand"
            data-cy="closeVisitConfirm"
          />
        </q-card-section>
        <q-card-actions align="right">
          <q-btn flat label="Cancel" wrap class="col" v-close-popup />
          <q-btn
            flat
            label="Done"
            color="secondary"
            wrap
            class="col"
            data-cy="doneCloseVisitBtn"
            :disabled="!visitEndDate || !closeVisitConfirm"
            @click="closeVisit()"
          />
        </q-card-actions>
      </q-card>
    </q-dialog>
  </div>
</template>

<script>
import { useDocumentationStore } from '../stores/documentation'
import { useApiModelsStore } from '../stores/apiModels'
import { useDataManagerStore } from '../stores/dataManager'
import { useBulkStore } from '../stores/bulk'
import { useEphemeralStore } from '../stores/ephemeral'
import DateTimeInput from './DateTimeInput.vue'
import {
  makeModelConfig,
  paratooErrorHandler,
  notifyHandler,
  paratooWarnMessage,
  isSessionDestroyed
} from 'src/misc/helpers'
import {
  doCoreApiPut,
} from 'src/misc/api'
import { useAuthStore } from 'src/stores/auth'
import {
  layoutAndVisitUuid,
  serverUnavailableMsg
} from 'src/misc/constants'

export default {
  name: 'CloseVisit',
  components: {
    DateTimeInput,
  },
  async setup() {
    const documentationStore = useDocumentationStore()
    const apiModelsStore = useApiModelsStore()
    const dataManager = useDataManagerStore()
    const bulkStore = useBulkStore()
    const ephemeralStore = useEphemeralStore()
    const authStore = useAuthStore()

    const visitProtocolInfo = apiModelsStore.cachedModel({
      modelName: 'protocol',
    }).find(o => o.identifier === layoutAndVisitUuid)

    let modelConfig = null
    if (visitProtocolInfo) {
      try {
        modelConfig = makeModelConfig(
          'plot-visit',
          await documentationStore.getDocumentation(),
          visitProtocolInfo.id,
        )
      } catch (err) {
        paratooErrorHandler(`Failed to make model config for visit closing component`, err)
      }

      if (modelConfig) {
        modelConfig.fields.end_date.required = true
        modelConfig.fields.end_date.label += ' *'
        const requiredRule = (value) =>
          (value !== undefined && value !== null && value !== '') ||
          `This field is required`

        modelConfig.fields.end_date.rules.push(requiredRule)

        modelConfig.fields.end_date.hint = 'Record the end date representative of when the visit finished'
      }
    }
    
    return {
      config: modelConfig?.fields?.end_date || null,
      dataManager,
      bulkStore,
      ephemeralStore,
      apiModelsStore,
      authStore,
      serverUnavailableMsg
    }
  },
  data() {
    return {
      closeVisitPopup: false,
      visitEndDate: null,
      closeVisitConfirm: false,
    }
  },
  methods: {
    async closeVisit() {
      const startDate = new Date(this.visitData?.start_date)
      const proposedEndDate = new Date(this.visitEndDate)

      if (proposedEndDate.getTime() <= startDate.getTime()) {
        let msg = `Cannot close visit '${this.visitData?.visit_field_name} (${this.visitData?.start_date_local_string})' with an end date that is before the start date.`
        notifyHandler(
          'negative',
          msg,
        )
      } else {
        const resp = await doCoreApiPut({
          urlSuffix: `/api/plot-visits/${this.visitData.id}`,
          body: {
            data: {
              end_date: this.visitEndDate,
            },
          },
        })
          .catch((err) => {
            let msg = 'Failed to close visit'
            paratooErrorHandler(msg, err)
            notifyHandler('negative', msg)
          })
        if (resp) {
          this.closeVisitPopup = false
          this.visitEndDate = null
          this.closeVisitConfirm = false

          //need to refresh so that the end_date is populated
          await this.apiModelsStore.populateVerifiedModels({
            modelNames: ['plot-visit'],
          })

          //need to make a copy, as we clear context above and thus lose the data
          const closedVisit = {
            visit_field_name: this.visitData?.visit_field_name,
            start_date_local_string: this.visitData?.start_date_local_string,
          }

          this.bulkStore.clearStaticPlotContext()
          this.authStore.clearAuthContext()

          const msg = `Successfully closed visit '${closedVisit.visit_field_name } (${closedVisit?.start_date_local_string })'`
          notifyHandler('positive', msg)
        }
      }
    },
    onCloseVisit() {
      const inProgressProtocolsReferVisitContext = this.bulkStore.inProgressProtocolsReferVisitContext
      const contextVisitData = this.bulkStore?.getStaticPlotContext?.plotVisit
      if (this?.visitData === 'offline') {
        //if it's null then there is no context, but in other cases there was an
        //unexpected issue getting the context
        if (!contextVisitData && contextVisitData !== null) {
          paratooWarnMessage(`Failed to access visit from static plot context. Accessed data: ${contextVisitData}`)
        }
        let msg = `The visit you are trying to close '${contextVisitData?.visit_field_name} (${new Date(contextVisitData?.start_date).toString()})' is still in the queue and pending sync with the cloud. Please ensure you have synced this visit with the cloud before closing it.`
        notifyHandler(
          'negative',
          msg,
          null,
          30,
        )
      } else if (this?.visitData?.end_date) {
        let msg = `Cannot close visit '${ this.visitData?.visit_field_name } (${ this.visitData?.start_date_local_string })' that is already closed.`
        notifyHandler(
          'negative',
          msg,
          null,
          30,
        )
      } else if (inProgressProtocolsReferVisitContext.length > 0) {
        const mappedProtocolNames = []
        for (const protocolId of inProgressProtocolsReferVisitContext) {
          const info = this.apiModelsStore.protocolInformation({
            protId: protocolId,
          })
          mappedProtocolNames.push(info.name)
        }
        let msg = `Unable to close visit '${contextVisitData?.visit_field_name} (${new Date(contextVisitData?.start_date).toString()})' as there are in-progress protocols that depend on it: ${mappedProtocolNames.join(', ')}. You must finish or discard these surveys before closing the visit`
        notifyHandler(
          'negative',
          msg,
          null,
          30,
        )
      } else {
        this.closeVisitPopup = true
      }
    },
    isSessionDestroyed_() {
      return isSessionDestroyed()
    },
  },
  computed: {
    visitData() {
      const visitContext = this.bulkStore?.getStaticPlotContext?.plotVisit
      //if it's null then there is no context, but in other cases there was an
      //unexpected issue getting the context
      if (!visitContext && visitContext !== null) {
        paratooWarnMessage(`Failed to access visit from static plot context. Accessed data: ${visitContext}`)
      }
      if (visitContext) {
        const plotData = this.dataManager.getPlotDataFromContext({ modelName: 'plot-visit' })

        if (
          !Number.isInteger(plotData?.id) &&
          (
            visitContext?.temp_offline_id ||
            visitContext?.id?.temp_offline_id
          )
        ) {
          return 'offline'
        }

        if (plotData?.start_date) {
          //want a human-readable (i.e., formatting and local timezone).
          //since Strapi stores and returns UTC dates (but converts to timezone in admin
          //panel), and the datetime input for the end date is local time, we need to
          //convert to local time to prevent confusion.
          //but we don't want to squash the existing `start_date` (datetime) so that we
          //can still make comparisons
          plotData.start_date_local_string = new Date(plotData.start_date).toString()
        }

        return plotData
      }
      return null
    },
  },
}
</script>

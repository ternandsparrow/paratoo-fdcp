//register the help button field components

const requireComponent = require.context('../helpInfo', false, /\.vue$/)

const components = {}

requireComponent.keys().forEach((fileName) => {
  const componentConfig = requireComponent(fileName)
  const componentName = fileName.replace(/^\.\//, '').replace(/\.\w+$/, '')

  components[componentName] = componentConfig.default || componentConfig
})

export default components

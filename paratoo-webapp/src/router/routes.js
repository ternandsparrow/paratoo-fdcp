import * as cc from 'src/misc/constants'
import { notifyHandler, paratooWarnMessage } from 'src/misc/helpers'
import { useApiModelsStore } from 'src/stores/apiModels'
import { useDataManagerStore } from 'src/stores/dataManager'

const routes = [
  {
    path: '/',
    // FIXME does this lazy-loading approach work with PWA mode?
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', redirect: '/projects' },
      { path: '/home', component: () => import('pages/Index.vue') },
      { path: '/login', component: () => import('pages/Login.vue') },
      { path: '/register', component: () => import('pages/Register.vue') },
      {
        path: '/profile',
        component: () => import('pages/Profile.vue'),
        meta: {
          [cc.requiresLoggedIn]: true,
        },
      },
      {
        path: '/projects',
        component: () => import('pages/Projects.vue'),
        meta: {
          [cc.requiresLoggedIn]: true,
        },
      },
      {
        path: '/zzdebug',
        component: () => import('pages/Debug.vue'),
        meta: {
          [cc.requiresLoggedIn]: true,
        },
      },
      {
        path: '/workflow/:protocolId',
        component: () => import('pages/Workflow.vue'),
        meta: {
          [cc.requiresLoggedIn]: true,
        },
        name: 'workflow',
        // reject the navigation that not coming from /project
        beforeEnter(to, from, next) {
          if(process.env.DEV && !window.Cypress) {
            paratooWarnMessage('Skipping checks when entering workflow as we\'re in dev mode')
            return next()
          }

          //don't allow navigating directly to a protocol (e.g., via URL) - only navigate via projects page
          const protocolId = parseInt(to.params?.protocolId)
          const protocolInfo = useApiModelsStore().protUuidFromId(protocolId)
          const protocolName = protocolInfo?.name || ''
          const opportuneProtUuid = '068d17e8-e042-ae42-1e42-cff4006e64b0'
          // as we have shortcut for opportune so only allow going to opportune without going through /project
          const isOpportune =
            protocolInfo?.name === 'Opportune' ||
            protocolInfo?.uuid === opportuneProtUuid
          const projectPath = '/projects'
          if (from.path !== projectPath && !isOpportune) {
            notifyHandler('warning', `You must select the protocol "${protocolName}" from the project screen`)
            return next(projectPath)
          }

          //don' allow going to workflow while the queue is syncing
          const queueSubmissionProgress = useDataManagerStore()?.queueSubmissionProgress
          if (queueSubmissionProgress === undefined) {
            paratooWarnMessage('Could not get the queue submission progress')
          } else if (queueSubmissionProgress !== null) {
            notifyHandler('negative', `Cannot collect data for ${protocolName} while syncing queue`)
            return next(from)
          }
          
          return next()
        },
      },
      {
        path: 'grassy-weeds-export',
        component: () => import('pages/GrassyWeedsExport.vue'),
        meta: {
          [cc.requiresLoggedIn]: true,
        },
        name: 'grassyWeedsExport',
      },
      {
        path: '/preferences',
        component: () => import('pages/Preferences.vue'),
        meta: {
          [cc.requiresLoggedIn]: true,
        },
      },
    ],
  },
  {
    path: '/404',
    component: () => import('pages/Error404.vue'),
  },
  {
    path: '/403',
    component: () => import('pages/Error403.vue'),
  },
  {
    path: '/_health',
    component: () => import('pages/HealthCheck.vue'),
  },
  {
    path: '/loading',
    component: () => import('pages/Loading.vue'),
  },
  {
    // Always leave this as last one
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
]

export default routes

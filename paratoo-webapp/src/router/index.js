import { route } from 'quasar/wrappers'
import {
  createRouter,
  createMemoryHistory,
  createWebHistory,
  createWebHashHistory,
} from 'vue-router'
import routes from './routes'
import * as cc from 'src/misc/constants'
import { useAuthStore } from '../stores/auth'
import { useEphemeralStore } from '../stores/ephemeral'
import { getAccessTokenFromUrlCode } from 'src/misc/oidcALA'
import {
  paratooWarnMessage,
  paratooSentryBreadcrumbMessage,
  paratooWarnHandler,
  notifyHandler,
  checkStaticAssets,
} from 'src/misc/helpers'
import {
  dexiePersistPlugin,
} from '../stores/persist'

export default route(function () {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === 'history'
    ? createWebHistory
    : createWebHashHistory

  const router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,
    history: createHistory(
      process.env.MODE === 'ssr' ? void 0 : process.env.VUE_ROUTER_BASE,
    ),
  })

  router.beforeEach(async (to, from) => {
    paratooSentryBreadcrumbMessage(
      `Going to ${to.fullPath} from ${from.fullPath}`,
      'routerBeforeEach',
    )
    const goingToLoadingPage = to?.fullPath?.includes('loading')
    const goingToOidcLoginRedirect = cc.enableOIDC && to?.fullPath?.includes('code=')
    let allStoresHydrated = true
    for (const store of dexiePersistPlugin.stores) {
      if (localStorage.getItem(`${store}HasHydrated`) != 'true') {
        allStoresHydrated = false
      }
    }
    if (!goingToLoadingPage && allStoresHydrated) {
      // References to the Pinia Stores
      const authStore = useAuthStore()
      const ephemeralStore = useEphemeralStore()

      checkStaticAssets()

      // // if oidc enabled
      if (cc.enableOIDC) {
        await getAccessTokenFromUrlCode()
      }

      // Perform auth logic
      const isRequiresAuth = to.matched.some(
        (r) => r.meta.requiresLoggedIn || r.meta.requiresAdmin,
      )
      if (!isRequiresAuth) {
        return true
      }
      ephemeralStore.isLogin(true)

      const isLoggedIn = !!authStore.username
      if (!isLoggedIn) {
        paratooSentryBreadcrumbMessage(
          'User is not logged-in - redirecting',
          'router',
        )
        router.push('/login')
      }
      const isAdmin = authStore.roleType === cc.adminRoleType
      const isAdminRequired = to.matched.every((r) => r.meta[cc.requiresAdmin])
      if (isAdminRequired && !isAdmin) {
        // if they don't have access, it doesn't exist for them
        paratooSentryBreadcrumbMessage(
          `User *is* logged in, but does not have the required admin role`,
          'router',
        )
        router.push('/404')
      }
      //block access to workflows that the User is not assigned to
      if (to.name === 'workflow') {
        const userProtocols = authStore.protocols
        if (!userProtocols.includes(parseInt(to.params.protocolId))) {
          paratooWarnMessage(
            `User does not have access to the requested protocol`,
          )
          router.push('/403')
        }
      }
      
    } else if (
      !goingToLoadingPage && !allStoresHydrated
    ) {
      const routerObj = {
        path: '/loading',
        query: {
          prevRoute: to.path,
        },
      }
      const currentUrl = new URL(window.location.href)
      const code = currentUrl.searchParams.get('code')
      if (goingToOidcLoginRedirect) {
        Object.assign(routerObj.query, {
          code: code,
        })
      }
      router.push(routerObj)
    }

    //if we're allowing redirect, there's no hydration or init to be done, so everything
    //should be ready to check the assets
    // checkStaticAssets()

    return true
  })
  router.onError((error, to, from) => {
	  const ephemeralStore = useEphemeralStore()
	  
  	let errMsg = `Failed to load page at URL to: ${to.fullPath} from: ${from.fullPath}`
  	let shouldReload = false
  	
  	//NOTE: add cases as-needed
  	//https://blog.francium.tech/vue-lazy-routes-loading-chunk-failed-9ee407bbd58
    if (/loading chunk .* failed/i.test(error.message)) {
    	errMsg += '. Received chunk error when trying to load page'
      shouldReload = ephemeralStore.networkOnline
    } else {
    	errMsg += '. Received error when trying to load page'
    }
    paratooWarnHandler(errMsg, error)
    notifyHandler('warning', errMsg, error)
    
    //do this last so we can capture error and notify user
    if (shouldReload) {
    	window.location.reload()
    }
  })
  return router
})

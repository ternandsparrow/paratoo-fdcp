export const initState = {
  "models": {
    "lut-aerial-experience": [
      {
        "id": 1,
        "symbol": "<5",
        "label": "<5 hours",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "5-10",
        "label": "5 hours-10 hours",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "10-20",
        "label": "10 hours-20 hours",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "20-50",
        "label": "20 hours-50 hours",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "50-200",
        "label": "50 hours-200 hours",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "200-500",
        "label": "200 hours-500 hours",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "500-1,000",
        "label": "500 hours-1,000 hours",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": ">1,000",
        "label": ">1,000 hours",
        "description": "",
        "uri": ""
      }
    ],
    "lut-aerial-setup-or-survey": [
      {
        "id": 1,
        "symbol": "Setup",
        "label": "Create Survey Setup",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Survey",
        "label": "Conduct Field Survey",
        "description": "",
        "uri": ""
      }
    ],
    "lut-aerial-survey-type": [
      {
        "id": 1,
        "symbol": "ST",
        "label": "Strip transect",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "LT",
        "label": "Line transect (distance sampling)",
        "description": "",
        "uri": ""
      }
    ],
    "lut-aircraft-model": [
      {
        "id": 1,
        "symbol": "C182",
        "label": "Cessna 182",
        "description": "Plane",
        "uri": "",
        "aircraft_type": {
          "id": 1,
          "symbol": "Plane",
          "label": "Plane",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 2,
        "symbol": "C206",
        "label": "Cessna 206",
        "description": "Plane",
        "uri": "",
        "aircraft_type": {
          "id": 1,
          "symbol": "Plane",
          "label": "Plane",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 3,
        "symbol": "C210",
        "label": "Cessna 210",
        "description": "Plane",
        "uri": "",
        "aircraft_type": {
          "id": 1,
          "symbol": "Plane",
          "label": "Plane",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 4,
        "symbol": "BJR",
        "label": "Bell Jet Ranger",
        "description": "Helicopter",
        "uri": "",
        "aircraft_type": {
          "id": 2,
          "symbol": "Helicopter",
          "label": "Helicopter",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 5,
        "symbol": "H500",
        "label": "Hughes 500",
        "description": "Helicopter",
        "uri": "",
        "aircraft_type": {
          "id": 2,
          "symbol": "Helicopter",
          "label": "Helicopter",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 6,
        "symbol": "R22",
        "label": "Robinson R22",
        "description": "Helicopter",
        "uri": "",
        "aircraft_type": {
          "id": 2,
          "symbol": "Helicopter",
          "label": "Helicopter",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 7,
        "symbol": "R44",
        "label": "Robinson R44",
        "description": "Helicopter",
        "uri": "",
        "aircraft_type": {
          "id": 2,
          "symbol": "Helicopter",
          "label": "Helicopter",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 8,
        "symbol": "Other",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-aircraft-type": [
      {
        "id": 1,
        "symbol": "Plane",
        "label": "Plane",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Helicopter",
        "label": "Helicopter",
        "description": "",
        "uri": ""
      }
    ],
    "lut-animal-fate": [
      {
        "id": 1,
        "symbol": "RC",
        "label": "Released at point of capture",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "RE",
        "label": "Released elsewhere (comments must address rationale)",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "H1",
        "label": "Held for observation or further processing",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "H2",
        "label": "Held for museum vouchering",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "TD",
        "label": "Trap death",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "HD",
        "label": "Handling death",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "VD",
        "label": "Death by vouchering",
        "description": "",
        "uri": ""
      }
    ],
    "lut-bait-station-mount": [
      {
        "id": 1,
        "symbol": "SD",
        "label": "Star dropper",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "SSS",
        "label": "Survey stake - steel",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "SSW",
        "label": "Survey stake - wood",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "PS",
        "label": "Tent peg - steel",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "PW",
        "label": "Tent peg - wood",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-basal-area-factor": [
      {
        "id": 1,
        "symbol": "A",
        "label": "0.1",
        "description": "Refers to the categorical value for the 'Basal Area Factors'. The value '0.1' represents a basal area of 0.1 m2/ha on the TERN Basal Wedge (ref. Table 2. in 'Basal Area Protocol').",
        "uri": "https://linked.data.gov.au/def/nrm/b4e06a59-4227-5e3d-93c7-d8b95502cde7"
      },
      {
        "id": 2,
        "symbol": "B",
        "label": "0.25",
        "description": "Refers to the categorical value for the 'Basal Area Factors'. The value '0.25' represents a basal area of 0.25 m2/ha on the TERN Basal Wedge (ref. Table 2. in 'Basal Area Protocol').",
        "uri": "https://linked.data.gov.au/def/nrm/4fc9fd3e-7de6-5f6d-97d1-fe2f9429dd71"
      },
      {
        "id": 3,
        "symbol": "C",
        "label": "0.5",
        "description": "Refers to the categorical value for the 'Basal Area Factors'. The value '0.5' represents a basal area of 0.5 m2/ha on the TERN Basal Wedge (ref. Table 2. in 'Basal Area Protocol').",
        "uri": "https://linked.data.gov.au/def/nrm/aa1ebddc-619a-5053-8dd9-bde315e50aac"
      },
      {
        "id": 4,
        "symbol": "D",
        "label": "0.75",
        "description": "Refers to the categorical value for the 'Basal Area Factors'. The value '0.75' represents a basal area of 0.75 m2/ha on the TERN Basal Wedge (ref. Table 2. in 'Basal Area Protocol').",
        "uri": "https://linked.data.gov.au/def/nrm/35a02ea0-2884-5e88-892b-78271053e16f"
      },
      {
        "id": 5,
        "symbol": "E",
        "label": "1",
        "description": "Refers to the categorical value for the 'Basal Area Factors'. The value '0.1' represents a basal area of 0.1 m2/ha on the TERN Basal Wedge (ref. Table 2. in 'Basal Area Protocol').",
        "uri": "https://linked.data.gov.au/def/nrm/b4e06a59-4227-5e3d-93c7-d8b95502cde7"
      },
      {
        "id": 6,
        "symbol": "F",
        "label": "2",
        "description": "Refers to the categorical value for the 'Basal Area Factors'. The value '0.25' represents a basal area of 0.25 m2/ha on the TERN Basal Wedge (ref. Table 2. in 'Basal Area Protocol').",
        "uri": "https://linked.data.gov.au/def/nrm/4fc9fd3e-7de6-5f6d-97d1-fe2f9429dd71"
      }
    ],
    "lut-basal-dbh-instrument": [
      {
        "id": 1,
        "symbol": "DIA",
        "label": "Diameter Tape Measure",
        "description": "The type of instrument used to measure the diameter of a stem/tree trunk, usually at 1.3 m height above ground as diameter at breast height (DBH).",
        "uri": "https://linked.data.gov.au/def/nrm/237b799a-a1f3-5a1e-b88a-69429d05523a"
      },
      {
        "id": 2,
        "symbol": "TAP",
        "label": "Tape Measure",
        "description": "The type of instrument used to measure the diameter of a stem/tree trunk, usually at 1.3 m height above ground as diameter at breast height (DBH).",
        "uri": "https://linked.data.gov.au/def/nrm/9ab8c8fe-0484-5280-a979-c13c32a1c675"
      },
      {
        "id": 3,
        "symbol": "CAL",
        "label": "Tree Caliper",
        "description": "The type of instrument used to measure the diameter of a stem/tree trunk with angular stems, usually at 1.3 m height above ground as diameter at breast height (DBH).",
        "uri": "https://linked.data.gov.au/def/nrm/6a7442bd-230f-5be2-aeb4-8afc7ba02725"
      }
    ],
    "lut-basal-sweep-sampling-point": [
      {
        "id": 1,
        "symbol": "NW",
        "label": "Northwest",
        "description": "The spatial point location, denoting the 'northwest' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/e611ec7c-ae72-560e-bf6c-d47b46485d18"
      },
      {
        "id": 2,
        "symbol": "N",
        "label": "North",
        "description": "The spatial point location, denoting the 'north' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, coarse woody debris transect, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/72e36449-10fe-5e01-a360-a57ce1d8312d"
      },
      {
        "id": 3,
        "symbol": "NE",
        "label": "Northeast",
        "description": "The spatial point location, denoting the 'northeast' end or orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/f8205401-c97c-59ed-a8a9-1ca3257e0cf8"
      },
      {
        "id": 4,
        "symbol": "E",
        "label": "East",
        "description": "The spatial point location, denoting the 'east' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, coarse woody debris transect, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/0cad1fc8-0741-5dc8-90f5-7a4c6c7fba5f"
      },
      {
        "id": 5,
        "symbol": "SE",
        "label": "Southeast",
        "description": "The spatial point location, denoting the 'southeast' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, coarse woody debris transect, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/3ef55e81-e3bd-5626-93da-fc84f5129d5f"
      },
      {
        "id": 6,
        "symbol": "S",
        "label": "South",
        "description": "The spatial point location, denoting the 'south' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, coarse woody debris transect, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/20b65a7c-3128-55ee-b761-4428dc76ca5d"
      },
      {
        "id": 7,
        "symbol": "SW",
        "label": "Southwest",
        "description": "The spatial point location, denoting the 'southwest' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, coarse woody debris transect, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/0488dab6-dea8-530e-9928-65b84cc892dd"
      },
      {
        "id": 8,
        "symbol": "W",
        "label": "West",
        "description": "The spatial point location, denoting the 'northwest' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/e611ec7c-ae72-560e-bf6c-d47b46485d18"
      },
      {
        "id": 9,
        "symbol": "C",
        "label": "Centre",
        "description": "The spatial point location, denoting the 'centre' of the study plot, e.g. in a basal sweep sampling point, camera trap point, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/df58bcc5-34d9-5020-a519-bc83b18824db"
      }
    ],
    "lut-battery-type": [
      {
        "id": 1,
        "symbol": "NiMH rechargeable",
        "label": "NiMH rechargeable",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Lithium",
        "label": "Lithium",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Alkaline",
        "label": "Alkaline",
        "description": "",
        "uri": ""
      }
    ],
    "lut-bioregion": [
      {
        "id": 1,
        "symbol": "ARC",
        "label": "Arnhem Coast",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          }
        ]
      },
      {
        "id": 2,
        "symbol": "ARP",
        "label": "Arnhem Plateau",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          }
        ]
      },
      {
        "id": 3,
        "symbol": "AUA",
        "label": "Australian Alps",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 1,
            "symbol": "CT",
            "label": "Australian Capital Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/4253c9af-b390-5b69-aaf3-ce5cda0c36bf"
          },
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ]
      },
      {
        "id": 4,
        "symbol": "AVW",
        "label": "Avon Wheatbelt",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 5,
        "symbol": "BBN",
        "label": "Brigalow Belt North",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 6,
        "symbol": "BBS",
        "label": "Brigalow Belt South",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 7,
        "symbol": "BEL",
        "label": "Ben Lomond",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          }
        ]
      },
      {
        "id": 8,
        "symbol": "BHC",
        "label": "Broken Hill Complex",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ]
      },
      {
        "id": 9,
        "symbol": "BRT",
        "label": "Burt Plain",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          }
        ]
      },
      {
        "id": 10,
        "symbol": "CAR",
        "label": "Carnavon",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 11,
        "symbol": "CEA",
        "label": "Central Arnhem",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          }
        ]
      },
      {
        "id": 12,
        "symbol": "CEK",
        "label": "Central Kimberley",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 13,
        "symbol": "CER",
        "label": "Central Ranges",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 14,
        "symbol": "CMC",
        "label": "Central Mackay Coast",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 15,
        "symbol": "CHC",
        "label": "Channel Country",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ]
      },
      {
        "id": 16,
        "symbol": "COO",
        "label": "Coolgardie",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 17,
        "symbol": "COP",
        "label": "Cobar Peneplain",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ]
      },
      {
        "id": 18,
        "symbol": "CYP",
        "label": "Cape York Peninsula",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 19,
        "symbol": "DAB",
        "label": "Daly Basin",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          }
        ]
      },
      {
        "id": 20,
        "symbol": "DAC",
        "label": "Darwin Coastal",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          }
        ]
      },
      {
        "id": 21,
        "symbol": "DEU",
        "label": "Desert Uplands",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 22,
        "symbol": "DAL",
        "label": "Dampierland",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 23,
        "symbol": "DMR",
        "label": "Davenport Murchison Ranges",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          }
        ]
      },
      {
        "id": 24,
        "symbol": "DRP",
        "label": "Darling Riverine Plains",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 25,
        "symbol": "EIU",
        "label": "Einsaleigh Uplands",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 26,
        "symbol": "FIN",
        "label": "Finke",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ]
      },
      {
        "id": 27,
        "symbol": "FLB",
        "label": "Flinders Lofty Block",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ]
      },
      {
        "id": 28,
        "symbol": "GAS",
        "label": "Gascoyne",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 29,
        "symbol": "GAW",
        "label": "Gawler",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ]
      },
      {
        "id": 30,
        "symbol": "GID",
        "label": "Gibson Desert",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 31,
        "symbol": "GFU",
        "label": "Gulf Fall and Uplands",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 32,
        "symbol": "GSD",
        "label": "Great Sandy Desert",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 33,
        "symbol": "GUC",
        "label": "Gulf Coastal",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          }
        ]
      },
      {
        "id": 34,
        "symbol": "GUP",
        "label": "Gulf Plains",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 35,
        "symbol": "GVD",
        "label": "Great Victoria Desert",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 36,
        "symbol": "HAM",
        "label": "Hampton",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 37,
        "symbol": "LSD",
        "label": "Little Sandy Desert",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 38,
        "symbol": "MAC",
        "label": "MacDonnell Ranges",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          }
        ]
      },
      {
        "id": 39,
        "symbol": "MDD",
        "label": "Murray Darling Depression",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ]
      },
      {
        "id": 40,
        "symbol": "MGD",
        "label": "Mitchell Grass Downs",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 41,
        "symbol": "MII",
        "label": "Mount Isa Inlier",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 42,
        "symbol": "MUL",
        "label": "Mulga Lands",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 43,
        "symbol": "MUR",
        "label": "Murchison",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 44,
        "symbol": "NOK",
        "label": "Northern Kimberley",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 45,
        "symbol": "NUL",
        "label": "Nullarbor",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 46,
        "symbol": "OVP",
        "label": "Ord Victoria Plain",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 47,
        "symbol": "PCK",
        "label": "Pine Creek",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          }
        ]
      },
      {
        "id": 48,
        "symbol": "PIL",
        "label": "Pilbara",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 49,
        "symbol": "RIV",
        "label": "Riverina",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ]
      },
      {
        "id": 50,
        "symbol": "SSD",
        "label": "Simpson Strzlecki Dunefields",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ]
      },
      {
        "id": 51,
        "symbol": "STP",
        "label": "Stony Plains",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ]
      },
      {
        "id": 52,
        "symbol": "STU",
        "label": "Sturt Plateau",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          }
        ]
      },
      {
        "id": 53,
        "symbol": "SWA",
        "label": "Swan Coastal Plain",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 54,
        "symbol": "SYB",
        "label": "Sydney Basin",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ]
      },
      {
        "id": 55,
        "symbol": "TAN",
        "label": "Tanami",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 56,
        "symbol": "TIW",
        "label": "Tiwi Coburg",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          }
        ]
      },
      {
        "id": 57,
        "symbol": "TCH",
        "label": "Tasmanian Central Highlands",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          }
        ]
      },
      {
        "id": 58,
        "symbol": "TNM",
        "label": "Tasmanian Northern Midlands",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          }
        ]
      },
      {
        "id": 59,
        "symbol": "TNS",
        "label": "Tasmanian Northern Slopes",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          }
        ]
      },
      {
        "id": 60,
        "symbol": "TSE",
        "label": "Tasmanian South East",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          }
        ]
      },
      {
        "id": 61,
        "symbol": "TSR",
        "label": "Tasmanian Southern Ranges",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          }
        ]
      },
      {
        "id": 62,
        "symbol": "TWE",
        "label": "Tasmanian West",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          }
        ]
      },
      {
        "id": 63,
        "symbol": "VIB",
        "label": "Victoria Bonaparte",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 64,
        "symbol": "VIM",
        "label": "Victorian Midlands",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ]
      },
      {
        "id": 65,
        "symbol": "YAL",
        "label": "Yalgoo",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 66,
        "symbol": "WAR",
        "label": "Warren",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 67,
        "symbol": "WET",
        "label": "Wet Tropics",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 68,
        "symbol": "ESP",
        "label": "Esperance Plains",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 69,
        "symbol": "EYB",
        "label": "Eyre Yorke Block",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ]
      },
      {
        "id": 70,
        "symbol": "FUR",
        "label": "Furneaux",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ]
      },
      {
        "id": 71,
        "symbol": "GES",
        "label": "Geraldton Sandplains",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 72,
        "symbol": "JAF",
        "label": "Jarrah Forest",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 73,
        "symbol": "KAN",
        "label": "Kanmantoo",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ]
      },
      {
        "id": 74,
        "symbol": "KIN",
        "label": "King",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          }
        ]
      },
      {
        "id": 75,
        "symbol": "MAL",
        "label": "Mallee",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 76,
        "symbol": "NAN",
        "label": "Nandewar",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 77,
        "symbol": "NCP",
        "label": "Naracoorte Coastal Plains",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ]
      },
      {
        "id": 78,
        "symbol": "NET",
        "label": "New England Tablelands",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 79,
        "symbol": "NNC",
        "label": "NSW North Coast",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 80,
        "symbol": "NSS",
        "label": "NSW South Western Slopes",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ]
      },
      {
        "id": 81,
        "symbol": "SCP",
        "label": "South East Coastal Plain",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ]
      },
      {
        "id": 82,
        "symbol": "SEC",
        "label": "South East Corner",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ]
      },
      {
        "id": 83,
        "symbol": "SEH",
        "label": "South Eastern Highlands",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 1,
            "symbol": "CT",
            "label": "Australian Capital Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/4253c9af-b390-5b69-aaf3-ce5cda0c36bf"
          },
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ]
      },
      {
        "id": 84,
        "symbol": "SEQ",
        "label": "South Eastern Queensland",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 85,
        "symbol": "SVP",
        "label": "Southern Volcanic Plain",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ]
      },
      {
        "id": 86,
        "symbol": "COS",
        "label": "Coral Sea",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      },
      {
        "id": 87,
        "symbol": "SAI",
        "label": "Subantarctic Islands",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 88,
        "symbol": "ITI",
        "label": "Indian Tropical Islands",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ]
      },
      {
        "id": 89,
        "symbol": "PSI",
        "label": "Pacific Subtropical Islands",
        "description": "",
        "uri": "",
        "states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ]
      }
    ],
    "lut-broad-monitoring-type": [
      {
        "id": 1,
        "symbol": "FLM",
        "label": "Flora monitoring",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "FAM",
        "label": "Fauna monitoring",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "HC",
        "label": "Habitat condition (includes restoration/fire recovery)",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "ECA",
        "label": "Ecological community assessment",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "ECC",
        "label": "Ecological community condition (includes restoration)",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "LSM",
        "label": "Land/soil monitoring",
        "description": "",
        "uri": ""
      }
    ],
    "lut-camera-activation-mechanism": [
      {
        "id": 1,
        "symbol": "Passive infrared",
        "label": "Passive infrared",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Active infrared",
        "label": "Active infrared",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Beam break",
        "label": "Beam break",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Microwave motion",
        "label": "Microwave motion",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "Mechanical trigger",
        "label": "Mechanical trigger",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "Time lapse",
        "label": "Time lapse",
        "description": "",
        "uri": ""
      }
    ],
    "lut-camera-aspect-ratio": [
      {
        "id": 1,
        "symbol": "4:3 standard",
        "label": "4:3 standard",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "16:9 wide",
        "label": "16:9 wide",
        "description": "",
        "uri": ""
      }
    ],
    "lut-camera-illumination-type": [
      {
        "id": 1,
        "symbol": "Infrared - red glow",
        "label": "Infrared - red glow",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Infrared - low glow",
        "label": "Infrared - low glow",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Infrared - no glow",
        "label": "Infrared - no glow",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "White flash - xenon",
        "label": "White flash - xenon",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "White flash - LED",
        "label": "White flash - LED",
        "description": "",
        "uri": ""
      }
    ],
    "lut-camera-make": [
      {
        "id": 1,
        "symbol": "Browning",
        "label": "Browning",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Bushnell",
        "label": "Bushnell",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Covert",
        "label": "Covert",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Moultrie",
        "label": "Moultrie",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "Muddy",
        "label": "Muddy",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "Primos",
        "label": "Primos",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "Reconyx",
        "label": "Reconyx",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "ScoutGuard",
        "label": "ScoutGuard",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "Spartan",
        "label": "Spartan",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "Spypoint",
        "label": "Spypoint",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "Stealth Cam",
        "label": "Stealth Cam",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "Swift",
        "label": "Swift",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "Other",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-camera-media-type": [
      {
        "id": 1,
        "symbol": "I",
        "label": "Image",
        "description": "The mode of media capture selected for camera trap for a single shoot.",
        "uri": "https://linked.data.gov.au/def/nrm/1bd869ac-a542-5707-a24b-725952225989"
      },
      {
        "id": 2,
        "symbol": "V",
        "label": "Video",
        "description": "The mode of media capture selected for camera trap for a video output.",
        "uri": "https://linked.data.gov.au/def/nrm/0aaa2dee-40b2-50de-a78e-85c9f3fd00eb"
      },
      {
        "id": 3,
        "symbol": "IV",
        "label": "Image + Video",
        "description": "The mode of media capture selected for camera trap for a collection of image and videos. ",
        "uri": "https://linked.data.gov.au/def/nrm/5b035450-d4df-549e-811d-041b1dff0455"
      }
    ],
    "lut-camera-model": [
      {
        "id": 1,
        "symbol": "Command Ops Elite",
        "label": "Command Ops Elite",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 2,
        "symbol": "Command Ops RPO",
        "label": "Command Ops RPO",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 3,
        "symbol": "Dark Ops Apex",
        "label": "Dark Ops Apex",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 4,
        "symbol": "Dark Ops HD Max",
        "label": "Dark Ops HD Max",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 5,
        "symbol": "Dark Ops HD Pro X",
        "label": "Dark Ops HD Pro X",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 6,
        "symbol": "Dark Ops Max Plus",
        "label": "Dark Ops Max Plus",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 7,
        "symbol": "Dark Ops Pro DCL",
        "label": "Dark Ops Pro DCL",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "Dark Ops Pro XD",
        "label": "Dark Ops Pro XD",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 9,
        "symbol": "Defender Ridgeline",
        "label": "Defender Ridgeline",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 10,
        "symbol": "Patriot Trail Camera",
        "label": "Patriot Trail Camera",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 11,
        "symbol": "Recon Force Edge",
        "label": "Recon Force Edge",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 12,
        "symbol": "Recon Force 4K Edge",
        "label": "Recon Force 4K Edge",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 13,
        "symbol": "Spec Ops Advantage",
        "label": "Spec Ops Advantage",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 14,
        "symbol": "Spec Ops Edge",
        "label": "Spec Ops Edge",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 15,
        "symbol": "Spec Ops Elite HP4",
        "label": "Spec Ops Elite HP4",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 16,
        "symbol": "Spec Ops Elite HP5",
        "label": "Spec Ops Elite HP5",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 17,
        "symbol": "Strike Force Apex",
        "label": "Strike Force Apex",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 18,
        "symbol": "Strike Force HD Pro",
        "label": "Strike Force HD Pro",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 19,
        "symbol": "Strike Force HD Pro X",
        "label": "Strike Force HD Pro X",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 20,
        "symbol": "Strike Force Max Plus",
        "label": "Strike Force Max Plus",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 21,
        "symbol": "Strike Force Pro DCL",
        "label": "Strike Force Pro DCL",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 22,
        "symbol": "Strike Force Pro XD",
        "label": "Strike Force Pro XD",
        "description": "",
        "uri": "",
        "make": {
          "id": 1,
          "symbol": "Browning",
          "label": "Browning",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 23,
        "symbol": "Aggressor No Glow",
        "label": "Aggressor No Glow",
        "description": "",
        "uri": "",
        "make": {
          "id": 2,
          "symbol": "Bushnell",
          "label": "Bushnell",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 24,
        "symbol": "Aggressor HD Low Glow",
        "label": "Aggressor HD Low Glow",
        "description": "",
        "uri": "",
        "make": {
          "id": 2,
          "symbol": "Bushnell",
          "label": "Bushnell",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 25,
        "symbol": "Aggressor HD No Glow",
        "label": "Aggressor HD No Glow",
        "description": "",
        "uri": "",
        "make": {
          "id": 2,
          "symbol": "Bushnell",
          "label": "Bushnell",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 26,
        "symbol": "Core No Glow",
        "label": "Core No Glow",
        "description": "",
        "uri": "",
        "make": {
          "id": 2,
          "symbol": "Bushnell",
          "label": "Bushnell",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 27,
        "symbol": "Core DS Low Glow",
        "label": "Core DS Low Glow",
        "description": "",
        "uri": "",
        "make": {
          "id": 2,
          "symbol": "Bushnell",
          "label": "Bushnell",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 28,
        "symbol": "Core DS No Glow",
        "label": "Core DS No Glow",
        "description": "",
        "uri": "",
        "make": {
          "id": 2,
          "symbol": "Bushnell",
          "label": "Bushnell",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 29,
        "symbol": "Essential E2",
        "label": "Essential E2",
        "description": "",
        "uri": "",
        "make": {
          "id": 2,
          "symbol": "Bushnell",
          "label": "Bushnell",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 30,
        "symbol": "NatureView HD",
        "label": "NatureView HD",
        "description": "",
        "uri": "",
        "make": {
          "id": 2,
          "symbol": "Bushnell",
          "label": "Bushnell",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 31,
        "symbol": "Prime Low Glow",
        "label": "Prime Low Glow",
        "description": "",
        "uri": "",
        "make": {
          "id": 2,
          "symbol": "Bushnell",
          "label": "Bushnell",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 32,
        "symbol": "Prime L20 Low Glow",
        "label": "Prime L20 Low Glow",
        "description": "",
        "uri": "",
        "make": {
          "id": 2,
          "symbol": "Bushnell",
          "label": "Bushnell",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 33,
        "symbol": "Code Black Select",
        "label": "Code Black Select",
        "description": "",
        "uri": "",
        "make": {
          "id": 3,
          "symbol": "Covert",
          "label": "Covert",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 34,
        "symbol": "MP9",
        "label": "MP9",
        "description": "",
        "uri": "",
        "make": {
          "id": 3,
          "symbol": "Covert",
          "label": "Covert",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 35,
        "symbol": "MP30",
        "label": "MP30",
        "description": "",
        "uri": "",
        "make": {
          "id": 3,
          "symbol": "Covert",
          "label": "Covert",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 36,
        "symbol": "MP32",
        "label": "MP32",
        "description": "",
        "uri": "",
        "make": {
          "id": 3,
          "symbol": "Covert",
          "label": "Covert",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 37,
        "symbol": "NWF18",
        "label": "NWF18",
        "description": "",
        "uri": "",
        "make": {
          "id": 3,
          "symbol": "Covert",
          "label": "Covert",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 38,
        "symbol": "WC20-A",
        "label": "WC20-A",
        "description": "",
        "uri": "",
        "make": {
          "id": 3,
          "symbol": "Covert",
          "label": "Covert",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 39,
        "symbol": "WC20-V",
        "label": "WC20-V",
        "description": "",
        "uri": "",
        "make": {
          "id": 3,
          "symbol": "Covert",
          "label": "Covert",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 40,
        "symbol": "WC30-A",
        "label": "WC30-A",
        "description": "",
        "uri": "",
        "make": {
          "id": 3,
          "symbol": "Covert",
          "label": "Covert",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 41,
        "symbol": "WC30-V",
        "label": "WC30-V",
        "description": "",
        "uri": "",
        "make": {
          "id": 3,
          "symbol": "Covert",
          "label": "Covert",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 42,
        "symbol": "A-900",
        "label": "A-900",
        "description": "",
        "uri": "",
        "make": {
          "id": 4,
          "symbol": "Moultrie",
          "label": "Moultrie",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 43,
        "symbol": "A-900i",
        "label": "A-900i",
        "description": "",
        "uri": "",
        "make": {
          "id": 4,
          "symbol": "Moultrie",
          "label": "Moultrie",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 44,
        "symbol": "Delta Base",
        "label": "Delta Base",
        "description": "",
        "uri": "",
        "make": {
          "id": 4,
          "symbol": "Moultrie",
          "label": "Moultrie",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 45,
        "symbol": "Micro-32i",
        "label": "Micro-32i",
        "description": "",
        "uri": "",
        "make": {
          "id": 4,
          "symbol": "Moultrie",
          "label": "Moultrie",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 46,
        "symbol": "Micro-42",
        "label": "Micro-42",
        "description": "",
        "uri": "",
        "make": {
          "id": 4,
          "symbol": "Moultrie",
          "label": "Moultrie",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 47,
        "symbol": "Micro-42i",
        "label": "Micro-42i",
        "description": "",
        "uri": "",
        "make": {
          "id": 4,
          "symbol": "Moultrie",
          "label": "Moultrie",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 48,
        "symbol": "Mobile Delta Edge",
        "label": "Mobile Delta Edge",
        "description": "",
        "uri": "",
        "make": {
          "id": 4,
          "symbol": "Moultrie",
          "label": "Moultrie",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 49,
        "symbol": "Mobile Edge",
        "label": "Mobile Edge",
        "description": "",
        "uri": "",
        "make": {
          "id": 4,
          "symbol": "Moultrie",
          "label": "Moultrie",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 50,
        "symbol": "Mobile Exo",
        "label": "Mobile Exo",
        "description": "",
        "uri": "",
        "make": {
          "id": 4,
          "symbol": "Moultrie",
          "label": "Moultrie",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 51,
        "symbol": "W-800",
        "label": "W-800",
        "description": "",
        "uri": "",
        "make": {
          "id": 4,
          "symbol": "Moultrie",
          "label": "Moultrie",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 52,
        "symbol": "Manifest 2.0",
        "label": "Manifest 2.0",
        "description": "",
        "uri": "",
        "make": {
          "id": 5,
          "symbol": "Muddy",
          "label": "Muddy",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 53,
        "symbol": "Merge",
        "label": "Merge",
        "description": "",
        "uri": "",
        "make": {
          "id": 5,
          "symbol": "Muddy",
          "label": "Muddy",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 54,
        "symbol": "Morph",
        "label": "Morph",
        "description": "",
        "uri": "",
        "make": {
          "id": 5,
          "symbol": "Muddy",
          "label": "Muddy",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 55,
        "symbol": "MTC100",
        "label": "MTC100",
        "description": "",
        "uri": "",
        "make": {
          "id": 5,
          "symbol": "Muddy",
          "label": "Muddy",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 56,
        "symbol": "MTC100K",
        "label": "MTC100K",
        "description": "",
        "uri": "",
        "make": {
          "id": 5,
          "symbol": "Muddy",
          "label": "Muddy",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 57,
        "symbol": "Pro-Cam 14",
        "label": "Pro-Cam 14",
        "description": "",
        "uri": "",
        "make": {
          "id": 5,
          "symbol": "Muddy",
          "label": "Muddy",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 58,
        "symbol": "Pro-Cam 18",
        "label": "Pro-Cam 18",
        "description": "",
        "uri": "",
        "make": {
          "id": 5,
          "symbol": "Muddy",
          "label": "Muddy",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 59,
        "symbol": "Pro-Cam 20",
        "label": "Pro-Cam 20",
        "description": "",
        "uri": "",
        "make": {
          "id": 5,
          "symbol": "Muddy",
          "label": "Muddy",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 60,
        "symbol": "Pro-Cam 24",
        "label": "Pro-Cam 24",
        "description": "",
        "uri": "",
        "make": {
          "id": 5,
          "symbol": "Muddy",
          "label": "Muddy",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 61,
        "symbol": "Proof Cam 1",
        "label": "Proof Cam 1",
        "description": "",
        "uri": "",
        "make": {
          "id": 6,
          "symbol": "Primos",
          "label": "Primos",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 62,
        "symbol": "Proof Cam 2",
        "label": "Proof Cam 2",
        "description": "",
        "uri": "",
        "make": {
          "id": 6,
          "symbol": "Primos",
          "label": "Primos",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 63,
        "symbol": "Proof Cam 3",
        "label": "Proof Cam 3",
        "description": "",
        "uri": "",
        "make": {
          "id": 6,
          "symbol": "Primos",
          "label": "Primos",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 64,
        "symbol": "HyperFire HC500",
        "label": "HyperFire HC500",
        "description": "",
        "uri": "",
        "make": {
          "id": 7,
          "symbol": "Reconyx",
          "label": "Reconyx",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 65,
        "symbol": "HyperFire HC550",
        "label": "HyperFire HC550",
        "description": "",
        "uri": "",
        "make": {
          "id": 7,
          "symbol": "Reconyx",
          "label": "Reconyx",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 66,
        "symbol": "HyperFire HC600",
        "label": "HyperFire HC600",
        "description": "",
        "uri": "",
        "make": {
          "id": 7,
          "symbol": "Reconyx",
          "label": "Reconyx",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 67,
        "symbol": "MicroFire MR5",
        "label": "MicroFire MR5",
        "description": "",
        "uri": "",
        "make": {
          "id": 7,
          "symbol": "Reconyx",
          "label": "Reconyx",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 68,
        "symbol": "Professional PC800",
        "label": "Professional PC800",
        "description": "",
        "uri": "",
        "make": {
          "id": 7,
          "symbol": "Reconyx",
          "label": "Reconyx",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 69,
        "symbol": "Professional PC850",
        "label": "Professional PC850",
        "description": "",
        "uri": "",
        "make": {
          "id": 7,
          "symbol": "Reconyx",
          "label": "Reconyx",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 70,
        "symbol": "Professional PC900",
        "label": "Professional PC900",
        "description": "",
        "uri": "",
        "make": {
          "id": 7,
          "symbol": "Reconyx",
          "label": "Reconyx",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 71,
        "symbol": "Security SM750",
        "label": "Security SM750",
        "description": "",
        "uri": "",
        "make": {
          "id": 7,
          "symbol": "Reconyx",
          "label": "Reconyx",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 72,
        "symbol": "Security SC950",
        "label": "Security SC950",
        "description": "",
        "uri": "",
        "make": {
          "id": 7,
          "symbol": "Reconyx",
          "label": "Reconyx",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 73,
        "symbol": "UltraFire WR6",
        "label": "UltraFire WR6",
        "description": "",
        "uri": "",
        "make": {
          "id": 7,
          "symbol": "Reconyx",
          "label": "Reconyx",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 74,
        "symbol": "UltraFire XR6",
        "label": "UltraFire XR6",
        "description": "",
        "uri": "",
        "make": {
          "id": 7,
          "symbol": "Reconyx",
          "label": "Reconyx",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 75,
        "symbol": "Ecoglow White Flash",
        "label": "Ecoglow White Flash",
        "description": "",
        "uri": "",
        "make": {
          "id": 8,
          "symbol": "ScoutGuard",
          "label": "ScoutGuard",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 76,
        "symbol": "No Glow",
        "label": "No Glow",
        "description": "",
        "uri": "",
        "make": {
          "id": 8,
          "symbol": "ScoutGuard",
          "label": "ScoutGuard",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 77,
        "symbol": "SG550",
        "label": "SG550",
        "description": "",
        "uri": "",
        "make": {
          "id": 8,
          "symbol": "ScoutGuard",
          "label": "ScoutGuard",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 78,
        "symbol": "SG551",
        "label": "SG551",
        "description": "",
        "uri": "",
        "make": {
          "id": 8,
          "symbol": "ScoutGuard",
          "label": "ScoutGuard",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 79,
        "symbol": "SG560C",
        "label": "SG560C",
        "description": "",
        "uri": "",
        "make": {
          "id": 8,
          "symbol": "ScoutGuard",
          "label": "ScoutGuard",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 80,
        "symbol": "SG565",
        "label": "SG565",
        "description": "",
        "uri": "",
        "make": {
          "id": 8,
          "symbol": "ScoutGuard",
          "label": "ScoutGuard",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 81,
        "symbol": "SG880MK-8M",
        "label": "SG880MK-8M",
        "description": "",
        "uri": "",
        "make": {
          "id": 8,
          "symbol": "ScoutGuard",
          "label": "ScoutGuard",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 82,
        "symbol": "Zeroglow 8M-HD",
        "label": "Zeroglow 8M-HD",
        "description": "",
        "uri": "",
        "make": {
          "id": 8,
          "symbol": "ScoutGuard",
          "label": "ScoutGuard",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 83,
        "symbol": "Zeroglow-10M",
        "label": "Zeroglow-10M",
        "description": "",
        "uri": "",
        "make": {
          "id": 8,
          "symbol": "ScoutGuard",
          "label": "ScoutGuard",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 84,
        "symbol": "GoCam AT&T Red Glow",
        "label": "GoCam AT&T Red Glow",
        "description": "",
        "uri": "",
        "make": {
          "id": 9,
          "symbol": "Spartan",
          "label": "Spartan",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 85,
        "symbol": "GoCam No Glow",
        "label": "GoCam No Glow",
        "description": "",
        "uri": "",
        "make": {
          "id": 9,
          "symbol": "Spartan",
          "label": "Spartan",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 86,
        "symbol": "Force 10",
        "label": "Force 10",
        "description": "",
        "uri": "",
        "make": {
          "id": 10,
          "symbol": "Spypoint",
          "label": "Spypoint",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 87,
        "symbol": "Force 11D",
        "label": "Force 11D",
        "description": "",
        "uri": "",
        "make": {
          "id": 10,
          "symbol": "Spypoint",
          "label": "Spypoint",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 88,
        "symbol": "Link 3G",
        "label": "Link 3G",
        "description": "",
        "uri": "",
        "make": {
          "id": 10,
          "symbol": "Spypoint",
          "label": "Spypoint",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 89,
        "symbol": "MMS",
        "label": "MMS",
        "description": "",
        "uri": "",
        "make": {
          "id": 10,
          "symbol": "Spypoint",
          "label": "Spypoint",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 90,
        "symbol": "Solar",
        "label": "Solar",
        "description": "",
        "uri": "",
        "make": {
          "id": 10,
          "symbol": "Spypoint",
          "label": "Spypoint",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 91,
        "symbol": "G34 Pro",
        "label": "G34 Pro",
        "description": "",
        "uri": "",
        "make": {
          "id": 11,
          "symbol": "Stealth Cam",
          "label": "Stealth Cam",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 92,
        "symbol": "G45NG Pro",
        "label": "G45NG Pro",
        "description": "",
        "uri": "",
        "make": {
          "id": 11,
          "symbol": "Stealth Cam",
          "label": "Stealth Cam",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 93,
        "symbol": "PX36NG",
        "label": "PX36NG",
        "description": "",
        "uri": "",
        "make": {
          "id": 11,
          "symbol": "Stealth Cam",
          "label": "Stealth Cam",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 94,
        "symbol": "ZX24",
        "label": "ZX24",
        "description": "",
        "uri": "",
        "make": {
          "id": 11,
          "symbol": "Stealth Cam",
          "label": "Stealth Cam",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 95,
        "symbol": "ZX36NG",
        "label": "ZX36NG",
        "description": "",
        "uri": "",
        "make": {
          "id": 11,
          "symbol": "Stealth Cam",
          "label": "Stealth Cam",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 96,
        "symbol": "Enduro",
        "label": "Enduro",
        "description": "",
        "uri": "",
        "make": {
          "id": 12,
          "symbol": "Swift",
          "label": "Swift",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 97,
        "symbol": "Enduro 4G",
        "label": "Enduro 4G",
        "description": "",
        "uri": "",
        "make": {
          "id": 12,
          "symbol": "Swift",
          "label": "Swift",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 98,
        "symbol": "Enduro Wide Angle",
        "label": "Enduro Wide Angle",
        "description": "",
        "uri": "",
        "make": {
          "id": 12,
          "symbol": "Swift",
          "label": "Swift",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 99,
        "symbol": "Other (specify)",
        "label": "Other (specify)",
        "description": "",
        "uri": ""
      }
    ],
    "lut-camera-operational-status": [
      {
        "id": 1,
        "symbol": "Operational",
        "label": "Operational",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "SD card full",
        "label": "SD card full",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "SD card failure",
        "label": "SD card failure",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Hardware failure",
        "label": "Hardware failure",
        "description": "Refers to the camera operational status, i.e., has a hardware failure.",
        "uri": "https://linked.data.gov.au/def/nrm/bc3fde81-5545-512a-8881-1eec39ffdeba"
      },
      {
        "id": 5,
        "symbol": "Batteries flat",
        "label": "Batteries flat",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "Unknown failure",
        "label": "Unknown failure",
        "description": "Refers to the camera operational status, i.e., unknown or unable to be determined.",
        "uri": "https://linked.data.gov.au/def/nrm/ca8e760a-c587-5864-9bb6-b2c110ddb178"
      },
      {
        "id": 7,
        "symbol": "Vandalism",
        "label": "Vandalism",
        "description": "Refers to the camera operational status, i.e., has been subject to vandalism.",
        "uri": "https://linked.data.gov.au/def/nrm/fdf0fa65-27f8-562f-82a8-c9618268c1a7"
      },
      {
        "id": 8,
        "symbol": "Missing/theft",
        "label": "Missing/theft",
        "description": "Refers to the camera operational status, i.e., camera went missing or has been a subject to theft.",
        "uri": "https://linked.data.gov.au/def/nrm/2938eac3-2b43-5dad-9941-d98625859b1f"
      },
      {
        "id": 9,
        "symbol": "Wildlife damage",
        "label": "Wildlife damage",
        "description": "",
        "uri": ""
      }
    ],
    "lut-camera-resolution": [
      {
        "id": 1,
        "symbol": "7680p (8K) (7680x4320)",
        "label": "7680p (8K) (7680x4320)",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "2160p (4K) (3840x2160)",
        "label": "2160p (4K) (3840x2160)",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "1440p (2560x1440)",
        "label": "1440p (2560x1440)",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "1080p (1920x1080)",
        "label": "1080p (1920x1080)",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "720p (1280x720",
        "label": "720p (1280x720",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "480p (854x480)",
        "label": "480p (854x480)",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "360p (640x360)",
        "label": "360p (640x360)",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "240p (426x240)",
        "label": "240p (426x240)",
        "description": "",
        "uri": ""
      }
    ],
    "lut-camera-sensor-sensitivity": [
      {
        "id": 1,
        "symbol": "Low",
        "label": "Low",
        "description": "A mode of motion sensitivity setting in a camera trap.",
        "uri": "https://linked.data.gov.au/def/nrm/2acae838-85f3-5a46-a4f6-fae56c973a35"
      },
      {
        "id": 2,
        "symbol": "Low/Med",
        "label": "Low/Med",
        "description": "A mode of motion sensitivity setting in a camera trap.",
        "uri": "https://linked.data.gov.au/def/nrm/3a885b01-6075-59c0-a119-be4b50d0dc26"
      },
      {
        "id": 3,
        "symbol": "Med",
        "label": "Med",
        "description": "A mode of motion sensitivity setting in a camera trap.",
        "uri": "https://linked.data.gov.au/def/nrm/3eb7fb66-1b5b-5b46-9c5e-7c0272e8042e"
      },
      {
        "id": 4,
        "symbol": "Med/High",
        "label": "Med/High",
        "description": "A mode of motion sensitivity setting in a camera trap.",
        "uri": "https://linked.data.gov.au/def/nrm/199599fb-3d1d-5ad9-b80e-3050090f0a6c"
      },
      {
        "id": 5,
        "symbol": "High",
        "label": "High",
        "description": "A mode of motion sensitivity setting in a camera trap.",
        "uri": "https://linked.data.gov.au/def/nrm/64c0b1f8-faf9-5b88-a8f9-7e23c599290d"
      },
      {
        "id": 6,
        "symbol": "Very High",
        "label": "Very High",
        "description": "",
        "uri": ""
      }
    ],
    "lut-camera-trap-mount": [
      {
        "id": 1,
        "symbol": "Star dropper",
        "label": "Star dropper",
        "description": "The method of mounting or securing the camera trap.",
        "uri": "https://linked.data.gov.au/def/nrm/62dcac55-f4c8-5214-aaba-82dc969aea00"
      },
      {
        "id": 2,
        "symbol": "Survey stake - steel",
        "label": "Survey stake - steel",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Survey stake - wood",
        "label": "Survey stake - wood",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Tree - trunk or stem",
        "label": "Tree - trunk or stem",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "Tree - branch",
        "label": "Tree - branch",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "Tree - felled log",
        "label": "Tree - felled log",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "Tree - stump",
        "label": "Tree - stump",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "Tripod",
        "label": "Tripod",
        "description": "Tripods or tripedal stands are one of the method/s used for mounting or securing the camera trap. ",
        "uri": "https://linked.data.gov.au/def/nrm/308c769b-270b-5df4-a55b-650a33877dff"
      },
      {
        "id": 9,
        "symbol": "Rock face",
        "label": "Rock face",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "Rock pile",
        "label": "Rock pile",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "Building wall or ledge",
        "label": "Building wall or ledge",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "Building post",
        "label": "Building post",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "Sign post",
        "label": "Sign post",
        "description": "",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "Bridge",
        "label": "Bridge",
        "description": "",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "Over or underpass",
        "label": "Over or underpass",
        "description": "",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "Culvert",
        "label": "Culvert",
        "description": "",
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "Other",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      }
    ],
    "lut-camera-trap-survey-type": [
      {
        "id": 1,
        "symbol": "P",
        "label": "Point",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "AG",
        "label": "Array-grid",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "AT",
        "label": "Array-transect",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "F",
        "label": "Fauna Plot",
        "description": "",
        "uri": ""
      }
    ],
    "lut-cat-coat-colour": [
      {
        "id": 1,
        "symbol": "G",
        "label": "Ginger",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "B",
        "label": "Black",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "T",
        "label": "Tabby",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "TT",
        "label": "Tortoiseshell",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "U",
        "label": "Unknown",
        "description": "",
        "uri": ""
      }
    ],
    "lut-chemosis-eye": [
      {
        "id": 1,
        "symbol": "N",
        "label": "Normal",
        "description": "Normal",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "MI",
        "label": "Mild",
        "description": "Swelling <50% of conjunctiva and the nictitating membrane only",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "MO",
        "label": "Moderate",
        "description": "Swelling ≥ 50% of conjunctiva and nictitating membrane total; obscuring the cornea by ≤ 50%",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "S",
        "label": "Severe",
        "description": "Severe swelling of the entire conjunctiva that obscures the cornea > 50%",
        "uri": ""
      }
    ],
    "lut-condition-damage-type": [
      {
        "id": 1,
        "symbol": "Gall",
        "label": "Gall",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Lerp",
        "label": "Lerp",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Scale",
        "label": "Scale",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Blistering",
        "label": "Blistering",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "Skeletonisation",
        "label": "Skeletonisation",
        "description": "",
        "uri": ""
      }
    ],
    "lut-condition-disturbance-category": [
      {
        "id": 1,
        "symbol": "HAN",
        "label": "Herbivory by native animals (e.g. kangaroos)",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "DIS",
        "label": "Damage by invasive species (e.g. goats)",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "EWE",
        "label": "Extreme weather event (e.g. flood)",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "P",
        "label": "Pollution (e.g. chemical)",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "PD",
        "label": "Pathogen or disease (e.g. phytophthora)",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "LC",
        "label": "Limited clearing (e.g. understorey only)",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "EC",
        "label": "Extensive clearing (e.g. complete clearing of patches)",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "CC",
        "label": "Complete clearing",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "TLC",
        "label": "Total land-use conversion (e.g. mining, urban)",
        "description": "",
        "uri": ""
      }
    ],
    "lut-condition-disturbance-temporal-scale": [
      {
        "id": 1,
        "symbol": "O",
        "label": "Ongoing",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "H",
        "label": "Historical",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "C",
        "label": "Complete",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "U",
        "label": "Unknown",
        "description": "",
        "uri": ""
      }
    ],
    "lut-condition-disturbance-type": [
      {
        "id": 1,
        "symbol": "D",
        "label": "Direct",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "I",
        "label": "Indirect",
        "description": "",
        "uri": ""
      }
    ],
    "lut-condition-grazing-severity": [
      {
        "id": 1,
        "symbol": "Severe",
        "label": "Severe",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Heavy",
        "label": "Heavy",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Light",
        "label": "Light",
        "description": "",
        "uri": ""
      }
    ],
    "lut-condition-growth-stage-tree": [
      {
        "id": 1,
        "symbol": "RC",
        "label": "Recruiting",
        "description": "From the juvenile and sapling stages to well-developed individuals with a crown of small branches, but below maximum height for a stand, crown exhibits apical dominance – approx. 0-30 years",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "RSE",
        "label": "Resprouting"
      },
      {
        "id": 3,
        "symbol": "M",
        "label": "Mature",
        "description": "Tree has reached maximum height and crown has reached full lateral development although branch thickening can occur. Apical dominance lost – approx. 30-80 years",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "S",
        "label": "Senescent",
        "description": "Crown form contracting and becoming ‘stag headed’ decrease in crown diameter and crown leaf area. Distorted branches and burls common – approx. >80 years",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "D",
        "label": "Dead",
        "description": ""
      }
    ],
    "lut-condition-height-instrument": [
      {
        "id": 1,
        "symbol": "RF",
        "label": "Range Finder",
        "description": "A range finder was used to measure tree height",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "C",
        "label": "Clinometer",
        "description": "A clinometer was used to measure tree height",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "O",
        "label": "Other",
        "description": "Other equipment was used to measure tree height",
        "uri": ""
      }
    ],
    "lut-condition-human-induced-damage": [
      {
        "id": 1,
        "symbol": "LP",
        "label": "Lopping",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "R",
        "label": "Ringbarking",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "LG",
        "label": "Logging",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "P",
        "label": "Poisoning",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-condition-life-stage": [
      {
        "id": 1,
        "symbol": "SED",
        "label": "Seedling",
        "description": "A life stage/phenological stage of plants. For Trees: individuals <2 cm DBH; For Shrubs: <5cm height.",
        "uri": "https://linked.data.gov.au/def/nrm/6ab77673-43ec-51ce-ad13-8e5cf772ec8f"
      },
      {
        "id": 2,
        "symbol": "SAP",
        "label": "Sapling",
        "description": "A life stage of plants. For Trees: individuals <2 m height and ~2-10 cm DBH; For Shrubs: has not reached maximum height, apical dominance.",
        "uri": "https://linked.data.gov.au/def/nrm/f458582f-86fa-53be-b151-c4d7bbee4af2"
      },
      {
        "id": 3,
        "symbol": "BUD",
        "label": "Buds",
        "description": "Plants have buds formed in varying stages of development for flowering.",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "FLO",
        "label": "Flowers",
        "description": "Plants are in flower.",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "IMF",
        "label": "Immature Fruit",
        "description": "Life stage of plants with immature fruits, not shedding seed.",
        "uri": "https://linked.data.gov.au/def/nrm/d04f948c-2bd6-5e3d-af55-b35e48dc654e"
      },
      {
        "id": 6,
        "symbol": "MAF",
        "label": "Mature",
        "description": "Fruits fruits ripe and/or shedding seed.",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "RES",
        "label": "Recently Shed",
        "description": "Life stage of plants in a non-reproductive phase which show signs of having shed seed or fruits within the last 12 months.",
        "uri": "https://linked.data.gov.au/def/nrm/033e289d-45d5-5d1f-a48b-ebc2e2260dd0"
      },
      {
        "id": 8,
        "symbol": "DD",
        "label": "Dead/dormant",
        "description": "Indicates above-ground material only is dead and includes plant species that may still have dormant below-ground organs (eg orchids, lilies etc.)",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "RE",
        "label": "Regenerating",
        "description": "Woody perennial which is resprouting after significant loss of foliage.",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "VEG",
        "label": "Vegetative",
        "description": "Life stage of plants in a non-reproductive phase (i.e. no flowers, buds or unshed seed), that do not classify as seedlings or saplings.",
        "uri": "https://linked.data.gov.au/def/nrm/a5b9db61-6631-5dbc-b8b0-f116e9ff58e4"
      }
    ],
    "lut-control-activity-animal-fate": [
      {
        "id": 1,
        "symbol": "D",
        "label": "Destroyed",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "R",
        "label": "Relocated",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-corner": [
      {
        "id": 1,
        "symbol": "NW",
        "label": "North West",
        "description": "",
        "uri": "https://linked.data.gov.au/def/nrm/8a9b8a59-1c11-5ade-b7f8-2193e2f1b5f6"
      },
      {
        "id": 2,
        "symbol": "NE",
        "label": "North East",
        "description": "",
        "uri": "https://linked.data.gov.au/def/nrm/0144ee64-be0e-50a9-a644-07f60cedcfe0"
      },
      {
        "id": 3,
        "symbol": "SW",
        "label": "South West",
        "description": "",
        "uri": "https://linked.data.gov.au/def/nrm/ea731bb7-9a01-52be-9ffc-1a7c9be58f8c"
      },
      {
        "id": 4,
        "symbol": "SE",
        "label": "South East",
        "description": "",
        "uri": "https://linked.data.gov.au/def/nrm/3e3a1882-86a3-52b5-a456-36578ce1f746"
      }
    ],
    "lut-cover-transect-start-point": [
      {
        "id": 1,
        "symbol": "N1",
        "label": "North 1",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "N2",
        "label": "North 2",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "N3",
        "label": "North 3",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "N4",
        "label": "North 4",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "N5",
        "label": "North 5",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "E1",
        "label": "East 1",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "E2",
        "label": "East 2",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "E3",
        "label": "East 3",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "E4",
        "label": "East 4",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "E5",
        "label": "East 5",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "S1",
        "label": "South 1",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "S2",
        "label": "South 2",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "S3",
        "label": "South 3",
        "description": "",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "S4",
        "label": "South 4",
        "description": "",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "S5",
        "label": "South 5",
        "description": "",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "W1",
        "label": "West 1",
        "description": "",
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "W2",
        "label": "West 2",
        "description": "",
        "uri": ""
      },
      {
        "id": 18,
        "symbol": "W3",
        "label": "West 3",
        "description": "",
        "uri": ""
      },
      {
        "id": 19,
        "symbol": "W4",
        "label": "West 4",
        "description": "",
        "uri": ""
      },
      {
        "id": 20,
        "symbol": "W5",
        "label": "West 5",
        "description": "",
        "uri": ""
      }
    ],
    "lut-cwd-cut-off": [
      {
        "id": 1,
        "symbol": "C10",
        "label": "10 cm",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "C5",
        "label": "5 cm",
        "description": "",
        "uri": ""
      }
    ],
    "lut-cwd-cut-off-length": [
      {
        "id": 1,
        "symbol": "C50",
        "label": "50 cm",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "C30",
        "label": "30 cm",
        "description": "",
        "uri": ""
      }
    ],
    "lut-cwd-decay-class": [
      {
        "id": 1,
        "symbol": "C1",
        "label": "Class 1",
        "description": "Recently fallen. Structurally intact or almost so; bark or small branches still attached; few signs of wood decay; wood mostly retains original colour.",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "C2",
        "label": "Class 2",
        "description": "Structurally less intact but still hard when kicked; small branches absent; little or no bark present; early signs of wood decay, bark loss or discolouration.",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "C3",
        "label": "Class 3",
        "description": "Clearly decaying but still supports own weight; may be slightly soft when kicked; may be hollow in places; no bark; moss and fungi may be prominent.",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "C4",
        "label": "Class 4",
        "description": "Cannot support its own weight; soft to kick (but may still be hard in places; in which case may be extensively hollow); moss, fungi and invading roots likely.",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "C5",
        "label": "Class 5",
        "description": "No longer retains original shape; wood very soft or largely disintegrated; sometimes only outline visible beneath moss, invading roots.",
        "uri": ""
      }
    ],
    "lut-cwd-method": [
      {
        "id": 1,
        "symbol": "P40",
        "label": "40 x 40 m",
        "survey_method": {
          "id": 1,
          "symbol": "T",
          "label": "Transects",
          "description": "",
          "uri": ""
        },
        "description": "Use the transects laid out for the Cover or Condition Modules to delineate the 40 x 40 m sub-plot survey area between the N/S2 and N/S4, and E/W2 and E/W4 point-intercept transects. This 40 x 40 m subplot is the survey area for the Standard Protocol.",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "P100",
        "label": "100 x 100 m",
        "survey_method": {
          "id": 1,
          "symbol": "T",
          "label": "Transects",
          "description": "",
          "uri": ""
        },
        "description": "The 100 x 100 m (1 ha) core monitoring plot is the survey area for the Enhanced protocol.",
        "uri": ""
      }
    ],
    "lut-cwd-sampling-survey-method": [
      {
        "id": 1,
        "symbol": "T",
        "label": "Transects",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "P",
        "label": "Plots",
        "description": "",
        "uri": ""
      }
    ],
    "lut-cwd-transect-number": [
      {
        "id": 1,
        "symbol": "4",
        "label": "4 Transects",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "6",
        "label": "6 Transects",
        "description": "",
        "uri": ""
      }
    ],
    "lut-data-captured-flora": [
      {
        "id": 1,
        "symbol": "Observations",
        "label": "Observations",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Species",
        "label": "Species",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "COI",
        "label": "Count of indivdiuals",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "MIINN",
        "label": "Marked individual identification number/name",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "BW",
        "label": "Body weight",
        "description": "",
        "uri": "",
        "key_target_taxa": [
          {
            "id": 3,
            "symbol": "VC",
            "label": "Vegetation community",
            "description": "",
            "uri": ""
          },
          {
            "id": 4,
            "symbol": "Birds",
            "label": "Birds",
            "description": "",
            "uri": ""
          },
          {
            "id": 5,
            "symbol": "Mammals",
            "label": "Mammals",
            "description": "",
            "uri": ""
          },
          {
            "id": 6,
            "symbol": "Reptiles",
            "label": "Reptiles",
            "description": "",
            "uri": ""
          },
          {
            "id": 7,
            "symbol": "Frogs",
            "label": "Frogs",
            "description": "",
            "uri": ""
          }
        ]
      },
      {
        "id": 6,
        "symbol": "BM",
        "label": "Body measures",
        "description": "",
        "uri": "",
        "key_target_taxa": [
          {
            "id": 3,
            "symbol": "VC",
            "label": "Vegetation community",
            "description": "",
            "uri": ""
          },
          {
            "id": 4,
            "symbol": "Birds",
            "label": "Birds",
            "description": "",
            "uri": ""
          },
          {
            "id": 5,
            "symbol": "Mammals",
            "label": "Mammals",
            "description": "",
            "uri": ""
          },
          {
            "id": 6,
            "symbol": "Reptiles",
            "label": "Reptiles",
            "description": "",
            "uri": ""
          },
          {
            "id": 7,
            "symbol": "Frogs",
            "label": "Frogs",
            "description": "",
            "uri": ""
          }
        ]
      },
      {
        "id": 7,
        "symbol": "Height",
        "label": "Height",
        "description": "",
        "uri": "",
        "key_target_taxa": [
          {
            "id": 1,
            "symbol": "VF",
            "label": "Vascular flora",
            "description": "",
            "uri": ""
          },
          {
            "id": 2,
            "symbol": "NF",
            "label": "Non-vascular flora",
            "description": "",
            "uri": ""
          }
        ]
      },
      {
        "id": 8,
        "symbol": "SW",
        "label": "Stem width",
        "description": "",
        "uri": "",
        "key_target_taxa": [
          {
            "id": 1,
            "symbol": "VF",
            "label": "Vascular flora",
            "description": "",
            "uri": ""
          },
          {
            "id": 2,
            "symbol": "NF",
            "label": "Non-vascular flora",
            "description": "",
            "uri": ""
          }
        ]
      },
      {
        "id": 9,
        "symbol": "CW",
        "label": "Canopy width",
        "description": "",
        "uri": "",
        "key_target_taxa": [
          {
            "id": 1,
            "symbol": "VF",
            "label": "Vascular flora",
            "description": "",
            "uri": ""
          },
          {
            "id": 2,
            "symbol": "NF",
            "label": "Non-vascular flora",
            "description": "",
            "uri": ""
          }
        ]
      },
      {
        "id": 10,
        "symbol": "CH",
        "label": "Condition/health",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "RS",
        "label": "Reproductive status",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "AC",
        "label": "Age class",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "SSC",
        "label": "Sample/specimen collected",
        "description": "",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "PT",
        "label": "Photos taken",
        "description": "",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "LOIRT",
        "label": "Location of individual – radio-tracked",
        "description": "",
        "uri": "",
        "key_target_taxa": [
          {
            "id": 3,
            "symbol": "VC",
            "label": "Vegetation community",
            "description": "",
            "uri": ""
          },
          {
            "id": 4,
            "symbol": "Birds",
            "label": "Birds",
            "description": "",
            "uri": ""
          },
          {
            "id": 5,
            "symbol": "Mammals",
            "label": "Mammals",
            "description": "",
            "uri": ""
          },
          {
            "id": 6,
            "symbol": "Reptiles",
            "label": "Reptiles",
            "description": "",
            "uri": ""
          },
          {
            "id": 7,
            "symbol": "Frogs",
            "label": "Frogs",
            "description": "",
            "uri": ""
          }
        ]
      },
      {
        "id": 16,
        "symbol": "Other",
        "label": "Other",
        "description": "",
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "Phenology",
        "label": "Phenology",
        "description": "",
        "uri": ""
      }
    ],
    "lut-day-night-recording": [
      {
        "id": 1,
        "symbol": "Day only",
        "label": "Day only",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Night only",
        "label": "Night only",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Day + night",
        "label": "Day + night",
        "description": "",
        "uri": ""
      }
    ],
    "lut-deer-pest": [
      {
        "id": 1,
        "symbol": "FDDD",
        "label": "Fallow deer (Dama dama)",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "RDRT",
        "label": "Rusa deer (Rusa timorensis)",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "SDRU",
        "label": "Sambar deer (Rusa unicolor)",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "RDCE",
        "label": "Red deer (Cervus elaphus)",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "CDAA",
        "label": "Chital deer (Axis axis)",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "HDAP",
        "label": "Hog deer (Axis porcinus) ",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "Unknown",
        "label": "Unknown",
        "description": "",
        "uri": ""
      }
    ],
    "lut-direction": [
      {
        "id": 1,
        "symbol": "S",
        "label": "South",
        "description": "The spatial point location, denoting the 'south' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, coarse woody debris transect, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/f22f3722-89ee-5416-a296-113cccbe8647"
      },
      {
        "id": 2,
        "symbol": "N",
        "label": "North",
        "description": "The spatial point location, denoting the 'north' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, coarse woody debris transect, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/60acb00b-caa1-58cd-918f-25eccb01262e"
      },
      {
        "id": 3,
        "symbol": "E",
        "label": "East",
        "description": "The spatial point location, denoting the 'east' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, coarse woody debris transect, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/0cad1fc8-0741-5dc8-90f5-7a4c6c7fba5f"
      },
      {
        "id": 4,
        "symbol": "W",
        "label": "West",
        "description": "The spatial point location, denoting the 'west' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, coarse woody debris transect, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/56cc819c-6dbb-575d-a422-3809da923354"
      },
      {
        "id": 5,
        "symbol": "SW",
        "label": "South West",
        "description": "The spatial point location, denoting the 'southwest' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, coarse woody debris transect, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/945f0667-7479-557b-80a5-6926b508a073"
      },
      {
        "id": 6,
        "symbol": "SE",
        "label": "South East",
        "description": "The spatial point location, denoting the 'southeast' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, coarse woody debris transect, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/f4445dd3-6c4c-54ea-be31-61e651442209"
      },
      {
        "id": 7,
        "symbol": "NW",
        "label": "North West",
        "description": "The spatial point location, denoting the 'northwest' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/9f1e5344-fb46-5f36-afd0-dceae586839c"
      },
      {
        "id": 8,
        "symbol": "NE",
        "label": "North East",
        "description": "The spatial point location, denoting the 'northeast' end or orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/1822dbd3-3f96-5f45-9a03-b95235ede90f"
      }
    ],
    "lut-disturbance": [
      {
        "id": 1,
        "symbol": "0",
        "label": "No effective disturbance",
        "description": "No effective disturbance",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "1L",
        "label": "No effective disturbance except light grazing by hoofed animals",
        "description": "A description of the type of disturbance in a study plot.",
        "uri": "https://linked.data.gov.au/def/nrm/8457585c-9358-5bc0-b975-0a449b304006"
      },
      {
        "id": 3,
        "symbol": "1M",
        "label": "No effective disturbance except medium grazing by hoofed animals",
        "description": "A description of the type of disturbance in a study plot.",
        "uri": "https://linked.data.gov.au/def/nrm/49716af0-4f12-5886-87db-69297558c1e8"
      },
      {
        "id": 4,
        "symbol": "1H",
        "label": "No effective disturbance except high grazing by hoofed animals",
        "description": "A description of the type of disturbance in a study plot.",
        "uri": "https://linked.data.gov.au/def/nrm/f505c960-4d90-52f6-aee5-18102b784746"
      },
      {
        "id": 5,
        "symbol": "2",
        "label": "Limited clearing, for example limited logging",
        "description": "A description of the type of disturbance in a study plot.",
        "uri": "https://linked.data.gov.au/def/nrm/da08e0d3-2d54-5651-8499-8b2364e7c44d"
      },
      {
        "id": 6,
        "symbol": "3",
        "label": "Extensive clearing, for example poisoning, ringbarking",
        "description": "A description of the type of disturbance in a study plot.",
        "uri": "https://linked.data.gov.au/def/nrm/fcfc3e39-0508-5533-8a19-27f6f510e0aa"
      },
      {
        "id": 7,
        "symbol": "4",
        "label": "Complete clearing, pasture, native or improved, but never cultivated",
        "description": "A description of the type of disturbance in a study plot.",
        "uri": "https://linked.data.gov.au/def/nrm/afbe4699-68c2-5213-926e-a3baa6833383"
      },
      {
        "id": 8,
        "symbol": "5",
        "label": "Complete clearing, pasture, native or improved, cultivated at some stage",
        "description": "A description of the type of disturbance in a study plot.",
        "uri": "https://linked.data.gov.au/def/nrm/15f2b28b-e417-5073-b8d7-f1ef6e4ab1ad"
      },
      {
        "id": 9,
        "symbol": "6",
        "label": "Cultivated; rain fed",
        "description": "A description of the type of disturbance in a study plot.",
        "uri": "https://linked.data.gov.au/def/nrm/f86e5ee3-6e77-5c6c-a842-5ff43f659f6d"
      },
      {
        "id": 10,
        "symbol": "7",
        "label": "Cultivation, irrigated, past or present",
        "description": "A description of the type of disturbance in a study plot.",
        "uri": "https://linked.data.gov.au/def/nrm/72f73ab2-7c87-52c9-a32e-5243ad10990f"
      },
      {
        "id": 11,
        "symbol": "8",
        "label": "Highly disturbed, e.g. quarrying, road words, mining, landfill, urban",
        "description": "A description of the type of disturbance in a study plot.",
        "uri": "https://linked.data.gov.au/def/nrm/f125acdb-8cf9-53f6-b02f-38afd3cb2155"
      },
      {
        "id": 12,
        "symbol": "9",
        "label": "Regrowth after clearing",
        "description": "A description of the type of disturbance in a study plot.",
        "uri": "https://linked.data.gov.au/def/nrm/5837f40e-8013-4050-800f-699058f4324b"
      },
      {
        "id": 13,
        "symbol": "10",
        "label": "Significant natural disturbances, e.g. cyclonic impact",
        "description": "A description of the type of disturbance in a study plot.",
        "uri": "https://linked.data.gov.au/def/nrm/ea48040d-b81b-428a-b620-51bc019171aa"
      },
      {
        "id": 14,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "",
        "uri": ""
      }
    ],
    "lut-drone-capture-mode": [
      {
        "id": 1,
        "symbol": "T",
        "label": "Timer",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-drone-micasense-crp": [
      {
        "id": 1,
        "symbol": "P1",
        "label": "Pre-flight",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "P2",
        "label": "Post-flight",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "P3",
        "label": "Pre-flight and Post-flight",
        "description": "",
        "uri": ""
      }
    ],
    "lut-drone-return-mode": [
      {
        "id": 1,
        "symbol": "S",
        "label": "Single",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "D",
        "label": "Dual",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "T",
        "label": "Triple",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "PR",
        "label": "Penta-Return",
        "description": "",
        "uri": ""
      }
    ],
    "lut-drone-sampling-rate": [
      {
        "id": 1,
        "symbol": "160000",
        "label": "160 KHz",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "240000",
        "label": "240 KHz",
        "description": "",
        "uri": ""
      }
    ],
    "lut-drone-scanning-mode": [
      {
        "id": 1,
        "symbol": "R",
        "label": "Repetitive",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "NR",
        "label": "Non-repetitive",
        "description": "",
        "uri": ""
      }
    ],
    "lut-drone-sensor": [
      {
        "id": 1,
        "symbol": "P1REMXD",
        "label": "P1 + RedEdgeMX-Dual",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "LI2",
        "label": "Lidar L2",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "P1REMX",
        "label": "P1 + RedEdgeMX",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "DJIM3M",
        "label": "DJI Mavic 3M",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-drone-sky-code": [
      {
        "id": 1,
        "symbol": "0",
        "label": "0 Clear Sky",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "1 Haze",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "2 Thin cirrus, sun not obscured",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "3 Thin cirrus, sun obscured",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "4",
        "label": "4 Scattered cumulus, sun not obscured",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "5",
        "label": "5 Cumulus over most of sky, sun not obscured",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "6",
        "label": "6 Cumulus cirrus, sun obscured",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "7",
        "label": "7 Complete cumulus cover",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "8",
        "label": "8 Stratus, sun obscured",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "9",
        "label": "9 Drizzle",
        "description": "",
        "uri": ""
      }
    ],
    "lut-drone-white-balance": [
      {
        "id": 1,
        "symbol": "SU",
        "label": "Sunny (skycode 0 – 6)",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "OV",
        "label": "Overcast (skycode 7 – 9)",
        "description": "",
        "uri": ""
      }
    ],
    "lut-employed-method": [
      {
        "id": 1,
        "symbol": "Observation",
        "label": "Observation",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "OM",
        "label": "Observation - meander",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "OT",
        "label": "Observation - transects",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "OQP",
        "label": "Observation - quadrats or plots",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "AS",
        "label": "Active search",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "SS",
        "label": "Signs-search",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "Trapping",
        "label": "Trapping",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "CAT",
        "label": "Camera trapping",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "AR",
        "label": "Acoustic recorders",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "TS",
        "label": "Timed survey",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "HT",
        "label": "Harp traps",
        "description": "",
        "uri": ""
      }
    ],
    "lut-epbc-status": [
      {
        "id": 1,
        "symbol": "E",
        "label": "Endangered",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "CE",
        "label": "Critically Endangered",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "V",
        "label": "Vulnerable",
        "description": "",
        "uri": ""
      }
    ],
    "lut-exact-or-estimate": [
      {
        "id": 1,
        "symbol": "Exact",
        "label": "Exact",
        "description": "Whether the quantity is 'exact' or an 'estimate'.",
        "uri": "https://linked.data.gov.au/def/nrm/8c1c83f2-66b1-5651-ba3c-adbbf3806433"
      },
      {
        "id": 2,
        "symbol": "Estimate",
        "label": "Estimate",
        "description": "Whether the quantity is 'exact' or an 'estimate'.",
        "uri": "https://linked.data.gov.au/def/nrm/68c919a7-5759-5169-92dd-a99b6b7be407"
      }
    ],
    "lut-fauna-age-class": [
      {
        "id": 1,
        "symbol": "AD",
        "label": "Adult",
        "description": "Represents the fauna growth stage. Adult- refers to a maturity quality of an individual by virtue of the individual having attained sexual maturity and full growth.",
        "uri": "https://linked.data.gov.au/def/nrm/436e8a65-505a-505b-811e-c6be4e4ae651"
      },
      {
        "id": 2,
        "symbol": "IM",
        "label": "Immature (sub-adult)",
        "description": "Represents the fauna growth stage. Immature, refers to an early period of life or development or growth.",
        "uri": "https://linked.data.gov.au/def/nrm/0ce0cabf-e499-5360-82f4-67866d9f60da"
      },
      {
        "id": 3,
        "symbol": "JU",
        "label": "Juvenile",
        "description": "Represents the fauna growth stage. Juvenile refers to traits of an individual that are not fully grown or developed.",
        "uri": "https://linked.data.gov.au/def/nrm/6de1f874-fdcb-520a-9ff8-481a7968e76c"
      },
      {
        "id": 4,
        "symbol": "TA",
        "label": "Tadpole",
        "description": "Represents the fauna growth stage. A larval amphibian; specifically: a frog or toad larva that has a rounded body with a long tail bordered by fins and external gills soon replaced by internal gills and that undergoes a metamorphosis to the adult.",
        "uri": "https://linked.data.gov.au/def/nrm/f7ea86d0-7c49-5324-8180-f4cc0fecca9c"
      },
      {
        "id": 5,
        "symbol": "EG",
        "label": "Eggs/egg mass",
        "description": "Represents the fauna growth stage. Eggs/egg mass refers to the animal reproductive bodies- an organic vessel in which an embryo first begins to develop.",
        "uri": "https://linked.data.gov.au/def/nrm/42ecaeb5-306a-59f5-abb3-a27181d12cb6"
      },
      {
        "id": 6,
        "symbol": "ME",
        "label": "Metamorph",
        "description": "Represents the fauna growth stage in which an animal physically develops after birth or hatching, involving a conspicuous and relatively abrupt change in the animal's form or structure.",
        "uri": "https://linked.data.gov.au/def/nrm/6a07f0fb-6872-5143-b4ee-d2ddb1713cd6"
      },
      {
        "id": 7,
        "symbol": "NY",
        "label": "Nymph",
        "description": "Represents the fauna growth stage. The immature stage in the life cycle of those orders of insects characterized by gradual metamorphosis, in which the young resemble the imago in general form of body, including compound eyes and external wings; also the 8-legged stage of mites and ticks that follows the first moult.",
        "uri": "https://linked.data.gov.au/def/nrm/a3821c90-12cd-503a-9c67-edd109919aab"
      },
      {
        "id": 8,
        "symbol": "LA",
        "label": "Larvae",
        "description": "Represents the fauna growth stage. A larvae is an independently living, post-embryonic stage of an animal that is markedly differ­ent in form from the adult and which under­goes metamorphosis into the adult form, e.g. caterpillar, grub, tadpole. ",
        "uri": "https://linked.data.gov.au/def/nrm/35157e34-237b-5f09-8c55-0bebcf835191"
      },
      {
        "id": 9,
        "symbol": "PU",
        "label": "Pupa",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "UN",
        "label": "Unknown",
        "description": "Unknown/unable to be determined.",
        "uri": "https://linked.data.gov.au/def/nrm/f6b0f6d8-16d8-5dd7-b1b7-66b0c020b96f"
      }
    ],
    "lut-fauna-bird-activity-type": [
      {
        "id": 1,
        "symbol": "FO",
        "label": "Flying overhead",
        "description": "Represents the diagnostic behaviour of the bird under study. ",
        "uri": "https://linked.data.gov.au/def/nrm/c04ecd37-4825-5007-bf9b-c969c5d378a0"
      },
      {
        "id": 2,
        "symbol": "FC",
        "label": "Flying over circling",
        "description": "Represents the diagnostic behaviour of the bird under study. ",
        "uri": "https://linked.data.gov.au/def/nrm/ed6d796a-0a24-5d36-9077-223486105be4"
      },
      {
        "id": 3,
        "symbol": "FW",
        "label": "Flying within survey area at strata level",
        "description": "Represents the diagnostic behaviour of the bird under study. ",
        "uri": "https://linked.data.gov.au/def/nrm/6874404c-068c-58a9-a014-7442278ed160"
      },
      {
        "id": 4,
        "symbol": "ROT",
        "label": "Resting on tree",
        "description": "Represents the diagnostic behaviour of the bird under study. ",
        "uri": "https://linked.data.gov.au/def/nrm/0fe9b0b5-a35f-55c1-9f0b-004b7fe1b3a3"
      },
      {
        "id": 5,
        "symbol": "FOT",
        "label": "Foraging on tree",
        "description": "Represents the diagnostic behaviour of the bird under study. ",
        "uri": "https://linked.data.gov.au/def/nrm/62105a5f-1421-5941-84b8-25a8764a3d8b"
      },
      {
        "id": 6,
        "symbol": "ROG",
        "label": "Resting on ground",
        "description": "Represents the diagnostic behaviour of the bird under study. ",
        "uri": "https://linked.data.gov.au/def/nrm/3760dd88-caaf-55ae-8980-e7fc09738df7"
      },
      {
        "id": 7,
        "symbol": "FOG",
        "label": "Foraging on ground",
        "description": "Represents the diagnostic behaviour of the bird under study. ",
        "uri": "https://linked.data.gov.au/def/nrm/cc58f0fe-c48c-5de2-b5bf-e9c69d68dc93"
      },
      {
        "id": 8,
        "symbol": "ROS",
        "label": "Resting on shrub",
        "description": "Represents the diagnostic behaviour of the bird under study. ",
        "uri": "https://linked.data.gov.au/def/nrm/bc097808-d785-5269-a642-c7ac57d53dd5"
      },
      {
        "id": 9,
        "symbol": "FOS",
        "label": "Foraging on shrub",
        "description": "Represents the diagnostic behaviour of the bird under study. ",
        "uri": "https://linked.data.gov.au/def/nrm/490c945d-5a3d-5fd8-921c-e7ac130278dd"
      },
      {
        "id": 10,
        "symbol": "U",
        "label": "Unknown",
        "description": "Unknown/unable to be determined.",
        "uri": "https://linked.data.gov.au/def/nrm/f6b0f6d8-16d8-5dd7-b1b7-66b0c020b96f"
      }
    ],
    "lut-fauna-bird-breeding-type": [
      {
        "id": 1,
        "symbol": "NO",
        "label": "None",
        "description": "No breeding activity observed.",
        "uri": "https://linked.data.gov.au/def/nrm/5488c9ea-e3a2-5348-887b-065d29b848ab"
      },
      {
        "id": 2,
        "symbol": "NE",
        "label": "Nest with eggs",
        "description": "Eggs observed in nest.",
        "uri": "https://linked.data.gov.au/def/nrm/80615861-ca91-5aba-97ae-7d664723f547"
      },
      {
        "id": 3,
        "symbol": "NY",
        "label": "Nest with young",
        "description": "Young observed in nest.",
        "uri": "https://linked.data.gov.au/def/nrm/f70398df-73c5-53dd-815b-f469b345f9ee"
      },
      {
        "id": 4,
        "symbol": "AY",
        "label": "Adult(s) on nest",
        "description": "Adults observed incubating/brooding but contents not sighted.",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "YON",
        "label": "Young out of nest",
        "description": "Precocial chicks of any age or altricial species which have left the nest but which are still dependent on adults (not yet fledged).",
        "uri": "https://linked.data.gov.au/def/nrm/224da91e-0a65-5997-bc0c-4be688c70bcc"
      },
      {
        "id": 6,
        "symbol": "RFY",
        "label": "Recently fledged young",
        "description": "Juveniles no longer dependent on adults. This is weak evidence of actual nesting having occurred at the site/time of observation as juveniles often move quickly from natal territories after fledging.",
        "uri": "https://linked.data.gov.au/def/nrm/ed3ef29c-00f5-525c-aea5-cc915e752754"
      },
      {
        "id": 7,
        "symbol": "DBE",
        "label": "Diagnostic behaviour",
        "description": "Behaviour confirming an active nesting attempt. Distraction displays, dive bombing, brood patch evident, carrying food repeatedly to nest or hollow.",
        "uri": "https://linked.data.gov.au/def/nrm/2760d712-8211-557e-8caa-0af73af4e825"
      },
      {
        "id": 8,
        "symbol": "SBE",
        "label": "Suggestive behaviour",
        "description": "Behaviour suggesting a nesting attempt, but evidence insufficient to be used as confirmation. Nest building, courtship/copulation, single observation of carrying food.",
        "uri": "https://linked.data.gov.au/def/nrm/aca0e5ce-51ce-5deb-824e-63dbbcd09a70"
      },
      {
        "id": 9,
        "symbol": "NEC",
        "label": "Nesting colony",
        "description": "Nesting colony.",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "U",
        "label": "Unknown",
        "description": "Unknown/unable to be determined.",
        "uri": "https://linked.data.gov.au/def/nrm/f6b0f6d8-16d8-5dd7-b1b7-66b0c020b96f"
      }
    ],
    "lut-fauna-bird-observation-location-type": [
      {
        "id": 1,
        "symbol": "WS",
        "label": "Within Survey Area",
        "description": "The survey location representative of the habitat where the birds were recorded during a fauna survey.",
        "uri": "https://linked.data.gov.au/def/nrm/92ddecfe-f8f4-5de1-a00e-755de48f732d"
      },
      {
        "id": 2,
        "symbol": "OSH",
        "label": "Outside Survey - Same Habitat",
        "description": "The survey location outside a study area representing a habitat, similar to where the birds were recorded during a fauna survey.",
        "uri": "https://linked.data.gov.au/def/nrm/9748f268-c841-5d5b-9f5f-5973b2fc55f9"
      },
      {
        "id": 3,
        "symbol": "ODH",
        "label": "Outside survey - Different Habitat",
        "description": "The survey location outside a study area representing a habitat, different from the one where the birds were recorded during a fauna survey.",
        "uri": "https://linked.data.gov.au/def/nrm/8cf3f3eb-a056-52df-9367-92196de1f2f1"
      }
    ],
    "lut-fauna-bird-survey-type": [
      {
        "id": 1,
        "symbol": "202",
        "label": "20 minute, 2ha",
        "description": "The survey method implemented for bird observations. In this case, the bird survey is carried out for 20 minutes in the 2ha plot.",
        "uri": "https://linked.data.gov.au/def/nrm/b6c0cb28-56a1-513a-8294-49151cda478c"
      },
      {
        "id": 2,
        "symbol": "500",
        "label": "500m area search",
        "description": "The survey method implemented for bird observations. In this case, the bird survey is carried out over a 500m area.",
        "uri": "https://linked.data.gov.au/def/nrm/bc04f48a-f455-5c21-8695-5048a68807fa"
      }
    ],
    "lut-fauna-breeding-code": [
      {
        "id": 1,
        "symbol": "UK",
        "label": "Unknown",
        "description": "Unknown/unable to be determined.",
        "uri": "https://linked.data.gov.au/def/nrm/f6b0f6d8-16d8-5dd7-b1b7-66b0c020b96f"
      },
      {
        "id": 2,
        "symbol": "BR",
        "label": "Breeding",
        "description": "A categorical description of the 'Breeding Status' sighted during bird observation. 'Breeding' activity refers to sexual reproduction that produces an offspring.",
        "uri": "https://linked.data.gov.au/def/nrm/e8b5be54-1aae-5b8d-8f38-7bb0c26e5c5b"
      },
      {
        "id": 3,
        "symbol": "TL",
        "label": "Teats - Lactating",
        "description": "Teats and surrounding mammary tissue swollen. Milk can be squeezed from teat",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "TD",
        "label": "Teats - Distended",
        "description": "Teats easy to see, enlarged",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "TB",
        "label": "Teats - Button",
        "description": "Teats barely visible",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "TS",
        "label": "Teats - Scrotal (placentals only)",
        "description": "Scrotal sack obvious, may appear empty or full",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "TA",
        "label": "Teats - Abdominal (placentals only)",
        "description": "Scrotum not obvious",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "NE",
        "label": "Nesting",
        "description": "A categorical description of the 'Breeding Status' sighted during bird observation. Animal behavior associated with the nest; includes construction, effects of size and material; behavior of the adult during the nesting period and the effect of the nest on the behavior of the young.",
        "uri": "https://linked.data.gov.au/def/nrm/29173358-a88d-5c89-b92b-28d82846fbe2"
      },
      {
        "id": 9,
        "symbol": "NB",
        "label": "Not breeding",
        "description": "A categorical description of the 'Breeding Status' sighted during bird observation. ",
        "uri": "https://linked.data.gov.au/def/nrm/f7829329-c918-5bdd-b2ec-088e29ad1a7b"
      },
      {
        "id": 10,
        "symbol": "PR",
        "label": "Pregnant",
        "description": "A categorical description of the 'Breeding Status' sighted during bird observation. Refers to the a bird containing a developing embryo, fetus, or unborn offspring within the body.",
        "uri": "https://linked.data.gov.au/def/nrm/5c263122-0e2d-58bc-94b6-78fae732aa67"
      },
      {
        "id": 11,
        "symbol": "NPR",
        "label": "Not Pregnant",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "PM",
        "label": "Pouch not developed (marsupials only)",
        "description": "No pouch visible, usually just a ring of button teats",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "PNM",
        "label": "Pouch not developed, no young present (marsupials only)",
        "description": "Swollen, flaps present, teats enlarged",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "DE",
        "label": "Dependent young",
        "description": "A categorical description of the 'Breeding Status' sighted during bird observation. 'Dependent young' refers to chicks/juveniles that are dependent for nourishment and shelter. ",
        "uri": "https://linked.data.gov.au/def/nrm/81fdf73f-b666-58ac-9519-5b5aeccf481b"
      },
      {
        "id": 15,
        "symbol": "DYP",
        "label": "Dependent young in pouch (marsupials only)",
        "description": "Enter the number of young in observation notes field",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "DY",
        "label": "Dependent young (in nest)",
        "description": "A categorical description of the 'Breeding Status' sighted during bird observation.  Refers to the chicks/juveniles in nest that are dependent for nourishment and shelter. ",
        "uri": "https://linked.data.gov.au/def/nrm/e1747781-ed7d-59da-83f8-08a94c0f5242"
      },
      {
        "id": 17,
        "symbol": "GR",
        "label": "Gravid (carrying eggs or young)",
        "description": "A categorical description of the 'Breeding Status' sighted during bird observation. Refers to the a female quality inhering in a individual who is carrying fertilized eggs.",
        "uri": "https://linked.data.gov.au/def/nrm/1b0dc27f-55d3-5ad6-a134-2afbf5b42af4"
      },
      {
        "id": 18,
        "symbol": "NG",
        "label": "Not gravid",
        "description": "",
        "uri": ""
      },
      {
        "id": 19,
        "symbol": "BM",
        "label": "In amplexus",
        "description": "",
        "uri": ""
      }
    ],
    "lut-fauna-observation-type": [
      {
        "id": 1,
        "symbol": "S",
        "label": "Seen",
        "description": "Refers to the method of sighting a fauna by the observer in a survey area.",
        "uri": "https://linked.data.gov.au/def/nrm/dbc20b66-84b7-5e72-83e9-9091be8d7de7"
      },
      {
        "id": 2,
        "symbol": "H",
        "label": "Heard",
        "description": "The method of fauna sighting in the form of calls, or acoustic signals. ",
        "uri": "https://linked.data.gov.au/def/nrm/ffb1c491-bbb1-5823-ac03-0543f9a9627a"
      }
    ],
    "lut-fauna-plot-point": [
      {
        "id": 1,
        "symbol": "C",
        "description": "Centre",
        "label": "Centre",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "NW",
        "description": "North-West",
        "label": "North-West",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "SW",
        "description": "South-West",
        "label": "South-West",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "SE",
        "description": "South-East",
        "label": "South-East",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "NE",
        "description": "North-East",
        "label": "North-East",
        "uri": ""
      }
    ],
    "lut-fauna-pouch-condition": [
      {
        "id": 1,
        "symbol": "1",
        "label": "Not developed",
        "description": "Represents the pouch condition of the animal. 'Not developed' - no pouch visible, usually just a ring of button teats.",
        "uri": "https://linked.data.gov.au/def/nrm/e06f9cc1-962f-5e8b-9a2b-9a4cae6b3ce0"
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "Developed",
        "description": "Represents the pouch condition of the animal. 'Developed'- swollen, flaps present, teats enlarged.",
        "uri": "https://linked.data.gov.au/def/nrm/76e74327-adb8-52be-9f50-4c2eef108f69"
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "Young present",
        "description": "Represents the pouch condition of the animal. 'Young present'- put number of young in separate column provided.",
        "uri": "https://linked.data.gov.au/def/nrm/a2674a72-8d73-528e-a5e5-49de3f13fbb3"
      }
    ],
    "lut-fauna-sex": [
      {
        "id": 1,
        "symbol": "M",
        "label": "Male",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "F",
        "label": "Female",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "U",
        "label": "Unknown",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "X",
        "label": "Mixed Sexes",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "N",
        "label": "Not Recorded",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "A",
        "label": "NA",
        "description": "",
        "uri": ""
      }
    ],
    "lut-fauna-survey-intent": [
      {
        "id": 1,
        "symbol": "OOM",
        "label": "Once-off measure",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "RM",
        "label": "Repeated measure",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "PRA",
        "label": "Linked to a pre-control activity",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "POA",
        "label": "Linked to a post-control activity",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-fauna-taxa-type": [
      {
        "id": 1,
        "symbol": "AM",
        "label": "Amphibian",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "BI",
        "label": "Bird",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "INV",
        "label": "Invertebrate",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "MA",
        "label": "Mammal",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "RE",
        "label": "Reptile",
        "description": "",
        "uri": ""
      }
    ],
    "lut-fauna-teates-size-category": [
      {
        "id": 1,
        "symbol": "N",
        "label": "Not enlarged",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "E",
        "label": "Enlarged",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "U",
        "label": "Unknown",
        "description": "",
        "uri": ""
      }
    ],
    "lut-fauna-teats-condition": [
      {
        "id": 1,
        "symbol": "1",
        "label": "Lactating",
        "description": "Describing the animal teat condition. 'Distended'- easy to see, enlarged.",
        "uri": "https://linked.data.gov.au/def/nrm/c05aeb0d-4186-527f-bed1-0273e2b104eb"
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "Distended/post-lactating",
        "description": "Describing the animal teat condition. 'Distended'- easy to see, enlarged.",
        "uri": "https://linked.data.gov.au/def/nrm/36024777-d9d8-5ec9-9ee2-c91af3a56279"
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "Button",
        "description": "Describing the animal teat condition. 'Button/undeveloped'- represents a barely visible or undeveloped test.",
        "uri": "https://linked.data.gov.au/def/nrm/b86a0b47-9a48-5371-a0b9-8161299395ce"
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "Button/un-developed",
        "description": "Describing the animal teat condition. 'Button/undeveloped'- represents a barely visible or undeveloped test.",
        "uri": "https://linked.data.gov.au/def/nrm/b86a0b47-9a48-5371-a0b9-8161299395ce"
      },
      {
        "id": 5,
        "symbol": "5",
        "label": "Unknown",
        "description": "Unknown/unable to be determined.",
        "uri": "https://linked.data.gov.au/def/nrm/f6b0f6d8-16d8-5dd7-b1b7-66b0c020b96f"
      }
    ],
    "lut-fauna-testes-condition": [
      {
        "id": 1,
        "symbol": "1",
        "label": "Scrotal",
        "description": "Scrotal sack obvious, may appear empty or full",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "Abdominal",
        "description": "Scrotum not obvious",
        "uri": ""
      }
    ],
    "lut-feature": [
      {
        "id": 1,
        "symbol": "BS",
        "label": "Bait station",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "BSPC",
        "label": "Bait station - pest control",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "PT",
        "label": "Pitfall trap",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "DF",
        "label": "Drift fence",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "CN",
        "label": "Carcass - natural",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "CP",
        "label": "Carcass - placed",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "RS",
        "label": "Road - sealed",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "RU",
        "label": "Road - unsealed",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "TU",
        "label": "Track - unsealed",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "TA",
        "label": "Trail - animal",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "TH",
        "label": "Trail - human",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "CL",
        "label": "Creekline",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "CB",
        "label": "Creek bank",
        "description": "",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "WN",
        "label": "Waterpoint - natural",
        "description": "",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "WI",
        "label": "Waterpoint - infrastructure",
        "description": "",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "HO",
        "label": "Habitat opening",
        "description": "",
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "SH",
        "label": "Suitabe habitat",
        "description": "",
        "uri": ""
      },
      {
        "id": 18,
        "symbol": "FA",
        "label": "Foraging area",
        "description": "",
        "uri": ""
      },
      {
        "id": 19,
        "symbol": "PP",
        "label": "Palatable plant",
        "description": "",
        "uri": ""
      },
      {
        "id": 20,
        "symbol": "FP",
        "label": "Flowering/fruiting plant",
        "description": "",
        "uri": ""
      },
      {
        "id": 21,
        "symbol": "THO",
        "label": "Tree hollow",
        "description": "",
        "uri": ""
      },
      {
        "id": 22,
        "symbol": "ST",
        "label": "Sign - tracks",
        "description": "",
        "uri": ""
      },
      {
        "id": 23,
        "symbol": "SSCA",
        "label": "Sign - scats",
        "description": "",
        "uri": ""
      },
      {
        "id": 24,
        "symbol": "SSCR",
        "label": "Sign - scratchings",
        "description": "",
        "uri": ""
      },
      {
        "id": 25,
        "symbol": "SB",
        "label": "Sign - burrow",
        "description": "",
        "uri": ""
      },
      {
        "id": 26,
        "symbol": "SN",
        "label": "Sign - nest",
        "description": "",
        "uri": ""
      },
      {
        "id": 27,
        "symbol": "SPR",
        "label": "Sign - prey remains",
        "description": "",
        "uri": ""
      },
      {
        "id": 28,
        "symbol": "RT",
        "label": "Ridge top",
        "description": "",
        "uri": ""
      },
      {
        "id": 29,
        "symbol": "RF",
        "label": "Rock face",
        "description": "",
        "uri": ""
      },
      {
        "id": 30,
        "symbol": "RP",
        "label": "Rock pile",
        "description": "",
        "uri": ""
      },
      {
        "id": 31,
        "symbol": "WPD",
        "label": "Wood pile/debris",
        "description": "",
        "uri": ""
      },
      {
        "id": 32,
        "symbol": "BU",
        "label": "Building",
        "description": "",
        "uri": ""
      },
      {
        "id": 33,
        "symbol": "BR",
        "label": "Bridge",
        "description": "",
        "uri": ""
      },
      {
        "id": 34,
        "symbol": "OU",
        "label": "Over/underpass",
        "description": "",
        "uri": ""
      },
      {
        "id": 35,
        "symbol": "CU",
        "label": "Culvert",
        "description": "",
        "uri": ""
      },
      {
        "id": 36,
        "symbol": "CT",
        "label": "Cork tile",
        "description": "",
        "uri": ""
      },
      {
        "id": 37,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-female-mammal-young-present": [
      {
        "id": 1,
        "symbol": "PYP",
        "label": "Pouch young present",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "NPY",
        "label": "No pouch young present",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "YOB",
        "label": "Young on back",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "U",
        "label": "Unknown",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "NA",
        "label": "Not applicable",
        "description": "",
        "uri": ""
      }
    ],
    "lut-fire-history": [
      {
        "id": 1,
        "symbol": "K",
        "label": "Known",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "U",
        "label": "Unknown",
        "description": "",
        "uri": ""
      }
    ],
    "lut-fire-regeneration-status": [
      {
        "id": 1,
        "symbol": "B",
        "label": "Basal",
        "description": "Resprouting arising from buds at or below ground level.",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "E",
        "label": "Epicormic",
        "description": "Resprouting arising from the apical bud.",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "A",
        "label": "Apical",
        "description": "Resprouting arising from buds in the stems and branches.",
        "uri": ""
      }
    ],
    "lut-floristics-habit": [
      {
        "id": 1,
        "symbol": "ASC",
        "label": "Ascending",
        "description": "Weakly erect",
        "uri": "https://linked.data.gov.au/def/nrm/d5bd18f1-8ed8-4b4e-a313-e96535598fc9"
      },
      {
        "id": 2,
        "symbol": "CLI",
        "label": "Climbing",
        "description": "Plants that climb upwards with the help of extrenal support",
        "uri": "https://linked.data.gov.au/def/nrm/786f935f-0b7d-4a72-b582-5bac740a14ac"
      },
      {
        "id": 3,
        "symbol": "CRE",
        "label": "Creeping",
        "description": "Spread and grow outward from the original plant",
        "uri": "https://linked.data.gov.au/def/nrm/cf37a9ea-25f7-4e7e-b29e-4248074ad72b"
      },
      {
        "id": 4,
        "symbol": "DEC",
        "label": "Decumbent",
        "description": "More or less horizontal with down-turned tips",
        "uri": "https://linked.data.gov.au/def/nrm/62eb2c61-92b4-4861-a3e6-2c876ba43073"
      },
      {
        "id": 5,
        "symbol": "ERC",
        "label": "Erect",
        "description": "Upright, perpendicular",
        "uri": "https://linked.data.gov.au/def/nrm/2d997343-4be2-411a-944a-7ef956e696a2"
      },
      {
        "id": 6,
        "symbol": "PEN",
        "label": "Pendulous",
        "description": "Hanging down loosely",
        "uri": "https://linked.data.gov.au/def/nrm/607b0b06-caa2-4882-86bd-d4da70b3b63c"
      },
      {
        "id": 7,
        "symbol": "PRC",
        "label": "Procumbent",
        "description": "More or less horizontal with up-turned tips",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "PRS",
        "label": "Prostrate",
        "description": "Completely flat on the substrate including trailing plants",
        "uri": "https://linked.data.gov.au/def/nrm/5459d7c1-b29f-4616-9572-f2f323520cfc"
      },
      {
        "id": 9,
        "symbol": "RHI",
        "label": "Rhizomatous",
        "description": "Plants that send out roots and shoots from its nodes",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "STR",
        "label": "Straggling",
        "description": "Leaning or lying over other plants",
        "uri": "https://linked.data.gov.au/def/nrm/e3bb5cd7-f56c-41bc-8a4f-e6627c37826e"
      },
      {
        "id": 11,
        "symbol": "SUC",
        "label": "Succulent",
        "description": "Fleshy, juicy, soft in texture and usually thickened",
        "uri": "https://linked.data.gov.au/def/nrm/cb6d4feb-aaef-42b7-b9d2-7edb9b5871bd"
      },
      {
        "id": 12,
        "symbol": "TUF",
        "label": "Tufted",
        "description": "Of many stems growing in clusters attached at the base",
        "uri": "https://linked.data.gov.au/def/nrm/df842727-5c00-4758-993e-3bb1558afa43"
      },
      {
        "id": 13,
        "symbol": "TWIN",
        "label": "Twinning",
        "description": "Climbing plants which wind themselves around supports",
        "uri": "https://linked.data.gov.au/def/nrm/8bf40f11-ec32-4119-a183-113fa04a4363"
      }
    ],
    "lut-floristics-phenology": [
      {
        "id": 1,
        "symbol": "FLO",
        "label": "Flowers",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "FRU",
        "label": "Fruit",
        "description": "The fleshy or dry ripened ovary of a plant, enclosing the seed or seeds.",
        "uri": "https://linked.data.gov.au/def/nrm/2ee1bcb5-6cc3-478d-b094-b042a53f381f"
      },
      {
        "id": 3,
        "symbol": "BUD",
        "label": "Buds",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "STE",
        "label": "Sterile",
        "description": "A fertility quality inhering in a bearer by virtue of the bearer's being incapable of initiating, sustaining, or supporting reproduction.",
        "uri": "https://linked.data.gov.au/def/nrm/9d100076-b85d-4b9d-b5b1-306ec82c2d0b"
      },
      {
        "id": 5,
        "symbol": "FER",
        "label": "Fertile",
        "description": "A fertility quality inhering in a bearer by virtue of the bearer's being capable of initiating, sustaining, or supporting reproduction.",
        "uri": "https://linked.data.gov.au/def/nrm/8d9a21a6-a162-4aa9-a09d-305ea0c21e7d"
      },
      {
        "id": 6,
        "symbol": "LEA",
        "label": "Leaf",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "ADU",
        "label": "Adult",
        "description": "Adult- refers to a maturity quality of an individual by virtue of the individual having attained sexual maturity and full growth. Also defined as, an adult is a plant, animal or person who has reached full growth or alternatively is capable of reproduction.",
        "uri": "https://linked.data.gov.au/def/nrm/436e8a65-505a-505b-811e-c6be4e4ae651"
      },
      {
        "id": 8,
        "symbol": "JUV",
        "label": "Juvenile",
        "description": "A juvenile is an individual organism that has not yet reached its adult form, sexual maturity or size. Juveniles sometimes look very different from the adult form, particularly in terms of their colour. In many organisms the juvenile has a different name from the adult.",
        "uri": "https://linked.data.gov.au/def/nrm/6de1f874-fdcb-520a-9ff8-481a7968e76c"
      },
      {
        "id": 9,
        "symbol": "SEE",
        "label": "Seedling",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "SEN",
        "label": "Senescent",
        "description": "",
        "uri": ""
      }
    ],
    "lut-fractional-cover-intercept": [
      {
        "id": 1,
        "symbol": "PV",
        "label": "Photosynthetic vegetation",
        "description": "Typically green leaves or other plant parts that are photosynthetically active.",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "NPV",
        "label": "Non-photosynthetic vegetation",
        "description": "Dried or dead leaves.",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "B",
        "label": "Branch",
        "description": "Woody component of the plant.",
        "uri": ""
      }
    ],
    "lut-ground-control-survey-time": [
      {
        "id": 1,
        "symbol": "Diurnal",
        "label": "Diurnal",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Nocturnal",
        "label": "Nocturnal",
        "description": "",
        "uri": ""
      }
    ],
    "lut-ground-count-transect-type": [
      {
        "id": 1,
        "symbol": "W",
        "label": "Walked",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "D",
        "label": "Driven",
        "description": "",
        "uri": ""
      }
    ],
    "lut-habitat": [
      {
        "id": 1,
        "symbol": "CD",
        "label": "Closed chenopod shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of Chenopodiaceae. ",
        "uri": "https://linked.data.gov.au/def/nrm/7cf0b84b-3277-5bfd-9fd0-0a799cda4588"
      },
      {
        "id": 2,
        "symbol": "CI",
        "label": "Isolated chenopod shrub",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "CL",
        "label": "Isolated clump of chenopod shrubs",
        "description": "Refers to the type of habitat characterised by isolated clumps of chenopod shrubs.",
        "uri": "https://linked.data.gov.au/def/nrm/1928d26f-10b6-5a61-84cb-feac377781b0"
      },
      {
        "id": 4,
        "symbol": "CM",
        "label": "Chenopod shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% members of Chenopodiaceae. ",
        "uri": "https://linked.data.gov.au/def/nrm/99807739-a552-5004-9105-6e580a123002"
      },
      {
        "id": 5,
        "symbol": "CS",
        "label": "Open chenopod shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of members of Chenopodiaceae.",
        "uri": "https://linked.data.gov.au/def/nrm/740661b3-4178-508a-a7a4-64f07c6c3a8a"
      },
      {
        "id": 6,
        "symbol": "CV",
        "label": "Sparse chenopod shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of members of Chenopodiaceae. ",
        "uri": "https://linked.data.gov.au/def/nrm/c8a4a268-54ef-5327-9530-3acb1c6bb048"
      },
      {
        "id": 7,
        "symbol": "DD",
        "label": "Closed sod grassland",
        "description": "Refers to the type of habitat representative of a characteristic sod-like (turf) grass.",
        "uri": "https://linked.data.gov.au/def/nrm/18d6c67c-5a1e-5146-a49a-a17615eb6656"
      },
      {
        "id": 8,
        "symbol": "DI",
        "label": "Isolated sod grasses",
        "description": "Refers to the type of habitat characterised by isolated or sparse sod or turf-like grasses.",
        "uri": "https://linked.data.gov.au/def/nrm/cf2c27f3-1f44-50cc-a382-4a18c5c24985"
      },
      {
        "id": 9,
        "symbol": "DL",
        "label": "Isolated clump of sod grasses",
        "description": "Refers to the type of habitat characterised by isolated clumps of sod grass.",
        "uri": "https://linked.data.gov.au/def/nrm/f2c496b6-ea23-546b-83f2-14ebb6f5ca82"
      },
      {
        "id": 10,
        "symbol": "DM",
        "label": "Sod grassland",
        "description": "Refers to the type of habitat characterised by mid-dense (30-70% cover) sod or turf-like grasses.",
        "uri": "https://linked.data.gov.au/def/nrm/585bbfcc-cb43-5d7a-9a58-1c90031300fd"
      },
      {
        "id": 11,
        "symbol": "DS",
        "label": "Open sod grassland",
        "description": "Refers to the type of habitat characterised by open or sparse (10-30% ground cover) of a characteristic sod-like (turf) grass.",
        "uri": "https://linked.data.gov.au/def/nrm/a0aa9bd3-3256-54f1-a5bf-746b3025743f"
      },
      {
        "id": 12,
        "symbol": "DV",
        "label": "Sparse sod grassland",
        "description": "Refers to the type of habitat characterised by very sparse (<10% cover) sod or turf-like grasses.",
        "uri": "https://linked.data.gov.au/def/nrm/a4fccf87-04df-5982-a89f-4aaf6b089777"
      },
      {
        "id": 13,
        "symbol": "ED",
        "label": "Closed fernland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of Fern and Fern-allies. ",
        "uri": "https://linked.data.gov.au/def/nrm/77dc385f-c9a0-56f3-b14e-4485f7adcb78"
      },
      {
        "id": 14,
        "symbol": "EI",
        "label": "Isolated ferns",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of fern and fern allies. ",
        "uri": "https://linked.data.gov.au/def/nrm/aa9b68be-a749-537e-baf9-6880088198cc"
      },
      {
        "id": 15,
        "symbol": "EL",
        "label": "Isolated clumps of ferns",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of Fern and Fern-allies. ",
        "uri": "https://linked.data.gov.au/def/nrm/e692ff93-02cd-5bdf-bc4e-8f76de59089a"
      },
      {
        "id": 16,
        "symbol": "EM",
        "label": "Fernland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% members of Fern and Fern-allies. ",
        "uri": "https://linked.data.gov.au/def/nrm/87e02dde-35a9-5ee3-b919-5bd34d83a2ff"
      },
      {
        "id": 17,
        "symbol": "ES",
        "label": "Open fernland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of ferns and fern allies. ",
        "uri": "https://linked.data.gov.au/def/nrm/5deef8a7-e536-57a9-9b7a-caf9f5d4d39d"
      },
      {
        "id": 18,
        "symbol": "EV",
        "label": "Sparse fernland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of members of fern and fern-allies. ",
        "uri": "https://linked.data.gov.au/def/nrm/30c6a39a-211b-5526-93b3-467bbb9e56b4"
      },
      {
        "id": 19,
        "symbol": "FD",
        "label": "Closed forbland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of Forbs or herbs other than grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/7a9329ca-a03d-5c22-9c3c-3ca03d038fb9"
      },
      {
        "id": 20,
        "symbol": "FI",
        "label": "Isolated forbs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of forbs or herbs other than grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/f03afa59-394c-50e6-ba58-3b7dc38a1363"
      },
      {
        "id": 21,
        "symbol": "FL",
        "label": "Isolated clumps of forbs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of Forbs or herbs other than grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/37b756c2-5607-58de-a31b-eb5b1ce626ed"
      },
      {
        "id": 22,
        "symbol": "FM",
        "label": "Forbland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% members of Forbs or herbaceous plants other than grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/6f68413c-4dcd-5d02-a0c7-0ad3a56d62d3"
      },
      {
        "id": 23,
        "symbol": "FS",
        "label": "Open forbland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of forbs or herbs other than grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/32532a0a-3b85-5ebd-b72e-2b87666baf62"
      },
      {
        "id": 24,
        "symbol": "FV",
        "label": "Sparse forbland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of members of forbs and herbs other than grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/bff147ed-4af8-50d5-be4e-b2d817686767"
      },
      {
        "id": 25,
        "symbol": "GD",
        "label": "Closed tussock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of tussock grasses (e.g., Poa).",
        "uri": "https://linked.data.gov.au/def/nrm/2879f1a4-9a89-5faa-b43c-ae04aeeb9d23"
      },
      {
        "id": 26,
        "symbol": "GI",
        "label": "Isolated tussock grasses",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of tussock grass (e.g. Poa species). ",
        "uri": "https://linked.data.gov.au/def/nrm/d958b180-ff63-56c5-a39a-4d04bd07207b"
      },
      {
        "id": 27,
        "symbol": "GL",
        "label": "Isolated clump of tussock grasses",
        "description": "Refers to the type of habitat characterised by isolated clumps of tussock grasses (e.g., Poa spp).",
        "uri": "https://linked.data.gov.au/def/nrm/f51c20dd-6026-5b24-9f5f-a705b8d88a95"
      },
      {
        "id": 28,
        "symbol": "GM",
        "label": "Tussock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of tussock grass (e.g., Poa species). ",
        "uri": "https://linked.data.gov.au/def/nrm/d474a334-fb65-5496-917c-7f879697157e"
      },
      {
        "id": 29,
        "symbol": "GS",
        "label": "Open tussock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of tussock grasses (e.g. Poa species).",
        "uri": "https://linked.data.gov.au/def/nrm/021ac7e0-af3f-5c5f-9835-6566cd7d45dd"
      },
      {
        "id": 30,
        "symbol": "GV",
        "label": "Sparse tussock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of tussock grass (e.g., Poa species). ",
        "uri": "https://linked.data.gov.au/def/nrm/eba388b7-bf44-5c65-b468-aebb1afbc5e2"
      },
      {
        "id": 31,
        "symbol": "HD",
        "label": "Closed hummock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of hummock (e.g., Triodia) grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/5a0039b6-1652-59cf-95ba-0ea41b7626e8"
      },
      {
        "id": 32,
        "symbol": "HI",
        "label": "Isolated hummock grass",
        "description": "",
        "uri": ""
      },
      {
        "id": 33,
        "symbol": "HL",
        "label": "Isolated clump of hummock grasses",
        "description": "Refers to the type of habitat characterised by isolated clumps of hummocky grass (e.g., Triodia spp., Spinifex spp.).",
        "uri": "https://linked.data.gov.au/def/nrm/f62e2bf1-408f-536e-9da1-82d7e980498c"
      },
      {
        "id": 34,
        "symbol": "HM",
        "label": "Hummock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% members of hummock grasses (e.g., Triodia). ",
        "uri": "https://linked.data.gov.au/def/nrm/b77c7b89-0dd2-52cb-9eb3-7319d4465cfe"
      },
      {
        "id": 35,
        "symbol": "HS",
        "label": "Open hummock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of hummock grasses (e.g., Triodia). ",
        "uri": "https://linked.data.gov.au/def/nrm/9c0fd421-510f-56f5-92e6-e18ceb62419d"
      },
      {
        "id": 36,
        "symbol": "HV",
        "label": "Sparse grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/e28adc38-5e40-5795-93d1-6582d0013f7e"
      },
      {
        "id": 37,
        "symbol": "LD",
        "label": "Closed vineland",
        "description": "Refers to the type of habitat represented by a closed vegetation dominated by stragglers and woody climbers.",
        "uri": "https://linked.data.gov.au/def/nrm/49b4614a-b1ad-5ba3-8157-f884b61d09c5"
      },
      {
        "id": 38,
        "symbol": "LI",
        "label": "Isolated vines",
        "description": "Refers to the type of habitat characterised by isolated or sparse stragglers or climbing woody vines.",
        "uri": "https://linked.data.gov.au/def/nrm/d42312a8-1e09-5dc4-ae32-df980671e7d7"
      },
      {
        "id": 39,
        "symbol": "LL",
        "label": "Isolated clump of vines",
        "description": "Refers to the type of habitat characterised by isolated clumps of vines.",
        "uri": "https://linked.data.gov.au/def/nrm/f8c0c37c-3021-5f90-bb23-658966b5326d"
      },
      {
        "id": 40,
        "symbol": "LM",
        "label": "Vineland",
        "description": "Refers to the type of habitat characterised by woody climbers/straggling vines. ",
        "uri": "https://linked.data.gov.au/def/nrm/329d8b4e-fb69-5786-af63-8fc92ec018f7"
      },
      {
        "id": 41,
        "symbol": "LS",
        "label": "Open vineland",
        "description": "Refers to the type of habitat represented by a closed vegetation dominated by stragglers and woody climbers.",
        "uri": "https://linked.data.gov.au/def/nrm/4bc3fcb1-26c6-5b11-93df-9381fa12282e"
      },
      {
        "id": 42,
        "symbol": "LV",
        "label": "Sparse vineland",
        "description": "Refers to the type of habitat characterised by well separated or very sparse crown stragglers or woody vines. ",
        "uri": "https://linked.data.gov.au/def/nrm/f20b4449-da26-5423-ad11-eeed042e4072"
      },
      {
        "id": 43,
        "symbol": "MD",
        "label": "Closed mallee forest",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of tree mallee (e.g., some members of Eucalyptus). ",
        "uri": "https://linked.data.gov.au/def/nrm/36a6499f-b1f6-5405-a2ec-73498ac874eb"
      },
      {
        "id": 44,
        "symbol": "MI",
        "label": "Isolated mallee trees",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of tree mallee (e.g., some multistemmed individuals from base of Eucalyptus). ",
        "uri": "https://linked.data.gov.au/def/nrm/8c26e6d6-6287-5139-89c3-03c730b544e0"
      },
      {
        "id": 45,
        "symbol": "ML",
        "label": "Isolated clump of mallee trees",
        "description": "Refers to the type of habitat characterised by isolated clumps of tree mallee (members of Eucalyptus spp., multistemmed from base).",
        "uri": "https://linked.data.gov.au/def/nrm/541a8566-33f2-5998-af10-c89046b17a1c"
      },
      {
        "id": 46,
        "symbol": "MM",
        "label": "Open mallee forest",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of tree Mallee (e.g., certain individuals of Eucalypts multistemmed from base). ",
        "uri": "https://linked.data.gov.au/def/nrm/3f0f6d0b-e136-5cad-bd74-f229aeb6fe04"
      },
      {
        "id": 47,
        "symbol": "MS",
        "label": "Mallee woodland",
        "description": "Refers to the dominant vegetation structural formation, with a percent cover of about 20-50% of Tree Mallee. ",
        "uri": "https://linked.data.gov.au/def/nrm/3de3bfd1-7d1c-5656-8439-85f56f487c40"
      },
      {
        "id": 48,
        "symbol": "MV",
        "label": "Open mallee woodland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of tree mallee (e.g., certain individuals of Eucalypts multistemmed from base). ",
        "uri": "https://linked.data.gov.au/def/nrm/484d51eb-c1ee-597c-aed0-664a28e7b1d1"
      },
      {
        "id": 49,
        "symbol": "ND",
        "label": "Closed lichenland",
        "description": "Refers to the type of habitat characterised by lichenised tree trunks and rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/936bf21a-191e-50c2-a767-695931abefeb"
      },
      {
        "id": 50,
        "symbol": "NI",
        "label": "Isolated lichens",
        "description": "Refers to the type of habitat characterised by isolated or sparse lichens.",
        "uri": "https://linked.data.gov.au/def/nrm/69a2aa31-2a87-5306-87e0-baa53745032b"
      },
      {
        "id": 51,
        "symbol": "NL",
        "label": "Isolated clup of lichens",
        "description": "Refers to the type of habitat characterised by isolated clumps of lichens.",
        "uri": "https://linked.data.gov.au/def/nrm/f5a80ba0-7b0b-51ee-bfd3-8e96199e8298"
      },
      {
        "id": 52,
        "symbol": "NM",
        "label": "Lichenland",
        "description": "Refers to the type of habitat predominated by lichens on rocks, trees or tree stumps, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/700fd743-4b00-59cf-87fd-a2bb56d93002"
      },
      {
        "id": 53,
        "symbol": "NS",
        "label": "Open lichenland",
        "description": "Refers to the type of habitat represented by open or sparse (i.e., 10-30%) hummocky grasses (e.g., Spinifex spp., Triodia spp.).",
        "uri": "https://linked.data.gov.au/def/nrm/5291eed4-b125-51fc-bf5a-6deff5d3e0d8"
      },
      {
        "id": 54,
        "symbol": "NV",
        "label": "Sparse lichenland",
        "description": "Refers to the type of habitat characterised by very sparse (<10% cover) lichens.",
        "uri": "https://linked.data.gov.au/def/nrm/3a884e64-9ead-5a93-96af-594709e77fd4"
      },
      {
        "id": 55,
        "symbol": "OA",
        "label": "Beach",
        "description": "Type of Landform Element, which is usually short; low; very wide slope; gently or moderately inclined; built up or eroded by waves; forming the shore of a lake or sea.",
        "uri": "https://linked.data.gov.au/def/nrm/63960cfe-9e04-52b7-8940-4d6c2ebe0a02"
      },
      {
        "id": 56,
        "symbol": "OB",
        "label": "Billabong or Swamp",
        "description": "A swamp is a wetland that features temporary or permanent inundation of large areas of land by shallow bodies of water, generally with a substantial number of hammocks, or dry-land protrusions, and covered by aquatic vegetation, or vegetation that tolerates periodical inundation.",
        "uri": "https://linked.data.gov.au/def/nrm/55f988ef-b922-5ae6-a23e-748aefd62d21"
      },
      {
        "id": 57,
        "symbol": "OC",
        "label": "Coastal Waters",
        "description": "Refers to the type of habitat representative of an aquatic body typically characterized by a shallow continental shelf, gently sloping seaward to a continental slope, which drops relatively abruptly to the deep ocean. ",
        "uri": "https://linked.data.gov.au/def/nrm/09b57b74-ee2d-5270-8e32-4368989eac08"
      },
      {
        "id": 58,
        "symbol": "OE",
        "label": "Estuary",
        "description": "Type of Landform Element which has a stream channel close to its junction with a sea or lake; where the action of channelled stream flow is modified by tide and waves. The width typically increases downstream.",
        "uri": "https://linked.data.gov.au/def/nrm/7cb2f197-c3e8-5491-b0db-78b839405fd7"
      },
      {
        "id": 59,
        "symbol": "OF",
        "label": "Freshwater Lake",
        "description": "Refers to the type of habitat representative of an enclosed aquatic body having a relatively low mineral content, generally less than 500 mg/l of dissolved solids.",
        "uri": "https://linked.data.gov.au/def/nrm/9f69fdbd-4d86-5116-8822-8c79ee64702e"
      },
      {
        "id": 60,
        "symbol": "OG",
        "label": "Grazing Land",
        "description": "Refers to the type of habitat representative of a land predominantly used for grazing.",
        "uri": "https://linked.data.gov.au/def/nrm/4f1acfe6-f8bc-5318-ba80-d40402109504"
      },
      {
        "id": 61,
        "symbol": "OL",
        "label": "Saltwater Lake",
        "description": "Refers to the type of habitat representative of an aquatic body filled with water (with high salinity) of considerable size contained in a depression on a landmass.",
        "uri": "https://linked.data.gov.au/def/nrm/1b65f313-89bd-57ac-8506-07cfdc72e757"
      },
      {
        "id": 62,
        "symbol": "OM",
        "label": "Mudflat",
        "description": "Refers to the type of habitat characterised by a wetland that forms when mud is deposited by the tides, rivers, sea or oceans.",
        "uri": "https://linked.data.gov.au/def/nrm/3e8b4e76-de24-557c-93c0-a71219a0931b"
      },
      {
        "id": 63,
        "symbol": "OO",
        "label": "Open Ocean",
        "description": "Refers to the type of habitat surrounded by ocean, i.e., a continuous saline-water bodies that surround the continents and fill the Earth's great depressions. ",
        "uri": "https://linked.data.gov.au/def/nrm/b17ea99e-d037-5f41-adc1-1323fe3fbdce"
      },
      {
        "id": 64,
        "symbol": "OP",
        "label": "Crop Land",
        "description": "Refers to the type of habitat representative of a cultivated land or land on which agricultural crops are grown or land that is set aside or temporarily not being used for crop production.",
        "uri": "https://linked.data.gov.au/def/nrm/c742f5dd-15e7-5212-8304-a34a46b93d78"
      },
      {
        "id": 65,
        "symbol": "OR",
        "label": "Rock Outcrop",
        "description": "Refers to the type of habitat characterised by rocks, which protrudes through the surface layer.",
        "uri": "https://linked.data.gov.au/def/nrm/dc4aeb3c-5a67-5232-89d3-9ee250cf3cda"
      },
      {
        "id": 66,
        "symbol": "OS",
        "label": "Stream or River",
        "description": "Refers to the type of habitat representative of an aquatic body with a watercourse which is linear and flows across the solid portion of a planetary surface.",
        "uri": "https://linked.data.gov.au/def/nrm/353214cb-0cda-55f9-9e5e-e3c0bd8dd07c"
      },
      {
        "id": 67,
        "symbol": "OU",
        "label": "Urban",
        "description": "Refers to the type of habitat relating to, located in, or characteristic of a city or densely populated area.",
        "uri": "https://linked.data.gov.au/def/nrm/6fb7d262-c3d5-5b65-baf0-3fd82fa924a7"
      },
      {
        "id": 68,
        "symbol": "OV",
        "label": "Cave",
        "description": "The type of habitat representative of a naturally formed, subterranean open area or chamber.",
        "uri": "https://linked.data.gov.au/def/nrm/03ee0064-f289-52ae-91a8-ed0aace9a1f2"
      },
      {
        "id": 69,
        "symbol": "RD",
        "label": "Closed rushland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of Rushes (e.g., Juncaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/230bb275-6421-539b-9852-07a5f76c660f"
      },
      {
        "id": 70,
        "symbol": "RI",
        "label": "Isolated rushes",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of rushes (e.g., Juncaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/3a1e4a07-df4a-5a18-9f61-72f7c87306f5"
      },
      {
        "id": 71,
        "symbol": "RL",
        "label": "Isolated clump of rushes",
        "description": "Refers to the type of habitat characterised by isolated clumps of rushes.",
        "uri": "https://linked.data.gov.au/def/nrm/9a309ff4-8499-5bf4-92cc-8d394a53e6b9"
      },
      {
        "id": 72,
        "symbol": "RM",
        "label": "Rushland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of rushes (e.g. Juncaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/3de9060b-389c-5340-9e1c-288719d05007"
      },
      {
        "id": 73,
        "symbol": "RS",
        "label": "Open rushland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of rushes (e.g. Juncaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/4e22f9a2-1bc6-53ba-8226-e18fef762fb9"
      },
      {
        "id": 74,
        "symbol": "RV",
        "label": "Sparse rushland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of rushes (e.g., Juncaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/3c0c31d0-7360-55cd-bd02-5e2a2e47b11b"
      },
      {
        "id": 75,
        "symbol": "SD",
        "label": "Closed shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of sedges (e.g., Cyperaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/97fa02b0-f5c2-5cae-a822-aa0385e299b7"
      },
      {
        "id": 76,
        "symbol": "SI",
        "label": "Isolated shrubs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of shrubs including cycads, grass-tree and tree-fern. ",
        "uri": "https://linked.data.gov.au/def/nrm/97d1820d-545f-5694-b379-9f360b8d123b"
      },
      {
        "id": 77,
        "symbol": "SL",
        "label": "Isolated clump of shrubs",
        "description": "Refers to the type of habitat characterised by isolated clumps of shrubs.",
        "uri": "https://linked.data.gov.au/def/nrm/3d57e6c5-e288-56c2-89c3-1d1f8abd5847"
      },
      {
        "id": 78,
        "symbol": "SM",
        "label": "Shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of shrubs (e.g., shrub, cycad, grass-tree, tree-fern).",
        "uri": "https://linked.data.gov.au/def/nrm/18e5a444-f39c-50ea-84a5-9f677ee46601"
      },
      {
        "id": 79,
        "symbol": "SS",
        "label": "Open shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of shrubs (e.g. shrubs, cycads, grass-tree, tree-fern).",
        "uri": "https://linked.data.gov.au/def/nrm/c5fb3713-be56-56e3-8fb7-922c27dd2ae9"
      },
      {
        "id": 80,
        "symbol": "SV",
        "label": "Sparse shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of shrubs, including cycad, grass-tree, tree-fern. ",
        "uri": "https://linked.data.gov.au/def/nrm/7ecfb199-be34-5656-887b-c6205faf0cd1"
      },
      {
        "id": 81,
        "symbol": "TD",
        "label": "Closed forest",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of Forbs or herbs other than grassess. ",
        "uri": "https://linked.data.gov.au/def/nrm/ac2238c8-b7bc-5a89-8cc3-774bc3a53064"
      },
      {
        "id": 82,
        "symbol": "TI",
        "label": "Isolated trees",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of trees including palms. ",
        "uri": "https://linked.data.gov.au/def/nrm/9935b7bc-2bfa-5b34-ae38-12da54c24bbc"
      },
      {
        "id": 83,
        "symbol": "TL",
        "label": "Isolated clump of trees",
        "description": "Refers to the type of habitat characterised by isolated clumps of trees.",
        "uri": "https://linked.data.gov.au/def/nrm/7e8f9627-8796-5e1e-ad72-33f2a863de98"
      },
      {
        "id": 84,
        "symbol": "TM",
        "label": "Open forest",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of trees including palms. ",
        "uri": "https://linked.data.gov.au/def/nrm/9b644ae5-6279-5794-8535-b56cef440f3d"
      },
      {
        "id": 85,
        "symbol": "TS",
        "label": "Woodland",
        "description": "Refers to the type of habitat characterised by a low-density forest forming open habitats with plenty of sunlight and limited shade.",
        "uri": "https://linked.data.gov.au/def/nrm/e2193542-9203-532b-99d3-853a4640114d"
      },
      {
        "id": 86,
        "symbol": "TV",
        "label": "Open woodland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of trees including palms.",
        "uri": "https://linked.data.gov.au/def/nrm/09fdd99f-e627-542c-afed-824af660bf48"
      },
      {
        "id": 87,
        "symbol": "VD",
        "label": "Closed sedgeland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of sedges (e.g., Cyperaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/193d73c9-b3e8-59b5-860e-109eae4efd16"
      },
      {
        "id": 88,
        "symbol": "VI",
        "label": "Isolated sedges",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of sedges (e.g., Cyperaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/150a361f-0b85-59da-93d0-65206b6eae9e"
      },
      {
        "id": 89,
        "symbol": "VL",
        "label": "Isolated clump of sedges",
        "description": "Refers to the type of habitat characterised by isolated clumps of sedges.",
        "uri": "https://linked.data.gov.au/def/nrm/032937cf-f227-570d-a20d-9e2e04c27be8"
      },
      {
        "id": 90,
        "symbol": "VM",
        "label": "Sedgeland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of sedges (e.g., Cyperaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/6e866ca0-1492-5e47-a520-8bac223f2dc8"
      },
      {
        "id": 91,
        "symbol": "VS",
        "label": "Open sedgeland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of sedges (e.g. Cyperaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/1f71cccb-10a3-5dad-a1f1-2007c1c670c3"
      },
      {
        "id": 92,
        "symbol": "VV",
        "label": "Sparse sedgeland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of sedges (e.g., Cyperaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/864f2e87-9c4c-59c5-936d-557ac7942996"
      },
      {
        "id": 93,
        "symbol": "WD",
        "label": "Closed liverwortland",
        "description": "Refers to the type of habitat characterised by lower plant groups such as moss, liverworts and bryophytes.",
        "uri": "https://linked.data.gov.au/def/nrm/b190259a-9301-55bd-bf66-07d98a0d9af6"
      },
      {
        "id": 94,
        "symbol": "WI",
        "label": "Isolated liverworts",
        "description": "Refers to the type of habitat characterised by isolated or sparse liverworts.",
        "uri": "https://linked.data.gov.au/def/nrm/d228dc9f-9e51-567b-8fde-f3bac5f57265"
      },
      {
        "id": 95,
        "symbol": "WL",
        "label": "Isolated clump of liverworts",
        "description": "Refers to the type of habitat characterised by isolated clumps of bryophytes, moss and liverworts.",
        "uri": "https://linked.data.gov.au/def/nrm/2aad6ae5-d64f-55b7-a394-a90394c30d29"
      },
      {
        "id": 96,
        "symbol": "WM",
        "label": "Liverwortland",
        "description": "Refers to the type of habitat predominated by liverworts.",
        "uri": "https://linked.data.gov.au/def/nrm/1ad6b767-f8f0-51ed-8430-50819e0ad899"
      },
      {
        "id": 97,
        "symbol": "WS",
        "label": "Open liverwortland",
        "description": "Refers to the type of habitat characterised by open or sparse lichenised tree trunks and rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/30d4c6a6-5558-5fd3-bed3-487c6fd7d015"
      },
      {
        "id": 98,
        "symbol": "WV",
        "label": "Sparse liverwortland",
        "description": "Refers to the type of habitat characterised by very sparse (<10% cover) liverworts.",
        "uri": "https://linked.data.gov.au/def/nrm/64c1dc43-cde9-55b4-b1bb-fe58a340e20a"
      },
      {
        "id": 99,
        "symbol": "XD",
        "label": "Closed mossland",
        "description": "Refers to the type of habitat characterised by lower plant groups such as moss, liverworts and bryophytes.",
        "uri": "https://linked.data.gov.au/def/nrm/44f77309-6690-5e04-a873-7418632d1420"
      },
      {
        "id": 100,
        "symbol": "XI",
        "label": "Isolated mosses",
        "description": "Refers to the type of habitat characterised by isolated mosses, including bryophytes and liverworts.",
        "uri": "https://linked.data.gov.au/def/nrm/3f630070-deb6-596f-92ac-9906008d0f07"
      },
      {
        "id": 101,
        "symbol": "XL",
        "label": "Isolated clump of mosses",
        "description": "Refers to the type of habitat characterised by isolated clumps of bryophytes, moss and liverworts.",
        "uri": "https://linked.data.gov.au/def/nrm/4f1dc06e-910b-50a9-890a-d8d7ba3665b9"
      },
      {
        "id": 102,
        "symbol": "XM",
        "label": "Mossland",
        "description": "Refers to the type of habitat dominated by mosses.",
        "uri": "https://linked.data.gov.au/def/nrm/9ad6291c-900f-5538-af2d-6e241b0264ba"
      },
      {
        "id": 103,
        "symbol": "XS",
        "label": "Open mossland",
        "description": "Refers to the type of habitat characterised by open or sparse members of lower plant groups such as moss, liverworts and bryophytes.",
        "uri": "https://linked.data.gov.au/def/nrm/06328712-eb21-5ed1-8ac3-91d4e753deff"
      },
      {
        "id": 104,
        "symbol": "XV",
        "label": "Sparse mossland",
        "description": "Refers to the type of habitat characterised by very sparse (<10% cover) mosses.",
        "uri": "https://linked.data.gov.au/def/nrm/dacaf8da-6064-5374-89c5-01606b54d774"
      },
      {
        "id": 105,
        "symbol": "YD",
        "label": "Closed mallee shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of mallee shrubs (e.g., some members of Eucalyptus). ",
        "uri": "https://linked.data.gov.au/def/nrm/c751ff0d-e9a4-5a3d-bf46-27fbb352a1e4"
      },
      {
        "id": 106,
        "symbol": "YI",
        "label": "Isolated mallee shrubs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of mallee shrubs (e.g., some multistemmed individuals from base of Eucalyptus). ",
        "uri": "https://linked.data.gov.au/def/nrm/02056871-964d-5bc5-b5eb-dcd4c761f0ff"
      },
      {
        "id": 107,
        "symbol": "YL",
        "label": "Isolated clump of mallee shrubs",
        "description": "Refers to the type of habitat characterised by isolated clumps of mallee shrubs (members of Eucalyptus spp., multistemmed from base).",
        "uri": "https://linked.data.gov.au/def/nrm/d2f7baae-9238-5978-972c-179fa11e39ad"
      },
      {
        "id": 108,
        "symbol": "YM",
        "label": "Mallee shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of shrub mallee (e.g., individuals of some Eucalypts multistemmed from base). ",
        "uri": "https://linked.data.gov.au/def/nrm/f5e68c19-0e4c-5aa9-8076-7879f2a6cce4"
      },
      {
        "id": 109,
        "symbol": "YS",
        "label": "Open mallee shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of Mallee shrubs (e.g., certain individuals of Eucalypts multistemmed from base). ",
        "uri": "https://linked.data.gov.au/def/nrm/9baa87b2-346a-595c-bcb9-f50a2e7101f2"
      },
      {
        "id": 110,
        "symbol": "YV",
        "label": "Sparse mallee shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of members of shrub Mallee. ",
        "uri": "https://linked.data.gov.au/def/nrm/3c2c0ce9-5d47-5e85-952d-1e8584d9d9d0"
      },
      {
        "id": 111,
        "symbol": "ZD",
        "label": "Closed heathland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of heath shrubs (e.g., members of Ericaceae, Myrtaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/c1781447-eaa9-534a-8d3a-4935cc0ab29e"
      },
      {
        "id": 112,
        "symbol": "ZI",
        "label": "Isolated heath shrub",
        "description": "",
        "uri": ""
      },
      {
        "id": 113,
        "symbol": "ZL",
        "label": "Isolated clump of heath shrubs",
        "description": "Refers to the type of habitat characterised by isolated clumps of heath or heath-like shrubs.",
        "uri": "https://linked.data.gov.au/def/nrm/e1dd0210-be1b-548e-aa4b-5567d973a9f5"
      },
      {
        "id": 114,
        "symbol": "ZM",
        "label": "Heathland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% members of Heath (e.g., Ericaceae, Myrtaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/517d2f98-9d24-5b68-b8d8-f5393eba310a"
      },
      {
        "id": 115,
        "symbol": "ZS",
        "label": "Open heath",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of heaths (e.g., Ericaceae, Myrtaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/c904cc78-d7f8-5645-9d28-632af88c1d98"
      },
      {
        "id": 116,
        "symbol": "ZV",
        "label": "Sparse heath",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of members of heath (e.g., Ericaceae, Myrtaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/8a8ac1b4-3906-58ea-be1f-9bc51c2d17f1"
      }
    ],
    "lut-hapd-number-of-transect": [
      {
        "id": 1,
        "symbol": "1",
        "label": "1",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "2",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "4",
        "label": "4",
        "description": "",
        "uri": ""
      }
    ],
    "lut-hapd-plot-size": [
      {
        "id": 1,
        "symbol": "S",
        "label": "40x40m subplot",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "P",
        "label": "100x100m plot",
        "description": "",
        "uri": ""
      }
    ],
    "lut-herbivory-or-damage": [
      {
        "id": 1,
        "symbol": "N",
        "label": "No herbivory or damage encountered",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "H",
        "label": "Herbivory encountered",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "D",
        "label": "Physical damage encountered",
        "description": "",
        "uri": ""
      }
    ],
    "lut-horizontal-resolution": [
      {
        "id": 1,
        "symbol": "LT1",
        "label": "< 1 meter",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "1TO30",
        "label": "1 meter - < 30 meters",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "30TO100",
        "label": "30 meters - < 100 meters",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "100TO250",
        "label": "100 meters - < 250 meters",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "250TO500",
        "label": "250 meters - < 500 meters",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "500TO1K",
        "label": "500 meters - < 1 km",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "1KTO10K",
        "label": "1 km - < 10 km or approximately .01 degree - < .09 degree",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "10KTO50K",
        "label": "10 km - < 50 km or approximately .09 degree - < .5 degree",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "50KTO100K",
        "label": "50 km  - < 100 km or approximately .5 degree - < 1 degree",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "100KTO250K",
        "label": "100 km - < 250 km or approximately 1 degree - < 2.5 degrees",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "250KTO500K",
        "label": "250 km - <  500 km  or approximately  2.5 degrees - < 5.0 degrees",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "500KTO1000K",
        "label": "500 km - < 1000 km or approximately 5 degrees - < 10 degrees",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "GT1000K",
        "label": "> 1000 km or > 10 degrees",
        "description": "",
        "uri": ""
      }
    ],
    "lut-intervention-access-control-type": [
      {
        "id": 1,
        "symbol": "B",
        "label": "Broadwalks",
        "description": "Broadwalks are physical walkways built to control access during an intervention activity.",
        "uri": "https://linked.data.gov.au/def/nrm/4b45361a-8733-5954-9565-3e25edeab131"
      },
      {
        "id": 2,
        "symbol": "BB",
        "label": "Bollards and Barriers",
        "description": "Bollards and Barriers are physical structures used to control access during an intervention activity.",
        "uri": "https://linked.data.gov.au/def/nrm/71650354-5c83-5020-b177-8b9b240becea"
      },
      {
        "id": 3,
        "symbol": "CPB",
        "label": "Constructed Parking Bays",
        "description": "Areas designated for parking and used for access control during an intervention activity.",
        "uri": "https://linked.data.gov.au/def/nrm/3a0fabca-e778-52a6-ac65-d3ad79e82156"
      },
      {
        "id": 4,
        "symbol": "FSGG",
        "label": "Fencing, Styles, Gates and Grids",
        "description": "Physical structures used to control access during an intervention activity.",
        "uri": "https://linked.data.gov.au/def/nrm/b480b93e-4bde-5cbd-8e80-625119d391fc"
      },
      {
        "id": 5,
        "symbol": "FTW",
        "label": "Formed Traffic Ways",
        "description": "A pre-defined path used to direct traffic to control access of a particular site during an intervention activity.",
        "uri": "https://linked.data.gov.au/def/nrm/7e953a89-0e19-5458-956b-c549fbfc8c2f"
      },
      {
        "id": 6,
        "symbol": "S",
        "label": "Signage",
        "description": "A sign, symbol or collection of information used for identification or as a means of giving directions or warning about something.",
        "uri": "https://linked.data.gov.au/def/nrm/735f8fde-1816-56ca-a00e-cb4fec9de72d"
      },
      {
        "id": 7,
        "symbol": "O",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      }
    ],
    "lut-intervention-breeding-technique": [
      {
        "id": 1,
        "symbol": "SBCB",
        "label": "Seed Bank or Captive Breeding",
        "description": "Refers to the type of breeding technique implemented during an intervention activity.  Seed bank is a repository of seeds maintained under optimal conditions that ensure long term storage, intended for use as a source of replenishment of seed stock or plant genetic material. Captive breeding involves the breeding of specimens of a plant or animal under ex-situ conditions from the parent stock.",
        "uri": "https://linked.data.gov.au/def/nrm/8a0587b5-6eef-5fd6-b3d6-4df0eec4eadd"
      },
      {
        "id": 2,
        "symbol": "SO",
        "label": "Seed Orchard",
        "description": "The type of breeding technique. An intentional planting of trees or shrubs maintained for food, typically fruit, production.",
        "uri": "https://linked.data.gov.au/def/nrm/db0e038f-46a4-5a4d-a79f-d652b7cbdbb2"
      },
      {
        "id": 3,
        "symbol": "SN",
        "label": "Seed Nursery",
        "description": "The type of breeding technique, which involves a collection of seeds, seedlings or saplings that are nurtured under controlled environmental settings.",
        "uri": "https://linked.data.gov.au/def/nrm/838ad921-8017-5a27-9256-3f5234a0424c"
      },
      {
        "id": 4,
        "symbol": "P",
        "label": "Propagation",
        "description": "Refers to the type of breeding technique, i.e., involving the breeding of specimens of a plant or animal by natural processes from the parent stock.",
        "uri": "https://linked.data.gov.au/def/nrm/d5af36fe-86f6-5128-8767-04ccb3cc9769"
      },
      {
        "id": 5,
        "symbol": "SVT",
        "label": "Seed Viability Testing",
        "description": "The type of breeding technique which involves testing the viability or fitness of seed/s in a collection.",
        "uri": "https://linked.data.gov.au/def/nrm/dc69d9bb-3153-5f9c-ae21-503a3c58f3b0"
      },
      {
        "id": 6,
        "symbol": "SGT",
        "label": "Seed Genetic Testing",
        "description": "The type of breeding technique. The process of isolating and testing the DNA of an embryo to detect the presence of genetic alterations or defects that may predispose to the future development of a disease or disorder.",
        "uri": "https://linked.data.gov.au/def/nrm/79bd8494-55c4-5ba7-ae67-a45537836710"
      },
      {
        "id": 7,
        "symbol": "FCBP",
        "label": "Fauna Captive Breeding Program",
        "description": "Refers to the type of breeding technique, i.e., involving captive fauna, implemented during an intervention activity.",
        "uri": "https://linked.data.gov.au/def/nrm/6a321728-83e7-5492-b439-ecf1468dbdfa"
      },
      {
        "id": 8,
        "symbol": "FWBP",
        "label": "Fauna Wild Breeding Program",
        "description": "Refers to the type of breeding program, i.e., involving fauna in wild, implemented during an intervention activity.",
        "uri": "https://linked.data.gov.au/def/nrm/bdedc21c-0177-5e52-bba3-381bfde8e714"
      },
      {
        "id": 9,
        "symbol": "O",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      }
    ],
    "lut-intervention-debris-removal-type": [
      {
        "id": 1,
        "symbol": "BIW",
        "label": "Building and Industrial Waste",
        "description": "Refers to the type of waste products generated from a building or an industry. ",
        "uri": "https://linked.data.gov.au/def/nrm/0b482077-636a-540c-9676-ebf55e639b94"
      },
      {
        "id": 2,
        "symbol": "DW",
        "label": "Domestic Waste",
        "description": "Refers to the type of waste products generated from domestic activities.",
        "uri": "https://linked.data.gov.au/def/nrm/e13ca35a-a74e-5ccd-a3fd-ff984d06a665"
      },
      {
        "id": 3,
        "symbol": "GW",
        "label": "Green Waste",
        "description": "Refers to the type of waste products of an organic source (e.g., plant, plant parts, etc.).",
        "uri": "https://linked.data.gov.au/def/nrm/7a1bc7e1-c6d0-5817-85f6-5566f24986fd"
      },
      {
        "id": 4,
        "symbol": "O",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      }
    ],
    "lut-intervention-disease-management-or-treatment-type": [
      {
        "id": 1,
        "symbol": "BCA",
        "label": "Biological Control Agents",
        "description": "The type of treatment implemented for disease management. Biological or biologically derived pest control agents that are based on microorganisms or natural products. They include naturally occurring and genetically engineered substances. ",
        "uri": "https://linked.data.gov.au/def/nrm/bef6706e-ddb4-551e-9989-4234c35c8c90"
      },
      {
        "id": 2,
        "symbol": "F",
        "label": "Fumigation",
        "description": "The type of treatment implemented for disease management. The application of smoke, vapor, or gas for the purpose of disinfecting or destroying pests or microorganisms.",
        "uri": "https://linked.data.gov.au/def/nrm/c3ca8e8f-ddc9-52d2-96a6-57ea676e67ff"
      },
      {
        "id": 3,
        "symbol": "HD",
        "label": "Host Destruction",
        "description": "The type of treatment implemented for disease management. This involves destruction of an organism (host) that nourishes and supports another but does not benefit by the association. ",
        "uri": "https://linked.data.gov.au/def/nrm/92a4f2c0-6b3e-5e51-b442-f667467ffdc6"
      },
      {
        "id": 4,
        "symbol": "PDMQ",
        "label": "Plant Disease Management - Quarantine",
        "description": "Refers to the type of plant disease management action implemented. Quarantine- is an indication of plants carrying pathogens or pests that are of concern to the immediate environment and requires isolation.",
        "uri": "https://linked.data.gov.au/def/nrm/0eea8019-8b72-5a41-8c75-439f92a19e83"
      },
      {
        "id": 5,
        "symbol": "PDMT",
        "label": "Plant Disease Management - Treatment",
        "description": "Refers to the type of plant disease management action implemented. Treatment- is an indication of plants being treated to eliminate any pathogens or pests that are of concern.",
        "uri": "https://linked.data.gov.au/def/nrm/3a3fc944-8ac6-5528-a584-31be1c209f3c"
      },
      {
        "id": 6,
        "symbol": "PDMH",
        "label": "Plant Disease Management - Hygiene",
        "description": "Refers to the type of plant disease management action implemented. Hygiene- is free from any pathogens or pests that are of concern.",
        "uri": "https://linked.data.gov.au/def/nrm/c9c6b1a5-6d7c-547c-969e-600ac1e6d46b"
      },
      {
        "id": 7,
        "symbol": "PC",
        "label": "Positive Competition",
        "description": "Refers to the type of plant disease management action implemented. Positive Control- is an active demand by an organism or groups for a material or con­dition, so that both are inhibited by the demand, e.g. certain plants competing for light, nutrients and water.",
        "uri": "https://linked.data.gov.au/def/nrm/9c81067e-0a4d-53dc-9cb3-09e349c4c583"
      },
      {
        "id": 8,
        "symbol": "O",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      }
    ],
    "lut-intervention-erosion-treatment-type": [
      {
        "id": 1,
        "symbol": "AWP",
        "label": "Alternate Watering Point",
        "description": "Refers to the type of treatment implemented for erosion control, such as the use of an alternate watering point.",
        "uri": "https://linked.data.gov.au/def/nrm/be15ee57-24a1-570b-92d1-a1ded45b8f29"
      },
      {
        "id": 2,
        "symbol": "ECS",
        "label": "Erosion Control Structure",
        "description": "Refers to the type of treatment implemented for erosion control, such as building an erosion control structure.",
        "uri": "https://linked.data.gov.au/def/nrm/e28131d2-a8f1-5c86-8f59-4d57a3a959d3"
      },
      {
        "id": 3,
        "symbol": "FPC",
        "label": "Farming Practice Change",
        "description": "Refers to the type of treatment implemented for erosion control, such as changing the farming practices.",
        "uri": "https://linked.data.gov.au/def/nrm/658968cd-b114-5829-b759-b78c256088dc"
      },
      {
        "id": 4,
        "symbol": "F",
        "label": "Fencing",
        "description": "Refers to the type of land management action implemented, i.e., activities such as building fences or related physical structures.",
        "uri": "https://linked.data.gov.au/def/nrm/65d5ff53-aa45-5b05-8cfa-2abd47b9782b"
      },
      {
        "id": 5,
        "symbol": "R",
        "label": "Revegetation",
        "description": "Revegetation is an activity that involves revegetating the landscape with plant communities in ecosystems which have previously been, partially or wholly, stripped of existing plant communities.",
        "uri": "https://linked.data.gov.au/def/nrm/f0aa6bfb-c354-5541-a83b-9e72af911aff"
      },
      {
        "id": 6,
        "symbol": "T",
        "label": "Terracing",
        "description": "Refers to the type of treatment implemented for erosion control, such as practicing a terrace method of farming.",
        "uri": "https://linked.data.gov.au/def/nrm/3edc63f2-4168-5730-9df1-efb07bcf592d"
      },
      {
        "id": 7,
        "symbol": "VW",
        "label": "Vegetated Waterway (Bioswale)",
        "description": "Refers to the type of treatment implemented for erosion control, such as building a vegetated waterway.",
        "uri": "https://linked.data.gov.au/def/nrm/03169edb-713d-5b4c-a9c1-06ab8a8ec58c"
      },
      {
        "id": 8,
        "symbol": "W",
        "label": "Windbreaks",
        "description": "Refers to the type of treatment implemented for erosion control, such as a physical barrier that act as a windbreak.",
        "uri": "https://linked.data.gov.au/def/nrm/dccdfec5-741e-58dc-a5e5-9d89994a62f9"
      },
      {
        "id": 9,
        "symbol": "O",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      }
    ],
    "lut-intervention-fire-management-type": [
      {
        "id": 1,
        "symbol": "CB",
        "label": "Cultural Burn",
        "description": "Refers to the type of fire management action implemented. Cultural burn is a traditional burn practice at a particular location in a particular time of the year.",
        "uri": "https://linked.data.gov.au/def/nrm/c9f1f747-3677-5e72-aac0-b49733ea4629"
      },
      {
        "id": 2,
        "symbol": "EB",
        "label": "Ecological Burn",
        "description": "Refers to the type of fire management action implemented. Ecological burn is a practice used to maintain, regenerate and increase plant species diversity, and to continue to provide habitat for many life forms. ",
        "uri": "https://linked.data.gov.au/def/nrm/4fd433ef-18e6-5471-b6ea-5636d1fb721e"
      },
      {
        "id": 3,
        "symbol": "G",
        "label": "Grading",
        "description": "Refers to the type of fire management action implemented. Grading allows the categorical or selective burns of a landscape to avoid future risks from natural fires. ",
        "uri": "https://linked.data.gov.au/def/nrm/cbb71575-5e48-5445-ae5e-e2852130bf11"
      },
      {
        "id": 4,
        "symbol": "HRB",
        "label": "Hazard Reduction Burn",
        "description": "Refers to the type of fire management action implemented. Hazard reduction burning is removing vegetation to result in less intense bush or grass fires.",
        "uri": "https://linked.data.gov.au/def/nrm/ffeeb01a-18d6-5442-ba5a-d5ecce323848"
      },
      {
        "id": 5,
        "symbol": "H",
        "label": "Herbicide",
        "description": "Refers to the type of fire management action implemented. Herbicide is a chemical agent used to destroy unwanted vegetation.",
        "uri": "https://linked.data.gov.au/def/nrm/26fbbfc6-c48f-52fa-9b54-eec6503191af"
      },
      {
        "id": 6,
        "symbol": "S",
        "label": "Slashing",
        "description": "This method involves physical removal of plants by the action of slashing.",
        "uri": "https://linked.data.gov.au/def/nrm/afce6a61-09ec-5aa1-b445-7b9ab5aacf8e"
      },
      {
        "id": 7,
        "symbol": "O",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      }
    ],
    "lut-intervention-habitat-augmentation-type": [
      {
        "id": 1,
        "symbol": "AFMD",
        "label": "Artificial Fauna Movement Devices",
        "description": "Refers to the type of habitat augmentation implemented, i.e., tracking the movement of a fauna with the assistance of a device.",
        "uri": "https://linked.data.gov.au/def/nrm/72c2e9b5-e1ca-5471-a4f0-446f7f8ede91"
      },
      {
        "id": 2,
        "symbol": "ANRH",
        "label": "Aritificial Nesting or Roosting Habitat (Incl. Tiles, Fence Posts",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "ET",
        "label": "Environmental Thinning",
        "description": "Refers to the type of habitat augmentation implemented, i.e., by clearing a section of vegetation growth.",
        "uri": "https://linked.data.gov.au/def/nrm/d2150f7d-8253-5e28-ae20-5dd9c89e2cef"
      },
      {
        "id": 4,
        "symbol": "IFP",
        "label": "Improving Fish Passage",
        "description": "Refers to the type of habitat augmentation implemented, i.e., improving the aquatic body for allowing passage of fishes.",
        "uri": "https://linked.data.gov.au/def/nrm/87844c7c-f11a-5573-865a-a3ef35824f62"
      },
      {
        "id": 5,
        "symbol": "NF",
        "label": "Natural Features (Rocks, Logs)",
        "description": "Refers to the type of habitat augmentation implemented, i.e., with the assistance of natural features such as logs, rocks, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/ca9bdb5d-1c8c-538c-ab3f-0604234f3cbe"
      },
      {
        "id": 6,
        "symbol": "O",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      }
    ],
    "lut-intervention-individuals-or-group": [
      {
        "id": 1,
        "symbol": "Individuals",
        "label": "Individuals",
        "description": "Refers to an intervention activity/survey conducted on 'individuals' rather than on 'groups'.",
        "uri": "https://linked.data.gov.au/def/nrm/e475d3e2-71ef-5a34-9ad7-921ed86853af"
      },
      {
        "id": 2,
        "symbol": "Groups",
        "label": "Groups",
        "description": "Refers to an intervention activity/survey conducted on 'groups' rather than on 'individuals'.",
        "uri": "https://linked.data.gov.au/def/nrm/a863d9fb-b72b-58fe-8006-e7afb04d857b"
      }
    ],
    "lut-intervention-initial-or-followup": [
      {
        "id": 1,
        "symbol": "Initial",
        "label": "Initial",
        "description": "Refers to the an 'initial' or the baseline survey undertaken.",
        "uri": "https://linked.data.gov.au/def/nrm/99a2dc81-5f94-5a46-9709-9837d7abfca6"
      },
      {
        "id": 2,
        "symbol": "Revisit",
        "label": "Revisit",
        "description": "",
        "uri": ""
      }
    ],
    "lut-intervention-land-management-industry-type": [
      {
        "id": 1,
        "symbol": "BAC",
        "label": "Broad Acre Cropping",
        "description": "Refers to the type of industry for which the site is being prepared. Broad acre cropping is a type of land suitable for farms practicing large-scale crop operations (e.g., sunflower, sugarcane, oilseeds, etc.). ",
        "uri": "https://linked.data.gov.au/def/nrm/3ddf383d-12e3-5a97-9ba0-9727863c84e5"
      },
      {
        "id": 2,
        "symbol": "D",
        "label": "Dairy",
        "description": "Refers to the type of industry for which the site is being prepared. Dairy is an industry in which animal milk is harvested and, optionally, processed for human consumption.",
        "uri": "https://linked.data.gov.au/def/nrm/a3a9099f-da10-5f81-906a-66dd9ca9ee34"
      },
      {
        "id": 3,
        "symbol": "H",
        "label": "Horticulture",
        "description": "Refers to the type of industry for which the site is being prepared for land management. Horticulture is a branch of agriculture which involves the cultivation of vegetables, flowers, or ornamental plants for food, industrial, medicinal, or aesthetic purposes. ",
        "uri": "https://linked.data.gov.au/def/nrm/5eb6f4e5-8ed3-58ae-b014-cbd05bcd4057"
      },
      {
        "id": 4,
        "symbol": "G",
        "label": "Grazing",
        "description": "Refers to the type of industry for which the site is being prepared for land management. Grazing are land suitable for livestock. ",
        "uri": "https://linked.data.gov.au/def/nrm/7f0f52d9-0a40-5e53-a8fa-b007050342e8"
      },
      {
        "id": 5,
        "symbol": "F",
        "label": "Fisheries",
        "description": "Refers to the type of industry for which the site is being prepared for land management. Fisheries are places for cultivation and harvesting of fish, particularly in sea waters. ",
        "uri": "https://linked.data.gov.au/def/nrm/1662aec0-9738-5608-8150-162edfca44c3"
      },
      {
        "id": 6,
        "symbol": "A",
        "label": "Aquaculture",
        "description": "Refers to the type of industry for which the site is being prepared. Aquaculture is a practice of culturing of fish, shellfish, aquatic plants, and/or other organisms in captivity or under controlled conditions in the near shore environment.",
        "uri": "https://linked.data.gov.au/def/nrm/ec2b45f9-bd5c-504c-a89f-4f94906c5e0b"
      },
      {
        "id": 7,
        "symbol": "E",
        "label": "Environment",
        "description": "",
        "uri": ""
      }
    ],
    "lut-intervention-pest-control": [
      {
        "id": 1,
        "symbol": "AS",
        "label": "Aerial Shoot",
        "description": "A method of pest control that involves the use of manned or unmanned aerial vehicles during an intervention activity.",
        "uri": "https://linked.data.gov.au/def/nrm/e0fbaaab-3d2f-54b2-89d8-0c3da6b94c12"
      },
      {
        "id": 2,
        "symbol": "BT",
        "label": "Bait and Trap",
        "description": "A method of pest control that involves the use of baits or lures and traps during an intervention activity.",
        "uri": "https://linked.data.gov.au/def/nrm/7073a3cc-d82a-5036-a8ea-022060dc40bf"
      },
      {
        "id": 3,
        "symbol": "B",
        "label": "Bait Only",
        "description": "A method of pest control that involves the use of baits or lures during an intervention activity.",
        "uri": "https://linked.data.gov.au/def/nrm/ccea3578-58d8-5d6a-9335-b1646a103461"
      },
      {
        "id": 4,
        "symbol": "FFE",
        "label": "Feral Free Enclosure",
        "description": "An area/enclosure free of feral/pest animals built for pest control activity.",
        "uri": "https://linked.data.gov.au/def/nrm/e9be40d2-61d7-53f4-b421-a74f1b5ccd50"
      },
      {
        "id": 5,
        "symbol": "EF",
        "label": "Exlcusion Fencing",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "F",
        "label": "Fumigation",
        "description": "The type of treatment implemented for disease management. The application of smoke, vapor, or gas for the purpose of disinfecting or destroying pests or microorganisms.",
        "uri": "https://linked.data.gov.au/def/nrm/c3ca8e8f-ddc9-52d2-96a6-57ea676e67ff"
      },
      {
        "id": 7,
        "symbol": "GS",
        "label": "Ground Shoot",
        "description": "A method that involves targeted elimination of feral or pest animals (e.g., feral pigs, donkeys etc.).",
        "uri": "https://linked.data.gov.au/def/nrm/59c68695-fa2f-5322-97f9-c948128b057f"
      },
      {
        "id": 8,
        "symbol": "TC",
        "label": "Trap and Cull",
        "description": "A method involving trapping and eliminating pest animals from the study system.",
        "uri": "https://linked.data.gov.au/def/nrm/a193a17f-e8d6-552e-bb2b-241c32ff2d20"
      },
      {
        "id": 9,
        "symbol": "TR",
        "label": "Trap and Remove",
        "description": "A method that involves trapping and removal of pest animals.",
        "uri": "https://linked.data.gov.au/def/nrm/db586a91-f7f2-5039-a99f-4f93bd86c971"
      },
      {
        "id": 10,
        "symbol": "O",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      }
    ],
    "lut-intervention-planting-method": [
      {
        "id": 1,
        "symbol": "DS",
        "label": "Direct Seeding",
        "description": "Refers to the planting method, i.e., 'direct seeding', implemented during an intervention activity.",
        "uri": "https://linked.data.gov.au/def/nrm/8168f57d-78b3-5e55-a596-fbc2377a1d0b"
      },
      {
        "id": 2,
        "symbol": "TP",
        "label": "Tubestock Planting",
        "description": "Refers to the planting method, i.e., 'tubestock planting', implemented during an intervention activity. Tubestock planting involves planting individual tubestocks of seedling/sapling material directly in the ground. ",
        "uri": "https://linked.data.gov.au/def/nrm/b5e4451f-a718-5e37-8c06-ed6c760f1d38"
      },
      {
        "id": 3,
        "symbol": "B",
        "label": "Both",
        "description": "Refers to the 'both' the planting methods, i.e., 'direct seeding' or 'tubestock planting', implemented during an intervention activity.",
        "uri": "https://linked.data.gov.au/def/nrm/78c51b0e-1596-586d-baca-74c40eaa35b7"
      },
      {
        "id": 4,
        "symbol": "O",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      }
    ],
    "lut-intervention-remediation-type": [
      {
        "id": 1,
        "symbol": "AC",
        "label": "Access Control",
        "description": "Refers to the method of riparian or aquatic remediation action with the use of physical structures or barriers.",
        "uri": "https://linked.data.gov.au/def/nrm/e484bcef-d0b9-547e-b9d5-9a70699459a6"
      },
      {
        "id": 2,
        "symbol": "BG",
        "label": "Bank Grooming",
        "description": "Refers to the method of riparian or aquatic remediation with the action of grooming the banks.",
        "uri": "https://linked.data.gov.au/def/nrm/f8424725-1e7d-5235-87d3-cd8730f95c18"
      },
      {
        "id": 3,
        "symbol": "FGS",
        "label": "Flow Gauging Stations",
        "description": "Refers to the method of riparian or aquatic remediation action. Flow gauging stations are built to control the water flow. ",
        "uri": "https://linked.data.gov.au/def/nrm/b41060b9-9fa7-59b6-83f3-b05f71d10c60"
      },
      {
        "id": 4,
        "symbol": "F",
        "label": "Fords",
        "description": "Refers to the method of riparian or aquatic remediation action. Fords are the shallow part of a stream which can be easily crossed. ",
        "uri": "https://linked.data.gov.au/def/nrm/d062147c-7207-5f37-bfeb-0b905eb5cdd9"
      },
      {
        "id": 5,
        "symbol": "C",
        "label": "Culverts",
        "description": "Refers to the method of riparian or aquatic remediation action. Culverts are watercourses that allow the passage of water.",
        "uri": "https://linked.data.gov.au/def/nrm/abc9d827-121c-5efe-8586-aa8808993786"
      },
      {
        "id": 6,
        "symbol": "R",
        "label": "Revegetation",
        "description": "Revegetation is an activity that involves revegetating the landscape with plant communities in ecosystems which have previously been, partially or wholly, stripped of existing plant communities.",
        "uri": "https://linked.data.gov.au/def/nrm/f0aa6bfb-c354-5541-a83b-9e72af911aff"
      },
      {
        "id": 7,
        "symbol": "W",
        "label": "Weirs",
        "description": "The method of riparian or aquatic remediation action. Weirs are a low obstruction built across the path of a stream or watercourse to raise its level.",
        "uri": "https://linked.data.gov.au/def/nrm/ca963db4-b281-5ae5-a2ce-511dd59ea2a4"
      },
      {
        "id": 8,
        "symbol": "O",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      }
    ],
    "lut-intervention-site-preparation-action-type": [
      {
        "id": 1,
        "symbol": "BBS",
        "label": "Basal Bark Spraying",
        "description": "Bark spraying involves mixing an oil-soluble herbicide in diesel (or other recommended product) and spraying the full circumference of the trunk or stem of the plant.",
        "uri": "https://linked.data.gov.au/def/nrm/8e3ee4a0-402a-59c0-bb16-dd5afe236281"
      },
      {
        "id": 2,
        "symbol": "BA",
        "label": "Biological Agents",
        "description": "Biological agents are live organisms (such as bacteria, fungi, etc.) used on target vegetation.",
        "uri": "https://linked.data.gov.au/def/nrm/36cb8d90-c777-54ce-9ad2-3adea2019d95"
      },
      {
        "id": 3,
        "symbol": "CS",
        "label": "Cut Stump",
        "description": "Cut stump involves physical cutting/removal of stumps. ",
        "uri": "https://linked.data.gov.au/def/nrm/4191bc66-002f-5c7f-a7bc-46ee26d3ae0a"
      },
      {
        "id": 4,
        "symbol": "CAS",
        "label": "Cut and Swab",
        "description": "This method is similar to the cut stump method, but is suited to vines and multi-stemmed shrubs. Here, the plant stems are cut through completely, close to the ground. Herbicide is then applied immediately to the cut surface emerging from the ground, via spray or brush application.",
        "uri": "https://linked.data.gov.au/def/nrm/374de199-bda5-5f70-a4fa-acc81f96d63c"
      },
      {
        "id": 5,
        "symbol": "D",
        "label": "Dozing",
        "description": "This method involves removal of vegetation using a heavy machinery or dozer.",
        "uri": "https://linked.data.gov.au/def/nrm/4937adaa-59f0-5b15-ada6-bc56dc951249"
      },
      {
        "id": 6,
        "symbol": "F",
        "label": "Felling",
        "description": "This method involves removal of vegetation through felling.",
        "uri": "https://linked.data.gov.au/def/nrm/01e02a52-886b-5aed-bda6-3bb894c5cfe9"
      },
      {
        "id": 7,
        "symbol": "FI",
        "label": "Fire",
        "description": "This method involves the removal of vegetation through fire actions.",
        "uri": "https://linked.data.gov.au/def/nrm/399025a1-712b-5e4b-a7c6-7056c3cea915"
      },
      {
        "id": 8,
        "symbol": "FS",
        "label": "Foliar Spraying",
        "description": "This method involves the removal of vegetation through chemical sprays to limit plant growth.",
        "uri": "https://linked.data.gov.au/def/nrm/f581e6df-d808-586a-8f01-df2a172fbfa1"
      },
      {
        "id": 9,
        "symbol": "GC",
        "label": "Grubbing and Chipping",
        "description": "This method involves weeds to be dug out using a mattock or chip hoe.",
        "uri": "https://linked.data.gov.au/def/nrm/4fe22dfa-6869-5cee-8182-7dc4abc17e62"
      },
      {
        "id": 10,
        "symbol": "HP",
        "label": "Hand Pulling",
        "description": "This method involves physical removal of plants by pulling the entire plant from the ground.",
        "uri": "https://linked.data.gov.au/def/nrm/7e102980-366a-5938-8479-247ab2240b05"
      },
      {
        "id": 11,
        "symbol": "MNC",
        "label": "Moisture and Nutrient Control",
        "description": "This method involves control of plant growth via monitoring its nutrient and water inputs.",
        "uri": "https://linked.data.gov.au/def/nrm/6f31f719-d116-57bc-9c05-3efd4017a69f"
      },
      {
        "id": 12,
        "symbol": "M",
        "label": "Mowing",
        "description": "This method involves use of mowers for removal of vegetation.",
        "uri": "https://linked.data.gov.au/def/nrm/2347cb08-4906-5bd5-80f2-6f55ef5850a7"
      },
      {
        "id": 13,
        "symbol": "OE",
        "label": "Other Earthworks",
        "description": "Refers to any 'Other Earthworks' other than Dozing used for the removal of vegetation undertaken for preparation of sites for subsequent activities.  ",
        "uri": "https://linked.data.gov.au/def/nrm/09f62da8-090b-5e67-bd52-15883926866e"
      },
      {
        "id": 14,
        "symbol": "OP",
        "label": "Overplanting",
        "description": "Refers to the type of planting action undertaken for preparation of sites for subsequent activities.",
        "uri": "https://linked.data.gov.au/def/nrm/6aac97ac-d69c-570f-9912-e4e966670489"
      },
      {
        "id": 15,
        "symbol": "P",
        "label": "Pushing",
        "description": "This method involves physical removal of plants by pushing the entire plant.",
        "uri": "https://linked.data.gov.au/def/nrm/ffd97a33-d86f-5b7f-a60f-14bc02ef00cc"
      },
      {
        "id": 16,
        "symbol": "S",
        "label": "Slashing",
        "description": "This method involves physical removal of plants by the action of slashing.",
        "uri": "https://linked.data.gov.au/def/nrm/afce6a61-09ec-5aa1-b445-7b9ab5aacf8e"
      },
      {
        "id": 17,
        "symbol": "SS",
        "label": "Spot Spraying",
        "description": "This method involves the use of herbicides as spot sprays or precision sprays on targeted surfaces of leaves or certain plants (such as weeds). ",
        "uri": "https://linked.data.gov.au/def/nrm/30775984-367e-57b0-baf7-ee653236af1a"
      },
      {
        "id": 18,
        "symbol": "SG",
        "label": "Splatter Gun",
        "description": "This method involves destruction of the vegetation where targeted streams of herbicide can be shot around sensitive native vegetation.",
        "uri": "https://linked.data.gov.au/def/nrm/c4eb909d-4414-57a6-8fb6-27c589fea7a9"
      },
      {
        "id": 19,
        "symbol": "SI",
        "label": "Stem Injection",
        "description": "This method involves the removal of unwanted large trees of larger diameter by drilling or injecting herbicides into the stem. ",
        "uri": "https://linked.data.gov.au/def/nrm/786f0780-95ee-5e85-b31c-5df4b81258a2"
      },
      {
        "id": 20,
        "symbol": "STS",
        "label": "Stem Scrapper",
        "description": "This method involves the removal of unwanted small shrubs and vines with thin and relatively soft bark tissue by scraping off its stem. ",
        "uri": "https://linked.data.gov.au/def/nrm/6904db0c-3ea1-5e48-8661-0e2790444523"
      },
      {
        "id": 21,
        "symbol": "WA",
        "label": "Wick Applicators",
        "description": "This method involves the removal of weeds or unwanted vegetation via wick applicators that cover a large area such as pasture land or cropland, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/546434d9-6c85-5b4e-89af-f383b3f3cbce"
      },
      {
        "id": 22,
        "symbol": "O",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      }
    ],
    "lut-intervention-type": [
      {
        "id": 1,
        "symbol": "CA",
        "label": "Controlling Access",
        "description": "This type of intervention activity involves the use of physical structures to be installed to control access and may result in any off-site areas being protected by this action.",
        "uri": "https://linked.data.gov.au/def/nrm/ab5f639d-c281-555e-af04-2d7de09fa623"
      },
      {
        "id": 2,
        "symbol": "CPA",
        "label": "Controlling Pest Animals",
        "description": "This type of intervention activity involves undertaking targeted control of feral or pest animals across a mapped area.",
        "uri": "https://linked.data.gov.au/def/nrm/0bfdb45e-952b-5a5b-95c6-a8403d807929"
      },
      {
        "id": 3,
        "symbol": "DR",
        "label": "Debris Removal",
        "description": "Tells us about the actions that have been undertaken to remove debris from land and/or water systems in the mapped area.",
        "uri": "https://linked.data.gov.au/def/nrm/2de6b7a0-e56d-5361-b4b8-571de11d9394"
      },
      {
        "id": 4,
        "symbol": "EM",
        "label": "Erosion Management",
        "description": "Tells us about the area affected by erosion, methods used to control or avoid erosion across the mapped area, and any areas that have benefited from this work.",
        "uri": "https://linked.data.gov.au/def/nrm/e7edfad9-423b-5a8f-893e-1620b33e1ead"
      },
      {
        "id": 5,
        "symbol": "EMF",
        "label": "Establishing and Maintaining  Feral-Free Enclosures",
        "description": "Tells us about the actions undertaken to protect native species or control feral species across the mapped area.",
        "uri": "https://linked.data.gov.au/def/nrm/f1147466-f429-5855-b2e2-0a09eef29130"
      },
      {
        "id": 6,
        "symbol": "EMB",
        "label": "Establishing and Maintaining Breeding Program",
        "description": "Tells us about the breeding programs undertaken across the mapped area.",
        "uri": "https://linked.data.gov.au/def/nrm/a6e81715-0442-5f83-a8ff-5cc45e4854fe"
      },
      {
        "id": 7,
        "symbol": "IFM",
        "label": "Implementing Fire Management Actions",
        "description": "Tells us about the fire management actions undertaken across the mapped area and any areas that have benefitted from this work.",
        "uri": "https://linked.data.gov.au/def/nrm/31b9cc85-696d-5e6e-9875-92a811ff0a4a"
      },
      {
        "id": 8,
        "symbol": "HA",
        "label": "Habitat Augmentation",
        "description": "Habitat augmentation are intervention actions undertaken across a mapped area to improve or enhance the ecological/environmental attributes; and areas that may benefit from this type of intervention work.",
        "uri": "https://linked.data.gov.au/def/nrm/2021899a-913c-59bd-8593-acf82cdd4915"
      },
      {
        "id": 9,
        "symbol": "IHR",
        "label": "Improving Hydrological Regime",
        "description": "Tells us about the treatments implemented to improve water management across the mapped area and any areas that have benefitted from this work.",
        "uri": "https://linked.data.gov.au/def/nrm/6127a739-d765-54b8-91f4-81623db98406"
      },
      {
        "id": 10,
        "symbol": "MD",
        "label": "Managing Disease",
        "description": "Tells us about the disease/s being managed and the actions undertaken across the mapped area and any areas that have benefitted from this work.",
        "uri": "https://linked.data.gov.au/def/nrm/db53c292-b016-5d2f-86f5-6dc5c8ed807c"
      },
      {
        "id": 11,
        "symbol": "RRAA",
        "label": "Remediating Riparian and Aquatic Area",
        "description": "Tells us about the remediation actions undertaken across the mapped area and any areas that have benefitted from this work.",
        "uri": "https://linked.data.gov.au/def/nrm/57481f19-e629-59d1-b597-49fff6dc185d"
      },
      {
        "id": 12,
        "symbol": "RW",
        "label": "Removing Weeds",
        "description": "Tells us about the weed removal activities undertaken across the mapped area.",
        "uri": "https://linked.data.gov.au/def/nrm/4fbbcc37-c031-500d-a3a1-a1a9256d49f9"
      },
      {
        "id": 13,
        "symbol": "RH",
        "label": "Revegetating Habitat",
        "description": "Tells us about the revegetation activities undertaken in a mapped area.",
        "uri": "https://linked.data.gov.au/def/nrm/712a61c8-0a18-5072-83dc-e009e43e5cfe"
      },
      {
        "id": 14,
        "symbol": "SP",
        "label": "Site Preparation",
        "description": "Tells us about the actions and methods used to prepare sites for subsequent activities.",
        "uri": "https://linked.data.gov.au/def/nrm/602c9e40-9ff7-5482-b2a3-1c77d82bb7c8"
      },
      {
        "id": 15,
        "symbol": "UEI",
        "label": "Undertaking Emergency Interventions to Prevent Extinctions",
        "description": "Tells us about emergency interventions undertaken to prevent extinctions in your mapped area.",
        "uri": "https://linked.data.gov.au/def/nrm/df856bae-d206-59a6-8139-065f3498282c"
      }
    ],
    "lut-intervention-water-treatment-management-type": [
      {
        "id": 1,
        "symbol": "RB",
        "label": "Removing Barriers (e.g. Fish Barriers)",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "SIM",
        "label": "Structure Instalment or Modification (e.g. Weirs, Flow Gauging Stations, Fords, Culverts)",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "R",
        "label": "Resnagging",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-interventions-established-or-maintained": [
      {
        "id": 1,
        "symbol": "Newly established",
        "label": "Newly established",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Maintained",
        "label": "Maintained",
        "description": "",
        "uri": ""
      }
    ],
    "lut-invertebrate-active-sampling-capture-equipment": [
      {
        "id": 1,
        "symbol": "AN",
        "label": "Aerial Net",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. Aerial nets are netting devices that allow sampling of flying invertebrates (e.g., moths).",
        "uri": "https://linked.data.gov.au/def/nrm/7e9b347e-1f3b-5727-955f-3dc678386752"
      },
      {
        "id": 2,
        "symbol": "A",
        "label": "Aspirator",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. An aspirator is an instrument as a pooter, used in the collection of insects, crustaceans or other small invertebrates.",
        "uri": "https://linked.data.gov.au/def/nrm/2fed0ca5-d57f-5268-80bd-f301904a76b0"
      },
      {
        "id": 3,
        "symbol": "BS",
        "label": "Beat stick",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. A beat stick is used to collect invertebrate samples by gently striking vegetation, dislodging insects onto a collecting sheet or container placed underneath.",
        "uri": "https://linked.data.gov.au/def/nrm/c9113c2a-4031-4317-ad34-3fbca7fcf30f"
      },
      {
        "id": 4,
        "symbol": "BT",
        "label": "Beat Tray",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. A beating tray consists of a pale coloured cloth that is usually stretched out using a frame. The frame is then held under a tree or shrub and the foliage is then shaken. Invertebrates fall from the foliage and land on the cloth. They can then be examined or collected using a pooter. ",
        "uri": "https://linked.data.gov.au/def/nrm/1d5ac3cf-093a-5095-8622-f5adf972eadf"
      },
      {
        "id": 5,
        "symbol": "BN",
        "label": "Butterfly Net",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. A butterfly net is a tool used for capturing flying insects, primarily butterflies and moths, by swooping the net over the insect in mid-air and then carefully transferring it into a container or collection vial.",
        "uri": "https://linked.data.gov.au/def/nrm/58fa7f15-bbb7-479d-a357-879ee607d5a5"
      },
      {
        "id": 6,
        "symbol": "GS",
        "label": "Ground sheet",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. Sheets are generally with a white background useful for pinning invertebrates.",
        "uri": "https://linked.data.gov.au/def/nrm/64f72425-4021-519c-ac5c-ad2c3d491afd"
      },
      {
        "id": 7,
        "symbol": "KB",
        "label": "Killing Bottle",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. Sheets are generally with a white background useful for pinning invertebrates.",
        "uri": "https://linked.data.gov.au/def/nrm/f99a1f6f-d1ef-496b-a557-483fe3a45da2"
      },
      {
        "id": 8,
        "symbol": "K",
        "label": "Knife(e.g. for opening galls)",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. A knife is used for carefully opening plant galls to access invertebrate inhabitants for sampling.",
        "uri": "https://linked.data.gov.au/def/nrm/039c4521-c2c6-4d57-8e6b-8c7dfcb6e16a"
      },
      {
        "id": 9,
        "symbol": "L",
        "label": "Light(specify type)",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. A light source attracts nocturnal invertebrates, facilitating their collection for study in entomology and ecology.",
        "uri": "https://linked.data.gov.au/def/nrm/558b878c-bbab-4e61-8607-2f376558b203"
      },
      {
        "id": 10,
        "symbol": "R",
        "label": "Rake",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. A rake is used for collecting invertebrates from aquatic habitats, such as ponds and streams, by sweeping through vegetation.",
        "uri": "https://linked.data.gov.au/def/nrm/4db50df1-222e-45e9-bfcf-9d4534436e61"
      },
      {
        "id": 11,
        "symbol": "PR",
        "label": "Paint brush",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. A paintbrush is gently used to transfer and handle delicate invertebrate specimens during collection and study.",
        "uri": "https://linked.data.gov.au/def/nrm/69edbf70-2c5c-43b0-8d9d-85c92d32b964"
      },
      {
        "id": 12,
        "symbol": "S",
        "label": "Screwdriver",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. A screwdriver is employed for lifting bark or detaching objects to access invertebrates during collection.",
        "uri": "https://linked.data.gov.au/def/nrm/8e68da05-be02-4cbd-99e4-e6c52ef84259"
      },
      {
        "id": 13,
        "symbol": "SN",
        "label": "Sweep Net",
        "description": "The equipment/method used during a passive, 'targeted fauna survey'. Sweep nets are usually used for capturing insects using a number of sweeps. The net is made of fine diameter mesh fitted to a metal handle to trap invertebrates in air.",
        "uri": "https://linked.data.gov.au/def/nrm/f2b4d479-e3a3-5855-b5bb-24350de1be22"
      },
      {
        "id": 14,
        "symbol": "T1",
        "label": "Trowel",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. A trowel is a small hand tool used for digging, applying, smoothing, or moving small amounts of viscous or particulate material.",
        "uri": "https://linked.data.gov.au/def/nrm/deeadf63-9de8-5ec5-a129-c171f92e7d12"
      },
      {
        "id": 15,
        "symbol": "T2",
        "label": "Tweezers",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. Tweezers are used to carefully handle and collect small invertebrates without harming them during sampling.",
        "uri": "https://linked.data.gov.au/def/nrm/ff9c2cfc-5510-4e76-8ec8-589211a0d660"
      },
      {
        "id": 16,
        "symbol": "WS",
        "label": "Wire saw",
        "description": "Refers to the capture method used for 'active sampling' of invertebrate fauna. A wire saw helps in carefully cutting through wood or other substrates to access hidden invertebrates for collection.",
        "uri": "https://linked.data.gov.au/def/nrm/2f778ac8-49d6-42ff-8693-f9c1cb64d29b"
      },
      {
        "id": 17,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-invertebrate-liquid-type": [
      {
        "id": 1,
        "symbol": "WDS",
        "label": "Water-Dishwashing Solution",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "UPG",
        "label": "Undiluted Propylene Glycol",
        "description": "",
        "uri": ""
      }
    ],
    "lut-invertebrate-malaise-trapping-voucher-type": [
      {
        "id": 1,
        "symbol": "LP",
        "label": "Liquid Preservation",
        "description": "Method of trapping and storage of invertebrate samples.",
        "uri": "https://linked.data.gov.au/def/nrm/42bb06dc-fd54-510a-957b-5ea7f4608d48"
      },
      {
        "id": 2,
        "symbol": "D",
        "label": "Dry",
        "description": "Method of trapping and storage of invertebrate samples.",
        "uri": "https://linked.data.gov.au/def/nrm/ec3886ce-d13d-5e27-9ffc-b79a30c51277"
      }
    ],
    "lut-invertebrate-pan-placement": [
      {
        "id": 1,
        "symbol": "OG",
        "label": "On ground",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "OT",
        "label": "On stand",
        "description": "",
        "uri": ""
      }
    ],
    "lut-invertebrate-pan-trapping-colour": [
      {
        "id": 1,
        "symbol": "W",
        "label": "White",
        "description": "Colour identifier used in invertebrate 'Pan Traps'.",
        "uri": "https://linked.data.gov.au/def/nrm/710a6585-3c80-5436-8be3-4bd651c6e6a4"
      },
      {
        "id": 2,
        "symbol": "B",
        "label": "Blue",
        "description": "Colour identifier used in invertebrate 'Pan Traps'.",
        "uri": "https://linked.data.gov.au/def/nrm/dc9540f6-9933-51bf-9d19-347656e32236"
      },
      {
        "id": 3,
        "symbol": "Y",
        "label": "Yellow",
        "description": "Colour identifier used in invertebrate 'Pan Traps'.",
        "uri": "https://linked.data.gov.au/def/nrm/9d35d131-5302-56ae-a2f8-baaa7dc635d3"
      }
    ],
    "lut-invertebrate-pan-trapping-duration": [
      {
        "id": 1,
        "symbol": "S",
        "label": "Short-term",
        "description": "<24 Hours",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "L",
        "label": "Long-term",
        "description": "Up to 7 Days",
        "uri": ""
      }
    ],
    "lut-invertebrate-post-field-guideline-group-tier-1": [
      {
        "id": 1,
        "symbol": "I",
        "label": "Insecta",
        "description": "Insects",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "A",
        "label": "Arachnida",
        "description": "Spiders, Scorpions, Etc",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "M",
        "label": "Myriapoda",
        "description": "Millipedes, Centipedes, Etc",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "C",
        "label": "Crustacea",
        "description": "Crabs",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "O",
        "label": "Onychophora",
        "description": "Velvet, Worms",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "H",
        "label": "Hexapoda",
        "description": "Proturans, Springtails",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "MO",
        "label": "Mollusca",
        "description": "Molluscs",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "AN",
        "label": "Annelida",
        "description": "Segmented Worms",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "N",
        "label": "Nematoda",
        "description": "Nematodes, Roundworms",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "AC",
        "label": "Acanthocephala",
        "description": "Thorny-Headed Worms",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "P",
        "label": "Platyhelminthes",
        "description": "Flat Worms",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "U",
        "label": "Unkown",
        "description": "",
        "uri": ""
      }
    ],
    "lut-invertebrate-post-field-guideline-group-tier-2": [
      {
        "id": 1,
        "symbol": "A",
        "label": "Archeognatha",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "B",
        "label": "Blattodea",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "C",
        "label": "Coleoptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "D",
        "label": "Dermaptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "DI",
        "label": "Diptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "E",
        "label": "Embioptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "EP",
        "label": "Ephemeroptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "G",
        "label": "Grylloblattaria",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "H",
        "label": "Hemiptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "HY",
        "label": "Hymenoptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "I",
        "label": "Isoptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "L",
        "label": "Lepidoptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "M",
        "label": "Mantodea",
        "description": "",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "ME",
        "label": "Mecoptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "MG",
        "label": "Megaloptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "N",
        "label": "Neuroptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "O",
        "label": "Odonata",
        "description": "",
        "uri": ""
      },
      {
        "id": 18,
        "symbol": "OR",
        "label": "Orthoptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 19,
        "symbol": "P",
        "label": "Phasmatodea",
        "description": "Phasmida",
        "uri": ""
      },
      {
        "id": 20,
        "symbol": "PH",
        "label": "Phthiraptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 21,
        "symbol": "PL",
        "label": "Plecoptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 22,
        "symbol": "PS",
        "label": "Psocoptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 23,
        "symbol": "S",
        "label": "Siphonaptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 24,
        "symbol": "ST",
        "label": "Strepisptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 25,
        "symbol": "T",
        "label": "Thysanoptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 26,
        "symbol": "TR",
        "label": "Trichoptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 27,
        "symbol": "Z",
        "label": "Zoraptera",
        "description": "",
        "uri": ""
      },
      {
        "id": 28,
        "symbol": "ZY",
        "label": "Zygentoma",
        "description": "Thysanura",
        "uri": ""
      },
      {
        "id": 29,
        "symbol": "AC",
        "label": "Acarina",
        "description": "",
        "uri": ""
      },
      {
        "id": 30,
        "symbol": "AM",
        "label": "Amblypygi",
        "description": "",
        "uri": ""
      },
      {
        "id": 31,
        "symbol": "AR",
        "label": "Araneae",
        "description": "",
        "uri": ""
      },
      {
        "id": 32,
        "symbol": "OP",
        "label": "Opiliones",
        "description": "",
        "uri": ""
      },
      {
        "id": 33,
        "symbol": "PA",
        "label": "Palpigradi",
        "description": "",
        "uri": ""
      },
      {
        "id": 34,
        "symbol": "PD",
        "label": "Pseudoscorpiondia",
        "description": "",
        "uri": ""
      },
      {
        "id": 35,
        "symbol": "R",
        "label": "Ricinulei",
        "description": "",
        "uri": ""
      },
      {
        "id": 36,
        "symbol": "SC",
        "label": "Scorpionida",
        "description": "",
        "uri": ""
      },
      {
        "id": 37,
        "symbol": "SZ",
        "label": "Schizomida",
        "description": "",
        "uri": ""
      },
      {
        "id": 38,
        "symbol": "SO",
        "label": "Solifugae",
        "description": "",
        "uri": ""
      },
      {
        "id": 39,
        "symbol": "U",
        "label": "Uropygi",
        "description": "",
        "uri": ""
      },
      {
        "id": 40,
        "symbol": "CH",
        "label": "Chilopoda",
        "description": "Centipedes",
        "uri": ""
      },
      {
        "id": 41,
        "symbol": "DP",
        "label": "Diploda",
        "description": "Millipede",
        "uri": ""
      },
      {
        "id": 42,
        "symbol": "PR",
        "label": "Pauropoda",
        "description": "Centipede-Like Arthropods",
        "uri": ""
      },
      {
        "id": 43,
        "symbol": "SY",
        "label": "Symphyla",
        "description": "Glasshouse Symphylans",
        "uri": ""
      },
      {
        "id": 44,
        "symbol": "CO",
        "label": "Collembola",
        "description": "",
        "uri": ""
      },
      {
        "id": 45,
        "symbol": "DL",
        "label": "Diplura",
        "description": "",
        "uri": ""
      },
      {
        "id": 46,
        "symbol": "PT",
        "label": "Protura",
        "description": "",
        "uri": ""
      },
      {
        "id": 47,
        "symbol": "MO",
        "label": "Molluscs",
        "description": "Snails",
        "uri": ""
      }
    ],
    "lut-key-target-taxa": [
      {
        "id": 1,
        "symbol": "VF",
        "label": "Vascular flora",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "NF",
        "label": "Non-vascular flora",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "VC",
        "label": "Vegetation community",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Birds",
        "label": "Birds",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "Mammals",
        "label": "Mammals",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "Reptiles",
        "label": "Reptiles",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "Frogs",
        "label": "Frogs",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "Invertebrates",
        "label": "Invertebrates",
        "description": "",
        "uri": ""
      }
    ],
    "lut-land-use-history": [
      {
        "id": 1,
        "symbol": "1.1.0",
        "label": "Nature conservation",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "1.1.1",
        "label": "Strict nature reserves",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "1.1.2",
        "label": "Wilderness area",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "1.1.3",
        "label": "National park",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "1.1.4",
        "label": "Natural feature protection",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "1.1.5",
        "label": "Habitat/species management area",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "1.1.6",
        "label": "Protected landscape",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "1.1.7",
        "label": "Other conserved area",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "1.2.0",
        "label": "Managed resource protection",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "1.2.1",
        "label": "Biodiversity",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "1.2.2",
        "label": "Surface water supply",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "1.2.3",
        "label": "Groundwater",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "1.2.4",
        "label": "Landscape",
        "description": "",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "1.2.5",
        "label": "Traditional Indigenous uses",
        "description": "",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "1.3.0",
        "label": "Other minimal use",
        "description": "",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "1.3.1",
        "label": "Defence land - natural areas",
        "description": "",
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "1.3.2",
        "label": "Stock route",
        "description": "",
        "uri": ""
      },
      {
        "id": 18,
        "symbol": "1.3.3",
        "label": "Residual native cover",
        "description": "",
        "uri": ""
      },
      {
        "id": 19,
        "symbol": "1.3.4",
        "label": "Rehabilitation",
        "description": "",
        "uri": ""
      },
      {
        "id": 20,
        "symbol": "2.1.0",
        "label": "Grazing native vegetation",
        "description": "",
        "uri": ""
      },
      {
        "id": 21,
        "symbol": "2.2.0",
        "label": "Production native forests",
        "description": "",
        "uri": ""
      },
      {
        "id": 22,
        "symbol": "3.1.0",
        "label": "Plantation forests",
        "description": "",
        "uri": ""
      },
      {
        "id": 23,
        "symbol": "3.2.0",
        "label": "Grazing modified pastures",
        "description": "",
        "uri": ""
      }
    ],
    "lut-landform-element": [
      {
        "id": 1,
        "symbol": "BKP",
        "label": "Backplain",
        "description": "Type of Landform Element with a large flat resulting from aggradation by overbank stream flow at some distance from the stream channel and in some cases biological (peat) accumulation; often characterised by a high watertable and the presence of swamps or lakes; part of a covered plain landform pattern.",
        "uri": "https://linked.data.gov.au/def/nrm/95d1041f-0a82-50d5-9702-f58a95029f95"
      },
      {
        "id": 2,
        "symbol": "BRK",
        "label": "Breakaway",
        "description": "Type of Landform Element that is generally steep with a maximal mid-slope or upper slope; generally comprising both a very short scarp (free face) that is often bare rockland; and a stony scarp-footslope (debris slope); often standing above a pediment.",
        "uri": "https://linked.data.gov.au/def/nrm/37480929-9e0f-5ad7-8df9-914920540692"
      },
      {
        "id": 3,
        "symbol": "DDE",
        "label": "Drainage depression",
        "description": "Type of Landform Element that is level to gently-inclined; long; narrow, shallow open depression with smoothly concave cross-section rising to moderately inclined side slopes; eroded or aggraded by sheet wash.",
        "uri": "https://linked.data.gov.au/def/nrm/5de0ce45-d559-57b0-8328-83e133672104"
      },
      {
        "id": 4,
        "symbol": "DUN",
        "label": "Dune",
        "description": "Type of Landform Element with moderately inclined to very steep ridge or hillock built up by the wind. This element may comprise Dune Crest and Dune Slope.",
        "uri": "https://linked.data.gov.au/def/nrm/e874a1f0-e788-5a8c-9331-4b8329222759"
      },
      {
        "id": 5,
        "symbol": "ALC",
        "label": "Alcove",
        "description": "Type of Landform Pattern that is moderately-inclined to very steep; short open depression with concave cross-section; eroded by collapse; landslides; creep or surface wash.",
        "uri": "https://linked.data.gov.au/def/nrm/e2ca4495-5e11-5f98-9bf1-14ff4bb4bbd5"
      },
      {
        "id": 6,
        "symbol": "FAN",
        "label": "Fan",
        "description": "Type of Landform Element that is large; gently inclined to level element with radial slope lines inclined away from a point; resulting from aggradation; or occasionally from erosion; by channelled; often braided; stream flow; or possibly by sheet flow.",
        "uri": "https://linked.data.gov.au/def/nrm/4524d1f6-1951-502e-b7fe-8439acd3448a"
      },
      {
        "id": 7,
        "symbol": "FLD",
        "label": "Floodout",
        "description": "Type of Landform Element that is Flat, inclined radially away from a point on the margin or at the end of a stream channel; aggraded by overbank stream flow; or by channelled stream flow associated with channels developed within the overbank flow; part of a covered plain landform pattern.",
        "uri": "https://linked.data.gov.au/def/nrm/6ff1c19e-cd58-5c14-b39d-6c093ba8b994"
      },
      {
        "id": 8,
        "symbol": "HCR",
        "label": "Hillcrest",
        "description": "Type of Landform Element that has very gently inclined to steep crest; smoothly convex; eroded mainly by creep and sheet wash. A typical element of mountains; hills; low hills and rises.",
        "uri": "https://linked.data.gov.au/def/nrm/30bd2063-582d-5156-8ea0-faab8d16b102"
      },
      {
        "id": 9,
        "symbol": "HSL",
        "label": "Hillslope",
        "description": "Type of Landform Element with gently inclined to precipitous slope; commonly simple and maximal; eroded by sheet wash; creep or water-aided mass movement. A typical element of mountains; hills; low hills and rises.",
        "uri": "https://linked.data.gov.au/def/nrm/f1c3145c-ff9d-5303-8281-e2a9866167d7"
      },
      {
        "id": 10,
        "symbol": "LUN",
        "label": "Lunette",
        "description": "Type of Landform Element which is elongated; gently curved; low ridge built up by wind on the margin of a playa; typically with a moderate; wave-modified slope towards the playa and a gentle outer slope.",
        "uri": "https://linked.data.gov.au/def/nrm/5cd65691-a295-577c-9f94-cd4756337e3f"
      },
      {
        "id": 11,
        "symbol": "PLA",
        "label": "Plain",
        "description": "Type of Landform Element that is large; very gently inclined or level element; of unspecified geomorphological agent or mode of activity. Level to undulating or; rarely; rolling landform pattern of extremely low relief (less than 9 m).",
        "uri": "https://linked.data.gov.au/def/nrm/d7154911-ea70-5613-9b4d-8fb80b36cda0"
      },
      {
        "id": 12,
        "symbol": "PLY",
        "label": "Playa/pan",
        "description": "Type of Landform Element that is large; shallow; level-floored closed depression; intermittently water-filled; but mainly dry due to evaporation; bounded as a rule by flats aggraded by sheet flow and channelled stream flow.",
        "uri": "https://linked.data.gov.au/def/nrm/45db9fbc-4730-51bb-9329-8818736698cc"
      },
      {
        "id": 13,
        "symbol": "RES",
        "label": "Riseslope",
        "description": "Type of Landform Element with slope of hillock of very low to extremely low relief (<30 m) (see Residual rise).",
        "uri": "https://linked.data.gov.au/def/nrm/bceaebcb-9ab0-54b0-8ea2-b66c3efe9490"
      },
      {
        "id": 14,
        "symbol": "SCA",
        "label": "Scarp",
        "description": "Type of Landform Element that is very wide; steep to precipitous maximal slope eroded by gravity; water-aided mass movement or sheet flow.",
        "uri": "https://linked.data.gov.au/def/nrm/115cecfd-35f7-51bf-b884-6267b453f50a"
      },
      {
        "id": 15,
        "symbol": "SWL",
        "label": "Swale",
        "description": "Type of Landform Element that is generally: i) Linear; level-floored open depression excavated by wind; or left relict between ridges built up by wind or waves; or built up to a lesser height than them. (ii) Long; curved open or closed depression left relict between scrolls built up by channelled stream flow.",
        "uri": "https://linked.data.gov.au/def/nrm/d901256e-66ab-5e26-a806-17a6ab335ae7"
      },
      {
        "id": 16,
        "symbol": "SWP",
        "label": "Swamp",
        "description": "Type of Landform Element that is almost level; closed or almost closed depression with a seasonal or permanent watertable at or above the surface; commonly aggraded by overbank stream flow and sometimes biological (peat) accumulation.",
        "uri": "https://linked.data.gov.au/def/nrm/4b14473a-033a-58f1-b02c-16f47da90265"
      },
      {
        "id": 17,
        "symbol": "BAN",
        "label": "Bank (stream bank)",
        "description": "Type of Landform Element with a very-short; very-wide slope; moderately inclined to precipitous; forming the marginal upper parts of a stream channel and resulting from erosion or aggradation by channelled stream flow.",
        "uri": "https://linked.data.gov.au/def/nrm/d110a3b2-a34f-5d94-828d-e539c22ae2b5"
      },
      {
        "id": 18,
        "symbol": "BAR",
        "label": "Bar (stream bar)",
        "description": "Type of Landform Element that is Elongated; gently to moderately inclined; low-ridge built up by channelled stream flow; part of a stream bed.",
        "uri": "https://linked.data.gov.au/def/nrm/f38db675-d185-5bc3-ba02-3c30394b38dc"
      },
      {
        "id": 19,
        "symbol": "DUB",
        "label": "Barchan dune",
        "description": "Type of Landform Element that is crescent-shaped dune, with tips extending leeward (downwind); making this side concave and the windward (upwind) side convex. Barchan dunes tend to be arranged in chains extending in the dominant wind direction.",
        "uri": "https://linked.data.gov.au/def/nrm/287d64df-98f4-58c3-950e-2356a15687fd"
      },
      {
        "id": 20,
        "symbol": "BEA",
        "label": "Beach",
        "description": "Type of Landform Element, which is usually short; low; very wide slope; gently or moderately inclined; built up or eroded by waves; forming the shore of a lake or sea.",
        "uri": "https://linked.data.gov.au/def/nrm/63960cfe-9e04-52b7-8940-4d6c2ebe0a02"
      },
      {
        "id": 21,
        "symbol": "BRI",
        "label": "Beach ridge",
        "description": "Type of Landform Element with a very long; nearly straight; low ridge; built-up by waves and usually modified by wind. A beach ridge is often a relict feature remote from the beach.",
        "uri": "https://linked.data.gov.au/def/nrm/1470deb8-2762-5fd9-8c53-fc6cf2f18a4e"
      },
      {
        "id": 22,
        "symbol": "BEN",
        "label": "Bench",
        "description": "Type of Landform Element which is short; gently or very gently inclined minimal mid-slope element eroded or aggraded by any agent.",
        "uri": "https://linked.data.gov.au/def/nrm/9da5cf89-4487-5f93-b351-0782a42ae362"
      },
      {
        "id": 23,
        "symbol": "BER",
        "label": "Berm",
        "description": "Type of Landform Element which is: (i) Short; very gently inclined to level minimal mid-slope in an embankment or cut face; eroded or aggraded by human activity. (ii) Flat built-up by waves above a beach.",
        "uri": "https://linked.data.gov.au/def/nrm/283386fa-c297-5d2a-b453-fca857ddf51d"
      },
      {
        "id": 24,
        "symbol": "BOU",
        "label": "Blow-out",
        "description": "Type of Landform Element that is usually small; open or closed depression excavated by the wind.",
        "uri": "https://linked.data.gov.au/def/nrm/e13edfb1-b27f-5f73-ac06-bb40b2520684"
      },
      {
        "id": 25,
        "symbol": "CBE",
        "label": "Channel bench",
        "description": "Type of Landform Element with a flat at the margin of a stream channel aggraded and partly eroded by overbank and channelled stream flow; an incipient flood plain. Channel benches have been referred to as 'low terraces'; but the term 'terrace' should be restricted to landform patterns above the influence of active stream flow.",
        "uri": "https://linked.data.gov.au/def/nrm/c68c6fca-6465-5607-a2eb-32634881cddf"
      },
      {
        "id": 26,
        "symbol": "CIR",
        "label": "Cirque",
        "description": "Type of Landform Element that is Precipitous to gently inclined; typically closed depression of concave contour and profile excavated by ice. The closed part of the depression may be shallow; the larger part being an open depression like an alcove.",
        "uri": "https://linked.data.gov.au/def/nrm/c5274b62-aa6d-5168-a941-37842c6d4791"
      },
      {
        "id": 27,
        "symbol": "CLI",
        "label": "Cliff",
        "description": "Type of Landform Element that is very wide; cliffed (greater than 72 degrees) maximal slope usually eroded by gravitational fall as a result of erosion of the base by various agencies; sometimes built up by marine organisms.",
        "uri": "https://linked.data.gov.au/def/nrm/41df86fc-66ec-5ef5-a605-edbe1b3c5edc"
      },
      {
        "id": 28,
        "symbol": "CFS",
        "label": "Cliff-footslope",
        "description": "Type of Landform Element with a slope situated below a cliff; with its contours generally parallel to the line of the cliff eroded by sheet wash or water-aided mass movement; and aggraded locally by collapsed material from above.",
        "uri": "https://linked.data.gov.au/def/nrm/141a8baa-89e0-5a55-b9fd-05a3c0b8c79c"
      },
      {
        "id": 29,
        "symbol": "DOC",
        "label": "Collapse doline",
        "description": "Type of Landform Element that is steep-sided; circular or elliptical closed depression; commonly funnel-shaped; characterised by subsurface drainage and formed by collapse of underlying caves within bedrock.",
        "uri": "https://linked.data.gov.au/def/nrm/b4c53a4f-3e36-507e-8f29-6468c146cd3c"
      },
      {
        "id": 30,
        "symbol": "CON",
        "label": "Cone (volcanic)",
        "description": "Type of Landform Element characterised by a hillock with a circular symmetry built up by volcanism. The crest may form a ring around a crater.",
        "uri": "https://linked.data.gov.au/def/nrm/85b66cd6-ced2-5176-8338-6cf889145d08"
      },
      {
        "id": 31,
        "symbol": "CRA",
        "label": "Crater",
        "description": "Type of Landform Element with a steep to precipitous closed depression excavated by explosions due to volcanism; human action; or impact of an extraterrestrial object.",
        "uri": "https://linked.data.gov.au/def/nrm/1431e5bb-f41a-56cc-b685-9aeed406fe63"
      },
      {
        "id": 32,
        "symbol": "CUT",
        "label": "Cut face",
        "description": "Type of Landform Element with a slope eroded by human activity.",
        "uri": "https://linked.data.gov.au/def/nrm/a75e7c17-b8c5-50b2-a0ca-74cfc6a65229"
      },
      {
        "id": 33,
        "symbol": "COS",
        "label": "Cut-over surface",
        "description": "Type of Landform Element with a flat eroded by human activity.",
        "uri": "https://linked.data.gov.au/def/nrm/47eeb3d4-8cd0-50b5-b837-4a54a22e3f31"
      },
      {
        "id": 34,
        "symbol": "DAM",
        "label": "Dam",
        "description": "Type of Landform Element with a ridge built-up by human activity so as to close a depression.",
        "uri": "https://linked.data.gov.au/def/nrm/2ac8be18-9aae-56e3-aa07-65929e667648"
      },
      {
        "id": 35,
        "symbol": "DBA",
        "label": "Deflation basin",
        "description": "Type of Landform Element with a basin excavated by wind erosion, which removes loose material; commonly above a resistant or a wet layer.",
        "uri": "https://linked.data.gov.au/def/nrm/8e448737-8d60-56a6-94df-9399d5b9e174"
      },
      {
        "id": 36,
        "symbol": "DUC",
        "label": "Dune crest",
        "description": "Type of Landform Element with a crest built-up or eroded by the wind.",
        "uri": "https://linked.data.gov.au/def/nrm/5c7e64fc-188c-5d94-b727-c88458bc7f95"
      },
      {
        "id": 37,
        "symbol": "DUS",
        "label": "Dune slope",
        "description": "Type of Landform Element with a slope built-up or eroded by the wind (similar to a Dune).",
        "uri": "https://linked.data.gov.au/def/nrm/032a1c8b-1048-5c81-bd06-8afce5c12451"
      },
      {
        "id": 38,
        "symbol": "EMB",
        "label": "Embankment",
        "description": "Type of Landform Element with a ridge or slope built-up by human activity over-time.",
        "uri": "https://linked.data.gov.au/def/nrm/045be349-f9ed-52d6-89ca-68c7ade53e41"
      },
      {
        "id": 39,
        "symbol": "EST",
        "label": "Estuary",
        "description": "Type of Landform Element which has a stream channel close to its junction with a sea or lake; where the action of channelled stream flow is modified by tide and waves. The width typically increases downstream.",
        "uri": "https://linked.data.gov.au/def/nrm/7cb2f197-c3e8-5491-b0db-78b839405fd7"
      },
      {
        "id": 40,
        "symbol": "FIL",
        "label": "Fil-top",
        "description": "Type of Landform Element that is similar to a Flat, aggraded by human activity.",
        "uri": "https://linked.data.gov.au/def/nrm/1958c732-bbdd-5984-acec-5f56c2b82a06"
      },
      {
        "id": 41,
        "symbol": "FOO",
        "label": "Footslope",
        "description": "Type of Landform Element with a slope situated below a cliff; with its contours generally parallel to the line of the cliff eroded by sheet wash or water-aided mass movement; and aggraded locally by collapsed material from above.",
        "uri": "https://linked.data.gov.au/def/nrm/c87e531a-ced5-579b-9c58-befdf3ee53b6"
      },
      {
        "id": 42,
        "symbol": "FOR",
        "label": "Foredune",
        "description": "Type of Landform Element that is very long; nearly straight moderately inclined to very steep ridge built up by the wind from material from an adjacent beach.",
        "uri": "https://linked.data.gov.au/def/nrm/1ed7b887-8971-5d45-8ada-48e18386de45"
      },
      {
        "id": 43,
        "symbol": "GUL",
        "label": "Gully",
        "description": "Type of Landform Element characterised by open depression with short; precipitous walls and moderately inclined to very gently inclined floor or small stream channel; eroded by channelled stream flow and consequent collapse and water-aided mass movement.",
        "uri": "https://linked.data.gov.au/def/nrm/37421601-50ec-5371-b697-e9e18a54ba92"
      },
      {
        "id": 44,
        "symbol": "DUH",
        "label": "Hummocky (weakly oriented dune)",
        "description": "Type of Landform Element with a very gently to moderately inclined rises or hillocks built-up or eroded by wind and lacking distinct orientation or regular pattern.",
        "uri": "https://linked.data.gov.au/def/nrm/a19a72a0-2433-5fd2-8f64-16503637b9ee"
      },
      {
        "id": 45,
        "symbol": "ITF",
        "label": "Intertidal flat",
        "description": "Type of Landform Element similar to a Tidal flat.",
        "uri": "https://linked.data.gov.au/def/nrm/c21bb284-7262-5dee-bd5b-8ecfe47961a0"
      },
      {
        "id": 46,
        "symbol": "LAG",
        "label": "Lagoon",
        "description": "Type of Landform Element characterised by closed depression filled with water that is typically salt or brackish; bounded at least in part by forms aggraded or built up by waves or reef-building organisms.",
        "uri": "https://linked.data.gov.au/def/nrm/5b1e7e84-5a23-5444-91c1-f97a872bc38e"
      },
      {
        "id": 47,
        "symbol": "LAK",
        "label": "Lake",
        "description": "Type of Landform Element with a large, water-filled closed depression.",
        "uri": "https://linked.data.gov.au/def/nrm/077edbc4-8980-5e85-a6eb-1f4bd3aa1bbf"
      },
      {
        "id": 48,
        "symbol": "LDS",
        "label": "Landslide",
        "description": "Type of Landform Element that is moderately inclined to very steep slope; eroded in the upper part and aggraded in the lower part by water-aided mass movement; characterised by irregular hummocks.",
        "uri": "https://linked.data.gov.au/def/nrm/eecee680-ecd8-5955-8434-dc9e7246f08d"
      },
      {
        "id": 49,
        "symbol": "LEV",
        "label": "Levee",
        "description": "Type of Landform Element which is very long; low; narrow; nearly level; sinuous ridge immediately adjacent to a stream channel; built up by overbank flow. Levees are built usually in pairs bounding the two sides of a stream channel; at the level reached by frequent floods. This element is part of a covered plain landform pattern. For an artificial levee; use Embankment. See also Prior stream.",
        "uri": "https://linked.data.gov.au/def/nrm/a749658e-2521-5fe0-8aaf-e352c3639fc1"
      },
      {
        "id": 50,
        "symbol": "DUF",
        "label": "Linear or longitudinal (seif) dune",
        "description": "Type of Landform Element that is large; sharp-crested; elongated; longitudinal (linear) dune or chain of sand dunes; oriented parallel; rather than transverse (perpendicular); to the prevailing wind. Not to be confused with the trailing arms of parabolic dunes.",
        "uri": "https://linked.data.gov.au/def/nrm/1fc7edc1-8d57-568f-9a98-9ab9ddcf2816"
      },
      {
        "id": 51,
        "symbol": "MAA",
        "label": "Maar",
        "description": "Type of Landform Element that is level-floored; commonly water-filled closed depression with a nearly circular; steep rim; excavated by volcanism.",
        "uri": "https://linked.data.gov.au/def/nrm/b357ae54-1094-5957-bf45-500ceab54a37"
      },
      {
        "id": 52,
        "symbol": "MOU",
        "label": "Mound",
        "description": "Type of Landform Element with a hillock built up by human activity.",
        "uri": "https://linked.data.gov.au/def/nrm/c94a751a-13d2-5840-b147-928c89ed1d7b"
      },
      {
        "id": 53,
        "symbol": "OXB",
        "label": "Ox-bow",
        "description": "Type of Landform Element that is long; curved; commonly water-filled closed depression eroded by channelled stream flow but closed as a result of aggradation by channelled or overbank stream flow during the formation of a meander plain landform pattern. The floor of an ox-bow may be more or less aggraded by overbank stream flow; wind; and biological (peat) accumulation.",
        "uri": "https://linked.data.gov.au/def/nrm/1248ad7d-fb9c-564f-93c4-0c6bf3fe9088"
      },
      {
        "id": 54,
        "symbol": "DUP",
        "label": "Parabolic dune",
        "description": "Type of Landform Element characterised by sand dune with a long, scoop-shaped form; convex in the downwind direction so that its horns point upwind; whose ground plan approximates the form of a parabola. The dunes left behind can be referred to as trailing arms. Where many such dunes have traversed an area; these can give the appearance of linear dunes.",
        "uri": "https://linked.data.gov.au/def/nrm/b2025ffe-3999-5c93-a6ef-295bfed37242"
      },
      {
        "id": 55,
        "symbol": "PED",
        "label": "Pediment",
        "description": "Type of Landform Element that is large; gently inclined to level (<1%) waning lower slope; with slope lines inclined in a single direction; or somewhat convergent or divergent; eroded or sometimes slightly aggraded by sheet flow. It is underlain by bedrock.",
        "uri": "https://linked.data.gov.au/def/nrm/392c68e8-e681-556d-a1ae-4a7aecf5144d"
      },
      {
        "id": 56,
        "symbol": "PIT",
        "label": "Pit",
        "description": "Type of Landform Element that is closed depression excavated by human activity.",
        "uri": "https://linked.data.gov.au/def/nrm/0de17230-6ae6-5c2e-ab07-0bbe262a3501"
      },
      {
        "id": 57,
        "symbol": "PST",
        "label": "Prior stream",
        "description": "Type of Landform Element that is long; generally sinuous; low ridge built up from materials originally deposited by stream flow along the line of a former stream channel. The landform element may include a depression marking the old stream bed; and relict levees.",
        "uri": "https://linked.data.gov.au/def/nrm/e705f231-3807-5f0b-9afd-59a1c9093a36"
      },
      {
        "id": 58,
        "symbol": "REF",
        "label": "Reef flat",
        "description": "Type of Landform Element with a flat, built up to sea level by marine organisms.",
        "uri": "https://linked.data.gov.au/def/nrm/0eec5735-6c35-58bf-9cc7-d129f4c6b8b2"
      },
      {
        "id": 59,
        "symbol": "RER",
        "label": "Residual rise",
        "description": "Type of Landform Element with a hillock of very low to extremely low relief (<30 m) and very gentle to steep slopes. This term is used to refer to an isolated rise surrounded by other landforms.",
        "uri": "https://linked.data.gov.au/def/nrm/4ee55457-4922-5d00-b13d-ca381ad44f63"
      },
      {
        "id": 60,
        "symbol": "REC",
        "label": "Risecrest",
        "description": "Type of Landform Element with a crest of hillock of very low to extremely low relief (<30 m) (see Residual rise).",
        "uri": "https://linked.data.gov.au/def/nrm/9969b531-7f6c-54a6-a23c-123c332d3428"
      },
      {
        "id": 61,
        "symbol": "RFL",
        "label": "Rock flat",
        "description": "Type of Landform Element that is a Flat of bare consolidated rock; usually eroded by sheet wash.",
        "uri": "https://linked.data.gov.au/def/nrm/bc6e0ac3-f1ef-5f6b-bcf9-7e23b620f256"
      },
      {
        "id": 62,
        "symbol": "RPL",
        "label": "Rock platform",
        "description": "Type of Landform Element that is a flat of consolidated rock eroded by waves.",
        "uri": "https://linked.data.gov.au/def/nrm/9fcf1019-135e-5fad-9fab-ea6da190eb05"
      },
      {
        "id": 63,
        "symbol": "SCD",
        "label": "Scald",
        "description": "Type of Landform Element with characteristics as that of a flat; bare of vegetation; from which soil has been eroded or excavated by surface wash or wind.",
        "uri": "https://linked.data.gov.au/def/nrm/54c94292-798f-5a26-8455-d1a302756f4f"
      },
      {
        "id": 64,
        "symbol": "SFS",
        "label": "Scarp-footslope",
        "description": "Type of Landform Element, generally waning or minimal slope situated below a scarp; with its contours generally parallel to the line of the scarp.",
        "uri": "https://linked.data.gov.au/def/nrm/d9f167e1-bc10-56b3-8c13-5d6ee1213b10"
      },
      {
        "id": 65,
        "symbol": "SCR",
        "label": "Scroll",
        "description": "Type of Landform Element that is long; curved; very low ridge built up by channelled stream flow and left relict by channel migration. Part of a meander plain landform pattern.",
        "uri": "https://linked.data.gov.au/def/nrm/d85de894-c9f3-545d-a131-906301d4ff91"
      },
      {
        "id": 66,
        "symbol": "SRP",
        "label": "Scroll plain",
        "description": "Type of Landform Element with large flat resulting from aggradation by channelled stream flow as a stream migrates from side to side; the dominant element of a meander plain landform pattern. This landform element may include occurrences of scroll; swale and ox-bow.",
        "uri": "https://linked.data.gov.au/def/nrm/69a64f8c-65b5-5185-8e16-39ce4f28a839"
      },
      {
        "id": 67,
        "symbol": "DOL",
        "label": "Solution doline",
        "description": "Type of Landform Element characterised by steep-sided; circular or elliptical closed depression; commonly funnel-shaped; characterised by subsurface drainage and formed by dissolution of the surface or underlying within bedrock.",
        "uri": "https://linked.data.gov.au/def/nrm/130cad8e-0a82-515a-ad75-6b84d165db6c"
      },
      {
        "id": 68,
        "symbol": "STB",
        "label": "Stream bed",
        "description": "Type of Landform Element that is linear; generally sinuous open depression forming the bottom of a stream channel; eroded and locally excavated; aggraded or built up by channelled stream flow. Parts that are built up include bars.",
        "uri": "https://linked.data.gov.au/def/nrm/71bc069c-03d3-53a5-a8d7-ef6a8f039389"
      },
      {
        "id": 69,
        "symbol": "STC",
        "label": "Stream channel",
        "description": "Type of Landform Element characterised by linear, generally sinuous open depression; in parts eroded; excavated; built up and aggraded by channelled stream flow. This element comprises stream bed and banks.",
        "uri": "https://linked.data.gov.au/def/nrm/987ae37a-02be-5ef7-a4a6-1b13406fa277"
      },
      {
        "id": 70,
        "symbol": "SUS",
        "label": "Summit surface",
        "description": "Type of Landform Element generally characterised by a very wide, level to gently inclined crest with abrupt margins; commonly eroded by water-aided mass movement or sheet wash.",
        "uri": "https://linked.data.gov.au/def/nrm/368641eb-467f-570b-9846-e8ebccb10186"
      },
      {
        "id": 71,
        "symbol": "STF",
        "label": "Supratidal flat",
        "description": "Type of Landform Element similar to tidal flat, generally referring to large flat, subject to inundation by water that is usually salt or brackish; aggraded by tides. An intertidal flat (ITF) is frequently inundated; a supratidal flat (STF) is seldom inundated.",
        "uri": "https://linked.data.gov.au/def/nrm/dd2d43e4-2a30-509f-817a-407b271e8204"
      },
      {
        "id": 72,
        "symbol": "TAL",
        "label": "Talus",
        "description": "Type of Landform Element which refers to a moderately inclined or steep waning lower slope; consisting of rock fragments aggraded by gravity.",
        "uri": "https://linked.data.gov.au/def/nrm/93fed9db-1788-54b3-8731-89430eb8f79a"
      },
      {
        "id": 73,
        "symbol": "TEF",
        "label": "Terrace Flat",
        "description": "Type of Landform Element which refers to a small flat aggraded or eroded by channelled or overbank stream flow; standing above a scarp and no longer frequently inundated; a former valley flat or part of a former flood plain.",
        "uri": "https://linked.data.gov.au/def/nrm/64008844-d6ff-5fa3-9218-48a3d57ada21"
      },
      {
        "id": 74,
        "symbol": "TEP",
        "label": "Terrace plain",
        "description": "Type of Landform Element which refers to a large or very large flat aggraded by channelled or overbank stream flow; standing above a scarp and no longer frequently inundated; part of a former flood plain.",
        "uri": "https://linked.data.gov.au/def/nrm/496d5410-d95c-5fe0-9b67-f9364550c4e4"
      },
      {
        "id": 75,
        "symbol": "TDC",
        "label": "Tidal creek",
        "description": "Type of Landform Element that is characterised by intermittently water-filled open depression and in-parts eroded; excavated; built up and aggraded by channelled tide-water flow; type of stream channel characterised by a rapid increase in width downstream.",
        "uri": "https://linked.data.gov.au/def/nrm/b82cfeb6-1194-513a-ba81-5010cb9eb38b"
      },
      {
        "id": 76,
        "symbol": "TDF",
        "label": "Tidal flat",
        "description": "Type of Landform Element generally referring to large flat, subject to inundation by water that is usually salt or brackish; aggraded by tides. An intertidal flat (ITF) is frequently inundated; a supratidal flat (STF) is seldom inundated.",
        "uri": "https://linked.data.gov.au/def/nrm/c20b62d5-3426-5317-9f27-b894f814cecf"
      },
      {
        "id": 77,
        "symbol": "TOR",
        "label": "Tor",
        "description": "Type of Landform Element generally characterised by steep to precipitous hillock; typically convex; with a surface mainly of bare rock; either coherent or comprising subangular to rounded; large boulders (exhumed core-stones; also themselves called tors) separated by open fissures; eroded by sheet wash or water-aided mass movement.",
        "uri": "https://linked.data.gov.au/def/nrm/c1c77cbf-4251-5437-b8c8-c3e44304f023"
      },
      {
        "id": 78,
        "symbol": "TRE",
        "label": "Trench",
        "description": "Type of Landform Element generally characterised by an open depression excavated by human activity.",
        "uri": "https://linked.data.gov.au/def/nrm/e31ccd59-0ce4-56ad-87a9-4e7a1cda9605"
      },
      {
        "id": 79,
        "symbol": "TUM",
        "label": "Tumulus",
        "description": "Type of Landform Element generally characterised by a hillock heaved up by volcanism (or; elsewhere; built up by human activity at a burial site).",
        "uri": "https://linked.data.gov.au/def/nrm/548c40d1-6c33-511d-be44-0c9f4f1360da"
      },
      {
        "id": 80,
        "symbol": "VLF",
        "label": "Valley flat",
        "description": "Type of Landform Element generally characterised by a small; gently inclined to level flat; aggraded or sometimes eroded by channelled or overbank stream flow; typically enclosed by hillslopes; a miniature alluvial plain landform pattern.",
        "uri": "https://linked.data.gov.au/def/nrm/f823af68-b200-57b2-9d6e-c1f15f01b4a4"
      }
    ],
    "lut-landform-pattern": [
      {
        "id": 1,
        "symbol": "FLO",
        "label": "Floodplain",
        "description": "Alluvial plain characterised by frequently active erosion and aggradation by channelled or overbank stream flow. Unless otherwise specified; 'frequently active' is to mean that flow has an Average Recurrence Interval of 50 years or less. Included types of landform pattern are: bar plain; meander plain; covered plain; anastomotic plain. Related relict landform patterns are: stagnant alluvial plain; terrace; terraced land (partly relict).",
        "uri": "https://linked.data.gov.au/def/nrm/09f2d5d6-21b7-5e0d-b3c4-aad508a27e00"
      },
      {
        "id": 2,
        "symbol": "HIL",
        "label": "Hills",
        "description": "Landform pattern of high relief (90-300 m) with gently inclined to precipitous slopes. Fixed; shallow; erosional stream channels; closely to very widely spaced; form a non-directional or convergent; integrated tributary network. There is continuously active erosion by wash and creep and; in some cases; rarely active erosion by landslides.",
        "uri": "https://linked.data.gov.au/def/nrm/2b145527-2fda-5970-bddf-3f65219f537c"
      },
      {
        "id": 3,
        "symbol": "KAR",
        "label": "Karst",
        "description": "Landform pattern of unspecified relief and slope typically with fixed; deep; erosional stream channels forming a non-directional; disintegrated tributary pattern and many closed depressions without stream channels. It is eroded by continuously active solution and rarely active collapse; the products being removed through underground channels.",
        "uri": "https://linked.data.gov.au/def/nrm/6ee19567-5635-5226-9415-188b629498f0"
      },
      {
        "id": 4,
        "symbol": "LAC",
        "label": "Laclustrine plain",
        "description": "Level landform pattern with extremely low relief formerly occupied by a lake but now partly or completely dry. It is relict after aggradation by waves and by deposition of material from suspension and solution in standing water. The pattern is usually bounded by wave-formed features such as cliffs; rock platforms; beaches; berms and lunettes. These may be included or excluded.",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "LAV",
        "label": "Lava plain",
        "description": "Level to undulating landform pattern of very low to extremely low relief typically with widely spaced; fixed; erosional stream channels that form a non-directional; integrated or interrupted tributary pattern. The landform pattern is aggraded by volcanism (lava flow) that is generally relict; it is subject to erosion by continuously active sheet flow; creep; and channelled stream flow.",
        "uri": "https://linked.data.gov.au/def/nrm/2757b086-873a-51b9-82a6-8b0687712165"
      },
      {
        "id": 6,
        "symbol": "LON",
        "label": "Longitudinal dunefield",
        "description": "Dunefield characterised by long; narrow sand dunes and wide; flat swales. The dunes are oriented parallel with the direction of the prevailing wind; and in cross-section one slope is typically steeper than the other.",
        "uri": "https://linked.data.gov.au/def/nrm/18c8b51a-e0a6-54e5-98c9-09defdef9763"
      },
      {
        "id": 7,
        "symbol": "LOW",
        "label": "Low hills",
        "description": "Landform pattern of low relief (30-90 m) and gentle to very steep slopes; typically with fixed; erosional stream channels; closely to very widely spaced; which form a non-directional or convergent; integrated tributary pattern. There is continuously active sheet flow; creep; and channelled stream flow.",
        "uri": "https://linked.data.gov.au/def/nrm/5af57dba-5cdd-53f6-952d-f91c490735f7"
      },
      {
        "id": 8,
        "symbol": "MAD",
        "label": "Made land",
        "description": "Landform pattern typically of very low or extremely low relief and with slopes either level or very steep. Sparse; fixed; deep; artificial stream channels form a non-directional; interrupted tributary pattern. The landform pattern is eroded and aggraded; and locally built up or excavated; by rarely active human agency.",
        "uri": "https://linked.data.gov.au/def/nrm/e269a514-c9cc-5e54-a09f-95a715724cc2"
      },
      {
        "id": 9,
        "symbol": "MAR",
        "label": "Marine plain",
        "description": "Plain eroded or aggraded by waves; tides; or submarine currents; and aggraded by deposition of material from suspension and solution in sea water; elevated above sea level by earth movements or eustasy; and little modified by subaerial agents such as stream flow or wind.",
        "uri": "https://linked.data.gov.au/def/nrm/55ea97b3-31c5-5004-95e2-646277c4f9fb"
      },
      {
        "id": 10,
        "symbol": "MEA",
        "label": "Meander plain",
        "description": "Flood plain with widely spaced; rapidly migrating; moderately deep alluvial stream channels which form a unidirectional; integrated non-tributary network. There is frequently active aggradation and erosion by channelled stream flow with subordinate aggradation by overbank stream flow.",
        "uri": "https://linked.data.gov.au/def/nrm/22eacf7a-4c3a-5777-9cc7-7d40b0b3b7ee"
      },
      {
        "id": 11,
        "symbol": "MET",
        "label": "Meteor crator",
        "description": "Rare landform pattern comprising a circular closed depression (see crater landform element) with a raised margin; it is typically of low to high relief and has a large range of slope values; without stream channels; or with a peripheral integrated pattern of centrifugal tributary streams. The pattern is excavated; heaved up and built up by a meteor impact and is now relict.",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "MOU",
        "label": "Mountains",
        "description": "Landform pattern of very high relief (greater than 300 m) with moderate to precipitous slopes and fixed; erosional stream channels that are closely to very widely spaced and form a non-directional or diverging; integrated tributary network. There is continuously active erosion by collapse; landslide; sheet flow; creep; and channelled stream flow.",
        "uri": "https://linked.data.gov.au/def/nrm/cd36dfa8-1280-57d6-863d-aa944ca964ed"
      },
      {
        "id": 13,
        "symbol": "PAR",
        "label": "Parabolic dunefield",
        "description": "Type of Landform Element characterised by sand dune with a long, scoop-shaped form; convex in the downwind direction so that its horns point upwind; whose ground plan approximates the form of a parabola. The dunes left behind can be referred to as trailing arms. Where many such dunes have traversed an area; these can give the appearance of linear dunes.",
        "uri": "https://linked.data.gov.au/def/nrm/cffe8060-0916-5217-9b90-dfd262fbc12e"
      },
      {
        "id": 14,
        "symbol": "ALF",
        "label": "Aluvial fan",
        "description": "Level (less than 1% slope) to very gently inclined; complex landform pattern of extremely low relief. The rapidly migrating alluvial stream channels are shallow to moderately deep; locally numerous; but elsewhere widely spaced. The channels form a centrifugal to divergent; integrated; reticulated to distributary pattern. The landform pattern includes areas that are bar plains; being aggraded or eroded by frequently active channelled stream flow; and other areas comprising terraces or stagnant alluvial plains with slopes that are greater than usual; formed by channelled stream flow but now relict. Incision in the upslope area may give rise to an erosional stream bed between scarps.",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "ALP",
        "label": "Aluvial plain",
        "description": "Level landform pattern with extremely low relief. The shallow to deep alluvial stream channels are sparse to widely spaced; forming a unidirectional; integrated network. There may be frequently active erosion and aggradation by channelled and overbank stream flow; or the landforms may be relict from these processes. Included types of landform pattern are: Flood plain; Bar plain; Meander plain; Covered plain; Anastomotic plain; Delta; Stagnant alluvial plain; Terrace; Terraced land.",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "ANA",
        "label": "Anastomotic plain",
        "description": "Flood plain with slowly migrating; deep alluvial channels; usually moderately spaced; forming a divergent to unidirectional; integrated reticulated network. There is frequently active aggradation by overbank and channelled stream flow",
        "uri": "https://linked.data.gov.au/def/nrm/fde010ad-fa2a-54cf-b5a0-0695c4e26cf9"
      },
      {
        "id": 17,
        "symbol": "BAD",
        "label": "Badlands",
        "description": "Landform pattern of low to extremely low relief (less than 90 m) and steep to precipitous slopes; typically with numerous fixed; erosional stream channels which form a non-directional; integrated tributary network. There is continuously active erosion by collapse; landslide; sheet flow; creep and channelled stream flow.",
        "uri": "https://linked.data.gov.au/def/nrm/ccb1f667-0c07-54e4-95ea-c4fc9038ee9b"
      },
      {
        "id": 18,
        "symbol": "BAR",
        "label": "Bar Plain",
        "description": "Flood plain with numerous rapidly migrating; shallow alluvial channels forming a unidirectional; integrated reticulated network. There is frequently active aggradation and erosion by channelled stream flow.",
        "uri": "https://linked.data.gov.au/def/nrm/87804991-be35-577e-a860-e705d43b15de"
      },
      {
        "id": 19,
        "symbol": "BEA",
        "label": "Beach ridge plain",
        "description": "Level to gently undulating landform pattern of extremely low relief on which stream channels are absent or very rare; it consists of relict; parallel beach ridges.",
        "uri": "https://linked.data.gov.au/def/nrm/5aeedd22-e88a-536e-bb85-7037686528b3"
      },
      {
        "id": 20,
        "symbol": "CAL",
        "label": "Caldera",
        "description": "Rare landform pattern typically of very high relief and steep to precipitous slope. It is without stream channels or has fixed; erosional channels forming a centripetal; integrated tributary pattern. The landform has subsided or was excavated as a result of volcanism.",
        "uri": "https://linked.data.gov.au/def/nrm/73d97557-6934-5036-8262-96be453e7adf"
      },
      {
        "id": 21,
        "symbol": "CHE",
        "label": "Chenier plain",
        "description": "Level to gently undulating landform pattern of extremely low relief on which stream channels are very rare. The pattern consists of relict; parallel; linear ridges built up by waves; separated by; and built over; flats (mud flats) aggraded by tides or overbank stream flow.",
        "uri": "https://linked.data.gov.au/def/nrm/73e51c76-d4f4-5e93-a310-e5100958ae61"
      },
      {
        "id": 22,
        "symbol": "COR",
        "label": "Coral reef",
        "description": "Continuously active or relict landform pattern built up to the sea level of the present day or of a former time by corals and other organisms. It is mainly level; with moderately inclined to precipitous slopes below the sea level. Stream channels are generally absent; but there may occasionally be fixed; deep; erosional tidal stream channels forming a disintegrated non-tributary pattern.",
        "uri": "https://linked.data.gov.au/def/nrm/3e6d793d-77c7-520d-af77-d22c32ed6e8a"
      },
      {
        "id": 23,
        "symbol": "COV",
        "label": "Covered plain",
        "description": "Flood plain with a slowly migrating; deep alluvial channels; usually widely spaced and forming a unidirectional; integrated non-tributary network. There is frequently active aggradation by overbank stream flow.",
        "uri": "https://linked.data.gov.au/def/nrm/b4956cb2-d66f-5e4e-9e34-ff8ed1e9df24"
      },
      {
        "id": 24,
        "symbol": "DEL",
        "label": "Delta",
        "description": "Flood plain projecting into a sea or lake; with slowly migrating; deep alluvial channels; usually moderately spaced; typically forming a divergent; integrated distributary network. This landform is aggraded by frequently active overbank and channelled stream flow that is modified by tides.",
        "uri": "https://linked.data.gov.au/def/nrm/d554d4b1-a716-5d61-a96e-57a95df29050"
      },
      {
        "id": 25,
        "symbol": "DUN",
        "label": "Dunefield",
        "description": "Level to rolling landform pattern of very low or extremely low relief without stream channels; built up or locally excavated; eroded or aggraded by wind. Included types of landform pattern are: longitudinal dunefield; parabolic dunefield.",
        "uri": "https://linked.data.gov.au/def/nrm/101075ba-daf8-5cfa-bc3e-faa8e4f7ea55"
      },
      {
        "id": 26,
        "symbol": "ESC",
        "label": "Escarpment",
        "description": "Steep to precipitous landform pattern forming a linearly extensive; straight or sinuous; inclined surface; which separates terrains at different altitudes; a plateau is commonly above the escarpment. Relief within the landform pattern may be high (hilly) or low (planar). The upper margin is often marked by an included cliff or scarp.",
        "uri": "https://linked.data.gov.au/def/nrm/8250c6af-a20a-58b1-8796-a9ee90cb15f9"
      },
      {
        "id": 27,
        "symbol": "PED",
        "label": "Pediment",
        "description": "Type of Landform Element that is large; gently inclined to level (<1%) waning lower slope; with slope lines inclined in a single direction; or somewhat convergent or divergent; eroded or sometimes slightly aggraded by sheet flow. It is underlain by bedrock.",
        "uri": "https://linked.data.gov.au/def/nrm/392c68e8-e681-556d-a1ae-4a7aecf5144d"
      },
      {
        "id": 28,
        "symbol": "PEP",
        "label": "Pediplain",
        "description": "Level to very gently inclined landform pattern with extremely low relief and no stream channels; eroded by barely active sheet flow and wind. Largely relict from more effective erosion by stream flow in incipient stream channels as on a pediment.",
        "uri": "https://linked.data.gov.au/def/nrm/ddb933d6-468f-552b-a752-c5a2fe383537"
      },
      {
        "id": 29,
        "symbol": "PNP",
        "label": "Peneplain",
        "description": "Level to gently undulating landform pattern with extremely low relief and sparse; slowly migrating Alluvial stream channels which form a non-directional; integrated tributary pattern. It is eroded by barely active sheet flow; creep; and channelled and overbank stream flow.",
        "uri": "https://linked.data.gov.au/def/nrm/49c66272-9b1e-5d4c-b9c2-d12b242edbce"
      },
      {
        "id": 30,
        "symbol": "PLA",
        "label": "Plain",
        "description": "Type of Landform Element that is large; very gently inclined or level element; of unspecified geomorphological agent or mode of activity. Level to undulating or; rarely; rolling landform pattern of extremely low relief (less than 9 m).",
        "uri": "https://linked.data.gov.au/def/nrm/d7154911-ea70-5613-9b4d-8fb80b36cda0"
      },
      {
        "id": 31,
        "symbol": "PLT",
        "label": "Plateau",
        "description": "Level to rolling landform pattern of plains; rises or low hills standing above a cliff scarp or escarpment that extends around a large part of its perimeter. A bounding scarp or cliff landform element may be included or excluded; a bounding escarpment would be an adjacent landform pattern.",
        "uri": "https://linked.data.gov.au/def/nrm/d0cc9e41-ca4c-50a4-bfe5-c2fa623a8dd5"
      },
      {
        "id": 32,
        "symbol": "PLY",
        "label": "Playa plain",
        "description": "Level landform pattern with extremely low relief; typically without stream channels; aggraded by rarely active sheet flow and modified by wind; waves; and soil phenomena.",
        "uri": "https://linked.data.gov.au/def/nrm/4762dc21-27f1-5ed9-b5cb-82d2b6e4398f"
      },
      {
        "id": 33,
        "symbol": "RIS",
        "label": "Rises",
        "description": "Landform pattern of very low relief (9-30 m) and very gentle to steep slopes. The fixed; erosional stream channels are closely to very widely spaced and form a non directional to convergent; integrated or interrupted tributary pattern. The pattern is eroded by continuously active to barely active creep and sheet flow.",
        "uri": "https://linked.data.gov.au/def/nrm/f0e21988-4659-5de2-aa77-9d4112ead940"
      },
      {
        "id": 34,
        "symbol": "SAN",
        "label": "Sandplain",
        "description": "Level to gently undulating landform pattern of extremely low relief and without channels; formed possibly by sheet flow or stream flow; but now relict and modified by wind action.",
        "uri": "https://linked.data.gov.au/def/nrm/e32ddb5c-4d3b-5204-a3d8-9987e8ca493c"
      },
      {
        "id": 35,
        "symbol": "SHF",
        "label": "Sheet-flood fan",
        "description": "Type of Landform Pattern, usually level (less than 1% slope) to very gently inclined landform pattern of extremely low relief with numerous rapidly migrating; very shallow incipient stream channels forming a divergent to unidirectional integrated or interrupted reticulated pattern. The pattern is aggraded by frequently active sheet flow and channelled stream flow; with subordinate wind erosion.",
        "uri": "https://linked.data.gov.au/def/nrm/abcf11d3-2aa2-5ec8-9123-99b22c9e8cc9"
      },
      {
        "id": 36,
        "symbol": "STA",
        "label": "Stagnant alluvial plain",
        "description": "Type of Landform Pattern with an alluvial plain on which erosion and aggradation by channelled and overbank stream flow is barely active or inactive because of reduced water supply.",
        "uri": "https://linked.data.gov.au/def/nrm/4ac6938b-e588-528d-bb91-a344217fb6de"
      },
      {
        "id": 37,
        "symbol": "TER",
        "label": "Terrace (alluvial)",
        "description": "Type of Landform Pattern that is a former flood-plain on which erosion and aggradation by channelled and overbank stream flow is barely active or inactive because deepening or enlargement of the stream channel has lowered the level of flooding. A pattern that has both a former flood plain and a significant active flood plain; or that has former flood plains at more than one level; becomes terraced land.",
        "uri": "https://linked.data.gov.au/def/nrm/36cac678-b741-5649-8ab7-b2a0d11168b3"
      },
      {
        "id": 38,
        "symbol": "TEL",
        "label": "Terraced land (alluvial)",
        "description": "Landform pattern including one or more terraces and often a flood plain. Relief is low or very low (9-90 m). Terrace plains or terrace flats occur at stated heights above the top of the stream bank.",
        "uri": "https://linked.data.gov.au/def/nrm/1208831d-e64f-5543-a28c-99693b0d7643"
      },
      {
        "id": 39,
        "symbol": "TID",
        "label": "Tidal Flat",
        "description": "Type of Landform Pattern that is mostly level, with extremely low relief and slowly migrating; deep alluvial stream channels; which form non-directional; integrated tributary patterns; it is aggraded by frequently active tides.",
        "uri": "https://linked.data.gov.au/def/nrm/428d2787-a221-523b-9cbd-3b8a7a2312f8"
      },
      {
        "id": 40,
        "symbol": "VOL",
        "label": "Volcano",
        "description": "Type of Landform Element that is typically very high and very steep, without stream channels; or with erosional stream channels forming a centrifugal; interrupted tributary pattern. The landform is built up by volcanism; and is modified by erosional agents.",
        "uri": "https://linked.data.gov.au/def/nrm/49e8888f-04c3-5689-8a49-421b0169ae53"
      },
      {
        "id": 41,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      }
    ],
    "lut-landscape-category": [
      {
        "id": 1,
        "symbol": "Terrestrial",
        "label": "Terrestrial",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Coastal",
        "label": "Coastal",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Riparian/riverine",
        "label": "Riparian/riverine",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Wetlands",
        "label": "Wetlands",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "Marine",
        "label": "Marine",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-location-type": [
      {
        "id": 1,
        "symbol": "EC",
        "label": "Erosion control",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "EE",
        "label": "Erosion evident",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "ATFMA",
        "label": "Area treated by fire management action",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "OABMA",
        "label": "Off-set area benefited by management action",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "SHA",
        "label": "Site of habitat augmentation",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "OABHA",
        "label": "Off-set area benefited from habitat augmentation",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "HRI",
        "label": "Hydrological regime improvement",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "CMRMA",
        "label": "Catchment managed as a result of management action",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "SDM",
        "label": "Site of disease management",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "OABDM",
        "label": "Off-site area benefited from disease management",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "RS",
        "label": "Revegetation site",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "OABRA",
        "label": "Off-site area if they benefited from this revegetation activity",
        "description": "",
        "uri": ""
      }
    ],
    "lut-lure-type": [
      {
        "id": 1,
        "symbol": "Food",
        "label": "Food",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Olfactory",
        "label": "Olfactory",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Audio",
        "label": "Audio",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Visual",
        "label": "Visual",
        "description": "",
        "uri": ""
      }
    ],
    "lut-lure-variety": [
      {
        "id": 1,
        "symbol": "Carasweet with grain",
        "label": "Carasweet with grain",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 2,
        "symbol": "CUROSITY",
        "label": "CUROSITY",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 3,
        "symbol": "DOGABAIT",
        "label": "DOGABAIT",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 4,
        "symbol": "DOGGONE",
        "label": "DOGGONE",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 5,
        "symbol": "Dried meat",
        "label": "Dried meat",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 6,
        "symbol": "FOXECUTE",
        "label": "FOXECUTE",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 7,
        "symbol": "FOXOFF",
        "label": "FOXOFF",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 8,
        "symbol": "FOXOFF Liver",
        "label": "FOXOFF Liver",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 9,
        "symbol": "FOXSHIELD",
        "label": "FOXSHIELD",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 10,
        "symbol": "Fresh meat",
        "label": "Fresh meat",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 11,
        "symbol": "HOGGONE",
        "label": "HOGGONE",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 12,
        "symbol": "Hogmat",
        "label": "Hogmat",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 13,
        "symbol": "Mammal mix",
        "label": "Mammal mix",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 14,
        "symbol": "MOUSEOFF Blocks",
        "label": "MOUSEOFF Blocks",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 15,
        "symbol": "MOUSEOFF Grain",
        "label": "MOUSEOFF Grain",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 16,
        "symbol": "MOUSEOFF Zinc",
        "label": "MOUSEOFF Zinc",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 17,
        "symbol": "PIGOUT",
        "label": "PIGOUT",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 18,
        "symbol": "RABBAIT Carrot",
        "label": "RABBAIT Carrot",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 19,
        "symbol": "RABBAIT Oat",
        "label": "RABBAIT Oat",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 20,
        "symbol": "Tinned meat",
        "label": "Tinned meat",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 1,
          "symbol": "Food",
          "label": "Food",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 21,
        "symbol": "Carman's Canine Call",
        "label": "Carman's Canine Call",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 22,
        "symbol": "Carman's Final Touch",
        "label": "Carman's Final Touch",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 23,
        "symbol": "Carman's Magna Glan",
        "label": "Carman's Magna Glan",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 24,
        "symbol": "Carman's Trail's End",
        "label": "Carman's Trail's End",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 25,
        "symbol": "Cat-A-Tonic",
        "label": "Cat-A-Tonic",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 26,
        "symbol": "Cats Me Dead",
        "label": "Cats Me Dead",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 27,
        "symbol": "Caven's Feline Fix",
        "label": "Caven's Feline Fix",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 28,
        "symbol": "Caven's Gusto",
        "label": "Caven's Gusto",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 29,
        "symbol": "Caven's Minnesota Red",
        "label": "Caven's Minnesota Red",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 30,
        "symbol": "Caven's Terminator",
        "label": "Caven's Terminator",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 31,
        "symbol": "Caven's Yodel Dog",
        "label": "Caven's Yodel Dog",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 32,
        "symbol": "Cottontail Cager Rabbit Lure",
        "label": "Cottontail Cager Rabbit Lure",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 33,
        "symbol": "D'Aigles Anise Paste",
        "label": "D'Aigles Anise Paste",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 34,
        "symbol": "D'Aigles Skunk Junk",
        "label": "D'Aigles Skunk Junk",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 35,
        "symbol": "Dobbin's Backbreaker",
        "label": "Dobbin's Backbreaker",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 36,
        "symbol": "Dog Urine",
        "label": "Dog Urine",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 37,
        "symbol": "Dunlap Lures Hell Fire",
        "label": "Dunlap Lures Hell Fire",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 38,
        "symbol": "Fox A Lot",
        "label": "Fox A Lot",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 39,
        "symbol": "Fox Hollow GH-II",
        "label": "Fox Hollow GH-II",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 40,
        "symbol": "Fox Hollow Voo-Doo",
        "label": "Fox Hollow Voo-Doo",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 41,
        "symbol": "Fox Urine",
        "label": "Fox Urine",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 42,
        "symbol": "Grawe's Cat Puddy",
        "label": "Grawe's Cat Puddy",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 43,
        "symbol": "Grawe's Snow Cat",
        "label": "Grawe's Snow Cat",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 44,
        "symbol": "Hawbaker's Coyote Food No. 10",
        "label": "Hawbaker's Coyote Food No. 10",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 45,
        "symbol": "Hawbaker's Wildcat No. 1",
        "label": "Hawbaker's Wildcat No. 1",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 46,
        "symbol": "Hawbaker's Wildcat No. 2",
        "label": "Hawbaker's Wildcat No. 2",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 47,
        "symbol": "Hawbaker's Wiley Red 500",
        "label": "Hawbaker's Wiley Red 500",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 48,
        "symbol": "Iron Dog Stimulator",
        "label": "Iron Dog Stimulator",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 49,
        "symbol": "Jameson's Fox Cream",
        "label": "Jameson's Fox Cream",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 50,
        "symbol": "Jameson's Prairie King",
        "label": "Jameson's Prairie King",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 51,
        "symbol": "Jameson's Sierra Mist",
        "label": "Jameson's Sierra Mist",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 52,
        "symbol": "K-9 A Plenty",
        "label": "K-9 A Plenty",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 53,
        "symbol": "Lenon's Coyote Nature's Call",
        "label": "Lenon's Coyote Nature's Call",
        "description": "",
        "uri": ""
      },
      {
        "id": 54,
        "symbol": "Long Distance Call Lure",
        "label": "Long Distance Call Lure",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 55,
        "symbol": "Magnum Fox",
        "label": "Magnum Fox",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 56,
        "symbol": "Mark June's Predator Frenzy",
        "label": "Mark June's Predator Frenzy",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 57,
        "symbol": "Mark June's Smokey Post",
        "label": "Mark June's Smokey Post",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 58,
        "symbol": "Mark June's Widowmaker",
        "label": "Mark June's Widowmaker",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 59,
        "symbol": "Mark June's Windwalker",
        "label": "Mark June's Windwalker",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 60,
        "symbol": "Milligan Brand 200% Shellfish Plus",
        "label": "Milligan Brand 200% Shellfish Plus",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 61,
        "symbol": "Milligan Brand Cat-Man-Do",
        "label": "Milligan Brand Cat-Man-Do",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 62,
        "symbol": "Minnesota Ambrette Mush Oil",
        "label": "Minnesota Ambrette Mush Oil",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 63,
        "symbol": "Minnesota Civet Musk Oil Imitation",
        "label": "Minnesota Civet Musk Oil Imitation",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 64,
        "symbol": "Minnesota Muscaro Musk Lure",
        "label": "Minnesota Muscaro Musk Lure",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 65,
        "symbol": "Miranda's High Plains Predator Call",
        "label": "Miranda's High Plains Predator Call",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 66,
        "symbol": "Miranda's Novemeber Red",
        "label": "Miranda's Novemeber Red",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 67,
        "symbol": "O'Gormans Foxey",
        "label": "O'Gormans Foxey",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 68,
        "symbol": "O'Gormans Government Call",
        "label": "O'Gormans Government Call",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 69,
        "symbol": "O'Gormans Gumbo",
        "label": "O'Gormans Gumbo",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 70,
        "symbol": "O'Gormans Long Distance Call",
        "label": "O'Gormans Long Distance Call",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 71,
        "symbol": "O'Gormans Revenge",
        "label": "O'Gormans Revenge",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 72,
        "symbol": "Outfoxed Pest Control Catastrophe A",
        "label": "Outfoxed Pest Control Catastrophe A",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 73,
        "symbol": "Paste Bait",
        "label": "Paste Bait",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 74,
        "symbol": "Pest Lures Bait Master",
        "label": "Pest Lures Bait Master",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 75,
        "symbol": "Pest Lures GAV",
        "label": "Pest Lures GAV",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 76,
        "symbol": "Pest Lures Gotcha",
        "label": "Pest Lures Gotcha",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 77,
        "symbol": "Pest Lures Hogstopper",
        "label": "Pest Lures Hogstopper",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 78,
        "symbol": "Pest Lures Reg Dog",
        "label": "Pest Lures Reg Dog",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 79,
        "symbol": "Pest Lures Stinger",
        "label": "Pest Lures Stinger",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 80,
        "symbol": "Red Ring",
        "label": "Red Ring",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 81,
        "symbol": "Reuswaat's Tombstone",
        "label": "Reuswaat's Tombstone",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 82,
        "symbol": "TKO Blackies Blend",
        "label": "TKO Blackies Blend",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 83,
        "symbol": "True Catnip",
        "label": "True Catnip",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 84,
        "symbol": "Valerian Oil",
        "label": "Valerian Oil",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 85,
        "symbol": "Weiser Western Lure Bad Company",
        "label": "Weiser Western Lure Bad Company",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 86,
        "symbol": "Weiser Western Lure Bobcat Blend",
        "label": "Weiser Western Lure Bobcat Blend",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 87,
        "symbol": "Weiser Western Lure Canine #14",
        "label": "Weiser Western Lure Canine #14",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 88,
        "symbol": "Weiser Western Lure Carcass Wasteland",
        "label": "Weiser Western Lure Carcass Wasteland",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 89,
        "symbol": "Weiser Western Lure Old Timer",
        "label": "Weiser Western Lure Old Timer",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 90,
        "symbol": "Weiser Western Lure Rocky Mountain Rub",
        "label": "Weiser Western Lure Rocky Mountain Rub",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 91,
        "symbol": "Weiser Western Lure Sucker Punch",
        "label": "Weiser Western Lure Sucker Punch",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 92,
        "symbol": "Weiser Western Lure Super Cat",
        "label": "Weiser Western Lure Super Cat",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 93,
        "symbol": "Welch's Fascination",
        "label": "Welch's Fascination",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 2,
          "symbol": "Olfactory",
          "label": "Olfactory",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 94,
        "symbol": "Feline-attracting Phonic (FAP)",
        "label": "Feline-attracting Phonic (FAP)",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 3,
          "symbol": "Audio",
          "label": "Audio",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 95,
        "symbol": "Conspecific social calls",
        "label": "Conspecific social calls",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 3,
          "symbol": "Audio",
          "label": "Audio",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 96,
        "symbol": "Competitor playback",
        "label": "Competitor playback",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 3,
          "symbol": "Audio",
          "label": "Audio",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 97,
        "symbol": "Predator playback",
        "label": "Predator playback",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 3,
          "symbol": "Audio",
          "label": "Audio",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 98,
        "symbol": "Prey playback",
        "label": "Prey playback",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 3,
          "symbol": "Audio",
          "label": "Audio",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 99,
        "symbol": "Ultrasonic",
        "label": "Ultrasonic",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 3,
          "symbol": "Audio",
          "label": "Audio",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 100,
        "symbol": "Unnatural sounds",
        "label": "Unnatural sounds",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 3,
          "symbol": "Audio",
          "label": "Audio",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 101,
        "symbol": "Aluminium dish/tray",
        "label": "Aluminium dish/tray",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 4,
          "symbol": "Visual",
          "label": "Visual",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 102,
        "symbol": "Compact Disc",
        "label": "Compact Disc",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 4,
          "symbol": "Visual",
          "label": "Visual",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 103,
        "symbol": "Feathers",
        "label": "Feathers",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 4,
          "symbol": "Visual",
          "label": "Visual",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 104,
        "symbol": "Flashing LEDs - red",
        "label": "Flashing LEDs - red",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 4,
          "symbol": "Visual",
          "label": "Visual",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 105,
        "symbol": "Flashing LEDs - white",
        "label": "Flashing LEDs - white",
        "description": "",
        "uri": "",
        "lure_type": {
          "id": 4,
          "symbol": "Visual",
          "label": "Visual",
          "description": "",
          "uri": ""
        }
      },
      {
        "id": 106,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-male-frog-breeing-status": [
      {
        "id": 1,
        "symbol": "UK",
        "label": "Unknown",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "VS",
        "label": "Vocal sac obvious",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "N1",
        "label": "Nuptial pads inconspicuous",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "N2",
        "label": "Nuptial pads obvious but not fully developed",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "N3",
        "label": "Nuptial pads fully developed",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "BM",
        "label": "In amplexus",
        "description": "",
        "uri": ""
      }
    ],
    "lut-microhabitat": [
      {
        "id": 1,
        "symbol": "Artificial surface",
        "label": "Artificial surface",
        "description": "Refers to any surface that mimics or represents a natural habitat but is not a true habitat for a fauna under survey.",
        "uri": "https://linked.data.gov.au/def/nrm/41e9f83d-305b-590d-bdfb-79c50b3dcfc9"
      },
      {
        "id": 2,
        "symbol": "Bare ground",
        "label": "Bare ground",
        "description": "Refers to the type of land surface substrate. Bare- represents exposed ground or soil.",
        "uri": "https://linked.data.gov.au/def/nrm/73e3624e-7012-5bce-8567-326397fefbee"
      },
      {
        "id": 3,
        "symbol": "Bark on tree",
        "label": "Bark on tree",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Beach",
        "label": "Beach",
        "description": "Type of Landform Element, which is usually short; low; very wide slope; gently or moderately inclined; built up or eroded by waves; forming the shore of a lake or sea.",
        "uri": "https://linked.data.gov.au/def/nrm/b0a10539-838f-59d3-b625-4a099d5d86e0"
      },
      {
        "id": 5,
        "symbol": "Bridge",
        "label": "Bridge",
        "description": "Refers to the microhabitat where the targeted fauna was observed. Bridge is a physical structure built at an elevated platform to allow movement/passage of traffic.",
        "uri": "https://linked.data.gov.au/def/nrm/9292d220-67c5-533b-9dda-b79c336c4fe1"
      },
      {
        "id": 6,
        "symbol": "Building",
        "label": "Building",
        "description": "Refers to the microhabitat where the targeted fauna was observed. Buildings are any physical structures used for dwelling and commercial purposes.",
        "uri": "https://linked.data.gov.au/def/nrm/f959562e-27fc-5d55-9b5d-74c233eb2345"
      },
      {
        "id": 7,
        "symbol": "Burrow",
        "label": "Burrow",
        "description": "Refers to the microhabitat where the targeted fauna was observed. A burrow is a hole or tunnel excavated into the ground by an animal to create a space suitable for habitation, temporary refuge, or as a byproduct of locomotion.",
        "uri": "https://linked.data.gov.au/def/nrm/34d0e128-771c-53e1-90c5-6ebbb987dff2"
      },
      {
        "id": 8,
        "symbol": "Canopy",
        "label": "Canopy",
        "description": "Refers to the microhabitat where the targeted fauna was observed. A canopy is the uppermost layer of the stratum of a vegetation.",
        "uri": "https://linked.data.gov.au/def/nrm/079a0393-3fa3-5595-a06f-4fcf13e547f0"
      },
      {
        "id": 9,
        "symbol": "Cave",
        "label": "Cave",
        "description": "The type of habitat representative of a naturally formed, subterranean open area or chamber.",
        "uri": "https://linked.data.gov.au/def/nrm/a33ba5a1-d6d4-51d5-aa0d-6fd7a7188102"
      },
      {
        "id": 10,
        "symbol": "Cliff",
        "label": "Cliff",
        "description": "Type of Landform Element that is very wide; cliffed (greater than 72 degrees) maximal slope usually eroded by gravitational fall as a result of erosion of the base by various agencies; sometimes built up by marine organisms.",
        "uri": "https://linked.data.gov.au/def/nrm/18811849-a39f-5fb0-8f94-031d7f2c2b31"
      },
      {
        "id": 11,
        "symbol": "Crest",
        "label": "Crest",
        "description": "Landform element that stands above all, or almost all, points in the adjacent terrain. It is characteristically smoothly convex upwards in downslope profile or in contour, or both. ",
        "uri": "https://linked.data.gov.au/def/nrm/d70040b2-0621-5c1b-aca1-e060877fff43"
      },
      {
        "id": 12,
        "symbol": "Crown",
        "label": "Crown",
        "description": "Refers to the microhabitat where a targeted fauna was observed. Crown is the upper part of a tree canopy, which includes the branches and leaves.",
        "uri": "https://linked.data.gov.au/def/nrm/6c512b04-413f-5f45-87ff-8e9886a9d315"
      },
      {
        "id": 13,
        "symbol": "Dam",
        "label": "Dam",
        "description": "Type of Landform Element with a ridge built-up by human activity so as to close a depression.",
        "uri": "https://linked.data.gov.au/def/nrm/ba3d5401-184b-5d60-a637-1e3a65dc8099"
      },
      {
        "id": 14,
        "symbol": "Dead mallee",
        "label": "Dead mallee",
        "description": "Refers to the microhabitat where a target fauna was observed. A tree mallee is generally of Eucalyptus species multistemmed from base.",
        "uri": "https://linked.data.gov.au/def/nrm/88851ebc-0fee-5787-a23d-e8067aecc3ce"
      },
      {
        "id": 15,
        "symbol": "Dead shrub",
        "label": "Dead shrub",
        "description": "Refers to the microhabitat where a target fauna was observed. A mallee shrub is generally a shrub of Eucalyptus species multistemmed from base.",
        "uri": "https://linked.data.gov.au/def/nrm/4c33fb42-105b-57ab-9445-38deff1c7e34"
      },
      {
        "id": 16,
        "symbol": "Dead tree",
        "label": "Dead tree",
        "description": "Refers to the microhabitat where a target fauna was observed. A dead tree is generally any tree with signs of deterioration and devoid of any physiological functioning (such as photosynthesis, nutrient and water absorption).",
        "uri": "https://linked.data.gov.au/def/nrm/a4db485d-1e36-5a99-b3eb-77c8ab8d7c0c"
      },
      {
        "id": 17,
        "symbol": "Drainage line",
        "label": "Drainage line",
        "description": "Refers to the microhabitat where a target fauna was observed. A drainage line is a horizontal or near-horizontal pipe that facilitates water movement and is interconnected with any larger lines or the main drain.",
        "uri": "https://linked.data.gov.au/def/nrm/77340b00-a26d-5c9a-aa05-4cb962ed64ad"
      },
      {
        "id": 18,
        "symbol": "Edge of water",
        "label": "Edge of water",
        "description": "Refers to the microhabitat where a target fauna was observed. The edge of any body of water, such as a lake or sea.",
        "uri": "https://linked.data.gov.au/def/nrm/c8b5dc15-c1b7-5d23-9a3a-cdc2d7422c5b"
      },
      {
        "id": 19,
        "symbol": "Fence",
        "label": "Fence",
        "description": "Refers to the microhabitat where a target fauna was observed. A physical barrier that confines/restricts movement of a fauna.",
        "uri": "https://linked.data.gov.au/def/nrm/6d2bf662-c186-5fcc-b3b3-30f6ca941976"
      },
      {
        "id": 20,
        "symbol": "Foliage",
        "label": "Foliage",
        "description": "Refers to the microhabitat where a target fauna was observed. Foliage are leaves or collection of leaves of a plant.",
        "uri": "https://linked.data.gov.au/def/nrm/3f26c2b2-4b1a-5f7e-9bfa-c46f2c0d7b42"
      },
      {
        "id": 21,
        "symbol": "Grass",
        "label": "Grass",
        "description": "Refers to the microhabitat where a target fauna was observed. Grasses or graminoids are monocotyledonous from Poales Order, usually herbaceous plants with narrow leaves growing from the base.",
        "uri": "https://linked.data.gov.au/def/nrm/02ea9cd4-b649-5040-bd98-bc406c8e297a"
      },
      {
        "id": 22,
        "symbol": "Ground",
        "label": "Ground",
        "description": "Refers to the type of land surface substrate. Bare- represents exposed ground or soil.",
        "uri": "https://linked.data.gov.au/def/nrm/73e3624e-7012-5bce-8567-326397fefbee"
      },
      {
        "id": 23,
        "symbol": "Ground cover",
        "label": "Ground cover",
        "description": "Refers to the lowermost layer of a vegetation stratum or the land surface layer covered by vegetation. ",
        "uri": "https://linked.data.gov.au/def/nrm/99b370dc-ddf1-5915-96e3-2e2c6f5125e3"
      },
      {
        "id": 24,
        "symbol": "Herbs/forbs",
        "label": "Herbs/forbs",
        "description": "Any seed plant without woody stems. ",
        "uri": "https://linked.data.gov.au/def/nrm/dce85d57-a8fc-5b5a-ab17-e4f9baf32725"
      },
      {
        "id": 25,
        "symbol": "Hummock grass",
        "label": "Hummock grass",
        "description": "A type of evergreen perennials that appear as mounds up to 1m in height. In between the mounds or hummocks the ground is usually bare or exposed, except after seasonal or cyclonic rains, when multiple short-lived, ephemeral plants proliferate. Some examples are species of the genus Triodia, Spinifex.",
        "uri": "https://linked.data.gov.au/def/nrm/d4ccd815-5d81-5842-b059-3a3963b92e2b"
      },
      {
        "id": 26,
        "symbol": "Iron",
        "label": "Iron",
        "description": "A metallic element found in certain minerals, in nearly all soils, and in mineral waters. ",
        "uri": "https://linked.data.gov.au/def/nrm/dad56df8-24b9-5648-9be5-7a49f20394d8"
      },
      {
        "id": 27,
        "symbol": "Leaf litter",
        "label": "Leaf litter",
        "description": "A land surface substrate composed of dead leaves and contribute to organic matter in soil.",
        "uri": "https://linked.data.gov.au/def/nrm/e71cb4a3-7fe0-5ce5-a9bb-eefc3973ca88"
      },
      {
        "id": 28,
        "symbol": "Live mallee",
        "label": "Live mallee",
        "description": "Refers to the microhabitat where a target fauna was observed. A live standing tree mallee is generally of Eucalyptus species multistemmed from base.",
        "uri": "https://linked.data.gov.au/def/nrm/70a28e99-9b14-5f88-bddb-3dc8fd8f9901"
      },
      {
        "id": 29,
        "symbol": "Live shrub",
        "label": "Live shrub",
        "description": "A live standing mallee shrub is generally a shrub of Eucalyptus species multistemmed from base.",
        "uri": "https://linked.data.gov.au/def/nrm/ebc11b32-80e7-5753-8dcd-d364044e900d"
      },
      {
        "id": 30,
        "symbol": "Live tree",
        "label": "Live tree",
        "description": "A live tree is generally any tree with active physiological functioning (such as photosynthesis, nutrient and water absorption).",
        "uri": "https://linked.data.gov.au/def/nrm/01260f56-9beb-5b18-8af8-96fd476cdb97"
      },
      {
        "id": 31,
        "symbol": "Log / fallen wood / bark",
        "label": "Log / fallen wood / bark",
        "description": "Refers to the microhabitat where a target fauna was observed. A log or fallen wood or bark is a woody part of a tree or a shrub.",
        "uri": "https://linked.data.gov.au/def/nrm/9f36ec4a-2e37-577c-9034-8145be2c7331"
      },
      {
        "id": 32,
        "symbol": "Low shrub",
        "label": "Low shrub",
        "description": "Refers to the microhabitat where a target fauna was observed. A shrub which is generally low-lying or not fully erect.",
        "uri": "https://linked.data.gov.au/def/nrm/6516e6a3-b830-5197-bf94-9f18fd3e529f"
      },
      {
        "id": 33,
        "symbol": "Lower canopy",
        "label": "Lower canopy",
        "description": "Refers to the microhabitat where the targeted fauna was observed. A lower canopy is the lower layer of the canopy or the stratum of a vegetation.",
        "uri": "https://linked.data.gov.au/def/nrm/ee80985f-cf32-5dfe-9ffd-2f9f91db0653"
      },
      {
        "id": 34,
        "symbol": "Mid canopy",
        "label": "Mid canopy",
        "description": "Refers to the microhabitat where the targeted fauna was observed. A mid canopy is the middle layer or the stratum of a vegetation.",
        "uri": "https://linked.data.gov.au/def/nrm/f0236a7c-f31e-5dfb-be89-d22cf1acfba2"
      },
      {
        "id": 35,
        "symbol": "Nest",
        "label": "Nest",
        "description": "Refers to the microhabitat where the targeted fauna was observed. A nest is a place of refuge to hold an animal's eggs or provide a place to live or raise offspring.",
        "uri": "https://linked.data.gov.au/def/nrm/a018ca68-7d19-5319-b9a4-d41b0e2de428"
      },
      {
        "id": 36,
        "symbol": "Sky",
        "label": "Sky",
        "description": "Sky is the region of the atmosphere and outer space seen from the earth.",
        "uri": "https://linked.data.gov.au/def/nrm/a74e073c-0f3f-5f9c-a2e8-ea0903b4cfab"
      },
      {
        "id": 37,
        "symbol": "Pool",
        "label": "Pool",
        "description": "Refers to the microhabitat where the targeted fauna was observed. A pool is a body of water, usually of smaller size than a lake.",
        "uri": "https://linked.data.gov.au/def/nrm/f7d133e9-6b5d-5789-97cb-219b46049005"
      },
      {
        "id": 38,
        "symbol": "Post / stump",
        "label": "Post / stump",
        "description": "Refers to the microhabitat where the targeted fauna was observed. A post or a stump is a physical structure used for displaying signs (as in sign post).",
        "uri": "https://linked.data.gov.au/def/nrm/25aa23a2-65fe-5b67-a5da-397a3cac707c"
      },
      {
        "id": 39,
        "symbol": "Power line",
        "label": "Power line",
        "description": "Refers to the microhabitat where the targeted fauna was observed. Power lines are lines which carry electrical charge.",
        "uri": "https://linked.data.gov.au/def/nrm/3accad8e-d088-5e9b-b13b-7fb4941f129a"
      },
      {
        "id": 40,
        "symbol": "Reeds",
        "label": "Reeds",
        "description": "Refers to the microhabitat where the targeted fauna was observed. Reeds are tufts of grasses which generally grow on marshes and near-aquatic bodies.",
        "uri": "https://linked.data.gov.au/def/nrm/2a6fcdaf-8034-51bb-82f4-b333e488f0f0"
      },
      {
        "id": 41,
        "symbol": "Ridge",
        "label": "Ridge",
        "description": "Refers to the microhabitat where the targeted fauna was observed. Bridge is a physical structure built at an elevated platform to allow movement/passage of traffic.",
        "uri": "https://linked.data.gov.au/def/nrm/9292d220-67c5-533b-9dda-b79c336c4fe1"
      },
      {
        "id": 42,
        "symbol": "Road / track",
        "label": "Road / track",
        "description": "Refers to the microhabitat where the targeted fauna was observed. A path used for vehicular movement.",
        "uri": "https://linked.data.gov.au/def/nrm/1a9976c1-72a4-5dba-b326-2b96ed42cf47"
      },
      {
        "id": 43,
        "symbol": "Rock",
        "label": "Rock",
        "description": "Refers to the microhabitat where the targeted fauna was observed. Rock is a land geologic substrate occurring as a solid aggregate of one or more minerals or mineraloids.",
        "uri": "https://linked.data.gov.au/def/nrm/57a167b9-537b-5b43-95c6-951a530301ec"
      },
      {
        "id": 44,
        "symbol": "Rocks",
        "label": "Rocks",
        "description": "Refers to the microhabitat where the targeted fauna was observed. Rock is a land geologic substrate occurring as a solid aggregate of one or more minerals or mineraloids.",
        "uri": "https://linked.data.gov.au/def/nrm/d3d98097-50e2-5ac7-bc80-1769a8cd8bf0"
      },
      {
        "id": 45,
        "symbol": "Rocky outcrop",
        "label": "Rocky outcrop",
        "description": "Refers to the type of habitat characterised by rocks, which protrudes through the surface layer.",
        "uri": "https://linked.data.gov.au/def/nrm/756b33c0-d851-56fa-b2b6-3ec1ebf7daf0"
      },
      {
        "id": 46,
        "symbol": "Roost",
        "label": "Roost",
        "description": "Refers to the microhabitat where the targeted fauna was observed. Roost is a place where birds rest or sleep.",
        "uri": "https://linked.data.gov.au/def/nrm/698613dc-feb6-54c4-8743-31fb3eb72100"
      },
      {
        "id": 47,
        "symbol": "Rubbish",
        "label": "Rubbish",
        "description": "Refers to the microhabitat where the targeted fauna was observed. Rubbish are any waste material generated from human activities.",
        "uri": "https://linked.data.gov.au/def/nrm/0af978b5-1416-5ccc-863f-e97aed200ba0"
      },
      {
        "id": 48,
        "symbol": "Ruins",
        "label": "Ruins",
        "description": "Refers to the microhabitat where the targeted fauna was observed. Ruins are remains of a building, city, etc., that has been destroyed or that is in disrepair or a state of decay.",
        "uri": "https://linked.data.gov.au/def/nrm/9f7f3c92-9ab0-5c0b-9aa5-cdd99db55812"
      },
      {
        "id": 49,
        "symbol": "Shrub",
        "label": "Shrub",
        "description": "Refers to the microhabitat where a target fauna was observed. A mallee shrub is generally a shrub of Eucalyptus species multistemmed from base.",
        "uri": "https://linked.data.gov.au/def/nrm/4c33fb42-105b-57ab-9445-38deff1c7e34"
      },
      {
        "id": 50,
        "symbol": "Sand",
        "label": "Sand",
        "description": "Lithology of soil with an unconsolidated material.",
        "uri": "https://linked.data.gov.au/def/nrm/7f73f520-17cb-530a-a072-7bdcaaf4f0f3"
      },
      {
        "id": 51,
        "symbol": "Soil",
        "label": "Soil",
        "description": "It's a natural body consisting of layers (soil horizons) that are primarily composed of minerals, mixed with at least some organic matter, which differ from their parent materials in their texture, structure, consistency, color, chemical, biological and other characteristics.",
        "uri": "https://linked.data.gov.au/def/nrm/4bbda02a-9013-5ce8-b36e-8a12031add1a"
      },
      {
        "id": 52,
        "symbol": "Termite mound",
        "label": "Termite mound",
        "description": "Refers to the type of habitat characterised by mounds made by termites, primarily composed of termite saliva, feces and clay.",
        "uri": "https://linked.data.gov.au/def/nrm/d9e73ac4-9a5b-5047-b19b-467e1a58ec97"
      },
      {
        "id": 53,
        "symbol": "Tree",
        "label": "Tree",
        "description": "Refers to the microhabitat where a target fauna was observed. A dead tree is generally any tree with signs of deterioration and devoid of any physiological functioning (such as photosynthesis, nutrient and water absorption).",
        "uri": "https://linked.data.gov.au/def/nrm/a4db485d-1e36-5a99-b3eb-77c8ab8d7c0c"
      },
      {
        "id": 54,
        "symbol": "Tree hollow",
        "label": "Tree hollow",
        "description": "Refers to the microhabitat where the targeted fauna was observed. Tree hollows are specialised hollows or holes formed inside the trunk or branch of a trees.",
        "uri": "https://linked.data.gov.au/def/nrm/7d235739-f863-527f-b8cf-6d605c9779f1"
      },
      {
        "id": 55,
        "symbol": "Tree which has a hollow",
        "label": "Tree which has a hollow",
        "description": "Refers to the tree with a hollow where the targeted fauna was observed. ",
        "uri": "https://linked.data.gov.au/def/nrm/7b8f4598-8c2e-5863-afde-bc2ffce6da94"
      },
      {
        "id": 56,
        "symbol": "Trunk",
        "label": "Trunk",
        "description": "Refers to the microhabitat where the targeted fauna was observed. Trunk is the woody stem of the tree. ",
        "uri": "https://linked.data.gov.au/def/nrm/5519bb5f-e42b-5cb1-87f3-378e168d86d2"
      },
      {
        "id": 57,
        "symbol": "Tussock grass",
        "label": "Tussock grass",
        "description": "Tussock grasses or bunch grasses are a group of grass species in the family Poaceae (Gramineae). ",
        "uri": "https://linked.data.gov.au/def/nrm/e1eb0092-da6c-57b6-adde-707ed97fb30c"
      },
      {
        "id": 58,
        "symbol": "Undergrowth",
        "label": "Undergrowth",
        "description": "Refers to the microhabitat where the targeted fauna was observed. Undergrowth is the vegetation growth in the understory layer of a forested ecosystem.",
        "uri": "https://linked.data.gov.au/def/nrm/c48a77ac-18b7-5733-a6e5-ff1d2e5b1733"
      },
      {
        "id": 59,
        "symbol": "Upper canopy",
        "label": "Upper canopy",
        "description": "Refers to the microhabitat where the targeted fauna was observed. Upper canopy is the uppermost surface of the canopy layer or stratum of the vegetation.",
        "uri": "https://linked.data.gov.au/def/nrm/22e7b53b-dab7-5bb5-a693-ceac461a2503"
      },
      {
        "id": 60,
        "symbol": "Water",
        "label": "Water",
        "description": "Refers to the microhabitat where a target fauna was observed. The edge of any body of water, such as a lake or sea.",
        "uri": "https://linked.data.gov.au/def/nrm/c8b5dc15-c1b7-5d23-9a3a-cdc2d7422c5b"
      },
      {
        "id": 61,
        "symbol": "Waterbody",
        "label": "Waterbody",
        "description": "Refers to any aquatic body such as lake, stream, river where the targeted fauna was observed. ",
        "uri": "https://linked.data.gov.au/def/nrm/4add4256-af54-59fb-8763-720bd071558d"
      },
      {
        "id": 62,
        "symbol": "Woodpile or human debris",
        "label": "Woodpile or human debris",
        "description": "",
        "uri": ""
      }
    ],
    "lut-monitoring-scale": [
      {
        "id": 1,
        "symbol": "State",
        "label": "State",
        "description": "",
        "uri": "",
        "sort_order": 1
      },
      {
        "id": 2,
        "symbol": "NRM",
        "label": "NRM region",
        "description": "",
        "uri": "",
        "sort_order": 2
      },
      {
        "id": 3,
        "symbol": "IBRA",
        "label": "IBRA region",
        "description": "",
        "uri": "",
        "sort_order": 3
      },
      {
        "id": 4,
        "symbol": "IBRAS",
        "label": "IBRA sub-region",
        "description": "",
        "uri": "",
        "sort_order": 4
      },
      {
        "id": 5,
        "symbol": "Site",
        "label": "Site",
        "description": "",
        "uri": "",
        "sort_order": 6
      },
      {
        "id": 6,
        "symbol": "PA",
        "label": "Protected Area",
        "description": "",
        "uri": "",
        "sort_order": 5
      }
    ],
    "lut-moon-phase": [
      {
        "id": 1,
        "symbol": "NM",
        "label": "New moon",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "FQ",
        "label": "First quarter",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "FM",
        "label": "Full moon",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "TQ",
        "label": "Third quarter",
        "description": "",
        "uri": ""
      }
    ],
    "lut-mvg": [
      {
        "id": 1,
        "symbol": "MVG1",
        "label": "Rainforests and Vine Thickets",
        "description": "",
        "uri": "",
        "sort_order": 1
      },
      {
        "id": 2,
        "symbol": "MVG2",
        "label": "Eucalypt Tall Open Forests",
        "description": "",
        "uri": "",
        "sort_order": 2
      },
      {
        "id": 3,
        "symbol": "MVG3",
        "label": "Eucalypt Open Forests",
        "description": "",
        "uri": "",
        "sort_order": 3
      },
      {
        "id": 4,
        "symbol": "MVG4",
        "label": "Eucalypt Low Open Forests",
        "description": "",
        "uri": "",
        "sort_order": 4
      },
      {
        "id": 5,
        "symbol": "MVG5",
        "label": "Eucalypt Woodlands",
        "description": "",
        "uri": "",
        "sort_order": 5
      },
      {
        "id": 6,
        "symbol": "MVG6",
        "label": "Acacia Forests and Woodlands",
        "description": "",
        "uri": "",
        "sort_order": 6
      },
      {
        "id": 7,
        "symbol": "MVG7",
        "label": "Callitris Forests and Woodlands",
        "description": "",
        "uri": "",
        "sort_order": 7
      },
      {
        "id": 8,
        "symbol": "MVG8",
        "label": "Casuarina Forests and Woodlands",
        "description": "",
        "uri": "",
        "sort_order": 8
      },
      {
        "id": 9,
        "symbol": "MVG9",
        "label": "Melaleuca Forests and Woodlands",
        "description": "",
        "uri": "",
        "sort_order": 9
      },
      {
        "id": 10,
        "symbol": "MVG10",
        "label": "Other Forests and Woodlands",
        "description": "",
        "uri": "",
        "sort_order": 10
      },
      {
        "id": 11,
        "symbol": "MVG11",
        "label": "Eucalypt Open Woodlands",
        "description": "",
        "uri": "",
        "sort_order": 14
      },
      {
        "id": 12,
        "symbol": "MVG12",
        "label": "Tropical Eucalypt Woodlands/Grasslands",
        "description": "",
        "uri": "",
        "sort_order": 11
      },
      {
        "id": 13,
        "symbol": "MVG13",
        "label": "Acacia Open Woodlands",
        "description": "",
        "uri": "",
        "sort_order": 15
      },
      {
        "id": 14,
        "symbol": "MVG14",
        "label": "Mallee Woodlands and Shrublands",
        "description": "",
        "uri": "",
        "sort_order": 12
      },
      {
        "id": 15,
        "symbol": "MVG15",
        "label": "Low Closed Forests and Tall Closed Shrublands",
        "description": "",
        "uri": "",
        "sort_order": 18
      },
      {
        "id": 16,
        "symbol": "MVG16",
        "label": "Acacia Shrublands",
        "description": "",
        "uri": "",
        "sort_order": 19
      },
      {
        "id": 17,
        "symbol": "MVG17",
        "label": "Other Shrublands",
        "description": "",
        "uri": "",
        "sort_order": 20
      },
      {
        "id": 18,
        "symbol": "MVG18",
        "label": "Heathlands",
        "description": "",
        "uri": "",
        "sort_order": 21
      },
      {
        "id": 19,
        "symbol": "MVG19",
        "label": "Tussock Grasslands",
        "description": "",
        "uri": "",
        "sort_order": 22
      },
      {
        "id": 20,
        "symbol": "MVG20",
        "label": "Hummock Grasslands",
        "description": "",
        "uri": "",
        "sort_order": 23
      },
      {
        "id": 21,
        "symbol": "MVG21",
        "label": "Other Grasslands, Herblands, Sedgelands and Rushlands",
        "description": "",
        "uri": "",
        "sort_order": 24
      },
      {
        "id": 22,
        "symbol": "MVG22",
        "label": "Chenopod Shrublands, Samphire Shrublands and Forblands",
        "description": "",
        "uri": "",
        "sort_order": 25
      },
      {
        "id": 23,
        "symbol": "MVG23",
        "label": "Mangroves",
        "description": "",
        "uri": "",
        "sort_order": 26
      },
      {
        "id": 24,
        "symbol": "MVG24",
        "label": "Inland Aquatic - freshwater, salt lakes, lagoons",
        "description": "",
        "uri": "",
        "sort_order": 27
      },
      {
        "id": 25,
        "symbol": "MVG25",
        "label": "Cleared, Non-Native Vegetation, Buildings",
        "description": "",
        "uri": "",
        "sort_order": 28
      },
      {
        "id": 26,
        "symbol": "MVG26",
        "label": "Unclassified Native Vegetation",
        "description": "",
        "uri": "",
        "sort_order": 29
      },
      {
        "id": 27,
        "symbol": "MVG27",
        "label": "Naturally Bare - sand, rock, claypan, mudflat",
        "description": "",
        "uri": "",
        "sort_order": 30
      },
      {
        "id": 28,
        "symbol": "MVG28",
        "label": "Sea and Estuaries",
        "description": "",
        "uri": "",
        "sort_order": 31
      },
      {
        "id": 29,
        "symbol": "MVG29",
        "label": "Regrowth, Modified Native Vegetation",
        "description": "",
        "uri": "",
        "sort_order": 32
      },
      {
        "id": 30,
        "symbol": "MVG30",
        "label": "Unclassified Forest",
        "description": "",
        "uri": "",
        "sort_order": 13
      },
      {
        "id": 31,
        "symbol": "MVG31",
        "label": "Other Open Woodlands",
        "description": "",
        "uri": "",
        "sort_order": 17
      },
      {
        "id": 32,
        "symbol": "MVG32",
        "label": "Mallee Open Woodlands and Sparse Mallee Shrublands",
        "description": "",
        "uri": "",
        "sort_order": 16
      },
      {
        "id": 33,
        "symbol": "MVG99",
        "label": "Unknown/No Data",
        "description": "",
        "uri": "",
        "sort_order": 33
      }
    ],
    "lut-nesp-weed-aircraft-position": [
      {
        "id": 1,
        "symbol": "P",
        "label": "Port",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "S",
        "label": "Starboard",
        "description": "",
        "uri": ""
      }
    ],
    "lut-nesp-weed-density-category": [
      {
        "id": 1,
        "symbol": "1",
        "label": "None",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "<1%",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "1-10%",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "10-50%",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "5",
        "label": ">50%",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "6",
        "label": "Present",
        "description": "",
        "uri": ""
      }
    ],
    "lut-nesp-weed-survey-type": [
      {
        "id": 1,
        "symbol": "D",
        "label": "Desktop Setup",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "F",
        "label": "Field Survey",
        "description": "",
        "uri": ""
      }
    ],
    "lut-nesp-weed-target": [
      {
        "id": 1,
        "symbol": "GAG",
        "label": "Gamba grass",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "OH",
        "label": "Olive Hymenahcne",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "PG",
        "label": "Para grass",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "MS",
        "label": "Mission grass",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "GRG",
        "label": "Grader grass",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "MM",
        "label": "Mimosa",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "SW",
        "label": "Siam weed",
        "description": "",
        "uri": ""
      }
    ],
    "lut-observer-confidence": [
      {
        "id": 1,
        "symbol": "High",
        "label": "High",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Intermediate",
        "label": "Intermediate",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Low",
        "label": "Low",
        "description": "",
        "uri": ""
      }
    ],
    "lut-observer-position": [
      {
        "id": 1,
        "symbol": "Front left",
        "label": "Front left",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Front right",
        "label": "Front right",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Rear left",
        "label": "Rear left",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Rear right",
        "label": "Rear right",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "Rear centre  ",
        "label": "Rear centre  ",
        "description": "",
        "uri": ""
      }
    ],
    "lut-observer-role": [
      {
        "id": 1,
        "symbol": "S",
        "label": "Spotter",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "D",
        "label": "Driver",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "D1",
        "label": "Data entry",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-opportune-voucher-type-tier-1": [
      {
        "id": 1,
        "symbol": "AT",
        "label": "Animal tissue",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Blood",
        "label": "Blood",
        "description": "",
        "uri": "https://linked.data.gov.au/def/nrm/9430fb9f-e9e0-5c2b-950c-e525fedb6908"
      },
      {
        "id": 3,
        "symbol": "Carcass",
        "label": "Carcass",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Plant",
        "label": "Plant",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "PT",
        "label": "Plant tissue",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "Regurgitant",
        "label": "Regurgitant",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "Scat",
        "label": "Scat",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "Other",
        "label": "Other (specify)",
        "description": "",
        "uri": ""
      }
    ],
    "lut-opportune-voucher-type-tier-2": [
      {
        "id": 1,
        "symbol": "ECOP",
        "label": "Ear clip or punch",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Feather",
        "label": "Feather",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "FF",
        "label": "Fur/hair",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Heart",
        "label": "Heart",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "Kidney",
        "label": "Kidney",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "Liver",
        "label": "Liver",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "Spleen",
        "label": "Spleen",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "Muscle",
        "label": "Muscle",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "Fat",
        "label": "Fat",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "Scale",
        "label": "Scale",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "Skin",
        "label": "Skin",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "Skull",
        "label": "Skull",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "Bone",
        "label": "Bone",
        "description": "",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "SC",
        "label": "Stomach contents",
        "description": "",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "Tail",
        "label": "Tail",
        "description": "",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "Toe",
        "label": "Toe",
        "description": "",
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "Wing",
        "label": "Wing",
        "description": "",
        "uri": ""
      },
      {
        "id": 18,
        "symbol": "BS",
        "label": "Blood smear",
        "description": "",
        "uri": ""
      },
      {
        "id": 19,
        "symbol": "WB",
        "label": "Whole blood",
        "description": "",
        "uri": ""
      },
      {
        "id": 20,
        "symbol": "Complete",
        "label": "Complete",
        "description": "",
        "uri": ""
      },
      {
        "id": 21,
        "symbol": "Partial",
        "label": "Partial",
        "description": "",
        "uri": ""
      }
    ],
    "lut-pest-wild-dog": [
      {
        "id": 1,
        "symbol": "DCLF",
        "label": "Dog (Canis lupus familiaris) ",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "DCLD",
        "label": "Dingo (Canis lupus dingo)",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "DDH",
        "label": "Dingo-Dog hybrid",
        "description": "",
        "uri": ""
      }
    ],
    "lut-photopoints-protocol-variant": [
      {
        "id": 1,
        "symbol": "lite",
        "label": "Lite",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "full",
        "label": "Full",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "ODL",
        "label": "On-Device Lite",
        "description": "",
        "uri": ""
      }
    ],
    "lut-plot-area": [
      {
        "id": 1,
        "symbol": "C",
        "label": "Core monitoring plot",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "F",
        "label": "Fauna plot",
        "description": "",
        "uri": ""
      }
    ],
    "lut-plot-corner-and-centre": [
      {
        "id": 1,
        "symbol": "NW",
        "label": "North West",
        "description": "The spatial point location, denoting the 'northwest' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/d14dc71e-156d-5e64-95eb-d28dd774ed5d"
      },
      {
        "id": 2,
        "symbol": "NE",
        "label": "North East",
        "description": "The spatial point location, denoting the 'northeast' end or orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/3dc4bc55-b40a-5ed9-94ba-697d59489e67"
      },
      {
        "id": 3,
        "symbol": "SW",
        "label": "South West",
        "description": "The spatial point location, denoting the 'southwest' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, coarse woody debris transect, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/afa07308-191b-5daf-a106-c3208a477155"
      },
      {
        "id": 4,
        "symbol": "SE",
        "label": "South East",
        "description": "The spatial point location, denoting the 'southeast' end or the orientation of the study plot, e.g. in a basal sweep sampling point, camera trap point, coarse woody debris transect, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/1eb0f9ed-a928-527c-8724-4a636db55b9d"
      },
      {
        "id": 5,
        "symbol": "C",
        "label": "Centre",
        "description": "The spatial point location, denoting the 'centre' of the study plot, e.g. in a basal sweep sampling point, camera trap point, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/df58bcc5-34d9-5020-a519-bc83b18824db"
      },
      {
        "id": 6,
        "symbol": "O",
        "label": "Other",
        "description": "Other drone base station location",
        "uri": ""
      }
    ],
    "lut-plot-dimensions": [
      {
        "id": 1,
        "symbol": "100m",
        "label": "100 x 100",
        "description": "100 m by 100 m (1 hectare)",
        "uri": ""
      }
    ],
    "lut-plot-layout-marker": [
      {
        "id": 1,
        "symbol": "SD",
        "label": "Star dropper",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "SSS",
        "label": "Survey stake - steel",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "SSW",
        "label": "Survey stake - wood",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "PS",
        "label": "Tent peg - steel",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "PW",
        "label": "Tent peg - wood",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-plot-nearest-infrastructure": [
      {
        "id": 1,
        "symbol": "AC",
        "label": "Access tracks",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Q",
        "label": "Quarry",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "D",
        "label": "Drain",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "FL",
        "label": "Fence line",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "PL",
        "label": "Power line",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "R",
        "label": "Road",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "WP",
        "label": "Watering point",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "H",
        "label": "Homestead",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-plot-points": [
      {
        "id": 1,
        "symbol": "C",
        "description": "Centre",
        "label": "Centre",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "E1",
        "description": "East 1",
        "label": "East 1",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "E2",
        "description": "East 2",
        "label": "East 2",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "E3",
        "description": "East 3",
        "label": "East 3",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "E4",
        "description": "East 4",
        "label": "East 4",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "E5",
        "description": "East 5",
        "label": "East 5",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "S1",
        "description": "South 1",
        "label": "South 1",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "S2",
        "description": "South 2",
        "label": "South 2",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "S3",
        "description": "South 3",
        "label": "South 3",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "S4",
        "description": "South 4",
        "label": "South 4",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "S5",
        "description": "South 5",
        "label": "South 5",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "W1",
        "description": "West 1",
        "label": "West 1",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "W2",
        "description": "West 2",
        "label": "West 2",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "W3",
        "description": "West 3",
        "label": "West 3",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "W4",
        "description": "West 4",
        "label": "West 4",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "W5",
        "description": "West 5",
        "label": "West 5",
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "N1",
        "description": "North 1",
        "label": "North 1",
        "uri": ""
      },
      {
        "id": 18,
        "symbol": "N2",
        "description": "North 2",
        "label": "North 2",
        "uri": ""
      },
      {
        "id": 19,
        "symbol": "N3",
        "description": "North 3",
        "label": "North 3",
        "uri": ""
      },
      {
        "id": 20,
        "symbol": "N4",
        "description": "North 4",
        "label": "North 4",
        "uri": ""
      },
      {
        "id": 21,
        "symbol": "N5",
        "description": "North 5",
        "label": "North 5",
        "uri": ""
      },
      {
        "id": 22,
        "symbol": "SW",
        "description": "South West",
        "label": "South West",
        "uri": ""
      },
      {
        "id": 23,
        "symbol": "SE",
        "description": "South East",
        "label": "South East",
        "uri": ""
      },
      {
        "id": 24,
        "symbol": "NW",
        "description": "North West",
        "label": "North West",
        "uri": ""
      },
      {
        "id": 25,
        "symbol": "NE",
        "description": "North East",
        "label": "North East",
        "uri": ""
      }
    ],
    "lut-plot-relief": [
      {
        "id": 1,
        "symbol": "M",
        "label": "Very high (>300 m)",
        "description": "Refers to the soil relief size class. Relief is the difference between the high and low points of the land surface. Landform patterns with a very high relief are mountains (such as volcano).",
        "uri": "https://linked.data.gov.au/def/nrm/7a02f61b-b7e6-5573-8167-8cf5ac0d66fe"
      },
      {
        "id": 2,
        "symbol": "H",
        "label": "High (90–300 m)",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "L",
        "label": "Low (30–90 m)",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "R",
        "label": "Very low (9–30 m)",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "P",
        "label": "Extremely low (<9 m)",
        "description": "Refers to the soil relief size class. Relief is the difference between the high and low points of the land surface. Landform patterns with extremely low relief are a Plain (such as pediment, pediplain, sheet-flood fan, alluvial fan, tidal flat, etc.)",
        "uri": "https://linked.data.gov.au/def/nrm/5b38b1ec-de00-51cc-a081-63f438ddf67f"
      }
    ],
    "lut-plot-type": [
      {
        "id": 1,
        "symbol": "C",
        "label": "Control",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "I",
        "label": "Impact",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "NA",
        "label": "Not applicable",
        "description": "Refers to any categorical values/attributes that are 'Not Applicable' to a particular observation in the study.",
        "uri": "https://linked.data.gov.au/def/nrm/5488c9ea-e3a2-5348-887b-065d29b848ab"
      }
    ],
    "lut-preservation-type": [
      {
        "id": 1,
        "symbol": "DS",
        "label": "Dried and stored",
        "description": "This type or method of preservation of fauna samples involves dehydration of specimens using sun, heaters or silica gel.",
        "uri": "https://linked.data.gov.au/def/nrm/0a1bbc95-bfa2-5ef6-9592-3065681cad93"
      },
      {
        "id": 2,
        "symbol": "E1",
        "label": "Ethanol 95%",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "E2",
        "label": "Ethanol 80%",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "E3",
        "label": "Ethanol 70%",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "EF",
        "label": "Ethanol/formalin mixed solution",
        "description": "The method of preservation of invertebrate fauna samples, i.e., with the use of a mixture of Ethanol and formalin solution.",
        "uri": "https://linked.data.gov.au/def/nrm/84404b43-9cac-5523-b723-f97d871de9f6"
      },
      {
        "id": 6,
        "symbol": "PG",
        "label": "Propylene glycol",
        "description": "A clear, colorless, viscous organic solvent and diluent used in pharmaceutical preparations and as a preservative.",
        "uri": "https://linked.data.gov.au/def/nrm/fe471531-c329-5d14-94e8-32cea05e0313"
      },
      {
        "id": 7,
        "symbol": "UPG",
        "label": "Undiluted propylene glycol",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "F1",
        "label": "Formalin (10% buffered solution)",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "FZ",
        "label": "Frozen",
        "description": "Refers to the method of preservation of fauna specimens by freezing them in domestic freezers (at -20°C).",
        "uri": "https://linked.data.gov.au/def/nrm/7af6b4ff-45f3-5ded-9ebe-36817f60541c"
      },
      {
        "id": 10,
        "symbol": "LN",
        "label": "Liquid nitrogen",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "MS",
        "label": "Methylated spirits",
        "description": "Denatured alcohol, with a mixture of ethanol with at least 10% or more methanol added to it.",
        "uri": "https://linked.data.gov.au/def/nrm/c67d7f7f-211f-52cd-87d9-9958f1fc08cd"
      },
      {
        "id": 12,
        "symbol": "OS",
        "label": "Other (specify)",
        "description": "Other types of liquid preservative used to store invertebrate samples.",
        "uri": "https://linked.data.gov.au/def/nrm/565511a8-c50b-53fb-8f59-995e52a01ed2"
      },
      {
        "id": 13,
        "symbol": "PD",
        "label": "Pressed and dried",
        "description": "",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "RF",
        "label": "Refrigerated",
        "description": "This method involves the storage of biological specimens in refrigerator (0-4°C).",
        "uri": "https://linked.data.gov.au/def/nrm/a0553acd-eba3-53d7-aac2-9d825fea7fa1"
      },
      {
        "id": 15,
        "symbol": "SS",
        "label": "Saline solution",
        "description": "A solution made of salt and water. Saline usually refers to normal or physiological saline, which is an aqueous solution containing 0.9% sodium chloride.",
        "uri": "https://linked.data.gov.au/def/nrm/702fde58-0761-573d-975a-ff932044b012"
      },
      {
        "id": 16,
        "symbol": "US",
        "label": "Unspecified",
        "description": "Refers to NO specific method or type stated explicitly or in detail.",
        "uri": "https://linked.data.gov.au/def/nrm/e64762bf-32d2-545d-92d6-397a075acb34"
      },
      {
        "id": 17,
        "symbol": "Fs",
        "label": "Fixed (smear)",
        "description": "This method is generally for preservation of fauna tissue or blood samples, by fixing it usually as a simple smear on glass slides with fixatives.",
        "uri": "https://linked.data.gov.au/def/nrm/e417f294-9da1-5003-a4ef-f193d12b11bf"
      }
    ],
    "lut-program": [
      {
        "id": 1,
        "symbol": "NLP",
        "label": "National Landcare Program",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "A",
        "label": "Ausplot Rangelands",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "T",
        "label": "Transects",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "L",
        "label": "LTERN",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "F",
        "label": "Ausplots Forests",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "G",
        "label": "General Use",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "S",
        "label": "Super Sites",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "TRA",
        "label": "Training",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "M",
        "label": "MERIT",
        "description": "A generic program for MERIT use, as they can have many 'programs' for a given plot",
        "uri": ""
      }
    ],
    "lut-proliferation-eye": [
      {
        "id": 1,
        "symbol": "N",
        "label": "Normal",
        "description": "Normal",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "MI",
        "label": "Mild",
        "description": "Exuberant proliferative vegetative conjunctival hypertrophy occupying < 50% of conjunctiva and nictitating membrane total",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "MO",
        "label": "Moderate",
        "description": "Exuberant proliferative vegetative conjunctival hypertrophy occupying ≥ 50% of conjunctiva and nictitating membrane total but extending across the cornea ≤ 50%",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "S",
        "label": "Severe",
        "description": "Exuberant proliferative vegetative conjunctival hypertrophy occupying > 50% of the conjunctiva and nictitating membrane that obscures the cornea > 50%",
        "uri": ""
      }
    ],
    "lut-protocol-family": [
      {
        "id": 1,
        "symbol": "E",
        "label": "EMSA",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-protocol-type": [
      {
        "id": 1,
        "symbol": "S",
        "label": "Standard",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "E",
        "label": "Enhanced",
        "description": "",
        "uri": ""
      }
    ],
    "lut-protocol-variant": [
      {
        "id": 1,
        "symbol": "lite",
        "label": "Lite",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "full",
        "label": "Full",
        "description": "",
        "uri": ""
      }
    ],
    "lut-published-status": [
      {
        "id": 1,
        "symbol": "Complete",
        "label": "Complete",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Published",
        "label": "Published",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Pending publishing",
        "label": "Pending publishing",
        "description": "",
        "uri": ""
      }
    ],
    "lut-recruitment-study-area-type": [
      {
        "id": 1,
        "symbol": "Belt transect",
        "label": "Belt transect",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Population extent",
        "label": "Population extent",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Plot",
        "label": "Plot",
        "description": "",
        "uri": ""
      }
    ],
    "lut-recruitment-transect": [
      {
        "id": 1,
        "symbol": "1",
        "label": "1",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "2",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "3",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "4",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "5",
        "label": "5",
        "description": "",
        "uri": ""
      }
    ],
    "lut-recruitment-tree-status": [
      {
        "id": 1,
        "symbol": "Alive",
        "label": "Alive",
        "description": "Refers to the status of plant growth, i.e., alive.",
        "uri": "https://linked.data.gov.au/def/nrm/00e4dd62-fabe-5fb4-83b1-e2cf93e6a792"
      },
      {
        "id": 2,
        "symbol": "Dead",
        "label": "Dead",
        "description": "Refers to the status of plant growth, i.e. dead.",
        "uri": "https://linked.data.gov.au/def/nrm/2fd1ed97-3e7b-59a0-8d93-9be76f276071"
      },
      {
        "id": 3,
        "symbol": "Resprouting",
        "label": "Resprouting",
        "description": "The dominant 'Growth-stage'. Tree description: Woody perennial which is resprouting after significant loss of foliage. Shrub description: Woody perennial which is resprouting after significant loss of foliage.",
        "uri": "https://linked.data.gov.au/def/nrm/e237bdc2-8d42-52ba-9885-ee9ab013106d"
      }
    ],
    "lut-revisit-reason": [
      {
        "id": 1,
        "symbol": "PE",
        "label": "Phenological event"
      },
      {
        "id": 2,
        "symbol": "SC",
        "label": "Seasonal change"
      },
      {
        "id": 3,
        "symbol": "MD",
        "label": "Major disturbance"
      },
      {
        "id": 4,
        "symbol": "CE",
        "label": "Climatic event"
      },
      {
        "id": 5,
        "symbol": "OM",
        "label": "Ongoing monitoring"
      },
      {
        "id": 6,
        "symbol": "O",
        "label": "Other"
      }
    ],
    "lut-rump": [
      {
        "id": 1,
        "symbol": "0",
        "label": "0",
        "description": "Normal",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "1",
        "description": "Slight discolouration of fur around cloaca; evidence of mild urine leakage and slight odour",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "2",
        "description": "Slight discolouration of fur around cloaca/tail area; Occasional urine dribble; Mild odour",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "3",
        "description": "Discolouration of the tail area fur more evident; Stronger odour; Urine discharge, greasy texture evident on cloaca/tail",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "4",
        "label": "4",
        "description": "Fur stain, greasy, darkened; Strong, pungent ‘wet bottom smell’; Inflammation of cloacal margins, clitoris, vestibule; Discharge containing urinary debris",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "5",
        "label": "5",
        "description": "Stained, greasy fur covering a large areaVery strong pungent acidic smell; Blood in urine; Coat brown, dry and lustreless; Cloaca and tail area swollen; Grinding teeth ",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "6",
        "label": "6",
        "description": "Stained, greasy, wet matted fur around rump/cloaca area; Ulcerated oedematous cloaca/tail area; Constant purulent discharge; Crying, straining, distressed, flat, ear flicking, teeth grinding",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "7",
        "label": "7",
        "description": "Progressive decline; Myiasis",
        "uri": ""
      }
    ],
    "lut-scat": [
      {
        "id": 1,
        "symbol": "CLS",
        "label": "Counted and left in situ",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Removed",
        "label": "Removed",
        "description": "",
        "uri": ""
      }
    ],
    "lut-select-protocol-variant": [
      {
        "id": 1,
        "symbol": "S",
        "label": "Standard",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "E",
        "label": "Enhanced",
        "description": "",
        "uri": ""
      }
    ],
    "lut-sightability-distance": [
      {
        "id": 1,
        "symbol": "<20",
        "label": "<20",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "20-40",
        "label": "20-40",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "40-60",
        "label": "40-60",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "60-80",
        "label": "60-80",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "80 +",
        "label": "80 +",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-sign-age": [
      {
        "id": 1,
        "symbol": "F",
        "label": "Fresh 1-2 days",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "O",
        "label": "Old 3-7 days",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "V",
        "label": "Very old 7+ days",
        "description": "",
        "uri": ""
      }
    ],
    "lut-sign-based-data-type": [
      {
        "id": 1,
        "symbol": "SPO",
        "label": "sign presence only",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "SPA",
        "label": "sign presence by attributable species",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "CBA",
        "label": "count of each sign by attributable species",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "CES",
        "label": "count of each sign",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "AS",
        "label": "age of sign",
        "description": "",
        "uri": ""
      }
    ],
    "lut-sign-based-plot-type": [
      {
        "id": 1,
        "symbol": "C",
        "label": "Core monitoring plot"
      },
      {
        "id": 2,
        "symbol": "F",
        "label": "Fauna plot"
      },
      {
        "id": 3,
        "symbol": "B",
        "label": "Biodiversity plot"
      }
    ],
    "lut-sign-based-survey-intent": [
      {
        "id": 1,
        "symbol": "O",
        "label": "Once-off measure",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "R1",
        "label": "Repeated measure",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "L",
        "label": "Linked to a pre-control/post-control activity",
        "description": "",
        "uri": ""
      }
    ],
    "lut-sign-type": [
      {
        "id": 1,
        "symbol": "TRA",
        "label": "Tracks",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "SCA",
        "label": "Scats",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "DEN",
        "label": "Dens",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "BUC",
        "label": "Buck heaps",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "WAR",
        "label": "Warrens",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "BUR",
        "label": "Burrows",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "DIG",
        "label": "Diggings",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "WAL",
        "label": "Wallows",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "GRO",
        "label": "Ground rooting",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "RUB",
        "label": "Rubs",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "ROL",
        "label": "Roll pits",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "PUG",
        "label": "Pugging",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-skin-condition": [
      {
        "id": 1,
        "symbol": "HL",
        "label": "Hair loss",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "SM",
        "label": "Scruffy or matted coat",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "FM",
        "label": "Fleas or mites",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "T",
        "label": "Ticks",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "WG",
        "label": "Wart-like growths",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "L",
        "label": "Lesions",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "TRK",
        "label": "Thickened red skin",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "W",
        "label": "Wounds",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "TL",
        "label": "Tail loss",
        "description": "",
        "uri": ""
      }
    ],
    "lut-slope-class": [
      {
        "id": 1,
        "symbol": "LE",
        "label": "Level",
        "slope_percent": "<1%",
        "slope_deg_DMS": "0-0°35'",
        "slope_deg_DD": "0-0.59°",
        "description": "Slope descriptor representing a slope angle in the range from between, 0 - 0°35' degree.",
        "uri": "https://linked.data.gov.au/def/nrm/20c6df85-e2ee-5e2f-ac88-564cdd18506c"
      },
      {
        "id": 2,
        "symbol": "VG",
        "label": "Very gently inclined",
        "slope_percent": "1-3%",
        "slope_deg_DMS": "0°36'-1°45'",
        "slope_deg_DD": "0.60-1.76°",
        "description": "Slope descriptor representing a slope angle in the range from between 0°36' - 1°45' degree.",
        "uri": "https://linked.data.gov.au/def/nrm/7ddc91f5-a5f0-5f3e-b18a-5b693d7100db"
      },
      {
        "id": 3,
        "symbol": "GE",
        "label": "Gently inclined",
        "slope_percent": "3-10%",
        "slope_deg_DMS": "1°46'-5°45'",
        "slope_deg_DD": "1.77-5.76°",
        "description": "Slope descriptor representing a slope angle in the range from between 1°46' - 5°45' degree.",
        "uri": "https://linked.data.gov.au/def/nrm/dd551efe-9dcd-5c46-8735-8f4642e03f5a"
      },
      {
        "id": 4,
        "symbol": "MO",
        "label": "Moderately inclined",
        "slope_percent": "10-32%",
        "slope_deg_DMS": "5°46'-18°",
        "slope_deg_DD": "5.77-17.99°",
        "description": "Slope descriptor representing a slope angle in the range ferom between, 5°46' - 18 degree.",
        "uri": "https://linked.data.gov.au/def/nrm/43c0c157-aa65-5101-a0e9-f12e47e91cc1"
      },
      {
        "id": 5,
        "symbol": "ST",
        "label": "Steep",
        "slope_percent": "32-56%",
        "slope_deg_DMS": "18-30°",
        "slope_deg_DD": "18-29.99°",
        "description": "Slope descriptor representing a slope angle in the range ferom between, 18 - 30 degree.",
        "uri": "https://linked.data.gov.au/def/nrm/5c009ffc-dc41-50b9-8859-a76e3debc09c"
      },
      {
        "id": 6,
        "symbol": "VS",
        "label": "Very steep",
        "slope_percent": "56-100%",
        "slope_deg_DMS": "30-45°",
        "slope_deg_DD": "30-44.99°",
        "description": "Slope descriptor representing a slope angle in the range ferom between, 30 - 45 degree.",
        "uri": "https://linked.data.gov.au/def/nrm/19775f85-619f-5bc2-84cf-db064f4083b4"
      },
      {
        "id": 7,
        "symbol": "PR",
        "label": "Precipitous",
        "slope_percent": "100-300%",
        "slope_deg_DMS": "45-72°",
        "slope_deg_DD": "45-71.99°",
        "description": "Slope descriptor representing a slope angle in the range from between, 45 - 72 degree.",
        "uri": "https://linked.data.gov.au/def/nrm/3fdad807-ff90-516a-a5bb-0a196d86014e"
      },
      {
        "id": 8,
        "symbol": "CL",
        "label": "Cliffed",
        "slope_percent": ">300%",
        "slope_deg_DMS": ">=72°",
        "slope_deg_DD": ">=72°",
        "description": "Slope descriptor representing a slope angle of >72 degree.",
        "uri": "https://linked.data.gov.au/def/nrm/f7e4411c-273c-5169-939d-14bd21285c01"
      }
    ],
    "lut-soil-asc-family-detail": [
      {
        "id": 1,
        "symbol": "NR",
        "label": "Non Water Repellant",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "WR",
        "label": "Water Repellant",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "SR",
        "label": "Strong Water Repellant",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "A",
        "label": "Thin",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "B",
        "label": "Moderately Thick",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "C",
        "label": "Thick",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "D",
        "label": "Very Thick",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "E",
        "label": "Non Gravelly",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "F",
        "label": "Slight Gravelly",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "G",
        "label": "Gravelly",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "H",
        "label": "Moderately Gravelly",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "I",
        "label": "Very Gravelly",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "J",
        "label": "Peaty",
        "description": "",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "K",
        "label": "Sandy",
        "description": "",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "L",
        "label": "Loamy",
        "description": "",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "M",
        "label": "Clay Loamy",
        "description": "",
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "N",
        "label": "Silty",
        "description": "",
        "uri": ""
      },
      {
        "id": 18,
        "symbol": "O",
        "label": "Clayey",
        "description": "",
        "uri": ""
      },
      {
        "id": 19,
        "symbol": "Q",
        "label": " Fine",
        "description": "",
        "uri": ""
      },
      {
        "id": 20,
        "symbol": "R",
        "label": "Medium Fine",
        "description": "",
        "uri": ""
      },
      {
        "id": 21,
        "symbol": "S",
        "label": "Very Fine",
        "description": "",
        "uri": ""
      },
      {
        "id": 22,
        "symbol": "T",
        "label": "Very Shallow",
        "description": "",
        "uri": ""
      },
      {
        "id": 23,
        "symbol": "U",
        "label": "Shallow",
        "description": "",
        "uri": ""
      },
      {
        "id": 24,
        "symbol": "V",
        "label": "Moderately Deep",
        "description": "",
        "uri": ""
      },
      {
        "id": 25,
        "symbol": "W",
        "label": "Deep",
        "description": "",
        "uri": ""
      },
      {
        "id": 26,
        "symbol": "X",
        "label": "Very Deep",
        "description": "",
        "uri": ""
      },
      {
        "id": 27,
        "symbol": "Y",
        "label": "Giant",
        "description": "",
        "uri": ""
      },
      {
        "id": 28,
        "symbol": "NA",
        "label": "Not Recorded"
      },
      {
        "id": 29,
        "symbol": "Z",
        "label": "Materials Absent",
        "description": "",
        "uri": ""
      },
      {
        "id": 30,
        "symbol": "OA",
        "label": "Fibre Peat",
        "description": "",
        "uri": ""
      },
      {
        "id": 31,
        "symbol": "OB",
        "label": "Hemic Peat",
        "description": "",
        "uri": ""
      },
      {
        "id": 32,
        "symbol": "OC",
        "label": "Sapric Peat",
        "description": "",
        "uri": ""
      },
      {
        "id": 33,
        "symbol": "OD",
        "label": "Sandy Peat",
        "description": "",
        "uri": ""
      },
      {
        "id": 34,
        "symbol": "OE",
        "label": "Loam Peat",
        "description": "",
        "uri": ""
      },
      {
        "id": 35,
        "symbol": "OF",
        "label": "Clayey Peat",
        "description": "",
        "uri": ""
      },
      {
        "id": 36,
        "symbol": "P",
        "label": "Granular",
        "description": "",
        "uri": ""
      },
      {
        "id": 37,
        "symbol": "HZ",
        "label": "Ashy",
        "description": "",
        "uri": ""
      },
      {
        "id": 38,
        "symbol": "JZ",
        "label": "Vitric",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soil-location-within-the-landform-element": [
      {
        "id": 1,
        "symbol": "T",
        "label": "Top third of the height of the landform element",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "M",
        "label": "Midle third of the height of the landform element",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "B",
        "label": "Bottom third of the height of the landform element",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soil-root-abundance": [
      {
        "id": 1,
        "symbol": "0",
        "label": "No roots",
        "description": "No roots",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Few",
        "description": "1-10 roots per 0.01m2 for fine and very fine roots, or 1-2 roots per 0.01m2 for medium and coarse roots",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Common",
        "description": "10-25 roots per 0.01m2 for fine and very fine roots, or 2-5 roots per 0.01m2 for medium and coarse roots",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "Many",
        "description": "25-100 roots per 0.01m2 for fine and very fine roots, or >5 roots per 0.01m2 for medium and coarse roots",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "4",
        "label": "Abundant",
        "description": ">200 roots per 0.01m2 for fine and very fine roots, or >5 roots per 0.01m2 for medium and coarse roots",
        "uri": ""
      }
    ],
    "lut-soil-sub-pit-collection-method": [
      {
        "id": 1,
        "symbol": "DG",
        "label": "Differential GNSS",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "HG",
        "label": "Handheld GNSS",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "T",
        "label": "Tablet",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soil-sub-pit-variant": [
      {
        "id": 1,
        "symbol": "lite",
        "label": "Standard",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "full",
        "label": "Enhanced",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soil-survey-variant": [
      {
        "id": 1,
        "symbol": "F",
        "label": "Field",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "L",
        "label": "Lab",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-30cm-sampling-interval": [
      {
        "id": 1,
        "symbol": "1",
        "label": "0.00m - 0.10m",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "0.10m - 0.20m",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "0.20m - 0.30m",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-asc-class": [
      {
        "id": 1,
        "symbol": "AA",
        "label": "Red",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "AB",
        "label": "Brown",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "AC",
        "label": "Yellow",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "AD",
        "label": "Grey",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "AE",
        "label": "Black",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "AF",
        "label": "Dystrophic",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "AG",
        "label": "Mesotrophic",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "AH",
        "label": "Eutrophic",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "AI",
        "label": "Acidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "AJ",
        "label": "Acidic-Mottled",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "AK",
        "label": "Andic",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "AL",
        "label": "Aeric",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "AM",
        "label": "Aquic",
        "description": "",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "AN",
        "label": "Anthroposols",
        "description": "",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "AO",
        "label": "Arenic",
        "description": "",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "AP",
        "label": "Argic",
        "description": "",
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "AQ",
        "label": "Argillaceous",
        "description": "",
        "uri": ""
      },
      {
        "id": 18,
        "symbol": "AR",
        "label": "Basic",
        "description": "",
        "uri": ""
      },
      {
        "id": 19,
        "symbol": "AS",
        "label": "Bauxitic",
        "description": "",
        "uri": ""
      },
      {
        "id": 20,
        "symbol": "AT",
        "label": "Bleached",
        "description": "",
        "uri": ""
      },
      {
        "id": 21,
        "symbol": "AU",
        "label": "Bleached-Acidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 22,
        "symbol": "AV",
        "label": "Bleached-Ferric",
        "description": "",
        "uri": ""
      },
      {
        "id": 23,
        "symbol": "AW",
        "label": "Bleached-Leptic",
        "description": "",
        "uri": ""
      },
      {
        "id": 24,
        "symbol": "AX",
        "label": "Bleached-Magnesic",
        "description": "",
        "uri": ""
      },
      {
        "id": 25,
        "symbol": "AY",
        "label": "Bleached-Manganic",
        "description": "",
        "uri": ""
      },
      {
        "id": 26,
        "symbol": "AZ",
        "label": "Bleached-Mottled",
        "description": "",
        "uri": ""
      },
      {
        "id": 27,
        "symbol": "BA",
        "label": "Bleached-Sodic",
        "description": "",
        "uri": ""
      },
      {
        "id": 28,
        "symbol": "BB",
        "label": "Bleached-Vertic",
        "description": "",
        "uri": ""
      },
      {
        "id": 29,
        "symbol": "BC",
        "label": "Calcareous",
        "description": "",
        "uri": ""
      },
      {
        "id": 30,
        "symbol": "BD",
        "label": "Calcic",
        "description": "",
        "uri": ""
      },
      {
        "id": 31,
        "symbol": "BE",
        "label": "Chernic",
        "description": "",
        "uri": ""
      },
      {
        "id": 32,
        "symbol": "BF",
        "label": "Chernic-Leptic",
        "description": "",
        "uri": ""
      },
      {
        "id": 33,
        "symbol": "BG",
        "label": "Chromosolic",
        "description": "",
        "uri": ""
      },
      {
        "id": 34,
        "symbol": "BH",
        "label": "Crusty",
        "description": "",
        "uri": ""
      },
      {
        "id": 35,
        "symbol": "BI",
        "label": "Densic",
        "description": "",
        "uri": ""
      },
      {
        "id": 36,
        "symbol": "BJ",
        "label": "Duric",
        "description": "",
        "uri": ""
      },
      {
        "id": 37,
        "symbol": "BK",
        "label": "Pedaric",
        "description": "",
        "uri": ""
      },
      {
        "id": 38,
        "symbol": "BL",
        "label": "Endoacidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 39,
        "symbol": "BN",
        "label": "Episodic",
        "description": "",
        "uri": ""
      },
      {
        "id": 40,
        "symbol": "BP",
        "label": "Endohypersodic",
        "description": "",
        "uri": ""
      },
      {
        "id": 41,
        "symbol": "BR",
        "label": "Epihypersodic",
        "description": "",
        "uri": ""
      },
      {
        "id": 42,
        "symbol": "BT",
        "label": "Extratidal",
        "description": "",
        "uri": ""
      },
      {
        "id": 43,
        "symbol": "BU",
        "label": "Ferric",
        "description": "",
        "uri": ""
      },
      {
        "id": 44,
        "symbol": "BV",
        "label": "Arenaceous",
        "description": "",
        "uri": ""
      },
      {
        "id": 45,
        "symbol": "BW",
        "label": "Fibric",
        "description": "",
        "uri": ""
      },
      {
        "id": 46,
        "symbol": "BX",
        "label": "Fluvic",
        "description": "",
        "uri": ""
      },
      {
        "id": 47,
        "symbol": "BY",
        "label": "Fragic",
        "description": "",
        "uri": ""
      },
      {
        "id": 48,
        "symbol": "BZ",
        "label": "Gypsic",
        "description": "",
        "uri": ""
      },
      {
        "id": 49,
        "symbol": "CA",
        "label": "Calcarosol",
        "description": "",
        "uri": ""
      },
      {
        "id": 50,
        "symbol": "CB",
        "label": "Calcarosolic",
        "description": "",
        "uri": ""
      },
      {
        "id": 51,
        "symbol": "CC",
        "label": "Halic",
        "description": "",
        "uri": ""
      },
      {
        "id": 52,
        "symbol": "CD",
        "label": "Haplic",
        "description": "",
        "uri": ""
      },
      {
        "id": 53,
        "symbol": "CE",
        "label": "Hemic",
        "description": "",
        "uri": ""
      },
      {
        "id": 54,
        "symbol": "CF",
        "label": "Histic",
        "description": "",
        "uri": ""
      },
      {
        "id": 55,
        "symbol": "CG",
        "label": "Humic",
        "description": "",
        "uri": ""
      },
      {
        "id": 56,
        "symbol": "CH",
        "label": "Chromosol",
        "description": "",
        "uri": ""
      },
      {
        "id": 57,
        "symbol": "CI",
        "label": "Humic/Humosesquic",
        "description": "",
        "uri": ""
      },
      {
        "id": 58,
        "symbol": "CJ",
        "label": "Humic/Sesquic",
        "description": "",
        "uri": ""
      },
      {
        "id": 59,
        "symbol": "CK",
        "label": "Humose",
        "description": "",
        "uri": ""
      },
      {
        "id": 60,
        "symbol": "CL",
        "label": "Humose-Magnesic",
        "description": "",
        "uri": ""
      },
      {
        "id": 61,
        "symbol": "CM",
        "label": "Humose-Mottled",
        "description": "",
        "uri": ""
      },
      {
        "id": 62,
        "symbol": "CN",
        "label": "Humose-Parapanic",
        "description": "",
        "uri": ""
      },
      {
        "id": 63,
        "symbol": "CO",
        "label": "Humosesquic",
        "description": "",
        "uri": ""
      },
      {
        "id": 64,
        "symbol": "CP",
        "label": "Hypervescent",
        "description": "",
        "uri": ""
      },
      {
        "id": 65,
        "symbol": "CQ",
        "label": "Hypercalcic",
        "description": "",
        "uri": ""
      },
      {
        "id": 66,
        "symbol": "CR",
        "label": "Hypernatric",
        "description": "",
        "uri": ""
      },
      {
        "id": 67,
        "symbol": "CS",
        "label": "Hypersalic",
        "description": "",
        "uri": ""
      },
      {
        "id": 68,
        "symbol": "CU",
        "label": "Epihypersodic-Epiacidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 69,
        "symbol": "CV",
        "label": "Hypocalcic",
        "description": "",
        "uri": ""
      },
      {
        "id": 70,
        "symbol": "CW",
        "label": "Intertidal",
        "description": "",
        "uri": ""
      },
      {
        "id": 71,
        "symbol": "CX",
        "label": "Kurosolic",
        "description": "",
        "uri": ""
      },
      {
        "id": 72,
        "symbol": "CY",
        "label": "Leptic",
        "description": "",
        "uri": ""
      },
      {
        "id": 73,
        "symbol": "CZ",
        "label": "Lithic",
        "description": "",
        "uri": ""
      },
      {
        "id": 74,
        "symbol": "DA",
        "label": "Lithocalcic",
        "description": "",
        "uri": ""
      },
      {
        "id": 75,
        "symbol": "DB",
        "label": "Magnesic",
        "description": "",
        "uri": ""
      },
      {
        "id": 76,
        "symbol": "DC",
        "label": "Manganic",
        "description": "",
        "uri": ""
      },
      {
        "id": 77,
        "symbol": "DD",
        "label": "Marly",
        "description": "",
        "uri": ""
      },
      {
        "id": 78,
        "symbol": "DE",
        "label": "Dermosol",
        "description": "",
        "uri": ""
      },
      {
        "id": 79,
        "symbol": "DF",
        "label": "Massive",
        "description": "",
        "uri": ""
      },
      {
        "id": 80,
        "symbol": "DG",
        "label": "Melacic",
        "description": "",
        "uri": ""
      },
      {
        "id": 81,
        "symbol": "DH",
        "label": "Melacic-Magnesic",
        "description": "",
        "uri": ""
      },
      {
        "id": 82,
        "symbol": "DI",
        "label": "Melacic-Mottled",
        "description": "",
        "uri": ""
      },
      {
        "id": 83,
        "symbol": "DJ",
        "label": "Melacic-Parapanic",
        "description": "",
        "uri": ""
      },
      {
        "id": 84,
        "symbol": "DK",
        "label": "Melanic",
        "description": "",
        "uri": ""
      },
      {
        "id": 85,
        "symbol": "DL",
        "label": "Melanic-Bleached",
        "description": "",
        "uri": ""
      },
      {
        "id": 86,
        "symbol": "DM",
        "label": "Melanic-Mottled",
        "description": "",
        "uri": ""
      },
      {
        "id": 87,
        "symbol": "DN",
        "label": "Melanic-Vertic",
        "description": "",
        "uri": ""
      },
      {
        "id": 88,
        "symbol": "DO",
        "label": "Mellic",
        "description": "",
        "uri": ""
      },
      {
        "id": 89,
        "symbol": "DP",
        "label": "Mesonatric",
        "description": "",
        "uri": ""
      },
      {
        "id": 90,
        "symbol": "DQ",
        "label": "Mottled",
        "description": "",
        "uri": ""
      },
      {
        "id": 91,
        "symbol": "DR",
        "label": "Subhumose",
        "description": "",
        "uri": ""
      },
      {
        "id": 92,
        "symbol": "DT",
        "label": "Oxyaquic",
        "description": "",
        "uri": ""
      },
      {
        "id": 93,
        "symbol": "DU",
        "label": "Paralithic",
        "description": "",
        "uri": ""
      },
      {
        "id": 94,
        "symbol": "DV",
        "label": "Parapanic",
        "description": "",
        "uri": ""
      },
      {
        "id": 95,
        "symbol": "DW",
        "label": "Peaty",
        "description": "",
        "uri": ""
      },
      {
        "id": 96,
        "symbol": "DX",
        "label": "Peaty-Parapanic",
        "description": "",
        "uri": ""
      },
      {
        "id": 97,
        "symbol": "DY",
        "label": "Pedal",
        "description": "",
        "uri": ""
      },
      {
        "id": 98,
        "symbol": "DZ",
        "label": "Petrocalcic",
        "description": "",
        "uri": ""
      },
      {
        "id": 99,
        "symbol": "EA",
        "label": "Petroferric",
        "description": "",
        "uri": ""
      },
      {
        "id": 100,
        "symbol": "EB",
        "label": "Pipey",
        "description": "",
        "uri": ""
      },
      {
        "id": 101,
        "symbol": "EC",
        "label": "Placic",
        "description": "",
        "uri": ""
      },
      {
        "id": 102,
        "symbol": "ED",
        "label": "Redoxic",
        "description": "",
        "uri": ""
      },
      {
        "id": 103,
        "symbol": "EE",
        "label": "Rendic",
        "description": "",
        "uri": ""
      },
      {
        "id": 104,
        "symbol": "EF",
        "label": "Reticulate",
        "description": "",
        "uri": ""
      },
      {
        "id": 105,
        "symbol": "EG",
        "label": "Salic",
        "description": "",
        "uri": ""
      },
      {
        "id": 106,
        "symbol": "EH",
        "label": "Sapric",
        "description": "",
        "uri": ""
      },
      {
        "id": 107,
        "symbol": "EI",
        "label": "Self-Mulching",
        "description": "",
        "uri": ""
      },
      {
        "id": 108,
        "symbol": "EJ",
        "label": "Semiaquic",
        "description": "",
        "uri": ""
      },
      {
        "id": 109,
        "symbol": "EK",
        "label": "Sesquic",
        "description": "",
        "uri": ""
      },
      {
        "id": 110,
        "symbol": "EL",
        "label": "Shelly",
        "description": "",
        "uri": ""
      },
      {
        "id": 111,
        "symbol": "EM",
        "label": "Silpanic",
        "description": "",
        "uri": ""
      },
      {
        "id": 112,
        "symbol": "EN",
        "label": "Snuffy",
        "description": "",
        "uri": ""
      },
      {
        "id": 113,
        "symbol": "EO",
        "label": "Sodic",
        "description": "",
        "uri": ""
      },
      {
        "id": 114,
        "symbol": "EP",
        "label": "Episodic-Epiacidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 115,
        "symbol": "EQ",
        "label": "Sodosolic",
        "description": "",
        "uri": ""
      },
      {
        "id": 116,
        "symbol": "ER",
        "label": "Stratic",
        "description": "",
        "uri": ""
      },
      {
        "id": 117,
        "symbol": "ES",
        "label": "Subnatric",
        "description": "",
        "uri": ""
      },
      {
        "id": 118,
        "symbol": "ET",
        "label": "Subplastic",
        "description": "",
        "uri": ""
      },
      {
        "id": 119,
        "symbol": "EU",
        "label": "Sulphidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 120,
        "symbol": "EV",
        "label": "Sulphuric",
        "description": "",
        "uri": ""
      },
      {
        "id": 121,
        "symbol": "EW",
        "label": "Supratidal",
        "description": "",
        "uri": ""
      },
      {
        "id": 122,
        "symbol": "EX",
        "label": "Vertic",
        "description": "",
        "uri": ""
      },
      {
        "id": 123,
        "symbol": "EY",
        "label": "Humose-Bleached",
        "description": "",
        "uri": ""
      },
      {
        "id": 124,
        "symbol": "EZ",
        "label": "Melacic-Bleached",
        "description": "",
        "uri": ""
      },
      {
        "id": 125,
        "symbol": "FB",
        "label": "Supracalcic",
        "description": "",
        "uri": ""
      },
      {
        "id": 126,
        "symbol": "FC",
        "label": "Melanic-Calcareous",
        "description": "",
        "uri": ""
      },
      {
        "id": 127,
        "symbol": "FD",
        "label": "Natric",
        "description": "",
        "uri": ""
      },
      {
        "id": 128,
        "symbol": "FE",
        "label": "Ferrosol",
        "description": "",
        "uri": ""
      },
      {
        "id": 129,
        "symbol": "FF",
        "label": "Submelacic",
        "description": "",
        "uri": ""
      },
      {
        "id": 130,
        "symbol": "FG",
        "label": "Submelanic",
        "description": "",
        "uri": ""
      },
      {
        "id": 131,
        "symbol": "FH",
        "label": "Palic @",
        "description": "",
        "uri": ""
      },
      {
        "id": 132,
        "symbol": "FJ",
        "label": "Hypergypsic",
        "description": "",
        "uri": ""
      },
      {
        "id": 133,
        "symbol": "FK",
        "label": "Ferric-Duric",
        "description": "",
        "uri": ""
      },
      {
        "id": 134,
        "symbol": "FL",
        "label": "Gypsic-Subplastic",
        "description": "",
        "uri": ""
      },
      {
        "id": 135,
        "symbol": "FM",
        "label": "Epicalcareous-Epihypersodic",
        "description": "",
        "uri": ""
      },
      {
        "id": 136,
        "symbol": "FN",
        "label": "Mottled-Subnatric",
        "description": "",
        "uri": ""
      },
      {
        "id": 137,
        "symbol": "FO",
        "label": "Mottled-Mesonatric",
        "description": "",
        "uri": ""
      },
      {
        "id": 138,
        "symbol": "FP",
        "label": "Mottled-Hypernatric",
        "description": "",
        "uri": ""
      },
      {
        "id": 139,
        "symbol": "FQ",
        "label": "Dermosolic",
        "description": "",
        "uri": ""
      },
      {
        "id": 140,
        "symbol": "FR",
        "label": "Kandosolic",
        "description": "",
        "uri": ""
      },
      {
        "id": 141,
        "symbol": "FT",
        "label": "Terric",
        "description": "",
        "uri": ""
      },
      {
        "id": 142,
        "symbol": "FU",
        "label": "Melacic-Basic",
        "description": "",
        "uri": ""
      },
      {
        "id": 143,
        "symbol": "FV",
        "label": "Melanic-Acidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 144,
        "symbol": "FW",
        "label": "Faunic",
        "description": "",
        "uri": ""
      },
      {
        "id": 145,
        "symbol": "FX",
        "label": "Lutaceous",
        "description": "",
        "uri": ""
      },
      {
        "id": 146,
        "symbol": "FY",
        "label": "Epicalcareous",
        "description": "",
        "uri": ""
      },
      {
        "id": 147,
        "symbol": "FZ",
        "label": "Endocalcareous",
        "description": "",
        "uri": ""
      },
      {
        "id": 148,
        "symbol": "GA",
        "label": "Epiacidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 149,
        "symbol": "GB",
        "label": "Epicalcareous-Endohypersodic",
        "description": "",
        "uri": ""
      },
      {
        "id": 150,
        "symbol": "GC",
        "label": "Melacic-Reticulate",
        "description": "",
        "uri": ""
      },
      {
        "id": 151,
        "symbol": "GD",
        "label": "Peaty-Placic",
        "description": "",
        "uri": ""
      },
      {
        "id": 152,
        "symbol": "GE",
        "label": "Ferric-Petroferric",
        "description": "",
        "uri": ""
      },
      {
        "id": 153,
        "symbol": "GF",
        "label": "Regolithic",
        "description": "",
        "uri": ""
      },
      {
        "id": 154,
        "symbol": "GG",
        "label": "Episodic-Endoacidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 155,
        "symbol": "GH",
        "label": "Episodic-Epicalcareous",
        "description": "",
        "uri": ""
      },
      {
        "id": 156,
        "symbol": "GI",
        "label": "Episodic-Endocalcareous",
        "description": "",
        "uri": ""
      },
      {
        "id": 157,
        "symbol": "GJ",
        "label": "Epicalcareous-Endoacidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 158,
        "symbol": "GK",
        "label": "Epiacidic-Mottled",
        "description": "",
        "uri": ""
      },
      {
        "id": 159,
        "symbol": "GL",
        "label": "Endoacidic-Mottled",
        "description": "",
        "uri": ""
      },
      {
        "id": 160,
        "symbol": "GM",
        "label": "Endocalcareous-Endohypersodic",
        "description": "",
        "uri": ""
      },
      {
        "id": 161,
        "symbol": "GN",
        "label": "Epihypersodic-Endoacidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 162,
        "symbol": "GO",
        "label": "Epihypersodic-Endocalcareous",
        "description": "",
        "uri": ""
      },
      {
        "id": 163,
        "symbol": "GP",
        "label": "Magnesic-Natric",
        "description": "",
        "uri": ""
      },
      {
        "id": 164,
        "symbol": "GQ",
        "label": "Episodic-Gypsic",
        "description": "",
        "uri": ""
      },
      {
        "id": 165,
        "symbol": "GR",
        "label": "Rudosolic",
        "description": "",
        "uri": ""
      },
      {
        "id": 166,
        "symbol": "GS",
        "label": "Epipedal",
        "description": "",
        "uri": ""
      },
      {
        "id": 167,
        "symbol": "GT",
        "label": "Tenosolic",
        "description": "",
        "uri": ""
      },
      {
        "id": 168,
        "symbol": "GU",
        "label": "Humose-Calcareous",
        "description": "",
        "uri": ""
      },
      {
        "id": 169,
        "symbol": "GV",
        "label": "Lutic",
        "description": "",
        "uri": ""
      },
      {
        "id": 170,
        "symbol": "GW",
        "label": "Ferric-Acidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 171,
        "symbol": "GX",
        "label": "Manganic-Acidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 172,
        "symbol": "GY",
        "label": "Humose-Acidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 173,
        "symbol": "HA",
        "label": "Melanic-Sodic",
        "description": "",
        "uri": ""
      },
      {
        "id": 174,
        "symbol": "HB",
        "label": "Mottled-Sodic",
        "description": "",
        "uri": ""
      },
      {
        "id": 175,
        "symbol": "HC",
        "label": "Ferric-Sodic",
        "description": "",
        "uri": ""
      },
      {
        "id": 176,
        "symbol": "HD",
        "label": "Rudaceous",
        "description": "",
        "uri": ""
      },
      {
        "id": 177,
        "symbol": "HE",
        "label": "Endocalcareous-Mottled",
        "description": "",
        "uri": ""
      },
      {
        "id": 178,
        "symbol": "HF",
        "label": "Tephric",
        "description": "",
        "uri": ""
      },
      {
        "id": 179,
        "symbol": "HG",
        "label": "Carbic",
        "description": "",
        "uri": ""
      },
      {
        "id": 180,
        "symbol": "HH",
        "label": "Clastic",
        "description": "",
        "uri": ""
      },
      {
        "id": 181,
        "symbol": "HI",
        "label": "Colluvic",
        "description": "",
        "uri": ""
      },
      {
        "id": 182,
        "symbol": "HJ",
        "label": "Lithosolic",
        "description": "",
        "uri": ""
      },
      {
        "id": 183,
        "symbol": "HK",
        "label": "Supravescent",
        "description": "",
        "uri": ""
      },
      {
        "id": 184,
        "symbol": "HL",
        "label": "Episulphidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 185,
        "symbol": "HM",
        "label": "Episulphidic-Petrocalcic",
        "description": "",
        "uri": ""
      },
      {
        "id": 186,
        "symbol": "HN",
        "label": "Densic-Placic",
        "description": "",
        "uri": ""
      },
      {
        "id": 187,
        "symbol": "HO",
        "label": "Acidic-Sodic",
        "description": "",
        "uri": ""
      },
      {
        "id": 188,
        "symbol": "HR",
        "label": "Cumulic",
        "description": "",
        "uri": ""
      },
      {
        "id": 189,
        "symbol": "HS",
        "label": "Hortic",
        "description": "",
        "uri": ""
      },
      {
        "id": 190,
        "symbol": "HT",
        "label": "Garbic",
        "description": "",
        "uri": ""
      },
      {
        "id": 191,
        "symbol": "HU",
        "label": "Urbic",
        "description": "",
        "uri": ""
      },
      {
        "id": 192,
        "symbol": "HV",
        "label": "Dredgic",
        "description": "",
        "uri": ""
      },
      {
        "id": 193,
        "symbol": "HW",
        "label": "Spolic",
        "description": "",
        "uri": ""
      },
      {
        "id": 194,
        "symbol": "HX",
        "label": "Scalpic",
        "description": "",
        "uri": ""
      },
      {
        "id": 195,
        "symbol": "HY",
        "label": "Hydrosol",
        "description": "",
        "uri": ""
      },
      {
        "id": 196,
        "symbol": "IA",
        "label": "Inceptic",
        "description": "",
        "uri": ""
      },
      {
        "id": 197,
        "symbol": "IB",
        "label": "Epibasic",
        "description": "",
        "uri": ""
      },
      {
        "id": 198,
        "symbol": "IC",
        "label": "Ceteric",
        "description": "",
        "uri": ""
      },
      {
        "id": 199,
        "symbol": "ID",
        "label": "Subpeaty",
        "description": "",
        "uri": ""
      },
      {
        "id": 200,
        "symbol": "IE",
        "label": "Effervescent",
        "description": "",
        "uri": ""
      },
      {
        "id": 201,
        "symbol": "IF",
        "label": "Folic",
        "description": "",
        "uri": ""
      },
      {
        "id": 202,
        "symbol": "IG",
        "label": "Humosesquic/Sesquic",
        "description": "",
        "uri": ""
      },
      {
        "id": 203,
        "symbol": "IH",
        "label": "Humic/Alsilic",
        "description": "",
        "uri": ""
      },
      {
        "id": 204,
        "symbol": "IJ",
        "label": "Modic",
        "description": "",
        "uri": ""
      },
      {
        "id": 205,
        "symbol": "IL",
        "label": "Sesqui-Nodular",
        "description": "",
        "uri": ""
      },
      {
        "id": 206,
        "symbol": "IM",
        "label": "Calcenic",
        "description": "",
        "uri": ""
      },
      {
        "id": 207,
        "symbol": "IS",
        "label": "Ferric-Reticulate",
        "description": "",
        "uri": ""
      },
      {
        "id": 208,
        "symbol": "IT",
        "label": "Fusic",
        "description": "",
        "uri": ""
      },
      {
        "id": 209,
        "symbol": "IU",
        "label": "Subtidal",
        "description": "",
        "uri": ""
      },
      {
        "id": 210,
        "symbol": "IV",
        "label": "Subaqueous",
        "description": "",
        "uri": ""
      },
      {
        "id": 211,
        "symbol": "IW",
        "label": "Monsulfidic-Sulfuric",
        "description": "",
        "uri": ""
      },
      {
        "id": 212,
        "symbol": "IX",
        "label": "Monohypersulfidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 213,
        "symbol": "IY",
        "label": "Histic-Hypersulfidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 214,
        "symbol": "IZ",
        "label": "Hypersulfidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 215,
        "symbol": "JA",
        "label": "Monohyposulfidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 216,
        "symbol": "JB",
        "label": "Histic-Hyposulfidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 217,
        "symbol": "JC",
        "label": "Hyposulfidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 218,
        "symbol": "JD",
        "label": "Aquic-Sulfuric",
        "description": "",
        "uri": ""
      },
      {
        "id": 219,
        "symbol": "JE",
        "label": "Aquic-Hypersulfidic",
        "description": "",
        "uri": ""
      },
      {
        "id": 220,
        "symbol": "JF",
        "label": "Ferric-Endohypersodic",
        "description": "",
        "uri": ""
      },
      {
        "id": 221,
        "symbol": "JG",
        "label": "Manganic-Endohypersodic",
        "description": "",
        "uri": ""
      },
      {
        "id": 222,
        "symbol": "JH",
        "label": "Ferric-Endocalcareous",
        "description": "",
        "uri": ""
      },
      {
        "id": 223,
        "symbol": "JI",
        "label": "Manganic-Endocalcareous",
        "description": "",
        "uri": ""
      },
      {
        "id": 224,
        "symbol": "JK",
        "label": "Subhalic",
        "description": "",
        "uri": ""
      },
      {
        "id": 225,
        "symbol": "JL",
        "label": "Tenic",
        "description": "",
        "uri": ""
      },
      {
        "id": 226,
        "symbol": "JM",
        "label": "Arenosolic",
        "description": "",
        "uri": ""
      },
      {
        "id": 227,
        "symbol": "JN",
        "label": "Calsilic",
        "description": "",
        "uri": ""
      },
      {
        "id": 228,
        "symbol": "JP",
        "label": "Kandic",
        "description": "",
        "uri": ""
      },
      {
        "id": 229,
        "symbol": "JQ",
        "label": "Gritty",
        "description": "",
        "uri": ""
      },
      {
        "id": 230,
        "symbol": "JR",
        "label": "Fluvic(introduced in third edition)",
        "description": "",
        "uri": ""
      },
      {
        "id": 231,
        "symbol": "JT",
        "label": "Carbonatic",
        "description": "",
        "uri": ""
      },
      {
        "id": 232,
        "symbol": "JU",
        "label": "Coquinic",
        "description": "",
        "uri": ""
      },
      {
        "id": 233,
        "symbol": "JV1",
        "label": "Gritty-SilpaniC",
        "description": "",
        "uri": ""
      },
      {
        "id": 234,
        "symbol": "JV2",
        "label": "Gritty-Lithic",
        "description": "",
        "uri": ""
      },
      {
        "id": 235,
        "symbol": "JX",
        "label": "Gritty-Paralithic",
        "description": "",
        "uri": ""
      },
      {
        "id": 236,
        "symbol": "YY",
        "label": "Class Undetermined",
        "description": "",
        "uri": ""
      },
      {
        "id": 237,
        "symbol": "ZZ",
        "label": "No Available Class",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-asc-order": [
      {
        "id": 1,
        "symbol": "AN",
        "label": "Anthroposols",
        "description": "Refers to the soil orders based on the Australian Soil Classification. Anthroposols are soils resulting from human activities which have led to a profound modification, truncation or burial of the original soil horizons, or the creation of new soil parent materials by a variety of mechanical means. (Reference: The Australian Soil Classification, Third Edition)",
        "uri": "https://linked.data.gov.au/def/nrm/4e81f233-436c-5125-a357-d4b49bff333e"
      },
      {
        "id": 2,
        "symbol": "RE",
        "label": "Arenosols",
        "description": "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Arenosols- are of \"deep sandy soils\", with predominantly sandy field textures (10% clay content), and with no horizons containing more than 15% clay within the upper 1.0 m of the profile. The presence of argic horizons are the only exception.(Reference: The Australian Soil Classification, Third Edition).",
        "uri": "https://linked.data.gov.au/def/nrm/280331e0-341c-512b-899b-f487bf7eef5f"
      },
      {
        "id": 3,
        "symbol": "CA",
        "label": "Calcarosols",
        "description": "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Calcarosols- are usually calcareous throughout the profile, often highly so. They constitute a widespread and important group of soils in southern Australia. (Reference: The Australian Soil Classification, Third Edition).",
        "uri": "https://linked.data.gov.au/def/nrm/5da640ad-ccae-5645-89a5-a8a23b902ebc"
      },
      {
        "id": 4,
        "symbol": "CH",
        "label": "Chromosols",
        "description": "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Chromosols- are soils with a clear or abrupt textural B horizon and in which the major part of the upper 0.2 m of the B2t horizon (or the major part of the entire B2t horizon if it is less than 0.2 m thick) is not strongly acid. (Reference: The Australian Soil Classification, Third Edition).",
        "uri": "https://linked.data.gov.au/def/nrm/c095ca69-2abf-5d73-b5ca-495dfb50fd9b"
      },
      {
        "id": 5,
        "symbol": "DE",
        "label": "Dermosols",
        "description": "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Dermosols- are soils with B2 horizons that have grade of pedality greater than weak throughout the major part of the horizon. (Reference: The Australian Soil Classification, Third Edition).",
        "uri": "https://linked.data.gov.au/def/nrm/09a09d5a-e23d-5eee-a7a9-9927ebff79dd"
      },
      {
        "id": 6,
        "symbol": "FE",
        "label": "Ferrosols",
        "description": "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Ferrosols- are soils with B2 horizons in which the major part has a free iron oxide content greater than 5% Fe in the fine earth fraction (<2 mm). Soils with a B2 horizon in which at least 0.3m has vertic properties are excluded (see also Comment and footnote in Ferrosols). (Reference: The Australian Soil Classification, Third Edition).",
        "uri": "https://linked.data.gov.au/def/nrm/b594e58d-251c-5fcc-b75b-319a7938d793"
      },
      {
        "id": 7,
        "symbol": "HY",
        "label": "Hydrosols",
        "description": "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Hydrosols- are soils that are saturated in the major part of the soil profile for at least 2-3 months in most years (ie. includes tidal waters).(Reference: The Australian Soil Classification, Third Edition).",
        "uri": "https://linked.data.gov.au/def/nrm/339a0f8c-b6fe-56dd-b435-d8f0d3c77e3a"
      },
      {
        "id": 8,
        "symbol": "KA",
        "label": "Kandosols",
        "description": "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Kandosols- soils which lack strong texture contrast, have massive or only weakly structured B horizons, and are not calcareous throughout. The soils of this order range throughout the continent, often occurring locally as very large areas (Reference: The Australian Soil Classification, Third Edition).",
        "uri": "https://linked.data.gov.au/def/nrm/6a252cf1-d760-5dd5-845f-7c092a612f6a"
      },
      {
        "id": 9,
        "symbol": "KU",
        "label": "Kurosols",
        "description": "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Kurosols- soils with a clear or abrupt textural B horizon and in which the major part of the upper 0.2 m of the B2t horizon (or the major part of the entire B2t horizon if it is less than 0.2 m thick) is strongly acid. (Reference: The Australian Soil Classification, Third Edition).",
        "uri": "https://linked.data.gov.au/def/nrm/b5e96d13-44fa-5ada-8da1-46fc4e50b30d"
      },
      {
        "id": 10,
        "symbol": "OR",
        "label": "Organosols",
        "description": "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Organosols- caters for most soils dominated by organic materials. Although they are found from the wet tropics to the alpine regions, areas are mostly small except in south west Tasmania. (Reference: The Australian Soil Classification, Third Edition).",
        "uri": "https://linked.data.gov.au/def/nrm/3c0b65ab-b74b-5ccc-972a-71e302e57a63"
      },
      {
        "id": 11,
        "symbol": "PO",
        "label": "Podosols",
        "description": "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Podosols- are soils that have a Bs, Bhs or Bh horizon. These horizons may occur either singly or in combination. (Reference: The Australian Soil Classification, Third Edition).",
        "uri": "https://linked.data.gov.au/def/nrm/885c1e7c-c55b-55a4-a139-5efb95cc9e03"
      },
      {
        "id": 12,
        "symbol": "RU",
        "label": "Rudosols",
        "description": "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Rudosols- are soils with negligible (rudimentary), if any, pedologic organisation apart from the minimal development of an A1 horizon or the presence of less than 10% of B horizon material (including pedogenic carbonate) in fissures in the parent rock or saprolite. The soils have a grade of pedality of single grain, massive or weak in the A1 horizon and show no pedological colour change apart from darkening of an A1 horizon. (Reference: The Australian Soil Classification, Third Edition).",
        "uri": "https://linked.data.gov.au/def/nrm/dd038cec-458f-532b-97c8-a917ba5d931c"
      },
      {
        "id": 13,
        "symbol": "SO",
        "label": "Sodosols",
        "description": "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Sodosols- are soils with a clear or abrupt textural B horizon and in which the major part of the upper 0.2 m of the B2t horizon (or the major part of the entire B2t horizon if it is less than 0.2 m thick) is sodic and is not strongly subplastic. (Reference: The Australian Soil Classification, Third Edition).",
        "uri": "https://linked.data.gov.au/def/nrm/5f8a5a25-7f7a-55ff-8619-2a547a32b443"
      },
      {
        "id": 14,
        "symbol": "TE",
        "label": "Tenosols",
        "description": "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Tenosols- are designed to embrace soils with generally only weak pedologic organisation apart from the A horizons, excluding soils that have deep sandy profiles with a field texture of sand, loamy sand or clayey sand in 80% or more of the upper 1.0 m. (Reference: The Australian Soil Classification, Third Edition).",
        "uri": "https://linked.data.gov.au/def/nrm/6e19da3d-db5a-5b5a-b37d-82a86079f73a"
      },
      {
        "id": 15,
        "symbol": "VE",
        "label": "Vertosols",
        "description": "Refers to the soil orders, as listed on the Australian Soil Classification Orders. Vertosols- are clay soils with shrink-swell properties that exhibit strong cracking when dry and at depth have slickensides and/or lenticular peds. (Reference: The Australian Soil Classification, Third Edition).",
        "uri": "https://linked.data.gov.au/def/nrm/219f7ae4-4aed-5ffe-8fc9-82cc83f77d7a"
      },
      {
        "id": 16,
        "symbol": "YY",
        "label": "Class Undetermined",
        "description": "Refers to the soils that cannot be determined to a specific soil order, as per the Australian Soil Classification orders.",
        "uri": "https://linked.data.gov.au/def/nrm/c3e8a0e8-bdeb-5142-b114-94ae0ff86611"
      },
      {
        "id": 17,
        "symbol": "ZZ",
        "label": "No Class Available",
        "description": "Refers to the soils with no specific soil order, as per the Australian Soil Classification orders.",
        "uri": "https://linked.data.gov.au/def/nrm/c786373b-900d-5e82-b438-d8a58f26726e"
      }
    ],
    "lut-soils-biotic-relief-agent": [
      {
        "id": 1,
        "symbol": "N",
        "label": "Animal",
        "description": "The type of relief caused by biotic agents of animal origin in soil. ",
        "uri": "https://linked.data.gov.au/def/nrm/66e330a2-9037-5e57-b749-17bd4e92dc3a"
      },
      {
        "id": 2,
        "symbol": "M",
        "label": "Man",
        "description": "The type of relief caused by biotic agents of human origin in soil. ",
        "uri": "https://linked.data.gov.au/def/nrm/1d659815-2aa8-582e-a28e-f1196afa55f5"
      },
      {
        "id": 3,
        "symbol": "B",
        "label": "Bird",
        "description": "The type of relief caused by biotic agents of bird origin in soil. ",
        "uri": "https://linked.data.gov.au/def/nrm/458c9166-d57e-5e18-993b-2733a89c1b6e"
      },
      {
        "id": 4,
        "symbol": "T",
        "label": "Termite",
        "description": "The type of relief caused by biotic agents such as termite mounds in soil. ",
        "uri": "https://linked.data.gov.au/def/nrm/1dc1d17c-c0f5-5720-ad26-072674ce2c74"
      },
      {
        "id": 5,
        "symbol": "A",
        "label": "Ant",
        "description": "The type of relief caused by biotic agents such as mounds from ants in soil. ",
        "uri": "https://linked.data.gov.au/def/nrm/fcfe527a-1d34-5aa0-9ae8-b85524843b25"
      },
      {
        "id": 6,
        "symbol": "V",
        "label": "Vegetation",
        "description": "The type of relief caused by biotic agents such as vegetation mounds  (e.g., Spinifex or Dillon Bush) in soil. ",
        "uri": "https://linked.data.gov.au/def/nrm/9e34a04c-5128-5cef-aeed-18be9f7f4abe"
      },
      {
        "id": 7,
        "symbol": "O",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      },
      {
        "id": 8,
        "symbol": "N/A",
        "label": "N/A",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-coarse-frag-abundance": [
      {
        "id": 1,
        "symbol": "0",
        "label": "No coarse fragments",
        "description": "Refers to the percentage abundance class '0', i.e., 'No' soil coarse fragments.",
        "uri": "https://linked.data.gov.au/def/nrm/7b351daa-670c-5669-ab48-d4279719b333",
        "sort_order": 1
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Very slightly or very few (< 2%)",
        "description": "Refers to the percentage abundance class '1' on a scale of '0-6' percentage class of soil coarse fragments. '0' being the lowest (no coarse fragments) and '6' being extremely abundant (>90%).",
        "uri": "https://linked.data.gov.au/def/nrm/fe4ad04e-32eb-557d-b59b-6455005184ff",
        "sort_order": 2
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Slightly or few (2-10%)",
        "description": "Refers to the percentage abundance class '2' on a scale of '0-6' percentage class of soil coarse fragments. '0' being the lowest (no coarse fragments) and '6' being extremely abundant (>90%).",
        "uri": "https://linked.data.gov.au/def/nrm/df8792e6-f1eb-5584-b152-ff942f54abbb",
        "sort_order": 3
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "No qualifier or common (10 - 20%)",
        "description": "Refers to the percentage abundance class '3' of soil coarse fragments.",
        "uri": "https://linked.data.gov.au/def/nrm/3ed76135-19e7-555c-9528-7b195720e29f",
        "sort_order": 4
      },
      {
        "id": 5,
        "symbol": "5",
        "label": "Very or abundant (50 - 90%)",
        "description": "Refers to the percentage abundance class '5' on a scale of '0-6' percentage class of soil coarse fragments. '0' being the lowest (no coarse fragments) and '6' being extremely abundant (>90%).",
        "uri": "https://linked.data.gov.au/def/nrm/91fbc097-c683-5f34-929b-3dc3f8b6ffd4",
        "sort_order": 6
      },
      {
        "id": 6,
        "symbol": "4",
        "label": "Moderately or many (20 - 50%)",
        "description": "Refers to the percentage abundance class '4' on a scale of '0-6' percentage class of soil coarse fragments. '0' being the lowest (no coarse fragments) and '6' being extremely abundant (>90%).",
        "uri": "https://linked.data.gov.au/def/nrm/a552f03c-d68b-5e93-ab6b-0555846a1aa5",
        "sort_order": 5
      },
      {
        "id": 7,
        "symbol": "6",
        "label": "Extremely or abundant (> 90%)",
        "description": "Refers to the percentage abundance class of '6' on a scale of '0-6' percentage class of soil coarse fragments. '0' being the lowest (no coarse fragments) and '6' being extremely abundant (>90%).",
        "uri": "https://linked.data.gov.au/def/nrm/b0c7a591-fb76-5bfd-bbab-f096352cab7c",
        "sort_order": 7
      }
    ],
    "lut-soils-coarse-frag-alteration": [
      {
        "id": 1,
        "symbol": "F",
        "label": "Ferruginised",
        "description": "Refers to the type of alterations of soil coarse fragments substrate material. Ferruginised is an iron enriched soil, such as in the lateritic soils.",
        "uri": "https://linked.data.gov.au/def/nrm/69264d3f-4dab-5a31-b691-8862e318d4e2"
      },
      {
        "id": 2,
        "symbol": "L",
        "label": "Kaolinised",
        "description": "Refers to the type of alterations of soil coarse fragments of substrate material. Kaolinised soils are clay enriched, usually pale coloured (e.g., the pallid zone of a laterite profile). ",
        "uri": "https://linked.data.gov.au/def/nrm/34830e2e-8597-59ed-9320-be68cfa96660"
      },
      {
        "id": 3,
        "symbol": "S",
        "label": "Silicified",
        "description": "Refers to the type of alterations of soil coarse fragments substrate material. Silicified soils are silica enriched, and may be associated with deep-weathering profiles not exclusively so; for instance, some limestones may be variably silicified.",
        "uri": "https://linked.data.gov.au/def/nrm/7f19eee8-bf3e-567f-ae83-251d07b9457e"
      },
      {
        "id": 4,
        "symbol": "C",
        "label": "Calcified",
        "description": "Refers to the type of alterations of soil coarse fragments substrate material. Calcified soils are enriched by calcium carbonate, and is not usually associated with deep weathering.",
        "uri": "https://linked.data.gov.au/def/nrm/31a1b5e5-6e09-5ac1-a909-5589c68a9a1a"
      },
      {
        "id": 5,
        "symbol": "O",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      }
    ],
    "lut-soils-coarse-frag-distribution": [
      {
        "id": 1,
        "symbol": "D",
        "label": "Dispersed",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "R",
        "label": "Reoriented",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "S",
        "label": "Stratified",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "U",
        "label": "Undisturbed",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-coarse-frag-shape": [
      {
        "id": 1,
        "symbol": "n/a",
        "label": "Not Applicable",
        "description": "Refers to any categorical values/attributes that are 'Not Applicable' to a particular observation in the study.",
        "uri": "https://linked.data.gov.au/def/nrm/5488c9ea-e3a2-5348-887b-065d29b848ab"
      },
      {
        "id": 2,
        "symbol": "A",
        "label": "Angular",
        "description": "Refers to the shape of soil coarse fragments substrate material. Angular substrates are the least rounded but with high sphericity.",
        "uri": "https://linked.data.gov.au/def/nrm/8c54fd41-7cc8-5dfe-bac9-0376fb7058d5"
      },
      {
        "id": 3,
        "symbol": "AP",
        "label": "Angular platy",
        "description": "Refers to the shape of soil coarse fragments substrate material. Angular platy are substrates that are the least rounded and with least sphericity.",
        "uri": "https://linked.data.gov.au/def/nrm/b3cd8f53-f594-507d-8698-81e1947f39f0"
      },
      {
        "id": 4,
        "symbol": "R",
        "label": "Rounded",
        "description": "Refers to the shape of soil coarse fragments substrate material. Rounded substrates have the highest roundness and sphericity.",
        "uri": "https://linked.data.gov.au/def/nrm/9f2e49e4-06dc-5fac-b892-ed9f6ffc6772"
      },
      {
        "id": 5,
        "symbol": "RP",
        "label": "Rounded platy",
        "description": "Refers to the shape of soil coarse fragments substrate material. Rounded platy substrates have the highest roundness but with least sphericity.",
        "uri": "https://linked.data.gov.au/def/nrm/edad2ce0-d790-5afd-8704-3805068e6250"
      },
      {
        "id": 6,
        "symbol": "RT",
        "label": "Rounded tabular",
        "description": "Refers to the shape of soil coarse fragments substrate material. Rounded tabular substrates have the highest roundness but with low sphericity.",
        "uri": "https://linked.data.gov.au/def/nrm/0de22135-3925-58f5-ad48-4ab6bdf96da4"
      },
      {
        "id": 7,
        "symbol": "S",
        "label": "Subangular",
        "description": "Refers to the shape of soil coarse fragments substrate material. Subangular substrates have low roundness but are of high sphericity.",
        "uri": "https://linked.data.gov.au/def/nrm/aaa2c3d6-0c4a-56e5-a003-1f6e4ca40f04"
      },
      {
        "id": 8,
        "symbol": "SP",
        "label": "Subangular platy",
        "description": "Refers to the shape of soil coarse fragments substrate material. Subangular platy substrates have low roundness and the least sphericity.",
        "uri": "https://linked.data.gov.au/def/nrm/06f1d7c0-b179-5edf-b703-ec134d04e9f5"
      },
      {
        "id": 9,
        "symbol": "ST",
        "label": "Subangular tabular",
        "description": "Refers to the shape of soil coarse fragments substrate material. Subangular tabular substrates have low roundness and sphericity.",
        "uri": "https://linked.data.gov.au/def/nrm/2b732121-d06d-5dba-898d-5821020ad4e4"
      },
      {
        "id": 10,
        "symbol": "U",
        "label": "Subrounded",
        "description": "Refers to the shape of soil coarse fragments substrate material. Subrounded substrates have moderate roundness but moderate to high sphericity.",
        "uri": "https://linked.data.gov.au/def/nrm/9eadb45f-96ea-55dd-99a6-fff8804292c0"
      },
      {
        "id": 11,
        "symbol": "UP",
        "label": "Subrounded platy",
        "description": "Refers to the shape of soil coarse fragments substrate material. Subrounded platy substrates have moderate roundness but lowest sphericity.",
        "uri": "https://linked.data.gov.au/def/nrm/0bc37a8e-8b55-5916-a0af-227cde52e082"
      },
      {
        "id": 12,
        "symbol": "UT",
        "label": "Subrounded tabular",
        "description": "Refers to the shape of soil coarse fragments substrate material. Subrounded tabular substrates have moderate roundness and low sphericity.",
        "uri": "https://linked.data.gov.au/def/nrm/20971a94-6c4b-599b-866c-97793a6be2c1"
      },
      {
        "id": 13,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      },
      {
        "id": 14,
        "symbol": "AT",
        "label": "Angular tabular",
        "description": "Refers to the shape of soil coarse fragments substrate material. Angular tabular are substrates that are the least rounded and with low sphericity.",
        "uri": "https://linked.data.gov.au/def/nrm/f890df5e-90d2-5725-ba6d-a7adda689b71"
      }
    ],
    "lut-soils-coarse-frag-size": [
      {
        "id": 1,
        "symbol": "1",
        "label": "Fine gravelly or small pebbles (2-6mm)",
        "description": "Refers to the size class of '1' on a scale of '1-7' size class of the soil coarse fragments substrate. '1' being the lowest (fine gravelly) and '7' being the highest (large boulders).",
        "uri": "https://linked.data.gov.au/def/nrm/f18f0758-20d4-516f-a7b5-f01c884520b2"
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "Medium gravelly or medium pebbles (6-20mm)",
        "description": "Refers to the size class of '2' on a scale of '1-7' size class of the soil coarse fragments substrate. '1' being the lowest (fine gravelly) and '7' being the highest (large boulders).",
        "uri": "https://linked.data.gov.au/def/nrm/1b1a0c2e-70a2-5aad-954e-52eab30320c5"
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "Coarse gravelly or large pebbles (20-60mm)",
        "description": "Refers to the size class of '3' on a scale of '1-7' size class of the soil coarse fragments substrate. '1' being the lowest (fine gravelly) and '7' being the highest (large boulders).",
        "uri": "https://linked.data.gov.au/def/nrm/b9e01bed-982b-5feb-858a-e7d374e6a244"
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "Cobbly or cobbles (60-200mm)",
        "description": "Refers to the size class of '4' on a scale of '1-7' size class of the soil coarse fragments substrate. '1' being the lowest (fine gravelly) and '7' being the highest (large boulders).",
        "uri": "https://linked.data.gov.au/def/nrm/08e3ba0e-6ef8-59b3-87c5-7943f9895820"
      },
      {
        "id": 5,
        "symbol": "5",
        "label": "Stony or stones (200-600mm)",
        "description": "Refers to the size class of '5' on a scale of '1-7' size class of the soil coarse fragments substrate. '1' being the lowest (fine gravelly) and '7' being the highest (large boulders).",
        "uri": "https://linked.data.gov.au/def/nrm/ac37dbbc-c655-5dbd-b204-2923f3135e8c"
      },
      {
        "id": 6,
        "symbol": "6",
        "label": "Bouldery or boulders (600-2000mm)",
        "description": "Refers to the size class of '6' on a scale of '1-7' size class of the soil coarse fragments substrate. '1' being the lowest (fine gravelly) and '7' being the highest (large boulders).",
        "uri": "https://linked.data.gov.au/def/nrm/0fc1f1d2-a9c0-51a1-a26f-0bc7af65e708"
      },
      {
        "id": 7,
        "symbol": "7",
        "label": "Large Boulders (>2000mm)",
        "description": "Refers to the size class of '7' on a scale of '1-7' size class of the soil coarse fragments substrate. '1' being the lowest (fine gravelly) and '7' being the highest (large boulders).",
        "uri": "https://linked.data.gov.au/def/nrm/65f4fafb-2255-5026-ab8d-56c3d5fa3edb"
      },
      {
        "id": 8,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      },
      {
        "id": 9,
        "symbol": "n/a",
        "label": "Not Applicable",
        "description": "Refers to any categorical values/attributes that are 'Not Applicable' to a particular observation in the study.",
        "uri": "https://linked.data.gov.au/def/nrm/5488c9ea-e3a2-5348-887b-065d29b848ab"
      }
    ],
    "lut-soils-coarse-frag-strength": [
      {
        "id": 1,
        "symbol": "VW",
        "label": "Very weak rock (1-35 MPa)",
        "description": "Refers to the strength class of an intact rock material of a soil surface substrate. Very weak rocks have an unconfined compressive strength of less than 1-25 MPa, and can be made into deep cuts using a knife, or crumbles using a pick or flattened or powdered using a single blow hammer.",
        "uri": "https://linked.data.gov.au/def/nrm/c31bdf12-5c6b-530a-8c83-0076a02ce5bc"
      },
      {
        "id": 2,
        "symbol": "W",
        "label": "Weak rock (25-50 MPa)",
        "description": "Refers to the strength class of an intact rock material of a soil surface substrate. Weak rocks have an unconfined compressive strength of less 25-50 MPa, and shallow cuts can be made using a knife, or indents made using a pick or shattered into smaller fragments using a single blow hammer.",
        "uri": "https://linked.data.gov.au/def/nrm/838bb2eb-5344-5a01-af94-59be8bf8a54e"
      },
      {
        "id": 3,
        "symbol": "M",
        "label": "Moderately strong rock (50-100 MPa)",
        "description": "Refers to the strength class of an intact rock material of a soil surface substrate. Moderately strong rocks have an unconfined compressive strength of 50 - 100 MPa, and can be broken into smaller fragments only using a single blow hammer.",
        "uri": "https://linked.data.gov.au/def/nrm/d8ee1363-1d0f-5a15-9633-1a4e42151882"
      },
      {
        "id": 4,
        "symbol": "S",
        "label": "Strong rock (100-200 MPa)",
        "description": "Refers to the strength class of an intact rock material of a soil surface substrate. Strong rocks have an unconfined compressive strength of 100-200 MPa, and cannot be broken into fragments using knife or a pick but can be broken into one or two large fragments using a single blow hammer.",
        "uri": "https://linked.data.gov.au/def/nrm/8a17328c-dc90-51fd-bd7b-d4e933af15f6"
      },
      {
        "id": 5,
        "symbol": "VS",
        "label": "Very strong rock (>200 MPa)",
        "description": "Refers to the strength class of an intact rock material of a soil surface substrate. Very strong rocks have an unconfined compressive strength of greater than 200 MPa, and cannot be broken into fragments using knife, pick or a hammer.",
        "uri": "https://linked.data.gov.au/def/nrm/73a29e6a-f17c-5df3-a0c6-837de0e1c80c"
      }
    ],
    "lut-soils-colour-chroma": [
      {
        "id": 1,
        "symbol": "1",
        "label": "1",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "2",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "3",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "4",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "6",
        "label": "6",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "8",
        "label": "8",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-colour-hue": [
      {
        "id": 1,
        "symbol": "5R",
        "label": "5R",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "7.5R",
        "label": "7.5R",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "10R",
        "label": "10R",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "2.5YR",
        "label": "2.5YR",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "5YR",
        "label": "5YR",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "7.5YR",
        "label": "7.5YR",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "10YR",
        "label": "10YR",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "10Y",
        "label": "10Y",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "5GY",
        "label": "5GY",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "10GY",
        "label": "10GY",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "N",
        "label": "N",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "5G",
        "label": "5G",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "5BG",
        "label": "5BG",
        "description": "",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "10BG",
        "label": "10BG",
        "description": "",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "10B",
        "label": "10B",
        "description": "",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "5PB",
        "label": "5PB",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-colour-value": [
      {
        "id": 1,
        "symbol": "2",
        "label": "2",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "2.5",
        "label": "2.5",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "3",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "4",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "5",
        "label": "5",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "6",
        "label": "6",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "7",
        "label": "7",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "8",
        "label": "8",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-confidence-level": [
      {
        "id": 1,
        "symbol": "1",
        "label": "1",
        "description": "Denotes the level of confidence while classifying a soil on a scale of '1-4'. '1' - All necessary analytical and/or morphological data are available for the profile being classified.",
        "uri": "https://linked.data.gov.au/def/nrm/c0473d78-c78d-5482-bff5-3d42053afe1b"
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "2",
        "description": "Denotes the level of confidence while classifying a soil on a scale of '1-4'.  '2'- Necessary analytical and/or morphological data for the profile are incomplete but are sufficient to classify the soil with a reasonable degree of confidence.",
        "uri": "https://linked.data.gov.au/def/nrm/3eb35504-17b5-5a3a-a317-41822227ae4f"
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "3",
        "description": "Denotes the level of confidence while classifying a soil on a scale of '1-4'.  '3'- No necessary analytical or limited morphological data are available for the profile but confidence is fair, based on knowledge of similar soils in similar environments.",
        "uri": "https://linked.data.gov.au/def/nrm/3567f2f8-eb2d-5f68-ad6a-95b80259d284"
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "4",
        "description": "Denotes the level of confidence while classifying a soil on a scale of '1-4'. '4'- No necessary analytical or limited morphological data are available for the profile and the classifier has little knowledge or experience with this kind of soil, hence the classification is provisional.",
        "uri": "https://linked.data.gov.au/def/nrm/cef047b7-a927-56fc-9489-d5d5ef77b24a"
      }
    ],
    "lut-soils-cutan-abundance": [
      {
        "id": 1,
        "symbol": "0",
        "label": "No cutans",
        "description": "Refers to the 'no' soil cutans observed.",
        "uri": "https://linked.data.gov.au/def/nrm/0fc43f3f-248f-55c0-b218-03cb1c5cce4e"
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Few",
        "description": "Refers to the soil cutan abundance score of '1' on a scale of '0 and 3', i.e., <10% ped faces or pore walls with cutans.",
        "uri": "https://linked.data.gov.au/def/nrm/d314cefd-311f-5b50-8165-b5b839d35d69"
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Common",
        "description": "Refers to the soil cutan abundance score of '2' on a scale of '0 and 3', i.e., 10-50% ped faces or pore walls with cutans.",
        "uri": "https://linked.data.gov.au/def/nrm/9051cdda-0b44-586d-955e-a9e7c36cd5d3"
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "Many",
        "description": "Refers to the soil cutan abundance score of '3' on a scale of '0 and 3', i.e., >50% ped faces or pore walls with cutans.",
        "uri": "https://linked.data.gov.au/def/nrm/fd2075b7-536f-58e6-a9dc-c51f1bd0b2ed"
      }
    ],
    "lut-soils-cutan-distinctness": [
      {
        "id": 1,
        "symbol": "F",
        "label": "Faint",
        "description": "Evident only on close examination with ×10 magnification. Little contrast with adjacent material.",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "D",
        "label": "Distinct",
        "description": "Can be detected without magnification. Contrast with adjacent material is evident in colour, texture or other properties.",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "P",
        "label": "Prominent",
        "description": "Conspicuous without magnification when compared with a surface broken through the soil. Colour, texture or some other property contrasts sharply with properties of the adjacent material, or the feature is thick enough to be conspicuous.",
        "uri": ""
      }
    ],
    "lut-soils-cutan-type": [
      {
        "id": 1,
        "symbol": "Z",
        "label": "Zero or no cutans",
        "description": "Refers to 'No' cutans observed in the soil.",
        "uri": "https://linked.data.gov.au/def/nrm/e5640b76-cdf9-571f-869e-0426aab9f7d8"
      },
      {
        "id": 2,
        "symbol": "U",
        "label": "Unspecified",
        "description": "Refers to the nature of soil cutans that cannot be determined.",
        "uri": "https://linked.data.gov.au/def/nrm/86739c9a-1b37-57a7-adaf-54f39b260361"
      },
      {
        "id": 3,
        "symbol": "C",
        "label": "Clay skins",
        "description": "Refers to the type of soil cutans. Clay skins are coatings often different in colour from the matrix of the ped. They are frequently difficult to distinguish from stress cutans, which are not true coatings.",
        "uri": "https://linked.data.gov.au/def/nrm/2df84f69-501f-5652-b5f4-5261217c12f9"
      },
      {
        "id": 4,
        "symbol": "M",
        "label": "Mangans",
        "description": "Refers to the type of soil cutans. Mangans are coatings of manganese oxides or hydroxides. The material may have a glazed appearance and is very dark brown to black.",
        "uri": "https://linked.data.gov.au/def/nrm/670e4e4a-2dc6-5351-a12f-f24571ccdf4a"
      },
      {
        "id": 5,
        "symbol": "S",
        "label": "Stress cutans",
        "description": "Refers to the type of soil cutans. Stress cutans are in situ modifications of natural surfaces in soil materials due to differential forces such as shearing. They are not true coatings.",
        "uri": "https://linked.data.gov.au/def/nrm/3f51e279-be0b-5e7c-a1e3-7e212dae1273"
      },
      {
        "id": 6,
        "symbol": "O",
        "label": "Other cutans",
        "description": "Refers to any 'other' type of soil cutans not listed as part of the categorical collection.",
        "uri": "https://linked.data.gov.au/def/nrm/90e130f5-a89c-5627-99dc-18b82b3c93e9"
      }
    ],
    "lut-soils-deposition-mechanism": [
      {
        "id": 1,
        "symbol": "S",
        "label": "Spoil",
        "description": "material deposited by mechanical means, anthropogenic processes"
      },
      {
        "id": 2,
        "symbol": "F",
        "label": "Flood deposition",
        "description": "material sourced from flooding"
      },
      {
        "id": 3,
        "symbol": "E",
        "label": "Erosion",
        "description": "material deposited by sediment transport/runoff sourced from nearby erosion upslope"
      },
      {
        "id": 4,
        "symbol": "A",
        "label": "Aeolian",
        "description": "wind-blown material"
      },
      {
        "id": 5,
        "symbol": "G",
        "label": "Gravity or mass movement",
        "description": "material from landslides, slumping"
      }
    ],
    "lut-soils-digging-stopped-by": [
      {
        "id": 1,
        "symbol": "N/A",
        "label": "N/A",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "R",
        "label": "Rock",
        "description": "Refers to the type of soil substrate. Rock is a naturally occurring solid aggregate of minerals and/or mineraloids.",
        "uri": "https://linked.data.gov.au/def/nrm/7e97ae42-691a-5f51-8e22-99ee7b98f877"
      },
      {
        "id": 3,
        "symbol": "G",
        "label": "Gravel",
        "description": "Gravel is an environmental material which is composed of pieces of rock that are at least two millimeters (2mm) in its largest dimension and no more than 75 millimeters.",
        "uri": "https://linked.data.gov.au/def/nrm/65069cb1-ae75-59be-9303-deb816fa8566"
      },
      {
        "id": 4,
        "symbol": "D",
        "label": "Too hard",
        "description": "Describes the condition of soil substrate material the digging activity was stopped-by. ",
        "uri": "https://linked.data.gov.au/def/nrm/25c158d7-1115-51cc-b30f-2830c81bfc1c"
      },
      {
        "id": 5,
        "symbol": "H",
        "label": "Hard pan",
        "description": "Refers to the type of soil substrate the digging activity was stopped-by. Hard pans are an indurated and/or cemented soil horizon that is very hard and with vertical cracks and irregular laminar cleavage.",
        "uri": "https://linked.data.gov.au/def/nrm/5c7425fe-f08e-5dbd-85dd-213d8f3479b3"
      },
      {
        "id": 6,
        "symbol": "W",
        "label": "Too wet",
        "description": "Describes the condition of soil substrate material the digging activity was stopped-by. ",
        "uri": "https://linked.data.gov.au/def/nrm/8d2c6569-d821-5263-8ec1-975eac549283"
      },
      {
        "id": 7,
        "symbol": "L",
        "label": "Too loose",
        "description": "Describes the condition of soil substrate material the digging activity was stopped-by. ",
        "uri": "https://linked.data.gov.au/def/nrm/b2a2f0b3-0c5a-59c8-a8c9-17eecc39e488"
      }
    ],
    "lut-soils-drainage": [
      {
        "id": 1,
        "symbol": "1",
        "label": "Very poor",
        "description": "Refers to the soil drainage type. Under a very poorly drained soil, water is removed from the soil so slowly that the watertable remains at or near the surface for most of the year. Surface flow; groundwater and subsurface flow are major sources of water; although precipitation may be important where there is a perched watertable and precipitation exceeds evapotranspiration. Soils have a wide range in texture and depth; and often occur in depressed sites. Strong gleying and accumulation of surface organic matter are usually features of most soils.",
        "uri": "https://linked.data.gov.au/def/nrm/c253ea62-a05e-56b6-8712-01c9d443b17a"
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "Poor",
        "description": "Refers to the soil drainage type. Under a poorly drained soil, water is removed very slowly in relation to supply. Subsurface and/or groundwater flow; as well as precipitation; may be a significant water source. Seasonal ponding; resulting from run-on and insufficient outfall; also occurs. A perched watertable may be present. Soils have a wide range in texture and depth; many have horizons that are gleyed; mottled; or possess orange or rusty linings of root channels. All horizons remain wet for several months.",
        "uri": "https://linked.data.gov.au/def/nrm/0eedea8e-9af7-5ad0-a9c6-ffd4956cb7d2"
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "Imperfect",
        "description": "Refers to the soil drainage type. Under an imperfectly drained soil, water is removed on slowly in relation to supply. Precipitation is the main source if available water storage capacity is high; but surface subflow and/or groundwater contribute as available water storage capacity decreases. Soils have a wide range in texture and depth. Some horizons may be mottled and/or have orange or rusty linings of root channels; and are wet for several weeks.",
        "uri": "https://linked.data.gov.au/def/nrm/8946d928-71f8-56c4-8e6e-fb0fcc982f54"
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "Moderately well",
        "description": "Refers to the soil drainage type. Under a moderately-well drained soil, water is removed from the soil somewhat slowly in relation to supply; due to low permeability; shallow watertable; lack of gradient; or some combination of these. Soils are usually medium to fine in texture. Significant additions of water by subsurface flow are necessary in coarse-textured soil. Some horizons may remain wet for as long as one week after water addition.",
        "uri": "https://linked.data.gov.au/def/nrm/1e32b973-177d-5669-8646-8130e4a70281"
      },
      {
        "id": 5,
        "symbol": "5",
        "label": "Well",
        "description": "Refers to the soil drainage type. Under a well drained soil, water is removed from the soil readily but not rapidly. Excess water flows downward readily into underlying; moderately permeable material or laterally as subsurface flow. The soils are often medium in texture. Some horizons may remain wet for several days after water addition.",
        "uri": "https://linked.data.gov.au/def/nrm/ef398e3e-e7dd-5f5d-b470-15d294d2d461"
      },
      {
        "id": 6,
        "symbol": "6",
        "label": "Rapid",
        "description": "Refers to the soil drainage type. Under a rapidly drained soil, water is removed from the soil rapidly in relation to supply. Excess water flows downward rapidly if underlying material is highly permeable. There may be rapid subsurface lateral flow during heavy rainfall provided there is a steep gradient. Soils are usually coarse-textured; or shallow; or both. No horizon is normally wet for more than several hours after water addition.",
        "uri": "https://linked.data.gov.au/def/nrm/f361712b-db6f-5804-931b-24c5465ed351"
      },
      {
        "id": 7,
        "symbol": "7",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      }
    ],
    "lut-soils-effervescence": [
      {
        "id": 1,
        "symbol": "H",
        "label": "Highly calcareous",
        "description": "Describes the degree of calcareous soils, i.e., with moderately visible effervescence.",
        "uri": "https://linked.data.gov.au/def/nrm/60289f83-a0a9-50d9-bf9c-a0879c08ad55"
      },
      {
        "id": 2,
        "symbol": "M",
        "label": "Moderately calcareous",
        "description": "Describes the degree of calcareous soils, i.e., with audible and slightly visible effervescence.",
        "uri": "https://linked.data.gov.au/def/nrm/afc9dd03-c19c-58fb-a928-bc12ddfe1c46"
      },
      {
        "id": 3,
        "symbol": "N",
        "label": "Non-calcareous",
        "description": "Describes the degree of calcareous soils, i.e., no audible or visible effervescence.",
        "uri": "https://linked.data.gov.au/def/nrm/31abd77e-9e8e-587a-ab7f-455d4dc03eac"
      },
      {
        "id": 4,
        "symbol": "S",
        "label": "Slightly calcareous",
        "description": "Describes the degree of calcareous soils, i.e., with slightly audible but no visible effervescence.",
        "uri": "https://linked.data.gov.au/def/nrm/a9f57eb6-65b3-5c61-bbc5-97559fad6571"
      },
      {
        "id": 5,
        "symbol": "V",
        "label": "Very highly calcareous",
        "description": "Describes the degree of calcareous soils, i.e., with strongly visible effervescence.",
        "uri": "https://linked.data.gov.au/def/nrm/f956de8a-1d07-5fb3-b9ab-9db2a08f9c3f"
      },
      {
        "id": 6,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      }
    ],
    "lut-soils-erosion-state": [
      {
        "id": 1,
        "symbol": "A",
        "label": "Active",
        "description": "Refers to an active state of soil erosion.",
        "uri": "https://linked.data.gov.au/def/nrm/783a0a27-f824-50b7-a054-c18f06087a28"
      },
      {
        "id": 2,
        "symbol": "P",
        "label": "Partially stabilised",
        "description": "Refers to a partially stabilised state of soil erosion.",
        "uri": "https://linked.data.gov.au/def/nrm/1b8ab821-f93e-51e1-9c92-deb47f54d14a"
      },
      {
        "id": 3,
        "symbol": "S",
        "label": "Stabilised",
        "description": "Refers to a stabilised state of soil erosion.",
        "uri": "https://linked.data.gov.au/def/nrm/1ac163de-ee5b-590f-9bf2-346451da656a"
      },
      {
        "id": 4,
        "symbol": "Z",
        "label": "Absent",
        "description": "Refers to the state of soil erosion being absent.",
        "uri": "https://linked.data.gov.au/def/nrm/2e0f6e54-05f7-5c17-b181-be6531011be6"
      }
    ],
    "lut-soils-erosion-type": [
      {
        "id": 1,
        "symbol": "B",
        "label": "Stream bank",
        "description": "A stream bank erosion is the removal of soil from a stream bank, typically during periods of high stream flow.",
        "uri": "https://linked.data.gov.au/def/nrm/16b69b7d-0ee8-5750-8f28-05afb580439c"
      },
      {
        "id": 2,
        "symbol": "C",
        "label": "Scald",
        "description": "Type of Landform Element with characteristics as that of a flat; bare of vegetation; from which soil has been eroded or excavated by surface wash or wind.",
        "uri": "https://linked.data.gov.au/def/nrm/54c94292-798f-5a26-8455-d1a302756f4f"
      },
      {
        "id": 3,
        "symbol": "G",
        "label": "Gully",
        "description": "Type of Landform Element characterised by open depression with short; precipitous walls and moderately inclined to very gently inclined floor or small stream channel; eroded by channelled stream flow and consequent collapse and water-aided mass movement.",
        "uri": "https://linked.data.gov.au/def/nrm/37421601-50ec-5371-b697-e9e18a54ba92"
      },
      {
        "id": 4,
        "symbol": "M",
        "label": "Mass movement",
        "description": "Refers to a relatively large downslope movement of soil, rock or mixture of both (e.g. landslides, slumps, earth flows, debris avalanches and solifluxion).",
        "uri": "https://linked.data.gov.au/def/nrm/5be04bde-2eda-51fa-81a5-aa20e3265845"
      },
      {
        "id": 5,
        "symbol": "R",
        "label": "Rill",
        "description": "A rill is a small channel up to 0.3 m deep, which can be largely obliterated by tillage operations.",
        "uri": "https://linked.data.gov.au/def/nrm/56b91fdf-aef1-56bf-a3ca-68cf9a54dc57"
      },
      {
        "id": 6,
        "symbol": "S",
        "label": "Sheet",
        "description": "Sheet erosion is a relatively uniform removal of soil from an area without the development of conspicuous channels.",
        "uri": "https://linked.data.gov.au/def/nrm/f4dc62bc-3a2b-5f82-849d-1fa0258a9815"
      },
      {
        "id": 7,
        "symbol": "T",
        "label": "Tunnel",
        "description": "A tunnel erosion is the removal of subsoil by water while the surface soil remains relatively intact.",
        "uri": "https://linked.data.gov.au/def/nrm/45c6241e-0ffb-542f-8821-c1c3198ca25e"
      },
      {
        "id": 8,
        "symbol": "V",
        "label": "Wave",
        "description": "Wave erosion refers to erosion of beaches, beach ridges and/or dunes. This is the removal of sand or soil from the margins of beaches, beach ridges, dunes, lakes or dams by wave action.",
        "uri": "https://linked.data.gov.au/def/nrm/55ff7396-842b-5010-a24a-e9f06a03e0d5"
      },
      {
        "id": 9,
        "symbol": "W",
        "label": "Wind",
        "description": "Wind erosion refers to the loss of surface or removal or loose material from the soil surface depending on the severity class of wind.",
        "uri": "https://linked.data.gov.au/def/nrm/3da2c095-4a3b-59bc-823c-4c499f8aeb65"
      },
      {
        "id": 10,
        "symbol": "Z",
        "label": "None",
        "description": "Refers to 'No' soil erosion observed.",
        "uri": "https://linked.data.gov.au/def/nrm/1717bebd-6e3c-5f76-927a-c6dd029ad097"
      }
    ],
    "lut-soils-evaluation-means": [
      {
        "id": 1,
        "symbol": "T",
        "label": "Tripod-mounted instrument and staff",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "A",
        "label": "Abney level or clinometer and tape",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "P",
        "label": "Contour plan at 1:10,000 or larger scale",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "E",
        "label": "Estimate",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-field-dispersion-score": [
      {
        "id": 1,
        "symbol": "0",
        "label": "No dispersion within 2 hours",
        "description": "Refers to the soil field dispersion scores, where no dispersion occurs within 2 hours.",
        "uri": "https://linked.data.gov.au/def/nrm/5654ba32-b848-556a-8380-2aa7d809e5fb"
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "No dispersion within 10 minutes, slight dispersion within 2 hours",
        "description": "Refers to the soil field dispersion scores, where no dispersion occurs within 10 minutes, but slight dispersion occurs within 2 hours.",
        "uri": "https://linked.data.gov.au/def/nrm/96248e20-c291-5cf4-93a9-2213407fd612"
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Slight dispersion within 10 minutes or strong dispersion within 2 hours",
        "description": "Refers to the soil field dispersion scores, where slight dispersion occurs within 10 minutes or strong dispersion within 2 hours.",
        "uri": "https://linked.data.gov.au/def/nrm/60da1dfd-5b80-5a72-8ed9-db161672570a"
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "Strong dispersion within 10 minutes or complete dispersion within 2 hours",
        "description": "Refers to the soil field dispersion scores, where strong dispersion occurs within 10 minutes or complete dispersion within 2 hours.",
        "uri": "https://linked.data.gov.au/def/nrm/87b13922-707f-5778-8ba5-4cda1a5781b1"
      },
      {
        "id": 5,
        "symbol": "4",
        "label": "Complete dispersion after 10 minutes",
        "description": "Refers to the soil field dispersion scores, where complete dispersion occurs after 10 minutes.",
        "uri": "https://linked.data.gov.au/def/nrm/0e596a19-e9ad-50fb-a4f6-e95efc2907d5"
      }
    ],
    "lut-soils-field-slaking-score": [
      {
        "id": 1,
        "symbol": "0",
        "label": "Not slaked within 10 minutes",
        "description": "Refers to the soil field slaking scores, where no slaking occurs within 10 minutes.",
        "uri": "https://linked.data.gov.au/def/nrm/bf714323-aaf2-5ba9-9219-852a06d29e67"
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Partially slaked within 10 minutes",
        "description": "Refers to the soil field slaking scores, where partial slaking occurs within 10 minutes.",
        "uri": "https://linked.data.gov.au/def/nrm/54efb939-c6e3-5466-95a4-ef11f3f5a228"
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Completely slaked within 10 minutes",
        "description": "Refers to the soil field slaking scores, where complete slaking occurs within 10 minutes.",
        "uri": "https://linked.data.gov.au/def/nrm/6d66f362-a9da-5d7d-bde7-5563e34188d0"
      }
    ],
    "lut-soils-fine-macropore-abundance": [
      {
        "id": 1,
        "symbol": "0",
        "label": "No very fine or fine macropores",
        "description": "Refers to No fine or very fine macropores present in soil.",
        "uri": "https://linked.data.gov.au/def/nrm/2c1a80f3-3c56-5fb3-9585-6a9fd84f7150"
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Few",
        "description": "Refers to the abundance of fine macropores in soil, i.e., <1 per 100mm^2; 10x10mm.",
        "uri": "https://linked.data.gov.au/def/nrm/76006889-a6d2-5a8e-97fb-861bcb64005e"
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Common",
        "description": "Refers to the abundance of fine macropores in soil, i.e., 1-5 per 100mm^2; 10x10mm.",
        "uri": "https://linked.data.gov.au/def/nrm/b6d0395d-7972-50bb-a4d0-54d345978a65"
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "Many",
        "description": "Refers to the abundance of fine macropores in soil, i.e., >5 per 100mm^2.",
        "uri": "https://linked.data.gov.au/def/nrm/9b0e0fbb-b83c-5f82-a95c-8dbc3e8f259c"
      }
    ],
    "lut-soils-gilgai-proportion": [
      {
        "id": 1,
        "symbol": "A",
        "label": "Equal mounds and depressions; no shelf present",
        "description": "",
        "uri": "https://linked.data.gov.au/def/nrm/4268c09d-3ae4-56f3-9ea6-f09375ff76f1"
      },
      {
        "id": 2,
        "symbol": "B",
        "label": "More mounds than depressions; no shelf present",
        "description": "",
        "uri": "https://linked.data.gov.au/def/nrm/020ca838-97f9-54d6-88b4-c4735620e909"
      },
      {
        "id": 3,
        "symbol": "C",
        "label": "Fewer mounds than depressions; no shelf present",
        "description": "",
        "uri": "https://linked.data.gov.au/def/nrm/decef65d-57fa-59c6-b028-a97fc7726eb2"
      },
      {
        "id": 4,
        "symbol": "D",
        "label": "Mound, shelf and depressions; shelf forms prominent part of gilgai",
        "description": "",
        "uri": "https://linked.data.gov.au/def/nrm/82ff39b2-e539-555c-aab1-634236bed547"
      },
      {
        "id": 5,
        "symbol": "N/A",
        "label": "N/A",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-gully-water-erosion-degree": [
      {
        "id": 1,
        "symbol": "0",
        "label": "No gully erosion",
        "description": "Refers to No soil gully water erosion.",
        "uri": "https://linked.data.gov.au/def/nrm/b9b999f7-437c-5860-bc52-6b2a8ab204d3"
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Minor",
        "description": "Refers to the degree of soil gully water erosion.",
        "uri": "https://linked.data.gov.au/def/nrm/e585ed3e-d966-5763-8b3e-039b2a597066"
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Moderate",
        "description": "Refers to the degree of soil gully water erosion.",
        "uri": "https://linked.data.gov.au/def/nrm/f69455ba-0a8b-50b0-9f2c-454bb0d404b3"
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "Severe",
        "description": "Refers to the degree of soil gully water erosion. Severe gully is when the erosion is large enough to create huge depressions on ground surface.",
        "uri": "https://linked.data.gov.au/def/nrm/388b1ef2-1e13-5360-893e-df303a909d5c"
      }
    ],
    "lut-soils-horizon-boundary-distinctness": [
      {
        "id": 1,
        "symbol": "S",
        "label": "Sharp (<5mm)",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "A",
        "label": "Abrupt (5-20mm)",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "C",
        "label": "Clear (20-50mm)",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "G",
        "label": "Gradual (50-100mm)",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "D",
        "label": "Diffuse (>100mm)",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-horizon-boundary-shape": [
      {
        "id": 1,
        "symbol": "S",
        "label": "Smooth",
        "description": "Refers to the shape of soil horizon boundary.",
        "uri": "https://linked.data.gov.au/def/nrm/3c3a287c-b63e-5e82-9aa7-a0664d81ecae"
      },
      {
        "id": 2,
        "symbol": "W",
        "label": "Wavy",
        "description": "Refers to the shape of soil horizon boundary.",
        "uri": "https://linked.data.gov.au/def/nrm/1a88db87-69c3-5add-8955-5b9cdd4d4316"
      },
      {
        "id": 3,
        "symbol": "I",
        "label": "Irregular",
        "description": "Refers to the shape of soil horizon boundary.",
        "uri": "https://linked.data.gov.au/def/nrm/3c613a9f-a095-569a-96d8-196d9a15da9a"
      },
      {
        "id": 4,
        "symbol": "T",
        "label": "Tongued",
        "description": "Refers to the shape of soil horizon boundary. Tongued depressions are considerably deeper than they are wide.",
        "uri": "https://linked.data.gov.au/def/nrm/37a466a4-43a4-5d95-9fea-89fc655bb953"
      },
      {
        "id": 5,
        "symbol": "B",
        "label": "Broken",
        "description": "Refers to the shape of soil horizon boundary. Broken- can be broken by cracks and the fragments are disoriented.",
        "uri": "https://linked.data.gov.au/def/nrm/8996d00f-3fe6-5daf-8150-f93a77ef23a3"
      }
    ],
    "lut-soils-horizon-details": [
      {
        "id": 1,
        "symbol": "M",
        "label": "M",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "O",
        "label": "O",
        "description": "Organic materials in varying stages of decomposition that have accumulated on the mineral soil surface.",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "O1",
        "label": "O1",
        "description": "Undecomposed organic debris, usually dominated by leaves and twigs.",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "O2",
        "label": "O2",
        "description": "Organic debris in various stages of decomposition.",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "P",
        "label": "P",
        "description": "Organic materials in various stages of decomposition that have accumulated either under water or in conditions of excessive wetness.",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "P1",
        "label": "P1",
        "description": "Undecomposed or weakly decomposed organic material (fibric peat).",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "P2",
        "label": "P2",
        "description": "Moderately to completely decomposed organic material (hemic to sapric peat).",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "A",
        "label": "A",
        "description": "One or more surface mineral horizons with some organic accumulation and usually darker in colour than the underlying horizons, or consisting of surface and subsurface horizons that are lighter in colour but have a lower content of silicate clay and/or sesquioxides than the underlying horizons.",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "A1",
        "label": "A1",
        "description": "Some accumulation of humified organic matter, usually darker in colour than underlying horizons and with maximum biologic activity for any given soil profile.",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "A2",
        "label": "A2",
        "description": "Less organic matter, sesquioxides, or silicate clay than immediately adjacent horizons, either alone or in combination.",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "A3",
        "label": "A3",
        "description": "Transitional horizon between A and B, which is dominated by properties characteristic of an overlying A1 or A2.",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "B",
        "label": "B",
        "description": "One or more mineral soil layers characterised by one or more of the following: a concentration of silicate clay, iron, aluminium, organic material or several of these; a structure and/or consistence unlike that of the A horizons above or of any horizons immediately below; stronger colours, usually expressed as higher chroma and/or redder hue, than those of the A horizons above or those of the horizons below.",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "B1",
        "label": "B1",
        "description": "Transitional horizon between A and B, which is dominated by properties characteristic of an underlying B2.",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "B2",
        "label": "B2",
        "description": "One or more of the following: clay, or iron, aluminium or humus, either alone or in combination; maximum development of pedologic organisation as evidenced by a different structure and/or consistence, and/or stronger colours than the A horizons above or any horizon immediately below.",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "B3",
        "label": "B3",
        "description": "Transitional horizon between B and C or other subsolum material in which properties characteristic of an overlying B2 dominate, but intergrade to those of the underlying material.",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "C",
        "label": "C",
        "description": "Consolidated or unconsolidated material, usually partially weathered, little affected by pedogenic processes, and either like or unlike the material from which the solum presumably formed. Lacks properties characteristic of O, P, A, B or D horizons.",
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "C1",
        "label": "C1"
      },
      {
        "id": 18,
        "symbol": "C2",
        "label": "C2"
      },
      {
        "id": 19,
        "symbol": "C3",
        "label": "C3"
      },
      {
        "id": 20,
        "symbol": "D",
        "label": "D",
        "description": "Any soil material below the solum that is unlike the solum in its general character, is not C horizon, and cannot be given reliable horizon designation as described in ‘Lithologic discontinuities’ or ‘Buried soils’.",
        "uri": ""
      },
      {
        "id": 21,
        "symbol": "D1",
        "label": "D1"
      },
      {
        "id": 22,
        "symbol": "D2",
        "label": "D2"
      },
      {
        "id": 23,
        "symbol": "D3",
        "label": "D3"
      },
      {
        "id": 24,
        "symbol": "R",
        "label": "R",
        "description": "Continuous masses (not boulders) of moderately strong to very strong rock (excluding pans) such as bedrock.",
        "uri": ""
      },
      {
        "id": 25,
        "symbol": "A/B",
        "label": "A/B"
      },
      {
        "id": 26,
        "symbol": "AB",
        "label": "AB"
      },
      {
        "id": 27,
        "symbol": "A/C",
        "label": "A/C"
      },
      {
        "id": 28,
        "symbol": "AC",
        "label": "AC"
      },
      {
        "id": 29,
        "symbol": "B/C",
        "label": "B/C"
      },
      {
        "id": 30,
        "symbol": "BC",
        "label": "BC"
      },
      {
        "id": 31,
        "symbol": "C/R",
        "label": "C/R"
      },
      {
        "id": 32,
        "symbol": "CR",
        "label": "CR"
      },
      {
        "id": 33,
        "symbol": "Other",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-horizon-numeric-prefix": [
      {
        "id": 1,
        "symbol": "2",
        "label": "2"
      },
      {
        "id": 2,
        "symbol": "3",
        "label": "3"
      },
      {
        "id": 3,
        "symbol": "4",
        "label": "4"
      },
      {
        "id": 4,
        "symbol": "5",
        "label": "5"
      },
      {
        "id": 5,
        "symbol": "6",
        "label": "6"
      },
      {
        "id": 6,
        "symbol": "7",
        "label": "7"
      },
      {
        "id": 7,
        "symbol": "8",
        "label": "8"
      },
      {
        "id": 8,
        "symbol": "9",
        "label": "9"
      },
      {
        "id": 9,
        "symbol": "10",
        "label": "10"
      }
    ],
    "lut-soils-horizon-subdivision": [
      {
        "id": 1,
        "symbol": "1",
        "label": "1"
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "2"
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "3"
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "4"
      },
      {
        "id": 5,
        "symbol": "5",
        "label": "5"
      },
      {
        "id": 6,
        "symbol": "6",
        "label": "6"
      },
      {
        "id": 7,
        "symbol": "7",
        "label": "7"
      },
      {
        "id": 8,
        "symbol": "8",
        "label": "8"
      },
      {
        "id": 9,
        "symbol": "9",
        "label": "9"
      },
      {
        "id": 10,
        "symbol": "10",
        "label": "10"
      }
    ],
    "lut-soils-horizon-suffix": [
      {
        "id": 1,
        "symbol": "b",
        "label": "b",
        "description": "Buried soil horizons. Used in mineral soils only.",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "c",
        "label": "c",
        "description": "Horizons with accumulation of concretions or nodules of iron and/or aluminium and/or manganese.",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "d",
        "label": "d",
        "description": "Densipans. Very fine, sandy, earthy pan.",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "e",
        "label": "e",
        "description": "Conspicuously bleached horizons.",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "f",
        "label": "f",
        "description": "Faunal accumulation, such as worm casts, dominates certain A1 horizons.",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "g",
        "label": "g",
        "description": "Indicates strong gleying.",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "h",
        "label": "h",
        "description": "Horizons that contain accumulation of amorphous, organic matter–aluminium complexes in which iron contents are very low.",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "j",
        "label": "j",
        "description": "Sporadically bleached horizons.",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "k",
        "label": "k",
        "description": "Horizons with accumulation of carbonates, commonly calcium carbonate.",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "m",
        "label": "m",
        "description": "Horizons with strong cementation or induration.",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "p",
        "label": "p",
        "description": "Horizons where ploughing, tillage practices or other disturbance by humans has occurred (e.g. deep ripping).",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "q",
        "label": "q",
        "description": "Horizons with accumulation of secondary silica. If silica cementation is continuous or nearly continuous, ‘qm’ is used.",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "r",
        "label": "r",
        "description": "Horizons with layers of weathered rock (including saprolite) that, although consolidated, can be dug with hand tools.",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "s",
        "label": "s",
        "description": "Horizons with an accumulation of sesquioxide– organic matter complexes in which iron is dominant relative to aluminium. Often used in combination with ‘h’.",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "t",
        "label": "t",
        "description": "Horizons with accumulation of silicate clay (from German ton, clay).",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "w",
        "label": "w",
        "description": "Development of colour and/or structure, or both, observed in the B horizon, with little or no accumulation of sesquioxide– organic matter complexes.",
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "x",
        "label": "x",
        "description": "Fragipans or earthy pans.",
        "uri": ""
      },
      {
        "id": 18,
        "symbol": "y",
        "label": "y",
        "description": "Horizons with accumulation of calcium sulfate (gypsum).",
        "uri": ""
      },
      {
        "id": 19,
        "symbol": "z",
        "label": "z",
        "description": "Horizons with accumulation of salts more soluble than calcium sulfate and calcium carbonate.",
        "uri": ""
      },
      {
        "id": 20,
        "symbol": "?",
        "label": "?",
        "description": "Query. Used where doubt is associated with the nomenclature of the horizon, with the query following the horizon notation (e.g. ‘D?’)"
      }
    ],
    "lut-soils-lithology": [
      {
        "id": 1,
        "symbol": "AD",
        "label": "Adamellite",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/1e4cbd45-2660-5bfb-a3ee-a0eb32ea7a94"
      },
      {
        "id": 2,
        "symbol": "AG",
        "label": "Agglomerate",
        "description": "Lithology of soil characterised by sedimentary (pyroclastic) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/fe4e8ce9-fae4-544e-92cd-ea58fcb5ca97"
      },
      {
        "id": 3,
        "symbol": "AC",
        "label": "Alcrete (bauxite)",
        "description": "Alcrete- indurated material rich in aluminium hydroxides. Commonly consists of cemented pisoliths and usually known as bauxite.",
        "uri": "https://linked.data.gov.au/def/nrm/54e540de-abed-5a83-a9e6-e68bc9c93499"
      },
      {
        "id": 4,
        "symbol": "AM",
        "label": "Amphibolite",
        "description": "Lithology of soil characterised by metamorphic rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/06c9a059-d814-57d9-a91e-0dad585e01f6"
      },
      {
        "id": 5,
        "symbol": "AN",
        "label": "Andesite",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/a3898cc3-7354-59b0-a60a-7bcc240986a7"
      },
      {
        "id": 6,
        "symbol": "AH",
        "label": "Anhydrite",
        "description": "Lithology of soil characterised by sedimentary (chemical or organic) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/6c98697f-17a5-584c-a809-8baecc24fa92"
      },
      {
        "id": 7,
        "symbol": "AP",
        "label": "Aplite",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/7861ad7a-bbca-54c5-8f01-d4a27e1dc433"
      },
      {
        "id": 8,
        "symbol": "AR",
        "label": "Arkose",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/b1dbe2d3-a257-5876-8d0e-fb4f83fb4c75"
      },
      {
        "id": 9,
        "symbol": "AF",
        "label": "Ash (fine)",
        "description": "Lithology of soil with an unconsolidated material.",
        "uri": "https://linked.data.gov.au/def/nrm/23c9dfb7-537f-5abe-a731-05ee886f6431"
      },
      {
        "id": 10,
        "symbol": "AS",
        "label": "Ash (sandy)",
        "description": "Lithology of soil with an unconsolidated material.",
        "uri": "https://linked.data.gov.au/def/nrm/088c63bd-a49a-5d68-bd21-f10686278cc6"
      },
      {
        "id": 11,
        "symbol": "BA",
        "label": "Basalt",
        "description": "Lithology of soil characterised by igneous rocks, of volcanic origin.",
        "uri": "https://linked.data.gov.au/def/nrm/9a496dfe-be26-56b6-8e76-af055b755c1b"
      },
      {
        "id": 12,
        "symbol": "BB",
        "label": "Bombs (volcanic)",
        "description": "Lithology of soil with an unconsolidated material.",
        "uri": "https://linked.data.gov.au/def/nrm/5866a53f-5c43-59ab-a1b5-8d2770a4e3d7"
      },
      {
        "id": 13,
        "symbol": "BR",
        "label": "Breccia",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/8ccbe87a-88cb-5275-8c6e-96ab8a4420ee"
      },
      {
        "id": 14,
        "symbol": "KA",
        "label": "Calcarenite",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/4e9de67d-af69-5ed6-a80d-2fcbe4bf6ddb"
      },
      {
        "id": 15,
        "symbol": "KM",
        "label": "Calcareous mudstone",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/c1f92d3c-3ecd-543e-8758-1a3fb11199e1"
      },
      {
        "id": 16,
        "symbol": "KS",
        "label": "Calcareous sand",
        "description": "Lithology of soil with an unconsolidated material.",
        "uri": "https://linked.data.gov.au/def/nrm/31fd6b6b-89ce-53d1-9a71-d4f116719add"
      },
      {
        "id": 17,
        "symbol": "KL",
        "label": "Calcilutite",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/4f46806a-588b-5493-8277-7d5da499bc1b"
      },
      {
        "id": 18,
        "symbol": "KR",
        "label": "Calcirudite",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/7232dc0f-b6bf-5cd3-8803-b9c639c618b7"
      },
      {
        "id": 19,
        "symbol": "KC",
        "label": "Calcrete",
        "description": "Calcrete - any cemented, terrestrial carbonate accumulation that may vary significantly in morphology and degree of cementation.",
        "uri": "https://linked.data.gov.au/def/nrm/91d366f9-3947-54a1-a2a3-beabe7f34e33"
      },
      {
        "id": 20,
        "symbol": "CH",
        "label": "Chert",
        "description": "Lithology of soil characterised by sedimentary (chemical or organic) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/c3f9b5ad-d150-5cfd-b69a-9d76bc8d6d23"
      },
      {
        "id": 21,
        "symbol": "C",
        "label": "Clay",
        "description": "Lithology of soil with an unconsolidated material.",
        "uri": "https://linked.data.gov.au/def/nrm/73045f94-7df7-517f-80a9-b29e6d9acd65"
      },
      {
        "id": 22,
        "symbol": "CO",
        "label": "Coal",
        "description": "Lithology of soil characterised by sedimentary (chemical or organic) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/ac2717d5-8fca-51e3-9f2f-923bf640f51f"
      },
      {
        "id": 23,
        "symbol": "CG",
        "label": "Conglomerate",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/1890ee16-340a-5fb3-ad41-af9e393e6adc"
      },
      {
        "id": 24,
        "symbol": "CU",
        "label": "Consolidated rock (unidentified)",
        "description": "Lithology of soil of an unidentified consolidated nature.",
        "uri": "https://linked.data.gov.au/def/nrm/2f05f0a2-972b-5cb2-95c8-3a5183c13be5"
      },
      {
        "id": 25,
        "symbol": "SD",
        "label": "Detrital sedimentary rock (unidentified)",
        "description": "Lithology of soil of an unidentified sedimentary (detrital) nature.",
        "uri": "https://linked.data.gov.au/def/nrm/a9af3bfe-aff1-5f3a-a88d-d91fc1c57d78"
      },
      {
        "id": 26,
        "symbol": "DI",
        "label": "Diorite",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/f7805d01-ed0b-5b8c-a05f-6c35812ad9de"
      },
      {
        "id": 27,
        "symbol": "DR",
        "label": "Dolerite",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/1a7a76af-8518-58c6-a83a-ece675d8525d"
      },
      {
        "id": 28,
        "symbol": "DM",
        "label": "Dolomite",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/2b1c148b-6293-5ea7-92e6-c06fc3a1630a"
      },
      {
        "id": 29,
        "symbol": "FC",
        "label": "Ferricrete",
        "description": "Ferricrete - indurated material rich in hydrated oxides of iron (usually goethite and hematite) occurring as cemented nodules and/or concretions, or as massive sheets. This material has been commonly referred to in local usage around Australia as laterite, duricrust or ironstone.",
        "uri": "https://linked.data.gov.au/def/nrm/ee3219ca-ab0e-5ad7-b6f0-7a1348aebba6"
      },
      {
        "id": 30,
        "symbol": "GA",
        "label": "Gabbro",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/47ae571d-003e-5b53-82f1-479175e8c95b"
      },
      {
        "id": 31,
        "symbol": "GS",
        "label": "Gneiss",
        "description": "Lithology of soil characterised by metamorphic rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/65640eec-2b02-53d1-81fa-7c9ec33f4653"
      },
      {
        "id": 32,
        "symbol": "GN",
        "label": "Granite",
        "description": "Lithology of soil characterised by igneous rocks which is intrusive, felsic, granular, and phaneritic.",
        "uri": "https://linked.data.gov.au/def/nrm/c85d5e39-5a20-5834-8bf7-fcff13447654"
      },
      {
        "id": 33,
        "symbol": "GD",
        "label": "Granodiorite",
        "description": "Lithology of soil characterised by igneous rocks which is intrusive, felsic, granular, and phaneritic.",
        "uri": "https://linked.data.gov.au/def/nrm/180ea337-34d3-5fc4-a01f-1d72a45adefc"
      },
      {
        "id": 34,
        "symbol": "GR",
        "label": "Granulite",
        "description": "Lithology of soil characterised by metamorphic rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/c4a5b97b-6897-5c1f-ab78-1795dfe04f5b"
      },
      {
        "id": 35,
        "symbol": "GV",
        "label": "Gravel",
        "description": "Gravel is an environmental material which is composed of pieces of rock that are at least two millimeters (2mm) in its largest dimension and no more than 75 millimeters.",
        "uri": "https://linked.data.gov.au/def/nrm/65069cb1-ae75-59be-9303-deb816fa8566"
      },
      {
        "id": 36,
        "symbol": "GW",
        "label": "Graywacke",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/0fec4466-03e6-54d4-b5a9-0a69acc83907"
      },
      {
        "id": 37,
        "symbol": "GE",
        "label": "Greenstone",
        "description": "Lithology of soil characterised by metamorphic rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/0e5f7ead-4dc6-5c43-b5a0-28f60fc4d3ef"
      },
      {
        "id": 38,
        "symbol": "GY",
        "label": "Gypsum",
        "description": "Lithology of soil characterised by sedimentary (chemical or organic) rocks. Gypsum is a soft sulfate mineral composed of calcium sulfate dihydrate.",
        "uri": "https://linked.data.gov.au/def/nrm/4b0c65ff-6f40-5fa3-b235-7aa6e9920b65"
      },
      {
        "id": 39,
        "symbol": "HA",
        "label": "Halite",
        "description": "Lithology of soil characterised by sedimentary (chemical or organic) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/cf0da518-f81b-5b28-a507-0ae222842444"
      },
      {
        "id": 40,
        "symbol": "HO",
        "label": "Hornfels",
        "description": "Lithology of soil characterised by metamorphic rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/62bbd5ce-e7d5-5bc6-a839-50fd1cc5f094"
      },
      {
        "id": 41,
        "symbol": "IG",
        "label": "Igneous rock (unidentified)",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/ae4d6ee1-a4a4-58d7-afe3-2232149a96a1"
      },
      {
        "id": 42,
        "symbol": "JA",
        "label": "Jasper",
        "description": "Lithology of soil characterised by sedimentary (chemical or organic) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/4d5899d8-2386-5f67-a2c3-c6fe8ed69126"
      },
      {
        "id": 43,
        "symbol": "LI",
        "label": "Limestone",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/39cbf831-0026-5b11-b610-c70431878973"
      },
      {
        "id": 44,
        "symbol": "MB",
        "label": "Marble",
        "description": "Lithology of soil characterised by metamorphic rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/0d4dc3da-f5e1-51a2-9ec2-a97aaf22ce79"
      },
      {
        "id": 45,
        "symbol": "ML",
        "label": "Marl",
        "description": "Lithology of soil with an unconsolidated material. Marl is a mass of calcium carbonate derived from mollusk shells and mixed with silt and clay.",
        "uri": "https://linked.data.gov.au/def/nrm/6dcf8c42-1283-54f3-bb31-3443bfc62fec"
      },
      {
        "id": 46,
        "symbol": "ME",
        "label": "Metamorphic rock (unidentified)",
        "description": "Lithology of soil with an unidentified metamorphic rock.",
        "uri": "https://linked.data.gov.au/def/nrm/59d93466-20ae-569f-843b-b650c1fa107e"
      },
      {
        "id": 47,
        "symbol": "MD",
        "label": "Microdiorite",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/42ee27e9-4004-5029-9ef7-2867082b696a"
      },
      {
        "id": 48,
        "symbol": "MG",
        "label": "Microgranite",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/3f0c9581-8804-51d5-a0f7-16f91e051e1f"
      },
      {
        "id": 49,
        "symbol": "MS",
        "label": "Microsyenite",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/694e4a7b-121f-5e45-bf76-8ab6e56dcda7"
      },
      {
        "id": 50,
        "symbol": "MI",
        "label": "Migmatite",
        "description": "Lithology of soil characterised by metamorphic rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/e0ca0a14-011f-5f61-83c1-cc09f5d1a87c"
      },
      {
        "id": 51,
        "symbol": "MU",
        "label": "Mudstone",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/91e8f02c-f3fb-564f-9fdb-0c6602584525"
      },
      {
        "id": 52,
        "symbol": "MY",
        "label": "Mylonite",
        "description": "Lithology of soil characterised by metamorphic rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/89369453-ec29-5f9e-9471-16ed1690dd98"
      },
      {
        "id": 53,
        "symbol": "PG",
        "label": "Pegmatite",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/a2140d42-8810-51e3-af4e-106ac830d079"
      },
      {
        "id": 54,
        "symbol": "PE",
        "label": "Peridotite",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/e705b9d5-f50e-597e-abdb-d25123197ca7"
      },
      {
        "id": 55,
        "symbol": "PL",
        "label": "Phonolite",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/99e0c7c7-1ffd-5280-ba21-7c3b828aeb66"
      },
      {
        "id": 56,
        "symbol": "PH",
        "label": "Phyllite",
        "description": "Lithology of soil characterised by metamorphic rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/6bd73d3f-a60c-5ae5-86e3-d8ca87af3550"
      },
      {
        "id": 57,
        "symbol": "PC",
        "label": "Porcellanite",
        "description": "Lithology of soil characterised by sedimentary (chemical or organic) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/508422bc-125f-5e11-aa15-2d0f95c0de81"
      },
      {
        "id": 58,
        "symbol": "PO",
        "label": "Porphyry",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/edaf7cad-4453-52f7-9fb8-3eda31d76b49"
      },
      {
        "id": 59,
        "symbol": "PY",
        "label": "Pyroxenite",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/fae4f1f5-2726-54fb-923c-43dd3eca19e4"
      },
      {
        "id": 60,
        "symbol": "QZ",
        "label": "Quartz",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/9bd96ebc-1bce-5e46-bc67-6bbe2f614a83"
      },
      {
        "id": 61,
        "symbol": "QU",
        "label": "Quartzite",
        "description": "Lithology of soil characterised by metamorphic rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/69bea4e4-5031-509f-a730-95d6f3ac31a2"
      },
      {
        "id": 62,
        "symbol": "QP",
        "label": "Quartz porphyry",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/eb3a011e-f1e8-55bc-960a-eed77ddab058"
      },
      {
        "id": 63,
        "symbol": "QS",
        "label": "Quartz sandstone",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/84d17f4a-6d82-5daa-9b23-3274b66b77d2"
      },
      {
        "id": 64,
        "symbol": "RB",
        "label": "Red-brown hardpan",
        "description": "Refers to the soil pan type. Red-brown hardpan - earthy pan, which is normally reddish brown to red with dense yet porous appearance; it is very hard, has an irregular laminar cleavage and some vertical cracks, and varies from less than 0.3 m to over 30 m thick. Other variable features are bedded and unsorted sand and gravel lenses; wavy black veinings, probably manganiferous; and, less commonly, off-white veins of calcium carbonate.",
        "uri": "https://linked.data.gov.au/def/nrm/519baa24-f4ee-5d63-8318-62aa261e1a4a"
      },
      {
        "id": 65,
        "symbol": "RH",
        "label": "Rhyolite",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/f4b3dd58-8107-5714-94b9-7acb252bbaf5"
      },
      {
        "id": 66,
        "symbol": "S",
        "label": "Sand",
        "description": "Lithology of soil with an unconsolidated material.",
        "uri": "https://linked.data.gov.au/def/nrm/bc445bd5-0c49-5108-84ef-430efb48ee06"
      },
      {
        "id": 67,
        "symbol": "SA",
        "label": "Sandstone",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/06b1db2a-5cdc-553c-b8c0-c792be29a19e"
      },
      {
        "id": 68,
        "symbol": "SC",
        "label": "Chemical or organic sedimentary rock",
        "description": "(unidentified)",
        "uri": ""
      },
      {
        "id": 69,
        "symbol": "SP",
        "label": "Pyroclastic",
        "description": "(unidentified)",
        "uri": ""
      },
      {
        "id": 70,
        "symbol": "ST",
        "label": "Schist",
        "description": "Lithology of soil characterised by metamorphic rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/44ecce8d-19b5-58f1-8867-95a78d766523"
      },
      {
        "id": 71,
        "symbol": "SK",
        "label": "Scoria",
        "description": "Lithology of soil with an unconsolidated material.",
        "uri": "https://linked.data.gov.au/def/nrm/2df24a98-bf52-59c5-9af8-76ccbf63bd70"
      },
      {
        "id": 72,
        "symbol": "SR",
        "label": "Serpentinite",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/844db8e3-86ec-5864-be4f-d6fb5cfc9c37"
      },
      {
        "id": 73,
        "symbol": "SH",
        "label": "Shale",
        "description": "Lithology of soil characterised by igneous rocks. A fine-grained sedimentary rock whose original constituents were clays or muds. It is characterized by thin laminae breaking with an irregular curving fracture, often splintery and usually parallel to the often-indistinguishable bedding plane.",
        "uri": "https://linked.data.gov.au/def/nrm/53cb257e-0fc6-5c09-9e85-c70f2a874930"
      },
      {
        "id": 74,
        "symbol": "LC",
        "label": "Silcrete",
        "description": "Silcrete - strongly indurated siliceous material cemented by, and largely composed of, forms of silica, including quartz, chalcedony, opal and chert.",
        "uri": "https://linked.data.gov.au/def/nrm/40dbae74-33a4-5d8f-b4a2-05915a30b587"
      },
      {
        "id": 75,
        "symbol": "Z",
        "label": "Silt",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks. Silt is granular material of a size somewhere between sand and clay whose mineral origin is quartz and feldspar.",
        "uri": "https://linked.data.gov.au/def/nrm/763d8a6b-1a37-567a-9e33-9e42c9885fea"
      },
      {
        "id": 76,
        "symbol": "ZS",
        "label": "Siltstone",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/97ec29d7-9544-5422-b3f8-ab4445d78707"
      },
      {
        "id": 77,
        "symbol": "SL",
        "label": "Slate",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/7eef58af-04fe-5f42-9034-3fb8c66a471a"
      },
      {
        "id": 78,
        "symbol": "SY",
        "label": "Syenite",
        "description": "Lithology of soil characterised by metamorphic rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/6865367f-2061-5cb3-92cb-2b83fbe1717c"
      },
      {
        "id": 79,
        "symbol": "TR",
        "label": "Trachyte",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/36f1ed4d-cf2e-5ccd-a9ed-a3f6a6e37657"
      },
      {
        "id": 80,
        "symbol": "TU",
        "label": "Tuff",
        "description": "Lithology of soil characterised by igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/d210379b-e17a-5ea0-8ece-cc9b975e4441"
      },
      {
        "id": 81,
        "symbol": "UC",
        "label": "Unconsolidated material (unidentified)",
        "description": "Lithology of soil with an unidentified, unconsolidated material.",
        "uri": "https://linked.data.gov.au/def/nrm/dc1bdfa6-5315-5b03-bd00-6b8cd4ebb256"
      },
      {
        "id": 82,
        "symbol": "VB",
        "label": "Volcanic breccia",
        "description": "Lithology of soil characterised by Sedimentary (pyroclastic) system.",
        "uri": "https://linked.data.gov.au/def/nrm/d4a911df-cc99-54b2-a23e-aaf06a888976"
      },
      {
        "id": 83,
        "symbol": "VG",
        "label": "Volcanic glass",
        "description": "Lithology of the soil that is characterised by Igneous rocks.",
        "uri": "https://linked.data.gov.au/def/nrm/4452d6f1-8bf8-5780-a52f-c5a7613bcb5c"
      },
      {
        "id": 84,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      },
      {
        "id": 85,
        "symbol": "M",
        "label": "Same as Substrate Lithology",
        "description": "Lithology of soil, similar to the substrate.",
        "uri": "https://linked.data.gov.au/def/nrm/a33614db-d49d-5230-984e-c1c8a843583d"
      },
      {
        "id": 86,
        "symbol": "IS",
        "label": "Ironstone (where not considered of pedogenic origin)",
        "description": "Lithology of soil characterised by sedimentary (detrital) rocks of ferruginous nature, and not considered of pedonegic origin.",
        "uri": "https://linked.data.gov.au/def/nrm/ad52691c-1b34-56ac-b8eb-f83c72694e96"
      },
      {
        "id": 87,
        "symbol": "SS",
        "label": "Shells",
        "description": "Lithology of soil. Soils of calcareous origin, restricted to soils dominated by shell fragments in Rudosols. ",
        "uri": "https://linked.data.gov.au/def/nrm/dc82e9f5-49a7-532a-a721-61a92986572e"
      },
      {
        "id": 88,
        "symbol": "CC",
        "label": "Charcoal",
        "description": "Lithology of soil characterised by black carbon residue.",
        "uri": "https://linked.data.gov.au/def/nrm/e9328e66-5712-5a8b-a9fa-9dbb917ab35e"
      },
      {
        "id": 89,
        "symbol": "PU",
        "label": "Pumice",
        "description": "Lithology of soil characterised by igneous rocks- lightweight, porous volcanic rock formed from frothy lava, often in soil as an aerating component..",
        "uri": "https://linked.data.gov.au/def/nrm/657c4f58-39ad-5b6f-8b73-e8e4e3cf0027"
      },
      {
        "id": 90,
        "symbol": "OW",
        "label": "Opalised Wood",
        "description": "Lithology of soil characterised by petrified wood that is composed of opal.",
        "uri": "https://linked.data.gov.au/def/nrm/8d13c39e-cba1-553e-be22-ba738876a6c6"
      }
    ],
    "lut-soils-macropore-diameter": [
      {
        "id": 1,
        "symbol": "1",
        "label": "Very fine",
        "description": "Refers to the size class of soil macropore, 0.075-1mm.",
        "uri": "https://linked.data.gov.au/def/nrm/facffade-5380-54a6-ad22-3dd7e1292838"
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "Fine",
        "description": "Refers to the size class of soil macropore, 1-2mm.",
        "uri": "https://linked.data.gov.au/def/nrm/45025c1b-8dea-56ed-8e58-c2668393dd52"
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "Medium",
        "description": "Refers to the size class of soil macropore, 2-5mm.",
        "uri": "https://linked.data.gov.au/def/nrm/0406410e-7fb7-5188-8027-04efc55e15b4"
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "Coarse",
        "description": "Refers to the size class of soil macropore, i.e., >5mm.",
        "uri": "https://linked.data.gov.au/def/nrm/2154a6b2-0d48-5fc7-90f8-78221c7c178c"
      }
    ],
    "lut-soils-mass-movement-erosion-degree": [
      {
        "id": 1,
        "symbol": "0",
        "label": "No mass movement",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Present",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-medium-macropore-abundance": [
      {
        "id": 1,
        "symbol": "0",
        "label": "No medium or coarse macropores",
        "description": "Refers to the 'no' soil medium or coarse macropore.",
        "uri": "https://linked.data.gov.au/def/nrm/d8bb72ce-d609-553c-b696-21c671cc9c3a"
      },
      {
        "id": 2,
        "symbol": "4",
        "label": "Few",
        "description": "Refers to the soil medium macropore abundance, i.e., <1/0.1m^2; 100x100mm.",
        "uri": "https://linked.data.gov.au/def/nrm/ab34c0ec-dd74-5ff0-9736-f9ae0964d3f8"
      },
      {
        "id": 3,
        "symbol": "5",
        "label": "Common",
        "description": "Refers to the soil medium macropore abundance, i.e., 1-5 /0.1m^2.",
        "uri": "https://linked.data.gov.au/def/nrm/b9538897-3cca-5f7c-bc66-59c7ffa59d5d"
      },
      {
        "id": 4,
        "symbol": "6",
        "label": "Many",
        "description": "Refers to the soil medium macropore abundance, i.e., >5/0.1m^2.",
        "uri": "https://linked.data.gov.au/def/nrm/6eb699aa-8426-5966-a4e0-1f421eff8c14"
      }
    ],
    "lut-soils-microrelief-component": [
      {
        "id": 1,
        "symbol": "M",
        "label": "Mound",
        "description": "Refers to the type soil microrelief components. Mound is convex; long axis not more than 3 times the shorter axis.",
        "uri": "https://linked.data.gov.au/def/nrm/3dfdb608-bc4b-51ed-a212-4b60ccf90d76"
      },
      {
        "id": 2,
        "symbol": "E",
        "label": "Elongate mound",
        "description": "Refers to the type of soil microrelief components. Elongate mounds are convex; long axis more than 3 times the shorter axis.",
        "uri": "https://linked.data.gov.au/def/nrm/cc0028c4-945d-5f2e-8368-9dda9d3a525a"
      },
      {
        "id": 3,
        "symbol": "D",
        "label": "Depression",
        "description": "Refers to the type of soil microrelief component. Landform element that stands below all, or almost all, points in the adjacent terrain.",
        "uri": "https://linked.data.gov.au/def/nrm/01390db2-5ecd-5175-8c1f-a06ea525e792"
      },
      {
        "id": 4,
        "symbol": "L",
        "label": "Elongate depression",
        "description": "Refers to the type of soil microrelief components.  Elongate depression is concave; occurs as open-ended form. ",
        "uri": "https://linked.data.gov.au/def/nrm/93f1c354-58e9-5708-bdf0-53b5ab9fbbed"
      },
      {
        "id": 5,
        "symbol": "S",
        "label": "Shelf",
        "description": "Refers to the type of soil microrelief components. Shelf is more or less planar; occurs between mounds and depressions.",
        "uri": "https://linked.data.gov.au/def/nrm/58ca1ed3-666b-5c10-906d-63680a034abb"
      },
      {
        "id": 6,
        "symbol": "HU",
        "label": "Hummock",
        "description": "Refers to the type of soil microrelief components. Hummock rises above a flat or planar surface. Sides vary from rounded to near vertical and tops from rounded to flat.",
        "uri": "https://linked.data.gov.au/def/nrm/da677f9a-9bbd-5cac-aabd-1f3ab1a262e9"
      },
      {
        "id": 7,
        "symbol": "F",
        "label": "Flat",
        "description": "Refers to the type of soil microrelief components. Flat is a surface in which hummocks, mounds, depressions or sinkholes are set.",
        "uri": "https://linked.data.gov.au/def/nrm/e3284ff9-18c8-5f37-8f63-abcd8837477d"
      },
      {
        "id": 8,
        "symbol": "HO",
        "label": "Hole",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "T",
        "label": "Terrace",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-microrelief-type": [
      {
        "id": 1,
        "symbol": "Z",
        "label": "Zero or no microrelief",
        "description": "Refers to zero or no microrelief on soil surface.",
        "uri": "https://linked.data.gov.au/def/nrm/33815e3c-b5e6-5bc3-a35f-0f3ef169fa3a"
      },
      {
        "id": 2,
        "symbol": "C",
        "label": "Crabhole gilgai",
        "description": "Irregularly distributed; small depressions and mounds separated by a more or less continuous shelf. Vertical interval usually less than 0.3 m. Horizontal interval usually 3-20 m; surface almost level.",
        "uri": "https://linked.data.gov.au/def/nrm/ceb10fd5-6e4c-5f36-8c62-378687e48b40"
      },
      {
        "id": 3,
        "symbol": "N",
        "label": "Normal gilgai",
        "description": "Irregularly distributed; small mounds and subcircular depressions varying in size and spacing. Vertical interval usually less than 0.3 m. Horizontal interval usually 3-20 m; surface almost level.",
        "uri": "https://linked.data.gov.au/def/nrm/a0a47d27-a9d5-5597-8822-683970bcc0ee"
      },
      {
        "id": 4,
        "symbol": "L",
        "label": "Linear gilgai",
        "description": "Long; narrow; parallel; elongate mounds and broader elongate depressions more or less at right angles to the contour. Usually in sloping lands. Vertical interval usually less than 0.3 m; Horizontal interval usually 5-8 m.",
        "uri": "https://linked.data.gov.au/def/nrm/e3c17f87-50bf-556b-9066-69cf3ee0254d"
      },
      {
        "id": 5,
        "symbol": "A",
        "label": "Lattice gilgai",
        "description": "Discontinuous; elongate mounds and/or elongate depressions more or less at right angles to the contour. Usually in sloping lands; commonly between linear gilgai on lower slopes and plains.",
        "uri": "https://linked.data.gov.au/def/nrm/a7eedbf0-09b8-5653-be29-eeccb6205140"
      },
      {
        "id": 6,
        "symbol": "M",
        "label": "Melonhole gilgai",
        "description": "Irregularly distributed; large depressions; usually greater than 3 m in diameter or greatest dimension; subcircular or irregular and varying from closely spaced in a network of elongate mounds to isolated depressions set in an undulating shelf with occasional small mounds. Some depressions may also contain sinkholes of gilgai usually greater than 0.3 m; horizontal interval usually 5-6 m; surface almost level.",
        "uri": "https://linked.data.gov.au/def/nrm/e45b99f2-81a0-5de3-9299-7d855cdff76b"
      },
      {
        "id": 7,
        "symbol": "G",
        "label": "Contour gilgai",
        "description": "Long; elongate depressions and parallel adjacent downslope mounds; which follow the contour. These depression-mound associations are separated from each other by shelves 10-100 m wide. Mounds are low; usually less than 0.5 m high; and often poorly defined.",
        "uri": "https://linked.data.gov.au/def/nrm/8fb86eca-3038-5378-937b-6acd0744e815"
      },
      {
        "id": 8,
        "symbol": "D",
        "label": "Debil-debil",
        "description": "Small hummocks rising above planar surface. They vary from rounded; both planar and vertically; to flat-topped; relatively steep sided and elongate. They are usually closely and regularly spaced; ranging from 0.06 m to 0.6 m in both vertical and horizontal dimensions. They are common in northern Australia on soils with impeded internal drainage and in areas of short seasonal ponding. Many observers consider them to be formed by biological activity.",
        "uri": "https://linked.data.gov.au/def/nrm/c1676b68-74ec-5dba-8485-50474e766953"
      },
      {
        "id": 9,
        "symbol": "W",
        "label": "Swamp hummock",
        "description": "Steep-sided hummocks rising above a flat surface. Hummocks are frequently occupied by trees or shrubs while the lower surface may be vegetation free or occupied by sedges or reeds. They are subject to prolonged seasonal flooding.",
        "uri": "https://linked.data.gov.au/def/nrm/fc14047c-999b-571d-98db-92776e5ef759"
      },
      {
        "id": 10,
        "symbol": "B",
        "label": "Biotic",
        "description": "Refers to the type of soil microrelief, of biotic origin, i.e., caused by agents such as termites rabbits, pigs in the form of mounds or burrows, etc.",
        "uri": "https://linked.data.gov.au/def/nrm/47f48c42-3beb-5e3b-ba47-3657a8c5da03"
      },
      {
        "id": 11,
        "symbol": "U",
        "label": "Mound/depression",
        "description": "Refers to the type of soil microrelief. Undifferentiated; irregularly distributed or isolated mounds and/or depressions set in a planar surface.",
        "uri": "https://linked.data.gov.au/def/nrm/11897a5e-4e66-5b00-9692-2fa6d4e01ca5"
      },
      {
        "id": 12,
        "symbol": "K",
        "label": "Karst microrelief",
        "description": "Refers to the type of soil microrelief. Generally characterised with depressions in limestone country.",
        "uri": "https://linked.data.gov.au/def/nrm/f4c07d86-1d85-520c-bfa4-6db33d9ad863"
      },
      {
        "id": 13,
        "symbol": "I",
        "label": "Sinkhole",
        "description": "Refers to the type of soil microrelief, characterised by a closed depression with vertical or funnel-shaped sides.",
        "uri": "https://linked.data.gov.au/def/nrm/4d3e3098-d49d-572d-8066-d5d26c24a5d1"
      },
      {
        "id": 14,
        "symbol": "S",
        "label": "Mass movement",
        "description": "Refers to the type of soil microrelief. Hummocky microrelief on the surface of landslides; slumps; earth flows; debris avalanches.",
        "uri": "https://linked.data.gov.au/def/nrm/b139ca67-164a-52c3-a0d1-296d9713b0d8"
      },
      {
        "id": 15,
        "symbol": "R",
        "label": "Terracettes",
        "description": "Refers to the type of soil microrelief, with small terraces on slopes resulting either from soil creep or trampling by hoofed animals.",
        "uri": "https://linked.data.gov.au/def/nrm/ad1ccb2b-1a03-5e6e-b98a-cd50ea26baf4"
      },
      {
        "id": 16,
        "symbol": "T",
        "label": "Contour trench",
        "description": "Refers to the type of soil microrelief. Trenches typically 0.2 m deep and 0.6 m wide; with near vertical walls; alternating with flat-crested ridges about 1.3 m wide; which extend along the contour for several metres or tens of metres. Contour trenches are known in areas in south-eastern Australia above 350 m altitude with a high effective rainfall; where they are associated with a grassland or heathland vegetation on undulating rises.",
        "uri": "https://linked.data.gov.au/def/nrm/c576c8d2-9808-5f19-bb34-35b6dead979b"
      },
      {
        "id": 17,
        "symbol": "P",
        "label": "Spring mound",
        "description": "Refers to the type of soil microrelief, with mound associated with water flowing from rock or soil without human intervention.",
        "uri": "https://linked.data.gov.au/def/nrm/b0834f8f-251e-5a13-b085-5ef743287448"
      },
      {
        "id": 18,
        "symbol": "H",
        "label": "Spring hollow",
        "description": "Refers to the type of soil microrelief, characterised with a depression associated with water flowing from rock or soil without human intervention.",
        "uri": "https://linked.data.gov.au/def/nrm/86ad53a1-9058-5afe-b592-2fcd1549ad9e"
      },
      {
        "id": 19,
        "symbol": "O",
        "label": "Other (see site notes)",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-moisture-status": [
      {
        "id": 1,
        "symbol": "M",
        "label": "Moist",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "D",
        "label": "Dry",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-morphological-type": [
      {
        "id": 1,
        "symbol": "C",
        "label": "Crest",
        "description": "Landform element that stands above all, or almost all, points in the adjacent terrain. It is characteristically smoothly convex upwards in downslope profile or in contour, or both. ",
        "uri": "https://linked.data.gov.au/def/nrm/de95610f-5132-53b5-8c2c-c9ba546254ea"
      },
      {
        "id": 2,
        "symbol": "H",
        "label": "Hillock",
        "description": "Refers to the soil morphology type. Hillocks are compound landform element comprising a narrow crest and short adjoining slopes, the crest length being less than the width of the landform element.",
        "uri": "https://linked.data.gov.au/def/nrm/e2ad0aeb-21e8-5b84-aa28-c007965fc237"
      },
      {
        "id": 3,
        "symbol": "R",
        "label": "Ridge",
        "description": "A compound landform element comprising a narrow crest and short adjoining slopes, the crest length being greater than the width of the landform element.",
        "uri": "https://linked.data.gov.au/def/nrm/bb108e33-0907-5137-b641-473558ce5bc8"
      },
      {
        "id": 4,
        "symbol": "S",
        "label": "Simple slope",
        "description": "Refers to the soil morphology type. Simple slope is a slope element adjacent below a crest or flat and adjacent above a flat or depression.",
        "uri": "https://linked.data.gov.au/def/nrm/4d6ecdd4-38c3-5953-85b7-934881364443"
      },
      {
        "id": 5,
        "symbol": "U",
        "label": "Upper slope",
        "description": "Refers to the soil morphology type. Upper slope is an element adjacent below a crest or flat but not adjacent above a flat or depression.",
        "uri": "https://linked.data.gov.au/def/nrm/c9810382-2056-57ea-8eb0-3f2a39a080ec"
      },
      {
        "id": 6,
        "symbol": "M",
        "label": "Mid-slope",
        "description": "Refers to the soil morphology type. Mid-slope is a slope element not adjacent below a crest or flat and not adjacent above a flat or depression.",
        "uri": "https://linked.data.gov.au/def/nrm/f66a2d61-5903-5c73-8e42-d9ba20bc43eb"
      },
      {
        "id": 7,
        "symbol": "L",
        "label": "Lower slope",
        "description": "Refers to the soil morphology type. Lower slope are slope element not adjacent below a crest or flat but\n adjacent above a flat or depression.",
        "uri": "https://linked.data.gov.au/def/nrm/baba8d7f-9127-5f8a-a437-f79454612ef9"
      },
      {
        "id": 8,
        "symbol": "F",
        "label": "Flat",
        "description": "Refers to the soil morphology type. Flat is a level landform pattern with extremely low relief.",
        "uri": "https://linked.data.gov.au/def/nrm/5bf2a4ff-ca1d-59ba-bf90-1c204bef12af"
      },
      {
        "id": 9,
        "symbol": "V",
        "label": "Open depression (vale)",
        "description": "Refers to the soil morphology type. Landform element that stands below all, or almost all, points in the adjacent terrain. An open depression extends at the same elevation, or lower.",
        "uri": "https://linked.data.gov.au/def/nrm/de3a894e-9dc3-5841-b43a-92cff5e10662"
      },
      {
        "id": 10,
        "symbol": "D",
        "label": "Closed depression",
        "description": "Refers to the soil morphology type. Landform element that stands below all, or almost all, points in the adjacent terrain. A closed depression stands below all such points.",
        "uri": "https://linked.data.gov.au/def/nrm/dfbad8be-04ff-5813-9351-dedbade110f6"
      }
    ],
    "lut-soils-mottle-abundance": [
      {
        "id": 1,
        "symbol": "0",
        "label": "No mottles or other colour patterns",
        "description": "Refers to the 'No mottles/colour patterns' observed.",
        "uri": "https://linked.data.gov.au/def/nrm/064805f3-9e17-5849-943d-d9a8f8cd7c65"
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Very few (<2%)",
        "description": "Refers to the categorical class describing the percentage of soil mottle abundance.",
        "uri": "https://linked.data.gov.au/def/nrm/3c29feed-c081-5852-b4a1-cf6b90002088"
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Few (2-10%)",
        "description": "Refers to the categorical class describing the percentage of soil mottle abundance.",
        "uri": "https://linked.data.gov.au/def/nrm/1e09068d-53c6-59c2-9c9f-2896a1e9fdd2"
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "Common (10-20%)",
        "description": "Refers to the categorical class describing the percentage of soil mottle abundance.",
        "uri": "https://linked.data.gov.au/def/nrm/6b082dd5-98c8-5aaf-ab78-ec89b47a31c0"
      },
      {
        "id": 5,
        "symbol": "4",
        "label": "Many (20-50%)",
        "description": "Refers to the categorical class describing the percentage of soil mottle abundance.",
        "uri": "https://linked.data.gov.au/def/nrm/4749bf65-92c4-5a4c-927e-47c6dfbcfd26"
      },
      {
        "id": 6,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      }
    ],
    "lut-soils-mottle-boundary-distinctness": [
      {
        "id": 1,
        "symbol": "S",
        "label": "Sharp",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "C",
        "label": "Clear",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "D",
        "label": "Diffuse",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-mottle-colour": [
      {
        "id": 1,
        "symbol": "n/a",
        "label": "Not Applicable",
        "description": "Refers to any categorical values/attributes that are 'Not Applicable' to a particular observation in the study.",
        "uri": "https://linked.data.gov.au/def/nrm/5488c9ea-e3a2-5348-887b-065d29b848ab"
      },
      {
        "id": 2,
        "symbol": "B",
        "label": "Brown",
        "description": "Refers to the soil mottle colour.",
        "uri": "https://linked.data.gov.au/def/nrm/6e7eb737-05f5-5c47-a3f3-bde1e6feb689"
      },
      {
        "id": 3,
        "symbol": "D",
        "label": "Dark",
        "description": "Refers to the soil mottle colour. Values 3 or less and chromas 2 or less for all hues.",
        "uri": "https://linked.data.gov.au/def/nrm/ec9e7d3d-44f7-5b99-8c80-d29b2363b10b"
      },
      {
        "id": 4,
        "symbol": "G",
        "label": "Grey",
        "description": "Refers to the soil mottle colour.",
        "uri": "https://linked.data.gov.au/def/nrm/547bbb47-e4ff-53be-b74f-5864cd03cb74"
      },
      {
        "id": 5,
        "symbol": "L",
        "label": "Gley",
        "description": "Refers to the soil mottle colour. Gley refers to a sticky waterlogged soil lacking in oxygen, typically grey to blue in colour.",
        "uri": "https://linked.data.gov.au/def/nrm/1c743f5a-329d-58a7-8a36-1e5c51388637"
      },
      {
        "id": 6,
        "symbol": "O",
        "label": "Orange",
        "description": "Refers to the soil mottle colour.",
        "uri": "https://linked.data.gov.au/def/nrm/00fe4535-5c28-5a9b-985c-e68cf242c4e1"
      },
      {
        "id": 7,
        "symbol": "P",
        "label": "Pale",
        "description": "Refers to the soil mottle colour, with values 7 or more and chromas 2 or less for all hues.",
        "uri": "https://linked.data.gov.au/def/nrm/b545ca80-2b9a-540c-88c1-457858aeb8ec"
      },
      {
        "id": 8,
        "symbol": "R",
        "label": "Red",
        "description": "Refers to the soil mottle colour.",
        "uri": "https://linked.data.gov.au/def/nrm/929eaf9b-fae1-5432-9ff8-9cd39080cb76"
      },
      {
        "id": 9,
        "symbol": "Y",
        "label": "Yellow",
        "description": "Refers to the soil mottle colour.",
        "uri": "https://linked.data.gov.au/def/nrm/9160f408-d47e-5eb4-a46b-66d50bdf655d"
      },
      {
        "id": 10,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      }
    ],
    "lut-soils-mottle-contrast": [
      {
        "id": 1,
        "symbol": "F",
        "label": "Faint",
        "description": "Refers to the soil mottle contrasts. 'Faint' - evident only on close examination with 10x magnification. Little contrast with adjacent material.",
        "uri": "https://linked.data.gov.au/def/nrm/4dfbbd0c-0216-5cdf-9dac-da3f0f234532"
      },
      {
        "id": 2,
        "symbol": "D",
        "label": "Distinct",
        "description": "Refers to the soil mottle contrasts.",
        "uri": "https://linked.data.gov.au/def/nrm/613c80b7-739e-5940-9be3-162d3cc44826"
      },
      {
        "id": 3,
        "symbol": "P",
        "label": "Prominent",
        "description": "Refers to the soil mottle contrasts.",
        "uri": "https://linked.data.gov.au/def/nrm/f76c8b3d-4aee-5f31-87dc-23dabc231443"
      }
    ],
    "lut-soils-mottle-size": [
      {
        "id": 1,
        "symbol": "1",
        "label": "Fine (<5mm)",
        "description": "Refers to the size class of soil mottles, measured along its greatest dimension.",
        "uri": "https://linked.data.gov.au/def/nrm/49afe82d-45d9-54e3-bea9-e11ff4d18313"
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "Medium (5-15mm)",
        "description": "Refers to the size class of soil mottles, measured along its greatest dimension.",
        "uri": "https://linked.data.gov.au/def/nrm/c869555f-9fe9-56c8-ad9f-00717a5c69d3"
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "Coarse (15-30mm)",
        "description": "Refers to the size class of soil mottles, measured along its greatest dimension.",
        "uri": "https://linked.data.gov.au/def/nrm/49778dd7-2312-5038-a80e-c5ad5f73d73b"
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "Very coarse (>30mm)",
        "description": "Refers to the size class of soil mottles, measured along its greatest dimension.",
        "uri": "https://linked.data.gov.au/def/nrm/c63f6be5-ae96-52d3-8367-22807218fc1e"
      },
      {
        "id": 5,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      },
      {
        "id": 6,
        "symbol": "n/a",
        "label": "Not Applicable",
        "description": "Refers to any categorical values/attributes that are 'Not Applicable' to a particular observation in the study.",
        "uri": "https://linked.data.gov.au/def/nrm/5488c9ea-e3a2-5348-887b-065d29b848ab"
      }
    ],
    "lut-soils-mottle-type": [
      {
        "id": 1,
        "symbol": "M",
        "label": "Mottles",
        "description": "A description of the soil mottle types. Mottles are blotches or spots of different colour, patterns that are usually dispersed with the dominant soil colour. ",
        "uri": "https://linked.data.gov.au/def/nrm/0642bcfd-f594-5aba-970d-ffa86daaee3a"
      },
      {
        "id": 2,
        "symbol": "X",
        "label": "Colour patterns due to biological mixing of soil material from other horizons (e.g. worm casts).",
        "description": "A brief description of the soil mottle types.",
        "uri": "https://linked.data.gov.au/def/nrm/4d80644f-7b08-537e-9979-b5d2d2ffe3fb"
      },
      {
        "id": 3,
        "symbol": "Y",
        "label": "Colour patterns due to mechanical mixing of soil material from other horizons (e.g. inclusions of B horizon material in Ap horizons).",
        "description": "A brief description of the soil mottle types.",
        "uri": "https://linked.data.gov.au/def/nrm/8bc0057f-169e-500c-94ca-c4096295eb4b"
      },
      {
        "id": 4,
        "symbol": "Z",
        "label": "Colour patterns due to inclusions of weathered substrate material.",
        "description": "A brief description of the soil mottle types.",
        "uri": "https://linked.data.gov.au/def/nrm/e174f6b5-e1cf-5cca-b9fd-632629f212d4"
      }
    ],
    "lut-soils-observation-type": [
      {
        "id": 1,
        "symbol": "A",
        "label": "Auger boring",
        "description": "Refers to the type/method of soil sample collection and observation.",
        "uri": "https://linked.data.gov.au/def/nrm/3e15274e-2ff4-5e93-b098-55f4df6c3be0"
      },
      {
        "id": 2,
        "symbol": "P",
        "label": "Soil pit",
        "description": "Refers to the type/method of soil sample collection and observation.",
        "uri": "https://linked.data.gov.au/def/nrm/f73cee78-1dd5-5335-a1de-a77183b40144"
      },
      {
        "id": 3,
        "symbol": "E",
        "label": "Exposure",
        "description": "Refers to the type/method of soil sample collection and observation.",
        "uri": "https://linked.data.gov.au/def/nrm/b5827388-51ed-549e-9af1-e36ae44fbd24"
      },
      {
        "id": 4,
        "symbol": "C",
        "label": "Soil core",
        "description": "Refers to the type/method of soil sample collection and observation.",
        "uri": "https://linked.data.gov.au/def/nrm/062073d1-be81-5898-a849-22a74f984a9b"
      },
      {
        "id": 5,
        "symbol": "PA",
        "label": "Pit and Auger",
        "description": "Refers to the type/method of soil sample collection and observation.",
        "uri": "https://linked.data.gov.au/def/nrm/57f86532-7538-5187-89c3-2dc5383e2b42"
      }
    ],
    "lut-soils-pan-cementation": [
      {
        "id": 1,
        "symbol": "0",
        "label": "Uncemented",
        "description": "Refers to the type of cementation in soil pans. Uncemented pans often slakes.",
        "uri": "https://linked.data.gov.au/def/nrm/59cc07af-6b7e-52ae-9061-aa6dee1c4aa5"
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Weakly cemented",
        "description": "Refers to the type of cementation in soil pans. Weakly cemented pans can be crushed between thumb and fore finger.",
        "uri": "https://linked.data.gov.au/def/nrm/ddde41ec-2df6-5440-a047-d1578c79c669"
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Moderately cemented",
        "description": "Refers to the type of cementation in soil pans. Moderately cemented pans are beyond power of thumb and forefinger. Crushes underfoot on hard, flat surface with weight of average person (80 kg) applied slowly.",
        "uri": "https://linked.data.gov.au/def/nrm/535309c7-0d31-5342-9bf9-fa2a24db32d7"
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "Strongly cemented",
        "description": "Refers to the type of cementation in soil pans. Strongly cemented pans cannot be crushed underfoot by weight of  average person applied slowly. Can be broken by  hammer.",
        "uri": "https://linked.data.gov.au/def/nrm/a56c1f80-dbe6-5d04-b908-8542dd295303"
      },
      {
        "id": 5,
        "symbol": "4",
        "label": "Very strongly cemented",
        "description": "Refers to the type of cementation in soil pans. Very strongly cemented pans cannot be broken by hammer, or only with extreme difficulty.",
        "uri": "https://linked.data.gov.au/def/nrm/cade5f11-eb31-51ff-ab1a-8ae5f3bc483c"
      }
    ],
    "lut-soils-pan-continuity": [
      {
        "id": 1,
        "symbol": "C",
        "label": "Continuous",
        "description": "Refers to the continuity of soil pans. Broken - can be broken by cracks and the fragments are disoriented.",
        "uri": "https://linked.data.gov.au/def/nrm/c393210d-5c16-525a-a427-47214e8ff23a"
      },
      {
        "id": 2,
        "symbol": "D",
        "label": "Discontinuous",
        "description": "Refers to the continuity of soil pans. Discontinuous - broken by cracks but original orientation of fragments is preserved.",
        "uri": "https://linked.data.gov.au/def/nrm/2364339f-cd61-515c-b9e7-16fe98f76853"
      },
      {
        "id": 3,
        "symbol": "B",
        "label": "Broken",
        "description": "Refers to the continuity of soil pans. Broken- can be broken by cracks and the fragments are disoriented.",
        "uri": "https://linked.data.gov.au/def/nrm/58e0d269-8f29-5221-ba6a-1bad252d303e"
      }
    ],
    "lut-soils-pan-structure": [
      {
        "id": 1,
        "symbol": "V",
        "label": "Massive",
        "description": "Refers to the structure of soil pans. Massive - no recognisable structure.",
        "uri": "https://linked.data.gov.au/def/nrm/05254074-af21-56bd-8681-f2dd0cca667f"
      },
      {
        "id": 2,
        "symbol": "S",
        "label": "Vesicular",
        "description": "Refers to the structure of soil pans. Vesicular - sponge-like structure having large pores, which may or may not be filled with softer material.",
        "uri": "https://linked.data.gov.au/def/nrm/4116811a-5258-5930-a33b-323ec3720419"
      },
      {
        "id": 3,
        "symbol": "C",
        "label": "Concretionary",
        "description": "Refers to the structure of soil pans. Concretionary - spheroidal concretions cemented together.",
        "uri": "https://linked.data.gov.au/def/nrm/3fc85733-99bb-58c9-aa93-d79f4eb595ee"
      },
      {
        "id": 4,
        "symbol": "N",
        "label": "Nodular",
        "description": "Refers to the structure of soil pans. Nodular - nodules of irregular shape cemented together.",
        "uri": "https://linked.data.gov.au/def/nrm/94ef2c83-9b01-5170-83e6-c5427ff8d924"
      },
      {
        "id": 5,
        "symbol": "L",
        "label": "Platy",
        "description": "Refers to the type of soil peds structure. Platy structures are soil particles arranged around a horizontal plane and bounded by relatively flat horizontal faces with much accommodation to the faces of surrounding peds.",
        "uri": "https://linked.data.gov.au/def/nrm/843789d5-e934-5fc3-85e3-e3dddf30230a"
      },
      {
        "id": 6,
        "symbol": "R",
        "label": "Vermicular",
        "description": "Refers to the structure of soil pans. Vermicular - worm-like structure and/or cavities.",
        "uri": "https://linked.data.gov.au/def/nrm/587fac90-6ff9-5fc0-b354-ac1fe2b6f17e"
      }
    ],
    "lut-soils-pan-type": [
      {
        "id": 1,
        "symbol": "Z",
        "label": "Zero or no pan",
        "description": "Refers to the categorical value with zero or 'No' soil pan observed.",
        "uri": "https://linked.data.gov.au/def/nrm/94486ede-c585-548d-8ef8-41dd83a51923"
      },
      {
        "id": 2,
        "symbol": "K",
        "label": "Calcrete",
        "description": "Calcrete - any cemented, terrestrial carbonate accumulation that may vary significantly in morphology and degree of cementation.",
        "uri": "https://linked.data.gov.au/def/nrm/91d366f9-3947-54a1-a2a3-beabe7f34e33"
      },
      {
        "id": 3,
        "symbol": "L",
        "label": "Silcrete",
        "description": "Silcrete - strongly indurated siliceous material cemented by, and largely composed of, forms of silica, including quartz, chalcedony, opal and chert.",
        "uri": "https://linked.data.gov.au/def/nrm/40dbae74-33a4-5d8f-b4a2-05915a30b587"
      },
      {
        "id": 4,
        "symbol": "R",
        "label": "Red-brown hardpan",
        "description": "Refers to the soil pan type. Red-brown hardpan - earthy pan, which is normally reddish brown to red with dense yet porous appearance; it is very hard, has an irregular laminar cleavage and some vertical cracks, and varies from less than 0.3 m to over 30 m thick. Other variable features are bedded and unsorted sand and gravel lenses; wavy black veinings, probably manganiferous; and, less commonly, off-white veins of calcium carbonate.",
        "uri": "https://linked.data.gov.au/def/nrm/519baa24-f4ee-5d63-8318-62aa261e1a4a"
      },
      {
        "id": 5,
        "symbol": "D",
        "label": "Duripan",
        "description": "Refers to the soil pan type. Duripans - earthy pan so cemented by silica that dry fragments do not slake in water and are always brittle, even after prolonged wetting.",
        "uri": "https://linked.data.gov.au/def/nrm/89762449-5153-52d8-b6f0-7faf72d1acd5"
      },
      {
        "id": 6,
        "symbol": "F",
        "label": "Fragipan",
        "description": "Refers to the soil pan type. Fragipan - earthy pan, which is usually loamy. A dry fragment slakes in water. A wet fragment does not slake in water but has moderate or weak brittleness. Fragipans are more stable on exposure than overlying or underlying horizons.",
        "uri": "https://linked.data.gov.au/def/nrm/d4505c47-a032-52d5-937d-3d34a67fabe6"
      },
      {
        "id": 7,
        "symbol": "N",
        "label": "Densipan",
        "description": "Refers to the soil pan type. Densipans - earthy pan, which is very fine sandy (0.02–0.05 mm). Fragments, both wet and dry, slake in water. Densipans are less stable on exposure than overlying or underlying horizons.",
        "uri": "https://linked.data.gov.au/def/nrm/42de428a-3417-5079-bbcd-b152f0cabfac"
      },
      {
        "id": 8,
        "symbol": "I",
        "label": "Thin ironpan",
        "description": "Refers to the soil pan type. Thin ironpan - commonly thin (2–10 mm), black to dark reddish pan cemented by iron, iron and manganese, or iron–organic matter complexes. Rarely 40 mm thick. Has wavy or convolute form and usually occurs as a single pan.",
        "uri": "https://linked.data.gov.au/def/nrm/39cce638-e1e3-5f39-aaea-0b6ebd295d01"
      },
      {
        "id": 9,
        "symbol": "E",
        "label": "Ferricrete",
        "description": "Ferricrete - indurated material rich in hydrated oxides of iron (usually goethite and hematite) occurring as cemented nodules and/or concretions, or as massive sheets. This material has been commonly referred to in local usage around Australia as laterite, duricrust or ironstone.",
        "uri": "https://linked.data.gov.au/def/nrm/ee3219ca-ab0e-5ad7-b6f0-7a1348aebba6"
      },
      {
        "id": 10,
        "symbol": "A",
        "label": "Alcrete (bauxite)",
        "description": "Alcrete- indurated material rich in aluminium hydroxides. Commonly consists of cemented pisoliths and usually known as bauxite.",
        "uri": "https://linked.data.gov.au/def/nrm/54e540de-abed-5a83-a9e6-e68bc9c93499"
      },
      {
        "id": 11,
        "symbol": "M",
        "label": "Manganiferous pan",
        "description": "Refers to the soil pan type. Manganiferous pan - indurated material dominated by oxides of manganese.",
        "uri": "https://linked.data.gov.au/def/nrm/c6b3a2f3-da6c-5728-9a82-d61f60bc2924"
      },
      {
        "id": 12,
        "symbol": "T",
        "label": "Ortstein",
        "description": "Refers to the soil pan type. Ortstein - horizon strongly cemented by iron and organic matter. It has marked local variability in colour, both laterally and vertically. It may occur in the B horizon of podzols.",
        "uri": "https://linked.data.gov.au/def/nrm/e3349c29-079c-5789-b76c-d86f68513b25"
      },
      {
        "id": 13,
        "symbol": "C",
        "label": "Organic pan",
        "description": "Refers to the soil pan type. Organic pan - horizon relatively high in organic matter but low in iron. It is relatively thick and weakly to strongly cemented by aluminium and usually becomes progressively more cemented with depth. It is usually relatively uniform in appearance laterally. It is commonly the B horizon of humus podzols, where it is often known as coffee rock or sandrock.",
        "uri": "https://linked.data.gov.au/def/nrm/2ed50ae4-a999-5ea1-9fe8-a2d2a7484bac"
      },
      {
        "id": 14,
        "symbol": "V",
        "label": "Cultivation pan",
        "description": "Refers to the soil pan type. Cultivation pan - subsurface soil horizon having higher bulk density, lower total porosity, and lower permeability to both air and water than horizons directly above and below as a result of cultivation practices.",
        "uri": "https://linked.data.gov.au/def/nrm/ff4fc9b6-e9a4-5949-8982-cabd9cea85f5"
      },
      {
        "id": 15,
        "symbol": "O",
        "label": "Other pans",
        "description": "Refers to the categorical value describing any 'other' soil pan type not listed in the collection.",
        "uri": "https://linked.data.gov.au/def/nrm/6fc46b99-2d15-59e3-b171-76350ec1fea7"
      }
    ],
    "lut-soils-permeability": [
      {
        "id": 1,
        "symbol": "1",
        "label": "Very slowly permeable",
        "description": "Refers to the soil permeability with a saturated hydraulic conductivity-Ks range: <5 mm/day, Drainage time: months.",
        "uri": "https://linked.data.gov.au/def/nrm/4c4746ba-b5d2-59ba-989b-a643b3a784ef"
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "Slowly permeable",
        "description": "Refers to the soil permeability with a saturated hydraulic conductivity-Ks range: 5-50mm/day, Drainage time: weeks.",
        "uri": "https://linked.data.gov.au/def/nrm/18f7cf37-11c8-5463-b2bc-65a500c83d6a"
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "Moderately permeable",
        "description": "Refers to the soil permeability with a saturated hydraulic conductivity- Ks range: 50-500mm/day, Drainage time: days.",
        "uri": "https://linked.data.gov.au/def/nrm/3de91acc-a4b3-5141-9861-ce8ef4d949b5"
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "Highly permeable",
        "description": "Ks range: >500mm/day, Drainage time: hours",
        "uri": ""
      }
    ],
    "lut-soils-plasticity-type": [
      {
        "id": 1,
        "symbol": "S",
        "label": "Superplastic",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "N",
        "label": "Normal plasticity",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "U",
        "label": "Subplastic",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "T",
        "label": "Strongly subplastic",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-relative-inclination-of-slope-elements": [
      {
        "id": 1,
        "symbol": "X",
        "label": "Waxing",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "N",
        "label": "Waning",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "M",
        "label": "Maximal",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "I",
        "label": "Minimal",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-rill-water-erosion-degree": [
      {
        "id": 1,
        "symbol": "0",
        "label": "No rill erosion",
        "description": "Refers to the categorical class describing the degree of rill water erosion in soils. A rill is a small channel up to 0.3 m deep, which can be largely obliterated by tillage operations. No rill erosion indicates no signs of rill erosion.",
        "uri": "https://linked.data.gov.au/def/nrm/3b93056e-97da-5ea9-86e1-5e9ff48e1ed8"
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Minor",
        "description": "Refers to the categorical class describing the degree of rill water erosion in soils. A rill is a small channel up to 0.3 m deep, which can be largely obliterated by tillage operations. Minor is for occasional rills.",
        "uri": "https://linked.data.gov.au/def/nrm/95929a81-7427-5633-bc49-509b56672445"
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Moderate",
        "description": "Refers to the categorical class describing the degree of rill water erosion in soils. A rill is a small channel up to 0.3 m deep, which can be largely obliterated by tillage operations. Moderate is for common rills.",
        "uri": "https://linked.data.gov.au/def/nrm/3bce1524-ec7f-58a0-9ca3-500a8e86d3ef"
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "Severe",
        "description": "Refers to the categorical class describing the degree of rill water erosion in soils. A rill is a small channel up to 0.3 m deep, which can be largely obliterated by tillage operations. Severe rill is for numerous rills forming corrugated ground surface.",
        "uri": "https://linked.data.gov.au/def/nrm/cf5b622e-7490-502d-861c-11d0a1ed13b5"
      }
    ],
    "lut-soils-rock-outcrop-abundance": [
      {
        "id": 1,
        "symbol": "0",
        "label": "No rock outcrop (no bedrock exposed)",
        "description": "Refers to the categorical class describing soil rock outcrop abundance. No rock outcrop, with abundance code '0' suggests no bedrock exposed.",
        "uri": "https://linked.data.gov.au/def/nrm/84cad8b2-45c5-5af0-bdae-8ef96d0adc7b"
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Very slightly rocky (<2% bedrock exposed)",
        "description": "Refers to the categorical class describing soil rock outcrop abundance. Very slightly rocky outcrop, with abundance code '1' suggests <2% bedrock exposed.",
        "uri": "https://linked.data.gov.au/def/nrm/b04ac0f3-f7ee-5ff1-a212-2580f743b181"
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Slightly rocky (2-10% bedrock exposed)",
        "description": "Refers to the categorical class describing soil rock outcrop abundance. Slightly rocky outcrop, with abundance code '2' suggests 2-10% bedrock exposed.",
        "uri": "https://linked.data.gov.au/def/nrm/64646801-af41-538b-a5fa-b72a1f0ca02b"
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "Rocky (10-20% bedrock exposed)",
        "description": "Refers to the categorical class describing soil rock outcrop abundance. Rocky outcrop, with abundance code '3' suggests 10-20% bedrock exposed.",
        "uri": "https://linked.data.gov.au/def/nrm/acfa557b-cc6c-52ba-a316-820b6c258009"
      },
      {
        "id": 5,
        "symbol": "4",
        "label": "Very rocky (20-50% bedrock exposed)",
        "description": "Refers to the categorical class describing soil rock outcrop abundance. Very rocky outcrop, with abundance code '4' suggests 20-50% bedrock exposed.",
        "uri": "https://linked.data.gov.au/def/nrm/f99876ce-4384-5d25-85c9-ef212ebdbcf5"
      },
      {
        "id": 6,
        "symbol": "5",
        "label": "Rockland (>50% bedrock exposed)",
        "description": "Refers to the categorical class describing soil rock outcrop abundance. Rockland outcrop, with abundance code '5' suggests >50% bedrock exposed.",
        "uri": "https://linked.data.gov.au/def/nrm/5eba3fe8-5f65-57cc-ba1e-8714c2dd91da"
      }
    ],
    "lut-soils-root-size": [
      {
        "id": 1,
        "symbol": "1",
        "label": "Very fine (<1 mm)",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "Fine (1-2 mm)",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "Medium (2-5 mm)",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "Coarse (>5 mm)",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-scald-erosion-degree": [
      {
        "id": 1,
        "symbol": "0",
        "label": "No scalding",
        "description": "Refers to the categorical class describing the degree of soil scald erosion. Scald erosion is the removal of surface soil by water and/or wind, often exposing a more clayey subsoil which is devoid of vegetation and relatively impermeable to water. No scalding suggests no indication of scalding observed.",
        "uri": "https://linked.data.gov.au/def/nrm/5a5b27be-ff82-5ab0-8a83-9cea61ad1dae"
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Minor scalding (<5%)",
        "description": "Refers to the categorical class describing the degree of soil scald erosion. Scald erosion is the removal of surface soil by water and/or wind, often exposing a more clayey subsoil which is devoid of vegetation and relatively impermeable to water. Minor scalding suggests <5% of site scalded.",
        "uri": "https://linked.data.gov.au/def/nrm/2f8188d1-3b51-5c04-8a6b-9f3b80a2a4d1"
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Moderate scalding (5-50%)",
        "description": "Refers to the categorical class describing the degree of soil scald erosion. Scald erosion is the removal of surface soil by water and/or wind, often exposing a more clayey subsoil which is devoid of vegetation and relatively impermeable to water. Moderate scalding suggests 5-50% of site scalded.",
        "uri": "https://linked.data.gov.au/def/nrm/056e4b3a-d26a-5061-8bcd-3d25f2cfb2f3"
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "Severe scalding (>50%)",
        "description": "Refers to the categorical class describing the degree of soil scald erosion. Scald erosion is the removal of surface soil by water and/or wind, often exposing a more clayey subsoil which is devoid of vegetation and relatively impermeable to water. Severe scalding suggests >50% of site scalded.",
        "uri": "https://linked.data.gov.au/def/nrm/89a31040-6702-5381-b9bf-5452694293c0"
      }
    ],
    "lut-soils-segregation-magnetic-attributes": [
      {
        "id": 1,
        "symbol": "N",
        "label": "Non-magnetic",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "M",
        "label": "Magnetic",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-segregation-strength": [
      {
        "id": 1,
        "symbol": "1",
        "label": "Weak",
        "description": "Refers to the strength of soil segregation. Weak suggests that segregations can be broken between thumb and forefinger.",
        "uri": "https://linked.data.gov.au/def/nrm/216de94b-d66f-5173-a85e-3750a64ecf1d"
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "Strong",
        "description": "Refers to the strength of soil segregation. Strong suggests that segregations cannot be broken between thumb and forefinger.",
        "uri": "https://linked.data.gov.au/def/nrm/549a8392-52b5-50c5-8eed-4f3c9d7f51db"
      }
    ],
    "lut-soils-segregations-abundance": [
      {
        "id": 1,
        "symbol": "0",
        "label": "No segregations",
        "description": "Refers to the abundance of soil segregations. No segregations suggests no observed segregation of soil.",
        "uri": "https://linked.data.gov.au/def/nrm/1703ba11-9827-5882-ac58-bae72032ed49"
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Very few (<2%)",
        "description": "Refers to the abundance of soil segregations. Very few suggests <2% segregation.",
        "uri": "https://linked.data.gov.au/def/nrm/a825d243-d9b0-5c4b-9b96-95795d6acdc1"
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Few (2-10%)",
        "description": "Refers to the abundance of soil segregations. Few suggests 2-10% segregations.",
        "uri": "https://linked.data.gov.au/def/nrm/81f11aec-77c6-5737-8fa7-7691ad8100fe"
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "Common (10-20%)",
        "description": "Refers to the abundance of soil segregations. Common suggests 10-20% segregations.",
        "uri": "https://linked.data.gov.au/def/nrm/ae03f771-f885-5c43-8892-c0c7814668b4"
      },
      {
        "id": 5,
        "symbol": "4",
        "label": "Many (20-50%)",
        "description": "Refers to the abundance of soil segregations. Many suggests 20-50% segregations.",
        "uri": "https://linked.data.gov.au/def/nrm/2b188163-74ed-5047-91f9-857e004b125c"
      },
      {
        "id": 6,
        "symbol": "5",
        "label": "Very many (>50%)",
        "description": "Refers to the abundance of soil segregations. Very many suggests >50% segregation.",
        "uri": "https://linked.data.gov.au/def/nrm/8e888553-695c-5c96-94df-7c6e44b0c4ec"
      },
      {
        "id": 7,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      }
    ],
    "lut-soils-segregations-form": [
      {
        "id": 1,
        "symbol": "n/a",
        "label": "Not Applicable",
        "description": "Refers to any categorical values/attributes that are 'Not Applicable' to a particular observation in the study.",
        "uri": "https://linked.data.gov.au/def/nrm/5488c9ea-e3a2-5348-887b-065d29b848ab"
      },
      {
        "id": 2,
        "symbol": "C",
        "label": "Concretions",
        "description": "Refers to the form of soil segregations. Concretions are spheroidal mineral aggregates. Crudely concentric internal fabric can be seen with naked eye. Includes pisoliths and ooliths.",
        "uri": "https://linked.data.gov.au/def/nrm/c910d744-bb22-5223-b2ee-1e8175b3394e"
      },
      {
        "id": 3,
        "symbol": "F",
        "label": "Fragments",
        "description": "Refers to the form of soil segregations. Fragments are broken pieces of segregations.",
        "uri": "https://linked.data.gov.au/def/nrm/86640f86-d71f-5e3f-8c40-01746c72e922"
      },
      {
        "id": 4,
        "symbol": "L",
        "label": "Laminae",
        "description": "Refers to the form of soil segregations. Laminae are planar, plate-like or sheet-like segregations.",
        "uri": "https://linked.data.gov.au/def/nrm/20cc6c76-ddaf-5155-9fb9-b6fc726c2c0b"
      },
      {
        "id": 5,
        "symbol": "N",
        "label": "Nodules",
        "description": "Refers to the form of soil segregations. Nodules are irregular, rounded mineral aggregates. No concentric or symmetric internal fabric. Can have hollow interior.",
        "uri": "https://linked.data.gov.au/def/nrm/a5d3986c-d20b-55b7-9c3e-3963012d6e55"
      },
      {
        "id": 6,
        "symbol": "R",
        "label": "Root linings",
        "description": "Refers to the form of soil segregations. Root linings are linings of former or current root channels.",
        "uri": "https://linked.data.gov.au/def/nrm/4401bba6-d852-5744-86cf-59a61896566c"
      },
      {
        "id": 7,
        "symbol": "S",
        "label": "Soft segregations",
        "description": "Refers to the form of soil segregations. Soft segregations are finely divided segregations. They contrast with surrounding soil in colour and composition but are not easily separated as discrete bodies. Boundaries may be clearly defined or diffuse.",
        "uri": "https://linked.data.gov.au/def/nrm/a364e1a7-8768-5a69-a4df-fa17f95c44d3"
      },
      {
        "id": 8,
        "symbol": "T",
        "label": "Tubules",
        "description": "Refers to the form of soil segregations. Tubules are medium or coarser (>2 mm wide) tube-like  segregations, which may or may not be hollow.",
        "uri": "https://linked.data.gov.au/def/nrm/32282d46-ce5b-5c9a-ab3e-3ccf48b1dad8"
      },
      {
        "id": 9,
        "symbol": "V",
        "label": "Veins",
        "description": "Refers to the form of soil segregations. Veins are fine (<2 mm wide) linear segregations.",
        "uri": "https://linked.data.gov.au/def/nrm/db17690f-b65b-503f-af0a-c96d0eddb70f"
      },
      {
        "id": 10,
        "symbol": "X",
        "label": "Crystals",
        "description": "Refers to the form of soil segregations. Crystals are single or complex clusters of crystals visible with naked eye or ×10 hand lens.",
        "uri": "https://linked.data.gov.au/def/nrm/72310f07-d86f-5603-807f-c7fd529ead60"
      },
      {
        "id": 11,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      }
    ],
    "lut-soils-segregations-nature": [
      {
        "id": 1,
        "symbol": "A",
        "label": "Aluminous (aluminium)",
        "description": "Refers to the nature of soil segregations. Alumnious suggests the soil segregation rich in aluminium.",
        "uri": "https://linked.data.gov.au/def/nrm/32b3cc3b-5350-5c77-9eec-e4c461192cea"
      },
      {
        "id": 2,
        "symbol": "E",
        "label": "Earthy (dominantly non-clayey)",
        "description": "Refers to the nature of soil segregations. Earthy suggests the soil segregation rich in non-clayey components.",
        "uri": "https://linked.data.gov.au/def/nrm/64243bdc-e212-5a32-801c-a84d2bc83a2a"
      },
      {
        "id": 3,
        "symbol": "F",
        "label": "Ferruginous (iron)",
        "description": "Refers to the nature of soil segregations. Ferruginous suggests the soil segregations rich in iron.",
        "uri": "https://linked.data.gov.au/def/nrm/f75264c6-1e37-50e8-98d3-238025b5c3b5"
      },
      {
        "id": 4,
        "symbol": "G",
        "label": "Ferruginous-organic (iron-organic matter)",
        "description": "Refers to the nature of soil segregations. Ferruginous-organic suggests the soil segregations rich in iron derived from organic matter.",
        "uri": "https://linked.data.gov.au/def/nrm/76a354e9-b79b-51db-8eca-4df06af103cb"
      },
      {
        "id": 5,
        "symbol": "H",
        "label": "Organic (humified; well-decomposed organic matter)",
        "description": "Refers to the nature of soil segregations. Organic suggests the soil segregations rich in humus, organic matter.",
        "uri": "https://linked.data.gov.au/def/nrm/5011c8b4-824b-5585-bd30-44fc4ca4fae2"
      },
      {
        "id": 6,
        "symbol": "K",
        "label": "Calcareous (carbonate)",
        "description": "Refers to the nature of soil segregations. Calcareous suggests the soil segregation rich in calcium carbonates.",
        "uri": "https://linked.data.gov.au/def/nrm/b72a8822-2859-5413-bd70-f88ed486fb9c"
      },
      {
        "id": 7,
        "symbol": "L",
        "label": "Argillaceous (clayey)",
        "description": "Refers to the nature of soil segregations. Argillaceous suggests the soil segregation rich in clays.",
        "uri": "https://linked.data.gov.au/def/nrm/7348607d-d17f-5d9f-8a87-3a90c91968e4"
      },
      {
        "id": 8,
        "symbol": "M",
        "label": "Manganiferous (manganese)",
        "description": "Refers to the nature of soil segregations. Manganiferous suggests the soil segregation rich in manganese.",
        "uri": "https://linked.data.gov.au/def/nrm/af751430-8245-54c7-a0d8-5ed6a16038bb"
      },
      {
        "id": 9,
        "symbol": "N",
        "label": "Ferromanganiferous (iron-manganese)",
        "description": "Refers to the nature of soil segregations. Ferromanganiferous suggests the soil segregation rich in iron and manganese.",
        "uri": "https://linked.data.gov.au/def/nrm/4c385849-82eb-501d-ad4b-f61c74ffa3b9"
      },
      {
        "id": 10,
        "symbol": "O",
        "label": "Other",
        "description": "Represents any 'other' categorical collection NOT listed, actual number to enter.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      },
      {
        "id": 11,
        "symbol": "S",
        "label": "Sulfurous (sulfur)",
        "description": "Refers to the nature of soil segregations. Sulfurous suggests the soil segregation is rich in sulfur.",
        "uri": "https://linked.data.gov.au/def/nrm/a524dcf0-1731-55b1-a9cc-d772ad0eb42e"
      },
      {
        "id": 12,
        "symbol": "Y",
        "label": "Gypseous (gypsum)",
        "description": "Refers to the nature of soil segregations. Gypseous suggests the soil segregation rich in gypsum.",
        "uri": "https://linked.data.gov.au/def/nrm/789d858a-c7f3-59ac-81af-8423e6f59e01"
      },
      {
        "id": 13,
        "symbol": "Z",
        "label": "Saline (visible salt)",
        "description": "Refers to the nature of soil segregations. Saline suggests the soil segregation is rich in salt (visible).",
        "uri": "https://linked.data.gov.au/def/nrm/e6240311-a94d-5e25-832f-b50500c73815"
      },
      {
        "id": 14,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      },
      {
        "id": 15,
        "symbol": "U",
        "label": "Unidentified",
        "description": "Refers to the an unidentified type of soil segregations.",
        "uri": "https://linked.data.gov.au/def/nrm/13575533-bf21-538e-a39f-8d0bf52eccc6"
      },
      {
        "id": 16,
        "symbol": "n/a",
        "label": "Not Applicable",
        "description": "Refers to any categorical values/attributes that are 'Not Applicable' to a particular observation in the study.",
        "uri": "https://linked.data.gov.au/def/nrm/5488c9ea-e3a2-5348-887b-065d29b848ab"
      }
    ],
    "lut-soils-segregations-size": [
      {
        "id": 1,
        "symbol": "n/a",
        "label": "Not Applicable",
        "description": "Refers to any categorical values/attributes that are 'Not Applicable' to a particular observation in the study.",
        "uri": "https://linked.data.gov.au/def/nrm/5488c9ea-e3a2-5348-887b-065d29b848ab"
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Fine (<2mm)",
        "description": "Refers to the size class of soil segregations. Fine type of segregation is when the soil segregates into units measuring <2 mm.",
        "uri": "https://linked.data.gov.au/def/nrm/3ebe19a4-b9e7-5752-9a0d-ec208266e34b"
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Medium (2-6mm)",
        "description": "Refers to the size class of soil segregations. Medium type of segregation is when the soil segregates into units measuring 2-6 mm.",
        "uri": "https://linked.data.gov.au/def/nrm/f67fb429-d7ea-5958-b779-3c626e3f401e"
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "Coarse (6-20mm)",
        "description": "Refers to the size class of soil segregations. Coarse type of segregation is when the soil segregates into units measuring 6-20 mm.",
        "uri": "https://linked.data.gov.au/def/nrm/cbbe0bf0-d0c0-5236-973f-fba8e47936c3"
      },
      {
        "id": 5,
        "symbol": "4",
        "label": "Very coarse (20-60mm)",
        "description": "Refers to the size class of soil segregations. Very coarse type of segregation is when the soil segregates into units measuring 20-60 mm.",
        "uri": "https://linked.data.gov.au/def/nrm/0ee7e5d2-6fe9-5ed8-90ed-22fa37acb872"
      },
      {
        "id": 6,
        "symbol": "5",
        "label": "Extremely coarse (>60mm)",
        "description": "Refers to the size class of soil segregations. Extremely coarse type of segregation is when the soil segregates into units measuring >60 mm.",
        "uri": "https://linked.data.gov.au/def/nrm/645bd97b-96f1-5803-963e-73d146cd4718"
      },
      {
        "id": 7,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      }
    ],
    "lut-soils-sheet-water-erosion-degree": [
      {
        "id": 1,
        "symbol": "X",
        "label": "Not apparent",
        "description": "Refers to the extent of sheet water erosion in soil. In this case, the sheet water erosion is not apparent.",
        "uri": "https://linked.data.gov.au/def/nrm/4922e8d9-d801-5cd5-828b-2f061aca668a"
      },
      {
        "id": 2,
        "symbol": "0",
        "label": "No sheet erosion",
        "description": "Refers to the extent of sheet water erosion in soil. In this case, No evidence of sheet water erosion observed.",
        "uri": "https://linked.data.gov.au/def/nrm/ecaef504-69de-5d52-868e-9a089371de1c"
      },
      {
        "id": 3,
        "symbol": "1",
        "label": "Minor",
        "description": "Refers to the extent of sheet water erosion in soil. 'Minor' suggests indicators which may include shallow soil deposits in downslope sediment traps (fencelines, farm dams). Often very difficult to assess as evidence may be lost with cultivation, pedoturbation or revegetation.",
        "uri": "https://linked.data.gov.au/def/nrm/82827a31-2a73-5a87-842f-df03225af9c5"
      },
      {
        "id": 4,
        "symbol": "2",
        "label": "Moderate",
        "description": "Refers to the extent of sheet water erosion in soil. 'Moderate' suggests indicators that may include partial exposure of roots, moderate soil deposits in downslope sediment traps (fencelines, farm dams).",
        "uri": "https://linked.data.gov.au/def/nrm/5eb1f912-c9d2-5fe7-8289-fcbd1b51d27d"
      },
      {
        "id": 5,
        "symbol": "3",
        "label": "Severe",
        "description": "Refers to the extent of sheet water erosion in soil. 'Severe' suggests indicators that may include loss of surface horizons, exposure of subsoil horizons, pedestalling, root exposure, substantial soil deposits in downslope sediment traps (fencelines, farm dams).",
        "uri": "https://linked.data.gov.au/def/nrm/c09496b7-5439-516c-9358-bc40a433b314"
      }
    ],
    "lut-soils-soil-strength": [
      {
        "id": 1,
        "symbol": "0",
        "label": "Loose",
        "description": "Refers to the strength of soil. Strength of soil is the resistance to breaking or deformation. Loose soils require no force to separate particles such as loose sands.",
        "uri": "https://linked.data.gov.au/def/nrm/562206dd-5064-55cd-8b56-df48aa4012ee"
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Very weak",
        "description": "Refers to the strength of soil. Very weak soil requires very small force, almost nil.",
        "uri": "https://linked.data.gov.au/def/nrm/870a12cc-5b77-5e56-9b14-415019e9c82d"
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Weak",
        "description": "Refers to the strength of soil. Very weak soil requires very small force, almost nil.",
        "uri": "https://linked.data.gov.au/def/nrm/870a12cc-5b77-5e56-9b14-415019e9c82d"
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "Firm",
        "description": "Refers to the strength of soil. Strength of soil is the resistance to breaking or deformation. Firm soils require moderate or firm force to be applied.",
        "uri": "https://linked.data.gov.au/def/nrm/9c2fb110-c57d-5ffc-9aa0-b906f3cafd16"
      },
      {
        "id": 5,
        "symbol": "4",
        "label": "Very firm",
        "description": "Refers to the strength of soil. Strength of soil is the resistance to breaking or deformation. Very firm soils require strong force but within power of thumb and forefinger.",
        "uri": "https://linked.data.gov.au/def/nrm/fa081977-b014-55cc-ad7b-de85090ad8fd"
      },
      {
        "id": 6,
        "symbol": "5",
        "label": "Strong",
        "description": "Refers to the strength of soil. Strength of soil is the resistance to breaking or deformation. Strong soils are beyond the power of thumb and forefinger. Crushes underfoot on hard, flat surface with small force.",
        "uri": "https://linked.data.gov.au/def/nrm/1df397c4-9970-52a3-9998-d5b17e13c0c8"
      },
      {
        "id": 7,
        "symbol": "6",
        "label": "Very strong",
        "description": "Refers to the strength of soil. Very strong soil crushes underfoot on hard, flat surface with full body weight applied slowly.",
        "uri": "https://linked.data.gov.au/def/nrm/134b07ee-248a-55bc-8be0-5abafed7d2c4"
      },
      {
        "id": 8,
        "symbol": "7",
        "label": "Rigid",
        "description": "Refers to the strength of soil. Strength of soil is the resistance to breaking or deformation. Rigid soils cannot be crushed under foot by full body weight applied slowly.",
        "uri": "https://linked.data.gov.au/def/nrm/2de47545-5d51-56d0-b131-1c87368337ab"
      }
    ],
    "lut-soils-stickiness": [
      {
        "id": 1,
        "symbol": "0",
        "label": "Non-sticky",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "1",
        "label": "Slightly sticky",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "2",
        "label": "Moderately sticky",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "3",
        "label": "Very sticky",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-stream-bank-water-erosion-degree": [
      {
        "id": 1,
        "symbol": "X",
        "label": "Not apparent",
        "description": "Refers to 'no apparent' stream bank water erosion observed in soil.",
        "uri": "https://linked.data.gov.au/def/nrm/44738bce-88d2-59f9-bed1-787a0894ec15"
      },
      {
        "id": 2,
        "symbol": "0",
        "label": "No stream bank erosion",
        "description": "Refers to 'no' evidence of stream bank water erosion observed in soil.",
        "uri": "https://linked.data.gov.au/def/nrm/45b4a990-a65f-5d24-90c6-fee77b720e2b"
      },
      {
        "id": 3,
        "symbol": "1",
        "label": "Present",
        "description": "Refers to the presence of stream bank water erosion in soil.",
        "uri": "https://linked.data.gov.au/def/nrm/b0cc5678-f157-590b-ada2-8eb1b3c1d98c"
      }
    ],
    "lut-soils-structure-compound-pedality": [
      {
        "id": 1,
        "symbol": "1",
        "label": "Largest peds (in the type of soil observation described), parting to",
        "description": "A description of the structure of compound peds in soil. Compound pedality occurs where large peds part along natural planes of weakness to form smaller peds, which may again part to smaller peds, and so on to the smallest or primary peds.",
        "uri": "https://linked.data.gov.au/def/nrm/ef6e5a6b-335d-5964-9843-1e77e41e43d2"
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "Next size peds, parting to",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "Next size peds, ... and further, if required, to the primary ped.",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-structure-grade": [
      {
        "id": 1,
        "symbol": "G",
        "label": "Single grain",
        "description": "Refers to the soil structure grade size. Single grain suggests loose mass of individual particles.  When displaced, soil separates into ultimate particles.",
        "uri": "https://linked.data.gov.au/def/nrm/08745403-0d44-5e04-81fd-efed7fea5ec3"
      },
      {
        "id": 2,
        "symbol": "V",
        "label": "Massive",
        "description": "Refers to the soil structure grade size. Massive suggests the peds are coherent. When displaced, soil separates into fragments, which may be crushed to ultimate particles.",
        "uri": "https://linked.data.gov.au/def/nrm/9f9f6e46-68c3-5494-b3b8-c1c3ba74bafd"
      },
      {
        "id": 3,
        "symbol": "W",
        "label": "Weak",
        "description": "Refers to the soil structure grade size. Weak peds are indistinct and barely observable in undisplaced soil. When displaced, up to one-third of the soil material consists of peds, the remainder consisting of variable amounts of fragments and ultimate particles.",
        "uri": "https://linked.data.gov.au/def/nrm/4ed9e4f5-5538-5f56-a74a-d6c1ab764d7a"
      },
      {
        "id": 4,
        "symbol": "M",
        "label": "Moderate",
        "description": "Refers to the soil structure grade size. Moderate suggests the peds are well-formed and evident but not distinct in undisplaced soil. Adhesion between peds is usually firm or stronger. When displaced, more than one-third of the soil material consists of entire peds, the remainder consisting of broken peds, fragments and ultimate particles. When displaced, soil separates into fragments, which may be crushed to ultimate particles.",
        "uri": "https://linked.data.gov.au/def/nrm/63cd3132-df93-580f-945f-dd4ca3c163e6"
      },
      {
        "id": 5,
        "symbol": "S",
        "label": "Strong",
        "description": "Refers to the soil structure grade size. Strong peds are quite distinct in undisplaced soil. Adhesion between peds is usually firm or weaker. When displaced, more than two-thirds of the soil material consists of entire peds.",
        "uri": "https://linked.data.gov.au/def/nrm/13d1b9b5-5189-5006-b807-46f33cc1be13"
      }
    ],
    "lut-soils-structure-size": [
      {
        "id": 1,
        "symbol": "1",
        "label": "<2mm",
        "description": "Refers to the size class of soil peds structure.",
        "uri": "https://linked.data.gov.au/def/nrm/63ebf87c-7eee-52c6-bbb4-cf3fd46e0c04"
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "2-5mm",
        "description": "Refers to the size class of soil peds structure.",
        "uri": "https://linked.data.gov.au/def/nrm/127ce77c-9dd5-5b29-a4e3-37946fb4b469"
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "5-10mm",
        "description": "Refers to the size class of soil peds structure.",
        "uri": "https://linked.data.gov.au/def/nrm/fe89ce0e-8689-5f07-9713-12f479711b89"
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "10-20mm",
        "description": "Refers to the size class of soil peds structure.",
        "uri": "https://linked.data.gov.au/def/nrm/a4db3418-bd23-5e5f-9ed1-2d3c2c030f3f"
      },
      {
        "id": 5,
        "symbol": "5",
        "label": "20-50mm",
        "description": "Refers to the size class of soil peds structure.",
        "uri": "https://linked.data.gov.au/def/nrm/864e0574-cf14-5329-bdb0-cba5396c4223"
      },
      {
        "id": 6,
        "symbol": "6",
        "label": "50-100mm",
        "description": "Refers to the size class of soil peds structure.",
        "uri": "https://linked.data.gov.au/def/nrm/8a26cbd1-4b3a-5ada-a5cb-e9f37d88b499"
      },
      {
        "id": 7,
        "symbol": "7",
        "label": "100-200mm",
        "description": "Refers to the size class of soil peds structure.",
        "uri": "https://linked.data.gov.au/def/nrm/b446e536-bd08-52fe-b8b1-a74dc9ea1c36"
      },
      {
        "id": 8,
        "symbol": "8",
        "label": "200-500mm",
        "description": "Refers to the size class of soil peds structure.",
        "uri": "https://linked.data.gov.au/def/nrm/dd95d9f3-b3a6-5b25-b99e-e7dc055593f6"
      },
      {
        "id": 9,
        "symbol": "9",
        "label": ">500mm",
        "description": "Refers to the size class of soil peds structure.",
        "uri": "https://linked.data.gov.au/def/nrm/990eaa40-e067-5215-b9ce-dc6d7a8c16d3"
      },
      {
        "id": 10,
        "symbol": "10",
        "label": "Not Applicable",
        "description": "Refers to any categorical values/attributes that are 'Not Applicable' to a particular observation in the study.",
        "uri": "https://linked.data.gov.au/def/nrm/5488c9ea-e3a2-5348-887b-065d29b848ab"
      },
      {
        "id": 11,
        "symbol": "11",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      }
    ],
    "lut-soils-structure-type": [
      {
        "id": 1,
        "symbol": "PL",
        "label": "Platy",
        "description": "Refers to the type of soil peds structure. Platy structures are soil particles arranged around a horizontal plane and bounded by relatively flat horizontal faces with much accommodation to the faces of surrounding peds.",
        "uri": "https://linked.data.gov.au/def/nrm/cced4eb1-b8e8-5a0c-b991-996966242c03"
      },
      {
        "id": 2,
        "symbol": "PR",
        "label": "Prismatic",
        "description": "Refers to the type of soil peds structure. Prismatic structures are soil particles arranged around a vertical axis and bounded by well-defined, relatively flat faces with much accommodation to the faces of surrounding peds. Vertices between adjoining faces are usually angular.",
        "uri": "https://linked.data.gov.au/def/nrm/9437de19-5084-577c-a418-075bd5b6c941"
      },
      {
        "id": 3,
        "symbol": "CO",
        "label": "Columnar",
        "description": "Refers to the type of soil peds structure. Columnar as for prismatic but with domed tops.",
        "uri": "https://linked.data.gov.au/def/nrm/ceea60d8-93ee-564d-8b4a-12d7a48fbd16"
      },
      {
        "id": 4,
        "symbol": "AB",
        "label": "Angular blocky",
        "description": "Refers to the type of soil peds structure. Angular blocky soil particles are arranged around a point and bounded by six relatively flat, roughly equal faces. Re-entrant angles between adjoining faces are few or absent. There is usually much accommodation of ped faces to the faces of surrounding peds. Most vertices between adjoining faces are angular.",
        "uri": "https://linked.data.gov.au/def/nrm/d3fad458-def2-5187-84d3-2a013b9f1e83"
      },
      {
        "id": 5,
        "symbol": "SB",
        "label": "Subangular blocky",
        "description": "Refers to the type of soil peds structure. Subangular blocky are similar to angular blocky except peds are bounded by flat and rounded faces with limited accommodation to the faces of surrounding peds. Many vertices are rounded.",
        "uri": "https://linked.data.gov.au/def/nrm/f61c48e2-282e-5553-be93-e3a0d34b5d8a"
      },
      {
        "id": 6,
        "symbol": "PO",
        "label": "Polyhedral",
        "description": "Refers to the type of soil peds structure. Polyhedral structures are soil particles arranged around a point and bounded by more than six relatively flat, unequal, dissimilar faces. Re-entrant angles between adjoining faces are a feature. There is usually much accommodation of ped faces to the faces of surrounding peds. Most vertices are angular.",
        "uri": "https://linked.data.gov.au/def/nrm/89b73841-35b7-5227-8f46-a4c629436a87"
      },
      {
        "id": 7,
        "symbol": "LE",
        "label": "Lenticular",
        "description": "Refers to the type of soil peds structure. Lenticular structures are soil particles arranged around an elliptical or circular plane and bounded by curved faces with much accommodation to the faces of surrounding peds. Most vertices are angular and acute.",
        "uri": "https://linked.data.gov.au/def/nrm/bcad5d94-ff42-54ae-8ada-cff8ebf3a679"
      },
      {
        "id": 8,
        "symbol": "GR",
        "label": "Granular",
        "description": "Refers to the type of soil peds structure. Granular structures are spheroidal with limited accommodation to the faces of surrounding peds.",
        "uri": "https://linked.data.gov.au/def/nrm/b29f14c0-04aa-5150-afa1-eb2c75938b5a"
      },
      {
        "id": 9,
        "symbol": "CA",
        "label": "Cast",
        "description": "Refers to the type of soil peds structure. Casts are formed from, or are deposited in, the O horizons or the soil solum and include: a) excreta of soil fauna which may be discrete particles, for example insect faeces or the dense, coherent, globular forms of earthworm excreta. They are generally spherical or ovate in shape and have a strong conchoidal fracture; b) Soil masticated with salivary secretions into globular forms, for example, by ants, crickets, wasps.",
        "uri": "https://linked.data.gov.au/def/nrm/3ba45b0a-e9cc-5e97-b515-8bf31207ab51"
      },
      {
        "id": 10,
        "symbol": "CL",
        "label": "Clod",
        "description": "Refers to the soil texture. Coherent plastic bolus; medium sized sand grains visible in the finer matrix; will form ribbon of 40-50mm.",
        "uri": "https://linked.data.gov.au/def/nrm/983fe35a-651c-5f0a-a0a2-1f6323c3690d"
      },
      {
        "id": 11,
        "symbol": "FR",
        "label": "Fragment",
        "description": "Refers to the type of soil structure. Clods are artificial aggregates formed by cultivation or work being done on the soil. They differ from natural peds and are often with 100mm diameter or less.",
        "uri": "https://linked.data.gov.au/def/nrm/c104a471-0584-5b57-96f6-4a8a2a5d564d"
      }
    ],
    "lut-soils-surface-soil-condition": [
      {
        "id": 1,
        "symbol": "C",
        "label": "Surface crust",
        "description": "Refers to the condition of the soil surface when dry. Surface crust are distinct surface layer, often laminated, ranging in thickness from a few millimetres to a few tens of millimetres, which is hard and brittle when dry and cannot be readily separated from, and lifted off, the underlying soil material.",
        "uri": "https://linked.data.gov.au/def/nrm/90a75387-df88-541b-aec8-a47dda66eef8"
      },
      {
        "id": 2,
        "symbol": "F",
        "label": "Firm",
        "description": "Refers to the condition of the soil surface when dry. Firm surface are coherent mass of individual particles or aggregates. Surface disturbed or indented by moderate pressure of forefinger.",
        "uri": "https://linked.data.gov.au/def/nrm/6a624a2c-bc16-5ef0-ad2e-800438e5b711"
      },
      {
        "id": 3,
        "symbol": "G",
        "label": "Cracking",
        "description": "Refers to the condition of the soil surface when dry. Soils with cracks at least 5 mm wide and extending upwards to the surface or to the base of any plough layer or thin (<0.03 m) surface horizon.",
        "uri": "https://linked.data.gov.au/def/nrm/262a6e4d-dd78-579e-a79b-d08a704d5a4b"
      },
      {
        "id": 4,
        "symbol": "H",
        "label": "Hard setting",
        "description": "Refers to the condition of the soil surface when dry. Compact, hard, apparently apedal condition forms on drying but softens on wetting. When dry, the material is hard below any surface crust or flake that may occur, and is not disturbed or indented by pressure of forefinger.",
        "uri": "https://linked.data.gov.au/def/nrm/b1f666fc-45c7-5c95-a8e3-87b08356ad2a"
      },
      {
        "id": 5,
        "symbol": "L",
        "label": "Loose",
        "description": "Refers to the strength of soil. Strength of soil is the resistance to breaking or deformation. Loose soils require no force to separate particles such as loose sands.",
        "uri": "https://linked.data.gov.au/def/nrm/562206dd-5064-55cd-8b56-df48aa4012ee"
      },
      {
        "id": 6,
        "symbol": "M",
        "label": "Self-mulching",
        "description": "Refers to the condition of the soil surface when dry. Self-mulching surface are strongly pedal, loose surface mulch forms on wetting and drying. Peds commonly less than 5 mm in the least dimension.",
        "uri": "https://linked.data.gov.au/def/nrm/6860779f-d4a1-5ef1-a894-06e15cfb0711"
      },
      {
        "id": 7,
        "symbol": "P",
        "label": "Poached",
        "description": "Refers to the condition of the soil surface when dry. Poached surface are soils that has been extensively trampled under wet conditions by hoofed animals.",
        "uri": "https://linked.data.gov.au/def/nrm/f8e80425-9d2b-5b0d-8746-9c94babefeaa"
      },
      {
        "id": 8,
        "symbol": "R",
        "label": "Recently cultivated",
        "description": "Refers to the condition of the soil surface when dry. Recently cultivated soils are those where the effect of cultivation is obvious.",
        "uri": "https://linked.data.gov.au/def/nrm/ff695281-8919-5897-8eff-00bb86fd4572"
      },
      {
        "id": 9,
        "symbol": "S",
        "label": "Soft",
        "description": "Refers to the condition of the soil surface when dry. Soft surface are coherent mass of individual particles or aggregates. Surface easily disturbed by pressure of forefinger.",
        "uri": "https://linked.data.gov.au/def/nrm/04c552d5-8cf7-58de-9bc8-39a1c6baf09b"
      },
      {
        "id": 10,
        "symbol": "T",
        "label": "Trampled",
        "description": "Refers to the condition of the soil surface when dry. Surface soil that has been extensively trampled under dry conditions by hoofed animals.",
        "uri": "https://linked.data.gov.au/def/nrm/43ab637f-fc39-5ced-9776-bfe2c5e7eece"
      },
      {
        "id": 11,
        "symbol": "V",
        "label": "Sandy veneer",
        "description": "Refers to the condition of the soil surface when dry. Sandy veneer are surface veneer with sand grains, and are generally hydrophobic in nature.",
        "uri": "https://linked.data.gov.au/def/nrm/a4f2f67a-bb7a-58be-aedf-ad37001dcd24"
      },
      {
        "id": 12,
        "symbol": "X",
        "label": "Surface flake",
        "description": "Refers to the condition of the soil surface when dry. Surface flake are thin, massive surface layer, usually less than 10 mm thick, which on drying separates from, and can be readily lifted off, the soil below. It usually consists mainly of dispersed clay, and may become increasingly fragile as the soil dries.",
        "uri": "https://linked.data.gov.au/def/nrm/4185ceda-417f-5835-a4be-23b91e2d3b75"
      },
      {
        "id": 13,
        "symbol": "Y",
        "label": "Cryptogam surface",
        "description": "Refers to the condition of the soil surface when dry. Cryptogram are thin, more or less continuous crust of biologically stabilised soil material usually due to algae, liverworts and mosses.",
        "uri": "https://linked.data.gov.au/def/nrm/81a57a57-3c7c-55ea-8d92-4c872cd9ad33"
      },
      {
        "id": 14,
        "symbol": "Z",
        "label": "Saline",
        "description": "Refers to the condition of the soil surface when dry. Saline surface has a visible salt, or salinity is evident from the absence or nature of the vegetation or from soil consistence. These conditions are  characterised by their notable difference from adjacent non-saline areas.",
        "uri": "https://linked.data.gov.au/def/nrm/6185d451-12ea-50d6-ae72-5695c65edb1d"
      },
      {
        "id": 15,
        "symbol": "U",
        "label": "Unknown",
        "description": "Unknown/unable to be determined.",
        "uri": "https://linked.data.gov.au/def/nrm/f6b0f6d8-16d8-5dd7-b1b7-66b0c020b96f"
      },
      {
        "id": 16,
        "symbol": "O",
        "label": "Other (see site notes)",
        "description": "",
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      }
    ],
    "lut-soils-texture-grade": [
      {
        "id": 1,
        "symbol": "S",
        "label": "Sand",
        "description": "Refers to the soil texture. Coherence nil to very slight; cannot be moulded; sand grains of medium size; single sand grains adhere to fingers.",
        "uri": "https://linked.data.gov.au/def/nrm/cbc99014-2280-59e3-b381-dfcee73e301d"
      },
      {
        "id": 2,
        "symbol": "SL",
        "label": "Sandy loam",
        "description": "Refers to the soil texture. Bolus coherent but very sandy to touch; will form ribbon of 15-25mm; dominant sand grains are of medium size and are readily visible.",
        "uri": "https://linked.data.gov.au/def/nrm/dc8d3653-4b29-5d3d-a80d-23e6e85a922e"
      },
      {
        "id": 3,
        "symbol": "SCL",
        "label": "Sandy clay loam",
        "description": "Refers to the soil texture. Strongly coherent bolus; sandy to touch; medium size sand grains visible in finer matrix; will form ribbon of 25-40mm.",
        "uri": "https://linked.data.gov.au/def/nrm/0e40af07-4ebd-56d2-8de4-2a6ce998abc3"
      },
      {
        "id": 4,
        "symbol": "CLS",
        "label": "Clay loam sandy",
        "description": "Refers to the soil texture. Coherent plastic bolus; medium sized sand grains visible in the finer matrix; will form ribbon of 40-50mm.",
        "uri": "https://linked.data.gov.au/def/nrm/3f60f399-ab5c-54bd-af68-bb6016c0c485"
      },
      {
        "id": 5,
        "symbol": "CS",
        "label": "Clayey sand",
        "description": "Refers to the soil texture. Slight coherence; sand grains of medium size; sticky when wet; many sand grains stick to fingers; will form minimal ribbon of 5-15mm; discolours fingers with clay stain.",
        "uri": "https://linked.data.gov.au/def/nrm/ef5d653b-7d5a-52b8-9bcf-7adfd6ef77ad"
      },
      {
        "id": 6,
        "symbol": "CL",
        "label": "Clay loam",
        "description": "Refers to the soil texture. Coherent plastic bolus; smooth to manipulate; will form ribbon of 40-50mm.",
        "uri": "https://linked.data.gov.au/def/nrm/4fc26d95-cc5e-5b14-80f4-1a3acf8569a6"
      },
      {
        "id": 7,
        "symbol": "HC",
        "label": "Heavy clay",
        "description": "Refers to the soil texture. Smooth plastic bolus; handles like stiff plasticine; can be moulded into rods without fracture; has firm resistance to ribboning shear; will form ribbon of 75mm or more.",
        "uri": "https://linked.data.gov.au/def/nrm/f3c2fb29-5975-5370-8873-d0ef91200524"
      },
      {
        "id": 8,
        "symbol": "L",
        "label": "Loam",
        "description": "Refers to the soil texture. Bolus coherent and rather spongy; smooth feel when manipulated but with no obvious sandiness or 'silkiness'; may be somewhat greasy to the touch if organic matter present; will form ribbon of about 25mm.",
        "uri": "https://linked.data.gov.au/def/nrm/057de404-300b-5a80-b9ce-4dee368ba91a"
      },
      {
        "id": 9,
        "symbol": "LC",
        "label": "Light clay",
        "description": "Refers to the soil texture. Plastic bolus; smooth to touch; slight resistance to shearing between thumb and forefinger; will form ribbon of 50-75mm.",
        "uri": "https://linked.data.gov.au/def/nrm/3247291a-a766-5b32-9c2e-9a1e8cb197dc"
      },
      {
        "id": 10,
        "symbol": "LMC",
        "label": "Light medium clay",
        "description": "Refers to the soil texture. Plastic bolus; smooth to touch; slight to moderate resistance to ribboning shear; will form ribbon of about 75mm.",
        "uri": "https://linked.data.gov.au/def/nrm/39155a96-3ae9-51fa-9eef-592f05fcf7b6"
      },
      {
        "id": 11,
        "symbol": "LS",
        "label": "Loamy sand",
        "description": "Refers to the soil texture. Slight coherence; sand grains of medium size; can be sheared between thumb and forefinger to give minimal ribbon of about 5mm.",
        "uri": "https://linked.data.gov.au/def/nrm/86b2e947-62b1-52a5-b205-9fcc60b1eee9"
      },
      {
        "id": 12,
        "symbol": "MC",
        "label": "Medium clay",
        "description": "Refers to the soil texture. Smooth plastic bolus; handles like plasticine and can be moulded into rods without fracture; has some resistance to ribboning shear; will form ribbon of 75mm or more.",
        "uri": "https://linked.data.gov.au/def/nrm/1785f66b-3dd8-52a9-979a-4ec5f8cc32e9"
      },
      {
        "id": 13,
        "symbol": "MHC",
        "label": "Medium heavy clay",
        "description": "Refers to the soil texture. Smooth plastic bolus; handles like plasticine and can be moulded into rods without fracture; has moderate to firm resistance to ribboning shear; will form ribbon of 75mm or more.",
        "uri": "https://linked.data.gov.au/def/nrm/59b5495b-258d-5133-9131-b0ce3ed70b4c"
      },
      {
        "id": 14,
        "symbol": "ZCL",
        "label": "Silty clay loam",
        "description": "Refers to the soil texture. Coherent smooth bolus; plastic and silky to the touch; will form ribbon of 40-50 mm.",
        "uri": "https://linked.data.gov.au/def/nrm/04bded35-e5d8-5b55-84d0-02cd90193be7"
      },
      {
        "id": 15,
        "symbol": "ZL",
        "label": "Silty loam",
        "description": "Refers to the soil texture. Coherent bolus; very smooth to often silky when manipulated; will form ribbon of about 25mm.",
        "uri": "https://linked.data.gov.au/def/nrm/a9526e2b-9058-571a-a22d-b798d682431d"
      },
      {
        "id": 16,
        "symbol": "NA",
        "label": "Not Applicable",
        "description": "Refers to any categorical values/attributes that are 'Not Applicable' to a particular observation in the study.",
        "uri": "https://linked.data.gov.au/def/nrm/5488c9ea-e3a2-5348-887b-065d29b848ab"
      },
      {
        "id": 17,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Refers to the 'No Information/data' collected.",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      }
    ],
    "lut-soils-texture-modifier": [
      {
        "id": 1,
        "symbol": "F",
        "label": "Fine sandy",
        "description": "Refers to the soil texture modifier class. Fine sandy can be felt and often heard when manipulated. Sand grains are clearly evident under a 10x hand lens.",
        "uri": "https://linked.data.gov.au/def/nrm/aeec2d65-6679-55e9-8b34-a39dfc5f530b"
      },
      {
        "id": 2,
        "symbol": "K",
        "label": "Coarse sandy",
        "description": "Refers to the soil texture modifier class. Coarse sandy is obviously coarse to touch. Sand\n grains are very readily seen with the naked eye.",
        "uri": "https://linked.data.gov.au/def/nrm/d8296db9-adef-58ae-829d-d6c1329db400"
      },
      {
        "id": 3,
        "symbol": "S",
        "label": "Medium sandy",
        "description": "Refers to the soil texture modifier. Medium sandy soil cannot be moulded; sand grains of medium size; single sand grains adhere to fingers.",
        "uri": "https://linked.data.gov.au/def/nrm/4b9e285a-989f-5a6c-83de-c4d678c13030"
      },
      {
        "id": 4,
        "symbol": "Z",
        "label": "Silty",
        "description": "Refers to the soil texture modifier. Silty are coherent bolus; very smooth to often silky when manipulated; will form ribbon of about 25 mm.",
        "uri": "https://linked.data.gov.au/def/nrm/bbfc48dc-0699-5d9f-b439-b044b47f8052"
      }
    ],
    "lut-soils-texture-qualification": [
      {
        "id": 1,
        "symbol": "-",
        "label": "Light",
        "description": "Refers to the soil texture qualification. The non-clay field texture grades (clay loams and coarser) may be qualified according as light (lower clay content) end of the range for that particular field texture grade.",
        "uri": "https://linked.data.gov.au/def/nrm/e21bff99-c397-5b4e-8826-a5f7413f439f"
      },
      {
        "id": 2,
        "symbol": "+",
        "label": "Heavy",
        "description": "Refers to the soil texture qualification. The non-clay field texture grades (clay loams and coarser) may be qualified according as Heavy (higher clay content) end of the range for that particular field texture grade.",
        "uri": "https://linked.data.gov.au/def/nrm/06bf97b7-92fc-58ef-8161-4006078baa59"
      },
      {
        "id": 3,
        "symbol": "A",
        "label": "Sapric",
        "description": "Refers to the soil texture qualification. Sapric are organic and non-fibrous; dark organic stain discolours fingers; greasy feel in clayey textures and coherence in sandy textures. Fibres (excluding living roots) or plant tissue remains are not visible to naked eye and little or none visible with ×10 hand lens.",
        "uri": "https://linked.data.gov.au/def/nrm/611fa53a-3695-5e0d-8028-44477bdd0395"
      },
      {
        "id": 4,
        "symbol": "I",
        "label": "Fibric",
        "description": "Fibric texture are organic and fibrous; dark organic stain discolours fingers; greasy feel in clayey textures and coherence in sandy textures. Fibres (excluding living roots) or plant tissue remain visible to naked eye or easily visible with ×10  hand lens.",
        "uri": "https://linked.data.gov.au/def/nrm/ee60f3ef-c175-5bbd-97df-3ad118bef145"
      }
    ],
    "lut-soils-tunnel-water-erosion-degree": [
      {
        "id": 1,
        "symbol": "X",
        "label": "Not apparent",
        "description": "Refers to the 'No apparent' soil water tunnel erosion observed.",
        "uri": "https://linked.data.gov.au/def/nrm/98da7c8e-3997-565b-bb22-12b874f91641"
      },
      {
        "id": 2,
        "symbol": "0",
        "label": "No tunnel erosion",
        "description": "Refers to No soil water tunnel erosion observed.",
        "uri": "https://linked.data.gov.au/def/nrm/4133bf48-7c5a-530e-8d9c-6b6db22314e1"
      },
      {
        "id": 3,
        "symbol": "1",
        "label": "Present",
        "description": "Refers to the presence of soil water tunnel erosion.",
        "uri": "https://linked.data.gov.au/def/nrm/65de8cd3-25a0-5b09-bc85-893f620cd31d"
      }
    ],
    "lut-soils-void-cracks": [
      {
        "id": 1,
        "symbol": "1",
        "label": "Fine (<5mm)",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "Medium (5-10mm)",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "Coarse (10-20mm)",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "Very coarse (20-50mm)",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "5",
        "label": "Extremely coarse (>50mm)",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-water-status": [
      {
        "id": 1,
        "symbol": "D",
        "label": "Dry",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "T",
        "label": "Moderately moist",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "M",
        "label": "Moist",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "W",
        "label": "Wet",
        "description": "",
        "uri": ""
      }
    ],
    "lut-soils-wave-water-erosion-degree": [
      {
        "id": 1,
        "symbol": "X",
        "label": "Not apparent",
        "description": "Refers to No apparent wave water erosion observed.",
        "uri": "https://linked.data.gov.au/def/nrm/9f909ee2-8ee7-5277-96d1-04b445f98ede"
      },
      {
        "id": 2,
        "symbol": "0",
        "label": "No wave erosion",
        "description": "Refers to No wave water erosion observed.",
        "uri": "https://linked.data.gov.au/def/nrm/04acf0e0-8c61-5737-8c7f-a94e1e710c12"
      },
      {
        "id": 3,
        "symbol": "1",
        "label": "Present",
        "description": "Refers to the presence of wave water erosion.",
        "uri": "https://linked.data.gov.au/def/nrm/31f219f1-4c57-5126-b1e5-0861822e8386"
      }
    ],
    "lut-soils-wind-erosion-degree": [
      {
        "id": 1,
        "symbol": "X",
        "label": "Not apparent",
        "description": "Refers to No apparent wind erosion observed.",
        "uri": "https://linked.data.gov.au/def/nrm/29ccb926-f0c2-5b7e-b009-dcb7c56b2b4d"
      },
      {
        "id": 2,
        "symbol": "0",
        "label": "No wind erosion",
        "description": "Refers to No wind erosion observed.",
        "uri": "https://linked.data.gov.au/def/nrm/bedf946e-cd8f-5cc3-9f16-caae1c548094"
      },
      {
        "id": 3,
        "symbol": "1",
        "label": "Minor or present",
        "description": "Refers to the presence/absence or extent of wind erosion. Minor or present suggests some loss of surface.",
        "uri": "https://linked.data.gov.au/def/nrm/444a24a1-cd65-59ac-8f2d-d285d5191c44"
      },
      {
        "id": 4,
        "symbol": "2",
        "label": "Moderate",
        "description": "Refers to the presence/absence or extent of wind erosion. Moderate suggests most or all or surface removed leaving soft or loose material.",
        "uri": "https://linked.data.gov.au/def/nrm/bad82863-335a-56e7-9c8e-72537aa093c1"
      },
      {
        "id": 5,
        "symbol": "3",
        "label": "Severe",
        "description": "Refers to the presence/absence or extent of wind erosion. Very severe is observed when deeper layers are exposed, leaving hard material (e.g., subsoil, weathered country rock or pans).",
        "uri": "https://linked.data.gov.au/def/nrm/0fd7d506-51ae-5d1f-821c-56fa6506e28c"
      },
      {
        "id": 6,
        "symbol": "4",
        "label": "Very severe",
        "description": "Refers to the presence/absence or extent of wind erosion. Very severe is observed when deeper layers are exposed, leaving hard material (e.g., subsoil, weathered country rock or pans).",
        "uri": "https://linked.data.gov.au/def/nrm/0fd7d506-51ae-5d1f-821c-56fa6506e28c"
      }
    ],
    "lut-species-determiner": [
      {
        "id": 1,
        "symbol": "SO",
        "label": "Surveyor observation",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "FG",
        "label": "Field guides",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "SVT",
        "label": "Samples/vouchers taken",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "SVIHM",
        "label": "Samples/vouchers identified by herbarium/museum",
        "description": "",
        "uri": ""
      }
    ],
    "lut-species-pest": [
      {
        "id": 1,
        "symbol": "G",
        "label": "Goat",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "P",
        "label": "Pig",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "CM",
        "label": "Camel",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "D",
        "label": "Deer",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "H",
        "label": "Horse",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "WD",
        "label": "Wild Dog",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "F",
        "label": "Fox",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "CA",
        "label": "Cat",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "R",
        "label": "Rabbit",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "UK",
        "label": "Unknown",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "NA",
        "label": "N/A",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-state": [
      {
        "id": 1,
        "symbol": "CT",
        "label": "Australian Capital Territory",
        "description": "State/Jurisdiction where the study is/was conducted.",
        "uri": "https://linked.data.gov.au/def/nrm/4253c9af-b390-5b69-aaf3-ce5cda0c36bf"
      },
      {
        "id": 2,
        "symbol": "NS",
        "label": "New South Wales",
        "description": "State/Jurisdiction where the study is/was conducted.",
        "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
      },
      {
        "id": 3,
        "symbol": "NT",
        "label": "Northern Territory",
        "description": "State/Jurisdiction where the study is/was conducted.",
        "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
      },
      {
        "id": 4,
        "symbol": "QD",
        "label": "Queensland",
        "description": "State/Jurisdiction where the study is/was conducted.",
        "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
      },
      {
        "id": 5,
        "symbol": "SA",
        "label": "South Australia",
        "description": "State/Jurisdiction where the study is/was conducted.",
        "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
      },
      {
        "id": 6,
        "symbol": "TC",
        "label": "Tasmania",
        "description": "State/Jurisdiction where the study is/was conducted.",
        "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
      },
      {
        "id": 7,
        "symbol": "VC",
        "label": "Victoria",
        "description": "State/Jurisdiction where the study is/was conducted.",
        "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
      },
      {
        "id": 8,
        "symbol": "WA",
        "label": "Western Australia",
        "description": "State/Jurisdiction where the study is/was conducted.",
        "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
      }
    ],
    "lut-status-of-helicopter-door": [
      {
        "id": 1,
        "symbol": "On",
        "label": "On",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Removed",
        "label": "Removed",
        "description": "",
        "uri": ""
      }
    ],
    "lut-subplot-corner": [
      {
        "id": 1,
        "symbol": "NW",
        "label": "North West",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "NE",
        "label": "North East",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "SE",
        "label": "South East",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "SW",
        "label": "South West",
        "description": "",
        "uri": ""
      }
    ],
    "lut-substrate": [
      {
        "id": 1,
        "symbol": "BR",
        "label": "Bare",
        "description": "Bare soil.",
        "uri": "https://linked.data.gov.au/def/nrm/08f21bd5-b4f1-5cc1-9c16-50e7b1676f5b"
      },
      {
        "id": 2,
        "symbol": "CR",
        "label": "Crypto",
        "description": "Cryptogram - any moss, lichen, algae, etc. present on the soil surface with a different colour to the base soil.",
        "uri": "https://linked.data.gov.au/def/nrm/fd31a502-ab70-51c3-a10e-6901a35c87ce"
      },
      {
        "id": 3,
        "symbol": "LT",
        "label": "Litter",
        "description": "Dead vegetation not attached or identifiable; if litter is attached and identifiable as a species, it should be recorded as this species but flagged as being dead.",
        "uri": "https://linked.data.gov.au/def/nrm/287cff17-cee5-5b26-ad61-0195d6127b82"
      },
      {
        "id": 4,
        "symbol": "CWD",
        "label": "CWD",
        "description": "Coarse Woody Debris - detached wood with >10 cm diameter at the intercept point.",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "GR",
        "label": "Gravel",
        "description": "Particles 2 mm - 2 cm.",
        "uri": "https://linked.data.gov.au/def/nrm/65069cb1-ae75-59be-9303-deb816fa8566"
      },
      {
        "id": 6,
        "symbol": "RK",
        "label": "Rock",
        "description": "Particles <20 cm.",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "OC",
        "label": "Outcrop",
        "description": "Exposed consolidated rock (bedrock).",
        "uri": "https://linked.data.gov.au/def/nrm/64af781a-b209-5206-835d-d6b4cbb13d46"
      },
      {
        "id": 8,
        "symbol": "LOR",
        "label": "Lichen on rock",
        "description": "Lichen growing on rock.",
        "uri": "https://linked.data.gov.au/def/nrm/ecc34667-f36a-5de2-814b-e7304bd970ca"
      },
      {
        "id": 9,
        "symbol": "LOO",
        "label": "Lichen on outcrop",
        "description": "Lichen growing on bedrock.",
        "uri": "https://linked.data.gov.au/def/nrm/fb36eb90-059a-553b-92d7-01d5a04345e7"
      },
      {
        "id": 10,
        "symbol": "WR",
        "label": "Water",
        "description": "Body of water (sheet water, Gilgai).",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "BA",
        "label": "Black ash",
        "description": "Powdery black residue remaining after the incomplete combustion of wood during a bushfire.",
        "uri": "https://linked.data.gov.au/def/nrm/0a8dd17b-eadd-5f60-bb97-2a61fc61ec76"
      },
      {
        "id": 12,
        "symbol": "WA",
        "label": "White ash",
        "description": "Powdery white residue remaining after the complete combustion of wood during a bushfire.",
        "uri": "https://linked.data.gov.au/def/nrm/67f0e621-e61d-55a6-9963-e0becc048aaa"
      },
      {
        "id": 13,
        "symbol": "UK",
        "label": "Unknown",
        "description": "Unknown (unable to be determined, e.g. under dense shrub).",
        "uri": "https://linked.data.gov.au/def/nrm/f6b0f6d8-16d8-5dd7-b1b7-66b0c020b96f"
      },
      {
        "id": 14,
        "symbol": "O",
        "label": "Other",
        "description": "Substrate not listed.",
        "uri": "https://linked.data.gov.au/def/nrm/a96d7507-e823-5f3a-a26b-62313538e0bb"
      },
      {
        "id": 15,
        "symbol": "NC",
        "label": "Not Collected",
        "description": "Point-intercept data not collected (e.g. cannot survey too dense area within plot).",
        "uri": "https://linked.data.gov.au/def/nrm/94ee6b46-e2f1-5101-8666-3cbcd8697f0f"
      }
    ],
    "lut-survey-frequency": [
      {
        "id": 1,
        "symbol": "Daily",
        "label": "Daily",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Weekly",
        "label": "Weekly",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Monthly",
        "label": "Monthly",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Seasonally",
        "label": "Seasonal",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "Bi-annual",
        "label": "Bi-annual",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "Annual",
        "label": "Annual",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "Biennial",
        "label": "Biennial",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "Ad-hoc",
        "label": "Ad-hoc",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "One-off",
        "label": "One-off",
        "description": "",
        "uri": ""
      }
    ],
    "lut-surveyor-type": [
      {
        "id": 1,
        "symbol": "NRMP",
        "label": "NRM personnel",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Landholders",
        "label": "Landholders",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "CS",
        "label": "Citizen scientist",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "CSC",
        "label": "Consultant/sub-contractor",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "HP",
        "label": "Herbarium personnel",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "MP",
        "label": "Museum personnel",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "SGP",
        "label": "State government personnel",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "Other",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-target-surface-take-off-point": [
      {
        "id": 1,
        "symbol": "0",
        "label": "0m",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "10",
        "label": "10m",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "15",
        "label": "15m",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "30",
        "label": "30m",
        "description": "",
        "uri": ""
      }
    ],
    "lut-targeted-trap-type": [
      {
        "id": 1,
        "symbol": "BT",
        "label": "Box traps",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "CT",
        "label": "Cage traps",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "PT",
        "label": "Pitfall traps",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "MN",
        "label": "Mist nets",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "HT",
        "label": "Harp traps",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "FT",
        "label": "Funnel traps",
        "description": "",
        "uri": ""
      }
    ],
    "lut-taxa-type": [
      {
        "id": 1,
        "symbol": "VP",
        "label": "Vascular plant",
        "description": "Refers to the target taxa studied in a fauna survey. Non-vascular plants are plants that do not possess a true vascular tissue (such as xylem-water conducting, phloem-sugar transport). Instead, they may possess simpler tissues that have specialized functions for the internal transport of food and water. They are members of bryophytes for example.",
        "uri": "https://linked.data.gov.au/def/nrm/7def5e09-00a6-5ace-b48b-6f93b4d4bf8a"
      },
      {
        "id": 2,
        "symbol": "NVP",
        "label": "Non-vascular plant",
        "description": "Refers to the target taxa studied in a fauna survey. Non-vascular plants are plants that do not possess a true vascular tissue (such as xylem-water conducting, phloem-sugar transport). Instead, they may possess simpler tissues that have specialized functions for the internal transport of food and water. They are members of bryophytes for example.",
        "uri": "https://linked.data.gov.au/def/nrm/599d9085-baad-5f32-86dc-3d112ecdae9c"
      },
      {
        "id": 3,
        "symbol": "AM",
        "label": "Amphibian",
        "description": "Refers to the target taxa studied in a fauna survey. Amphibians are vertebrates belonging to the class amphibia such as frogs, toads, newts and salamanders that live in a semi-aquatic environment.",
        "uri": "https://linked.data.gov.au/def/nrm/538e423a-3f73-5c52-8a30-13ef2ba5e77f"
      },
      {
        "id": 4,
        "symbol": "BI",
        "label": "Bird",
        "description": "Refers to the target taxa studied in a fauna survey. Warm-blooded vertebrates possessing feather and belonging to the class Aves.",
        "uri": "https://linked.data.gov.au/def/nrm/f9874c91-f61d-5b74-90d1-aa71d3805b45"
      },
      {
        "id": 5,
        "symbol": "INV",
        "label": "Invertebrate",
        "description": "Refers to the target taxa studied in a fauna survey. Animals that have no spinal column (e.g., insects, molluscs, spiders).",
        "uri": "https://linked.data.gov.au/def/nrm/f03ce044-9c2c-52ec-adf9-5aa4960d58a8"
      },
      {
        "id": 6,
        "symbol": "MA",
        "label": "Mammal",
        "description": "Warm-blooded vertebrate animals belonging to the class Mammalia, including all that possess hair and suckle their young.",
        "uri": "https://linked.data.gov.au/def/nrm/e537a3de-6b76-5638-8d95-0a47cc00359a"
      },
      {
        "id": 7,
        "symbol": "RE",
        "label": "Reptile",
        "description": "Refers to the target taxa studied in a fauna survey. Cold-blooded, air-breathing Vertebrates belonging to the class Reptilia, usually covered with external scales or bony plates.",
        "uri": "https://linked.data.gov.au/def/nrm/51bf988a-8672-56f7-96cb-9c363eb0cfd4"
      }
    ],
    "lut-tec": [
      {
        "id": 1,
        "symbol": "ASBAF",
        "label": "Alpine Sphagnum Bogs and Associated Fens",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 1,
            "symbol": "CT",
            "label": "Australian Capital Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/4253c9af-b390-5b69-aaf3-ce5cda0c36bf"
          },
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "ARMC1CLNR",
        "label": "Aquatic Root Mat Community 1 in Caves of the Leeuwin Naturaliste Ridge",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "ARMC2CLNR",
        "label": "Aquatic Root Mat Community 2 in Caves of the Leeuwin Naturaliste Ridge",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "ARMC3CLNR",
        "label": "Aquatic Root Mat Community 3 in Caves of the Leeuwin Naturaliste Ridge",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "ARMC4CLNR",
        "label": "Aquatic Root Mat Community 4 in Caves of the Leeuwin Naturaliste Ridge",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "ARMCCSCP",
        "label": "Aquatic Root Mat Community in Caves of the Swan Coastal Plain",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "ASGF",
        "label": "Araluen Scarp Grassy Forest",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "APSSC",
        "label": "Arnhem Plateau Sandstone Shrubland Complex",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          }
        ],
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "APIATSSCP",
        "label": "Assemblages of plants and invertebrate animals of tumulus (organic mound) springs of the Swan Coastal Plain",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "ASAWOSEWCVEC",
        "label": "Assemblages of species associated with open-coast salt-wedge estuaries of western and central Victoria ecological community",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "BWSCPEC",
        "label": "Banksia Woodlands of the Swan Coastal Plain ecological community",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "BHGSMCTR",
        "label": "Ben Halls Gap Sphagnum Moss Cool Temperate Rainforest",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "BGHFSBB",
        "label": "Blue Gum High Forest of the Sydney Basin Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "B",
        "label": "Brigalow (Acacia harpophylla dominant and co-dominant)",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "BLTWHRCNQ",
        "label": "Broad leaf tea-tree (Melaleuca viridiflora) woodlands in high rainfall coastal north Queensland",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "BVFSECB",
        "label": "Brogo Vine Forest of the South East Corner Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "BWRMDB",
        "label": "Buloke Woodlands of the Riverina and Murray-Darling Depression Bioregions",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 18,
        "symbol": "CSGABWSBB",
        "label": "Castlereagh Scribbly Gum and Agnes Banks Woodlands of the Sydney Basin Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 19,
        "symbol": "CHVEFW",
        "label": "Central Hunter Valley eucalypt forest and woodland",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 20,
        "symbol": "CPSCP",
        "label": "Clay Pans of the Swan Coastal Plain",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 21,
        "symbol": "CSOFNSWSEQEC",
        "label": "Coastal Swamp Oak (Casuarina glauca) Forest of New South Wales and South East Queensland ecological community",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 22,
        "symbol": "CSSFNSWSEQ",
        "label": "Coastal Swamp Sclerophyll Forest of New South Wales and South East Queensland",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 23,
        "symbol": "CUSSBB",
        "label": "Coastal Upland Swamps in the Sydney Basin Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 24,
        "symbol": "CRIFSBB",
        "label": "Cooks River/Castlereagh Ironbark Forest of the Sydney Basin Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 25,
        "symbol": "CBBWDRPBBSB",
        "label": "Coolibah - Black Box Woodlands of the Darling Riverine Plains and the Brigalow Belt South Bioregions",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 26,
        "symbol": "CCKAWHSSCP",
        "label": "Corymbia calophylla - Kingia australis woodlands on heavy soils of the Swan Coastal Plain",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 27,
        "symbol": "CCXPWSSCP",
        "label": "Corymbia calophylla - Xanthorrhoea preissii woodlands and shrublands of the Swan Coastal Plain",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 28,
        "symbol": "CPSWSTF",
        "label": "Cumberland Plain Shale Woodlands and Shale-Gravel Transition Forest",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 29,
        "symbol": "DSGWCEYBB",
        "label": "Drooping sheoak grassy woodland on calcrete of the Eyre Yorke Block Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ],
        "uri": ""
      },
      {
        "id": 30,
        "symbol": "DWGMFNNSWSQ",
        "label": "Dunn's white gum (Eucalyptus dunnii) moist forest in north-east New South Wales and south-east Queensland",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 31,
        "symbol": "ESRMHT",
        "label": "Eastern Stirling Range Montane Heath and Thicket",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 32,
        "symbol": "ESBSSR",
        "label": "Eastern Suburbs Banksia Scrub of the Sydney Region",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 33,
        "symbol": "EBSFSBB",
        "label": "Elderslie Banksia Scrub Forest in the Sydney Basin Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 34,
        "symbol": "EPSA",
        "label": "Empodisma peatlands of southwestern Australia\r\n",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 35,
        "symbol": "EWWAW",
        "label": "Eucalypt Woodlands of the Western Australian Wheatbelt",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 36,
        "symbol": "EOCOF",
        "label": "Eucalyptus ovata - Callitris oblonga Forest",
        "description": "",
        "lut_ebpc_status": {
          "id": 3,
          "symbol": "V",
          "label": "Vulnerable",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          }
        ],
        "uri": ""
      },
      {
        "id": 37,
        "symbol": "EPBGW",
        "label": "Eyre Peninsula Blue Gum (Eucalyptus petiolaris) Woodland",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ],
        "uri": ""
      },
      {
        "id": 38,
        "symbol": "GKMFSEA",
        "label": "Giant Kelp Marine Forests of South East Australia",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 39,
        "symbol": "GRGGWANG",
        "label": "Gippsland Red Gum (Eucalyptus tereticornis subsp. mediana) Grassy Woodland and Associated Native Grassland",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 40,
        "symbol": "GEWVVP",
        "label": "Grassy Eucalypt Woodland of the Victorian Volcanic Plain",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 41,
        "symbol": "GBGWDNGSA",
        "label": "Grey Box (Eucalyptus microcarpa) Grassy Woodlands and Derived Native Grasslands of South-eastern Australia",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 42,
        "symbol": "GBGWFSEA",
        "label": "Grey box-grey gum wet forest of subtropical eastern Australia",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 43,
        "symbol": "HSLRSCPB",
        "label": "Honeymyrtle shrubland on limestone ridges of the Swan Coastal Plain Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 44,
        "symbol": "HVWMW",
        "label": "Hunter Valley Weeping Myall (Acacia pendula) Woodland",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 45,
        "symbol": "ISCLFWEC",
        "label": "Illawarra and south coast lowland forest and woodland ecological community",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 46,
        "symbol": "ISRSBB",
        "label": "Illawarra-Shoalhaven Subtropical Rainforest of the Sydney Basin Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 47,
        "symbol": "INTGSA",
        "label": "Iron-grass Natural Temperate Grassland of South Australia",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ],
        "uri": ""
      },
      {
        "id": 48,
        "symbol": "KINMW",
        "label": "Kangaroo Island Narrow-leaved Mallee (Eucalyptus cneorifolia) Woodland",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ],
        "uri": ""
      },
      {
        "id": 49,
        "symbol": "KSAAFNCPB",
        "label": "Karst springs and associated alkaline fens of the Naracoorte Coastal Plain Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ],
        "uri": ""
      },
      {
        "id": 50,
        "symbol": "KSSWSBB",
        "label": "Kurri sand swamp woodland of the Sydney Basin bioregion\r\n",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 51,
        "symbol": "LRCVTEA",
        "label": "Littoral Rainforest and Coastal Vine Thickets of Eastern Australia",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 52,
        "symbol": "LGWSECB",
        "label": "Lowland Grassy Woodland in the South East Corner Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 53,
        "symbol": "LNGT",
        "label": "Lowland Native Grasslands of Tasmania",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          }
        ],
        "uri": ""
      },
      {
        "id": 54,
        "symbol": "LRSA",
        "label": "Lowland Rainforest of Subtropical Australia",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 55,
        "symbol": "LTRWT",
        "label": "Lowland tropical rainforest of the Wet Tropics",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 56,
        "symbol": "MF",
        "label": "Mabi Forest (Complex Notophyll Vine Forest 5b)",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 57,
        "symbol": "MBCMDDB",
        "label": "Mallee Bird Community of the Murray Darling Depression Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 58,
        "symbol": "MVTCSDDP",
        "label": "Monsoon vine thickets on the coastal sand dunes of Dampier Peninsula",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 59,
        "symbol": "MKLSSC",
        "label": "Mount Kaputar land snail and slug community",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 60,
        "symbol": "NDGVCP",
        "label": "Natural Damp Grassland of the Victorian Coastal Plains",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 61,
        "symbol": "NGMVP",
        "label": "Natural Grasslands of the Murray Valley Plains",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 62,
        "symbol": "NGQCHNFB",
        "label": "Natural Grasslands of the Queensland Central Highlands and northern Fitzroy Basin",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 63,
        "symbol": "NTGSEH",
        "label": "Natural Temperate Grassland of the South Eastern Highlands",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 1,
            "symbol": "CT",
            "label": "Australian Capital Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/4253c9af-b390-5b69-aaf3-ce5cda0c36bf"
          },
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 64,
        "symbol": "NTGVVP",
        "label": "Natural Temperate Grassland of the Victorian Volcanic Plain",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 65,
        "symbol": "NGBFAPNNSWSQ",
        "label": "Natural grasslands on basalt and fine-textured alluvial plains of northern New South Wales and southern Queensland",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 66,
        "symbol": "NEPGW",
        "label": "New England Peppermint (Eucalyptus nova-anglica) Grassy Woodlands",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 67,
        "symbol": "PBGWSA",
        "label": "Peppermint Box (Eucalyptus odorata) Grassy Woodland of South Australia",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ],
        "uri": ""
      },
      {
        "id": 68,
        "symbol": "PWWRWESLSPALF",
        "label": "Perched Wetlands of the Wheatbelt region with extensive stands of living sheoak and paperbark across the lake floor (Toolibin Lake)",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 69,
        "symbol": "PMBWMDDRNCPB",
        "label": "Plains mallee box woodlands of the Murray Darling Depression, Riverina and Naracoorte Coastal Plain Bioregions",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 70,
        "symbol": "PBGWAP",
        "label": "Poplar Box Grassy Woodland on Alluvial Plains",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 71,
        "symbol": "PASMME",
        "label": "Posidonia australis seagrass meadows of the Manning-Hawkesbury ecoregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 72,
        "symbol": "PDKSSCFPWA",
        "label": "Proteaceae Dominated Kwongkan Shrublands of the Southeast Coastal Floristic Province of Western Australia",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 73,
        "symbol": "REFCFSNSWEV",
        "label": "River-flat eucalypt forest on coastal floodplains of southern New South Wales and eastern Victoria",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 74,
        "symbol": "RRSBB",
        "label": "Robertson Rainforest in the Sydney Basin Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 75,
        "symbol": "SRIA",
        "label": "Scott River Ironstone Association",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 76,
        "symbol": "SHWTLP",
        "label": "Seasonal Herbaceous Wetlands (Freshwater) of the Temperate Lowland Plains",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 77,
        "symbol": "SHDSSSCP",
        "label": "Sedgelands in Holocene dune swales of the southern Swan Coastal Plain",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 78,
        "symbol": "SVTBBNB",
        "label": "Semi-evergreen vine thickets of the Brigalow Belt (North and South) and Nandewar Bioregions",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 79,
        "symbol": "SSTFSBB",
        "label": "Shale Sandstone Transition Forest of the Sydney Basin Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 80,
        "symbol": "SWESCP",
        "label": "Shrublands and Woodlands of the eastern Swan Coastal Plain",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 81,
        "symbol": "SWMLSCP",
        "label": "Shrublands and Woodlands on Muchea Limestone of the Swan Coastal Plain",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 82,
        "symbol": "SWPGISCP",
        "label": "Shrublands and Woodlands on Perth to Gingin ironstone (Perth to Gingin ironstone association) of the Swan Coastal Plain",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 83,
        "symbol": "SSSCPI",
        "label": "Shrublands on southern Swan Coastal Plain ironstones",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 84,
        "symbol": "SLPSSECAAB",
        "label": "Silurian Limestone Pomaderris Shrubland of the South East Corner and Australian Alps Bioregions",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      },
      {
        "id": 85,
        "symbol": "SHSFWSBB",
        "label": "Southern Highlands Shale Forest and Woodland in the Sydney Basin Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 86,
        "symbol": "STCS",
        "label": "Subtropical and Temperate Coastal Saltmarsh",
        "description": "",
        "lut_ebpc_status": {
          "id": 3,
          "symbol": "V",
          "label": "Vulnerable",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          },
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 87,
        "symbol": "SEFFWNSWNCSEQB",
        "label": "Subtropical eucalypt floodplain forest and woodland of the New South Wales North Coast and South East Queensland bioregions",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 88,
        "symbol": "STFSQ",
        "label": "Swamp Tea-tree (Melaleuca irbyana) Forest of South-east Queensland",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 89,
        "symbol": "SFP",
        "label": "Swamps of the Fleurieu Peninsula",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ],
        "uri": ""
      },
      {
        "id": 90,
        "symbol": "TFWDBGOBG",
        "label": "Tasmanian Forests and Woodlands dominated by black gum or Brookers gum (Eucalyptus ovata / E. brookeriana)",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          }
        ],
        "uri": ""
      },
      {
        "id": 91,
        "symbol": "TWGWF",
        "label": "Tasmanian white gum (Eucalyptus viminalis) wet forest",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 6,
            "symbol": "TC",
            "label": "Tasmania",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5dce0cbd-3bf0-5c0d-b056-19ade7f1093b"
          }
        ],
        "uri": ""
      },
      {
        "id": 92,
        "symbol": "THPSS",
        "label": "Temperate Highland Peat Swamps on Sandstone",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 93,
        "symbol": "TCNSDNDGFGAB",
        "label": "The community of native species dependent on natural discharge of groundwater from the Great Artesian Basin",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 3,
            "symbol": "NT",
            "label": "Northern Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/7652e30a-5799-5bb5-9063-b645ea7d9a9c"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          }
        ],
        "uri": ""
      },
      {
        "id": 94,
        "symbol": "TCCFLSCP",
        "label": "Thrombolite (microbial) community of coastal freshwater lakes of the Swan Coastal Plain (Lake Richmond)",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 95,
        "symbol": "TCCBL",
        "label": "Thrombolite (microbialite) Community of a Coastal Brackish Lake (Lake Clifton)",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 96,
        "symbol": "TWFSCPEC",
        "label": "Tuart (Eucalyptus gomphocephala) Woodlands and Forests of the Swan Coastal Plain ecological community",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 8,
            "symbol": "WA",
            "label": "Western Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/ea1f3036-2068-58a9-9ec9-46042fa52179"
          }
        ],
        "uri": ""
      },
      {
        "id": 97,
        "symbol": "TFSBB",
        "label": "Turpentine-Ironbark Forest of the Sydney Basin Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 98,
        "symbol": "UBEFSBB",
        "label": "Upland Basalt Eucalypt Forests of the Sydney Basin Bioregion",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 99,
        "symbol": "UWNETMP",
        "label": "Upland Wetlands of the New England Tablelands (New England Tableland Bioregion) and the Monaro Plateau (South Eastern Highlands Bioregion)",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 100,
        "symbol": "WSWHV",
        "label": "Warkworth Sands Woodland of the Hunter Valley",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 101,
        "symbol": "WMW",
        "label": "Weeping Myall Woodlands",
        "description": "",
        "lut_ebpc_status": {
          "id": 1,
          "symbol": "E",
          "label": "Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          }
        ],
        "uri": ""
      },
      {
        "id": 102,
        "symbol": "WSDRMWS",
        "label": "Western Sydney Dry Rainforest and Moist Woodland on Shale",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          }
        ],
        "uri": ""
      },
      {
        "id": 103,
        "symbol": "WBBRGGWDNG",
        "label": "White Box-Yellow Box-Blakely's Red Gum Grassy Woodland and Derived Native Grassland",
        "description": "",
        "lut_ebpc_status": {
          "id": 2,
          "symbol": "CE",
          "label": "Critically Endangered",
          "description": "",
          "uri": ""
        },
        "lut_states": [
          {
            "id": 1,
            "symbol": "CT",
            "label": "Australian Capital Territory",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/4253c9af-b390-5b69-aaf3-ce5cda0c36bf"
          },
          {
            "id": 2,
            "symbol": "NS",
            "label": "New South Wales",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/14b8c3fe-3a60-5f3a-bb7a-b9cf5df49b4e"
          },
          {
            "id": 4,
            "symbol": "QD",
            "label": "Queensland",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/d9d48a48-f74e-55b7-a360-82975c6be412"
          },
          {
            "id": 5,
            "symbol": "SA",
            "label": "South Australia",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5648c02d-6120-5cb1-858a-d92e970c1da0"
          },
          {
            "id": 7,
            "symbol": "VC",
            "label": "Victoria",
            "description": "State/Jurisdiction where the study is/was conducted.",
            "uri": "https://linked.data.gov.au/def/nrm/5e6382e6-d26c-5889-ba7e-f051d82d6173"
          }
        ],
        "uri": ""
      }
    ],
    "lut-temp-texta-colour": [
      {
        "id": 1,
        "symbol": "Red",
        "label": "Red",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Blue",
        "label": "Blue",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Black",
        "label": "Black",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Green",
        "label": "Green",
        "description": "",
        "uri": ""
      }
    ],
    "lut-temperature-unit": [
      {
        "id": 1,
        "symbol": "C",
        "label": "Celcius",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "F",
        "label": "Fahrenheit",
        "description": "A unit of temperature of the temperature scale designed so that the freezing point of water is 32 degrees and the boiling point is 212 degrees, placing the boiling and melting points of water 180 degrees apart. One degree Fahrenheit is 5/9ths of a kelvin (or of a degree Celsius), and minus 40 degrees Fahrenheit is equal to minus 40 degrees Celsius.",
        "uri": "https://linked.data.gov.au/def/nrm/c894c705-8ab4-530d-8e5b-f1ba62d606df"
      }
    ],
    "lut-temporal-resolution": [
      {
        "id": 1,
        "symbol": "LT1S",
        "label": "< 1 second",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "1STO1M",
        "label": "1 second - < 1 minute",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "1MTO1H",
        "label": "1 minute - < 1 hour",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "HD",
        "label": "Hourly - < Daily",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "DW",
        "label": "Daily - < Weekly",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "WM",
        "label": "Weekly - < Monthly",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "MA",
        "label": "Monthly - < Annual",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "D",
        "label": "Diurnal",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "SE",
        "label": "Seasonal",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "SU",
        "label": "Subannual",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "A",
        "label": "Annual",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "DE",
        "label": "Decadal",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "HC",
        "label": "Hourly Climatology",
        "description": "",
        "uri": ""
      },
      {
        "id": 14,
        "symbol": "DC",
        "label": "Daily Climatology",
        "description": "",
        "uri": ""
      },
      {
        "id": 15,
        "symbol": "PC",
        "label": "Pentad Climatology",
        "description": "",
        "uri": ""
      },
      {
        "id": 16,
        "symbol": "WC",
        "label": "Weekly Climatology",
        "description": "",
        "uri": ""
      },
      {
        "id": 17,
        "symbol": "MC",
        "label": "Monthly Climatology",
        "description": "",
        "uri": ""
      },
      {
        "id": 18,
        "symbol": "AC",
        "label": "Annual Climatology",
        "description": "",
        "uri": ""
      },
      {
        "id": 19,
        "symbol": "CN",
        "label": "Climate Normal (30-year climatology)",
        "description": "",
        "uri": ""
      }
    ],
    "lut-tier-1-observation-method": [
      {
        "id": 1,
        "symbol": "SE",
        "label": "Seen",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "HE",
        "label": "Heard",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "SI",
        "label": "Sign",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
        "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
      },
      {
        "id": 4,
        "symbol": "CA",
        "label": "Captured",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
        "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
      }
    ],
    "lut-tier-2-observation-method": [
      {
        "id": 1,
        "symbol": "NM",
        "label": "No method stated",
        "description": "Refers to No recognised method of observation stated for a target fauna.",
        "uri": "https://linked.data.gov.au/def/nrm/a8db263e-7a39-5b85-a5d2-8e1af7ce1e39",
        "tier_1": [
          {
            "id": 1,
            "symbol": "SE",
            "label": "Seen",
            "description": "",
            "uri": ""
          },
          {
            "id": 2,
            "symbol": "HE",
            "label": "Heard",
            "description": "",
            "uri": ""
          }
        ]
      },
      {
        "id": 2,
        "symbol": "SL",
        "label": "Spotlighting",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made on a fauna using spotlighting technique.",
        "uri": "https://linked.data.gov.au/def/nrm/db0cb2f2-e9f8-5679-a566-afce45b28da0",
        "tier_1": [
          {
            "id": 1,
            "symbol": "SE",
            "label": "Seen",
            "description": "",
            "uri": ""
          }
        ]
      },
      {
        "id": 3,
        "symbol": "AH",
        "label": "Air - helicopter",
        "description": "",
        "uri": "",
        "tier_1": [
          {
            "id": 1,
            "symbol": "SE",
            "label": "Seen",
            "description": "",
            "uri": ""
          }
        ]
      },
      {
        "id": 4,
        "symbol": "AF",
        "label": "Air - fixed wing",
        "description": "",
        "uri": "",
        "tier_1": [
          {
            "id": 1,
            "symbol": "SE",
            "label": "Seen",
            "description": "",
            "uri": ""
          }
        ]
      },
      {
        "id": 5,
        "symbol": "RC",
        "label": "Remote camera",
        "description": "Refers to the targeted fauna observation method, i.e., any observations on a fauna made using a remote camera device.",
        "uri": "https://linked.data.gov.au/def/nrm/d9ded7e1-e012-53c0-a612-3f5585281cef",
        "tier_1": [
          {
            "id": 1,
            "symbol": "SE",
            "label": "Seen",
            "description": "",
            "uri": ""
          }
        ]
      },
      {
        "id": 6,
        "symbol": "OT",
        "label": "Other (specify)",
        "description": "Other types of liquid preservative used to store invertebrate samples.",
        "uri": "https://linked.data.gov.au/def/nrm/565511a8-c50b-53fb-8f59-995e52a01ed2",
        "tier_1": [
          {
            "id": 1,
            "symbol": "SE",
            "label": "Seen",
            "description": "",
            "uri": ""
          },
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          },
          {
            "id": 4,
            "symbol": "CA",
            "label": "Captured",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
            "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
          }
        ]
      },
      {
        "id": 7,
        "symbol": "AP",
        "label": "After call playback",
        "description": "",
        "uri": "",
        "tier_1": [
          {
            "id": 2,
            "symbol": "HE",
            "label": "Heard",
            "description": "",
            "uri": ""
          }
        ]
      },
      {
        "id": 8,
        "symbol": "AR",
        "label": "Acoustic recording",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made from acoustic signals.",
        "uri": "https://linked.data.gov.au/def/nrm/5f601104-ea57-524d-8a7c-dde7e616f039",
        "tier_1": [
          {
            "id": 2,
            "symbol": "HE",
            "label": "Heard",
            "description": "",
            "uri": ""
          }
        ]
      },
      {
        "id": 9,
        "symbol": "UR",
        "label": "Ultrasonic recording",
        "description": "Refers to the targeted fauna observation method, i.e., any signs of a fauna with the assistance of ultrasonic recording device/s.",
        "uri": "https://linked.data.gov.au/def/nrm/e405c9c0-2c1f-5dee-8d63-1edfc6ee5b36",
        "tier_1": [
          {
            "id": 2,
            "symbol": "HE",
            "label": "Heard",
            "description": "",
            "uri": ""
          }
        ]
      },
      {
        "id": 10,
        "symbol": "CF",
        "label": "Carcass (full)",
        "description": "",
        "uri": "",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 11,
        "symbol": "CP",
        "label": "Carcass (partial)",
        "description": "",
        "uri": "",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 12,
        "symbol": "BT",
        "label": "Bone/teeth",
        "description": "",
        "uri": "",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 13,
        "symbol": "SN",
        "label": "Skin",
        "description": "Refers to the targeted fauna observation method, i.e., any signs of a fauna detected from observations of skin.",
        "uri": "https://linked.data.gov.au/def/nrm/b28a6ddd-c9c6-5839-9945-028f92707770",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 14,
        "symbol": "SH",
        "label": "Shell",
        "description": "Refers to the targeted fauna observation method, i.e., any signs of a fauna detected from observations on shells.",
        "uri": "https://linked.data.gov.au/def/nrm/ad89e464-2ef1-5342-bdbc-20a71eccd260",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 15,
        "symbol": "FE",
        "label": "Feather",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made from the feather of a bird.",
        "uri": "https://linked.data.gov.au/def/nrm/18d4c44d-4c5b-57b2-8eb1-37f3ba0826a0",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 16,
        "symbol": "HA",
        "label": "Hair/fur",
        "description": "Refers to the targeted fauna observation method, i.e., any observations on a fauna made using 'hair' or 'fur'.",
        "uri": "https://linked.data.gov.au/def/nrm/fe8cac2d-e965-514a-8138-dadf6f6751fa",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 17,
        "symbol": "TR",
        "label": "Tracks",
        "description": "Refers to the fauna observation method, i.e., any signs of a fauna detected from observations of tracks.",
        "uri": "https://linked.data.gov.au/def/nrm/8b50e443-c84a-5c85-868c-2fb689524e80",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 18,
        "symbol": "DI",
        "label": "Diggings",
        "description": "The type of evidence of a pest animal presence in the form of 'diggings'.",
        "uri": "https://linked.data.gov.au/def/nrm/82e8ae05-1bbe-55b0-addd-78d3e1be62de",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 19,
        "symbol": "SG",
        "label": "Scratchings (ground)",
        "description": "Refers to the targeted fauna observation method, i.e., any signs of a fauna detected from observations of scratchings on a ground.",
        "uri": "https://linked.data.gov.au/def/nrm/c4291c6d-3873-5f01-9a03-40f0e8209f5a",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 20,
        "symbol": "SA",
        "label": "Scratchings (arboreal)",
        "description": "Refers to the targeted fauna observation method, i.e., any signs of a fauna detected from observations of scratchings on a tree.",
        "uri": "https://linked.data.gov.au/def/nrm/fb1b5882-1e51-503f-874d-b1309df36508",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 21,
        "symbol": "WA",
        "label": "Wallow",
        "description": "",
        "uri": "",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 22,
        "symbol": "TP",
        "label": "Travel pad",
        "description": "",
        "uri": "",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 23,
        "symbol": "OD",
        "label": "Odour",
        "description": "",
        "uri": "",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 24,
        "symbol": "SC",
        "label": "Scats",
        "description": "",
        "uri": "",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 25,
        "symbol": "WS",
        "label": "Within scat",
        "description": "Refers to the targeted fauna observation method, i.e., any signs of a fauna detected from observations within scats of an animal.",
        "uri": "https://linked.data.gov.au/def/nrm/057f361a-8abf-58ed-83fd-c57730076622",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 26,
        "symbol": "PE",
        "label": "Pellet",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made using a pellet (refers to bird observations only).",
        "uri": "https://linked.data.gov.au/def/nrm/b2086fc8-11ae-53e0-99b2-6e42bd210202",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 27,
        "symbol": "WP",
        "label": "Within pellet",
        "description": "Refers to the targeted fauna observation method, i.e., any signs of a fauna detected from observations within pellets of an animal.",
        "uri": "https://linked.data.gov.au/def/nrm/488ee615-5c57-5080-b991-04b4bfe05d7a",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 28,
        "symbol": "WG",
        "label": "Within gut content",
        "description": "Refers to the targeted fauna observation method, i.e., any signs of a fauna detected from observations made within the gut of an animal.",
        "uri": "https://linked.data.gov.au/def/nrm/a6005006-e7a4-5cd6-b285-c3cb9b6f6c51",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 29,
        "symbol": "BU",
        "label": "Burrow",
        "description": "Burrow is a small tunnel or a hole made by certain ground-dwelling mammals as a place of refuge.",
        "uri": "https://linked.data.gov.au/def/nrm/7230bbb8-9f03-5dd0-9ecf-fddfe4bb3c7c",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 30,
        "symbol": "DE",
        "label": "Den",
        "description": "",
        "uri": "",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 31,
        "symbol": "NE",
        "label": "Nest",
        "description": "Refers to the microhabitat where the targeted fauna was observed. A nest is a place of refuge to hold an animal's eggs or provide a place to live or raise offspring.",
        "uri": "https://linked.data.gov.au/def/nrm/b300ac98-8599-5de9-b99b-a2a17055cef8",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 32,
        "symbol": "EG",
        "label": "Eggs/eggshell",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made from eggs or egg shells of a fauna.",
        "uri": "https://linked.data.gov.au/def/nrm/9890b2a0-29c0-5122-b592-3ccd445abc70",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 33,
        "symbol": "FO",
        "label": "Subfossil/fossil",
        "description": "Any signs of a fauna detected from observations made on a fossil or sub-fossils within pellets.",
        "uri": "https://linked.data.gov.au/def/nrm/2000e7d5-9851-54e7-8ade-379a07c0cfab",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 34,
        "symbol": "EX",
        "label": "Exoskeleton",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made from the exoskeleton- the outer rigid covering of an invertebrate fauna that has structural features intact.",
        "uri": "https://linked.data.gov.au/def/nrm/eb3a1100-9028-5562-a12d-b4b3d6e6d92f",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 35,
        "symbol": "RT",
        "label": "Radio-tracking",
        "description": "",
        "uri": "",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 36,
        "symbol": "GT",
        "label": "GPS tracking",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made on a fauna using geospatial tracking devices.",
        "uri": "https://linked.data.gov.au/def/nrm/cb745f17-e984-5b2b-b8d4-e3d3f784d93f",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 37,
        "symbol": "ST",
        "label": "Satellite tracking",
        "description": "Refers to the targeted fauna observation method, i.e., any signs of a fauna with the assistance of satellite tracking.",
        "uri": "https://linked.data.gov.au/def/nrm/c21b24dd-4ea9-5c31-af53-8f6a80b40411",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 38,
        "symbol": "HB",
        "label": "Hair tube",
        "description": "Refers to the targeted fauna observation method, i.e., any observations on a fauna made using 'Hair Tubes', which are short sections of PVC pipe lined with pieces of double-sided sticky-tape.",
        "uri": "https://linked.data.gov.au/def/nrm/7ed366d3-9c85-5974-863d-127d1ff103ce",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 39,
        "symbol": "SP",
        "label": "Scent Pad",
        "description": "Refers to the targeted fauna observation method, i.e., any signs of a fauna detected using a scent pad.",
        "uri": "https://linked.data.gov.au/def/nrm/2ae64fa5-44b2-54a7-a90f-bcf37fb2378c",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 40,
        "symbol": "DN",
        "label": "DNA",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made on a fauna using their DNA information.",
        "uri": "https://linked.data.gov.au/def/nrm/6c2cfedc-c95a-5c6f-affb-77a3cc1db64b",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 41,
        "symbol": "ED",
        "label": "eDNA",
        "description": "Refers to the method of identification of Vertebrate fauna. Taxon is identified using DNA sequencing techniques from environmental samples (eDNA).",
        "uri": "https://linked.data.gov.au/def/nrm/4d5d3f71-d98f-5619-9f4d-45330bcb51f3",
        "tier_1": [
          {
            "id": 3,
            "symbol": "SI",
            "label": "Sign",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
            "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
          }
        ]
      },
      {
        "id": 42,
        "symbol": "HC",
        "label": "Hand capture",
        "description": "Refers to the targeted fauna observation method, i.e., any observations on a fauna from hand (physical) captures.",
        "uri": "https://linked.data.gov.au/def/nrm/c904310e-59ce-5c35-b95f-11c048b703a7",
        "tier_1": [
          {
            "id": 4,
            "symbol": "CA",
            "label": "Captured",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
            "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
          }
        ]
      },
      {
        "id": 43,
        "symbol": "PT",
        "label": "Pitfall trap",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures in a pitfall trap. A pitfall trap is a simple device used to catch small animals - particularly insects and other invertebrates - that spend most of their time on the ground. ",
        "uri": "https://linked.data.gov.au/def/nrm/aa78fa0e-3bd0-5c87-8ac7-0779f0699a11",
        "tier_1": [
          {
            "id": 4,
            "symbol": "CA",
            "label": "Captured",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
            "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
          }
        ]
      },
      {
        "id": 44,
        "symbol": "ET",
        "label": "Elliott trap",
        "description": "The equipment/method used during a passive, 'targeted fauna survey'. Elliott trapping is a technique used to trap small to medium sized mammals. The are usually hinged design that allows trapping to be conducted by folding into a compact panel and easy transport to field locations and storage.",
        "uri": "https://linked.data.gov.au/def/nrm/d1bdaf17-c484-59c6-84a0-8344a5ce5b5c",
        "tier_1": [
          {
            "id": 4,
            "symbol": "CA",
            "label": "Captured",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
            "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
          }
        ]
      },
      {
        "id": 45,
        "symbol": "FT",
        "label": "Funnel trap",
        "description": "The equipment/method used in a 'targeted fauna survey'. Funnel trap is a trapping method used in trapping insects/invertebrates. Funnel traps are made of nested black funnels (up to as many as 12). Insects fall through the funnels to a cup that is filled with a preservative. ",
        "uri": "https://linked.data.gov.au/def/nrm/970058a7-946d-5a5d-bb5b-c5b801a4def4",
        "tier_1": [
          {
            "id": 4,
            "symbol": "CA",
            "label": "Captured",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
            "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
          }
        ]
      },
      {
        "id": 46,
        "symbol": "HT",
        "label": "Harp trap",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made on a fauna captured in a 'Harp trap' (especially designed for bats). They are particularly useful in situations where bats in flight can be channeled through a natural funnel such as above a water course, a cave or mine entrance or a clear area within a forest. ",
        "uri": "https://linked.data.gov.au/def/nrm/b7a02786-f651-5199-877b-96d5dc4c66c6",
        "tier_1": [
          {
            "id": 4,
            "symbol": "CA",
            "label": "Captured",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
            "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
          }
        ]
      },
      {
        "id": 47,
        "symbol": "MN",
        "label": "Mist net",
        "description": "Refers to the targeted fauna observation method, i.e., any observations on a fauna captured using mist nets. The net is made of a very fine diameter cord, which is almost invisible when set up and is often used to capture birds, because they fail to see it, and fly straight into it.",
        "uri": "https://linked.data.gov.au/def/nrm/8ca1c330-b64a-5f97-aab4-9023fddaf009",
        "tier_1": [
          {
            "id": 4,
            "symbol": "CA",
            "label": "Captured",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
            "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
          }
        ]
      },
      {
        "id": 48,
        "symbol": "CT",
        "label": "Cage trap",
        "description": "The equipment/method used during a passive, 'targeted fauna survey'. A cage trap is a trap made of metal or galvanised mesh and used in trapping mammals. ",
        "uri": "https://linked.data.gov.au/def/nrm/8f6764de-0f3e-5f6f-b86e-e5e9f6053e24",
        "tier_1": [
          {
            "id": 4,
            "symbol": "CA",
            "label": "Captured",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
            "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
          }
        ]
      },
      {
        "id": 49,
        "symbol": "TU",
        "label": "Trap type unknown",
        "description": "",
        "uri": "",
        "tier_1": [
          {
            "id": 4,
            "symbol": "CA",
            "label": "Captured",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
            "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
          }
        ]
      },
      {
        "id": 50,
        "symbol": "WT",
        "label": "Wet pitfall trap",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures using a wet pitfall trap.",
        "uri": "https://linked.data.gov.au/def/nrm/4caf0cde-9ffb-56c2-bdc6-17c4c2a7275a",
        "tier_1": [
          {
            "id": 4,
            "symbol": "CA",
            "label": "Captured",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
            "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
          }
        ]
      },
      {
        "id": 51,
        "symbol": "MT",
        "label": "Malaise trap",
        "description": "Refers to the targeted fauna observation method, i.e., any observations on a fauna captured using a malaise trap. A Malaise trap is a type of insect trap primarily used to capture invertebrates. They are large, tent-like structure effective in capturing flying insects (e.g., members of Hymenoptera and Diptera). ",
        "uri": "https://linked.data.gov.au/def/nrm/8feff6f2-9d17-5bce-8933-d877198ea1ac",
        "tier_1": [
          {
            "id": 4,
            "symbol": "CA",
            "label": "Captured",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
            "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
          }
        ]
      },
      {
        "id": 52,
        "symbol": "PN",
        "label": "Pan trap",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures in a pan trap. A pan trap is a type of insect trap primarily used to capture small invertebrates (e.g., members of Hymenoptera) and often used to sample the abundance and diversity of insects. ",
        "uri": "https://linked.data.gov.au/def/nrm/9a7c0772-85f7-5f0b-8e6d-73b312137261",
        "tier_1": [
          {
            "id": 4,
            "symbol": "CA",
            "label": "Captured",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
            "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
          }
        ]
      },
      {
        "id": 53,
        "symbol": "LT",
        "label": "Light trap",
        "description": "The equipment/method used during a passive, 'targeted fauna survey'. Light trapping is designed for collecting flying insects attracted to ultra violet light and is useful for sampling insect populations.",
        "uri": "https://linked.data.gov.au/def/nrm/2ee3f6d7-6340-50c6-b9e3-1b115d75100b",
        "tier_1": [
          {
            "id": 4,
            "symbol": "CA",
            "label": "Captured",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
            "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
          }
        ]
      },
      {
        "id": 54,
        "symbol": "SW",
        "label": "Sweep net",
        "description": "The equipment/method used during a passive, 'targeted fauna survey'. Sweep nets are usually used for capturing insects using a number of sweeps. The net is made of fine diameter mesh fitted to a metal handle to trap invertebrates in air.",
        "uri": "https://linked.data.gov.au/def/nrm/f2b4d479-e3a3-5855-b5bb-24350de1be22",
        "tier_1": [
          {
            "id": 4,
            "symbol": "CA",
            "label": "Captured",
            "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures. ",
            "uri": "https://linked.data.gov.au/def/nrm/11cd363c-c75b-5436-9f26-19b457d15afc"
          }
        ]
      }
    ],
    "lut-tier-3-observation-methods": [
      {
        "id": 1,
        "symbol": "FR",
        "label": "Fresh",
        "description": "",
        "uri": "",
        "tier_2": [
          {
            "id": 24,
            "symbol": "SC",
            "label": "Scats",
            "description": "",
            "uri": "",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          }
        ]
      },
      {
        "id": 2,
        "symbol": "OL",
        "label": "Old",
        "description": "",
        "uri": "",
        "tier_2": [
          {
            "id": 24,
            "symbol": "SC",
            "label": "Scats",
            "description": "",
            "uri": "",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          }
        ]
      },
      {
        "id": 3,
        "symbol": "RE",
        "label": "Recent",
        "description": "",
        "uri": "",
        "tier_2": [
          {
            "id": 27,
            "symbol": "WP",
            "label": "Within pellet",
            "description": "Refers to the targeted fauna observation method, i.e., any signs of a fauna detected from observations within pellets of an animal.",
            "uri": "https://linked.data.gov.au/def/nrm/488ee615-5c57-5080-b991-04b4bfe05d7a",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          }
        ]
      },
      {
        "id": 4,
        "symbol": "SU",
        "label": "Subfossil",
        "description": "",
        "uri": "",
        "tier_2": [
          {
            "id": 27,
            "symbol": "WP",
            "label": "Within pellet",
            "description": "Refers to the targeted fauna observation method, i.e., any signs of a fauna detected from observations within pellets of an animal.",
            "uri": "https://linked.data.gov.au/def/nrm/488ee615-5c57-5080-b991-04b4bfe05d7a",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          }
        ]
      },
      {
        "id": 5,
        "symbol": "AC",
        "label": "Active",
        "description": "",
        "uri": "",
        "tier_2": [
          {
            "id": 29,
            "symbol": "BU",
            "label": "Burrow",
            "description": "Burrow is a small tunnel or a hole made by certain ground-dwelling mammals as a place of refuge.",
            "uri": "https://linked.data.gov.au/def/nrm/7230bbb8-9f03-5dd0-9ecf-fddfe4bb3c7c",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          },
          {
            "id": 30,
            "symbol": "DE",
            "label": "Den",
            "description": "",
            "uri": "",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          },
          {
            "id": 31,
            "symbol": "NE",
            "label": "Nest",
            "description": "Refers to the microhabitat where the targeted fauna was observed. A nest is a place of refuge to hold an animal's eggs or provide a place to live or raise offspring.",
            "uri": "https://linked.data.gov.au/def/nrm/b300ac98-8599-5de9-b99b-a2a17055cef8",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          }
        ]
      },
      {
        "id": 6,
        "symbol": "IN",
        "label": "Inactive",
        "description": "",
        "uri": "",
        "tier_2": [
          {
            "id": 29,
            "symbol": "BU",
            "label": "Burrow",
            "description": "Burrow is a small tunnel or a hole made by certain ground-dwelling mammals as a place of refuge.",
            "uri": "https://linked.data.gov.au/def/nrm/7230bbb8-9f03-5dd0-9ecf-fddfe4bb3c7c",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          },
          {
            "id": 30,
            "symbol": "DE",
            "label": "Den",
            "description": "",
            "uri": "",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          },
          {
            "id": 31,
            "symbol": "NE",
            "label": "Nest",
            "description": "Refers to the microhabitat where the targeted fauna was observed. A nest is a place of refuge to hold an animal's eggs or provide a place to live or raise offspring.",
            "uri": "https://linked.data.gov.au/def/nrm/b300ac98-8599-5de9-b99b-a2a17055cef8",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          }
        ]
      },
      {
        "id": 7,
        "symbol": "WE",
        "label": "With egg/s",
        "description": "",
        "uri": "",
        "tier_2": [
          {
            "id": 31,
            "symbol": "NE",
            "label": "Nest",
            "description": "Refers to the microhabitat where the targeted fauna was observed. A nest is a place of refuge to hold an animal's eggs or provide a place to live or raise offspring.",
            "uri": "https://linked.data.gov.au/def/nrm/b300ac98-8599-5de9-b99b-a2a17055cef8",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          }
        ]
      },
      {
        "id": 8,
        "symbol": "WC",
        "label": "With chick/s",
        "description": "",
        "uri": "",
        "tier_2": [
          {
            "id": 31,
            "symbol": "NE",
            "label": "Nest",
            "description": "Refers to the microhabitat where the targeted fauna was observed. A nest is a place of refuge to hold an animal's eggs or provide a place to live or raise offspring.",
            "uri": "https://linked.data.gov.au/def/nrm/b300ac98-8599-5de9-b99b-a2a17055cef8",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          }
        ]
      },
      {
        "id": 9,
        "symbol": "SO",
        "label": "Soil",
        "description": "",
        "uri": "",
        "tier_2": [
          {
            "id": 41,
            "symbol": "ED",
            "label": "eDNA",
            "description": "Refers to the method of identification of Vertebrate fauna. Taxon is identified using DNA sequencing techniques from environmental samples (eDNA).",
            "uri": "https://linked.data.gov.au/def/nrm/4d5d3f71-d98f-5619-9f4d-45330bcb51f3",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          }
        ]
      },
      {
        "id": 10,
        "symbol": "WA",
        "label": "Water",
        "description": "",
        "uri": "",
        "tier_2": [
          {
            "id": 41,
            "symbol": "ED",
            "label": "eDNA",
            "description": "Refers to the method of identification of Vertebrate fauna. Taxon is identified using DNA sequencing techniques from environmental samples (eDNA).",
            "uri": "https://linked.data.gov.au/def/nrm/4d5d3f71-d98f-5619-9f4d-45330bcb51f3",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          }
        ]
      },
      {
        "id": 11,
        "symbol": "PL",
        "label": "Plant",
        "description": "",
        "uri": "",
        "tier_2": [
          {
            "id": 41,
            "symbol": "ED",
            "label": "eDNA",
            "description": "Refers to the method of identification of Vertebrate fauna. Taxon is identified using DNA sequencing techniques from environmental samples (eDNA).",
            "uri": "https://linked.data.gov.au/def/nrm/4d5d3f71-d98f-5619-9f4d-45330bcb51f3",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          }
        ]
      },
      {
        "id": 12,
        "symbol": "AI",
        "label": "Air",
        "description": "",
        "uri": "",
        "tier_2": [
          {
            "id": 41,
            "symbol": "ED",
            "label": "eDNA",
            "description": "Refers to the method of identification of Vertebrate fauna. Taxon is identified using DNA sequencing techniques from environmental samples (eDNA).",
            "uri": "https://linked.data.gov.au/def/nrm/4d5d3f71-d98f-5619-9f4d-45330bcb51f3",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          }
        ]
      },
      {
        "id": 13,
        "symbol": "ID",
        "label": "Invertebrate derived",
        "description": "",
        "uri": "",
        "tier_2": [
          {
            "id": 41,
            "symbol": "ED",
            "label": "eDNA",
            "description": "Refers to the method of identification of Vertebrate fauna. Taxon is identified using DNA sequencing techniques from environmental samples (eDNA).",
            "uri": "https://linked.data.gov.au/def/nrm/4d5d3f71-d98f-5619-9f4d-45330bcb51f3",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          }
        ]
      },
      {
        "id": 14,
        "symbol": "VD",
        "label": "Vertebrate derived",
        "description": "",
        "uri": "",
        "tier_2": [
          {
            "id": 41,
            "symbol": "ED",
            "label": "eDNA",
            "description": "Refers to the method of identification of Vertebrate fauna. Taxon is identified using DNA sequencing techniques from environmental samples (eDNA).",
            "uri": "https://linked.data.gov.au/def/nrm/4d5d3f71-d98f-5619-9f4d-45330bcb51f3",
            "tier_1": [
              {
                "id": 3,
                "symbol": "SI",
                "label": "Sign",
                "description": "Refers to the targeted fauna observation method, i.e., any observations made from signs (e.g., tracks, skin, hair, scats, etc.) of fauna.",
                "uri": "https://linked.data.gov.au/def/nrm/2a831dcb-1d8f-55b4-8799-f0f75d8e5b6d"
              }
            ]
          }
        ]
      }
    ],
    "lut-time-format": [
      {
        "id": 1,
        "symbol": "12h",
        "label": "12h",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "24h",
        "label": "24h",
        "description": "",
        "uri": ""
      }
    ],
    "lut-track-station-micro-habitat": [
      {
        "id": 1,
        "symbol": "Open area",
        "label": "Open area",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Under tree",
        "label": "Under tree",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Under shrub",
        "label": "Under shrub",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Along animal pathway",
        "label": "Along animal pathway",
        "description": "",
        "uri": ""
      }
    ],
    "lut-track-station-substrate": [
      {
        "id": 1,
        "symbol": "Sand",
        "label": "Sand",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "Gravel",
        "label": "Gravel",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Tracking plate",
        "label": "Tracking plate",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "Tracking pad",
        "label": "Tracking pad",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-tracking-substrate": [
      {
        "id": 1,
        "symbol": "S",
        "label": "Sand",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "G",
        "label": "Gravel",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "M",
        "label": "Mud",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-transect-route-type": [
      {
        "id": 1,
        "symbol": "VR",
        "label": "Vehicle road",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "VD",
        "label": "Vehicle dirt track",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "WT",
        "label": "Walking track",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "OT",
        "label": "Off-track no defined path",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "Other",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-transect-sampling-type": [
      {
        "id": 1,
        "symbol": "DS",
        "label": "Distance sample",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "FW",
        "label": "Fixed width",
        "description": "",
        "uri": ""
      }
    ],
    "lut-trap-check-interval": [
      {
        "id": 1,
        "symbol": "MO",
        "label": "Morning",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "A",
        "label": "Afternoon",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "MI",
        "label": "Midday",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-type-of-damage": [
      {
        "id": 1,
        "symbol": "L",
        "label": "Leaf chewing",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "BR",
        "label": "Browsing",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "BG",
        "label": "Bark gnawing",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "S",
        "label": "Severed stems",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-type-of-physical-damage": [
      {
        "id": 1,
        "symbol": "D",
        "label": "Dig",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "B",
        "label": "Burrow",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "WR",
        "label": "Warren",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "PA",
        "label": "Pad",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "TP",
        "label": "Trail/pathway",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "WA",
        "label": "Wallow",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "RO",
        "label": "Rooting",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "PU",
        "label": "Pugging",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "RR",
        "label": "Rub/rut",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "RP",
        "label": "Roll pit",
        "description": "",
        "uri": ""
      },
      {
        "id": 11,
        "symbol": "TM",
        "label": "Tusk mark",
        "description": "",
        "uri": ""
      },
      {
        "id": 12,
        "symbol": "FW",
        "label": "Fouled water source",
        "description": "",
        "uri": ""
      },
      {
        "id": 13,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-uav-type": [
      {
        "id": 1,
        "symbol": "DJIMAM350",
        "label": "DJI Matrice M350",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "DJIMAM300",
        "label": "DJI Matrice M300",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "DJIMV3M",
        "label": "DJI Mavic 3M",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vantage-point-equipment": [
      {
        "id": 1,
        "symbol": "Binoculars",
        "label": "Binoculars",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "SS",
        "label": "Spotting scope",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "Rangefinder",
        "label": "Rangefinder",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "TH",
        "label": "Torch/headtorch",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "TD",
        "label": "Thermal device",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "CS",
        "label": "Camera – still",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "CV",
        "label": "Camera – video",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "Vehicle",
        "label": "Vehicle",
        "description": "",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "Other",
        "label": "Other",
        "description": "",
        "uri": ""
      },
      {
        "id": 10,
        "symbol": "SP",
        "label": "Spotlight",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vantage-point-type": [
      {
        "id": 1,
        "symbol": "V",
        "label": "Viewpoint",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "TWP",
        "label": "Target water point",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "TFS",
        "label": "Target food source",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "O",
        "label": "Other",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vector": [
      {
        "id": 1,
        "symbol": "P",
        "label": "Vector – point",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "L",
        "label": "Vector – line",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "PRG",
        "label": "Vector – polygon, Raster (grid).",
        "description": "",
        "uri": ""
      }
    ],
    "lut-veg-association": [
      {
        "id": 1,
        "symbol": "U",
        "label": "Upper Storey",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "M",
        "label": "Mid Storey",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "G",
        "label": "Ground Storey",
        "description": "",
        "uri": ""
      }
    ],
    "lut-veg-growth-form": [
      {
        "id": 1,
        "symbol": "V",
        "label": "Sedge",
        "description": "Herbaceous, usually perennial erect plant generally with a tufted habit and of the families Cyperaceae (true sedges) or Restionaceae (node sedges).",
        "uri": "https://linked.data.gov.au/def/nrm/a7b8b3c8-a329-5a62-8741-e1d6efe9528b"
      },
      {
        "id": 2,
        "symbol": "X",
        "label": "Grass-tree",
        "description": "Australian grass trees. Members of the Xanthorrhoeaceae family.",
        "uri": "https://linked.data.gov.au/def/nrm/1be47880-4ee6-5df9-8eda-551c58078771"
      },
      {
        "id": 3,
        "symbol": "Z",
        "label": "Heath shrub",
        "description": "Shrub usually less than 2 m, with sclerophyllous leaves having high fibre:protein ratios and with an area of nanophyll or smaller (less than 225 mm2). Often a member of one the following families: Ericaceae, Myrtaceae, Fabaceae and Proteaceae. Commonly occur on nutrient-poor substrates.",
        "uri": "https://linked.data.gov.au/def/nrm/2f9fed56-ebfe-5ca3-b520-f0666413c367"
      },
      {
        "id": 4,
        "symbol": "Q",
        "label": "Aquatic",
        "description": "Plant growing in an inland waterway or wetland with the majority of its biomass under water for most of the year. Fresh, saline or brackish water.",
        "uri": "https://linked.data.gov.au/def/nrm/b5ee3b40-0c5d-5b73-8550-e7e46dc74bee"
      },
      {
        "id": 5,
        "symbol": "T",
        "label": "Tree",
        "description": "Woody plants, more than 2 m tall with a single stem or branches well above the base.",
        "uri": "https://linked.data.gov.au/def/nrm/9b841f72-4903-5d65-818f-d386a6eca15f"
      },
      {
        "id": 6,
        "symbol": "D",
        "label": "Tree-fern",
        "description": "Characterised by large and usually branched leaves (fronds), arborescent and terrestrial; spores in sporangia on the leaves.",
        "uri": "https://linked.data.gov.au/def/nrm/2b964103-c6d7-56c1-8906-6f7b586cf352"
      },
      {
        "id": 7,
        "symbol": "A",
        "label": "Cycad",
        "description": "Members of the families Cycadaceae and Zamiaceae.",
        "uri": "https://linked.data.gov.au/def/nrm/02a1b24e-42fb-5c7c-b5a1-0d73d48ed364"
      },
      {
        "id": 8,
        "symbol": "O",
        "label": "Lower plant",
        "description": "Algae, fungus.",
        "uri": ""
      },
      {
        "id": 9,
        "symbol": "F",
        "label": "Forb",
        "description": "Herbaceous or slightly woody, annual or sometimes perennial plant (usually a dicotyledon).",
        "uri": "https://linked.data.gov.au/def/nrm/3d134553-3ac4-5634-9711-8a52204b038e"
      },
      {
        "id": 10,
        "symbol": "R",
        "label": "Rush",
        "description": "Herbaceous, usually perennial erect monocot that is neither a grass nor a sedge. For the purposes of NVIS, rushes include the monocotyledon families Juncaceae, Typhaceae, Liliaceae, Iridaceae, Xyridaceae and the genus Lomandra (i.e. \"graminoid\" or grass-like genera).",
        "uri": "https://linked.data.gov.au/def/nrm/ebca532a-2d76-5622-8078-5751be80b7e6"
      },
      {
        "id": 11,
        "symbol": "B",
        "label": "Bryophyte",
        "description": "Mosses and Liverworts. Mosses are small plants usually with a slender leaf-bearing stem with no true vascular tissue. Liverworts are often moss-like in appearance or consisting of a flat, ribbon-like green thallus.",
        "uri": "https://linked.data.gov.au/def/nrm/bb8edf40-de5b-5088-aa42-3a006a91ef7f"
      },
      {
        "id": 12,
        "symbol": "C",
        "label": "Chenopod shrub",
        "description": "Single or multi-stemmed, semi-succulent shrub of the family Chenopodiaceae exhibiting drought and salt tolerance.",
        "uri": "https://linked.data.gov.au/def/nrm/18a999a5-938c-5a4c-88f2-de7fd1f56bcc"
      },
      {
        "id": 13,
        "symbol": "K",
        "label": "Epiphyte",
        "description": "Epiphytes, mistletoes and parasites. Plant with roots attached to the aerial portions of other plants. Can often be another growth form, such as fern or forb.",
        "uri": "https://linked.data.gov.au/def/nrm/33261434-d5b6-560b-b036-866660c8bcbd"
      },
      {
        "id": 14,
        "symbol": "E",
        "label": "Fern",
        "description": "Ferns and fern allies, except tree-fern, above. Characterised by large and usually branched leaves (fronds), herbaceous and terrestrial to aquatic; spores in sporangia on the leaves.",
        "uri": "https://linked.data.gov.au/def/nrm/860d33aa-2b8b-51c4-aa39-641ef684a4c7"
      },
      {
        "id": 15,
        "symbol": "H",
        "label": "Hummock grass",
        "description": "Coarse xeromorphic grass with a mound-like form often dead in the middle; genus is Triodia.",
        "uri": "https://linked.data.gov.au/def/nrm/549135a7-1d2d-5201-ab60-1b0d8be3c816"
      },
      {
        "id": 16,
        "symbol": "S",
        "label": "Shrub",
        "description": "Woody plants multi-stemmed at the base (or within 200 mm from ground level) or if single stemmed, less than 2 m.",
        "uri": "https://linked.data.gov.au/def/nrm/5712f289-b19b-5e09-9ebb-37d697a95185"
      },
      {
        "id": 17,
        "symbol": "Y",
        "label": "Mallee shrub",
        "description": "Commonly less than 8 m tall, usually with five or more trunks, of which at least three of the largest do not exceed 100 mm diameter at breast height (1.3 m).",
        "uri": "https://linked.data.gov.au/def/nrm/de758c7e-e109-5c65-83a9-d27eaed1ab94"
      },
      {
        "id": 18,
        "symbol": "M",
        "label": "Tree Mallee",
        "description": "Woody perennial plant usually of the genus Eucalyptus. Multi-stemmed with fewer than five trunks of which at least three exceed 100 mm diameter at breast height (1.3 m). Usually 8 m or more.",
        "uri": "https://linked.data.gov.au/def/nrm/66aa8058-5e60-5025-a84e-de1234f23d8b"
      },
      {
        "id": 19,
        "symbol": "G",
        "label": "Tussock grass",
        "description": "Grass forming discrete but open tussocks usually with distinct individual shoots, or if not, then forming a hummock. These are the common agricultural grasses.",
        "uri": "https://linked.data.gov.au/def/nrm/e1eb0092-da6c-57b6-adde-707ed97fb30c"
      },
      {
        "id": 20,
        "symbol": "L",
        "label": "Vine",
        "description": "Climbing, twining, winding or sprawling plants usually with a woody stem.",
        "uri": "https://linked.data.gov.au/def/nrm/97b443f0-335e-5974-856f-6067e1bfab7d"
      },
      {
        "id": 21,
        "symbol": "W",
        "label": "Other grass",
        "description": "Member of the family Poaceae, but having neither a distinctive tussock nor hummock appearance. Examples include stoloniferous species such as Cynodon dactylon.",
        "uri": ""
      },
      {
        "id": 22,
        "symbol": "UK",
        "label": "Unknown",
        "description": "Unknown/unable to be determined.",
        "uri": "https://linked.data.gov.au/def/nrm/f6b0f6d8-16d8-5dd7-b1b7-66b0c020b96f"
      },
      {
        "id": 23,
        "symbol": "U",
        "label": "Samphire shrub",
        "description": "Genera (of Tribe Salicornioideae, viz.: Halosarcia, Pachycornia, Sarcocornia, Sclerostegia, Tecticornia and Tegicornia) with articulate branches, fleshy stems and reduced flowers within the Chenopodiaceae family, succulent chenopods (Wilson 1980). Also the genus Suaeda.",
        "uri": "https://linked.data.gov.au/def/nrm/0042bfe6-1df3-5456-8ab4-45b6028980d3"
      },
      {
        "id": 24,
        "symbol": "N",
        "label": "Lichen",
        "description": "Composite plant consisting of a fungus living symbiotically with algae; without true roots, stems or leaves.",
        "uri": "https://linked.data.gov.au/def/nrm/4e5e46b9-d5da-5975-9bba-d6eacad12875"
      },
      {
        "id": 25,
        "symbol": "P",
        "label": "Palm",
        "description": "Palms and other arborescent monocotyledons. Members of the Arecaceae family or the genus Pandanus (often multi-stemmed).",
        "uri": "https://linked.data.gov.au/def/nrm/a35e09f9-051c-527b-b99c-a2fa4df9bd01"
      },
      {
        "id": 26,
        "symbol": "J",
        "label": "Seagrass",
        "description": "Flowering angiosperms forming sparse to dense mats of material at the subtidal and down to 30 m below MSL. Occasionally exposed..",
        "uri": "https://linked.data.gov.au/def/nrm/581291c8-5ff5-54b8-829c-93804f4b3691"
      }
    ],
    "lut-veg-growth-stage": [
      {
        "id": 1,
        "symbol": "ER",
        "label": "Early Regeneration",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "AR",
        "label": "Advanced Regeneration",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "UA",
        "label": "Uneven Age",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "MP",
        "label": "Mature Phase",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "SP",
        "label": "Senescent Phase",
        "description": "",
        "uri": ""
      }
    ],
    "lut-veg-structural-formation": [
      {
        "id": 1,
        "symbol": "CFOR",
        "label": "Closed forest",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of Forbs or herbs other than grassess. ",
        "uri": "https://linked.data.gov.au/def/nrm/ac2238c8-b7bc-5a89-8cc3-774bc3a53064"
      },
      {
        "id": 2,
        "symbol": "OFOR",
        "label": "Open forest",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of trees including palms. ",
        "uri": "https://linked.data.gov.au/def/nrm/9b644ae5-6279-5794-8535-b56cef440f3d"
      },
      {
        "id": 3,
        "symbol": "W",
        "label": "Woodland",
        "description": "Refers to the type of habitat characterised by a low-density forest forming open habitats with plenty of sunlight and limited shade.",
        "uri": "https://linked.data.gov.au/def/nrm/e2193542-9203-532b-99d3-853a4640114d"
      },
      {
        "id": 4,
        "symbol": "OW",
        "label": "Open woodland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of trees including palms.",
        "uri": "https://linked.data.gov.au/def/nrm/09fdd99f-e627-542c-afed-824af660bf48"
      },
      {
        "id": 5,
        "symbol": "IT",
        "label": "Isolated trees",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of trees including palms. ",
        "uri": "https://linked.data.gov.au/def/nrm/9935b7bc-2bfa-5b34-ae38-12da54c24bbc"
      },
      {
        "id": 6,
        "symbol": "ICT",
        "label": "Isolated clumps of trees",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of trees including palms.",
        "uri": "https://linked.data.gov.au/def/nrm/f9f75088-5788-5af5-9398-2f24f411ec6b"
      },
      {
        "id": 7,
        "symbol": "T",
        "label": "Trees",
        "description": "Woody plants, more than 2 m tall with a single stem or branches well above the base.",
        "uri": "https://linked.data.gov.au/def/nrm/c9e162d5-640b-52d4-985a-794b5f02fa05"
      },
      {
        "id": 8,
        "symbol": "CMFOR",
        "label": "Closed mallee forest",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of tree mallee (e.g., some members of Eucalyptus). ",
        "uri": "https://linked.data.gov.au/def/nrm/36a6499f-b1f6-5405-a2ec-73498ac874eb"
      },
      {
        "id": 9,
        "symbol": "OMFOR",
        "label": "Open mallee forest",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of tree Mallee (e.g., certain individuals of Eucalypts multistemmed from base). ",
        "uri": "https://linked.data.gov.au/def/nrm/3f0f6d0b-e136-5cad-bd74-f229aeb6fe04"
      },
      {
        "id": 10,
        "symbol": "MW",
        "label": "Mallee woodland",
        "description": "Refers to the dominant vegetation structural formation, with a percent cover of about 20-50% of Tree Mallee. ",
        "uri": "https://linked.data.gov.au/def/nrm/3de3bfd1-7d1c-5656-8439-85f56f487c40"
      },
      {
        "id": 11,
        "symbol": "OMW",
        "label": "Open mallee woodland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of tree mallee (e.g., certain individuals of Eucalypts multistemmed from base). ",
        "uri": "https://linked.data.gov.au/def/nrm/484d51eb-c1ee-597c-aed0-664a28e7b1d1"
      },
      {
        "id": 12,
        "symbol": "IMT",
        "label": "Isolated mallee trees",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of tree mallee (e.g., some multistemmed individuals from base of Eucalyptus). ",
        "uri": "https://linked.data.gov.au/def/nrm/8c26e6d6-6287-5139-89c3-03c730b544e0"
      },
      {
        "id": 13,
        "symbol": "ICMT",
        "label": "Isolated clumps of mallee trees",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of tree Mallee (e.g., some multistemmed Eucalyptus). ",
        "uri": "https://linked.data.gov.au/def/nrm/e5b03111-b3e5-5010-bb98-7d0e43fb3888"
      },
      {
        "id": 14,
        "symbol": "MT",
        "label": "Mallee Trees",
        "description": "Refers to the dominant vegetation structural formation. Woody perennial plant usually of the genus Eucalyptus. Multi-stemmed with fewer than five trunks of which at least three exceed 100 mm diameter at breast height (1.3 m). Usually 8 m or more.",
        "uri": "https://linked.data.gov.au/def/nrm/380517df-4ccc-57b1-af0e-940872681545"
      },
      {
        "id": 15,
        "symbol": "CSHR",
        "label": "Closed shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of sedges (e.g., Cyperaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/97fa02b0-f5c2-5cae-a822-aa0385e299b7"
      },
      {
        "id": 16,
        "symbol": "SHR",
        "label": "Shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of shrubs (e.g., shrub, cycad, grass-tree, tree-fern).",
        "uri": "https://linked.data.gov.au/def/nrm/18e5a444-f39c-50ea-84a5-9f677ee46601"
      },
      {
        "id": 17,
        "symbol": "OSHR",
        "label": "Open shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of shrubs (e.g. shrubs, cycads, grass-tree, tree-fern).",
        "uri": "https://linked.data.gov.au/def/nrm/c5fb3713-be56-56e3-8fb7-922c27dd2ae9"
      },
      {
        "id": 18,
        "symbol": "SSHR",
        "label": "Sparse shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of shrubs, including cycad, grass-tree, tree-fern. ",
        "uri": "https://linked.data.gov.au/def/nrm/7ecfb199-be34-5656-887b-c6205faf0cd1"
      },
      {
        "id": 19,
        "symbol": "ISHR",
        "label": "Isolated shrubs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of shrubs including cycads, grass-tree and tree-fern. ",
        "uri": "https://linked.data.gov.au/def/nrm/97d1820d-545f-5694-b379-9f360b8d123b"
      },
      {
        "id": 20,
        "symbol": "ICSHR",
        "label": "Isolated clumps of shrubs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of shrubs including cycads, grass-tree, tree-fern.",
        "uri": "https://linked.data.gov.au/def/nrm/21f7183e-c70c-5daf-99bd-f53abb4a9b3c"
      },
      {
        "id": 21,
        "symbol": "S",
        "label": "Shrubs",
        "description": "Woody plants multi-stemmed at the base (or within 200mm from ground level) or if single stemmed, less than 2 m.",
        "uri": "https://linked.data.gov.au/def/nrm/586ff52e-7e6d-53e6-a756-1e1a64c0d325"
      },
      {
        "id": 22,
        "symbol": "CMS",
        "label": "Closed mallee shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of mallee shrubs (e.g., some members of Eucalyptus). ",
        "uri": "https://linked.data.gov.au/def/nrm/c751ff0d-e9a4-5a3d-bf46-27fbb352a1e4"
      },
      {
        "id": 23,
        "symbol": "MSL",
        "label": "Mallee shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of shrub mallee (e.g., individuals of some Eucalypts multistemmed from base). ",
        "uri": "https://linked.data.gov.au/def/nrm/f5e68c19-0e4c-5aa9-8076-7879f2a6cce4"
      },
      {
        "id": 24,
        "symbol": "OMS",
        "label": "Open mallee shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of Mallee shrubs (e.g., certain individuals of Eucalypts multistemmed from base). ",
        "uri": "https://linked.data.gov.au/def/nrm/9baa87b2-346a-595c-bcb9-f50a2e7101f2"
      },
      {
        "id": 25,
        "symbol": "SMS",
        "label": "Sparse mallee shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of members of shrub Mallee. ",
        "uri": "https://linked.data.gov.au/def/nrm/3c2c0ce9-5d47-5e85-952d-1e8584d9d9d0"
      },
      {
        "id": 26,
        "symbol": "IMS",
        "label": "Isolated mallee shrubs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of mallee shrubs (e.g., some multistemmed individuals from base of Eucalyptus). ",
        "uri": "https://linked.data.gov.au/def/nrm/02056871-964d-5bc5-b5eb-dcd4c761f0ff"
      },
      {
        "id": 27,
        "symbol": "ICMS",
        "label": "Isolated clumps of mallee shrubs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of Mallee shrubs (e.g., some multistemmed Eucalyptus). ",
        "uri": "https://linked.data.gov.au/def/nrm/796f2b03-46fb-5b38-96b3-e8c0838e7e88"
      },
      {
        "id": 28,
        "symbol": "MS",
        "label": "Mallee Shrubs",
        "description": "Refers to the dominant vegetation structural formation. Commonly less than 8 m tall, usually with five or more trunks, of which at least three of the largest do not exceed 100 mm diameter at breast height (1.3 m).",
        "uri": "https://linked.data.gov.au/def/nrm/9ad2a86d-3836-5254-bc8e-093ceb7a8b11"
      },
      {
        "id": 29,
        "symbol": "CH",
        "label": "Closed heathland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of heath shrubs (e.g., members of Ericaceae, Myrtaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/c1781447-eaa9-534a-8d3a-4935cc0ab29e"
      },
      {
        "id": 30,
        "symbol": "H",
        "label": "Heathland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% members of Heath (e.g., Ericaceae, Myrtaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/517d2f98-9d24-5b68-b8d8-f5393eba310a"
      },
      {
        "id": 31,
        "symbol": "OH",
        "label": "Open heathland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of heaths (e.g., Ericaceae, Myrtaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/70e3ed2b-5bdf-5113-9b1e-8107f50955e0"
      },
      {
        "id": 32,
        "symbol": "SH",
        "label": "Sparse heathland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of members of heath (e.g., Ericaceae, Myrtaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/5fc3bd77-3c7f-54b2-8bcd-2a01c2412bb0"
      },
      {
        "id": 33,
        "symbol": "HIS",
        "label": "Isolated heath shrubs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of heath shrubs (e.g., Ericaceae, Myrtaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/824bdcd9-2c63-5a01-9c14-d38479118daa"
      },
      {
        "id": 34,
        "symbol": "ICHS",
        "label": "Isolated clumps of heath shrubs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of Heath (e.g., Ericaceae, Myrtaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/b23cd508-96e0-5d0d-8738-8e432d04e730"
      },
      {
        "id": 35,
        "symbol": "HS",
        "label": "Heath Shrubs",
        "description": "",
        "uri": ""
      },
      {
        "id": 36,
        "symbol": "CCS",
        "label": "Closed chenopod shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of Chenopodiaceae. ",
        "uri": "https://linked.data.gov.au/def/nrm/7cf0b84b-3277-5bfd-9fd0-0a799cda4588"
      },
      {
        "id": 37,
        "symbol": "CSL",
        "label": "Chenopod shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% members of Chenopodiaceae. ",
        "uri": "https://linked.data.gov.au/def/nrm/99807739-a552-5004-9105-6e580a123002"
      },
      {
        "id": 38,
        "symbol": "OCS",
        "label": "Open chenopod shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of members of Chenopodiaceae.",
        "uri": "https://linked.data.gov.au/def/nrm/740661b3-4178-508a-a7a4-64f07c6c3a8a"
      },
      {
        "id": 39,
        "symbol": "SCS",
        "label": "Sparse chenopod shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of members of Chenopodiaceae. ",
        "uri": "https://linked.data.gov.au/def/nrm/c8a4a268-54ef-5327-9530-3acb1c6bb048"
      },
      {
        "id": 40,
        "symbol": "ICS",
        "label": "Isolated chenopod shrubs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% members of Chenopodiaceae. ",
        "uri": "https://linked.data.gov.au/def/nrm/9435f240-8da8-5b84-83fa-ffdd0692c6dc"
      },
      {
        "id": 41,
        "symbol": "ICCS",
        "label": "Isolated clumps of chenopod shrubs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of Chenopodiaceae. ",
        "uri": "https://linked.data.gov.au/def/nrm/62eb903f-9902-502c-a4a1-1512ad756bfc"
      },
      {
        "id": 42,
        "symbol": "CS",
        "label": "Chenopod Shrubs",
        "description": "Refers to the growth form as defined by the NVIS. Chenopod Shrubs are single or multi-stemmed, semi-succulent shrub of the family Chenopodiaceae exhibiting drought and salt tolerance.",
        "uri": "https://linked.data.gov.au/def/nrm/99589075-d720-5d62-bbbc-f41357df577a"
      },
      {
        "id": 43,
        "symbol": "CSS",
        "label": "Closed samphire shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of samphire (e.g., Saltbush).",
        "uri": "https://linked.data.gov.au/def/nrm/af971a83-75af-55ab-9763-9f8c52c094b8"
      },
      {
        "id": 44,
        "symbol": "SS",
        "label": "Samphire shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of samphire shrubs (e.g., Saltbush).",
        "uri": "https://linked.data.gov.au/def/nrm/4a87d37f-c78f-535b-904a-6fc021883475"
      },
      {
        "id": 45,
        "symbol": "OSS",
        "label": "Open samphire shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of samphire bushes (e.g. salt bush).",
        "uri": "https://linked.data.gov.au/def/nrm/4f670e17-b4f0-5d0c-8453-29e173f96d71"
      },
      {
        "id": 46,
        "symbol": "SSS",
        "label": "Sparse samphire shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of samphire shrubs (e.g., saltbush). ",
        "uri": "https://linked.data.gov.au/def/nrm/adf35f1b-1790-518a-b9ef-bd82e540d181"
      },
      {
        "id": 47,
        "symbol": "ISS",
        "label": "Isolated samphire shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of samphire shrubs (e.g., saltbush). ",
        "uri": "https://linked.data.gov.au/def/nrm/35a105da-95fb-5df9-9563-f06913551d6c"
      },
      {
        "id": 48,
        "symbol": "ICSS",
        "label": "Isolated clumps of samphire shrubs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of tree samphire (e.g., saltbush).",
        "uri": "https://linked.data.gov.au/def/nrm/541d2400-275f-59ff-afb5-afd41c95c820"
      },
      {
        "id": 49,
        "symbol": "SAS",
        "label": "Samphire shrubs",
        "description": "",
        "uri": ""
      },
      {
        "id": 50,
        "symbol": "CHG",
        "label": "Closed hummock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of hummock (e.g., Triodia) grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/5a0039b6-1652-59cf-95ba-0ea41b7626e8"
      },
      {
        "id": 51,
        "symbol": "HG",
        "label": "Hummock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% members of hummock grasses (e.g., Triodia). ",
        "uri": "https://linked.data.gov.au/def/nrm/b77c7b89-0dd2-52cb-9eb3-7319d4465cfe"
      },
      {
        "id": 52,
        "symbol": "OHG",
        "label": "Open hummock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of hummock grasses (e.g., Triodia). ",
        "uri": "https://linked.data.gov.au/def/nrm/9c0fd421-510f-56f5-92e6-e18ceb62419d"
      },
      {
        "id": 53,
        "symbol": "SHG",
        "label": "Sparse hummock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of members of hummock grasses (e.g. Triodia). ",
        "uri": "https://linked.data.gov.au/def/nrm/6fe44ec9-3315-5ce3-b5ad-c6c6e56565c2"
      },
      {
        "id": 54,
        "symbol": "IHG",
        "label": "Isolated hummock grasses",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of hummock grasses (e.g., Triodia). ",
        "uri": "https://linked.data.gov.au/def/nrm/a137d32b-5133-52f0-a127-ab64ad4e9331"
      },
      {
        "id": 55,
        "symbol": "ICHG",
        "label": "Isolated clumps of hummock grasses",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of Hummock grasses (e.g., Triodia). ",
        "uri": "https://linked.data.gov.au/def/nrm/84937582-fcca-5126-baf7-01a6b7b1017a"
      },
      {
        "id": 56,
        "symbol": "HGR",
        "label": "Hummock Grasses",
        "description": "A type of evergreen perennials that appear as mounds up to 1m in height. In between the mounds or hummocks the ground is usually bare or exposed, except after seasonal or cyclonic rains, when multiple short-lived, ephemeral plants proliferate. Some examples are species of the genus Triodia, Spinifex.",
        "uri": "https://linked.data.gov.au/def/nrm/06781373-6f7d-5fb6-b224-18589380d9dd"
      },
      {
        "id": 57,
        "symbol": "CTG",
        "label": "Closed tussock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of tussock grasses (e.g., Poa).",
        "uri": "https://linked.data.gov.au/def/nrm/2879f1a4-9a89-5faa-b43c-ae04aeeb9d23"
      },
      {
        "id": 58,
        "symbol": "TG",
        "label": "Tussock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of tussock grass (e.g., Poa species). ",
        "uri": "https://linked.data.gov.au/def/nrm/d474a334-fb65-5496-917c-7f879697157e"
      },
      {
        "id": 59,
        "symbol": "OTG",
        "label": "Open tussock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of tussock grasses (e.g. Poa species).",
        "uri": "https://linked.data.gov.au/def/nrm/021ac7e0-af3f-5c5f-9835-6566cd7d45dd"
      },
      {
        "id": 60,
        "symbol": "STG",
        "label": "Sparse tussock grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of tussock grass (e.g., Poa species). ",
        "uri": "https://linked.data.gov.au/def/nrm/eba388b7-bf44-5c65-b468-aebb1afbc5e2"
      },
      {
        "id": 61,
        "symbol": "ITG",
        "label": "Isolated tussock grasses",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of tussock grass (e.g. Poa species). ",
        "uri": "https://linked.data.gov.au/def/nrm/d958b180-ff63-56c5-a39a-4d04bd07207b"
      },
      {
        "id": 62,
        "symbol": "ICTG",
        "label": "Isolated clumps of tussock grasses",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of tussock grasses (e.g., Poa).",
        "uri": "https://linked.data.gov.au/def/nrm/c059d313-da9a-5fb8-a9af-2e603d8b2444"
      },
      {
        "id": 63,
        "symbol": "TGR",
        "label": "Tussock Grasses",
        "description": "Grass forming discrete but open tussocks usually with distinct individual shoots, or if not, then forming a hummock. These are the common agricultural grasses, also known as bunch grasses, and are a group of grass species in the family Poaceae (Gramineae). ",
        "uri": "https://linked.data.gov.au/def/nrm/ceff3c31-f6ea-5f06-9b41-be18e4e9446e"
      },
      {
        "id": 64,
        "symbol": "CG",
        "label": "Closed grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/89f6663f-a6b1-5041-a5f4-db236fa2bc8a"
      },
      {
        "id": 65,
        "symbol": "GR",
        "label": "Grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% members of grasses (Gramineae). ",
        "uri": "https://linked.data.gov.au/def/nrm/403def1f-93b1-5d8e-a823-044747e20980"
      },
      {
        "id": 66,
        "symbol": "OGR",
        "label": "Open grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/8bb55f19-ebf1-58c1-9f2f-bc3aa73275ab"
      },
      {
        "id": 67,
        "symbol": "SGR",
        "label": "Sparse grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/e28adc38-5e40-5795-93d1-6582d0013f7e"
      },
      {
        "id": 68,
        "symbol": "IGR",
        "label": "Isolated grasses",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/04b93639-cbca-5de5-a687-5d401fb74d28"
      },
      {
        "id": 69,
        "symbol": "ICGR",
        "label": "Isolated clumps of grasses",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of tussock grasses (e.g., Poa).",
        "uri": "https://linked.data.gov.au/def/nrm/93db9060-a653-55f4-a979-3c9d67b6a59e"
      },
      {
        "id": 70,
        "symbol": "OG",
        "label": "Other Grasses",
        "description": "Member of the family Poaceae, but having neither a distinctive tussock nor hummock appearance. Examples include stoloniferous species such as Cynodon dactylon.",
        "uri": "https://linked.data.gov.au/def/nrm/ae81906f-7fd3-5aec-9557-33e95d05eaa0"
      },
      {
        "id": 71,
        "symbol": "CSE",
        "label": "Closed sedgeland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of sedges (e.g., Cyperaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/193d73c9-b3e8-59b5-860e-109eae4efd16"
      },
      {
        "id": 72,
        "symbol": "SEL",
        "label": "Sedgeland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of sedges (e.g., Cyperaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/6e866ca0-1492-5e47-a520-8bac223f2dc8"
      },
      {
        "id": 73,
        "symbol": "OSE",
        "label": "Open sedgeland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of sedges (e.g. Cyperaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/1f71cccb-10a3-5dad-a1f1-2007c1c670c3"
      },
      {
        "id": 74,
        "symbol": "SSE",
        "label": "Sparse sedgeland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of sedges (e.g., Cyperaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/864f2e87-9c4c-59c5-936d-557ac7942996"
      },
      {
        "id": 75,
        "symbol": "ISE",
        "label": "Isolated sedges",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of sedges (e.g., Cyperaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/150a361f-0b85-59da-93d0-65206b6eae9e"
      },
      {
        "id": 76,
        "symbol": "ICSE",
        "label": "Isolated clumps of sedges",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of sedges (e.g., Cyperaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/ee5b50e1-071d-5b9d-a9c8-a4c47e03a160"
      },
      {
        "id": 77,
        "symbol": "SE",
        "label": "Sedges",
        "description": "Herbaceous, usually perennial erect plant generally with a tufted habit and of the families Cyperaceae (true sedges) or Restionaceae (node sedges).",
        "uri": "https://linked.data.gov.au/def/nrm/18df20f5-5d44-595c-8b7c-b0b83f43f410"
      },
      {
        "id": 78,
        "symbol": "CRU",
        "label": "Closed rushland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of Rushes (e.g., Juncaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/230bb275-6421-539b-9852-07a5f76c660f"
      },
      {
        "id": 79,
        "symbol": "RU",
        "label": "Rushland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of rushes (e.g. Juncaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/3de9060b-389c-5340-9e1c-288719d05007"
      },
      {
        "id": 80,
        "symbol": "ORU",
        "label": "Open rushland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of rushes (e.g. Juncaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/4e22f9a2-1bc6-53ba-8226-e18fef762fb9"
      },
      {
        "id": 81,
        "symbol": "SRU",
        "label": "Sparse rushland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of rushes (e.g., Juncaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/3c0c31d0-7360-55cd-bd02-5e2a2e47b11b"
      },
      {
        "id": 82,
        "symbol": "IRU",
        "label": "Isolated rushes",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of rushes (e.g., Juncaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/3a1e4a07-df4a-5a18-9f61-72f7c87306f5"
      },
      {
        "id": 83,
        "symbol": "ICRU",
        "label": "Isolated clumps of rushes",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of tree Rushes (e.g., Juncaceae). ",
        "uri": "https://linked.data.gov.au/def/nrm/0ea92f61-5198-51cf-a27e-14b083bddb7c"
      },
      {
        "id": 84,
        "symbol": "R",
        "label": "Rushes",
        "description": "The vegetation represents herbaceous, usually perennial erect monocot that is neither a grass nor a sedge. Rushes include the monocotyledon families Juncaceae, Typhaceae, Liliaceae, Iridaceae, Xyridaceae and the genus Lomandra. I.e. graminoid or grass-like genera.",
        "uri": "https://linked.data.gov.au/def/nrm/f0184838-2674-56ac-b5bb-d97e82016c93"
      },
      {
        "id": 85,
        "symbol": "CFO",
        "label": "Closed forbland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of Forbs or herbs other than grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/7a9329ca-a03d-5c22-9c3c-3ca03d038fb9"
      },
      {
        "id": 86,
        "symbol": "FO",
        "label": "Forbland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% members of Forbs or herbaceous plants other than grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/6f68413c-4dcd-5d02-a0c7-0ad3a56d62d3"
      },
      {
        "id": 87,
        "symbol": "OFO",
        "label": "Open forbland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of forbs or herbs other than grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/32532a0a-3b85-5ebd-b72e-2b87666baf62"
      },
      {
        "id": 88,
        "symbol": "SFO",
        "label": "Sparse forbland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of members of forbs and herbs other than grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/bff147ed-4af8-50d5-be4e-b2d817686767"
      },
      {
        "id": 89,
        "symbol": "IFO",
        "label": "Isolated forbs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of forbs or herbs other than grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/f03afa59-394c-50e6-ba58-3b7dc38a1363"
      },
      {
        "id": 90,
        "symbol": "ICFO",
        "label": "Isolated clumps of forbs",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of Forbs or herbs other than grasses. ",
        "uri": "https://linked.data.gov.au/def/nrm/37b756c2-5607-58de-a31b-eb5b1ce626ed"
      },
      {
        "id": 91,
        "symbol": "F",
        "label": "Forbs",
        "description": "Herbaceous or slightly woody, annual or sometimes perennial plant. (Usually a dicotyledon).",
        "uri": "https://linked.data.gov.au/def/nrm/ecfeb7c1-e2b2-5cae-abc4-8718d98facd1"
      },
      {
        "id": 92,
        "symbol": "CFE",
        "label": "Closed fernland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% members of Fern and Fern-allies. ",
        "uri": "https://linked.data.gov.au/def/nrm/77dc385f-c9a0-56f3-b14e-4485f7adcb78"
      },
      {
        "id": 93,
        "symbol": "FE",
        "label": "Fernland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% members of Fern and Fern-allies. ",
        "uri": "https://linked.data.gov.au/def/nrm/87e02dde-35a9-5ee3-b919-5bd34d83a2ff"
      },
      {
        "id": 94,
        "symbol": "OFE",
        "label": "Open fernland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of ferns and fern allies. ",
        "uri": "https://linked.data.gov.au/def/nrm/5deef8a7-e536-57a9-9b7a-caf9f5d4d39d"
      },
      {
        "id": 95,
        "symbol": "SFE",
        "label": "Sparse fernland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of members of fern and fern-allies. ",
        "uri": "https://linked.data.gov.au/def/nrm/30c6a39a-211b-5526-93b3-467bbb9e56b4"
      },
      {
        "id": 96,
        "symbol": "IFE",
        "label": "Isolated ferns",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of fern and fern allies. ",
        "uri": "https://linked.data.gov.au/def/nrm/aa9b68be-a749-537e-baf9-6880088198cc"
      },
      {
        "id": 97,
        "symbol": "ICFE",
        "label": "Isolated clumps of ferns",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% members of Fern and Fern-allies. ",
        "uri": "https://linked.data.gov.au/def/nrm/e692ff93-02cd-5bdf-bc4e-8f76de59089a"
      },
      {
        "id": 98,
        "symbol": "FER",
        "label": "Ferns",
        "description": "Ferns and fern allies, except tree-fern are characterised by large and usually branched leaves (fronds), herbaceous and terrestrial to aquatic; spores in sporangia on the leaves.",
        "uri": "https://linked.data.gov.au/def/nrm/647cc53b-9c40-5691-8ec8-69cd70299915"
      },
      {
        "id": 99,
        "symbol": "CBR",
        "label": "Closed bryophyteland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about >80% of bryophytes. ",
        "uri": "https://linked.data.gov.au/def/nrm/96d5c5ac-8a16-5441-924d-35b02071f912"
      },
      {
        "id": 100,
        "symbol": "BR",
        "label": "Bryophyteland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of bryophytes of about 50-80%. ",
        "uri": "https://linked.data.gov.au/def/nrm/e46d25e4-e076-5974-8dff-61bc47c0a9ed"
      },
      {
        "id": 101,
        "symbol": "OBR",
        "label": "Open bryophyteland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 20-50% of bryophytes, including moss and lichens. ",
        "uri": "https://linked.data.gov.au/def/nrm/f2872370-c8ce-549c-be10-9914e3026817"
      },
      {
        "id": 102,
        "symbol": "SBR",
        "label": "Sparse Bryophyteland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0.25-20% of bryophytes, including moss and lichens. ",
        "uri": "https://linked.data.gov.au/def/nrm/d4c5b958-4627-5a44-8a23-094ee1a13b6e"
      },
      {
        "id": 103,
        "symbol": "IBR",
        "label": "Isolated bryophytes",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about <0.25% of bryophytes. ",
        "uri": "https://linked.data.gov.au/def/nrm/b7939d9f-cfc0-566a-b4bc-be048f5d2b54"
      },
      {
        "id": 104,
        "symbol": "ICBR",
        "label": "Isolated clumps of bryophytes",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 0-5% of bryophytes. ",
        "uri": "https://linked.data.gov.au/def/nrm/f07384f0-4e8e-5023-857d-c4c3259a179c"
      },
      {
        "id": 105,
        "symbol": "B",
        "label": "Bryophtyes",
        "description": "Bryophytes are Mosses and Liverworts. Mosses are small plants usually with a slender leafbearing stem with no true vascular tissue. Liverworts are often moss-like in appearance or consisting of a flat, ribbon-like green thallus.",
        "uri": "https://linked.data.gov.au/def/nrm/bc97d560-add5-590d-8c52-f50600356a93"
      }
    ],
    "lut-vegetation-type": [
      {
        "id": 1,
        "symbol": "FO",
        "label": "Forest",
        "description": "Forest is an area with high density of trees, including palms in some cases (especially the tropics). ",
        "uri": "https://linked.data.gov.au/def/nrm/3cb567ad-6232-5f24-8c99-2dafb5a16626"
      },
      {
        "id": 2,
        "symbol": "WO",
        "label": "Woodland",
        "description": "Refers to the type of habitat characterised by a low-density forest forming open habitats with plenty of sunlight and limited shade.",
        "uri": "https://linked.data.gov.au/def/nrm/e2193542-9203-532b-99d3-853a4640114d"
      },
      {
        "id": 3,
        "symbol": "SH",
        "label": "Shrubland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of shrubs (e.g., shrub, cycad, grass-tree, tree-fern).",
        "uri": "https://linked.data.gov.au/def/nrm/18e5a444-f39c-50ea-84a5-9f677ee46601"
      },
      {
        "id": 4,
        "symbol": "MA",
        "label": "Mallee",
        "description": "Refers to the type of habitat representative of a characteristic dominant tree mallee vegetation (members of certain Eucalyptus species, with multistemmed close growth).",
        "uri": "https://linked.data.gov.au/def/nrm/e71152f4-f6df-5ba3-9ff3-df134d3c770a"
      },
      {
        "id": 5,
        "symbol": "SC",
        "label": "Scrubland",
        "description": "A dryland open habitat dominated by shrubs.",
        "uri": "https://linked.data.gov.au/def/nrm/c0a34405-508b-56f7-8011-991bfa87c5ec"
      },
      {
        "id": 6,
        "symbol": "GR",
        "label": "Grassland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% members of grasses (Gramineae). ",
        "uri": "https://linked.data.gov.au/def/nrm/403def1f-93b1-5d8e-a823-044747e20980"
      },
      {
        "id": 7,
        "symbol": "SP",
        "label": "Spinifex",
        "description": "A land predominated by Spinifex grass (hummock grass).",
        "uri": "https://linked.data.gov.au/def/nrm/081fed66-17c9-554b-9759-5d8202baddc1"
      },
      {
        "id": 8,
        "symbol": "SE",
        "label": "Sedgeland",
        "description": "Refers to the NVIS dominant vegetation structural formation class, with a percent cover of about 50-80% of sedges (e.g., Cyperaceae).",
        "uri": "https://linked.data.gov.au/def/nrm/6e866ca0-1492-5e47-a520-8bac223f2dc8"
      },
      {
        "id": 9,
        "symbol": "SA",
        "label": "Saltbush/Samphire",
        "description": "A dryland habitat dominated by the genus Atriplex (saltbush) and or samphire bushes.",
        "uri": "https://linked.data.gov.au/def/nrm/72fa6f41-c434-5baa-9564-890efe6ef457"
      },
      {
        "id": 10,
        "symbol": "RO",
        "label": "Rock Community",
        "description": "A rocky habitat is primarily composed of rocks of various cuts formed by natural, geologic or biogeochemical activities.",
        "uri": "https://linked.data.gov.au/def/nrm/14fe23bb-1be8-5806-9c1e-556122e72cf4"
      },
      {
        "id": 11,
        "symbol": "WE",
        "label": "Wetland/Riparian",
        "description": "A wetland is an environment at the interface between truly terrestrial ecosystems and aquatic systems making them inherently different from each other yet highly dependent on both.",
        "uri": "https://linked.data.gov.au/def/nrm/603d987e-68fe-5dfd-a139-540203d6d730"
      },
      {
        "id": 12,
        "symbol": "REM",
        "label": "Remnant vegetation",
        "description": "The native vegetation remaining after a land has been modified by anthropogenic activities and or due to historical events.",
        "uri": "https://linked.data.gov.au/def/nrm/b733eae7-535a-5c08-9fcc-6e6a89b51011"
      },
      {
        "id": 13,
        "symbol": "REV",
        "label": "Revegetation",
        "description": "Revegetation is an activity that involves revegetating the landscape with plant communities in ecosystems which have previously been, partially or wholly, stripped of existing plant communities.",
        "uri": "https://linked.data.gov.au/def/nrm/f0aa6bfb-c354-5541-a83b-9e72af911aff"
      },
      {
        "id": 14,
        "symbol": "FA",
        "label": "Farmland",
        "description": "Areas of land set aside to grow crops or raise livestock.",
        "uri": "https://linked.data.gov.au/def/nrm/3bce881d-7803-57cc-9b10-f7d9700dc006"
      },
      {
        "id": 15,
        "symbol": "OR",
        "label": "Orchard",
        "description": "Refers to the type of habitat characterised by an intentional planting of trees or shrubs maintained for food, typically fruit, production.",
        "uri": "https://linked.data.gov.au/def/nrm/b779fa74-daed-5921-833a-6b032fe71475"
      },
      {
        "id": 16,
        "symbol": "PL",
        "label": "Plantation",
        "description": "An intentional planting of a crop, on a large scale, usually for uses other than cereal production or pasture. The term is currently most often used for plantings of trees and shrubs.",
        "uri": "https://linked.data.gov.au/def/nrm/83d49dd7-7efa-5bd3-9943-6289e7ca3f5d"
      },
      {
        "id": 17,
        "symbol": "PA",
        "label": "Park",
        "description": "A bounded area of land, or water, usually in its natural or semi-natural (landscaped) state and set aside for some purpose, usually to do with recreation or conservation.",
        "uri": "https://linked.data.gov.au/def/nrm/7f8250d8-c402-5a08-9fb9-877617a62233"
      },
      {
        "id": 18,
        "symbol": "GA",
        "label": "Garden",
        "description": "A garden is a planned space, usually outdoors, set aside for the cultivation, display, and enjoyment of plants and other forms of nature.",
        "uri": "https://linked.data.gov.au/def/nrm/09a64f1a-1bb4-52e2-8edb-58144bcb9cf2"
      }
    ],
    "lut-vertebrate-age-class": [
      {
        "id": 1,
        "symbol": "J",
        "label": "Juvenile",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "S",
        "label": "Sub-adult",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "A",
        "label": "Adult",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "U",
        "label": "Unknown",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vertebrate-body-condition": [
      {
        "id": 1,
        "symbol": "1",
        "label": "1",
        "description": "Emaciated - backbone, ribs and pelvis easily felt without applying pressure, no fat layer between bone and skin",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "2",
        "label": "2",
        "description": "Poor - slightly dishing or concave muscle on either side of scapula, scapula spine very obvious on palpation, edges of scapula bone palpable",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "3",
        "label": "3",
        "description": "Fair - flat to slightly convex muscle on either side of scapula, scapula spine prominent on palpation",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "4",
        "label": "4",
        "description": "Good - scapula spine is easily palpable",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "5",
        "label": "5",
        "description": "Excellent - good covering of fat, hard to fee backbone and pelvis when applying light pressure, muscle mass on either side of the scapula",
        "uri": ""
      }
    ],
    "lut-vertebrate-capture-status": [
      {
        "id": 1,
        "symbol": "N",
        "label": "New",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "R",
        "label": "Recapture",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "U",
        "label": "Unknown",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vertebrate-class": [
      {
        "id": 1,
        "symbol": "AM",
        "label": "Amphibian",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "MA",
        "label": "Mammal",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "RE",
        "label": "Reptile",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vertebrate-drift-fence-name": [
      {
        "id": 1,
        "symbol": "A",
        "label": "Trap line A",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "B",
        "label": "Drift fence B",
        "description": "Drift fence",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "C",
        "label": "Trap line C",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "D",
        "label": "Trap line D",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "E",
        "label": "Drift fence E",
        "description": "Drift fence",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "F",
        "label": "Trap line F",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vertebrate-drift-fence-type": [
      {
        "id": 1,
        "symbol": "F",
        "label": "Fibreglass/shadecloth",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "PS",
        "label": "Plastic sheet",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "RS",
        "label": "Rigid plastic",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vertebrate-permanent-id-type": [
      {
        "id": 1,
        "symbol": "MC",
        "label": "Microchip",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "ET",
        "label": "Ear-tag",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "BT",
        "label": "Bluetooth-tag",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "B",
        "label": "Band",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vertebrate-search-method": [
      {
        "id": 1,
        "symbol": "DAS",
        "label": "Diurnal active search",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "NAS",
        "label": "Nocturnal passive search",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vertebrate-search-type": [
      {
        "id": 1,
        "symbol": "A",
        "label": "Active",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "P",
        "label": "Passive",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vertebrate-sex": [
      {
        "id": 1,
        "symbol": "M",
        "label": "Male",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "F",
        "label": "Female",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "U",
        "label": "Unknown",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vertebrate-testes-position": [
      {
        "id": 1,
        "symbol": "Scrotal",
        "label": "Scrotal",
        "description": "Scrotal' - represents an obvious sack and may appear either empty or full.",
        "uri": "https://linked.data.gov.au/def/nrm/47e71470-3a77-5c15-9d54-b9a6fff840e9"
      },
      {
        "id": 2,
        "symbol": "Abdominal",
        "label": "Abdominal",
        "description": "The position of the Animal testes. 'Abdominal'- represents the scrotum is not obvious.",
        "uri": "https://linked.data.gov.au/def/nrm/18dcac46-65f6-5b8c-acb7-93ce7abc8b03"
      },
      {
        "id": 3,
        "symbol": "Unknown",
        "label": "Unknown",
        "description": "Unknown/unable to be determined.",
        "uri": "https://linked.data.gov.au/def/nrm/f6b0f6d8-16d8-5dd7-b1b7-66b0c020b96f"
      }
    ],
    "lut-vertebrate-trap-check-status": [
      {
        "id": 1,
        "symbol": "E",
        "label": "Empty",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "F",
        "label": "False trigger",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "C",
        "label": "Capture",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vertebrate-trap-specific": [
      {
        "id": 1,
        "symbol": "PVC",
        "label": "PVC pipe",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "P1",
        "label": "Plastic bucket",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "P2",
        "label": "Temporary plastic sheet folded",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "M",
        "label": "Foldable aluminium or galvanised steel ",
        "description": "i.e. Elliott and Sherman style",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "SC",
        "label": "Shadecloth",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "SCH",
        "label": "Shadecloth with hessian/other bag",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "WF",
        "label": "Wire mesh, foot plate mechanism",
        "description": "",
        "uri": ""
      },
      {
        "id": 8,
        "symbol": "WH",
        "label": "Wire mesh, hook mechanism",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vertebrate-trap-status": [
      {
        "id": 1,
        "symbol": "O",
        "label": "Trap open",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "CP",
        "label": "Trap closed - permanently",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "CT",
        "label": "Trap closed - temporarily",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "CW",
        "label": "Trap closed - wired shut",
        "description": "i.e. Elliott and Sherman style",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "TCR",
        "label": "Trap closed and removed",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "PCF",
        "label": "Pitfall closed - filled in",
        "description": "",
        "uri": ""
      },
      {
        "id": 7,
        "symbol": "PCL",
        "label": "Pitfall closed - lid secured",
        "description": "",
        "uri": ""
      }
    ],
    "lut-vertebrate-trap-type": [
      {
        "id": 1,
        "symbol": "P",
        "label": "Pitfall Trap",
        "description": "Refers to the targeted fauna observation method, i.e., any observations made from fauna captures in a pitfall trap. A pitfall trap is a simple device used to catch small animals - particularly insects and other invertebrates - that spend most of their time on the ground. ",
        "uri": "https://linked.data.gov.au/def/nrm/aa78fa0e-3bd0-5c87-8ac7-0779f0699a11"
      },
      {
        "id": 2,
        "symbol": "F",
        "label": "Funnel Trap",
        "description": "The equipment/method used in a 'targeted fauna survey'. Funnel trap is a trapping method used in trapping insects/invertebrates. Funnel traps are made of nested black funnels (up to as many as 12). Insects fall through the funnels to a cup that is filled with a preservative. ",
        "uri": "https://linked.data.gov.au/def/nrm/970058a7-946d-5a5d-bb5b-c5b801a4def4"
      },
      {
        "id": 3,
        "symbol": "B",
        "label": "Box Trap",
        "description": "Elliot Trap",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "C",
        "label": "Cage Trap",
        "description": "The equipment/method used during a passive, 'targeted fauna survey'. A cage trap is a trap made of metal or galvanised mesh and used in trapping mammals. ",
        "uri": "https://linked.data.gov.au/def/nrm/8f6764de-0f3e-5f6f-b86e-e5e9f6053e24"
      }
    ],
    "lut-vertical-resolution": [
      {
        "id": 1,
        "symbol": "LT1",
        "label": "< 1 meter",
        "description": "",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "1TO10",
        "label": "1 meter - < 10 meters",
        "description": "",
        "uri": ""
      },
      {
        "id": 3,
        "symbol": "10TO30",
        "label": "10 meters - < 30 meters",
        "description": "",
        "uri": ""
      },
      {
        "id": 4,
        "symbol": "30TO100",
        "label": "30 meters - < 100 meters",
        "description": "",
        "uri": ""
      },
      {
        "id": 5,
        "symbol": "100TO1K",
        "label": "100 meters - < 1km",
        "description": "",
        "uri": ""
      },
      {
        "id": 6,
        "symbol": "GT1K",
        "label": "> 1 km",
        "description": "",
        "uri": ""
      }
    ],
    "lut-voucher-condition": [
      {
        "id": 1,
        "symbol": "FS",
        "label": "Fresh (estimate <24 hours)",
        "description": "Refers to the condition of the animal when the voucher specimen was made.",
        "uri": "https://linked.data.gov.au/def/nrm/53691227-fec8-5518-9278-ac3e10a587a6"
      },
      {
        "id": 2,
        "symbol": "FL",
        "label": "Fresh (estimate 24 hours - <48 hours)",
        "description": "Refers to the condition of the animal when the voucher specimen was made.",
        "uri": "https://linked.data.gov.au/def/nrm/7829c93e-d5c5-59b4-abc8-3d3d62cc03f7"
      },
      {
        "id": 3,
        "symbol": "PDS",
        "label": "Partially degenerated (estimate 2 - 7 days)",
        "description": "Refers to the condition of the animal when the voucher specimen was made.",
        "uri": "https://linked.data.gov.au/def/nrm/c1ae9e5f-f1f9-517b-b665-eb6c1b1fa437"
      },
      {
        "id": 4,
        "symbol": "PDL",
        "label": "Partially degenerated (estimate 1-2 weeks)",
        "description": "Refers to the condition of the animal when the voucher specimen was made.",
        "uri": "https://linked.data.gov.au/def/nrm/f41958cf-21dc-5222-b01d-fb9d94743cfc"
      },
      {
        "id": 5,
        "symbol": "D",
        "label": "Degenerated (estimate 2 - 4 weeks)",
        "description": "Refers to the condition of the fauna specimen when collected. Degenerate is when the structural quality deteriorates or is lost over time.",
        "uri": "https://linked.data.gov.au/def/nrm/9144bc37-88a2-5551-8087-dbebd939b49a"
      },
      {
        "id": 6,
        "symbol": "LD",
        "label": "Long dead (> 4 weeks)",
        "description": "Refers to the condition of the animal when the voucher specimen was made.",
        "uri": "https://linked.data.gov.au/def/nrm/2e79ab2b-8862-554a-8163-3aaa58b3c7e8"
      },
      {
        "id": 7,
        "symbol": "U",
        "label": "Unknown",
        "description": "Unknown/unable to be determined.",
        "uri": "https://linked.data.gov.au/def/nrm/f6b0f6d8-16d8-5dd7-b1b7-66b0c020b96f"
      }
    ],
    "lut-weather-cloud-cover": [
      {
        "id": 1,
        "symbol": "CL",
        "label": "Clear",
        "description": "Little chance of the sun or moon (if occurring) being obscured by cloud. Cloud covering < ¼ of the sky. Note: High level cirrus clouds are often thin and wispy, allowing a considerable amount of light to penetrate them, sufficient to produce shadows. In this case the day could be termed 'clear' even though the sky may consist of cirrus cloud.",
        "uri": "https://linked.data.gov.au/def/nrm/b390bc2f-d2ff-52c9-a084-62afaf8371c6"
      },
      {
        "id": 2,
        "symbol": "MC",
        "label": "Mostly Clear",
        "description": "Predominantly more clear sky than cloud. Cloud covering 1/4 of the sky.",
        "uri": "https://linked.data.gov.au/def/nrm/449a90bc-53c3-5aab-b259-89a39c278d33"
      },
      {
        "id": 3,
        "symbol": "PC",
        "label": "Partly Cloudy",
        "description": "Predominantly more clear sky than cloud. Cloud covering 1/4 - 3/4 of the sky.",
        "uri": "https://linked.data.gov.au/def/nrm/7773b3e9-83e7-5057-b7d6-6a025e0b6a2b"
      },
      {
        "id": 4,
        "symbol": "C",
        "label": "Cloudy",
        "description": "Predominantly more cloud than clear sky. Cloud covering ¾ - nearly all of the sky.",
        "uri": "https://linked.data.gov.au/def/nrm/34e20339-1dc9-55f8-b466-a080c969eb70"
      },
      {
        "id": 5,
        "symbol": "O",
        "label": "Overcast",
        "description": "Sky completely covered with cloud. Cloud expected to cover all of the sky.",
        "uri": "https://linked.data.gov.au/def/nrm/94d0cbdd-9631-5bda-b70e-00b77e80fc8e"
      }
    ],
    "lut-weather-precipitation": [
      {
        "id": 1,
        "symbol": "NO",
        "label": "None observed",
        "description": "Refers to the precipitation weather code. No rain observed.",
        "uri": "https://linked.data.gov.au/def/nrm/507fab84-10d1-5153-ab14-9f6a28ce9cbc"
      },
      {
        "id": 2,
        "symbol": "SH",
        "label": "Showers",
        "description": "Showers are rainfall patterns that usually begin and end with a sudden, brief pause, relatively short-lived, but may last half an hour. Fall from cumulus clouds, often separated by blue sky. Showers may fall in patches rather than across the whole forecast area. Range in intensity from light to very heavy.",
        "uri": "https://linked.data.gov.au/def/nrm/7226696a-51a8-5c67-afa6-c65f497b25b8"
      },
      {
        "id": 3,
        "symbol": "RA",
        "label": "Rain",
        "description": "Refers to the precipitation weather code. In contrast to showers, rain is steadier and normally falls from stratiform (layer) cloud. Rain can range in intensity from light to very heavy.",
        "uri": "https://linked.data.gov.au/def/nrm/411727ee-2a7b-51b5-9988-d523d6c14589"
      },
      {
        "id": 4,
        "symbol": "DR",
        "label": "Drizzle",
        "description": "Refers to the precipitation weather code. Fairly uniform precipitation composed exclusively of very small water droplets very close to one another.",
        "uri": "https://linked.data.gov.au/def/nrm/cd065b9c-0b6d-5053-91fa-d66331f11770"
      },
      {
        "id": 5,
        "symbol": "FR",
        "label": "Frost",
        "description": "Refers to the precipitation weather code. Deposit of soft white ice crystals or frozen dew drops on objects near the ground; formed when surface temperature falls below freezing point.",
        "uri": "https://linked.data.gov.au/def/nrm/c3e4038a-0305-5efe-8c9a-c895049d01f4"
      },
      {
        "id": 6,
        "symbol": "FO",
        "label": "Fog",
        "description": "Refers to the precipitation weather code. Suspension of very small water droplets in the air, reducing visibility at ground level to less than a kilometre.",
        "uri": "https://linked.data.gov.au/def/nrm/f2033741-7f8b-502e-a702-ab3f7887f086"
      },
      {
        "id": 7,
        "symbol": "MI",
        "label": "Mist",
        "description": "Refers to the precipitation weather code. Similar to fog, but visibility remains more than a kilometre.",
        "uri": "https://linked.data.gov.au/def/nrm/db598b18-9c69-5398-b1ce-aa39517cb7ca"
      },
      {
        "id": 8,
        "symbol": "TH",
        "label": "Thunderstorms",
        "description": "Refers to the precipitation weather code. Thunderstorms are one or more convective clouds in which electrical discharge can be seen as lightning and heard as thunder by a person on the earth's surface. A severe thunderstorm produces one or more of : 1) hail at the ground with diameter of 2 cm or more; 2) wind gusts at the ground of 90 km/h or more; 3) tornadoes; or 4) very heavy rain likely to cause flash flooding.",
        "uri": "https://linked.data.gov.au/def/nrm/57ad1abd-7b0b-5e61-a0cc-81ff3f04422f"
      }
    ],
    "lut-weather-precipitation-duration": [
      {
        "id": 1,
        "symbol": "I",
        "label": "Intermittent",
        "description": "Precipitation which ceases at times",
        "uri": ""
      },
      {
        "id": 2,
        "symbol": "O",
        "label": "Occasional",
        "description": "Refers to the duration of precipitation, i.e., while not frequent, is recurrent.",
        "uri": "https://linked.data.gov.au/def/nrm/f77ad4ac-ecde-5bcc-b6be-833bb1e57f94"
      },
      {
        "id": 3,
        "symbol": "F",
        "label": "Frequent",
        "description": "Refers to the duration of precipitation- showers occurring regularly and often.",
        "uri": "https://linked.data.gov.au/def/nrm/540c6471-1223-51f5-9cef-774b98b119d6"
      },
      {
        "id": 4,
        "symbol": "C",
        "label": "Continuous",
        "description": "Refers to the duration of precipitation- precipitation which does not cease, or ceases only briefly.",
        "uri": "https://linked.data.gov.au/def/nrm/816742a6-004d-58e6-b60f-c404c73f33c7"
      },
      {
        "id": 5,
        "symbol": "P",
        "label": "Periods of rain",
        "description": "Refers to the duration of precipitation- rain falling most of the time, but with breaks.",
        "uri": "https://linked.data.gov.au/def/nrm/89e6226e-a4d5-5d5c-9813-b43a8d424b75"
      },
      {
        "id": 6,
        "symbol": "B",
        "label": "Brief",
        "description": "Refers to the duration of precipitation- short duration.",
        "uri": "https://linked.data.gov.au/def/nrm/08eed747-4ef0-5546-b032-93705178b1fd"
      }
    ],
    "lut-weather-wind": [
      {
        "id": 1,
        "symbol": "C",
        "label": "Calm",
        "description": "Refers to the type of wind- smoke rises vertically.",
        "uri": "https://linked.data.gov.au/def/nrm/82169328-a11d-5d65-ac0c-2f6f4b2c85b7"
      },
      {
        "id": 2,
        "symbol": "LW",
        "label": "Light winds",
        "description": "Refers to the type of wind- wind felt on face; leaves rustle.",
        "uri": "https://linked.data.gov.au/def/nrm/3d574a9d-51a5-5a62-83ca-0569e0495677"
      },
      {
        "id": 3,
        "symbol": "MW",
        "label": "Moderate winds",
        "description": "Refers to the type of wind- raises dust and loose paper; small branches are moved.",
        "uri": "https://linked.data.gov.au/def/nrm/aca653e3-8962-5714-ab63-36e08d734842"
      },
      {
        "id": 4,
        "symbol": "FW",
        "label": "Fresh winds",
        "description": "Describes the wind type. Fresh wind- is when small trees begin to sway.",
        "uri": "https://linked.data.gov.au/def/nrm/35d47144-41cd-538f-8e26-48a2c4def95e"
      },
      {
        "id": 5,
        "symbol": "SW",
        "label": "Strong winds",
        "description": "Refers to the type of wind- large branches in motion; umbrellas used with difficulty.",
        "uri": "https://linked.data.gov.au/def/nrm/7a7e2364-cd5c-54a4-8bbe-5dd261ecb386"
      },
      {
        "id": 6,
        "symbol": "NG",
        "label": "Near gale",
        "description": "Refers to the type of wind- whole trees in motion; inconvenience felt when walking against wind.",
        "uri": "https://linked.data.gov.au/def/nrm/d69a47c8-a36e-5eea-a4a4-5551b11d58db"
      },
      {
        "id": 7,
        "symbol": "G",
        "label": "Gale",
        "description": "Describes the wind type. Gale- is severe wind force where twigs break off trees.",
        "uri": "https://linked.data.gov.au/def/nrm/a667a542-43da-5704-8e2c-24fa3a4ae915"
      },
      {
        "id": 8,
        "symbol": "SG",
        "label": "Strong gale",
        "description": "Refers to the type of wind- slight structural damage occurs; larger branches break off.",
        "uri": "https://linked.data.gov.au/def/nrm/786f0acf-ea07-5c51-a173-f456f541e28d"
      },
      {
        "id": 9,
        "symbol": "S",
        "label": "Storm",
        "description": "Refers to the type of wind- seldom experienced inland; trees uprooted.",
        "uri": "https://linked.data.gov.au/def/nrm/391aa4cd-0bce-5afb-9be3-64fecd29c81d"
      },
      {
        "id": 10,
        "symbol": "VS",
        "label": "Violent storm",
        "description": "Refers to the type of wind- Very rarely experienced and causes widespread damage.",
        "uri": "https://linked.data.gov.au/def/nrm/9f7daec3-4fcf-5cf2-b67d-c7d93c87f4fa"
      }
    ]
  }
}
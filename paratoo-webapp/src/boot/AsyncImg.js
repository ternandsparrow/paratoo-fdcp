import { boot } from 'quasar/wrappers'
import AsyncImg from '../components/AsyncImg.vue'

export default boot(({app}) => {
  app.component('async-img', AsyncImg)
})

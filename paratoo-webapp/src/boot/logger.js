import VueLogger from 'vuejs3-logger';
const isProduction = process.env.NODE_ENV === 'production'

const options = {
  isEnabled: true,
  logLevel: isProduction ? 'error' : 'debug',
  stringifyArguments: false,
  showLogLevel: true,
  showMethodName: true,
  separator: '|',
  showConsoleColors: true,
}

export default async ({ app }) => {
  // INITIALISATIONS
  app.use(VueLogger, options)
}

import { boot } from 'quasar/wrappers'
import * as Sentry from '@sentry/vue'
import { buildGitHash, paratooWebAppBuildVersion } from 'src/misc/constants'
function envBoolCheck(envFlag) {
  if (!envFlag) return false

  const value = envFlag.toString()
  if (value == 'true') return true
  return false
}

export default boot(({ app, router }) => {
  // disable sentry on dev
  // as Sentry will hide traces that would normally appear in the console
  if (process.env.DEV) return
  if (process.env.VUE_APP_SENTRY_DSN == undefined) {
    console.warn('Could not initialise Sentry - empty DSN')
    return
  } else {
    console.log(
      'Initialising Sentry with Sentry Environment:' +
        process.env.VUE_APP_DEPLOYED_ENV_NAME,
    )

    Sentry.init({
      app,
      dsn: process.env.VUE_APP_SENTRY_DSN,
      environment: process.env.VUE_APP_DEPLOYED_ENV_NAME,
      release: `${buildGitHash}@${paratooWebAppBuildVersion}`,
      integrations: [
        new Sentry.BrowserTracing({
          routingInstrumentation: Sentry.vueRouterInstrumentation(router),
        }),
        new Sentry.Replay(),
      ],
      // sentry offline caching using index DB
      transport: Sentry.makeBrowserOfflineTransport(Sentry.makeFetchTransport),
      // queue size of indexDB
      transportOptions: { maxQueueSize: 10 },
      trackComponents: true,
      // https://stackoverflow.com/questions/73120842/suppress-sentry-logs
      // Sentry recommends lowering this amount, e.g. to 0.2 for production
      tracesSampleRate: 0.2,
      replaysOnErrorSampleRate: 0.2,
      // https://github.com/quasarframework/quasar/issues/2233
      ignoreErrors: ['ResizeObserver loop limit exceeded', 'ResizeObserver loop completed with undelivered notifications'],
      // disable when node is in localdev mode, so when developing on a local machine
      enabled: process.env.VUE_APP_DEPLOYED_ENV_NAME !== 'localdev',
      beforeSend(event, hint) {
        const error = hint.originalException
        const userAgent = event.request?.headers?.['user-agent']
        // abort if it is a bot
        if(userAgent && isBot(userAgent)) return null
        // FIXME - flesh this out with better and more relevant fingerprints as we see new exceptions occur
        if (
          error &&
          error.message &&
          error.message.match(/core unavailable/i)
        ) {
          event.fingerprint = ['core-unavailable']
        } else {
          console.log(
            'SHHH, Sentry should enrich this exception with a fingerprint ' +
              JSON.stringify(error),
          )
          console.log(event)
        }

        // Check if it is an exception, and if so, show the report dialog so users can give us direct feedback
        if (!window.Cypress && event.exception && event.level === 'error') {
          try {
            console.debug(`VUE_APP_DEPLOYED_ENV_NAME: '${JSON.stringify(process.env.VUE_APP_DEPLOYED_ENV_NAME)}'`)
          } catch {
            console.debug('Could not print VUE_APP_DEPLOYED_ENV_NAME')
          }
          if (process.env.VUE_APP_DEPLOYED_ENV_NAME != 'demo') {
            let reducedErrorMessages = null
            if (
              typeof event.exception === 'object' &&
              Array.isArray(event.exception.values)
            ) {
              reducedErrorMessages = event.exception.values.reduce((accum, curr) => {
                accum.push(curr.value)
                return accum
              }, [])
            } else {
              //it's not an object just keep as-is
              reducedErrorMessages = event.exception
            }
            // if report dialog is enabled
            const isReportDialogOn = envBoolCheck(process.env.VUE_APP_ENABLE_SENTRY_DIALOG)
            console.log(`Sentry report dialog is ${isReportDialogOn ? 'enabled' : 'disabled'}`)
            if (isReportDialogOn) {
              Sentry.showReportDialog({
                eventId: event.event_id,
                subtitle: `Our team has been notified of error: ${reducedErrorMessages}`
              })
            }
            Sentry.addBreadcrumb({
              category: 'Sentry report dialog',
              message: event,
              level: 'error',
            })
          } else {
            // FIXME: Demo release HACK
            console.log('FIXME: skipped getting user feedback on Demo, for now')
          }
        }
        return event
      },
    })
  }
})

/**
 * just a simple function to check bots from google or bing
 * @param {*} userAgent 
 * @returns 
 */
function isBot(userAgent) {
  const regex =  /bot|Googlebot|Bingbot/i
  return regex.test(userAgent) 
}
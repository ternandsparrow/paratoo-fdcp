#!/bin/bash
if [ -z "$1" ]; then
  echo "Path to key file required to build the apk file"
  exit 1
fi
keylocation="$1"
# build apt file
yarn quasar build -m capacitor -T android

# install two packages for signing the apk file
if [ ! "$(which apksigner)" ]; then
echo Install apksigner
sudo apt install apksigner
fi

if [ ! "$(which zipalign)" ]; then
echo install zipalign
sudo apt install zipalign
fi

unsignedFile=./dist/capacitor/android/apk/release/app-release-unsigned.apk
signedFile=./dist/capacitor/android/apk/release/paratoo_fdcp.apk

echo $unsignedFile
zipalign -v 4 $unsignedFile $signedFile

apksigner sign --ks $keylocation $signedFile
#!/bin/sh

URL=${HOSTNAME:-localhost}:${PORT:-3000}

if ! wget --quiet --spider $URL >/dev/null; then
  exit 1
fi

exit 0
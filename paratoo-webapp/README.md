> the webapp component of the Paratoo Field Data Collection Platform (FDCP)

# Developer quickstart
  1. clone the repo
  1. install dependencies:
      ```bash
      yarn install
      ```
  1. optionally override any config items you need by creating a `.env.local`
     file and adding overrides to values you find in `.env`. **DO NOT** just
     copy `.env` because then you'll shadow all values and will have
     pain when it comes to pulling new code that also updates the
     defaults.
     ```bash
     touch .env.local
     vim .env.local
     # DO NOT cp .env .env.local !!!
     ```
  2. override config items for `oidc`
     ```bash
     # configs for local paratoo-cas-server
     ORG_API_URL_BASE=http://localhost:1338
     ORG_API_PREFIX="/api/org"
     ORG_CUSTOM_API_PREFIX="/api/org"
     VUE_APP_ENABLE_OIDC=true
     VUE_APP_OIDC_AUTH_TYPE='cas'
     VUE_APP_OIDC_CLIENT='PARATOO CAS'
     VUE_APP_OIDC_DISCOVERY_URI='http://127.0.0.1:6060/cas/oidc/.well-known/openid-configuration'
     VUE_APP_OIDC_CLIENT_ID='test1234'
     VUE_APP_OIDC_SCOPES='openid profile email ala/attrs ala/roles'
     VUE_APP_OIDC_LOGIN_REDIRECT_URI=http://localhost:8080
     VUE_APP_OIDC_LOGOUT_REDIRECT_URI='http://127.0.0.1:6060/cas/oidc/logout?id_token_hint[equal][id_token_hint][and]redirect_uri=http%3A%2F%2Flocalhost%3A8080'
     ```
  1. start the hot-reloading dev server
      ```bash
      yarn start
      ```

# Technologies
  - Vue.js
  - Quasar

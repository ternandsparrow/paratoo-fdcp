/*
 * This file runs in a Node context (it's NOT transpiled by Babel), so use only
 * the ES6 features that are supported by your Node version. https://node.green/
 */
const dotenv = require('dotenv')
var path = require('path')
const ESLintPlugin = require('eslint-webpack-plugin')

// // We will bake this suffix string into the manifest at build-time
// var buildName

// if (process.env.CI_COMMIT_BRANCH === 'develop') {
//   buildName = 'DEV'
// } else if (process.env.CI_COMMIT_BRANCH === 'beta') {
//   buildName = 'BETA'
// } else if (process.env.CI_COMMIT_BRANCH === 'main') {
//   buildName = 'PROD'
// } else {
//   buildName = 'localdev'
// }

// Configuration for your app
// https://quasar.dev/quasar-cli/quasar-conf-js
/* eslint-env node */

module.exports = function (ctx) {
  return {
    bin: { linuxAndroidStudio: `${__dirname}/android-studio/bin/studio.sh` },
    // https://quasar.dev/quasar-cli/supporting-ts
    supportTS: false,

    // https://quasar.dev/quasar-cli/prefetch-feature
    // preFetch: true,

    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    // https://quasar.dev/quasar-cli/boot-files
    boot: ['logger', 'axios', 'sentry', 'AsyncImg'],

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-css
    css: ['app.scss'],

    // https://github.com/quasarframework/quasar/tree/dev/extras
    extras: [
      // 'ionicons-v4',
      // 'mdi-v5',
      // 'fontawesome-v5',
      // 'eva-icons',
      // 'themify',
      'line-awesome',
      // 'roboto-font-latin-ext', // this or either 'roboto-font', NEVER both!

      'roboto-font', // optional, you are not bound to it
      'material-icons', // optional, you are not bound to it
    ],

    // Full list of options: https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-build
    build: {
      env: {
        ...(function () {
          // Yes, I know there are pre-made options like:
          //   - https://github.com/quasarframework/app-extension-dotenv/
          //   - https://github.com/quasarframework/app-extension-qenv
          // ...but both of these suffer the same failing. The only have a single
          // file. We want one file that's in version control with defaults and an
          // optional second file for overrides, which is ignored by version
          // control.
          const defaultVals = dotenv.config({ path: '.env' }).parsed
          const overrideVals = dotenv.config({ path: '.env.local' }).parsed
          const overrideCypressVals = dotenv.config({
            path: '.env.cypress.local',
          }).parsed
          if (overrideCypressVals && overrideCypressVals !== '{}') {
            console.log(
              `Cypress-specific envs provided: ${JSON.stringify(
                overrideCypressVals,
              )}`,
            )
            return Object.assign(
              {},
              defaultVals,
              overrideVals,
              overrideCypressVals,
            )
          }

          //completely ignore Cypress-specific envs if not provided (don't want to grab
          //stale vars)
          return Object.assign({}, defaultVals, overrideVals)
        })(),
      },

      vueRouterMode: 'history', // available values: 'hash', 'history'

      // this is a configuration passed on to the underlying Webpack
      devtool: 'eval-source-map',

      chainWebpack(chain) {
        const nodePolyfillWebpackPlugin = require('node-polyfill-webpack-plugin')
        chain.plugin('node-polyfill').use(nodePolyfillWebpackPlugin)

        chain.module
          .rule('preprocessor')
          .test(/\.(js|vue)$/)
          .enforce('pre')
          .exclude.add(/[\\/]node_modules[\\/]/)
          .end()
          .use('webpack-preprocessor-loader')
          .loader('webpack-preprocessor-loader')
          .options({
            params: {
              capacitor: ctx.modeName == 'capacitor',
            },
            directives: {
              capacitor: ctx.modeName == 'capacitor',
            },
          })
      },

      // transpile: false,

      // Add dependencies for transpiling with Babel (Array of string/regex)
      // (from node_modules, which are by default not transpiled).
      // Applies only if "transpile" is set to true.
      transpileDependencies: ['oidc-vue'],

      // rtl: false, // https://quasar.dev/options/rtl-support
      // preloadChunks: true,
      // showProgress: false,
      // gzip: true,
      // analyze: true,

      // Options below are automatically set depending on the env, set them if you want to override
      // extractCSS: false,

      // https://quasar.dev/quasar-cli/handling-webpack
      extendWebpack(cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules/,
        })
        cfg.resolve.alias = {
          ...cfg.resolve.alias,
          '@': path.resolve(__dirname, './src'),
        }
        cfg.module.rules.push({
          test: /\.csv$/,
          loader: 'csv-loader',
          options: {
            dynamicTyping: true,
            header: true,
            skipEmptyLines: true,
          },
        })
        if (process.env.SENTRY_UPLOAD === 'enabled') {
          const { sentryWebpackPlugin } = require('@sentry/webpack-plugin')
          const sentryPluginInstance = sentryWebpackPlugin({
            authToken: process.env.SENTRY_AUTH_TOKEN,
            org: 'emg-bp',
            project: 'paratoo-app',
            ignore: ['node_modules', 'webpack.config.js', 'tests'],
          })
          cfg.plugins.push(sentryPluginInstance)
        }
      },
    },

    // Full list of options: https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-devServer
    devServer: {
      // before(app) {
      //   const cors = require('cors')
      //   app.use(cors())
      // },
      https: false,
      port: 8080,
      open: false,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      client: {
        webSocketURL: 'auto://0.0.0.0:0/ws',
      },
    },

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-framework
    framework: {
      iconSet: 'material-icons', // Quasar icon set
      lang: 'en-US', // Quasar language pack
      config: {},

      // Possible values for "importStrategy":
      // * 'auto' - (DEFAULT) Auto-import needed Quasar components & directives
      // * 'all'  - Manually specify what to import
      importStrategy: 'auto',

      // For special cases outside of where "auto" importStrategy can have an impact
      // (like functional components as one of the examples),
      // you can manually specify Quasar components/directives to be available everywhere:
      //
      // components: [],
      // directives: [],

      // Quasar plugins
      plugins: ['Notify', 'Loading', 'Dialog'],
      cssAddon: true,
    },

    // animations: 'all', // --- includes all animations
    // https://quasar.dev/options/animations
    animations: 'all',

    // https://quasar.dev/quasar-cli/developing-ssr/configuring-ssr
    ssr: {
      pwa: false,
    },

    // https://quasar.dev/quasar-cli/developing-pwa/configuring-pwa
    pwa: {
      workboxPluginMode: 'InjectManifest', // 'GenerateSW' or 'InjectManifest'
      workboxOptions: {
        maximumFileSizeToCacheInBytes: 5000000000000,
      },
      appleMobileWebAppCapable: true,
      appleMobileWebAppCache: true,

      chainWebpackCustomSW(chain) {
        chain
          .plugin('eslint-webpack-plugin')
          .use(ESLintPlugin, [{ extensions: ['js'] }])
      },

      manifest: {
        name: `Monitor Field Data Collection`,
        short_name: `Monitor`,
        description: `A platform for field data collection`,
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          // {
          //   src: 'icons/icon-128x128.png',
          //   sizes: '128x128',
          //   type: 'image/png',
          // },
          // {
          //   src: 'icons/icon-192x192.png',
          //   sizes: '192x192',
          //   type: 'image/png',
          // },
          // {
          //   src: 'icons/icon-256x256.png',
          //   sizes: '256x256',
          //   type: 'image/png',
          // },
          // {
          //   src: 'icons/icon-384x384.png',
          //   sizes: '384x384',
          //   type: 'image/png',
          // },
          {
            src: 'icons/icon-512x512.png',
            sizes: '512x512',
            type: 'image/png',
          },
        ],
        permissions: ["notifications"]
      },
    },

    // Full list of options: https://quasar.dev/quasar-cli/developing-cordova-apps/configuring-cordova
    cordova: {
      // noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing
    },

    // Full list of options: https://quasar.dev/quasar-cli/developing-capacitor-apps/configuring-capacitor
    capacitor: {
      hideSplashscreen: true,
    },

    // Full list of options: https://quasar.dev/quasar-cli/developing-electron-apps/configuring-electron
    electron: {
      bundler: 'packager', // 'packager' or 'builder'

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options
        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',
        // Windows only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration

        appId: 'monitor-fdcp-webapp',
      },

      // More info: https://quasar.dev/quasar-cli/developing-electron-apps/node-integration
      nodeIntegration: true,

      extendWebpack(/* cfg */) {
        // do something with Electron main process Webpack cfg
        // chainWebpack also available besides this extendWebpack
      },
    },
  }
}

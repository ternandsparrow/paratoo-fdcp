/* eslint-disable no-useless-escape */
// key is available in the gitlab ci/cd environment
const recordCloud = `--record --key ${process.env.CYPRESS_RECORD_KEY}` 
// cant return key directly so it gets output to the stout (which isn't actually to the logs in the pipeline) 
console.log(recordCloud)

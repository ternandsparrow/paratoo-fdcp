# Paratoo-FDCP Integration Test Cases
>TODO Kira: write more guidelines on specificity of test cases

If you write a test case, make yourself as an author.<br>
If you edit/ammend a test case, append your name to the list of authors
<br><br>

**Ensure correct usage of 'authorize' and 'authenticate'.<br>**
*Authorize* means to give access or permission to something. It is usually associated with access control or client privilege.
*Authenticate* means to validate the identity of a user. It is usually associated with login functionalities.

## Test Cases

<!-- TODO add specifics when unit tests are completed for these examples -->

<!-- |                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | clientapp-create-account-001 |
| Author            | Michael |
| Cross-reference   | (webapp new account),  |
| Title             | Successful Creation of Account Procedures |
| Description       | Tests the ability of the client-application, paratoo-org, to successfully create an account and redirect the user to their account page |
| Initial Condition | User has their intended login credentials, user is on the account creation page |
| Testing Steps     | client enters intended credentials into the account creation page, web application confirms this with paratoo-org, paratoo-org sends success status to client-app, client-app directs user to intended page |
| Success Criteria  | Client is directed to their new account page |
| Data              | \<data> | -->

<br>

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | login-process-001 |
| Author            | Michael |
| Cross-reference   | (login), Org-Intfc-GET-UserProj-002, (client app read/display list) |
| Title             | Process Login Procedures |
| Description       | Tests the ability of the client-application, paratoo-org of logging in, getting the JWT, and returning a list of the relevant projects to the client-app |
| Initial Condition | User has account and credentials |
| Testing Steps     | client logs in with client-app to paratoo-org and client-app gets authentication token, client-app GETs list of user projects. List of projects is returned in JSON form to app, JSON is displayed on the web-app |
| Success Criteria  | All of the relevant projects are retreived without error |
| Data              | \<data> |

<br>


# Appendicies
## Appendix 1: Code for Tests

## Appendix 2: Template:
>TODO will this template work for integrations tests?<br>
>(its the template from the units tests)

|                   |                                                          |
|-------------------|----------------------------------------------------------|
| Test Case ID      | \<id> |
| Author            | \<author-1>, \<author-2>, etc. |
| Cross-reference   | \<unit-test-id-cross-reference> |
| Title             | \<title> |
| Description       | \<description> |
| Initial Condition | \<init-cond.> |
| Testing Steps     | \<steps> |
| Success Criteria  | \<criteria> |
| Data              | \<data> |

>TODO specify format of test case ID's like in the unit test cases files